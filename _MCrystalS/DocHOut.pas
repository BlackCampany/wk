unit DocHOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar;

type
  TfmDocsOutH = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOut: TcxGrid;
    ViewDocsOut: TcxGridDBTableView;
    LevelDocsOut: TcxGridLevel;
    amDocsOut: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCardsOut: TcxGridDBTableView;
    PopupMenu1: TPopupMenu;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    teInLn: TdxMemData;
    teInLnDepart: TIntegerField;
    teInLnDateInvoice: TIntegerField;
    teInLnNumber: TStringField;
    teInLnCliName: TStringField;
    teInLnCode: TIntegerField;
    teInLniM: TIntegerField;
    teInLnNdsSum: TFloatField;
    teInLnNdsProc: TFloatField;
    teInLnKol: TFloatField;
    teInLnKolMest: TFloatField;
    teInLnKolWithMest: TFloatField;
    teInLnCenaTovar: TFloatField;
    teInLnNewCenaTovar: TFloatField;
    teInLnProc: TFloatField;
    teInLnSumCenaTovar: TFloatField;
    teInLnSumNewCenaTovar: TFloatField;
    teInLnCodeTara: TIntegerField;
    teInLnCenaTara: TFloatField;
    teInLnSumCenaTara: TFloatField;
    teInLnDepName: TStringField;
    teInLnCardName: TStringField;
    teInLniCat: TIntegerField;
    teInLniTop: TIntegerField;
    teInLniNov: TIntegerField;
    dsteInLn: TDataSource;
    ViewCardsOutDepart: TcxGridDBColumn;
    ViewCardsOutDepName: TcxGridDBColumn;
    ViewCardsOutDateInvoice: TcxGridDBColumn;
    ViewCardsOutNumber: TcxGridDBColumn;
    ViewCardsOutCliName: TcxGridDBColumn;
    ViewCardsOutCode: TcxGridDBColumn;
    ViewCardsOutCardName: TcxGridDBColumn;
    ViewCardsOutiM: TcxGridDBColumn;
    ViewCardsOutKol: TcxGridDBColumn;
    ViewCardsOutKolMest: TcxGridDBColumn;
    ViewCardsOutKolWithMest: TcxGridDBColumn;
    ViewCardsOutCenaTovar: TcxGridDBColumn;
    ViewCardsOutNewCenaTovar: TcxGridDBColumn;
    ViewCardsOutSumCenaTovar: TcxGridDBColumn;
    ViewCardsOutSumNewCenaTovar: TcxGridDBColumn;
    ViewCardsOutProc: TcxGridDBColumn;
    ViewCardsOutNdsProc: TcxGridDBColumn;
    ViewCardsOutNdsSum: TcxGridDBColumn;
    ViewCardsOutCodeTara: TcxGridDBColumn;
    ViewCardsOutCenaTara: TcxGridDBColumn;
    ViewCardsOutSumCenaTara: TcxGridDBColumn;
    ViewCardsOutiCat: TcxGridDBColumn;
    ViewCardsOutiTop: TcxGridDBColumn;
    ViewCardsOutiNov: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    acChangePost: TAction;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teInLniMaker: TIntegerField;
    teInLnsMaker: TStringField;
    ViewCardsOutsMaker: TcxGridDBColumn;
    PopupMenu2: TPopupMenu;
    N13: TMenuItem;
    teBuffLn: TdxMemData;
    teBuffLniDep: TIntegerField;
    teBuffLnNumDoc: TStringField;
    teBuffLnCode: TIntegerField;
    teInLnV11: TIntegerField;
    ViewCardsOutV11: TcxGridDBColumn;
    teBuffLniMaker: TIntegerField;
    teBuffLnsMaker: TStringField;
    teBuffLniDateDoc: TIntegerField;
    teInLnVolIn: TFloatField;
    teInLnVol: TFloatField;
    teInLniMakerCard: TIntegerField;
    teInLnsMakerCard: TStringField;
    ViewCardsOutVol: TcxGridDBColumn;
    ViewCardsOutVolIn: TcxGridDBColumn;
    ViewCardsOutsMakerCard: TcxGridDBColumn;
    ViewDocsOutIDSKL: TcxGridDBColumn;
    ViewDocsOutID: TcxGridDBColumn;
    ViewDocsOutDATEDOC: TcxGridDBColumn;
    ViewDocsOutNUMDOC: TcxGridDBColumn;
    ViewDocsOutDATESF: TcxGridDBColumn;
    ViewDocsOutNUMSF: TcxGridDBColumn;
    ViewDocsOutIDCLI: TcxGridDBColumn;
    ViewDocsOutSUMIN: TcxGridDBColumn;
    ViewDocsOutSUMUCH: TcxGridDBColumn;
    ViewDocsOutSUMTAR: TcxGridDBColumn;
    ViewDocsOutIACTIVE: TcxGridDBColumn;
    ViewDocsOutNAMECLI: TcxGridDBColumn;
    ViewDocsOutNAMESKL: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure acTestMoveDocExecute(Sender: TObject);
    procedure ViewDocsOutCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOprihExecute(Sender: TObject);
    procedure acRazrubExecute(Sender: TObject);
    procedure acCheckBuhExecute(Sender: TObject);
    procedure acChangePostExecute(Sender: TObject);
    procedure acChangeTypePolExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
    procedure ViewCardsOutSelectionChanged(Sender: TcxCustomGridTableView);
    procedure acMoveCardExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc2(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

Function OprDocsOut(IDH:integer; Memo1:TcxMemo):Boolean;
Function OtkatDocsOut(IDH:integer; Memo1:TcxMemo):Boolean;

var
  fmDocsOutH: TfmDocsOutH;
  bClearDocIn:Boolean = false;
  bOpenSpOut:Boolean = False;

implementation

uses Un1, DM, Period1, MainMC, DocSIn, DocSOut;

{$R *.dfm}

procedure TfmDocsOutH.prSetValsAddDoc2(iT:SmallINt); //0-����������, 1-��������������, 2-��������
// var user:string;
begin
  with dmMCS do
  begin
    bOpenSpOut:=True;

    if iT=0 then
    begin
      fmDocSOut.Caption:='���������: ����������.';

      fmDocSOut.Tag:=0;

      fmDocSOut.cxTextEdit1.Text:=fGetNumDoc(fmMainMC.Label3.tag,2);  fmDocSOut.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocSOut.cxTextEdit2.Text:='';  fmDocSOut.cxTextEdit2.Properties.ReadOnly:=False;

      fmDocSOut.cxDateEdit1.Date:=date;  fmDocSOut.cxDateEdit1.Properties.ReadOnly:=False;
      fmDocSOut.cxDateEdit2.Date:=date;  fmDocSOut.cxDateEdit2.Properties.ReadOnly:=False;

      fmDocSOut.cxButtonEdit1.Tag:=0;
      fmDocSOut.cxButtonEdit1.EditValue:=0;
      fmDocSOut.cxButtonEdit1.Text:='';
      fmDocSOut.cxButtonEdit1.Properties.ReadOnly:=True;

      fmDocSOut.Label17.Caption:='-';
      fmDocSOut.Label17.Tag:=0;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',fmMainMC.Label3.tag,[]);

      fmDocSOut.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocSOut.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocSOut.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocSOut.cxLabel5.Enabled:=True;
      fmDocSOut.cxLabel1.Enabled:=True;
      fmDocSOut.cxLabel2.Enabled:=True;
      fmDocSOut.cxLabel6.Enabled:=True;
      fmDocSOut.cxLabel3.Enabled:=True;

      fmDocSOut.acSaveDoc.Enabled:=True;
      fmDocSOut.cxButton1.Enabled:=True;
      fmDocSOut.cxButton7.Enabled:=True;

      fmDocSOut.ViewDoc2.OptionsData.Editing:=True;
      fmDocSOut.ViewDoc2.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmDocSOut.Caption:='���������: ��������������.';

      fmDocSOut.Tag:=quTTNOutID.AsInteger;

      fmDocSOut.cxTextEdit1.Text:=quTTNOutNUMDOC.AsString;  fmDocSOut.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocSOut.cxTextEdit2.Text:=quTTNOutNUMSF.AsString;  fmDocSOut.cxTextEdit2.Properties.ReadOnly:=False;

      fmDocSOut.cxDateEdit1.Date:=quTTNOutDATEDOC.AsDateTime;  fmDocSOut.cxDateEdit1.Properties.ReadOnly:=False;
      fmDocSOut.cxDateEdit2.Date:=quTTNOutDATESF.AsDateTime;  fmDocSOut.cxDateEdit2.Properties.ReadOnly:=False;

      fmDocSOut.cxButtonEdit1.Tag:=quTTNOutIDCLI.AsInteger;
      fmDocSOut.cxButtonEdit1.EditValue:=quTTNOutIDCLI.AsInteger;
      fmDocSOut.cxButtonEdit1.Text:=quTTNOutNAMECLI.AsString;
      fmDocSOut.cxButtonEdit1.Properties.ReadOnly:=False;

      fmDocSOut.Label17.Tag:=quTTNOutPayNDS.AsInteger;
      if fmDocSOut.Label17.Tag=1 then fmDocSOut.Label17.Caption:='���������� ���' else fmDocSOut.Label17.Caption:='-';

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNOutIDSKL.AsInteger,[]);

      fmDocSOut.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocSOut.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocSOut.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocSOut.cxLabel5.Enabled:=True;
      fmDocSOut.cxLabel1.Enabled:=True;
      fmDocSOut.cxLabel2.Enabled:=True;
      fmDocSOut.cxLabel6.Enabled:=True;
      fmDocSOut.cxLabel3.Enabled:=True;

      fmDocSOut.acSaveDoc.Enabled:=True;
      fmDocSOut.cxButton1.Enabled:=True;
      fmDocSOut.cxButton7.Enabled:=True;

      fmDocSOut.ViewDoc2.OptionsData.Editing:=True;
      fmDocSOut.ViewDoc2.OptionsData.Deleting:=True;
    end;

    if iT=2 then
    begin
      fmDocSOut.Caption:='���������: ��������.';

      fmDocSOut.Tag:=quTTNOutID.AsInteger;

      fmDocSOut.cxTextEdit1.Text:=quTTNOutNUMDOC.AsString;  fmDocSOut.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocSOut.cxTextEdit2.Text:=quTTNOutNUMSF.AsString;  fmDocSOut.cxTextEdit2.Properties.ReadOnly:=True;

      fmDocSOut.cxDateEdit1.Date:=quTTNOutDATEDOC.AsDateTime;  fmDocSOut.cxDateEdit1.Properties.ReadOnly:=True;
      fmDocSOut.cxDateEdit2.Date:=quTTNOutDATESF.AsDateTime;  fmDocSOut.cxDateEdit2.Properties.ReadOnly:=True;

      fmDocSOut.cxButtonEdit1.Tag:=quTTNOutIDCLI.AsInteger;
      fmDocSOut.cxButtonEdit1.EditValue:=quTTNOutIDCLI.AsInteger;
      fmDocSOut.cxButtonEdit1.Text:=quTTNOutNAMECLI.AsString;
      fmDocSOut.cxButtonEdit1.Properties.ReadOnly:=True;

      fmDocSOut.Label17.Caption:='-';
      fmDocSOut.Label17.Tag:=0;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNOutIDSKL.AsInteger,[]);

      fmDocSOut.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocSOut.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocSOut.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmDocSOut.cxLabel5.Enabled:=False;
      fmDocSOut.cxLabel1.Enabled:=False;
      fmDocSOut.cxLabel2.Enabled:=False;
      fmDocSOut.cxLabel6.Enabled:=False;
      fmDocSOut.cxLabel3.Enabled:=False;

      fmDocSOut.acSaveDoc.Enabled:=False;
      fmDocSOut.cxButton1.Enabled:=False;
      fmDocSOut.cxButton7.Enabled:=False;

      fmDocSOut.ViewDoc2.OptionsData.Editing:=False;
      fmDocSOut.ViewDoc2.OptionsData.Deleting:=False;
    end;

  end;
end;

procedure TfmDocsOutH.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocsOutH.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOutH.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=sFormIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOut.Align:=AlClient;
  ViewDocsOut.RestoreFromIniFile(sGridIni);
  ViewCardsOut.RestoreFromIniFile(sGridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsOutH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsOut.StoreToIniFile(sGridIni,False);
  ViewCardsOut.StoreToIniFile(sGridIni,False);
end;

procedure TfmDocsOutH.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      if LevelDocsOut.Visible then
      begin
        fmDocsOutH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocsOutH.ViewDocsOut.BeginUpdate;
        try
          quTTNOut.Active:=False;
          quTTNOut.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
          quTTNOut.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
          quTTNOut.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
          quTTNOut.Active:=True;
        finally
          fmDocsOutH.ViewDocsOut.EndUpdate;
        end;

      end else
      begin
        fmDocsOutH.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ...');

        ViewCardsOut.BeginUpdate;

        ViewCardsOut.EndUpdate;
        Memo1.Lines.Add('������������ ��.');
      end;
    end;
  end;
end;

procedure TfmDocsOutH.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDoc2(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmDocSOut.taSpecOut);
    fmDocSOut.acSaveDoc.Enabled:=True;
    fmDocSOut.Show;
  end;
end;

procedure TfmDocsOutH.acEditDoc1Execute(Sender: TObject);
//Var //IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //�������������
  if not CanDo('prEditDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� �������������
    begin
      if quTTNOutIACTIVE.AsInteger<=1 then
      begin
        prSetValsAddDoc2(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmDocSOut.taSpecOut);
        fmDocSOut.acSaveDoc.Enabled:=True;

        fmDocSOut.ViewDoc2.BeginUpdate;

        quSpecOut.Active:=False;
        quSpecOut.Parameters.ParamByName('IDH').Value:=quTTNOutID.AsInteger;
        quSpecOut.Parameters.ParamByName('IDSKL').Value:=quTTNOutIDSKL.AsInteger;  //��� ���������� ���
        quSpecOut.Active:=True;

        quSpecOut.First;
        while not quSpecOut.Eof do
        begin
          with fmDocSOut do
          begin
            taSpecOut.Append;
            taSpecOutNum.AsInteger:=quSpecOutNUM.AsInteger;
            taSpecOutCodeTovar.AsInteger:=quSpecOutIDCARD.AsInteger;
            taSpecOutCodeEdIzm.AsInteger:=quSpecOutIDM.AsInteger;
            taSpecOutBarCode.AsString:=quSpecOutSBAR.AsString;
            taSpecOutNDSProc.AsFloat:=quSpecOutNDS.AsFloat;
            taSpecOutNDSSumIn.AsFloat:=quSpecOutSUMNDSIN.AsFloat;
            taSpecOutKol.AsFloat:=quSpecOutQUANT.AsFloat;
            taSpecOutName.AsString:=quSpecOutNAME.AsString;
            taSpecOutTovarType.AsInteger:=quSpecOutTTOVAR.AsInteger;
            taSpecOutPriceIn.AsFloat:=quSpecOutPRICEIN.AsFloat;
            taSpecOutPriceIn0.AsFloat:=quSpecOutPRICEIN0.AsFloat;
            taSpecOutSumIn.AsFloat:=quSpecOutSUMIN.AsFloat;
            taSpecOutSumIn0.AsFloat:=quSpecOutSUMIN0.AsFloat;
            taSpecOutPriceR.AsFloat:=quSpecOutPRICER.AsFloat;
            taSpecOutPriceR0.AsFloat:=quSpecOutPRICER0.AsFloat;
            taSpecOutSumR0.AsFloat:=quSpecOutSUMR0.AsFloat;
            taSpecOutSumR.AsFloat:=quSpecOutSUMR.AsFloat;
            taSpecOutVolDL.AsFloat:=quSpecOutVOL.AsFloat*quSpecOutQUANT.AsFloat;
            taSpecOutNDSSumOut.AsFloat:=quSpecOutSUMNDSOUT.AsFloat;
            taSpecOutiMaker.AsInteger:=quSpecOutMAKER.AsInteger;
            taSpecOutRemn.AsFloat:=prFindRemnDepD(quSpecOutIDCARD.AsInteger,quTTNOutIDSKL.AsInteger,Trunc(date));
            taSpecOutsMaker.AsString:=prFindMaker(quSpecOutMAKER.AsInteger);
            taSpecOutAVid.AsInteger:=quSpecOutAVID.AsInteger;
            taSpecOutVol.AsFloat:=quSpecOutVOL.AsFloat;
            taSpecOut.Post;
          end;
          quSpecOut.Next;
        end;
        fmDocSOut.ViewDoc2.EndUpdate;

        fmDocSOut.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsOutH.acViewDoc1Execute(Sender: TObject);
//Var //iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //��������
  if not CanDo('prViewDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc2(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmDocSOut.taSpecOut);
      fmDocSOut.acSaveDoc.Enabled:=False;

      fmDocSOut.ViewDoc2.BeginUpdate;

      quSpecOut.Active:=False;
      quSpecOut.Parameters.ParamByName('IDH').Value:=quTTNOutID.AsInteger;
      quSpecOut.Parameters.ParamByName('IDSKL').Value:=quTTNOutIDSKL.AsInteger;  //��� ���������� ���
      quSpecOut.Active:=True;

      quSpecOut.First;
      while not quSpecOut.Eof do
      begin
        with fmDocSOut do
        begin
          taSpecOut.Append;
          taSpecOutNum.AsInteger:=quSpecOutNUM.AsInteger;
          taSpecOutCodeTovar.AsInteger:=quSpecOutIDCARD.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quSpecOutIDM.AsInteger;
          taSpecOutBarCode.AsString:=quSpecOutSBAR.AsString;
          taSpecOutNDSProc.AsFloat:=quSpecOutNDS.AsFloat;
          taSpecOutNDSSumIn.AsFloat:=quSpecOutSUMNDSIN.AsFloat;
          taSpecOutKol.AsFloat:=quSpecOutQUANT.AsFloat;
          taSpecOutName.AsString:=quSpecOutNAME.AsString;
          taSpecOutTovarType.AsInteger:=quSpecOutTTOVAR.AsInteger;
          taSpecOutPriceIn.AsFloat:=quSpecOutPRICEIN.AsFloat;
          taSpecOutPriceIn0.AsFloat:=quSpecOutPRICEIN0.AsFloat;
          taSpecOutSumIn.AsFloat:=quSpecOutSUMIN.AsFloat;
          taSpecOutSumIn0.AsFloat:=quSpecOutSUMIN0.AsFloat;
          taSpecOutPriceR.AsFloat:=quSpecOutPRICER.AsFloat;
          taSpecOutPriceR0.AsFloat:=quSpecOutPRICER0.AsFloat;
          taSpecOutSumR0.AsFloat:=quSpecOutSUMR0.AsFloat;
          taSpecOutSumR.AsFloat:=quSpecOutSUMR.AsFloat;
          taSpecOutVolDL.AsFloat:=quSpecOutVOL.AsFloat*quSpecOutQUANT.AsFloat;
          taSpecOutNDSSumOut.AsFloat:=quSpecOutSUMNDSOUT.AsFloat;
          taSpecOutiMaker.AsInteger:=quSpecOutMAKER.AsInteger;
          taSpecOutRemn.AsFloat:=prFindRemnDepD(quSpecOutIDCARD.AsInteger,quTTNOutIDSKL.AsInteger,Trunc(date));
          taSpecOutsMaker.AsString:=prFindMaker(quSpecOutMAKER.AsInteger);
          taSpecOutAVid.AsInteger:=quSpecOutAVID.AsInteger;
          taSpecOutVol.AsFloat:=quSpecOutVOL.AsFloat;
          taSpecOut.Post;
        end;
        quSpecOut.Next;
      end;
      fmDocSOut.ViewDoc2.EndUpdate;

      fmDocSOut.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsOutH.ViewDocsOutDblClick(Sender: TObject);
begin
  //������� �������
  with dmMCS do
  begin
    if quTTnOut.RecordCount>0 then
    begin
      if quTTnOutIACTIVE.AsInteger<3 then acEditDoc1.Execute //��������������
      else acViewDoc1.Execute; //��������}
    end;
  end;
end;

procedure TfmDocsOutH.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMCS do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� �������
    begin
      if quTTNOutIACTIVE.AsInteger<3 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTNOutNUMDOC.AsString+' �� '+quTTNOutNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          prAddHist(2,quTTNOutID.AsInteger,trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('DELETE FROM [dbo].[DOCOUT_HEAD]');
          quD.SQL.Add('where IDSKL='+its(quTTNOutIDSKL.AsInteger));
          quD.SQL.Add('and IDATEDOC='+its(quTTNOutIDATEDOC.AsInteger));
          quD.SQL.Add('and ID='+its(quTTNOutID.AsInteger));
          quD.ExecSQL;
          delay(100);

          quTTNOut.Requery();
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsOutH.acOnDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  with dmMCS do
  begin
    if not CanDo('prOnDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    if quTTnOut.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnOutIACTIVE.AsInteger<=1 then
      begin
        if quTTnOutDATEDOC.AsDateTime<prOpenDate(quTTnOutIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quTTnOutIDSKL.AsInteger,quTTnOutIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quTTnOutIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;

        if quTTnOutIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('������������ �������� �'+quTTnOutNUMDOC.AsString+' �� '+quTTnOutNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTnOutID.AsInteger;
          if OprDocsOut(IDH,Memo1) then
          begin
            ViewDocsOut.BeginUpdate;
            quTTnOut.Requery();
            quTTnOut.Locate('ID',IDH,[]);
            ViewDocsOut.EndUpdate;

            prWrLog(quTTNOutIDSKL.AsInteger,2,1,IDH,'');

            GridDocsOut.SetFocus;
            ViewDocsOut.Controller.FocusRecord(ViewDocsOut.DataController.FocusedRowIndex,True);
          end;  
        end;
      end;
    end;
  end;
end;

procedure TfmDocsOutH.acOffDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
//��������
  with dmMCS do
  begin
    if not CanDo('prOffDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    if quTTnOut.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnOutIACTIVE.AsInteger=3 then
      begin
        if quTTnOutDATEDOC.AsDateTime<=prOpenDate(quTTnOutIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quTTnOutIDSKL.AsInteger,quTTnOutIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quTTnOutIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, �������� ����������!!!');
          exit;
        end;

        if quTTnOutIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('�������� �������� �'+quTTnOutNUMDOC.AsString+' �� '+quTTnOutNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTnOutID.AsInteger;
          if OtkatDocsOut(IDH,fmDocsOutH.Memo1) then
          begin
            ViewDocsOut.BeginUpdate;
            quTTnOut.Requery();
            quTTnOut.Locate('ID',IDH,[]);
            ViewDocsOut.EndUpdate;

            prWrLog(quTTNOutIDSKL.AsInteger,2,0,IDH,'');

            GridDocsOut.SetFocus;
            ViewDocsOut.Controller.FocusRecord(ViewDocsOut.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;


  end;
end;

procedure TfmDocsOutH.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsOutH.acVidExecute(Sender: TObject);
//Var iC,iCliCB:INteger;
//    Sinn:String;
begin
  //���
  with dmMCS do
  begin
    {
    if LevelDocsIn.Visible then
    begin
      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;
      Gr1.Visible:=True;

      fmDocsInH.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      ViewCards.BeginUpdate;
      dsteInLn.DataSet:=nil;

      CloseTe(teInLn);
      if ptInLn.Active=False then ptINLn.Active:=True;

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ...');

      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          Memo1.Lines.Add('   '+OemToAnsiConvert(ptDepName.AsString)); delay(10);
          ptInLn.CancelRange;
          ptInLn.SetRange([ptDepID.AsInteger,CommonSet.DateBeg],[ptDepID.AsInteger,CommonSet.DateEnd]);
          ptInLn.First;

          iC:=0;
          while not ptInLn.Eof do
          begin
            if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
            begin
              teInLn.Append;
              teInLnDepart.AsInteger:=ptInLnDepart.AsInteger;
              teInLnDepName.AsString:=OemToAnsiConvert(ptDepName.AsString);
              teInLnDateInvoice.AsInteger:=Trunc(ptInLnDateInvoice.AsDateTime);
              teInLnNumber.AsString:=OemToAnsiConvert(ptInLnNumber.AsString);
              teInLnCliName.AsString:=prFindCliName1(ptInLnIndexPost.AsInteger,ptInLnCodePost.AsInteger,Sinn,iCliCB);
              teInLnCode.AsInteger:=ptInLnCodeTovar.AsInteger;
              teInLnCardName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teInLniM.AsInteger:=ptInLnCodeEdIzm.AsInteger;
              teInLnKol.AsFloat:=ptInLnKol.AsFloat;
              teInLnKolMest.AsFloat:=ptInLnKolMest.AsFloat;
              teInLnKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
              teInLnCenaTovar.AsFloat:=ptInLnCenaTovar.AsFloat;
              teInLnNewCenaTovar.AsFloat:=ptInLnNewCenaTovar.AsFloat;
              teInLnSumCenaTovar.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
              teInLnSumNewCenaTovar.AsFloat:=ptInLnSumCenaTovar.AsFloat;
              teInLnProc.AsFloat:=ptInLnProcent.AsFloat;
              teInLnNdsProc.AsFloat:=ptInLnNDSProc.AsFloat;
              teInLnNdsSum.AsFloat:=ptInLnNDSSum.AsFloat;
              teInLnCodeTara.AsInteger:=ptInLnCodeTara.AsInteger;
              teInLnCenaTara.AsFloat:=ptInLnCenaTara.AsFloat;
              teInLnSumCenaTara.AsFloat:=ptInLnSumCenaTara.AsFloat;
              teInLniCat.AsInteger:=taCardsV02.AsInteger;
              teInLniTop.AsInteger:=taCardsV04.AsInteger;
              teInLniNov.AsInteger:=taCardsV05.AsInteger;
              teInLniMaker.AsInteger:=Trunc(ptInLnSumVesTara.AsFloat);
              teInLnsMaker.AsString:=prFindMakerName(Trunc(ptInLnSumVesTara.AsFloat));
              teInLnV11.AsInteger:=taCardsV11.AsInteger;
              teInLnVol.AsFloat:=taCardsV10.AsInteger/1000;
              teInLnVolIn.AsFloat:=(taCardsV10.AsInteger/1000)*ptInLnKol.AsFloat;
              teInLniMakerCard.AsInteger:=taCardsV12.AsInteger;
              teInLnsMakerCard.AsString:=prFindMakerName(taCardsV12.AsInteger);
              teInLn.Post;
            end;

            ptInLn.Next;
            inc(iC);
            if (iC mod 100 = 0) then
            begin
              StatusBar1.Panels[0].Text:=its(iC); delay(10);
            end;
          end;
        end;
        ptDep.Next;
      end;

      dsteInLn.DataSet:=teInLn;

      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

      Memo1.Lines.Add('������������ ��.');

    end else
    begin
      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;
      Gr1.Visible:=False;

      fmDocsInH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      fmDocsInH.ViewDocsIn.BeginUpdate;
      try
        quTTNOut.Active:=False;
        quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
        quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
        quTTNOut.Active:=True;
      finally
        fmDocsInH.ViewDocsIn.EndUpdate;
      end;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;
    end;}
  end;
end;

procedure TfmDocsOutH.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOutH.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsIn.Visible=False then exit;
  with dmMCS do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTNOutID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTNOutNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTNOutNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTNOutDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTNOutNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocsOutH.acCopyExecute(Sender: TObject);
//var Par:Variant;
//    iC:INteger;
begin
  //����������
  with dmMCS do
  begin
    {
    if ViewDocsIn.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTNOutID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTNOutID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTNOutDateInvoice.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTNOutNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTNOutCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTNOutNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTNOutDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTNOutNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTNOutSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTNOutSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTNOutIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTNOutSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      iC:=1;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTNOutID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecInCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecInKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInNewCenaTovar.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSumCenaTovar.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecInNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecInNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecInCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=quSpecInProcent.AsFloat;
        taSpecDocIdTara.AsInteger:=quSpecInCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecInCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecInBarCode.AsString;
        taSpecDoc.Post;

        quSpecIn.Next;   inc(iC);
      end;
      quSpecIn.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocsOutH.acInsertDExecute(Sender: TObject);
//Var StrWk:String;
//    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMCS do
  begin
    {
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocSOut.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmDocSOut.Label4.Tag:=taHeadDociTypeCli.AsInteger;

          if prCliNDS(taHeadDociTypeCli.AsInteger,taHeadDocIdCli.AsInteger,0)=1 then
          begin
            fmDocSOut.Label17.Caption:='���������� ���';
            fmDocSOut.Label17.Tag:=1;
          end else
          begin
            fmDocSOut.Label17.Caption:='�� ���������� ���';
            fmDocSOut.Label17.Tag:=0;
          end;

          fmDocSOut.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmDocSOut.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmDocSOut.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocSOut.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmDocSOut.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmDocSOut.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocSOut.cxLabel1.Enabled:=True;
          fmDocSOut.cxLabel2.Enabled:=True;
          fmDocSOut.cxLabel3.Enabled:=True;
          fmDocSOut.cxLabel4.Enabled:=True;
          fmDocSOut.cxLabel5.Enabled:=True;
          fmDocSOut.cxLabel6.Enabled:=True;
          fmDocSOut.cxButton1.Enabled:=True;

          fmDocSOut.ViewDoc2.OptionsData.Editing:=True;
          fmDocSOut.ViewDoc2.OptionsData.Deleting:=True;
          fmDocSOut.Panel4.Visible:=True;

          CloseTe(fmDocSOut.taSpecIn);

          if taG.Active=False then taG.Active:=True;

          fmDocSOut.ViewDoc2.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmDocSOut do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecIn.Append;
                  taSpecInNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecInCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecInCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecInBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecInNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecInNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecInOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecInBestBefore.AsDateTime:=Date;
                  taSpecInKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecInKolEdMest.AsFloat:=0;
                  taSpecInKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecInKolWithNecond.AsFloat:=0;
                  taSpecInKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecInCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecInProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecInNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecInSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecInProcentN.AsFloat:=0;
                  taSpecInNecond.AsFloat:=0;
                  taSpecInCenaNecond.AsFloat:=0;
                  taSpecInSumNecond.AsFloat:=0;
                  taSpecInProcentZ.AsFloat:=0;
                  taSpecInZemlia.AsFloat:=0;
                  taSpecInProcentO.AsFloat:=0;
                  taSpecInOthodi.AsFloat:=0;
                  taSpecInName.AsString:=taSpecDocNameC.AsString;
                  taSpecInCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecInNameTara.AsString:=StrWk;

                  taSpecInVesTara.AsFloat:=0;
                  taSpecInCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecInSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecInTovarType.AsInteger:=0;

                  taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecInNac1.AsFloat:=rn1;
                  taSpecInNac2.AsFloat:=rn2;
                  taSpecInNac3.AsFloat:=rn3;
                  taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInsMaker.AsString:='';
                  end;
                  taSpecIn.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmDocSOut.ViewDoc2.EndUpdate;

          fmDocSOut.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocSOut.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocsOutH.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsOut.Visible then
  begin
    prNExportExel6(ViewDocsOut);
  end else
  begin
    prNExportExel6(ViewCardsOut);
  end;
end;

procedure TfmDocsOutH.FormShow(Sender: TObject);
begin
  if (Pos('����',Person.Name)=0)and(Pos('OPER CB',Person.Name)=0)and(Pos('OPERZAK',Person.Name)=0) then
  begin
    acChangePost.Enabled:=False;
  end else
  begin
    acChangePost.Enabled:=True;
  end;
  Memo1.Clear;
end;

procedure TfmDocsOutH.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViewDocsOut);
end;

procedure TfmDocsOutH.acEdit1Execute(Sender: TObject);
begin
{  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    if FileExists(CurDir+'SpecIn.cds') then fmDocSOut.taSpecIn.Active:=True
    else fmDocSOut.taSpecIn.CreateDataSet;

    fmDocSOut.acSaveDoc.Enabled:=True;
    fmDocSOut.Show;
  end;}
end;

procedure TfmDocsOutH.acTestMoveDocExecute(Sender: TObject);
begin
  //�������� ��������������
  with dmMCS do
  begin
    if not CanDo('prTestMoveDoc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTNOut.RecordCount>0 then //���� ��� ���������
    begin
      if quTTNOutAcStatus.AsInteger=3 then
      begin
        if MessageDlg('��������� �������������� �� ��������� �'+quTTNOutNumber.AsString+' �� '+quTTNOutNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ��������.',Memo1);
         // 1 - �������� ��������������
         // 2 - � ����� ��� �������������
         // 3 - �������� ������

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecIn1.Eof do
          begin
//            prDelTMoveId(quTTNOutDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quSpecIn1Kol.AsFloat);
            prWh('  -- ��� '+its(quSpecIn1CodeTovar.AsInteger),Memo1); delay(10);

            //������ ��� �������� �� ���� �� ���� ���������� � ���������� ��������
            prDelTMoveDate(quTTNOutDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),1);

            //�������� ��� ������� ������ �� ���� � �������� �������� �� ����
            quSpecInTest.Active:=False;
            quSpecInTest.SQL.Clear;
            quSpecInTest.SQL.Add('select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln');
            quSpecInTest.SQL.Add('left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoice=hd.DateInvoice) and(Ln.Number=hd.Number)');
            quSpecInTest.SQL.Add('where ln.DateInvoice='''+ds(quTTNOutDateInvoice.AsDateTime)+'''');
            quSpecInTest.SQL.Add('and ln.CodeTovar='+its(quSpecIn1CodeTovar.AsInteger));
            quSpecInTest.SQL.Add('and ln.Depart='+its(quTTNOutDepart.AsInteger));
            quSpecInTest.SQL.Add('and hd.AcStatus=3');
            quSpecInTest.SQL.Add('group by hd.ID');
            quSpecInTest.Active:=True;
            quSpecInTest.First;
            while not quSpecInTest.Eof do
            begin
              prAddTMoveId(quTTNOutDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quSpecInTestID.AsInteger,1,quSpecInTestKol.AsFloat);
              delay(20);
              quSpecInTest.Next;
            end;
            quSpecInTest.Active:=False;

            quSpecIn1.Next; delay(20);
          end;
          quSpecIn1.Active:=False;

         // 4 - �������� ������

          prWh('�������� ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsOutH.ViewDocsOutCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA,sO:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsOut.ColumnCount-1 do
  begin
    if ViewDocsOut.Columns[i].Name='ViewDocsOutIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;

    if ViewDocsOut.Columns[i].Name='ViewDocsOutChekBuh' then
    begin
      sO:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;


  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

  if pos('�����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00E6E6CA; //����������� - ������� ��������
  if pos('����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00F4F4EA; //����������� - �������� �������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocsOutH.acOprihExecute(Sender: TObject);
//Var bOpr:Boolean;
//    iTestZ:INteger;
//    sNumZ:String;
begin
//������������
  with dmMCS do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTNOut.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNOutAcStatus.AsInteger=0 then
      begin
        if quTTNOutDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTNOutDepart.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quTTNOutNumber.AsString+' �� '+quTTNOutNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          bOpr:=True;

          iTestZ:=0;
          if (quTTNOutRaion.AsInteger>=1)and(quTTNOutRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quTTNOutORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
            quZakHNum.ParamByName('ICLI').AsInteger:=quTTNOutCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;
          end;

          if bOpr = False then
          begin
            prWH('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...',Memo1);
            Showmessage('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...');
          end else
          begin
            prWH('����� ... ���� ������.',Memo1);

            prAddHist(1,quTTNOutID.AsInteger,trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutNumber.AsString,'�����.',1);

           // 1 - �������� ��������������
           // 2 - � ����� ��� �������������
           // 3 - �������� ������

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
            quSpecIn1.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
            quSpecIn1.Active:=True;

//            prClearBuf(quTTNOutID.AsInteger,1,Trunc(quTTNOutDateInvoice.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quTTNOutDateInvoice.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTNOutID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quTTNOutDepart.AsInteger)+r+its(quTTNOutID.AsInteger)+r+ds(quTTNOutDateInvoice.AsDateTime)+r+quTTNOutNumber.AsString+r+quTTNOutINN.AsString+r+fs(quTTNOutSummaTovarPost.AsFloat)+r+fs(quTTNOutSummaTovar.AsFloat)+r+fs(quTTNOutSummaTara.AsFloat)+r+fs(quTTNOutNDS10.AsFloat)+r+fs(quTTNOutNDS20.AsFloat)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quTTNOutDepart.AsInteger)+r+its(quTTNOutID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecIn1.Eof do
              begin
                if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quTTNOutNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                prDelTMoveId(quTTNOutDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quSpecIn1Kol.AsFloat);
                prAddTMoveId(quTTNOutDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quSpecIn1Kol.AsFloat);

                quSpecIn1.Next;
              end;
              quSpecIn1.Active:=False;


              if quTTNOutSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecInT.Active:=False;
                quSpecInT.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
                quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
                quSpecInT.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
                quSpecInT.Active:=True;

                quSpecInT.First;  // ��������������
                while not quSpecInT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quTTNOutDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quSpecInTKolMest.AsFloat);
                  prAddTaraMoveId(quTTNOutDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime),quTTNOutID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                  quSpecInT.Next;
                end;
                quSpecInT.Active:=False;
              end;

           // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quTTNOutDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTNOutDateInvoice.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
              quE.ExecSQL;

              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quTTNOut,ViewDocsIn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTNOutDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTNOutDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsOutH.acRazrubExecute(Sender: TObject);
//Var iC:INteger;
//    StrWk:String;
//    rSumP,rQ,rNac,rSumNDS,rSumOutNDS:Real;
begin
// ������������ �������� �� ������
  with dmMCS do
  begin
    if not CanDo('prRazrIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTNOut.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNOutAcStatus.AsInteger<3 then
      begin
        if quTTNOutDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTNOutDepart.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������
          prButtonSet(False);

          prWH('����� ... ���� ������������.',Memo1);

          // 4 - �������� ������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=2');
          quE.SQL.Add('where Depart='+its(quTTNOutDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTNOutDateInvoice.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTNOut,ViewDocsIn,0);

          prWh('������ ��.',Memo1);

          prButtonSet(True);


          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocSOut.cxCurrencyEdit1.EditValue:=quTTNOutSummaTovarPost.AsFloat;
          fmDocSOut.cxTextEdit1.Text:=quTTNOutNumber.AsString+'rzr';
          fmDocSOut.Label4.Tag:=quTTNOutIndexPost.AsInteger;

          if prCliNDS(quTTNOutIndexPost.AsInteger,quTTNOutCodePost.AsInteger,0)=1 then
          begin
            fmDocSOut.Label17.Caption:='���������� ���';
            fmDocSOut.Label17.Tag:=1;
          end else
          begin
            fmDocSOut.Label17.Caption:='�� ���������� ���';
            fmDocSOut.Label17.Tag:=0;
          end;

          fmDocSOut.cxButtonEdit1.Tag:=quTTNOutCodePost.AsInteger;
          fmDocSOut.cxButtonEdit1.Text:=quTTNOutNameCli.AsString;
          fmDocSOut.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocSOut.cxLookupComboBox1.EditValue:=quTTNOutDepart.AsInteger;
          fmDocSOut.cxLookupComboBox1.Text:=quTTNOutNameDep.AsString;
          fmDocSOut.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocSOut.cxLabel1.Enabled:=True;
          fmDocSOut.cxLabel2.Enabled:=True;
          fmDocSOut.cxLabel3.Enabled:=True;
          fmDocSOut.cxLabel4.Enabled:=True;
          fmDocSOut.cxLabel5.Enabled:=True;
          fmDocSOut.cxLabel6.Enabled:=True;
          fmDocSOut.cxButton1.Enabled:=True;

          fmDocSOut.ViewDoc2.OptionsData.Editing:=True;
          fmDocSOut.ViewDoc2.OptionsData.Deleting:=True;
          fmDocSOut.Panel4.Visible:=True;

          CloseTe(fmDocSOut.taSpecIn);

          if taG.Active=False  then taG.Active:=True;
          if ptTTK.Active=False  then ptTTK.Active:=True;

          fmDocSOut.ViewDoc2.BeginUpdate;

          quSpecIn.Active:=False;
          quSpecIn.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
          quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
          quSpecIn.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
          quSpecIn.Active:=True;

          quSpecIn.First;
          iC:=1;
          while not quSpecIn.Eof do
          begin
            with fmDocSOut do
            begin
              if ptTTK.Active=False then ptTTK.Active:=True;
              ptTTK.CancelRange;
              if ptTTK.locate('ID',quSpecInCodeTovar.AsInteger,[]) then
              begin
                rSumP:=rv(quSpecInSumCenaTovarPost.AsFloat);
                rSumNDS:=rv(quSpecInNDSSum.AsFloat);
                rSumOutNDS:=rv(quSpecInOutNDSSum.AsFloat);

                ptTTK.CancelRange;
                ptTTK.SetRange([quSpecInCodeTovar.AsInteger,0],[quSpecInCodeTovar.AsInteger,100]);
                ptTTK.First;
                while not ptTTK.Eof do
                begin
                  rNac:=ptTTKPROC2.AsFloat;
                  if rNac<1 then rNac:=rNac*100-100;

                  if dmMT.taG.FindKey([ptTTKIDC.AsInteger]) then
                  begin
                    rQ:=R1000((quSpecInKolWithMest.AsFloat*quSpecInKolMest.AsInteger)*ptTTKPROC1.AsFloat/100);

                    taSpecIn.Append;
                    taSpecInNum.AsInteger:=iC;
                    taSpecInCodeTovar.AsInteger:=ptTTKIDC.AsInteger;
                    taSpecInCodeEdIzm.AsInteger:=dmMt.taGEdIzm.AsInteger;
                    taSpecInBarCode.AsString:=dmMt.taGBarCode.AsString;
                    taSpecInNDSProc.AsFloat:=dmMt.taGNDS.AsFloat;
                    taSpecInNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInOutNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInBestBefore.AsDateTime:=date+dmMt.taGSrokReal.AsInteger;
                    taSpecInKolMest.AsInteger:=1;
                    taSpecInKolEdMest.AsFloat:=0;
                    taSpecInKolWithMest.AsFloat:=rQ;
                    taSpecInKolWithNecond.AsFloat:=rQ;
                    taSpecInKol.AsFloat:=rQ;
                    taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                    taSpecInProcent.AsFloat:=rNac;
                    taSpecInNewCenaTovar.AsFloat:=rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInSumCenaTovarPost.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat);
                    taSpecInSumCenaTovar.AsFloat:=rQ*rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInProcentN.AsFloat:=0;
                    taSpecInNecond.AsFloat:=0;
                    taSpecInCenaNecond.AsFloat:=0;
                    taSpecInSumNecond.AsFloat:=0;
                    taSpecInProcentZ.AsFloat:=0;
                    taSpecInZemlia.AsFloat:=0;
                    taSpecInProcentO.AsFloat:=0;
                    taSpecInOthodi.AsFloat:=0;
                    taSpecInName.AsString:=OemToAnsiConvert(dmMt.taGName.AsString);
                    taSpecInCodeTara.AsInteger:=0;
                    taSpecInNameTara.AsString:='';

                    taSpecInVesTara.AsFloat:=0;
                    taSpecInCenaTara.AsFloat:=0;
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInSumCenaTara.AsFloat:=0;
                    taSpecInTovarType.AsInteger:=dmMt.taGTovarType.AsInteger;
                    taSpecInRealPrice.AsFloat:=dmMt.taGCena.AsFloat;
                    taSpecIniCat.AsInteger:=dmMt.taGV02.AsInteger;
                    if taGV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                    if taGV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                    taSpecInsMaker.AsString:='';
                    taSpecIn.Post;

                    rSumP:=rSumP-rv(rQ*quSpecInCenaTovar.AsFloat);
                    rSumNDS:=rSumNDS-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    rSumOutNDS:=rSumOutNDS-(rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat)));
                  end;

                  ptTTK.Next; inc(iC);
                end;
                //��������� ������� ��������� ���� ����
                if abs(rSumP)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat+rSumP;
                  taSpecIn.Post;
                end;
                if abs(rSumNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInNDSSum.AsFloat:=taSpecInNDSSum.AsFloat+rSumNDS;
                  taSpecIn.Post;
                end;
                if abs(rSumOutNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat+rSumOutNDS;
                  taSpecIn.Post;
                end;

              end else
              begin
                taSpecIn.Append;
                taSpecInNum.AsInteger:=iC;
                taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
                taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
                taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
                taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
                taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
                taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
                taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
                taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
                taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
                taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
                taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
                taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
                taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
                taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
                taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
                taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
                taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
                taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
                taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
                taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
                taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
                taSpecInName.AsString:=quSpecInName.AsString;
                taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

                StrWk:='';
                if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
                taSpecInNameTara.AsString:=StrWk;

                taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
                taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
                taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
                taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
                taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
                taSpecIniCat.AsInteger:=quSpecInV02.AsInteger;
                if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
                if quSpecInSumVesTara.AsFloat>0.1 then  taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

                taSpecIn.Post;

                inc(iC);
              end;
            end;
            quSpecIn.Next;
          end;

          ptTTK.Active:=False;

          fmDocSOut.ViewDoc2.EndUpdate;

          fmDocSOut.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocSOut.Show;
        end;
      end else
      begin
        showmessage('�������� ��������� �.�. �������� �����������.');
      end;
    end;}
  end;
end;

procedure TfmDocsOutH.acCheckBuhExecute(Sender: TObject);
//Var iSt:INteger;
begin
//
  with dmMCS do
  begin
    {
    if quTTNOut.RecordCount>0 then
    begin
      if quTTNOutChekBuh.AsInteger=0 then fmChangeB.cxRadioButton1.Checked:=True;
      if quTTNOutChekBuh.AsInteger=1 then fmChangeB.cxRadioButton2.Checked:=True;
      if quTTNOutChekBuh.AsInteger=2 then fmChangeB.cxRadioButton3.Checked:=True;

      fmChangeB.ShowModal;
      if fmChangeB.ModalResult=mrOk then
      begin
        iSt:=0;
        if fmChangeB.cxRadioButton2.Checked then iSt:=1;
        if fmChangeB.cxRadioButton3.Checked then iSt:=2;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ChekBuh='+its(iSt));
        quE.SQL.Add('where Depart='+its(quTTNOutDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNOutDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNOut,ViewDocsIn,0);

        showmessage('��.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsOutH.acChangePostExecute(Sender: TObject);
begin
// �������� ����������
  with dmMCS do
  begin
    {
    if quTTNOut.RecordCount>0 then //���� ��� �������
    begin
      try
        fmChangePost:=tfmChangePost.Create(Application);
        fmChangePost.Label1.Caption:='�������� ���������� � ��������� � '+quTTNOutNumber.AsString;
        fmChangePost.Label3.Caption:=quTTNOutNameCli.AsString;
        fmChangePost.cxButtonEdit1.Text:='';
        fmChangePost.cxButtonEdit1.Tag:=0;
        fmChangePost.Label4.Tag:=0;

        fmDocSOut.Label17.Caption:='';
        fmDocSOut.Label17.Tag:=0;

        fmChangePost.ShowModal;
      finally
        fmChangePost.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsOutH.acChangeTypePolExecute(Sender: TObject);
begin
  //�������� ����������
  with dmMCS do
  begin
    {
    if quTTNOut.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quTTNOutNumber.AsString+' �� '+DateToStr(quTTNOutDateInvoice.AsDateTime)+' ������ ��� '+its(quTTNOutProvodType.AsInteger);
      if quTTNOutProvodType.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ProvodType='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quTTNOutDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNOutDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNOut,ViewDocsIn,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsOutH.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMCS do
  begin
{    if ViewDocsIn.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsIn.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=1;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsOutH.ViewCardsOutSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewCardsOut.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
{    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teInLnCode.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;}
  end;
end;

procedure TfmDocsOutH.acMoveCardExecute(Sender: TObject);
begin
  if ViewCardsOut.Visible then
  begin
  //��������������
    with dmMCS do
    begin
      if ViewCardsOut.Controller.SelectedRecordCount>0 then
      begin
        {
        fmMove.ViewM.BeginUpdate;
        fmMove.GridM.Tag:=teInLnCode.AsInteger;
        prFillMove(teInLnCode.AsInteger);

        fmMove.ViewM.EndUpdate;

        fmMove.Caption:=teInLnCardName.AsString+'('+teInLnCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

        fmMove.LevelP.Visible:=False;
        fmMove.LevelM.Visible:=True;

        fmMove.Show;}
      end;
    end;
  end;
end;

Function OprDocsOut(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOnDocOut') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnOut.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNOutID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNOutIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNOutIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 3');
        quProc.SQL.Add('EXECUTE [dbo].[prOutSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;

Function OtkatDocsOut(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOffDocOut') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnOut.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNOutID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNOutIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNOutIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 1');
        quProc.SQL.Add('EXECUTE [dbo].[prOutSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


end.
