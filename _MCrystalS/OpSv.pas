unit OpSv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, IniFiles, FileUtil;

type
  TfmOpSv = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    cxButton1: TcxButton;
    Timer1: TTimer;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure prWM(Strwk_:String; Memo1:TcxMemo);
procedure WriteHistory(Strwk_: string);
procedure Delay(MSecs: Longint);
Function its(i:Integer):String;
Function fDelF(FName:String):Boolean;
function fmt:String;
function dst(d:TDateTime):String;
Function fts(rSum:Real):String;
function RV( X: Double ): Double;
function fDatePoint(SDate:String):String;
function fFloatPoint(SFloat:String):String;

Const CurIni:String = 'Profiles.ini';

type
  TCommonSet = record
                 PathHistory,Tmp:String;
                 POSLoad,POSReport,POSOper:String;
               end;

  TChLine = Record
            Depart,iDate,Operation,ChNum,Casher,CashNum,ZNum,PaymentT,Replace,Num,IdCard:INteger;
            DSum,Quant,rPrice,rSum:Real;
            sTime:String;
          end;

var
  fmOpSv: TfmOpSv;
  CommonSet:TCommonSet;
  CurDir:String;

implementation

uses DMOPSVOD;

{$R *.dfm}

function fFloatPoint(SFloat:String):String;
Var S:String;
begin
  S:=SFloat;
  while pos('.',S)>0 do S[pos('.',S)]:=',';
  Result:=S;
end;


function fDatePoint(SDate:String):String;
Var S:String;
begin
  S:=SDate;
  while pos('/',S)>0 do S[pos('/',S)]:='.';
  Result:=S;
end;

function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RV:= ScaledFractPart/100;
end;


Function fts(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;


function dst(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd hh:nn:ss',d);
end;


function fmt:String;
begin
  result:=FormatDateTime('dd.mm hh:nn:ss:zzz  ',now)+'  ';
end;


Function fDelF(FName:String):Boolean;
Var FEr:TextFile;
begin
  Result:=True;
  try
    if FileExists(FName) then
    begin
      DeleteFile(FName);
      delay(10);
    end;
    if FileExists(FName) then
    begin
      AssignFile(FEr,FName);
      Erase(FEr);
    end;
  except
    Result:=False;
  end;
end;

Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


Procedure ReadIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  CommonSet.PathHistory:=f.ReadString('Config','PathHistory',CurDir+'History\');
  f.WriteString('Config','PathHistory',CommonSet.PathHistory);

  CommonSet.POSLoad:=f.ReadString('Config','POSLoad',CurDir+'pos\cashload\');
  if CommonSet.POSLoad[Length(CommonSet.POSLoad)]<>'\' then CommonSet.POSLoad:=CommonSet.POSLoad+'\';
  f.WriteString('Config','POSLoad',CommonSet.POSLoad);

  CommonSet.POSReport:=f.ReadString('Config','POSReport',CurDir+'pos\cashrep\');
  if CommonSet.POSReport[Length(CommonSet.POSReport)]<>'\' then CommonSet.POSReport:=CommonSet.POSReport+'\';
  f.WriteString('Config','POSReport',CommonSet.POSReport);

  CommonSet.POSOper:=f.ReadString('Config','POSOper',CurDir+'pos\cashoper\');
  if CommonSet.POSOper[Length(CommonSet.POSOper)]<>'\' then CommonSet.POSOper:=CommonSet.POSOper+'\';
  f.WriteString('Config','POSOper',CommonSet.POSOper);

  CommonSet.Tmp:=f.ReadString('Config','Tmp',CurDir+'Tmp\');
  if CommonSet.Tmp[Length(CommonSet.Tmp)]<>'\' then CommonSet.Tmp:=CommonSet.Tmp+'\';
  f.WriteString('Config','Tmp',CommonSet.Tmp);


  f.Free;
end;


procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    StrWk1:=FormatDateTime('dd hh:nn',now)+' ';
    WriteLn(F,StrWk1+Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Procedure prWM(Strwk_:String; Memo1:TcxMemo);
begin
  WriteHistory(Strwk_);
  if Memo1<>nil then
  begin
    if Memo1.Lines.Count>5000 then Memo1.Clear;
    Memo1.Lines.Add(fmt+Strwk_);
  end;
  Delay(10);
end;

procedure TfmOpSv.cxButton1Click(Sender: TObject);
Var StrWk:String;
    sPath:String;
    iShop:INteger;
    ChLine:TChLine;
    n:INteger;
    sLine:String;
    fLine:TextFile;
    bErr:Boolean;
    ChTime,ChTimePre:TDateTime;
    iCurD:INteger;
    NumC,IdCh,iCurSkl:INteger;
    Koef:Integer;
    iC:INteger;
begin
  //�������
  Memo1.Clear;
  prWM('������',Memo1);
  with dmOPSV do
  begin
    msConnection.Connected:=False;
    Strwk:='FILE NAME='+CurDir+'mcr.udl';
    msConnection.ConnectionString:=Strwk;
    delay(10);
    msConnection.Connected:=True;
    delay(10);
    if msConnection.Connected then
    begin
      prWM('  ���� ��',Memo1);
      quShops.Active:=False;
      quShops.Active:=True;
      quShops.First;
      while not quShops.Eof do
      begin
        iShop:=quShopsIDS.asInteger;
        sPath:=CommonSet.POSOper+its(iShop)+'\';

        prWM('    ������� � '+its(iShop),Memo1);
        prWM('      ���� - '+sPath,Memo1);

        prWM('      ������ ���������� ������.',Memo1);

        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from [dbo].[cqHeadsOSV]');
        quD.SQL.Add('where [Id_Depart] in (Select [ID] from dbo.DEPARTS where [IDS]='+its(iShop)+')');
        quD.SQL.Add('and [IDate]<'+its(Trunc(Date)));
        quD.ExecSQL;

        prWM('      ������ ��������� ������� - '+CommonSet.Tmp,Memo1);
        if fDelF(CommonSet.Tmp+'CASHDCRD.DAT')
           and fDelF(CommonSet.Tmp+'CASHDISC.DAT')
           and fDelF(CommonSet.Tmp+'CASHPAY.DAT')
           and fDelF(CommonSet.Tmp+'CASHSAIL.DAT')
           and fDelF(CommonSet.Tmp+'CASHTAX.DAT')
        then
        begin  //��� �� ������� ����������
          try
            MoveFile(sPath+'CASHDCRD.DAT', CommonSet.Tmp+'CASHDCRD.DAT');
            MoveFile(sPath+'CASHDISC.DAT', CommonSet.Tmp+'CASHDISC.DAT');
            MoveFile(sPath+'CASHPAY.DAT', CommonSet.Tmp+'CASHPAY.DAT');
            MoveFile(sPath+'CASHSAIL.DAT', CommonSet.Tmp+'CASHSAIL.DAT');
            MoveFile(sPath+'CASHTAX.DAT', CommonSet.Tmp+'CASHTAX.DAT');

            if FileExists(CommonSet.Tmp+'CASHSAIL.DAT') then
            begin
              prWM('        ���� ����� ������... ��������',Memo1);
              ChTime:=0;
              iCurD:=Trunc(date);
              NumC:=-1;  //������� ����� ����
              IdCh:=0;
              iCurSkl:=-1;
              iC:=0;

              try
                assignfile(fLine,CommonSet.Tmp+'CASHSAIL.DAT');
                reset(fLine);

                while not EOF(fLine) do
                begin
                  ReadLn(fLine,sLine);

                  inc(iC);
                  if iC mod 100 = 0 then prWM('        ������� - '+Its(iC),Memo1);

                  bErr:=True; //��� ������
                  for n:=1 to 22 do
                  begin
                    if pos(',',sLine)>0 then
                    begin
                      strwk:=Copy(sLine,1,pos(',',sLine)-1);
                      Delete(sLine,1,pos(',',sLine));
                    end else StrWk:=sLine;

                    Case n of
                    1: begin end;
                    2: begin ChLine.CashNum:=StrToIntDef(StrWk,-1); if ChLine.CashNum<0 then bErr:=False; end;
                    3: begin ChLine.ZNum:=StrToIntDef(StrWk,-1); if ChLine.ZNum<0 then bErr:=False; end;
                    4: begin ChLine.ChNum:=StrToIntDef(StrWk,-1); if ChLine.ChNum<0 then bErr:=False; end;
                    5: begin ChLine.Num:=StrToIntDef(StrWk,-1); if ChLine.Num<0 then bErr:=False; end;
                    6: begin ChLine.iDate:=Trunc(StrToDateDef(fDatePoint(StrWk),date)); if ChLine.iDate=0 then bErr:=False; end;
                    7: begin ChLine.sTime:=StrWk; end;
                    8: begin ChLine.IdCard:=StrToIntDef(StrWk,-1); if ChLine.IdCard<0 then bErr:=False; end;
                    9: begin end;
                    10: begin ChLine.Quant:=StrToFloatDef(fFloatPoint(StrWk),0); end;
                    11: begin ChLine.rPrice:=StrToFloatDef(fFloatPoint(StrWk),0); end;
                    12: begin end;
                    13: begin ChLine.rSum:=StrToFloatDef(fFloatPoint(StrWk),0); end;
                    14: begin end;
                    15: begin end;
                    16: begin ChLine.Casher:=StrToIntDef(StrWk,-1); if ChLine.Casher<0 then bErr:=False; end;
                    17: begin ChLine.Depart:=StrToIntDef(StrWk,-1); if ChLine.Depart<0 then bErr:=False; end;
                    18: begin ChLine.Replace:=StrToIntDef(StrWk,-1); if ChLine.Replace<0 then bErr:=False; end;
                    19: begin ChLine.Operation:=StrToIntDef(StrWk,-1); if ChLine.Operation<0 then bErr:=False; end;
                    20: begin end;
                    21: begin end;
                    22: begin end;

//13,8,342,35,1,18/08/2014,1029,479,NOSIZE,1.000,67.00,2010.00,67.00,2010.00,0,2,8,1,1,0,0,0

                    end;
                  end;

                  if bErr then //��� �� - ������������ - ���� ���������
                  begin
                    while Length(ChLine.sTime)<4 do ChLine.sTime:='0'+ChLine.sTime;
                    ChLine.sTime:=Copy(ChLine.sTime,1,2)+':'+Copy(ChLine.sTime,3,2);
                    ChTimePre:=ChTime;
                    ChTime:=ChLine.iDate+StrToTime(ChLine.sTime); //����� ���� � ����������� ��������� ���������

                    koef:=1;
                    if ChLine.Replace=0 then koef:=-1;

                    if (ChTime>=(iCurD+0.0833333)) and (ChTime<(iCurD+1+0.0833333)) then // 2-���� ����
                    begin
                      if (ChLine.ChNum>NumC)or(ChLine.Depart<>iCurSkl) then
                      begin
                        //���� ����������� ���������
                        IdCh:=0;

                        if ChLine.Depart<>iCurSkl then //��������� ����� - ����� ��������� �� ��������� , ���� ����� ���� ��������� �� ��������� �����
                        begin
                          quSel.Active:=False;
                          quSel.SQL.Clear;
                          quSel.SQL.Add('Select [Id],[Id_Depart],[IDate],[Operation],[DateOperation],[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[PaymentType],[sNum] from [dbo].[cqHeadsOSV]');
                          quSel.SQL.Add('where [Id_Depart]='+its(ChLine.Depart));
                          quSel.SQL.Add('and [IDate]='+its(iCurD));
                          quSel.SQL.Add('and [Cash_Code]='+its(ChLine.CashNum));
                          quSel.SQL.Add('and [NSmena]='+its(ChLine.ZNum));
                          quSel.SQL.Add('and [Ck_Number]='+its(ChLine.ChNum));
                          quSel.Active:=True;
                          if quSel.RecordCount>0 then  IdCh:=quSel.Fields.FieldByName('Id').Value; //��������� ����
                          quSel.Active:=False;
                        end;

                        if IdCh=0 then //��������� ��� - ��������
                        begin
                          IdCh:=fGetID(15);
                          quA.Active:=False;  //��������� ���������
                          quA.SQL.Clear;
                          quA.SQL.Add('INSERT INTO [dbo].[cqHeadsOSV]');
                          quA.SQL.Add('([Id],[Id_Depart],[IDate],[Operation],[DateOperation],[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[PaymentType],[sNum])');
                          quA.SQL.Add('VALUES');
                          quA.SQL.Add('(');
                          quA.SQL.Add(its(IdCh));
                          quA.SQL.Add(','+its(ChLine.Depart));
                          quA.SQL.Add(','+its(iCurD));
                          quA.SQL.Add(','+its(ChLine.Operation));
                          quA.SQL.Add(','''+dst(ChTime)+'''');
                          quA.SQL.Add(','+its(ChLine.ChNum));
                          quA.SQL.Add(','+its(ChLine.Casher));
                          quA.SQL.Add(','+its(ChLine.CashNum));
                          quA.SQL.Add(','+its(ChLine.ZNum));
                          quA.SQL.Add(',''''');   //CardNumber
                          if (ChLine.Operation=5)or(ChLine.Operation=4) then quA.SQL.Add(',1')
                          else quA.SQL.Add(',0');  //PaymentType
                          quA.SQL.Add(',''''');   //sNum
                          quA.SQL.Add(')');
                          quA.ExecSQL;
                        end;

                        NumC:=ChLine.ChNum;
                        iCurSkl:=ChLine.Depart;
                      end;

                      if (abs(ChLine.Quant)>0.0001)and(IdCh>0) then
                      begin
                        if ChLine.IdCard>=0 then
                        begin
                          quA.Active:=False;  //��������� ������
                          quA.SQL.Clear;
                          quA.SQL.Add('INSERT INTO [dbo].[cqLinesOSV]');
                          quA.SQL.Add('([IdHead],[Num],[Code],[BarCode],[Quant],[Price],[Summa],[ProcessingTime],[DProc],[DSum])');
                          quA.SQL.Add('VALUES');
                          quA.SQL.Add('(');
                          quA.SQL.Add(its(IdCh));                          // IdHead
                          quA.SQL.Add(','+its(ChLine.Num));          // Num
                          quA.SQL.Add(','+its(ChLine.IdCard));       // Code
                          quA.SQL.Add(',''''');                            // BarCode  - ���
                          quA.SQL.Add(','+fts(ChLine.Quant * koef));      // Quant
                          quA.SQL.Add(','+fts(ChLine.rPrice));      // Price
                          quA.SQL.Add(','+fts(ChLine.rSum * koef));      // Summa, real,
                          if ChLine.Num=1 then quA.SQL.Add(','+fts(0))
                          else quA.SQL.Add(','+fts(ChTime-ChTimePre));  // ProcessingTime, real
                          quA.SQL.Add(','+fts(0));   // DProc
                          quA.SQL.Add(','+fts(rv(ChLine.rPrice * ChLine.Quant)-ChLine.rSum));   // DSum
                          quA.SQL.Add(')');
                          quA.ExecSQL;
                        end else  prWM('     ���.  '+its(ChLine.IdCard)+' �� ������.',Memo1);
                      end;
                    end;
                  end;
                end;
              finally
                closefile(fLine);
              end;

{
13,8,342,35,1,18/08/2014,1029,479,NOSIZE,1.000,67.00,2010.00,67.00,2010.00,0,2,8,1,1,0,0,0
13,8,342,36,1,18/08/2014,1035,20342,NOSIZE,1.000,41.00,1230.00,41.00,1230.00,0,2,101,1,1,0,0,0
13,8,342,37,1,18/08/2014,1036,67321,NOSIZE,1.000,134.00,4020.00,134.00,4020.00,0,2,101,1,1,0,0,0
13,8,342,37,2,18/08/2014,1036,15501,NOSIZE,1.000,49.00,1470.00,49.00,1470.00,0,2,101,1,1,0,0,0
13,8,342,38,1,18/08/2014,1042,18847,NOSIZE,0.354,113.00,3390.00,40.00,1200.00,0,2,8,1,1,0,0,0
13,8,342,38,2,18/08/2014,1042,3293,NOSIZE,1.000,90.00,2700.00,90.00,2700.00,0,2,8,1,1,0,0,0
13,8,342,38,3,18/08/2014,1042,15537,NOSIZE,1.000,58.00,1740.00,58.00,1740.00,0,2,8,1,1,0,0,0
}


              fDelF(CommonSet.Tmp+'CASHDCRD.DAT');
              fDelF(CommonSet.Tmp+'CASHDISC.DAT');
              fDelF(CommonSet.Tmp+'CASHPAY.DAT');
              fDelF(CommonSet.Tmp+'CASHSAIL.DAT');
              fDelF(CommonSet.Tmp+'CASHTAX.DAT');
            end;
          except
            prWM('      ������ �����������',Memo1);
          end;
        end else prWM('      ������ ��������',Memo1);
        quShops.Next;
      end;
      quShops.Active:=False;
      msConnection.Connected:=False;
    end;
  end;
  prWM('������� ��������',Memo1);
end;

procedure TfmOpSv.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

procedure TfmOpSv.Timer1Timer(Sender: TObject);
begin
  cxButton1.Click;
end;

end.
