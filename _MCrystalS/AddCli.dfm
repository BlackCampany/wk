object fmAddCli: TfmAddCli
  Left = 711
  Top = 329
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1082#1072
  ClientHeight = 602
  ClientWidth = 459
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 11
    Width = 59
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 43
    Width = 105
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 84
    Width = 28
    Height = 13
    Caption = #1048#1053#1053
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 256
    Top = 351
    Width = 23
    Height = 13
    Caption = #1058#1080#1087
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 240
    Top = 84
    Width = 27
    Height = 13
    Caption = #1050#1055#1055
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 24
    Top = 146
    Width = 45
    Height = 13
    Caption = #1048#1085#1076#1077#1082#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 216
    Top = 146
    Width = 36
    Height = 13
    Caption = #1043#1086#1088#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label8: TLabel
    Left = 24
    Top = 116
    Width = 38
    Height = 13
    Caption = #1059#1083#1080#1094#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label9: TLabel
    Left = 320
    Top = 116
    Width = 27
    Height = 13
    Caption = #1044#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 24
    Top = 184
    Width = 102
    Height = 13
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 24
    Top = 212
    Width = 111
    Height = 13
    Caption = #1040#1076#1088#1077#1089' '#1075#1088#1091#1079#1086#1087#1086#1083#1091#1095'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label12: TLabel
    Left = 24
    Top = 251
    Width = 59
    Height = 13
    Caption = #1056#1072#1089#1095'.'#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label13: TLabel
    Left = 252
    Top = 251
    Width = 26
    Height = 13
    Caption = #1041#1048#1050
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label14: TLabel
    Left = 24
    Top = 283
    Width = 30
    Height = 13
    Caption = #1041#1072#1085#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label15: TLabel
    Left = 24
    Top = 315
    Width = 64
    Height = 13
    Caption = #1050#1086#1088#1088'. '#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 530
    Width = 459
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = 14211286
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 68
      Top = 8
      Width = 97
      Height = 33
      Caption = #1054#1082
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 272
      Top = 8
      Width = 105
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 583
    Width = 459
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxTextEdit1: TcxTextEdit
    Left = 128
    Top = 5
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit1'
    Width = 193
  end
  object cxTextEdit2: TcxTextEdit
    Left = 128
    Top = 37
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit2'
    Width = 305
  end
  object cxTextEdit3: TcxTextEdit
    Left = 80
    Top = 78
    Style.Shadow = True
    TabOrder = 5
    Text = 'cxTextEdit3'
    Width = 129
  end
  object cxImageComboBox1: TcxImageComboBox
    Left = 296
    Top = 341
    EditValue = '1'
    Properties.Images = dmMCS.ImageList1
    Properties.Items = <
      item
        Description = #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
        ImageIndex = 21
        Value = 1
      end
      item
        Description = #1048#1055
        ImageIndex = 10
        Value = 2
      end>
    Properties.LargeImages = dmMCS.ImageList1
    Style.Shadow = True
    TabOrder = 4
    Width = 137
  end
  object cxTextEdit4: TcxTextEdit
    Left = 296
    Top = 78
    Style.Shadow = True
    TabOrder = 6
    Text = 'cxTextEdit4'
    Width = 137
  end
  object cxTextEdit5: TcxTextEdit
    Left = 80
    Top = 140
    Style.Shadow = True
    TabOrder = 7
    Text = 'cxTextEdit5'
    Width = 97
  end
  object cxTextEdit6: TcxTextEdit
    Left = 272
    Top = 140
    Style.Shadow = True
    TabOrder = 8
    Text = 'cxTextEdit6'
    Width = 161
  end
  object cxTextEdit7: TcxTextEdit
    Left = 80
    Top = 110
    Style.Shadow = True
    TabOrder = 9
    Text = 'cxTextEdit7'
    Width = 225
  end
  object cxTextEdit8: TcxTextEdit
    Left = 368
    Top = 110
    Style.Shadow = True
    TabOrder = 10
    Text = 'cxTextEdit8'
    Width = 65
  end
  object cxTextEdit9: TcxTextEdit
    Left = 140
    Top = 178
    Style.Shadow = True
    TabOrder = 11
    Text = 'cxTextEdit9'
    Width = 293
  end
  object cxTextEdit10: TcxTextEdit
    Left = 140
    Top = 206
    Style.Shadow = True
    TabOrder = 12
    Text = 'cxTextEdit10'
    Width = 293
  end
  object cxTextEdit11: TcxTextEdit
    Left = 100
    Top = 245
    Style.Shadow = True
    TabOrder = 13
    Text = 'cxTextEdit11'
    Width = 135
  end
  object cxTextEdit12: TcxTextEdit
    Left = 298
    Top = 245
    Style.Shadow = True
    TabOrder = 14
    Text = 'cxTextEdit12'
    Width = 135
  end
  object cxTextEdit13: TcxTextEdit
    Left = 80
    Top = 277
    Style.Shadow = True
    TabOrder = 15
    Text = 'cxTextEdit13'
    Width = 353
  end
  object cxTextEdit14: TcxTextEdit
    Left = 100
    Top = 309
    Style.Shadow = True
    TabOrder = 16
    Text = 'cxTextEdit14'
    Width = 333
  end
  object GroupBox1: TGroupBox
    Left = 17
    Top = 380
    Width = 417
    Height = 146
    Caption = '  '#1040#1074#1090#1086#1079#1072#1082#1072#1079'  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 17
    object Label21: TLabel
      Left = 12
      Top = 24
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label22: TLabel
      Left = 216
      Top = 98
      Width = 74
      Height = 13
      Caption = 'GLN ('#1043#1051#1053' '#1082#1086#1076')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label23: TLabel
      Left = 12
      Top = 98
      Width = 29
      Height = 13
      Caption = 'E-Mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label24: TLabel
      Left = 184
      Top = 26
      Width = 56
      Height = 13
      Caption = #1055#1088#1086#1074#1072#1081#1076#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label25: TLabel
      Left = 12
      Top = 72
      Width = 99
      Height = 13
      Caption = #1052#1080#1085'. '#1089#1091#1084#1084#1072' '#1079#1072#1082#1072#1079#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label16: TLabel
      Left = 12
      Top = 48
      Width = 127
      Height = 13
      Caption = #1044#1085#1077#1081' '#1084#1077#1078#1076#1091' '#1087#1086#1089#1090#1072#1074#1082#1072#1084#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cxImageComboBox2: TcxImageComboBox
      Left = 52
      Top = 20
      EditValue = 0
      ParentFont = False
      Properties.Items = <
        item
          Description = #1085#1077#1090
          ImageIndex = 0
          Value = 0
        end
        item
          Description = 'EDI'
          Value = 1
        end
        item
          Description = 'E-Mail'
          Value = 2
        end
        item
          Description = 'EDI ('#1073#1077#1079' '#1072#1074#1090#1086#1086#1090#1087#1088#1072#1074#1082#1080')'
          Value = 3
        end
        item
          Description = #1048#1085#1086#1081
          Value = 4
        end
        item
          Description = #1055#1088#1080#1085#1080#1084#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1073#1077#1079' '#1079#1072#1103#1074#1082#1080
          Value = 5
        end
        item
          Description = #1057#1074#1086#1105' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
          Value = 6
        end
        item
          Description = 'E-Mail ('#1073#1077#1079' '#1072'.'#1086'.)'
          Value = 7
        end
        item
          Description = #1057#1074#1086#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1072#1074#1090#1086
          Value = 8
        end>
      Style.Shadow = True
      TabOrder = 0
      Width = 93
    end
    object cxTextEdit18: TcxTextEdit
      Left = 296
      Top = 93
      ParentFont = False
      Style.Shadow = True
      TabOrder = 4
      Text = 'cxTextEdit18'
      Width = 109
    end
    object cxTextEdit19: TcxTextEdit
      Left = 52
      Top = 93
      ParentFont = False
      Style.Shadow = True
      TabOrder = 5
      Text = 'cxTextEdit19'
      Width = 141
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 248
      Top = 20
      ParentFont = False
      Properties.KeyFieldNames = 'Id'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'Name'
        end>
      Properties.ListSource = dsquEDIProv
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 157
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 124
      Top = 67
      EditValue = 0.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      Width = 121
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 152
      Top = 44
      TabOrder = 2
      Width = 49
    end
  end
  object cxCheckBox2: TcxCheckBox
    Left = 24
    Top = 344
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057
    ParentFont = False
    Properties.Alignment = taRightJustify
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 18
    Width = 149
  end
  object quEDIProv: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [Id]'
      '      ,[Name]'
      '      ,[HostIPEDI]'
      '      ,[HostUserEDI]'
      '      ,[HostPasswEDI]'
      '      ,[DestEDI]'
      '      ,[InpEDIOrderSp]'
      '      ,[InpEDIRep]'
      '      ,[InpEDINacl]'
      '      ,[IActive]'
      '      ,[weblink]'
      '      ,[mailsd]'
      '      ,[phone]'
      '      ,[DestEDIRecadv]'
      '      ,[InpEDIESF]'
      '      ,[DestEDIESF]'
      '      ,[DespEDIBonus]'
      '      ,[InpEDIBonus]'
      '      ,[DestEDIPretenzia]'
      '      ,[InpEDIPretenzia]'
      '      ,[DestEDIPricat]'
      '      ,[InpEDIPricat]'
      '      ,[DestEDIPartin]'
      '      ,[InpEDIPartin]'
      '      ,[DestEDICoacsu]'
      '      ,[InpEDICoacsu]'
      '      ,[DestEDIOrderSp]'
      '      ,[DestEDINacl]'
      '      ,[GLN]'
      '      ,[DestEDIStatus]'
      '      ,[SendEDIStatus]'
      '  FROM [dbo].[EDIProv]'
      '  order by [Name]'
      ''
      ''
      '')
    Left = 200
    Top = 336
    object quEDIProvId: TAutoIncField
      FieldName = 'Id'
      ReadOnly = True
    end
    object quEDIProvName: TStringField
      FieldName = 'Name'
      Size = 50
    end
    object quEDIProvHostIPEDI: TStringField
      FieldName = 'HostIPEDI'
      Size = 50
    end
    object quEDIProvHostUserEDI: TStringField
      FieldName = 'HostUserEDI'
      Size = 50
    end
    object quEDIProvHostPasswEDI: TStringField
      FieldName = 'HostPasswEDI'
      Size = 50
    end
    object quEDIProvDestEDI: TStringField
      FieldName = 'DestEDI'
      Size = 50
    end
    object quEDIProvInpEDIOrderSp: TStringField
      FieldName = 'InpEDIOrderSp'
      Size = 50
    end
    object quEDIProvInpEDIRep: TStringField
      FieldName = 'InpEDIRep'
      Size = 50
    end
    object quEDIProvInpEDINacl: TStringField
      FieldName = 'InpEDINacl'
      Size = 50
    end
    object quEDIProvIActive: TSmallintField
      FieldName = 'IActive'
    end
    object quEDIProvweblink: TMemoField
      FieldName = 'weblink'
      BlobType = ftMemo
    end
    object quEDIProvmailsd: TMemoField
      FieldName = 'mailsd'
      BlobType = ftMemo
    end
    object quEDIProvphone: TMemoField
      FieldName = 'phone'
      BlobType = ftMemo
    end
    object quEDIProvDestEDIRecadv: TStringField
      FieldName = 'DestEDIRecadv'
      Size = 50
    end
    object quEDIProvInpEDIESF: TStringField
      FieldName = 'InpEDIESF'
      Size = 50
    end
    object quEDIProvDestEDIESF: TStringField
      FieldName = 'DestEDIESF'
      Size = 50
    end
    object quEDIProvDespEDIBonus: TStringField
      FieldName = 'DespEDIBonus'
      Size = 50
    end
    object quEDIProvInpEDIBonus: TStringField
      FieldName = 'InpEDIBonus'
      Size = 50
    end
    object quEDIProvDestEDIPretenzia: TStringField
      FieldName = 'DestEDIPretenzia'
      Size = 50
    end
    object quEDIProvInpEDIPretenzia: TStringField
      FieldName = 'InpEDIPretenzia'
      Size = 50
    end
    object quEDIProvDestEDIPricat: TStringField
      FieldName = 'DestEDIPricat'
      Size = 50
    end
    object quEDIProvInpEDIPricat: TStringField
      FieldName = 'InpEDIPricat'
      Size = 50
    end
    object quEDIProvDestEDIPartin: TStringField
      FieldName = 'DestEDIPartin'
      Size = 50
    end
    object quEDIProvInpEDIPartin: TStringField
      FieldName = 'InpEDIPartin'
      Size = 50
    end
    object quEDIProvDestEDICoacsu: TStringField
      FieldName = 'DestEDICoacsu'
      Size = 50
    end
    object quEDIProvInpEDICoacsu: TStringField
      FieldName = 'InpEDICoacsu'
      Size = 50
    end
    object quEDIProvDestEDIOrderSp: TStringField
      FieldName = 'DestEDIOrderSp'
      Size = 50
    end
    object quEDIProvDestEDINacl: TStringField
      FieldName = 'DestEDINacl'
      Size = 50
    end
    object quEDIProvGLN: TStringField
      FieldName = 'GLN'
    end
    object quEDIProvDestEDIStatus: TStringField
      FieldName = 'DestEDIStatus'
      Size = 50
    end
    object quEDIProvSendEDIStatus: TSmallintField
      FieldName = 'SendEDIStatus'
    end
  end
  object dsquEDIProv: TDataSource
    DataSet = quEDIProv
    Left = 232
    Top = 360
  end
end
