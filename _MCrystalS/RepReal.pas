unit RepReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar;

type
  TfmRepReal = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRepReal: TcxGrid;
    ViewRepReal: TcxGridDBTableView;
    LevelRepReal: TcxGridLevel;
    Panel5: TPanel;
    Memo1: TcxMemo;
    fmplRepReal: TFormPlacement;
    amRepReal: TActionManager;
    acMoveRep1: TAction;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    acAdvRep: TAction;
    SpeedItem7: TSpeedItem;
    ViewRepRealId_Depart: TcxGridDBColumn;
    ViewRepRealCode: TcxGridDBColumn;
    ViewRepRealIDS: TcxGridDBColumn;
    ViewRepRealDEPNAME: TcxGridDBColumn;
    ViewRepRealNAME: TcxGridDBColumn;
    ViewRepRealBARCODE: TcxGridDBColumn;
    ViewRepRealEDIZM: TcxGridDBColumn;
    ViewRepRealRNAC: TcxGridDBColumn;
    ViewRepRealQuant: TcxGridDBColumn;
    ViewRepRealRSum: TcxGridDBColumn;
    ViewRepRealPriceR: TcxGridDBColumn;
    ViewRepRealDSUM: TcxGridDBColumn;
    ViewRepRealNameCl: TcxGridDBColumn;
    ViewRepRealsClass2: TcxGridDBColumn;
    ViewRepRealabcp: TcxGridDBColumn;
    ViewRepRealabcq: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure acMoveRep1Execute(Sender: TObject);
    procedure acAdvRepExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepReal: TfmRepReal;

implementation

uses Un1, Dm, MainMC, Move, SummaryRDoc;

{$R *.dfm}

procedure TfmRepReal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewRepReal.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmRepReal.FormCreate(Sender: TObject);
begin
  fmplRepReal.IniFileName:=sFormIni;
  fmplRepReal.Active:=True; delay(10);

  GridRepReal.Align:=AlClient;
//  ViewRepReal.RestoreFromIniFile(sGridIni); delay(10);
end;

procedure TfmRepReal.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepReal.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepReal);
end;

procedure TfmRepReal.acMoveRep1Execute(Sender: TObject);
begin
  with dmMCS do
  begin
    if quRepReal.RecordCount>0 then
    begin
      prFormMove(quRepRealId_Depart.AsInteger,quRepRealCode.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),quRepRealNAME.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmRepReal.acAdvRepExecute(Sender: TObject);
begin
//�����
// ����������� ���������
  fmSummary1:=tfmSummary1.Create(Application);
  fmSummary1.PivotGrid1.Visible:=False;
  fmSummary1.PivotGrid2.Visible:=True;
  fmSummary1.Caption:=fmRepReal.Caption;
  fmSummary1.ShowModal;
  fmSummary1.Release;

end;

end.
