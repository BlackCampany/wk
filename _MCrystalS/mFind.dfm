object fmFindC: TfmFindC
  Left = 536
  Top = 132
  Width = 653
  Height = 467
  Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '#1087#1086#1080#1089#1082#1072
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 521
    Top = 0
    Width = 124
    Height = 433
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 384
      Width = 49
      Height = 13
      AutoSize = False
      Caption = '....'
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 16
      Width = 89
      Height = 25
      Caption = #1055#1077#1088#1077#1081#1090#1080
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 16
      Top = 64
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridFind: TcxGrid
    Left = 8
    Top = 4
    Width = 501
    Height = 417
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewFind: TcxGridDBTableView
      OnDblClick = ViewFindDblClick
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewFindCustomDrawCell
      OnSelectionChanged = ViewFindSelectionChanged
      DataController.DataSource = dmMCS.dsquFindC
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object ViewFindID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Styles.Content = dmMCS.cxStyle5
      end
      object ViewFindName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmMCS.cxStyle5
        Width = 172
      end
      object ViewFindPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        Styles.Content = dmMCS.cxStyle5
        Width = 58
      end
      object ViewFindBarCode: TcxGridDBColumn
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        DataBinding.FieldName = 'BARCODE'
        Width = 91
      end
      object ViewFindEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EDIZM'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1050#1075'.'
            ImageIndex = 0
            Value = 2
          end
          item
            Description = #1064#1090'.'
            ImageIndex = 0
            Value = 1
          end>
      end
      object ViewFindSubGroupName: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SubGroupName'
        Visible = False
        Styles.Content = dmMCS.cxStyle18
        Width = 112
      end
      object ViewFindGroupName: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'GroupName'
        Visible = False
        Styles.Content = dmMCS.cxStyle17
        Width = 131
      end
      object ViewFindISTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        Visible = False
      end
    end
    object LevelFind: TcxGridLevel
      GridView = ViewFind
    end
  end
end
