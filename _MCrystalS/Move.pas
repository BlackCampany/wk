unit Move;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan, cxCalendar;

type
  TfmMove = class(TForm)
    FormPlacementMove: TFormPlacement;
    ViewM: TcxGridDBTableView;
    LevelM: TcxGridLevel;
    GridM: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    amMove: TActionManager;
    acExit: TAction;
    ViewMISKL: TcxGridDBColumn;
    ViewMICODE: TcxGridDBColumn;
    ViewMIDATE: TcxGridDBColumn;
    ViewMITYPED: TcxGridDBColumn;
    ViewMIDDOC: TcxGridDBColumn;
    ViewMQUANT: TcxGridDBColumn;
    ViewMREMN: TcxGridDBColumn;
    ViewMQUANTF: TcxGridDBColumn;
    Label1: TLabel;
    ViParts: TcxGridDBTableView;
    LeParts: TcxGridLevel;
    GrParts: TcxGrid;
    ViPartsIDSKL: TcxGridDBColumn;
    ViPartsIDATEDOC: TcxGridDBColumn;
    ViPartsITYPED: TcxGridDBColumn;
    ViPartsIDHEAD: TcxGridDBColumn;
    ViPartsIDCARD: TcxGridDBColumn;
    ViPartsIDPOS: TcxGridDBColumn;
    ViPartsID: TcxGridDBColumn;
    ViPartsPARTIN: TcxGridDBColumn;
    ViPartsQPART: TcxGridDBColumn;
    ViPartsQREMN: TcxGridDBColumn;
    ViPartsPRICEIN: TcxGridDBColumn;
    ViPartsPRICEIN0: TcxGridDBColumn;
    ViPartsPRICER: TcxGridDBColumn;
    ViPartsNAME: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    acParts: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPartsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure prFormMove(iSkl,iCode,iDateB,iDateE:INteger;Name:String);

var
  fmMove: TfmMove;

implementation

uses Un1, Period1, Dm;

{$R *.dfm}

procedure prFormMove(iSkl,iCode,iDateB,iDateE:INteger;Name:String);
begin
  fmMove.Tag:=iSkl;
  fmMove.GridM.Tag:=iCode;
  fmMove.Label1.Caption:=Name;

  with dmMCS do
  begin
    fmMove.ViewM.BeginUpdate;
    quMove.Active:=False;
    quMove.SQL.Clear;
    quMove.SQL.Add('EXECUTE [dbo].[prGetMoveCard] '+its(iSkl)+','+its(iCode)+','+its(iDateB)+','+its(iDateE));
    quMove.Active:=True;
    quMove.First;
    fmMove.ViewM.EndUpdate;
    fmMove.Caption:=Name+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy ',iDateE);

    if fmMove.GrParts.Visible then
    begin 
      CommonSet.DateBeg:=iDateB;
      CommonSet.DateEnd:=iDateE;
      fmMove.acParts.Execute
    end;

  end;
end;


procedure TfmMove.FormCreate(Sender: TObject);
begin
  FormPlacementMove.IniFileName := sFormIni;
  FormPlacementMove.Active:=True;
  GridM.Align:=AlClient;
  ViewM.RestoreFromIniFile(sGridIni);
end;

procedure TfmMove.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmMove.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMove.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    prFormMove(fmMove.Tag,GridM.Tag,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),Label1.Caption);
  end;
end;

procedure TfmMove.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel6(ViewM)
end;

procedure TfmMove.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMove.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewM.StoreToIniFile(sGridIni,False);
  GrParts.Visible:=False;
end;

procedure TfmMove.acPartsExecute(Sender: TObject);
begin
  with dmMCS do
  begin
    if GrParts.Visible=False then GrParts.Visible:=True;

//   prFormMove(fmMove.Tag,GridM.Tag,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),Label1.Caption);

    ViParts.BeginUpdate;
    dsquParts.DataSet:=nil;
    quParts.Active:=False;
    quParts.SQL.Clear;
    quParts.SQL.Add('');
    quParts.SQL.Add('declare @ISKL int = '+its(fmMove.Tag));
    quParts.SQL.Add('declare @ICODE int = '+its(GridM.Tag));
    quParts.SQL.Add('declare @IDATEB int = '+its(Trunc(CommonSet.DateBeg)));
    quParts.SQL.Add('declare @IDATEE int = '+its(Trunc(CommonSet.DateEnd)));

    quParts.SQL.Add(' ( SELECT [IDSKL],[IDATEDOC],[ITYPED],[IDHEAD],[IDCARD],[IDPOS],[ID],[PARTIN],[QUANT]*(-1) as [QPART],[QUANT]*0 as [QREMN],[PRICEIN],[PRICEIN0],[PRICER],dtype.NAME FROM [MCRYSTAL].[dbo].[PARTOUT]');
    quParts.SQL.Add(' left join [dbo].[DOCTYPE] dtype on dtype.ITD=[ITYPED]');
    quParts.SQL.Add('  where [IDSKL]=@ISKL  and [IDATEDOC]>=@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=@ICODE');
    quParts.SQL.Add('   UNION ALL');
    quParts.SQL.Add(' SELECT [IDSKL],[IDATEDOC],[ITYPED],[IDHEAD],[IDCARD],[IDPOS],[ID],0,[QPART],[QREMN],[PRICEIN],[PRICEIN0],[PRICER],dtype.NAME FROM [MCRYSTAL].[dbo].[PARTIN]');
    quParts.SQL.Add(' left join [dbo].[DOCTYPE] dtype on dtype.ITD=[ITYPED]');
    quParts.SQL.Add('  where [IDSKL]=@ISKL and [IDATEDOC]>=@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=@ICODE)');
    quParts.SQL.Add('  order by [IDATEDOC],[ITYPED]');
    quParts.Active:=True;

    dsquParts.DataSet:=quParts;
    ViParts.EndUpdate;
  end;
end;

end.
