object dmOPSV: TdmOPSV
  OldCreateOrder = False
  Left = 71
  Top = 660
  Height = 276
  Width = 498
  object msConnection: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 'FILE NAME=C:\_MCrystalS\Bin\MCR.udl'
    ConnectionTimeout = 1800
    DefaultDatabase = 'MCRYSTAL'
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 13
  end
  object quShops: TADOQuery
    Connection = msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDS] FROM [dbo].[DEPARTS]'
      'group by [IDS]'
      'order by [IDS] ')
    Left = 120
    Top = 12
    object quShopsIDS: TIntegerField
      FieldName = 'IDS'
    end
  end
  object quSel: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      '')
    Left = 180
    Top = 13
  end
  object quA: TADOQuery
    Connection = msConnection
    CommandTimeout = 500
    Parameters = <>
    Left = 232
    Top = 12
  end
  object quGetId: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'ITN'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'EXECUTE [dbo].[fGetId]  :ITN')
    Left = 288
    Top = 13
    object quGetIdretnum: TIntegerField
      FieldName = 'retnum'
      ReadOnly = True
    end
  end
  object quD: TADOQuery
    Connection = msConnection
    CommandTimeout = 500
    Parameters = <>
    Left = 348
    Top = 16
  end
end
