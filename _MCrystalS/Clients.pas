unit Clients;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, RXSplit, ComCtrls, ExtCtrls,
  SpeedBar, Placemnt, Menus, ActnList, XPStyleActnCtrls, ActnMan,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxCheckBox, cxContainer,
  cxTextEdit;

type
  TfmClients = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    Panel1: TPanel;
    Panel2: TPanel;
    CliTree: TTreeView;
    RxSplitter1: TRxSplitter;
    Panel3: TPanel;
    CliGr: TcxGrid;
    CliView: TcxGridDBTableView;
    CliLevel: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    CliViewId: TcxGridDBColumn;
    CliViewINN: TcxGridDBColumn;
    CliViewName: TcxGridDBColumn;
    CliViewFullName: TcxGridDBColumn;
    CliViewNAMEOTP: TcxGridDBColumn;
    CliViewADROTPR: TcxGridDBColumn;
    CliViewRSch: TcxGridDBColumn;
    CliViewKSch: TcxGridDBColumn;
    CliViewBank: TcxGridDBColumn;
    CliViewBik: TcxGridDBColumn;
    CliViewGln: TcxGridDBColumn;
    CliViewPayNDS: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    amCli: TActionManager;
    acAddCli: TAction;
    acEditCli: TAction;
    acViewCli: TAction;
    acDelCli: TAction;
    Label1: TLabel;
    TextEdit1: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    acFCliCode: TAction;
    acUnDelete: TAction;
    PopupMenu2: TPopupMenu;
    N7: TMenuItem;
    CliViewDayZ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure CliTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure SpeedItem11Click(Sender: TObject);
    procedure acAddCliExecute(Sender: TObject);
    procedure acEditCliExecute(Sender: TObject);
    procedure acViewCliExecute(Sender: TObject);
    procedure acDelCliExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acFCliCodeExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure CliTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure CliViewStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure CliTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure CliViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acUnDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClients: TfmClients;
  bDrCliGroup:Boolean=False;

implementation

uses Dm, Un1, AddClassif, AddCli, mFindCli, DocSIn, DocSOut, Period2,
  Period3;

{$R *.dfm}

procedure TfmClients.FormCreate(Sender: TObject);
begin
  CliTree.Items.BeginUpdate;
  prReadClassCli;
  ClassExpNewTCli(nil,CliTree);
  CliTree.Items.EndUpdate;

  CliView.RestoreFromIniFile(sGridIni);

  FormPlacement1.IniFileName := sFormIni;
  FormPlacement1.Active:=True;
  Panel3.Align:=AlClient;
end;

procedure TfmClients.CliTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bRefreshCli=False then exit;
  with dmMCS do
  begin
    try
      CliView.BeginUpdate;
      prRefreshCli(Integer(Node.Data),cxCheckBox1.Checked);
      acUnDelete.Enabled:=cxCheckBox1.Checked;
    finally
      CliView.EndUpdate;
    end;
  end;
end;

procedure TfmClients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CliView.StoreToIniFile(sGridIni,False);
end;

procedure TfmClients.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmClients.N1Click(Sender: TObject);
Var iNum,i:Integer;
     TreeNode : TTreeNode;
begin

  if not CanDo('prAddClientsGr') then begin Showmessage('��� ����.'); exit; end;

  //�������� ������
  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ����������';
  fmAddClassif.Label1.Caption:='���������� ������ ������������.';
  fmAddClassif.TextEdit1.Text:='';
  fmAddClassif.Label3.Visible:=False;
  fmAddClassif.cxCalcEdit1.Visible:=False;

  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    //�������� ������
    with dmMCS do
    begin
      iNum:=fGetID(7); //������������� ����� �������

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[CLASSIFCLI]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([ID],[IDPARENT],[NAME])');
      quA.SQL.Add('VALUES  ('+its(iNum)+',0,'''+fmAddClassif.TextEdit1.Text+''')');
      quA.ExecSQL;

      CliTree.Items.BeginUpdate;

      TreeNode:=CliTree.Items.AddChildObject(nil,fmAddClassif.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      CliTree.Items.EndUpdate;

      FOR i:=0 To CliTree.Items.Count-1 Do
        IF Integer(CliTree.Items[i].Data) = iNum Then
        Begin
          CliTree.Items[i].Expand(False);
          CliTree.Items[i].Selected:=True;
          CliTree.Repaint;
          Break;
        End;

    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmClients.N2Click(Sender: TObject);
Var iNum,i,iParent:Integer;
    TreeNode : TTreeNode;
    CurNode:TTreeNode;
begin
  //�������� ���������

  if not CanDo('prAddClientsSubGr') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    CurNode:=CliTree.Selected;
    CurNode.Expand(False);

    iParent:=INteger(CurNode.Data);
    fmAddClassif:=TfmAddClassif.Create(Application);
    fmAddClassif.Caption:='�������������';
    fmAddClassif.Label1.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
    fmAddClassif.TextEdit1.Text:='';
    fmAddClassif.Label3.Visible:=False;
    fmAddClassif.cxCalcEdit1.Visible:=False;
    fmAddClassif.ShowModal;

    if fmAddClassif.ModalResult=mrOk then
    begin
    //�������� ���������
      iNum:=fGetID(7);

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[CLASSIFCLI]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([ID],[IDPARENT],[NAME])');
      quA.SQL.Add('VALUES  ('+its(iNum)+','+its(iParent)+','''+fmAddClassif.TextEdit1.Text+''')');
      quA.ExecSQL;


      CliTree.Items.BeginUpdate;

      TreeNode:=CliTree.Items.AddChildObject(CurNode,fmAddClassif.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      CliTree.Items.EndUpdate;

      FOR i:=0 To CliTree.Items.Count-1 Do
        IF Integer(CliTree.Items[i].Data) = iNum Then
        Begin
          CliTree.Items[i].Expand(False);
          CliTree.Repaint;
          CliTree.Items[i].Selected:=True;
          Break;
        End;
    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmClients.N6Click(Sender: TObject);
Var iNum:Integer;
    TreeNode : TTreeNode;
    sName:String;
begin
  if not CanDo('prEditClientsGr') then begin Showmessage('��� ����.'); exit; end;
  TreeNode:=CliTree.Selected;
  if TreeNode=nil then exit;
  sName:=TreeNode.Text;
  iNum:=Integer(CliTree.Selected.Data);

  //�������� ��������
  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ���������';
  fmAddClassif.Label1.Caption:='�������� ��������� ������ ������������.';
  fmAddClassif.Label3.Visible:=False;
  fmAddClassif.cxCalcEdit1.Visible:=False;

  with dmMCS do
  begin
    quA.SQL.Clear;
    quA.SQL.Add('Select [NAME] from [dbo].[CLASSIFCLI]');
    quA.SQL.Add('where [ID]='+IntToStr(iNum));
    quA.Open;
    if quA.RecordCount>0 then
    begin
      fmAddClassif.TextEdit1.Text:=quA.FieldByName('NAME').Value;
    end;
    quA.Close;
  end;

  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    CliTree.Items.BeginUpdate;
    TreeNode.Text:=fmAddClassif.TextEdit1.Text;
    CliTree.Items.EndUpdate;

    with dmMCS do
    begin
      quA.SQL.Clear;
      quA.SQL.Add('Update [dbo].[CLASSIFCLI] Set [NAME]='''+fmAddClassif.TextEdit1.Text+'''');
      quA.SQL.Add('where [ID]='+IntToStr(iNum));
      quA.ExecSQL;
    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmClients.N4Click(Sender: TObject);
Var iNum:Integer;
    CurNode:TTreeNode;
begin
  //��������
  if not CanDo('prDelClientsGr') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    CurNode:=CliTree.Selected;
    if CurNode=nil then exit;

    if MessageDlg('�� ������������� ������ ������� ������ - '+CliTree.Selected.Text+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iNum:=Integer(CurNode.Data);
      if CurNode.getFirstChild=nil then
      begin
        if not fCanDel(4) then
        begin
          ShowMessage('�������� ���������� - ������ �� �����.');
          exit;
        end else
        begin
          quA.Close;
          quA.SQL.Clear;
          quA.SQL.Add('delete from [dbo].[CLASSIFCLI] where [ID]='+IntToStr(iNum));
          quA.ExecSQL;

          CliTree.Selected.Delete;
        end;
      end
      else
      begin
        ShowMessage('�������� ���������� - ���� ������� ������.');
        exit;
      end;
    end;
  end;
end;

procedure TfmClients.SpeedItem11Click(Sender: TObject);
begin
  prNExportExel6(CliView);
end;

procedure TfmClients.acAddCliExecute(Sender: TObject);
Var IdCli:Integer;
begin
  //�������� ��
  if not CanDo('prAddClients') then begin Showmessage('��� ����.'); exit; end;
  fmAddCli.prSetEditCli(0);
  fmAddCli.ShowModal;
  if fmAddCli.ModalResult=mrOk then
  begin
    // ��������� �����������
    IdCli:=fGetID(8);

    with dmMCS do
    begin
      try
        with fmAddCli do
        begin
          quClients.Append;
          quClientsId.AsInteger:=IdCli;
          quClientsIdParent.AsInteger:=Integer(Integer(CliTree.Selected.Data));
          quClientsIActive.AsInteger:=1;
          quClientsName.AsString:=cxTextEdit1.Text;    //��������
          quClientsFullName.AsString:=cxTextEdit2.Text;     //������ ��������
          quClientsINN.AsString:=cxTextEdit3.Text;        //���
          quClientsKPP.AsString:=cxTextEdit4.Text;        //���
          quClientsPostIndex.AsString:=cxTextEdit5.Text;   //������
          quClientsGorod.AsString:=cxTextEdit6.Text;        //�����
          quClientsStreet.AsString:=cxTextEdit7.Text;      //�����
          quClientsHouse.AsString:=cxTextEdit8.Text;       //���
          quClientsNAMEOTP.AsString:=cxTextEdit9.Text;     //���������������
          quClientsADROTPR.AsString:=cxTextEdit10.Text;    //����� ���������������
          quClientsRSch.AsString:=cxTextEdit11.Text;       //��������� ����
          quClientsBik.AsString:=cxTextEdit12.Text;         //���
          quClientsBank.AsString:=cxTextEdit13.Text;      //����
          quClientsKSch.AsString:=cxTextEdit14.Text;       //��� ����
          quClientsGln.AsString:=cxTextEdit18.Text;        //���
          quClientsEmail.AsString:=cxTextEdit19.Text;     //�����-��

          quClientsIType.AsInteger:=cxImageComboBox1.ItemIndex;    //����������� ����
          quClientsZakazT.AsInteger:=cxImageComboBox2.ItemIndex;   //��� ������ ��
          quClientsEDIProvider.AsInteger:=cxLookupComboBox1.EditValue;     //EDI ���������

          if cxCheckBox2.Checked then quClientsPayNDS.AsInteger:=1 else quClientsPayNDS.AsInteger:=0; //���������� ���
//          if cxCheckBox5.Checked then quClientsNoNaclZ.AsInteger:=1 else quClientsNoNaclZ.AsInteger:=0; //
//          if cxCheckBox7.Checked then quClientsNoRecadv.AsInteger:=1 else quClientsNoRecadv.AsInteger:=0; // �� ���������� RECADV

          quClientsMinSumZak.AsFloat:=cxCalcEdit1.Value;

          quClients.Post;

          CliView.Controller.FocusRecord(Cliview.DataController.FocusedRowIndex,True);
        end;
      except
      end;
    end;

  end;
end;

procedure TfmClients.acEditCliExecute(Sender: TObject);
Var IdCli:Integer;
    j:INteger;
    Rec:TcxCustomGridRecord;
begin
  //������������� ��
  if not CanDo('prEditClients') then begin Showmessage('��� ����.'); exit; end;

  IdCli:=0;

  if CliView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CliView.Controller.SelectedRecords[0];
  for j:=0 to Rec.ValueCount-1 do
    if CliView.Columns[j].Name='CliViewId' then begin IdCli:=Rec.Values[j]; break; end;;

  if IdCli>0 then
  begin
    if dmMCS.quClients.Locate('Id',IdCli,[]) then
    begin
      fmAddCli.prSetEditCli(1);
      fmAddCli.ShowModal;
      if fmAddCli.ModalResult=mrOk then
      begin
        // ���������� �����������
        with dmMCS do
        begin
          try
            with fmAddCli do
            begin
              quClients.Edit;
              quClientsName.AsString:=cxTextEdit1.Text;    //��������
              quClientsFullName.AsString:=cxTextEdit2.Text;     //������ ��������
              quClientsINN.AsString:=cxTextEdit3.Text;        //���
              quClientsKPP.AsString:=cxTextEdit4.Text;        //���
              quClientsPostIndex.AsString:=cxTextEdit5.Text;   //������
              quClientsGorod.AsString:=cxTextEdit6.Text;        //�����
              quClientsStreet.AsString:=cxTextEdit7.Text;      //�����
              quClientsHouse.AsString:=cxTextEdit8.Text;       //���
              quClientsNAMEOTP.AsString:=cxTextEdit9.Text;     //���������������
              quClientsADROTPR.AsString:=cxTextEdit10.Text;    //����� ���������������
              quClientsRSch.AsString:=cxTextEdit11.Text;       //��������� ����
              quClientsBik.AsString:=cxTextEdit12.Text;         //���
              quClientsBank.AsString:=cxTextEdit13.Text;      //����
              quClientsKSch.AsString:=cxTextEdit14.Text;       //��� ����
              quClientsGln.AsString:=cxTextEdit18.Text;        //���
              quClientsEmail.AsString:=cxTextEdit19.Text;     //�����-��

              quClientsIType.AsInteger:=cxImageComboBox1.ItemIndex;    //����������� ����
              quClientsZakazT.AsInteger:=cxImageComboBox2.ItemIndex;   //��� ������ ��
              quClientsEDIProvider.AsInteger:=cxLookupComboBox1.EditValue;     //EDI ���������

              if cxCheckBox2.Checked then quClientsPayNDS.AsInteger:=1 else quClientsPayNDS.AsInteger:=0; //���������� ���
//              if cxCheckBox5.Checked then quClientsNoNaclZ.AsInteger:=1 else quClientsNoNaclZ.AsInteger:=0; //
//              if cxCheckBox7.Checked then quClientsNoRecadv.AsInteger:=1 else quClientsNoRecadv.AsInteger:=0; // �� ���������� RECADV

              quClientsMinSumZak.AsFloat:=cxCalcEdit1.Value;
              quClientsDayZ.AsInteger:=cxSpinEdit1.Value;

              quClients.Post;
            end;
          except
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.acViewCliExecute(Sender: TObject);
Var IdCli:Integer;
    j:INteger;
    Rec:TcxCustomGridRecord;
begin
  //������������� ��
  if not CanDo('prViewClients') then begin Showmessage('��� ����.'); exit; end;

  IdCli:=0;

  if CliView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CliView.Controller.SelectedRecords[0];
  for j:=0 to Rec.ValueCount-1 do
    if CliView.Columns[j].Name='CliViewId' then begin IdCli:=Rec.Values[j]; break; end;;

  if IdCli>0 then
  begin
    if dmMCS.quClients.Locate('Id',IdCli,[]) then
    begin
      fmAddCli.prSetEditCli(2);
      fmAddCli.ShowModal;
    end;
  end;
end;

procedure TfmClients.acDelCliExecute(Sender: TObject);
Var IdCli:Integer;
    j:INteger;
    Rec:TcxCustomGridRecord;
begin
  //������������� ��
  if not CanDo('prDelClients') then begin Showmessage('��� ����.'); exit; end;

  IdCli:=0;

  if CliView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CliView.Controller.SelectedRecords[0];
  for j:=0 to Rec.ValueCount-1 do
    if CliView.Columns[j].Name='CliViewId' then begin IdCli:=Rec.Values[j]; break; end;;

  if IdCli>0 then
  begin
    if dmMCS.quClients.Locate('Id',IdCli,[]) then
    begin
      if MessageDlg('�� ������������� ������ ������� ����������� "'+dmMCS.quClientsName.AsString+'" ��� - '+dmMCS.quClientsId.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMCS do
        begin
          CliView.BeginUpdate;

          quClients.Edit;
          quClientsIActive.AsInteger:=0;
          quClients.Post;

          quClients.Requery();

          CliView.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.cxButton1Click(Sender: TObject);
Var iC,i:Integer;
begin
//����� �� ����
  if Length(TextEdit1.Text)<1 then exit;
  with dmMCS do
  begin
    iC:=StrToIntDef(TextEdit1.Text,0);
    if iC>0 then
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
      quFCli.SQL.Add('where cli.[Id]='+its(iC));
      quFCli.Active:=True;

      bRefreshCli:=False;
      for i:=0 to CliTree.Items.Count-1 Do
      begin
        if Integer(CliTree.Items[i].Data) = quFCli.FieldByName('IdParent').AsInteger then
        begin
          CliTree.Items[i].Expand(False);
          CliTree.Repaint;
          CliTree.Items[i].Selected:=True;
          try
            CliView.BeginUpdate;
            prRefreshCli(quFCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
            quClients.Locate('ID',quFCli.FieldByName('Id').AsInteger,[]);
          finally
            CliView.EndUpdate;
            CliGr.SetFocus;
            CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCli:=True;

      if quFCli.FieldByName('IActive').AsInteger=0 then
      begin
        Showmessage('�������� ���������� ��������� � ��������������.');
      end;

      quFCli.Active:=False;
    end;
  end;
end;

procedure TfmClients.acFCliCodeExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmClients.cxButton3Click(Sender: TObject);
Var i:Integer;
    sBar:String;
begin
  //����� �� ���
  if Length(TextEdit1.Text)<>13 then begin showmessage('������������ �������� (������ ���� 13 ��������).'); exit; end;
  with dmMCS do
  begin
    sBar:=Trim(TextEdit1.Text);

    quFCli.Active:=False;
    quFCli.SQL.Clear;
    quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
    quFCli.SQL.Add('where cli.[Gln]='''+sBar+'''');
    quFCli.Active:=True;

    bRefreshCli:=False;
    for i:=0 to CliTree.Items.Count-1 Do
    begin
      if Integer(CliTree.Items[i].Data) = quFCli.FieldByName('IdParent').AsInteger then
      begin
        CliTree.Items[i].Expand(False);
        CliTree.Repaint;
        CliTree.Items[i].Selected:=True;
        try
          CliView.BeginUpdate;
          prRefreshCli(quFCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
          quClients.Locate('ID',quFCli.FieldByName('Id').AsInteger,[]);
        finally
          CliView.EndUpdate;
          CliGr.SetFocus;
          CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
        end;
        Break;
      End;
    end;
    bRefreshCli:=True;

    if quFCli.FieldByName('IActive').AsInteger=0 then
    begin
      Showmessage('�������� ���������� ��������� � ��������������.');
    end;

    quFCli.Active:=False;
  end;
end;

procedure TfmClients.cxButton2Click(Sender: TObject);
Var i:Integer;
    s1:String;
begin
//����� �� ��������
  if Length(TextEdit1.Text)<3 then exit;
  with dmMCS do
  begin
    fmFindCli.ViewFind.BeginUpdate;
    dsquFindCli.DataSet:=nil;
    try
      s1:=trim(TextEdit1.Text);
      quFindCli.Active:=False;
      quFindCli.SQL.Clear;
      quFindCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
      quFindCli.SQL.Add('where Upper(cli.[Name]) like ''%'+AnsiUpperCase(s1)+'%''');
      quFindCli.Active:=True;
    finally
      dsquFindCli.DataSet:=quFindCli;
      fmFindCli.ViewFind.EndUpdate;
    end;
    fmFindCli.ShowModal;

    if fmFindCli.ModalResult=mrOk then
    begin
      if quFindCli.RecordCount>0 then
      begin
        bRefreshCli:=False;
        for i:=0 to CliTree.Items.Count-1 Do
        begin
          if Integer(CliTree.Items[i].Data) = quFindCli.FieldByName('IdParent').AsInteger then
          begin
            CliTree.Items[i].Expand(False);
            CliTree.Repaint;
            CliTree.Items[i].Selected:=True;
            try
              CliView.BeginUpdate;
              prRefreshCli(quFindCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
              quClients.Locate('ID',quFindCli.FieldByName('Id').AsInteger,[]);
            finally
              CliView.EndUpdate;
              CliGr.SetFocus;
              CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
            end;
            Break;
          End;
        end;
        bRefreshCli:=True;

        if quFindCli.FieldByName('IActive').AsInteger=0 then
        begin
          Showmessage('�������� ���������� ��������� � ��������������.');
        end;
        quFindCli.Active:=False;

      end;
    end;
  end;
end;

procedure TfmClients.CliTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrCliGroup then Accept:=True;
end;

procedure TfmClients.CliViewStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  bDrCliGroup:=True;
end;

procedure TfmClients.CliTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sNUms:String;
begin
  if bDrCliGroup then
  begin
    bDrCliGroup:=False;
    sGr:=CliTree.DropTarget.Text;
    iGr:=Integer(CliTree.DropTarget.data);
    iCo:=CliView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMCS do
        begin
          sNUms:='';
          for i:=0 to CliView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=CliView.Controller.SelectedRecords[i];

            iNum:=0;
            for j:=0 to Rec.ValueCount-1 do
            begin
              if CliView.Columns[j].Name='CliViewId' then begin iNum:=Rec.Values[j]; break;end;
            end;


          //��� ��� - ����������
            if iNum>0 then sNums:=sNums+','+its(iNum);
          end;
          if length(sNUms)>1 then
          begin
            delete(sNUms,1,1); //������ ������ �������

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update [dbo].[CLIENTS] Set ');
            quE.SQL.Add('IdParent='+its(iGr));
            quE.SQL.Add('where Id in ('+sNUms+')');
            quE.ExecSQL;

            delay(100);

            CliView.BeginUpdate;
            quClients.Requery();
            CliView.EndUpdate;
            CliView.Controller.FocusRecord(cliview.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.CliViewCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if iDirect>0 then
  begin
    with dmMCS do
    begin
      if (iDirect=1)and(fmDocsInS.Showing=True) then
      begin
        if quClients.RecordCount>0 then
        begin
          fmDocsInS.cxButtonEdit1.Tag:=quClientsId.AsInteger;
          fmDocsInS.cxButtonEdit1.Text:=quClientsName.AsString;
          if quClientsPayNDS.AsInteger>0 then begin fmDocsInS.Label17.Caption:='���������� ���'; fmDocsInS.Label17.Tag:=1; end
          else begin fmDocsInS.Label17.Caption:='-'; fmDocsInS.Label17.Tag:=0; end;

          Close;
        end;
      end;
      if (iDirect=2)and(fmDocSOut.Showing=True) then
      begin
        if quClients.RecordCount>0 then
        begin
          fmDocSOut.cxButtonEdit1.Tag:=quClientsId.AsInteger;
          fmDocSOut.cxButtonEdit1.Text:=quClientsName.AsString;
          if quClientsPayNDS.AsInteger>0 then begin fmDocSOut.Label17.Caption:='���������� ���'; fmDocsInS.Label17.Tag:=1; end
          else begin fmDocSOut.Label17.Caption:='-'; fmDocSOut.Label17.Tag:=0; end;

          Close;
        end;
      end;
      if (iDirect=101)and(fmPer1.Showing=True) then
      begin
        if quClients.RecordCount>0 then
        begin
          fmPer1.cxButtonEdit1.Tag:=quClientsId.AsInteger;
          fmPer1.cxButtonEdit1.Text:=quClientsName.AsString;

          Close;
        end;
      end;
      if (iDirect=102)and(fmPer2.Showing=True) then
      begin
        if quClients.RecordCount>0 then
        begin
          fmPer2.cxButtonEdit1.Tag:=quClientsId.AsInteger;
          fmPer2.cxButtonEdit1.Text:=quClientsName.AsString;
          
          Close;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.acUnDeleteExecute(Sender: TObject);
Var IdCli:Integer;
    j:INteger;
    Rec:TcxCustomGridRecord;
begin   //��������� � ������������
  if not CanDo('prDelClients') then begin Showmessage('��� ����.'); exit; end;

  IdCli:=0;

  if CliView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CliView.Controller.SelectedRecords[0];
  for j:=0 to Rec.ValueCount-1 do
    if CliView.Columns[j].Name='CliViewId' then begin IdCli:=Rec.Values[j]; break; end;;

  if IdCli>0 then
  begin
    if dmMCS.quClients.Locate('Id',IdCli,[]) then
    begin
      if MessageDlg('�� ������������� ������ ������������ ����������� "'+dmMCS.quClientsName.AsString+'" ��� - '+dmMCS.quClientsId.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMCS do
        begin
          CliView.BeginUpdate;

          quClients.Edit;
          quClientsIActive.AsInteger:=1;
          quClients.Post;

          quClients.Requery();

          CliView.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.FormShow(Sender: TObject);
begin
  acUnDelete.Enabled:=cxCheckBox1.Checked;
end;

procedure TfmClients.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  acUnDelete.Enabled:=cxCheckBox1.Checked;
end;

end.
