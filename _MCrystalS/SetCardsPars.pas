unit SetCardsPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxDropDownEdit, cxCalc, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, StdCtrls, ExtCtrls, Menus,
  cxLookAndFeelPainters, cxButtons, cxCheckBox, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit, DB, ADODB;

type
  TfmSetCardsParams = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Label2: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxCheckBox4: TcxCheckBox;
    Label3: TLabel;
    cxCheckBox5: TcxCheckBox;
    Label4: TLabel;
    cxCheckBox6: TcxCheckBox;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Label5: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    Label6: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    cxCheckBox2: TcxCheckBox;
    Label7: TLabel;
    cxLookupComboBox4: TcxLookupComboBox;
    cxCheckBox3: TcxCheckBox;
    Label8: TLabel;
    cxLookupComboBox5: TcxLookupComboBox;
    cxCheckBox7: TcxCheckBox;
    quAVid: TADOQuery;
    quAVidID: TIntegerField;
    quAVidNAME: TStringField;
    quAVidAFM: TSmallintField;
    dsquAVid: TDataSource;
    quCateg: TADOQuery;
    quCategID: TIntegerField;
    quCategSID: TStringField;
    quCategCOMMENT: TStringField;
    dsquCateg: TDataSource;
    quCountry: TADOQuery;
    quCountryIDPARENT: TIntegerField;
    quCountryID: TIntegerField;
    quCountryNAME: TStringField;
    quCountryKOD: TIntegerField;
    dsquCountry: TDataSource;
    quCatMan: TADOQuery;
    quCatManID: TIntegerField;
    quCatManID_PARENT: TIntegerField;
    quCatManNAME: TStringField;
    quCatManUVOLNEN: TSmallintField;
    quCatManPASSW: TStringField;
    quCatManMODUL1: TSmallintField;
    quCatManMODUL2: TSmallintField;
    quCatManMODUL3: TSmallintField;
    quCatManMODUL4: TSmallintField;
    quCatManMODUL5: TSmallintField;
    quCatManMODUL6: TSmallintField;
    quCatManBARCODE: TStringField;
    quCatManPATHEXP: TStringField;
    quCatManHOSTFTP: TStringField;
    quCatManLOGINFTP: TStringField;
    quCatManPASSWFTP: TStringField;
    quCatManSCLI: TStringField;
    quCatManALLSHOPS: TSmallintField;
    quCatManCATMAN: TSmallintField;
    quCatManPOSTACCESS: TSmallintField;
    dsquCatMan: TDataSource;
    quBrands: TADOQuery;
    quBrandsID: TIntegerField;
    quBrandsNAMEBRAND: TStringField;
    dsquBrands: TDataSource;
    Label9: TLabel;
    cxCheckBox8: TcxCheckBox;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure Label5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetCardsParams: TfmSetCardsParams;

implementation

uses Makers;

{$R *.dfm}

procedure TfmSetCardsParams.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if fmMakers.Showing=False then
  begin
    if fmMakers.quMakers.Active=False then
    begin
      fmMakers.ViewMakers.BeginUpdate;
      fmMakers.quMakers.Active:=False;
      fmMakers.quMakers.Active:=True;

      if cxButtonEdit1.Tag>0 then fmMakers.quMakers.Locate('ID',cxButtonEdit1.Tag,[]);

      fmMakers.ViewMakers.EndUpdate;
    end;
    fmMakers.Show;
  end;  
end;

procedure TfmSetCardsParams.Label5Click(Sender: TObject);
begin
  fmSetCardsParams.cxButtonEdit1.Tag:=0;
  fmSetCardsParams.cxButtonEdit1.Text:='';
end;

end.
