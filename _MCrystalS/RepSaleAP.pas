unit RepSaleAP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo, ExtCtrls,
  ComCtrls, JvSpeedbar, JvExExtCtrls, JvExtComponent, Menus
  ,Un1
  ,ADODB, ActnList, XPStyleActnCtrls, ActnMan, FR_DSet, FR_DBSet, FR_Class;

type
     TDelta = record
     ColB,ColE:SmallInt;
     end;

  TfmRepSaleAP = class(TForm)
    SpeedBar1: TJvSpeedBar;
    SpeedbarSection1: TJvSpeedBarSection;
    SpeedItem1: TJvSpeedItem;
    SpeedItem2: TJvSpeedItem;
    SpeedItem3: TJvSpeedItem;
    SpeedItem4: TJvSpeedItem;
    SpeedItem5: TJvSpeedItem;
    SpeedItem6: TJvSpeedItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridRepSaleAP: TcxGrid;
    ViewRepSaleAP: TcxGridDBTableView;
    LevelRepSaleAP: TcxGridLevel;
    ViewRepSaleAPId_Depart: TcxGridDBColumn;
    ViewRepSaleAPIDate: TcxGridDBColumn;
    ViewRepSaleAPRowNum: TcxGridDBColumn;
    ViewRepSaleAPDateOperation: TcxGridDBColumn;
    ViewRepSaleAPCode: TcxGridDBColumn;
    ViewRepSaleAPBARCODE: TcxGridDBColumn;
    ViewRepSaleAPFULLNAME: TcxGridDBColumn;
    ViewRepSaleAPVOL: TcxGridDBColumn;
    ViewRepSaleAPAVID: TcxGridDBColumn;
    ViewRepSaleAPQuant: TcxGridDBColumn;
    frRepAP: TfrReport;
    frdsSpecAP: TfrDBDataSet;
    frdsSpecAPItog: TfrDBDataSet;
    ViewRepSaleAPQuantDAL: TcxGridDBColumn;
    ViewRepSaleAPMAKER: TcxGridDBColumn;
    ViewRepSaleAPNAME: TcxGridDBColumn;
    ViewRepSaleAPINN: TcxGridDBColumn;
    ViewRepSaleAPKPP: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acMoveRep11Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    sDeps:String;
    iDateB,iDateE:INteger;
  end;

var
  fmRepSaleAP: TfmRepSaleAP;

implementation

uses Dm;


{$R *.dfm}

procedure TfmRepSaleAP.FormCreate(Sender: TObject);
begin
  GridRepSaleAP.Align:=alClient;
end;

procedure TfmRepSaleAP.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmRepSaleAP.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel15(ViewRepSaleAP);
end;

procedure TfmRepSaleAP.SpeedItem4Click(Sender: TObject);
var iDep:Integer;
begin
  //������
  with dmMCS do
  begin
    if quRepSaleAP.RecordCount>0 then
    begin
      iDep:=quRepSaleAPId_Depart.AsInteger;

      quFDep.Active:=False;
      quFDep.Parameters.ParamByName('IDEP').Value:=iDep;
      quFDep.Active:=True;

      quRepSaleAPItog.Active:=False;

      quRepSaleAPItog.SQL.Clear;
      quRepSaleAPItog.SQL.Add('');

      quRepSaleAPItog.SQL.Add('DECLARE @SSKL varchar(900)');
      quRepSaleAPItog.SQL.Add('DECLARE @IDATEB int');
      quRepSaleAPItog.SQL.Add('DECLARE @IDATEE int');
      quRepSaleAPItog.SQL.Add('Set @IDATEB='+its(Trunc(CommonSet.DateBeg)));
      quRepSaleAPItog.SQL.Add('Set @IDATEE='+its(Trunc(CommonSet.DateEnd)));
      quRepSaleAPItog.SQL.Add('Set @SSKL='''+sDeps+'''');
      quRepSaleAPItog.SQL.Add('EXECUTE [dbo].[prRepSaleAPItog]  @SSKL,@IDATEB,@IDATEE');
      quRepSaleAPItog.Active:=True;


      frRepAP.LoadFromFile(CommonSet.PathReport+'RepRealAP.frf');

      frVariables.Variable['NameOrg']:=quFDepNAMEOTPR.AsString;
      frVariables.Variable['InnKpp']:=quFDepINN.AsString+'/'+quFDepKPP.AsString;
      frVariables.Variable['Adress']:=quFDepCITY.AsString+', '+quFDepSTREET.AsString+', '+quFDepHOUSE.AsString;
      frVariables.Variable['Info']:='���.� '+quFDepIDS.AsString+'. '+quFDepNAMEOTPR.AsString+' (���/��� '+quFDepINN.AsString+'/'+quFDepKPP.AsString+'). ����� '+quFDepCITY.AsString+', '+quFDepSTREET.AsString+', '+quFDepHOUSE.AsString;

      ViewRepSaleAP.BeginUpdate;
      frRepAP.PrepareReport;
      ViewRepSaleAP.EndUpdate;

      quFDep.Active:=False;
      quRepSaleAPItog.Active:=False;

      frRepAP.ShowPreparedReport;
    end;
  end;
end;

procedure TfmRepSaleAP.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmMCS do
  begin
    quRepSaleAP.Close;
  end;
end;

procedure TfmRepSaleAP.acMoveRep11Click(Sender: TObject);
begin
  prExpandVi(ViewRepSaleAP,True);
end;

procedure TfmRepSaleAP.N1Click(Sender: TObject);
begin
  prExpandVi(ViewRepSaleAP,False);
end;

end.
