unit DocHIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar, ADODB;

type
  TfmDocsInH = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    GridDocsIn: TcxGrid;
    ViewDocsIn: TcxGridDBTableView;
    LevelDocsIn: TcxGridLevel;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCardsIn: TcxGridDBTableView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Excel1: TMenuItem;
    acEdit1: TAction;
    acTestMoveDoc: TAction;
    N5: TMenuItem;
    acOprih: TAction;
    acRazrub: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acCheckBuh: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    teInLn: TdxMemData;
    teInLnDepart: TIntegerField;
    teInLnDateInvoice: TIntegerField;
    teInLnNumber: TStringField;
    teInLnCliName: TStringField;
    teInLnCode: TIntegerField;
    teInLniM: TIntegerField;
    teInLnNdsSum: TFloatField;
    teInLnNdsProc: TFloatField;
    teInLnKol: TFloatField;
    teInLnKolMest: TFloatField;
    teInLnKolWithMest: TFloatField;
    teInLnCenaTovar: TFloatField;
    teInLnNewCenaTovar: TFloatField;
    teInLnProc: TFloatField;
    teInLnSumCenaTovar: TFloatField;
    teInLnSumNewCenaTovar: TFloatField;
    teInLnCodeTara: TIntegerField;
    teInLnCenaTara: TFloatField;
    teInLnSumCenaTara: TFloatField;
    teInLnDepName: TStringField;
    teInLnCardName: TStringField;
    teInLniCat: TIntegerField;
    teInLniTop: TIntegerField;
    teInLniNov: TIntegerField;
    dsteInLn: TDataSource;
    Panel1: TPanel;
    Memo1: TcxMemo;
    acChangePost: TAction;
    N10: TMenuItem;
    acChangeTypePol: TAction;
    N11: TMenuItem;
    acPrintAddDocs: TAction;
    N12: TMenuItem;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teInLniMaker: TIntegerField;
    teInLnsMaker: TStringField;
    PopupMenu2: TPopupMenu;
    acGroupChange: TAction;
    N13: TMenuItem;
    acMoveCard: TAction;
    teBuffLn: TdxMemData;
    teBuffLniDep: TIntegerField;
    teBuffLnNumDoc: TStringField;
    teBuffLnCode: TIntegerField;
    teInLnV11: TIntegerField;
    teBuffLniMaker: TIntegerField;
    teBuffLnsMaker: TStringField;
    teBuffLniDateDoc: TIntegerField;
    teInLnVolIn: TFloatField;
    teInLnVol: TFloatField;
    teInLniMakerCard: TIntegerField;
    teInLnsMakerCard: TStringField;
    acCrVozTara: TAction;
    N14: TMenuItem;
    acPrintSCHF: TAction;
    N15: TMenuItem;
    ViewDocsInIDSKL: TcxGridDBColumn;
    ViewDocsInID: TcxGridDBColumn;
    ViewDocsInDATEDOC: TcxGridDBColumn;
    ViewDocsInNUMDOC: TcxGridDBColumn;
    ViewDocsInDATESF: TcxGridDBColumn;
    ViewDocsInNUMSF: TcxGridDBColumn;
    ViewDocsInIDCLI: TcxGridDBColumn;
    ViewDocsInSUMIN: TcxGridDBColumn;
    ViewDocsInSUMUCH: TcxGridDBColumn;
    ViewDocsInSUMTAR: TcxGridDBColumn;
    ViewDocsInIACTIVE: TcxGridDBColumn;
    ViewDocsInNAMECLI: TcxGridDBColumn;
    ViewDocsInNAMESKL: TcxGridDBColumn;
    fpfmDocsInH: TFormPlacement;
    acOplata: TAction;
    N16: TMenuItem;
    ViewDocsInSUMOPL: TcxGridDBColumn;
    quInLn: TADOQuery;
    dsquInLn: TDataSource;
    quInLnIDHEAD: TLargeintField;
    quInLnID: TLargeintField;
    quInLnNUM: TIntegerField;
    quInLnIDCARD: TIntegerField;
    quInLnSBAR: TStringField;
    quInLnIDM: TIntegerField;
    quInLnQUANT: TFloatField;
    quInLnPRICEIN: TFloatField;
    quInLnSUMIN: TFloatField;
    quInLnPRICEIN0: TFloatField;
    quInLnSUMIN0: TFloatField;
    quInLnPRICEUCH: TFloatField;
    quInLnSUMUCH: TFloatField;
    quInLnNDSPROC: TFloatField;
    quInLnSUMNDS: TFloatField;
    quInLnGTD: TStringField;
    quInLnSNUMHEAD: TStringField;
    quInLnDATEDOC: TDateTimeField;
    quInLnIDATEDOC: TIntegerField;
    quInLnIDCLI: TIntegerField;
    quInLnNUMDOC: TStringField;
    quInLnNUMSF: TStringField;
    quInLnIDSKL: TIntegerField;
    quInLnNAMEC: TStringField;
    quInLnAVID: TIntegerField;
    quInLnMAKER: TIntegerField;
    quInLnVOL: TFloatField;
    quInLnKREP: TFloatField;
    quInLnNAMEGR: TStringField;
    quInLnNAMEGR2: TStringField;
    quInLnCLINAME: TStringField;
    quInLnMAKNAME: TStringField;
    quInLnIDS: TIntegerField;
    quInLnDEPNAME: TStringField;
    ViewCardsInIDHEAD: TcxGridDBColumn;
    ViewCardsInID: TcxGridDBColumn;
    ViewCardsInNUM: TcxGridDBColumn;
    ViewCardsInIDCARD: TcxGridDBColumn;
    ViewCardsInSBAR: TcxGridDBColumn;
    ViewCardsInIDM: TcxGridDBColumn;
    ViewCardsInQUANT: TcxGridDBColumn;
    ViewCardsInPRICEIN: TcxGridDBColumn;
    ViewCardsInSUMIN: TcxGridDBColumn;
    ViewCardsInPRICEIN0: TcxGridDBColumn;
    ViewCardsInSUMIN0: TcxGridDBColumn;
    ViewCardsInPRICEUCH: TcxGridDBColumn;
    ViewCardsInSUMUCH: TcxGridDBColumn;
    ViewCardsInNDSPROC: TcxGridDBColumn;
    ViewCardsInSUMNDS: TcxGridDBColumn;
    ViewCardsInGTD: TcxGridDBColumn;
    ViewCardsInDATEDOC: TcxGridDBColumn;
    ViewCardsInIDATEDOC: TcxGridDBColumn;
    ViewCardsInIDCLI: TcxGridDBColumn;
    ViewCardsInNUMDOC: TcxGridDBColumn;
    ViewCardsInNUMSF: TcxGridDBColumn;
    ViewCardsInIDSKL: TcxGridDBColumn;
    ViewCardsInNAMEC: TcxGridDBColumn;
    ViewCardsInAVID: TcxGridDBColumn;
    ViewCardsInMAKER: TcxGridDBColumn;
    ViewCardsInVOL: TcxGridDBColumn;
    ViewCardsInKREP: TcxGridDBColumn;
    ViewCardsInNAMEGR: TcxGridDBColumn;
    ViewCardsInNAMEGR2: TcxGridDBColumn;
    ViewCardsInCLINAME: TcxGridDBColumn;
    ViewCardsInMAKNAME: TcxGridDBColumn;
    ViewCardsInIDS: TcxGridDBColumn;
    ViewCardsInDEPNAME: TcxGridDBColumn;
    quInLnQAPDAL: TFloatField;
    ViewCardsInQAPDAL: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure acTestMoveDocExecute(Sender: TObject);
    procedure ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOprihExecute(Sender: TObject);
    procedure acRazrubExecute(Sender: TObject);
    procedure acCheckBuhExecute(Sender: TObject);
    procedure acChangePostExecute(Sender: TObject);
    procedure acChangeTypePolExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
    procedure ViewCardsInSelectionChanged(Sender: TcxCustomGridTableView);
    procedure acMoveCardExecute(Sender: TObject);
    procedure acOplataExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

Function OprDocsIn(IDH:integer; Memo1:TcxMemo):Boolean;
Function OtkatDocsIn(IDH:integer; Memo1:TcxMemo):Boolean;

var
  fmDocsInH: TfmDocsInH;
  bClearDocIn:Boolean = false;
  bOpenSpIn:Boolean = False;

implementation

uses Un1, DM, Period1, MainMC, DocSIn, Oplata;

{$R *.dfm}

Function OtkatDocsIn(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ����� ���������.');

  if CanDo('prOffDocIn') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnIn.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNInID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNInIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNInIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 1');
        quProc.SQL.Add('EXECUTE [dbo].[prInSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


Function OprDocsIn(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOnDocIn') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnIn.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNInID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNInIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNInIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 3');
        quProc.SQL.Add('EXECUTE [dbo].[prInSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


procedure TfmDocsInH.prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������
// var user:string;
begin
  with dmMCS do
  begin
    bOpenSpIn:=True;
    if iT=0 then
    begin
      fmDocsInS.Caption:='���������: ����������.';

      fmDocsInS.Tag:=0;

      fmDocsInS.cxTextEdit1.Text:='';  fmDocsInS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsInS.cxTextEdit2.Text:=fGetNumDoc(fmMainMC.Label3.tag,1);  fmDocsInS.cxTextEdit2.Properties.ReadOnly:=False;
      fmDocsInS.cxTextEdit3.Text:='';  fmDocsInS.cxTextEdit3.Properties.ReadOnly:=False;

      fmDocsInS.cxDateEdit1.Date:=date;  fmDocsInS.cxDateEdit1.Properties.ReadOnly:=False;
      fmDocsInS.cxDateEdit2.Date:=date;  fmDocsInS.cxDateEdit2.Properties.ReadOnly:=False;

      fmDocsInS.cxButtonEdit1.Tag:=0;
      fmDocsInS.cxButtonEdit1.EditValue:=0;
      fmDocsInS.cxButtonEdit1.Text:='';
      fmDocsInS.cxButtonEdit1.Properties.ReadOnly:=True;

      fmDocsInS.Label17.Caption:='-';
      fmDocsInS.Label17.Tag:=0;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',fmMainMC.Label3.tag,[]);

      fmDocsInS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsInS.cxLabel5.Enabled:=True;
      fmDocsInS.cxLabel1.Enabled:=True;
      fmDocsInS.cxLabel2.Enabled:=True;
      fmDocsInS.cxLabel6.Enabled:=True;
      fmDocsInS.cxLabel3.Enabled:=True;
      fmDocsInS.cxLabel4.Enabled:=True;
      fmDocsInS.cxLabel11.Enabled:=True;
      fmDocsInS.cxLabel12.Enabled:=True;

      fmDocsInS.Panel4.Visible:=True;

      fmDocsInS.acSaveDoc.Enabled:=True;
      fmDocsInS.cxButton1.Enabled:=True;
      fmDocsInS.cxButton7.Enabled:=True;

      fmDocsInS.ViewDoc1.OptionsData.Editing:=True;
      fmDocsInS.ViewDoc1.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmDocsInS.Caption:='���������: ��������������.';

      fmDocsInS.Tag:=quTTNInID.AsInteger;

      fmDocsInS.cxTextEdit1.Text:=quTTNInNUMDOC.AsString;  fmDocsInS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsInS.cxTextEdit2.Text:=quTTNInNUMSF.AsString;  fmDocsInS.cxTextEdit2.Properties.ReadOnly:=False;
      fmDocsInS.cxTextEdit3.Text:=quTTNInSNUMZ.AsString; fmDocsInS.cxTextEdit3.Tag:=quTTNInIDZ.AsInteger; fmDocsInS.cxTextEdit3.Properties.ReadOnly:=False;

      fmDocsInS.cxDateEdit1.Date:=quTTNInDATEDOC.AsDateTime;  fmDocsInS.cxDateEdit1.Properties.ReadOnly:=False;
      fmDocsInS.cxDateEdit2.Date:=quTTNInDATESF.AsDateTime;  fmDocsInS.cxDateEdit2.Properties.ReadOnly:=False;

      fmDocsInS.cxButtonEdit1.Tag:=quTTNInIDCLI.AsInteger;
      fmDocsInS.cxButtonEdit1.EditValue:=quTTNInIDCLI.AsInteger;
      fmDocsInS.cxButtonEdit1.Text:=quTTNInNAMECLI.AsString;
      fmDocsInS.cxButtonEdit1.Properties.ReadOnly:=False;

      fmDocsInS.Label17.Tag:=quTTNInPayNDS.AsInteger;
      if fmDocsInS.Label17.Tag=1 then fmDocsInS.Label17.Caption:='���������� ���' else fmDocsInS.Label17.Caption:='-'; 

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNInIDSKL.AsInteger,[]);

      fmDocsInS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsInS.cxLabel5.Enabled:=True;
      fmDocsInS.cxLabel1.Enabled:=True;
      fmDocsInS.cxLabel2.Enabled:=True;
      fmDocsInS.cxLabel6.Enabled:=True;
      fmDocsInS.cxLabel3.Enabled:=True;
      fmDocsInS.cxLabel4.Enabled:=True;
      fmDocsInS.cxLabel11.Enabled:=True;
      fmDocsInS.cxLabel12.Enabled:=True;

      fmDocsInS.Panel4.Visible:=True;

      fmDocsInS.acSaveDoc.Enabled:=True;
      fmDocsInS.cxButton1.Enabled:=True;
      fmDocsInS.cxButton7.Enabled:=True;

      fmDocsInS.ViewDoc1.OptionsData.Editing:=True;
      fmDocsInS.ViewDoc1.OptionsData.Deleting:=True;
    end;

    if iT=2 then
    begin
      fmDocsInS.Caption:='���������: ��������.';

      fmDocsInS.Tag:=quTTNInID.AsInteger;

      fmDocsInS.cxTextEdit1.Text:=quTTNInNUMDOC.AsString;  fmDocsInS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsInS.cxTextEdit2.Text:=quTTNInNUMSF.AsString;  fmDocsInS.cxTextEdit2.Properties.ReadOnly:=True;
      fmDocsInS.cxTextEdit3.Text:=quTTNInSNUMZ.AsString; fmDocsInS.cxTextEdit3.Tag:=quTTNInIDZ.AsInteger; fmDocsInS.cxTextEdit3.Properties.ReadOnly:=True;

      fmDocsInS.cxDateEdit1.Date:=quTTNInDATEDOC.AsDateTime;  fmDocsInS.cxDateEdit1.Properties.ReadOnly:=True;
      fmDocsInS.cxDateEdit2.Date:=quTTNInDATESF.AsDateTime;  fmDocsInS.cxDateEdit2.Properties.ReadOnly:=True;

      fmDocsInS.cxButtonEdit1.Tag:=quTTNInIDCLI.AsInteger;
      fmDocsInS.cxButtonEdit1.EditValue:=quTTNInIDCLI.AsInteger;
      fmDocsInS.cxButtonEdit1.Text:=quTTNInNAMECLI.AsString;
      fmDocsInS.cxButtonEdit1.Properties.ReadOnly:=True;

      fmDocsInS.Label17.Caption:='-';
      fmDocsInS.Label17.Tag:=0;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNInIDSKL.AsInteger,[]);

      fmDocsInS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInS.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmDocsInS.cxLabel5.Enabled:=False;
      fmDocsInS.cxLabel1.Enabled:=False;
      fmDocsInS.cxLabel2.Enabled:=False;
      fmDocsInS.cxLabel6.Enabled:=False;
      fmDocsInS.cxLabel3.Enabled:=False;
      fmDocsInS.cxLabel4.Enabled:=False;
      fmDocsInS.cxLabel11.Enabled:=False;
      fmDocsInS.cxLabel12.Enabled:=False;

      fmDocsInS.Panel4.Visible:=False;

      fmDocsInS.acSaveDoc.Enabled:=False;
      fmDocsInS.cxButton1.Enabled:=False;
      fmDocsInS.cxButton7.Enabled:=False;

      fmDocsInS.ViewDoc1.OptionsData.Editing:=False;
      fmDocsInS.ViewDoc1.OptionsData.Deleting:=False;
    end;
  end;
end;

procedure TfmDocsInH.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocsInH.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInH.FormCreate(Sender: TObject);
begin
  fpfmDocsInH.IniFileName:=sFormIni;
  fpfmDocsInH.Active:=True; delay(10);
  Timer1.Enabled:=True;
  GridDocsIn.Align:=AlClient;
  ViewDocsIn.RestoreFromIniFile(sGridIni); delay(10);
  ViewCardsIn.RestoreFromIniFile(sGridIni); delay(10);
  StatusBar1.Color:=$00FFCACA;

  acCheckBuh.Enabled:=True;
  acCheckBuh.Visible:=True;
end;

procedure TfmDocsInH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsIn.StoreToIniFile(sGridIni,False); delay(10);
  ViewCardsIn.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmDocsInH.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      if LevelDocsIn.Visible then
      begin
        fmDocsInH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocsInH.ViewDocsIn.BeginUpdate;
        try
          quTTNIn.Active:=False;
          quTTNIn.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
          quTTNIn.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
          quTTNIn.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
          quTTNIn.Active:=True;
        finally
          fmDocsInH.ViewDocsIn.EndUpdate;
        end;

      end else
      begin
        fmDocsInH.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ...');

        ViewCardsIn.BeginUpdate;
        dsquInLn.DataSet:=nil;

        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ...');

        quInLn.Close;
        quInLn.Parameters.ParamByName('SSKL').Value:=fDepsDep(fmMainMC.Label3.tag);
        quInLn.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
        quInLn.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
        quInLn.Open;

        dsquInLn.DataSet:=quInLn;

        ViewCardsIn.EndUpdate;

        Memo1.Lines.Add('������������ ��.');
      end;
    end;
  end;
end;

procedure TfmDocsInH.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmDocsInS.taSpecIn);
    fmDocsInS.acSaveDoc.Enabled:=True;
    fmDocsInS.Show;      
  end;
end;

procedure TfmDocsInH.acEditDoc1Execute(Sender: TObject);
//Var //IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //�������������
  if not CanDo('prEditDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnInIACTIVE.AsInteger<3 then
      begin
        prSetValsAddDoc1(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmDocsInS.taSpecIn);

        fmDocsInS.ViewDoc1.BeginUpdate;
        quSpecIn.Active:=False;
        quSpecIn.Parameters.ParamByName('IDH').Value:=quTTnInID.AsInteger;
        quSpecIn.Parameters.ParamByName('IDSKL').Value:=quTTNInIDSKL.AsInteger;  //��� ���������� ���
        quSpecIn.Active:=True;

        quSpecIn.First;
        while not quSpecIn.Eof do
        begin
          with fmDocsInS do
          begin
     //     prFindNacGr(quSpecInCodeTovar.AsInteger,rn1,rn2,rn3,rPr);

     //     prCalcPriceOut(quSpecInCodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecInCenaTovar.AsFloat,quSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

            taSpecIn.Append;
            taSpecInNum.AsInteger:=quSpecInNUM.AsInteger;
            taSpecInCodeTovar.AsInteger:=quSpecInIDCARD.asinteger;
            taSpecInCodeEdIzm.AsInteger:=quSpecInIDM.AsInteger;
            taSpecInBarCode.AsString:=quSpecInSBAR.AsString;
            taSpecInNDSProc.AsFloat:=quSpecInNDSPROC.AsFloat;
            taSpecInNDSSum.AsFloat:=quSpecInSUMNDS.AsFloat;
            taSpecInBestBefore.AsDateTime:=date;
            taSpecInKolMest.AsInteger:=1;
            taSpecInKolEdMest.AsFloat:=0;
            taSpecInKolWithMest.AsFloat:=quSpecInQUANT.AsFloat;
            taSpecInKolWithNecond.AsFloat:=quSpecInQUANT.AsFloat;
            taSpecInKol.AsFloat:=quSpecInQUANT.AsFloat;
            taSpecInPriceIn.AsFloat:=quSpecInPRICEIN.AsFloat;
            taSpecInSumIn.AsFloat:=quSpecInSUMIN.AsFloat;
            taSpecInPriceIn0.AsFloat:=quSpecInPRICEIN0.AsFloat;
            taSpecInSumIn0.AsFloat:=quSpecInSUMIN0.AsFloat;
            taSpecInPriceR.AsFloat:=quSpecInPRICEUCH.AsFloat;
            taSpecInSumR.AsFloat:=quSpecInSUMUCH.AsFloat;

            try
              taSpecInProcentNac.AsFloat:=rv(((taSpecInSumR.AsFloat-taSpecInSumIn.AsFloat)/taSpecInSumIn.AsFloat)*100);
            except
              taSpecInProcentNac.AsFloat:=0;
            end;

            taSpecInName.AsString:=quSpecInNAME.AsString;
            taSpecInCodeTara.AsInteger:=0;
            taSpecInVesTara.AsFloat:=0;
            taSpecInCenaTara.AsFloat:=0;
            taSpecInSumVesTara.AsFloat:=0;
            taSpecInSumCenaTara.AsFloat:=0;
            taSpecInRealPrice.AsFloat:=quSpecInPRICE.AsFloat;
            taSpecInRemn.AsFloat:=prFindRemnDepD(quSpecInIDCARD.AsInteger,quTTNInIDSKL.AsInteger,Trunc(date));
            taSpecInNac1.AsFloat:=quSpecInRNAC.AsFloat;
            taSpecInNac2.AsFloat:=0;
            taSpecInNac3.AsFloat:=0;
            taSpecIniCat.AsINteger:=0;
            taSpecInPriceNac.AsFloat:=quSpecInRNAC.AsFloat;
            taSpecInsMaker.AsString:=prFindMaker(quSpecInMAKER.AsInteger);
            taSpecInAVid.AsInteger:=quSpecInAVID.AsInteger;
            taSpecInVol.AsFloat:=quSpecInVOL.AsFloat;
            taSpecIn.Post;

          end;
          quSpecIn.Next;
        end;
        quSpecIn.Active:=False;
        fmDocsInS.ViewDoc1.EndUpdate;

     // fmDocsInS.prSetNac;

        fmDocsInS.Show;
      end else  showmessage('� ������ ������� ������������� �������� ���������.');
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsInH.acViewDoc1Execute(Sender: TObject);
//Var //iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //��������
  if not CanDo('prViewDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc1(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmDocsInS.taSpecIn);

      fmDocsInS.ViewDoc1.BeginUpdate;
      quSpecIn.Active:=False;
      quSpecIn.Parameters.ParamByName('IDH').Value:=quTTnInID.AsInteger;
      quSpecIn.Parameters.ParamByName('IDSKL').Value:=quTTNInIDSKL.AsInteger;  //��� ���������� ���
      quSpecIn.Active:=True;

      quSpecIn.First;
      while not quSpecIn.Eof do
      begin
        with fmDocsInS do
        begin
     //     prFindNacGr(quSpecInCodeTovar.AsInteger,rn1,rn2,rn3,rPr);

     //     prCalcPriceOut(quSpecInCodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecInCenaTovar.AsFloat,quSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

          taSpecIn.Append;
          taSpecInNum.AsInteger:=quSpecInNUM.AsInteger;
          taSpecInCodeTovar.AsInteger:=quSpecInIDCARD.asinteger;
          taSpecInCodeEdIzm.AsInteger:=quSpecInIDM.AsInteger;
          taSpecInBarCode.AsString:=quSpecInSBAR.AsString;
          taSpecInNDSProc.AsFloat:=quSpecInNDSPROC.AsFloat;
          taSpecInNDSSum.AsFloat:=quSpecInSUMNDS.AsFloat;
          taSpecInBestBefore.AsDateTime:=date;
          taSpecInKolMest.AsInteger:=1;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=quSpecInQUANT.AsFloat;
          taSpecInKolWithNecond.AsFloat:=quSpecInQUANT.AsFloat;
          taSpecInKol.AsFloat:=quSpecInQUANT.AsFloat;
          taSpecInPriceIn.AsFloat:=quSpecInPRICEIN.AsFloat;
          taSpecInSumIn.AsFloat:=quSpecInSUMIN.AsFloat;
          taSpecInPriceIn0.AsFloat:=quSpecInPRICEIN0.AsFloat;
          taSpecInSumIn0.AsFloat:=quSpecInSUMIN0.AsFloat;
          taSpecInPriceR.AsFloat:=quSpecInPRICEUCH.AsFloat;
          taSpecInSumR.AsFloat:=quSpecInSUMUCH.AsFloat;

          try
            taSpecInProcentNac.AsFloat:=rv(((taSpecInSumR.AsFloat-taSpecInSumIn.AsFloat)/taSpecInSumIn.AsFloat)*100);
          except
            taSpecInProcentNac.AsFloat:=0;
          end;

          taSpecInName.AsString:=quSpecInNAME.AsString;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInVesTara.AsFloat:=0;
          taSpecInCenaTara.AsFloat:=0;
          taSpecInSumVesTara.AsFloat:=0;
          taSpecInSumCenaTara.AsFloat:=0;
          taSpecInRealPrice.AsFloat:=quSpecInPRICE.AsFloat;
          taSpecInRemn.AsFloat:=prFindRemnDepD(quSpecInIDCARD.AsInteger,quTTNInIDSKL.AsInteger,Trunc(date));
          taSpecInNac1.AsFloat:=quSpecInRNAC.AsFloat;
          taSpecInNac2.AsFloat:=0;
          taSpecInNac3.AsFloat:=0;
          taSpecIniCat.AsINteger:=0;
          taSpecInPriceNac.AsFloat:=quSpecInRNAC.AsFloat;
          taSpecInsMaker.AsString:=prFindMaker(quSpecInMAKER.AsInteger);
          taSpecInAVid.AsInteger:=quSpecInAVID.AsInteger;
          taSpecInVol.AsFloat:=quSpecInVOL.AsFloat;
          taSpecIn.Post;
        end;
        quSpecIn.Next;
      end;
      quSpecIn.Active:=False;
      fmDocsInS.ViewDoc1.EndUpdate;

     // fmDocsInS.prSetNac;

      fmDocsInS.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsInH.ViewDocsInDblClick(Sender: TObject);
begin
  //������� �������
  with dmMCS do
  begin
    if quTTnIn.RecordCount>0 then
    begin
      if quTTNInIACTIVE.AsInteger<3 then acEditDoc1.Execute //��������������
      else acViewDoc1.Execute; //��������}
    end;  
  end;
end;

procedure TfmDocsInH.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMCS do
  begin
    if quTTNIn.RecordCount>0 then //���� ��� �������
    begin
      if quTTNInIACTIVE.AsInteger<3 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTNInNUMDOC.AsString+' �� '+quTTNInNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          prAddHist(2,quTTNInID.AsInteger,trunc(quTTNInDateInvoice.AsDateTime),quTTNInNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('DELETE FROM [dbo].[DOCIN_HEAD]');
          quD.SQL.Add('where IDSKL='+its(quTTNInIDSKL.AsInteger));
          quD.SQL.Add('and IDATEDOC='+its(quTTNInIDATEDOC.AsInteger));
          quD.SQL.Add('and ID='+its(quTTNInID.AsInteger));
          quD.ExecSQL;
          delay(100);

          quTTNIn.Requery();
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsInH.acOnDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  with dmMCS do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInIACTIVE.AsInteger<=1 then
      begin
        if quTTNInDATEDOC.AsDateTime<prOpenDate(quTTNInIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quTTNInIDSKL.AsInteger,quTTNInIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quTTNInIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;

        if quTTNInIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('������������ �������� �'+quTTNInNUMDOC.AsString+' �� '+quTTNInNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTNInID.AsInteger;
          if OprDocsIn(IDH,fmDocsInH.Memo1) then
          begin
            if CommonSet.AutoLoadCash=1 then prCashLoadFromDoc(IDH,quTTNInIDSKL.AsInteger,1,Memo1);

            ViewDocsIn.BeginUpdate;
            quTTNIn.Requery();
            quTTNIn.Locate('ID',IDH,[]);
            ViewDocsIn.EndUpdate;

            prWrLog(quTTNInIDSKL.AsInteger,1,1,IDH,'');

            GridDocsIn.SetFocus;
            ViewDocsIn.Controller.FocusRecord(ViewDocsIn.DataController.FocusedRowIndex,True);
          end;  
        end;
      end;
    end;
  end;
end;

procedure TfmDocsInH.acOffDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
//��������
  with dmMCS do
  begin
    if not CanDo('prOffDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInIACTIVE.AsInteger=3 then
      begin
        if quTTNInDATEDOC.AsDateTime<prOpenDate(quTTNInIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quTTNInIDSKL.AsInteger,quTTNInIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quTTNInIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, �������� ����������!!!');
          exit;
        end;

        if quTTNInIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;
        
        if MessageDlg('�������� �������� �'+quTTNInNUMDOC.AsString+' �� '+quTTNInNAMECLI.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTNInID.AsInteger;
          if OtkatDocsIn(IDH,fmDocsInH.Memo1) then
          begin
            ViewDocsIn.BeginUpdate;
            quTTNIn.Requery();
            quTTNIn.Locate('ID',IDH,[]);
            ViewDocsIn.EndUpdate;

            prWrLog(quTTNInIDSKL.AsInteger,1,0,IDH,'');

            GridDocsIn.SetFocus;
            ViewDocsIn.Controller.FocusRecord(ViewDocsIn.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;


  end;
end;

procedure TfmDocsInH.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsInH.acVidExecute(Sender: TObject);
//Var iC,iCliCB:INteger;
//    Sinn:String;
begin
  //���
  with dmMCS do
  begin

    if LevelDocsIn.Visible then
    begin
      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;
      Gr1.Visible:=True;

      fmDocsInH.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      ViewCardsIn.BeginUpdate;
      dsquInLn.DataSet:=nil;


      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ...');

      quInLn.Close;
      quInLn.Parameters.ParamByName('SSKL').Value:=fDepsDep(fmMainMC.Label3.tag);
      quInLn.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quInLn.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quInLn.Open;

      dsquInLn.DataSet:=quInLn;

      ViewCardsIn.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

      Memo1.Lines.Add('������������ ��.');

    end else
    begin
      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;
      Gr1.Visible:=False;

      fmDocsInH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      fmDocsInH.ViewDocsIn.BeginUpdate;
      try
        quTTNIn.Active:=False;
        quTTNIn.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
        quTTNIn.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
        quTTNIn.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
        quTTNIn.Active:=True;
      finally
        fmDocsInH.ViewDocsIn.EndUpdate;
      end;


      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;
    end;
  end;
end;

procedure TfmDocsInH.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInH.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsIn.Visible=False then exit;
  with dmMCS do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTnInNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnInNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnInNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocsInH.acCopyExecute(Sender: TObject);
//var Par:Variant;
//    iC:INteger;
begin
  //����������
  with dmMCS do
  begin
    {
    if ViewDocsIn.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnInID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnInID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTNInDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnInNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTnInCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnInNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnInDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnInNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnInSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnInSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTnInIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTnInSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNInDATEDOC.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      iC:=1;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnInID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecInCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecInKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInNewCenaTovar.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSumCenaTovar.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecInNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecInNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecInCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=quSpecInProcent.AsFloat;
        taSpecDocIdTara.AsInteger:=quSpecInCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecInCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecInBarCode.AsString;
        taSpecDoc.Post;

        quSpecIn.Next;   inc(iC);
      end;
      quSpecIn.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocsInH.acInsertDExecute(Sender: TObject);
//Var StrWk:String;
//    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMCS do
  begin
    {
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsInS.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmDocsInS.Label4.Tag:=taHeadDociTypeCli.AsInteger;

          if prCliNDS(taHeadDociTypeCli.AsInteger,taHeadDocIdCli.AsInteger,0)=1 then
          begin
            fmDocsInS.Label17.Caption:='���������� ���';
            fmDocsInS.Label17.Tag:=1;
          end else
          begin
            fmDocsInS.Label17.Caption:='�� ���������� ���';
            fmDocsInS.Label17.Tag:=0;
          end;

          fmDocsInS.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmDocsInS.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmDocsInS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsInS.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmDocsInS.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmDocsInS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsInS.cxLabel1.Enabled:=True;
          fmDocsInS.cxLabel2.Enabled:=True;
          fmDocsInS.cxLabel3.Enabled:=True;
          fmDocsInS.cxLabel4.Enabled:=True;
          fmDocsInS.cxLabel5.Enabled:=True;
          fmDocsInS.cxLabel6.Enabled:=True;
          fmDocsInS.cxButton1.Enabled:=True;

          fmDocsInS.ViewDoc1.OptionsData.Editing:=True;
          fmDocsInS.ViewDoc1.OptionsData.Deleting:=True;
          fmDocsInS.Panel4.Visible:=True;

          CloseTe(fmDocsInS.taSpecIn);

          if taG.Active=False then taG.Active:=True;

          fmDocsInS.ViewDoc1.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmDocsInS do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecIn.Append;
                  taSpecInNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecInCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecInCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecInBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecInNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecInNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecInOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecInBestBefore.AsDateTime:=Date;
                  taSpecInKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecInKolEdMest.AsFloat:=0;
                  taSpecInKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecInKolWithNecond.AsFloat:=0;
                  taSpecInKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecInCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecInProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecInNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecInSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecInProcentN.AsFloat:=0;
                  taSpecInNecond.AsFloat:=0;
                  taSpecInCenaNecond.AsFloat:=0;
                  taSpecInSumNecond.AsFloat:=0;
                  taSpecInProcentZ.AsFloat:=0;
                  taSpecInZemlia.AsFloat:=0;
                  taSpecInProcentO.AsFloat:=0;
                  taSpecInOthodi.AsFloat:=0;
                  taSpecInName.AsString:=taSpecDocNameC.AsString;
                  taSpecInCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecInNameTara.AsString:=StrWk;

                  taSpecInVesTara.AsFloat:=0;
                  taSpecInCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecInSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecInTovarType.AsInteger:=0;

                  taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecInNac1.AsFloat:=rn1;
                  taSpecInNac2.AsFloat:=rn2;
                  taSpecInNac3.AsFloat:=rn3;
                  taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInsMaker.AsString:='';
                  end;
                  taSpecIn.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmDocsInS.ViewDoc1.EndUpdate;

          fmDocsInS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsInS.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocsInH.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsIn.Visible then
  begin
    prNExportExel6(ViewDocsIn);
  end else
  begin
    prNExportExel6(ViewCardsIn);
  end;
end;

procedure TfmDocsInH.FormShow(Sender: TObject);
begin
  if (Pos('����',Person.Name)=0)and(Pos('OPER CB',Person.Name)=0)and(Pos('OPERZAK',Person.Name)=0) then
  begin
    acChangePost.Enabled:=False;
  end else
  begin
    acChangePost.Enabled:=True;
  end;
  Memo1.Clear;
end;

procedure TfmDocsInH.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViewDocsIn);
end;

procedure TfmDocsInH.acEdit1Execute(Sender: TObject);
begin
{  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    if FileExists(CurDir+'SpecIn.cds') then fmDocsInS.taSpecIn.Active:=True
    else fmDocsInS.taSpecIn.CreateDataSet;

    fmDocsInS.acSaveDoc.Enabled:=True;
    fmDocsInS.Show;
  end;}
end;

procedure TfmDocsInH.acTestMoveDocExecute(Sender: TObject);
begin
  //�������� ��������������
  with dmMCS do
  begin
    if not CanDo('prTestMoveDoc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTnIn.RecordCount>0 then //���� ��� ���������
    begin
      if quTTNInIACTIVE.AsInteger=3 then
      begin
        if MessageDlg('��������� �������������� �� ��������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ��������.',Memo1);
         // 1 - �������� ��������������
         // 2 - � ����� ��� �������������
         // 3 - �������� ������

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNInDATEDOC.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecIn1.Eof do
          begin
//            prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
            prWh('  -- ��� '+its(quSpecIn1CodeTovar.AsInteger),Memo1); delay(10);

            //������ ��� �������� �� ���� �� ���� ���������� � ���������� ��������
            prDelTMoveDate(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),1);

            //�������� ��� ������� ������ �� ���� � �������� �������� �� ����
            quSpecInTest.Active:=False;
            quSpecInTest.SQL.Clear;
            quSpecInTest.SQL.Add('select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln');
            quSpecInTest.SQL.Add('left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoice=hd.DateInvoice) and(Ln.Number=hd.Number)');
            quSpecInTest.SQL.Add('where ln.DateInvoice='''+ds(quTTNInDATEDOC.AsDateTime)+'''');
            quSpecInTest.SQL.Add('and ln.CodeTovar='+its(quSpecIn1CodeTovar.AsInteger));
            quSpecInTest.SQL.Add('and ln.Depart='+its(quTTnInDepart.AsInteger));
            quSpecInTest.SQL.Add('and hd.AcStatus=3');
            quSpecInTest.SQL.Add('group by hd.ID');
            quSpecInTest.Active:=True;
            quSpecInTest.First;
            while not quSpecInTest.Eof do
            begin
              prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quSpecInTestID.AsInteger,1,quSpecInTestKol.AsFloat);
              delay(20);
              quSpecInTest.Next;
            end;
            quSpecInTest.Active:=False;

            quSpecIn1.Next; delay(20);
          end;
          quSpecIn1.Active:=False;

         // 4 - �������� ������

          prWh('�������� ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsInH.ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA,sO:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsIn.ColumnCount-1 do
  begin
    if ViewDocsIn.Columns[i].Name='ViewDocsInIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;

    if ViewDocsIn.Columns[i].Name='ViewDocsInChekBuh' then
    begin
      sO:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;


  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

  if pos('�����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00E6E6CA; //����������� - ������� ��������
  if pos('����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00F4F4EA; //����������� - �������� �������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocsInH.acOprihExecute(Sender: TObject);
//Var bOpr:Boolean;
//    iTestZ:INteger;
//    sNumZ:String;
begin
//������������
  with dmMCS do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    {
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInIACTIVE.AsInteger=0 then
      begin
        if quTTNInDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          bOpr:=True;

          iTestZ:=0;
          if (quTTnInRaion.AsInteger>=1)and(quTTnInRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quTTnInORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
            quZakHNum.ParamByName('ICLI').AsInteger:=quTTnInCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;
          end;

          if bOpr = False then
          begin
            prWH('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...',Memo1);
            Showmessage('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...');
          end else
          begin
            prWH('����� ... ���� ������.',Memo1);

            prAddHist(1,quTTnInID.AsInteger,trunc(quTTNInDATEDOC.AsDateTime),quTTnInNumber.AsString,'�����.',1);

           // 1 - �������� ��������������
           // 2 - � ����� ��� �������������
           // 3 - �������� ������

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNInDATEDOC.AsDateTime);
            quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
            quSpecIn1.Active:=True;

//            prClearBuf(quTTnInID.AsInteger,1,Trunc(quTTNInDATEDOC.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quTTNInDATEDOC.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTnInID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger)+r+ds(quTTNInDATEDOC.AsDateTime)+r+quTTnInNumber.AsString+r+quTTnInINN.AsString+r+fs(quTTnInSummaTovarPost.AsFloat)+r+fs(quTTnInSummaTovar.AsFloat)+r+fs(quTTnInSummaTara.AsFloat)+r+fs(quTTnInNDS10.AsFloat)+r+fs(quTTnInNDS20.AsFloat)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecIn1.Eof do
              begin
                if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quTTnInNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);

                quSpecIn1.Next;
              end;
              quSpecIn1.Active:=False;


              if quTTnInSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecInT.Active:=False;
                quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTNInDATEDOC.AsDateTime);
                quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                quSpecInT.Active:=True;

                quSpecInT.First;  // ��������������
                while not quSpecInT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat);
                  prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                  quSpecInT.Next;
                end;
                quSpecInT.Active:=False;
              end;

           // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTNInDATEDOC.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
              quE.ExecSQL;

              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quTTnIn,ViewDocsIn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTNInDATEDOC.AsDateTime))>=1 then prRecalcTO(trunc(quTTNInDATEDOC.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsInH.acRazrubExecute(Sender: TObject);
//Var iC:INteger;
//    StrWk:String;
//    rSumP,rQ,rNac,rSumNDS,rSumOutNDS:Real;
begin
// ������������ �������� �� ������
  with dmMCS do
  begin
    if not CanDo('prRazrIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInIACTIVE.AsInteger<3 then
      begin
        if quTTNInDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTNInDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������
          prButtonSet(False);

          prWH('����� ... ���� ������������.',Memo1);

          // 4 - �������� ������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=2');
          quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTNInDATEDOC.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTnIn,ViewDocsIn,0);

          prWh('������ ��.',Memo1);

          prButtonSet(True);


          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsInS.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;
          fmDocsInS.cxTextEdit1.Text:=quTTnInNumber.AsString+'rzr';
          fmDocsInS.Label4.Tag:=quTTnInIndexPost.AsInteger;

          if prCliNDS(quTTnInIndexPost.AsInteger,quTTnInCodePost.AsInteger,0)=1 then
          begin
            fmDocsInS.Label17.Caption:='���������� ���';
            fmDocsInS.Label17.Tag:=1;
          end else
          begin
            fmDocsInS.Label17.Caption:='�� ���������� ���';
            fmDocsInS.Label17.Tag:=0;
          end;

          fmDocsInS.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
          fmDocsInS.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
          fmDocsInS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsInS.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
          fmDocsInS.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
          fmDocsInS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsInS.cxLabel1.Enabled:=True;
          fmDocsInS.cxLabel2.Enabled:=True;
          fmDocsInS.cxLabel3.Enabled:=True;
          fmDocsInS.cxLabel4.Enabled:=True;
          fmDocsInS.cxLabel5.Enabled:=True;
          fmDocsInS.cxLabel6.Enabled:=True;
          fmDocsInS.cxButton1.Enabled:=True;

          fmDocsInS.ViewDoc1.OptionsData.Editing:=True;
          fmDocsInS.ViewDoc1.OptionsData.Deleting:=True;
          fmDocsInS.Panel4.Visible:=True;

          CloseTe(fmDocsInS.taSpecIn);

          if taG.Active=False  then taG.Active:=True;
          if ptTTK.Active=False  then ptTTK.Active:=True;

          fmDocsInS.ViewDoc1.BeginUpdate;

          quSpecIn.Active:=False;
          quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNInDATEDOC.AsDateTime);
          quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn.Active:=True;

          quSpecIn.First;
          iC:=1;
          while not quSpecIn.Eof do
          begin
            with fmDocsInS do
            begin
              if ptTTK.Active=False then ptTTK.Active:=True;
              ptTTK.CancelRange;
              if ptTTK.locate('ID',quSpecInCodeTovar.AsInteger,[]) then
              begin
                rSumP:=rv(quSpecInSumCenaTovarPost.AsFloat);
                rSumNDS:=rv(quSpecInNDSSum.AsFloat);
                rSumOutNDS:=rv(quSpecInOutNDSSum.AsFloat);

                ptTTK.CancelRange;
                ptTTK.SetRange([quSpecInCodeTovar.AsInteger,0],[quSpecInCodeTovar.AsInteger,100]);
                ptTTK.First;
                while not ptTTK.Eof do
                begin
                  rNac:=ptTTKPROC2.AsFloat;
                  if rNac<1 then rNac:=rNac*100-100;

                  if dmMT.taG.FindKey([ptTTKIDC.AsInteger]) then
                  begin
                    rQ:=R1000((quSpecInKolWithMest.AsFloat*quSpecInKolMest.AsInteger)*ptTTKPROC1.AsFloat/100);

                    taSpecIn.Append;
                    taSpecInNum.AsInteger:=iC;
                    taSpecInCodeTovar.AsInteger:=ptTTKIDC.AsInteger;
                    taSpecInCodeEdIzm.AsInteger:=dmMt.taGEdIzm.AsInteger;
                    taSpecInBarCode.AsString:=dmMt.taGBarCode.AsString;
                    taSpecInNDSProc.AsFloat:=dmMt.taGNDS.AsFloat;
                    taSpecInNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInOutNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInBestBefore.AsDateTime:=date+dmMt.taGSrokReal.AsInteger;
                    taSpecInKolMest.AsInteger:=1;
                    taSpecInKolEdMest.AsFloat:=0;
                    taSpecInKolWithMest.AsFloat:=rQ;
                    taSpecInKolWithNecond.AsFloat:=rQ;
                    taSpecInKol.AsFloat:=rQ;
                    taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                    taSpecInProcent.AsFloat:=rNac;
                    taSpecInNewCenaTovar.AsFloat:=rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInSumCenaTovarPost.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat);
                    taSpecInSumCenaTovar.AsFloat:=rQ*rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInProcentN.AsFloat:=0;
                    taSpecInNecond.AsFloat:=0;
                    taSpecInCenaNecond.AsFloat:=0;
                    taSpecInSumNecond.AsFloat:=0;
                    taSpecInProcentZ.AsFloat:=0;
                    taSpecInZemlia.AsFloat:=0;
                    taSpecInProcentO.AsFloat:=0;
                    taSpecInOthodi.AsFloat:=0;
                    taSpecInName.AsString:=OemToAnsiConvert(dmMt.taGName.AsString);
                    taSpecInCodeTara.AsInteger:=0;
                    taSpecInNameTara.AsString:='';

                    taSpecInVesTara.AsFloat:=0;
                    taSpecInCenaTara.AsFloat:=0;
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInSumCenaTara.AsFloat:=0;
                    taSpecInTovarType.AsInteger:=dmMt.taGTovarType.AsInteger;
                    taSpecInRealPrice.AsFloat:=dmMt.taGCena.AsFloat;
                    taSpecIniCat.AsInteger:=dmMt.taGV02.AsInteger;
                    if taGV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                    if taGV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                    taSpecInsMaker.AsString:='';
                    taSpecIn.Post;

                    rSumP:=rSumP-rv(rQ*quSpecInCenaTovar.AsFloat);
                    rSumNDS:=rSumNDS-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    rSumOutNDS:=rSumOutNDS-(rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat)));
                  end;

                  ptTTK.Next; inc(iC);
                end;
                //��������� ������� ��������� ���� ����
                if abs(rSumP)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat+rSumP;
                  taSpecIn.Post;
                end;
                if abs(rSumNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInNDSSum.AsFloat:=taSpecInNDSSum.AsFloat+rSumNDS;
                  taSpecIn.Post;
                end;
                if abs(rSumOutNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat+rSumOutNDS;
                  taSpecIn.Post;
                end;

              end else
              begin
                taSpecIn.Append;
                taSpecInNum.AsInteger:=iC;
                taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
                taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
                taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
                taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
                taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
                taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
                taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
                taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
                taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
                taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
                taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
                taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
                taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
                taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
                taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
                taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
                taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
                taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
                taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
                taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
                taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
                taSpecInName.AsString:=quSpecInName.AsString;
                taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

                StrWk:='';
                if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
                taSpecInNameTara.AsString:=StrWk;

                taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
                taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
                taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
                taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
                taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
                taSpecIniCat.AsInteger:=quSpecInV02.AsInteger;
                if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
                if quSpecInSumVesTara.AsFloat>0.1 then  taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

                taSpecIn.Post;

                inc(iC);
              end;
            end;
            quSpecIn.Next;
          end;

          ptTTK.Active:=False;

          fmDocsInS.ViewDoc1.EndUpdate;

          fmDocsInS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsInS.Show;
        end;
      end else
      begin
        showmessage('�������� ��������� �.�. �������� �����������.');
      end;
    end;}
  end;
end;

procedure TfmDocsInH.acCheckBuhExecute(Sender: TObject);
//Var iSt:INteger;
begin
//
  with dmMCS do
  begin
    {
    if quTTnIn.RecordCount>0 then
    begin
      if quTTnInChekBuh.AsInteger=0 then fmChangeB.cxRadioButton1.Checked:=True;
      if quTTnInChekBuh.AsInteger=1 then fmChangeB.cxRadioButton2.Checked:=True;
      if quTTnInChekBuh.AsInteger=2 then fmChangeB.cxRadioButton3.Checked:=True;

      fmChangeB.ShowModal;
      if fmChangeB.ModalResult=mrOk then
      begin
        iSt:=0;
        if fmChangeB.cxRadioButton2.Checked then iSt:=1;
        if fmChangeB.cxRadioButton3.Checked then iSt:=2;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ChekBuh='+its(iSt));
        quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNInDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTnIn,ViewDocsIn,0);

        showmessage('��.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsInH.acChangePostExecute(Sender: TObject);
begin
// �������� ����������
  with dmMCS do
  begin
    {
    if quTTnIn.RecordCount>0 then //���� ��� �������
    begin
      try
        fmChangePost:=tfmChangePost.Create(Application);
        fmChangePost.Label1.Caption:='�������� ���������� � ��������� � '+quTTnInNumber.AsString;
        fmChangePost.Label3.Caption:=quTTnInNameCli.AsString;
        fmChangePost.cxButtonEdit1.Text:='';
        fmChangePost.cxButtonEdit1.Tag:=0;
        fmChangePost.Label4.Tag:=0;

        fmDocsInS.Label17.Caption:='';
        fmDocsInS.Label17.Tag:=0;

        fmChangePost.ShowModal;
      finally
        fmChangePost.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsInH.acChangeTypePolExecute(Sender: TObject);
begin
  //�������� ����������
  with dmMCS do
  begin
    {
    if quTTnIn.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quTTnInNumber.AsString+' �� '+DateToStr(quTTNInDATEDOC.AsDateTime)+' ������ ��� '+its(quTTnInProvodType.AsInteger);
      if quTTnInProvodType.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ProvodType='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNInDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTnIn,ViewDocsIn,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsInH.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMCS do
  begin
{    if ViewDocsIn.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsIn.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=1;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsInH.ViewCardsInSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewCardsIn.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
{    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teInLnCode.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;}
  end;
end;

procedure TfmDocsInH.acMoveCardExecute(Sender: TObject);
begin
  if ViewCardsIn.Visible then
  begin
  //��������������
    with dmMCS do
    begin
      if ViewCardsIn.Controller.SelectedRecordCount>0 then
      begin
        {
        fmMove.ViewM.BeginUpdate;
        fmMove.GridM.Tag:=teInLnCode.AsInteger;
        prFillMove(teInLnCode.AsInteger);

        fmMove.ViewM.EndUpdate;

        fmMove.Caption:=teInLnCardName.AsString+'('+teInLnCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

        fmMove.LevelP.Visible:=False;
        fmMove.LevelM.Visible:=True;

        fmMove.Show;}
      end;
    end;
  end;
end;

procedure TfmDocsInH.acOplataExecute(Sender: TObject);
var rSum:Real;
begin
  //������ ���������
  if not CanDo('prViewOplata') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
//      if MessageDlg('Welcome to my Delphi application.  Exit now?',mtConfirmation, [mbYes, mbNo], 0)
      fmOplata:=TfmOplata.Create(Application);
      with fmOplata do
      begin
        quOplata.Active:=False;
        quOplata.SQL.Clear;
        quOplata.SQL.Add('');
        quOplata.SQL.Add('SELECT [IDSKL],[IDDOC],[ID],[ITYPED],[IDATEPL],[DATEPL],[SUMPL],[COMMENT]');
        quOplata.SQL.Add('  FROM [MCRYSTAL].[dbo].[OPLATA]');
        quOplata.SQL.Add('  where [IDSKL]='+its(quTTNInIDSKL.AsInteger));
        quOplata.SQL.Add('  and [IDDOC]='+its(quTTNInID.AsInteger));
        quOplata.SQL.Add('  and [ITYPED]=1');
        quOplata.SQL.Add('  order by [IDATEPL]');
        quOplata.Active:=True;

        rSum:=0;
        quOplata.First;
        while not quOplata.Eof do
        begin
          rSum:=rSum+quOplataSUMPL.AsFloat;
          quOplata.Next;
        end;
        quOplata.First;
      end;
      fmOplata.FrSumD:=quTTNInSUMIN.AsFloat;
      fmOplata.FiSkl:=quTTNInIDSKL.AsInteger;
      fmOplata.FiDDoc:=quTTNInID.AsInteger;
      fmOplata.FIDateDoc:=quTTNInIDATEDOC.AsInteger;

      fmOplata.cxCalcEdit1.Value:=RV(quTTNInSUMIN.AsFloat)-rv(rSum);
      fmOplata.cxDateEdit1.Date:=quTTNInIDATEDOC.AsInteger;
      fmOplata.cxTextEdit1.Text:='';

      fmOplata.ShowModal;
      fmOplata.quOplata.Active:=False;
      fmOplata.Release;
    end;
  end;
end;

end.
