unit Checks;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, SpeedBar, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, cxContainer, cxTextEdit,
  cxMemo, Placemnt;

type
  TfmChecks = class(TForm)
    Memo1: TcxMemo;
    GridCh: TcxGrid;
    ViewCh: TcxGridDBTableView;
    LevelCh: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    ViewChIdHead: TcxGridDBColumn;
    ViewChNum: TcxGridDBColumn;
    ViewChCode: TcxGridDBColumn;
    ViewChBarCode: TcxGridDBColumn;
    ViewChQuant: TcxGridDBColumn;
    ViewChPrice: TcxGridDBColumn;
    ViewChSumma: TcxGridDBColumn;
    ViewChProcessingTime: TcxGridDBColumn;
    ViewChDProc: TcxGridDBColumn;
    ViewChDSum: TcxGridDBColumn;
    ViewChId_Depart: TcxGridDBColumn;
    ViewChDateOperation: TcxGridDBColumn;
    ViewChOperation: TcxGridDBColumn;
    ViewChCk_Number: TcxGridDBColumn;
    ViewChCassir: TcxGridDBColumn;
    ViewChCash_Code: TcxGridDBColumn;
    ViewChNSmena: TcxGridDBColumn;
    ViewChCardNumber: TcxGridDBColumn;
    ViewChPaymentType: TcxGridDBColumn;
    ViewChNAME: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem6Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmChecks: TfmChecks;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmChecks.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := sFormIni;
  FormPlacement1.Active:=True;
  GridCh.Align:=AlClient;
  ViewCh.RestoreFromIniFile(sGridIni);
end;

procedure TfmChecks.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCh.StoreToIniFile(sGridIni,False);
end;

procedure TfmChecks.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewCh);
end;

procedure TfmChecks.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

end.
