unit CashReps;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo, SpeedBar,
  ExtCtrls, cxCalendar, cxImageComboBox, Placemnt, ComCtrls, Menus,
  ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmCashRep = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    Memo1: TcxMemo;
    GridCRep: TcxGrid;
    ViewCRep: TcxGridDBTableView;
    LevelCRep: TcxGridLevel;
    ViewCRepIDS: TcxGridDBColumn;
    ViewCRepNAME: TcxGridDBColumn;
    ViewCRepId_Depart: TcxGridDBColumn;
    ViewCRepIDate: TcxGridDBColumn;
    ViewCRepCash_Code: TcxGridDBColumn;
    ViewCRepNSmena: TcxGridDBColumn;
    ViewCRepOperation: TcxGridDBColumn;
    ViewCRepPaymentType: TcxGridDBColumn;
    ViewCRepQCHECK: TcxGridDBColumn;
    ViewCRepSUMR: TcxGridDBColumn;
    ViewCRepSUMD: TcxGridDBColumn;
    ViewCRepAVSUM: TcxGridDBColumn;
    fpfmCashRep: TFormPlacement;
    StatusBar1: TStatusBar;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    amCashRep: TActionManager;
    acWriteSale: TAction;
    SpeedItem7: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure ViewCRepDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure acWriteSaleExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashRep: TfmCashRep;

implementation

uses Dm, Un1, Period1, MainMC, PeriodSklList, WriteSale;

{$R *.dfm}

procedure TfmCashRep.FormCreate(Sender: TObject);
begin
  fpfmCashRep.IniFileName:=sFormIni;
  fpfmCashRep.Active:=True; delay(10);
  GridCRep.Align:=AlClient;
  ViewCRep.RestoreFromIniFile(sGridIni); delay(10);
end;

procedure TfmCashRep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCRep.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmCashRep.SpeedItem5Click(Sender: TObject);
Var iDateE,iDateB:INteger;
begin
  with dmMCS do
  begin
    fmPerSklList.ShowModal;
    if fmPerSklList.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmPerSklList.cxDateEdit1.Date);
      iDateE:=Trunc(fmPerSklList.cxDateEdit2.Date);
      CommonSet.DateBeg:=Trunc(fmPerSklList.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPerSklList.cxDateEdit2.Date);

      fmCashRep.Memo1.Clear;
      fmCashRep.Show;
      delay(10);
      fmCashRep.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE);
      fmCashRep.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      fmCashRep.ViewCRep.BeginUpdate;
      try
        quCashRep.Active:=False;
        quCashRep.Parameters.ParamByName('SDEP').Value:=fmPerSklList.cxCheckComboBox1.Text;
        quCashRep.Parameters.ParamByName('IDATEB').Value:=iDateB;
        quCashRep.Parameters.ParamByName('IDATEE').Value:=iDateE;
        quCashRep.Active:=True;
      finally
        fmCashRep.ViewCRep.EndUpdate;
      end;
      fmCashRep.Memo1.Lines.Add('������� ��������.'); delay(10);
    end;
  end;
end;

procedure TfmCashRep.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCashRep.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewCRep);
end;

procedure TfmCashRep.ViewCRepDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  prCalcSumAv(1,2,3,ViewCRep);
  prCalcDefGroupAv(1,2,3,-1,ViewCRep);
end;

procedure TfmCashRep.N1Click(Sender: TObject);
begin
  prExpandVi(ViewCRep,True);
end;

procedure TfmCashRep.N2Click(Sender: TObject);
begin
  prExpandVi(ViewCRep,False);
end;

procedure TfmCashRep.acWriteSaleExecute(Sender: TObject);
begin
  if not CanDo('prWriteSale') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    quDepsRep.Active:=False;
    quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quDepsRep.Active:=True;
    quDepsRep.First;

    fmWriteSale:=tfmWriteSale.create(Application);

    with fmWriteSale do
    begin
      fmWriteSale.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;
      fmWriteSale.cxLookupComboBox2.Text:=quDepsRepNAME.AsString;

      cxDateEdit1.Date:=Date-1;
      cxSpinEdit1.EditValue:=1;
      cxSpinEdit2.EditValue:=1;

      fmWriteSale.cxCalcEdit1.EditValue:=0;
      fmWriteSale.cxCalcEdit2.EditValue:=0;
      fmWriteSale.cxCalcEdit3.EditValue:=0;
      fmWriteSale.cxCalcEdit4.EditValue:=0;
    end;

    fmWriteSale.ShowModal;
    if fmWriteSale.ModalResult=mrOk then
    begin
      if fmWriteSale.cxLookupComboBox2.EditValue>0 then
      begin
        if Trunc(fmWriteSale.cxDateEdit1.Date)<prOpenDate(fmWriteSale.cxLookupComboBox2.EditValue) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; end
        else
        begin
          quProc.SQL.Clear;
          quProc.SQL.Add('');

          quProc.SQL.Add('DECLARE @IDATE int = '+its(Trunc(fmWriteSale.cxDateEdit1.Date)));
          quProc.SQL.Add('DECLARE @ISKL int = '+its(fmWriteSale.cxLookupComboBox2.EditValue));
          quProc.SQL.Add('DECLARE @CASHNUM int  ='+its(fmWriteSale.cxSpinEdit1.EditValue));
          quProc.SQL.Add('DECLARE @ZNUM int = '+its(fmWriteSale.cxSpinEdit2.EditValue));
          quProc.SQL.Add('DECLARE @SUMNAL float = '+fs(fmWriteSale.cxCalcEdit1.EditValue));
          quProc.SQL.Add('DECLARE @SUMBN float = '+fs(fmWriteSale.cxCalcEdit2.EditValue));
          quProc.SQL.Add('DECLARE @DISCNAL float = '+fs(fmWriteSale.cxCalcEdit3.EditValue));
          quProc.SQL.Add('DECLARE @DISCBN float = '+fs(fmWriteSale.cxCalcEdit4.EditValue));

          quProc.SQL.Add('EXECUTE [dbo].[prWriteSale] @IDATE,@ISKL,@CASHNUM,@ZNUM,@SUMNAL,@SUMBN,@DISCNAL,@DISCBN');
          quProc.ExecSQL;

          fmCashRep.ViewCRep.BeginUpdate;
          try
            quCashRep.Active:=False;
  //          quCashRep.Parameters.ParamByName('SDEP').Value:=fmPerSklList.cxCheckComboBox1.Text;
  //          quCashRep.Parameters.ParamByName('IDATEB').Value:=iDateB;
  //          quCashRep.Parameters.ParamByName('IDATEE').Value:=iDateE;
            quCashRep.Active:=True;
          finally
            fmCashRep.ViewCRep.EndUpdate;
          end;
        end;
      end;
    end;

    quDepsRep.Active:=False;
    fmWriteSale.Free;
  end;
end;

end.
