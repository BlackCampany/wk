unit ClassAlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  ADODB, cxCheckBox;

type
  TfmAlgClass = class(TForm)
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    amAlgClass: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    ViAlgClass: TcxGridDBTableView;
    leAlgClass: TcxGridLevel;
    GrAlgClass: TcxGrid;
    ViAlgClassID: TcxGridDBColumn;
    ViAlgClassNAME: TcxGridDBColumn;
    quAlgClass: TADOQuery;
    quAlgClassID: TIntegerField;
    quAlgClassNAME: TStringField;
    quAlgClassAFM: TSmallintField;
    dsquAlgClass: TDataSource;
    ViAlgClassAFM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAlgClass: TfmAlgClass;

implementation

uses Un1, ImpExcel, AddAlgClass, Dm;

{$R *.dfm}

procedure TfmAlgClass.FormCreate(Sender: TObject);
begin
  GrAlgClass.Align:=AlClient;
end;

procedure TfmAlgClass.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViAlgClass);
end;

procedure TfmAlgClass.SpeedItem3Click(Sender: TObject);
var ExcelApp, Workbook, ISheet: Variant;
    i:integer;
    iCode:integer;
begin
  if not CanDo('prImportAlcoClass') then begin Showmessage('��� ����.'); exit; end;
  if MessageDlg('������� ������������� �� ����� Excel? (��� ��������� ������� ����� �������))',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
  //  ShowMessage('��������� �� Excel');
    fmImpExcel:=tfmImpExcel.Create(Application);
    try
      fmImpExcel.ShowModal;
      if fmImpExcel.ModalResult=mrOk then
      begin
        if fmImpExcel.cxButtonEdit1.Text>'' then
        begin
          if fmImpExcel.cxSpinEdit1.Value>0 then
          begin
            // ������� �� Excel

            with dmMCS do
            begin
              if FileExists(fmImpExcel.cxButtonEdit1.Text) then
              begin
                quAlgClass.First;
                while not quAlgClass.Eof do quAlgClass.Delete;

                ExcelApp := CreateOleObject('Excel.Application');
                ExcelApp.Application.EnableEvents := false;
                Workbook := ExcelApp.WorkBooks.Add(fmImpExcel.cxButtonEdit1.Text);
                ISheet := Workbook.Worksheets.Item[1];

                i:=fmImpExcel.cxSpinEdit1.Value;

                ViAlgClass.BeginUpdate;
                While String(ISheet.Cells.Item[i, 1].Value)<>'' do
                begin
                  iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 1].Value),0);
                  if iCode>0 then
                  begin
                    if quAlgClass.Locate('ID',iCode,[])=False then quAlgClass.Append else quAlgClass.Edit;
                    quAlgClassID.AsInteger:=iCode;
                    quAlgClassNAME.AsString:=String(ISheet.Cells.Item[i, 2].Value);
                    quAlgClass.Post;
                  end;
                  inc(i);
                end;

                quAlgClass.First;

                ViAlgClass.EndUpdate;
                ExcelApp.Quit;
                ExcelApp:=Unassigned;
              end else ShowMessage('���� - '+fmImpExcel.cxButtonEdit1.Text+' �� ������.');
            end;
          end;
        end;
      end;
    finally
      fmImpExcel.Release;
    end;
  end;
end;

procedure TfmAlgClass.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAlgClass.acAddExecute(Sender: TObject);
begin
  if not CanDo('prAddAlcoClass') then begin Showmessage('��� ����.'); exit; end;
  try
    fmAddAlgClass:=tfmAddAlgClass.Create(Application);

    fmAddAlgClass.cxSpinEdit1.Value:=0;
    fmAddAlgClass.cxTextEdit1.Text:='';
    fmAddAlgClass.cxCheckBox1.Checked:=True;

    fmAddAlgClass.ShowModal;
    if fmAddAlgClass.ModalResult=mrOk then
    begin
      try
        fmAlgClass.ViAlgClass.BeginUpdate;
        if fmAddAlgClass.cxSpinEdit1.Value>0 then
        begin
          with dmMCS do
          begin
            if quAlgClass.Active then
            begin
              if quAlgClass.locate('ID',fmAddAlgClass.cxSpinEdit1.Value,[]) then
              begin
                showmessage('������� � ����� ����� ��� ����. ���������� ����������.');
              end else
              begin
                quAlgClass.Append;
                quAlgClassID.AsInteger:=fmAddAlgClass.cxSpinEdit1.Value;
                quAlgClassNAME.AsString:=fmAddAlgClass.cxTextEdit1.Text;
                if fmAddAlgClass.cxCheckBox1.Checked then quAlgClassAFM.AsInteger:=1 else quAlgClassAFM.AsInteger:=0;
                quAlgClass.Post;
              end;
            end;
          end;
        end else showmessage('������� �������� ���� �����������. ���������� ����������.');
      finally
        fmAlgClass.ViAlgClass.EndUpdate;
      end;
    end;

  finally
    fmAddAlgClass.Release;
  end;
end;

procedure TfmAlgClass.acEditExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditAlcoClass') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    if quAlgClass.RecordCount>0 then
    begin
      try
        fmAddAlgClass:=tfmAddAlgClass.Create(Application);

        fmAddAlgClass.cxSpinEdit1.Value:=quAlgClassID.AsInteger;
        fmAddAlgClass.cxSpinEdit1.Properties.ReadOnly:=True;
        fmAddAlgClass.cxTextEdit1.Text:=quAlgClassNAME.AsString;
        if quAlgClassAFM.AsInteger=1 then fmAddAlgClass.cxCheckBox1.Checked:=True else fmAddAlgClass.cxCheckBox1.Checked:=False; 

        fmAddAlgClass.ShowModal;
        if fmAddAlgClass.ModalResult=mrOk then
        begin
          quAlgClass.Edit;
          quAlgClassNAME.AsString:=fmAddAlgClass.cxTextEdit1.Text;
          if fmAddAlgClass.cxCheckBox1.Checked then quAlgClassAFM.AsInteger:=1 else quAlgClassAFM.AsInteger:=0;
          quAlgClass.Post;

          quAlgClass.Refresh;
        end;
      finally
        fmAddAlgClass.Release;
      end;
    end;
  end;
end;

procedure TfmAlgClass.acDelExecute(Sender: TObject);
begin
  if not CanDo('prDelAlcoClass') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    if quAlgClass.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quAlgClassNAME.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quAlgClass.Delete;
      end;
    end;
  end;
end;

end.
