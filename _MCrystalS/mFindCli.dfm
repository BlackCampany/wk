object fmFindCli: TfmFindCli
  Left = 698
  Top = 132
  Width = 724
  Height = 467
  Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '#1087#1086#1080#1089#1082#1072
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 592
    Top = 0
    Width = 124
    Height = 433
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 384
      Width = 49
      Height = 13
      AutoSize = False
      Caption = '....'
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 16
      Width = 89
      Height = 25
      Caption = #1055#1077#1088#1077#1081#1090#1080
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 16
      Top = 64
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridFind: TcxGrid
    Left = 8
    Top = 4
    Width = 577
    Height = 417
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewFind: TcxGridDBTableView
      OnDblClick = ViewFindDblClick
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewFindCustomDrawCell
      DataController.DataSource = dmMCS.dsquFindCli
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object ViewFindId: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Id'
        Styles.Content = dmMCS.cxStyle5
        Width = 63
      end
      object ViewFindName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmMCS.cxStyle5
        Width = 172
      end
      object ViewFindFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Width = 184
      end
      object ViewFindIActive: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IActive'
        Visible = False
      end
      object ViewFindINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'INN'
      end
    end
    object LevelFind: TcxGridLevel
      GridView = ViewFind
    end
  end
end
