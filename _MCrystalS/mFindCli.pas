unit mFindCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, cxImageComboBox;

type
  TfmFindCli = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFind: TcxGridDBTableView;
    LevelFind: TcxGridLevel;
    GridFind: TcxGrid;
    ViewFindId: TcxGridDBColumn;
    ViewFindName: TcxGridDBColumn;
    Label1: TLabel;
    ViewFindIActive: TcxGridDBColumn;
    ViewFindFullName: TcxGridDBColumn;
    ViewFindINN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewFindCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ViewFindDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindCli: TfmFindCli;

implementation

uses Dm;

{$R *.dfm}

procedure TfmFindCli.FormCreate(Sender: TObject);
begin
  GridFind.Align:=AlClient;
end;

procedure TfmFindCli.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmFindCli.ViewFindCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;

begin

//  if CountRec1(dmMC.quFindC)=False then exit;

  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewFind.ColumnCount-1 do
  begin
    if ViewFind.Columns[i].Name='ViewFindIActive' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;
end;

procedure TfmFindCli.FormShow(Sender: TObject);
begin
  GridFind.SetFocus;
end;

procedure TfmFindCli.ViewFindDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
