unit RepPostRemnDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxCalendar, cxImageComboBox, Menus,
  Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar, cxContainer, cxTextEdit, cxMemo, ExtCtrls, ADODB;

type
  TfmRepPostRemnDay = class(TForm)
    Panel5: TPanel;
    Memo1: TcxMemo;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    GridRepPostRemnDay: TcxGrid;
    ViewRepPostRemnDay: TcxGridDBTableView;
    LevelRepPostRemnDay: TcxGridLevel;
    fmplRepPostRemnDay: TFormPlacement;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    quRepPostRemnDay: TADOQuery;
    dsquRepPostRemnDay: TDataSource;
    quRepPostRemnDayISKL: TIntegerField;
    quRepPostRemnDayNAMEDEP: TStringField;
    quRepPostRemnDayICODE: TIntegerField;
    quRepPostRemnDayNAMEC: TStringField;
    quRepPostRemnDayNAMEGR2: TStringField;
    quRepPostRemnDayNAMEGR: TStringField;
    quRepPostRemnDayEDIZM: TSmallintField;
    quRepPostRemnDayVOL: TFloatField;
    quRepPostRemnDayKREP: TFloatField;
    quRepPostRemnDayAVID: TIntegerField;
    quRepPostRemnDayMAKER: TIntegerField;
    quRepPostRemnDayNAMEPROD: TStringField;
    quRepPostRemnDayREMND: TFloatField;
    quRepPostRemnDayIDDOC: TLargeintField;
    quRepPostRemnDayIDATEDOC: TIntegerField;
    quRepPostRemnDayNUMDOC: TStringField;
    quRepPostRemnDayIDCLI: TIntegerField;
    quRepPostRemnDayNAMECLI: TStringField;
    quRepPostRemnDayPRICEIN: TFloatField;
    quRepPostRemnDayPRICER: TFloatField;
    quRepPostRemnDaySUMIN: TFloatField;
    quRepPostRemnDaySUMR: TFloatField;
    ViewRepPostRemnDayISKL: TcxGridDBColumn;
    ViewRepPostRemnDayNAMEDEP: TcxGridDBColumn;
    ViewRepPostRemnDayICODE: TcxGridDBColumn;
    ViewRepPostRemnDayNAMEC: TcxGridDBColumn;
    ViewRepPostRemnDayNAMEGR2: TcxGridDBColumn;
    ViewRepPostRemnDayNAMEGR: TcxGridDBColumn;
    ViewRepPostRemnDayEDIZM: TcxGridDBColumn;
    ViewRepPostRemnDayVOL: TcxGridDBColumn;
    ViewRepPostRemnDayKREP: TcxGridDBColumn;
    ViewRepPostRemnDayAVID: TcxGridDBColumn;
    ViewRepPostRemnDayMAKER: TcxGridDBColumn;
    ViewRepPostRemnDayNAMEPROD: TcxGridDBColumn;
    ViewRepPostRemnDayREMND: TcxGridDBColumn;
    ViewRepPostRemnDayIDDOC: TcxGridDBColumn;
    ViewRepPostRemnDayIDATEDOC: TcxGridDBColumn;
    ViewRepPostRemnDayNUMDOC: TcxGridDBColumn;
    ViewRepPostRemnDayIDCLI: TcxGridDBColumn;
    ViewRepPostRemnDayNAMECLI: TcxGridDBColumn;
    ViewRepPostRemnDayPRICEIN: TcxGridDBColumn;
    ViewRepPostRemnDayPRICER: TcxGridDBColumn;
    ViewRepPostRemnDaySUMIN: TcxGridDBColumn;
    ViewRepPostRemnDaySUMR: TcxGridDBColumn;
    N1: TMenuItem;
    quRepPostRemnDayREMNDAL: TFloatField;
    ViewRepPostRemnDayREMNDAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure acMoveRep11Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPostRemnDay: TfmRepPostRemnDay;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmRepPostRemnDay.FormCreate(Sender: TObject);
begin
  fmplRepPostRemnDay.IniFileName:=sFormIni;
  fmplRepPostRemnDay.Active:=True; delay(10);

  GridRepPostRemnDay.Align:=AlClient;
end;

procedure TfmRepPostRemnDay.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel15(ViewRepPostRemnDay);
end;

procedure TfmRepPostRemnDay.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPostRemnDay.acMoveRep11Click(Sender: TObject);
begin
  prExpandVi(ViewRepPostRemnDay,True);
end;

procedure TfmRepPostRemnDay.N1Click(Sender: TObject);
begin
  prExpandVi(ViewRepPostRemnDay,False);
end;

end.
