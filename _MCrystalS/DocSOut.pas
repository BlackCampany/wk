unit DocSOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxImageComboBox, dxmdaset, cxLabel, cxDropDownEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxMemo, ComCtrls, cxGroupBox,
  cxRadioGroup, StdCtrls, cxButtons, cxCheckBox, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit, cxMaskEdit, cxCalendar,
  cxContainer, cxTextEdit, ExtCtrls, Placemnt, ActnList, XPStyleActnCtrls,
  ActnMan, FR_DSet, FR_DBSet, FR_Class;

type
  TfmDocSOut = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label17: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    cxButton3: TcxButton;
    cxRadioGroup1: TcxRadioGroup;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton4: TcxButton;
    cxButton7: TcxButton;
    Panel5: TPanel;
    Memo1: TcxMemo;
    Label13: TLabel;
    cxComboBox1: TcxComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    GridDoc2: TcxGrid;
    ViewDoc2: TcxGridDBTableView;
    LevelDoc2: TcxGridLevel;
    taSpecOut: TdxMemData;
    taSpecOutNum: TIntegerField;
    taSpecOutCodeTovar: TIntegerField;
    taSpecOutCodeEdIzm: TSmallintField;
    taSpecOutBarCode: TStringField;
    taSpecOutNDSProc: TFloatField;
    taSpecOutNDSSumIn: TFloatField;
    taSpecOutKol: TFloatField;
    taSpecOutName: TStringField;
    taSpecOutTovarType: TIntegerField;
    taSpecOutRemn: TFloatField;
    taSpecOutsMaker: TStringField;
    taSpecOutAVid: TIntegerField;
    taSpecOutVol: TFloatField;
    taSpecOutPriceIn: TFloatField;
    taSpecOutPriceIn0: TFloatField;
    taSpecOutSumIn: TFloatField;
    taSpecOutSumIn0: TFloatField;
    taSpecOutPriceR: TFloatField;
    taSpecOutSumR: TFloatField;
    taSpecOutVolDL: TFloatField;
    dstaSpecOut: TDataSource;
    taSpecOutNDSSumOut: TFloatField;
    taSpecOutiMaker: TIntegerField;
    ViewDoc2Num: TcxGridDBColumn;
    ViewDoc2CodeTovar: TcxGridDBColumn;
    ViewDoc2CodeEdIzm: TcxGridDBColumn;
    ViewDoc2BarCode: TcxGridDBColumn;
    ViewDoc2Name: TcxGridDBColumn;
    ViewDoc2TovarType: TcxGridDBColumn;
    ViewDoc2Kol: TcxGridDBColumn;
    ViewDoc2PriceIn: TcxGridDBColumn;
    ViewDoc2PriceIn0: TcxGridDBColumn;
    ViewDoc2PriceR: TcxGridDBColumn;
    ViewDoc2SumIn: TcxGridDBColumn;
    ViewDoc2SumIn0: TcxGridDBColumn;
    ViewDoc2SumR: TcxGridDBColumn;
    ViewDoc2NDSProc: TcxGridDBColumn;
    ViewDoc2NDSSumIn: TcxGridDBColumn;
    ViewDoc2NDSSumOut: TcxGridDBColumn;
    ViewDoc2Remn: TcxGridDBColumn;
    ViewDoc2sMaker: TcxGridDBColumn;
    ViewDoc2AVid: TcxGridDBColumn;
    ViewDoc2Vol: TcxGridDBColumn;
    ViewDoc2VolDL: TcxGridDBColumn;
    taSpecOutPriceR0: TFloatField;
    taSpecOutSumR0: TFloatField;
    ViewDoc2PriceR0: TcxGridDBColumn;
    ViewDoc2SumR0: TcxGridDBColumn;
    FormPlacementOut: TFormPlacement;
    Edit1: TEdit;
    amDocOut: TActionManager;
    acSaveDoc: TAction;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acReadBar: TAction;
    taSpPrint: TdxMemData;
    taSpPrintIGr: TSmallintField;
    taSpPrintNum: TIntegerField;
    taSpPrintCodeTovar: TIntegerField;
    taSpPrintFullName: TStringField;
    taSpPrintCodeEdIzm: TSmallintField;
    taSpPrintNDSProc: TFloatField;
    taSpPrintOutNDSSum: TFloatField;
    taSpPrintKolMest: TFloatField;
    taSpPrintKolWithMest: TFloatField;
    taSpPrintKol: TFloatField;
    taSpPrintCenaTovar: TFloatField;
    taSpPrintSumCenaTovar: TFloatField;
    taSpPrintNDSSum: TFloatField;
    taSpPrintCenaMag: TFloatField;
    taSpPrintSumCenaMag: TFloatField;
    taSpPrintPostQuant: TFloatField;
    taSpPrintPostDate: TDateField;
    taSpPrintPostNum: TStringField;
    taSpPrintVesTara: TFloatField;
    taSpPrintBrak: TFloatField;
    taSpPrintKolF: TFloatField;
    taSpPrintSumF: TFloatField;
    taSpPrintCenaTovar0: TFloatField;
    taSpPrintSumCenaTovar0: TFloatField;
    taSpPrintNac: TFloatField;
    taSpPrintOutNDSPrice: TFloatField;
    taSpPrintLastTTNNum: TStringField;
    taSpPrintLastTTNDate: TStringField;
    frRepDOUT: TfrReport;
    frtaSpecOut: TfrDBDataSet;
    frtaSpPrint: TfrDBDataSet;
    taSpecOutPostDate: TDateField;
    taSpecOutPostNum: TStringField;
    taSpecOutPostQuant: TFloatField;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1DATEDOC: TcxGridDBColumn;
    Vi1Name: TcxGridDBColumn;
    Vi1QUANT: TcxGridDBColumn;
    Vi1PRICEIN: TcxGridDBColumn;
    Vi1PROCN: TcxGridDBColumn;
    Vi1PRICEUCH: TcxGridDBColumn;
    Vi1SUMIN: TcxGridDBColumn;
    Vi1SUMUCH: TcxGridDBColumn;
    Vi1NUMDOC: TcxGridDBColumn;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennQuant: TcxGridDBColumn;
    ViCennQuanrR: TcxGridDBColumn;
    le1: TcxGridLevel;
    Le2: TcxGridLevel;
    acCalcLastPrice: TAction;
    acOprih: TAction;
    acMove: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton4Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure ViewDoc2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxButton1Click(Sender: TObject);
    procedure taSpecOutKolChange(Sender: TField);
    procedure taSpecOutPriceInChange(Sender: TField);
    procedure taSpecOutPriceIn0Change(Sender: TField);
    procedure taSpecOutPriceRChange(Sender: TField);
    procedure taSpecOutPriceR0Change(Sender: TField);
    procedure taSpecOutSumInChange(Sender: TField);
    procedure taSpecOutSumIn0Change(Sender: TField);
    procedure taSpecOutSumRChange(Sender: TField);
    procedure taSpecOutSumR0Change(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewDoc2SelectionChanged(Sender: TcxCustomGridTableView);
    procedure Vi1DblClick(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure acCalcLastPriceExecute(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acOprihExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prCalcValF;
  end;

Function SaveDocsOut(var IdH:Integer):boolean;

var
  fmDocSOut: TfmDocSOut;
  iCol:Integer;

implementation

uses Un1, Dm, Clients, Cards, BarcodeNo, mFind, DocHOut, sumprops, Move,
  MainMC;

{$R *.dfm}

procedure tfmDocSOut.prCalcValF;
begin
  if taSpecOut.State in [dsInsert,dsEdit] then
  begin
    taSpecOutSumIn.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceIn.AsFloat);
    taSpecOutSumIn0.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceIn0.AsFloat);
    taSpecOutSumR.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceR.AsFloat);
    taSpecOutSumR0.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceR0.AsFloat);
    taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
    taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
  end;
end;


Function SaveDocsOut(var IdH:Integer):boolean;
Var rS:array[1..10] of Real;
    iSS,i:INteger;
    bErr:Boolean;
begin
  with dmMCS do
  with fmDocSOut do
  begin
    Result:=False;
    if cxLookupComboBox1.EditValue<1 then exit;

    bErr:=False;
    quTTNOutRec.Active:=False;
    IDH:=fmDocSOut.Tag;
    iSS:=fSS(cxLookupComboBox1.EditValue);
    try
      ViewDoc2.BeginUpdate;

      if IDH=0 then //����������
      begin
        IDH:=fGetId(11); //��������� ���������� ���������
        quTTNOutRec.Parameters.ParamByName('IDH').Value:=IDH;
        quTTNOutRec.Active:=True;
        quTTNOutRec.Append;
        quTTNOutRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quTTNOutRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quTTNOutRecID.AsInteger:=IDH;
        quTTNOutRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quTTNOutRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quTTNOutRecDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
        quTTNOutRecNUMSF.AsString:=Trim(cxTextEdit2.Text);
        quTTNOutRecIDCLI.AsInteger:=cxButtonEdit1.tag;
        quTTNOutRecSUMIN.AsFloat:=0;
        quTTNOutRecSUMUCH.AsFloat:=0;
        quTTNOutRecSUMTAR.AsFloat:=0;
        quTTNOutRecIACTIVE.AsInteger:=1;
        quTTNOutRecOPRIZN.AsInteger:=cxComboBox1.ItemIndex;
        quTTNOutRecSNUMZ.AsString:='';
        quTTNOutRec.Post;

        fmDocSOut.Tag:=IDH;
      end else
      begin
        quTTNOutRec.Parameters.ParamByName('IDH').Value:=IDH;
        quTTNOutRec.Active:=True;
        quTTNOutRec.Edit;
        quTTNOutRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quTTNOutRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quTTNOutRecID.AsInteger:=IDH;
        quTTNOutRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quTTNOutRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quTTNOutRecDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
        quTTNOutRecNUMSF.AsString:=Trim(cxTextEdit2.Text);
        quTTNOutRecIDCLI.AsInteger:=cxButtonEdit1.tag;
        quTTNOutRecSUMIN.AsFloat:=0;
        quTTNOutRecSUMUCH.AsFloat:=0;
        quTTNOutRecSUMTAR.AsFloat:=0;
        quTTNOutRecIACTIVE.AsInteger:=1;
        quTTNOutRecOPRIZN.AsInteger:=cxComboBox1.ItemIndex;
        quTTNOutRecSNUMZ.AsString:='';
        quTTNOutRec.Post;
      end;

      quSpecOutRec.Active:=False;
      quSpecOutRec.Parameters.ParamByName('IDH').Value:=IDH;
      quSpecOutRec.Active:=True;

      quSpecOutRec.First;
      while not quSpecOutRec.Eof do quSpecOutRec.Delete; //���� �������

      for i:=1 to 10 do rS[i]:=0;

      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        taSpecOut.Edit;
        taSpecOutSumIn.AsFloat:=rv(taSpecOutSumIn.AsFloat);
        taSpecOutSumIn0.AsFloat:=rv(taSpecOutSumIn0.AsFloat);
        taSpecOutSumR.AsFloat:=rv(taSpecOutSumR.AsFloat);
        taSpecOutSumR0.AsFloat:=rv(taSpecOutSumR0.AsFloat);
        taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
        taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
        taSpecOut.Post;

        rS[1]:=rS[1]+taSpecOutSumIn.AsFloat;
        rS[2]:=rS[2]+taSpecOutSumIn0.AsFloat;
        rS[3]:=rS[3]+taSpecOutSumR.AsFloat;
        rS[4]:=rS[4]+taSpecOutSumR0.AsFloat;

        quSpecOutRec.Append;
        quSpecOutRecIDHEAD.AsInteger:=IDH;
        quSpecOutRecNUM.AsInteger:=taSpecOutNum.AsInteger;
        quSpecOutRecIDCARD.AsInteger:=taSpecOutCodeTovar.AsInteger;
        quSpecOutRecSBAR.AsString:=taSpecOutBarCode.AsString;
        quSpecOutRecIDM.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
        quSpecOutRecQUANT.AsFloat:=taSpecOutKol.AsFloat;
        quSpecOutRecPRICEIN.AsFloat:=taSpecOutPriceIn.AsFloat;
        quSpecOutRecSUMIN.AsFloat:=taSpecOutSumIn.AsFloat;
        quSpecOutRecPRICEIN0.AsFloat:=taSpecOutPriceIn0.AsFloat;
        quSpecOutRecSUMIN0.AsFloat:=taSpecOutSumIn0.AsFloat;
        quSpecOutRecPRICER.AsFloat:=taSpecOutPriceR.AsFloat;
        quSpecOutRecSUMR.AsFloat:=taSpecOutSumR.AsFloat;
        quSpecOutRecPRICER0.AsFloat:=taSpecOutPriceR0.AsFloat;
        quSpecOutRecSUMR0.AsFloat:=taSpecOutSumR0.AsFloat;
        quSpecOutRecNDSPROC.AsFloat:=taSpecOutNDSProc.AsFloat;
        quSpecOutRecSUMNDSIN.AsFloat:=taSpecOutNDSSumIn.AsFloat;
        quSpecOutRecSUMNDSOUT.AsFloat:=taSpecOutNDSSumOut.AsFloat;
        quSpecOutRecSNUMHEAD.AsString:='';
        quSpecOutRec.Post;

        taSpecOut.Next;
      end;
      quSpecOutRec.Active:=False;

      quTTNOutRec.Edit;
      if iSS=2 then quTTNOutRecSUMIN.AsFloat:=rS[2] else quTTNOutRecSUMIN.AsFloat:=rS[1];
      quTTNOutRecSUMUCH.AsFloat:=rS[3];
      quTTNOutRecSUMTAR.AsFloat:=rS[5];
      quTTNOutRec.Post;

      bErr:=True;
    finally
      ViewDoc2.EndUpdate;
      if bErr then Result:=True;
    end;
  end;
end;


procedure TfmDocSOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc2.StoreToIniFile(sGridIni,False);
end;

procedure TfmDocSOut.cxButton4Click(Sender: TObject);
begin
  prNExportExel6(ViewDoc2);
end;

procedure TfmDocSOut.acSaveDocExecute(Sender: TObject);
Var IDH:INteger;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if SaveDocsOut(IdH) then
  begin
    with dmMCS do
    begin
      fmDocsOutH.ViewDocsOut.BeginUpdate;
      quTTNOut.Requery();
      quTTNOut.Locate('ID',IDH,[]);
      fmDocsOutH.ViewDocsOut.EndUpdate;
      fmDocsOutH.ViewDocsOut.Controller.FocusRecord(fmDocsOutH.ViewDocsOut.DataController.FocusedRowIndex,True);

      Memo1.Lines.Add('���������� ��.'); delay(10);
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocSOut.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocSOut.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var i:Integer;
begin
  with dmMCS do
  with fmClients do
  begin
    iDirect:=2; //0 - ������ , 2- �������

    fmClients.Show;

    if cxButtonEdit1.Tag>0 then  //�����-�� ��������� - ���� �� ���� ���������
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
      quFCli.SQL.Add('where cli.[Id]='+its(cxButtonEdit1.Tag));
      quFCli.Active:=True;

      bRefreshCli:=False;
      for i:=0 to CliTree.Items.Count-1 Do
      begin
        if Integer(CliTree.Items[i].Data) = quFCli.FieldByName('IdParent').AsInteger then
        begin
          CliTree.Items[i].Expand(False);
          CliTree.Repaint;
          CliTree.Items[i].Selected:=True;
          try
            CliView.BeginUpdate;
            prRefreshCli(quFCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
            quClients.Locate('ID',quFCli.FieldByName('Id').AsInteger,[]);
          finally
            CliView.EndUpdate;
            CliGr.SetFocus;
            CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCli:=True;
      quFCli.Active:=False;
    end;
  end;
end;

procedure TfmDocSOut.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  if cxButton1.Enabled then //��������� ���������
  begin
    iMax:=1;
    ViewDoc2.BeginUpdate;

    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      if taSpecOutNum.AsInteger>=iMax then  iMax:=taSpecOutNum.AsInteger+1;
      taSpecOut.Next;
    end;

    taSpecOut.Append;
    taSpecOutNum.AsInteger:=iMax;
    taSpecOutCodeTovar.AsInteger:=0;
    taSpecOutCodeEdIzm.AsInteger:=1;
    taSpecOutBarCode.AsString:='';
    taSpecOutNDSProc.AsFloat:=0;
    taSpecOutNDSSumIn.AsFloat:=0;
    taSpecOutKol.AsFloat:=0;
    taSpecOutName.AsString:='';
    taSpecOutTovarType.AsInteger:=0;
    taSpecOutPriceIn.AsFloat:=0;
    taSpecOutPriceIn0.AsFloat:=0;
    taSpecOutSumIn.AsFloat:=0;
    taSpecOutSumIn0.AsFloat:=0;
    taSpecOutPriceR.AsFloat:=0;
    taSpecOutPriceR0.AsFloat:=0;
    taSpecOutSumR0.AsFloat:=0;
    taSpecOutSumR.AsFloat:=0;
    taSpecOutVolDL.AsFloat:=0;
    taSpecOutNDSSumOut.AsFloat:=0;
    taSpecOutiMaker.AsInteger:=0;
    taSpecOutRemn.AsFloat:=0;
    taSpecOutsMaker.AsString:='';
    taSpecOutAVid.AsInteger:=0;
    taSpecOutVol.AsFloat:=0;
    taSpecOut.Post;

    ViewDoc2.EndUpdate;
    GridDoc2.SetFocus;

    ViewDoc2CodeTovar.Options.Editing:=True;
    ViewDoc2Name.Options.Editing:=True;

    ViewDoc2Name.Focused:=True;
    ViewDoc2.Controller.FocusRecord(ViewDoc2.DataController.FocusedRowIndex,True);
  end;
end;

procedure TfmDocSOut.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if cxButton1.Enabled then //��������� ���������
    if taSpecOut.RecordCount>0 then
    begin
      taSpecOut.Delete;
    end;
end;

procedure TfmDocSOut.acAddListExecute(Sender: TObject);
begin
  if cxButton1.Enabled then //��������� ���������
  begin
    iDirect:=3;
    if cxLookupComboBox1.EditValue<>fmMainMC.Label3.Tag then
    begin
      fmMainMC.Label3.Tag:=cxLookupComboBox1.EditValue;
      CurDep:=Label3.Tag;
      fmMainMC.Label3.Caption:=cxLookupComboBox1.Text;
      try
        fmCards.CardsView.BeginUpdate;
        prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),cxLookupComboBox1.EditValue,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
      finally
        fmCards.CardsView.EndUpdate;
      end;
    end;
    fmCards.Show;
  end;
end;

procedure TfmDocSOut.acDelAllExecute(Sender: TObject);
begin
  if cxButton1.Enabled then //��������� ���������
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      taSpecOut.First; while not taSpecOut.Eof do taSpecOut.Delete;
    end;
  end;
end;

procedure TfmDocSOut.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmDocSOut.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var sBar:String;
    IC,iMax,kNDS:INteger;
begin
  if (Key=$0D) then
  begin

    if cxButton1.Enabled=False then exit;
    if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

    sBar:=Edit1.Text;
    if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
    if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
    with dmMCS do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iC));
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount>0 then
        begin
          if quFCPISTATUS.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������. ���������� ����������.');
          end else
          begin
            if taSpecOut.RecordCount>0 then
              if taSpecOut.Locate('CodeTovar',iC,[]) then
              begin
                ViewDoc2.Controller.FocusRecord(ViewDoc2.DataController.FocusedRowIndex,True);
                GridDoc2.SetFocus;
                exit;
              end;
            iMax:=1;

            fmDocSOut.ViewDoc2.BeginUpdate;
            taSpecOut.First;
            while not taSpecOut.Eof do
            begin
              if taSpecOutNum.AsInteger>=iMax then  iMax:=taSpecOutNum.AsInteger+1;
              taSpecOut.Next;
            end;
            fmDocSOut.ViewDoc2.EndUpdate;

            kNDS:=Label17.Tag;

            taSpecOut.Append;
            taSpecOutNum.AsInteger:=iMax;
            taSpecOutCodeTovar.AsInteger:=quFCPID.asinteger;
            taSpecOutCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
            taSpecOutBarCode.AsString:=sBar;
            taSpecOutNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
            taSpecOutNDSSumIn.AsFloat:=0;
            taSpecOutKol.AsFloat:=0;
            taSpecOutName.AsString:=quFCPNAME.AsString;
            taSpecOutTovarType.AsInteger:=0;
            taSpecOutPriceIn.AsFloat:=0;
            taSpecOutPriceIn0.AsFloat:=0;
            taSpecOutSumIn.AsFloat:=0;
            taSpecOutSumIn0.AsFloat:=0;
            taSpecOutPriceR.AsFloat:=quFCPPRICE.AsFloat;
            taSpecOutPriceR0.AsFloat:=0;
            taSpecOutSumR0.AsFloat:=0;
            taSpecOutSumR.AsFloat:=0;
            taSpecOutVolDL.AsFloat:=0;
            taSpecOutNDSSumOut.AsFloat:=0;
            taSpecOutiMaker.AsInteger:=quFCPMAKER.AsInteger;
            taSpecOutRemn.AsFloat:=0;
            taSpecOutsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
            taSpecOutAVid.AsInteger:=quFCPAVID.AsInteger;
            taSpecOutVol.AsFloat:=quFCPVOL.AsFloat;
            taSpecOut.Post;
          end;
        end;
        quFCP.Active:=False;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmDocSOut.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmDocSOut.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmDocSOut.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDocSOut.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmDocSOut.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmDocSOut.ViewDoc2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Kol' then iCol:=1;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2PriceIn' then iCol:=2;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2PriceIn0' then iCol:=3;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2PriceR' then iCol:=4;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2PriceR0' then iCol:=5;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumIn' then iCol:=6;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumIn0' then iCol:=7;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumR' then iCol:=8;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumR0' then iCol:=9;
end;

procedure TfmDocSOut.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  iCol:=0;
end;

procedure TfmDocSOut.ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  with dmMCS do
  begin
    if (Key=$0D) then
    begin
      if cxButton1.Enabled=False then exit;
      if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

      kNDS:=0;
      iSS:=fSS(cxLookupComboBox1.EditValue);
      if (iSS=2)and(Label17.Tag=1) then kNDS:=1;

      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iCode));
//        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(s1)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          taSpecOut.Edit;
          taSpecOutCodeTovar.AsInteger:=quFCPID.asinteger;
          taSpecOutCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecOutBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecOutNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecOutNDSSumIn.AsFloat:=0;
          taSpecOutKol.AsFloat:=0;
          taSpecOutName.AsString:=quFCPNAME.AsString;
          taSpecOutTovarType.AsInteger:=0;
          taSpecOutPriceIn.AsFloat:=0;
          taSpecOutPriceIn0.AsFloat:=0;
          taSpecOutSumIn.AsFloat:=0;
          taSpecOutSumIn0.AsFloat:=0;
          taSpecOutPriceR.AsFloat:=quFCPPRICE.AsFloat;
          taSpecOutPriceR0.AsFloat:=0;
          taSpecOutSumR0.AsFloat:=0;
          taSpecOutSumR.AsFloat:=0;
          taSpecOutVolDL.AsFloat:=0;
          taSpecOutNDSSumOut.AsFloat:=0;
          taSpecOutiMaker.AsInteger:=quFCPMAKER.AsInteger;
          taSpecOutRemn.AsFloat:=0;
          taSpecOutsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
          taSpecOutAVid.AsInteger:=quFCPAVID.AsInteger;
          taSpecOutVol.AsFloat:=quFCPVOL.AsFloat;
          taSpecOut.Post;
        end;
        quFCP.Active:=False;
      end;
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Name' then
      begin
        bAdd:=True;

        sName:=VarAsType(AEdit.EditingValue, varString);
        if Length(sName)>3 then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
            quFindC.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//            quFindC.SQL.Add('and [TTOVAR]=0');
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if quFindC.RecordCount>0 then
            begin
              taSpecOut.Edit;
              taSpecOutCodeTovar.AsInteger:=quFindCID.asinteger;
              taSpecOutCodeEdIzm.AsInteger:=quFindCEDIZM.AsInteger;
              taSpecOutBarCode.AsString:=quFindCBARCODE.AsString;
              taSpecOutNDSProc.AsFloat:=kNDS*quFindCNDS.AsFloat;
              taSpecOutNDSSumIn.AsFloat:=0;
              taSpecOutKol.AsFloat:=0;
              taSpecOutName.AsString:=quFindCNAME.AsString;
              taSpecOutTovarType.AsInteger:=0;
              taSpecOutPriceIn.AsFloat:=0;
              taSpecOutPriceIn0.AsFloat:=0;
              taSpecOutSumIn.AsFloat:=0;
              taSpecOutSumIn0.AsFloat:=0;
              taSpecOutPriceR.AsFloat:=quFindCPRICE.AsFloat;
              taSpecOutPriceR0.AsFloat:=0;
              taSpecOutSumR0.AsFloat:=0;
              taSpecOutSumR.AsFloat:=0;
              taSpecOutVolDL.AsFloat:=0;
              taSpecOutNDSSumOut.AsFloat:=0;
              taSpecOutiMaker.AsInteger:=quFindCMAKER.AsInteger;
              taSpecOutRemn.AsFloat:=0;
              taSpecOutsMaker.AsString:=prFindMaker(quFindCMAKER.AsInteger);
              taSpecOutAVid.AsInteger:=quFindCAVID.AsInteger;
              taSpecOutVol.AsFloat:=quFindCVOL.AsFloat;
              taSpecOut.Post;

              AEdit.SelectAll;
            end;
          end;
          quFindC.Active:=False;
        end;

        bAdd:=False;
      end;

    end else
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTovar' then
        if fTestKey(Key)=False then
          if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Cancel;
  end;
end;

procedure TfmDocSOut.ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  if bAdd then exit;
  with dmMCS do
  begin
    if cxButton1.Enabled=False then exit;
    if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

    if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Name' then
    begin
      sName:=VarAsType(AEdit.EditingValue, varString);
      if Length(sName)>3 then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT TOP 2 * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          kNDS:=0;
          iSS:=fSS(cxLookupComboBox1.EditValue);
          if (iSS=2)and(Label17.Tag=1) then kNDS:=1;

          ViewDoc2.BeginUpdate;

          taSpecOut.Edit;
          taSpecOutCodeTovar.AsInteger:=quFCPID.asinteger;
          taSpecOutCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecOutBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecOutNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecOutNDSSumIn.AsFloat:=0;
          taSpecOutKol.AsFloat:=0;
          taSpecOutName.AsString:=quFCPNAME.AsString;
          taSpecOutTovarType.AsInteger:=0;
          taSpecOutPriceIn.AsFloat:=0;
          taSpecOutPriceIn0.AsFloat:=0;
          taSpecOutSumIn.AsFloat:=0;
          taSpecOutSumIn0.AsFloat:=0;
          taSpecOutPriceR.AsFloat:=quFCPPRICE.AsFloat;
          taSpecOutPriceR0.AsFloat:=0;
          taSpecOutSumR0.AsFloat:=0;
          taSpecOutSumR.AsFloat:=0;
          taSpecOutVolDL.AsFloat:=0;
          taSpecOutNDSSumOut.AsFloat:=0;
          taSpecOutiMaker.AsInteger:=quFCPMAKER.AsInteger;
          taSpecOutRemn.AsFloat:=0;
          taSpecOutsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
          taSpecOutAVid.AsInteger:=quFCPAVID.AsInteger;
          taSpecOutVol.AsFloat:=quFCPVOL.AsFloat;
          taSpecOut.Post;
          ViewDoc2.EndUpdate;

          AEdit.SelectAll;
          ViewDoc2Name.Options.Editing:=False;
          ViewDoc2Name.Focused:=True;
          Key:=#0;
        end;
        quFCP.Active:=False;
      end;
    end;
  end;
end;


procedure TfmDocSOut.cxButton1Click(Sender: TObject);
begin
  if acSaveDoc.Enabled then acSaveDoc.Execute;
end;

procedure TfmDocSOut.taSpecOutKolChange(Sender: TField);
begin
  if iCol=1 then prCalcValF;
end;

procedure TfmDocSOut.taSpecOutPriceInChange(Sender: TField);
begin
  if iCol=2 then
  begin
    taSpecOutPriceIn0.AsFloat:=taSpecOutPriceIn.AsFloat/(100+taSpecOutNDSProc.AsFloat*Label17.Tag)*100;
    prCalcValF;
  end;
end;

procedure TfmDocSOut.taSpecOutPriceIn0Change(Sender: TField);
begin
  if iCol=3 then
  begin
    taSpecOutPriceIn.AsFloat:=taSpecOutPriceIn0.AsFloat*(100+taSpecOutNDSProc.AsFloat*Label17.Tag)/100;
    prCalcValF;
  end;
end;

procedure TfmDocSOut.taSpecOutPriceRChange(Sender: TField);
begin
  if iCol=4 then
  begin
    taSpecOutPriceR0.AsFloat:=taSpecOutPriceR.AsFloat/(100+taSpecOutNDSProc.AsFloat*Label17.Tag)*100;
    prCalcValF;
  end;
end;

procedure TfmDocSOut.taSpecOutPriceR0Change(Sender: TField);
begin
  if iCol=5 then
  begin
    taSpecOutPriceR.AsFloat:=taSpecOutPriceR0.AsFloat*(100+taSpecOutNDSProc.AsFloat*Label17.Tag)/100;
    prCalcValF;
  end;
end;

procedure TfmDocSOut.taSpecOutSumInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=6 then
  begin
    rQ:=taSpecOutKol.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecOutPriceIn.AsFloat:=taSpecOutSumIn.AsFloat/rQ;
      taSpecOutPriceIn0.AsFloat:=taSpecOutPriceIn.AsFloat/(100+taSpecOutNDSProc.AsFloat*Label17.Tag)*100;
      taSpecOutSumIn0.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceIn0.AsFloat);
    end;
    taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
    taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
  end;
end;

procedure TfmDocSOut.taSpecOutSumIn0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=7 then
  begin
    rQ:=taSpecOutKol.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecOutPriceIn0.AsFloat:=taSpecOutSumIn0.AsFloat/rQ;
      taSpecOutPriceIn.AsFloat:=taSpecOutPriceIn0.AsFloat*(100+taSpecOutNDSProc.AsFloat*Label17.Tag)/100;
      taSpecOutSumIn.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceIn.AsFloat);
    end;
    taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
    taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
  end;
end;

procedure TfmDocSOut.taSpecOutSumRChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=8 then
  begin
    rQ:=taSpecOutKol.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecOutPriceR.AsFloat:=taSpecOutSumR.AsFloat/rQ;
      taSpecOutPriceR0.AsFloat:=taSpecOutPriceR.AsFloat/(100+taSpecOutNDSProc.AsFloat*Label17.Tag)*100;
      taSpecOutSumR0.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceR0.AsFloat);
    end;
    taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
    taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
  end;
end;

procedure TfmDocSOut.taSpecOutSumR0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=9 then
  begin
    rQ:=taSpecOutKol.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecOutPriceR0.AsFloat:=taSpecOutSumR0.AsFloat/rQ;
      taSpecOutPriceR.AsFloat:=taSpecOutPriceR0.AsFloat*(100+taSpecOutNDSProc.AsFloat*Label17.Tag)/100;
      taSpecOutSumR.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutPriceR.AsFloat);
    end;
    taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
    taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
  end;
end;

procedure TfmDocSOut.FormCreate(Sender: TObject);
begin
  GridDoc2.Align:=alClient;
  FormPlacementOut.IniFileName:=sFormIni;
  FormPlacementOut.Active:=True;
  ViewDoc2.RestoreFromIniFile(sGridIni);
end;

procedure TfmDocSOut.cxButton3Click(Sender: TObject);
Var iC,iNDS,i:Integer;
    rS:Array[1..10] of real;
    sStr:String;
    sCli:TArrCli;
begin
  if cxLookupComboBox1.EditValue<1 then exit;
  try
    with dmMCS do
    begin
      //������� ������ - ����� ������ - �.�. ��� ���� - ����������� � ������
      ViewDoc2.BeginUpdate;

      For i:=1 to 10 do rS[i]:=0;

      iC:=0;
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        taSpecOut.Edit;
        taSpecOutSumIn.AsFloat:=rv(taSpecOutSumIn.AsFloat);
        taSpecOutSumIn0.AsFloat:=rv(taSpecOutSumIn0.AsFloat);
        taSpecOutSumR.AsFloat:=rv(taSpecOutSumR.AsFloat);
        taSpecOutSumR0.AsFloat:=rv(taSpecOutSumR0.AsFloat);
        taSpecOutNDSSumIn.AsFloat:=taSpecOutSumIn.AsFloat-taSpecOutSumIn0.AsFloat;
        taSpecOutNDSSumOut.AsFloat:=taSpecOutSumR.AsFloat-taSpecOutSumR0.AsFloat;
        taSpecOut.Post;

        taSpecOut.Next; inc(iC);
      end;
      taSpecOut.First;

      CloseTe(taSpPrint);
      iNDS:=0;

      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        if taSpecOutTovarType.AsInteger=0 then
        begin
          quFCP.Active:=False;
          quFCP.SQL.Clear;
          quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
          quFCP.SQL.Add('where ca.[ID]='+its(taSpecOutCodeTovar.AsInteger));
//          quFCP.SQL.Add('and [TTOVAR]=0');
          quFCP.Active:=True;

          if quFCP.RecordCount=1 then
          begin
            taSpPrint.Append;
            taSpPrintIGr.AsInteger:=1;
            taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
            taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
            taSpPrintFullName.AsString:=quFCPFULLNAME.AsString;
            taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;

            if iNDS=1 then
            begin
              //taSpecOutNDSProc.AsFloat:=10;
              taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
              taSpPrintNDSSum.AsFloat:=taSpecOutNDSSumIn.AsFloat;
              taSpPrintOutNDSSum.AsFloat:=taSpecOutSumIn0.AsFloat;
            end else
            begin
              taSpPrintNDSProc.AsFloat:=0;
              taSpPrintNDSSum.AsFloat:=0;
              taSpPrintOutNDSSum.AsFloat:=taSpecOutSumIn.AsFloat;
            end;

            taSpPrintKolMest.AsFloat:=1;
            taSpPrintKolWithMest.AsFloat:=taSpecOutKol.AsFloat;
            taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;

            taSpPrintCenaTovar.AsFloat:=taSpecOutPriceIn.AsFloat;
            taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumIn.AsFloat;
            taSpPrintCenaMag.AsFloat:=taSpecOutPriceR.AsFloat;
            taSpPrintSumCenaMag.AsFloat:=taSpecOutSumR.AsFloat;

            rS[1]:=rS[1]+taSpecOutSumIn.AsFloat;
            rS[2]:=rS[2]+taSpecOutSumR.AsFloat;

            taSpPrintVesTara.AsFloat:=0;
            taSpPrintBrak.AsFloat:=0;
            taSpPrintKolF.AsFloat:=0;

            taSpPrintPostDate.AsDateTime:=taSpecOutPostDate.AsDateTime;
            taSpPrintPostNum.AsString:=taSpecOutPostNum.AsString;
            taSpPrintPostQuant.AsFloat:=taSpecOutPostQuant.AsFloat;

            taSpPrint.Post;
          end;
        end else
        begin

        end;
        taSpecOut.Next;
      end;

      if cxRadioGroup1.ItemIndex=0 then //���������
      begin
        frRepDOUT.LoadFromFile(CommonSet.PathReport + 'torg12.frf');
        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['RSum']:=rS[1];
        frVariables.Variable['SSum']:=MoneyToString(rS[1],True,False);
        frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

        sStr:=MoneyToString(iC,True,False);
        if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
        frVariables.Variable['SQStr']:=' ('+sStr+')';

        if ((cxComboBox1.Text='2') or (cxComboBox1.Text='3')) then  frVariables.Variable['Osnovanie']:='�������� �������';
        if ((cxComboBox1.Text='0') or (cxComboBox1.Text='1')) then  frVariables.Variable['Osnovanie']:='������� ������';

        frVariables.Variable['Cli1Name']:=quDepartsStFULLNAME.AsString;
        frVariables.Variable['Cli1Inn']:=quDepartsStINN.AsString+' '+quDepartsStKPP.AsString;
        frVariables.Variable['Cli1Adr']:=quDepartsStCITY.AsString+','+quDepartsStPOSTINDEX.AsString+','+quDepartsStSTREET.AsString+','+quDepartsStHOUSE.AsString+' '+quDepartsStKORP.AsString;
        frVariables.Variable['Otpr']:=quDepartsStNAMEOTPR.AsString;
        frVariables.Variable['OtprAdr']:=quDepartsStADROPR.AsString;
        frVariables.Variable['Cli1RSch']:=quDepartsStRSCH.AsString;
        frVariables.Variable['Cli1Bank']:=quDepartsStBANK.AsString;
        frVariables.Variable['Cli1KSch']:=quDepartsStKSCH.AsString;
        frVariables.Variable['Cli1Bik']:=quDepartsStBIK.AsString;

        prFCli(cxButtonEdit1.Tag,sCli);

        frVariables.Variable['Cli2Name']:=sCli[2];
        frVariables.Variable['Cli2Inn']:=sCli[3];
        frVariables.Variable['Cli2Adr']:=sCli[13]+','+sCli[14]+','+sCli[15]+','+sCli[16];
        frVariables.Variable['Poluch']:=sCli[6];
        frVariables.Variable['PoluchAdr']:=sCli[8];

        frRepDOUT.ReportName:='���������.';
        frRepDOUT.PrepareReport;

        if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
        else frRepDOUT.PrintPreparedReport('',1,False,frAll);
      end;
      if cxRadioGroup1.ItemIndex=1 then //����
      begin
      end;
      if cxRadioGroup1.ItemIndex=2 then //��� �������� �����. ������ �������
      begin
        frRepDOUT.LoadFromFile(CommonSet.PathReport + 'ActOutM1.frf');
        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['Cli1Name']:=quDepartsStFULLNAME.AsString;
        frVariables.Variable['Cli1Inn']:=quDepartsStINN.AsString+' '+quDepartsStKPP.AsString;
        frVariables.Variable['Cli1Adr']:=quDepartsStCITY.AsString+','+quDepartsStPOSTINDEX.AsString+','+quDepartsStSTREET.AsString+','+quDepartsStHOUSE.AsString+' '+quDepartsStKORP.AsString;
        frVariables.Variable['Otpr']:=quDepartsStNAMEOTPR.AsString;
        frVariables.Variable['OtprAdr']:=quDepartsStADROPR.AsString;
        frVariables.Variable['OGRN']:='';
        frVariables.Variable['FL']:='';

        prFCli(cxButtonEdit1.Tag,sCli);

        frVariables.Variable['Cli2Name']:=sCli[2];
        frVariables.Variable['Cli2Inn']:=sCli[3];
        frVariables.Variable['Cli2Adr']:=sCli[13]+','+sCli[14]+','+sCli[15]+','+sCli[16];
        frVariables.Variable['Poluch']:=sCli[6];
        frVariables.Variable['PoluchAdr']:=sCli[8];

        frVariables.Variable['SumTovar']:=rS[2];
        frVariables.Variable['SumTovarP']:=rS[1];
        frVariables.Variable['SumTara']:=0;
        frVariables.Variable['SumNDS']:=0;

        frVariables.Variable['SSumTovarP']:=MoneyToString(rS[1],True,False);
        frVariables.Variable['SSumTovar']:=MoneyToString(rS[2],True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(0,True,False);
        frVariables.Variable['SSumNDS']:=MoneyToString(0,True,False);
        frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

        frRepDOUT.ReportName:='��� �� �������.';
        frRepDOUT.PrepareReport;

        if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
        else frRepDOUT.PrintPreparedReport('',1,False,frAll);
      end;
      if cxRadioGroup1.ItemIndex=3 then //��� �������� �����. ������ �������
      begin
        frRepDOUT.LoadFromFile(CommonSet.PathReport + 'ActOut.frf');
        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['Cli1Name']:=quDepartsStFULLNAME.AsString;
        frVariables.Variable['Cli1Inn']:=quDepartsStINN.AsString+' '+quDepartsStKPP.AsString;
        frVariables.Variable['Cli1Adr']:=quDepartsStCITY.AsString+','+quDepartsStPOSTINDEX.AsString+','+quDepartsStSTREET.AsString+','+quDepartsStHOUSE.AsString+' '+quDepartsStKORP.AsString;
        frVariables.Variable['Otpr']:=quDepartsStNAMEOTPR.AsString;
        frVariables.Variable['OtprAdr']:=quDepartsStADROPR.AsString;
        frVariables.Variable['OGRN']:='';
        frVariables.Variable['FL']:='';

        prFCli(cxButtonEdit1.Tag,sCli);

        frVariables.Variable['Cli2Name']:=sCli[2];
        frVariables.Variable['Cli2Inn']:=sCli[3];
        frVariables.Variable['Cli2Adr']:=sCli[13]+','+sCli[14]+','+sCli[15]+','+sCli[16];
        frVariables.Variable['Poluch']:=sCli[6];
        frVariables.Variable['PoluchAdr']:=sCli[8];

        frVariables.Variable['SumTovar']:=rS[1];
        frVariables.Variable['SumTara']:=0;
        frVariables.Variable['SumNDS']:=0;

        frVariables.Variable['SSumTovar']:=MoneyToString(rS[1],True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(0,True,False);
        frVariables.Variable['SSumNDS']:=MoneyToString(0,True,False);
        frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

        frRepDOUT.ReportName:='��� �� �������.';
        frRepDOUT.PrepareReport;

        if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
        else frRepDOUT.PrintPreparedReport('',1,False,frAll);
      end;
      if cxRadioGroup1.ItemIndex=4 then //��� �������� �����. ������ ����������
      begin
        frRepDOUT.LoadFromFile(CommonSet.PathReport + 'ActOutP.frf');
        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['Cli1Name']:=quDepartsStFULLNAME.AsString;
        frVariables.Variable['Cli1Inn']:=quDepartsStINN.AsString+' '+quDepartsStKPP.AsString;
        frVariables.Variable['Cli1Adr']:=quDepartsStCITY.AsString+','+quDepartsStPOSTINDEX.AsString+','+quDepartsStSTREET.AsString+','+quDepartsStHOUSE.AsString+' '+quDepartsStKORP.AsString;
        frVariables.Variable['Otpr']:=quDepartsStNAMEOTPR.AsString;
        frVariables.Variable['OtprAdr']:=quDepartsStADROPR.AsString;
        frVariables.Variable['OGRN']:='';
        frVariables.Variable['FL']:='';

        prFCli(cxButtonEdit1.Tag,sCli);

        frVariables.Variable['Cli2Name']:=sCli[2];
        frVariables.Variable['Cli2Inn']:=sCli[3];
        frVariables.Variable['Cli2Adr']:=sCli[13]+','+sCli[14]+','+sCli[15]+','+sCli[16];
        frVariables.Variable['Poluch']:=sCli[6];
        frVariables.Variable['PoluchAdr']:=sCli[8];

        frVariables.Variable['SumTovar']:=rS[1];
        frVariables.Variable['SumTara']:=0;
        frVariables.Variable['SumNDS']:=0;

        frVariables.Variable['SSumTovar']:=MoneyToString(rS[1],True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(0,True,False);
        frVariables.Variable['SSumNDS']:=MoneyToString(0,True,False);
        frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

        frRepDOUT.ReportName:='��� �� �������.';
        frRepDOUT.PrepareReport;

        if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
        else frRepDOUT.PrintPreparedReport('',1,False,frAll);
      end;

    end; //with
  finally
    taSpPrint.Active:=False;
    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmDocSOut.ViewDoc2SelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewDoc2.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
    Vi1.BeginUpdate;
    quPosts2.Active:=False;
    quPosts2.Parameters.ParamByName('ICODE').Value:=taSpecOutCodeTovar.AsInteger;
    quPosts2.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
    quPosts2.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmDocSOut.Vi1DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    with dmMCS do
    begin
      if quPosts2.RecordCount>0 then
      begin
        if taSpecOut.RecordCount>0 then
        begin
          iCol:=2; //PriceIn
          taSpecOut.Edit;
          taSpecOutPriceIn.AsFloat:=quPosts2PRICEIN.AsFloat;
          //��� ������ � � ��� �����������
//          if quPost2SertNumber.AsString>'' then taSpecOutGTD.AsString:=quPost2SertNumber.AsString else taSpecOutGTD.AsString:='';
          taSpecOut.Post;

          ViewDoc2PRICEIN.Focused:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmDocSOut.cxLabel3Click(Sender: TObject);
begin
  acCalcLastPrice.Execute;
end;

procedure TfmDocSOut.acCalcLastPriceExecute(Sender: TObject);
var rPrIn0,rPrIn,rPrR:Real;
begin
  if cxLookupComboBox1.EditValue<1 then exit;
  if cxButton1.Enabled then
  begin
    ViewDoc2.BeginUpdate;
    iCol:=0;
    Memo1.Lines.Add('��������� � ����� ���������� �������.'); delay(10);
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      if taSpecOutTovarType.AsInteger=0 then
      begin
        with dmMCS do
        begin
          quFCP.Active:=False;
          quFCP.SQL.Clear;
          quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
          quFCP.SQL.Add('where ca.[ID]='+its(taSpecOutCodeTovar.AsInteger));
//          quFCP.SQL.Add('and [TTOVAR]=0');
          quFCP.Active:=True;

          if quFCP.RecordCount=1 then
          begin
            taSpecOut.Edit;
            taSpecOutPriceIn.AsFloat:=prFLP(taSpecOutCodeTovar.AsInteger,cxLookupComboBox1.EditValue,rPrIn0,rPrIn,rPrR);
            taSpecOutPriceIn0.AsFloat:=rPrIn0;
            taSpecOutPriceR.AsFloat:=quFCPPRICE.AsFloat;
            taSpecOutRemn.AsFloat:=prFindRemnDepD(taSpecOutCodeTovar.AsInteger,cxLookupComboBox1.EditValue,Trunc(date));
            prCalcValF;
            taSpecOut.Post;
          end;
        end;
      end;
      taSpecOut.Next;
    end;
    ViewDoc2.EndUpdate;
    Memo1.Lines.Add('������� ��������.'); delay(10);
  end;
end;

procedure TfmDocSOut.cxButton7Click(Sender: TObject);
begin
  //������������
  acOprih.Execute;
end;

procedure TfmDocSOut.acOprihExecute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if cxButton1.Enabled=False then exit;
  if SaveDocsOut(IdH) then
  begin
    with dmMCS do
    begin
      fmDocsOutH.ViewDocsOut.BeginUpdate;
      quTTNOut.Requery();
      quTTNOut.Locate('ID',IDH,[]);
      fmDocsOutH.ViewDocsOut.EndUpdate;

      if quTTnOut.Locate('ID',IDH,[])then
      begin
        if quTTnOutIACTIVE.AsInteger<=1 then
        begin
          if quTTnOutDATEDOC.AsDateTime<prOpenDate(quTTnOutIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

          if fTestInv(quTTnOutIDSKL.AsInteger,quTTnOutIDATEDOC.AsInteger)=False then begin Memo1.Lines.Add('������ ������ (��������������).'); StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

          if OprDocsOut(quTTnOutID.AsInteger,Memo1) then
          begin
            fmDocsOutH.prSetValsAddDoc2(2);

            fmDocsOutH.ViewDocsOut.BeginUpdate;
            quTTNOut.Requery();
            quTTNOut.Locate('ID',IDH,[]);
            fmDocsOutH.ViewDocsOut.EndUpdate;
          end;
        end;
      end;
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocSOut.acMoveExecute(Sender: TObject);
begin
  //�������� ������ - ����� �� ��������� �� �����
  with dmMCS do
  begin
    if taSpecOut.RecordCount>0 then
    begin
      prFormMove(cxLookupComboBox1.EditValue,taSpecOutCodeTovar.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),taSpecOutName.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmDocSOut.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmDocSOut.Showing then
    if fmDocSOut.Tag=0 then cxTextEdit1.Text:=fGetNumDoc(cxLookupComboBox1.EditValue,2);
end;

end.
