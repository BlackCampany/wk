object fmImpD: TfmImpD
  Left = 393
  Top = 140
  Width = 1099
  Height = 669
  Caption = #1048#1084#1087#1086#1088#1090
  Color = 13158600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 216
    Top = 476
    Width = 41
    Height = 13
    Caption = 'SUMIN0'
    FocusControl = DBEdit1
  end
  object cxButton1: TcxButton
    Left = 16
    Top = 12
    Width = 213
    Height = 25
    Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088' '#1090#1086#1074#1072#1088#1072
    TabOrder = 0
    OnClick = cxButton1Click
  end
  object Memo1: TcxMemo
    Left = 8
    Top = 312
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 317
    Width = 1073
  end
  object cxButton2: TcxButton
    Left = 16
    Top = 44
    Width = 213
    Height = 25
    Caption = #1058#1086#1074#1072#1088#1099
    TabOrder = 2
    OnClick = cxButton2Click
  end
  object cxButton3: TcxButton
    Left = 16
    Top = 76
    Width = 213
    Height = 25
    Caption = #1064#1090#1088#1080#1093#1082#1086#1076#1099
    TabOrder = 3
    OnClick = cxButton3Click
  end
  object cxButton4: TcxButton
    Left = 16
    Top = 108
    Width = 213
    Height = 25
    Caption = #1062#1077#1085#1099
    TabOrder = 4
    OnClick = cxButton4Click
  end
  object cxButton5: TcxButton
    Left = 16
    Top = 140
    Width = 213
    Height = 25
    Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1086#1074
    TabOrder = 5
    OnClick = cxButton5Click
  end
  object cxButton6: TcxButton
    Left = 16
    Top = 172
    Width = 213
    Height = 25
    Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
    TabOrder = 6
    OnClick = cxButton6Click
  end
  object cxButton7: TcxButton
    Left = 268
    Top = 12
    Width = 213
    Height = 25
    Caption = #1047#1072#1075#1086#1083#1086#1074#1082#1080' '#1055#1088#1080#1093'.'#1076#1086#1082
    TabOrder = 7
    OnClick = cxButton7Click
  end
  object cxButton8: TcxButton
    Left = 268
    Top = 44
    Width = 213
    Height = 25
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080' '#1087#1088#1080#1093'.'#1076#1086#1082'.'
    TabOrder = 8
    OnClick = cxButton8Click
  end
  object cxButton9: TcxButton
    Left = 268
    Top = 84
    Width = 213
    Height = 25
    Caption = #1047#1072#1075#1086#1083#1086#1074#1082#1080' '#1063#1077#1082#1086#1074
    TabOrder = 9
    OnClick = cxButton9Click
  end
  object cxButton10: TcxButton
    Left = 268
    Top = 116
    Width = 213
    Height = 25
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080' '#1095#1077#1082#1086#1074
    TabOrder = 10
    OnClick = cxButton10Click
  end
  object cxButton11: TcxButton
    Left = 268
    Top = 160
    Width = 213
    Height = 25
    Caption = #1047#1072#1075#1086#1083#1086#1074#1082#1080' '#1056#1072#1089#1093'.'#1076#1086#1082
    TabOrder = 11
    OnClick = cxButton11Click
  end
  object cxButton12: TcxButton
    Left = 268
    Top = 192
    Width = 213
    Height = 25
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080' '#1088#1072#1089#1093'.'#1076#1086#1082'.'
    TabOrder = 12
    OnClick = cxButton12Click
  end
  object cxButton13: TcxButton
    Left = 268
    Top = 236
    Width = 213
    Height = 25
    Caption = #1047#1072#1075#1086#1083#1086#1074#1082#1080' '#1048#1085#1074
    TabOrder = 13
    OnClick = cxButton13Click
  end
  object cxButton14: TcxButton
    Left = 268
    Top = 268
    Width = 213
    Height = 25
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080' '#1048#1085#1074
    TabOrder = 14
    OnClick = cxButton14Click
  end
  object DBEdit1: TDBEdit
    Left = 92
    Top = 268
    Width = 134
    Height = 21
    DataField = 'SUMIN0'
    DataSource = DataSource1
    TabOrder = 15
  end
  object cxButton15: TcxButton
    Left = 496
    Top = 268
    Width = 213
    Height = 25
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080' '#1048#1085#1074' '#1092#1072#1082#1090
    TabOrder = 16
    OnClick = cxButton15Click
  end
  object cxButton16: TcxButton
    Left = 508
    Top = 16
    Width = 213
    Height = 25
    Caption = #1058#1086#1074#1072#1088#1099' ???'
    TabOrder = 17
  end
  object cxButton17: TcxButton
    Left = 20
    Top = 216
    Width = 213
    Height = 25
    Caption = #1062#1077#1085#1099' '#1072#1083#1082#1086
    TabOrder = 18
    OnClick = cxButton17Click
  end
  object OpenDialog1: TOpenDialog
    Left = 444
    Top = 312
  end
  object taClassif: TADOTable
    Connection = msConnection
    TableName = 'CLASSIF'
    Left = 96
    Top = 344
    object taClassifID: TIntegerField
      FieldName = 'ID'
    end
    object taClassifIDPARENT: TIntegerField
      FieldName = 'IDPARENT'
    end
    object taClassifNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object taClassifNAC: TFloatField
      FieldName = 'NAC'
    end
    object taClassifR1: TFloatField
      FieldName = 'R1'
    end
    object taClassifR2: TFloatField
      FieldName = 'R2'
    end
    object taClassifR3: TFloatField
      FieldName = 'R3'
    end
    object taClassifI1: TIntegerField
      FieldName = 'I1'
    end
    object taClassifI2: TIntegerField
      FieldName = 'I2'
    end
    object taClassifI3: TIntegerField
      FieldName = 'I3'
    end
    object taClassifS1: TStringField
      FieldName = 'S1'
      Size = 50
    end
    object taClassifS2: TStringField
      FieldName = 'S2'
      Size = 50
    end
    object taClassifS3: TStringField
      FieldName = 'S3'
      Size = 50
    end
  end
  object taCards: TADOTable
    Connection = msConnection
    TableName = 'CARDS'
    Left = 144
    Top = 320
    object taCardsIDCLASSIF: TIntegerField
      FieldName = 'IDCLASSIF'
    end
    object taCardsID: TIntegerField
      FieldName = 'ID'
    end
    object taCardsNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taCardsFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 200
    end
    object taCardsBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 15
    end
    object taCardsTTOVAR: TSmallintField
      FieldName = 'TTOVAR'
    end
    object taCardsEDIZM: TSmallintField
      FieldName = 'EDIZM'
    end
    object taCardsNDS: TFloatField
      FieldName = 'NDS'
    end
    object taCardsOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object taCardsISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object taCardsPRSISION: TFloatField
      FieldName = 'PRSISION'
    end
    object taCardsMANCAT: TIntegerField
      FieldName = 'MANCAT'
    end
    object taCardsCOUNTRY: TIntegerField
      FieldName = 'COUNTRY'
    end
    object taCardsBRAND: TIntegerField
      FieldName = 'BRAND'
    end
    object taCardsISTENDER: TSmallintField
      FieldName = 'ISTENDER'
    end
    object taCardsISTOP: TSmallintField
      FieldName = 'ISTOP'
    end
    object taCardsISNOV: TSmallintField
      FieldName = 'ISNOV'
    end
    object taCardsCATEG: TIntegerField
      FieldName = 'CATEG'
    end
    object taCardsSROKGODN: TIntegerField
      FieldName = 'SROKGODN'
    end
    object taCardsQUANTPACK: TFloatField
      FieldName = 'QUANTPACK'
    end
    object taCardsVESBUTTON: TIntegerField
      FieldName = 'VESBUTTON'
    end
    object taCardsVESFORMAT: TSmallintField
      FieldName = 'VESFORMAT'
    end
    object taCardsVES: TFloatField
      FieldName = 'VES'
    end
    object taCardsVOL: TFloatField
      FieldName = 'VOL'
    end
    object taCardsWW: TIntegerField
      FieldName = 'WW'
    end
    object taCardsHH: TIntegerField
      FieldName = 'HH'
    end
    object taCardsLL: TIntegerField
      FieldName = 'LL'
    end
    object taCardsKREP: TFloatField
      FieldName = 'KREP'
    end
    object taCardsAVID: TIntegerField
      FieldName = 'AVID'
    end
    object taCardsMAKER: TIntegerField
      FieldName = 'MAKER'
    end
    object taCardsDATECREATE: TIntegerField
      FieldName = 'DATECREATE'
    end
    object taCardsAZKOEF: TFloatField
      FieldName = 'AZKOEF'
    end
    object taCardsAZSTRAHR: TFloatField
      FieldName = 'AZSTRAHR'
    end
    object taCardsAZSROKREAL: TIntegerField
      FieldName = 'AZSROKREAL'
    end
    object taCardsAZDAYSPROC: TIntegerField
      FieldName = 'AZDAYSPROC'
    end
    object taCardsAZQUANTMAX: TFloatField
      FieldName = 'AZQUANTMAX'
    end
    object taCardsRNAC: TFloatField
      FieldName = 'RNAC'
    end
  end
  object taBar: TADOTable
    Connection = msConnection
    TableName = 'BARCODE'
    Left = 200
    Top = 320
    object taBarBar: TStringField
      FieldName = 'Bar'
      Size = 13
    end
    object taBarGoodsId: TIntegerField
      FieldName = 'GoodsId'
    end
    object taBarQuant: TFloatField
      FieldName = 'Quant'
    end
    object taBarBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object taBarBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object taBarCost: TFloatField
      FieldName = 'Cost'
    end
    object taBarStatus: TSmallintField
      FieldName = 'Status'
    end
    object taBarId_Producer: TIntegerField
      FieldName = 'Id_Producer'
    end
  end
  object taCountry: TADOTable
    Connection = msConnection
    TableName = 'COUNTRY'
    Left = 256
    Top = 320
    object taCountryIDPARENT: TIntegerField
      FieldName = 'IDPARENT'
    end
    object taCountryID: TIntegerField
      FieldName = 'ID'
    end
    object taCountryNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object taCountryKOD: TIntegerField
      FieldName = 'KOD'
    end
  end
  object taDeps: TADOTable
    Connection = msConnection
    TableName = 'DEPARTS'
    Left = 124
    Top = 388
    object taDepsIDS: TIntegerField
      FieldName = 'IDS'
    end
    object taDepsID: TIntegerField
      FieldName = 'ID'
    end
    object taDepsNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object taDepsFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 150
    end
    object taDepsISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object taDepsIDORG: TIntegerField
      FieldName = 'IDORG'
    end
    object taDepsGLN: TStringField
      FieldName = 'GLN'
      Size = 15
    end
    object taDepsISS: TSmallintField
      FieldName = 'ISS'
    end
    object taDepsPRIOR: TSmallintField
      FieldName = 'PRIOR'
    end
    object taDepsINN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object taDepsKPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
    object taDepsDIR1: TStringField
      FieldName = 'DIR1'
      Size = 50
    end
    object taDepsDIR2: TStringField
      FieldName = 'DIR2'
      Size = 50
    end
    object taDepsDIR3: TStringField
      FieldName = 'DIR3'
      Size = 50
    end
    object taDepsGB1: TStringField
      FieldName = 'GB1'
      Size = 50
    end
    object taDepsGB2: TStringField
      FieldName = 'GB2'
      Size = 50
    end
    object taDepsGB3: TStringField
      FieldName = 'GB3'
      Size = 50
    end
    object taDepsCITY: TStringField
      FieldName = 'CITY'
      Size = 50
    end
    object taDepsSTREET: TStringField
      FieldName = 'STREET'
      Size = 50
    end
    object taDepsHOUSE: TStringField
      FieldName = 'HOUSE'
      Size = 10
    end
    object taDepsKORP: TStringField
      FieldName = 'KORP'
      Size = 5
    end
    object taDepsPOSTINDEX: TStringField
      FieldName = 'POSTINDEX'
      Size = 10
    end
    object taDepsPHONE: TStringField
      FieldName = 'PHONE'
    end
    object taDepsSERLIC: TStringField
      FieldName = 'SERLIC'
      Size = 50
    end
    object taDepsNUMLIC: TStringField
      FieldName = 'NUMLIC'
      Size = 50
    end
    object taDepsORGAN: TStringField
      FieldName = 'ORGAN'
      Size = 200
    end
    object taDepsDATEB: TDateTimeField
      FieldName = 'DATEB'
    end
    object taDepsDATEE: TDateTimeField
      FieldName = 'DATEE'
    end
    object taDepsIP: TSmallintField
      FieldName = 'IP'
    end
    object taDepsIPREG: TStringField
      FieldName = 'IPREG'
      Size = 200
    end
  end
  object taPrice: TADOTable
    Connection = msConnection
    TableName = 'CARDSPRICE'
    Left = 184
    Top = 388
    object taPriceGOODSID: TIntegerField
      FieldName = 'GOODSID'
    end
    object taPriceIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object taPricePRICE: TFloatField
      FieldName = 'PRICE'
    end
  end
  object taClassifCli: TADOTable
    Connection = msConnection
    TableName = 'CLASSIFCLI'
    Left = 44
    Top = 384
    object taClassifCliID: TIntegerField
      FieldName = 'ID'
    end
    object taClassifCliIDPARENT: TIntegerField
      FieldName = 'IDPARENT'
    end
    object taClassifCliNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
  end
  object taClients: TADOTable
    Connection = msConnection
    TableName = 'CLIENTS'
    Left = 244
    Top = 392
    object taClientsId: TIntegerField
      FieldName = 'Id'
    end
    object taClientsINN: TStringField
      FieldName = 'INN'
    end
    object taClientsName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taClientsFullName: TStringField
      FieldName = 'FullName'
      Size = 200
    end
    object taClientsIType: TSmallintField
      FieldName = 'IType'
    end
    object taClientsIActive: TSmallintField
      FieldName = 'IActive'
    end
    object taClientsPostIndex: TStringField
      FieldName = 'PostIndex'
      FixedChar = True
      Size = 10
    end
    object taClientsGorod: TStringField
      FieldName = 'Gorod'
      FixedChar = True
      Size = 50
    end
    object taClientsStreet: TStringField
      FieldName = 'Street'
      FixedChar = True
      Size = 50
    end
    object taClientsHouse: TStringField
      FieldName = 'House'
      FixedChar = True
      Size = 10
    end
    object taClientsKPP: TStringField
      FieldName = 'KPP'
      FixedChar = True
      Size = 15
    end
    object taClientsNAMEOTP: TStringField
      FieldName = 'NAMEOTP'
      Size = 150
    end
    object taClientsADROTPR: TStringField
      FieldName = 'ADROTPR'
      Size = 150
    end
    object taClientsRSch: TStringField
      FieldName = 'RSch'
    end
    object taClientsKSch: TStringField
      FieldName = 'KSch'
    end
    object taClientsBank: TStringField
      FieldName = 'Bank'
      Size = 150
    end
    object taClientsBik: TStringField
      FieldName = 'Bik'
      Size = 9
    end
    object taClientsDogNum: TStringField
      FieldName = 'DogNum'
      Size = 50
    end
    object taClientsDogDate: TDateTimeField
      FieldName = 'DogDate'
    end
    object taClientsEmail: TStringField
      FieldName = 'Email'
      Size = 200
    end
    object taClientsPhone: TStringField
      FieldName = 'Phone'
      Size = 50
    end
    object taClientsMol: TStringField
      FieldName = 'Mol'
      Size = 100
    end
    object taClientsZakazT: TIntegerField
      FieldName = 'ZakazT'
    end
    object taClientsGln: TStringField
      FieldName = 'Gln'
      Size = 50
    end
    object taClientsPayNDS: TSmallintField
      FieldName = 'PayNDS'
    end
    object taClientsITypeCli: TSmallintField
      FieldName = 'ITypeCli'
    end
    object taClientsEDIProvider: TIntegerField
      FieldName = 'EDIProvider'
    end
    object taClientsMinSumZak: TFloatField
      FieldName = 'MinSumZak'
    end
    object taClientsManyStore: TSmallintField
      FieldName = 'ManyStore'
    end
    object taClientsEmailVoz: TStringField
      FieldName = 'EmailVoz'
      Size = 200
    end
    object taClientsPhoneVoz: TStringField
      FieldName = 'PhoneVoz'
      Size = 50
    end
    object taClientsMolVoz: TStringField
      FieldName = 'MolVoz'
      Size = 100
    end
    object taClientsTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
    object taClientsNoNaclZ: TSmallintField
      FieldName = 'NoNaclZ'
    end
    object taClientsIDPERS: TIntegerField
      FieldName = 'IDPERS'
    end
    object taClientsPretOrg: TStringField
      FieldName = 'PretOrg'
      Size = 200
    end
    object taClientsEmailPret: TStringField
      FieldName = 'EmailPret'
      Size = 200
    end
    object taClientsZakAccess: TSmallintField
      FieldName = 'ZakAccess'
    end
    object taClientsNoSpecif: TSmallintField
      FieldName = 'NoSpecif'
    end
    object taClientsEmailSpecif: TStringField
      FieldName = 'EmailSpecif'
      Size = 200
    end
    object taClientsNoRecadv: TSmallintField
      FieldName = 'NoRecadv'
    end
    object taClientsIdParent: TIntegerField
      FieldName = 'IdParent'
    end
  end
  object taDocHIn: TADOTable
    Connection = msConnection
    TableName = 'DOCIN_HEAD'
    Left = 44
    Top = 456
    object taDocHInIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object taDocHInIDATEDOC: TIntegerField
      FieldName = 'IDATEDOC'
    end
    object taDocHInID: TLargeintField
      FieldName = 'ID'
    end
    object taDocHInDATEDOC: TDateTimeField
      FieldName = 'DATEDOC'
    end
    object taDocHInNUMDOC: TStringField
      FieldName = 'NUMDOC'
      Size = 30
    end
    object taDocHInDATESF: TDateTimeField
      FieldName = 'DATESF'
    end
    object taDocHInNUMSF: TStringField
      FieldName = 'NUMSF'
      Size = 30
    end
    object taDocHInIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object taDocHInSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object taDocHInSUMUCH: TFloatField
      FieldName = 'SUMUCH'
    end
    object taDocHInSUMTAR: TFloatField
      FieldName = 'SUMTAR'
    end
    object taDocHInIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object taDocHInOPRIZN: TSmallintField
      FieldName = 'OPRIZN'
    end
  end
  object taDocSpecIn: TADOTable
    Connection = msConnection
    TableName = 'DOCIN_SPEC'
    Left = 120
    Top = 456
    object taDocSpecInIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
    end
    object taDocSpecInID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object taDocSpecInNUM: TIntegerField
      FieldName = 'NUM'
    end
    object taDocSpecInIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taDocSpecInSBAR: TStringField
      FieldName = 'SBAR'
      Size = 15
    end
    object taDocSpecInIDM: TIntegerField
      FieldName = 'IDM'
    end
    object taDocSpecInQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object taDocSpecInPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object taDocSpecInSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object taDocSpecInPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object taDocSpecInSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object taDocSpecInPRICEUCH: TFloatField
      FieldName = 'PRICEUCH'
    end
    object taDocSpecInSUMUCH: TFloatField
      FieldName = 'SUMUCH'
    end
    object taDocSpecInNDSPROC: TFloatField
      FieldName = 'NDSPROC'
    end
    object taDocSpecInSUMNDS: TFloatField
      FieldName = 'SUMNDS'
    end
    object taDocSpecInGTD: TStringField
      FieldName = 'GTD'
      Size = 50
    end
    object taDocSpecInSNUMHEAD: TStringField
      FieldName = 'SNUMHEAD'
      Size = 30
    end
  end
  object cqHd: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 364
    Top = 396
    object cqHdsNum: TStringField
      FieldName = 'sNum'
      Size = 50
    end
    object cqHdiDep: TIntegerField
      FieldName = 'iDep'
    end
    object cqHdiDate: TIntegerField
      FieldName = 'iDate'
    end
  end
  object taChH: TADOTable
    Connection = msConnection
    TableName = 'cqHeads'
    Left = 236
    Top = 476
    object taChHId: TLargeintField
      FieldName = 'Id'
    end
    object taChHId_Depart: TIntegerField
      FieldName = 'Id_Depart'
    end
    object taChHIDate: TIntegerField
      FieldName = 'IDate'
    end
    object taChHOperation: TSmallintField
      FieldName = 'Operation'
    end
    object taChHDateOperation: TDateTimeField
      FieldName = 'DateOperation'
    end
    object taChHCk_Number: TIntegerField
      FieldName = 'Ck_Number'
    end
    object taChHCassir: TIntegerField
      FieldName = 'Cassir'
    end
    object taChHCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object taChHNSmena: TIntegerField
      FieldName = 'NSmena'
    end
    object taChHCardNumber: TStringField
      FieldName = 'CardNumber'
    end
    object taChHPaymentType: TSmallintField
      FieldName = 'PaymentType'
    end
    object taChHsNum: TStringField
      FieldName = 'sNum'
      Size = 50
    end
  end
  object taChSp: TADOTable
    Connection = msConnection
    TableName = 'cqLines'
    Left = 304
    Top = 476
    object taChSpIdHead: TLargeintField
      FieldName = 'IdHead'
    end
    object taChSpId: TLargeintField
      FieldName = 'Id'
      ReadOnly = True
    end
    object taChSpNum: TWordField
      FieldName = 'Num'
    end
    object taChSpCode: TIntegerField
      FieldName = 'Code'
    end
    object taChSpBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taChSpQuant: TFloatField
      FieldName = 'Quant'
    end
    object taChSpPrice: TFloatField
      FieldName = 'Price'
    end
    object taChSpSumma: TFloatField
      FieldName = 'Summa'
    end
    object taChSpProcessingTime: TFloatField
      FieldName = 'ProcessingTime'
    end
    object taChSpDProc: TFloatField
      FieldName = 'DProc'
    end
    object taChSpDSum: TFloatField
      FieldName = 'DSum'
    end
  end
  object taDocHOut: TADOTable
    Connection = msConnection
    TableName = 'DOCOUT_HEAD'
    Left = 48
    Top = 520
    object taDocHOutIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object taDocHOutIDATEDOC: TIntegerField
      FieldName = 'IDATEDOC'
    end
    object taDocHOutID: TLargeintField
      FieldName = 'ID'
    end
    object taDocHOutDATEDOC: TDateTimeField
      FieldName = 'DATEDOC'
    end
    object taDocHOutNUMDOC: TStringField
      FieldName = 'NUMDOC'
      Size = 30
    end
    object taDocHOutDATESF: TDateTimeField
      FieldName = 'DATESF'
    end
    object taDocHOutNUMSF: TStringField
      FieldName = 'NUMSF'
      Size = 30
    end
    object taDocHOutIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object taDocHOutSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object taDocHOutSUMUCH: TFloatField
      FieldName = 'SUMUCH'
    end
    object taDocHOutSUMTAR: TFloatField
      FieldName = 'SUMTAR'
    end
    object taDocHOutIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object taDocHOutOPRIZN: TSmallintField
      FieldName = 'OPRIZN'
    end
    object taDocHOutSNUMZ: TStringField
      FieldName = 'SNUMZ'
      Size = 50
    end
  end
  object taDocSpecOut: TADOTable
    Connection = msConnection
    TableName = 'DOCOUT_SPEC'
    Left = 120
    Top = 520
    object taDocSpecOutIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
    end
    object taDocSpecOutID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object taDocSpecOutNUM: TIntegerField
      FieldName = 'NUM'
    end
    object taDocSpecOutIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taDocSpecOutSBAR: TStringField
      FieldName = 'SBAR'
      Size = 15
    end
    object taDocSpecOutIDM: TIntegerField
      FieldName = 'IDM'
    end
    object taDocSpecOutQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object taDocSpecOutPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object taDocSpecOutSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object taDocSpecOutPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object taDocSpecOutSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object taDocSpecOutPRICER: TFloatField
      FieldName = 'PRICER'
    end
    object taDocSpecOutSUMR: TFloatField
      FieldName = 'SUMR'
    end
    object taDocSpecOutPRICER0: TFloatField
      FieldName = 'PRICER0'
    end
    object taDocSpecOutSUMR0: TFloatField
      FieldName = 'SUMR0'
    end
    object taDocSpecOutNDSPROC: TFloatField
      FieldName = 'NDSPROC'
    end
    object taDocSpecOutSUMNDSIN: TFloatField
      FieldName = 'SUMNDSIN'
    end
    object taDocSpecOutSUMNDSOUT: TFloatField
      FieldName = 'SUMNDSOUT'
    end
    object taDocSpecOutGTD: TStringField
      FieldName = 'GTD'
      Size = 50
    end
    object taDocSpecOutSNUMHEAD: TStringField
      FieldName = 'SNUMHEAD'
      Size = 30
    end
  end
  object taDocHInv: TADOTable
    Connection = msConnection
    TableName = 'DOCINV_HEAD'
    Left = 420
    Top = 464
    object taDocHInvIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object taDocHInvIDATEDOC: TIntegerField
      FieldName = 'IDATEDOC'
    end
    object taDocHInvDATEDOC: TDateTimeField
      FieldName = 'DATEDOC'
    end
    object taDocHInvNUMDOC: TStringField
      FieldName = 'NUMDOC'
      Size = 30
    end
    object taDocHInvSUMINR: TFloatField
      FieldName = 'SUMINR'
    end
    object taDocHInvSUMINF: TFloatField
      FieldName = 'SUMINF'
    end
    object taDocHInvSUMINRTO: TFloatField
      FieldName = 'SUMINRTO'
    end
    object taDocHInvRSUMR: TFloatField
      FieldName = 'RSUMR'
    end
    object taDocHInvRSUMF: TFloatField
      FieldName = 'RSUMF'
    end
    object taDocHInvRSUMTO: TFloatField
      FieldName = 'RSUMTO'
    end
    object taDocHInvSUMTARR: TFloatField
      FieldName = 'SUMTARR'
    end
    object taDocHInvSUMTARF: TFloatField
      FieldName = 'SUMTARF'
    end
    object taDocHInvIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object taDocHInvIDETAIL: TSmallintField
      FieldName = 'IDETAIL'
    end
    object taDocHInvID: TLargeintField
      FieldName = 'ID'
    end
    object taDocHInvIN_TO: TSmallintField
      FieldName = 'IN_TO'
    end
  end
  object taDocSpecInv: TADOTable
    Connection = msConnection
    IndexFieldNames = 'IDHEAD'
    TableName = 'DOCINV_SPEC'
    Left = 356
    Top = 536
    object taDocSpecInvNUM: TIntegerField
      FieldName = 'NUM'
    end
    object taDocSpecInvIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taDocSpecInvSBAR: TStringField
      FieldName = 'SBAR'
      Size = 15
    end
    object taDocSpecInvQUANTR: TFloatField
      FieldName = 'QUANTR'
    end
    object taDocSpecInvQUANTF: TFloatField
      FieldName = 'QUANTF'
    end
    object taDocSpecInvPRICER: TFloatField
      FieldName = 'PRICER'
    end
    object taDocSpecInvSUMR: TFloatField
      FieldName = 'SUMR'
    end
    object taDocSpecInvSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object taDocSpecInvSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object taDocSpecInvSUMRF: TFloatField
      FieldName = 'SUMRF'
    end
    object taDocSpecInvSUMIN0F: TFloatField
      FieldName = 'SUMIN0F'
    end
    object taDocSpecInvSUMINF: TFloatField
      FieldName = 'SUMINF'
    end
    object taDocSpecInvQUANTC: TFloatField
      FieldName = 'QUANTC'
    end
    object taDocSpecInvSUMC: TFloatField
      FieldName = 'SUMC'
    end
    object taDocSpecInvSUMRSC: TFloatField
      FieldName = 'SUMRSC'
    end
    object taDocSpecInvSumRTO: TFloatField
      FieldName = 'SumRTO'
    end
    object taDocSpecInvIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
    end
    object taDocSpecInvID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
  end
  object quPricePos: TADOQuery
    Connection = msConnection
    Parameters = <
      item
        Name = 'ICODE'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'ISKL'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [GOODSID]'
      '      ,[IDSKL]'
      '      ,[PRICE]'
      '  FROM [dbo].[CARDSPRICE]'
      'where [GOODSID]=:ICODE'
      'and [IDSKL]=:ISKL'
      '')
    Left = 448
    Top = 384
    object quPricePosGOODSID: TIntegerField
      FieldName = 'GOODSID'
    end
    object quPricePosIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object quPricePosPRICE: TFloatField
      FieldName = 'PRICE'
    end
  end
  object msConnection: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 'FILE NAME=C:\_MCrystalS\Bin\MCR.udl'
    ConnectionTimeout = 1800
    DefaultDatabase = 'MCRYSTAL'
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'SQLOLEDB.1'
    Left = 44
    Top = 321
  end
  object quGetId: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'ITN'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'EXECUTE [dbo].[fGetId]  :ITN')
    Left = 556
    Top = 453
    object quGetIdretnum: TIntegerField
      FieldName = 'retnum'
      ReadOnly = True
    end
  end
  object DataSource1: TDataSource
    DataSet = taDocSpecInv
    Left = 296
    Top = 324
  end
  object quPrice: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <
      item
        Name = 'ISKL'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [GOODSID]'
      '      ,[IDSKL]'
      '      ,[PRICE]'
      '  FROM [dbo].[CARDSPRICE]'
      'where [IDSKL]=:ISKL')
    Left = 240
    Top = 544
    object quPriceGOODSID: TIntegerField
      FieldName = 'GOODSID'
    end
    object quPriceIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object quPricePRICE: TFloatField
      FieldName = 'PRICE'
    end
  end
  object quDocSpecInv: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <
      item
        Name = 'IDH'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [IDHEAD]'
      '      ,[ID]'
      '      ,[NUM]'
      '      ,[IDCARD]'
      '      ,[SBAR]'
      '      ,[QUANTR]'
      '      ,[QUANTF]'
      '      ,[PRICER]'
      '      ,[SUMR]'
      '      ,[SUMIN0]'
      '      ,[SUMIN]'
      '      ,[SUMRF]'
      '      ,[SUMIN0F]'
      '      ,[SUMINF]'
      '      ,[QUANTC]'
      '      ,[SUMC]'
      '      ,[SUMRSC]'
      '      ,[SumRTO]'
      '  FROM [dbo].[DOCINV_SPEC]'
      'where [IDHEAD]=:IDH')
    Left = 492
    Top = 532
    object quDocSpecInvNUM: TIntegerField
      FieldName = 'NUM'
    end
    object quDocSpecInvIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object quDocSpecInvSBAR: TStringField
      FieldName = 'SBAR'
      Size = 15
    end
    object quDocSpecInvQUANTR: TFloatField
      FieldName = 'QUANTR'
    end
    object quDocSpecInvQUANTF: TFloatField
      FieldName = 'QUANTF'
    end
    object quDocSpecInvPRICER: TFloatField
      FieldName = 'PRICER'
    end
    object quDocSpecInvSUMR: TFloatField
      FieldName = 'SUMR'
    end
    object quDocSpecInvSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object quDocSpecInvSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quDocSpecInvSUMRF: TFloatField
      FieldName = 'SUMRF'
    end
    object quDocSpecInvSUMIN0F: TFloatField
      FieldName = 'SUMIN0F'
    end
    object quDocSpecInvSUMINF: TFloatField
      FieldName = 'SUMINF'
    end
    object quDocSpecInvQUANTC: TFloatField
      FieldName = 'QUANTC'
    end
    object quDocSpecInvSUMC: TFloatField
      FieldName = 'SUMC'
    end
    object quDocSpecInvSUMRSC: TFloatField
      FieldName = 'SUMRSC'
    end
    object quDocSpecInvSumRTO: TFloatField
      FieldName = 'SumRTO'
    end
    object quDocSpecInvIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
    end
    object quDocSpecInvID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
  end
end
