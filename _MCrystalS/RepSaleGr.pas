unit RepSaleGr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar;

type
  TfmRepSaleGr = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRepSaleGr: TcxGrid;
    ViewRepSaleGr: TcxGridDBTableView;
    LevelRepSaleGr: TcxGridLevel;
    Panel5: TPanel;
    Memo1: TcxMemo;
    fmplRepSaleGr: TFormPlacement;
    amRepSaleGr: TActionManager;
    acMoveRep1: TAction;
    acAdvRep: TAction;
    SpeedItem7: TSpeedItem;
    ViewRepSaleGrISKL: TcxGridDBColumn;
    ViewRepSaleGrNAME: TcxGridDBColumn;
    ViewRepSaleGrIGR2: TcxGridDBColumn;
    ViewRepSaleGrNAMEGR: TcxGridDBColumn;
    ViewRepSaleGrIGR: TcxGridDBColumn;
    ViewRepSaleGrNAMESGR: TcxGridDBColumn;
    ViewRepSaleGrREALQUANT: TcxGridDBColumn;
    ViewRepSaleGrREALSUM: TcxGridDBColumn;
    ViewRepSaleGrPROCQUANT: TcxGridDBColumn;
    ViewRepSaleGrPROCSUM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepSaleGr: TfmRepSaleGr;

implementation

uses Un1, Dm, MainMC, Move, SummaryRDoc;

{$R *.dfm}

procedure TfmRepSaleGr.FormCreate(Sender: TObject);
begin
  fmplRepSaleGr.IniFileName:=sFormIni;
  fmplRepSaleGr.Active:=True; delay(10);

  GridRepSaleGr.Align:=AlClient;
end;

procedure TfmRepSaleGr.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepSaleGr.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel7(ViewRepSaleGr);
end;

end.
