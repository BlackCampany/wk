object fmDocsVnS: TfmDocsVnS
  Left = 661
  Top = 271
  Width = 959
  Height = 675
  Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 943
    Height = 89
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label5: TLabel
      Left = 20
      Top = 60
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label2: TLabel
      Left = 360
      Top = 60
      Width = 70
      Height = 13
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 10
      Properties.ReadOnly = True
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxButton3: TcxButton
      Left = 656
      Top = 8
      Width = 77
      Height = 37
      Caption = 'F7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        160B0000424D160B00000000000036040000280000002C000000280000000100
        080000000000E006000000000000000000000001000000000000E3E1DF00CBC8
        C300FDFDFD00ECEAE500E2DFDD00FAF9F600F5F5F50056B62700A39971008279
        5500736A4A00E5E4E200BBB9B500D3CDB900BAB4AB00BBB39500C0B89C00C8C2
        A9001961EF00C4BDA300F2F1EC00C6752200F8F8F800F0EFED00F7D4AB001649
        A900E8A76600B6AD8C0097928E00F4F4F300F1F1F000FCC88700A2E9AC0069BD
        9F00DCD9D600D0CCC800EBEAEA00F3B97900DEDCD800FEE2B600C1BBB300EEED
        EC00A49D8D00B1ADA800ADA9A300AAA49F008C878400B3A8990089845A00D3D0
        CC00D8D6D300D68F4A0075716F00FFF3D400CAC4BE00D5D3D000DAD5C300FDFD
        FA00E1B68900CA966D00DDD9CA00968A6200C4C1BD00E8E6E400E0DDCF00AFA5
        8400B4590400EEECE900A9A07A00948E89009A947C009C989600BD670C00CFCA
        B500ECEAE800FFD79D00FFFBF4009D936700FBFBFA00D4BDA900FFEAC400DAE9
        9800CCC6AF00F4DBD100EEB37300F3F3F100FFFFED00DAD4CF00E7E4DA00E8C7
        9D00E3E0D400D1823800A3857600918B6E00BA713600FEF4E600E29C5900D7D2
        C0008F531B00D6CFBD00F7F6F500FEEDE00082C8CE00FFD08E00837F7C006861
        4300F9F9F900FFFBE000EBE9DF0097B88B00E2A468006D67C500E7E5F600C5C3
        E300B394A00099BF2C00FFF4EA00DECFD800EDAE6E00EFBCA600FDFAFA00F7C0
        80004537EA00AECD4C00AC7F5100E9E9E900FFFFFF00FAFAFA00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242622242424242424242424242424242424242424242424242424242424
        2424242424242424242424242422372301363624242424242424242424242424
        24242424242424242424242424242424242424242424243231012C470E0E0E28
        2824242424242424242424242424242424242424242424242424242426573123
        242301360E2E682E682E2E2E2C0E282424242424242424242424242424242424
        242424242424225723363E2828282B2E6847524F1C1C1C45681C2B0E24242424
        24242424242424242424242424242424223723284534342A2C452E1C3E4C7E7E
        502F1C1C1C45682D282424242424242424242424242424242424223723284534
        2E2D47682E1C2A537E7E7E207E560D2A1C1C1C1C0E3624242424242424242424
        242424242657232845342C240B042D1C453B537E4C7E212021207E35101C1C2E
        2C36572424242424242424242424030437282E683E7E7E7E06042C3D6E7E653A
        4F19211D200707517E272F1C1C23223F242424242424242429290B012E2E2339
        7E7E7E7E1E042B4F56335E5C19192166660707077B7E56592E0C004324242424
        2424242455001C45227F7E7E7E7E7E7E17040C3B424219191212121207070773
        51397E4F2B004A2424242424242424241E472D7E7F4E7F7F7F7F7F7F430B3E2F
        4262191912121212757E7E7E7E5F0E3100432424242424242424242464687F06
        061D1D1D1D1D1D55433F23282F5E62191919127277657E7E2C2C3E0003242424
        2424242424242424641C1E1E1717171717171717294A32013E0C444862625E3B
        747E531C0E363224242424242424242424242424642B4A4A4A4A4A4A4A4A4A4A
        1717003231010C0E7C423A7E185C1C340E222424242424242424242424242424
        642B3F3F3F0B0B0B0B0B0B4A5555173F2637013E0C2C497C0A341C340E242424
        242424242424242424242424642B00000000000404040455647E7E385A003231
        360C0E2C2E341C3428242424242424242424242424242424162B040404040404
        04044A02027E59424233402637013E0C2B2C4734282424242424242424242424
        242424246A2E040404040404043F7E7E7E5F15484242423A003231360C0E2C34
        282424242424242424242424242424247F2E370404040404067E7E7E7E6E5B15
        4848424215612637013E0C3428242424242424242424242424242424163E0C29
        1717787E7E7E3049181A60335B15484242423B04322336453624242424242424
        2424242424242424241647327E7E7E706F3F6D3A1F257660335B154848424248
        4F26376801242424242424242424242424242424247F3E3E0C370B717A722579
        791F1F541A60335B15484842154A014731242424242424242424242424242424
        244E2B372331106E6E1A765425791F1F797660335B1515483A05470E22242424
        24242424242424242424242424022B0157274B1F76601A76762579791F1F251A
        60335B5B7E5568570024242424242424242424242424242424241E0E0D7E2767
        671F1A6E1A765425791F1F79541A605F7E473604242424242424242424242424
        242424242424241D0D7E2767676767791A1A1A765425791F1F795F7E232C0424
        2424242424242424242424242424242424242464637E504B4B4B676767251A1A
        76542579675F7E012B0024242424242424242424242424242424242424242416
        637E35505027274B4B4B67546E1A541843432B0E3F2424242424242424242424
        24242424242424242424247F637E6B566B3535505027271B344122040C472229
        242424242424242424242424242424242424242424242439287E7E397E395656
        6B35352F2E2C2D2B322924242424242424242424242424242424242424242424
        24242402320F567E4C7E7E7E7E394C0E45365700242424242424242424242424
        24242424242424242424242424242424024E01137E7E397E7E7E7E0C2D312424
        242424242424242424242424242424242424242424242424242424242424391E
        0E3C7E7E397E7E0C0E2224242424242424242424242424242424242424242424
        242424242424242424242424243F2F657E39390C3E0B24242424242424242424
        2424242424242424242424242424242424242424242424242424243113397E5C
        2217242424242424242424242424242424242424242424242424242424242424
        2424242424242424242828221D64242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424}
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox1: TcxCheckBox
      Left = 452
      Top = 12
      Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
      State = cbsChecked
      TabOrder = 3
      Width = 193
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 56
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmMCS.dsquDepartsSt1
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Width = 213
    end
    object cxTextEdit2: TcxTextEdit
      Left = 440
      Top = 56
      Style.BorderStyle = ebsOffice11
      TabOrder = 5
      Text = 'cxTextEdit2'
      Width = 385
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 89
    Width = 153
    Height = 384
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 7
      Top = 124
      Cursor = crHandPoint
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1094#1077#1085#1099', '#1086#1089#1090#1072#1090#1082#1080' '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 618
    Width = 943
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GridDoc4: TcxGrid
    Left = 163
    Top = 96
    Width = 770
    Height = 373
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
    object ViewDoc4: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnEditing = ViewDoc4Editing
      OnSelectionChanged = ViewDoc4SelectionChanged
      DataController.DataSource = dstaSpecVn
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumD'
          Column = ViewDoc4SumD
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn'
          Column = ViewDoc4SumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn0'
          Column = ViewDoc4SumIn0
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumR'
          Column = ViewDoc4SumR
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumD'
          Column = ViewDoc4SumD
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn'
          Column = ViewDoc4SumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn0'
          Column = ViewDoc4SumIn0
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumR'
          Column = ViewDoc4SumR
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      Styles.Footer = dmMCS.cxStyle5
      object ViewDoc4Num: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Num'
        Options.Editing = False
        Width = 42
      end
      object ViewDoc4CodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
      end
      object ViewDoc4Name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 206
      end
      object ViewDoc4BarCode: TcxGridDBColumn
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        DataBinding.FieldName = 'BarCode'
        Options.Editing = False
      end
      object ViewDoc4EdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
        Options.Editing = False
      end
      object ViewDoc4Price: TcxGridDBColumn
        Caption = #1058#1077#1082#1091#1097#1072#1103' '#1094#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'Price'
        Options.Editing = False
        Styles.Content = dmMCS.cxStyle5
      end
      object ViewDoc4Quant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
        Styles.Content = dmMCS.cxStyle5
      end
      object ViewDoc4PriceR: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'PriceR'
      end
      object ViewDoc4PriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewDoc4PriceIn0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PriceIn0'
        Visible = False
        Options.Editing = False
      end
      object ViewDoc4SumR: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumR'
      end
      object ViewDoc4SumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn'
      end
      object ViewDoc4SumIn0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn0'
        Visible = False
        Options.Editing = False
      end
      object ViewDoc4SumD: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'SumD'
        Options.Editing = False
      end
      object ViewDoc4QuantRemn: TcxGridDBColumn
        Caption = #1058#1077#1082#1091#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'QuantRemn'
        Options.Editing = False
        Styles.Content = dmMCS.cxStyle1
      end
    end
    object LevelDoc4: TcxGridLevel
      GridView = ViewDoc4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 575
    Width = 943
    Height = 43
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 4
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 121
      Height = 29
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 212
      Top = 8
      Width = 137
      Height = 29
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 400
      Top = 4
      Width = 45
      Height = 37
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 473
    Width = 943
    Height = 102
    Align = alBottom
    BevelInner = bvSpace
    BevelOuter = bvLowered
    Color = clBtnHighlight
    TabOrder = 5
    object Memo1: TcxMemo
      Left = 2
      Top = 2
      Align = alLeft
      Lines.Strings = (
        'Memo1')
      Properties.ScrollBars = ssVertical
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Height = 98
      Width = 319
    end
    object Gr1: TcxGrid
      Left = 469
      Top = 2
      Width = 472
      Height = 98
      Align = alRight
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      object Vi1: TcxGridDBTableView
        OnDblClick = Vi1DblClick
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMCS.dsquPosts2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object Vi1DATEDOC: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'DATEDOC'
          Width = 40
        end
        object Vi1Name: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'Name'
          Width = 140
        end
        object Vi1QUANT: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'QUANT'
          Width = 49
        end
        object Vi1PRICEIN: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'PRICEIN'
          Width = 46
        end
        object Vi1PROCN: TcxGridDBColumn
          Caption = '% '#1085#1072#1094'.'
          DataBinding.FieldName = 'PROCN'
          Visible = False
          Width = 45
        end
        object Vi1PRICEUCH: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
          DataBinding.FieldName = 'PRICEUCH'
          Width = 52
        end
        object Vi1SUMIN: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'SUMIN'
          Width = 54
        end
        object Vi1SUMUCH: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
          DataBinding.FieldName = 'SUMUCH'
          Width = 54
        end
        object Vi1NUMDOC: TcxGridDBColumn
          Caption = #8470' '#1076#1086#1082'.'
          DataBinding.FieldName = 'NUMDOC'
          Width = 65
        end
      end
      object ViCenn: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMCS.dstaCen
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViCennRecId: TcxGridDBColumn
          Caption = #8470' '#1087#1087
          DataBinding.FieldName = 'RecId'
          Visible = False
        end
        object ViCennIdCard: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'IdCard'
          Width = 53
        end
        object ViCennFullName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'FullName'
          Width = 145
        end
        object ViCennEdIzm: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084
          DataBinding.FieldName = 'EdIzm'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1096#1090'.'
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1082#1075'.'
              Value = 2
            end>
          Width = 46
        end
        object ViCennPrice1: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'Price1'
        end
        object ViCennCountry: TcxGridDBColumn
          Caption = #1057#1090#1088#1072#1085#1072
          DataBinding.FieldName = 'Country'
          Width = 84
        end
        object ViCennBarCode: TcxGridDBColumn
          Caption = #1064#1090#1088#1080#1093#1082#1086#1076
          DataBinding.FieldName = 'BarCode'
          Width = 53
        end
        object ViCennQuant: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Quant'
        end
        object ViCennQuanrR: TcxGridDBColumn
          Caption = #1054#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'QuanrR'
        end
      end
      object le1: TcxGridLevel
        GridView = Vi1
      end
      object Le2: TcxGridLevel
        GridView = ViCenn
        Visible = False
      end
    end
  end
  object taSpecVn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 380
    Top = 240
    object taSpecVnNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecVnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpecVnName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taSpecVnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taSpecVnEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object taSpecVnPrice: TFloatField
      FieldName = 'Price'
      DisplayFormat = '0.00'
    end
    object taSpecVnQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecVnQuantChange
      DisplayFormat = '0.000'
    end
    object taSpecVnPriceR: TFloatField
      FieldName = 'PriceR'
      OnChange = taSpecVnPriceRChange
      DisplayFormat = '0.00'
    end
    object taSpecVnPriceIn: TFloatField
      FieldName = 'PriceIn'
      OnChange = taSpecVnPriceInChange
      DisplayFormat = '0.00'
    end
    object taSpecVnPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      OnChange = taSpecVnPriceIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecVnSumR: TFloatField
      FieldName = 'SumR'
      OnChange = taSpecVnSumRChange
      DisplayFormat = '0.00'
    end
    object taSpecVnSumIn: TFloatField
      FieldName = 'SumIn'
      OnChange = taSpecVnSumInChange
      DisplayFormat = '0.00'
    end
    object taSpecVnSumIn0: TFloatField
      FieldName = 'SumIn0'
      OnChange = taSpecVnSumIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecVnSumD: TFloatField
      FieldName = 'SumD'
      DisplayFormat = '0.00'
    end
    object taSpecVnQuantRemn: TFloatField
      FieldName = 'QuantRemn'
      DisplayFormat = '0.000'
    end
  end
  object dstaSpecVn: TDataSource
    DataSet = taSpecVn
    Left = 380
    Top = 292
  end
  object amAddDocVn: TActionManager
    Left = 508
    Top = 172
    StyleName = 'XP Style'
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acSetPriceRemn: TAction
      Caption = 'acSetPriceRemn'
      OnExecute = acSetPriceRemnExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      OnExecute = acDelAllExecute
    end
  end
  object FormPlacementVn: TFormPlacement
    Active = False
    Left = 240
    Top = 172
  end
  object frRepDVn: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit, pbPageSetup]
    Title = #1055#1077#1095#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    RebuildPrinter = False
    Left = 196
    Top = 348
    ReportForm = {19000000}
  end
  object frtaSpecVn: TfrDBDataSet
    DataSet = taSpecVn
    Left = 260
    Top = 348
  end
end
