unit Bar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls,
  SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxContainer, cxTextEdit, cxDataStorage,
  cxImageComboBox, ADODB;

type
  TfmBar = class(TForm)
    FormPlacementBar: TFormPlacement;
    StatusBar1: TStatusBar;
    amBar: TActionManager;
    acExit: TAction;
    SpeedBar1: TSpeedBar;
    acAddBar: TAction;
    acDelBar: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    BarView: TcxGridDBTableView;
    BarLevel: TcxGridLevel;
    BarGr: TcxGrid;
    BarViewBar: TcxGridDBColumn;
    BarViewGoodsId: TcxGridDBColumn;
    BarViewQuant: TcxGridDBColumn;
    BarViewBarFormat: TcxGridDBColumn;
    BarViewBarStatus: TcxGridDBColumn;
    BarViewCost: TcxGridDBColumn;
    BarViewStatus: TcxGridDBColumn;
    Panel1: TPanel;
    acAddBarCode: TAction;
    TextEdit1: TcxTextEdit;
    acCancBar: TAction;
    acMakeMain: TAction;
    SpeedItem4: TSpeedItem;
    Timer2: TTimer;
    quBarsEdit: TADOQuery;
    quBarsEditBar: TStringField;
    quBarsEditGoodsId: TIntegerField;
    quBarsEditQuant: TFloatField;
    quBarsEditBarFormat: TSmallintField;
    quBarsEditBarStatus: TSmallintField;
    quBarsEditCost: TFloatField;
    quBarsEditStatus: TSmallintField;
    quBarsEditId_Producer: TIntegerField;
    dsquBars: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddBarExecute(Sender: TObject);
    procedure acDelBarExecute(Sender: TObject);
    procedure TextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acAddBarCodeExecute(Sender: TObject);
    procedure acCancBarExecute(Sender: TObject);
    procedure acMakeMainExecute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBar: TfmBar;

implementation

uses Un1, Dm, Cards, MainMC;

{$R *.dfm}

procedure TfmBar.FormCreate(Sender: TObject);
begin
  FormPlacementBar.IniFileName := sFormIni;
  FormPlacementBar.Active:=True;
end;

procedure TfmBar.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmBar.acAddBarExecute(Sender: TObject);
begin
  if not CanDo('prAddBarcode') then begin Showmessage('��� ����.'); exit; end;

  TextEdit1.Visible:=True;
  TextEdit1.SetFocus;
end;

procedure TfmBar.acDelBarExecute(Sender: TObject);
Var sBar:String;
begin
//�������
  if not CanDo('prDelBarcode') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    if quBarsEdit.RecordCount=0 then
    begin
      showmessage('������� ������.');
      exit;
    end;
    if MessageDlg('�� ������������� ������ ������� �������� "'+quBarsEditBar.AsString+'" �� ����.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      sBar:=quBarsEditBar.AsString;

      if quCardsBARCODE.AsString<>sBar then
      begin
        quBarsEdit.delete;
      end else
      begin
        showmessage('������ �� �������� �������� � �������� - �������� ����������.');
      end;
    end;
  end;
end;

procedure TfmBar.TextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var bCh:Byte;
    iC:INteger;
begin
  bCh:=ord(Key);

  if bCh = 13 then
  begin
    with dmMCS do
    begin
      if TextEdit1.Text='' then
      begin
        delay(10);
        TextEdit1.Text:='';
        TextEdit1.Visible:=False;
        exit;
      end;

      if not prFindBar(Trim(TextEdit1.Text),-1,iC) then  //�.�. ��� ��� ���
      begin
        if not CanDo('prAddBarcode') then
        begin
          Showmessage('��� ����.');
          TextEdit1.Text:='';
          TextEdit1.Visible:=False;
          exit;
        end;

        //���������
        bAddB:=True;
        quBarsEdit.append;
        quBarsEditBar.AsString:=Trim(TextEdit1.Text);
        quBarsEditGoodsId.AsInteger:=quCardsId.AsInteger;
        quBarsEditQuant.AsFloat:=1;
        quBarsEditBarFormat.AsInteger:=0;
        quBarsEditBarStatus.AsInteger:=0;
        quBarsEditCost.AsFloat:=0;
        quBarsEditStatus.AsInteger:=0;
        quBarsEditId_Producer.AsInteger:=0;
        quBarsEdit.Post;

        quBarsEdit.Locate('Bar',trim(TextEdit1.Text),[]);
      end
      else
      begin
        showmessage('�������� "'+TextEdit1.Text+'" ��� ���� � ��c���� ('+INtToStr(iC)+').');
      end;

      delay(10);
      TextEdit1.Text:='';
      TextEdit1.Visible:=False;
    end;
  end;
end;

procedure TfmBar.acAddBarCodeExecute(Sender: TObject);
begin
  if not CanDo('prAddBarcode') then begin Showmessage('��� ����.'); exit; end;

  TextEdit1.Visible:=True;
  TextEdit1.SetFocus;
  TextEdit1.SelectAll;
end;

procedure TfmBar.acCancBarExecute(Sender: TObject);
begin
  TextEdit1.Text:='';
  TextEdit1.Visible:=False;
end;

procedure TfmBar.acMakeMainExecute(Sender: TObject);
Var iC:Integer;
begin
  if not CanDo('prSetMainBarcode') then begin Showmessage('��� ����.'); exit; end;

//������� ��������
  with dmMCS do
  begin
    if quBarsEdit.RecordCount=0  then exit;

    if MessageDlg('�� ������������� ������ ������� �� '+quBarsEditBar.AsString+' ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update [dbo].[CARDS] Set ');
      quE.SQL.Add('BARCODE='''+quBarsEditBar.AsString+'''');
      quE.SQL.Add('where ID='+its(quCardsID.AsInteger));
      quE.ExecSQL;

      iC:=quCardsID.AsInteger;
      try
        fmCards.CardsView.BeginUpdate;
        prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),fmMainMC.Label3.tag,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
        quCards.Locate('ID',iC,[]);
      finally
        fmCards.CardsView.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmBar.Timer2Timer(Sender: TObject);
begin
  if bClearStatusBar=True then begin StatusBar1.Panels[0].Text:=''; Delay(10); end;
  if StatusBar1.Panels[0].Text>'' then bClearStatusBar:=True;
end;

end.
