unit AddDoc1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxCheckBox,
  FR_Class, FR_DSet, FR_DBSet, cxCalc, cxRadioGroup, cxGroupBox, dxmdaset,
  cxMemo;

type
  TfmAddDoc1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpecIn2: TClientDataSet;
    dstaSpecIn: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    amDocIn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    taSpecIn2CodeTovar: TIntegerField;
    taSpecIn2CodeEdIzm: TSmallintField;
    taSpecIn2BarCode: TStringField;
    taSpecIn2NDSProc: TFloatField;
    taSpecIn2NDSSum: TFloatField;
    taSpecIn2OutNDSSum: TFloatField;
    taSpecIn2BestBefore: TDateField;
    taSpecIn2KolMest: TIntegerField;
    taSpecIn2KolEdMest: TFloatField;
    taSpecIn2KolWithMest: TFloatField;
    taSpecIn2KolWithNecond: TFloatField;
    taSpecIn2Kol: TFloatField;
    taSpecIn2CenaTovar: TFloatField;
    taSpecIn2Procent: TFloatField;
    taSpecIn2NewCenaTovar: TFloatField;
    taSpecIn2SumCenaTovarPost: TFloatField;
    taSpecIn2SumCenaTovar: TFloatField;
    taSpecIn2ProcentN: TFloatField;
    taSpecIn2Necond: TFloatField;
    taSpecIn2CenaNecond: TFloatField;
    taSpecIn2SumNecond: TFloatField;
    taSpecIn2ProcentZ: TFloatField;
    taSpecIn2Zemlia: TFloatField;
    taSpecIn2ProcentO: TFloatField;
    taSpecIn2Othodi: TFloatField;
    taSpecIn2Name: TStringField;
    taSpecIn2Num: TIntegerField;
    taSpecIn2CodeTara: TIntegerField;
    taSpecIn2VesTara: TFloatField;
    taSpecIn2CenaTara: TFloatField;
    taSpecIn2SumVesTara: TFloatField;
    taSpecIn2SumCenaTara: TFloatField;
    taSpecIn2NameTara: TStringField;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    Label9: TLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    Label10: TLabel;
    cxCurrencyEdit5: TcxCurrencyEdit;
    Label11: TLabel;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxComboBox1: TcxComboBox;
    Label13: TLabel;
    taSpecIn2TovarType: TIntegerField;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc1: TcxGrid;
    ViewDoc1: TcxGridDBTableView;
    ViewDoc1Num: TcxGridDBColumn;
    ViewDoc1CodeTovar: TcxGridDBColumn;
    ViewDoc1Name: TcxGridDBColumn;
    ViewDoc1CodeEdIzm: TcxGridDBColumn;
    ViewDoc1BarCode: TcxGridDBColumn;
    ViewDoc1NDSProc: TcxGridDBColumn;
    ViewDoc1NDSSum: TcxGridDBColumn;
    ViewDoc1OutNDSSum: TcxGridDBColumn;
    ViewDoc1KolMest: TcxGridDBColumn;
    ViewDoc1KolWithMest: TcxGridDBColumn;
    ViewDoc1KolWithNecond: TcxGridDBColumn;
    ViewDoc1Kol: TcxGridDBColumn;
    ViewDoc1CenaTovar: TcxGridDBColumn;
    ViewDoc1Procent: TcxGridDBColumn;
    ViewDoc1NewCenaTovar: TcxGridDBColumn;
    ViewDoc1SumCenaTovarPost: TcxGridDBColumn;
    ViewDoc1SumCenaTovar: TcxGridDBColumn;
    ViewDoc1ProcentN: TcxGridDBColumn;
    ViewDoc1Necond: TcxGridDBColumn;
    ViewDoc1CenaNecond: TcxGridDBColumn;
    ViewDoc1SumNecond: TcxGridDBColumn;
    ViewDoc1ProcentZ: TcxGridDBColumn;
    ViewDoc1Zemlia: TcxGridDBColumn;
    ViewDoc1ProcentO: TcxGridDBColumn;
    ViewDoc1Othodi: TcxGridDBColumn;
    ViewDoc1BestBefore: TcxGridDBColumn;
    ViewDoc1CodeTara: TcxGridDBColumn;
    ViewDoc1NameTara: TcxGridDBColumn;
    ViewDoc1VesTara: TcxGridDBColumn;
    ViewDoc1CenaTara: TcxGridDBColumn;
    ViewDoc1SumVesTara: TcxGridDBColumn;
    ViewDoc1SumCenaTara: TcxGridDBColumn;
    LevelDoc1: TcxGridLevel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label6: TLabel;
    taSpecIn2RealPrice: TFloatField;
    ViewDoc1RealPrice: TcxGridDBColumn;
    acSetPrice: TAction;
    cxCheckBox1: TcxCheckBox;
    frRepDIN: TfrReport;
    frtaSpecIn: TfrDBDataSet;
    acPrintDoc: TAction;
    Panel4: TPanel;
    cxLabel7: TcxLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxLabel9: TcxLabel;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    cxRadioButton4: TcxRadioButton;
    cxLabel8: TcxLabel;
    acCalcPrice: TAction;
    cxCheckBox2: TcxCheckBox;
    acPrintCen: TAction;
    taSpecIn2Remn: TFloatField;
    ViewDoc1Remn: TcxGridDBColumn;
    acPostav: TAction;
    cxButton3: TcxButton;
    cxRadioGroup1: TcxRadioGroup;
    taSp: TdxMemData;
    frtaSp: TfrDBDataSet;
    taSpiNum: TIntegerField;
    taSpBarcode: TStringField;
    taSpiCode: TIntegerField;
    taSpEdIzm: TIntegerField;
    taSpName: TStringField;
    taSpQuant: TFloatField;
    taSpPriceP: TFloatField;
    taSpPriceM: TFloatField;
    taSpScaleNum: TStringField;
    acTermoP: TAction;
    cxLabel10: TcxLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    PopupMenu1: TPopupMenu;
    acScaleView: TAction;
    N1: TMenuItem;
    acAddToScale: TAction;
    N2: TMenuItem;
    cxButton6: TcxButton;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    cxCalcEdit5: TcxCalcEdit;
    cxCalcEdit6: TcxCalcEdit;
    cxCalcEdit7: TcxCalcEdit;
    cxLabel11: TcxLabel;
    acSetRemn: TAction;
    taSpecIn2Nac1: TFloatField;
    taSpecIn2Nac2: TFloatField;
    taSpecIn2Nac3: TFloatField;
    ViewDoc1Nac1: TcxGridDBColumn;
    taSpecIn2iNac: TIntegerField;
    ViewDoc1iNac: TcxGridDBColumn;
    taSpecIn2iCat: TSmallintField;
    ViewDoc1iCat: TcxGridDBColumn;
    cxButton7: TcxButton;
    teSpecIn: TdxMemData;
    teSpecInNum: TIntegerField;
    teSpecInCodeTovar: TIntegerField;
    teSpecInCodeEdIzm: TSmallintField;
    teSpecInBarCode: TStringField;
    teSpecInNDSProc: TFloatField;
    teSpecInOutNDSSum: TFloatField;
    teSpecInBestBefore: TDateField;
    teSpecInKolMest: TIntegerField;
    teSpecInEdMest: TFloatField;
    teSpecInKolWithMest: TFloatField;
    teSpecInKolWithNecond: TFloatField;
    teSpecInKol: TFloatField;
    teSpecInCenaTovar: TFloatField;
    teSpecInProcent: TFloatField;
    teSpecInNewCenaTovar: TFloatField;
    teSpecInSumCenaTovarPost: TFloatField;
    teSpecInSumCenaTovar: TFloatField;
    teSpecInProcentN: TFloatField;
    teSpecInNecond: TFloatField;
    teSpecInCenaNecond: TFloatField;
    teSpecInSumNecond: TFloatField;
    teSpecInProcentZ: TFloatField;
    teSpecInZemlia: TFloatField;
    teSpecInProcentO: TFloatField;
    teSpecInOthodi: TFloatField;
    teSpecInCodeTara: TIntegerField;
    teSpecInVesTara: TFloatField;
    teSpecInCenaTara: TFloatField;
    teSpecInSumVesTara: TFloatField;
    teSpecInSumCenaTara: TFloatField;
    teSpecInTovarType: TIntegerField;
    teSpecInNDSSum: TFloatField;
    acSaveDoc1: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    taSpPricePPre: TFloatField;
    cxLabel12: TcxLabel;
    cxLabel13: TcxLabel;
    acCalcPriceOut: TAction;
    taSpecIn2PriceNac: TFloatField;
    acPrintYellow: TAction;
    Panel5: TPanel;
    Memo1: TcxMemo;
    Panel6: TPanel;
    Label7: TLabel;
    Panel7: TPanel;
    Label8: TLabel;
    Panel8: TPanel;
    Label14: TLabel;
    Panel9: TPanel;
    Label15: TLabel;
    taSpecIn2sTop: TStringField;
    taSpecIn2sNov: TStringField;
    ViewDoc1sTop: TcxGridDBColumn;
    ViewDoc1sNov: TcxGridDBColumn;
    taSpecIn2CenaTovarAvr: TFloatField;
    ViewDoc1CenaTovarAvr: TcxGridDBColumn;
    acAddSpis3: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acPrintPassport: TAction;
    tePrintP: TdxMemData;
    tePrintPiNum: TIntegerField;
    tePrintPBarcode: TStringField;
    tePrintPiCode: TIntegerField;
    tePrintPName: TStringField;
    tePrintPDocDate: TStringField;
    tePrintPDocNum: TStringField;
    tePrintPCliName: TStringField;
    tePrintPPrBrak: TStringField;
    tePrintPNameTara: TStringField;
    tePrintPBrakVozm: TStringField;
    frtePrintP: TfrDBDataSet;
    N8: TMenuItem;
    tePrintPEdIzm: TStringField;
    Label16: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxButton8: TcxButton;
    teSpecZ: TdxMemData;
    teSpecZCode: TIntegerField;
    teSpecZName: TStringField;
    teSpecZFullName: TStringField;
    teSpecZEdIzm: TSmallintField;
    teSpecZRemn1: TFloatField;
    teSpecZvReal: TFloatField;
    teSpecZRemnDay: TFloatField;
    teSpecZRemnMin: TFloatField;
    teSpecZRemnMax: TFloatField;
    teSpecZRemn2: TFloatField;
    teSpecZQuant1: TFloatField;
    teSpecZPK: TFloatField;
    teSpecZQuantZ: TFloatField;
    teSpecZQuant2: TFloatField;
    teSpecZQuant3: TFloatField;
    teSpecZPriceIn: TFloatField;
    teSpecZSumIn: TFloatField;
    teSpecZPriceIn0: TFloatField;
    teSpecZSumIn0: TFloatField;
    teSpecZNDSProc: TStringField;
    teSpecZiCat: TIntegerField;
    teSpecZiTop: TIntegerField;
    teSpecZiN: TIntegerField;
    teSpecZBarCode: TStringField;
    teSpecZCliQuant: TFloatField;
    teSpecZCliPrice: TFloatField;
    teSpecZCliPrice0: TFloatField;
    teSpecZCliSumIn: TFloatField;
    teSpecZCliSumIn0: TFloatField;
    teSpecZCliQuantN: TFloatField;
    teSpecZCliPriceN: TFloatField;
    teSpecZCliPrice0N: TFloatField;
    teSpecZCliSumInN: TFloatField;
    teSpecZCliSumIn0N: TFloatField;
    Label17: TLabel;
    teSpecZNNum: TIntegerField;
    cxLabel14: TcxLabel;
    taSpecIn2GTD: TStringField;
    ViewDoc1GTD: TcxGridDBColumn;
    taSpecIn: TdxMemData;
    taSpecInNum: TIntegerField;
    taSpecInCodeTovar: TIntegerField;
    taSpecInBarCode: TStringField;
    taSpecInNDSProc: TFloatField;
    taSpecInNDSSum: TFloatField;
    taSpecInOutNDSSum: TFloatField;
    taSpecInBestBefore: TDateField;
    taSpecInKolMest: TFloatField;
    taSpecInKolEdMest: TFloatField;
    taSpecInKolWithMest: TFloatField;
    taSpecInKolWithNecond: TFloatField;
    taSpecInKol: TFloatField;
    taSpecInCenaTovar: TFloatField;
    taSpecInProcent: TFloatField;
    taSpecInNewCenaTovar: TFloatField;
    taSpecInSumCenaTovarPost: TFloatField;
    taSpecInSumCenaTovar: TFloatField;
    taSpecInProcentN: TFloatField;
    taSpecInNecond: TFloatField;
    taSpecInCenaNecond: TFloatField;
    taSpecInSumNecond: TFloatField;
    taSpecInProcentZ: TFloatField;
    taSpecInZemlia: TFloatField;
    taSpecInProcentO: TFloatField;
    taSpecInOthodi: TFloatField;
    taSpecInName: TStringField;
    taSpecInCodeTara: TIntegerField;
    taSpecInVesTara: TFloatField;
    taSpecInCenaTara: TFloatField;
    taSpecInSumVesTara: TFloatField;
    taSpecInSumCenaTara: TFloatField;
    taSpecInNameTara: TStringField;
    taSpecInTovarType: TIntegerField;
    taSpecInRealPrice: TFloatField;
    taSpecInRemn: TFloatField;
    taSpecInNac1: TFloatField;
    taSpecInNac2: TFloatField;
    taSpecInNac3: TFloatField;
    taSpecIniCat: TSmallintField;
    taSpecInPriceNac: TFloatField;
    taSpecInsTop: TStringField;
    taSpecInsNov: TStringField;
    taSpecInCenaTovarAvr: TFloatField;
    taSpecInGTD: TStringField;
    taSpecIniNac: TFloatField;
    taSpecInCodeEdIzm: TSmallintField;
    taSpecInsMaker: TStringField;
    ViewDoc1sMaker: TcxGridDBColumn;
    acSetGTD: TAction;
    N9: TMenuItem;
    cxLabel15: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ViewDoc1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure taSpecIn2KolMestChange(Sender: TField);
    procedure taSpecIn2KolWithMestChange(Sender: TField);
    procedure taSpecIn2KolWithNecondChange(Sender: TField);
    procedure taSpecIn2NecondChange(Sender: TField);
    procedure taSpecIn2ZemliaChange(Sender: TField);
    procedure taSpecIn2OthodiChange(Sender: TField);
    procedure taSpecIn2CenaTovarChange(Sender: TField);
    procedure taSpecIn2SumCenaTovarPostChange(Sender: TField);
    procedure taSpecIn2NDSProcChange(Sender: TField);
    procedure taSpecIn2NDSSumChange(Sender: TField);
    procedure taSpecIn2OutNDSSumChange(Sender: TField);
    procedure taSpecIn2ProcentChange(Sender: TField);
    procedure taSpecIn2NewCenaTovarChange(Sender: TField);
    procedure taSpecIn2SumCenaTovarChange(Sender: TField);
    procedure taSpecIn2ProcentNChange(Sender: TField);
    procedure taSpecIn2ProcentZChange(Sender: TField);
    procedure taSpecIn2ProcentOChange(Sender: TField);
    procedure taSpecIn2CenaTaraChange(Sender: TField);
    procedure taSpecIn2SumCenaTaraChange(Sender: TField);
    procedure ViewDoc1DblClick(Sender: TObject);
    procedure cxCurrencyEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxCurrencyEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acCalcPriceExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acPostavExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acTermoPExecute(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acScaleViewExecute(Sender: TObject);
    procedure acAddToScaleExecute(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure acSetRemnExecute(Sender: TObject);
    procedure ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure taSpecIn2CalcFields(DataSet: TDataSet);
    procedure cxButton7Click(Sender: TObject);
    procedure acSaveDoc1Execute(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure acCalcPriceOutExecute(Sender: TObject);
    procedure cxLabel12Click(Sender: TObject);
    procedure cxLabel13Click(Sender: TObject);
    procedure acPrintYellowExecute(Sender: TObject);
    procedure taSpecIn2BeforePost(DataSet: TDataSet);
    procedure ViewDoc1NameTaraPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acAddSpis3Execute(Sender: TObject);
    procedure acPrintPassportExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure taSpecInCalcFields(DataSet: TDataSet);
    procedure taSpecInBeforePost(DataSet: TDataSet);
    procedure taSpecInNDSProcChange(Sender: TField);
    procedure taSpecInNDSSumChange(Sender: TField);
    procedure taSpecInOutNDSSumChange(Sender: TField);
    procedure taSpecInKolMestChange(Sender: TField);
    procedure taSpecInKolWithMestChange(Sender: TField);
    procedure taSpecInKolWithNecondChange(Sender: TField);
    procedure taSpecInCenaTovarChange(Sender: TField);
    procedure taSpecInProcentChange(Sender: TField);
    procedure taSpecInNewCenaTovarChange(Sender: TField);
    procedure taSpecInSumCenaTovarPostChange(Sender: TField);
    procedure taSpecInSumCenaTovarChange(Sender: TField);
    procedure taSpecInProcentNChange(Sender: TField);
    procedure taSpecInNecondChange(Sender: TField);
    procedure taSpecInProcentZChange(Sender: TField);
    procedure taSpecInZemliaChange(Sender: TField);
    procedure taSpecInProcentOChange(Sender: TField);
    procedure taSpecInOthodiChange(Sender: TField);
    procedure taSpecInCenaTaraChange(Sender: TField);
    procedure taSpecInSumCenaTaraChange(Sender: TField);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acSetGTDExecute(Sender: TObject);
    procedure cxLabel15Click(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prFormSumDoc;
    procedure prSetNac;
    procedure prSetFields1;
  end;


var
  fmAddDoc1: TfmAddDoc1;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, DocsIn, sumprops, MainMCryst,
  Move, QuantMess, u2fdk, AddInScale, MT, CorrDoc, TBuff, AddDoc4, DocsVn,
  ZakHPost, mMakers, SetGTD, dmPS, SetMatrix, SpInEr;

{$R *.dfm}

procedure TfmAddDoc1.prSetFields1;
Var rPr,rPrA,rPrCard:Real;
    rn1,rn2,rn3:Real;
begin
  with dmMT do
  begin
      if abs(taSpecInCenaTovar.AsFloat)<0.01 then
      begin
        rPr:=1;
        if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then rPr:=taCardsCena.AsFloat;
         taSpecInNewCenaTovar.AsFloat:=rPr;
        taSpecInSumCenaTovar.AsFloat:=RoundVal(rPr*taSpecInKol.AsFloat);
        if taSpecInCenaTovar.AsFloat<>0 then
         taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
        else
         taSpecInProcent.AsFloat:=100;
      end else
      begin
        prCalcPriceOut(taSpecInCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),taSpecInCenaTovar.AsFloat,taSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

        if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then
        begin
          rPrCard:=taCardsCena.AsFloat;

          if abs(rPr-rPrCard)<0.11 then rPr:=rPrCard; //���� ������� � ������� ����� ����� 11 ������ �� ���� �� ��������.
          if abs(rPr)<0.01 then rPr:=rPrCard; //���� rPr =0  �� ���� �� �������� �.�. �� ��������.

          taSpecInNewCenaTovar.AsFloat:=rPr;
          taSpecInSumCenaTovar.AsFloat:=RoundVal(rPr*taSpecInKol.AsFloat);
          taSpecInCenaTovarAvr.AsFloat:=rPrA;

          if taSpecInCenaTovar.AsFloat<>0 then
           taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
          else
           taSpecInProcent.AsFloat:=100;

          if taCardsV02.AsInteger=4 then taSpecInNac1.AsFloat:=rn1;
        end;
      end;
    end;

end;

procedure TfmAddDoc1.prSetNac;
Var n1,n2,n3,rPr:Real;
begin
  with dmMt do
  begin
    try
      ViewDoc1.BeginUpdate;
      taSpecIn.First;
      while not taSpecIn.Eof do
      begin
        prFindNacGr(taSpecInCodeTovar.AsInteger,n1,n2,n3,rPr);

        taSpecIn.Edit;
        taSpecInNac1.AsFloat:=n1;
        taSpecInNac2.AsFloat:=n2;
        taSpecInNac3.AsFloat:=n3;
        taSpecInPriceNac.AsFloat:=rPr;
        taSpecIn.Post;

        taSpecIn.Next;
      end;
    finally
      ViewDoc1.EndUpdate;
    end;
  end;
end;



procedure TfmAddDoc1.prFormSumDoc;
var rSumIn,rN10,rN20,rSum10,rSum20,rSum0:Real;
begin
  try
    ViewDoc1.BeginUpdate;
    iCol:=0;
    rSumIn:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum0:=0;
    taSpecIn.First;
    while not taSpecIN.Eof do
    begin
      if (taSpecInCodeTovar.AsInteger>0) then
      begin
        taSpecIn.Edit;
        taSpecInCenaTovar.AsFloat:=rv(taSpecInCenaTovar.AsFloat);
        taSpecInNewCenaTovar.AsFloat:=rv(taSpecInNewCenaTovar.AsFloat);
        taSpecInSumCenaTovar.AsFloat:=rv(taSpecInSumCenaTovar.AsFloat);
        taSpecInSumCenaTovarPost.AsFloat:=rv(taSpecInSumCenaTovarPost.AsFloat);
        taSpecInOutNDSSum.AsFloat:=rv(taSpecInOutNDSSum.AsFloat);
        taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat-taSpecInOutNDSSum.AsFloat;
        taSpecIn.Post;

        rSumIn:=rSumIn+rv(taSpecInSumCenaTovarPost.AsFloat);

        if taSpecInNDSProc.AsFloat>=15 then
        begin
          rSum20:=rSum20+rv(taSpecInOutNDSSum.AsFloat);
          rN20:=rN20+rv(taSpecInNDSSum.AsFloat);
        end else
        begin
          if taSpecInNDSProc.AsFloat>=8 then
          begin
            rSum10:=rSum10+rv(taSpecInOutNDSSum.AsFloat);
            rN10:=rN10+rv(taSpecInNDSSum.AsFloat);
          end else //0
          begin
            rSum0:=rSum0+rv(taSpecInSumCenaTovarPost.AsFloat);
          end;
        end;
      end;
      taSpecIn.Next;
    end;

    fmDocCorr.cxCurrencyEdit1.Value:=rSumIn;
    fmDocCorr.cxCurrencyEdit2.Value:=rSum0;
    fmDocCorr.cxCurrencyEdit3.Value:=rSum10;
    fmDocCorr.cxCurrencyEdit4.Value:=rN10;
    fmDocCorr.cxCurrencyEdit5.Value:=rSum20;
    fmDocCorr.cxCurrencyEdit6.Value:=rN20;

    if rv(cxCalcEdit2.EditValue)<>rv(rSumIn) then
    begin
      fmDocCorr.cxCurrencyEdit7.Value:=rSumIn;
      fmDocCorr.cxCurrencyEdit8.Value:=rSum0;
      fmDocCorr.cxCurrencyEdit9.Value:=rSum10;
      fmDocCorr.cxCurrencyEdit10.Value:=rN10;
      fmDocCorr.cxCurrencyEdit11.Value:=rSum20;
      fmDocCorr.cxCurrencyEdit12.Value:=rN20;

      cxCalcEdit2.EditValue:=rSumIn;
      cxCalcEdit3.EditValue:=rSum0;
      cxCalcEdit4.EditValue:=rSum10;
      cxCalcEdit5.EditValue:=rN10;
      cxCalcEdit6.EditValue:=rSum20;
      cxCalcEdit7.EditValue:=rN20;
    end else
    begin
      fmDocCorr.cxCurrencyEdit7.Value:=cxCalcEdit2.EditValue;
      fmDocCorr.cxCurrencyEdit8.Value:=cxCalcEdit3.EditValue;
      fmDocCorr.cxCurrencyEdit9.Value:=cxCalcEdit4.EditValue;
      fmDocCorr.cxCurrencyEdit10.Value:=cxCalcEdit5.EditValue;
      fmDocCorr.cxCurrencyEdit11.Value:=cxCalcEdit6.EditValue;
      fmDocCorr.cxCurrencyEdit12.Value:=cxCalcEdit7.EditValue;
    end;

  finally
    ViewDoc1.EndUpdate;
  end;
//  prSetNac;
end;

procedure TfmAddDoc1.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc1.Align:=alClient;
  ViewDoc1.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);

  cxComboBox1.Properties.Items.Clear;
  cxComboBox1.Properties.Items.Add('�������');
  cxComboBox1.Properties.Items.Add(CommonSet.NamePost);
end;

procedure TfmAddDoc1.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S,user:String;
begin
  if fmclients.Showing then  fmclients.Close;
  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  if Label4.Tag<=1 then //����������
  begin
    fmClients.LevelCli.Active:=True;
    fmClients.LevelIP.Active:=False;
  end;
  if Label4.Tag=2 then //���������������
  begin
    fmClients.LevelCli.Active:=False;
    fmClients.LevelIP.Active:=True;
  end;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;

        if quCliSTBLOCK.AsInteger=1 then
        begin
          showmessage('��������� ������������ ��� �������. ���� ������������ ��������! ��� �������� ����������: '+quCliTYPEVOZ.AsString);
          quFCli.Active:=False;
          quFIP.Active:=False;
          exit;
        end;

        //p   b
        user:=Person.Name;
        if (dmMC.quCliRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
        begin
          cxLabel3.Enabled:=false;                              //���� ������������ ���, �� ��������� �������������� ���� � ����� ����������
         { if((user='Oper1') or (user='Oper2') or (user='Zaved')) then
          begin            }
            ViewDoc1CenaTovar.Options.Editing:=True; // false; p
            ViewDoc1SumCenaTovarPost.Options.Editing:=true; // false; p
            cxLabel15.Enabled:=true;
      {    end
          else               //���� ������������ ������ �� ��������� �������������� ������ �����
          begin
            if (user='OPER CB') then
            begin
               ViewDoc1CenaTovar.Options.Editing:=true;
               ViewDoc1SumCenaTovarPost.Options.Editing:=true;
               cxLabel5.Enabled:=false;//true; p
            end
            else
            begin
              ViewDoc1CenaTovar.Options.Editing:=true;
              ViewDoc1SumCenaTovarPost.Options.Editing:=True; // false; p
              cxLabel15.Enabled:=false;//true; p
            end;
          end;}
        end
        else
        begin
           ViewDoc1CenaTovar.Options.Editing:=true;
           ViewDoc1SumCenaTovarPost.Options.Editing:=true;
           cxLabel3.Enabled:=true;
           cxLabel15.Enabled:=true;
        end;
        //p end


      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end else
  begin


  end;

  bOpen:=False;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin

      if pos('��',dmMC.quCliName.AsString)<>1 then
      begin
        if (cxButtonEdit1.Tag<>dmMC.quCliVendor.AsInteger)and(dmMC.quCliRaion.AsInteger>0)and(dmMC.quCliRaion.AsInteger<>6) then  //���� ������ � EDI
        begin //������ ������������
          ViewDoc1.BeginUpdate;

          taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
          ViewDoc1.EndUpdate;
        end;

        Label4.Tag:=1;

        if dmMC.quCliSTBLOCK.AsInteger=1 then
        begin
          showmessage('��������� ������������ ��� �������. ���� ������������ ��������! ��� �������� ����������: '+dmMC.quCliTYPEVOZ.AsString);
          dmMC.quFCli.Active:=False;
          dmMC.quFIP.Active:=False;
          exit;
        end;

    {    with dmP do
        begin
          quPredzCli.Active:=false;
          quPredzCli.ParamByName('ICLI').AsInteger:=dmMC.quCliStatus.AsInteger;
          quPredzCli.Active:=true;
          if quPredzClikol.AsInteger>0 then showmessage('� ������� ���������� ���� ���������� �� �������!');
          quPredzCli.Active:=false;
        end;  }

        if prCliNDS(1,dmMC.quCliVendor.AsInteger,0)=1 then
        begin
          Label17.Caption:='���������� ���';
          Label17.Tag:=1;
        end else
        begin
          Label17.Caption:='�� ���������� ���';
          Label17.Tag:=0;
        end;


         //p   b
        user:=Person.Name;
        if (dmMC.quCliRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
        begin
          cxLabel3.Enabled:=false;                              //���� ������������ ���, �� ��������� �������������� ���� � ����� ����������
         { if((user='Oper1') or (user='Oper2') or (user='Zaved')) then
          begin            }
            ViewDoc1CenaTovar.Options.Editing:=True; // false; p
            ViewDoc1SumCenaTovarPost.Options.Editing:=true; // false; p
            cxLabel15.Enabled:=true;
      {    end
          else               //���� ������������ ������ �� ��������� �������������� ������ �����
          begin
            if (user='OPER CB') then
            begin
               ViewDoc1CenaTovar.Options.Editing:=true;
               ViewDoc1SumCenaTovarPost.Options.Editing:=true;
               cxLabel5.Enabled:=false;//true; p
            end
            else
            begin
              ViewDoc1CenaTovar.Options.Editing:=true;
              ViewDoc1SumCenaTovarPost.Options.Editing:=True; // false; p
              cxLabel15.Enabled:=false;//true; p
            end;
          end;}
        end
        else
        begin
           ViewDoc1CenaTovar.Options.Editing:=true;
           ViewDoc1SumCenaTovarPost.Options.Editing:=true;
           cxLabel3.Enabled:=true;
           cxLabel15.Enabled:=true;
        end;
        //p end

        if (dmMC.quCliRaion.AsInteger>0)and(dmMC.quCliRaion.AsInteger<>6) then
        begin
          if (cxTextEdit3.Tag=0)or(cxButtonEdit1.Tag<>dmMC.quCliVendor.AsInteger) then
          begin
            cxTextEdit3.Text:='';
            ViewDoc1.BeginUpdate;
            taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
            ViewDoc1.EndUpdate;
          end;
          cxTextEdit3.Tag:=1;
          ViewDoc1CodeTovar.Options.Editing:=False;
          ViewDoc1Name.Options.Editing:=False;
          ViewDoc1BarCode.Options.Editing:=False;
        end else
        begin //���� �� EDI � ������ �� ����� ����������� ���
          if (cxButtonEdit1.Tag<>dmMC.quCliVendor.AsInteger) then
          begin
            ViewDoc1.BeginUpdate;
            taSpecIn.First;
            while not taSpecIn.Eof do
            begin
              iCol:=1;
              taSpecIn.Edit;
              if Label17.Tag=0 then
              begin
                taSpecInNDSProc.AsFloat:=0;
              end else
              begin
                if dmMT.taCards.FindKey([taSpecInCodeTovar.AsInteger]) then taSpecInNDSProc.AsFloat:=dmMT.taCardsNDS.AsFloat;
              end;
              taSpecIn.Post;

              taSpecIn.Next;
            end;
            ViewDoc1.EndUpdate;
          end;

          ViewDoc1CodeTovar.Options.Editing:=True;
          ViewDoc1Name.Options.Editing:=True;
          ViewDoc1BarCode.Options.Editing:=True;
          cxTextEdit3.Tag:=0;
          cxTextEdit3.Text:='';
        end;

        cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quCliName.AsString;

      end else showmessage('����� ������� ���������� �������� !!!');
    end else
    begin
      if pos('��',dmMC.quIPName.AsString)<>1 then
      begin
        if (cxTextEdit3.Tag=1) then
        begin //������ ������������
          ViewDoc1.BeginUpdate;
          taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
          ViewDoc1.EndUpdate;
        end;

        Label4.Tag:=2;

        if prCliNDS(2,dmMC.quIPCode.AsInteger,0)=1 then
        begin
          Label17.Caption:='���������� ���';
          Label17.Tag:=1;
        end else
        begin
          Label17.Caption:='�� ���������� ���';
          Label17.Tag:=0;
        end;

        cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
        cxButtonEdit1.Text:=dmMC.quIPName.AsString;
        cxTextEdit3.Text:='';
        cxTextEdit3.Tag:=0;
        fmAddDoc1.ViewDoc1CodeTovar.Editing:=True;
        fmAddDoc1.ViewDoc1Name.Editing:=True;
        fmAddDoc1.ViewDoc1BarCode.Editing:=True;
      end  else showmessage('����� ������� ���������� �������� !!!');
    end;
  end;
{   else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;}

  iDirect:=0;
end;

procedure TfmAddDoc1.ViewDoc1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=2 then  Accept:=True;
end;

procedure TfmAddDoc1.ViewDoc1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    rn1,rn2,rn3,rPr,rPrA:Real;
begin
//

  if iDirect=2 then
  begin
    iDirect:=0; //������ ���������

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if (iCo>0)and(cxTextEdit3.tag=0) then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        iMax:=1;
        fmAddDoc1.taSpecIn.First;
        if not fmAddDoc1.taSpecIn.Eof then
        begin
          fmAddDoc1.taSpecIn.Last;
          iMax:=fmAddDoc1.taSpecInNum.AsInteger+1;
        end;

        with dmMT do
        begin
          ViewDoc1.BeginUpdate;
          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            if taCards.FindKey([iNum]) then
            begin
              if (taCardsStatus.AsInteger<100)  then
              begin
                prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

                taSpecIn.Append;
                taSpecInNum.AsInteger:=iMax;
                taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                taSpecInBarCode.AsString:=taCardsBarCode.AsString;
                taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat*Label17.Tag;
                taSpecInNDSSum.AsFloat:=0;
                taSpecInOutNDSSum.AsFloat:=0;
                taSpecInBestBefore.AsDateTime:=Date;
                taSpecInKolMest.AsInteger:=1;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=0;
                taSpecInKolWithNecond.AsFloat:=0;
                taSpecInKol.AsFloat:=0;
                taSpecInCenaTovar.AsFloat:=0;
                taSpecInProcent.AsFloat:=0;
                taSpecInNewCenaTovar.AsFloat:=rPr;
                taSpecInSumCenaTovarPost.AsFloat:=0;
                taSpecInSumCenaTovar.AsFloat:=0;
                taSpecInProcentN.AsFloat:=0;
                taSpecInNecond.AsFloat:=0;
                taSpecInCenaNecond.AsFloat:=0;
                taSpecInSumNecond.AsFloat:=0;
                taSpecInProcentZ.AsFloat:=0;
                taSpecInZemlia.AsFloat:=0;
                taSpecInProcentO.AsFloat:=0;
                taSpecInOthodi.AsFloat:=0;
                taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                taSpecInCodeTara.AsInteger:=0;
                taSpecInVesTara.AsFloat:=0;
                taSpecInCenaTara.AsFloat:=0;
                taSpecInSumCenaTara.AsFloat:=0;

                taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                taSpecInNac1.AsFloat:=rn1;
                taSpecInNac2.AsFloat:=rn2;
                taSpecInNac3.AsFloat:=rn3;
                taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;

                if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                taSpecInCenaTovarAvr.AsFloat:=rPrA;

                if taCardsV12.AsInteger>0 then
                begin
                  taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                  taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                end else
                begin
                  taSpecInSumVesTara.AsFloat:=0;
                  taSpecInsMaker.AsString:='';
                end;

                taSpecIn.Post;

                inc(iMax);

              end;
            end;
          end;

          prSetNac;

          ViewDoc1.EndUpdate;
          ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
          GridDoc1.SetFocus;

        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc1.cxLabel3Click(Sender: TObject);
Var rPriceIn,rPriceM:Real;
//    ,rn1,rn2,rn3,rPr:Real;
begin
  //����������� ����
  if cxButton1.Enabled then
  begin
    iCol:=8;
    taSpecIn.First;
    while not taSpecIn.Eof do
    begin
      rPriceIn:=prFLP(taSpecInCodeTovar.AsInteger,rPriceM);

//      prCalcPriceOut(taSpecInCodeTovar.AsInteger,rPriceIn,taSpecInKol.AsFloat,rn1,rn2,rn3,rPr);

      taSpecIn.Edit;
      if CommonSet.Single=1 then taSpecInNewCenaTovar.AsFloat:=rPriceM
      else taSpecInNewCenaTovar.AsFloat:=0;
      taSpecInCenaTovar.AsFloat:=rPriceIn;
      taSpecIn.Post;

      taSpecIn.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecIn.First;
  end;
end;

procedure TfmAddDoc1.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc1.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  ViewDoc1.BeginUpdate;
  taSpecIn.First;
  while not taSpecIn.Eof do
  begin
{      taSpecIn.Edit;
      taSpecInPrice2.AsFloat:=taSpecInPrice1.AsFloat;
      taSpecInSum2.AsFloat:=taSpecInSum1.AsFloat;
      taSpecInSumNac.AsFloat:=0;
      taSpecInProcNac.AsFloat:=0;
      taSpecIn.Post;
      }
    taSpecIn.Next;
    delay(10);
  end;
  taSpecIn.First;
  ViewDoc1.EndUpdate;
end;

procedure TfmAddDoc1.ViewDoc1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NDSProc' then iCol:=1;  //������ ��� ���
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NDSSum' then iCol:=2;  //����� ���         10%   47.76
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1OutNDSSum' then iCol:=3;  //����� ��� ���  100%   477.6
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1KolMest' then iCol:=4;   //���-�� ����            1
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1KolWithMest' then iCol:=5; //���-�� � ����� �����  24
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1KolWithNecond' then iCol:=6; //���-�� ����� �.�. 2*4   24
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Kol' then iCol:=7;  //���-�� ����� ��� ����������      23.88
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CenaTovar' then iCol:=8; //���� ���������� � ��� � ���  22
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Procent' then iCol:=9;  //������� �������  25
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NewCenaTovar' then iCol:=10;  //���� �������� 27,50
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumCenaTovarPost' then iCol:=11; //����� ����������  525.36
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumCenaTovar' then iCol:=12;  //����� �������� 656.7
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1ProcentN' then iCol:=13;     //���������� %
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Necond' then iCol:=14;       //���-�� ����������
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CenaNecond' then iCol:=15;   //���� ����������
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumNecond' then iCol:=16;    //����� ����������
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1ProcentZ' then iCol:=17;     //�����  %
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Zemlia' then iCol:=18;       //���-��
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1ProcentO' then iCol:=19;     //������ %   0,5
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Othodi' then iCol:=20;       //������ ���-��  0,12
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1VesTara' then iCol:=21;      //��� ����
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CenaTara' then iCol:=22;     //���� ����
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumVesTara' then iCol:=23;   //����� ��� ����
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumCenaTara' then iCol:=24;  //����� �� ����

end;

procedure TfmAddDoc1.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc1.SetFocus;

//  cxCalcEdit1.EditValue:=CommonSet.CurNac;
  cxCalcEdit1.EditValue:=CommonSet.DefNacIn;

  Memo1.Clear;

  if CommonSet.Single=0 then
  begin
    Panel4.Visible:=False;
  end else
  begin
  end;

//  ViewDoc1NameG.Options.Editing:=False;
//  ViewTaraNameG.Options.Editing:=False;
end;

procedure TfmAddDoc1.FormClose(Sender: TObject; var Action: TCloseAction);
Var bPrint:Boolean;
    bExit:Boolean;
begin
    //�������� �� ������ ������ ��������
  bPrint:=False;
  bExit:=False;
  ViewDoc1.BeginUpdate;
  taSpecIN.First;
  while not taSpecIn.Eof do
  begin
    bPrint:=False;
    if taSpecIniCat.AsInteger=2 then bPrint:=True;
    if taSpecIniCat.AsInteger=8 then bPrint:=True;
    if taSpecIniCat.AsInteger=9 then bPrint:=True;
    if taSpecIniCat.AsInteger=4 then bPrint:=True;

    taSpecIN.Next;
  end;
  ViewDoc1.EndUpdate;

  if bPrint then
  begin
    if MessageDlg('����������� ������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then acPrintYellow.Execute
    else bExit:=True;
  end else bExit:=True;

  if bExit then
  begin
    if cxButton1.Enabled then
    begin
      if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
      begin
        bDrIn:=False;
        ViewDoc1.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
      end else
      begin
       Action:= caNone;
      end;
    end else
    begin
      bDrIn:=False;
      ViewDoc1.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end;
  end else Action:= caNone;
end;

procedure TfmAddDoc1.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
//    rK:Real;
//    iM:Integer;
//    Sm:String;
    bAdd:Boolean;
    rn1,rn2,rn3,rPr,rPrA:Real;

begin
  with dmMC do
  with dmMT do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTara' then
      begin
        try
          iCode:=VarAsType(AEdit.EditingValue, varInteger);
        except
          iCode:=0;
        end;

        if iCode>0 then
        if fTara(iCode,sName) then
        begin
          taSpecIn.Edit;
          taSpecInCodeTara.AsInteger:=iCode;
          taSpecInNameTara.AsString:=sName;
          taSpecIn.Post;
        end else
        begin
          taSpecIn.Edit;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInNameTara.AsString:='';
          taSpecIn.Post;
        end;
      end;
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameTara' then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;

        iDirect:=2;
        PosP.Id:=0; PosP.Name:='';
        if quTara.Active=False then quTara.Active:=True;
        delay(10);
        if sName>'' then quTara.Locate('Name',sName,[loCaseInsensitive]);
        fmTara.ShowModal;
        iDirect:=0;
        if PosP.Id>0 then
        begin
          taSpecIn.Edit;
          taSpecInCodeTara.AsInteger:=PosP.Id;
          taSpecInNameTara.AsString:=PosP.Name;
          taSpecIn.Post;
        end;
      end;
      if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTovar')and(cxTextEdit3.Tag=0) then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        if taCards.Active=False then taCards.Active:=True;
        if taCards.FindKey([iCode]) then
        begin
          bAdd:=True;      
          if taCardsStatus.AsInteger>=100 then
          begin
            bAdd:=False;
            if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
          end;
          if bAdd then
          begin
//            prFindNacGr(taCardsID.AsInteger,rn1,rn2,rn3,rPr);
            prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

            taSpecIn.Edit;
            taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
            taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
            taSpecInBarCode.AsString:=taCardsBarCode.AsString;
            taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat;
            taSpecInNDSSum.AsFloat:=0;
            taSpecInOutNDSSum.AsFloat:=0;
            taSpecInBestBefore.AsDateTime:=Date;
            taSpecInKolMest.AsInteger:=1;
            taSpecInKolEdMest.AsFloat:=0;
            taSpecInKolWithMest.AsFloat:=0;
            taSpecInKolWithNecond.AsFloat:=0;
            taSpecInKol.AsFloat:=0;
            taSpecInCenaTovar.AsFloat:=0;
            taSpecInProcent.AsFloat:=0;
            taSpecInNewCenaTovar.AsFloat:=rPr;
            taSpecInSumCenaTovarPost.AsFloat:=0;
            taSpecInSumCenaTovar.AsFloat:=0;
            taSpecInProcentN.AsFloat:=0;
            taSpecInNecond.AsFloat:=0;
            taSpecInCenaNecond.AsFloat:=0;
            taSpecInSumNecond.AsFloat:=0;
            taSpecInProcentZ.AsFloat:=0;
            taSpecInZemlia.AsFloat:=0;
            taSpecInProcentO.AsFloat:=0;
            taSpecInOthodi.AsFloat:=0;
            taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
            taSpecInCodeTara.AsInteger:=0;
            taSpecInVesTara.AsFloat:=0;
            taSpecInCenaTara.AsFloat:=0;
            taSpecInSumCenaTara.AsFloat:=0;

            taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
            taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
            taSpecInNac1.AsFloat:=rn1;
            taSpecInNac2.AsFloat:=rn2;
            taSpecInNac3.AsFloat:=rn3;
            taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
            taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;

            if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
            if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
            taSpecInCenaTovarAvr.AsFloat:=rPrA;

            if taCardsV12.AsInteger>0 then
            begin
              taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
              taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
            end else
            begin
              taSpecInSumVesTara.AsFloat:=0;
              taSpecInsMaker.AsString:='';
            end;

            taSpecIn.Post;
          end;
          GridDoc1.SetFocus;
          ViewDoc1KolWithMest.Focused:=True;
        end;
      end;
      if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name')and(cxTextEdit3.Tag=0) then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if (sName>'')and(Length(sName)>=3) then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if taCards.Active=False then taCards.Active:=True;
            if taCards.FindKey([quFindCID.AsInteger]) then
            begin
              bAdd:=True;
              if taCardsStatus.AsInteger>=100 then
              begin
                bAdd:=False;
                if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
              end;
              if bAdd then
              begin
//                prFindNacGr(taCardsID.AsInteger,rn1,rn2,rn3,rPr);
                prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

                taSpecIn.Edit;
                taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                taSpecInBarCode.AsString:=taCardsBarCode.AsString;
                taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat;
                taSpecInNDSSum.AsFloat:=0;
                taSpecInOutNDSSum.AsFloat:=0;
                taSpecInBestBefore.AsDateTime:=Date;
                taSpecInKolMest.AsInteger:=1;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=0;
                taSpecInKolWithNecond.AsFloat:=0;
                taSpecInKol.AsFloat:=0;
                taSpecInCenaTovar.AsFloat:=0;
                taSpecInProcent.AsFloat:=0;
                taSpecInNewCenaTovar.AsFloat:=rPr;
                taSpecInSumCenaTovarPost.AsFloat:=0;
                taSpecInSumCenaTovar.AsFloat:=0;
                taSpecInProcentN.AsFloat:=0;
                taSpecInNecond.AsFloat:=0;
                taSpecInCenaNecond.AsFloat:=0;
                taSpecInSumNecond.AsFloat:=0;
                taSpecInProcentZ.AsFloat:=0;
                taSpecInZemlia.AsFloat:=0;
                taSpecInProcentO.AsFloat:=0;
                taSpecInOthodi.AsFloat:=0;
                taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                taSpecInCodeTara.AsInteger:=0;
                taSpecInVesTara.AsFloat:=0;
                taSpecInCenaTara.AsFloat:=0;
                taSpecInSumCenaTara.AsFloat:=0;

                taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                taSpecInNac1.AsFloat:=rn1;
                taSpecInNac2.AsFloat:=rn2;
                taSpecInNac3.AsFloat:=rn3;
                taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
                taSpecInCenaTovarAvr.AsFloat:=rPrA;

                if taCardsV12.AsInteger>0 then
                begin
                  taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                  taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                end else
                begin
                  taSpecInSumVesTara.AsFloat:=0;
                  taSpecInsMaker.AsString:='';
                end;

                taSpecIn.Post;

              end;
              GridDoc1.SetFocus;
              ViewDoc1KolWithMest.Focused:=True;
            end;
          end;
        end;
      end;
    end else
      if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTovar') or (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTara') then
        if fTestKey(Key)=False then
          if taSpecIn.State in [dsEdit,dsInsert] then taSpecIn.Cancel;
  end;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var // Km:Real;
    sName:String;
    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  if bAdd then exit;
  with dmMC do
  with dmMT do
  begin
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name')and(cxTextEdit3.Tag=0) then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if (sName>'')and(Length(sName)>=3) then
      begin
        quFindC1.Active:=False;
        quFindC1.SQL.Clear;
        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
        quFindC1.SQL.Add('FROM "Goods"');
        quFindC1.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
        quFindC1.Active:=True;

        if quFindC1.RecordCount=1 then //����� ���� ������
        begin
          if taCards.Active=False then taCards.Active:=True;
          if taCards.FindKey([quFindC1ID.AsInteger]) then
          begin
            if (taCardsStatus.AsInteger<100) then
            begin
//              prFindNacGr(taCardsID.AsInteger,rn1,rn2,rn3,rPr);
              prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

              taSpecIn.Edit;
              taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
              taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              taSpecInBarCode.AsString:=taCardsBarCode.AsString;
              taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat;
              taSpecInNDSSum.AsFloat:=0;
              taSpecInOutNDSSum.AsFloat:=0;
              taSpecInBestBefore.AsDateTime:=Date;
              taSpecInKolMest.AsInteger:=1;
              taSpecInKolEdMest.AsFloat:=0;
              taSpecInKolWithMest.AsFloat:=0;
              taSpecInKolWithNecond.AsFloat:=0;
              taSpecInKol.AsFloat:=0;
              taSpecInCenaTovar.AsFloat:=0;
              taSpecInProcent.AsFloat:=0;
              taSpecInNewCenaTovar.AsFloat:=rPr;
              taSpecInSumCenaTovarPost.AsFloat:=0;
              taSpecInSumCenaTovar.AsFloat:=0;
              taSpecInProcentN.AsFloat:=0;
              taSpecInNecond.AsFloat:=0;
              taSpecInCenaNecond.AsFloat:=0;
              taSpecInSumNecond.AsFloat:=0;
              taSpecInProcentZ.AsFloat:=0;
              taSpecInZemlia.AsFloat:=0;
              taSpecInProcentO.AsFloat:=0;
              taSpecInOthodi.AsFloat:=0;
              taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              taSpecInCodeTara.AsInteger:=0;
              taSpecInVesTara.AsFloat:=0;
              taSpecInCenaTara.AsFloat:=0;
              taSpecInSumCenaTara.AsFloat:=0;

              taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
              taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
              taSpecInNac1.AsFloat:=rn1;
              taSpecInNac2.AsFloat:=rn2;
              taSpecInNac3.AsFloat:=rn3;
              taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
              taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
              if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
              if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
              taSpecInCenaTovarAvr.AsFloat:=rPrA;

              if taCardsV12.AsInteger>0 then
              begin
                taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
              end else
              begin
                taSpecInSumVesTara.AsFloat:=0;
                taSpecInsMaker.AsString:='';
              end;

              taSpecIn.Post;
            end;
          end;
          AEdit.SelectAll;
          ViewDoc1Name.Options.Editing:=False;
          ViewDoc1Name.Focused:=True;
          Key:=#0;
        end;//}
      end;
    end;
  end;//}
end;

procedure TfmAddDoc1.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
{  //�������� �������
  if cxTextEdit3.Tag=1 then
  begin
    Memo1.Lines.Add('��������� (EDI) - ���������� ������ ������ �� ������');
    exit;
  end;
}
  iMax:=1;
  ViewDoc1.BeginUpdate;

  taSpecIn.First;
  if not taSpecIn.Eof then
  begin
    taSpecIn.Last;
    iMax:=taSpecInNum.AsInteger+1;
  end;

  taSpecIn.Append;
  taSpecInNum.AsInteger:=iMax;
  taSpecInCodeTovar.AsInteger:=0;
  taSpecInCodeEdIzm.AsInteger:=1;
  taSpecInBarCode.AsString:='';
  taSpecInNDSProc.AsFloat:=0;
  taSpecInNDSSum.AsFloat:=0;
  taSpecInOutNDSSum.AsFloat:=0;
  taSpecInBestBefore.AsDateTime:=0;
  taSpecInKolMest.AsInteger:=1;
  taSpecInKolEdMest.AsFloat:=0;
  taSpecInKolWithMest.AsFloat:=1;
  taSpecInKolWithNecond.AsFloat:=1;
  taSpecInKol.AsFloat:=1;
  taSpecInCenaTovar.AsFloat:=0;
  taSpecInProcent.AsFloat:=0;
  taSpecInNewCenaTovar.AsFloat:=0;
  taSpecInSumCenaTovarPost.AsFloat:=0;
  taSpecInSumCenaTovar.AsFloat:=0;
  taSpecInProcentN.AsFloat:=0;
  taSpecInNecond.AsFloat:=0;
  taSpecInCenaNecond.AsFloat:=0;
  taSpecInSumNecond.AsFloat:=0;
  taSpecInProcentZ.AsFloat:=0;
  taSpecInZemlia.AsFloat:=0;
  taSpecInProcentO.AsFloat:=0;
  taSpecInOthodi.AsFloat:=0;
  taSpecInName.AsString:='';
  taSpecInCodeTara.AsInteger:=0;
  taSpecInVesTara.AsFloat:=0;
  taSpecInCenaTara.AsFloat:=0;
  taSpecInSumVesTara.AsFloat:=0;
  taSpecInSumCenaTara.AsFloat:=0;
  taSpecInRealPrice.AsFloat:=0;
  taSpecInRemn.AsFloat:=0;
  taSpecInNac1.AsFloat:=0;
  taSpecInNac2.AsFloat:=0;
  taSpecInNac3.AsFloat:=0;
  taSpecIniCat.AsINteger:=0;
  taSpecInPriceNac.AsFloat:=0;
  taSpecInsMaker.AsString:='';
  taSpecIn.Post;

  ViewDoc1.EndUpdate;
  GridDoc1.SetFocus;

  if cxTextEdit3.Tag=0 then
  begin
    ViewDoc1CodeTovar.Options.Editing:=True;
    ViewDoc1Name.Options.Editing:=True;
  end;

  ViewDoc1Name.Focused:=True;

  ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
end;

procedure TfmAddDoc1.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecIn.RecordCount>0 then
  begin
    taSpecIn.Delete;
  end;
end;

procedure TfmAddDoc1.acAddListExecute(Sender: TObject);
begin
  if cxTextEdit3.Tag=1 then
  begin
    Memo1.Lines.Add('��������� (EDI) - ���������� ������ ������ �� ������');
  end else
  begin
    iDirect:=2;
    fmCards.Show;
  end;  
end;

procedure TfmAddDoc1.cxButton1Click(Sender: TObject);
Var IdH:Int64;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSumNec,rSum10M,rSum20M, rSum0:Real;
    bErr,bErrSum:Boolean; // ,bDel
//    par:Variant;
    sNum:String;
    idActP:String;
begin
  //��������
  with dmMC do
  with dmMt do
  begin
    if cxDateEdit1.Date<=prOpenDate then  begin  ShowMessage('������ ������.'); exit; end;

    if cxButtonEdit1.Tag=0 then
    begin
      ShowMessage('�������� ����������.');
      Memo1.Lines.Add('��������� �� ���������, ���������� ����������!!!');
      exit;
    end;

    if cxLookupComboBox1.EditValue<1 then
    begin
      ShowMessage('�������� �����.');
      Memo1.Lines.Add('����� �������� �� ����������, ���������� ����������!!!');
      exit;
    end;

    sNum:=STestNUm(cxTextEdit1.Text);
    cxTextEdit1.Text:=sNum; delay(10);
    if sNum='' then
    begin
      ShowMessage('������������ ����� ���������.');
      Memo1.Lines.Add('������������ ����� ���������, ���������� ����������!!!');
      exit;
    end;

    if fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False then begin ShowMessage('������ ������ (��������������).'); exit; end;

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton4.Enabled:=False;  cxButton7.Enabled:=False;

    IdH:=cxTextEdit1.Tag;

    if taSpecIn.State in [dsEdit,dsInsert] then taSpecIn.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;

    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "TTNIn"');
      quC.SQL.Add('where Depart='+INtToStr(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInvoice='''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='''+cxTextEdit1.Text+'''');
      quC.Active:=True;
      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        cxButton1.Enabled:=True; cxButton2.Enabled:=True;  cxButton4.Enabled:=True;  cxButton7.Enabled:=True;
        exit;
      end;
    end;

    try
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ����������..'); delay(10);

      Memo1.Lines.Add('��������� ���� ��������...'); delay(10);
      acCalcPriceOut.Execute;         //��������� ���� ���.

      CloseTe(teSpecIn);
      prFormSumDoc; //��� �������� � �����������

      delay(10);

      ViewDoc1.BeginUpdate;

      iCol:=0;
      bErr:=False;
      bErrSum:=False;
      //������� ������ ����
      rSum1:=0; rSum2:=0; rSum3:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSumNec:=0; rSum10M:=0; rSum20M:=0;
      rSum0:=0;
      taSpecIn.First;
      while not taSpecIN.Eof do
      begin
        rSum1:=rSum1+rv(taSpecInSumCenaTovarPost.AsFloat);
        rSum2:=rSum2+rv(taSpecInSumCenaTovar.AsFloat);
        rSum3:=rSum3+RV(taSpecInSumCenaTara.AsFloat);
        rSumNec:=rSumNec+RV(taSpecInSumNecond.AsFloat);


        if (taSpecInCodeTovar.AsInteger=0)and(taSpecInCodeTara.AsInteger=0)  then
        begin
          bErr:=True;
          Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. �������� ��� ����� ��� ����.'); delay(10);
        end else
        begin
          if (taSpecInCodeTovar.AsInteger>0) then
          begin
            if taCards.FindKey([taSpecInCodeTovar.AsInteger])=False then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ��� ������.'); delay(10); end;

            if abs(taSpecInKol.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���-��.'); delay(10); end;
            if abs(taSpecInSumCenaTovarPost.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ����������.'); delay(10); end;
            if abs(taSpecInSumCenaTovar.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ��������.'); delay(10); end;


            if taSpecInNDSProc.AsFloat>0.001 then
            begin
              if ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then
              begin

                if abs(taSpecInNDSSum.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ����� ���.'); delay(10); end;
                if abs(taSpecInOutNDSSum.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ����� ��� ���.'); delay(10); end;
              end;

              if taSpecInNDSProc.AsFloat>=15 then
              begin
                rSum20:=rSum20+RoundVal(taSpecInOutNDSSum.AsFloat);
                rN20:=rN20+RoundVal(taSpecInNDSSum.AsFloat);
                rSum20M:=rSum20M+RoundVal(taSpecInSumCenaTovar.AsFloat);
              end else
              begin
                if taSpecInNDSProc.AsFloat>=8 then
                begin
                  rSum10:=rSum10+RoundVal(taSpecInOutNDSSum.AsFloat);
                  rN10:=rN10+RoundVal(taSpecInNDSSum.AsFloat);
                  rSum10M:=rSum10M+RoundVal(taSpecInSumCenaTovar.AsFloat);
                end else rSum0:=rSum0+rv(taSpecInSumCenaTovarPost.AsFloat);
              end;
            end else rSum0:=rSum0+rv(taSpecInSumCenaTovarPost.AsFloat);

          end;
          if taSpecInCodeTara.AsInteger>0 then
          begin
            if abs(taSpecInKolMest.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���-�� ���� (����).'); delay(10); end;
            if abs(taSpecInCenaTara.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ����.'); delay(10); end;
          end;
        end;

        if abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0)>0.001 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'������ ���� ��������� (�����������). '+floatToStr(abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0))); delay(10); end;

        taSpecIn.Next;
      end;

      if abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0)>0.001 then begin bErr:=True; Memo1.Lines.Add('������ ���� ���������.'); delay(10); end;
//      cxCurrencyEdit1.EditValue:=rSum1+cxCurrencyEdit4.EditValue+cxCurrencyEdit5.EditValue+cxCurrencyEdit6.EditValue;
      cxCurrencyEdit1.EditValue:=cxCalcEdit2.Value+cxCurrencyEdit4.EditValue+cxCurrencyEdit5.EditValue+cxCurrencyEdit6.EditValue;

      if cxTextEdit1.Tag>0 then
      begin
        if abs(cxCalcEdit3.Value-rSum0)>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� ���).'); delay(10);  end;
        if abs((cxCalcEdit4.Value+cxCalcEdit5.Value)-(rSum10+rN10))>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� 10%).'); delay(10);  end;
        if abs((cxCalcEdit6.Value+cxCalcEdit7.Value)-(rSum20+rN20))>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� 20%).'); delay(10);  end;
      end;

      if bErr then
      begin
        if CommonSet.Single=1 then
        begin
{          if MessageDlg('���������� ����������� ��������� �������. ���������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            bErr:=False;
          end;
}
          bErr:=False;  //������
        end;
      end;


      if bErr then
      begin
{
        if MessageDlg('���������� ����������� ��������� �������. �������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True;  cxButton7.Enabled:=True;
          ViewDoc1.EndUpdate;
          Memo1.Lines.Add('C��������� ��������.'); delay(10);
          exit;
        end;}
        Memo1.Lines.Add('C��������� ��������.'); delay(10);
        if bErrSum then
        begin
//          fmDocCorr.ShowModal;
          Memo1.Lines.Add('��������� ����� ���������.'); delay(10);
          showmessage('��������� ����� ���������. C��������� ��������.');
        end else
        begin
          Memo1.Lines.Add('���������� ����������� ��������� �������.'); delay(10);
          showmessage('���������� ����������� ��������� �������. C��������� ��������.');
        end;
      end else
      begin

      if cxTextEdit1.Tag=0 then IDH:=prMax('DocsIn')+1;

      rSum0:=cxCalcEdit4.EditValue+cxCalcEdit5.EditValue+cxCalcEdit6.EditValue+cxCalcEdit7.EditValue;
      if abs(cxCalcEdit2.EditValue-rSum0)<0.1 then rSum0:=cxCalcEdit2.EditValue;

      //����� �������� ���������
      if cxTextEdit1.Tag=0 then //����������
      begin
          idActP:=its(fShop);
          while length(idActP)<4 do
          begin
            idActP:='0'+idActP;
          end;
          idActP:=idActP+'-1-'+its(IDH);


          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "TTNIn" ( ');
          quA.SQL.Add('Depart,DateInvoice,Number,SCFNumber,IndexPost,CodePost,SummaTovarPost,');
          quA.SQL.Add('OblNDSTovar,LgtNDSTovar,NotNDSTovar,NDS10,OutNDS10,NDS20,OutNDS20,LgtNDS,');
          quA.SQL.Add('OutLgtNDS,NDSTovar,OutNDSTovar,ReturnTovar,OutNDSSNTovar,Boy,OthodiPost,');
          quA.SQL.Add('DolgPost,SummaTovar,Nacenka,Transport,NDSTransport,ReturnTara,Strah,SummaTara,');
          quA.SQL.Add('AmortTara,AcStatus,ChekBuh,ProvodType,NotNDS10,NotNDS20,WithNDS10,WithNDS20,');
          quA.SQL.Add('Crock,OthodiPoluch,Discount,NDS010,NDS020,NDS_10,NDS_20,NSP_10,NSP_20,SCFDate,');
          quA.SQL.Add('GoodsNSP0,GoodsCostNSP,OrderNumber,ID,LinkInvoice,StartTransfer,EndTransfer,GoodsWasteNDS0,');
          quA.SQL.Add('GoodsWasteNDS10,GoodsWasteNDS20) values (');
          quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)+',');  //      Depart
          quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+''',');    // DateInvoice
          quA.SQL.Add(''''+cxTextEdit1.Text+''','); // Number
          quA.SQL.Add(''''+cxTextEdit2.Text+''',');  //      SCFNumber
          quA.SQL.Add(INtToStr(Label4.Tag)+',');     // IndexPost
          quA.SQL.Add(INtToStr(cxButtonEdit1.Tag)+',');  // CodePost
          quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');  // SummaTovarPost
          quA.SQL.Add(fs(rSum0)+',');  // OblNDSTovar
          quA.SQL.Add('0,');    // LgtNDSTovar
          quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');   //NotNDSTovar
          quA.SQL.Add(fs(cxCalcEdit5.EditValue)+',');   //NDS10
          quA.SQL.Add(fs(cxCalcEdit4.EditValue)+',');   //OutNDS10
          quA.SQL.Add(fs(cxCalcEdit7.EditValue)+',');   //NDS20
          quA.SQL.Add(fs(cxCalcEdit6.EditValue)+',');   //OutNDS20
          quA.SQL.Add('0,0,');    // LgtNDS //OutLgtNDS
          quA.SQL.Add(fs(cxCalcEdit5.EditValue+cxCalcEdit7.EditValue)+',');    //NDSTovar
          quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue)+',');    //OutNDSTovar
          quA.SQL.Add('0,');                   //ReturnTovar
          quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');  //OutNDSSNTovar
          quA.SQL.Add('0,0,');               //Boy  //OthodiPost
          quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');        //DolgPost
          quA.SQL.Add(fs(rSum2)+',');          //SummaTovar
          quA.SQL.Add(fs(rSum2-(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue))+','); //Nacenka
          quA.SQL.Add(fs(cxCurrencyEdit4.EditValue)+','); // Transport
          quA.SQL.Add(fs(cxCurrencyEdit5.EditValue)+',0,0,'); //NDSTransport  //ReturnTara  // Strah
          quA.SQL.Add(fs(rSum3)+',');   //SummaTara
          quA.SQL.Add(fs(cxCurrencyEdit6.EditValue)+',');  //AmortTara
          quA.SQL.Add('0,0,'+its(cxComboBox1.ItemIndex)+',0,0,'); // AcStatus  // ChekBuh  // ProvodType  //NotNDS10  //NotNDS20
          quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit5.EditValue)+','); //WithNDS10
          quA.SQL.Add(fs(cxCalcEdit6.EditValue+cxCalcEdit7.EditValue)+','); //WithNDS20
          quA.SQL.Add('0,'); //Crock
          quA.SQL.Add(fs(rSumNec)+','); //OthodiPoluch
          quA.SQL.Add('0,0,0,');      //Discount  //NDS010 //NDS020
          quA.SQL.Add(fs(rSum10M)+',');  //NDS_10
          quA.SQL.Add(fs(rSum20M)+',');   //NDS_20
          quA.SQL.Add('0,0,');   //NSP_10 //NSP_20
          quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit2.Date)+''',0,0,'+its(StrToIntDef(SOnlyDigit(cxTextEdit3.Text),0))+',');  //SCFDate  //GoodsNSP0 //GoodsCostNSP //OrderNumber
          quA.SQL.Add(its(IDH)+',0,0,0,0,0,0'); // ID LinkInvoice  StartTransfer EndTransfer  GoodsWasteNDS0  GoodsWasteNDS10 GoodsWasteNDS20 REZERV
          quA.SQL.Add(')');
          quA.ExecSQL;

{

        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "TTNIn" values (');
        quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)+',');  //      Depart
        quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+''',');    // DateInvoice
        quA.SQL.Add(''''+cxTextEdit1.Text+''','); // Number
        quA.SQL.Add(''''+cxTextEdit2.Text+''',');  //      SCFNumber
        quA.SQL.Add(INtToStr(Label4.Tag)+',');     // IndexPost
        quA.SQL.Add(INtToStr(cxButtonEdit1.Tag)+',');  // CodePost
        quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');  // SummaTovarPost
        quA.SQL.Add(fs(rSum0)+',');  // OblNDSTovar
        quA.SQL.Add('0,');    // LgtNDSTovar
        quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');   //NotNDSTovar
        quA.SQL.Add(fs(cxCalcEdit5.EditValue)+',');   //NDS10
        quA.SQL.Add(fs(cxCalcEdit4.EditValue)+',');   //OutNDS10
        quA.SQL.Add(fs(cxCalcEdit7.EditValue)+',');   //NDS20
        quA.SQL.Add(fs(cxCalcEdit6.EditValue)+',');   //OutNDS20
        quA.SQL.Add('0,0,');    // LgtNDS //OutLgtNDS
        quA.SQL.Add(fs(cxCalcEdit5.EditValue+cxCalcEdit7.EditValue)+',');    //NDSTovar
        quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue)+',');    //OutNDSTovar
        quA.SQL.Add('0,');                   //ReturnTovar
        quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');  //OutNDSSNTovar
        quA.SQL.Add('0,0,');               //Boy  //OthodiPost
        quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');        //DolgPost
        quA.SQL.Add(fs(rSum2)+',');          //SummaTovar
        quA.SQL.Add(fs(rSum2-(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue))+','); //Nacenka
        quA.SQL.Add(fs(cxCurrencyEdit4.EditValue)+','); // Transport
        quA.SQL.Add(fs(cxCurrencyEdit5.EditValue)+',0,0,'); //NDSTransport  //ReturnTara  // Strah
        quA.SQL.Add(fs(rSum3)+',');   //SummaTara
        quA.SQL.Add(fs(cxCurrencyEdit6.EditValue)+',');  //AmortTara
        quA.SQL.Add('0,0,'+its(cxComboBox1.ItemIndex)+',0,0,'); // AcStatus  // ChekBuh  // ProvodType  //NotNDS10  //NotNDS20
        quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit5.EditValue)+','); //WithNDS10
        quA.SQL.Add(fs(cxCalcEdit6.EditValue+cxCalcEdit7.EditValue)+','); //WithNDS20
        quA.SQL.Add('0,'); //Crock
        quA.SQL.Add(fs(rSumNec)+','); //OthodiPoluch
        quA.SQL.Add('0,0,0,');      //Discount  //NDS010 //NDS020
        quA.SQL.Add(fs(rSum10M)+',');  //NDS_10
        quA.SQL.Add(fs(rSum20M)+',');   //NDS_20
        quA.SQL.Add('0,0,');   //NSP_10 //NSP_20
        quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit2.Date)+''',0,0,'+its(StrToIntDef(SOnlyDigit(cxTextEdit3.Text),0))+',');  //SCFDate  //GoodsNSP0 //GoodsCostNSP //OrderNumber
        quA.SQL.Add(IntToStr(IDH)+',0,0,0,0,0,0,'''''); // ID LinkInvoice  StartTransfer EndTransfer  GoodsWasteNDS0  GoodsWasteNDS10 GoodsWasteNDS20 REZERV
        quA.SQL.Add(')');
        quA.ExecSQL;}
      end else //����������
      begin
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('Update "TTNIn" set');
        quA.SQL.Add('Depart='+INtToStr(cxLookupComboBox1.EditValue));
        quA.SQL.Add(',DateInvoice='+''''+ds(cxDateEdit1.Date)+'''');
        quA.SQL.Add(',Number='''+cxTextEdit1.Text+'''');
        quA.SQL.Add(',SCFNumber='''+cxTextEdit2.Text+'''');
        quA.SQL.Add(',IndexPost='+INtToStr(Label4.Tag));
        quA.SQL.Add(',CodePost='+INtToStr(cxButtonEdit1.Tag));
        quA.SQL.Add(',SummaTovarPost='+fs(cxCalcEdit2.EditValue));
        quA.SQL.Add(',OblNDSTovar='+fs(rSum0));
        quA.SQL.Add(',LgtNDSTovar=0');
        quA.SQL.Add(',NotNDSTovar='+fs(cxCalcEdit3.EditValue));
        quA.SQL.Add(',NDS10='+fs(cxCalcEdit5.EditValue));
        quA.SQL.Add(',OutNDS10='+fs(cxCalcEdit4.EditValue));
        quA.SQL.Add(',NDS20='+fs(cxCalcEdit7.EditValue));
        quA.SQL.Add(',OutNDS20='+fs(cxCalcEdit6.EditValue));
        quA.SQL.Add(',LgtNDS=0');
        quA.SQL.Add(',OutLgtNDS=0');
        quA.SQL.Add(',NDSTovar='+fs(cxCalcEdit5.EditValue+cxCalcEdit7.EditValue));
        quA.SQL.Add(',OutNDSTovar='+fs(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue));
        quA.SQL.Add(',ReturnTovar=0');
        quA.SQL.Add(',OutNDSSNTovar='+fs(cxCalcEdit3.EditValue));
        quA.SQL.Add(',Boy=0');
        quA.SQL.Add(',OthodiPost=0');
        quA.SQL.Add(',DolgPost='+fs(cxCalcEdit2.EditValue));
        quA.SQL.Add(',SummaTovar='+fs(rSum2));
        quA.SQL.Add(',Nacenka='+fs(rSum2-rSum1));
        quA.SQL.Add(',Transport='+fs(cxCurrencyEdit4.EditValue));
        quA.SQL.Add(',NDSTransport='+fs(cxCurrencyEdit5.EditValue));
        quA.SQL.Add(',ReturnTara=0');
        quA.SQL.Add(',Strah=0');
        quA.SQL.Add(',SummaTara='+fs(rSum3));
        quA.SQL.Add(',AmortTara='+fs(cxCurrencyEdit6.EditValue));
        quA.SQL.Add(',AcStatus=0');
        quA.SQL.Add(',ChekBuh=0');
        quA.SQL.Add(',ProvodType='+its(cxComboBox1.ItemIndex));
        quA.SQL.Add(',NotNDS10=0');
        quA.SQL.Add(',NotNDS20=0');
        quA.SQL.Add(',WithNDS10='+fs(cxCalcEdit4.EditValue+cxCalcEdit5.EditValue));
        quA.SQL.Add(',WithNDS20='+fs(cxCalcEdit6.EditValue+cxCalcEdit7.EditValue));
        quA.SQL.Add(',Crock=0');
        quA.SQL.Add(',OthodiPoluch='+fs(rSumNec));
        quA.SQL.Add(',Discount=0');
        quA.SQL.Add(',NDS010=0');
        quA.SQL.Add(',NDS020=0');
        quA.SQL.Add(',NDS_10='+fs(rSum10M));
        quA.SQL.Add(',NDS_20='+fs(rSum20M));
        quA.SQL.Add(',NSP_10=0');
        quA.SQL.Add(',NSP_20=0');
        quA.SQL.Add(',SCFDate='+''''+ds(cxDateEdit2.Date)+'''');
        quA.SQL.Add(',GoodsNSP0=0');
        quA.SQL.Add(',GoodsCostNSP=0');
        quA.SQL.Add(',OrderNumber='+its(StrToIntDef(SOnlyDigit(cxTextEdit3.Text),0)) );
        quA.SQL.Add(',ID='+IntToStr(IDH));
        quA.SQL.Add(',StartTransfer=0');
        quA.SQL.Add(',EndTransfer=0');
        quA.SQL.Add(',GoodsWasteNDS0=0');
        quA.SQL.Add(',GoodsWasteNDS10=0');
        quA.SQL.Add(',GoodsWasteNDS20=0');
        quA.SQL.Add(',REZERV=''''');
        quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quA.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
        quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quA.ExecSQL;
      end;

//������� �� ������������

      ptInLn.Active:=False; ptINLn.Active:=True;

      if cxTextEdit1.Tag<>0 then //��� ��������� - ����� ������� ������ ������������
      begin
        ptInLn.CancelRange;
        ptInLn.SetRange([cxDateEdit10.Tag,cxDateEdit10.Date,AnsiToOemConvert(cxTextEdit10.Text)],[cxDateEdit10.Tag,cxDateEdit10.Date,AnsiToOemConvert(cxTextEdit10.Text)]);
        ptInLn.First;
        while not ptInLn.Eof do ptInLn.Delete;
      end;

      ptInLn.CancelRange;
      ptInLn.SetRange([cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)],[cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)]);

//������ ��� ����� - ������� ��� ������� ����� ��� ���������
      ptInLn.First;
      while not ptInLn.Eof do
      begin
        if (ptInLnDepart.AsInteger=cxLookupComboBox1.EditValue)
        and(ptInLnDateInvoice.AsDateTime=cxDateEdit1.Date)
        and(OemToAnsiConvert(ptInLnNumber.AsString)=cxTextEdit1.Text)
        then ptInLn.Delete  //������ �� ������ ������ - ������ ��� �� ������ ���� � ����� ������

        else
        begin
          ptInLn.Next;
          Memo1.Lines.Add('   ������ ��������� ��� ��������� ������������.'); delay(10);
{          teSpecIn.active:=False;
          cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
          prRefrID(quTTnIn,fmDocs1.ViewDocsIn,0);
          ViewDoc1.EndUpdate;
          Memo1.Lines.Add('.'); delay(10);

          exit;}
        end;

      end;

      taSpecIn.First;
      while not taSpecIn.Eof do
      begin
        ptInLn.Append;
        ptInLnDepart.AsInteger:=cxLookupComboBox1.EditValue;
        ptInLnDateInvoice.AsDateTime:=cxDateEdit1.Date;
        ptInLnNumber.AsString:=AnsiToOemConvert(cxTextEdit1.Text); //��������� ���� ����������
        ptInLnIndexPost.AsInteger:=Label4.Tag;
        ptInLnCodePost.AsInteger:=cxButtonEdit1.Tag;
        ptInLnCodeGroup.AsInteger:=0;
        ptInLnCodePodgr.AsInteger:=0;
        ptInLnCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
        ptInLnCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
        ptInLnTovarType.AsInteger:=taSpecInTovarType.AsInteger;
        ptInLnBarCode.AsString:=taSpecInBarCode.AsString;
        ptInLnMediatorCost.AsFloat:=taSpecInNum.AsInteger;
        ptInLnNDSProc.AsFloat:=taSpecInNDSProc.AsFloat;
        ptInLnNDSSum.AsFloat:=taSpecInNDSSum.AsFloat;
        ptInLnOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat;
        ptInLnBestBefore.AsDateTime:=taSpecInBestBefore.AsDateTime;
        ptInLnInvoiceQuant.AsFloat:=0;
        ptInLnKolMest.AsInteger:=taSpecInKolMest.AsInteger;
        ptInLnKolEdMest.AsFloat:=0;
        ptInLnKolWithMest.AsFloat:=taSpecInKolWithMest.AsFloat;
        ptInLnKolWithNecond.AsFloat:=taSpecInKolWithNecond.AsFloat;
        ptInLnKol.AsFloat:=taSpecInKol.AsFloat;
        ptInLnCenaTovar.AsFloat:=taSpecInCenaTovar.AsFloat;
        ptInLnProcent.AsFloat:=taSpecInProcent.AsFloat;
        ptInLnNewCenaTovar.AsFloat:=taSpecInNewCenaTovar.AsFloat;
        ptInLnSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat;
        ptInLnSumCenaTovar.AsFloat:=taSpecInSumCenaTovar.AsFloat;
        ptInLnProcentN.AsFloat:=taSpecInProcentN.AsFloat;
        ptInLnNecond.AsFloat:=taSpecInNecond.AsFloat;
        ptInLnCenaNecond.AsFloat:=taSpecInCenaNecond.AsFloat;
        ptInLnSumNecond.AsFloat:=taSpecInSumNecond.AsFloat;
        ptInLnProcentZ.AsFloat:=taSpecInProcentZ.AsFloat;
        ptInLnZemlia.AsFloat:=taSpecInZemlia.AsFloat;
        ptInLnProcentO.AsFloat:=taSpecInProcentO.AsFloat;
        ptInLnOthodi.AsFloat:=taSpecInOthodi.AsFloat;
        ptInLnCodeTara.AsInteger:=taSpecInCodeTara.AsInteger;
        ptInLnVesTara.AsFloat:=taSpecInVesTara.AsFloat;
        ptInLnCenaTara.AsFloat:=taSpecInCenaTara.AsFloat;
        ptInLnSumVesTara.AsFloat:=taSpecInSumVesTara.AsFloat;
        ptInLnSumCenaTara.AsFloat:=taSpecInSumCenaTara.AsFloat;
        ptInLnChekBuh.AsBoolean:=False;
        ptInLnSertBeginDate.AsDateTime:=date;
        ptInLnSertEndDate.AsDateTime:=date;
        ptInLnSertNumber.AsString:=AnsiToOemConvert(taSpecInGTD.AsString);
        ptInLn.Post;

        taSpecIn.Next;
      end;

      Memo1.Lines.Add('   ��������.'); delay(10);
      Delay(30);

      ptInLn.CancelRange;
      ptInLn.SetRange([cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)],[cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)]);

      ptInLn.First;
      while not ptInLn.Eof do
      begin
      {  if (ptInLnCodeTovar.AsInteger<>56573) and (ptInLnCodeTovar.AsInteger<>71801) then
        begin      }
          rSum1:=rSum1-ptInLnSumCenaTovarPost.AsFloat;
          rSum2:=rSum2-ptInLnSumCenaTovar.AsFloat;
          rSum3:=rSum3-ptInLnSumCenaTara.AsFloat;
       // end;
        ptInLn.Next; delay(10);
      end;

      if (abs(rSum1)>0.001) or (abs(rSum2)>0.001) or (abs(rSum3)>0.001) then
      begin
        Memo1.Lines.Add('  ������ ��� ����������.'); delay(10);
      end else
      begin
        Memo1.Lines.Add('���������� ��.'); delay(10);
      end;

      ptInLn.Active:=False;

      cxTextEdit1.Tag:=IDH;
      cxTextEdit10.Tag:=IDH;
      cxTextEdit10.Text:=cxTextEdit1.Text;
      cxDateEdit10.Date:=cxDateEdit1.Date;
      cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;

      end;
    finally
      teSpecIn.active:=False;
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
      prRefrID(quTTnIn,fmDocs1.ViewDocsIn,0);
      ViewDoc1.EndUpdate;
      Memo1.Lines.Add('.'); delay(10);
    end;
  end;
end;

procedure TfmAddDoc1.acDelAllExecute(Sender: TObject);
begin
  if taSpecIn.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
    end;
  end;
end;

procedure TfmAddDoc1.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc1.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc1.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc1.acSaveDocExecute(Sender: TObject);
begin
  if cxButton1.Enabled then cxButton1.Click;
end;

procedure TfmAddDoc1.acExitDocExecute(Sender: TObject);
begin
  if cxButton2.Enabled then cxButton2.Click;
end;

procedure TfmAddDoc1.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc1.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc1.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc1.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmMC do
    begin
     { quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;}
    end;
  end;
end;

procedure TfmAddDoc1.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc1.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC,iMax,iMaker:INteger;
    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  if cxTextEdit3.tag>0 then exit; //��� ����������� � EDI

  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if taSpecIn.RecordCount>0 then
    if taSpecIn.Locate('BarCode',sBar,[]) then
    begin
      ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
      GridDoc1.SetFocus;
      exit;
    end;


  if prFindBarCMaker(sBar,0,iC,iMaker) then   //���������� ��� ������������� �� Status Barcode
  begin
    iMax:=1;
    if taSpecIn.RecordCount>0 then begin taSpecIn.Last; iMax:=taSpecInNum.AsInteger+1; end;


    with dmMT do
    begin
      if taCards.Active=False then taCards.Active:=True;
      if taCards.FindKey([iC]) then
      begin
//        prFindNacGr(taCardsID.AsInteger,rn1,rn2,rn3,rPr);

        prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

        taSpecIn.Append;
        taSpecInNum.AsInteger:=iMax;

        taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
        taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
        taSpecInBarCode.AsString:=sBar;
        taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat*fmAddDoc1.Label17.Tag;
        taSpecInNDSSum.AsFloat:=0;
        taSpecInOutNDSSum.AsFloat:=0;
        taSpecInBestBefore.AsDateTime:=Date;
        taSpecInKolMest.AsInteger:=1;
        taSpecInKolEdMest.AsFloat:=0;
        taSpecInKolWithMest.AsFloat:=0;
        taSpecInKolWithNecond.AsFloat:=0;
        taSpecInKol.AsFloat:=0;
        taSpecInCenaTovar.AsFloat:=0;
        taSpecInProcent.AsFloat:=0;
        taSpecInNewCenaTovar.AsFloat:=rPr;
        taSpecInSumCenaTovarPost.AsFloat:=0;
        taSpecInSumCenaTovar.AsFloat:=0;
        taSpecInProcentN.AsFloat:=0;
        taSpecInNecond.AsFloat:=0;
        taSpecInCenaNecond.AsFloat:=0;
        taSpecInSumNecond.AsFloat:=0;
        taSpecInProcentZ.AsFloat:=0;
        taSpecInZemlia.AsFloat:=0;
        taSpecInProcentO.AsFloat:=0;
        taSpecInOthodi.AsFloat:=0;
        taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
        taSpecInCodeTara.AsInteger:=0;
        taSpecInVesTara.AsFloat:=0;
        taSpecInCenaTara.AsFloat:=0;
        taSpecInSumCenaTara.AsFloat:=0;

        taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
        taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
        taSpecInNac1.AsFloat:=rn1;
        taSpecInNac2.AsFloat:=rn2;
        taSpecInNac3.AsFloat:=rn3;
        taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
        taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;

        if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
        if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
        taSpecInCenaTovarAvr.AsFloat:=rPrA;

        if iMaker>0 then
        begin
          taSpecInSumVesTara.AsFloat:=iMaker;
          taSpecInsMaker.AsString:=prFindMakerName(iMaker);
        end else
        begin
          if taCardsV12.AsInteger>0 then
          begin
            taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
            taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
          end else
          begin
            taSpecInSumVesTara.AsFloat:=0;
            taSpecInsMaker.AsString:='';
          end;
        end;

        taSpecIn.Post;
      end;
      ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);

      GridDoc1.SetFocus;

    end;
  end else
    showmessage('�������� � �� '+sBar+' ���.');
//    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc1.taSpecIn2KolMestChange(Sender: TField);
Var rQ:Real;
begin
  //���-�� ����            1
  if iCol=4 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;

    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;

  end;
end;

procedure TfmAddDoc1.taSpecIn2KolWithMestChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=5 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2KolWithNecondChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=6 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2NecondChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=14 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2ZemliaChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=18 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2OthodiChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=20 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2CenaTovarChange(Sender: TField);
begin
  if iCol=8 then   //���� ���������� � ���
  begin
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat;
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
    if taSpecInCenaTovar.AsFloat<>0 then
      taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
    else
      taSpecInProcent.AsFloat:=0;

    iCol:=0;
    prSetFields1;
    iCol:=8;
  end;
end;

procedure TfmAddDoc1.taSpecIn2SumCenaTovarPostChange(Sender: TField);
begin
  if iCol=11 then   //����� ���������� � ���
  begin
    if taSpecInKol.AsFloat<>0 then
    begin
      taSpecInCenaTovar.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat;
      taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
      taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
      if (taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat)<>0 then
        taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-(taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat))/(taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat)*100)
      else
        taSpecInProcent.AsFloat:=100;

     { if abs(taSpecInCenaTovar.AsFloat)<0.01 then
      begin
        with dmMT do
        begin
          rPr:=1;
          if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then rPr:=taCardsCena.AsFloat;

          taSpecInNewCenaTovar.AsFloat:=rPr;
          taSpecInSumCenaTovar.AsFloat:=RoundVal(rPr*taSpecInKol.AsFloat);
          if taSpecInCenaTovar.AsFloat<>0 then
           taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
          else
           taSpecInProcent.AsFloat:=100;
        end;
      end;}
    end;
    iCol:=0;
    prSetFields1;
    iCol:=11;
  end;
end;

procedure TfmAddDoc1.taSpecIn2NDSProcChange(Sender: TField);
begin
  if iCol=1 then   //������ ���
  begin
    taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
  end;
end;

procedure TfmAddDoc1.taSpecIn2NDSSumChange(Sender: TField);
//Var rPr:Real;
begin
  // ����� ���
  if iCol=2 then   //����� ���
  begin
    taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat-taSpecInNDSSum.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2OutNDSSumChange(Sender: TField);
Var rPr:Real;
begin
  // ����� ���
  if iCol=3 then   //����� ��� ���
  begin
    if taSpecInNDSProc.AsFloat<>0 then
    begin
      if taSpecInKol.AsFloat<>0 then
      begin
        rPr:=(taSpecInOutNDSSum.AsFloat+taSpecInOutNDSSum.AsFloat/100*taSpecInNDSProc.AsFloat)/taSpecInKol.AsFloat;
        taSpecInSumCenaTovarPost.AsFloat:=taSpecInOutNDSSum.AsFloat+taSpecInOutNDSSum.AsFloat/100*taSpecInNDSProc.AsFloat;
        taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-rPr)/rPr*100);
        taSpecInCenaTovar.AsFloat:=rPr;
      end
      else
      begin
        taSpecInCenaTovar.AsFloat:=0;
        taSpecInProcent.AsFloat:=0;
      end;
    end;
    taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat-taSpecInOutNDSSum.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2ProcentChange(Sender: TField);
//Var rn1,rn2,rn3,rPr:Real;
begin
  if iCol=9 then  //������� �������
  begin
//    prCalcPriceOut(taSpecInCodeTovar.AsInteger,taSpecInCenaTovar.AsFloat,taSpecInKol.AsFloat,rn1,rn2,rn3,rPr);
    taSpecInNewCenaTovar.AsFloat:=rv(taSpecInCenaTovar.AsFloat*(100+taSpecInProcent.AsFloat)/100);
    taSpecInSumCenaTovar.AsFloat:=RoundVal(rv(taSpecInCenaTovar.AsFloat*(100+taSpecInProcent.AsFloat)/100)*taSpecInKol.AsFloat);
  end;
end;

procedure TfmAddDoc1.taSpecIn2NewCenaTovarChange(Sender: TField);
begin
  if iCol=10 then   //���� ��������
  begin
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*taSpecInKol.AsFloat);
    if taSpecInCenaTovar.AsFloat<>0 then
      taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
    else
      taSpecInProcent.AsFloat:=100;
  end;
end;

procedure TfmAddDoc1.taSpecIn2SumCenaTovarChange(Sender: TField);
begin
  if iCol=12 then   //����� ��������
  begin
    if taSpecInKol.AsFloat<>0 then
    begin
      taSpecInNewCenaTovar.AsFloat:=RoundVal(taSpecInSumCenaTovar.AsFloat/taSpecInKol.AsFloat);

      if taSpecInCenaTovar.AsFloat<>0 then
        taSpecInProcent.AsFloat:=RoundVal((taSpecInSumCenaTovar.AsFloat/taSpecInKol.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
      else
        taSpecInProcent.AsFloat:=100;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecIn2ProcentNChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=13 then   //���-�� % ����������
  begin
    taSpecInNecond.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentN.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-(taSpecInKolWithNecond.AsFloat*taSpecInProcentN.AsFloat/100)-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2ProcentZChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=17 then   //���-�� % �����
  begin
    taSpecInZemlia.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentZ.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-taSpecInNecond.AsFloat-taSpecInKolWithNecond.AsFloat*taSpecInProcentZ.AsFloat/100-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2ProcentOChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=19 then   //���-�� % �������
  begin
    taSpecInOthodi.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentO.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInKolWithNecond.AsFloat*taSpecInProcentO.AsFloat/100;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2CenaTaraChange(Sender: TField);
begin
  // ���� ����
  if iCol=22 then
  begin
    taSpecInSumCenaTara.AsFloat:=taSpecInCenaTara.AsFloat*taSpecInKolMest.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecIn2SumCenaTaraChange(Sender: TField);
begin
  if iCol=24 then //����� �� ����
  begin
    if taSpecInKolMest.AsFloat<>0 then
      taSpecInCenaTara.AsFloat:=taSpecInSumCenaTara.AsFloat/taSpecInKolMest.AsFloat;
  end;
end;

procedure TfmAddDoc1.ViewDoc1DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name') then
    begin
      exit; //p
      ViewDoc1Name.Options.Editing:=True;
      ViewDoc1Name.Focused:=True;
    end;
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameTara') then
    begin
      iDirect:=2;
      PosP.Id:=0; PosP.Name:='';
      delay(10);
      if dmMC.quTara.Active=False then dmMC.quTara.Active:=True;
      fmTara.ShowModal;
      iDirect:=0;
      if PosP.Id>0 then
      begin
        taSpecIn.Edit;
        taSpecInCodeTara.AsInteger:=PosP.Id;
        taSpecInNameTara.AsString:=PosP.Name;
        taSpecIn.Post;
      end;
    end;
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1sMaker') then
    begin //����� �������������
      fmMakers.Show;
    end;
  end;  
end;

procedure TfmAddDoc1.cxCurrencyEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxCurrencyEdit5.SetFocus;
    cxCurrencyEdit5.SelectAll;
  end;
end;

procedure TfmAddDoc1.cxCurrencyEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxCurrencyEdit6.SetFocus;
    cxCurrencyEdit6.SelectAll;
  end;
end;

procedure TfmAddDoc1.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxButton1.SetFocus;
  end;
end;

procedure TfmAddDoc1.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc1.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit2.SetFocus;
  end;
end;

procedure TfmAddDoc1.cxButtonEdit1PropertiesChange(Sender: TObject);
Var S,user:String;
begin
  if bOpen then exit;
  S:=cxButtonEdit1.Text;

  if Length(S)>2 then
  begin
    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;



     quCli.Active:=false;
     quCli.Active:=true;
     if quFCli.RecordCount>0 then
     quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);

      if quCliSTBLOCK.AsInteger=1 then
      begin
        showmessage('��������� ������������ ��� �������. ���� ������������ ��������! ��� �������� ����������: '+quCliTYPEVOZ.AsString);
        quFCli.Active:=False;
        quFIP.Active:=False;
        exit;
      end;

      with dmP do
      begin
        quPredzCli.Active:=false;
        quPredzCli.ParamByName('ICLI').AsInteger:=quCliStatus.AsInteger;
        quPredzCli.Active:=true;
        if quPredzClikol.AsInteger>0 then showmessage('� ������� ���������� ���� ���������� �� �������! ��� �������� ����������: '+quCliTYPEVOZ.AsString);
        quPredzCli.Active:=false;
      end;

      if quFCli.RecordCount=1 then
      begin
        Label4.Tag:=1;

        if prCliNDS(1,dmMC.quFCliVendor.AsInteger,0)=1 then
        begin
          Label17.Caption:='���������� ���';
          Label17.Tag:=1;
        end else
        begin
          Label17.Caption:='�� ���������� ���';
          Label17.Tag:=0;
        end;

        if (cxButtonEdit1.Tag<>dmMC.quFCliVendor.AsInteger) and (dmMC.quFCliRaion.AsInteger>0)and(dmMC.quFCliRaion.AsInteger<>6) then
        begin //������ ������������
          cxTextEdit3.Text:='';
          ViewDoc1.BeginUpdate;
          taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
          ViewDoc1.EndUpdate;
        end else
        begin
          if (cxButtonEdit1.Tag<>dmMC.quFCliVendor.AsInteger) then
          begin
            ViewDoc1.BeginUpdate;
            taSpecIn.First;
            while not taSpecIn.Eof do
            begin
              iCol:=1;
              taSpecIn.Edit;
              if Label17.Tag=0 then
              begin
                taSpecInNDSProc.AsFloat:=0;
              end else
              begin
                if dmMT.taCards.FindKey([taSpecInCodeTovar.AsInteger]) then taSpecInNDSProc.AsFloat:=dmMT.taCardsNDS.AsFloat;
              end;
              taSpecIn.Post;

              taSpecIn.Next;
            end;
            ViewDoc1.EndUpdate;
          end;
        end;

        cxButtonEdit1.Tag:=dmMC.quFCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quFCliName.AsString;


       //p   b
        user:=Person.Name;
        if (dmMC.quCliRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
        begin
          cxLabel3.Enabled:=false;                              //���� ������������ ���, �� ��������� �������������� ���� � ����� ����������
         { if((user='Oper1') or (user='Oper2') or (user='Zaved')) then
          begin            }
            ViewDoc1CenaTovar.Options.Editing:=True; // false; p
            ViewDoc1SumCenaTovarPost.Options.Editing:=true; // false; p
            cxLabel15.Enabled:=true;
      {    end
          else               //���� ������������ ������ �� ��������� �������������� ������ �����
          begin
            if (user='OPER CB') then
            begin
               ViewDoc1CenaTovar.Options.Editing:=true;
               ViewDoc1SumCenaTovarPost.Options.Editing:=true;
               cxLabel5.Enabled:=false;//true; p
            end
            else
            begin
              ViewDoc1CenaTovar.Options.Editing:=true;
              ViewDoc1SumCenaTovarPost.Options.Editing:=True; // false; p
              cxLabel15.Enabled:=false;//true; p
            end;
          end;}
        end
        else
        begin
           ViewDoc1CenaTovar.Options.Editing:=true;
           ViewDoc1SumCenaTovarPost.Options.Editing:=true;
           cxLabel3.Enabled:=true;
           cxLabel15.Enabled:=true;
        end;
        //p end


        //quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        if (dmMC.quCliRaion.AsInteger>0)and(dmMC.quCliRaion.AsInteger<>6) then
        begin
          cxTextEdit3.Tag:=1;
          fmAddDoc1.ViewDoc1CodeTovar.Options.Editing:=False;
          fmAddDoc1.ViewDoc1Name.Options.Editing:=False;
          fmAddDoc1.ViewDoc1BarCode.Options.Editing:=False;
        end
        else
        begin
          cxTextEdit3.Text:='';
          cxTextEdit3.Tag:=0;
          fmAddDoc1.ViewDoc1CodeTovar.Options.Editing:=True;
          fmAddDoc1.ViewDoc1Name.Options.Editing:=True;
          fmAddDoc1.ViewDoc1BarCode.Options.Editing:=True;
        end;
        cxLookupComboBox1.SetFocus;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount=1 then
        begin
          if (cxTextEdit3.Tag=1) then
          begin //������ ������������
            ViewDoc1.BeginUpdate;
            taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
            ViewDoc1.EndUpdate;
          end;

          Label4.Tag:=2;

          if prCliNDS(2,dmMC.quFIPCode.AsInteger,0)=1 then
          begin
            Label17.Caption:='���������� ���';
            Label17.Tag:=1;
          end else
          begin
            Label17.Caption:='�� ���������� ���';
            Label17.Tag:=0;
          end;

          cxButtonEdit1.Tag:=dmMC.quFIPCode.AsInteger;
          cxButtonEdit1.Text:=dmMC.quFIPName.AsString;
          cxTextEdit3.Text:='';
          cxTextEdit3.Tag:=0;
          fmAddDoc1.ViewDoc1CodeTovar.Editing:=True;
          fmAddDoc1.ViewDoc1Name.Editing:=True;
          fmAddDoc1.ViewDoc1BarCode.Editing:=True;
          cxLookupComboBox1.SetFocus;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;
end;

procedure TfmAddDoc1.acSetPriceExecute(Sender: TObject);
Var rPriceIn,rPriceM,rRemn:Real;
begin
  //����������� ����
  if {(dmMC.quTTnInRaion.AsInteger>0) or }(dmMC.quCliRaion.AsInteger>0) then exit;   //���� � ���������� ������ ���������� ����� ����� ���

  if cxButton1.Enabled then
  begin
    ViewDoc1.BeginUpdate;
    taSpecIn.First;
    while not taSpecIn.Eof do
    begin
      iCol:=8;
      rPriceIn:=prFLP(taSpecInCodeTovar.AsInteger,rPriceM);
      if (cxLookupComboBox1.EditValue>0)and(taSpecInCodeTovar.AsInteger>0)
        then rRemn:=prFindTRemnDep(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
        else rRemn:=0;


      taSpecIn.Edit;
      taSpecInCenaTovar.AsFloat:=rPriceIn;
      if CommonSet.Single=1 then taSpecInNewCenaTovar.AsFloat:=rPriceM
      else taSpecInNewCenaTovar.AsFloat:=0;
      taSpecInRemn.AsFloat:=rREmn;
      taSpecIn.Post;

      taSpecIn.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecIn.First;
    ViewDoc1.EndUpdate;

    prSetNac;
  end;
end;

procedure TfmAddDoc1.acPrintDocExecute(Sender: TObject);
{Var vPP:TfrPrintPages;
    rSum1,rSum2,rSum3,rN10,rN20:Real;}
begin
  cxButton3.Click;
end;

procedure TfmAddDoc1.acCalcPriceExecute(Sender: TObject);
Var rNac,rPr:Real;
    iR:SmallInt;
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;

begin
// �������� ��������� ��� �� ���������
  if cxButton1.Enabled=False then exit;

  if CommonSet.Single=0 then exit;

  CommonSet.CurNac:=cxCalcEdit1.EditValue;

  prSetNac;

  ViewDoc1.BeginUpdate;

  iR:=0;  // rPr:=0;rNac:=0;
  if cxRadioButton1.Checked then iR:=1;
  if cxRadioButton2.Checked then iR:=2;
  if cxRadioButton3.Checked then iR:=3;
  if cxRadioButton4.Checked then iR:=4;

  if ViewDoc1.Controller.SelectedRecordCount=1 then
  begin
    taSpecIn.First;

    while not taSpecIn.Eof do
    begin
      iCol:=10;
      {
      if taSpecInNac1.AsFloat=0 then rNac:=cxCalcEdit1.EditValue
      else
      begin //������� �� �������
        if taSpecInProcent.AsFloat<(taSpecInNac1.AsFloat-taSpecInNac2.AsFloat) then rNac:=taSpecInNac1.AsFloat
        else rNac:=taSpecInProcent.AsFloat;
      end;

      if taSpecInNac1.AsFloat=-1 then
        if taSpecInCenaTovar.AsFloat<55 then rNac:=36 else rNac:=27;
      }
      
      rNac:=cxCalcEdit1.EditValue;

      rPr:=taSpecInCenaTovar.AsFloat+taSpecInCenaTovar.AsFloat*rNac/100;

      case iR of
      0:begin
          rPr:=RoundVal(rPr);
        end;
      1:begin
          rPr:=RoundHi(rPr*100)/100; //��  ������
        end;
      2:begin
          rPr:=RoundHi(rPr*10)/10; //�� 10 ������
        end;
      3:begin
          rPr:=RoundHi(rPr); //�� 1 ���
        end;
      4:begin
          rPr:=RoundHi(rPr/10)*10; //�� 10 ���
        end;
      end;

      if cxCheckBox2.Checked then rPr:=rPr-0.01;

      taSpecIn.Edit;
      taSpecInNewCenaTovar.AsFloat:=rPr;
      taSpecIn.Post;

      taSpecIn.Next;
    end;
  end else
  begin
    for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
    begin
      rNac:=cxCalcEdit1.EditValue;
      Rec:=ViewDoc1.Controller.SelectedRecords[i];

      iNum:=0;
      for j:=0 to Rec.ValueCount-1 do
        if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin  iNum:=Rec.Values[j]; break; end;

      if iNum>0 then
      begin
        if taSpecIn.Locate('CodeTovar',iNum,[]) then
        begin
          iCol:=10;
          rPr:=taSpecInCenaTovar.AsFloat+taSpecInCenaTovar.AsFloat*rNac/100;

          case iR of
          0:begin
              rPr:=RoundVal(rPr);
            end;
          1:begin
              rPr:=RoundHi(rPr*100)/100; //��  ������
            end;
          2:begin
              rPr:=RoundHi(rPr*10)/10; //�� 10 ������
            end;
          3:begin
              rPr:=RoundHi(rPr); //�� 1 ���
            end;
          4:begin
              rPr:=RoundHi(rPr/10)*10; //�� 10 ���
            end;
          end;

          if cxCheckBox2.Checked then rPr:=rPr-0.01;

          taSpecIn.Edit;
          taSpecInNewCenaTovar.AsFloat:=rPr;
          taSpecIn.Post;
        end;
      end;
    end;
  end;
  iCol:=0;
  ViewDoc1.EndUpdate;
end;

procedure TfmAddDoc1.cxLabel7Click(Sender: TObject);
begin
  GridDoc1.SetFocus;
  acCalcPrice.Execute;
end;

procedure TfmAddDoc1.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
    rPr:Real;
    iTypePr:INteger;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc1.Controller.SelectedRecordCount=0 then exit;

      if MessageDlg('�������� ������� � ����� ���������?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = 3 then iTypePr:=1 else iTypePr:=2;

      CloseTe(taCen);
      for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc1.Controller.SelectedRecords[i];

        iNum:=0;
        rPr:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then iNum:=Rec.Values[j];
          if ViewDoc1.Columns[j].Name='ViewDoc1NewCenaTovar' then rPr:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            if iTypePr=1 then taCenPrice1.AsFloat:=rPr
            else taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      if cxCheckBox1.Checked then
      begin
        RepCenn.ShowPreparedReport;
        CloseTe(taCen);
      end
      else
      begin
        RepCenn.PrintPreparedReport('',1,False,vPP);
        CloseTe(taCen);
      end;
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc1.acPostavExecute(Sender: TObject);
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if taSpecIn.RecordCount>0 then
    begin
      bDrIn:=True;
      
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=taSpecInCodeTovar.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=taSpecInCodeTovar.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=taSpecInName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc1.cxButton3Click(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum2,rSum3,rSumOP,rSumOM,rPrPre,rPriceM:Real;
    iNum,iPlNDS:INteger;
begin
  try
    prFormSumDoc;

    ViewDoc1.BeginUpdate;
    CloseTe(taSp);

    rSum2:=0; rSum3:=0; rSumOP:=0; rSumOM:=0;
    iNUm:=0;

    taSpecIn.First;
    while not taSpecIN.Eof do
    begin
      rSum2:=rSum2+rv(taSpecInSumCenaTovar.AsFloat);
      rSum3:=rSum3+rv(taSpecInSumCenaTara.AsFloat);

      rSumOP:=rSumOP+(taSpecInNecond.AsFloat+taSpecInZemlia.AsFloat+taSpecInOthodi.AsFloat)*taSpecInCenaTovar.AsFloat;
      rSumOM:=rSumOM+(taSpecInNecond.AsFloat+taSpecInZemlia.AsFloat+taSpecInOthodi.AsFloat)*taSpecInNewCenaTovar.AsFloat;

      if taSpecInCodeEdIzm.AsINteger=2 then
      begin
        inc(iNUm);

        taSp.Append;
        taSpiNum.AsInteger:=iNum;
        taSpBarcode.asstring:=taSpecInBarCode.AsString;
        taSpiCode.AsInteger:=taSpecInCodeTovar.AsInteger;
        taSpEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
        taSpName.AsString:=taSpecInName.AsString;
        taSpQuant.AsFloat:=taSpecInKol.AsFloat;
        taSpPriceP.AsFloat:=taSpecInNewCenaTovar.AsFloat;
        taSpPriceM.AsFloat:=taSpecInRealPrice.AsFloat;
        taSpScaleNum.AsString:=prFindPlu(taSpecInCodeTovar.AsInteger);
        taSpPricePPre.AsFloat:=0;
        taSp.Post;
      end;

      taSpecIn.Next;
    end;

    frRepDIN.ReportName:='������ ���.';

    if cxRadioGroup1.ItemIndex=0 then
    begin
      iPlNDS:=1;

      if dmMC.quDepartsSt.Locate('ID',cxLookupComboBox1.EditValue,[]) then iPlNDS:=dmMC.quDepartsStStatus3.AsInteger;
      if iPlNDS=1 then  //���������� ���
      begin
        frRepDIN.LoadFromFile(CommonSet.Reports+'reestrin.frf');

        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['CliFrom']:=cxButtonEdit1.Text;
        frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;
        frVariables.Variable['SumPost']:=cxCalcEdit2.EditValue;
        frVariables.Variable['SumMag']:=rSum2;
        frVariables.Variable['SumTara']:=rSum3;
        frVariables.Variable['SumNac']:=rSum2-cxCalcEdit2.EditValue;
        frVariables.Variable['SumNDS10']:=cxCalcEdit5.EditValue;
        frVariables.Variable['SumNDS20']:=cxCalcEdit7.EditValue;
        frVariables.Variable['SumNDS']:=cxCalcEdit5.EditValue+cxCalcEdit7.EditValue;
        frVariables.Variable['SSumPost']:=MoneyToString(cxCalcEdit2.EditValue,True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
        frVariables.Variable['SSumNac']:=MoneyToString((rSum2-cxCalcEdit2.EditValue),True,False);
        frVariables.Variable['SSumNDS10']:=MoneyToString(cxCalcEdit5.EditValue,True,False);
        frVariables.Variable['SSumNDS20']:=MoneyToString(cxCalcEdit7.EditValue,True,False);
        frVariables.Variable['SSumNDS']:=MoneyToString((cxCalcEdit5.EditValue+cxCalcEdit7.EditValue),True,False);
        frVariables.Variable['SSumMag']:=MoneyToString(rSum2,True,False);
      end;
      if iPlNDS=0 then //�� ���������� ���
      begin
        frRepDIN.LoadFromFile(CommonSet.Reports + 'reestrin1.frf');

        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['CliFrom']:=cxButtonEdit1.Text;
        frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;
        frVariables.Variable['SumPost']:=cxCalcEdit2.EditValue;
        frVariables.Variable['SumMag']:=rSum2;
        frVariables.Variable['SumTara']:=rSum3;
        frVariables.Variable['SumNac']:=rSum2-cxCalcEdit2.EditValue;
        frVariables.Variable['SSumPost']:=MoneyToString(cxCalcEdit2.EditValue,True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
        frVariables.Variable['SSumNac']:=MoneyToString((rSum2-cxCalcEdit2.EditValue),True,False);
        frVariables.Variable['SumOP']:=rSumOP;
        frVariables.Variable['SumOM']:=rSumOM;
        frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
        frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);
      end;
    end;

    if cxRadioGroup1.ItemIndex=1 then
    begin
      frRepDIN.LoadFromFile(CommonSet.Reports + 'reestrin2.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['CliFrom']:=cxButtonEdit1.Text;
      frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;

      frVariables.Variable['SumPost']:=cxCalcEdit2.EditValue;
      frVariables.Variable['SumMag']:=rSum2;
      frVariables.Variable['SumTara']:=rSum3;
      frVariables.Variable['SumNac']:=rSum2-cxCalcEdit2.EditValue;
      frVariables.Variable['SSumPost']:=MoneyToString(cxCalcEdit2.EditValue,True,False);
      frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
      frVariables.Variable['SSumNac']:=MoneyToString((rSum2-cxCalcEdit2.EditValue),True,False);
      frVariables.Variable['SumOP']:=rSumOP;
      frVariables.Variable['SumOM']:=rSumOM;
      frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
      frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);

    end;

    if cxRadioGroup1.ItemIndex=2 then
    begin
      CloseTe(taSp);

      iNUm:=0;

      taSpecIn.First;
      while not taSpecIN.Eof do
      begin
        rPrPre:=prFLP(taSpecInCodeTovar.AsInteger,rPriceM);
        if (rPrPre-taSpecInCenaTovar.AsFloat)>=0.5 then
        begin
          inc(iNUm);

          taSp.Append;
          taSpiNum.AsInteger:=iNum;
          taSpBarcode.asstring:=taSpecInBarCode.AsString;
          taSpiCode.AsInteger:=taSpecInCodeTovar.AsInteger;
          taSpEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
          taSpName.AsString:=taSpecInName.AsString;
          taSpQuant.AsFloat:=taSpecInKol.AsFloat;
          taSpPriceP.AsFloat:=taSpecInCenaTovar.AsFloat;
          taSpPriceM.AsFloat:=taSpecInRealPrice.AsFloat;
          taSpScaleNum.AsString:='';
          taSpPricePPre.AsFloat:=rPrPre;
          taSp.Post;
        end;
        taSpecIn.Next;
      end;


      frRepDIN.LoadFromFile(CommonSet.Reports + 'reestrin3.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['CliFrom']:=cxButtonEdit1.Text;
      frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;

      frVariables.Variable['SumPost']:=cxCalcEdit2.EditValue;
      frVariables.Variable['SumMag']:=rSum2;
      frVariables.Variable['SumTara']:=rSum3;
      frVariables.Variable['SumNac']:=rSum2-cxCalcEdit2.EditValue;
      frVariables.Variable['SSumPost']:=MoneyToString(cxCalcEdit2.EditValue,True,False);
      frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
      frVariables.Variable['SSumNac']:=MoneyToString((rSum2-cxCalcEdit2.EditValue),True,False);
      frVariables.Variable['SumOP']:=rSumOP;
      frVariables.Variable['SumOM']:=rSumOM;
      frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
      frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);

    end;

    frRepDIN.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDIN.ShowPreparedReport
    else frRepDIN.PrintPreparedReport('',1,False,vPP);
  finally
    ViewDoc1.EndUpdate;
  end;
end;

procedure TfmAddDoc1.acTermoPExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;
begin
  //������ �� �����
  if ViewDoc1.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
//              prWritePrinter(StrWk+#$0D+#$0A);
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if prOpenPrinter(CommonSet.TPrintN) then
          begin
            for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewDoc1.Controller.SelectedRecords[i];

              iNum:=0;
              for j:=0 to Rec.ValueCount-1 do
                if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin  iNum:=Rec.Values[j]; break; end;

              if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
//              if iNum>0 then
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  for k:=0 to Str.Count-1 do
                  begin
                    StrWk:=Str[k];
                    if pos('@38.3@',StrWk)>0 then
                    begin
                      insert('E30',StrWk,pos('@38.3@',StrWk));
                      delete(StrWk,pos('@38.3@',StrWk),6)
                    end;
                    if pos('@',StrWk)>0 then
                    begin //���� ��� �� ���� ����������??
                      Str1:=Strwk;

                      Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                      delete(Str1,1,pos('@',Str1));//3.30.36@"

                      Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                      delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                      Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                      if Str4='3' then   //��������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4FullName.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='5' then  //������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //5.30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4NameCu.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='7' then  //�������     7.6.36
                      begin
                        Str5:=INtToStr(iNum);
                      end;
                      if Str4='18' then   //����   18.10.36
                      begin
                        Str5:=ToStr(quFindC4Cena.AsFloat);
                      end;
                      if Str4='31' then    //��
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //31.13.8
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=13;

                        //�� �� ������� ����
                        sBar:=quFindC4BarCode.AsString;
                        if Length(sBar)<13 then
                        begin
                          if Length(sBar)=7 then //������� ����
                          begin
                            sBar:=ToStandart(sBar);
                            l:=13;
                          end;
                        end;
                        Str5:=Copy(sBar,1,l);
                      end;
                      if Str4='39' then   //���-��
                      begin
                        Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                      end;
                      StrWk:=Str2+Str5+Str1;
                    end;
                    prWritePrinter(StrWk+#$0D+#$0A);
                  end;
                end;
                quFindC4.Active:=False;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;

procedure TfmAddDoc1.cxButton4Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc1);
end;

procedure TfmAddDoc1.cxButton5Click(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;
    rPr:Real;

begin
  //�������� � ����
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMC do
  begin
    if ViewDoc1.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDoc1.Controller.SelectedRecords[i];

      iNum:=0;
      rPr:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then iNum:=Rec.Values[j];
        if ViewDoc1.Columns[j].Name='ViewDoc1BarCode' then sBar:=Rec.Values[j];
        if ViewDoc1.Columns[j].Name='ViewDoc1NewCenaTovar' then rPr:=Rec.Values[j];
      end;

      if (iNum>0)and(sBar[1]='2')and((sBar[2]='2')or(sBar[2]='1')or(sBar[2]='3')) then
      begin
        quFindC4.Active:=False;
        quFindC4.ParamByName('IDC').AsInteger:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          taToScale.Append;
          taToScaleId.AsInteger:=iNum;
          taToScaleBarcode.AsString:=sBar;
          taToScaleName.AsString:=quFindC4FullName.AsString;
          taToScaleSrok.AsInteger:=quFindC4SrokReal.AsInteger;
//          taToScalePrice.AsFloat:=quFindC4Cena.AsFloat;
          taToScalePrice.AsFloat:=rPr;
          taToScale.Post;
        end;
        quFindC4.Active:=False;
      end;
    end;
    quScSpr.Active:=False;
    quScSpr.Active:=True;
    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then  prAddToScale(quScSprNumber.AsString);

    quScSpr.Active:=False;
    taToScale.Active:=False;
    ViewDoc1.Controller.ClearSelection;
  end;
  fmAddInScale.Release;
end;

procedure TfmAddDoc1.acScaleViewExecute(Sender: TObject);
Var iNum,j:INteger;
    Rec:TcxCustomGridRecord;
    Str1:String;
begin
 //� �����
  if ViewDoc1.Controller.SelectedRecordCount>0 then
  begin
    Rec:=ViewDoc1.Controller.SelectedRecords[0];

    iNum:=0;
     for j:=0 to Rec.ValueCount-1 do
     begin
       if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin iNum:=Rec.Values[j]; break; end;
     end;

     Str1:=prFindPlu(iNum);

     showmessage('��� ������ '+INtToStr(iNum)+' � �����: '+Str1);
  end;
end;

procedure TfmAddDoc1.acAddToScaleExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;

begin
  //�������� � ����
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMC do
  begin
    if ViewDoc1.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDoc1.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then iNum:=Rec.Values[j];
        if ViewDoc1.Columns[j].Name='ViewDoc1BarCode' then sBar:=Rec.Values[j];
      end;

      if (iNum>0)and(sBar[1]='2')and((sBar[2]='2')or(sBar[1]='1')or(sBar[1]='3')) then
      begin
        quFindC4.Active:=False;
        quFindC4.ParamByName('IDC').AsInteger:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          taToScale.Append;
          taToScaleId.AsInteger:=iNum;
          taToScaleBarcode.AsString:=sBar;
          taToScaleName.AsString:=quFindC4FullName.AsString;
          taToScaleSrok.AsInteger:=quFindC4SrokReal.AsInteger;
          taToScalePrice.AsFloat:=quFindC4Cena.AsFloat;
          taToScale.Post;
        end;
        quFindC4.Active:=False;
      end;
    end;
    quScSpr.Active:=False;
    quScSpr.Active:=True;
    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then  prAddToScale(quScSprNumber.AsString);

    quScSpr.Active:=False;
    taToScale.Active:=False;
    ViewDoc1.Controller.ClearSelection;
  end;
  fmAddInScale.Release;
end;

procedure TfmAddDoc1.cxButton6Click(Sender: TObject);
begin
  prFormSumDoc;
  fmDocCorr.ShowModal;
  if fmDocCorr.ModalResult=mrOk then
  begin
    if cxButton1.Enabled then
    begin
      cxCalcEdit2.EditValue:=fmDocCorr.cxCurrencyEdit7.Value;
      cxCalcEdit3.EditValue:=fmDocCorr.cxCurrencyEdit8.Value;
      cxCalcEdit4.EditValue:=fmDocCorr.cxCurrencyEdit9.Value;
      cxCalcEdit5.EditValue:=fmDocCorr.cxCurrencyEdit10.Value;
      cxCalcEdit6.EditValue:=fmDocCorr.cxCurrencyEdit11.Value;
      cxCalcEdit7.EditValue:=fmDocCorr.cxCurrencyEdit12.Value;
    end else
    begin
      showmessage('��������� ��������� ���������.');
    end;
  end;
end;

procedure TfmAddDoc1.acSetRemnExecute(Sender: TObject);
Var rPriceM,rRemn:Real;
begin
  //��������� ������ �������
  ViewDoc1.BeginUpdate;
  iCol:=0;
  taSpecIn.First;
  while not taSpecIn.Eof do
  begin
    if (cxLookupComboBox1.EditValue>0)and(taSpecInCodeTovar.AsInteger>0) then
    begin
      rPriceM:=prFindPrice(taSpecInCodeTovar.AsInteger);
      rRemn:=prFindTRemnDep(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue);
    end else
    begin
      rPriceM:=0;
      rRemn:=0;
    end;

    taSpecIn.Edit;
    taSpecInRealPrice.AsFloat:=rPriceM;
    taSpecInRemn.AsFloat:=rREmn;
    taSpecIn.Post;

    taSpecIn.Next;
    delay(10);
  end;
  iCol:=0;
  taSpecIn.First;
  ViewDoc1.EndUpdate;

  prSetNac;
end;

procedure TfmAddDoc1.ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iNac,i:Integer;
    StrWk:String;
begin
  iNac:=0; StrWk:='';
  for i:=0 to ViewDoc1.ColumnCount-1 do
  begin
    if ViewDoc1.Columns[i].Name='ViewDoc1iNac' then
      iNac:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
    if ViewDoc1.Columns[i].Name='ViewDoc1iCat' then
      StrWk:=AViewInfo.GridRecord.DisplayTexts[i];
  end;

  //if (StrWk<>'0') then showMessage(StrWk);

  if (StrWk='M') or (StrWk='S') or (StrWk='O') then
  begin
    if (StrWk='M') or (StrWk='O') then ACanvas.Canvas.Brush.Color := $00FFD1A4; //�������
    if (StrWk='S') then
      begin
        ACanvas.Canvas.Brush.Color := $00AAFFFF;  //������
        ACanvas.Font.Color:=clBlack;
      end
  end;

  if iNac=1 then ACanvas.Canvas.Brush.Color := $00B3B3FF; //��� ��������� �������
  if iNac=2 then ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� ������� �������
end;

procedure TfmAddDoc1.taSpecIn2CalcFields(DataSet: TDataSet);
Var rPr,rFixPr,rPrMax,rPrMin:Real;
    iBStatus:INteger;
begin
  taSpecIn2iNac.AsInteger:=0;

//  if abs(taSpecIn2CenaTovar.AsFloat)<0.01 then exit; //��� ������� ���� ���������� ������ �� ���������.
  if abs(taSpecIn2CenaTovar.AsFloat)<0.01 then
  begin
    if (taSpecIn2NewCenaTovar.AsFloat-taSpecIn2RealPrice.AsFloat)>=0.01 then taSpecIn2iNac.AsInteger:=2; //��������� ���� ����������� ���.
    if (taSpecIn2NewCenaTovar.AsFloat-taSpecIn2RealPrice.AsFloat)<=(-0.01) then taSpecIn2iNac.AsInteger:=1; //����� ���� ���� ����. ����������
    exit;
  end;

  rFixPr:=0;
  with dmMT do
  begin
    if taCards.findkey([taSpecIn2CodeTovar.AsInteger]) then rFixPr:=taCardsFixPrice.AsFloat;
{
    if ((rFixPr>0.01)and(taCardsV02.AsInteger<>4))or((rFixPr>0.01)and(taCardsV02.AsInteger=4)and(taCardsReserv5.AsFloat>=taSpecIn2CenaTovarAvr.AsFloat)) then
    begin
      taSpecIn2iNac.AsInteger:=0;
      if (rFixPr-taSpecIn2NewCenaTovar.AsFloat)>=0.01 then taSpecIn2iNac.AsInteger:=1;
      if (taSpecIn2NewCenaTovar.AsFloat-rFixPr)>=0.01 then taSpecIn2iNac.AsInteger:=2;
    end;
}

    if ((rFixPr>0.01)and(taCardsReserv5.AsFloat>0))
    or((rFixPr>0.01)and(taCardsReserv5.AsFloat>=taSpecIn2CenaTovarAvr.AsFloat))
    or((rFixPr>0.01)and(taCardsReserv5.AsFloat<taSpecIn2CenaTovarAvr.AsFloat)and(rFixPr=taCardsCena.AsFloat)) then
    begin
      taSpecIn2iNac.AsInteger:=0;
      if (rFixPr-taSpecIn2NewCenaTovar.AsFloat)>=0.01 then taSpecIn2iNac.AsInteger:=1;
      if (taSpecIn2NewCenaTovar.AsFloat-rFixPr)>=0.01 then taSpecIn2iNac.AsInteger:=2;
    end
{//�� ��������
    if ((rFixPr>0.01)and(taCardsReserv5.AsFloat<taSpecIn2CenaTovarAvr.AsFloat)and(rFixPr<>taCardsCena.AsFloat)) then
    begin
      taSpecIn2iNac.AsInteger:=0;
      if (rFixAfterAkc-taSpecIn2NewCenaTovar.AsFloat)>=0.01 then taSpecIn2iNac.AsInteger:=1;
      if (taSpecIn2NewCenaTovar.AsFloat-rFixAfterAkc)>=0.01 then taSpecIn2iNac.AsInteger:=2;
    end;
}
    else
    begin
      if abs(taSpecIn2Nac1.AsFloat)>0.1 then  //���� ���� �����-�� ������� ����������
      begin
        rPr:=taSpecIn2CenaTovarAvr.AsFloat*(100+taSpecIn2Nac1.AsFloat)/100; //���� ��� ����������
        rPrMax:=taSpecIn2CenaTovarAvr.AsFloat*(100+taSpecIn2Nac1.AsFloat+taSpecIn2Nac3.AsFloat)/100; //������������ ���� ��� ����������
        rPrMin:=taSpecIn2CenaTovarAvr.AsFloat*(100+taSpecIn2Nac1.AsFloat-taSpecIn2Nac2.AsFloat)/100; //����������� ���� ��� ����������
        if (rPr-taSpecIn2NewCenaTovar.AsFloat)>=0.11 then
        begin
          if (rPrMin-taSpecIn2NewCenaTovar.AsFloat)>0 then taSpecIn2iNac.AsInteger:=1; //����� ���� ���� ����. ����������
        end;
        if (rPr-taSpecIn2NewCenaTovar.AsFloat)<=(-0.11) then
        begin
          iBStatus:=prFindBAkciya(taSpecIn2CodeTovar.AsInteger,Trunc(Date+1));
          if (iBStatus=2)or(iBStatus=9) then begin end else
            if (rPrMax-taSpecIn2NewCenaTovar.AsFloat)<0 then taSpecIn2iNac.AsInteger:=2; //��������� ���� ����������� ���.
        end;

      end;
    end;
  end;
end;

procedure TfmAddDoc1.cxButton7Click(Sender: TObject);
Var IdH:Int64;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSumNec,rSum10M,rSum20M, rSum0:Real;
    rSum01,rSum02:Real;
    bErr,bOpr,bErrSum:Boolean;  //,bDel
    bShowErrTab:Boolean;
//    par:Variant;
    sInn,s1,s2,sName,sMess,sInnF:String;
    iTestZ:Integer;
    sNumZ:String;
    sNum:String;
begin
  //�������� � ���������
  with dmMC do
  with dmMt do
  begin
    if cxDateEdit1.Date<=prOpenDate then  begin ShowMessage('������ ������.'); exit; end;
    if cxLookupComboBox1.EditValue<1 then
    begin
      ShowMessage('�������� �����.');
      Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
      exit;
    end;

    if cxButtonEdit1.Tag=0 then
    begin
      ShowMessage('�������� ����������.');
      Memo1.Lines.Add('��������� �� ���������, ���������� ����������!!!');
      exit;
    end;

    sNum:=STestNUm(cxTextEdit1.Text);
    cxTextEdit1.Text:=sNum; delay(10);
    if sNum='' then
    begin
      ShowMessage('������������ ����� ���������.');
      Memo1.Lines.Add('������������ ����� ���������, ���������� ����������!!!');
      exit;
    end;

    if fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False then begin ShowMessage('������ ������ (��������������).'); exit; end;

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton4.Enabled:=False; cxButton7.Enabled:=False;

    IdH:=cxTextEdit1.Tag;

    if taSpecIn.State in [dsEdit,dsInsert] then taSpecIn.Post;

    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;


    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "TTNIn"');
      quC.SQL.Add('where Depart='+INtToStr(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInvoice='''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='''+cxTextEdit1.Text+'''');
      quC.Active:=True;
      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
        exit;
      end;
    end;

    if ptTTNIn.Active=False then ptTTNIn.Active:=True else ptTTNIn.Refresh;
    ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';

    bOpr:=False; //������ �������������
    bShowErrTab:=false;    //������ ������ ������� ������
    try

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ���������� ...'); delay(10);

      CloseTe(teSpecIn);

      Memo1.Lines.Add('��������� ���� ��������...'); delay(10);
      acCalcPriceOut.Execute;         //��������� ���� ���.

      prFormSumDoc; //��� �������� � �����������

      delay(10);

      ViewDoc1.BeginUpdate;

      iCol:=0;
      bErr:=False;
      bErrSum:=False;
      //������� ������ ����
      rSum1:=0; rSum2:=0; rSum3:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSumNec:=0; rSum10M:=0; rSum20M:=0;
      rSum0:=0;
      taSpecIn.First;
      while not taSpecIN.Eof do
      begin
        rSum1:=rSum1+rv(taSpecInSumCenaTovarPost.AsFloat);
        rSum2:=rSum2+rv(taSpecInSumCenaTovar.AsFloat);
        rSum3:=rSum3+RV(taSpecInSumCenaTara.AsFloat);
        rSumNec:=rSumNec+RV(taSpecInSumNecond.AsFloat);

        if (taSpecInCodeTovar.AsInteger=0)and(taSpecInCodeTara.AsInteger=0) then
        begin
          bErr:=True;
          Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. �������� ��� ����� ��� ����.'); delay(10);
        end else
        begin
          if (taSpecInCodeTovar.AsInteger>0) then
          begin
            if taCards.FindKey([taSpecInCodeTovar.AsInteger])=False then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ��� ������.'); delay(10); end;

            if abs(taSpecInKol.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���-��.'); delay(10); end;
            if abs(taSpecInSumCenaTovarPost.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ����������.'); delay(10); end;
            if abs(taSpecInSumCenaTovar.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ��������.'); delay(10); end;

            if taSpecInNDSProc.AsFloat>0 then
            begin
              if ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then
              begin
                if abs(taSpecInNDSSum.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ����� ���.'); delay(10); end;
                if abs(taSpecInOutNDSSum.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ����� ��� ���.'); delay(10); end;
              end;
              
              if taSpecInNDSProc.AsFloat>=15 then
              begin
                rSum20:=rSum20+RoundVal(taSpecInOutNDSSum.AsFloat);
                rN20:=rN20+RoundVal(taSpecInNDSSum.AsFloat);
                rSum20M:=rSum20M+RoundVal(taSpecInSumCenaTovar.AsFloat);
              end else
              begin
                if taSpecInNDSProc.AsFloat>=8 then
                begin
                  rSum10:=rSum10+RoundVal(taSpecInOutNDSSum.AsFloat);
                  rN10:=rN10+RoundVal(taSpecInNDSSum.AsFloat);
                  rSum10M:=rSum10M+RoundVal(taSpecInSumCenaTovar.AsFloat);
                end;
              end;
            end else rSum0:=rSum0+rv(taSpecInSumCenaTovarPost.AsFloat);

            if abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0)>0.001 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'������ ���� ��������� (�����������). '+floatToStr(abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0))); delay(10); end;

          end;
          if taSpecInCodeTara.AsInteger>0 then
          begin
            if abs(taSpecInKolMest.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���-�� ���� (����).'); delay(10); end;
            if abs(taSpecInCenaTara.AsFloat)<0.01 then begin bErr:=True; Memo1.Lines.Add('������ ���. � '+taSpecInNum.AsString+'. ������������ ���� ����.'); delay(10); end;
          end;
        end;

        teSpecIn.Append;
        teSpecInNum.AsInteger:=taSpecInNum.AsInteger;
        teSpecInCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
        teSpecInCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
        teSpecInBarCode.asString:=taSpecInBarCode.asString;
        teSpecInNDSProc.AsFloat:=taSpecInNDSProc.AsFloat;
        teSpecInOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat;
        teSpecInBestBefore.AsDateTime:=taSpecInBestBefore.AsDateTime;
        teSpecInKolMest.AsInteger:=taSpecInKolMest.AsInteger;
        teSpecInEdMest.AsFloat:=taSpecInKolEdMest.AsFloat;
        teSpecInKolWithMest.AsFloat:=taSpecInKolWithMest.AsFloat;
        teSpecInKolWithNecond.AsFloat:=taSpecInKolWithNecond.AsFloat;
        teSpecInKol.AsFloat:=taSpecInKol.AsFloat;
        teSpecInCenaTovar.AsFloat:=taSpecInCenaTovar.AsFloat;
        teSpecInProcent.AsFloat:=taSpecInProcent.AsFloat;
        teSpecInNewCenaTovar.AsFloat:=taSpecInNewCenaTovar.AsFloat;
        teSpecInSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat;
        teSpecInSumCenaTovar.AsFloat:=taSpecInSumCenaTovar.AsFloat;
        teSpecInProcentN.AsFloat:=taSpecInProcentN.AsFloat;
        teSpecInNecond.AsFloat:=taSpecInNecond.AsFloat;
        teSpecInCenaNecond.AsFloat:=taSpecInCenaNecond.AsFloat;
        teSpecInSumNecond.AsFloat:=taSpecInSumNecond.AsFloat;
        teSpecInProcentZ.AsFloat:=taSpecInProcentZ.AsFloat;
        teSpecInZemlia.AsFloat:=taSpecInZemlia.AsFloat;
        teSpecInProcentO.AsFloat:=taSpecInProcentO.AsFloat;
        teSpecInOthodi.AsFloat:=taSpecInOthodi.AsFloat;
        teSpecInCodeTara.AsInteger:=taSpecInCodeTara.AsInteger;
        teSpecInVesTara.AsFloat:=taSpecInVesTara.AsFloat;
        teSpecInCenaTara.AsFloat:=taSpecInCenaTara.AsFloat;
        teSpecInSumVesTara.AsFloat:=taSpecInSumVesTara.AsFloat;
        teSpecInSumCenaTara.AsFloat:=taSpecInSumCenaTara.AsFloat;
        teSpecInTovarType.AsInteger:=taSpecInTovarType.AsInteger;
        teSpecInNDSSum.AsFloat:=taSpecInNDSSum.AsFloat;
        teSpecIN.Post;

        taSpecIn.Next;
      end;

      if abs(rSum1-rSum20-rN20-rSum10-rN10-rSum0)>0.001 then begin bErr:=True; Memo1.Lines.Add('������ ���� ���������.'); delay(10); end;

//      cxCurrencyEdit1.EditValue:=rSum1+cxCurrencyEdit4.EditValue+cxCurrencyEdit5.EditValue+cxCurrencyEdit6.EditValue;
      cxCurrencyEdit1.EditValue:=cxCalcEdit2.Value+cxCurrencyEdit4.EditValue+cxCurrencyEdit5.EditValue+cxCurrencyEdit6.EditValue;

      if cxTextEdit1.Tag>0 then
      begin
        if abs(cxCalcEdit3.Value-rSum0)>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� ���).'); delay(10);  end;
        if abs((cxCalcEdit4.Value+cxCalcEdit5.Value)-(rSum10+rN10))>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� 10%).'); delay(10);  end;
        if abs((cxCalcEdit6.Value+cxCalcEdit7.Value)-(rSum20+rN20))>0.001 then begin  bErr:=True; bErrSum:=True; Memo1.Lines.Add('�������������� ���� ��������� (��� 20%).'); delay(10);  end;
      end;

      if bErr then
      begin
        if CommonSet.Single=1 then
        begin
          bErr:=False;  //������
        end;
      end;

      if bErr then
      begin
{        if MessageDlg('���������� ������� � ������� ������� ��� (���� ����������� ������� ���-�� ��� ����). �������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
          ViewDoc1.EndUpdate;
          Memo1.Lines.Add('���������� �������� ...'); delay(10);
          exit;
        end;}

        Memo1.Lines.Add('���������� �������� ...'); delay(10);

        if bErrSum then
        begin
          Memo1.Lines.Add('��������� ����� ���������.'); delay(10);
        end else
        begin
          Memo1.Lines.Add('���������� ����������� ��������� �������.'); delay(10);
        end;
      end else
      begin

        if cxTextEdit1.Tag=0 then IDH:=prMax('DocsIn')+1;

        rSum0:=cxCalcEdit4.EditValue+cxCalcEdit5.EditValue+cxCalcEdit6.EditValue+cxCalcEdit7.EditValue;
        if abs(cxCalcEdit2.EditValue-rSum0)<0.1 then rSum0:=cxCalcEdit2.EditValue;

        //����� �������� ���������
        if cxTextEdit1.Tag=0 then //����������
        begin
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "TTNIn" values (');
          quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)+',');  //      Depart
          quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+''',');    // DateInvoice
          quA.SQL.Add(''''+cxTextEdit1.Text+''','); // Number
          quA.SQL.Add(''''+cxTextEdit2.Text+''',');  //      SCFNumber
          quA.SQL.Add(INtToStr(Label4.Tag)+',');     // IndexPost
          quA.SQL.Add(INtToStr(cxButtonEdit1.Tag)+',');  // CodePost
          quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');  // SummaTovarPost
          quA.SQL.Add(fs(rSum0)+',');  // OblNDSTovar
          quA.SQL.Add('0,');    // LgtNDSTovar
          quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');   //NotNDSTovar
          quA.SQL.Add(fs(cxCalcEdit5.EditValue)+',');   //NDS10
          quA.SQL.Add(fs(cxCalcEdit4.EditValue)+',');   //OutNDS10
          quA.SQL.Add(fs(cxCalcEdit7.EditValue)+',');   //NDS20
          quA.SQL.Add(fs(cxCalcEdit6.EditValue)+',');   //OutNDS20
          quA.SQL.Add('0,0,');    // LgtNDS //OutLgtNDS
          quA.SQL.Add(fs(cxCalcEdit5.EditValue+cxCalcEdit7.EditValue)+',');    //NDSTovar
          quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue)+',');    //OutNDSTovar
          quA.SQL.Add('0,');                   //ReturnTovar
          quA.SQL.Add(fs(cxCalcEdit3.EditValue)+',');  //OutNDSSNTovar
          quA.SQL.Add('0,0,');               //Boy  //OthodiPost
          quA.SQL.Add(fs(cxCalcEdit2.EditValue)+',');        //DolgPost
          quA.SQL.Add(fs(rSum2)+',');          //SummaTovar
          quA.SQL.Add(fs(rSum2-(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue))+','); //Nacenka
          quA.SQL.Add(fs(cxCurrencyEdit4.EditValue)+','); // Transport
          quA.SQL.Add(fs(cxCurrencyEdit5.EditValue)+',0,0,'); //NDSTransport  //ReturnTara  // Strah
          quA.SQL.Add(fs(rSum3)+',');   //SummaTara
          quA.SQL.Add(fs(cxCurrencyEdit6.EditValue)+',');  //AmortTara
          quA.SQL.Add('0,0,'+its(cxComboBox1.ItemIndex)+',0,0,'); // AcStatus  // ChekBuh  // ProvodType  //NotNDS10  //NotNDS20
          quA.SQL.Add(fs(cxCalcEdit4.EditValue+cxCalcEdit5.EditValue)+','); //WithNDS10
          quA.SQL.Add(fs(cxCalcEdit6.EditValue+cxCalcEdit7.EditValue)+','); //WithNDS20
          quA.SQL.Add('0,'); //Crock
          quA.SQL.Add(fs(rSumNec)+','); //OthodiPoluch
          quA.SQL.Add('0,0,0,');      //Discount  //NDS010 //NDS020
          quA.SQL.Add(fs(rSum10M)+',');  //NDS_10
          quA.SQL.Add(fs(rSum20M)+',');   //NDS_20
          quA.SQL.Add('0,0,');   //NSP_10 //NSP_20
          quA.SQL.Add(''''+FormatDateTime(sCrystDate,cxDateEdit2.Date)+''',0,0,'+its(StrToIntDef(SOnlyDigit(cxTextEdit3.Text),0))+',');  //SCFDate  //GoodsNSP0 //GoodsCostNSP //OrderNumber
          quA.SQL.Add(IntToStr(IDH)+',0,0,0,0,0,0,'''''); // ID LinkInvoice  StartTransfer EndTransfer  GoodsWasteNDS0  GoodsWasteNDS10 GoodsWasteNDS20 REZERV
          quA.SQL.Add(')');
          quA.ExecSQL;
        end else //����������
        begin
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('Update "TTNIn" set');
          quA.SQL.Add('Depart='+INtToStr(cxLookupComboBox1.EditValue));
          quA.SQL.Add(',DateInvoice='+''''+ds(cxDateEdit1.Date)+'''');
          quA.SQL.Add(',Number='''+cxTextEdit1.Text+'''');
          quA.SQL.Add(',SCFNumber='''+cxTextEdit2.Text+'''');
          quA.SQL.Add(',IndexPost='+INtToStr(Label4.Tag));
          quA.SQL.Add(',CodePost='+INtToStr(cxButtonEdit1.Tag));
          quA.SQL.Add(',SummaTovarPost='+fs(cxCalcEdit2.EditValue));
          quA.SQL.Add(',OblNDSTovar='+fs(rSum0));
          quA.SQL.Add(',LgtNDSTovar=0');
          quA.SQL.Add(',NotNDSTovar='+fs(cxCalcEdit3.EditValue));
          quA.SQL.Add(',NDS10='+fs(cxCalcEdit5.EditValue));
          quA.SQL.Add(',OutNDS10='+fs(cxCalcEdit4.EditValue));
          quA.SQL.Add(',NDS20='+fs(cxCalcEdit7.EditValue));
          quA.SQL.Add(',OutNDS20='+fs(cxCalcEdit6.EditValue));
          quA.SQL.Add(',LgtNDS=0');
          quA.SQL.Add(',OutLgtNDS=0');
          quA.SQL.Add(',NDSTovar='+fs(cxCalcEdit5.EditValue+cxCalcEdit7.EditValue));
          quA.SQL.Add(',OutNDSTovar='+fs(cxCalcEdit4.EditValue+cxCalcEdit6.EditValue));
          quA.SQL.Add(',ReturnTovar=0');
          quA.SQL.Add(',OutNDSSNTovar='+fs(cxCalcEdit3.EditValue));
          quA.SQL.Add(',Boy=0');
          quA.SQL.Add(',OthodiPost=0');
          quA.SQL.Add(',DolgPost='+fs(cxCalcEdit2.EditValue));
          quA.SQL.Add(',SummaTovar='+fs(rSum2));
          quA.SQL.Add(',Nacenka='+fs(rSum2-rSum1));
          quA.SQL.Add(',Transport='+fs(cxCurrencyEdit4.EditValue));
          quA.SQL.Add(',NDSTransport='+fs(cxCurrencyEdit5.EditValue));
          quA.SQL.Add(',ReturnTara=0');
          quA.SQL.Add(',Strah=0');
          quA.SQL.Add(',SummaTara='+fs(rSum3));
          quA.SQL.Add(',AmortTara='+fs(cxCurrencyEdit6.EditValue));
          quA.SQL.Add(',AcStatus=0');
          quA.SQL.Add(',ChekBuh=0');
          quA.SQL.Add(',ProvodType='+its(cxComboBox1.ItemIndex));
          quA.SQL.Add(',NotNDS10=0');
          quA.SQL.Add(',NotNDS20=0');
          quA.SQL.Add(',WithNDS10='+fs(cxCalcEdit4.EditValue+cxCalcEdit5.EditValue));
          quA.SQL.Add(',WithNDS20='+fs(cxCalcEdit6.EditValue+cxCalcEdit7.EditValue));
          quA.SQL.Add(',Crock=0');
          quA.SQL.Add(',OthodiPoluch='+fs(rSumNec));
          quA.SQL.Add(',Discount=0');
          quA.SQL.Add(',NDS010=0');
          quA.SQL.Add(',NDS020=0');
          quA.SQL.Add(',NDS_10='+fs(rSum10M));
          quA.SQL.Add(',NDS_20='+fs(rSum20M));
          quA.SQL.Add(',NSP_10=0');
          quA.SQL.Add(',NSP_20=0');
          quA.SQL.Add(',SCFDate='+''''+ds(cxDateEdit2.Date)+'''');
          quA.SQL.Add(',GoodsNSP0=0');
          quA.SQL.Add(',GoodsCostNSP=0');
          quA.SQL.Add(',OrderNumber='+its(StrToIntDef(SOnlyDigit(cxTextEdit3.Text),0)) );
          quA.SQL.Add(',ID='+IntToStr(IDH));
          quA.SQL.Add(',StartTransfer=0');
          quA.SQL.Add(',EndTransfer=0');
          quA.SQL.Add(',GoodsWasteNDS0=0');
          quA.SQL.Add(',GoodsWasteNDS10=0');
          quA.SQL.Add(',GoodsWasteNDS20=0');
          quA.SQL.Add(',REZERV=''''');
          quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
          quA.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
          quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
          quA.ExecSQL;
        end;

        //������� �� ������������

        if ptInLn.Active=False then ptINLn.Active:=True;

        if cxTextEdit1.Tag<>0 then //��� ��������� - ����� ������� ������ ������������
        begin
          ptInLn.CancelRange;
          ptInLn.SetRange([cxDateEdit10.Tag,cxDateEdit10.Date,AnsiToOemConvert(cxTextEdit10.Text)],[cxDateEdit10.Tag,cxDateEdit10.Date,AnsiToOemConvert(cxTextEdit10.Text)]);
          ptInLn.First;
          while not ptInLn.Eof do ptInLn.Delete;
        end;

        ptInLn.CancelRange;
        ptInLn.SetRange([cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)],[cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)]);

  //������ ��� ����� - ������� ��� ������� ����� ��� ���������
        ptInLn.First;
        while not ptInLn.Eof do
        begin
          if (ptInLnDepart.AsInteger=cxLookupComboBox1.EditValue)
          and(ptInLnDateInvoice.AsDateTime=cxDateEdit1.Date)
          and(OemToAnsiConvert(ptInLnNumber.AsString)=cxTextEdit1.Text)
          then ptInLn.Delete  //������ �� ������ ������ - ������ ��� �� ������ ���� � ����� ������

          else
          begin
            ptInLn.Next;
            Memo1.Lines.Add('   ������ ��������� ��� ��������� ������������.'); delay(10);
  {          teSpecIn.active:=False;
            cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
            prRefrID(quTTnIn,fmDocs1.ViewDocsIn,0);
            ViewDoc1.EndUpdate;
            Memo1.Lines.Add('.'); delay(10);

            exit;}
          end;

        end;

        taSpecIn.First;
        while not taSpecIn.Eof do
        begin
          ptInLn.Append;
          ptInLnDepart.AsInteger:=cxLookupComboBox1.EditValue;
          ptInLnDateInvoice.AsDateTime:=cxDateEdit1.Date;
          ptInLnNumber.AsString:=AnsiToOemConvert(cxTextEdit1.Text); //��������� ���� ����������
          ptInLnIndexPost.AsInteger:=Label4.Tag;
          ptInLnCodePost.AsInteger:=cxButtonEdit1.Tag;
          ptInLnCodeGroup.AsInteger:=0;
          ptInLnCodePodgr.AsInteger:=0;
          ptInLnCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
          ptInLnCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
          ptInLnTovarType.AsInteger:=taSpecInTovarType.AsInteger;
          ptInLnBarCode.AsString:=taSpecInBarCode.AsString;
          ptInLnMediatorCost.AsFloat:=taSpecInNum.AsInteger;
          ptInLnNDSProc.AsFloat:=taSpecInNDSProc.AsFloat;
          ptInLnNDSSum.AsFloat:=taSpecInNDSSum.AsFloat;
          ptInLnOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat;
          ptInLnBestBefore.AsDateTime:=taSpecInBestBefore.AsDateTime;
          ptInLnInvoiceQuant.AsFloat:=0;
          ptInLnKolMest.AsInteger:=taSpecInKolMest.AsInteger;
          ptInLnKolEdMest.AsFloat:=0;
          ptInLnKolWithMest.AsFloat:=taSpecInKolWithMest.AsFloat;
          ptInLnKolWithNecond.AsFloat:=taSpecInKolWithNecond.AsFloat;
          ptInLnKol.AsFloat:=taSpecInKol.AsFloat;
          ptInLnCenaTovar.AsFloat:=taSpecInCenaTovar.AsFloat;
          ptInLnProcent.AsFloat:=taSpecInProcent.AsFloat;
          ptInLnNewCenaTovar.AsFloat:=taSpecInNewCenaTovar.AsFloat;
          ptInLnSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat;
          ptInLnSumCenaTovar.AsFloat:=taSpecInSumCenaTovar.AsFloat;
          ptInLnProcentN.AsFloat:=taSpecInProcentN.AsFloat;
          ptInLnNecond.AsFloat:=taSpecInNecond.AsFloat;
          ptInLnCenaNecond.AsFloat:=taSpecInCenaNecond.AsFloat;
          ptInLnSumNecond.AsFloat:=taSpecInSumNecond.AsFloat;
          ptInLnProcentZ.AsFloat:=taSpecInProcentZ.AsFloat;
          ptInLnZemlia.AsFloat:=taSpecInZemlia.AsFloat;
          ptInLnProcentO.AsFloat:=taSpecInProcentO.AsFloat;
          ptInLnOthodi.AsFloat:=taSpecInOthodi.AsFloat;
          ptInLnCodeTara.AsInteger:=taSpecInCodeTara.AsInteger;
          ptInLnVesTara.AsFloat:=taSpecInVesTara.AsFloat;
          ptInLnCenaTara.AsFloat:=taSpecInCenaTara.AsFloat;
          ptInLnSumVesTara.AsFloat:=taSpecInSumVesTara.AsFloat;
          ptInLnSumCenaTara.AsFloat:=taSpecInSumCenaTara.AsFloat;
          ptInLnChekBuh.AsBoolean:=False;
          ptInLnSertBeginDate.AsDateTime:=date;
          ptInLnSertEndDate.AsDateTime:=date;
          ptInLnSertNumber.AsString:=AnsiToOemConvert(taSpecInGTD.AsString);
          ptInLn.Post;

          taSpecIn.Next;
        end;

        Memo1.Lines.Add('   ��������.'); delay(10);
        Delay(30);

        ptInLn.CancelRange;
        ptInLn.SetRange([cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)],[cxLookupComboBox1.EditValue,cxDateEdit1.Date,AnsiToOemConvert(cxTextEdit1.Text)]);

        rSum01:=rSum1;
        rSum02:=rSum2;

        ptInLn.First;
        while not ptInLn.Eof do
        begin
          rSum1:=rSum1-ptInLnSumCenaTovarPost.AsFloat;
          rSum2:=rSum2-ptInLnSumCenaTovar.AsFloat;
          rSum3:=rSum3-ptInLnSumCenaTara.AsFloat;

          ptInLn.Next; delay(10);
        end;

        if (abs(rSum1)>0.001) or (abs(rSum2)>0.001) or (abs(rSum3)>0.001) then
        begin
          Memo1.Lines.Add('  ������ ��� ����������.'); delay(10);
        end else
        begin
          Memo1.Lines.Add('���������� ��.'); delay(10);
        end;

        ptInLn.Active:=False;

        cxTextEdit1.Tag:=IDH;
        cxTextEdit10.Tag:=IDH;
        cxTextEdit10.Text:=cxTextEdit1.Text;
        cxDateEdit10.Date:=cxDateEdit1.Date;
        cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;

       // ����� ������������ ���
        delay(10);

        if CanDo('prOnDocIn') then
        begin
          Memo1.Lines.Add(''); delay(50);
          Memo1.Lines.Add('   ��������� ...  ����� ...'); delay(10);

          if taCards.Active=False then taCards.Active:=True;

          iTestZ:=0;
          if cxTextEdit3.Tag>=1 then
          begin
            //��� ������
            sNumZ:=cxTextEdit3.Text;
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
  //          quZakHNum.ParamByName('IDATE').AsINteger:=Trunc(cxDateEdit1.Date);
            quZakHNum.ParamByName('ICLI').AsInteger:=cxButtonEdit1.Tag;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;

            if iTestZ>0 then
            begin
              CloseTe(teSpecZ);

              if ptZSpec.Active=False then ptZSpec.Active:=True;
              ptZSpec.CancelRange;
              ptZSpec.IndexFieldNames:='IDH;IDS';
              ptZSpec.SetRange([iTestZ],[iTestZ]);
              ptZSpec.First;
              while not ptZSpec.Eof do
              begin
                teSpecZ.Append;
                teSpecZCode.AsInteger:=ptZSpecCODE.AsInteger;
                teSpecZCliQuantN.AsFloat:=ptZSpecCLIQUANTN.AsFloat;
                teSpecZCliPriceN.AsFloat:=rv(ptZSpecCLIPRICEN.AsFloat);
                teSpecZCliPrice0N.AsFloat:=rv(ptZSpecCLIPRICE0N.AsFloat);
                teSpecZCliSumInN.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICEN.AsFloat));
                teSpecZCliSumIn0N.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICE0N.AsFloat));
                teSpecZNNum.AsInteger:=ptZSpecCLINNUM.AsInteger;

                teSpecZ.Post;

                ptZSpec.Next;
              end;
            end;
          end;

          bShowErrTab:=false;
          closete(fmSpInEr.taSpEr);

          if dmMC.quCli.active=false then dmMC.quCli.active:=true;
          if ((dmMC.quFCli.Active=true) and (dmMC.quFCli.RecordCount>0)) then dmMc.quCli.Locate('Vendor',quFCliVendor.AsInteger,[])
          else quCli.Locate('Vendor',cxButtonEdit1.Tag,[]);

          bOpr:=True;

          taSpecIn.First;
          while (taSpecIn.Eof=False) {and (bOpr=True)} do
          begin       //�������                 //�������
            if (taSpecIniNac.AsInteger=1)or(taSpecIniNac.AsInteger=2) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801))  then
            begin
              if CommonSet.RC<>1 then
              begin
                bOpr:=False;
                sMess:='�������� !!! ������ �'+its(fmAddDoc1.taSpecInNum.AsInteger)+' ������� �� ������������� �������������� �������. ������������� ����������...';
                prWH(sMess,Memo1);

              end;
            end;

            if fShop<>0 then //���� ����������� �� ������
            begin
              if (taSpecInCodeTovar.AsInteger>0) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then
              begin
                if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then
                begin
                  sName:=OemToAnsiConvert(taCardsName.AsString);
                  if iTestZ=0 then  //������� ������������� ��� ������
                  begin
                    if sName[1]='*' then
                    begin
                      bOpr:=False;
                      sMess:='�������� !!! ������ �'+its(fmAddDoc1.taSpecInNum.AsInteger)+' ������������ ������ ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...';
                      prWH(sMess,Memo1);
                    end;
                    if taCardsStatus.AsInteger>=100 then
                    begin
                      bOpr:=False;
                      sMess:='�������� !!! ������ �'+its(fmAddDoc1.taSpecInNum.AsInteger)+' ������������ ������ ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...';
                      prWH(sMess,Memo1);
                    end;
                  end else //���� �� ������
                  begin
                    if teSpecZ.Locate('CODE',taSpecInCodeTovar.AsInteger,[]) then
                    begin
                      if taCardsStatus.AsInteger>=100 then
                      begin
                        taCards.Edit;
                        taCardsStatus.AsInteger:=1;
                        taCards.Post;
                      end;
                    end;
                  end;


                  //��������� � �������
                  if (iTestZ>0) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then  //�������� �� ������� � ������������ ������
                  begin
                    if teSpecZ.Locate('CODE',taSpecInCodeTovar.AsInteger,[]) then
                    begin
                      if (dmMC.quCliRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
                      begin
                        if taCardsBHTStatus.AsInteger=0 then
                        begin //��� �� ������
                          if (fmAddDoc1.teSpecZCliQuantN.AsFloat+0.2)<taSpecInKol.AsFloat then
                          begin
                            bOpr:=False;
                            bShowErrTab:=true;
                            sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � ������� �� ����������.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(fmAddDoc1.teSpecZCliQuantN.AsFloat)+' �� ��������� '+fs(taSpecInKol.AsFloat)+') ������������� ����������...';
                            prWH(sMess,Memo1);
                            with fmSpInEr do
                            begin
                              if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                              taSpEr.Append;
                              taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                              taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                              taSpErNameCode.AsString:=taSpecInName.AsString;
                              taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                              taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                              taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                              taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                              taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                              taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                              taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                              taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                              taSpEr.Post;
                            end;

                          end;
                        end else  //=1 ��� ������ + 15%
                        begin
                          if (teSpecZCliQuantN.AsFloat+(0.15)*teSpecZCliQuantN.AsFloat)<taSpecInKol.AsFloat then
                          begin
                            bOpr:=False;
                            bShowErrTab:=true;
                            sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � ������� �� ����������.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(fmAddDoc1.teSpecZCliQuantN.AsFloat)+' �� ��������� '+fs(taSpecInKol.AsFloat)+') ������������� ����������...';
                            prWH(sMess,Memo1);
                            with fmSpInEr do
                            begin
                              if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                              taSpEr.Append;
                              taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                              taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                              taSpErNameCode.AsString:=taSpecInName.AsString;
                              taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                              taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                              taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                              taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                              taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                              taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                              taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                              taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                              taSpEr.Post;
                            end;
                          end;
                        end;

//                        if abs(taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat)>0.05 then

                            //���� ���� ������ ��� � ������ �� 5 ��� ��� ������ �� 1 ����� - ���������

                        if ((taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat)>0.05) or ((teSpecZCliPriceN.AsFloat-taSpecInCenaTovar.AsFloat)>1)then
                        begin
                          bOpr:=False;
                          bShowErrTab:=true;
                          sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � ������� �� ����.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(teSpecZCliPriceN.AsFloat)+' �� ��������� '+fs(taSpecInCenaTovar.AsFloat)+') ������������� ����������...';
                          prWH(sMess,Memo1);
                          with fmSpInEr do
                          begin
                            if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                            taSpEr.Append;
                            taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                            taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                            taSpErNameCode.AsString:=taSpecInName.AsString;
                            taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                            taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                            taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                            taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                            taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                            taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                            taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                            taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                            taSpEr.Post;
                          end;

                        end;
                      end;
                    end else
                    begin
                      bOpr:=False;
                      sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � �������.( ��� - '+taSpecInCodeTovar.AsString+' ����������� � ������) ������������� ����������...';
                      prWH(sMess,Memo1);
                    end;
                  end;
                end else
                begin
                  bOpr:=False;
                  sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ��� ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...';
                  prWH(sMess,Memo1);
                end;
              end;
            end;

            taSpecIn.Next;
            delay(10);
          end;

          teSpecZ.Active:=False;

          if bOpr = False then
          begin
          //  Memo1.Lines.Add(sMess);
            Showmessage('���� ������. ������������� ����������!');
            if (bShowErrTab=true) then
            begin
              fmSpInEr.taSpEr.First;
              fmSpInEr.Show;
              exit;
            end;
            fmSpInEr.taSpEr.Active:=false;
            exit;
          end else
          begin
            prAddHist(1,IDH,trunc(cxDateEdit1.Date),cxTextEdit1.Text,'�����.',1);

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=cxLookupComboBox1.EditValue;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(cxDateEdit1.Date);
            quSpecIn1.ParamByName('SNUM').AsString:=cxTextEdit1.Text;
            quSpecIn1.Active:=True;

            quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
            while not quSpecIn1.Eof do
            begin
              rSum01:=rSum01-quSpecIn1SumCenaTovarPost.AsFloat;
              rSum02:=rSum02-quSpecIn1SumCenaTovar.AsFloat;

              quSpecIn1.Next;
            end;

            bOpr:=True;
            if NeedPost(trunc(cxDateEdit1.Date)) then
            begin
              iNumPost:=PostNum(IDH)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                prFCliINN(cxButtonEdit1.Tag,Label4.Tag,SInn,S1,s2,sInnF);

                writeln(fPost,'TTNIN;ON;'+its(cxLookupComboBox1.EditValue)+r+its(IDH)+r+ds(cxDateEdit1.Date)+r+cxTextEdit1.Text+r+SInnF+r+fs(cxCalcEdit2.EditValue)+r+fs(rSum2)+r+fs(rSum3)+r+fs(cxCalcEdit5.EditValue)+r+fs(cxCalcEdit7.EditValue)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(cxLookupComboBox1.EditValue)+r+its(IDH);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;


            if bOpr then
            begin

              if (abs(rSum01)<0.001) and (abs(rSum02)<0.001) then
              begin //��� ��������� �� ������ ������ �� ������
  //            prClearBuf(quTTnInID.AsInteger,1,Trunc(cxDateEdit1.Date),1);

                ptCliGoods.Active:=False; ptCliGoods.Active:=True;

                quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
                while not quSpecIn1.Eof do
                begin
                  if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
  //              prPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,cxTextEdit1.Text,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,cxTextEdit1.Text,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                  prDelTMoveId(cxLookupComboBox1.EditValue,quSpecIn1CodeTovar.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,quSpecIn1Kol.AsFloat);
                  prAddTMoveId(cxLookupComboBox1.EditValue,quSpecIn1CodeTovar.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,quSpecIn1Kol.AsFloat);

                  //�������� � ����������� ����������
                  if CommonSet.AssPostFromPrihod=1 then
                  begin
                    if ptCliGoods.FindKey([Label4.Tag,cxButtonEdit1.Tag,quSpecIn1CodeTovar.AsInteger]) then
                    begin
                      ptCliGoods.Edit;
                      ptCliGoodsxDate.AsDateTime:=cxDateEdit1.Date;
                      ptCliGoods.Post;
                    end else
                    begin
                      ptCliGoods.Append;
                      ptCliGoodsGoodsID.AsInteger:=quSpecIn1CodeTovar.AsInteger;
                      ptCliGoodsClientObjTypeID.AsInteger:=Label4.Tag;
                      ptCliGoodsClientID.AsInteger:=cxButtonEdit1.Tag;
                      ptCliGoodsxDate.AsDateTime:=cxDateEdit1.Date;
                      ptCliGoods.Post;
                    end;
                  end;

                  quSpecIn1.Next;
                end;
                quSpecIn1.Active:=False;
                ptCliGoods.Active:=False;


                if rSum3<>0 then
                begin //�������� �� ����
                  quSpecInT.Active:=False;
                  quSpecInT.ParamByName('IDEP').AsInteger:=cxLookupComboBox1.EditValue;
                  quSpecInT.ParamByName('SDATE').AsDate:=Trunc(cxDateEdit1.Date);
                  quSpecInT.ParamByName('SNUM').AsString:=cxTextEdit1.Text;
                  quSpecInT.Active:=True;

                  quSpecInT.First;  // ��������������
                  while not quSpecInT.Eof do
                  begin
  // ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(cxLookupComboBox1.EditValue,quSpecInTCodeTara.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,quSpecInTKolMest.AsFloat);
                    prAddTaraMoveId(cxLookupComboBox1.EditValue,quSpecInTCodeTara.AsInteger,Trunc(cxDateEdit1.Date),IDH,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                    quSpecInT.Next;
                  end;
                  quSpecInT.Active:=False;
                end;

                // 4 - �������� ������
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
                quE.SQL.Add('where Depart='+its(cxLookupComboBox1.EditValue));
                quE.SQL.Add('and DateInvoice='''+ds(cxDateEdit1.Date)+'''');
                quE.SQL.Add('and Number='''+cxTextEdit1.Text+'''');
                quE.ExecSQL;

                //��������� ������ ������

                 if iTestZ>0 then
                 begin
                   quE.Active:=False;
                   quE.SQL.Clear;
                   quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                   quE.SQL.Add('where ID='+its(iTestZ));
                   quE.ExecSQL;
                 end;

                //�������� ��
                Memo1.Lines.Add('  �������� ��.');
                if (trunc(Date)-trunc(cxDateEdit1.Date))>=1 then prRecalcTO(trunc(cxDateEdit1.Date),trunc(Date)-1,nil,1,0);

                Memo1.Lines.Add('   ������������ �������.'); delay(10);

              end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
            end else
            begin
              quSpecIn1.Active:=False;
              Memo1.Lines.Add('   ������ �������� ������������, ������������� ��������.'); delay(10);
            end;
          end;
          Memo1.Lines.Add(''); delay(10);
        end;
      end;
    finally
      teSpecIn.Active:=False;
      if bOpr then
      begin
        cxButton1.Enabled:=False; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=False;
        prRefrID(quTTnIn,fmDocs1.ViewDocsIn,IDH); //IDH
        ViewDoc1.EndUpdate;
        Memo1.Lines.Add('.'); delay(100);

        fmAddDoc1.Caption:='���������: ��������.';
        cxTextEdit1.Properties.ReadOnly:=True;

        cxTextEdit2.Properties.ReadOnly:=True;
        cxDateEdit1.Properties.ReadOnly:=True;
        cxDateEdit2.Properties.ReadOnly:=True;
        cxButtonEdit1.Properties.ReadOnly:=True;

        cxLookupComboBox1.Properties.ReadOnly:=True;

        cxLabel1.Enabled:=False;
        cxLabel2.Enabled:=False;
        cxLabel3.Enabled:=False;
        cxLabel4.Enabled:=False;
        cxLabel5.Enabled:=False;
        cxLabel6.Enabled:=False;
        cxButton1.Enabled:=False;
        cxButton7.Enabled:=False;

        ViewDoc1.OptionsData.Editing:=False;
        ViewDoc1.OptionsData.Deleting:=False;
        Panel4.Visible:=False;

//      Close;
      end else
      begin
        cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton4.Enabled:=True; cxButton7.Enabled:=True;
        prRefrID(quTTnIn,fmDocs1.ViewDocsIn,IDH);
        ViewDoc1.EndUpdate;
        Memo1.Lines.Add('.'); delay(100);
      end;
    end;
  end;
end;

procedure TfmAddDoc1.acSaveDoc1Execute(Sender: TObject);
begin
  if cxButton7.Enabled then cxButton7.Click;
end;

procedure TfmAddDoc1.N4Click(Sender: TObject);
var Par:Variant;
    iC:INteger;
    i,j: Integer;
    Rec:TcxCustomGridRecord;
begin
  //����������
  with dmMT do
  with dmMC do
  begin
    if ViewDoc1.Controller.SelectedRowCount=0 then exit;

    if cxTextEdit3.Tag>0 then
    begin
      showmessage('����������� ���������� ����������� EDI ���������.');
      exit;
    end;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnInID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnInID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnInDateInvoice.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnInNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTnInCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnInNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnInDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnInNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnInSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnInSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTnInIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTnInSummaTara.AsFloat;
      taHeadDoc.Post;

      iC:=1;

      for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDoc1.Controller.SelectedRecords[i];
          taSpecDoc.Append;
          for j:=0 to Rec.ValueCount-1 do
          begin
                                                                        taSpecDocIType.AsInteger:=1;
                                                                        taSpecDocIdHead.AsInteger:=quTTnInID.AsInteger;
                                                                        taSpecDocNum.AsInteger:=iC;
            if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar'        then taSpecDocIdCard.AsInteger:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1KolMest'          then taSpecDocQuantM.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1KolWithMest'      then taSpecDocQuant.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1CenaTovar'        then taSpecDocPriceIn.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1SumCenaTovarPost' then taSpecDocSumIn.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1NewCenaTovar'     then taSpecDocPriceUch.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1SumCenaTovar'     then taSpecDocSumUch.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1NDSProc'          then taSpecDocIdNds.AsInteger:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1NDSSum'           then taSpecDocSumNds.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1Name'             then taSpecDocNameC.AsString:=Copy(Rec.Values[j],1,30);
                                                                        taSpecDocSm.AsString:='';
            if ViewDoc1.Columns[j].Name='ViewDoc1CodeEdIzm'        then taSpecDocIdM.AsInteger:=Rec.Values[j];
                                                                        taSpecDocKm.AsFloat:=0;
                                                                        taSpecDocPriceUch1.AsFloat:=0;
                                                                        taSpecDocSumUch1.AsFloat:=0;
            if ViewDoc1.Columns[j].Name='ViewDoc1Procent'          then taSpecDocProcPrice.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1CodeTara'         then taSpecDocIdTara.AsInteger:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1KolMest'          then taSpecDocQuantT.AsFloat:=Rec.Values[j];
                                                                        taSpecDocNameT.AsString:='';
            if ViewDoc1.Columns[j].Name='ViewDoc1CenaTara'         then taSpecDocPriceT.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1SumCenaTara'      then taSpecDocSumTara.AsFloat:=Rec.Values[j];
            if ViewDoc1.Columns[j].Name='ViewDoc1BarCode'          then taSpecDocBarCode.AsString:=Rec.Values[j];
          end;
          inc(iC);
          taSpecDoc.Post;
        end;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;


procedure TfmAddDoc1.N5Click(Sender: TObject);
Var StrWk:String;
    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMC do
  with dmMT do
  begin
    if cxTextEdit3.Tag>0 then
    begin
      showmessage('������� ���������� ����������� EDI ���������.');
      exit;
    end;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          {DocsIn.fmDocs1.prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmAddDoc1.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmAddDoc1.Label4.Tag:=taHeadDociTypeCli.AsInteger;
          fmAddDoc1.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc1.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Active:=True;

          fmAddDoc1.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc1.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmAddDoc1.cxLabel1.Enabled:=True;
          fmAddDoc1.cxLabel2.Enabled:=True;
          fmAddDoc1.cxLabel3.Enabled:=True;
          fmAddDoc1.cxLabel4.Enabled:=True;
          fmAddDoc1.cxLabel5.Enabled:=True;
          fmAddDoc1.cxLabel6.Enabled:=True;
          fmAddDoc1.cxButton1.Enabled:=True;

          fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;
          fmAddDoc1.Panel4.Visible:=True;

          CloseTa(fmAddDoc1.taSpecIn);}

          if taG.Active=False then taG.Active:=True;

          fmAddDoc1.ViewDoc1.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc1 do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecIn.Append;
                  taSpecInNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecInCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecInCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecInBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecInNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecInNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecInOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecInBestBefore.AsDateTime:=Date;
                  taSpecInKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecInKolEdMest.AsFloat:=0;
                  taSpecInKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecInKolWithNecond.AsFloat:=0;
                  taSpecInKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecInCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecInProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecInNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecInSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecInProcentN.AsFloat:=0;
                  taSpecInNecond.AsFloat:=0;
                  taSpecInCenaNecond.AsFloat:=0;
                  taSpecInSumNecond.AsFloat:=0;
                  taSpecInProcentZ.AsFloat:=0;
                  taSpecInZemlia.AsFloat:=0;
                  taSpecInProcentO.AsFloat:=0;
                  taSpecInOthodi.AsFloat:=0;
                  taSpecInName.AsString:=taSpecDocNameC.AsString;
                  taSpecInCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecInNameTara.AsString:=StrWk;

                  taSpecInVesTara.AsFloat:=0;
                  taSpecInCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecInSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecInTovarType.AsInteger:=0;

                  taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecInNac1.AsFloat:=rn1;
                  taSpecInNac2.AsFloat:=rn2;
                  taSpecInNac3.AsFloat:=rn3;
                  taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInsMaker.AsString:='';
                  end;
                  taSpecIn.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmAddDoc1.ViewDoc1.EndUpdate;

          fmAddDoc1.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmAddDoc1.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;


procedure TfmAddDoc1.acCalcPriceOutExecute(Sender: TObject);
var rPr,rPrCard,rPrA:Real;
    rn1,rn2,rn3:Real;
begin
  //��������� ���� ��������
  if cxButton1.Enabled=False then exit;
  with dmMt do
  begin
    taSpecIn.First;
    while not taSpecIn.Eof do
    begin
      prCalcPriceOut(taSpecInCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),taSpecInCenaTovar.AsFloat,taSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

      iCol:=0;
      rPrCard:=0;
      if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then rPrCard:=taCardsCena.AsFloat;

      if abs(rPr-rPrCard)<0.11 then rPr:=rPrCard; //���� ������� � ������� ����� ����� 11 ������ �� ���� �� ��������.
      if abs(rPr)<0.01 then rPr:=rPrCard; //���� rPr =0  �� ���� �� �������� �.�. �� ��������.

      taSpecIn.Edit;
      taSpecInNewCenaTovar.AsFloat:=rPr;
      taSpecInSumCenaTovar.AsFloat:=RoundVal(rPr*taSpecInKol.AsFloat);
      if taSpecInCenaTovar.AsFloat<>0 then
       taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
      else
       taSpecInProcent.AsFloat:=100;

      taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
      taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
      taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
      taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
      taSpecInNac1.AsFloat:=rn1;
      taSpecInNac2.AsFloat:=rn2;
      taSpecInNac3.AsFloat:=rn3;
      taSpecInCenaTovarAvr.AsFloat:=rPrA;
      taSpecIn.Post;

      taSpecIn.Next;
    end;
    taSpecIN.First;
    ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
    ViewDoc1KolWithMest.Focused:=True;
  end;
end;

procedure TfmAddDoc1.cxLabel12Click(Sender: TObject);
begin
//  prSetNac;
  acCalcPriceOut.Execute;
end;

procedure TfmAddDoc1.cxLabel13Click(Sender: TObject);
begin
  acPrintYellow.Execute;
end;

procedure TfmAddDoc1.acPrintYellowExecute(Sender: TObject);
Var bPrint:Boolean;
    sLab:String;
//    vPP:TfrPrintPages;
begin
  //������ ������ ��������
  with dmMT do
  with dmMC do
  begin
    sLab:='';
    quLab1.Active:=False;
    quLab1.Active:=True;
    quLab1.First;
    while not quLab1.Eof do
    begin
      if pos('����',quLab1NAME.AsString)>0 then sLab:=quLab1FILEN.AsString;
      quLab1.Next;
    end;
    quLab1.Active:=False;

    if sLab>'' then
    begin
      CloseTe(taCen);

      ViewDoc1.BeginUpdate;
      taSpecIN.First;
      while not taSpecIn.Eof do
      begin
        bPrint:=False;
        if taSpecIniCat.AsInteger=2 then bPrint:=True;
        if taSpecIniCat.AsInteger=8 then bPrint:=True;
        if taSpecIniCat.AsInteger=9 then bPrint:=True;
        if taSpecIniCat.AsInteger=4 then
          if taCards.findkey([taSpecInCodeTovar.AsInteger]) then
            if (taCardsReserv5.AsFloat>=taSpecInCenaTovar.AsFloat) then //�������� ���� ������������� �������� �����
              bPrint:=True;
        if bPrint then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=taSpecInCodeTovar.AsInteger;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=taSpecInCodeTovar.AsInteger;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=rv(taSpecInNewCenaTovar.AsFloat);
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=taSpecInBarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(taSpecInCodeTovar.AsInteger) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(taSpecInCodeTovar.AsInteger);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(taSpecInCodeTovar.AsInteger);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;
          quFindC4.Active:=False;
        end;

        taSpecIN.Next;
      end;
      ViewDoc1.EndUpdate;

      if taCen.RecordCount>0 then
      begin
        RepCenn.LoadFromFile(CommonSet.Reports+sLab);
        RepCenn.ReportName:='�������.';
        RepCenn.PrepareReport;
//        vPP:=frAll;
        RepCenn.ShowPreparedReport;
//        RepCenn.PrintPreparedReport('',1,False,vPP);
      end;
      CloseTe(taCen);
      fmMainMCryst.cxDEdit1.Date:=Date;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecIn2BeforePost(DataSet: TDataSet);
begin
 //
  iCol:=0;
  if taSpecIn2CodeTovar.AsInteger<0 then taSpecIn2CodeTovar.AsInteger:=0;
  if taSpecIn2KolMest.AsFloat<0 then taSpecIn2KolMest.AsFloat:=0;
  if taSpecIn2KolWithMest.AsFloat<0 then taSpecIn2KolWithMest.AsFloat:=0;
  if taSpecIn2CenaTovar.AsFloat<0 then taSpecIn2CenaTovar.AsFloat:=0;
  if taSpecIn2NewCenaTovar.AsFloat<0 then taSpecIn2NewCenaTovar.AsFloat:=0;
  if taSpecIn2SumCenaTovarPost.AsFloat<0 then taSpecIn2SumCenaTovarPost.AsFloat:=0;
  if taSpecIn2SumCenaTovar.AsFloat<0 then taSpecIn2SumCenaTovar.AsFloat:=0;
  if taSpecIn2NDSProc.AsFloat<0 then taSpecIn2NDSProc.AsFloat:=0;
  if taSpecIn2NDSSum.AsFloat<0 then taSpecIn2NDSSum.AsFloat:=0;
  if taSpecIn2OutNDSSum.AsFloat<0 then taSpecIn2OutNDSSum.AsFloat:=0;
  if taSpecIn2ProcentN.AsFloat<0 then taSpecIn2ProcentN.AsFloat:=0;
  if taSpecIn2Necond.AsFloat<0 then taSpecIn2Necond.AsFloat:=0;
  if taSpecIn2CenaNecond.AsFloat<0 then taSpecIn2CenaNecond.AsFloat:=0;
  if taSpecIn2SumNecond.AsFloat<0 then taSpecIn2SumNecond.AsFloat:=0;
  if taSpecIn2ProcentZ.AsFloat<0 then taSpecIn2ProcentZ.AsFloat:=0;
  if taSpecIn2Zemlia.AsFloat<0 then taSpecIn2Zemlia.AsFloat:=0;
  if taSpecIn2ProcentO.AsFloat<0 then taSpecIn2ProcentO.AsFloat:=0;
  if taSpecIn2Othodi.AsFloat<0 then taSpecIn2Othodi.AsFloat:=0;
  if taSpecIn2VesTara.AsFloat<0 then taSpecIn2VesTara.AsFloat:=0;
  if taSpecIn2CenaTara.AsFloat<0 then taSpecIn2CenaTara.AsFloat:=0;
  if taSpecIn2SumVesTara.AsFloat<0 then taSpecIn2SumVesTara.AsFloat:=0;
  if taSpecIn2SumCenaTara.AsFloat<0 then taSpecIn2SumCenaTara.AsFloat:=0;
  if taSpecIn2CodeTara.AsInteger<0 then taSpecIn2CodeTara.AsInteger:=0;
end;

procedure TfmAddDoc1.ViewDoc1NameTaraPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
//
  with dmMC do
  begin
    iDirect:=2;
    PosP.Id:=0; PosP.Name:='';
    if quTara.Active=False then quTara.Active:=True;
    delay(10);
    fmTara.ShowModal;
    iDirect:=0;
    if PosP.Id>0 then
    begin
      taSpecIn.Edit;
      taSpecInCodeTara.AsInteger:=PosP.Id;
      taSpecInNameTara.AsString:=PosP.Name;
      taSpecIn.Post;
    end;
  end;
end;

procedure TfmAddDoc1.acAddSpis3Execute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iDocDate,iTp,iDepSpis,iMax:INteger;
    CurTime:TDateTime;
begin
  // ������� �������� �� �������
  if ViewDoc1.Controller.SelectedRecordCount=0 then exit;
  if not CanDo('prAddSpis3') then begin StatusBar1.Panels[0].Text:='��� ����.'; end
  else
  begin
    with dmMC do
    with dmMT do
    begin
      if quTTnInAcStatus.AsInteger=3 then
      begin
        iDocDate:=trunc(cxDateEdit1.Date);
        CurTime:=now;
        if CurTime<(iDocDate+1.5) then
        begin
          if MessageDlg('������� �������� �������� �� '+its(ViewDoc1.Controller.SelectedRecordCount)+' ���������� �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
          begin
            CloseTe(fmAddDoc4.taSpecVn);
            iMax:=1;

            for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewDoc1.Controller.SelectedRecords[i];

              iNum:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin iNum:=Rec.Values[j]; break; end;
              end;

              if (iNum>0) then
              begin
                with fmAddDoc4 do
                begin
                  if taSpecIn.Locate('CodeTovar',iNum,[]) then
                  begin
                    taSpecVn.Append;
                    taSpecVnNum.AsInteger:=iMax;
                    taSpecVnId.AsInteger:=0; //������
                    taSpecVnCodeTovar.AsInteger:=iNum;
                    taSpecVnCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                    taSpecVnBarCode.AsString:=taSpecInBarCode.AsString;
                    taSpecVnNDSProc.AsFloat:=taSpecInNDSProc.AsFloat;
//                    taSpecVnKolMest.AsFloat:=1;
//                    taSpecVnKolWithMest.AsFloat:=0;
                    taSpecVnKol.AsFloat:=0;
                    taSpecVnCena.AsFloat:=taSpecInCenaTovar.AsFloat;
                    taSpecVnNewCena.AsFloat:=taSpecInNewCenaTovar.AsFloat;
                    taSpecVnProc.AsFloat:=0;
                    taSpecVnSumCena.AsFloat:=0;
                    taSpecVnSumNewCena.AsFloat:=0;
                    taSpecVnSumRazn.AsFloat:=0;
                    taSpecVnName.AsString:=taSpecInName.AsString;
                    taSpecVnFullName.AsString:=taSpecInName.AsString;
                    taSpecVnQuantN.AsFloat:=taSpecInKol.AsFloat;
                    taSpecVnQuantM.AsFloat:=0;
                    taSpecVnProcBrak.AsFloat:=0;

                    taSpecVn.Post;
                  end;
                end;

                inc(iMax);
              end;
            end;

            fmDocs4.prSetValsAddDoc4(0,3); //0-����������, 1-��������������, 2-��������
            iTp:=3;

            fmAddDoc4.Label3.Caption:='�������� � �������';
            fmAddDoc4.cxDateEdit1.Date:=cxDateEdit1.Date+1;

            if ptDep.Active=False then ptDep.Active:=True;
            ptDep.First;  iDepSpis:=0;
            while not ptDep.Eof do
            begin
              if pos('����',OemToAnsiConvert(ptDepName.AsString))>0 then  iDepSpis:=ptDepID.AsInteger;
              ptDep.Next;
            end;
            if iDepSpis>0 then fmAddDoc4.cxLookupComboBox2.EditValue:=iDepSpis;

            fmAddDoc4.cxLookupComboBox1.EditValue:=fmAddDoc1.cxLookupComboBox1.EditValue;
            fmAddDoc4.cxTextEdit1.Text:=fmAddDoc1.cxTextEdit1.Text;

            fmAddDoc4.Tag:=iTp;

            fmAddDoc4.acSaveDoc.Enabled:=True;
            fmAddDoc4.ShowModal;

          end;
        end else showmessage('�������� ���������� - ����� �����.');
      end else showmessage('�������� ������ ���� �����������.');
      ViewDoc1.Controller.ClearSelection;
    end;
  end;
end;

procedure TfmAddDoc1.acPrintPassportExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iMax:INteger;
begin
//������ ��������� �������� �� ���������� ��������
  if ViewDoc1.Controller.SelectedRecordCount=0 then exit;
  with dmMC do
  with dmMT do
  begin
    CloseTe(tePrintP);

    iMax:=1;

    for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDoc1.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin iNum:=Rec.Values[j]; break; end;
      end;

      if (iNum>0) then
      begin
        if taSpecIn.Locate('CodeTovar',iNum,[]) then
        begin
          tePrintP.Append;
          tePrintPiNum.AsInteger:=iMax;
          tePrintPBarcode.AsString:=taSpecInBarCode.AsString;
          tePrintPiCode.AsInteger:=taSpecInCodeTovar.AsInteger;
          if taSpecInCodeEdIzm.AsInteger=1 then tePrintPEdIzm.AsString:='��.' else tePrintPEdIzm.AsString:='��.';
          tePrintPName.AsString:=taSpecInName.AsString;
          tePrintPDocDate.AsString:=FormatDateTime('dd mmm yyyy',cxDateEdit1.Date);
          tePrintPDocNum.AsString:=cxTextEdit1.Text;
          tePrintPCliName.AsString:=cxButtonEdit1.Text;
          tePrintPPrBrak.AsString:=fs(rv(taSpecInProcentN.AsFloat))+'%';
          tePrintPNameTara.AsString:=taSpecInNameTara.AsString;
          tePrintPBrakVozm.AsString:='';
          tePrintP.Post;

          inc(iMax);
        end;
      end;
    end;
    if tePrintP.RecordCount>0 then
    begin

      frRepDIN.ReportName:='�������� ��������.';

      frRepDIN.LoadFromFile(CommonSet.Reports+'passportin.frf');
      
      frRepDIN.PrepareReport;
      frRepDIN.ShowPreparedReport;

    end;
    ViewDoc1.Controller.ClearSelection;
  end;
  tePrintP.Active:=False;
end;

procedure TfmAddDoc1.cxButton8Click(Sender: TObject);
var // iMax:Integer;
    rn1,rn2,rn3,rPr,rPrA:Real;

begin
  with dmMC do
  with dmMt do
  with fmZakHPost do
  begin
    if cxTextEdit3.tag=1 then
    begin
      ViewZHPost.BeginUpdate;
      quZakHPost.Active:=False;
      quZakHPost.ParamByName('ICLI').AsInteger:=cxButtonEdit1.Tag;
      quZakHPost.ParamByName('IDATEB').AsInteger:=Trunc(cxDateEdit1.Date-3);
      quZakHPost.ParamByName('IDATEE').AsInteger:=Trunc(cxDateEdit1.Date+3);
      quZakHPost.Active:=True;
      ViewZHPost.EndUpdate;
      if quZakHPost.RecordCount>0 then
      begin
        fmZakHPost.ShowModal;
        if fmZakHPost.ModalResult=mrOk then
        begin //��������� ��������
          cxTextEdit3.Text:=quZakHPostDOCNUM.AsString;

          ViewDoc1.BeginUpdate;

          if cxLookupComboBox1.EditValue<>quZakHPostDEPART.AsInteger then
          begin //��������� ������ - ������ ������������
            taSpecIn.First;
            while not taSpecIn.Eof do taSpecIn.Delete;
          end;

          cxLookupComboBox1.EditValue:=quZakHPostDEPART.AsInteger;
          cxLookupComboBox1.Text:=quZakHPostName.AsString;

          CloseTe(teSpecZ);

          if ptZSpec.Active=False then ptZSpec.Active:=True;
          ptZSpec.CancelRange;
          ptZSpec.IndexFieldNames:='IDH;IDS';
          ptZSpec.SetRange([quZakHPostID.AsInteger],[quZakHPostID.AsInteger]);
          try
            ptZSpec.First;
            while not ptZSpec.Eof do
            begin
              if taCards.FindKey([ptZSpecCODE.AsInteger]) then
              begin
                teSpecZ.Append;
                teSpecZCode.AsInteger:=taCardsID.AsInteger;
           //     teSpecZName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                teSpecZEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
           //     teSpecZFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                teSpecZiCat.AsInteger:=taCardsV02.AsInteger;
                teSpecZiTop.AsInteger:=taCardsV04.AsInteger;
                teSpecZiN.AsInteger:=taCardsV05.AsInteger;
                teSpecZRemn1.AsFloat:=ptZSpecREMN1.AsFloat;
                teSpecZvReal.AsFloat:=ptZSpecVREAL.AsFloat;
                teSpecZRemnDay.AsFloat:=ptZSpecREMNDAY.AsFloat;
                teSpecZRemnMin.AsInteger:=ptZSpecREMNMIN.AsInteger;
                teSpecZRemnMax.AsInteger:=ptZSpecREMNMAX.AsInteger;
                teSpecZRemn2.AsFloat:=ptZSpecREMN2.AsFloat;
                teSpecZQuant1.AsFloat:=ptZSpecQUANT1.AsFloat;
                teSpecZPK.AsFloat:=ptZSpecPK.AsFloat;
                teSpecZQuantZ.AsFloat:=ptZSpecQUANTZ.AsFloat;
                teSpecZQuant2.AsFloat:=ptZSpecQUANT2.AsFloat;
                teSpecZQuant3.AsFloat:=ptZSpecQUANT3.AsFloat;
                teSpecZPriceIn.AsFloat:=ptZSpecPRICEIN.AsFloat;
                teSpecZSumIn.AsFloat:=ptZSpecSUMIN.AsFloat;
                teSpecZNDSProc.AsFloat:=ptZSpecNDSPROC.AsFloat;
                teSpecZBarCode.AsString:=taCardsBarCode.AsString;

//              teSpecZPriceIn0.AsFloat:=
//              teSpecZSumIn0.AsFloat:=

                teSpecZCliQuant.AsFloat:=ptZSpecCLIQUANT.AsFloat;
                teSpecZCliPrice.AsFloat:=rv(ptZSpecCLIPRICE.AsFloat);
                teSpecZCliPrice0.AsFloat:=rv(ptZSpecCLIPRICE0.AsFloat);
                teSpecZCliSumIn.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE.AsFloat));
                teSpecZCliSumIn0.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE0.AsFloat));

                teSpecZCliQuantN.AsFloat:=ptZSpecCLIQUANTN.AsFloat;
                teSpecZCliPriceN.AsFloat:=rv(ptZSpecCLIPRICEN.AsFloat);
                teSpecZCliPrice0N.AsFloat:=rv(ptZSpecCLIPRICE0N.AsFloat);
                teSpecZCliSumInN.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICEN.AsFloat));
                teSpecZCliSumIn0N.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICE0N.AsFloat));

                teSpecZNNum.AsInteger:=ptZSpecCLINNUM.AsInteger;

                teSpecZ.Post;
              end;
              ptZSpec.Next;
            end;
          except
          end;
            //������� - ���������

      {    iMax:=1;
          taSpecIn.First;
          if not fmAddDoc1.taSpecIn.Eof then
          begin
            taSpecIn.Last;
            iMax:=taSpecInNum.AsInteger+1;
          end;}

          teSpecZ.First;
          while not teSpecZ.Eof do
          begin
            if teSpecZCliQuantN.AsFloat>0.001 then
            if taSpecIn.Locate('CodeTovar',teSpecZCode.AsInteger,[])=False then
            begin //���������
              if taCards.FindKey([teSpecZCode.AsInteger]) then
              begin
                prCalcPriceOut(taCardsID.AsInteger,Trunc(cxDateEdit1.Date),0,0,rn1,rn2,rn3,rPr,rPrA);

                taSpecIn.Append;
                taSpecInNum.AsInteger:=teSpecZNNum.AsInteger;  //����� �� ���������
                taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                taSpecInBarCode.AsString:=taCardsBarCode.AsString;
                taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat*Label17.Tag;
                taSpecInNDSSum.AsFloat:=teSpecZCliSumInN.AsFloat-teSpecZCliSumIn0N.AsFloat;
                taSpecInOutNDSSum.AsFloat:=teSpecZCliSumIn0N.AsFloat;
                taSpecInBestBefore.AsDateTime:=cxDateEdit1.Date;
                taSpecInKolMest.AsInteger:=1;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=teSpecZCliQuantN.AsFloat;
                taSpecInKolWithNecond.AsFloat:=0;
                taSpecInKol.AsFloat:=teSpecZCliQuantN.AsFloat;
                taSpecInCenaTovar.AsFloat:=teSpecZCliPriceN.AsFloat;
                try
                  taSpecInProcent.AsFloat:=(rPr-teSpecZCliPriceN.AsFloat)/teSpecZCliPriceN.AsFloat*100;
                except
                  taSpecInProcent.AsFloat:=0;
                end;
                taSpecInNewCenaTovar.AsFloat:=rPr;
                taSpecInSumCenaTovarPost.AsFloat:=teSpecZCliSumInN.AsFloat;
                taSpecInSumCenaTovar.AsFloat:=rPr*teSpecZCliQuantN.AsFloat;
                taSpecInProcentN.AsFloat:=0;
                taSpecInNecond.AsFloat:=0;
                taSpecInCenaNecond.AsFloat:=0;
                taSpecInSumNecond.AsFloat:=0;
                taSpecInProcentZ.AsFloat:=0;
                taSpecInZemlia.AsFloat:=0;
                taSpecInProcentO.AsFloat:=0;
                taSpecInOthodi.AsFloat:=0;
                taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                taSpecInCodeTara.AsInteger:=0;
                taSpecInVesTara.AsFloat:=0;
                taSpecInCenaTara.AsFloat:=0;
                taSpecInSumCenaTara.AsFloat:=0;
                taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                taSpecInNac1.AsFloat:=rn1;
                taSpecInNac2.AsFloat:=rn2;
                taSpecInNac3.AsFloat:=rn3;
                taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
                taSpecInCenaTovarAvr.AsFloat:=rPrA;

                if taCardsV12.AsInteger>0 then
                begin
                  taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                  taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                end else
                begin
                  taSpecInSumVesTara.AsFloat:=0;
                  taSpecInsMaker.AsString:='';
                end;

                taSpecIn.Post;
              end;
            end;

            teSpecZ.Next;
          end;

          ViewDoc1.EndUpdate;
        end;
      end else
      begin
        Memo1.Lines.Add('����������� ��������� �� ������ � '+ds1(cxDateEdit1.Date-3)+' �� '+ds1(cxDateEdit1.Date+3)+' �� ������� ���������� ���.');
        showmessage('����������� ��������� �� ������ � '+ds1(cxDateEdit1.Date-3)+' �� '+ds1(cxDateEdit1.Date+3)+' �� ������� ���������� ���.');
      end;
      quZakHPost.Active:=False;

    end else Memo1.Lines.Add('��������� �� EDI - ������ �� ��������������');

  end;
end;

procedure TfmAddDoc1.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmAddDoc1.Visible then GridDoc1.SetFocus;
end;

procedure TfmAddDoc1.taSpecInCalcFields(DataSet: TDataSet);
Var rPr,rFixPr,rPrMax,rPrMin:Real;
    iBStatus:INteger;
begin

  taSpecIniNac.AsInteger:=0;
  
//  if abs(taSpecInCenaTovar.AsFloat)<0.01 then exit; //��� ������� ���� ���������� ������ �� ���������.
  if abs(taSpecInCenaTovar.AsFloat)<0.01 then
  begin
    if (taSpecInNewCenaTovar.AsFloat-taSpecInRealPrice.AsFloat)>=0.01 then taSpecIniNac.AsInteger:=2; //��������� ���� ����������� ���.
    if (taSpecInNewCenaTovar.AsFloat-taSpecInRealPrice.AsFloat)<=(-0.01) then taSpecIniNac.AsInteger:=1; //����� ���� ���� ����. ����������
    exit;
  end;

  rFixPr:=0;
  with dmMT do
  begin
    if taCards.findkey([taSpecInCodeTovar.AsInteger]) then rFixPr:=taCardsFixPrice.AsFloat;
{
    if ((rFixPr>0.01)and(taCardsV02.AsInteger<>4))or((rFixPr>0.01)and(taCardsV02.AsInteger=4)and(taCardsReserv5.AsFloat>=taSpecInCenaTovarAvr.AsFloat)) then
    begin
      taSpecIniNac.AsInteger:=0;
      if (rFixPr-taSpecInNewCenaTovar.AsFloat)>=0.01 then taSpecIniNac.AsInteger:=1;
      if (taSpecInNewCenaTovar.AsFloat-rFixPr)>=0.01 then taSpecIniNac.AsInteger:=2;
    end;
}

    if ((rFixPr>0.01)and(taCardsReserv5.AsFloat>0))
    or((rFixPr>0.01)and(taCardsReserv5.AsFloat>=taSpecInCenaTovarAvr.AsFloat))
    or((rFixPr>0.01)and(taCardsReserv5.AsFloat<taSpecInCenaTovarAvr.AsFloat)and(rFixPr=taCardsCena.AsFloat)) then
    begin
      taSpecIniNac.AsInteger:=0;
      if (rFixPr-taSpecInNewCenaTovar.AsFloat)>=0.01 then taSpecIniNac.AsInteger:=1;
      if (taSpecInNewCenaTovar.AsFloat-rFixPr)>=0.01 then taSpecIniNac.AsInteger:=2;
    end
{//�� ��������
    if ((rFixPr>0.01)and(taCardsReserv5.AsFloat<taSpecInCenaTovarAvr.AsFloat)and(rFixPr<>taCardsCena.AsFloat)) then
    begin
      taSpecIniNac.AsInteger:=0;
      if (rFixAfterAkc-taSpecInNewCenaTovar.AsFloat)>=0.01 then taSpecIniNac.AsInteger:=1;
      if (taSpecInNewCenaTovar.AsFloat-rFixAfterAkc)>=0.01 then taSpecIniNac.AsInteger:=2;
    end;
}
    else
    begin
      if abs(taSpecInNac1.AsFloat)>0.1 then  //���� ���� �����-�� ������� ����������
      begin
        rPr:=taSpecInCenaTovarAvr.AsFloat*(100+taSpecInNac1.AsFloat)/100; //���� ��� ����������
        rPrMax:=taSpecInCenaTovarAvr.AsFloat*(100+taSpecInNac1.AsFloat+taSpecInNac3.AsFloat)/100; //������������ ���� ��� ����������
        rPrMin:=taSpecInCenaTovarAvr.AsFloat*(100+taSpecInNac1.AsFloat-taSpecInNac2.AsFloat)/100; //����������� ���� ��� ����������
        if (rPr-taSpecInNewCenaTovar.AsFloat)>=0.11 then
        begin
          if (rPrMin-taSpecInNewCenaTovar.AsFloat)>0 then taSpecIniNac.AsInteger:=1; //����� ���� ���� ����. ����������
        end;
        if (rPr-taSpecInNewCenaTovar.AsFloat)<=(-0.11) then
        begin
          iBStatus:=prFindBAkciya(taSpecInCodeTovar.AsInteger,Trunc(Date+1));
          if (iBStatus=2)or(iBStatus=9) then begin end else
            if (rPrMax-taSpecInNewCenaTovar.AsFloat)<0 then taSpecIniNac.AsInteger:=2; //��������� ���� ����������� ���.
        end;

      end;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecInBeforePost(DataSet: TDataSet);
begin
  iCol:=0;

  if taSpecInCodeTovar.AsInteger<0 then taSpecInCodeTovar.AsInteger:=0;
  if (taSpecInCodeTovar.AsInteger=56573) or (taSpecInCodeTovar.AsInteger=71801) then exit;
  if taSpecInKolMest.AsFloat<0 then taSpecInKolMest.AsFloat:=0;
  if taSpecInKolWithMest.AsFloat<0 then taSpecInKolWithMest.AsFloat:=0;
  if taSpecInCenaTovar.AsFloat<0 then taSpecInCenaTovar.AsFloat:=0;
  if taSpecInNewCenaTovar.AsFloat<0 then taSpecInNewCenaTovar.AsFloat:=0;
  if taSpecInSumCenaTovarPost.AsFloat<0 then taSpecInSumCenaTovarPost.AsFloat:=0;
  if taSpecInSumCenaTovar.AsFloat<0 then taSpecInSumCenaTovar.AsFloat:=0;
  if taSpecInNDSProc.AsFloat<0 then taSpecInNDSProc.AsFloat:=0;
  if taSpecInNDSSum.AsFloat<0 then taSpecInNDSSum.AsFloat:=0;
  if taSpecInOutNDSSum.AsFloat<0 then taSpecInOutNDSSum.AsFloat:=0;
  if taSpecInProcentN.AsFloat<0 then taSpecInProcentN.AsFloat:=0;
  if taSpecInNecond.AsFloat<0 then taSpecInNecond.AsFloat:=0;
  if taSpecInCenaNecond.AsFloat<0 then taSpecInCenaNecond.AsFloat:=0;
  if taSpecInSumNecond.AsFloat<0 then taSpecInSumNecond.AsFloat:=0;
  if taSpecInProcentZ.AsFloat<0 then taSpecInProcentZ.AsFloat:=0;
  if taSpecInZemlia.AsFloat<0 then taSpecInZemlia.AsFloat:=0;
  if taSpecInProcentO.AsFloat<0 then taSpecInProcentO.AsFloat:=0;
  if taSpecInOthodi.AsFloat<0 then taSpecInOthodi.AsFloat:=0;
  if taSpecInVesTara.AsFloat<0 then taSpecInVesTara.AsFloat:=0;
  if taSpecInCenaTara.AsFloat<0 then taSpecInCenaTara.AsFloat:=0;
  if taSpecInSumVesTara.AsFloat<0 then taSpecInSumVesTara.AsFloat:=0;
  if taSpecInSumCenaTara.AsFloat<0 then taSpecInSumCenaTara.AsFloat:=0;
  if taSpecInCodeTara.AsInteger<0 then taSpecInCodeTara.AsInteger:=0;
end;

procedure TfmAddDoc1.taSpecInNDSProcChange(Sender: TField);
begin
  if iCol=1 then   //������ ���
  begin
    taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
  end;
end;

procedure TfmAddDoc1.taSpecInNDSSumChange(Sender: TField);
begin
  // ����� ���
  if iCol=2 then   //����� ���
  begin
    taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat-taSpecInNDSSum.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInOutNDSSumChange(Sender: TField);
Var rPr:Real;
begin
  // ����� ���
  if iCol=3 then   //����� ��� ���
  begin
    if taSpecInNDSProc.AsFloat<>0 then
    begin
      if taSpecInKol.AsFloat<>0 then
      begin
        rPr:=(taSpecInOutNDSSum.AsFloat+taSpecInOutNDSSum.AsFloat/100*taSpecInNDSProc.AsFloat)/taSpecInKol.AsFloat;
        taSpecInSumCenaTovarPost.AsFloat:=taSpecInOutNDSSum.AsFloat+taSpecInOutNDSSum.AsFloat/100*taSpecInNDSProc.AsFloat;
        taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-rPr)/rPr*100);
        taSpecInCenaTovar.AsFloat:=rPr;
      end
      else
      begin
        taSpecInCenaTovar.AsFloat:=0;
        taSpecInProcent.AsFloat:=0;
      end;
    end;
    taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat-taSpecInOutNDSSum.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInKolMestChange(Sender: TField);
Var rQ:Real;
begin
  //���-�� ����            1
  if iCol=4 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;

    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;

  end;
end;

procedure TfmAddDoc1.taSpecInKolWithMestChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=5 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInKolWithNecondChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=6 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInCenaTovarChange(Sender: TField);
begin
  if iCol=8 then   //���� ���������� � ���
  begin
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat;
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*taSpecInKol.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
    if taSpecInCenaTovar.AsFloat<>0 then
      taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
    else
      taSpecInProcent.AsFloat:=0;

    iCol:=0;
    prSetFields1;
    iCol:=8;
  end;
end;

procedure TfmAddDoc1.taSpecInProcentChange(Sender: TField);
begin
  if iCol=9 then  //������� �������
  begin
//    prCalcPriceOut(taSpecInCodeTovar.AsInteger,taSpecInCenaTovar.AsFloat,taSpecInKol.AsFloat,rn1,rn2,rn3,rPr);
    taSpecInNewCenaTovar.AsFloat:=rv(taSpecInCenaTovar.AsFloat*(100+taSpecInProcent.AsFloat)/100);
    taSpecInSumCenaTovar.AsFloat:=RoundVal(rv(taSpecInCenaTovar.AsFloat*(100+taSpecInProcent.AsFloat)/100)*taSpecInKol.AsFloat);
  end;
end;

procedure TfmAddDoc1.taSpecInNewCenaTovarChange(Sender: TField);
begin
  if iCol=10 then   //���� ��������
  begin
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*taSpecInKol.AsFloat);
    if taSpecInCenaTovar.AsFloat<>0 then
      taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
    else
      taSpecInProcent.AsFloat:=100;
  end;
end;

procedure TfmAddDoc1.taSpecInSumCenaTovarPostChange(Sender: TField);
begin
  if iCol=11 then   //����� ���������� � ���
  begin
    if taSpecInKol.AsFloat<>0 then
    begin
      taSpecInCenaTovar.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat;
      taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
      taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;
      if (taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat)<>0 then
        taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-(taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat))/(taSpecInSumCenaTovarPost.AsFloat/taSpecInKol.AsFloat)*100)
      else
        taSpecInProcent.AsFloat:=100;

     { if abs(taSpecInCenaTovar.AsFloat)<0.01 then
      begin
        with dmMT do
        begin
          rPr:=1;
          if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then rPr:=taCardsCena.AsFloat;

          taSpecInNewCenaTovar.AsFloat:=rPr;
          taSpecInSumCenaTovar.AsFloat:=RoundVal(rPr*taSpecInKol.AsFloat);
          if taSpecInCenaTovar.AsFloat<>0 then
           taSpecInProcent.AsFloat:=RoundVal((taSpecInNewCenaTovar.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
          else
           taSpecInProcent.AsFloat:=100;
        end;
      end;}
    end;
    iCol:=0;
    prSetFields1;
    iCol:=11;
  end;
end;

procedure TfmAddDoc1.taSpecInSumCenaTovarChange(Sender: TField);
begin
  if iCol=12 then   //����� ��������
  begin
    if taSpecInKol.AsFloat<>0 then
    begin
      taSpecInNewCenaTovar.AsFloat:=RoundVal(taSpecInSumCenaTovar.AsFloat/taSpecInKol.AsFloat);

      if taSpecInCenaTovar.AsFloat<>0 then
        taSpecInProcent.AsFloat:=RoundVal((taSpecInSumCenaTovar.AsFloat/taSpecInKol.AsFloat-taSpecInCenaTovar.AsFloat)/taSpecInCenaTovar.AsFloat*100)
      else
        taSpecInProcent.AsFloat:=100;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecInProcentNChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=13 then   //���-�� % ����������
  begin
    taSpecInNecond.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentN.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-(taSpecInKolWithNecond.AsFloat*taSpecInProcentN.AsFloat/100)-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInNecondChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=14 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInProcentZChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=17 then   //���-�� % �����
  begin
    taSpecInZemlia.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentZ.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-taSpecInNecond.AsFloat-taSpecInKolWithNecond.AsFloat*taSpecInProcentZ.AsFloat/100-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInZemliaChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=18 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInProcentOChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=19 then   //���-�� % �������
  begin
    taSpecInOthodi.AsFloat:=taSpecInKolWithNecond.AsFloat*taSpecInProcentO.AsFloat/100;
    rQ:=taSpecInKolWithNecond.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInKolWithNecond.AsFloat*taSpecInProcentO.AsFloat/100;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInOthodiChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=20 then   //���-�� ��������� ���������
  begin
    taSpecInKolWithNecond.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;

    taSpecInNecond.AsFloat:=taSpecInProcentN.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInZemlia.AsFloat:=taSpecInProcentZ.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;
    taSpecInOthodi.AsFloat:=taSpecInProcentO.AsFloat*taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat/100;

    rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;
    taSpecInKol.AsFloat:=rQ;
    taSpecInSumCenaTovarPost.AsFloat:=taSpecInCenaTovar.AsFloat*rQ;
    taSpecInSumCenaTovar.AsFloat:=RoundVal(taSpecInNewCenaTovar.AsFloat*rQ);
    taSpecInNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
    taSpecInOutNDSSum.AsFloat:=taSpecInCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
    taSpecInSumCenaTara.AsFloat:=taSpecInKolMest.AsFloat*taSpecInCenaTara.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInCenaTaraChange(Sender: TField);
begin
  // ���� ����
  if iCol=22 then
  begin
    taSpecInSumCenaTara.AsFloat:=taSpecInCenaTara.AsFloat*taSpecInKolMest.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecInSumCenaTaraChange(Sender: TField);
begin
  if iCol=24 then //����� �� ����
  begin
    if taSpecInKolMest.AsFloat<>0 then
      taSpecInCenaTara.AsFloat:=taSpecInSumCenaTara.AsFloat/taSpecInKolMest.AsFloat;
  end;
end;

procedure TfmAddDoc1.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then acReadBar1.Execute;
end;

procedure TfmAddDoc1.acSetGTDExecute(Sender: TObject);
Var i,j,iCode,iNum:Integer;
    Rec:TcxCustomGridRecord;
    sDocNum:String;
begin
  //��������� ��� �� ���������� ������
  if ViewDoc1.Controller.SelectedRecordCount=0 then exit;

  fmSetGTD:=tfmSetGTD.Create(Application);
  fmSetGTD.Label1.Caption:='�������� '+its(ViewDoc1.Controller.SelectedRecordCount)+' �������.';
  fmSetGTD.cxTextEdit1.Text:='';
  fmSetGTD.ShowModal;
  if fmSetGTD.ModalResult=mrOk then
  begin
    if fmSetGTD.cxTextEdit1.Text>'' then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� .. ���� ��������� ��������.');

      iCol:=0;
      ViewDoc1.BeginUpdate;
      if cxButton1.Enabled=False then
      begin
        if dmMT.ptTTnInLn.Active=False then dmMT.ptTTnInLn.Active:=True else dmMT.ptTTnInLn.Refresh;
        dmMT.ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
      end;  

      for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc1.Controller.SelectedRecords[i];
        iCode:=0;
        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin iCode:=Rec.Values[j];  end;
          if ViewDoc1.Columns[j].Name='ViewDoc1Num' then begin iNum:=Rec.Values[j]; end;
        end;

        if (iCode>0)and(iNum>0) then
        begin
          Memo1.Lines.Add('  ��� - '+its(iCode));

          if taSpecIn.Locate('Num',iNum,[]) then
          begin
            if taSpecInCodeTovar.AsInteger=iCode then
            begin
              taSpecIn.Edit;
              taSpecInGTD.AsString:=fmSetGTD.cxTextEdit1.Text;
              taSpecIn.Post;
              delay(10);

              //���� � ��������� ����� � � ����
              if cxButton1.Enabled=False then
              begin
                Memo1.Lines.Add('    ����� � ����..');

                with dmMt do
                begin
                  ptTTnInLn.CancelRange;
                  ptTTnInLn.SetRange([iCode,cxDateEdit1.Date],[iCode,cxDateEdit1.Date]);
                  ptTTnInLn.First;
                  while not ptTTnInLn.Eof do
                  begin
                    if abs(ptTTnInLnMediatorCost.AsFloat-iNum)<0.001 then
                    begin
                      sDocNum:=OemToAnsiConvert(ptTTnInLnNumber.AsString);
                      if sDocNum=cxTextEdit1.Text then
                      begin
                        ptTTnInLn.Edit;
                        ptTTnInLnSertNumber.AsString:=ansiToOemConvert(fmSetGTD.cxTextEdit1.Text);
                        ptTTnInLn.Post;
                        Memo1.Lines.Add('      ��.'); delay(10);
                      end;
                    end;

                    ptTTnInLn.Next;
                  end;
                end;
                delay(10);
              end;
            end;
          end;
        end;
      end;
      ViewDoc1.EndUpdate;
      Memo1.Lines.Add('������� ��������.');
    end;
  end;
  fmSetGTD.Release;
end;

procedure TfmAddDoc1.cxLabel15Click(Sender: TObject);
  var iMax:integer;
      rn1,rn2,rn3,rPr,rPrA:Real; 
begin
  with dmP do
  begin
    quSetMatrix.Active:=false;
    quSetMatrix.Active:=true;
    fmSetMatrix.ShowModal;
    if fmSetMatrix.ModalResult=mrOK then
    begin
      iMax:=1;
      taSpecIn.First;
      if not taSpecIn.Eof then
      begin
        taSpecIn.Last;
        iMax:=taSpecInNum.AsInteger+1;
      end;
      prCalcPriceOut(quSetMatrixID.AsInteger,Trunc(Date),0,0,rn1,rn2,rn3,rPr,rPrA);

      taSpecIn.Append;
      taSpecInNum.AsInteger:=iMax;
      taSpecInCodeTovar.AsInteger:=quSetMatrixID.AsInteger;
      taSpecInCodeEdIzm.AsInteger:=quSetMatrixEdIzm.AsInteger;
      taSpecInBarCode.AsString:=quSetMatrixBarCode.AsString;
      taSpecInNDSProc.AsFloat:=quSetMatrixNDS.AsFloat*fmAddDoc1.Label17.Tag;;
   {   taSpecInNDSSum.AsFloat:=0;
      taSpecInOutNDSSum.AsFloat:=0;  }
      taSpecInBestBefore.AsDateTime:=Date;
      taSpecInKolMest.AsInteger:=1;
      taSpecInKolEdMest.AsFloat:=1;
      taSpecInKolWithMest.AsFloat:=1;
      taSpecInKolWithNecond.AsFloat:=0;
      taSpecInKol.AsFloat:=1;
      taSpecInCenaTovar.AsFloat:=rPr;
      taSpecInProcent.AsFloat:=0;
      taSpecInNewCenaTovar.AsFloat:=rPr;
      taSpecInSumCenaTovarPost.AsFloat:=rPr;
      taSpecInSumCenaTovar.AsFloat:=rPr;
      taSpecInProcentN.AsFloat:=0;
      taSpecInNecond.AsFloat:=0;
      taSpecInCenaNecond.AsFloat:=0;
      taSpecInSumNecond.AsFloat:=0;
      taSpecInProcentZ.AsFloat:=0;
      taSpecInZemlia.AsFloat:=0;
      taSpecInProcentO.AsFloat:=0;
      taSpecInOthodi.AsFloat:=0;
      taSpecInName.AsString:=quSetMatrixName.AsString;
      taSpecInCodeTara.AsInteger:=0;
      taSpecInVesTara.AsFloat:=0;
      taSpecInCenaTara.AsFloat:=0;
      taSpecInSumCenaTara.AsFloat:=0;
      taSpecInRealPrice.AsFloat:=quSetMatrixCena.AsFloat;
      taSpecInRemn.AsFloat:=quSetMatrixReserv1.AsFloat;
      taSpecInNac1.AsFloat:=rn1;
      taSpecInNac2.AsFloat:=rn2;
      taSpecInNac3.AsFloat:=rn3;
      taSpecIniCat.AsINteger:=quSetMatrixV02.AsInteger;
      taSpecInPriceNac.AsFloat:=quSetMatrixReserv5.AsFloat;
      if quSetMatrixV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
      if quSetMatrixV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
      taSpecInCenaTovarAvr.AsFloat:=rPrA;
      taSpecInsMaker.AsString:=quSetMatrixNAMEM.AsString;
      taSpecInSumVesTara.AsFloat:=quSetMatrixV12.AsInteger;

      taSpecInNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
      taSpecInOutNDSSum.AsFloat:=taSpecInSumCenaTovarPost.AsFloat/(100+taSpecInNDSProc.AsFloat)*100;


      taSpecIn.Post;
    end;
  end;

end;

procedure TfmAddDoc1.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

end.
