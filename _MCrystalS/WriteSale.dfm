object fmWriteSale: TfmWriteSale
  Left = 1094
  Top = 976
  Width = 363
  Height = 373
  Caption = #1047#1072#1085#1077#1089#1090#1080' '#1074#1099#1088#1091#1095#1082#1091
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 82
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 26
    Height = 13
    Caption = #1044#1072#1090#1072
  end
  object Label3: TLabel
    Left = 16
    Top = 88
    Width = 45
    Height = 13
    Caption = #1050#1072#1089#1089#1072' '#8470
  end
  object Label4: TLabel
    Left = 16
    Top = 112
    Width = 47
    Height = 13
    Caption = #1057#1084#1077#1085#1072' '#8470
  end
  object Label5: TLabel
    Left = 16
    Top = 160
    Width = 58
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1085#1072#1083'.'
  end
  object Label6: TLabel
    Left = 16
    Top = 184
    Width = 52
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1041#1053
  end
  object Label7: TLabel
    Left = 16
    Top = 208
    Width = 61
    Height = 13
    Caption = #1057#1082#1080#1076#1082#1072' '#1085#1072#1083'.'
  end
  object Label8: TLabel
    Left = 16
    Top = 232
    Width = 55
    Height = 13
    Caption = #1057#1082#1080#1076#1082#1072' '#1041#1053
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 112
    Top = 20
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1052#1072#1075#1072#1079#1080#1085
        FieldName = 'IDS'
      end
      item
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListFieldIndex = 2
    Properties.ListSource = dmMCS.dsquDepsRep
    Style.ButtonStyle = btsOffice11
    TabOrder = 0
    Width = 209
  end
  object cxDateEdit1: TcxDateEdit
    Left = 200
    Top = 60
    TabOrder = 1
    Width = 121
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 200
    Top = 84
    TabOrder = 2
    Width = 121
  end
  object cxSpinEdit2: TcxSpinEdit
    Left = 200
    Top = 108
    TabOrder = 3
    Width = 121
  end
  object Panel1: TPanel
    Left = 0
    Top = 270
    Width = 347
    Height = 65
    Align = alBottom
    Color = 16763541
    TabOrder = 4
    object cxButton1: TcxButton
      Left = 63
      Top = 16
      Width = 89
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 196
      Top = 16
      Width = 89
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 200
    Top = 156
    EditValue = 0.000000000000000000
    TabOrder = 5
    Width = 121
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 200
    Top = 180
    EditValue = 0.000000000000000000
    TabOrder = 6
    Width = 121
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 200
    Top = 204
    EditValue = 0.000000000000000000
    TabOrder = 7
    Width = 121
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 200
    Top = 228
    EditValue = 0.000000000000000000
    TabOrder = 8
    Width = 121
  end
end
