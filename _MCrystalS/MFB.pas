unit MFB;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc, DB, FIBDataSet, pFIBDataSet;

type
  TdmFB = class(TDataModule)
    Cash: TpFIBDatabase;
    trSelectCash: TpFIBTransaction;
    trUpdateCash: TpFIBTransaction;
    prAddPos: TpFIBStoredProc;
    quCashSail: TpFIBDataSet;
    quCashPay: TpFIBDataSet;
    quCashSailSHOPINDEX: TFIBSmallIntField;
    quCashSailCASHNUMBER: TFIBIntegerField;
    quCashSailZNUMBER: TFIBIntegerField;
    quCashSailCHECKNUMBER: TFIBIntegerField;
    quCashSailID: TFIBIntegerField;
    quCashSailCHDATE: TFIBDateTimeField;
    quCashSailARTICUL: TFIBStringField;
    quCashSailBAR: TFIBStringField;
    quCashSailCARDSIZE: TFIBStringField;
    quCashSailQUANTITY: TFIBFloatField;
    quCashSailPRICERUB: TFIBFloatField;
    quCashSailDPROC: TFIBFloatField;
    quCashSailDSUM: TFIBFloatField;
    quCashSailTOTALRUB: TFIBFloatField;
    quCashSailCASHER: TFIBIntegerField;
    quCashSailDEPART: TFIBIntegerField;
    quCashSailOPERATION: TFIBSmallIntField;
    quCashPaySHOPINDEX: TFIBSmallIntField;
    quCashPayCASHNUMBER: TFIBIntegerField;
    quCashPayZNUMBER: TFIBIntegerField;
    quCashPayCHECKNUMBER: TFIBIntegerField;
    quCashPayCASHER: TFIBIntegerField;
    quCashPayCHECKSUM: TFIBFloatField;
    quCashPayPAYSUM: TFIBFloatField;
    quCashPayDSUM: TFIBFloatField;
    quCashPayDBAR: TFIBStringField;
    quCashPayCHDATE: TFIBDateTimeField;
    Casher: TpFIBDatabase;
    trSel1: TpFIBTransaction;
    trSel2: TpFIBTransaction;
    trSel3: TpFIBTransaction;
    trUpd1: TpFIBTransaction;
    trUpd2: TpFIBTransaction;
    trUpd3: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quDel: TpFIBQuery;
    taCardsFB: TpFIBDataSet;
    taBarFB: TpFIBDataSet;
    taDCSTOPFB: TpFIBDataSet;
    taCardsFBARTICUL: TFIBStringField;
    taCardsFBCLASSIF: TFIBIntegerField;
    taCardsFBDEPART: TFIBIntegerField;
    taCardsFBNAME: TFIBStringField;
    taCardsFBCARD_TYPE: TFIBIntegerField;
    taCardsFBMESURIMENT: TFIBStringField;
    taCardsFBPRICE_RUB: TFIBFloatField;
    taCardsFBDISCQUANT: TFIBFloatField;
    taCardsFBDISCOUNT: TFIBFloatField;
    taCardsFBSCALE: TFIBStringField;
    taBarFBBARCODE: TFIBStringField;
    taBarFBCARDARTICUL: TFIBStringField;
    taBarFBCARDSIZE: TFIBStringField;
    taBarFBQUANTITY: TFIBFloatField;
    taBarFBPRICERUB: TFIBFloatField;
    quFisSum: TpFIBDataSet;
    quFisSumCASHNUM: TFIBSmallIntField;
    quFisSumZNUM: TFIBIntegerField;
    quFisSumINOUT: TFIBSmallIntField;
    quFisSumITYPE: TFIBSmallIntField;
    quFisSumRSUM: TFIBFloatField;
    quFisSumIDATE: TFIBIntegerField;
    quFisSumDDATE: TFIBDateTimeField;
    trSelFis: TpFIBTransaction;
    taClassFB: TpFIBDataSet;
    taClassFBTYPE_CLASSIF: TFIBIntegerField;
    taClassFBID: TFIBIntegerField;
    taClassFBIACTIVE: TFIBSmallIntField;
    taClassFBID_PARENT: TFIBIntegerField;
    taClassFBNAME: TFIBStringField;
    taClassFBIEDIT: TFIBSmallIntField;
    taClassFBDISCSTOP: TFIBIntegerField;
    taDCSTOPFBSART: TFIBStringField;
    taDCSTOPFBPROC: TFIBFloatField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function prAddPosFB(ITYPE,ARTICUL,GROUP1,GROUP2,GROUP3,CLIINDEX,DELETED:INTEGER; BARCODE,CARDSIZE,FULLNAME,MESSUR,PASSW:String;QUANT,DSTOP,PRESISION,PRICE:Real):Boolean;
procedure prGetFisSum(CashNum,ZNum,IDate:INteger; Var rSumNal,rSumBN,rSumDisc,rSumRetNal,rSumRetBN:Real);

var
  dmFB: TdmFB;

implementation

uses Un1;

{$R *.dfm}

procedure prGetFisSum(CashNum,ZNum,IDate:INteger; Var rSumNal,rSumBN,rSumDisc,rSumRetNal,rSumRetBN:Real);
begin
  with dmFB do
  begin
    rSumNal:=0; rSumBN:=0; rSumDisc:=0; rSumRetNal:=0; rSumRetBN:=0;
{
SELECT CASHNUM,
       ZNUM,
       INOUT,
       ITYPE,
       RSUM,
       IDATE,
       DDATE
FROM ZLISTDET
where CASHNUM=:ICASHNUM
and ZNUM=:IZNUM
and IDATE=:IDATE
}

    quFisSum.Active:=False;
    quFisSum.ParamByName('ICASHNUM').AsInteger:=CashNum;
    quFisSum.ParamByName('IZNUM').AsInteger:=ZNum;
    quFisSum.ParamByName('IDATE').AsInteger:=IDate-1;
    quFisSum.Active:=True;

    quFisSum.First;
    while not quFisSum.Eof do
    begin
      if (quFisSumINOUT.AsInteger=1) and (quFisSumITYPE.AsInteger=1) then rSumNal:=rv(quFisSumRSUM.asFloat);
      if (quFisSumINOUT.AsInteger=1) and (quFisSumITYPE.AsInteger=2) then rSumBN:=rv(quFisSumRSUM.asFloat);
      if (quFisSumINOUT.AsInteger=-1) and (quFisSumITYPE.AsInteger=1) then rSumRetNal:=rv(quFisSumRSUM.asFloat);
      if (quFisSumINOUT.AsInteger=-1) and (quFisSumITYPE.AsInteger=2) then rSumRetBN:=rv(quFisSumRSUM.asFloat);
      if (quFisSumINOUT.AsInteger=0) then rSumDisc:=rv(quFisSumRSUM.asFloat);

      quFisSum.Next;
    end;

    quFisSum.Active:=False;
  end;
end;


Function prAddPosFB(ITYPE,ARTICUL,GROUP1,GROUP2,GROUP3,CLIINDEX,DELETED:INTEGER; BARCODE,CARDSIZE,FULLNAME,MESSUR,PASSW:String;QUANT,DSTOP,PRESISION,PRICE:Real):Boolean;
begin
  result:=True;
  with dmFB do
  begin
    if Cash.Connected then
    begin

//    EXECUTE PROCEDURE PR_ADDPOS (?ITYPE, ?ARTICUL, ?BARCODE, ?CARDSIZE, ?QUANT, ?DSTOP, ?NAME, ?MESSUR, ?PRESISION, ?GROUP1, ?GROUP2, ?GROUP3, ?PRICE, ?CLIINDEX, ?DELETED, ?PASSW)
    prAddPos.ParamByName('ITYPE').AsInteger:=ITYPE;
    prAddPos.ParamByName('ARTICUL').AsInteger:=ARTICUL;
    prAddPos.ParamByName('BARCODE').AsString:=BARCODE;
    prAddPos.ParamByName('CARDSIZE').AsString:=CARDSIZE;
    prAddPos.ParamByName('QUANT').AsFloat:=QUANT;
    prAddPos.ParamByName('DSTOP').AsFloat:=DSTOP;
    prAddPos.ParamByName('NAME').AsString:=FULLNAME;
    prAddPos.ParamByName('MESSUR').AsString:=MESSUR;
    prAddPos.ParamByName('PRESISION').AsFloat:=PRESISION;
    prAddPos.ParamByName('GROUP1').AsInteger:=GROUP1;
    prAddPos.ParamByName('GROUP2').AsInteger:=GROUP2;
    prAddPos.ParamByName('GROUP3').AsInteger:=GROUP3;
    prAddPos.ParamByName('PRICE').AsFloat:=PRICE;
    prAddPos.ParamByName('CLIINDEX').AsInteger:=CLIINDEX;
    prAddPos.ParamByName('DELETED').AsInteger:=DELETED;
    prAddPos.ParamByName('PASSW').AsString:=PASSW;
    prAddPos.ExecProc;

    end else Result:=False;
  end;
end;

procedure TdmFB.DataModuleCreate(Sender: TObject);
begin
  try
    Cash.Close;
//    Cash.DBName:=CommonSet.CashDB;
//    Cash.Open;
  except
    Cash.Close;
  end;
end;

end.
