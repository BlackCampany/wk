unit DMOPSVOD;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmOPSV = class(TDataModule)
    msConnection: TADOConnection;
    quShops: TADOQuery;
    quShopsIDS: TIntegerField;
    quSel: TADOQuery;
    quA: TADOQuery;
    quGetId: TADOQuery;
    quGetIdretnum: TIntegerField;
    quD: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fGetID(iT:INteger):Integer;

var
  dmOPSV: TdmOPSV;

implementation

{$R *.dfm}

function fGetID(iT:INteger):Integer;
begin
  with dmOPSV do
  begin
    quGetId.Active:=False;
    quGetId.Parameters.ParamByName('ITN').Value:=iT;
    quGetId.Active:=True;
    Result:=quGetIdretnum.AsInteger;
    quGetId.Active:=False;
  end;
end;


end.
