object fmSetCardsParams: TfmSetCardsParams
  Left = 869
  Top = 338
  BorderStyle = bsDialog
  Caption = #1043#1088#1091#1087#1087#1086#1074#1099#1077' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
  ClientHeight = 319
  ClientWidth = 456
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 244
    Width = 32
    Height = 13
    Caption = 'Label1'
    Transparent = True
  end
  object Label2: TLabel
    Left = 12
    Top = 14
    Width = 35
    Height = 13
    Caption = #1054#1073#1098#1077#1084
    Transparent = True
  end
  object Label3: TLabel
    Left = 12
    Top = 40
    Width = 75
    Height = 13
    Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    Transparent = True
  end
  object Label4: TLabel
    Left = 12
    Top = 68
    Width = 79
    Height = 13
    Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
    Transparent = True
  end
  object Label5: TLabel
    Left = 12
    Top = 96
    Width = 36
    Height = 13
    Caption = #1057#1090#1088#1072#1085#1072
    Transparent = True
  end
  object Label6: TLabel
    Left = 12
    Top = 124
    Width = 31
    Height = 13
    Caption = #1041#1088#1101#1085#1076
    Transparent = True
  end
  object Label7: TLabel
    Left = 12
    Top = 152
    Width = 73
    Height = 13
    Caption = #1050#1072#1090'.'#1084#1077#1085#1077#1076#1078#1077#1088
    Transparent = True
  end
  object Label8: TLabel
    Left = 12
    Top = 180
    Width = 70
    Height = 13
    Caption = #1050#1072#1090#1077#1075'. '#1090#1086#1074#1072#1088#1072
    Transparent = True
  end
  object Label9: TLabel
    Left = 12
    Top = 210
    Width = 173
    Height = 13
    Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1094#1077#1085#1091' '#1087#1088#1086#1076#1072#1078#1080' '#1087#1086' '#1086#1090#1076#1077#1083#1091
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 269
    Width = 456
    Height = 50
    Align = alBottom
    BevelInner = bvLowered
    Color = 16763025
    TabOrder = 0
    object cxButton2: TcxButton
      Left = 112
      Top = 8
      Width = 91
      Height = 33
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 264
      Top = 8
      Width = 89
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 208
    Top = 8
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Width = 81
  end
  object cxCheckBox4: TcxCheckBox
    Left = 304
    Top = 8
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 2
    Width = 105
  end
  object cxCheckBox5: TcxCheckBox
    Left = 304
    Top = 36
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 3
    Width = 105
  end
  object cxCheckBox6: TcxCheckBox
    Left = 304
    Top = 64
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 4
    Width = 105
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 100
    Top = 64
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Text = 'cxButtonEdit1'
    Width = 189
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 100
    Top = 36
    Properties.Alignment.Horz = taLeftJustify
    Properties.DropDownAutoSize = True
    Properties.DropDownRows = 10
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        Width = 100
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        MinWidth = 30
        Width = 600
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquAVid
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Width = 189
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 100
    Top = 92
    Properties.Alignment.Horz = taLeftJustify
    Properties.DropDownAutoSize = True
    Properties.DropDownRows = 10
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquCountry
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 7
    Width = 189
  end
  object cxCheckBox1: TcxCheckBox
    Left = 304
    Top = 92
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 8
    Width = 105
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 100
    Top = 120
    Properties.Alignment.Horz = taLeftJustify
    Properties.DropDownAutoSize = True
    Properties.DropDownRows = 10
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEBRAND'
      end>
    Properties.ListSource = dsquBrands
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 9
    Width = 189
  end
  object cxCheckBox2: TcxCheckBox
    Left = 304
    Top = 120
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 10
    Width = 105
  end
  object cxLookupComboBox4: TcxLookupComboBox
    Left = 100
    Top = 148
    Properties.Alignment.Horz = taLeftJustify
    Properties.DropDownAutoSize = True
    Properties.DropDownRows = 10
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 600
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquCatMan
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 11
    Width = 189
  end
  object cxCheckBox3: TcxCheckBox
    Left = 304
    Top = 148
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 12
    Width = 105
  end
  object cxLookupComboBox5: TcxLookupComboBox
    Left = 100
    Top = 176
    Properties.Alignment.Horz = taLeftJustify
    Properties.DropDownAutoSize = True
    Properties.DropDownRows = 10
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1090#1086#1074#1072#1088#1072
        FieldName = 'SID'
      end>
    Properties.ListSource = dsquCateg
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 13
    Width = 73
  end
  object cxCheckBox7: TcxCheckBox
    Left = 304
    Top = 176
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 14
    Width = 105
  end
  object cxCheckBox8: TcxCheckBox
    Left = 304
    Top = 204
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 15
    Width = 105
  end
  object quAVid: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[NAME]'
      '      ,[AFM]'
      '  FROM [dbo].[AVID]')
    Left = 132
    Top = 40
    object quAVidID: TIntegerField
      FieldName = 'ID'
    end
    object quAVidNAME: TStringField
      FieldName = 'NAME'
      Size = 150
    end
    object quAVidAFM: TSmallintField
      FieldName = 'AFM'
    end
  end
  object dsquAVid: TDataSource
    DataSet = quAVid
    Left = 132
    Top = 88
  end
  object quCateg: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[SID]'
      '      ,[COMMENT]'
      '  FROM [dbo].[CATEG]')
    Left = 192
    Top = 40
    object quCategID: TIntegerField
      FieldName = 'ID'
    end
    object quCategSID: TStringField
      FieldName = 'SID'
      FixedChar = True
      Size = 2
    end
    object quCategCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 100
    end
  end
  object dsquCateg: TDataSource
    DataSet = quCateg
    Left = 196
    Top = 92
  end
  object quCountry: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDPARENT]'
      '      ,[ID]'
      '      ,[NAME]'
      '      ,[KOD]'
      '  FROM [dbo].[COUNTRY]'
      'order by [NAME]'
      '')
    Left = 252
    Top = 40
    object quCountryIDPARENT: TIntegerField
      FieldName = 'IDPARENT'
    end
    object quCountryID: TIntegerField
      FieldName = 'ID'
    end
    object quCountryNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object quCountryKOD: TIntegerField
      FieldName = 'KOD'
    end
  end
  object dsquCountry: TDataSource
    DataSet = quCountry
    Left = 252
    Top = 92
  end
  object quCatMan: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <
      item
        Name = 'ICATMAN'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[ID_PARENT]'
      '      ,[NAME]'
      '      ,[UVOLNEN]'
      '      ,[PASSW]'
      '      ,[MODUL1]'
      '      ,[MODUL2]'
      '      ,[MODUL3]'
      '      ,[MODUL4]'
      '      ,[MODUL5]'
      '      ,[MODUL6]'
      '      ,[BARCODE]'
      '      ,[PATHEXP]'
      '      ,[HOSTFTP]'
      '      ,[LOGINFTP]'
      '      ,[PASSWFTP]'
      '      ,[SCLI]'
      '      ,[ALLSHOPS]'
      '      ,[CATMAN]'
      '      ,[POSTACCESS]'
      '  FROM [dbo].[RPERSONAL]'
      '  where [CATMAN]=1 or [ID_PARENT]=:ICATMAN'
      'order by [NAME]')
    Left = 312
    Top = 40
    object quCatManID: TIntegerField
      FieldName = 'ID'
    end
    object quCatManID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
    end
    object quCatManNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quCatManUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
    end
    object quCatManPASSW: TStringField
      FieldName = 'PASSW'
    end
    object quCatManMODUL1: TSmallintField
      FieldName = 'MODUL1'
    end
    object quCatManMODUL2: TSmallintField
      FieldName = 'MODUL2'
    end
    object quCatManMODUL3: TSmallintField
      FieldName = 'MODUL3'
    end
    object quCatManMODUL4: TSmallintField
      FieldName = 'MODUL4'
    end
    object quCatManMODUL5: TSmallintField
      FieldName = 'MODUL5'
    end
    object quCatManMODUL6: TSmallintField
      FieldName = 'MODUL6'
    end
    object quCatManBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object quCatManPATHEXP: TStringField
      FieldName = 'PATHEXP'
      Size = 100
    end
    object quCatManHOSTFTP: TStringField
      FieldName = 'HOSTFTP'
      Size = 100
    end
    object quCatManLOGINFTP: TStringField
      FieldName = 'LOGINFTP'
      Size = 100
    end
    object quCatManPASSWFTP: TStringField
      FieldName = 'PASSWFTP'
      Size = 100
    end
    object quCatManSCLI: TStringField
      FieldName = 'SCLI'
      Size = 200
    end
    object quCatManALLSHOPS: TSmallintField
      FieldName = 'ALLSHOPS'
    end
    object quCatManCATMAN: TSmallintField
      FieldName = 'CATMAN'
    end
    object quCatManPOSTACCESS: TSmallintField
      FieldName = 'POSTACCESS'
    end
  end
  object dsquCatMan: TDataSource
    DataSet = quCatMan
    Left = 312
    Top = 92
  end
  object quBrands: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[NAMEBRAND]'
      '  FROM [dbo].[BRANDS]')
    Left = 368
    Top = 40
    object quBrandsID: TIntegerField
      FieldName = 'ID'
    end
    object quBrandsNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
  end
  object dsquBrands: TDataSource
    DataSet = quBrands
    Left = 368
    Top = 92
  end
end
