// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program CalcSS;

uses
  Forms,
  MainCalcSS in 'MainCalcSS.pas' {fmMainCalcSS},
  Un1 in 'Un1.pas',
  Dm in 'DM.pas' {dmMCS: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainCalcSS, fmMainCalcSS);
  Application.CreateForm(TdmMCS, dmMCS);
  Application.Run;
end.
