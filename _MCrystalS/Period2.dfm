object fmPer1: TfmPer1
  Left = 1155
  Top = 350
  Width = 407
  Height = 332
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 104
    Width = 82
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 140
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 176
    Width = 35
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072
  end
  object Label4: TLabel
    Left = 16
    Top = 24
    Width = 50
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
  end
  object Label5: TLabel
    Left = 228
    Top = 24
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Label6: TLabel
    Left = 16
    Top = 68
    Width = 44
    Height = 13
    Caption = #1052#1072#1075#1072#1079#1080#1085
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 227
    Width = 391
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 80
      Top = 16
      Width = 85
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 236
      Top = 16
      Width = 81
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxDateEdit1: TcxDateEdit
    Left = 96
    Top = 20
    Style.ButtonStyle = btsOffice11
    TabOrder = 1
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 248
    Top = 20
    Style.ButtonStyle = btsOffice11
    TabOrder = 2
    Width = 121
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 112
    Top = 64
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #8470' '#1084#1072#1075#1072#1079#1080#1085#1072
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = dmMCS.dsquShops
    Style.ButtonStyle = btsOffice11
    TabOrder = 3
    Width = 189
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 112
    Top = 100
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1052#1072#1075#1072#1079#1080#1085
        FieldName = 'IDS'
      end
      item
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListFieldIndex = 2
    Properties.ListSource = dmMCS.dsquDepsRep
    Style.ButtonStyle = btsOffice11
    TabOrder = 4
    Width = 189
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 88
    Top = 136
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.ButtonStyle = btsOffice11
    TabOrder = 5
    Text = 'cxButtonEdit1'
    Width = 213
  end
  object cxCheckBox1: TcxCheckBox
    Left = 308
    Top = 64
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox1PropertiesChange
    TabOrder = 6
    Width = 69
  end
  object cxCheckBox2: TcxCheckBox
    Left = 308
    Top = 100
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox2PropertiesChange
    TabOrder = 7
    Width = 69
  end
  object cxCheckBox3: TcxCheckBox
    Left = 308
    Top = 136
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox3PropertiesChange
    TabOrder = 8
    Width = 69
  end
  object cxCheckBox4: TcxCheckBox
    Left = 308
    Top = 172
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox4PropertiesChange
    TabOrder = 9
    Width = 69
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 72
    Top = 172
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    Style.ButtonStyle = btsOffice11
    TabOrder = 10
    Text = 'cxButtonEdit2'
    Width = 229
  end
end
