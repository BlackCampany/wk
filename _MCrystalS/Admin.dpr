// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program Admin;

uses
  Forms,
  MainAdm in 'MainAdm.pas' {fmMainAdm},
  Right in 'Right.pas' {fmRight},
  Un1 in 'Un1.pas',
  Passw in 'PasswAdmin\Passw.pas' {fmPerA},
  Dm in 'DM.pas' {dmMCS: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainAdm, fmMainAdm);
  Application.CreateForm(TdmMCS, dmMCS);
  Application.CreateForm(TfmRight, fmRight);
  Application.Run;
end.
