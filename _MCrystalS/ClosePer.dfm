object fmClosePer: TfmClosePer
  Left = 538
  Top = 114
  Width = 820
  Height = 405
  Caption = #1052#1050#1088#1080#1089#1090#1072#1083'. '#1047#1072#1082#1088#1099#1090#1080#1077' '#1087#1077#1088#1080#1086#1076#1072'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 85
    Align = alTop
    BevelInner = bvLowered
    Color = clRed
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 56
      Width = 120
      Height = 13
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1087#1077#1088#1080#1086#1076' '#1087#1086' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 97
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxButton1: TcxButton
      Left = 296
      Top = 18
      Width = 75
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 384
      Top = 18
      Width = 75
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 144
      Top = 52
      Style.BorderStyle = ebsOffice11
      TabOrder = 2
      Width = 129
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 128
      Top = 20
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListSource = dsquMHList
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 145
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 352
    Width = 812
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GridClose: TcxGrid
    Left = 0
    Top = 85
    Width = 812
    Height = 267
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewClose: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquCloseHist
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewCloseID: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087'.'
        DataBinding.FieldName = 'ID'
        Width = 37
      end
      object ViewCloseDATEDOC: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1090#1086' '#1080#1079#1084#1077#1085#1077#1085#1080#1077' '#1087#1086
        DataBinding.FieldName = 'DDATE'
        Width = 82
      end
      object ViewCloseIDP: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1083' ('#1082#1086#1076')'
        DataBinding.FieldName = 'IPERS'
      end
      object ViewClosePNAME: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1083
        DataBinding.FieldName = 'PNAME'
        Width = 124
      end
      object ViewCloseVALEDIT: TcxGridDBColumn
        Caption = #1050#1086#1075#1076#1072
        DataBinding.FieldName = 'DDATEEDIT'
        SortIndex = 0
        SortOrder = soDescending
        Width = 125
      end
      object ViewCloseISKL: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'ISKL'
      end
      object ViewCloseNAMEMH: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'NAMEMH'
        Width = 216
      end
    end
    object LevelClose: TcxGridLevel
      GridView = ViewClose
    end
  end
  object msConn: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 'FILE NAME=C:\_MCrystalS\Bin\MCR.udl'
    ConnectionTimeout = 1800
    DefaultDatabase = 'MCRYSTAL'
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'SQLOLEDB.1'
    Left = 76
    Top = 177
  end
  object taPersonal: TADOTable
    Connection = msConn
    CursorType = ctStatic
    CommandTimeout = 1000
    TableName = 'RPERSONAL'
    Left = 140
    Top = 176
    object taPersonalID: TIntegerField
      FieldName = 'ID'
    end
    object taPersonalID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
    end
    object taPersonalNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taPersonalUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
    end
    object taPersonalPASSW: TStringField
      FieldName = 'PASSW'
    end
    object taPersonalMODUL1: TSmallintField
      FieldName = 'MODUL1'
    end
    object taPersonalMODUL2: TSmallintField
      FieldName = 'MODUL2'
    end
    object taPersonalMODUL3: TSmallintField
      FieldName = 'MODUL3'
    end
    object taPersonalMODUL4: TSmallintField
      FieldName = 'MODUL4'
    end
    object taPersonalMODUL5: TSmallintField
      FieldName = 'MODUL5'
    end
    object taPersonalMODUL6: TSmallintField
      FieldName = 'MODUL6'
    end
    object taPersonalBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object taPersonalPATHEXP: TStringField
      FieldName = 'PATHEXP'
      Size = 100
    end
    object taPersonalHOSTFTP: TStringField
      FieldName = 'HOSTFTP'
      Size = 100
    end
    object taPersonalLOGINFTP: TStringField
      FieldName = 'LOGINFTP'
      Size = 100
    end
    object taPersonalPASSWFTP: TStringField
      FieldName = 'PASSWFTP'
      Size = 100
    end
    object taPersonalSCLI: TStringField
      FieldName = 'SCLI'
      Size = 200
    end
    object taPersonalALLSHOPS: TSmallintField
      FieldName = 'ALLSHOPS'
    end
    object taPersonalCATMAN: TSmallintField
      FieldName = 'CATMAN'
    end
    object taPersonalPOSTACCESS: TSmallintField
      FieldName = 'POSTACCESS'
    end
  end
  object quPassw: TADOQuery
    Connection = msConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT [ID],[ID_PARENT],[NAME],[UVOLNEN],[PASSW],[MODUL1],[MODUL' +
        '2],[MODUL3],[MODUL4],[MODUL5],[MODUL6],[BARCODE],[ALLSHOPS]'
      'FROM [dbo].[RPERSONAL]'
      'where Uvolnen=1 and Modul3=0'
      'order by NAME')
    Left = 212
    Top = 177
    object quPasswID: TIntegerField
      FieldName = 'ID'
    end
    object quPasswID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
    end
    object quPasswNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quPasswUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
    end
    object quPasswPASSW: TStringField
      FieldName = 'PASSW'
    end
    object quPasswMODUL1: TSmallintField
      FieldName = 'MODUL1'
    end
    object quPasswMODUL2: TSmallintField
      FieldName = 'MODUL2'
    end
    object quPasswMODUL3: TSmallintField
      FieldName = 'MODUL3'
    end
    object quPasswMODUL4: TSmallintField
      FieldName = 'MODUL4'
    end
    object quPasswMODUL5: TSmallintField
      FieldName = 'MODUL5'
    end
    object quPasswMODUL6: TSmallintField
      FieldName = 'MODUL6'
    end
    object quPasswBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object quPasswALLSHOPS: TSmallintField
      FieldName = 'ALLSHOPS'
    end
  end
  object dsPassw: TDataSource
    DataSet = quPassw
    Left = 212
    Top = 233
  end
  object quMHList: TADOQuery
    Connection = msConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT dep.[IDS]'
      '      ,dep.[ID]'
      '      ,dep.[NAME]'
      ' FROM [dbo].[DEPARTS] dep')
    Left = 324
    Top = 177
    object quMHListIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quMHListID: TIntegerField
      FieldName = 'ID'
    end
    object quMHListNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
  end
  object dsquMHList: TDataSource
    DataSet = quMHList
    Left = 324
    Top = 233
  end
  object quCloseHist: TADOQuery
    Connection = msConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ISKL'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT h.[ISKL],h.[ID],h.[IDATE],h.[DDATE],h.[IPERS],h.[DDATEEDI' +
        'T],pe.NAME as PNAME,mh.NAME as NAMEMH'
      '  FROM [dbo].[CLOSEHIST] h'
      'left join dbo.RPERSONAL pe on h.IPERS=pe.ID'
      'left join dbo.DEPARTS mh on h.ISKL=mh.ID   '
      'where h.ISKL=:ISKL'
      'order by h.[ID] desc')
    Left = 404
    Top = 177
    object quCloseHistISKL: TIntegerField
      FieldName = 'ISKL'
    end
    object quCloseHistID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quCloseHistIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quCloseHistDDATE: TDateTimeField
      FieldName = 'DDATE'
    end
    object quCloseHistIPERS: TIntegerField
      FieldName = 'IPERS'
    end
    object quCloseHistDDATEEDIT: TDateTimeField
      FieldName = 'DDATEEDIT'
    end
    object quCloseHistPNAME: TStringField
      FieldName = 'PNAME'
      Size = 200
    end
    object quCloseHistNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 100
    end
  end
  object dsquCloseHist: TDataSource
    DataSet = quCloseHist
    Left = 404
    Top = 233
  end
  object taCloseHistBuh: TADOQuery
    Connection = msConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ISKL'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT h.[ISKL],h.[ID],h.[IDATE],h.[DDATE],h.[IPERS],h.[DDATEEDI' +
        'T]'
      '  FROM [dbo].[CLOSEHISTBUH] h'
      'where h.ISKL=:ISKL'
      'order by h.[ID] desc')
    Left = 480
    Top = 177
    object taCloseHistBuhISKL: TIntegerField
      FieldName = 'ISKL'
    end
    object taCloseHistBuhID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object taCloseHistBuhIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object taCloseHistBuhDDATE: TDateTimeField
      FieldName = 'DDATE'
    end
    object taCloseHistBuhIPERS: TIntegerField
      FieldName = 'IPERS'
    end
    object taCloseHistBuhDDATEEDIT: TDateTimeField
      FieldName = 'DDATEEDIT'
    end
  end
end
