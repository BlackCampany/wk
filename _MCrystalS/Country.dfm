object fmCountry: TfmCountry
  Left = 458
  Top = 254
  Width = 354
  Height = 620
  Caption = #1057#1090#1088#1072#1085#1099' ('#1088#1077#1075#1080#1086#1085#1099')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object CuTree: TTreeView
    Left = 4
    Top = 49
    Width = 293
    Height = 512
    Images = dmMCS.ImageList1
    Indent = 19
    PopupMenu = PopupMenu1
    ReadOnly = True
    RightClickSelect = True
    TabOrder = 0
  end
  object amCu: TActionManager
    Left = 208
    Top = 136
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      OnExecute = acExitExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1072#1085#1091
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelExecute
    end
    object acAddReg: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1088#1077#1075#1080#1086#1085
      OnExecute = acAddRegExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 104
    object N1: TMenuItem
      Action = acAdd
    end
    object N2: TMenuItem
      Action = acAddReg
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Action = acEdit
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = acDel
    end
  end
end
