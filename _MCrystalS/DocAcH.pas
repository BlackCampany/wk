unit DocAcH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar;

type
  TfmDocsAcH = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    GridDocsAc: TcxGrid;
    ViewDocsAc: TcxGridDBTableView;
    LevelDocsAc: TcxGridLevel;
    amDocsAc: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc3: TAction;
    acEditDoc3: TAction;
    acViewDoc3: TAction;
    acDelDoc3: TAction;
    acOnDoc3: TAction;
    acOffDoc3: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Excel1: TMenuItem;
    acEdit1: TAction;
    acTestMoveDoc: TAction;
    acOprih: TAction;
    acRazrub: TAction;
    acCheckBuh: TAction;
    teInLn: TdxMemData;
    teInLnDepart: TIntegerField;
    teInLnDateInvoice: TIntegerField;
    teInLnNumber: TStringField;
    teInLnCliName: TStringField;
    teInLnCode: TIntegerField;
    teInLniM: TIntegerField;
    teInLnNdsSum: TFloatField;
    teInLnNdsProc: TFloatField;
    teInLnKol: TFloatField;
    teInLnKolMest: TFloatField;
    teInLnKolWithMest: TFloatField;
    teInLnCenaTovar: TFloatField;
    teInLnNewCenaTovar: TFloatField;
    teInLnProc: TFloatField;
    teInLnSumCenaTovar: TFloatField;
    teInLnSumNewCenaTovar: TFloatField;
    teInLnCodeTara: TIntegerField;
    teInLnCenaTara: TFloatField;
    teInLnSumCenaTara: TFloatField;
    teInLnDepName: TStringField;
    teInLnCardName: TStringField;
    teInLniCat: TIntegerField;
    teInLniTop: TIntegerField;
    teInLniNov: TIntegerField;
    dsteInLn: TDataSource;
    Panel1: TPanel;
    Memo1: TcxMemo;
    acChangePost: TAction;
    acChangeTypePol: TAction;
    acPrintAddDocs: TAction;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teInLniMaker: TIntegerField;
    teInLnsMaker: TStringField;
    PopupMenu2: TPopupMenu;
    acGroupChange: TAction;
    N13: TMenuItem;
    acMoveCard: TAction;
    teBuffLn: TdxMemData;
    teBuffLniDep: TIntegerField;
    teBuffLnNumDoc: TStringField;
    teBuffLnCode: TIntegerField;
    teInLnV11: TIntegerField;
    teBuffLniMaker: TIntegerField;
    teBuffLnsMaker: TStringField;
    teBuffLniDateDoc: TIntegerField;
    teInLnVolIn: TFloatField;
    teInLnVol: TFloatField;
    teInLniMakerCard: TIntegerField;
    teInLnsMakerCard: TStringField;
    acCrVozTara: TAction;
    acPrintSCHF: TAction;
    ViewDocsAcIDSKL: TcxGridDBColumn;
    ViewDocsAcID: TcxGridDBColumn;
    ViewDocsAcIDATEDOC: TcxGridDBColumn;
    ViewDocsAcNUMDOC: TcxGridDBColumn;
    ViewDocsAcSUM1: TcxGridDBColumn;
    ViewDocsAcSUM2: TcxGridDBColumn;
    ViewDocsAcSum3: TcxGridDBColumn;
    ViewDocsAcIACTIVE: TcxGridDBColumn;
    ViewDocsAcNAMESKL: TcxGridDBColumn;
    fpfmDocsAcH: TFormPlacement;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc3Execute(Sender: TObject);
    procedure acEditDoc3Execute(Sender: TObject);
    procedure acViewDoc3Execute(Sender: TObject);
    procedure ViewDocsAcDblClick(Sender: TObject);
    procedure acDelDoc3Execute(Sender: TObject);
    procedure acOnDoc3Execute(Sender: TObject);
    procedure acOffDoc3Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure acTestMoveDocExecute(Sender: TObject);
    procedure ViewDocsAcCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOprihExecute(Sender: TObject);
    procedure acRazrubExecute(Sender: TObject);
    procedure acCheckBuhExecute(Sender: TObject);
    procedure acChangePostExecute(Sender: TObject);
    procedure acChangeTypePolExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDocAc(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

Function OprDocsAc(IDH:integer; Memo1:TcxMemo):Boolean;
Function OtkatDocsAc(IDH:integer; Memo1:TcxMemo):Boolean;

var
  fmDocsAcH: TfmDocsAcH;
  bClearDocIn:Boolean = false;
  bOpenSpIn:Boolean = False;

implementation

uses Un1, DM, Period1, MainMC, DocSIn, DocAcS;

{$R *.dfm}

Function OtkatDocsAc(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ����� ���������.');

  if CanDo('prOffDocAc') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quAcH.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quAcHID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quAcHIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quAcHIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 1');
        quProc.SQL.Add('EXECUTE [dbo].[prAcSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


Function OprDocsAc(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOnDocAc') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quAcH.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quAcHID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quAcHIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quAcHIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 3');
        quProc.SQL.Add('EXECUTE [dbo].[prAcSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


procedure TfmDocsAcH.prSetValsAddDocAc(iT:SmallINt); //0-����������, 1-��������������, 2-��������
// var user:string;
begin
  with dmMCS do
  begin
    bOpenSpIn:=True;
    if iT=0 then
    begin
      fmDocsAcS.Caption:='���������: ����������.';

      fmDocsAcS.Tag:=0;

      fmDocsAcS.cxTextEdit1.Text:=fGetNumDoc(fmMainMC.Label3.tag,4);  fmDocsAcS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsAcS.cxDateEdit1.Date:=date;  fmDocsAcS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',fmMainMC.Label3.tag,[]);

      fmDocsAcS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsAcS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsAcS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsAcS.cxLabel5.Enabled:=True;
      fmDocsAcS.cxLabel1.Enabled:=True;
      fmDocsAcS.cxLabel2.Enabled:=True;
      fmDocsAcS.cxLabel6.Enabled:=True;
      fmDocsAcS.cxLabel3.Enabled:=True;

      fmDocsAcS.acSaveDoc.Enabled:=True;
      fmDocsAcS.cxButton1.Enabled:=True;

      fmDocsAcS.ViewDoc3.OptionsData.Editing:=True;
      fmDocsAcS.ViewDoc3.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmDocsAcS.Caption:='���������: ��������������.';

      fmDocsAcS.Tag:=quAcHID.AsInteger;

      fmDocsAcS.cxTextEdit1.Text:=quAcHNUMDOC.AsString;  fmDocsAcS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsAcS.cxDateEdit1.Date:=quAcHDATEDOC.AsDateTime;  fmDocsAcS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quAcHIDSKL.AsInteger,[]);

      fmDocsAcS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsAcS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsAcS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsAcS.cxLabel5.Enabled:=True;
      fmDocsAcS.cxLabel1.Enabled:=True;
      fmDocsAcS.cxLabel2.Enabled:=True;
      fmDocsAcS.cxLabel6.Enabled:=True;
      fmDocsAcS.cxLabel3.Enabled:=True;

      fmDocsAcS.acSaveDoc.Enabled:=True;
      fmDocsAcS.cxButton1.Enabled:=True;

      fmDocsAcS.ViewDoc3.OptionsData.Editing:=True;
      fmDocsAcS.ViewDoc3.OptionsData.Deleting:=True;
    end;

    if iT=2 then
    begin
      fmDocsAcS.Caption:='���������: ��������.';

      fmDocsAcS.Tag:=quAcHID.AsInteger;

      fmDocsAcS.cxTextEdit1.Text:=quAcHNUMDOC.AsString;  fmDocsAcS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsAcS.cxDateEdit1.Date:=quAcHDATEDOC.AsDateTime;  fmDocsAcS.cxDateEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quAcHIDSKL.AsInteger,[]);

      fmDocsAcS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsAcS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsAcS.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmDocsAcS.cxLabel5.Enabled:=False;
      fmDocsAcS.cxLabel1.Enabled:=False;
      fmDocsAcS.cxLabel2.Enabled:=False;
      fmDocsAcS.cxLabel6.Enabled:=False;
      fmDocsAcS.cxLabel3.Enabled:=False;

      fmDocsAcS.acSaveDoc.Enabled:=False;
      fmDocsAcS.cxButton1.Enabled:=False;

      fmDocsAcS.ViewDoc3.OptionsData.Editing:=False;
      fmDocsAcS.ViewDoc3.OptionsData.Deleting:=False;
    end;
  end;
end;

procedure TfmDocsAcH.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocsAcH.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsAcH.FormCreate(Sender: TObject);
begin
  fpfmDocsAcH.IniFileName:=sFormIni;
  fpfmDocsAcH.Active:=True; delay(10);
  Timer1.Enabled:=True;
  GridDocsAc.Align:=AlClient;
  ViewDocsAc.RestoreFromIniFile(sGridIni); delay(10);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsAcH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsAc.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmDocsAcH.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      if LevelDocsAc.Visible then
      begin
        fmDocsAcH.Caption:='��������� ���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocsAcH.ViewDocsAc.BeginUpdate;
        try
          quAcH.Active:=False;
          quAcH.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
          quAcH.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
          quAcH.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
          quAcH.Active:=True;
        finally
          fmDocsAcH.ViewDocsAc.EndUpdate;
        end;

      end else
      begin
      end;
    end;
  end;
end;

procedure TfmDocsAcH.acAddDoc3Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDocAc(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmDocsAcS.taSpecAc);
    fmDocsAcS.acSaveDoc.Enabled:=True;
    fmDocsAcS.Show;
  end;
end;

procedure TfmDocsAcH.acEditDoc3Execute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quAcH.RecordCount>0 then //���� ��� �������������
    begin
      if quAcHIACTIVE.AsInteger<3 then
      begin
        prSetValsAddDocAc(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmDocsAcS.taSpecAc);

        fmDocsAcS.ViewDoc3.BeginUpdate;
        quSpecAc.Active:=False;
        quSpecAc.Parameters.ParamByName('IDH').Value:=quAcHID.AsInteger;
        quSpecAc.Parameters.ParamByName('ISKL').Value:=quAcHIDSKL.AsInteger;
        quSpecAc.Active:=True;

        quSpecAc.First;
        while not quSpecAc.Eof do
        begin
          with fmDocsAcS do
          begin
            taSpecAc.Append;
            taSpecAcNum.AsInteger:=quSpecAcNUM.AsInteger;
            taSpecAcName.AsString:=quSpecAcNAME.AsString;
            taSpecAcCodeTovar.AsInteger:=quSpecAcIDCARD.asinteger;
            taSpecAcEdIzm.AsInteger:=quSpecAcEDIZM.AsInteger;
            taSpecAcBarCode.AsString:=quSpecAcSBAR.AsString;
            taSpecAcQuant.AsFloat:=quSpecAcQUANT.AsFloat;
            taSpecAcPriceR.AsFloat:=quSpecAcPRICER.AsFloat;
            taSpecAcPrice.AsFloat:=quSpecAcPRICE.AsFloat;
            taSpecAcPriceN.AsFloat:=quSpecAcPRICEN.AsFloat;
            taSpecAcSumR.AsFloat:=quSpecAcSUMR.AsFloat;
            taSpecAcSumN.AsFloat:=quSpecAcSUMN.AsFloat;
            taSpecAcSumD.AsFloat:=quSpecAcSUMN.AsFloat-quSpecAcSUMR.AsFloat;
            taSpecAcQuantRemn.AsFloat:=0;
            taSpecAc.Post;
          end;
          quSpecAc.Next;
        end;
        quSpecAc.Active:=False;
        fmDocsAcS.acSaveDoc.Enabled:=True;
        fmDocsAcS.ViewDoc3.EndUpdate;

        fmDocsAcS.Show;
      end else  showmessage('� ������ ������� ������������� �������� ���������.');
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;    
  end;
end;

procedure TfmDocsAcH.acViewDoc3Execute(Sender: TObject);
//Var //iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //��������
  if not CanDo('prViewDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quAcH.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDocAc(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmDocsAcS.taSpecAc);

      fmDocsAcS.ViewDoc3.BeginUpdate;
      quSpecAc.Active:=False;
      quSpecAc.Parameters.ParamByName('IDH').Value:=quAcHID.AsInteger;
      quSpecAc.Parameters.ParamByName('ISKL').Value:=quAcHIDSKL.AsInteger;
      quSpecAc.Active:=True;

      quSpecAc.First;
      while not quSpecAc.Eof do
      begin
        with fmDocsAcS do
        begin
          taSpecAc.Append;
          taSpecAcNum.AsInteger:=quSpecAcNUM.AsInteger;
          taSpecAcCodeTovar.AsInteger:=quSpecAcIDCARD.asinteger;
          taSpecAcName.AsString:=quSpecAcNAME.AsString;
          taSpecAcEdIzm.AsInteger:=quSpecAcEDIZM.AsInteger;
          taSpecAcBarCode.AsString:=quSpecAcSBAR.AsString;
          taSpecAcQuant.AsFloat:=quSpecAcQUANT.AsFloat;
          taSpecAcPriceR.AsFloat:=quSpecAcPRICER.AsFloat;
          taSpecAcPrice.AsFloat:=quSpecAcPRICE.AsFloat;
          taSpecAcPriceN.AsFloat:=quSpecAcPRICEN.AsFloat;
          taSpecAcSumR.AsFloat:=quSpecAcSUMR.AsFloat;
          taSpecAcSumN.AsFloat:=quSpecAcSUMN.AsFloat;
          taSpecAcSumD.AsFloat:=quSpecAcSUMN.AsFloat-quSpecAcSUMR.AsFloat;
          taSpecAcQuantRemn.AsFloat:=0;
          taSpecAc.Post;
        end;
        quSpecAc.Next;
      end;
      quSpecAc.Active:=False;
      fmDocsAcS.acSaveDoc.Enabled:=False;
      fmDocsAcS.ViewDoc3.EndUpdate;

      fmDocsAcS.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsAcH.ViewDocsAcDblClick(Sender: TObject);
begin
  //������� �������
  with dmMCS do
  begin
    if quAcH.RecordCount>0 then
    begin
      if quAcHIACTIVE.AsInteger<3 then acEditDoc3.Execute //��������������
      else acViewDoc3.Execute; //��������}
    end;  
  end;
end;

procedure TfmDocsAcH.acDelDoc3Execute(Sender: TObject);
begin
  if not CanDo('prDelDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMCS do
  begin
    if quAcH.RecordCount>0 then //���� ��� �������
    begin
      if quAcHIACTIVE.AsInteger<3 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quAcHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          prAddHist(2,quAcHID.AsInteger,trunc(quAcHDateInvoice.AsDateTime),quAcHNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('DELETE FROM [dbo].[DOCAC_HEAD]');
          quD.SQL.Add('where IDSKL='+its(quAcHIDSKL.AsInteger));
          quD.SQL.Add('and IDATEDOC='+its(quAcHIDATEDOC.AsInteger));
          quD.SQL.Add('and ID='+its(quAcHID.AsInteger));
          quD.ExecSQL;
          delay(100);

          quAcH.Requery();
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsAcH.acOnDoc3Execute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  with dmMCS do
  begin
    if not CanDo('prOnDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    if quAcH.RecordCount>0 then //���� ��� ������������
    begin
      if quAcHIACTIVE.AsInteger<=1 then
      begin
        if quAcHDATEDOC.AsDateTime<prOpenDate(quAcHIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quAcHIDSKL.AsInteger,quAcHIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quAcHIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;

        if quAcHIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('������������ �������� �'+quAcHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quAcHID.AsInteger;
          if OprDocsAc(IDH,fmDocsAcH.Memo1) then
          begin
            if CommonSet.AutoLoadCash=1 then prCashLoadFromDoc(IDH,quAcHIDSKL.AsInteger,3,Memo1);

            ViewDocsAc.BeginUpdate;
            quAcH.Requery();
            quAcH.Locate('ID',IDH,[]);
            ViewDocsAc.EndUpdate;

            prWrLog(quAcHIDSKL.AsInteger,4,1,IDH,'');

            GridDocsAc.SetFocus;
            ViewDocsAc.Controller.FocusRecord(ViewDocsAc.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsAcH.acOffDoc3Execute(Sender: TObject);
Var IDH:INteger;
begin
//��������
  with dmMCS do
  begin
    if not CanDo('prOffDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    if quAcH.RecordCount>0 then //���� ��� ������������
    begin
      if quAcHIACTIVE.AsInteger=3 then
      begin
        if quAcHDATEDOC.AsDateTime<prOpenDate(quAcHIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quAcHIDSKL.AsInteger,quAcHIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quAcHIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, �������� ����������!!!');
          exit;
        end;

        if quAcHIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;
        
        if MessageDlg('�������� �������� �'+quAcHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quAcHID.AsInteger;
          if OtkatDocsAc(IDH,fmDocsAcH.Memo1) then
          begin
            ViewDocsAc.BeginUpdate;
            quAcH.Requery();
            quAcH.Locate('ID',IDH,[]);
            ViewDocsAc.EndUpdate;

            prWrLog(quAcHIDSKL.AsInteger,4,0,IDH,'');

            GridDocsAc.SetFocus;
            ViewDocsAc.Controller.FocusRecord(ViewDocsAc.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;


  end;
end;

procedure TfmDocsAcH.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsAcH.acVidExecute(Sender: TObject);
//Var iC,iCliCB:INteger;
//    Sinn:String;
begin
  //���
  with dmMCS do
  begin
  end;
end;

procedure TfmDocsAcH.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsAcH.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsAc.Visible=False then exit;
  with dmMCS do
  begin
    if quAcH.RecordCount>0 then //���� ��� �������������
    begin
      quSpecAc.Active:=False;
      quSpecAc.ParamByName('IDHD').AsInteger:=quAcHID.AsInteger;
      quSpecAc.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quAcHNAMECL.AsString;
      frVariables.Variable['DocNum']:=quAcHNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quAcHDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quAcHNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecAc.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocsAcH.acCopyExecute(Sender: TObject);
//var Par:Variant;
//    iC:INteger;
begin
  //����������
  with dmMCS do
  begin
    {
    if ViewDocsAc.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quAcHID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quAcHID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quAcHDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quAcHNumber.AsString;
      taHeadDocIdCli.AsInteger:=quAcHCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quAcHNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quAcHDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quAcHNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quAcHSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quAcHSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quAcHIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quAcHSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecAc.Active:=False;
      quSpecAc.ParamByName('IDEP').AsInteger:=quAcHDepart.AsInteger;
      quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quAcHDATEDOC.AsDateTime);
      quSpecAc.ParamByName('SNUM').AsString:=quAcHNumber.AsString;
      quSpecAc.Active:=True;

      quSpecAc.First;
      iC:=1;
      while not quSpecAc.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quAcHID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecAcCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecAcKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecAcKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecAcCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecAcSumCenaTovarPost.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecAcNewCenaTovar.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecAcSumCenaTovar.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecAcNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecAcNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecAcName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecAcCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=quSpecAcProcent.AsFloat;
        taSpecDocIdTara.AsInteger:=quSpecAcCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecAcKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecAcCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecAcSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecAcBarCode.AsString;
        taSpecDoc.Post;

        quSpecAc.Next;   inc(iC);
      end;
      quSpecAc.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocsAcH.acInsertDExecute(Sender: TObject);
//Var StrWk:String;
//    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMCS do
  begin
    {
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prSetValsAddDocAc(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsAcS.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmDocsAcS.Label4.Tag:=taHeadDociTypeCli.AsInteger;

          if prCliNDS(taHeadDociTypeCli.AsInteger,taHeadDocIdCli.AsInteger,0)=1 then
          begin
            fmDocsAcS.Label17.Caption:='���������� ���';
            fmDocsAcS.Label17.Tag:=1;
          end else
          begin
            fmDocsAcS.Label17.Caption:='�� ���������� ���';
            fmDocsAcS.Label17.Tag:=0;
          end;

          fmDocsAcS.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmDocsAcS.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmDocsAcS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsAcS.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmDocsAcS.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmDocsAcS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsAcS.cxLabel1.Enabled:=True;
          fmDocsAcS.cxLabel2.Enabled:=True;
          fmDocsAcS.cxLabel3.Enabled:=True;
          fmDocsAcS.cxLabel4.Enabled:=True;
          fmDocsAcS.cxLabel5.Enabled:=True;
          fmDocsAcS.cxLabel6.Enabled:=True;
          fmDocsAcS.cxButton1.Enabled:=True;

          fmDocsAcS.ViewDoc3.OptionsData.Editing:=True;
          fmDocsAcS.ViewDoc3.OptionsData.Deleting:=True;
          fmDocsAcS.Panel4.Visible:=True;

          CloseTe(fmDocsAcS.taSpecAc);

          if taG.Active=False then taG.Active:=True;

          fmDocsAcS.ViewDoc3.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmDocsAcS do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecAc.Append;
                  taSpecAcNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecAcCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecAcCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecAcBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecAcNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecAcNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecAcOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecAcBestBefore.AsDateTime:=Date;
                  taSpecAcKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecAcKolEdMest.AsFloat:=0;
                  taSpecAcKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecAcKolWithNecond.AsFloat:=0;
                  taSpecAcKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecAcCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecAcProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecAcNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecAcSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecAcSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecAcProcentN.AsFloat:=0;
                  taSpecAcNecond.AsFloat:=0;
                  taSpecAcCenaNecond.AsFloat:=0;
                  taSpecAcSumNecond.AsFloat:=0;
                  taSpecAcProcentZ.AsFloat:=0;
                  taSpecAcZemlia.AsFloat:=0;
                  taSpecAcProcentO.AsFloat:=0;
                  taSpecAcOthodi.AsFloat:=0;
                  taSpecAcName.AsString:=taSpecDocNameC.AsString;
                  taSpecAcCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecAcNameTara.AsString:=StrWk;

                  taSpecAcVesTara.AsFloat:=0;
                  taSpecAcCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecAcSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecAcTovarType.AsInteger:=0;

                  taSpecAcRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecAcRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecAcNac1.AsFloat:=rn1;
                  taSpecAcNac2.AsFloat:=rn2;
                  taSpecAcNac3.AsFloat:=rn3;
                  taSpecAciCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecAcPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecAcsTop.AsString:=' ' else taSpecAcsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecAcsNov.AsString:=' ' else taSpecAcsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecAcSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecAcsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecAcSumVesTara.AsFloat:=0;
                    taSpecAcsMaker.AsString:='';
                  end;
                  taSpecAc.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmDocsAcS.ViewDoc3.EndUpdate;

          fmDocsAcS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsAcS.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocsAcH.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsAc.Visible then
  begin
    prNExportExel6(ViewDocsAc);
  end else
  begin
//    prNExportExel6(ViewCardsIn);
  end;
end;

procedure TfmDocsAcH.FormShow(Sender: TObject);
begin
  if (Pos('����',Person.Name)=0)and(Pos('OPER CB',Person.Name)=0)and(Pos('OPERZAK',Person.Name)=0) then
  begin
    acChangePost.Enabled:=False;
  end else
  begin
    acChangePost.Enabled:=True;
  end;
  Memo1.Clear;
end;

procedure TfmDocsAcH.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViewDocsAc);
end;

procedure TfmDocsAcH.acEdit1Execute(Sender: TObject);
begin
{  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDocAc(0); //0-����������, 1-��������������, 2-��������

    if FileExists(CurDir+'SpecIn.cds') then fmDocsAcS.taSpecAc.Active:=True
    else fmDocsAcS.taSpecAc.CreateDataSet;

    fmDocsAcS.acSaveDoc.Enabled:=True;
    fmDocsAcS.Show;
  end;}
end;

procedure TfmDocsAcH.acTestMoveDocExecute(Sender: TObject);
begin
  //�������� ��������������
  with dmMCS do
  begin
    if not CanDo('prTestMoveDoc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quAcH.RecordCount>0 then //���� ��� ���������
    begin
      if quAcHIACTIVE.AsInteger=3 then
      begin
        if MessageDlg('��������� �������������� �� ��������� �'+quAcHNumber.AsString+' �� '+quAcHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ��������.',Memo1);
         // 1 - �������� ��������������
         // 2 - � ����� ��� �������������
         // 3 - �������� ������

          quSpecAc1.Active:=False;
          quSpecAc1.ParamByName('IDEP').AsInteger:=quAcHDepart.AsInteger;
          quSpecAc1.ParamByName('SDATE').AsDate:=Trunc(quAcHDATEDOC.AsDateTime);
          quSpecAc1.ParamByName('SNUM').AsString:=quAcHNumber.AsString;
          quSpecAc1.Active:=True;

          quSpecAc1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecAc1.Eof do
          begin
//            prDelTMoveId(quAcHDepart.AsInteger,quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quSpecAc1Kol.AsFloat);
            prWh('  -- ��� '+its(quSpecAc1CodeTovar.AsInteger),Memo1); delay(10);

            //������ ��� �������� �� ���� �� ���� ���������� � ���������� ��������
            prDelTMoveDate(quAcHDepart.AsInteger,quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),1);

            //�������� ��� ������� ������ �� ���� � �������� �������� �� ����
            quSpecAcTest.Active:=False;
            quSpecAcTest.SQL.Clear;
            quSpecAcTest.SQL.Add('select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln');
            quSpecAcTest.SQL.Add('left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoice=hd.DateInvoice) and(Ln.Number=hd.Number)');
            quSpecAcTest.SQL.Add('where ln.DateInvoice='''+ds(quAcHDATEDOC.AsDateTime)+'''');
            quSpecAcTest.SQL.Add('and ln.CodeTovar='+its(quSpecAc1CodeTovar.AsInteger));
            quSpecAcTest.SQL.Add('and ln.Depart='+its(quAcHDepart.AsInteger));
            quSpecAcTest.SQL.Add('and hd.AcStatus=3');
            quSpecAcTest.SQL.Add('group by hd.ID');
            quSpecAcTest.Active:=True;
            quSpecAcTest.First;
            while not quSpecAcTest.Eof do
            begin
              prAddTMoveId(quAcHDepart.AsInteger,quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quSpecAcTestID.AsInteger,1,quSpecAcTestKol.AsFloat);
              delay(20);
              quSpecAcTest.Next;
            end;
            quSpecAcTest.Active:=False;

            quSpecAc1.Next; delay(20);
          end;
          quSpecAc1.Active:=False;

         // 4 - �������� ������

          prWh('�������� ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsAcH.ViewDocsAcCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA,sO:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsAc.ColumnCount-1 do
  begin
    if ViewDocsAc.Columns[i].Name='ViewDocsAcIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;

    if ViewDocsAc.Columns[i].Name='ViewDocsAcChekBuh' then
    begin
      sO:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;


  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

  if pos('�����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00E6E6CA; //����������� - ������� ��������
  if pos('����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00F4F4EA; //����������� - �������� �������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocsAcH.acOprihExecute(Sender: TObject);
//Var bOpr:Boolean;
//    iTestZ:INteger;
//    sNumZ:String;
begin
//������������
  with dmMCS do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    {
    if quAcH.RecordCount>0 then //���� ��� ������������
    begin
      if quAcHIACTIVE.AsInteger=0 then
      begin
        if quAcHDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quAcHDepart.AsInteger,Trunc(quAcHDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quAcHNumber.AsString+' �� '+quAcHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          bOpr:=True;

          iTestZ:=0;
          if (quAcHRaion.AsInteger>=1)and(quAcHRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quAcHORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
            quZakHNum.ParamByName('ICLI').AsInteger:=quAcHCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;
          end;

          if bOpr = False then
          begin
            prWH('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...',Memo1);
            Showmessage('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...');
          end else
          begin
            prWH('����� ... ���� ������.',Memo1);

            prAddHist(1,quAcHID.AsInteger,trunc(quAcHDATEDOC.AsDateTime),quAcHNumber.AsString,'�����.',1);

           // 1 - �������� ��������������
           // 2 - � ����� ��� �������������
           // 3 - �������� ������

            quSpecAc1.Active:=False;
            quSpecAc1.ParamByName('IDEP').AsInteger:=quAcHDepart.AsInteger;
            quSpecAc1.ParamByName('SDATE').AsDate:=Trunc(quAcHDATEDOC.AsDateTime);
            quSpecAc1.ParamByName('SNUM').AsString:=quAcHNumber.AsString;
            quSpecAc1.Active:=True;

//            prClearBuf(quAcHID.AsInteger,1,Trunc(quAcHDATEDOC.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quAcHDATEDOC.AsDateTime)) then
            begin
              iNumPost:=PostNum(quAcHID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quAcHDepart.AsInteger)+r+its(quAcHID.AsInteger)+r+ds(quAcHDATEDOC.AsDateTime)+r+quAcHNumber.AsString+r+quAcHINN.AsString+r+fs(quAcHSummaTovarPost.AsFloat)+r+fs(quAcHSummaTovar.AsFloat)+r+fs(quAcHSummaTara.AsFloat)+r+fs(quAcHNDS10.AsFloat)+r+fs(quAcHNDS20.AsFloat)+r);

                quSpecAc1.First;
                while not quSpecAc1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quAcHDepart.AsInteger)+r+its(quAcHID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecAc1CodeTovar.asINteger)+r+fs(quSpecAc1Kol.asfloat)+r+fs(quSpecAc1CenaTovar.asfloat)+r+fs(quSpecAc1NewCenaTovar.asfloat)+r+fs(quSpecAc1SumCenaTovarPost.asfloat)+r+fs(quSpecAc1SumCenaTovar.asfloat)+r+fs(quSpecAc1Cena.asfloat)+r+fs(quSpecAc1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecAc1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              quSpecAc1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecAc1.Eof do
              begin
                if abs(quSpecAc1NewCenaTovar.AsFloat-quSpecAc1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quAcHNumber.AsString,quSpecAc1Cena.AsFloat,quSpecAc1NewCenaTovar.AsFloat);

                prDelTMoveId(quAcHDepart.AsInteger,quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quSpecAc1Kol.AsFloat);
                prAddTMoveId(quAcHDepart.AsInteger,quSpecAc1CodeTovar.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quSpecAc1Kol.AsFloat);

                quSpecAc1.Next;
              end;
              quSpecAc1.Active:=False;


              if quAcHSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecAcT.Active:=False;
                quSpecAcT.ParamByName('IDEP').AsInteger:=quAcHDepart.AsInteger;
                quSpecAcT.ParamByName('SDATE').AsDate:=Trunc(quAcHDATEDOC.AsDateTime);
                quSpecAcT.ParamByName('SNUM').AsString:=quAcHNumber.AsString;
                quSpecAcT.Active:=True;

                quSpecAcT.First;  // ��������������
                while not quSpecAcT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quAcHDepart.AsInteger,quSpecAcTCodeTara.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quSpecAcTKolMest.AsFloat);
                  prAddTaraMoveId(quAcHDepart.AsInteger,quSpecAcTCodeTara.AsInteger,Trunc(quAcHDATEDOC.AsDateTime),quAcHID.AsInteger,1,quSpecAcTKolMest.AsFloat,quSpecAcTCenaTara.AsFloat);

                  quSpecAcT.Next;
                end;
                quSpecAcT.Active:=False;
              end;

           // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quAcHDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quAcHDATEDOC.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quAcHNumber.AsString+'''');
              quE.ExecSQL;

              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quAcH,ViewDocsAc,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quAcHDATEDOC.AsDateTime))>=1 then prRecalcTO(trunc(quAcHDATEDOC.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsAcH.acRazrubExecute(Sender: TObject);
//Var iC:INteger;
//    StrWk:String;
//    rSumP,rQ,rNac,rSumNDS,rSumOutNDS:Real;
begin
// ������������ �������� �� ������
  with dmMCS do
  begin
    if not CanDo('prRazrIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quAcH.RecordCount>0 then //���� ��� ������������
    begin
      if quAcHIACTIVE.AsInteger<3 then
      begin
        if quAcHDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quAcHDepart.AsInteger,Trunc(quAcHDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������
          prButtonSet(False);

          prWH('����� ... ���� ������������.',Memo1);

          // 4 - �������� ������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=2');
          quE.SQL.Add('where Depart='+its(quAcHDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quAcHDATEDOC.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quAcHNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quAcH,ViewDocsAc,0);

          prWh('������ ��.',Memo1);

          prButtonSet(True);


          prSetValsAddDocAc(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsAcS.cxCurrencyEdit1.EditValue:=quAcHSummaTovarPost.AsFloat;
          fmDocsAcS.cxTextEdit1.Text:=quAcHNumber.AsString+'rzr';
          fmDocsAcS.Label4.Tag:=quAcHIndexPost.AsInteger;

          if prCliNDS(quAcHIndexPost.AsInteger,quAcHCodePost.AsInteger,0)=1 then
          begin
            fmDocsAcS.Label17.Caption:='���������� ���';
            fmDocsAcS.Label17.Tag:=1;
          end else
          begin
            fmDocsAcS.Label17.Caption:='�� ���������� ���';
            fmDocsAcS.Label17.Tag:=0;
          end;

          fmDocsAcS.cxButtonEdit1.Tag:=quAcHCodePost.AsInteger;
          fmDocsAcS.cxButtonEdit1.Text:=quAcHNameCli.AsString;
          fmDocsAcS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsAcS.cxLookupComboBox1.EditValue:=quAcHDepart.AsInteger;
          fmDocsAcS.cxLookupComboBox1.Text:=quAcHNameDep.AsString;
          fmDocsAcS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsAcS.cxLabel1.Enabled:=True;
          fmDocsAcS.cxLabel2.Enabled:=True;
          fmDocsAcS.cxLabel3.Enabled:=True;
          fmDocsAcS.cxLabel4.Enabled:=True;
          fmDocsAcS.cxLabel5.Enabled:=True;
          fmDocsAcS.cxLabel6.Enabled:=True;
          fmDocsAcS.cxButton1.Enabled:=True;

          fmDocsAcS.ViewDoc3.OptionsData.Editing:=True;
          fmDocsAcS.ViewDoc3.OptionsData.Deleting:=True;
          fmDocsAcS.Panel4.Visible:=True;

          CloseTe(fmDocsAcS.taSpecAc);

          if taG.Active=False  then taG.Active:=True;
          if ptTTK.Active=False  then ptTTK.Active:=True;

          fmDocsAcS.ViewDoc3.BeginUpdate;

          quSpecAc.Active:=False;
          quSpecAc.ParamByName('IDEP').AsInteger:=quAcHDepart.AsInteger;
          quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quAcHDATEDOC.AsDateTime);
          quSpecAc.ParamByName('SNUM').AsString:=quAcHNumber.AsString;
          quSpecAc.Active:=True;

          quSpecAc.First;
          iC:=1;
          while not quSpecAc.Eof do
          begin
            with fmDocsAcS do
            begin
              if ptTTK.Active=False then ptTTK.Active:=True;
              ptTTK.CancelRange;
              if ptTTK.locate('ID',quSpecAcCodeTovar.AsInteger,[]) then
              begin
                rSumP:=rv(quSpecAcSumCenaTovarPost.AsFloat);
                rSumNDS:=rv(quSpecAcNDSSum.AsFloat);
                rSumOutNDS:=rv(quSpecAcOutNDSSum.AsFloat);

                ptTTK.CancelRange;
                ptTTK.SetRange([quSpecAcCodeTovar.AsInteger,0],[quSpecAcCodeTovar.AsInteger,100]);
                ptTTK.First;
                while not ptTTK.Eof do
                begin
                  rNac:=ptTTKPROC2.AsFloat;
                  if rNac<1 then rNac:=rNac*100-100;

                  if dmMT.taG.FindKey([ptTTKIDC.AsInteger]) then
                  begin
                    rQ:=R1000((quSpecAcKolWithMest.AsFloat*quSpecAcKolMest.AsInteger)*ptTTKPROC1.AsFloat/100);

                    taSpecAc.Append;
                    taSpecAcNum.AsInteger:=iC;
                    taSpecAcCodeTovar.AsInteger:=ptTTKIDC.AsInteger;
                    taSpecAcCodeEdIzm.AsInteger:=dmMt.taGEdIzm.AsInteger;
                    taSpecAcBarCode.AsString:=dmMt.taGBarCode.AsString;
                    taSpecAcNDSProc.AsFloat:=dmMt.taGNDS.AsFloat;
                    taSpecAcNDSSum.AsFloat:=rv(rQ*quSpecAcCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecAcOutNDSSum.AsFloat:=rv(rQ*quSpecAcCenaTovar.AsFloat)-rv(rQ*quSpecAcCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecAcBestBefore.AsDateTime:=date+dmMt.taGSrokReal.AsInteger;
                    taSpecAcKolMest.AsInteger:=1;
                    taSpecAcKolEdMest.AsFloat:=0;
                    taSpecAcKolWithMest.AsFloat:=rQ;
                    taSpecAcKolWithNecond.AsFloat:=rQ;
                    taSpecAcKol.AsFloat:=rQ;
                    taSpecAcCenaTovar.AsFloat:=quSpecAcCenaTovar.AsFloat;
                    taSpecAcProcent.AsFloat:=rNac;
                    taSpecAcNewCenaTovar.AsFloat:=rv(quSpecAcCenaTovar.AsFloat+quSpecAcCenaTovar.AsFloat*rNac/100);
                    taSpecAcSumCenaTovarPost.AsFloat:=rv(rQ*quSpecAcCenaTovar.AsFloat);
                    taSpecAcSumCenaTovar.AsFloat:=rQ*rv(quSpecAcCenaTovar.AsFloat+quSpecAcCenaTovar.AsFloat*rNac/100);
                    taSpecAcProcentN.AsFloat:=0;
                    taSpecAcNecond.AsFloat:=0;
                    taSpecAcCenaNecond.AsFloat:=0;
                    taSpecAcSumNecond.AsFloat:=0;
                    taSpecAcProcentZ.AsFloat:=0;
                    taSpecAcZemlia.AsFloat:=0;
                    taSpecAcProcentO.AsFloat:=0;
                    taSpecAcOthodi.AsFloat:=0;
                    taSpecAcName.AsString:=OemToAnsiConvert(dmMt.taGName.AsString);
                    taSpecAcCodeTara.AsInteger:=0;
                    taSpecAcNameTara.AsString:='';

                    taSpecAcVesTara.AsFloat:=0;
                    taSpecAcCenaTara.AsFloat:=0;
                    taSpecAcSumVesTara.AsFloat:=0;
                    taSpecAcSumCenaTara.AsFloat:=0;
                    taSpecAcTovarType.AsInteger:=dmMt.taGTovarType.AsInteger;
                    taSpecAcRealPrice.AsFloat:=dmMt.taGCena.AsFloat;
                    taSpecAciCat.AsInteger:=dmMt.taGV02.AsInteger;
                    if taGV04.AsInteger=0 then taSpecAcsTop.AsString:=' ' else taSpecAcsTop.AsString:='T';
                    if taGV05.AsInteger=0 then taSpecAcsNov.AsString:=' ' else taSpecAcsNov.AsString:='N';

                    taSpecAcsMaker.AsString:='';
                    taSpecAc.Post;

                    rSumP:=rSumP-rv(rQ*quSpecAcCenaTovar.AsFloat);
                    rSumNDS:=rSumNDS-rv(rQ*quSpecAcCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    rSumOutNDS:=rSumOutNDS-(rv(rQ*quSpecAcCenaTovar.AsFloat)-rv(rQ*quSpecAcCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat)));
                  end;

                  ptTTK.Next; inc(iC);
                end;
                //��������� ������� ��������� ���� ����
                if abs(rSumP)>0.000001 then
                begin
                  taSpecAc.Edit;
                  taSpecAcSumCenaTovarPost.AsFloat:=taSpecAcSumCenaTovarPost.AsFloat+rSumP;
                  taSpecAc.Post;
                end;
                if abs(rSumNDS)>0.000001 then
                begin
                  taSpecAc.Edit;
                  taSpecAcNDSSum.AsFloat:=taSpecAcNDSSum.AsFloat+rSumNDS;
                  taSpecAc.Post;
                end;
                if abs(rSumOutNDS)>0.000001 then
                begin
                  taSpecAc.Edit;
                  taSpecAcOutNDSSum.AsFloat:=taSpecAcOutNDSSum.AsFloat+rSumOutNDS;
                  taSpecAc.Post;
                end;

              end else
              begin
                taSpecAc.Append;
                taSpecAcNum.AsInteger:=iC;
                taSpecAcCodeTovar.AsInteger:=quSpecAcCodeTovar.AsInteger;
                taSpecAcCodeEdIzm.AsInteger:=quSpecAcCodeEdIzm.AsInteger;
                taSpecAcBarCode.AsString:=quSpecAcBarCode.AsString;
                taSpecAcNDSProc.AsFloat:=quSpecAcNDSProc.AsFloat;
                taSpecAcNDSSum.AsFloat:=rv(quSpecAcNDSSum.AsFloat);
                taSpecAcOutNDSSum.AsFloat:=rv(quSpecAcOutNDSSum.AsFloat);
                taSpecAcBestBefore.AsDateTime:=quSpecAcBestBefore.AsDateTime;
                taSpecAcKolMest.AsInteger:=quSpecAcKolMest.AsInteger;
                taSpecAcKolEdMest.AsFloat:=0;
                taSpecAcKolWithMest.AsFloat:=quSpecAcKolWithMest.AsFloat;
                taSpecAcKolWithNecond.AsFloat:=quSpecAcKolWithNecond.AsFloat;
                taSpecAcKol.AsFloat:=quSpecAcKol.AsFloat;
                taSpecAcCenaTovar.AsFloat:=quSpecAcCenaTovar.AsFloat;
                taSpecAcProcent.AsFloat:=quSpecAcProcent.AsFloat;
                taSpecAcNewCenaTovar.AsFloat:=quSpecAcNewCenaTovar.AsFloat;
                taSpecAcSumCenaTovarPost.AsFloat:=rv(quSpecAcSumCenaTovarPost.AsFloat);
                taSpecAcSumCenaTovar.AsFloat:=rv(quSpecAcSumCenaTovar.AsFloat);
                taSpecAcProcentN.AsFloat:=quSpecAcProcentN.AsFloat;
                taSpecAcNecond.AsFloat:=quSpecAcNecond.AsFloat;
                taSpecAcCenaNecond.AsFloat:=quSpecAcCenaNecond.AsFloat;
                taSpecAcSumNecond.AsFloat:=quSpecAcSumNecond.AsFloat;
                taSpecAcProcentZ.AsFloat:=quSpecAcProcentZ.AsFloat;
                taSpecAcZemlia.AsFloat:=quSpecAcZemlia.AsFloat;
                taSpecAcProcentO.AsFloat:=quSpecAcProcentO.AsFloat;
                taSpecAcOthodi.AsFloat:=quSpecAcOthodi.AsFloat;
                taSpecAcName.AsString:=quSpecAcName.AsString;
                taSpecAcCodeTara.AsInteger:=quSpecAcCodeTara.AsInteger;

                StrWk:='';
                if quSpecAcCodeTara.AsInteger>0 then fTara(quSpecAcCodeTara.AsInteger,StrWk);
                taSpecAcNameTara.AsString:=StrWk;

                taSpecAcVesTara.AsFloat:=quSpecAcVesTara.AsFloat;
                taSpecAcCenaTara.AsFloat:=quSpecAcCenaTara.AsFloat;
                taSpecAcSumCenaTara.AsFloat:=rv(quSpecAcSumCenaTara.AsFloat);
                taSpecAcTovarType.AsInteger:=quSpecAcTovarType.AsInteger;
                taSpecAcRealPrice.AsFloat:=quSpecAcCena.AsFloat;
                taSpecAciCat.AsInteger:=quSpecAcV02.AsInteger;
                if quSpecAcV04.AsInteger=0 then taSpecAcsTop.AsString:=' ' else taSpecAcsTop.AsString:='T';
                if quSpecAcV05.AsInteger=0 then taSpecAcsNov.AsString:=' ' else taSpecAcsNov.AsString:='N';

                taSpecAcSumVesTara.AsFloat:=quSpecAcSumVesTara.AsFloat;
                if quSpecAcSumVesTara.AsFloat>0.1 then  taSpecAcsMaker.AsString:=prFindMakerName(RoundEx(quSpecAcSumVesTara.AsFloat)) else taSpecAcsMaker.AsString:='';

                taSpecAc.Post;

                inc(iC);
              end;
            end;
            quSpecAc.Next;
          end;

          ptTTK.Active:=False;

          fmDocsAcS.ViewDoc3.EndUpdate;

          fmDocsAcS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsAcS.Show;
        end;
      end else
      begin
        showmessage('�������� ��������� �.�. �������� �����������.');
      end;
    end;}
  end;
end;

procedure TfmDocsAcH.acCheckBuhExecute(Sender: TObject);
//Var iSt:INteger;
begin
//
  with dmMCS do
  begin
    {
    if quAcH.RecordCount>0 then
    begin
      if quAcHChekBuh.AsInteger=0 then fmChangeB.cxRadioButton1.Checked:=True;
      if quAcHChekBuh.AsInteger=1 then fmChangeB.cxRadioButton2.Checked:=True;
      if quAcHChekBuh.AsInteger=2 then fmChangeB.cxRadioButton3.Checked:=True;

      fmChangeB.ShowModal;
      if fmChangeB.ModalResult=mrOk then
      begin
        iSt:=0;
        if fmChangeB.cxRadioButton2.Checked then iSt:=1;
        if fmChangeB.cxRadioButton3.Checked then iSt:=2;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ChekBuh='+its(iSt));
        quE.SQL.Add('where Depart='+its(quAcHDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quAcHDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quAcHNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quAcH,ViewDocsAc,0);

        showmessage('��.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsAcH.acChangePostExecute(Sender: TObject);
begin
// �������� ����������
  with dmMCS do
  begin
    {
    if quAcH.RecordCount>0 then //���� ��� �������
    begin
      try
        fmChangePost:=tfmChangePost.Create(Application);
        fmChangePost.Label1.Caption:='�������� ���������� � ��������� � '+quAcHNumber.AsString;
        fmChangePost.Label3.Caption:=quAcHNameCli.AsString;
        fmChangePost.cxButtonEdit1.Text:='';
        fmChangePost.cxButtonEdit1.Tag:=0;
        fmChangePost.Label4.Tag:=0;

        fmDocsAcS.Label17.Caption:='';
        fmDocsAcS.Label17.Tag:=0;

        fmChangePost.ShowModal;
      finally
        fmChangePost.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsAcH.acChangeTypePolExecute(Sender: TObject);
begin
  //�������� ����������
  with dmMCS do
  begin
    {
    if quAcH.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quAcHNumber.AsString+' �� '+DateToStr(quAcHDATEDOC.AsDateTime)+' ������ ��� '+its(quAcHProvodType.AsInteger);
      if quAcHProvodType.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ProvodType='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quAcHDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quAcHDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quAcHNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quAcH,ViewDocsAc,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsAcH.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMCS do
  begin
{    if ViewDocsAc.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsAc.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=1;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;}
  end;
end;

end.
