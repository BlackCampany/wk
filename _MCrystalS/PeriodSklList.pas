unit PeriodSklList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxGraphics, cxButtonEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxCalendar, cxCheckBox, DB, ADODB, cxCheckComboBox;

type
  TfmPerSklList = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label4: TLabel;
    Label5: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    lbl2: TLabel;
    cxCheckBox2: TcxCheckBox;
    cxCheckComboBox1: TcxCheckComboBox;
    procedure FormCreate(Sender: TObject);
    procedure cxCheckBox2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPerSklList: TfmPerSklList;

implementation

uses Dm, Clients, Un1, ClasSel;

{$R *.dfm}

procedure TfmPerSklList.FormCreate(Sender: TObject);
var It:TcxCheckComboBoxItem;
    i:Integer; 
begin
  cxCheckBox2.Checked:=True;

  cxDateEdit1.Date:=Trunc(Date-30);
  cxDateEdit2.Date:=Date;

  with dmMCS do
  begin
    cxCheckComboBox1.Clear;
    cxCheckComboBox1.Properties.Items.Clear;

    quDeps.Active:=False;
    quDeps.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quDeps.Active:=True;
    quDeps.First;
    while not quDeps.Eof do
    begin
      It:=cxCheckComboBox1.Properties.Items.AddCheckItem('���.� '+quDepsIDS.AsString+'    '+quDepsNAME.AsString,quDepsID.AsString);
      It.Tag:=quDepsID.asinteger;

      quDeps.Next;
    end;
    quDeps.Active:=False;

    if cxCheckComboBox1.Properties.Items.Count>0 then
      for i:=0 to cxCheckComboBox1.Properties.Items.Count-1 do cxCheckComboBox1.States[i]:=cbsChecked;

//     cxCheckComboBox1.States[0]:=cbsChecked;
  end;
end;

procedure TfmPerSklList.cxCheckBox2PropertiesChange(Sender: TObject);
 var i:integer;
begin
  if cxCheckBox2.Checked then
  begin
   //�������� ���
    for i:=0 to cxCheckComboBox1.Properties.Items.Count-1 do
    begin
      cxCheckComboBox1.States[i]:=cbsChecked;
    end;
  end else
  begin
    for i:=0 to cxCheckComboBox1.Properties.Items.Count-1 do
    begin
      cxCheckComboBox1.States[i]:=cbsUnChecked;
    end;
  end;
end;

end.
