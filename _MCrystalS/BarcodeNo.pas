unit BarcodeNo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmBarMessage = class(TForm)
    Image1: TImage;
    Label17: TLabel;
    amBarMess: TActionManager;
    acExit: TAction;
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBarMessage: TfmBarMessage;

implementation

{$R *.dfm}

procedure TfmBarMessage.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
