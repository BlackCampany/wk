unit Country;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, XPStyleActnCtrls, ActnList,
  ActnMan, Menus;

type
  TfmCountry = class(TForm)
    CuTree: TTreeView;
    amCu: TActionManager;
    acExit: TAction;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acAddReg: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddRegExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCountry: TfmCountry;

implementation

uses Dm, AddCountry, Un1;

{$R *.dfm}

procedure TfmCountry.FormCreate(Sender: TObject);
begin
  CuTree.Align:=AlClient;
  CuTree.Items.BeginUpdate;
  prReadCu;
  ClassExpNewTCu(nil,CuTree);
  CuTree.Items.EndUpdate;
end;

procedure TfmCountry.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmCountry.acAddExecute(Sender: TObject);
Var iNum,i:Integer;
     TreeNode : TTreeNode;
begin
  if not CanDo('prAddCountry') then begin Showmessage('��� ����.'); exit; end;
  fmAddCountry:=TfmAddCountry.Create(Application);
  fmAddCountry.Caption:='������ - ����������';
  fmAddCountry.Label1.Caption:='���������� ������.';
  fmAddCountry.TextEdit1.Text:='';
  fmAddCountry.cxSpinEdit1.Value:=0;
  fmAddCountry.ShowModal;
  if fmAddCountry.ModalResult=mrOk then
  begin
    //�������� ������
    with dmMCS do
    begin
      iNum:=fGetID(6);

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[COUNTRY]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddCountry.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([IDPARENT],[ID],[NAME],[KOD])');
      quA.SQL.Add('VALUES  (0,'+its(iNum)+','''+fmAddCountry.TextEdit1.Text+''','+its(fmAddCountry.cxSpinEdit1.Value)+')');
      quA.ExecSQL;

      CuTree.Items.BeginUpdate;

      TreeNode:=CuTree.Items.AddChildObject(nil,fmAddCountry.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      CuTree.Items.EndUpdate;

      FOR i:=0 To CuTree.Items.Count-1 Do
        IF Integer(CuTree.Items[i].Data) = iNum Then
        Begin
          CuTree.Items[i].Expand(False);
          CuTree.Items[i].Selected:=True;
          CuTree.Repaint;
          Break;
        End;

    end;
  end;
  fmAddCountry.Release;
end;

procedure TfmCountry.acAddRegExecute(Sender: TObject);
Var iNum,i,iParent,iP:Integer;
    TreeNode : TTreeNode;
    CurNode,ParentN:TTreeNode;
    sN:String;
begin
  //�������� ������
  if not CanDo('prAddCountryReg') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    CurNode:=CuTree.Selected;
    CurNode.Expand(False);
    ParentN:=CurNode;

    iParent:=0;

    iNum:=INteger(CurNode.Data);

    with dmMCS do
    begin
      quA.SQL.Clear;
      quA.SQL.Add('Select [IDPARENT],[ID],[NAME],[KOD] from [dbo].[COUNTRY]');
      quA.SQL.Add('where [ID]='+its(iNum));
      quA.Open;
      if quA.RecordCount>0 then
      begin
        iP:=quA.FieldByName('IDPARENT').Value;
        sN:=quA.FieldByName('NAME').Value;
        iNum:=quA.FieldByName('ID').Value;
        quA.Close;

        if iP=0 then iParent:=iNum
        else
        begin
          iParent:=iP;
          sN:='';
          quA.SQL.Clear;
          quA.SQL.Add('Select [IDPARENT],[ID],[NAME],[KOD] from [dbo].[COUNTRY]');
          quA.SQL.Add('where [ID]='+its(iP));
          quA.Open;
          if quA.RecordCount>0 then sN:=quA.FieldByName('NAME').Value;
          quA.Close;

          ParentN:=CurNode.Parent;
        end;
      end else quA.Close;
    end;


    fmAddCountry:=TfmAddCountry.Create(Application);
    fmAddCountry.Caption:='������ - ����������';
    fmAddCountry.Label1.Caption:='���������� ������� �  "'+sN+'"';
    fmAddCountry.TextEdit1.Text:='';
    fmAddCountry.Label3.Visible:=False;
    fmAddCountry.cxSpinEdit1.Visible:=False;
    fmAddCountry.ShowModal;

    if fmAddCountry.ModalResult=mrOk then
    begin
    //�������� ������
      iNum:=fGetID(6);

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[COUNTRY]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddCountry.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([IDPARENT],[ID],[NAME],[KOD])');
      quA.SQL.Add('VALUES  ('+its(iParent)+','+its(iNum)+','''+fmAddCountry.TextEdit1.Text+''',0)');
      quA.ExecSQL;

      //�������, ��� ������
      CuTree.Items.BeginUpdate;

      TreeNode:=CuTree.Items.AddChildObject(ParentN,fmAddCountry.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      CuTree.Items.EndUpdate;

      FOR i:=0 To CuTree.Items.Count-1 Do
        IF Integer(CuTree.Items[i].Data) = iNum Then
        Begin
          CuTree.Items[i].Expand(False);
          CuTree.Repaint;
          CuTree.Items[i].Selected:=True;
          Break;
        End;
    end;
  end;
  fmAddCountry.Release;
end;

procedure TfmCountry.acEditExecute(Sender: TObject);
Var iNum,iP,iCod:Integer;
    CurNode:TTreeNode;
    sN:String;
begin
  //�������������
  if not CanDo('prEditCountry') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    CurNode:=CuTree.Selected;
    CurNode.Expand(False);

    iNum:=INteger(CurNode.Data);

    quA.SQL.Clear;
    quA.SQL.Add('Select [IDPARENT],[ID],[NAME],[KOD] from [dbo].[COUNTRY]');
    quA.SQL.Add('where [ID]='+its(iNum));
    quA.Open;
    if quA.RecordCount>0 then
    begin
      iP:=quA.FieldByName('IDPARENT').Value;
      sN:=quA.FieldByName('NAME').Value;
      iNum:=quA.FieldByName('ID').Value;
      iCod:=quA.FieldByName('KOD').Value;
      quA.Close;

      if iP=0 then //������
      begin
        fmAddCountry:=TfmAddCountry.Create(Application);
        fmAddCountry.Caption:='������ - ��������������';
        fmAddCountry.Label1.Caption:='������ - ��������������.';
        fmAddCountry.TextEdit1.Text:=sN;
        fmAddCountry.Label3.Visible:=True;
        fmAddCountry.cxSpinEdit1.Visible:=True;
        fmAddCountry.cxSpinEdit1.Value:=iCod;
        fmAddCountry.ShowModal;

        if fmAddCountry.ModalResult=mrOk then
        begin
          quE.SQL.Clear;
          quE.SQL.Add('UPDATE [dbo].[COUNTRY]');
          quE.SQL.Add('SET [NAME] = '''+fmAddCountry.TextEdit1.Text+'''');
          quE.SQL.Add(',[KOD] = '+its(fmAddCountry.cxSpinEdit1.Value));
          quE.SQL.Add('WHERE ID='+its(iNum));
          quE.ExecSQL;
        end;
        CurNode.Text:=fmAddCountry.TextEdit1.Text;

        fmAddCountry.Release;
      end else   //������
      begin
        fmAddCountry:=TfmAddCountry.Create(Application);
        fmAddCountry.Caption:='������ - ��������������';
        fmAddCountry.Label1.Caption:='������ - ��������������.';
        fmAddCountry.TextEdit1.Text:=sN;
        fmAddCountry.Label3.Visible:=False;
        fmAddCountry.cxSpinEdit1.Visible:=False;
        fmAddCountry.ShowModal;

        if fmAddCountry.ModalResult=mrOk then
        begin
          quE.SQL.Clear;
          quE.SQL.Add('UPDATE [dbo].[COUNTRY]');
          quE.SQL.Add('SET [NAME] = '''+fmAddCountry.TextEdit1.Text+'''');
          quE.SQL.Add('WHERE ID='+its(iNum));
          quE.ExecSQL;
        end;
        CurNode.Text:=fmAddCountry.TextEdit1.Text;

        fmAddCountry.Release;
      end;
    end else quA.Close;
  end;
end;

procedure TfmCountry.acDelExecute(Sender: TObject);
Var iNum:Integer;
    CurNode:TTreeNode;
    bDel:Boolean;
begin
  //�������
  if not CanDo('prDelCountry') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    CurNode:=CuTree.Selected;
    CurNode.Expand(False);

    iNum:=INteger(CurNode.Data);

    //��������� ������� ����� � ������� ����� ������������� � ���������
    if iNum>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� ����������� "'+CurNode.Text+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        bDel:=True;

        quA.SQL.Clear;
        quA.SQL.Add('Select [IDPARENT],[ID],[NAME],[KOD] from [dbo].[COUNTRY]');
        quA.SQL.Add('where [IDPARENT]='+its(iNum));
        quA.Open;
        if quA.RecordCount>0 then bDel:=False;
        quA.Close;

        quA.SQL.Clear;
        quA.SQL.Add('SELECT [ID] FROM [dbo].[CARDS]');
        quA.SQL.Add('where [COUNTRY]='+its(iNum));
        quA.Open;
        if quA.RecordCount>0 then bDel:=False;
        quA.Close;

        if bDel then
        begin
          quD.Active:=False;
          quD.SQL.Clear;
          quD.SQL.Add('DELETE FROM [dbo].[COUNTRY]');
          quD.SQL.Add('WHERE [ID]='+its(iNum));
          quD.ExecSQL;

          CurNode.Delete;
        end else showmessage('�� ������ ���� ������, �������� ����������.');
      end;
    end;
  end;
end;

end.
