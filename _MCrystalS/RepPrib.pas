unit RepPrib;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar;

type
  TfmRepPrib = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRepPrib: TcxGrid;
    ViewRepPrib: TcxGridDBTableView;
    LevelRepPrib: TcxGridLevel;
    Panel5: TPanel;
    Memo1: TcxMemo;
    fmplRepPrib: TFormPlacement;
    amRepPrib: TActionManager;
    acMoveRep1: TAction;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    ViewRepPribID: TcxGridDBColumn;
    ViewRepPribNAME: TcxGridDBColumn;
    ViewRepPribFULLNAME: TcxGridDBColumn;
    ViewRepPribBARCODE: TcxGridDBColumn;
    ViewRepPribEDIZM: TcxGridDBColumn;
    ViewRepPribRNAC: TcxGridDBColumn;
    ViewRepPribsClass: TcxGridDBColumn;
    ViewRepPribrQIn: TcxGridDBColumn;
    ViewRepPribrSIn: TcxGridDBColumn;
    ViewRepPribrSIn0: TcxGridDBColumn;
    ViewRepPribrSInR: TcxGridDBColumn;
    ViewRepPribrQRetOut: TcxGridDBColumn;
    ViewRepPribrSRetOut: TcxGridDBColumn;
    ViewRepPribrSRetOut0: TcxGridDBColumn;
    ViewRepPribrSRetOutR: TcxGridDBColumn;
    ViewRepPribrQSpisOut: TcxGridDBColumn;
    ViewRepPribrSSpisOut: TcxGridDBColumn;
    ViewRepPribrSSpisOut0: TcxGridDBColumn;
    ViewRepPribrSSpisOutR: TcxGridDBColumn;
    ViewRepPribrQRealOut: TcxGridDBColumn;
    ViewRepPribrSRealOut: TcxGridDBColumn;
    ViewRepPribrSRealOut0: TcxGridDBColumn;
    ViewRepPribrSRealOutR: TcxGridDBColumn;
    ViewRepPribrQInvItog: TcxGridDBColumn;
    ViewRepPribrSInvItog: TcxGridDBColumn;
    ViewRepPribrSInv0Itog: TcxGridDBColumn;
    ViewRepPribrSInvRItog: TcxGridDBColumn;
    ViewRepPribrQBeg: TcxGridDBColumn;
    ViewRepPribrSBeg: TcxGridDBColumn;
    ViewRepPribrSBeg0: TcxGridDBColumn;
    ViewRepPribrSBegR: TcxGridDBColumn;
    ViewRepPribrQEnd: TcxGridDBColumn;
    ViewRepPribrSEnd: TcxGridDBColumn;
    ViewRepPribrSEnd0: TcxGridDBColumn;
    ViewRepPribrSEndR: TcxGridDBColumn;
    ViewRepPribrSRealPrib: TcxGridDBColumn;
    acAdvRep: TAction;
    SpeedItem7: TSpeedItem;
    ViewRepPribIDS: TcxGridDBColumn;
    ViewRepPribIDDEP: TcxGridDBColumn;
    ViewRepPribNAMEDEP: TcxGridDBColumn;
    ViewRepPribsClassGr: TcxGridDBColumn;
    ViewRepPribsClass2: TcxGridDBColumn;
    ViewRepPribABCP: TcxGridDBColumn;
    ViewRepPribABCQ: TcxGridDBColumn;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure acMoveRep1Execute(Sender: TObject);
    procedure acAdvRepExecute(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPrib: TfmRepPrib;

implementation

uses Un1, Dm, MainMC, Move, SummaryRDoc;

{$R *.dfm}

procedure TfmRepPrib.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewRepPrib.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmRepPrib.FormCreate(Sender: TObject);
begin
  fmplRepPrib.IniFileName:=sFormIni;
  fmplRepPrib.Active:=True; delay(10);

  GridRepPrib.Align:=AlClient;
//  ViewRepPrib.RestoreFromIniFile(sGridIni); delay(10);
end;

procedure TfmRepPrib.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPrib.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepPrib);
end;

procedure TfmRepPrib.acMoveRep1Execute(Sender: TObject);
begin
  with dmMCS do
  begin
    if quRepPribData.RecordCount>0 then
    begin
      prFormMove(teRPIDDEP.AsInteger,teRPID.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),teRPNAME.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmRepPrib.acAdvRepExecute(Sender: TObject);
begin
//�����
// ����������� ���������
  fmSummary1:=tfmSummary1.Create(Application);
  fmSummary1.PivotGrid1.Visible:=True;
  fmSummary1.PivotGrid2.Visible:=False;
  fmSummary1.Caption:=fmRepPrib.Caption;
  fmSummary1.ShowModal;
  fmSummary1.Release;

end;

procedure TfmRepPrib.N2Click(Sender: TObject);
begin
  prExpandVi(ViewRepPrib,True)
end;

procedure TfmRepPrib.N3Click(Sender: TObject);
begin
  prExpandVi(ViewRepPrib,False);
end;

end.
