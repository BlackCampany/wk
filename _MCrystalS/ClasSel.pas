unit ClasSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  cxDataStorage;

type
  TfmClassSel = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    CardsTree: TTreeView;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure CardsTreeDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClassSel: TfmClassSel;
  z:integer;

implementation

uses Dm, Un1, Cards;

{$R *.dfm}

procedure TfmClassSel.cxButton2Click(Sender: TObject);
begin
 try
  PosP.Id:=integer(CardsTree.Selected.Data);
  PosP.IdGr:=0;
  PosP.Name:=CardsTree.Selected.Text;
 except
  PosP.Id:=0;
 end;
end;

procedure TfmClassSel.cxButton3Click(Sender: TObject);
begin
  PosP.Id:=0;
  PosP.IdGr:=0;
  PosP.Name:='';
end;

procedure TfmClassSel.CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
{  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,CardsTree,dmC.quClassif);
  end;}
end;

procedure TfmClassSel.CardsTreeDblClick(Sender: TObject);
begin
  if ((CardsTree.Selected<>nil) and (CardsTree.Selected.Level=2)) then
  begin
  end;
end;

procedure TfmClassSel.FormShow(Sender: TObject);
var
  nd:TTreeNode;
  i,q:integer;
begin
//  CardsExpandLevel(nil,CardsTree,dmC.quClassif);
//  z:=integer(fmCards.CardsTree.Selected.Data);
  z:=fmClassSel.Tag;
  if z>0 then
  begin
    nd:=nil;
    for i:=0 to CardsTree.Items.Count-1 do
    begin
      q:=integer(CardsTree.Items.Item[i].Data);
      if q=z then nd:=CardsTree.Items.Item[i];
    end;
    if nd<>nil then CardsTree.Select(nd);
  end;

  CardsTree.SetFocus;
end;

procedure TfmClassSel.FormCreate(Sender: TObject);
begin
  CardsTree.Items:=fmCards.CardsTree.Items;
  delay(10);
end;

end.
