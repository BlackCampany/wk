unit Brands;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  ADODB;

type
  TfmBrands = class(TForm)
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    amAlgClass: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    ViBrands: TcxGridDBTableView;
    leBrands: TcxGridLevel;
    GrBrands: TcxGrid;
    ViBrandsID: TcxGridDBColumn;
    ViBrandsNAME: TcxGridDBColumn;
    quBrands: TADOQuery;
    dsquBrands: TDataSource;
    quBrandsID: TIntegerField;
    quBrandsNAMEBRAND: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBrands: TfmBrands;

implementation

uses Un1, ImpExcel, AddAlgClass, Dm, AddName;

{$R *.dfm}

procedure TfmBrands.FormCreate(Sender: TObject);
begin
  GrBrands.Align:=AlClient;
end;

procedure TfmBrands.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViBrands);
end;

procedure TfmBrands.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmBrands.acAddExecute(Sender: TObject);
begin
  if not CanDo('prAddBrand') then begin Showmessage('��� ����.'); exit; end;
  try
    fmAddName:=tfmAddName.Create(Application);

    fmAddName.cxTextEdit1.Text:='';

    fmAddName.ShowModal;
    if fmAddName.ModalResult=mrOk then
    begin
      try
        fmBrands.ViBrands.BeginUpdate;
        if quBrands.Active then
        begin
          quBrands.Append;
          quBrandsNAMEBRAND.AsString:=fmAddName.cxTextEdit1.Text;
          quBrandsID.AsInteger:=fGetID(5);
          quBrands.Post;
        end;
      finally
        fmBrands.ViBrands.EndUpdate;
      end;
    end;

  finally
    fmAddName.Release;
  end;
end;

procedure TfmBrands.acEditExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditBrand') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    if quBrands.RecordCount>0 then
    begin
      try
        fmAddName:=tfmAddName.Create(Application);

        fmAddName.cxTextEdit1.Text:=quBrandsNAMEBRAND.AsString;
        fmAddName.ShowModal;
        if fmAddName.ModalResult=mrOk then
        begin
          quBrands.Edit;
          quBrandsNAMEBRAND.AsString:=fmAddName.cxTextEdit1.Text;
          quBrands.Post;

          quBrands.Refresh;
        end;
      finally
        fmAddName.Release;
      end;
    end;
  end;
end;

procedure TfmBrands.acDelExecute(Sender: TObject);
begin
  if not CanDo('prDelBrand') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    if quBrands.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quBrandsNAMEBRAND.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quBrands.Delete;
      end;
    end;
  end;
end;

procedure TfmBrands.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel6(ViBrands);
end;

end.
