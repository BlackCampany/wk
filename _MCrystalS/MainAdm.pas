unit MainAdm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ImgList, XPStyleActnCtrls, ActnList, ActnMan,
  ToolWin, ActnCtrls, ActnMenus, ActnColorMaps, ExtCtrls, StdCtrls, Buttons, Placemnt,
  SpeedBar;

type
  TfmMainAdm = class(TForm)
    StatusBar1: TStatusBar;
    AM1: TActionManager;
    ImageList1: TImageList;
    XPColorMap1: TXPColorMap;
    ActionMainMenuBar1: TActionMainMenuBar;
    System: TAction;
    Nastroiki: TAction;
    Exit: TAction;
    aCreateP: TAction;
    aCreateD: TAction;
    delP: TAction;
    DelD: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    procedure FormResize(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure N6Click(Sender: TObject);
    procedure SystemExecute(Sender: TObject);
    procedure NastroikiExecute(Sender: TObject);
    procedure ExitExecute(Sender: TObject);
    procedure aCreatePExecute(Sender: TObject);
    procedure aCreateDExecute(Sender: TObject);
    procedure delPExecute(Sender: TObject);
    procedure DelDExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainAdm: TfmMainAdm;

implementation

uses Right, Dm, Un1, Passw;

{$R *.dfm}

procedure TfmMainAdm.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainAdm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=140;
end;

procedure TfmMainAdm.N6Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAdm.SystemExecute(Sender: TObject);
begin
//  Showmessage('�������');
end;

procedure TfmMainAdm.NastroikiExecute(Sender: TObject);
begin
//  JvSpeedBar1.Visible:=True;
  fmRight.Show;

 // Showmessage('���������');
end;

procedure TfmMainAdm.ExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAdm.aCreatePExecute(Sender: TObject);
Var iNode,ID_Parent:Integer;
    TreeNode,TreeParent : TTreeNode;
begin
 //�������� ������������
  with fmRight do
  begin
    PageControl1.ActivePageIndex:=0;
    with dmMCS do
    begin
      if cxButton2.Enabled then
      begin
        showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
      end
      else
      begin
        if taPersonalId_Parent.AsInteger>0 then
        begin
          showmessage('�������� ��������� ��� ������ ������������.');
        end
        else
        begin
          id_Parent:=taPersonalId.AsInteger;
          TreeParent:=TreePersonal.Selected;
          delay(10);
          //�������� � ��������
          taPersonal.Append;
          taPersonalID.AsInteger:=fGetID(1);
          taPersonalID_PARENT.AsInteger:=ID_Parent;
          taPersonalNAME.AsString:='����� ������������';
          taPersonalUVOLNEN.AsInteger:=1;
          taPersonalPASSW.AsString:='';
          taPersonalModul1.AsInteger:=1;
          taPersonalModul2.AsInteger:=1;
          taPersonalModul3.AsInteger:=1;
          taPersonalModul4.AsInteger:=1;
          taPersonalModul5.AsInteger:=1;
          taPersonalModul6.AsInteger:=1;
          taPersonalBARCODE.AsString:='';
          taPersonal.Post;
          delay(10);

          //���������� ����� � RFunction
          prChangeRFunction.Parameters[1].Value:=taPersonalId.AsInteger;
          prChangeRFunction.Parameters[2].Value:=ID_Parent;
          prChangeRFunction.ExecProc;
          delay(10);

          iNode:=taPersonalId.AsInteger;

          TreePersonal.Items.BeginUpdate;
          TreeNode:=TreePersonal.Items.AddChildObject(TreeParent, '����� ������������', Pointer(iNode));
          TreeNode.ImageIndex:=1;
          TreeNode.SelectedIndex:=3;
          TreePersonal.Items.AddChildObject(TreeNode,'', nil);
          TreePersonal.Items.EndUpdate;

          TreePersonal.Refresh;
          TreePersonal.Selected:=TreeNode;

          delay(10);
          Edit1.Text:='����� ������������';
        end;
      end;
    end;
  end;
end;

procedure TfmMainAdm.aCreateDExecute(Sender: TObject);
Var iNode:Integer;
    TreeNode : TTreeNode;
begin
 //�������� ���������
  with fmRight do
  begin
    PageControl1.ActivePageIndex:=0;
    with dmMCS do
    begin
      if cxButton2.Enabled then
      begin
        showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
      end
      else
      begin
        taPersonal.Append;
        taPersonalID.AsInteger:=fGetID(1);
        taPersonalID_PARENT.AsInteger:=0;
        taPersonalNAME.AsString:='����� ���������';
        taPersonalUVOLNEN.AsInteger:=1;
        taPersonalPASSW.AsString:=' ';
        taPersonalModul1.AsInteger:=1;
        taPersonalModul2.AsInteger:=1;
        taPersonalModul3.AsInteger:=1;
        taPersonalModul4.AsInteger:=1;
        taPersonalModul5.AsInteger:=1;
        taPersonalModul6.AsInteger:=1;
        taPersonalBARCODE.AsString:='';
        taPersonal.Post;

        iNode:=taPersonalId.AsInteger;

        TreePersonal.Items.BeginUpdate;
        TreeNode:=TreePersonal.Items.AddChildObject(Nil, '����� ���������', Pointer(iNode));
        TreeNode.ImageIndex:=0;
        TreeNode.SelectedIndex:=2;
        TreePersonal.Items.AddChildObject(TreeNode,'', nil);
        TreePersonal.Items.EndUpdate;
        TreePersonal.Refresh;
        TreePersonal.Selected:=TreeNode;

        delay(10);

        Edit1.Text:='����� ���������';

      end;
    end;
  end;
end;

procedure TfmMainAdm.delPExecute(Sender: TObject);
begin
//������� ������������
  with dmMCS do
  begin
    if taPersonalID_Parent.AsInteger > 0 then
    begin
      //�������� ����������� �������� ������������.
      prDelPersonal.Active:=False;
      prDelPersonal.Parameters.ParamByName('@ID').Value:=taPersonalId.AsInteger;
      prDelPersonal.ExecProc;
      if prDelPersonal.Parameters.ParamByName('@RETURN_VALUE').Value>0 then
      begin
        showmessage('������� ������������ ������� ������. �� ���� ���� ������ � ������.');
      end
      else
      begin
        taPersonal.Delete;
        with fmRight do
        begin
          TreePersonal.Items.BeginUpdate;
          TreePersonal.Selected.Delete;
          TreePersonal.Items.EndUpdate;

          TreePersonal.Refresh;
        end;
      end;
    end
    else
    begin
      showmessage('�������� ������������.');
    end;
  end;
end;

procedure TfmMainAdm.DelDExecute(Sender: TObject);
begin
  //������� ���������
  with dmMCS do
  begin
    if taPersonalID_Parent.AsInteger = 0 then
    begin //���������
      //�������� ������� �������������.
      prExistPersonal.Active:=False;
      prExistPersonal.Parameters.ParamByName('@ID_Parent').Value:=taPersonalId.AsInteger;
      prExistPersonal.ExecProc;
      if prExistPersonal.Parameters.ParamByName('@RETURN_VALUE').Value>0 then
      begin
        showmessage('���������� ������������ � ������ ����������. �������� ����������.');
      end
      else
      begin
        taPersonal.Delete;
        with fmRight do
        begin
          TreePersonal.Items.BeginUpdate;
          TreePersonal.Selected.Delete;
          TreePersonal.Items.EndUpdate;
          TreePersonal.Refresh;
        end;
      end;
    end
    else
    begin
      showmessage('�������� ���������.');
    end;
  end;
end;

procedure TfmMainAdm.FormShow(Sender: TObject);
begin
  Person.Modul:='Adm';//Adm
  fmPerA:=tfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    fmPerA.Release;
    Caption:=Caption+'   : '+Person.Name;
  end
  else
  begin
    fmPerA.Release;
    delay(100);
    close;
    delay(100);
  end;
  fmRight.quShopsToSel.Active:=False;
  fmRight.quShopsToSel.Active:=True;
  fmRight.Show;
end;

procedure TfmMainAdm.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

procedure TfmMainAdm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Writeini;
end;

end.
