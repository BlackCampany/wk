object fmScaleList: TfmScaleList
  Left = 454
  Top = 472
  BorderStyle = bsDialog
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1077#1089#1086#1074
  ClientHeight = 315
  ClientWidth = 716
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GrSc: TcxGrid
    Left = 16
    Top = 12
    Width = 669
    Height = 285
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewSc: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMCS.dsquScaleList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object ViewScID: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1074#1077#1089#1086#1074
        DataBinding.FieldName = 'ID'
        Width = 61
      end
      object ViewScSCALETYPE: TcxGridDBColumn
        Caption = #1052#1086#1076#1077#1083#1100
        DataBinding.FieldName = 'SCALETYPE'
        Width = 63
      end
      object ViewScNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 156
      end
      object ViewScIP1: TcxGridDBColumn
        DataBinding.FieldName = 'IP1'
        Width = 70
      end
      object ViewScIP2: TcxGridDBColumn
        DataBinding.FieldName = 'IP2'
        Width = 70
      end
      object ViewScIP3: TcxGridDBColumn
        DataBinding.FieldName = 'IP3'
        Width = 70
      end
      object ViewScIP4: TcxGridDBColumn
        DataBinding.FieldName = 'IP4'
        Width = 70
      end
      object ViewScIP5: TcxGridDBColumn
        DataBinding.FieldName = 'IP5'
        Width = 70
      end
    end
    object LevelSc: TcxGridLevel
      GridView = ViewSc
    end
  end
end
