unit RepLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls;

type
  TfmRepLog = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRepLog: TcxGrid;
    ViewRepLog: TcxGridDBTableView;
    LevelRepLog: TcxGridLevel;
    ViewRepLogIDATE: TcxGridDBColumn;
    ViewRepLogITYPED: TcxGridDBColumn;
    ViewRepLogIDH: TcxGridDBColumn;
    ViewRepLogISKL: TcxGridDBColumn;
    ViewRepLogIOPER: TcxGridDBColumn;
    ViewRepLogSPERS: TcxGridDBColumn;
    ViewRepLogDDATE: TcxGridDBColumn;
    ViewRepLogDOCTYPE: TcxGridDBColumn;
    ViewRepLogNAMEDEP: TcxGridDBColumn;
    ViewRepLogID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepLog: TfmRepLog;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmRepLog.FormCreate(Sender: TObject);
begin
  GridRepLog.Align:=AlClient;
end;

procedure TfmRepLog.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepLog.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepLog);
end;

end.
