unit ClosePer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, ComCtrls,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxContainer, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls, cxButtons,
  ExtCtrls, ADODB;

type
  TfmClosePer = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    StatusBar1: TStatusBar;
    GridClose: TcxGrid;
    ViewClose: TcxGridDBTableView;
    ViewCloseID: TcxGridDBColumn;
    ViewCloseDATEDOC: TcxGridDBColumn;
    ViewCloseIDP: TcxGridDBColumn;
    ViewClosePNAME: TcxGridDBColumn;
    ViewCloseVALEDIT: TcxGridDBColumn;
    ViewCloseISKL: TcxGridDBColumn;
    ViewCloseNAMEMH: TcxGridDBColumn;
    LevelClose: TcxGridLevel;
    msConn: TADOConnection;
    taPersonal: TADOTable;
    taPersonalID: TIntegerField;
    taPersonalID_PARENT: TIntegerField;
    taPersonalNAME: TStringField;
    taPersonalUVOLNEN: TSmallintField;
    taPersonalPASSW: TStringField;
    taPersonalMODUL1: TSmallintField;
    taPersonalMODUL2: TSmallintField;
    taPersonalMODUL3: TSmallintField;
    taPersonalMODUL4: TSmallintField;
    taPersonalMODUL5: TSmallintField;
    taPersonalMODUL6: TSmallintField;
    taPersonalBARCODE: TStringField;
    taPersonalPATHEXP: TStringField;
    taPersonalHOSTFTP: TStringField;
    taPersonalLOGINFTP: TStringField;
    taPersonalPASSWFTP: TStringField;
    taPersonalSCLI: TStringField;
    taPersonalALLSHOPS: TSmallintField;
    taPersonalCATMAN: TSmallintField;
    taPersonalPOSTACCESS: TSmallintField;
    quPassw: TADOQuery;
    quPasswID: TIntegerField;
    quPasswID_PARENT: TIntegerField;
    quPasswNAME: TStringField;
    quPasswUVOLNEN: TSmallintField;
    quPasswPASSW: TStringField;
    quPasswMODUL1: TSmallintField;
    quPasswMODUL2: TSmallintField;
    quPasswMODUL3: TSmallintField;
    quPasswMODUL4: TSmallintField;
    quPasswMODUL5: TSmallintField;
    quPasswMODUL6: TSmallintField;
    quPasswBARCODE: TStringField;
    dsPassw: TDataSource;
    quPasswALLSHOPS: TSmallintField;
    quMHList: TADOQuery;
    dsquMHList: TDataSource;
    quMHListIDS: TIntegerField;
    quMHListID: TIntegerField;
    quMHListNAME: TStringField;
    quCloseHist: TADOQuery;
    dsquCloseHist: TDataSource;
    quCloseHistISKL: TIntegerField;
    quCloseHistID: TLargeintField;
    quCloseHistIDATE: TIntegerField;
    quCloseHistDDATE: TDateTimeField;
    quCloseHistIPERS: TIntegerField;
    quCloseHistDDATEEDIT: TDateTimeField;
    quCloseHistPNAME: TStringField;
    quCloseHistNAMEMH: TStringField;
    taCloseHistBuh: TADOQuery;
    taCloseHistBuhISKL: TIntegerField;
    taCloseHistBuhID: TLargeintField;
    taCloseHistBuhIDATE: TIntegerField;
    taCloseHistBuhDDATE: TDateTimeField;
    taCloseHistBuhIPERS: TIntegerField;
    taCloseHistBuhDDATEEDIT: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClosePer: TfmClosePer;
  sErr:String;

implementation

uses Un1, PaswClosePer;

{$R *.dfm}

procedure TfmClosePer.FormCreate(Sender: TObject);
Var StrWk:String;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  sErr:='';

  try
    msConn.Connected:=False;
    Strwk:='FILE NAME='+CurDir+'mcr.udl';
    msConn.ConnectionString:=Strwk;
    delay(10);
    msConn.Connected:=True;
    delay(10);
    if msConn.Connected then
    begin
      taPersonal.Active:=True;

    end;
  except
    sErr:='���� �� �����������.';
  end;
  if sErr>'' then showmessage(sErr);
end;

procedure TfmClosePer.FormShow(Sender: TObject);
begin
  fmPerA_ClosePer:=TfmPerA_ClosePer.Create(Application);
  fmPerA_ClosePer.StatusBar1.Panels[0].Text:=sErr;
  fmPerA_ClosePer.ShowModal;
  if fmPerA_ClosePer.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; // �������� �������� �������
  end
  else
  begin
    delay(100);
    close;
    delay(100);
  end;
  fmPerA_ClosePer.Release;

  quMHList.Active:=False;
  quMHList.SQL.Clear;
  quMHList.SQL.Add('SELECT dep.[IDS]');
  quMHList.SQL.Add('      ,dep.[ID] ');
  quMHList.SQL.Add('      ,dep.[NAME]');
  quMHList.SQL.Add(' FROM [dbo].[DEPARTS] dep');
  if Person.AllShop<>1 then quMHList.SQL.Add(' where dep.[IDS] in (Select ISHOP FROM RPERSONALSHOP where IDP='+its(Person.Id)+')');
  quMHList.Active:=True;

  if quMHList.RecordCount>0 then
  begin
    cxLookupComboBox1.EditValue:=quMHListID.AsInteger;
    quCloseHist.Active:=False;
    quCloseHist.Parameters.ParamByName('ISKL').Value:=quMHListID.AsInteger;
    quCloseHist.Active:=True;
    quCloseHist.First;
  end;

  cxDateEdit1.Date:=date;
end;

procedure TfmClosePer.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  ViewClose.BeginUpdate;
  quCloseHist.Active:=False;
  quCloseHist.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
  quCloseHist.Active:=True;
  quCloseHist.First;
  ViewClose.EndUpdate;
end;

procedure TfmClosePer.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmClosePer.cxButton1Click(Sender: TObject);
Var bClose:Boolean;
    DateCloseBuh:TDateTime;
begin
  if cxLookupComboBox1.EditValue=0 then
  begin
    ShowMessage('�������� ����� ��������.');
    exit;
  end;
  if MessageDlg('�� ������������� ������ ������� ��� ��������� ������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������ ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    bClose:=True;

    taCloseHistBuh.Active:=False;
    taCloseHistBuh.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
    taCloseHistBuh.Active:=True;

    taCloseHistBuh.First;

    quCloseHist.First;

    DateCloseBuh:=Date;

    if taCloseHistBuh.RecordCount>0 then
    begin
      DateCloseBuh:=taCloseHistBuhIDATE.AsInteger;
      if taCloseHistBuhIDATE.AsInteger>(Trunc(cxDateEdit1.Date)) then bClose:=False; //���� ������������� �������� ������� ������� ���� ��� �� ��������� �������
    end;
    taCloseHistBuh.Active:=False;

    if bClose then
    begin
      cxButton2.Enabled:=False;
      ViewClose.BeginUpdate;

      quCloseHist.Append;
      quCloseHistISKL.AsInteger:=cxLookupComboBox1.EditValue;
      quCloseHistIDATE.AsInteger:=Trunc(cxDateEdit1.Date);
      quCloseHistDDATE.AsDateTime:=Trunc(cxDateEdit1.Date);
      quCloseHistIPERS.AsInteger:=Person.Id;
      quCloseHistDDATEEDIT.AsDateTime:=Now;
      quCloseHist.Post;

      quCloseHist.Requery();
      quCloseHist.First;
      
      ViewClose.EndUpdate;
      cxButton2.Enabled:=True;

      showmessage('������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������, ������ ��� ���������.');

    end else showmessage('�������� ����������, ������ ������ ������������ �� '+FormatDateTime('dd.mm.yyyy',DateCloseBuh)+' ������������.');
  end;
end;

end.
