object fmSelParCen: TfmSelParCen
  Left = 287
  Top = 575
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1077#1095#1072#1090#1080' '#1094#1077#1085#1085#1080#1082#1086#1074
  ClientHeight = 193
  ClientWidth = 278
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 140
    Width = 278
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TcxButton
      Left = 28
      Top = 12
      Width = 101
      Height = 33
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object Button2: TcxButton
      Left = 152
      Top = 12
      Width = 89
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 16
    Top = 8
    Properties.Items = <
      item
        Caption = #1085#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1087#1086#1079#1080#1094#1080#1080
      end
      item
        Caption = #1085#1072' '#1074#1089#1077' '#1090#1086#1074#1072#1088#1099' '#1089' '#1080#1079#1084#1077#1085#1080#1074#1096#1077#1081#1089#1103' '#1094#1077#1085#1086#1081
      end
      item
        Caption = #1085#1072' '#1074#1089#1077' '#1090#1086#1074#1072#1088#1099
      end>
    ItemIndex = 0
    TabOrder = 1
    Height = 129
    Width = 249
  end
end
