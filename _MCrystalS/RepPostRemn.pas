unit RepPostRemn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo, ExtCtrls,
  SpeedBar, cxCalendar;

type
  TfmRepPostRemn = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    Panel5: TPanel;
    Memo1: TcxMemo;
    GridRepPostRemn: TcxGrid;
    ViewRepPostRemn: TcxGridDBTableView;
    LevelRepPostRemn: TcxGridLevel;
    fmplRepPostRemn: TFormPlacement;
    amRepPostRemn: TActionManager;
    acMoveRep1: TAction;
    acAdvRep: TAction;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    ViewRepPostRemnIDSKL: TcxGridDBColumn;
    ViewRepPostRemnIDCARD: TcxGridDBColumn;
    ViewRepPostRemnQPART: TcxGridDBColumn;
    ViewRepPostRemnQREMN: TcxGridDBColumn;
    ViewRepPostRemnPRICEIN: TcxGridDBColumn;
    ViewRepPostRemnPRICEIN0: TcxGridDBColumn;
    ViewRepPostRemnPRICER: TcxGridDBColumn;
    ViewRepPostRemnSumInRemn: TcxGridDBColumn;
    ViewRepPostRemnSumIn0Remn: TcxGridDBColumn;
    ViewRepPostRemnSumRRemn: TcxGridDBColumn;
    ViewRepPostRemnIDATEDOC: TcxGridDBColumn;
    ViewRepPostRemnNUMDOC: TcxGridDBColumn;
    ViewRepPostRemnNUMSF: TcxGridDBColumn;
    ViewRepPostRemnIDCLI: TcxGridDBColumn;
    ViewRepPostRemnSUMIN: TcxGridDBColumn;
    ViewRepPostRemnCLINAME: TcxGridDBColumn;
    ViewRepPostRemnCLIFULLNAME: TcxGridDBColumn;
    ViewRepPostRemnNAME: TcxGridDBColumn;
    ViewRepPostRemnFULLNAME: TcxGridDBColumn;
    ViewRepPostRemnBARCODE: TcxGridDBColumn;
    ViewRepPostRemnEDIZM: TcxGridDBColumn;
    ViewRepPostRemnRNAC: TcxGridDBColumn;
    ViewRepPostRemnsClass: TcxGridDBColumn;
    ViewRepPostRemnDEPNAME: TcxGridDBColumn;
    ViewRepPostRemnIDHEAD: TcxGridDBColumn;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPostRemn: TfmRepPostRemn;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmRepPostRemn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  ViewRepPostRemn.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmRepPostRemn.FormCreate(Sender: TObject);
begin
  fmplRepPostRemn.IniFileName:=sFormIni;
  fmplRepPostRemn.Active:=True; delay(10);

  GridRepPostRemn.Align:=AlClient;
end;

procedure TfmRepPostRemn.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPostRemn.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepPostRemn);
end;

procedure TfmRepPostRemn.N3Click(Sender: TObject);
begin
  prExpandVi(ViewRepPostRemn,False);
end;

procedure TfmRepPostRemn.N2Click(Sender: TObject);
begin
  prExpandVi(ViewRepPostRemn,True);
end;

end.
