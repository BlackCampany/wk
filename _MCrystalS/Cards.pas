unit Cards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxImageComboBox, cxDBLookupComboBox, StdCtrls, RXSplit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxCheckBox, cxButtons, cxContainer,
  cxTextEdit, ComCtrls, ExtCtrls, SpeedBar, Placemnt, ActnList,
  XPStyleActnCtrls, ActnMan, dxmdaset,FR_Class;

type
  TfmCards = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    Panel2: TPanel;
    CardsTree: TTreeView;
    Panel1: TPanel;
    Label1: TLabel;
    TextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennQuant: TcxGridDBColumn;
    ViCennQuanrR: TcxGridDBColumn;
    le1: TcxGridLevel;
    Le2: TcxGridLevel;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton4: TcxButton;
    cxButton8: TcxButton;
    RxSplitter1: TRxSplitter;
    Panel3: TPanel;
    Edit1: TEdit;
    CardGr: TcxGrid;
    CardsView: TcxGridDBTableView;
    CardLevel: TcxGridLevel;
    FormPlacementCard: TFormPlacement;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    amCard: TActionManager;
    acExit: TAction;
    acAdd: TAction;
    acEdit: TAction;
    acView: TAction;
    acDel: TAction;
    acBarView: TAction;
    acJurnal: TAction;
    acMove: TAction;
    acNotUse: TAction;
    acMove1: TAction;
    acTransGr: TAction;
    acRemns2: TAction;
    acCardUse: TAction;
    acFilials: TAction;
    acTransList: TAction;
    acExcel: TAction;
    acTPrint: TAction;
    acFindName: TAction;
    acCashLoad: TAction;
    acPrintCen: TAction;
    acCalcRemns: TAction;
    acCardsGoods: TAction;
    acPostTov: TAction;
    acAddPrint: TAction;
    acPrintQuPrint: TAction;
    acTermFile: TAction;
    acScaleIn: TAction;
    acAddInScale: TAction;
    acRecalcRemn1: TAction;
    acLoadScale: TAction;
    acReceipt: TAction;
    acSelectAll: TAction;
    acViewLoad: TAction;
    acCalcMoveLine: TAction;
    acPartSel: TAction;
    acSearchCode: TAction;
    acSetGroup: TAction;
    acGetVReal: TAction;
    acLoadSDisc: TAction;
    acPrintCutTail: TAction;
    acSaveAlco: TAction;
    acAddPosToDoc: TAction;
    acScan: TAction;
    CardsViewID: TcxGridDBColumn;
    CardsViewNAME: TcxGridDBColumn;
    CardsViewFULLNAME: TcxGridDBColumn;
    CardsViewBARCODE: TcxGridDBColumn;
    CardsViewEDIZM: TcxGridDBColumn;
    CardsViewNDS: TcxGridDBColumn;
    CardsViewISTATUS: TcxGridDBColumn;
    CardsViewISTENDER: TcxGridDBColumn;
    CardsViewISTOP: TcxGridDBColumn;
    CardsViewISNOV: TcxGridDBColumn;
    CardsViewKREP: TcxGridDBColumn;
    CardsViewAVID: TcxGridDBColumn;
    CardsViewRNAC: TcxGridDBColumn;
    CardsViewPRICE: TcxGridDBColumn;
    CardsViewNAMECU: TcxGridDBColumn;
    CardsViewNAMEPERS: TcxGridDBColumn;
    CardsViewNAMEBRAND: TcxGridDBColumn;
    CardsViewSID: TcxGridDBColumn;
    CardsViewVOL: TcxGridDBColumn;
    acReadBar: TAction;
    teBuff: TdxMemData;
    teBuffCode: TIntegerField;
    PopupMenu2: TPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    acAddPrint1: TMenuItem;
    Label2: TLabel;
    N10: TMenuItem;
    Vi1QUANT: TcxGridDBColumn;
    Vi1PRICEIN: TcxGridDBColumn;
    Vi1SUMIN: TcxGridDBColumn;
    Vi1PRICEUCH: TcxGridDBColumn;
    Vi1SUMUCH: TcxGridDBColumn;
    Vi1DATEDOC: TcxGridDBColumn;
    Vi1NUMDOC: TcxGridDBColumn;
    Vi1Name: TcxGridDBColumn;
    Vi1PROCN: TcxGridDBColumn;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    cxCheckBox2: TcxCheckBox;
    CardsViewRemn: TcxGridDBColumn;
    acRezerroPrice: TAction;
    StatusBar1: TStatusBar;
    CardsViewTTOVAR: TcxGridDBColumn;
    acAddGroup: TAction;
    N14: TMenuItem;
    cxCheckBox3: TcxCheckBox;
    CardsViewqOpSv: TcxGridDBColumn;
    CardsViewABC1: TcxGridDBColumn;
    CardsViewABC3: TcxGridDBColumn;
    acAddParam: TAction;
    N15: TMenuItem;
    N16: TMenuItem;
    SpeedItem12: TSpeedItem;
    acTPrintAdd: TAction;
    N17: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure CardsTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acSearchCodeExecute(Sender: TObject);
    procedure acBarViewExecute(Sender: TObject);
    procedure acScanExecute(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acReadBarExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure CardsViewStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure CardsTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure CardsTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acSetGroupExecute(Sender: TObject);
    procedure SpeedItem11Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acAddPrintExecute(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure acPrintQuPrintExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acTPrintExecute(Sender: TObject);
    procedure CardsViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acAddPosToDocExecute(Sender: TObject);
    procedure CardsViewSelectionChanged(Sender: TcxCustomGridTableView);
    procedure acCashLoadExecute(Sender: TObject);
    procedure acViewLoadExecute(Sender: TObject);
    procedure acAddInScaleExecute(Sender: TObject);
    procedure acMove1Execute(Sender: TObject);
    procedure acAddGroupExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acAddParamExecute(Sender: TObject);
    procedure acTPrintAddExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCards: TfmCards;
  bDrGroup:Boolean=False;

implementation

uses Un1, Dm, AddClassif, MainMC, mFind, BarcodeNo, Bar, AddCards,
  SetCardsPars, DocSIn, Status, u2fdk, GdsLoad, AddInScale, DocSInv,
  DocSOut, Move, DocAcS, DocVnS, AddCardsParams;

{$R *.dfm}

procedure TfmCards.FormCreate(Sender: TObject);
begin
  CardsTree.Items.BeginUpdate;
  prReadClass;
  ClassExpNewT(nil,CardsTree);
  CardsTree.Items.EndUpdate;

  CardsView.RestoreFromIniFile(sGridIni);
  
  FormPlacementCard.IniFileName := sFormIni;
  FormPlacementCard.Active:=True;
  Panel3.Align:=AlClient;
  Label2.Caption:='';
end;

procedure TfmCards.CardsTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bRefreshCard=False then exit;
  with dmMCS do
  begin
    try
      CardsView.BeginUpdate;
      prRefreshCards(Integer(Node.Data),fmMainMC.Label3.tag,cxCheckBox1.Checked,cxCheckBox2.Checked,cxCheckBox3.Checked);
    finally
      CardsView.EndUpdate;
    end;
  end;
end;

procedure TfmCards.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmCards.N1Click(Sender: TObject);
Var iNum,i:Integer;
     TreeNode : TTreeNode;
     
begin
  //�������� ������
  if not CanDo('prAddClassif') then begin Showmessage('��� ����.'); exit; end;

  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ����������';
  fmAddClassif.Label1.Caption:='���������� ������ �������.';
  fmAddClassif.TextEdit1.Text:='';
  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    //�������� ������
    with dmMCS do
    begin
      iNum:=fGetID(3);

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[CLASSIF]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([ID],[IDPARENT],[NAME],[NAC],[R1],[R2],[R3],[I1],[I2],[I3],[S1],[S2],[S3])');
      quA.SQL.Add('VALUES  ('+its(iNum)+',0,'''+fmAddClassif.TextEdit1.Text+''','+fts(fmAddClassif.cxCalcEdit1.Value)+',0,0,0,0,0,0,'''','''','''')');
      quA.ExecSQL;

      CardsTree.Items.BeginUpdate;

      TreeNode:=CardsTree.Items.AddChildObject(nil,fmAddClassif.TextEdit1.Text+' ('+fs(fmAddClassif.cxCalcEdit1.Value)+'%)',Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      // ������� ��������� (������) �������� ����� ������ ��� ����,
// ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
//     CardsTree.Items.AddChildObject(TreeNode,'', nil);

      CardsTree.Items.EndUpdate;


      FOR i:=0 To CardsTree.Items.Count-1 Do
        IF Integer(CardsTree.Items[i].Data) = iNum Then
        Begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Items[i].Selected:=True;
          CardsTree.Repaint;
          Break;
        End;

    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmCards.N2Click(Sender: TObject);
Var iNum,i,iParent:Integer;
    TreeNode : TTreeNode;
    CurNode:TTreeNode;
begin
  //�������� ���������
  if not CanDo('prAddClassifGr') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    CurNode:=CardsTree.Selected;
    CurNode.Expand(False);

    iParent:=INteger(CurNode.Data);
    fmAddClassif:=TfmAddClassif.Create(Application);
    fmAddClassif.Caption:='�������������';
    fmAddClassif.Label1.Caption:='���������� ��������� ������� � ������ "'+CurNode.Text+'"';
    fmAddClassif.TextEdit1.Text:='';
    fmAddClassif.ShowModal;

    if fmAddClassif.ModalResult=mrOk then
    begin
    //�������� ���������
      iNum:=fGetID(3);

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into [dbo].[CLASSIF]'); // values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.SQL.Add('([ID],[IDPARENT],[NAME],[NAC],[R1],[R2],[R3],[I1],[I2],[I3],[S1],[S2],[S3])');
      quA.SQL.Add('VALUES  ('+its(iNum)+','+its(iParent)+','''+fmAddClassif.TextEdit1.Text+''','+fts(fmAddClassif.cxCalcEdit1.Value)+',0,0,0,0,0,0,'''','''','''')');
      quA.ExecSQL;


      //�������, ��� ������
      CardsTree.Items.BeginUpdate;

      TreeNode:=CardsTree.Items.AddChildObject(CurNode,fmAddClassif.TextEdit1.Text+' ('+fs(fmAddClassif.cxCalcEdit1.Value)+'%)',Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;
      // ������� ��������� (������) �������� ����� ������ ��� ����,
      // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
      // CardsTree.Items.AddChildObject(TreeNode,'', nil);

      CardsTree.Items.EndUpdate;

      FOR i:=0 To CardsTree.Items.Count-1 Do
        IF Integer(CardsTree.Items[i].Data) = iNum Then
        Begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Repaint;
          CardsTree.Items[i].Selected:=True;
          Break;
        End;
    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmCards.N6Click(Sender: TObject);
Var iNum:Integer;
    TreeNode : TTreeNode;
    sName:String;
begin
  if not CanDo('prEditClassif') then begin Showmessage('��� ����.'); exit; end;

  TreeNode:=CardsTree.Selected;
  if TreeNode=nil then exit;
  sName:=TreeNode.Text;
  iNum:=Integer(CardsTree.Selected.Data);

  //�������� ��������
  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ���������';
  fmAddClassif.Label1.Caption:='�������� ��������� ������ �������.';

  with dmMCS do
  begin
    quA.SQL.Clear;
    quA.SQL.Add('Select [NAME],[NAC] from [dbo].[CLASSIF]');
    quA.SQL.Add('where [ID]='+IntToStr(iNum));
    quA.Open;
    if quA.RecordCount>0 then
    begin
      fmAddClassif.cxCalcEdit1.Value:=rv(quA.FieldByName('NAC').Value);
      fmAddClassif.TextEdit1.Text:=quA.FieldByName('NAME').Value;
    end;
    quA.Close;
  end;

  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    CardsTree.Items.BeginUpdate;
    TreeNode.Text:=fmAddClassif.TextEdit1.Text+' ('+fs(fmAddClassif.cxCalcEdit1.Value)+'%)';
    CardsTree.Items.EndUpdate;

    with dmMCS do
    begin
      quA.SQL.Clear;
      quA.SQL.Add('Update [dbo].[CLASSIF] Set [NAME]='''+fmAddClassif.TextEdit1.Text+''', [NAC]='+fts(fmAddClassif.cxCalcEdit1.Value));
      quA.SQL.Add('where [ID]='+IntToStr(iNum));
      quA.ExecSQL;
    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmCards.N4Click(Sender: TObject);
Var iNum:Integer;
    CurNode:TTreeNode;
begin
  //��������
  if not CanDo('prDelClassif') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    CurNode:=CardsTree.Selected;
    if CurNode=nil then exit;

    if MessageDlg('�� ������������� ������ ������� ������ - '+CardsTree.Selected.Text+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iNum:=Integer(CurNode.Data);
      if CurNode.getFirstChild=nil then
      begin
        iDel:=iNum;
        if not fCanDel(3) then
        begin
          ShowMessage('�������� ���������� - � ������ ���� �������� ������.');
          exit;
        end else
        begin
          quA.Close;
          quA.SQL.Clear;
          quA.SQL.Add('delete from [dbo].[CLASSIF] where [ID]='+IntToStr(iNum));
          quA.ExecSQL;

          CardsTree.Selected.Delete;
        end;
      end
      else
      begin
        ShowMessage('�������� ���������� - ���� ������� ������.');
        exit;
      end;
    end;
  end;
end;

procedure TfmCards.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CardsView.StoreToIniFile(sGridIni,False);
end;

procedure TfmCards.cxButton2Click(Sender: TObject);
Var i:Integer;
    iCod:INteger;
    s1:String;
begin
//����� �� ��������
  if Length(TextEdit1.Text)<3 then exit;
  with dmMCS do
  begin
    fmFindC.ViewFind.BeginUpdate;
    dsquFindC.DataSet:=nil;
    try
      s1:=TextEdit1.Text;
      quFindC.Active:=False;
      quFindC.SQL.Clear;
      quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
      quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
      quFindC.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(s1)+'%''');
//      quFindC.SQL.Add('and [TTOVAR]=0');
      quFindC.Active:=True;
    finally
      dsquFindC.DataSet:=quFindC;
      fmFindC.ViewFind.EndUpdate;
    end;
    fmFindC.ShowModal;

    if fmFindC.ModalResult=mrOk then
    begin
      if quFindC.RecordCount>0 then
      begin
        bRefreshCard:=False;
        for i:=0 to CardsTree.Items.Count-1 Do
        begin
          if Integer(CardsTree.Items[i].Data) = quFindCIDCLASSIF.AsInteger then
          begin
            CardsTree.Items[i].Expand(False);
            CardsTree.Repaint;
            CardsTree.Items[i].Selected:=True;
            try
              CardsView.BeginUpdate;
              prRefreshCards(quFindCIDCLASSIF.AsInteger,fmMainMC.Label3.Tag,cxCheckBox1.Checked,cxCheckBox2.Checked,cxCheckBox3.Checked);
              iCod:=quFindCID.AsInteger;
              quCards.Locate('ID',iCod,[]);
            finally
              CardsView.EndUpdate;
              CardGr.SetFocus;
              CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
            end;
            Break;
          End;
        end;
        bRefreshCard:=True;

        if quFindCIStatus.AsInteger>100 then
        begin
          Showmessage('�������� ����� ��������� � ��������������.');
        end;
      end;
    end;
  end;
end;

procedure TfmCards.cxButton3Click(Sender: TObject);
Var sBar:String;
    iC,i:Integer;
begin
//����� �� ��
  if Length(TextEdit1.Text)<3 then exit;
  with dmMCS do
  begin
    sBar:=TextEdit1.Text;
    if sBar>'' then
    begin
      if prFindBar(sBar,0,iC) then
      begin
        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
          quFindC.SQL.Add('where ca.[ID]='+its(iC));
//          quFindC.SQL.Add('and [TTOVAR]=0');
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if iC>0 then
        begin
//          StatusBar1.Panels[0].Text:='�� ����, �������� ('+IntToStr(iC)+').';
          bRefreshCard:=False;
          for i:=0 to CardsTree.Items.Count-1 Do
          begin
            if Integer(CardsTree.Items[i].Data) = quFindCIDCLASSIF.AsInteger then
            begin
              CardsTree.Items[i].Expand(False);
              CardsTree.Repaint;
              CardsTree.Items[i].Selected:=True;
              try
                CardsView.BeginUpdate;
                prRefreshCards(quFindCIDCLASSIF.AsInteger,fmMainMC.Label3.Tag,cxCheckBox1.Checked,cxCheckBox2.Checked,cxCheckBox3.Checked);
                quCards.Locate('ID',quFindCID.AsInteger,[]);
              finally
                CardsView.EndUpdate;
                CardGr.SetFocus;
                CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
              end;
              Break;
            End;
          end;
          bRefreshCard:=True;

          if quFindCIStatus.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������.');
          end;
        end;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmCards.cxButton1Click(Sender: TObject);
Var iC,i:Integer;
begin
//����� �� ����
  if Length(TextEdit1.Text)<1 then exit;
  with dmMCS do
  begin
    iC:=StrToIntDef(TextEdit1.Text,0);
    if iC>0 then
    begin
      quFC.Active:=False;
      quFC.SQL.Clear;
      quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
      quFC.SQL.Add('where ca.[ID]='+its(iC));
//      quFC.SQL.Add('and [TTOVAR]=0');
      quFC.Active:=True;

      bRefreshCard:=False;
      for i:=0 to CardsTree.Items.Count-1 Do
      begin
        if Integer(CardsTree.Items[i].Data) = quFCIDCLASSIF.AsInteger then
        begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Repaint;
          CardsTree.Items[i].Selected:=True;
          try
            CardsView.BeginUpdate;
            prRefreshCards(quFCIDCLASSIF.AsInteger,fmMainMC.Label3.Tag,cxCheckBox1.Checked,cxCheckBox2.Checked,cxCheckBox3.Checked);
            if quCards.Locate('ID',quFCID.AsInteger,[])=False then
            begin
              if cxCheckBox1.Checked=False then showmessage('��������� � ��������������');
            end;
          finally
            CardsView.EndUpdate;
            CardGr.SetFocus;
            CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCard:=True;
      quFC.Active:=False;

      if quFCIStatus.AsInteger>100 then
      begin
        Showmessage('�������� ����� ��������� � ��������������.');
      end;
    end;
  end;
end;

procedure TfmCards.acSearchCodeExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmCards.acBarViewExecute(Sender: TObject);
begin
  with dmMCS do
  with fmBar do
  begin
    if quCards.Active then
    begin
      if quCards.RecordCount=0 then exit;

      quBarsEdit.Active:=False;
      quBarsEdit.Parameters.ParamByName('GOODSID').Value:=quCardsId.AsInteger;
      quBarsEdit.Active:=True;

      fmBar.ShowModal;
    end;
  end;
end;

procedure TfmCards.acScanExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmCards.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then acReadBar.Execute;
end;

procedure TfmCards.acReadBarExecute(Sender: TObject);
Var sBar:String;
    IC,i:INteger;
begin
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
  TextEdit1.Text:=sBar;
  if prFindBar(sBar,0,IC) then
  begin
    with dmMCS do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        quFC.Active:=False;
        quFC.SQL.Clear;
        quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFC.SQL.Add('where ca.[ID]='+its(iC));
//        quFC.SQL.Add('and [TTOVAR]=0');
        quFC.Active:=True;

        if quFC.RecordCount>0 then
        begin
          bRefreshCard:=False;
          for i:=0 to CardsTree.Items.Count-1 Do
          begin
            if Integer(CardsTree.Items[i].Data) = quFCIDCLASSIF.AsInteger Then
            begin
              CardsTree.Items[i].Expand(False);
              CardsTree.Repaint;
              CardsTree.Items[i].Selected:=True;
              try
                CardsView.BeginUpdate;
                prRefreshCards(quFCIDCLASSIF.AsInteger,fmMainMC.Label3.Tag,cxCheckBox1.Checked,cxCheckBox2.Checked,cxCheckBox3.Checked);
                if quCards.Locate('ID',iC,[])=False then showmessage('����� '+sBar+' �� ������, ��������� ��������������.');
              finally
                CardsView.EndUpdate;
                CardGr.SetFocus;
                CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
              end;
              Break;
            End;
          end;
          bRefreshCard:=True;

          if quFCISTATUS.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������.');
          end;
        end;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmCards.acAddExecute(Sender: TObject);
Var iItem,iCe:INteger;
    sBar:String;
begin
  //�������� �������� ������
  if not CanDo('prAddCard') then begin Showmessage('��� ����.'); exit; end;

  with fmAddCard do
  begin
    fmAddCard.prSetEditCard(0);

  end;
  fmAddCard.ShowModal;
  if fmAddCard.ModalResult=mrOk then
  begin //��������
    with dmMCS do
    begin
      try
        iItem:=StrToIntDef(fmAddCard.MaskEdit1.Text,0);
        if (iItem>0)and(trim(fmAddCard.MaskEdit2.Text)>'') then
        begin
          sBar:=trim(fmAddCard.MaskEdit2.Text);
          if prFindBar(sBar,-1,iCe)=False then
          begin
            quFC.Active:=False;
            quFC.SQL.Clear;
            quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFC.SQL.Add('where ca.[ID]='+its(iItem));
            quFC.Active:=True;

            if quFC.RecordCount=0 then
            begin
              with fmAddCard do
              begin
                //��������� �����
                quFC.Append;
                quFCIDCLASSIF.AsInteger:=ButtonEdit1.Tag;
                quFCID.AsInteger:=iItem;
                quFCNAME.AsString:=TextEdit1.Text;
                quFCFULLNAME.AsString:=TextEdit2.Text;
                quFCBARCODE.AsString:=sBar;
                quFCTTOVAR.AsInteger:=cxComboBox3.ItemIndex; //�����
                quFCEDIZM.AsInteger:= RadioGroup1.ItemIndex+1;
                quFCNDS.AsFloat:=0;
                if cxComboBox2.ItemIndex=0 then quFCNDS.AsFloat:=18;
                if cxComboBox2.ItemIndex=1 then quFCNDS.AsFloat:=10;
                quFCOKDP.AsFloat:=0;
                quFCISTATUS.AsInteger:=0;
                quFCPRSISION.AsFloat:=1;
                quFCMANCAT.AsInteger:=cxLookupComboBox3.EditValue;
                quFCCOUNTRY.AsInteger:=cxLookupComboBox1.EditValue;
                quFCBRAND.AsInteger:=cxLookupComboBox2.EditValue;
                if cxCheckBox1.Checked then quFCISTENDER.AsInteger:=1 else quFCISTENDER.AsInteger:=0;
                if cxCheckBox2.Checked then quFCISTOP.AsInteger:=1 else quFCISTOP.AsInteger:=0;
                quFCISNOV.AsInteger:=1;
                quFCCATEG.AsInteger:=cxLookupComboBox5.EditValue;
                quFCSROKGODN.AsInteger:=0;
                quFCQUANTPACK.AsFloat:=cxCalcEdit12.EditValue;
                quFCVESBUTTON.AsInteger:=cxSpinEdit1.EditValue;
                quFCVESFORMAT.AsInteger:=cxComboBox1.ItemIndex;
                quFCVES.AsFloat:=cxCalcEdit6.EditValue;
                quFCVOL.AsFloat:=cxCalcEdit2.EditValue;
                quFCWW.AsInteger:=RoundEx(cxCalcEdit4.EditValue*10);
                quFCHH.AsInteger:=RoundEx(cxCalcEdit5.EditValue*10);
                quFCLL.AsInteger:=RoundEx(cxCalcEdit3.EditValue*10);
                quFCKREP.AsFloat:=cxCalcEdit13.EditValue;
                quFCAVID.AsInteger:=cxLookupComboBox4.EditValue;
                quFCMAKER.AsInteger:=cxButtonEdit1.Tag;
                quFCDATECREATE.AsInteger:=Trunc(date);
                quFCAZKOEF.AsFloat:=cxCalcEdit7.EditValue;
                quFCAZSTRAHR.AsFloat:=cxSpinEdit2.EditValue;
                quFCAZSROKREAL.AsInteger:=cxSpinEdit4.EditValue;
                quFCAZDAYSPROC.AsInteger:=cxSpinEdit3.EditValue;
                quFCAZQUANTMAX.AsFloat:=0;
                quFCRNAC.AsFloat:=cxCalcEdit1.EditValue;
                quFCSOSTAV.AsString:=mSostav.Text;
                quFCGOST.AsString:=mGost.Text;
                quFC.Post;

                //��������� ��������
                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT INTO [dbo].[BARCODE]');
                quA.SQL.Add('           ([Bar]');
                quA.SQL.Add('           ,[GoodsId]');
                quA.SQL.Add('           ,[Quant]');
                quA.SQL.Add('           ,[BarFormat]');
                quA.SQL.Add('           ,[BarStatus]');
                quA.SQL.Add('           ,[Cost]');
                quA.SQL.Add('           ,[Status]');
                quA.SQL.Add('           ,[Id_Producer])');
                quA.SQL.Add('     VALUES');
                quA.SQL.Add('           ('''+sBar+'''');
                quA.SQL.Add('           ,'+its(iItem));
                quA.SQL.Add('           ,1');
                quA.SQL.Add('           ,0');
                quA.SQL.Add('           ,0');
                quA.SQL.Add('           ,0');
                quA.SQL.Add('           ,0');
                quA.SQL.Add('           ,0)');
                quA.ExecSQL;

                if Integer(CardsTree.Selected.Data)=ButtonEdit1.Tag then
                begin //�������� �� ������� ��������� - ������
                  CardsView.BeginUpdate;
                  quCards.Requery();
                  quCards.Locate('ID',iItem,[]);
                  CardsView.EndUpdate;
                  CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
                end;
              end;
            end;
            quFC.Active:=False;
          end else showmessage('������ �������� ��� ���� � �������, ���������� ����������.');
        end;
      except
      end;
    end;
  end;
end;

procedure TfmCards.acEditExecute(Sender: TObject);
Var iItem,iCe:INteger;
    sBar,sBarOld:String;
begin
  //�������������
  if not CanDo('prEditCard') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    if quCards.RecordCount>0 then
    begin
      with fmAddCard do
      begin
        fmAddCard.prSetEditCard(1);
        sBarOld:=trim(quCardsBARCODE.AsString);
      end;
      fmAddCard.ShowModal;
      if fmAddCard.ModalResult=mrOk then
      begin //��������
        try
          iItem:=fmAddCard.Tag;
          sBar:=trim(fmAddCard.MaskEdit2.Text);

          if prFindBar(sBar,iItem,iCe)=False then
          begin
            quFC.Active:=False;
            quFC.SQL.Clear;
            quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFC.SQL.Add('where ca.[ID]='+its(iItem));
            quFC.Active:=True;

            if quFC.RecordCount=1 then
            begin
              with fmAddCard do
              begin
                //����������� �����
                quFC.Edit;
                quFCIDCLASSIF.AsInteger:=ButtonEdit1.Tag;
//                quFCID.AsInteger:=iItem;
                quFCNAME.AsString:=TextEdit1.Text;
                quFCFULLNAME.AsString:=TextEdit2.Text;
                quFCBARCODE.AsString:=sBar;
                quFCTTOVAR.AsInteger:=cxComboBox3.ItemIndex; //�����
                quFCEDIZM.AsInteger:= RadioGroup1.ItemIndex+1;
                quFCNDS.AsFloat:=0;
                if cxComboBox2.ItemIndex=0 then quFCNDS.AsFloat:=18;
                if cxComboBox2.ItemIndex=1 then quFCNDS.AsFloat:=10;
                quFCOKDP.AsFloat:=0;
                quFCISTATUS.AsInteger:=0;
                quFCPRSISION.AsFloat:=1;
                quFCMANCAT.AsInteger:=cxLookupComboBox3.EditValue;
                quFCCOUNTRY.AsInteger:=cxLookupComboBox1.EditValue;
                quFCBRAND.AsInteger:=cxLookupComboBox2.EditValue;
                if cxCheckBox1.Checked then quFCISTENDER.AsInteger:=1 else quFCISTENDER.AsInteger:=0;
                if cxCheckBox2.Checked then quFCISTOP.AsInteger:=1 else quFCISTOP.AsInteger:=0;
                quFCISNOV.AsInteger:=1;
                quFCCATEG.AsInteger:=cxLookupComboBox5.EditValue;
                quFCSROKGODN.AsInteger:=0;
                quFCQUANTPACK.AsFloat:=cxCalcEdit12.EditValue;
                quFCVESBUTTON.AsInteger:=cxSpinEdit1.EditValue;
                quFCVESFORMAT.AsInteger:=cxComboBox1.ItemIndex;
                quFCVES.AsFloat:=cxCalcEdit6.EditValue;
                quFCVOL.AsFloat:=cxCalcEdit2.EditValue;
                quFCWW.AsInteger:=RoundEx(cxCalcEdit4.EditValue*10);
                quFCHH.AsInteger:=RoundEx(cxCalcEdit5.EditValue*10);
                quFCLL.AsInteger:=RoundEx(cxCalcEdit3.EditValue*10);
                quFCKREP.AsFloat:=cxCalcEdit13.EditValue;
                quFCAVID.AsInteger:=cxLookupComboBox4.EditValue;
                quFCMAKER.AsInteger:=cxButtonEdit1.Tag;
                quFCDATECREATE.AsInteger:=Trunc(date);
                quFCAZKOEF.AsFloat:=cxCalcEdit7.EditValue;
                quFCAZSTRAHR.AsFloat:=cxSpinEdit2.EditValue;
                quFCAZSROKREAL.AsInteger:=cxSpinEdit4.EditValue;
                quFCAZDAYSPROC.AsInteger:=cxSpinEdit3.EditValue;
                quFCAZQUANTMAX.AsFloat:=0;
                quFCRNAC.AsFloat:=cxCalcEdit1.EditValue;
                quFCSOSTAV.AsString:=mSostav.Text;
                quFCGOST.AsString:=mGost.Text;
                quFC.Post;

                //��������
                if sBar<>sBarOld then
                begin
                  quBars.Active:=False;
                  quBars.Parameters.ParamByName('IDC').Value:=iItem;
                  quBars.Active:=True;
                  if quBars.Locate('Bar',sBar,[])=False then
                  begin
                    quBars.Append;
                    quBarsBar.AsString:=sBar;
                    quBarsGoodsId.AsInteger:=iItem;
                    quBarsQuant.AsFloat:=1;
                    quBarsBarFormat.AsInteger:=0;
                    quBarsBarStatus.AsInteger:=0;
                    quBarsCost.AsInteger:=0;
                    quBarsStatus.AsInteger:=0;
                    quBarsId_Producer.AsInteger:=0;
                    quBars.Post;
                  end;

                  if quBars.Locate('Bar',sBarOld,[]) then quBars.Delete;

                  quBars.Active:=False;
                end;

                if Integer(CardsTree.Selected.Data)=ButtonEdit1.Tag then
                begin //�������� �� ������� ��������� - ������
                  CardsView.BeginUpdate;
                  quCards.Requery();
                  quCards.Locate('ID',iItem,[]);
                  CardsView.EndUpdate;
                  CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
                end;
              end;
            end;
            quFC.Active:=False;
          end;
        except
        end;

      end;
    end;
  end;
end;

procedure TfmCards.acDelExecute(Sender: TObject);
begin
  //�������
  if not CanDo('prDelCard') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    if quCards.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� �������� "'+quCardsNAME.AsString+'" (��� '+its(quCardsID.AsInteger)+')',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          quBars.Active:=False;
          quBars.Parameters.ParamByName('IDC').Value:=quCardsID.AsInteger;
          quBars.Active:=True;
          quBars.First;
          while not quBars.Eof do quBars.Delete;
          quBars.Active:=False;

          quFC.Active:=False;
          quFC.SQL.Clear;
          quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFC.SQL.Add('where ca.[ID]='+its(quCardsID.AsInteger));
          quFC.Active:=True;
          if quFC.RecordCount=1 then quFC.Delete;
          quFC.Active:=False;

          CardsView.BeginUpdate;
          quCards.Requery();
          CardsView.EndUpdate;
          CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);

          showmessage('�������� ���������.');
        except
          showmessage('������ ��������.');
        end;
      end;
    end;
  end;
end;

procedure TfmCards.acViewExecute(Sender: TObject);
begin
  //��������
  if not CanDo('prViewCard') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    if quCards.RecordCount>0 then
    begin
      with fmAddCard do  fmAddCard.prSetEditCard(2);
      fmAddCard.ShowModal;
    end;
  end;
end;

procedure TfmCards.CardsViewStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
//  if not CanDo('prMoveMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDrGroup:=True;
end;

procedure TfmCards.CardsTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrGroup then Accept:=True;
end;

procedure TfmCards.CardsTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sNUms:String;
begin
  if bDrGroup then
  begin
    bDrGroup:=False;
    sGr:=CardsTree.DropTarget.Text;
    iGr:=Integer(CardsTree.DropTarget.data);
    iCo:=CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMCS do
        begin
          sNUms:='';
          for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=CardsView.Controller.SelectedRecords[i];

            iNum:=0;
            for j:=0 to Rec.ValueCount-1 do
            begin
              if CardsView.Columns[j].Name='CardsViewID' then begin iNum:=Rec.Values[j]; break;end;
            end;


          //��� ��� - ����������
            if iNum>0 then sNums:=sNums+','+its(iNum);
          end;
          if length(sNUms)>1 then
          begin
            delete(sNUms,1,1); //������ ������ �������

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update [dbo].[CARDS] Set ');
            quE.SQL.Add('IDCLASSIF='+its(iGr));
            quE.SQL.Add('where ID in ('+sNUms+')');
            quE.ExecSQL;

            delay(100);

            CardsView.BeginUpdate;
            quCards.Requery();
            CardsView.EndUpdate;
            CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmCards.acSetGroupExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sCode:String;
    iC:Integer;
begin
  //��������� ��������� �� ���������
//  if not CanDo('prSetSelectCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  with dmMCS do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    fmSetCardsParams:=tfmSetCardsParams.Create(Application);
    with fmSetCardsParams do
    begin
      Label1.Caption:='�������� - ' +its(CardsView.Controller.SelectedRecordCount)+' ��������.';
      quCateg.Active:=False; quCateg.Active:=True; quCateg.First; cxLookupComboBox5.EditValue:=quCardsCATEG.AsInteger;
      quCountry.Active:=False; quCountry.Active:=True; quCountry.First; quCountry.Locate('NAME','������',[loCaseInsensitive]); cxLookupComboBox2.EditValue:=quCardsCOUNTRY.AsInteger; cxLookupComboBox2.Text:=quCountryNAME.AsString;
      quCatMan.Active:=False; quCatMan.Parameters.ParamByName('ICATMAN').Value:=Commonset.CatManGroup; quCatMan.Active:=True; quCatMan.First; cxLookupComboBox4.EditValue:=quCardsMANCAT.AsInteger; cxLookupComboBox4.Text:=quCatManNAME.AsString;
      quBrands.Active:=False; quBrands.Active:=True; quBrands.First; cxLookupComboBox3.EditValue:=quCardsBRAND.AsInteger; cxLookupComboBox3.Text:=quBrandsNAMEBRAND.AsString;
      quAVid.Active:=False; quAVid.Active:=True; quAVid.First; cxLookupComboBox1.EditValue:=quCardsAVID.AsInteger;

      Label9.Caption:='�������� ���� ������� �� ������ '+fmMainMC.Label3.Caption+'('+its(fmMainMC.Label3.tag)+')';

      cxButtonEdit1.Tag:=0; cxButtonEdit1.Text:='';
    end;

    fmSetCardsParams.ShowModal;
    if fmSetCardsParams.ModalResult=mrOk then
    begin
      delay(10);
      if fmSetCardsParams.cxCheckBox1.Checked
      or fmSetCardsParams.cxCheckBox2.Checked
      or fmSetCardsParams.cxCheckBox3.Checked
      or fmSetCardsParams.cxCheckBox4.Checked
      or fmSetCardsParams.cxCheckBox5.Checked
      or fmSetCardsParams.cxCheckBox6.Checked
      or fmSetCardsParams.cxCheckBox7.Checked
      or fmSetCardsParams.cxCheckBox8.Checked
      then
      begin
        CloseTe(teBuff);
        sCode:='';

        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];
          iNum:=0;
          for j:=0 to Rec.ValueCount-1 do
            if CardsView.Columns[j].Name='CardsViewID' then begin iNum:=Rec.Values[j]; break; end;;

          if (iNum>0) then
          begin
            teBuff.Append; teBuffCode.AsInteger:=iNum; teBuff.Post;
            if fmSetCardsParams.cxCheckBox8.Checked then sCode:=sCode+','+its(iNum);
          end;
        end;

        CardsView.BeginUpdate;
        StatusBar1.Panels[0].Text:='� ��������� - '+its(teBuff.RecordCount); delay(10);
        StatusBar1.Panels[1].Text:='���������� - 0'; delay(10);

        if fmSetCardsParams.cxCheckBox1.Checked
        or fmSetCardsParams.cxCheckBox2.Checked
        or fmSetCardsParams.cxCheckBox3.Checked
        or fmSetCardsParams.cxCheckBox4.Checked
        or fmSetCardsParams.cxCheckBox5.Checked
        or fmSetCardsParams.cxCheckBox6.Checked
        or fmSetCardsParams.cxCheckBox7.Checked
        then
        begin
          iC:=0;

          teBuff.First;
          while not teBuff.Eof do
          begin
            if quCards.Locate('ID',teBuffCode.AsInteger,[]) then
            begin
              quCards.Edit;

              if fmSetCardsParams.cxCheckBox1.Checked then begin quCardsCOUNTRY.AsInteger:=fmSetCardsParams.cxLookupComboBox2.EditValue end;
              if fmSetCardsParams.cxCheckBox2.Checked then begin quCardsBRAND.AsInteger:=fmSetCardsParams.cxLookupComboBox3.EditValue end;
              if fmSetCardsParams.cxCheckBox3.Checked then begin quCardsMANCAT.AsInteger:=fmSetCardsParams.cxLookupComboBox4.EditValue end;
              if fmSetCardsParams.cxCheckBox4.Checked then quCardsVOL.AsFloat:=fmSetCardsParams.cxCalcEdit1.EditValue;
              if fmSetCardsParams.cxCheckBox5.Checked then quCardsAVID.AsInteger:=fmSetCardsParams.cxLookupComboBox1.EditValue;
              if fmSetCardsParams.cxCheckBox6.Checked then quCardsMAKER.AsInteger:=fmSetCardsParams.cxButtonEdit1.Tag;
              if fmSetCardsParams.cxCheckBox7.Checked then quCardsCATEG.AsInteger:=fmSetCardsParams.cxLookupComboBox5.EditValue;
              quCards.Post;
            end;

            teBuff.Next; inc(iC);
            if iC mod 10 = 0 then
            begin
              StatusBar1.Panels[1].Text:='���������� - '+its(iC);
              delay(10);
            end;
          end;
        end;

        if fmSetCardsParams.cxCheckBox8.Checked then
        begin
          if sCode>'' then
          begin
            delete(sCode,1,1); //������ �������

            StatusBar1.Panels[1].Text:='����� ... ���� ��������� ���.';

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('UPDATE [dbo].[CARDSPRICE] SET [PRICE] = 0 ');
            quE.SQL.Add('WHERE [GOODSID] in ('+sCode+')');
            quE.SQL.Add('and [IDSKL] = '+its(fmMainMC.Label3.tag));
            quE.ExecSQL;
          end;  
        end;

        StatusBar1.Panels[1].Text:='������� ��������.';

        if teBuff.RecordCount>0 then quCards.Requery();

        teBuff.Active:=False;

        CardsView.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmCards.SpeedItem11Click(Sender: TObject);
begin
  prNExportExel6(CardsView);
end;

procedure TfmCards.acPrintCenExecute(Sender: TObject);
Var i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
//    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMCS do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;
    try
      ViCenn.BeginUpdate;
      CloseTe(taCen);
      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.tag;
          quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=RV(quFindC4PRICE.AsFloat);
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BARCODE.AsString;
            taCenEdIzm.AsInteger:=quFindC4EDIZM.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum,fmMainMC.Label3.tag) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=quFindC4SOSTAV.AsString;
            taCenDepName.AsString:=fmMainMC.Label3.Caption;
            taCensDisc.AsString:=prSDisc(iNum,fmMainMC.Label3.tag);
            taCenScaleKey.AsInteger:=quFindC4VESBUTTON.AsInteger;
            taCensDate.AsString:=ds1(fmMainMC.cxDEdit1.Date);
//            taCenV02.AsInteger:=quFindC4V02.AsInteger;
//            taCenV12.AsInteger:=quFindC4V12.AsInteger;
//            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      if quLabs.Locate('ID',fmMainMC.cxLookupComboBox1.EditValue,[]) then
      begin
        if FileExists(CommonSet.PathReport+quLabsNameF.AsString) then
        begin
          RepCenn.LoadFromFile(CommonSet.PathReport+quLabsNameF.AsString);
          RepCenn.ReportName:='�������.';
          RepCenn.PrepareReport;
          RepCenn.ShowPreparedReport;
//        vPP:=frAll;
//        RepCenn.PrintPreparedReport('',1,False,vPP);
        end else showmessage('���� �� ������ : '+CommonSet.PathReport+quLabsNameF.AsString);



      end;
      CloseTe(taCen);
      fmMainMC.cxDEdit1.Date:=Date;
    finally
      ViCenn.EndUpdate;
    end;
  end;
end;

procedure TfmCards.acAddPrintExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
begin
  with dmMcS do
  begin
    if taCen.Active=False then CloseTe(taCen);
    ViCenn.BeginUpdate;
    if CardsView.Controller.SelectedRecordCount=0 then exit;
    for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
    begin
      Rec:=CardsView.Controller.SelectedRecords[i];

      iNum:=0;
      for j:=0 to Rec.ValueCount-1 do
      begin
        if CardsView.Columns[j].Name='CardsViewID' then
        begin
          iNum:=Rec.Values[j];
          break;
        end;
      end;

      if iNum>0 then
      begin
        quFindC4.Active:=False;
        quFindC4.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.tag;
        quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          taCen.Append;
          taCenIdCard.AsInteger:=iNum;
          taCenFullName.AsString:=quFindC4FullName.AsString;
          taCenCountry.AsString:=quFindC4NameCu.AsString;
          taCenPrice1.AsFloat:=rv(quFindC4PRICE.AsFloat);
          taCenPrice2.AsFloat:=0;
          taCenDiscount.AsFloat:=0;
          taCenBarCode.AsString:=quFindC4BARCODE.AsString;
          taCenEdIzm.AsInteger:=quFindC4EDIZM.AsInteger;
          if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum,fmMainMC.Label3.tag) else taCenPluScale.AsString:='';
          taCenReceipt.AsString:=quFindC4SOSTAV.AsString;
          taCenDepName.AsString:=fmMainMC.Label3.Caption;
          taCensDisc.AsString:=prSDisc(iNum,fmMainMC.Label3.tag);
          taCenScaleKey.AsInteger:=quFindC4VESBUTTON.AsInteger;
          taCensDate.AsString:=ds1(fmMainMC.cxDEdit1.Date);
//            taCenV02.AsInteger:=quFindC4V02.AsInteger;
//            taCenV12.AsInteger:=quFindC4V12.AsInteger;
//            taCenMaker.AsString:=quFindC4NAMEM.AsString;
          taCen.Post;
        end;

        quFindC4.Active:=False;
      end;
    end;
    ViCenn.EndUpdate;

    Label2.Caption:='� ������� - '+IntToStr(taCen.RecordCount)+' ��������.'
  end;
end;

procedure TfmCards.cxButton4Click(Sender: TObject);
begin
  if le1.Visible then
  begin
    Le1.Visible:=False;
    Le2.Visible:=True;
  end else
  begin
    Le1.Visible:=True;
    Le2.Visible:=False;
  end;
end;

procedure TfmCards.cxButton6Click(Sender: TObject);
begin
  ViCenn.BeginUpdate;
  CloseTe(dmMCS.taCen);
  ViCenn.EndUpdate;
  Label2.Caption:='';
  Showmessage('������� �����.');
end;

procedure TfmCards.acPrintQuPrintExecute(Sender: TObject);
begin
  with dmMCS do
  begin
    if taCen.Active and (taCen.RecordCount>0) then
    begin
      if quLabs.Locate('ID',fmMainMC.cxLookupComboBox1.EditValue,[]) then
      begin
        if FileExists(CommonSet.PathReport+quLabsNameF.AsString) then
        begin
          RepCenn.LoadFromFile(CommonSet.PathReport+quLabsNameF.AsString);
          RepCenn.ReportName:='�������.';
          RepCenn.PrepareReport;
          RepCenn.ShowPreparedReport;
        end;
      end;

      fmMainMC.cxDEdit1.Date:=Date;
      if MessageDlg('�������� �������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        CloseTe(taCen);
        Label2.Caption:='';
      end;
    end;
  end;
end;

procedure TfmCards.cxButton5Click(Sender: TObject);
begin
  acPrintQuPrint.Execute;
end;

procedure TfmCards.acTPrintExecute(Sender: TObject);
Var j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sBar,sCountry,sName,sTehno:String;
//Var SName,sTehno,sBar:String;
begin

  //������ �� �����
  if CardsView.Controller.SelectedRecordCount=0 then exit;
  try
   //���� ������� ���� , ������� �� ���������
    with dmMCS do
    begin
      Rec:=CardsView.Controller.SelectedRecords[0];

      iNum:=0;
      for j:=0 to Rec.ValueCount-1 do
      if CardsView.Columns[j].Name='CardsViewID' then begin  iNum:=Rec.Values[j]; break; end;

      if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
      begin
        quFindC4.Active:=False;
        quFindC4.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.tag;
        quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          sBar:=quFindC4BarCode.AsString;
          if pos('22',sBar)=1 then //��� ������� �����
          begin
            while length(sBar)<12 do sBar:=sBar+'0';
            sBar:=ToStandart(sBar);
          end;

          sName:=quFindC4FullName.AsString;
          sTehno:=its(iNum);
          sCountry:=quFindC4NameCu.AsString;

          if FileExists(CurDir + 'LabelBar.frf') then
          begin
            RepCenn.LoadFromFile(CurDir + 'LabelBar.frf');

            frVariables.Variable['SNAME']:=sName;
            frVariables.Variable['SBAR']:=sBar;
            frVariables.Variable['STEHNO']:=sTehno;
            frVariables.Variable['COUNTRY']:=sCountry;

            RepCenn.ReportName:='������ ��������.';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
          end else Showmessage('���� ������� �������� : '+CurDir + 'LabelBar.frf'+' �� ������.');
        end;
        quFindC4.Active:=False;
      end;
    end;
  finally
  end;
end;

procedure TfmCards.CardsViewCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acAddPosToDoc.Execute;
end;

procedure TfmCards.acAddPosToDocExecute(Sender: TObject);
Var iTd:INteger;
    iMax:INteger;
    kNDS:Integer;
begin
  //���������� ������� � ���������
  if dmMCS.quCards.RecordCount=0 then exit;

  if iDirect>0 then
  begin      //�������� � ������������
    iTd:=iDirect;
//    iDirect:=0;  //����� � 0

    if (iTd=2)and(fmDocsInS.Showing=True) then
    begin
      if fmDocsInS.cxTextEdit3.Tag=0 then //��������� ��������� ���� �� �����
      begin
        with fmDocsInS do
        with dmMCS do
        begin
          iMax:=1;

          fmDocsInS.ViewDoc1.BeginUpdate;
          taSpecIn.First;
          while not taSpecIn.Eof do
          begin
            if taSpecInNum.AsInteger>=iMax then  iMax:=taSpecInNum.AsInteger+1;
            taSpecIn.Next;
          end;
          fmDocsInS.ViewDoc1.EndUpdate;

          kNDS:=fmDocsInS.Label17.Tag;

          taSpecIn.Append;
          taSpecInNum.AsInteger:=iMax;
          taSpecInCodeTovar.AsInteger:=quCardsID.asinteger;
          taSpecInCodeEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
          taSpecInBarCode.AsString:=quCardsBARCODE.AsString;
          taSpecInNDSProc.AsFloat:=kNDS*quCardsNDS.AsFloat;
          taSpecInNDSSum.AsFloat:=0;
          taSpecInBestBefore.AsDateTime:=date;
          taSpecInKolMest.AsInteger:=1;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=1;
          taSpecInKolWithNecond.AsFloat:=1;
          taSpecInKol.AsFloat:=1;
          taSpecInPriceIn.AsFloat:=0;
          taSpecInSumIn.AsFloat:=0;
          taSpecInPriceIn0.AsFloat:=0;
          taSpecInSumIn0.AsFloat:=0;
          taSpecInPriceR.AsFloat:=quCardsPRICE.AsFloat;
          taSpecInSumR.AsFloat:=quCardsPRICE.AsFloat;
          taSpecInProcentNac.AsFloat:=quCardsRNAC.AsFloat;
          taSpecInName.AsString:=quCardsNAME.AsString;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInVesTara.AsFloat:=0;
          taSpecInCenaTara.AsFloat:=0;
          taSpecInSumVesTara.AsFloat:=0;
          taSpecInSumCenaTara.AsFloat:=0;
          taSpecInRealPrice.AsFloat:=quCardsPRICE.AsFloat;
          taSpecInRemn.AsFloat:=0;
          taSpecInNac1.AsFloat:=quCardsRNAC.AsFloat;
          taSpecInNac2.AsFloat:=0;
          taSpecInNac3.AsFloat:=0;
          taSpecIniCat.AsINteger:=0;
          taSpecInPriceNac.AsFloat:=quCardsRNAC.AsFloat;
          taSpecInsMaker.AsString:=prFindMaker(quCardsMAKER.AsInteger);
          taSpecInAVid.AsInteger:=quCardsAVID.AsInteger;
          taSpecInVol.AsFloat:=quCardsVOL.AsFloat;
          taSpecIn.Post;
        end;
      end;
    end;
    if (iTd=5)and(fmDocsInvS.Showing=True) then
    begin
      with fmDocsInvS do
      with dmMCS do
      begin
        if taSpecInv.Locate('CodeTovar',quCardsID.asinteger,[])=False then
        begin
          taSpecInv.Append;
          taSpecInvCodeTovar.AsInteger:=quCardsID.asinteger;
          taSpecInvBarCode.AsString:=quCardsBarCode.AsString;
          taSpecInvName.AsString:=quCardsNAME.AsString;
          taSpecInvEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
          taSpecInvNDSProc.AsFloat:=quCardsNDS.AsFloat;
          taSpecInvPrice.AsFloat:=quCardsPRICE.AsFloat;
          taSpecInvPriceSS.AsFloat:=0;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvQuantC.AsFloat:=0;
          taSpecInvqOpSv.AsFloat:=0;

          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvSumC.AsFloat:=0;

          taSpecInvSumRSS.AsFloat:=0;
          taSpecInvSumFSS.AsFloat:=0;
          taSpecInvSumRSC.AsFloat:=0;

          taSpecInvSumRTO.AsFloat:=0;
          taSpecInvSumDTO.AsFloat:=0;
          taSpecInvSumDTOSS.AsFloat:=0;

          taSpecInvBarCode.AsString:=quCardsBARCODE.AsString;

          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;
        end;
      end;
    end;

    if (iTd=3)and(fmDocSOut.Showing=True) then
    begin
      with fmDocSOut do
      with dmMCS do
      begin
        if taSpecOut.Locate('CodeTovar',quCardsID.asinteger,[])=False then
        begin
          iMax:=1;
          kNDS:=fmDocSOut.Label17.Tag;

          ViewDoc2.BeginUpdate;
          taSpecOut.First;
          while not taSpecOut.Eof do
          begin
            if taSpecOutNum.AsInteger>=iMax then  iMax:=taSpecOutNum.AsInteger+1;
            taSpecOut.Next;
          end;
          ViewDoc2.EndUpdate;

          taSpecOut.Append;
          taSpecOutNum.AsInteger:=iMax;
          taSpecOutCodeTovar.AsInteger:=quCardsID.asinteger;
          taSpecOutCodeEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
          taSpecOutBarCode.AsString:=quCardsBARCODE.AsString;
          taSpecOutNDSProc.AsFloat:=kNDS*quCardsNDS.AsFloat;
          taSpecOutNDSSumIn.AsFloat:=0;
          taSpecOutKol.AsFloat:=0;
          taSpecOutName.AsString:=quCardsNAME.AsString;
          taSpecOutTovarType.AsInteger:=0;
          taSpecOutPriceIn.AsFloat:=0;
          taSpecOutPriceIn0.AsFloat:=0;
          taSpecOutSumIn.AsFloat:=0;
          taSpecOutSumIn0.AsFloat:=0;
          taSpecOutPriceR.AsFloat:=quCardsPRICE.AsFloat;
          taSpecOutPriceR0.AsFloat:=0;
          taSpecOutSumR0.AsFloat:=0;
          taSpecOutSumR.AsFloat:=0;
          taSpecOutVolDL.AsFloat:=0;
          taSpecOutNDSSumOut.AsFloat:=0;
          taSpecOutiMaker.AsInteger:=quCardsMAKER.AsInteger;
          taSpecOutRemn.AsFloat:=0;
          taSpecOutsMaker.AsString:=prFindMaker(quCardsMAKER.AsInteger);
          taSpecOutAVid.AsInteger:=quCardsAVID.AsInteger;
          taSpecOutVol.AsFloat:=quCardsVOL.AsFloat;
          taSpecOut.Post;

//          GridDoc2.SetFocus;

          ViewDoc2CodeTovar.Options.Editing:=True;
          ViewDoc2Name.Options.Editing:=True;
        end;
      end;
    end;

    if (iTd=4)and(fmDocsAcS.Showing=True) then
    begin
      with fmDocsAcS do
      with dmMCS do
      begin
        if taSpecAc.Locate('CodeTovar',quCardsID.asinteger,[])=False then
        begin
          taSpecAc.Append;
          taSpecAcCodeTovar.AsInteger:=quCardsID.asinteger;
          taSpecAcName.AsString:=quCardsNAME.AsString;
          taSpecAcEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
          taSpecAcPriceR.AsFloat:=quCardsPRICE.AsFloat;
          taSpecAcPrice.AsFloat:=quCardsPRICE.AsFloat;
          taSpecAcPriceN.AsFloat:=quCardsPRICE.AsFloat;
          taSpecAcQuant.AsFloat:=0;
          taSpecAcSumR.AsFloat:=0;
          taSpecAcSumN.AsFloat:=0;
          taSpecAcSumD.AsFloat:=0;
          taSpecAcBarCode.AsString:=quCardsBARCODE.AsString;
          taSpecAcNum.AsInteger:=taSpecAc.RecordCount+1;
          taSpecAcQuantRemn.AsFloat:=0;
          taSpecAc.Post;
        end;
      end;
    end;
    if (iTd=6)and(fmDocsVnS.Showing=True) then
    begin
      with fmDocsVnS do
      with dmMCS do
      begin
        if taSpecVn.Locate('CodeTovar',quCardsID.asinteger,[])=False then
        begin
          taSpecVn.Append;
          taSpecVnNum.AsInteger:=taSpecVn.RecordCount+1;
          taSpecVnCodeTovar.AsInteger:=quCardsID.asinteger;
          taSpecVnName.AsString:=quCardsNAME.AsString;
          taSpecVnEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
          taSpecVnBarCode.AsString:=quCardsBARCODE.AsString;
          taSpecVnPrice.AsFloat:=quCardsPRICE.AsFloat;
          taSpecVnQuant.AsFloat:=0;
          taSpecVnPriceR.AsFloat:=quCardsPRICE.AsFloat;
          taSpecVnPriceIn.AsFloat:=0;
          taSpecVnPriceIn0.AsFloat:=0;
          taSpecVnSumR.AsFloat:=0;
          taSpecVnSumIn.AsFloat:=0;
          taSpecVnSumIn0.AsFloat:=0;
          taSpecVnSumD.AsFloat:=0;
          taSpecVnQuantRemn.AsFloat:=0;
          taSpecVn.Post;
        end;
      end;
    end;

  end;
end;

procedure TfmCards.CardsViewSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if CardsView.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
    Vi1.BeginUpdate;
    quPosts.Active:=False;
    quPosts.Parameters.ParamByName('ICODE').Value:=quCardsID.AsInteger;
    quPosts.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
    quPosts.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmCards.acCashLoadExecute(Sender: TObject);
Var sPath,sCashLdd,sMessur,sCSz,sName:String;
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    fc,fb,fupd:TextFile;
    sPos,sB,sM,sr,sDel:String;
    iShop:INteger;
    bAlco:Boolean;
    bL:Boolean;
begin
  //�������� ����
  with dmMCS do
  begin
    sr:=',';
    if CardsView.Controller.SelectedRecordCount=0 then exit;
    if fmSt.cxButton1.Enabled=False then exit;

    if CommonSet.UKM4=1 then
    begin
      try
        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        fmSt.Memo1.Lines.Add('���� �������� ����..');

        fmSt.Memo1.Lines.Add('  ����� ���� ������������ ������ ..');

        bAlco:=prTestDepAlco(fmMainMC.Label3.Tag);

        assignfile(fc,CurDir+Person.Name+'\'+'PLUCASH.DAT');
        assignfile(fb,CurDir+Person.Name+'\'+'BAR.DAT');
{
        if FileExists(CurDir+Person.Name+'\'+'PLUCASH.DAT') then Append(fc) else rewrite(fc);
        if FileExists(CurDir+Person.Name+'\'+'BAR.DAT') then Append(fb) else rewrite(fb);
 }
        //������������ ������
        rewrite(fc);
        rewrite(fb);

        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];

          iNum:=0;
          rPr:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
            if CardsView.Columns[j].Name='CardsViewPRICE' then rPr:=Rec.Values[j];
          end;

          quFCP.Active:=False;
          quFCP.SQL.Clear;
          quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
          quFCP.SQL.Add('where ca.[ID]='+its(iNum));
//          quFCP.SQL.Add('and [TTOVAR]=0');
          quFCP.Active:=True;

          if (quFCP.RecordCount=1)and(rPr>0.01) then
          begin
            bL:=True;
            if quFCPAVID.AsInteger>0 then
              if bAlco=False then bL:=False;

            if bL then
            begin
              quBars.Active:=False;
              quBars.Parameters.ParamByName('IDC').Value:=iNum;
              quBars.Active:=True;

              if quFCPEDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
              sName:=quFCPFULLNAME.AsString;

              sPos:='';

              if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';
              if (quFCPISTATUS.AsInteger<100)and(rPr>0.001) then  sDel:='1' else sDel:='0';
              sPos:=s1(quFCPID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+fAMark(quFCPAVID.AsInteger)+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+its(fmMainMC.Label3.Tag)+sr+sr+sDel+sr+sr+sr+sr;
              writeln(fc,AnsiToOemConvert(sPos));
              Flush(fc);

              quBars.First;
              while not quBars.Eof do
              begin
                if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                else sCSz:='QUANTITY';

                sB:=quBarsBar.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                writeln(fb,AnsiToOemConvert(sB));
                Flush(fb);

                quBars.Next;
              end;
              quBars.Active:=False;

              prAddCashLoadHist(iNum,fmMainMC.Label3.Tag,1,rPr,0);
            end else fmSt.Memo1.Lines.Add('  - ������� '+quFCPID.AsString+', �������� �������� ���������');
          end;

          quFCP.Active:=False;
        end;

        closefile(fc);
        closefile(fb);
        delay(100);

        //����� ������� - ����� ������� � ���������� �����
        iShop:=fShopDep(fmMainMC.Label3.Tag);

        if DirectoryExists(CommonSet.POSLoad+its(iShop)) then
        begin
          sPath:=CommonSet.POSLoad+its(iShop)+'\';

          if Fileexists(sPath+'PLUCASH.DAT') or Fileexists(sPath+'BAR.DAT') then
          begin
            fmSt.Memo1.Lines.Add('');
            fmSt.Memo1.Lines.Add('  ����� ��� �� ���������� ���������� ������...');
            fmSt.Memo1.Lines.Add('  �������� ����������!!!');
            fmSt.Memo1.Lines.Add('  ���������� �����...');
            fmSt.Memo1.Lines.Add('');
          end else
          begin
            if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
            begin
              fmSt.Memo1.Lines.Add('');
              fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
              fmSt.Memo1.Lines.Add('  �������� ����������!!!');
              fmSt.Memo1.Lines.Add('  ���������� �����...');
              fmSt.Memo1.Lines.Add('');
            end else
            begin //��� ����� ���������� �������� �����
              MoveFile(PChar(CurDir+Person.Name+'\'+'PLUCASH.DAT'),PChar(sPath+'PLUCASH.DAT'));
              MoveFile(PChar(CurDir+Person.Name+'\'+'BAR.DAT'),PChar(sPath+'BAR.DAT'));
              assignfile(fupd,sPath+CashUpd);
              rewrite(fupd);
              closefile(fupd);
            end;
          end;
          fmSt.Memo1.Lines.Add('�������� ��������� ��.');
        end else fmSt.Memo1.Lines.Add('������ �������� . ��� ����� �������� - '+CommonSet.POSLoad+its(iShop));
      except
        fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
      end;

      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

procedure TfmCards.acViewLoadExecute(Sender: TObject);
begin
  if CardsView.Controller.SelectedRecordCount=0 then exit;
  with dmMCS do
  begin
    quCLHistList.Active:=False;
    quCLHistList.SQL.Clear;
    quCLHistList.SQL.Add('SELECT [Code],[ISkl],[IDate],[Id],[DateLoad],[CType],[Price],[Disc],[Person]');
    quCLHistList.SQL.Add('FROM [dbo].[CASHLOAD]');
    quCLHistList.SQL.Add('where [Code]='+its(quCardsID.AsInteger));
    quCLHistList.SQL.Add('and [ISkl]='+its(fmMainMC.Label3.Tag));
    quCLHistList.SQL.Add('and [IDate]>='+its(Trunc(date-30)));
    quCLHistList.Active:=True;

    fmGdsLoad:=tfmGdsLoad.Create(Application);
    fmGdsLoad.ViewLoad.Tag:=quCardsID.AsInteger;
    fmGdsLoad.ShowModal;
    fmGdsLoad.Release;

    quCLHistList.Active:=False;
  end;
end;

procedure TfmCards.acAddInScaleExecute(Sender: TObject);
Var
    i,j,iNum,iSrok:INteger;
    Rec:TcxCustomGridRecord;
    sName,sBar:String;
    rPrice:Real;
begin
  //�������� � ����
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMCS do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
    begin
      Rec:=CardsView.Controller.SelectedRecords[i];

      iNum:=0;
      iSrok:=0;
      rPrice:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewBARCODE' then sBar:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewFULLNAME' then sName:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewSROKGODN' then iSrok:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewPRICE' then try rPrice:=Rec.Values[j]; except rPrice:=0; end;
      end;

      if (rPrice>0)and(iNum>0)and(sBar[1]='2')and((sBar[2]='2')or(sBar[2]='1')or(sBar[2]='3')) then
      begin
        taToScale.Append;
        taToScaleId.AsInteger:=iNum;
        taToScaleBarcode.AsString:=sBar;
        taToScaleName.AsString:=sName;
        taToScaleSrok.AsInteger:=iSrok;
        taToScalePrice.AsFloat:=rPrice;
        taToScale.Post;
      end;
    end;

    quScales.Active:=False;
    quScales.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
    quScales.Active:=True;
    quScales.First;

    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then prAddToScale(quScalesIDAI.AsInteger);

    quScales.Active:=False;
    taToScale.Active:=False;

    CardsView.Controller.ClearSelection;
  end;
  fmAddInScale.Release;
end;

procedure TfmCards.acMove1Execute(Sender: TObject);
begin
  //�������� ������ - ����� �� ��������� �� �����
  with dmMCS do
  begin
    if quCards.RecordCount>0 then
    begin
      prFormMove(fmMainMC.Label3.tag,quCardsID.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),quCardsName.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmCards.acAddGroupExecute(Sender: TObject);
Var iC,iCurDate:INteger;
begin
  //�������� ��� ������ � ��������������
  if not CanDo('prAddGrClassifToInv') then begin Showmessage('��� ����.'); exit; end;

  if (iDirect=5)and(fmDocsInvS.Showing=True) then
  begin
    if MessageDlg('�������� ��� ������ � �������������� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with fmDocsInvS do
      with dmMCS do
      begin
        try
          CardsView.BeginUpdate;
          fmDocsInvS.cxTextEdit3.Text:='�������������� �� ������ "'+CardsTree.Selected.Text+'"';
          fmDocsInvS.ViewDoc5.BeginUpdate;
          dstaSpecInv.DataSet:=nil;
          iC:=0;
          iCurDate:=Trunc(date);

          quCards.First;
          while not quCards.Eof do
          begin
            if quCardsPRICE.AsFloat>0 then
            begin
              if taSpecInv.Locate('CodeTovar',quCardsID.asinteger,[])=False then
              begin
                if (abs(prFindRemnDepD(quCardsID.asinteger,fmMainMC.Label3.tag,iCurDate))>0.001)or(iCurDate-prFindLastMove(quCardsID.asinteger,fmMainMC.Label3.tag)<=CommonSet.DaysTestMove) then
                begin
                  taSpecInv.Append;
                  taSpecInvCodeTovar.AsInteger:=quCardsID.asinteger;
                  taSpecInvBarCode.AsString:=quCardsBarCode.AsString;
                  taSpecInvName.AsString:=quCardsNAME.AsString;
                  taSpecInvEdIzm.AsInteger:=quCardsEDIZM.AsInteger;
                  taSpecInvNDSProc.AsFloat:=quCardsNDS.AsFloat;
                  taSpecInvPrice.AsFloat:=quCardsPRICE.AsFloat;
                  taSpecInvPriceSS.AsFloat:=0;
                  taSpecInvQuantR.AsFloat:=0;
                  taSpecInvQuantF.AsFloat:=0;
                  taSpecInvQuantC.AsFloat:=0;
                  taSpecInvqOpSv.AsFloat:=0;

                  taSpecInvSumR.AsFloat:=0;
                  taSpecInvSumF.AsFloat:=0;
                  taSpecInvSumC.AsFloat:=0;

                  taSpecInvSumRSS.AsFloat:=0;
                  taSpecInvSumFSS.AsFloat:=0;
                  taSpecInvSumRSC.AsFloat:=0;

                  taSpecInvSumRTO.AsFloat:=0;
                  taSpecInvSumDTO.AsFloat:=0;
                  taSpecInvSumDTOSS.AsFloat:=0;

                  taSpecInvBarCode.AsString:=quCardsBARCODE.AsString;

                  taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
                  taSpecInv.Post;
                end;
              end;
            end;
            quCards.Next;  inc(iC);

            if iC mod 100 = 0 then
            begin
              delay(10);
            end;
          end;
        finally
          dstaSpecInv.DataSet:=taSpecInv;
          fmDocsInvS.ViewDoc5.EndUpdate;
          CardsView.EndUpdate;
          fmCards.Close;
        end;
      end;
    end;
  end;
end;

procedure TfmCards.cxButton8Click(Sender: TObject);
begin
  prNExportExel6(CardsView);
end;

procedure TfmCards.acAddParamExecute(Sender: TObject);
Var bEdit:Boolean;
begin
  //�������������� ��������� �� ������������, �����, ������� ...
  if not CanDo('prViewDopPropsCard') then begin Showmessage('��� ����.'); exit; end;

  with dmMCS do
  begin
    if quCards.RecordCount>0 then
    begin
      fmAddParams.Label1.Caption:=quCardsNAME.AsString;
      fmAddParams.Label1.Tag:=quCardsID.AsInteger;

      fmAddParams.cxMemo1.Clear;
      fmAddParams.cxMemo2.Clear;
      fmAddParams.cxMemo3.Clear;
      fmAddParams.cxMemo4.Clear;
      fmAddParams.cxTextEdit1.Text:='';
      fmAddParams.cxTextEdit2.Text:='';
      fmAddParams.cxTextEdit3.Text:='';
      fmAddParams.cxTextEdit4.Text:='';
      fmAddParams.cxTextEdit5.Text:='';

      bEdit:=False;

      quSel.Active:=False;
      quSel.SQL.Clear;
      quSel.SQL.Add('SELECT [ID],[PROD],[GOST],[VES],[SOSTAV],[BGU],[EU],[GODN],[UHRAN],[NAMEIZD]');
      quSel.SQL.Add('FROM [dbo].[CARDSPROP]');
      quSel.SQL.Add('where [ID]='+its(quCardsID.AsInteger));
      quSel.Active:=True;
      if quSel.RecordCount>0 then
      begin
        fmAddParams.cxMemo1.Text:=quSel.FieldByName('PROD').AsString;
        fmAddParams.cxMemo2.Text:=quSel.FieldByName('SOSTAV').AsString;
        fmAddParams.cxMemo3.Text:=quSel.FieldByName('BGU').AsString;
        fmAddParams.cxMemo4.Text:=quSel.FieldByName('UHRAN').AsString;
        fmAddParams.cxTextEdit1.Text:=quSel.FieldByName('GOST').AsString;
        fmAddParams.cxTextEdit2.Text:=quSel.FieldByName('VES').AsString;
        fmAddParams.cxTextEdit3.Text:=quSel.FieldByName('EU').AsString;
        fmAddParams.cxTextEdit4.Text:=quSel.FieldByName('GODN').AsString;
        fmAddParams.cxTextEdit5.Text:=quSel.FieldByName('NAMEIZD').AsString;

        bEdit:=True;
      end;

      quSel.Active:=False;

      if CanDo('prEditDopPropsCard') then
      begin
        fmAddParams.cxMemo1.Properties.ReadOnly:=False;
        fmAddParams.cxMemo2.Properties.ReadOnly:=False;
        fmAddParams.cxMemo3.Properties.ReadOnly:=False;
        fmAddParams.cxMemo4.Properties.ReadOnly:=False;
        fmAddParams.cxTextEdit1.Enabled:=True;
        fmAddParams.cxTextEdit2.Enabled:=True;
        fmAddParams.cxTextEdit3.Enabled:=True;
        fmAddParams.cxTextEdit4.Enabled:=True;
        fmAddParams.cxTextEdit5.Enabled:=True;
        fmAddParams.cxButton1.Enabled:=True;
      end else
      begin
        fmAddParams.cxMemo1.Properties.ReadOnly:=True;
        fmAddParams.cxMemo2.Properties.ReadOnly:=True;
        fmAddParams.cxMemo3.Properties.ReadOnly:=True;
        fmAddParams.cxMemo4.Properties.ReadOnly:=True;
        fmAddParams.cxTextEdit1.Enabled:=False;
        fmAddParams.cxTextEdit2.Enabled:=False;
        fmAddParams.cxTextEdit3.Enabled:=False;
        fmAddParams.cxTextEdit4.Enabled:=False;
        fmAddParams.cxTextEdit5.Enabled:=False;
        fmAddParams.cxButton1.Enabled:=False;
      end;

      fmAddParams.ShowModal;
      if fmAddParams.ModalResult=mrOk then
      begin
        if bEdit then
        begin
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('');
          quE.SQL.Add('UPDATE [dbo].[CARDSPROP]');
          quE.SQL.Add('  SET [PROD] = '''+fmAddParams.cxMemo1.Text+'''');
          quE.SQL.Add('      ,[GOST] = '''+fmAddParams.cxTextEdit1.Text+'''');
          quE.SQL.Add('      ,[VES] = '''+fmAddParams.cxTextEdit2.Text+'''');
          quE.SQL.Add('      ,[SOSTAV] = '''+fmAddParams.cxMemo2.Text+'''');
          quE.SQL.Add('      ,[BGU] = '''+fmAddParams.cxMemo3.Text+'''');
          quE.SQL.Add('      ,[EU] = '''+fmAddParams.cxTextEdit3.Text+'''');
          quE.SQL.Add('      ,[GODN] = '''+fmAddParams.cxTextEdit4.Text+'''');
          quE.SQL.Add('      ,[UHRAN] = '''+fmAddParams.cxMemo4.Text+'''');
          quE.SQL.Add('      ,[NAMEIZD] = '''+fmAddParams.cxTextEdit5.Text+'''');
          quE.SQL.Add(' WHERE [ID]='+its(quCardsID.AsInteger));
          quE.ExecSQL;
        end else
        begin
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('');

          quE.SQL.Add('     INSERT INTO [dbo].[CARDSPROP] ([ID],[PROD],[GOST],[VES],[SOSTAV],[BGU],[EU],[GODN],[UHRAN],[NAMEIZD])');
          quE.SQL.Add('     VALUES');
          quE.SQL.Add('           ('+its(quCardsID.AsInteger));  //<ID, int,>
          quE.SQL.Add('           ,'''+fmAddParams.cxMemo1.Text+'''');  //<PROD, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxTextEdit1.Text+'''');  //<GOST, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxTextEdit2.Text+'''');  //<VES, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxMemo2.Text+'''');  //<SOSTAV, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxMemo3.Text+'''');  //<BGU, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxTextEdit3.Text+'''');  //<EU, varchar(max),>
          quE.SQL.Add('           ,'''+fmAddParams.cxTextEdit4.Text+'''');  //<GODN, varchar(50),>
          quE.SQL.Add('           ,'''+fmAddParams.cxMemo4.Text+'''');  //<UHRAN, varchar(max),>)
          quE.SQL.Add('           ,'''+fmAddParams.cxTextEdit5.Text+''' )');  //<NAMEIZD, varchar(max),>)

          quE.ExecSQL;
        end;
      end;
    end;
  end;
end;

procedure TfmCards.acTPrintAddExecute(Sender: TObject);
Var j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sBar,sCountry,sName,sTehno:String;

    sProd,sGost,sVes,sSostav,sBGU,sEU,sGodn,sUHran,sNameIzd:string;

//Var SName,sTehno,sBar:String;
begin

  //������ �� �����
  if CardsView.Controller.SelectedRecordCount=0 then exit;
  try
   //���� ������� ���� , ������� �� ���������
    with dmMCS do
    begin
      Rec:=CardsView.Controller.SelectedRecords[0];

      iNum:=0;
      for j:=0 to Rec.ValueCount-1 do
      if CardsView.Columns[j].Name='CardsViewID' then begin  iNum:=Rec.Values[j]; break; end;

      if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
      begin
        quFindC4.Active:=False;
        quFindC4.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.tag;
        quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          sBar:=quFindC4BarCode.AsString;
          if pos('22',sBar)=1 then //��� ������� �����
          begin
            while length(sBar)<12 do sBar:=sBar+'0';
            sBar:=ToStandart(sBar);
          end;

          sName:=quFindC4FullName.AsString;
          sTehno:=its(iNum);
          sCountry:=quFindC4NameCu.AsString;

          quSel.Active:=False;
          quSel.SQL.Clear;
          quSel.SQL.Add('SELECT [ID],[PROD],[GOST],[VES],[SOSTAV],[BGU],[EU],[GODN],[UHRAN],[NAMEIZD]');
          quSel.SQL.Add('FROM [dbo].[CARDSPROP]');
          quSel.SQL.Add('where [ID]='+its(iNum));
          quSel.Active:=True;
          if quSel.RecordCount>0 then
          begin
            sProd:=quSel.FieldByName('PROD').AsString;
            sSostav:=quSel.FieldByName('SOSTAV').AsString;
            sBGU:=quSel.FieldByName('BGU').AsString;
            sUHran:=quSel.FieldByName('UHRAN').AsString;
            sGost:=quSel.FieldByName('GOST').AsString;
            sVes:=quSel.FieldByName('VES').AsString;
            sEU:=quSel.FieldByName('EU').AsString;
            sGodn:=quSel.FieldByName('GODN').AsString;
            sNameIzd:=quSel.FieldByName('NAMEIZD').AsString;
          end;
          quSel.Active:=False;

          if FileExists(CurDir + 'LabelBig.frf') then
          begin
            RepCenn.LoadFromFile(CurDir + 'LabelBig.frf');

            frVariables.Variable['SNAME']:=sName;
            frVariables.Variable['SBAR']:=sBar;
            frVariables.Variable['STEHNO']:=sTehno;
            frVariables.Variable['COUNTRY']:=sCountry;

            frVariables.Variable['PROD']:=sProd;
            frVariables.Variable['GOST']:=sGost;
            frVariables.Variable['VES']:=sVes;
            frVariables.Variable['SOSTAV']:=sSostav;
            frVariables.Variable['BGU']:=sBGU;
            frVariables.Variable['EU']:=sEU;
            frVariables.Variable['GODN']:=sGodn;
            frVariables.Variable['UHRAN']:=sUHran;
            frVariables.Variable['NAMEIZD']:=sNameIzd;

            RepCenn.ReportName:='������ �������� (�����������).';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
          end else Showmessage('���� ������� �������� : '+CurDir + 'LabelBig.frf'+' �� ������.');
        end;
        quFindC4.Active:=False;
      end;
    end;
  finally
  end;
end;

end.
