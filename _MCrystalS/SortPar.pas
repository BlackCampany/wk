unit SortPar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, DB,
  dxmdaset, cxGraphics, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCheckComboBox, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxRadioGroup, cxSpinEdit, cxCheckBox;

type
  TfmSortF = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    taRowF: TdxMemData;
    taColVal: TdxMemData;
    taRowFId: TSmallintField;
    taRowFCapt: TStringField;
    taRowFNameF: TStringField;
    taColValId: TSmallintField;
    taColValCapt: TStringField;
    taColValNameF: TStringField;
    dstaRowF: TDataSource;
    dstaColVal: TDataSource;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    Panel2: TPanel;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    GroupBox1: TGroupBox;
    cxCheckBox1: TcxCheckBox;
    cxSpinEdit1: TcxSpinEdit;
    Label3: TLabel;
    CheckBox1: TCheckBox;
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSortF: TfmSortF;

implementation

{$R *.dfm}

procedure TfmSortF.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  cxSpinEdit1.Enabled:=not cxCheckBox1.Checked;
  CheckBox1.Enabled:=not cxCheckBox1.Checked;
end;

end.
