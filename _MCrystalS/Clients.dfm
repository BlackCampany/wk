object fmClients: TfmClients
  Left = 421
  Top = 225
  Width = 927
  Height = 657
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 911
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 764
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acAddCli
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E21
        0E210E211F7C1F7C1F7C1F7C1F7C1F7CD435FF73BE6FBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C1F7C1F7C1F7C1F7CD435BE6F7E5F5E5F5E5F5D5B5D5B7D5F
        5C5F0E211F7C1F7C1F7C1F7C1F7C1F7CD435DF737E5F5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C1F7C1F7C1F7C1F7CD435DF777F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7C1F7C1F7C1F7C1F7CD435FF7B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1F7C1F7C1F7CD4351F7C9F679F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7C1F7C6003C001E00060031F7CFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7CE000F75FF75FE0001F7C1F7C1F7C1F7CD435D435
        D435D4351F7C1F7C6003C001C001F75FF75FC001E00060031F7C1F7CD4359C2A
        D62D1F7C1F7C1F7CC001E313F75FF75FF75FF75F6003E000D435D435D435B535
        1F7C1F7C1F7C1F7CE31360036003ED3BE313C001C001E3131F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C6003ED3BED3BC0011F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CED3BE313E313ED3B1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = acAddCliExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acEditCli
      BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E210E21
        0E210E211F7C1F7C00001F7C1F7CD435FF73BE6FBE6BBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C000000001F7CD435BE6F7E5F5E5F5E5F5E5F5D5B5D5B7D5F
        5C5F0E211F7C1F7C42082129C2410000DF737E5F5E5B5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C10426277036FC56A4A297F5F7F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7CD65AA17B4277E46E00001F4B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1042817B2373C46A4A299F639F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7CD65AC07F6277036F0000FF7FFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7C1042A17B4277E46E4A294A291F7C1F7CD435D435
        D435D4351F7C1F7C1F7C1F7CD65AE07F817B2373000000001F7C1F7CD4359C2A
        D62D1F7C1F7C1F7C1F7C1F7C1F7C1042C07F0000000000000000D435D435B535
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C10423967524A524A10424A291F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD65ABD77D65AD65A104200001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C10421042104210421F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 84
      Top = 4
      Visible = True
      OnClick = acEditCliExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acViewCli
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C6B451F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CC36524556B45104200000000000000000000AD35CF35AD350000
        1F7C1F7C1F7C1F7C637EA06944594C452B5AFF7FF239573EB746D84A353A123A
        1F7C1F7C1F7C1F7C1F7C1F7C437AA2690D5255461C4BFF63FF73FF7BFF7F9846
        B02D1F7C1F7C1F7C1F7C1F7C1042FF7FD05E593AFF5BFF63FF6FFF7FFF7BFF6F
        1432AA351F7C1F7C1F7C1F7C1042FF7F13633D4FBF53DF5BFF6BFF73FF6FFF6B
        773AAD311F7C1F7C1F7C1F7C1042FF7FFF7F5B579F4F9F4FDF5BFF67FF63FF5F
        993AAE311F7C1F7C1F7C1F7C1042FF7F1042F84EDF5B9F579F539F537F4BDF57
        1532A9391F7C1F7C1F7C1F7C1042FF7FFF7FD8565D5BFF7FBF6BDF5BFF5BDB42
        F03D1F7C1F7C1F7C1F7C1F7C1042FF7F10421042B25AFA5E5B633B53773E3342
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FCF56AF56FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7F104210421042FF7F0000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F0000186300001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C10421042104210421042104200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 154
      Top = 4
      Visible = True
      OnClick = acViewCliExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acDelCli
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E210E21
        0E210E211F7C1F7C1F7C1F7C1F7CD435FF73BE6FBE6BBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C1F7C1F7C1F7CD435BF6F1F4B1F4B1F4B1F4B1F4B1F4B1F4B
        5C5F0E211F7C1F7C1F7C1F7C1F7CD435DF737E5F5E5B5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C1F7C1F7C1F7CD435DF777F5F7F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7C1F7C1F7C1F7CD435FF7B1F4B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1F7C1F7CD4351F7C9F679F639F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7C1F7C1F7CD4351F7C1F7CFF7FFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7C1F7CD4351F7C1F7C1F7C1F7C1F7C1F7CD435D435
        D435D4351F7C1F7C0048007C007CC67CC67CC67CCE7DCE7DCE7D1F7CD4359C2A
        D62D1F7C1F7C1F7C007CCE7DB57EB57EB57ECE7DC67CC67C00481F7C1F7C1F7C
        1F7C1F7C1F7C1F7CCE7DCE7DC67CC67CC67C007C007C007C00481F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 244
      Top = 4
      Visible = True
      OnClick = acDelCliExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      BtnCaption = #1064#1090#1088#1080#1093#1082#1086#1076
      Caption = #1064#1090#1088#1080#1093#1082#1086#1076
      Glyph.Data = {
        96030000424D9603000000000000360000002800000010000000120000000100
        18000000000060030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFEFA000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1064#1090#1088#1080#1093#1082#1086#1076'|'
      Spacing = 1
      Left = 394
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem7: TSpeedItem
      BtnCaption = #1046#1091#1088#1085#1072#1083
      Caption = #1046#1091#1088#1085#1072#1083
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF29200829200829
        2008FFFFFFFFFFFF0000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF292008292008EFE3CEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF292008
        2920089C694AEFE3CEC68E4AC68E4A292008FFFFFFFFFFFF0000FFFFFF101410
        084D52107184000000FFFFFFFFFFFF292008292008CEC3B5DED7C69C694AC68E
        4AEFE3CEEFE3CE292008FFFFFFFFFFFF0000FFFFFF84828410DFEF18C7DE29B2
        D6525552211808735139BDB6ADA57539B586429C694AEFE7D6C68E4AC68E4A29
        2008FFFFFFFFFFFF0000FFFFFFB5B2B508EFF710D7EF21BEDE000000BDBEBD73
        5139946D39CEC7BDE7DBCE9C694AC68E4AF7E7D6EFE3D6292008FFFFFFFFFFFF
        0000FFFFFFFFFFFF84828408E7F718CFE721B6D6525552735139BDBAB5A57539
        B586429C694AF7E7DE9C694A9C694A292008FFFFFFFFFFFF0000FFFFFFFFFFFF
        B5B2B500F7FF10DFEF18C7DE0000007351399C6D39CECBC6E7DFD69C694A9C69
        4AC67121C67121292008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF84828408EF
        F710D7EF21BEDE525552C6C3BD8C5D4294654ACE7529CE7521CE7121C6712129
        2008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFB5B2B500FBFF08E7F718CFE700
        00008C5D42C67529CE7929CE7929CE7929EFDFCEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF29200884828400F7FF000000000000000000D68231
        D67D31EFDFC6EFDFCEBD7152BD7152FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF292008B5B2B5EFEFEFB5B2B5848284000000E7D7BDEFDBC6BD7152BD71
        52FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF92
        42848284CECFCE949294848284525552BD7152FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF9242B5B2B5EFEFEFB5
        B2B5848284000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF846129D6BE94BD7152848284848284848284FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFBD7152BD7152FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Hint = #1046#1091#1088#1085#1072#1083' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
      Spacing = 1
      Left = 454
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      BtnCaption = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Caption = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6848284CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D684828484
        8284848284CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6848284848284848284848284848284CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6848284848284848284CE
        D3D6848284848284848284CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6848284848284CED3D6CED3D6CED3D6848284848284848284CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6848284CED3D6CED3D6CE
        D3D6CED3D6CED3D6848284848284848284CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D68482848482
        84CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6848284CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Spacing = 1
      Left = 234
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      BtnCaption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Glyph.Data = {
        1E060000424D1E06000000000000360000002800000018000000150000000100
        180000000000E8050000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF846100FF9E29
        8C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84
        6100FF9E29DEA67BFF9E298C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF846100DEA67BDEA67BDEA67BDEA67BFF9E298C6D18FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF846100DEA67BFFFFC6FFFFC6DEA67BDEA67BDEA67BFF9E
        298C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7B8C6D188C6D188C6D18FFFFC6FFFFC6
        DEA67B8C6D188C6D188C6D18E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C
        7D31FFFFC6FFFFC6FFF3AD9C7D31FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCB84D6B26BD6B26BD6B26BD6B26BFFFF
        FFFFFFFFFFFFFFAD8A42FFFFC6FFFFC6DEA67BFFD794FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7BFFD794DEA67B
        AD8A39FFFFFFFFFFFFFFFFFFBD964ADEA67BFFF3ADFFFFC6AD8A42FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD794E7BE73FF
        F3ADFFE79CBD964AFFFFFFFFFFFFFFFFFFFFFFFFBD964AFFE79CFFF3ADE7BE73
        FFD794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFAD8A42FFFFC6FFF3ADDEA67BBD964AFFFFFFFFFFFFFFFFFFAD8A39DEA67BFF
        D794E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFD794DEA67BFFFFC6FFFFC6AD8A42FFFFFFFFFFFFFFFFFFD6B2
        6BD6B26BD6B26BD6B26BEFCB84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF9C7D31FFF3ADFFFFC6FFFFC69C7D31FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7B8C6D188C6D188C6D18DEA67BFFFFC6FF
        FFC68C6D188C6D188C6D18E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA6
        7BDEA67BDEA67BFFFFC6FFFFC6DEA67B846100FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C6D18FF9E29DEA67BDEA67BDEA67BDEA67B846100FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA67BFF9E29846100FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E2984
        6100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF8C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF}
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Spacing = 1
      Left = 534
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      BtnCaption = #1054#1089#1090#1072#1090#1086#1082
      Caption = #1054#1089#1090#1072#1090#1086#1082
      Glyph.Data = {
        66020000424D660200000000000036000000280000000D0000000E0000000100
        18000000000030020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF846100FF9E298C
        6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF8461
        00FF9E29DEA67BFF9E298C6D18FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFF846100DEA67BDEA67BDEA67BDEA67BFF9E298C6D18FFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFF846100DEA67BFFFFC6FFFFC6DEA67BDEA67BDEA67BFF9E29
        8C6D18FFFFFFFFFFFF00FFFFFFE7BE7B8C6D188C6D188C6D18FFFFC6FFFFC6DE
        A67B8C6D188C6D188C6D18E7BE7BFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF9C7D
        31FFFFC6FFFFC6FFF3AD9C7D31FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFAD8A42FFFFC6FFFFC6DEA67BFFD794FFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFBD964ADEA67BFFF3ADFFFFC6AD8A42FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFBD964AFFE79CFFF3ADE7BE73FF
        D794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFAD8A39DEA67BFFD7
        94E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFD6B26B
        D6B26BD6B26BD6B26BEFCB84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00}
      Hint = #1054#1089#1090#1072#1090#1086#1082
      Spacing = 1
      Left = 594
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem11: TSpeedItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel|'
      Spacing = 1
      Left = 674
      Top = 4
      Visible = True
      OnClick = SpeedItem11Click
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 510
    Width = 911
    Height = 109
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 22
      Width = 32
      Height = 13
      Caption = #1055#1086#1080#1089#1082
    end
    object TextEdit1: TcxTextEdit
      Left = 64
      Top = 16
      BeepOnEnter = False
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 0
      Width = 133
    end
    object cxCheckBox1: TcxCheckBox
      Left = 208
      Top = 18
      Caption = #1053#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Properties.OnChange = cxCheckBox1PropertiesChange
      TabOrder = 1
      Width = 121
    end
    object cxButton2: TcxButton
      Left = 60
      Top = 51
      Width = 81
      Height = 25
      Caption = #1087#1086' '#1085#1072#1079#1074#1072#1085#1080#1102
      Default = True
      TabOrder = 2
      OnClick = cxButton2Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 220
      Top = 52
      Width = 69
      Height = 25
      Caption = #1087#1086' '#1082#1086#1076#1091
      TabOrder = 3
      OnClick = cxButton1Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 148
      Top = 51
      Width = 65
      Height = 25
      Caption = #1087#1086' '#1043#1051#1053
      TabOrder = 4
      OnClick = cxButton3Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      Glyph.Data = {
        96030000424D9603000000000000360000002800000010000000120000000100
        18000000000060030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFEFA000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 205
    Height = 462
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 2
    object CliTree: TTreeView
      Left = 2
      Top = 2
      Width = 201
      Height = 458
      Align = alClient
      Images = dmMCS.ImageList1
      Indent = 19
      PopupMenu = PopupMenu1
      ReadOnly = True
      RightClickSelect = True
      TabOrder = 0
      OnChange = CliTreeChange
      OnDragDrop = CliTreeDragDrop
      OnDragOver = CliTreeDragOver
    end
  end
  object RxSplitter1: TRxSplitter
    Left = 205
    Top = 48
    Width = 4
    Height = 462
    ControlFirst = Panel2
    Align = alLeft
  end
  object Panel3: TPanel
    Left = 216
    Top = 60
    Width = 605
    Height = 445
    BevelInner = bvLowered
    Caption = 'Panel3'
    TabOrder = 4
    object CliGr: TcxGrid
      Left = 2
      Top = 2
      Width = 601
      Height = 441
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object CliView: TcxGridDBTableView
        DragMode = dmAutomatic
        PopupMenu = PopupMenu2
        OnStartDrag = CliViewStartDrag
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = CliViewCellDblClick
        DataController.DataModeController.GridModeBufferCount = 500
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = dmMCS.dsquClients
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object CliViewId: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'Id'
          Width = 50
        end
        object CliViewName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'Name'
          Styles.Content = dmMCS.cxStyle5
          Width = 250
        end
        object CliViewFullName: TcxGridDBColumn
          Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'FullName'
          Width = 250
        end
        object CliViewRSch: TcxGridDBColumn
          Caption = #1056#1072#1089#1095'.'#1089#1095#1077#1090
          DataBinding.FieldName = 'RSch'
          Width = 100
        end
        object CliViewKSch: TcxGridDBColumn
          Caption = #1050#1086#1088#1088'.'#1089#1095#1077#1090
          DataBinding.FieldName = 'KSch'
          Width = 100
        end
        object CliViewINN: TcxGridDBColumn
          Caption = #1048#1053#1053
          DataBinding.FieldName = 'INN'
          Width = 80
        end
        object CliViewBank: TcxGridDBColumn
          Caption = #1041#1072#1085#1082
          DataBinding.FieldName = 'Bank'
          Width = 250
        end
        object CliViewBik: TcxGridDBColumn
          Caption = #1041#1080#1082
          DataBinding.FieldName = 'Bik'
          Width = 60
        end
        object CliViewNAMEOTP: TcxGridDBColumn
          Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          DataBinding.FieldName = 'NAMEOTP'
          Visible = False
          Width = 250
        end
        object CliViewADROTPR: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1103
          DataBinding.FieldName = 'ADROTPR'
          Visible = False
          Width = 250
        end
        object CliViewGln: TcxGridDBColumn
          Caption = 'GLN'
          DataBinding.FieldName = 'Gln'
          Width = 90
        end
        object CliViewPayNDS: TcxGridDBColumn
          Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057
          DataBinding.FieldName = 'PayNDS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1085#1077#1090
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1044#1072
              Value = 1
            end>
          Width = 50
        end
        object CliViewDayZ: TcxGridDBColumn
          Caption = #1055#1077#1088#1080#1086#1076' '#1084#1077#1078#1076#1091' '#1087#1086#1089#1090#1072#1074#1082#1072#1084#1080
          DataBinding.FieldName = 'DayZ'
        end
      end
      object CliLevel: TcxGridLevel
        GridView = CliView
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 372
    Top = 132
  end
  object PopupMenu1: TPopupMenu
    Left = 120
    Top = 144
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 8
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1075#1088#1091#1087#1087#1091
      ImageIndex = 8
      OnClick = N2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1085#1072#1079#1074#1072#1085#1080#1077
      OnClick = N6Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ('#1087#1086#1076#1075#1088#1091#1087#1087#1091')'
      ImageIndex = 9
      OnClick = N4Click
    end
  end
  object amCli: TActionManager
    Left = 596
    Top = 180
    StyleName = 'XP Style'
    object acAddCli: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddCliExecute
    end
    object acEditCli: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      OnExecute = acEditCliExecute
    end
    object acViewCli: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      OnExecute = acViewCliExecute
    end
    object acDelCli: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelCliExecute
    end
    object acFCliCode: TAction
      Caption = 'acFCliCode'
      ShortCut = 16397
      OnExecute = acFCliCodeExecute
    end
    object acUnDelete: TAction
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1074' '#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      OnExecute = acUnDeleteExecute
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 520
    Top = 220
    object N7: TMenuItem
      Action = acUnDelete
    end
  end
end
