unit uImport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit,Excel2000, OleServer, ExcelXP, ComObj,
  DB, ADODB, dxmdaset, Mask, DBCtrls;

type
  TfmImpD = class(TForm)
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    OpenDialog1: TOpenDialog;
    taClassif: TADOTable;
    taClassifID: TIntegerField;
    taClassifIDPARENT: TIntegerField;
    taClassifNAME: TStringField;
    taClassifNAC: TFloatField;
    taClassifR1: TFloatField;
    taClassifR2: TFloatField;
    taClassifR3: TFloatField;
    taClassifI1: TIntegerField;
    taClassifI2: TIntegerField;
    taClassifI3: TIntegerField;
    taClassifS1: TStringField;
    taClassifS2: TStringField;
    taClassifS3: TStringField;
    cxButton2: TcxButton;
    taCards: TADOTable;
    taCardsIDCLASSIF: TIntegerField;
    taCardsID: TIntegerField;
    taCardsNAME: TStringField;
    taCardsFULLNAME: TStringField;
    taCardsBARCODE: TStringField;
    taCardsTTOVAR: TSmallintField;
    taCardsEDIZM: TSmallintField;
    taCardsNDS: TFloatField;
    taCardsOKDP: TFloatField;
    taCardsISTATUS: TSmallintField;
    taCardsPRSISION: TFloatField;
    taCardsMANCAT: TIntegerField;
    taCardsCOUNTRY: TIntegerField;
    taCardsBRAND: TIntegerField;
    taCardsISTENDER: TSmallintField;
    taCardsISTOP: TSmallintField;
    taCardsISNOV: TSmallintField;
    taCardsCATEG: TIntegerField;
    taCardsSROKGODN: TIntegerField;
    taCardsQUANTPACK: TFloatField;
    taCardsVESBUTTON: TIntegerField;
    taCardsVESFORMAT: TSmallintField;
    taCardsVES: TFloatField;
    taCardsVOL: TFloatField;
    taCardsWW: TIntegerField;
    taCardsHH: TIntegerField;
    taCardsLL: TIntegerField;
    taCardsKREP: TFloatField;
    taCardsAVID: TIntegerField;
    taCardsMAKER: TIntegerField;
    taCardsDATECREATE: TIntegerField;
    taCardsAZKOEF: TFloatField;
    taCardsAZSTRAHR: TFloatField;
    taCardsAZSROKREAL: TIntegerField;
    taCardsAZDAYSPROC: TIntegerField;
    taCardsAZQUANTMAX: TFloatField;
    taBar: TADOTable;
    taBarBar: TStringField;
    taBarGoodsId: TIntegerField;
    taBarQuant: TFloatField;
    taBarBarFormat: TSmallintField;
    taBarBarStatus: TSmallintField;
    taBarCost: TFloatField;
    taBarStatus: TSmallintField;
    taBarId_Producer: TIntegerField;
    taCountry: TADOTable;
    taCountryIDPARENT: TIntegerField;
    taCountryID: TIntegerField;
    taCountryNAME: TStringField;
    taCountryKOD: TIntegerField;
    taCardsRNAC: TFloatField;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    taDeps: TADOTable;
    taDepsIDS: TIntegerField;
    taDepsID: TIntegerField;
    taDepsNAME: TStringField;
    taDepsFULLNAME: TStringField;
    taDepsISTATUS: TSmallintField;
    taDepsIDORG: TIntegerField;
    taDepsGLN: TStringField;
    taDepsISS: TSmallintField;
    taDepsPRIOR: TSmallintField;
    taDepsINN: TStringField;
    taDepsKPP: TStringField;
    taDepsDIR1: TStringField;
    taDepsDIR2: TStringField;
    taDepsDIR3: TStringField;
    taDepsGB1: TStringField;
    taDepsGB2: TStringField;
    taDepsGB3: TStringField;
    taDepsCITY: TStringField;
    taDepsSTREET: TStringField;
    taDepsHOUSE: TStringField;
    taDepsKORP: TStringField;
    taDepsPOSTINDEX: TStringField;
    taDepsPHONE: TStringField;
    taDepsSERLIC: TStringField;
    taDepsNUMLIC: TStringField;
    taDepsORGAN: TStringField;
    taDepsDATEB: TDateTimeField;
    taDepsDATEE: TDateTimeField;
    taDepsIP: TSmallintField;
    taDepsIPREG: TStringField;
    taPrice: TADOTable;
    taPriceGOODSID: TIntegerField;
    taPriceIDSKL: TIntegerField;
    taPricePRICE: TFloatField;
    cxButton5: TcxButton;
    taClassifCli: TADOTable;
    taClassifCliID: TIntegerField;
    taClassifCliIDPARENT: TIntegerField;
    taClassifCliNAME: TStringField;
    cxButton6: TcxButton;
    taClients: TADOTable;
    taClientsId: TIntegerField;
    taClientsINN: TStringField;
    taClientsName: TStringField;
    taClientsFullName: TStringField;
    taClientsIType: TSmallintField;
    taClientsIActive: TSmallintField;
    taClientsPostIndex: TStringField;
    taClientsGorod: TStringField;
    taClientsStreet: TStringField;
    taClientsHouse: TStringField;
    taClientsKPP: TStringField;
    taClientsNAMEOTP: TStringField;
    taClientsADROTPR: TStringField;
    taClientsRSch: TStringField;
    taClientsKSch: TStringField;
    taClientsBank: TStringField;
    taClientsBik: TStringField;
    taClientsDogNum: TStringField;
    taClientsDogDate: TDateTimeField;
    taClientsEmail: TStringField;
    taClientsPhone: TStringField;
    taClientsMol: TStringField;
    taClientsZakazT: TIntegerField;
    taClientsGln: TStringField;
    taClientsPayNDS: TSmallintField;
    taClientsITypeCli: TSmallintField;
    taClientsEDIProvider: TIntegerField;
    taClientsMinSumZak: TFloatField;
    taClientsManyStore: TSmallintField;
    taClientsEmailVoz: TStringField;
    taClientsPhoneVoz: TStringField;
    taClientsMolVoz: TStringField;
    taClientsTypeVoz: TIntegerField;
    taClientsNoNaclZ: TSmallintField;
    taClientsIDPERS: TIntegerField;
    taClientsPretOrg: TStringField;
    taClientsEmailPret: TStringField;
    taClientsZakAccess: TSmallintField;
    taClientsNoSpecif: TSmallintField;
    taClientsEmailSpecif: TStringField;
    taClientsNoRecadv: TSmallintField;
    taClientsIdParent: TIntegerField;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    taDocHIn: TADOTable;
    taDocHInIDSKL: TIntegerField;
    taDocHInIDATEDOC: TIntegerField;
    taDocHInID: TLargeintField;
    taDocHInDATEDOC: TDateTimeField;
    taDocHInNUMDOC: TStringField;
    taDocHInDATESF: TDateTimeField;
    taDocHInNUMSF: TStringField;
    taDocHInIDCLI: TIntegerField;
    taDocHInSUMIN: TFloatField;
    taDocHInSUMUCH: TFloatField;
    taDocHInSUMTAR: TFloatField;
    taDocHInIACTIVE: TSmallintField;
    taDocHInOPRIZN: TSmallintField;
    taDocSpecIn: TADOTable;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cqHd: TdxMemData;
    cqHdsNum: TStringField;
    cqHdiDep: TIntegerField;
    cqHdiDate: TIntegerField;
    taChH: TADOTable;
    taChSp: TADOTable;
    taChSpIdHead: TLargeintField;
    taChSpId: TLargeintField;
    taChSpNum: TWordField;
    taChSpCode: TIntegerField;
    taChSpBarCode: TStringField;
    taChSpQuant: TFloatField;
    taChSpPrice: TFloatField;
    taChSpSumma: TFloatField;
    taChSpProcessingTime: TFloatField;
    taChSpDProc: TFloatField;
    taChSpDSum: TFloatField;
    taChHId: TLargeintField;
    taChHId_Depart: TIntegerField;
    taChHIDate: TIntegerField;
    taChHOperation: TSmallintField;
    taChHDateOperation: TDateTimeField;
    taChHCk_Number: TIntegerField;
    taChHCassir: TIntegerField;
    taChHCash_Code: TIntegerField;
    taChHNSmena: TIntegerField;
    taChHCardNumber: TStringField;
    taChHPaymentType: TSmallintField;
    taChHsNum: TStringField;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    taDocHOut: TADOTable;
    taDocSpecOut: TADOTable;
    taDocHOutIDSKL: TIntegerField;
    taDocHOutIDATEDOC: TIntegerField;
    taDocHOutID: TLargeintField;
    taDocHOutDATEDOC: TDateTimeField;
    taDocHOutNUMDOC: TStringField;
    taDocHOutDATESF: TDateTimeField;
    taDocHOutNUMSF: TStringField;
    taDocHOutIDCLI: TIntegerField;
    taDocHOutSUMIN: TFloatField;
    taDocHOutSUMUCH: TFloatField;
    taDocHOutSUMTAR: TFloatField;
    taDocHOutIACTIVE: TSmallintField;
    taDocHOutOPRIZN: TSmallintField;
    taDocHOutSNUMZ: TStringField;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    taDocHInv: TADOTable;
    taDocSpecInv: TADOTable;
    taDocHInvIDSKL: TIntegerField;
    taDocHInvIDATEDOC: TIntegerField;
    taDocHInvDATEDOC: TDateTimeField;
    taDocHInvNUMDOC: TStringField;
    taDocHInvSUMINR: TFloatField;
    taDocHInvSUMINF: TFloatField;
    taDocHInvSUMINRTO: TFloatField;
    taDocHInvRSUMR: TFloatField;
    taDocHInvRSUMF: TFloatField;
    taDocHInvRSUMTO: TFloatField;
    taDocHInvSUMTARR: TFloatField;
    taDocHInvSUMTARF: TFloatField;
    taDocHInvIACTIVE: TSmallintField;
    taDocHInvIDETAIL: TSmallintField;
    quPricePos: TADOQuery;
    quPricePosGOODSID: TIntegerField;
    quPricePosIDSKL: TIntegerField;
    quPricePosPRICE: TFloatField;
    msConnection: TADOConnection;
    quGetId: TADOQuery;
    quGetIdretnum: TIntegerField;
    taDocSpecInIDHEAD: TLargeintField;
    taDocSpecInID: TLargeintField;
    taDocSpecInNUM: TIntegerField;
    taDocSpecInIDCARD: TIntegerField;
    taDocSpecInSBAR: TStringField;
    taDocSpecInIDM: TIntegerField;
    taDocSpecInQUANT: TFloatField;
    taDocSpecInPRICEIN: TFloatField;
    taDocSpecInSUMIN: TFloatField;
    taDocSpecInPRICEIN0: TFloatField;
    taDocSpecInSUMIN0: TFloatField;
    taDocSpecInPRICEUCH: TFloatField;
    taDocSpecInSUMUCH: TFloatField;
    taDocSpecInNDSPROC: TFloatField;
    taDocSpecInSUMNDS: TFloatField;
    taDocSpecInGTD: TStringField;
    taDocSpecInSNUMHEAD: TStringField;
    taDocSpecInvNUM: TIntegerField;
    taDocSpecInvIDCARD: TIntegerField;
    taDocSpecInvSBAR: TStringField;
    taDocSpecInvQUANTR: TFloatField;
    taDocSpecInvQUANTF: TFloatField;
    taDocSpecInvPRICER: TFloatField;
    taDocSpecInvSUMR: TFloatField;
    taDocSpecInvSUMIN0: TFloatField;
    taDocSpecInvSUMIN: TFloatField;
    taDocSpecInvSUMRF: TFloatField;
    taDocSpecInvSUMIN0F: TFloatField;
    taDocSpecInvSUMINF: TFloatField;
    taDocSpecInvQUANTC: TFloatField;
    taDocSpecInvSUMC: TFloatField;
    taDocSpecInvSUMRSC: TFloatField;
    taDocSpecInvSumRTO: TFloatField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    taDocSpecOutIDHEAD: TLargeintField;
    taDocSpecOutID: TLargeintField;
    taDocSpecOutNUM: TIntegerField;
    taDocSpecOutIDCARD: TIntegerField;
    taDocSpecOutSBAR: TStringField;
    taDocSpecOutIDM: TIntegerField;
    taDocSpecOutQUANT: TFloatField;
    taDocSpecOutPRICEIN: TFloatField;
    taDocSpecOutSUMIN: TFloatField;
    taDocSpecOutPRICEIN0: TFloatField;
    taDocSpecOutSUMIN0: TFloatField;
    taDocSpecOutPRICER: TFloatField;
    taDocSpecOutSUMR: TFloatField;
    taDocSpecOutPRICER0: TFloatField;
    taDocSpecOutSUMR0: TFloatField;
    taDocSpecOutNDSPROC: TFloatField;
    taDocSpecOutSUMNDSIN: TFloatField;
    taDocSpecOutSUMNDSOUT: TFloatField;
    taDocSpecOutGTD: TStringField;
    taDocSpecOutSNUMHEAD: TStringField;
    quPrice: TADOQuery;
    quPriceGOODSID: TIntegerField;
    quPriceIDSKL: TIntegerField;
    quPricePRICE: TFloatField;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    quDocSpecInv: TADOQuery;
    quDocSpecInvNUM: TIntegerField;
    quDocSpecInvIDCARD: TIntegerField;
    quDocSpecInvSBAR: TStringField;
    quDocSpecInvQUANTR: TFloatField;
    quDocSpecInvQUANTF: TFloatField;
    quDocSpecInvPRICER: TFloatField;
    quDocSpecInvSUMR: TFloatField;
    quDocSpecInvSUMIN0: TFloatField;
    quDocSpecInvSUMIN: TFloatField;
    quDocSpecInvSUMRF: TFloatField;
    quDocSpecInvSUMIN0F: TFloatField;
    quDocSpecInvSUMINF: TFloatField;
    quDocSpecInvQUANTC: TFloatField;
    quDocSpecInvSUMC: TFloatField;
    quDocSpecInvSUMRSC: TFloatField;
    quDocSpecInvSumRTO: TFloatField;
    taDocSpecInvIDHEAD: TLargeintField;
    taDocSpecInvID: TLargeintField;
    taDocHInvID: TLargeintField;
    taDocHInvIN_TO: TSmallintField;
    quDocSpecInvIDHEAD: TLargeintField;
    quDocSpecInvID: TLargeintField;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fGetID(iT:INteger):Integer;

var
  fmImpD: TfmImpD;

implementation

uses Un1, udmTmp;

{$R *.dfm}

function fGetID(iT:INteger):Integer;
begin
  with fmImpD do
  begin
    quGetId.Active:=False;
    quGetId.Parameters.ParamByName('ITN').Value:=iT;
    quGetId.Active:=True;
    Result:=quGetIdretnum.AsInteger;
    quGetId.Active:=False;
  end;
end;


procedure TfmImpD.cxButton1Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    iCode,iParent:INteger;
    sName:String;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taClassif.Active:=True;
        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;
        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          iParent:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          sName:=Trim(String(ISheet.Cells.Item[i,1].Value));
          if iCode>0 then
          begin
            if taClassif.Locate('ID',iCode,[])=False then
            begin
              taClassif.Append;
              taClassifID.AsInteger:=iCode;
              taClassifIDPARENT.AsInteger:=iParent;
              taClassifNAME.AsString:=sName;
              taClassifNAC.AsFloat:=0;
              taClassifR1.AsFloat:=0;
              taClassifR2.AsFloat:=0;
              taClassifR3.AsFloat:=0;
              taClassifI1.AsInteger:=0;
              taClassifI2.AsInteger:=0;
              taClassifI3.AsInteger:=0;
              taClassifS1.AsString:='';
              taClassifS2.AsString:='';
              taClassifS3.AsString:='';
              taClassif.Post;
            end;
          end;
          inc(i);
        end;
        taClassif.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton2Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i,iAdd:INteger;
    iCode,iParent:INteger;
    sName1,sName2,sCountry,sBar:String;
    iM,iCountry:INteger;
    rMin,rNac,rSrokR,rSrokHr:Real;
    fTest:TextFile;
begin
  //������
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    assignfile(fTest,CurDir+'test.csv');
    rewrite(fTest);

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taCards.Active:=True;
        taBar.Active:=True;
        taCountry.Active:=True;
        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;  iAdd:=0;
        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          if iCode>0 then
          begin
            if taCards.Locate('ID',iCode,[])=False then
            begin
              iParent:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
              sName1:=Trim(String(ISheet.Cells.Item[i,1].Value));
              iM:=StrToINtDef(String(ISheet.Cells.Item[i, 7].Value),0);  //1-�����  2-��
              rMin:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);
              sName2:=Trim(String(ISheet.Cells.Item[i,11].Value));
              rNac:=StrToFloatDef(String(ISheet.Cells.Item[i,16].Value),0);
              rSrokR:=StrToFloatDef(String(ISheet.Cells.Item[i,15].Value),0);
              rSrokHr:=StrToFloatDef(String(ISheet.Cells.Item[i,17].Value),0);
              sCountry:=Trim(String(ISheet.Cells.Item[i,18].Value));
              sBar:=Trim(String(ISheet.Cells.Item[i,20].Value));
              if sBar='' then sBar:=its(iCode);

              iCountry:=0;
              if sCountry>'' then
              begin
                if taCountry.Locate('NAME',sCountry,[loCaseInsensitive]) then iCountry:=taCountryID.AsInteger
                else
                begin //������ ��� - ���� ���������
                  iCountry:=fGetID(6);
                  taCountry.Append;
                  taCountryIDPARENT.AsInteger:=0;
                  taCountryID.AsInteger:=iCountry;
                  taCountryNAME.AsString:=sCountry;
                  taCountryKOD.AsInteger:=0;
                  taCountry.Post;
                end;
              end;

              if taBar.Locate('Bar',sBar,[])=False then
              begin
                if iM=1 then
                begin
                  sBar:=its(iCode);
                  while length(sBar)<5 do sBar:='0'+sBar;
                  sBar:='22'+sBar;
                end;

                taCards.Append;
                taCardsIDCLASSIF.AsInteger:=iParent;
                taCardsID.AsInteger:=iCode;
                taCardsNAME.AsString:=sName1;
                taCardsFULLNAME.AsString:=sName2;
                taCardsBARCODE.AsString:=Copy(sBar,1,13);
                taCardsTTOVAR.AsInteger:=0;
                taCardsEDIZM.AsInteger:=iM+1;
                taCardsNDS.AsFloat:=0;
                taCardsOKDP.AsFloat:=0;
                taCardsISTATUS.AsInteger:=1;
                if iM=0 then taCardsPRSISION.AsFloat:=1 else taCardsPRSISION.AsFloat:=0.001;
                taCardsMANCAT.AsInteger:=0;
                taCardsCOUNTRY.AsInteger:=iCountry;
                taCardsBRAND.AsInteger:=0;
                taCardsISTENDER.AsInteger:=0;
                taCardsISTOP.AsInteger:=0;
                taCardsISNOV.AsInteger:=0;
                taCardsCATEG.AsInteger:=0;
                taCardsSROKGODN.AsInteger:=RoundEx(rSrokHr);
                taCardsQUANTPACK.AsFloat:=0;
                taCardsVESBUTTON.AsInteger:=0;
                taCardsVESFORMAT.AsInteger:=0;
                taCardsVES.AsFloat:=0;
                taCardsVOL.AsFloat:=0;
                taCardsWW.AsInteger:=0;
                taCardsHH.AsInteger:=0;
                taCardsLL.AsInteger:=0;
                taCardsKREP.AsFloat:=0;
                taCardsAVID.AsInteger:=0;
                taCardsMAKER.AsInteger:=0;
                taCardsDATECREATE.AsInteger:=0;
                taCardsAZKOEF.AsFloat:=0;
                taCardsAZSTRAHR.AsFloat:=rMin;
                taCardsAZSROKREAL.AsInteger:=RoundEx(rSrokR);
                taCardsAZDAYSPROC.AsInteger:=0;
                taCardsAZQUANTMAX.AsFloat:=0;
                taCardsRNAC.AsFloat:=rNac;
                taCards.Post;

                taBar.Append;
                taBarBar.AsString:=sBar;
                taBarGoodsId.AsInteger:=iCode;
                taBarQuant.AsFloat:=1;
                taBarBarFormat.AsInteger:=0;
                taBarBarStatus.AsInteger:=0;
                taBarCost.AsFloat:=0;
                taBarStatus.AsInteger:=0;
                taBarId_Producer.AsInteger:=0;
                taBar.Post;

                Memo1.Lines.Add(its(iCode)+';'+sName1+';'+sName2);

                inc(iAdd);
              end;
            end else
            begin //����� ��� ��� ���� � ����
              sName1:=Trim(String(ISheet.Cells.Item[i,1].Value));
              if Copy(trim(taCardsNAME.AsString),1,10)<>Copy(trim(sName1),1,10) then //�������� ������ - ������� � ���
              begin
                writeln(fTest,taCardsID.AsString+';'+trim(taCardsNAME.AsString)+';'+trim(sName1));
                delay(10);
              end;
            end;
          end;
          inc(i);

          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)+'  '+its(iAdd)); delay(10);
          end;
        end;
        taCards.Active:=False;
        taBar.Active:=False;
        taCountry.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;
    CloseFile(fTest);
    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton3Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i,iAdd:INteger;
    iCode:INteger;
    sBar:String;
begin
  //���������
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taCards.Active:=True;
        taBar.Active:=True;
        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;      iAdd:=0;
        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          if iCode>0 then
          begin
            if taCards.Locate('ID',iCode,[]) then
            begin
              sBar:=Trim(String(ISheet.Cells.Item[i,1].Value));
              if (length(sBar)>7) and (length(sBar)<=13) then
              begin
                if taBar.Locate('Bar',sBar,[])=False then
                begin
                  taBar.Append;
                  taBarBar.AsString:=sBar;
                  taBarGoodsId.AsInteger:=iCode;
                  taBarQuant.AsFloat:=1;
                  taBarBarFormat.AsInteger:=0;
                  taBarBarStatus.AsInteger:=0;
                  taBarCost.AsFloat:=0;
                  taBarStatus.AsInteger:=0;
                  taBarId_Producer.AsInteger:=0;
                  taBar.Post;

                  inc(iAdd);
                end;
              end;
            end;
          end;
          inc(i);

          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)+'  '+its(iAdd)); delay(10);
          end;
        end;
        taCards.Active:=False;
        taBar.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton4Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i,iAdd:INteger;
    iCode,iDep:INteger;
    sDep:String;
    rPr:Real;
begin
  //����
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDeps.Active:=True;
        quPrice.Active:=False;
        quPrice.Parameters.ParamByName('ISKL').Value:=1;
        quPrice.Active:=True;


        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2; iAdd:=0;
//        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        While i<44000 do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          if iCode>0 then
          begin
            sDep:=Trim(String(ISheet.Cells.Item[i,3].Value));
            rPr:=StrToFloatDef(String(ISheet.Cells.Item[i,4].Value),0);
            iDep:=0;
            if sDep>'' then

            if pos('���� 4',sDep)>0 then
            begin
              iDep:=4;
            end;

            if iDep>0 then
            begin
              try
                if quPrice.Locate('GOODSID',iCode,[]) then
                begin
//                  quPrice.Edit;
//                  quPriceGOODSID.AsInteger:=iCode;
//                  quPriceIDSKL.AsInteger:=iDep;
//                  quPricePRICE.AsFloat:=rPr;
//                  quPrice.Post;
                end else
                begin
                  quPrice.Append;
                  quPriceGOODSID.AsInteger:=iCode;
                  quPriceIDSKL.AsInteger:=iDep;
                  quPricePRICE.AsFloat:=rPr;
                  quPrice.Post;

                  inc(iAdd)
                end;

              except
                quPrice.Cancel;
              end;
            end;
          end;

          inc(i);
          
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)+'  '+its(iAdd)); delay(10);
          end;
        end;
        taDeps.Active:=False;
//        taPrice.Active:=False;
        quPrice.Active:=False;

      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton5Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    iCode,iParent:INteger;
    sName:String;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taClassifCli.Active:=True;
        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;
        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          iParent:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          sName:=Trim(String(ISheet.Cells.Item[i,1].Value));
          if iCode>0 then
          begin
            if taClassifCli.Locate('ID',iCode,[])=False then
            begin
              taClassifCli.Append;
              taClassifCliID.AsInteger:=iCode;
              taClassifCliIDPARENT.AsInteger:=iParent;
              taClassifCliNAME.AsString:=sName;
              taClassifCli.Post;
            end;
          end;
          inc(i);
        end;
        taClassifCli.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton6Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    iCode,iParent:INteger;
    sName1,sName2,sTel,sINN,sKpp,sBank,sAdrBank,sKorBand,sRSch,sCorSch:String;
    rRsch,rCorSch:Real;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taClients.Active:=True;
        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;
        While String(ISheet.Cells.Item[i, 2].Value)<>'' do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          iParent:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          sName1:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sName2:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sTel:=Trim(String(ISheet.Cells.Item[i,6].Value));
          sINN:=Trim(String(ISheet.Cells.Item[i,7].Value));
          sKpp:=Trim(String(ISheet.Cells.Item[i,8].Value));
          sBank:=Trim(String(ISheet.Cells.Item[i,9].Value));
          sAdrBank:=Trim(String(ISheet.Cells.Item[i,10].Value));
          sKorBand:=Trim(String(ISheet.Cells.Item[i,11].Value));
          rRsch:=StrToFloatDef(String(ISheet.Cells.Item[i,12].Value),0);
          rCorSch:=StrToFloatDef(String(ISheet.Cells.Item[i,13].Value),0);

//          sRSch:=Trim(String(ISheet.Cells.Item[i,12].Value));
//          sCorSch:=Trim(String(ISheet.Cells.Item[i,13].Value));

          str(rRsch:20:0,sRSch);
          str(rCorSch:20:0,sCorSch);

          if iCode>0 then
          begin
            if taClients.Locate('ID',iCode,[])=False then
            begin
              taClients.Append;
              taClientsId.AsInteger:=iCode;
              taClientsIdParent.AsInteger:=iParent;
              taClientsName.AsString:=sName1;
              taClientsINN.AsString:=sINN;
              taClientsFullName.AsString:=sName2;
              taClientsIType.AsInteger:=0;
              taClientsIActive.AsInteger:=1;
              taClientsPostIndex.AsString:='';
              taClientsGorod.AsString:='';
              taClientsStreet.AsString:='';
              taClientsHouse.AsString:='';
              taClientsKPP.AsString:=sKpp;
              taClientsNAMEOTP.AsString:='';
              taClientsADROTPR.AsString:='';
              taClientsRSch.AsString:=sRSch;
              taClientsKSch.AsString:=sCorSch;
              taClientsBank.AsString:=sBank;
              taClientsBik.AsString:='';
              taClientsDogNum.AsString:='';
              taClientsEmail.AsString:='';
              taClientsPhone.AsString:=sTel;
              taClientsMol.AsString:='';
              taClientsZakazT.AsInteger:=0;
              taClientsGln.AsString:='';
              taClientsPayNDS.AsInteger:=0;
              taClientsITypeCli.AsInteger:=0;
              taClientsEDIProvider.AsInteger:=0;
              taClientsMinSumZak.AsFloat:=0;
              taClientsManyStore.AsInteger:=0;
              taClientsEmailVoz.AsString:='';
              taClientsPhoneVoz.AsString:='';
              taClientsMolVoz.AsString:='';
              taClientsTypeVoz.AsInteger:=0;
              taClientsNoNaclZ.AsInteger:=0;
              taClientsIDPERS.AsInteger:=0;
              taClientsPretOrg.AsString:='';
              taClientsEmailPret.AsString:='';
              taClientsZakAccess.AsInteger:=0;
              taClientsNoSpecif.AsInteger:=0;
              taClientsEmailSpecif.AsString:='';
              taClientsNoRecadv.AsInteger:=0;
              taClients.Post;
            end else
            begin
              Memo1.Lines.Add(its(iCode)); delay(10);
            end;
          end;
          inc(i);

          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;

        end;
        taClients.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton7Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sMh,sDate,sType:String;
    iCli,iMH,IdH:INteger;
    DocDate:TDateTime;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHIn.Active:=True;
        taDeps.Active:=True;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sType:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,4].Value));
          sMh:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sDate:=Trim(String(ISheet.Cells.Item[i,3].Value));
          iCli:=StrToINtDef(String(ISheet.Cells.Item[i, 8].Value),0);

          DocDate:=StrToDateDef(sDate,date);

          if pos('�������������',sType)>0 then
          begin
            if taDeps.Locate('NAME',sMh,[]) then
            begin
              iMH:=taDepsID.AsInteger;
              if iMH=4 then
              begin
                IdH:=fGetID(9);

                taDocHIn.Append;
                taDocHInIDSKL.AsInteger:=iMH;
                taDocHInIDATEDOC.AsInteger:=Trunc(DocDate);
                taDocHInID.AsInteger:=IdH;
                taDocHInDATEDOC.AsDateTime:=DocDate;
                taDocHInNUMDOC.AsString:=sNumDoc;
                taDocHInDATESF.AsDateTime:=DocDate;
                taDocHInNUMSF.AsString:='';
                taDocHInIDCLI.AsInteger:=iCli;
                taDocHInSUMIN.AsFloat:=0;
                taDocHInSUMUCH.AsFloat:=0;
                taDocHInSUMTAR.AsFloat:=0;
                taDocHInIACTIVE.AsInteger:=3;
                taDocHInOPRIZN.AsInteger:=0;
                taDocHIn.Post;
              end;
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDocHIn.Active:=False;
        taDeps.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton8Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sNum:String;
    iCode,IdH,iNum,IdHCur:INteger;
    rQuant,rPriceIn,rSumIn,rPriceOut,rSumOut:Real;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHIn.Active:=True;
        taDocSpecIn.Active:=True;

        iNum:=1; IdHCur:=0;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,1].Value));
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          rQuant:=StrToFloatDef(String(ISheet.Cells.Item[i,6].Value),0);
          rPriceIn:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
          rSumIn:=StrToFloatDef(String(ISheet.Cells.Item[i,8].Value),0);
          rPriceOut:=StrToFloatDef(String(ISheet.Cells.Item[i,9].Value),0);
          rSumOut:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);

          if pos('�������������',sNumDoc)>0 then
          begin
            sNum:=sNumDoc;
            delete(sNum,1,13);
            if (taDocHIn.Locate('NUMDOC',sNum,[])=True)and(iCode>0) then
            begin
              IdH:=taDocHInID.AsInteger;

              if IdHCur<>IdH then
              begin
                IdHCur:=IdH;
                iNum:=1;
              end;

              taDocSpecIn.Append;
              taDocSpecInIDHEAD.AsInteger:=IdH;
              taDocSpecInNUM.AsInteger:=iNum;
              taDocSpecInIDCARD.AsInteger:=iCode;
              taDocSpecInSBAR.AsString:='';
              taDocSpecInIDM.AsInteger:=1;
              taDocSpecInQUANT.AsFloat:=rQuant;
              taDocSpecInPRICEIN.AsFloat:=rPriceIn;
              taDocSpecInSUMIN.AsFloat:=rSumIn;
              taDocSpecInPRICEIN0.AsFloat:=rPriceIn;
              taDocSpecInSUMIN0.AsFloat:=rSumIn;
              taDocSpecInPRICEUCH.AsFloat:=rPriceOut;
              taDocSpecInSUMUCH.AsFloat:=rSumOut;
              taDocSpecInNDSPROC.AsFloat:=0;
              taDocSpecInSUMNDS.AsFloat:=0;
              taDocSpecInGTD.AsString:='';
              taDocSpecInSNUMHEAD.AsString:=sNum;
              taDocSpecIn.Post;

              inc(iNum);
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDocHIn.Active:=False;
        taDocSpecIn.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton9Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i,iAdd:INteger;
    sNumDoc,sMh,sDate,sType,sHd:String;
    iMH,iZ:INteger;
    DocDate:TDateTime;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      closete(cqHd);
      if msConnection.Connected then
      begin
        taDeps.Active:=True;
        taChH.Active:=True;

        i:=1;  iAdd:=0;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sType:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,4].Value));
          sMh:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sDate:=Trim(String(ISheet.Cells.Item[i,3].Value));

          DocDate:=StrToDateDef(sDate,date);
{
          if Trunc(DocDate)=41864 then
          begin
          DocDate:=41863;
}
          if pos('�����������',sType)>0 then
          begin
            if taDeps.Locate('NAME',sMh,[]) then
            begin
              iMH:=taDepsID.AsInteger;
              if iMH=4 then
              begin
                sHd:=sNumDoc;  //1\���-0003287
                delete(sHd,1,6);
                iZ:=StrToIntDef(sHd,0);

                cqHd.Append;
                cqHdsNum.AsString:=trim(sNumDoc);
                cqHdiDep.AsInteger:=iMH;
                cqHdiDate.AsInteger:=Trunc(DocDate);
                cqHd.Post;

                taChH.Append;
                taChHId.AsInteger:=fGetID(10);
                taChHId_Depart.AsInteger:=iMH;
                taChHIDate.AsInteger:=Trunc(DocDate);
                taChHOperation.AsInteger:=1;
                taChHDateOperation.AsDateTime:=Trunc(DocDate)+0.5;
                taChHCk_Number.AsInteger:=1;
                taChHCassir.AsInteger:=1;
                taChHCash_Code.AsInteger:=1;
                taChHNSmena.AsInteger:=iZ;
                taChHCardNumber.AsString:='';
                taChHPaymentType.AsInteger:=1;
                taChHsNum.AsString:=sNumDoc;
                taChH.Post;

                inc(iAdd);
              end;
            end;
          end;
//          end;
          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add(' ����� '+its(i)+'  ��������� '+its(iAdd)); delay(10);
          end;
        end;
        Memo1.Lines.Add(' ����� '+its(i)+'  ��������� '+its(iAdd)); delay(10);
        taDeps.Active:=False;
        taChH.Active:=False;
        cqHd.Close;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton10Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i,iNon,iAdd:INteger;
    sNumDoc,sNum:String;
    iCode:INteger;
    rQuant,rSumD,rPriceOut,rSumOut:Real;

begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taChH.Active:=True;
        taChSp.Active:=True;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;  iAdd:=0; iNon:=0;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,1].Value));
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          rQuant:=StrToFloatDef(String(ISheet.Cells.Item[i,6].Value),0);
//          rPriceIn:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
          rSumD:=StrToFloatDef(String(ISheet.Cells.Item[i,8].Value),0);  //������
          rPriceOut:=StrToFloatDef(String(ISheet.Cells.Item[i,9].Value),0);
          rSumOut:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);

          if pos('�����������',sNumDoc)>0 then
          begin
            sNum:=sNumDoc;
            delete(sNum,1,11);
            if (taChH.Locate('sNum',sNum,[])=True)and(iCode>0) then
            begin

//              if taChHIDate.AsInteger=41861 then
//              begin

              taChSp.Append;
              taChSpIdHead.AsInteger:=taChHId.AsInteger;
              taChSpNum.AsInteger:=1;
              taChSpCode.AsInteger:=iCode;
              taChSpBarCode.AsString:='';
              taChSpQuant.AsFloat:=rQuant;
              taChSpPrice.AsFloat:=rPriceOut;
              taChSpSumma.AsFloat:=rSumOut;
              taChSpProcessingTime.AsFloat:=0;
              taChSpDProc.AsFloat:=0;
              taChSpDSum.AsFloat:=rSumD;
              taChSp.Post;
              
               {
              //��������� ���� �������
              quPricePos.Active:=False;
              quPricePos.Parameters.ParamByName('ICODE').Value:=iCode;
              quPricePos.Parameters.ParamByName('ISKL').Value:=taChHId_Depart.AsInteger;
              quPricePos.Active:=True;
              if quPricePos.RecordCount>0 then
              begin
                quPricePos.First;
                quPricePos.Edit;
                quPricePosPRICE.AsFloat:=rPriceOut;
                quPricePos.Post;
              end else
              begin
                quPricePos.Append;
                quPricePosGOODSID.AsInteger:=iCode;
                quPricePosIDSKL.AsInteger:=taChHId_Depart.AsInteger;
                quPricePosPRICE.AsFloat:=rPriceOut;
                quPricePos.Post;
              end;
              quPricePos.Active:=False;
                 }
              inc(iAdd);
//              end;
            end else inc(iNon);
          end;

          inc(i);
          if i mod 100=0 then
          begin
            Memo1.Lines.Add(' ����� '+its(i)+'  ��������� '+its(iAdd)+' ��� '+its(iNon)); delay(10);
          end;
        end;
        taChH.Active:=True;
        taChSp.Active:=True;
        Memo1.Lines.Add(' ����� '+its(i)+'  ��������� '+its(iAdd)+' ��� '+its(iNon)); delay(10);
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton11Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sMh,sDate,sType:String;
    iCli,iMH,IdH:INteger;
    DocDate:TDateTime;
    iTd:Integer;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHOut.Active:=True;
        taDeps.Active:=True;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sType:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,4].Value));
          sMh:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sDate:=Trim(String(ISheet.Cells.Item[i,3].Value));
          iCli:=StrToINtDef(String(ISheet.Cells.Item[i, 8].Value),0);

          DocDate:=StrToDateDef(sDate,date);

          if (pos('�����������������',sType)>0)or(pos('�������������',sType)>0) then
          begin
            if taDeps.Locate('NAME',sMh,[]) then
            begin
              iMH:=taDepsID.AsInteger;
              IdH:=fGetID(11);

              if pos('�����������������',sType)>0 then iTd:=0 else iTd:=1; 

              taDocHOut.Append;
              taDocHOutIDSKL.AsInteger:=iMH;
              taDocHOutIDATEDOC.AsInteger:=Trunc(DocDate);
              taDocHOutID.AsInteger:=IdH;
              taDocHOutDATEDOC.AsDateTime:=DocDate;
              taDocHOutNUMDOC.AsString:=sNumDoc;
              taDocHOutDATESF.AsDateTime:=DocDate;
              taDocHOutNUMSF.AsString:='';
              taDocHOutIDCLI.AsInteger:=iCli;
              taDocHOutSUMIN.AsFloat:=0;
              taDocHOutSUMUCH.AsFloat:=0;
              taDocHOutSUMTAR.AsFloat:=0;
              taDocHOutIACTIVE.AsInteger:=3;
              taDocHOutOPRIZN.AsInteger:=iTd;
              taDocHOut.Post;
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDocHOut.Active:=False;
        taDeps.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton12Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sNum:String;
    iCode,IdH,iNum,IdHCur:INteger;
    rQuant,rPriceIn,rSumIn,rPriceOut,rSumOut:Real;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHOut.Active:=True;
        taDocSpecOut.Active:=True;

        iNum:=1; IdHCur:=0;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,1].Value));
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          rQuant:=StrToFloatDef(String(ISheet.Cells.Item[i,6].Value),0);
          rPriceIn:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
          rSumIn:=StrToFloatDef(String(ISheet.Cells.Item[i,8].Value),0);
          rPriceOut:=StrToFloatDef(String(ISheet.Cells.Item[i,9].Value),0);
          rSumOut:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);

          if (pos('�����������������',sNumDoc)>0)or(pos('�������������',sNumDoc)>0) then
          begin
            sNum:=sNumDoc;
            if pos('�����������������',sNumDoc)>0 then delete(sNum,1,17);        //�����������������1\�����-0001822
            if pos('�������������',sNumDoc)>0 then delete(sNum,1,13);        //�����������������1\�����-0001822
            if (taDocHOut.Locate('NUMDOC',sNum,[])=True)and(iCode>0) then
            begin
              IdH:=taDocHOutID.AsInteger;

              if IdHCur<>IdH then
              begin
                IdHCur:=IdH;
                iNum:=1;
              end;

              taDocSpecOut.Append;
              taDocSpecOutIDHEAD.AsInteger:=IdH;
              taDocSpecOutNUM.AsInteger:=iNum;
              taDocSpecOutIDCARD.AsInteger:=iCode;
              taDocSpecOutSBAR.AsString:='';
              taDocSpecOutIDM.AsInteger:=1;
              taDocSpecOutQUANT.AsFloat:=rQuant;
              taDocSpecOutPRICEIN.AsFloat:=rPriceIn;
              taDocSpecOutSUMIN.AsFloat:=rSumIn;
              taDocSpecOutPRICEIN0.AsFloat:=rPriceIn;
              taDocSpecOutSUMIN0.AsFloat:=rSumIn;
              taDocSpecOutPRICER.AsFloat:=rPriceOut;
              taDocSpecOutSUMR.AsFloat:=rSumOut;
              taDocSpecOutPRICER0.AsFloat:=rPriceOut;
              taDocSpecOutSUMR0.AsFloat:=rSumOut;

              taDocSpecOutNDSPROC.AsFloat:=0;
              taDocSpecOutSUMNDSIN.AsFloat:=0;
              taDocSpecOutSUMNDSOUT.AsFloat:=0;
              taDocSpecOutGTD.AsString:='';
              taDocSpecOutSNUMHEAD.AsString:=sNum;
              taDocSpecOut.Post;

              inc(iNum);
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDocHOut.Active:=False;
        taDocSpecOut.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton13Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sMh,sDate,sType:String;
    iMH,IdH:INteger;
    DocDate:TDateTime;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHInv.Active:=True;
        taDeps.Active:=True;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sType:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,4].Value));
          sMh:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sDate:=Trim(String(ISheet.Cells.Item[i,3].Value));

          DocDate:=StrToDateDef(sDate,date);

          if pos('�����������������������',sType)>0 then
          begin
//            if taDeps.Locate('NAME',sMh,[]) then
//            begin
//              iMH:=taDepsID.AsInteger;
              iMH:=1;
              IdH:=fGetID(12);

              taDocHInv.Append;
              taDocHInvIDSKL.AsInteger:=iMH;
              taDocHInvIDATEDOC.AsInteger:=Trunc(DocDate);
              taDocHInvID.AsInteger:=IDH;
              taDocHInvDATEDOC.AsDateTime:=DocDate;
              taDocHInvNUMDOC.AsString:=sNumDoc;
              taDocHInvSUMINR.AsFloat:=0;
              taDocHInvSUMINF.AsFloat:=0;
              taDocHInvSUMINRTO.AsFloat:=0;
              taDocHInvRSUMR.AsFloat:=0;
              taDocHInvRSUMF.AsFloat:=0;
              taDocHInvRSUMTO.AsFloat:=0;
              taDocHInvSUMTARR.AsFloat:=0;
              taDocHInvSUMTARF.AsFloat:=0;
              taDocHInvIACTIVE.AsInteger:=1;
              taDocHInvIDETAIL.AsInteger:=1;
              taDocHInv.Post;
//            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDocHInv.Active:=False;
        taDeps.Active:=False;
      end;
      
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton14Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sNum:String;
    iCode,IdH,iNum,IdHCur:INteger;
    rQuant,rPriceOut,rSumOut:Real;
    rQuantF,rSumOutF:Real;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

//    exit; //��������

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHInv.Active:=True;

        iNum:=1; IdHCur:=0;

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,1].Value));
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
          rQuant:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
          rPriceOut:=StrToFloatDef(String(ISheet.Cells.Item[i,6].Value),0);
          rSumOut:=StrToFloatDef(String(ISheet.Cells.Item[i,8].Value),0);
          rQuantF:=StrToFloatDef(String(ISheet.Cells.Item[i,9].Value),0);
          rSumOutF:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);

          if pos('�����������������������',sNumDoc)>0 then
          begin
            sNum:=sNumDoc;
            delete(sNum,1,23);        //�����������������������1\�����-0001822
            if (taDocHInv.Locate('NUMDOC',sNum,[])=True)and(iCode>0) then
            begin
              IdH:=taDocHInvID.AsInteger;

              if IdHCur<>IdH then
              begin
                IdHCur:=IdH;
                iNum:=1;
                quDocSpecInv.Active:=False;
                quDocSpecInv.Parameters.ParamByName('IDH').Value:=IdH;
                quDocSpecInv.Active:=True;
              end;

              if quDocSpecInv.Locate('IDCARD',iCode,[]) then
              begin
                rQuant:=rQuant+quDocSpecInvQUANTR.AsFloat;
                rPriceOut:=quDocSpecInvPRICER.AsFloat;
                rQuantF:=rQuantF+quDocSpecInvQUANTF.AsFloat;
                rSumOutF:=rSumOutF+quDocSpecInvSUMRF.AsFloat;

                quDocSpecInv.Edit;
                quDocSpecInvQUANTR.AsFloat:=rQuant;
                quDocSpecInvPRICER.AsFloat:=rPriceOut;
                quDocSpecInvSUMR.AsFloat:=rPriceOut*rQuant;
                quDocSpecInvQUANTF.AsFloat:=rQuantF;
                quDocSpecInvSUMRF.AsFloat:=rSumOutF;
                quDocSpecInv.Post;
              end else
              begin
                quDocSpecInv.Append;
                quDocSpecInvIDHEAD.AsInteger:=IdH;
                quDocSpecInvNUM.AsInteger:=iNum;
                quDocSpecInvIDCARD.AsInteger:=iCode;
                quDocSpecInvSBAR.AsString:='';
//              quDocSpecInvIDM.AsInteger:=1;

                quDocSpecInvQUANTR.AsFloat:=rQuant;
                quDocSpecInvPRICER.AsFloat:=rPriceOut;
                quDocSpecInvSUMR.AsFloat:=rSumOut;

                quDocSpecInvQUANTF.AsFloat:=rQuantF;
                quDocSpecInvSUMRF.AsFloat:=rSumOutF;

                quDocSpecInvSUMIN0.AsFloat:=0;
                quDocSpecInvSUMIN.AsFloat:=0;
                quDocSpecInvSUMIN0F.AsFloat:=0;
                quDocSpecInvSUMINF.AsFloat:=0;
                quDocSpecInvQUANTC.AsFloat:=0;
                quDocSpecInvSUMC.AsFloat:=0;
                quDocSpecInvSUMRSC.AsFloat:=0;
                quDocSpecInvSumRTO.AsFloat:=0;
                quDocSpecInv.Post;
              end;

              inc(iNum);
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        quPrice.Active:=False;
        taDocHInv.Active:=False;
        quDocSpecInv.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.FormCreate(Sender: TObject);
var StrWk:String;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  msConnection.Connected:=False;
  Strwk:='FILE NAME='+CurDir+'mcr.udl';
  msConnection.ConnectionString:=Strwk;
  delay(10);
  msConnection.Connected:=True;
  delay(10);
  if msConnection.Connected then Memo1.Lines.add('���������� ��');
end;

procedure TfmImpD.cxButton15Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    sNumDoc,sNum:String;
    iCode,IdH,iNum,IdHCur:INteger;
  //  rQuant,rPriceOut,rSumOut:Real;
    rQuantF,rSumOutF:Real;
    par:Variant;
begin
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

//    exit; //��������

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDocHInv.Active:=True;
        taDocSpecInv.Active:=True;

        iNum:=1; IdHCur:=0;
        par := VarArrayCreate([0,1], varInteger);

        Memo1.Lines.Add('���� ��'); delay(10);
        i:=1;

        
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,1].Value));
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 3].Value),0);
//          rQuant:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
//          rPriceOut:=StrToFloatDef(String(ISheet.Cells.Item[i,6].Value),0);
//          rSumOut:=StrToFloatDef(String(ISheet.Cells.Item[i,8].Value),0);
          rQuantF:=StrToFloatDef(String(ISheet.Cells.Item[i,9].Value),0);
          rSumOutF:=StrToFloatDef(String(ISheet.Cells.Item[i,10].Value),0);

          if pos('�����������������������',sNumDoc)>0 then
          begin
            sNum:=sNumDoc;
            delete(sNum,1,23);        //�����������������������1\�����-0001822
            if (taDocHInv.Locate('NUMDOC',sNum,[])=True)and(iCode>0) then
            begin
              IdH:=taDocHInvID.AsInteger;
              if taDocHInvIDSKL.AsInteger in [3,103] then
              begin
                par[0]:=IdH;
                par[1]:=iCode;

                if IdHCur<>IdH then
                begin
                  IdHCur:=IdH;
                end;

                if taDocSpecInv.Locate('IDHEAD;IDCARD',par,[]) then
                begin
                  rQuantF:=rQuantF+taDocSpecInvQUANTF.AsFloat;
                  rSumOutF:=rSumOutF+taDocSpecInvSUMRF.AsFloat;

                  taDocSpecInv.Edit;
                  taDocSpecInvQUANTF.AsFloat:=rQuantF;
                  taDocSpecInvSUMRF.AsFloat:=rSumOutF;
                  taDocSpecInv.Post;
                  inc(iNum);
                end;
              end;
            end;
          end;

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)+' '+its(iNum)); delay(10);
          end;
        end;
        taDocHInv.Active:=False;
        taDocSpecInv.Active:=False;
      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

procedure TfmImpD.cxButton17Click(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    iCode,iDep:INteger;
    sDep:String;
    rPr:Real;
begin
  //����
  if OpenDialog1.Execute then fName:=OpenDialog1.fileName;
  if FileExists(fName) then
  begin
    //����� ���������
    Memo1.Clear;
    Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fName);
    ISheet := Workbook.Worksheets.Item[1];

    //�������� ����
    with dmTMP do
    begin
      if msConnection.Connected then
      begin
        taDeps.Active:=True;
        quPrice.Active:=False;
        quPrice.Parameters.ParamByName('ISKL').Value:=111;
        quPrice.Active:=True;


        Memo1.Lines.Add('���� ��'); delay(10);
        i:=2;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
//        While i<43200 do
        begin
          iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 2].Value),0);
          if iCode>0 then
          begin
            sDep:=Trim(String(ISheet.Cells.Item[i,3].Value));
            rPr:=StrToFloatDef(String(ISheet.Cells.Item[i,4].Value),0);
            iDep:=0;
            if sDep>'' then

            if pos('111',sDep)>0 then
            begin
              iDep:=111;
            end;

            if iDep>0 then
            begin
              try
                if quPrice.Locate('GOODSID',iCode,[]) then
                begin
                  quPrice.Edit;
//                  quPriceGOODSID.AsInteger:=iCode;
//                  quPriceIDSKL.AsInteger:=iDep;
                  quPricePRICE.AsFloat:=rPr;
                  quPrice.Post;

                end else
                begin
                  quPrice.Append;
                  quPriceGOODSID.AsInteger:=iCode;
                  quPriceIDSKL.AsInteger:=iDep;
                  quPricePRICE.AsFloat:=rPr;
                  quPrice.Post;
                end;

              except
                quPrice.Cancel;
              end;
            end;
          end;

          inc(i);
          
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('��������� ����� '+its(i)); delay(10);
          end;
        end;
        taDeps.Active:=False;
//        taPrice.Active:=False;
        quPrice.Active:=False;

      end;
    end;
    ExcelApp.Quit;
    ExcelApp:=Unassigned;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end else showmessage('���� �� ������');
end;

end.
