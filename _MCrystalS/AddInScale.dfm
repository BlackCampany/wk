object fmAddInScale: TfmAddInScale
  Left = 343
  Top = 135
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1074#1077#1089#1099'.'
  ClientHeight = 352
  ClientWidth = 361
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 288
    Width = 32
    Height = 13
    Caption = 'Label1'
    Transparent = True
  end
  object cxButton1: TcxButton
    Left = 40
    Top = 312
    Width = 133
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' ('#1086#1073#1085#1086#1074#1080#1090#1100')'
    Default = True
    ModalResult = 1
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 228
    Top = 312
    Width = 91
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object GrAddInSc: TcxGrid
    Left = 12
    Top = 8
    Width = 337
    Height = 269
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViAddInSc: TcxGridDBTableView
      OnDblClick = ViAddInScDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMCS.dsquScales
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViAddInScID: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '
        DataBinding.FieldName = 'ID'
        Width = 68
      end
      object ViAddInScNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 237
      end
    end
    object LeAddInSc: TcxGridLevel
      GridView = ViAddInSc
    end
  end
end
