object fmAddParams: TfmAddParams
  Left = 1677
  Top = 284
  Width = 758
  Height = 584
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1099
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 56
    Height = 13
    Caption = #1050#1072#1088#1090#1086#1095#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 71
    Height = 13
    Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
  end
  object Label3: TLabel
    Left = 24
    Top = 152
    Width = 23
    Height = 13
    Caption = #1043#1086#1089#1090
  end
  object Label4: TLabel
    Left = 24
    Top = 176
    Width = 19
    Height = 13
    Caption = #1042#1077#1089
  end
  object Label5: TLabel
    Left = 24
    Top = 200
    Width = 36
    Height = 13
    Caption = #1057#1086#1089#1090#1072#1074
  end
  object Label6: TLabel
    Left = 24
    Top = 320
    Width = 97
    Height = 13
    Caption = #1055#1080#1097#1077#1074#1072#1103' '#1094#1077#1085#1085#1086#1089#1090#1100
  end
  object Label7: TLabel
    Left = 24
    Top = 397
    Width = 215
    Height = 13
    Caption = #1050#1072#1083#1086#1088#1080#1081#1085#1086#1089#1090#1100' / '#1069#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1072#1103' '#1094#1077#1085#1085#1086#1089#1090#1100
  end
  object Label8: TLabel
    Left = 24
    Top = 428
    Width = 74
    Height = 13
    Caption = #1057#1088#1086#1082' '#1075#1086#1076#1085#1086#1089#1090#1080
  end
  object Label9: TLabel
    Left = 24
    Top = 460
    Width = 94
    Height = 13
    Caption = #1059#1089#1083#1086#1074#1080#1103' '#1093#1088#1072#1085#1077#1085#1080#1103
  end
  object Label10: TLabel
    Left = 24
    Top = 32
    Width = 44
    Height = 13
    Caption = #1048#1079#1076#1077#1083#1080#1077
  end
  object Panel1: TPanel
    Left = 632
    Top = 0
    Width = 110
    Height = 546
    Align = alRight
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 24
      Width = 97
      Height = 41
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 88
      Width = 97
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxMemo1: TcxMemo
    Left = 112
    Top = 56
    Lines.Strings = (
      'cxMemo1')
    TabOrder = 1
    Height = 89
    Width = 497
  end
  object cxTextEdit1: TcxTextEdit
    Left = 112
    Top = 148
    TabOrder = 2
    Text = 'cxTextEdit1'
    Width = 497
  end
  object cxTextEdit2: TcxTextEdit
    Left = 112
    Top = 172
    TabOrder = 3
    Text = 'cxTextEdit2'
    Width = 497
  end
  object cxMemo2: TcxMemo
    Left = 112
    Top = 200
    Lines.Strings = (
      'cxMemo2')
    TabOrder = 4
    Height = 113
    Width = 497
  end
  object cxMemo3: TcxMemo
    Left = 144
    Top = 320
    Lines.Strings = (
      'cxMemo3')
    TabOrder = 5
    Height = 65
    Width = 241
  end
  object cxTextEdit3: TcxTextEdit
    Left = 256
    Top = 393
    TabOrder = 6
    Text = 'cxTextEdit3'
    Width = 353
  end
  object cxTextEdit4: TcxTextEdit
    Left = 120
    Top = 424
    TabOrder = 7
    Text = 'cxTextEdit4'
    Width = 313
  end
  object cxMemo4: TcxMemo
    Left = 136
    Top = 456
    Lines.Strings = (
      'cxMemo4')
    TabOrder = 8
    Height = 73
    Width = 473
  end
  object cxTextEdit5: TcxTextEdit
    Left = 112
    Top = 28
    TabOrder = 9
    Text = 'cxTextEdit5'
    Width = 497
  end
end
