object fmLabels: TfmLabels
  Left = 618
  Top = 355
  BorderStyle = bsDialog
  Caption = #1062#1077#1085#1085#1080#1082#1080
  ClientHeight = 292
  ClientWidth = 499
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GrLabels: TcxGrid
    Left = 16
    Top = 12
    Width = 325
    Height = 237
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewLabels: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquLabels
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object ViewLabelsID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Editing = False
        Width = 48
      end
      object ViewLabelsName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 139
      end
      object ViewLabelsNameF: TcxGridDBColumn
        Caption = #1060#1072#1081#1083
        DataBinding.FieldName = 'NameF'
        Width = 135
      end
    end
    object LevelLabels: TcxGridLevel
      GridView = ViewLabels
    end
  end
  object Panel1: TPanel
    Left = 354
    Top = 0
    Width = 145
    Height = 292
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 12
      Top = 16
      Width = 117
      Height = 29
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object dsquLabels: TDataSource
    DataSet = quLabels
    Left = 88
    Top = 120
  end
  object quLabels: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'select * from LABELS'
      'order by Name')
    Left = 88
    Top = 64
    object quLabelsID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quLabelsName: TStringField
      FieldName = 'Name'
      Size = 50
    end
    object quLabelsNameF: TStringField
      FieldName = 'NameF'
      Size = 50
    end
  end
end
