unit Period3Short;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxGraphics, cxButtonEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxCalendar, cxCheckBox, DB, ADODB;

type
  TfmPer3 = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxCheckBox2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPer3: TfmPer3;

implementation

uses Dm, Clients, Un1, ClasSel;

{$R *.dfm}

procedure TfmPer3.FormCreate(Sender: TObject);
begin
  cxCheckBox1.Checked:=False;
  cxCheckBox2.Checked:=True;

  cxDateEdit1.Date:=Trunc(Date-30);
  cxDateEdit2.Date:=Date;
end;

procedure TfmPer3.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxLookupComboBox1.Enabled:=False;
  end else
  begin
    cxLookupComboBox1.Enabled:=True;
  end;
end;

procedure TfmPer3.cxCheckBox2PropertiesChange(Sender: TObject);
begin
  if cxCheckBox2.Checked then
  begin
    cxLookupComboBox2.Enabled:=False;
  end else
  begin
    cxLookupComboBox2.Enabled:=True;
  end;
end;

end.
