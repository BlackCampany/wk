unit RemnsDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Placemnt,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, cxContainer, cxTextEdit,
  cxMemo, ExtCtrls, SpeedBar, dxmdaset, Menus, ActnList, XPStyleActnCtrls,
  ActnMan;

type
  TfmRemnsDay = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    Panel5: TPanel;
    Memo1: TcxMemo;
    GridRemnsDay: TcxGrid;
    ViewRemnsDay: TcxGridDBTableView;
    LevelRemnsDay: TcxGridLevel;
    fmplRemnsDay: TFormPlacement;
    teRD: TdxMemData;
    dsteRD: TDataSource;
    teRDICODE: TIntegerField;
    teRDISKL: TIntegerField;
    teRDIDCLASSIF: TIntegerField;
    teRDNAME: TStringField;
    teRDBARCODE: TStringField;
    teRDEDIZM: TIntegerField;
    teRDRNAC: TFloatField;
    teRDsDep: TStringField;
    teRDsClass: TStringField;
    teRDrQRemn: TFloatField;
    teRDrSpeed: TFloatField;
    teRDrDays: TFloatField;
    ViewRemnsDayICODE: TcxGridDBColumn;
    ViewRemnsDayISKL: TcxGridDBColumn;
    ViewRemnsDayIDCLASSIF: TcxGridDBColumn;
    ViewRemnsDayNAME: TcxGridDBColumn;
    ViewRemnsDayBARCODE: TcxGridDBColumn;
    ViewRemnsDayEDIZM: TcxGridDBColumn;
    ViewRemnsDayRNAC: TcxGridDBColumn;
    ViewRemnsDaysDep: TcxGridDBColumn;
    ViewRemnsDaysClass: TcxGridDBColumn;
    ViewRemnsDayrQRemn: TcxGridDBColumn;
    ViewRemnsDayrSpeed: TcxGridDBColumn;
    ViewRemnsDayrDays: TcxGridDBColumn;
    amRepRemnDay: TActionManager;
    acMoveRep1: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    teRDsClass2: TStringField;
    teRDABCQ: TSmallintField;
    teRDABCP: TSmallintField;
    ViewRemnsDaysClass2: TcxGridDBColumn;
    ViewRemnsDayABCP: TcxGridDBColumn;
    ViewRemnsDayABCQ: TcxGridDBColumn;
    teRDIdCli: TIntegerField;
    teRDNameCli: TStringField;
    teRDDayZ: TIntegerField;
    teRDDayZStrah: TIntegerField;
    teRDZKoef: TFloatField;
    teRDQuantZ: TFloatField;
    ViewRemnsDayIdCli: TcxGridDBColumn;
    ViewRemnsDayNameCli: TcxGridDBColumn;
    ViewRemnsDayDayZ: TcxGridDBColumn;
    ViewRemnsDayDayZStrah: TcxGridDBColumn;
    ViewRemnsDayZKoef: TcxGridDBColumn;
    ViewRemnsDayQuantZ: TcxGridDBColumn;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure acMoveRep1Execute(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRemnsDay: TfmRemnsDay;

implementation

uses Un1, Dm, Move;

{$R *.dfm}

procedure TfmRemnsDay.FormCreate(Sender: TObject);
begin
  fmplRemnsDay.IniFileName:=sFormIni;
  fmplRemnsDay.Active:=True; delay(10);

  GridRemnsDay.Align:=AlClient;
end;

procedure TfmRemnsDay.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRemnsDay);
end;

procedure TfmRemnsDay.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRemnsDay.acMoveRep1Execute(Sender: TObject);
begin
  with dmMCS do
  begin
    if teRD.RecordCount>0 then
    begin
      prFormMove(teRDISKL.AsInteger,teRDICODE.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),teRDNAME.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmRemnsDay.N3Click(Sender: TObject);
begin
  prExpandVi(ViewRemnsDay,True);
end;

procedure TfmRemnsDay.N4Click(Sender: TObject);
begin
  prExpandVi(ViewRemnsDay,False);
end;

end.
