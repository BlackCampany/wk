unit Shops;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit, ADODB,
  cxDropDownEdit;

type
  TfmShops = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewShops: TcxGridDBTableView;
    LevelShops: TcxGridLevel;
    GridShops: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    Timer1: TTimer;
    amShops: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    quShops: TADOQuery;
    quShopsID: TIntegerField;
    quShopsNAME: TStringField;
    quShopsISTATUS: TSmallintField;
    dsquShops: TDataSource;
    ViewShopsID: TcxGridDBColumn;
    ViewShopsNAME: TcxGridDBColumn;
    ViewShopsISTATUS: TcxGridDBColumn;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmShops: TfmShops;
  bClearShops:Boolean = False;

implementation

uses Un1,Dm, AddShop;

{$R *.dfm}

procedure TfmShops.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmShops.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmShops.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmShops.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmShops.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.FormCreate(Sender: TObject);
begin
  GridShops.Align:=AlClient;
end;

procedure TfmShops.Timer1Timer(Sender: TObject);
begin
  if bClearShops=True then begin StatusBar1.Panels[0].Text:=''; bClearShops:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearShops:=True;
end;

procedure TfmShops.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmShops.acDelExecute(Sender: TObject);
begin            //�������
  if not CanDo('prDelShop') then begin Showmessage('��� ����.'); exit; end;
  with fmShops do
  with dmMCS do
  begin
    if quShops.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quShopsNAME.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if fTestDel(1,quShopsID.AsInteger) then
        begin
          quShops.Delete;
        end else ShowMessage('�������� ���������, ���� ��������� �������.');
      end;
    end;
  end;
end;

procedure TfmShops.acEditExecute(Sender: TObject);
begin                  //�������������
  if not CanDo('prEditShop') then begin Showmessage('��� ����.'); exit; end;
  with fmShops do
  begin
    if quShops.RecordCount>0 then
    begin
      fmAddShop:=tfmAddShop.Create(Application);
      try
        fmAddShop.cxTextEdit1.Text:=quShopsNAME.AsString;
        fmAddShop.cxSpinEdit1.Value:=quShopsID.AsInteger;
        fmAddShop.cxSpinEdit1.Properties.ReadOnly:=True;
        fmAddShop.cxComboBox1.ItemIndex:=quShopsISTATUS.AsInteger;
        fmAddShop.ShowModal;
        if fmAddShop.ModalResult=mrOk then
        begin
          try
            ViewShops.BeginUpdate;
            quShops.Edit;
            quShopsNAME.AsString:=fmAddShop.cxTextEdit1.Text;
            quShopsISTATUS.AsInteger:=fmAddShop.cxComboBox1.ItemIndex;
            quShops.Post;
          finally
            ViewShops.EndUpdate;
          end;
        end;
      finally
        fmAddShop.Release;
      end;
    end else showmessage('�������� ������ ��� ��������������.');
  end;
end;

procedure TfmShops.acAddExecute(Sender: TObject);
begin                //��������
  if not CanDo('prAddShop') then begin Showmessage('��� ����.'); exit; end;
  with fmShops do
  begin
    fmAddShop:=tfmAddShop.Create(Application);
    try
      fmAddShop.cxTextEdit1.Text:='';
      fmAddShop.cxSpinEdit1.Properties.ReadOnly:=False;

      fmAddShop.cxComboBox1.ItemIndex:=1;
      fmAddShop.ShowModal;
      if fmAddShop.ModalResult=mrOk then
      begin
        if fmAddShop.cxSpinEdit1.Value>0 then
        begin
          if quShops.Locate('ID',fmAddShop.cxSpinEdit1.Value,[]) then
          begin
            ShowMessage('������� � ����� ������� ��� ����. ���������� ����������.');
          end else
          begin
            try
              ViewShops.BeginUpdate;
              quShops.Append;
              quShopsID.AsInteger:=fmAddShop.cxSpinEdit1.Value;
              quShopsNAME.AsString:=fmAddShop.cxTextEdit1.Text;
              quShopsISTATUS.AsInteger:=fmAddShop.cxComboBox1.ItemIndex;
              quShops.Post;
            finally
              ViewShops.EndUpdate;
            end;
          end;
        end else showmessage('������������ ����� ��������. ���������� ����������.');
      end;
    finally
      fmAddShop.Release;
    end;
  end;
end;

procedure TfmShops.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmShops.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmShops.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmShops.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmShops.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

end.
