object fmAddPost: TfmAddPost
  Left = 260
  Top = 196
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1082#1072
  ClientHeight = 562
  ClientWidth = 902
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 11
    Width = 59
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 43
    Width = 105
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 84
    Width = 28
    Height = 13
    Caption = #1048#1053#1053
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 464
    Top = 43
    Width = 23
    Height = 13
    Caption = #1058#1080#1087
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 240
    Top = 84
    Width = 27
    Height = 13
    Caption = #1050#1055#1055
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 464
    Top = 78
    Width = 45
    Height = 13
    Caption = #1048#1085#1076#1077#1082#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 656
    Top = 78
    Width = 36
    Height = 13
    Caption = #1043#1086#1088#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label8: TLabel
    Left = 24
    Top = 116
    Width = 38
    Height = 13
    Caption = #1059#1083#1080#1094#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label9: TLabel
    Left = 320
    Top = 116
    Width = 27
    Height = 13
    Caption = #1044#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 24
    Top = 148
    Width = 102
    Height = 13
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 24
    Top = 176
    Width = 111
    Height = 13
    Caption = #1040#1076#1088#1077#1089' '#1075#1088#1091#1079#1086#1087#1086#1083#1091#1095'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label12: TLabel
    Left = 24
    Top = 207
    Width = 59
    Height = 13
    Caption = #1056#1072#1089#1095'.'#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label13: TLabel
    Left = 252
    Top = 207
    Width = 26
    Height = 13
    Caption = #1041#1048#1050
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label14: TLabel
    Left = 24
    Top = 239
    Width = 30
    Height = 13
    Caption = #1041#1072#1085#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label15: TLabel
    Left = 24
    Top = 271
    Width = 64
    Height = 13
    Caption = #1050#1086#1088#1088'. '#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label16: TLabel
    Left = 24
    Top = 303
    Width = 93
    Height = 13
    Caption = #1044#1086#1075#1086#1074#1086#1088' '#1085#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label17: TLabel
    Left = 272
    Top = 303
    Width = 16
    Height = 13
    Caption = #1054#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label18: TLabel
    Left = 360
    Top = 11
    Width = 23
    Height = 13
    Caption = #1050#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label19: TLabel
    Left = 24
    Top = 335
    Width = 59
    Height = 13
    Caption = #1054#1090#1074'. '#1083#1080#1094#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label20: TLabel
    Left = 24
    Top = 366
    Width = 53
    Height = 13
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label31: TLabel
    Left = 464
    Top = 110
    Width = 110
    Height = 13
    Caption = #1044#1086#1089#1090#1091#1087' '#1082' '#1079#1072#1082#1072#1079#1072#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label32: TLabel
    Left = 455
    Top = 308
    Width = 214
    Height = 13
    Caption = 'E-Mail '#1076#1083#1103' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1103' '#1089#1087#1077#1094#1080#1092'-'#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 490
    Width = 902
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = 14211286
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 324
      Top = 12
      Width = 97
      Height = 33
      Caption = #1054#1082
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 508
      Top = 12
      Width = 105
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 543
    Width = 902
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxTextEdit1: TcxTextEdit
    Left = 128
    Top = 5
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit1'
    Width = 193
  end
  object cxTextEdit2: TcxTextEdit
    Left = 128
    Top = 37
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit2'
    Width = 305
  end
  object cxTextEdit3: TcxTextEdit
    Left = 80
    Top = 78
    Style.Shadow = True
    TabOrder = 5
    Text = 'cxTextEdit3'
    Width = 129
  end
  object cxImageComboBox1: TcxImageComboBox
    Left = 520
    Top = 37
    EditValue = '1'
    Properties.Images = dmC.ImageList1
    Properties.Items = <
      item
        Description = #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
        ImageIndex = 21
        Value = 1
      end
      item
        Description = #1048#1055
        ImageIndex = 10
        Value = 2
      end>
    Properties.LargeImages = dmC.ImageList1
    Style.Shadow = True
    TabOrder = 4
    Width = 185
  end
  object cxTextEdit4: TcxTextEdit
    Left = 296
    Top = 78
    Style.Shadow = True
    TabOrder = 6
    Text = 'cxTextEdit4'
    Width = 137
  end
  object cxTextEdit5: TcxTextEdit
    Left = 520
    Top = 72
    Style.Shadow = True
    TabOrder = 7
    Text = 'cxTextEdit5'
    Width = 97
  end
  object cxTextEdit6: TcxTextEdit
    Left = 712
    Top = 72
    Style.Shadow = True
    TabOrder = 8
    Text = 'cxTextEdit6'
    Width = 161
  end
  object cxTextEdit7: TcxTextEdit
    Left = 80
    Top = 110
    Style.Shadow = True
    TabOrder = 9
    Text = 'cxTextEdit7'
    Width = 225
  end
  object cxTextEdit8: TcxTextEdit
    Left = 368
    Top = 110
    Style.Shadow = True
    TabOrder = 10
    Text = 'cxTextEdit8'
    Width = 65
  end
  object cxTextEdit9: TcxTextEdit
    Left = 140
    Top = 142
    Style.Shadow = True
    TabOrder = 11
    Text = 'cxTextEdit9'
    Width = 293
  end
  object cxTextEdit10: TcxTextEdit
    Left = 140
    Top = 170
    Style.Shadow = True
    TabOrder = 12
    Text = 'cxTextEdit10'
    Width = 293
  end
  object cxTextEdit11: TcxTextEdit
    Left = 100
    Top = 201
    Style.Shadow = True
    TabOrder = 13
    Text = 'cxTextEdit11'
    Width = 135
  end
  object cxTextEdit12: TcxTextEdit
    Left = 298
    Top = 201
    Style.Shadow = True
    TabOrder = 14
    Text = 'cxTextEdit12'
    Width = 135
  end
  object cxTextEdit13: TcxTextEdit
    Left = 80
    Top = 233
    Style.Shadow = True
    TabOrder = 15
    Text = 'cxTextEdit13'
    Width = 353
  end
  object cxTextEdit14: TcxTextEdit
    Left = 100
    Top = 265
    Style.Shadow = True
    TabOrder = 16
    Text = 'cxTextEdit14'
    Width = 333
  end
  object cxTextEdit15: TcxTextEdit
    Left = 128
    Top = 297
    Style.Shadow = True
    TabOrder = 17
    Text = 'cxTextEdit15'
    Width = 135
  end
  object cxCheckBox1: TcxCheckBox
    Left = 16
    Top = 454
    Caption = #1053#1077' '#1074#1099#1074#1086#1076#1080#1090#1100' '#1074' '#1086#1090#1095#1077#1090' '#1087#1086' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1102' '#1094#1077#1085
    ParentFont = False
    Properties.Alignment = taRightJustify
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 18
    Width = 414
  end
  object cxDateEdit1: TcxDateEdit
    Left = 296
    Top = 297
    EditValue = 40179d
    Properties.DateButtons = [btnClear, btnToday]
    Properties.DateOnError = deNull
    Properties.SaveTime = False
    Properties.ShowTime = False
    Properties.WeekNumbers = True
    Style.Shadow = True
    TabOrder = 19
    Width = 137
  end
  object GroupBox1: TGroupBox
    Left = 453
    Top = 152
    Width = 417
    Height = 146
    Caption = '  '#1040#1074#1090#1086#1079#1072#1082#1072#1079'  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
    object Label21: TLabel
      Left = 12
      Top = 24
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label22: TLabel
      Left = 216
      Top = 86
      Width = 74
      Height = 13
      Caption = 'GLN ('#1043#1051#1053' '#1082#1086#1076')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label23: TLabel
      Left = 12
      Top = 90
      Width = 29
      Height = 13
      Caption = 'E-Mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label24: TLabel
      Left = 176
      Top = 58
      Width = 56
      Height = 13
      Caption = #1055#1088#1086#1074#1072#1081#1076#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label25: TLabel
      Left = 176
      Top = 24
      Width = 99
      Height = 13
      Caption = #1052#1080#1085'. '#1089#1091#1084#1084#1072' '#1079#1072#1082#1072#1079#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cxImageComboBox2: TcxImageComboBox
      Left = 52
      Top = 20
      EditValue = 0
      ParentFont = False
      Properties.Items = <
        item
          Description = #1085#1077#1090
          ImageIndex = 0
          Value = 0
        end
        item
          Description = 'EDI'
          Value = 1
        end
        item
          Description = 'E-Mail'
          Value = 2
        end
        item
          Description = 'EDI ('#1073#1077#1079' '#1072#1074#1090#1086#1086#1090#1087#1088#1072#1074#1082#1080')'
          Value = 3
        end
        item
          Description = #1048#1085#1086#1081
          Value = 4
        end
        item
          Description = #1055#1088#1080#1085#1080#1084#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1073#1077#1079' '#1079#1072#1103#1074#1082#1080
          Value = 5
        end
        item
          Description = #1057#1074#1086#1105' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
          Value = 6
        end
        item
          Description = 'E-Mail ('#1073#1077#1079' '#1072'.'#1086'.)'
          Value = 7
        end
        item
          Description = #1057#1074#1086#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1072#1074#1090#1086
          Value = 8
        end>
      Style.Shadow = True
      TabOrder = 0
      Width = 93
    end
    object cxTextEdit18: TcxTextEdit
      Left = 296
      Top = 83
      ParentFont = False
      Style.Shadow = True
      TabOrder = 1
      Text = 'cxTextEdit18'
      Width = 109
    end
    object cxTextEdit19: TcxTextEdit
      Left = 52
      Top = 83
      ParentFont = False
      Style.Shadow = True
      TabOrder = 2
      Text = 'cxTextEdit19'
      Width = 141
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 248
      Top = 50
      ParentFont = False
      Properties.KeyFieldNames = 'Id'
      Properties.ListColumns = <
        item
          Caption = #1055#1088#1086#1074#1072#1081#1076#1077#1088
          FieldName = 'Name'
        end>
      Properties.ListSource = dmC.dsquProv
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 3
      Width = 157
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 284
      Top = 20
      EditValue = 0.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Width = 121
    end
    object cxCheckBox5: TcxCheckBox
      Left = 24
      Top = 54
      Caption = #1053#1077#1090' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      State = cbsGrayed
      TabOrder = 5
      Width = 121
    end
    object cxCheckBox7: TcxCheckBox
      Left = 24
      Top = 116
      Caption = #1053#1077' '#1086#1090#1087#1088#1072#1074#1083#1103#1090#1100' RECADV'
      Properties.NullStyle = nssUnchecked
      TabOrder = 6
      Width = 169
    end
  end
  object cxTextEdit16: TcxTextEdit
    Left = 100
    Top = 329
    Style.Shadow = True
    TabOrder = 21
    Text = 'cxTextEdit16'
    Width = 329
  end
  object cxTextEdit17: TcxTextEdit
    Left = 100
    Top = 360
    Style.Shadow = True
    TabOrder = 22
    Text = 'cxTextEdit17'
    Width = 329
  end
  object cxCheckBox2: TcxCheckBox
    Left = 16
    Top = 396
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057
    ParentFont = False
    Properties.Alignment = taRightJustify
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 23
    Width = 201
  end
  object cxCheckBox3: TcxCheckBox
    Left = 16
    Top = 423
    Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1081' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090
    ParentFont = False
    Properties.Alignment = taRightJustify
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 24
    Width = 201
  end
  object cxCheckBox4: TcxCheckBox
    Left = 224
    Top = 396
    Caption = #1053#1077#1089#1082#1086#1083#1100#1082#1086' '#1089#1082#1083#1072#1076#1086#1074
    ParentFont = False
    Properties.Alignment = taRightJustify
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 25
    Width = 205
  end
  object GroupBox2: TGroupBox
    Left = 452
    Top = 332
    Width = 417
    Height = 144
    Caption = #1042#1086#1079#1074#1088#1072#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 26
    object Label26: TLabel
      Left = 10
      Top = 26
      Width = 49
      Height = 13
      Caption = #1054#1090#1074'. '#1083#1080#1094#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label27: TLabel
      Left = 10
      Top = 54
      Width = 45
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label28: TLabel
      Left = 219
      Top = 55
      Width = 29
      Height = 13
      Caption = 'E-Mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label29: TLabel
      Left = 10
      Top = 83
      Width = 69
      Height = 13
      Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label30: TLabel
      Left = 10
      Top = 118
      Width = 82
      Height = 13
      Caption = #1050#1086#1084#1091' '#1087#1088#1077#1090#1077#1085#1079#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cxTextEdit20: TcxTextEdit
      Left = 64
      Top = 20
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 0
      Text = 'cxTextEdit20'
      Width = 345
    end
    object cxTextEdit21: TcxTextEdit
      Left = 64
      Top = 49
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 1
      Text = 'cxTextEdit21'
      Width = 145
    end
    object cxTextEdit22: TcxTextEdit
      Left = 256
      Top = 48
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 2
      Text = 'cxTextEdit22'
      Width = 154
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 84
      Top = 80
      ParentFont = False
      Properties.DropDownListStyle = lsFixedList
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
          FieldName = 'TYPEVOZ'
        end>
      Properties.ListSource = dmP.dsquSelType
      Style.BorderStyle = ebsUltraFlat
      Style.Shadow = True
      TabOrder = 3
      Width = 122
    end
    object cxTextEdit23: TcxTextEdit
      Left = 104
      Top = 112
      ParentFont = False
      Properties.MaxLength = 200
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 4
      Text = 'cxTextEdit23'
      Width = 309
    end
  end
  object cxComboBox1: TcxComboBox
    Left = 584
    Top = 107
    Properties.Items.Strings = (
      #1085#1077#1076#1086#1089#1090#1091#1087#1077#1085' '#1092#1080#1083#1080#1072#1083#1072#1084
      #1076#1086#1089#1090#1091#1087#1077#1085' '#1092#1080#1083#1080#1072#1083#1072#1084)
    TabOrder = 27
    Text = 'cxComboBox1'
    Width = 169
  end
  object cxCheckBox6: TcxCheckBox
    Left = 223
    Top = 423
    Caption = #1041#1077#1079' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
    ParentFont = False
    Properties.Alignment = taRightJustify
    Properties.NullStyle = nssUnchecked
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 28
    Width = 206
  end
  object cxTextEdit24: TcxTextEdit
    Left = 676
    Top = 305
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 29
    Text = 'cxTextEdit24'
    Width = 197
  end
end
