unit RepRemnS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, Menus;

type
  TfmRepRemnSum = class(TForm)
    Panel5: TPanel;
    Memo1: TcxMemo;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    GridRepRemnS: TcxGrid;
    ViewRepRemnS: TcxGridDBTableView;
    ViewRepRemnSIDS: TcxGridDBColumn;
    ViewRepRemnSIDDEP: TcxGridDBColumn;
    ViewRepRemnSNAMEDEP: TcxGridDBColumn;
    ViewRepRemnSID: TcxGridDBColumn;
    ViewRepRemnSNAME: TcxGridDBColumn;
    ViewRepRemnSFULLNAME: TcxGridDBColumn;
    ViewRepRemnSBARCODE: TcxGridDBColumn;
    ViewRepRemnSEDIZM: TcxGridDBColumn;
    ViewRepRemnSRNAC: TcxGridDBColumn;
    ViewRepRemnSsClass: TcxGridDBColumn;
    ViewRepRemnSrQBeg: TcxGridDBColumn;
    ViewRepRemnSrSBeg: TcxGridDBColumn;
    ViewRepRemnSrSBeg0: TcxGridDBColumn;
    ViewRepRemnSrSBegR: TcxGridDBColumn;
    LevelRepRemnS: TcxGridLevel;
    fmplRepRemnSum: TFormPlacement;
    amRepPrib: TActionManager;
    acMoveRep1: TAction;
    acAdvRep: TAction;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    acMoveRepRemnS: TAction;
    ViewRepRemnSsClass2: TcxGridDBColumn;
    ViewRepRemnSABCP: TcxGridDBColumn;
    ViewRepRemnSABCQ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure acMoveRep1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepRemnSum: TfmRepRemnSum;

implementation

uses Un1, Dm, Move;

{$R *.dfm}

procedure TfmRepRemnSum.FormCreate(Sender: TObject);
begin
  fmplRepRemnSum.IniFileName:=sFormIni;
  fmplRepRemnSum.Active:=True; delay(10);
  GridRepRemnS.Align:=AlClient;
end;

procedure TfmRepRemnSum.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepRemnSum.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepRemnS);
end;

procedure TfmRepRemnSum.acMoveRep1Execute(Sender: TObject);
begin
  with dmMCS do
  begin
    if teRR.RecordCount>0 then
    begin
      prFormMove(teRRIDDEP.AsInteger,teRRID.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),teRRNAME.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

end.
