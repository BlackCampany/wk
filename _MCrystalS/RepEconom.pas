unit RepEconom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar;

type
  TfmRepEconom = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRepEconom: TcxGrid;
    ViewRepEconom: TcxGridDBTableView;
    LevelRepEconom: TcxGridLevel;
    Panel5: TPanel;
    Memo1: TcxMemo;
    fmplRepEconom: TFormPlacement;
    amRepEconom: TActionManager;
    acMoveRep1: TAction;
    acAdvRep: TAction;
    SpeedItem7: TSpeedItem;
    ViewRepEconomID: TcxGridDBColumn;
    ViewRepEconomNAME: TcxGridDBColumn;
    ViewRepEconomREMNR: TcxGridDBColumn;
    ViewRepEconomREMNIN: TcxGridDBColumn;
    ViewRepEconomREALR: TcxGridDBColumn;
    ViewRepEconomREALIN: TcxGridDBColumn;
    ViewRepEconomPRIB: TcxGridDBColumn;
    ViewRepEconomIDS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepEconom: TfmRepEconom;

implementation

uses Un1, Dm, MainMC, Move, SummaryRDoc;

{$R *.dfm}

procedure TfmRepEconom.FormCreate(Sender: TObject);
begin
  fmplRepEconom.IniFileName:=sFormIni;
  fmplRepEconom.Active:=True; delay(10);

  GridRepEconom.Align:=AlClient;
end;

procedure TfmRepEconom.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepEconom.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRepEconom);
end;

end.
