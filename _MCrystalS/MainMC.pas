unit MainMC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ActnList, ToolWin, ActnMan, ActnCtrls,
  ActnMenus, ActnColorMaps, XPStyleActnCtrls, cxGraphics, SpeedBar,
  cxDropDownEdit, cxCalendar, cxCheckBox, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  StdCtrls, ExtCtrls, COMOBJ;

const
  sVer:String = '16.009';

type
  TfmMainMC = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    XPColorMap1: TXPColorMap;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acShops: TAction;
    acMH: TAction;
    SpeedBar1: TSpeedBar;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    cxDEdit1: TcxDateEdit;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem12: TSpeedItem;
    SpeedItem13: TSpeedItem;
    SpeedItem14: TSpeedItem;
    acCards: TAction;
    acAVid: TAction;
    acAMakers: TAction;
    acBrands: TAction;
    acCountry: TAction;
    acClients: TAction;
    acDocsIn: TAction;
    acLabels: TAction;
    acChecks: TAction;
    acDocsOut: TAction;
    acCashLoadFull: TAction;
    acBuffPrice: TAction;
    acScales: TAction;
    acScaleLoad: TAction;
    acDocsInv: TAction;
    acCashClose: TAction;
    acCashRep: TAction;
    acCashDep: TAction;
    acTestMove: TAction;
    acDocsAC: TAction;
    acTO: TAction;
    acVnDoc: TAction;
    acRepMoveCa: TAction;
    acRecalcSS: TAction;
    acTestPartIn: TAction;
    acCreatePartIn: TAction;
    acRepPrib: TAction;
    acBeginPartIn: TAction;
    acRepPost: TAction;
    acRepReal: TAction;
    acRemnsDay: TAction;
    acLog: TAction;
    acRepRemnS: TAction;
    acEkonomRep: TAction;
    acRepPostDay: TAction;
    acRepSaleGr: TAction;
    acRepRealAP: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acShopsExecute(Sender: TObject);
    procedure acMHExecute(Sender: TObject);
    procedure acCardsExecute(Sender: TObject);
    procedure acAVidExecute(Sender: TObject);
    procedure acAMakersExecute(Sender: TObject);
    procedure acBrandsExecute(Sender: TObject);
    procedure acCountryExecute(Sender: TObject);
    procedure Label3DblClick(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acDocsInExecute(Sender: TObject);
    procedure acLabelsExecute(Sender: TObject);
    procedure acChecksExecute(Sender: TObject);
    procedure acDocsOutExecute(Sender: TObject);
    procedure acCashLoadFullExecute(Sender: TObject);
    procedure acBuffPriceExecute(Sender: TObject);
    procedure acScalesExecute(Sender: TObject);
    procedure acScaleLoadExecute(Sender: TObject);
    procedure acDocsInvExecute(Sender: TObject);
    procedure acCashCloseExecute(Sender: TObject);
    procedure acCashRepExecute(Sender: TObject);
    procedure acTestMoveExecute(Sender: TObject);
    procedure acDocsACExecute(Sender: TObject);
    procedure acTOExecute(Sender: TObject);
    procedure acVnDocExecute(Sender: TObject);
    procedure acRepMoveCaExecute(Sender: TObject);
    procedure acRecalcSSExecute(Sender: TObject);
    procedure acTestPartInExecute(Sender: TObject);
    procedure acCreatePartInExecute(Sender: TObject);
    procedure acRepPribExecute(Sender: TObject);
    procedure acBeginPartInExecute(Sender: TObject);
    procedure acRepPostExecute(Sender: TObject);
    procedure acRepRealExecute(Sender: TObject);
    procedure acRemnsDayExecute(Sender: TObject);
    procedure acLogExecute(Sender: TObject);
    procedure acRepRemnSExecute(Sender: TObject);
    procedure acEkonomRepExecute(Sender: TObject);
    procedure acRepPostDayExecute(Sender: TObject);
    procedure acRepSaleGrExecute(Sender: TObject);
    procedure acRepRealAPExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainMC: TfmMainMC;


implementation

uses Un1, Dm, Passw, uTermElisey, Shops, Departs, Cards, ClassAlg, Makers,
  Brands, Country, DepSel, Clients, DocHIn, Labels, Period1, Checks,
  DocHOut, Status, PXDB, MFB, u2fdk, DocSIn, BufPrice, ScalesList, Scales,
  DocHInv, DocSInv, DocSOut, DocAcH, DocAcS, CashReps, TORep, DocVnH,
  DocVnS, Period2, Rep1, MessFullCashLoad, PeriodSingleDate, RepPrib,
  Period3, RepPostRemn, RepReal, RemnsDay, RepLog, RepRemnS, Period3Short,
  RepEconom, RepPostRemnDay, RepSaleGr, RepSaleAP, PeriodSklList;

{$R *.dfm}

procedure TfmMainMC.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=131;
end;

procedure TfmMainMC.FormCreate(Sender: TObject);
begin
  CommonSet.Ip:=prGetIP;
  CurDir := ExtractFilePath(ParamStr(0));
  Person.WinName:=GetCurrentUserName;
 
  ReadIni;
  CommonSet.DateBeg:=Date-10;
  CommonSet.DateEnd:=Date;

  CurVal.IdShop:=1;
  CurVal.IdDep:=1;

//  StatusBar1.Panels[1].Text:='������ '+sVer;

  try
    if DirectoryExists(CurDir+'Arh')=False then createdir(CurDir+'Arh');
    if DirectoryExists(CurDir+'Tmp')=False then createdir(CurDir+'Tmp');
//    if DirectoryExists(CurDir+DelP(CommonSet.Ip))=False then createdir(CurDir+DelP(CommonSet.Ip));
//    if DirectoryExists(CurDir+DelP(CommonSet.Ip)+'\'+Person.WinName)=False then createdir(CurDir+DelP(CommonSet.Ip)+'\'+Person.WinName);
  except
  end;

  CommonSet.TmpPath:=CurDir+'Tmp\';
end;

procedure TfmMainMC.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMainMC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Writeini;
end;

procedure TfmMainMC.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainMC.FormShow(Sender: TObject);
begin
  with dmMCS do
  begin
    if msConnection.Connected then
    begin
      Person.Modul:='mc';
      fmPerA:=tfmPerA.Create(Application);
      fmPerA.ShowModal;
      if fmPerA.ModalResult=mrOk then
      begin
        fmPerA.Release;
        Caption:=Caption+'   : '+Person.Name;
      end
      else
      begin
        fmPerA.Release;
        delay(100);
        close;
        delay(100);
      end;

      quDeps.Active:=False;
      quDeps.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDeps.Active:=True;
      if quDeps.RecordCount>0 then
      begin
        quDeps.First;
        Label3.Caption:=Copy(quDepsFULLNAME.AsString,1,50);
        Label3.Tag:=quDepsID.AsInteger;
        CurDep:=Label3.Tag;
      end;
      quDeps.Active:=False;

{
      try
        sGridIni:=CurDir+DelP(CommonSet.Ip)+'\'+Person.Name+'\'+GridIni;
        if DirectoryExists(CurDir+DelP(CommonSet.Ip))=False then createdir(CurDir+DelP(CommonSet.Ip));
        if DirectoryExists(CurDir+DelP(CommonSet.Ip)+'\'+Person.Name)=False then createdir(CurDir+DelP(CommonSet.Ip)+'\'+Person.Name);
      except
      end;
}
      try
        sGridIni:=CurDir+Person.Name+'\'+GridIni;
        sFormIni:=CurDir+Person.Name+'\'+FormIni;

        if DirectoryExists(CurDir+Person.Name)=False then createdir(CurDir+Person.Name);
      except
      end;

      bRefreshCard:=False;
      fmCards:=TFmCards.Create(Application);
      try
        if fmCards.CardsTree.Items.Count>0 then fmCards.CardsTree.Items[0].Selected:=True;
      except
      end;
      bRefreshCard:=True;

      bRefreshCli:=False;
      fmClients:=TfmClients.Create(Application);
      try
        if fmClients.CliTree.Items.Count>0 then fmClients.CliTree.Items[0].Selected:=True;
      except
      end;
      bRefreshCli:=True;

      fmDocsInH:=TfmDocsInH.Create(Application);
      fmDocsInS:=TfmDocsInS.Create(Application);

      fmDocsInvH:=TfmDocsInvH.Create(Application);
      fmDocsInvS:=TfmDocsInvS.Create(Application);

      fmDocsOutH:=TfmDocsOutH.Create(Application);
      fmDocSOut:=TfmDocSOut.Create(Application);

      fmDocsAcH:=TfmDocsAcH.Create(Application);
      fmDocsAcS:=TfmDocsAcS.Create(Application);

      fmDocsVnH:=TfmDocsVnH.Create(Application);
      fmDocsVnS:=TfmDocsVnS.Create(Application);

      fmChecks:=TfmChecks.Create(Application);

      fmCashRep:=TfmCashRep.Create(Application);

      fmTO:=tfmTO.Create(Application);

      fmRep1:=TfmRep1.Create(Application);

      fmPerSklList:=tfmPerSklList.Create(Application);

      quLabs.Active:=False;
      quLabs.Active:=True;
      quLabs.First;

      cxLookupComboBox1.EditValue:=quLabsID.AsInteger;

      cxDEdit1.Date:=Date;

      //������� ������� ������ ��� �����
      try
        quF.Active:=False;
        quF.SQL.Clear;
        quF.SQL.Add('SELECT TOP 1000 [DEPART],[ID],[IDAI],[NAME],[SCALETYPE],[IP1],[IP2],[IP3],[IP4],[IP5]  FROM [MCRYSTAL].[dbo].[SCALES]');
        quF.SQL.Add('where [SCALETYPE]=''CAS_TCP''');
        quF.Active:=True;
        quF.First;
        if quF.RecordCount>0 then
        begin
          if IsOLEObjectInstalled('LP16DLLCOM.lp16') then       //CAScentre_DLL_printScale.Scale
          begin
            DrvScaleCas := CreateOleObject('LP16DLLCOM.lp16');
            ScaleCas:=True;
          end;
        end;
        quF.Active:=False;
      except
      end;
    end;
  end;
end;

procedure TfmMainMC.acShopsExecute(Sender: TObject);
begin
  fmShops:=tfmShops.Create(Application);
  fmShops.quShops.Active:=False;
  fmShops.quShops.Active:=True;
  fmShops.ShowModal;
  fmShops.Release;
end;

procedure TfmMainMC.acMHExecute(Sender: TObject);
begin
  fmDeparts:=tfmDeparts.Create(Application);
  fmDeparts.quDeps.Active:=False;
  fmDeparts.quDeps.Active:=True;
  fmDeparts.ShowModal;
  fmDeparts.Release;
end;

procedure TfmMainMC.acCardsExecute(Sender: TObject);
begin
  fmCards.Show;
end;

procedure TfmMainMC.acAVidExecute(Sender: TObject);
begin
  //������������� ����� ���������
  try
    fmAlgClass:=tfmAlgClass.Create(Application);
    fmAlgClass.quAlgClass.Active:=False; fmAlgClass.quAlgClass.Active:=True;
    fmAlgClass.ShowModal;
  finally
    fmAlgClass.quAlgClass.Active:=False;
    fmAlgClass.Release;
  end;
end;

procedure TfmMainMC.acAMakersExecute(Sender: TObject);
begin
  fmMakers.ViewMakers.BeginUpdate;
  fmMakers.quMakers.Active:=False;
  fmMakers.quMakers.Active:=True;
  fmMakers.ViewMakers.EndUpdate;

  fmMakers.Show;
end;

procedure TfmMainMC.acBrandsExecute(Sender: TObject);
begin
  fmBrands.ViBrands.BeginUpdate;
  fmBrands.quBrands.Active:=False;
  fmBrands.quBrands.Active:=True;
  fmBrands.ViBrands.EndUpdate;

  fmBrands.Show;

end;

procedure TfmMainMC.acCountryExecute(Sender: TObject);
begin
  try
    fmCountry:=tfmCountry.Create(Application);
    fmCountry.ShowModal;
  finally
    fmCountry.Release;
  end;
end;

procedure TfmMainMC.Label3DblClick(Sender: TObject);
Var sNameDep:String;
    iCurDep:Integer;
begin
  //����� ������
  with dmMCS do
  begin
    with fmDepSel do
    begin
      iCurDep:=Label3.Tag;
      closete(teDeps);
      quDeps.Active:=False;
      quDeps.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDeps.Active:=True;

      quDeps.First;
      while not quDeps.Eof do
      begin
        sNameDep:=Trim(quDepsFullName.AsString);
        if sNameDep>'' then
        begin
          teDeps.Append;
          teDepsIdS.AsInteger:=quDepsIDS.AsInteger;
          teDepsID.AsInteger:=quDepsID.AsInteger;
          teDepsName.AsString:=quDepsName.AsString;
          teDepsNameFull.AsString:=quDepsFullName.AsString;
          teDeps.Post;
        end;
        quDeps.Next;
      end;
      quDeps.Active:=False;
      teDeps.Locate('Id',iCurDep,[]);
    end;
    fmDepSel.ShowModal;
    if fmDepSel.ModalResult=mrOk then
    begin
      Label3.Caption:=Copy(fmDepSel.teDepsNameFull.AsString,1,50);
      Label3.Tag:=fmDepSel.teDepsID.AsInteger;
      CurDep:=Label3.Tag;
      if iCurDep<>Label3.Tag then
        if fmCards.Showing then
        begin
          try
            fmCards.CardsView.BeginUpdate;
            prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),fmMainMC.Label3.tag,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
          finally
            fmCards.CardsView.EndUpdate;
          end;
        end;
    end;
  end;
end;

procedure TfmMainMC.SpeedItem5Click(Sender: TObject);
begin
  fmClients.Show;
end;

procedure TfmMainMC.acDocsInExecute(Sender: TObject);
begin
// ������
  with dmMCS do
  begin
    fmDocsInH.LevelDocsIn.Visible:=True;
    fmDocsInH.LevelCards.Visible:=False;

    fmDocsInH.ViewDocsIn.BeginUpdate;
    try
      quTTNIn.Active:=False;
      quTTNIn.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quTTNIn.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quTTNIn.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
      quTTNIn.Active:=True;
    finally
      fmDocsInH.ViewDocsIn.EndUpdate;
    end;
  end;
  fmDocsInH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocsInH.Show;
end;

procedure TfmMainMC.acLabelsExecute(Sender: TObject);
begin
  //�������
  if not CanDo('prEditLabels') then begin Showmessage('��� ����.'); exit; end;
  fmLabels:=tfmLabels.Create(application);
  fmLabels.quLabels.Active:=True;
  fmLabels.ShowModal;
  fmLabels.quLabels.Active:=False;
  fmLabels.Release;
end;

procedure TfmMainMC.acChecksExecute(Sender: TObject);
begin
  // ������� ����
  if not CanDo('prViewCheck') then begin Showmessage('��� ����.'); exit; end;

  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    fmChecks.Memo1.Clear;
    fmChecks.Memo1.Lines.Add('����� ���� ������������ ������.'); delay(10);


    fmChecks.Show;

    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      try
        fmChecks.ViewCh.BeginUpdate;
        dsquChecks.DataSet:=nil;

        quChecks.Active:=False;
        quChecks.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.Tag;
        quChecks.Parameters.ParamByName('IDATEB').Value:=RoundEx(CommonSet.DateBeg);
        quChecks.Parameters.ParamByName('IDATEE').Value:=RoundEx(CommonSet.DateEnd);
        quChecks.Active:=True;

        quChecks.Active:=True;
        dsquChecks.DataSet:=quChecks;
      finally
        fmChecks.ViewCh.EndUpdate;
      end;

      fmChecks.Memo1.Lines.Add('������� ��������.'); delay(10);
    end;
  end;
end;

procedure TfmMainMC.acDocsOutExecute(Sender: TObject);
begin
// ������
  with dmMCS do
  begin
    fmDocsOutH.LevelDocsOut.Visible:=True;
    fmDocsOutH.LevelCards.Visible:=False;

    fmDocsOutH.ViewDocsOut.BeginUpdate;
    try
      quTTNOut.Active:=False;
      quTTNOut.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quTTNOut.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quTTNOut.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
      quTTNOut.Active:=True;
    finally
      fmDocsOutH.ViewDocsOut.EndUpdate;
    end;
  end;
  fmDocsOutH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocsOutH.Show;
end;

procedure TfmMainMC.acCashLoadFullExecute(Sender: TObject);
Var bOk:Boolean;
    sPath,StrWk,sCards,sBar,sNum,sMessur,sCSz,sName:String;
    iNum:INteger;
    rPr:Real;
    iC,NumPost:INteger;
    fc,fb,fupd:TextFile;
    sPos,sB,sM,sr,sDel,sCashLdd:String;
    iShop:INteger;
    bClear,bStart:Boolean;
    bAlco,bL:Boolean;

begin
  //������ ��������� ����

  sr:=',';

  if not CanDo('prFullCashLoad') then begin Showmessage('��� ����.'); end else
  begin
    if MessageDlg('�� ������������� ������ �������� ������ ��������� ����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      bClear:=False;
      bStart:=False;

      fmMessFullCashLoad:=tfmMessFullCashLoad.Create(Application);
      fmMessFullCashLoad.ShowModal;
      if fmMessFullCashLoad.ModalResult=mrOk then
      begin
        bStart:=True;
        if fmMessFullCashLoad.cxCheckBox1.Checked then bClear:=True;
      end;
      fmMessFullCashLoad.Release;

//      if MessageDlg('����������� ������ ��������� ����...',mtConfirmation, [mbYes, mbNo], 0) = mrYes then

      if bStart then
      begin
        //�������
        with dmMCS do
        begin
          prAddCashLoadHist(0,fmMainMC.Label3.Tag,1,0,0);
          if CommonSet.UKM4=1 then
          begin
            try
              fmSt.cxButton1.Enabled:=False;
              fmSt.Memo1.Clear;
              fmSt.Show;
              fmSt.Memo1.Lines.Add('���� �������� ����..');

              fmSt.Memo1.Lines.Add('  ����� ���� ������������ ������ ..');

              bAlco:=prTestDepAlco(fmMainMC.Label3.Tag);
              if bAlco then fmSt.Memo1.Lines.Add('  �������� �������� ���������.') else fmSt.Memo1.Lines.Add('  �������� �������� ���������.');

              assignfile(fc,CurDir+Person.Name+'\'+'PLUCASH.DAT');
              assignfile(fb,CurDir+Person.Name+'\'+'BAR.DAT');
{
              if FileExists(CurDir+Person.Name+'\'+'PLUCASH.DAT') then Append(fc) else rewrite(fc);
              if FileExists(CurDir+Person.Name+'\'+'BAR.DAT') then Append(fb) else rewrite(fb);
}
              //������������ ������
              rewrite(fc);
              rewrite(fb);

              quFindC5.Active:=False;
              quFindC5.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
              quFindC5.Active:=True;

              iC:=0;
              quFindC5.First;
              while not quFindC5.Eof do
              begin
                iNum:=quFindC5ID.AsInteger;
                rPr:=quFindC5PRICE.AsFloat;

                if rPr>0.001 then
                begin
                  bL:=True;
                  if quFindC5AVID.AsInteger>0 then  //��� ��������
                     if bAlco=False then bL:=False;

                  if bL then
                  begin
                    quBars.Active:=False;
                    quBars.Parameters.ParamByName('IDC').Value:=iNum;
                    quBars.Active:=True;
                    if quFindC5EDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                    sName:=quFindC5FULLNAME.AsString;

                    sPos:='';

                    if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';
                    if (quFindC5ISTATUS.AsInteger<100)and(rPr>0.001) then  sDel:='1' else sDel:='0';
                    sPos:=s1(quFindC5ID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+fAMark(quFindC5AVID.AsInteger)+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+its(fmMainMC.Label3.Tag)+sr+sr+sDel+sr+sr+sr+sr;
                    writeln(fc,AnsiToOemConvert(sPos));
                    Flush(fc);

                    quBars.First;
                    while not quBars.Eof do
                    begin
                      if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                      else sCSz:='QUANTITY';

                      sB:=quBarsBar.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                      writeln(fb,AnsiToOemConvert(sB));
                      Flush(fb);

                      quBars.Next;
                    end;
                    quBars.Active:=False;
                  end;
                end;
//                prAddCashLoadHist(iNum,fmMainMC.Label3.Tag,1,rPr,0); //������� ��� ������ ��������

                quFindC5.Next; inc(iC);

                if iC mod 1000 = 0 then
                begin
                  fmSt.Memo1.Lines.Add('    ���������� - '+INtToStr(iC));
                  delay(100);
                end;
              end;

              closefile(fc);
              closefile(fb);

              //����� ������� - ����� ������� � ���������� �����
              iShop:=fShopDep(fmMainMC.Label3.Tag);

              if DirectoryExists(CommonSet.POSLoad+its(iShop)) then
              begin
                sPath:=CommonSet.POSLoad+its(iShop)+'\';

                if Fileexists(sPath+'PLUCASH.DAT') or Fileexists(sPath+'BAR.DAT') then
                begin
                  fmSt.Memo1.Lines.Add('');
                  fmSt.Memo1.Lines.Add('  ����� ��� �� ���������� ���������� ������...');
                  fmSt.Memo1.Lines.Add('  �������� ����������!!!');
                  fmSt.Memo1.Lines.Add('  ���������� �����...');
                  fmSt.Memo1.Lines.Add('');
                end else
                begin
                  if Fileexists(sPath+CashNon) or Fileexists(sPath+CashLdd) or Fileexists(sPath+CashBsv) then
                  begin
                    fmSt.Memo1.Lines.Add('');
                    fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
                    fmSt.Memo1.Lines.Add('  �������� ����������!!!');
                    fmSt.Memo1.Lines.Add('  ���������� �����...');
                    fmSt.Memo1.Lines.Add('');
//                    bOk:=False;
                  end else
                  begin //��� ����� ���������� �������� �����
                    MoveFile(PChar(CurDir+Person.Name+'\'+'PLUCASH.DAT'),PChar(sPath+'PLUCASH.DAT'));
                    MoveFile(PChar(CurDir+Person.Name+'\'+'BAR.DAT'),PChar(sPath+'BAR.DAT'));
                    if bClear then   assignfile(fupd,sPath+CashCng)
                    else assignfile(fupd,sPath+CashUpd);
                    rewrite(fupd);
                    closefile(fupd);
                  end;
                end;
                fmSt.Memo1.Lines.Add('�������� ��������� ��.');
              end else
              begin
                fmSt.Memo1.Lines.Add('������ �������� . ��� ����� �������� - '+CommonSet.POSLoad+its(iShop));
              end;
            except
              fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
            end;

            fmSt.cxButton1.Enabled:=True;
            fmSt.cxButton1.SetFocus;
          end else
          begin
            try
              fmSt.cxButton1.Enabled:=False;
              fmSt.Memo1.Clear;
              fmSt.Show;
              fmSt.Memo1.Lines.Add('���� �������� ����..');

              fmSt.Memo1.Lines.Add('  ����� ���� ������������ ������ ..');


              if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX (TXT).');

              bOk:=True;

              dmPx.Session1.Active:=False;
              dmPx.Session1.NetFileDir:=CommonSet.NetPath;
              dmPx.Session1.Active:=True;

              //������ �������� �� �������� �������
              StrWk:=FloatToStr(now);
              Delete(StrWk,1,2);
              Delete(StrWk,pos(',',StrWk),1);
              StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

              NumPost:=StrToINtDef(StrWk,0);
              sCards:='_'+IntToHex(NumPost,7);
              sBar:='_'+IntToHex(NumPost+1,7);

//            sCards:=StrWk;
//            sBar:=IntToStr(StrToInt(StrWk)+1);

              //����� �������� ������
              CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
              CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
              CopyFile(PChar(CommonSet.TrfPath+'Bars.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
              CopyFile(PChar(CommonSet.TrfPath+'Bars.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

              try
                with dmPx do
                begin
                  taCard.Active:=False;
                  taCard.DatabaseName:=CommonSet.TmpPath;
                  taCard.TableName:=sCards+'.db';
                  taCard.Active:=True;

                  taBar.Active:=False;
                  taBar.DatabaseName:=CommonSet.TmpPath;
                  taBar.TableName:=sBar+'.db';
                  taBar.Active:=True;

                  quFindC5.Active:=False;
                  quFindC5.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
                  quFindC5.Active:=True;

                  iC:=0;
                  quFindC5.First;
                  while not quFindC5.Eof do
                  begin
                    iNum:=quFindC5ID.AsInteger;
                    rPr:=quFindC5PRICE.AsFloat;

                    quBars.Active:=False;
                    quBars.Parameters.ParamByName('IDC').Value:=iNum;
                    quBars.Active:=True;

//                  prAddCashLoad(iNum,1,rPr,0);

                    if quFindC5EDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

//                  sName:=prRetDName(quFindC5ID.AsInteger)+quFindC5FULLNAME.AsString;

                    sName:=quFindC5FULLNAME.AsString;

//                  prAddPosFB(1,quFindC5ID.AsInteger,0,0,quFindC5GoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),1,'','',sName,sMessur,'',0,0,RoundEx(quFindC5Prsision.AsFloat*1000)/1000,rv(rPr));

                    taCard.Append;
                    taCardArticul.AsString:=quFindC5ID.AsString;
                    taCardName.AsString:=AnsiToOemConvert(sName);
                    taCardMesuriment.AsString:=AnsiToOemConvert(sMessur);
                    taCardMesPresision.AsFloat:=RoundEx(quFindC5PRSISION.AsFloat*1000)/1000;
                    taCardAdd1.AsString:='';
                    taCardAdd2.AsString:='';
                    taCardAdd3.AsString:='';
                    taCardAddNum1.AsFloat:=0;
                    taCardAddNum2.AsFloat:=0;
                    taCardAddNum3.AsFloat:=0;
                    taCardScale.AsString:='NOSIZE';
                    taCardGroop1.AsInteger:=0;
                    taCardGroop2.AsInteger:=0;
                    taCardGroop3.AsInteger:=0;
                    taCardGroop4.AsInteger:=0;
                    taCardGroop5.AsInteger:=0;

                    taCardPriceRub.AsFloat:=rv(rPr);

                    taCardPriceCur.AsFloat:=0;
                    taCardClientIndex.AsInteger:=fmMainMC.Label3.Tag; //����� ����� ������ � ����������� �� ������
                    taCardCommentary.AsString:='';

                    if (quFindC5ISTATUS.AsInteger<100)and(rPr>0.001) then taCardDeleted.AsInteger:=1 else taCardDeleted.AsInteger:=0;

                    taCardModDate.AsDateTime:=Trunc(Date);
                    taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
                    taCardModPersonIndex.AsInteger:=0;
                    taCard.Post;

                    quBars.First;
                    while not quBars.Eof do
                    begin
                      try
                        if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                        else sCSz:='QUANTITY';

//                      prAddPosFB(2,quFindC5ID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(rPr));

                        if taBar.Locate('BarCode',quBarsBar.AsString,[])=False then
                        begin
                          taBar.Append;
                          taBarBarCode.AsString:=quBarsBar.AsString;
                          taBarCardArticul.AsString:=quBarsGoodsID.AsString;
                          taBarCardSize.AsString:=sCSz;
                          taBarQuantity.AsFloat:=quBarsQuant.AsFloat;
                          taBar.Post;
                        end;
                      except
                        fmSt.Memo1.Lines.Add('    ������ �� - '+quBarsBar.AsString);
                        taBar.Cancel;
                      end;
                      quBars.Next;
                    end;

                    quBars.Active:=False;
                    quFindC5.Next; inc(iC);

                    if iC mod 1000 = 0 then
                    begin
                      fmSt.Memo1.Lines.Add('    ���������� - '+INtToStr(iC));
                      delay(100);
                    end;
                  end;
                  taCard.Active:=False;
                  taBar.Active:=False;
                end;
              except
                fmSt.Memo1.Lines.Add('������ ������������ ������.');
                fmSt.Memo1.Lines.Add('  �������� ����������!!!');
                bOk:=False;
              end;

              quCash.Active:=False;
              quCash.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
              quCash.Active:=True;
              while not quCash.Eof do
              begin
                if (quCashIACTIVE.AsInteger=1)and(quCashITYPE.asinteger=1)  then
                begin
                  fmSt.Memo1.Lines.Add(' '+quCashName.AsString);
                  sPath:=quCashSPATH.AsString;

                  StrWk:=quCashID.AsString;
                  while length(StrWk)<2 do strwk:='0'+StrWk;
                  StrWk:='cash'+StrWk+'.ldd';
                  sCashLdd:=StrWk;

                  if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
                  begin
                    fmSt.Memo1.Lines.Add('');
                    fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
                    fmSt.Memo1.Lines.Add('  �������� ����������!!!');
                    fmSt.Memo1.Lines.Add('  ���������� �����...');
                    fmSt.Memo1.Lines.Add('');
                    bOk:=False;
                  end else
                  begin //��� ����� ���������� �������� �����
                  //���� �����������
                    try
                      with dmPx do
                      begin
                        sNum:=quCashID.AsString+'\';

                        //���������
                        CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(quCashSPATH.AsString+CashNon),False);
                        Delay(50);

                        CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(quCashSPATH.AsString+sCards+'.db'),False);
                        CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(quCashSPATH.AsString+sCards+'.px'),False);
                        CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(quCashSPATH.AsString+sBar+'.db'),False);
                        CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(quCashSPATH.AsString+sBar+'.px'),False);

                        StrWk:=quCashID.AsString;
                        while length(StrWk)<3 do strwk:='0'+StrWk;
                        StrWk:='cash'+StrWk+'.db';

                        if Fileexists(CommonSet.POSLoad+StrWk)=False then
                        begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                          CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                          MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(quCashSPATH.AsString+StrWk));
                        end;

                        taList.Active:=False;
                        taList.DatabaseName:=CommonSet.TmpPath;
                        taList.TableName:=quCashSPATH.AsString+StrWk;
                        taList.Active:=True;

                        taList.Append;
                        taListTName.AsString:=sCards;
                        taListDataType.AsInteger:=1;
                        taListOperation.AsInteger:=1;
                        taList.Post;

                        taList.Append;
                        taListTName.AsString:=sBar;
                        taListDataType.AsInteger:=2;
                        taListOperation.AsInteger:=1;
                        taList.Post;

                        taList.Active:=False;

                        if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                        delay(50);
                        if FileExists(quCashSPATH.AsString+CashNon) then DeleteFile(quCashSPATH.AsString+CashNon);
                      end; //dmPx
                    except
                      fmSt.Memo1.Lines.Add('  ������ ������ � �������..');
                      bOk:=False;
                    end;
                  end;

                  fmSt.Memo1.Lines.Add(' ��');
                end else
                begin
                  //  fmSt.Memo1.Lines.Add(' '+quCashName.AsString+' �� �������.');
                end;

                quCash.Next;
              end;

              //������ �����

              if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
              if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
              if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
              if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

              if bOk then
              begin
        //��������
        // ��� �� ���� ������, ��� ������
        {
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "Goods" Set Status=1');
        quE.ExecSQL;
         }
                fmSt.Memo1.Lines.Add('�������� ��������� ��.');
              end
              else //����� ������� ������ �� ����
                fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
            finally
              quCash.Active:=False;
              dmPx.Session1.Active:=False;
              fmSt.cxButton1.Enabled:=True;
              fmSt.cxButton1.SetFocus;
            end;
          end;
        end;

      end;
    end;
  end;
end;

procedure TfmMainMC.acBuffPriceExecute(Sender: TObject);
begin
  //����� ��������� ���
  if not CanDo('prViewBufPrice') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    fmBufPrice.ViewBufPr.BeginUpdate;
    quBuffPr.Active:=False;
    quBuffPr.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
    quBuffPr.Parameters.ParamByName('DATEB').Value:=Trunc(Date);
    quBuffPr.Parameters.ParamByName('DATEE').Value:=Trunc(Date);
    quBuffPr.Parameters.ParamByName('IST').Value:=0;
    quBuffPr.Active:=True;
    fmBufPrice.ViewBufPr.EndUpdate;

    fmBufPrice.Show;
  end;
end;

procedure TfmMainMC.acScalesExecute(Sender: TObject);
begin
  //������ �����
  if not CanDo('prEditScaleList') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmMCS do
  begin
    quScaleList.Active:=False;
    quScaleList.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
    quScaleList.Active:=True;

    fmScaleList:=tfmScaleList.Create(Application);
    fmScaleList.Tag:=fmMainMC.Label3.Tag;
    fmScaleList.ShowModal;
    fmScaleList.Release;
  end;
end;

procedure TfmMainMC.acScaleLoadExecute(Sender: TObject);
begin
  //�������� �����
  if not CanDo('prScaleLoad') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmMCS do
  begin
    quScaleList.Active:=False;
    quScaleList.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
    quScaleList.Active:=True;
    quScaleList.First;

    fmScale.Show;
  end;
end;

procedure TfmMainMC.acDocsInvExecute(Sender: TObject);
begin
  //��������������
  with dmMCS do
  begin
    fmDocsInvH.LevelDocsInv.Visible:=True;
    fmDocsInvH.ViewDocsInv.BeginUpdate;
    try
      quTTNInv.Active:=False;
      quTTNInv.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quTTNInv.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quTTNInv.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
      quTTNInv.Active:=True;
    finally
      fmDocsInvH.ViewDocsInv.EndUpdate;
    end;
  end;
  fmDocsInvH.Caption:='��������� �������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocsInvH.Show;
end;

procedure TfmMainMC.acCashCloseExecute(Sender: TObject);
Var iDateB,iDateE,iShop,iCurD:Integer;
    sPath:String;
    NumC:LongInt;
    IdCh:LongInt;
    ChTime,ChTimePre:TDateTime;
    bReadRec:Boolean;
    k:INteger;
    StrWk:String;
    rTimeShift:TDateTime;
    iC:INteger;
    iCurSkl:INteger;
    iCash:Integer;
begin
  //������ ������
  if not CanDo('prCashClose') then begin Showmessage('��� ����.'); end else
  begin
    if MessageDlg('�������� ������ ������ �� '+fmMainMC.Label3.Caption+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fmPeriod1.cxDateEdit1.Date:=Trunc(Date-1);
      fmPeriod1.cxDateEdit2.Date:=Trunc(Date-1);
      fmPeriod1.Label3.Caption:='';
      fmPeriod1.ShowModal;
      if fmPeriod1.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
        iShop:=fShopDep(fmMainMC.Label3.Tag);
        if CommonSet.UKM4=1 then
        begin
          try
            fmSt.cxButton1.Enabled:=False;
            fmSt.Memo1.Clear;
            fmSt.Show;
            fmSt.Memo1.Lines.Add('���� ������ ������ ����..');  delay(10);
            if iDateB<prOpenDate(fmMainMC.Label3.Tag) then
            begin
              fmSt.Memo1.Lines.Add('������ ������.'); delay(10);
            end else
            begin
              dmPx.Session1.Active:=False;
              dmPx.Session1.NetFileDir:=CommonSet.NetPath;
              dmPx.Session1.Active:=True;

              sPath:=CommonSet.POSReport+its(iShop)+'\';

              if FileExists(sPath+'CASHSAIL.DB') then
              begin
                with dmPx do
                with dmMCS do
                begin
                  taCs.Active:=False;
                  taCs.DatabaseName:=sPath; //M:\POS\1\OUTPUT
                  taCs.Active:=True;

{                  taDc.Active:=False;
                  taDc.DatabaseName:=sPath;  //M:\POS\1\OUTPUT
                  taDc.Active:=True;

                  taDCRD.Active:=False;
                  taDCRD.DatabaseName:=sPath;  //M:\POS\1\OUTPUT
                  taDCRD.Active:=True;}

                  fmSt.Memo1.Lines.Add('  - ����� ������ �������'); delay(10);

                  for iCurD:=iDateB to iDateE do   //����� ��� �� ��������� ��� ������������ - ������ ����
                  begin
                    fmSt.Memo1.Lines.Add('  ������������ ���� '+ds1(iCurD)+' ('+its(iCurD)+')'); delay(10);

                    prWrLog(fmMainMC.Label3.Tag,7,1,iCurD,' ������ ������ �� '+ds1(iCurD));

                    fmSt.Memo1.Lines.Add('    - ������ ��������� ������'); delay(10);

                    quD.Active:=False;
                    quD.SQL.Clear;
                    quD.SQL.Add('DELETE FROM [dbo].[cqHeads]');
                    quD.SQL.Add('WHERE [Id_Depart] in (SELECT [ID] from dbo.[DEPARTS] where [IDS]='+its(iShop)+' and [ISTATUS]=1)');
                    quD.SQL.Add('and [IDate]='+its(iCurD));
                    quD.ExecSQL;
                    delay(100);

                    fmSt.Memo1.Lines.Add('    - ������  ��'); delay(10);

                    fmSt.Memo1.Lines.Add('    - ��������� ..');

                    NumC:=-1;  //������� ����� ����
                    IdCh:=0;
                    iCurSkl:=-1;
                    iCash:=-1;

                    taCs.First;
                    if taCS.Locate('Date',iCurD,[]) then
                    begin
                      iC:=0;  ChTime:=0;
                      bReadRec:=True;
                      while bReadRec and (taCS.Eof=False) do
                      begin
                        k:=1;
                        if taCSReplace.AsInteger=0 then k:=-1;

                        StrWk:=taCSTime.AsString;
                        while Length(StrWk)<4 do StrWk:='0'+StrWk;
                        StrWk:=Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);

                        ChTimePre:=ChTime;
                        ChTime:=Trunc(taCSDate.AsDateTime)+StrToTime(StrWk); //����� ���� � ����������� ��������� ���������

                        if CommonSet.h12=1 then rTimeShift:=0 else rTimeShift:=0.0833333;

                        if (ChTime>=(iCurD+rTimeShift)) and (ChTime<(iCurD+1+rTimeShift)) then //� 2-�� ���� �� 2-�� ���� ���������� ���
                        begin
                          if (taCSCheckNumber.AsInteger>NumC)or(taCSUsingIndex.AsInteger<>iCurSkl)or(taCSCashNumber.AsInteger<>iCash) then
                          begin
                            //���� ����������� ���������
                            IdCh:=0;

                            if taCSUsingIndex.AsInteger<>iCurSkl then //��������� ����� - ����� ��������� �� ��������� , ���� ����� ���� ��������� �� ��������� �����
                            begin
                              quSel.Active:=False;
                              quSel.SQL.Clear;
                              quSel.SQL.Add('Select [Id],[Id_Depart],[IDate],[Operation],[DateOperation],[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[PaymentType],[sNum] from [dbo].[cqHeads]');
                              quSel.SQL.Add('where [Id_Depart]='+its(taCSUsingIndex.AsInteger));
                              quSel.SQL.Add('and [IDate]='+its(iCurD));
                              quSel.SQL.Add('and [Cash_Code]='+its(taCSCashNumber.AsInteger));
                              quSel.SQL.Add('and [NSmena]='+its(taCSZNumber.AsInteger));
                              quSel.SQL.Add('and [Ck_Number]='+its(taCSCheckNumber.AsInteger));
                              quSel.Active:=True;
                              if quSel.RecordCount>0 then  IdCh:=quSel.Fields.FieldByName('Id').Value; //��������� ����
                              quSel.Active:=False;
                            end;

                            if IdCh=0 then //��������� ��� - ��������
                            begin
                              IdCh:=fGetID(10);
                              quA.Active:=False;  //��������� ���������
                              quA.SQL.Clear;
                              quA.SQL.Add('INSERT INTO [dbo].[cqHeads]');
                              quA.SQL.Add('([Id],[Id_Depart],[IDate],[Operation],[DateOperation],[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[PaymentType],[sNum])');
                              quA.SQL.Add('VALUES');
                              quA.SQL.Add('(');
                              quA.SQL.Add(its(IdCh));
                              quA.SQL.Add(','+its(taCSUsingIndex.AsInteger));
                              quA.SQL.Add(','+its(iCurD));
                              quA.SQL.Add(','+its(taCSOperation.AsInteger));
                              quA.SQL.Add(','''+dst(ChTime)+'''');
                              quA.SQL.Add(','+its(taCSCheckNumber.AsInteger));
                              quA.SQL.Add(','+its(taCSCasher.AsInteger));
                              quA.SQL.Add(','+its(taCSCashNumber.AsInteger));
                              quA.SQL.Add(','+its(taCSZNumber.AsInteger));
                              quA.SQL.Add(',''''');   //CardNumber
                              if (taCSOperation.AsInteger=5)or(taCSOperation.AsInteger=4) then quA.SQL.Add(',1')
                              else quA.SQL.Add(',0');  //PaymentType
                              quA.SQL.Add(',''''');   //sNum
                              quA.SQL.Add(')');
                              quA.ExecSQL;
                            end;

                            NumC:=taCSCheckNumber.AsInteger;
                            iCurSkl:=taCSUsingIndex.AsInteger;
                            iCash:=taCSCashNumber.AsInteger;
//                            inc(CountCheck);
                          end;

                          if (abs(taCSQuantity.AsFloat)>0.0001)and(IdCh>0) then
                          begin
                            if taCSCardArticul.AsString>='0' then
                            begin
                              quA.Active:=False;  //��������� ������
                              quA.SQL.Clear;
                              quA.SQL.Add('INSERT INTO [dbo].[cqLines]');
                              quA.SQL.Add('([IdHead],[Num],[Code],[BarCode],[Quant],[Price],[Summa],[ProcessingTime],[DProc],[DSum])');
                              quA.SQL.Add('VALUES');
                              quA.SQL.Add('(');
                              quA.SQL.Add(its(IdCh));                          // IdHead
                              quA.SQL.Add(','+its(taCSID.AsInteger));          // Num
                              quA.SQL.Add(','+taCSCardArticul.AsString);       // Code
                              quA.SQL.Add(',''''');                            // BarCode  - ���
                              quA.SQL.Add(','+fts(taCSQuantity.AsFloat*k));      // Quant
                              quA.SQL.Add(','+fts(taCSPriceRub.AsFloat));      // Price
                              quA.SQL.Add(','+fts(taCSTotalRub.AsFloat*k));      // Summa, real,
                              if taCSID.AsInteger=1 then quA.SQL.Add(','+fts(0))
                              else quA.SQL.Add(','+fts(ChTime-ChTimePre));  // ProcessingTime, real
                              quA.SQL.Add(','+fts(0));   // DProc
                              quA.SQL.Add(','+fts(rv(taCSPriceRub.AsFloat*taCSQuantity.AsFloat)-taCSTotalRub.AsFloat));   // DSum
                              quA.SQL.Add(')');
                              quA.ExecSQL;
                            end else  fmSt.Memo1.Lines.Add('     ���.  '+taCSCardArticul.AsString);
                          end;
                          inc(iC);
                        end; //������� �� ������� ����

                        taCs.Next;
                        if iC mod 100 =0 then begin fmSt.StatusBar1.Panels[0].Text:=its(iC);  Delay(10); end;

                        if taCS.Eof=False then
                        begin
                          StrWk:=taCSTime.AsString;
                          while Length(StrWk)<4 do StrWk:='0'+StrWk;
                          StrWk:=Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);

                          ChTime:=Trunc(taCSDate.AsDateTime)+StrToTime(StrWk); //����� ���� � ����������� ��������� ���������

                          //  if ChTime>=(iCurD+1+rTimeShift) then bReadRec:=False; //������ ������ ������ (� ������ ���������� ���� ����������� �� ����� ������ �������.)
                        end else bReadRec:=False; //������ ������ ������}
                      end;

                      fmSt.Memo1.Lines.Add('     '+ds(iCurD)+' ������� '+its(iC)+' �������.');

                      //������ �� ����� �������  - ������������
                      fmSt.Memo1.Lines.Add('      ����� ... ���� ��������� �����.');
                      quProc.SQL.Clear;
                      quProc.SQL.Add('declare @ISHOP int = '+its(iShop));
                      quProc.SQL.Add('declare @IDATE int = '+its(iCurD));
                      quProc.SQL.Add('EXECUTE [dbo].[prCashProc] @ISHOP,@IDATE');
                      quProc.ExecSQL;
                      fmSt.Memo1.Lines.Add('      ��������� ����� ��.');
                      fmSt.Memo1.Lines.Add('      ����� ... ���� ������������ ������ �� ������.');

                      //����� ����� ������������ ��� ���? ����� ������ ��������

                      //����� �����������

                      fmSt.Memo1.Lines.Add('      ������������ ������ �� ������ ���������.');

                    end else  fmSt.Memo1.Lines.Add('     '+ds(iCurD)+' ������ ���. �������� ��������� �������� � ����.');
                  end;
                end;
              end;
            end;
            fmSt.Memo1.Lines.Add('������� �������� ��');
          finally
            fmSt.cxButton1.Enabled:=True;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMC.acCashRepExecute(Sender: TObject);
Var iDateE,iDateB:INteger;
begin
  //����� �� ������
  if not CanDo('prCashRep') then begin Showmessage('��� ����.'); end else
  with dmMCS do
  begin
    fmPerSklList.cxDateEdit1.Date:=Trunc(Date-1);
    fmPerSklList.cxDateEdit2.Date:=Trunc(Date-1);
    fmPerSklList.ShowModal;
    if fmPerSklList.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmPerSklList.cxDateEdit1.Date);
      iDateE:=Trunc(fmPerSklList.cxDateEdit2.Date);
      CommonSet.DateBeg:=Trunc(fmPerSklList.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPerSklList.cxDateEdit2.Date);

      fmCashRep.Memo1.Clear;
      fmCashRep.Show;
      delay(10);
      fmCashRep.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE);
      fmCashRep.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      fmCashRep.ViewCRep.BeginUpdate;
      try
        quCashRep.Active:=False;
        quCashRep.Parameters.ParamByName('SDEP').Value:=fmPerSklList.cxCheckComboBox1.Text;
        quCashRep.Parameters.ParamByName('IDATEB').Value:=iDateB;
        quCashRep.Parameters.ParamByName('IDATEE').Value:=iDateE;
        quCashRep.Active:=True;
      finally
        fmCashRep.ViewCRep.EndUpdate;
      end;
      fmCashRep.Memo1.Lines.Add('������� ��������.'); delay(10);
    end;


    {
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date-1);
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date-1);
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      fmCashRep.Memo1.Clear;
      fmCashRep.Show;
      delay(10);
      fmCashRep.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE);
      fmCashRep.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      fmCashRep.ViewCRep.BeginUpdate;
      try
        quCashRep.Active:=False;
        quCashRep.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.Tag;
        quCashRep.Parameters.ParamByName('IDATEB').Value:=iDateB;
        quCashRep.Parameters.ParamByName('IDATEE').Value:=iDateE;
        quCashRep.Active:=True;
      finally
        fmCashRep.ViewCRep.EndUpdate;
      end;
      fmCashRep.Memo1.Lines.Add('������� ��������.'); delay(10);
    end;}
  end;
end;

procedure TfmMainMC.acTestMoveExecute(Sender: TObject);
Var iDateB,iDateE,iCurD:Integer;
begin
  //�������� ��������������
  if not CanDo('prTestMove') then begin Showmessage('��� ����.'); end else
  begin
    if MessageDlg('�������� �������� �������������� �� '+fmMainMC.Label3.Caption+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fmPeriod1.cxDateEdit1.Date:=Trunc(Date-1);
      fmPeriod1.cxDateEdit2.Date:=Trunc(Date);
      fmPeriod1.Label3.Caption:='';
      fmPeriod1.ShowModal;
      if fmPeriod1.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
//        iShop:=fShopDep(fmMainMC.Label3.Tag);

        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        fmSt.Memo1.Lines.Add('����� ... ���� �������� �������������� ..');  delay(10);

        prWrLog(fmMainMC.Label3.tag,32,1,0,' ������������ �� ������ � '+ds1(iDateB)+'('+its(iDateB)+') �� '+ds1(iDateE)+'('+its(iDateE)+')');

        for iCurD:=iDateB to iDateE do
        begin
          fmSt.Memo1.Lines.Add('   ��������� ���� - '+ds1(iCurD)); delay(10);
          try
            with dmMCS do
            begin
              quProc.SQL.Clear;
              quProc.SQL.Add('declare @IDATE int = '+its(iCurD));
              quProc.SQL.Add('declare @ISKL int = '+its(fmMainMC.Label3.Tag));
              quProc.SQL.Add('EXECUTE [dbo].[prTestMove] @IDATE,@ISKL');
              delay(10); quProc.ExecSQL; delay(10);

              prWrLog(fmMainMC.Label3.tag,32,1,0,' ������ �� '+ds1(iCurD)+'('+its(iCurD)+')');
            end;
          except
            fmSt.Memo1.Lines.Add('     ������ ���������.');
          end;
        end;
        fmSt.Memo1.Lines.Add('������� �������� ��');
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmMainMC.acDocsACExecute(Sender: TObject);
begin
  //����������
  with dmMCS do
  begin
    fmDocsAcH.LevelDocsAc.Visible:=True;
    fmDocsAcH.ViewDocsAc.BeginUpdate;
    try
      quAcH.Active:=False;
      quAcH.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quAcH.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quAcH.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
      quAcH.Active:=True;
    finally
      fmDocsAcH.ViewDocsAc.EndUpdate;
    end;
  end;
  fmDocsAcH.Caption:='��������� ���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocsAcH.Show;
end;

procedure TfmMainMC.acTOExecute(Sender: TObject);
begin
  //�������� �����
  with dmMCS do
  begin
    fmTo.ViewTo.BeginUpdate;
    try
      quTORep.Active:=False;
      quTORep.SQL.Clear;
      quTORep.SQL.Add('EXECUTE [dbo].[prGetTO] '+its(fmMainMC.Label3.tag)+','+its(Trunc(CommonSet.DateBeg))+','+its(Trunc(CommonSet.DateEnd)));
      quTORep.Active:=True;
    finally
      fmTo.ViewTo.EndUpdate;
    end;
  end;
  fmTo.Caption:='�������� ������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmTo.Show;
end;

procedure TfmMainMC.acVnDocExecute(Sender: TObject);
begin
  //���������� ���������
   with dmMCS do
  begin
    fmDocsVnH.LevelDocsVn.Visible:=True;
    fmDocsVnH.ViewDocsVn.BeginUpdate;
    try
      quVnH.Active:=False;
      quVnH.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
      quVnH.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
      quVnH.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
      quVnH.Active:=True;
    finally
      fmDocsVnH.ViewDocsVn.EndUpdate;
    end;
  end;
  fmDocsVnH.Caption:='���������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocsVnH.Show;
end;

procedure TfmMainMC.acRepMoveCaExecute(Sender: TObject);
Var sPost,sClass, sDeps:String;
begin
  //����� �������� ������
  if not CanDo('prReportMove') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;
    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);

      fmPer1.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;

      quDepsRep.Locate('ID',CurVal.IdDep,[]);

      fmPer1.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer1.ShowModal;
      if fmPer1.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer1.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer1.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer1.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer1.cxLookupComboBox2.EditValue;

        fmRep1.Memo1.Clear;
        fmRep1.Show;
        fmRep1.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sPost:='';
        if (fmPer1.cxCheckBox3.Checked=False)and(fmPer1.cxButtonEdit1.Tag>0) then
        begin  //���� ����������� ����������
          fmRep1.Memo1.Lines.Add('  - ����������� ����������'); delay(10);
          quCliAss.Active:=False;
          quCliAss.Parameters.ParamByName('ICLI').Value:=fmPer1.cxButtonEdit1.Tag;
          quCliAss.Active:=True;
          quCliAss.First;
          while not quCliAss.Eof do
          begin
            sPost:=sPost+','+its(quCliAssIDCARD.AsInteger);
            quCliAss.Next;
          end;
          if sPost>'' then delete(sPost,1,1); //������ ������� ������
          quCliAss.Active:=False;
        end;

        sClass:='';
        if (fmPer1.cxCheckBox4.Checked=False)and(fmPer1.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ����������
          fmRep1.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer1.cxButtonEdit2.Tag)+INtToStr(fmPer1.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer1.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer1.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer1.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer1.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRep1.Caption:='�������� ������ �� ������ � '+fmPer1.cxDateEdit1.Text+' �� '+fmPer1.cxDateEdit2.Text +' ������ ('+sDeps+') ��������� ('+sPost+') ������ ('+sClass+')';

        //��� ����� ���������  - ���� ����������� ������
        fmRep1.Memo1.Lines.Add('  - ��������� ������'); delay(10);

        fmRep1.ViewRep1.BeginUpdate;
        dsquRep1Data.DataSet:=nil;

        try
          quRep1Data.Active:=False;
          quRep1Data.SQL.Clear;
          quRep1Data.SQL.Add('');
          quRep1Data.SQL.Add('Declare @IDATEB int='+its(Trunc(fmPer1.cxDateEdit1.Date)));
          quRep1Data.SQL.Add('Declare @IDATEE int='+its(Trunc(fmPer1.cxDateEdit2.Date)));

          quRep1Data.SQL.Add('select t.IDCLASSIF,t.ID,t.NAME,t.FULLNAME,t.BARCODE,t.EDIZM,t.RNAC,t.rQB,t.rQIn,t.rQOut,t.sClass,sClass2 from');
          quRep1Data.SQL.Add('(SELECT ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC]');
          quRep1Data.SQL.Add('  ,rQB=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in ('+sDeps+') and [ICODE]=ca.[ID] and [IDATE]<@IDATEB),0)');
          quRep1Data.SQL.Add('  ,rQIn=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in ('+sDeps+') and [ICODE]=ca.[ID] and [IDATE]>=@IDATEB and [IDATE]<=@IDATEE and [QUANT]>0),0)');
          quRep1Data.SQL.Add('  ,rQOut=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in ('+sDeps+') and [ICODE]=ca.[ID] and [IDATE]>=@IDATEB and [IDATE]<=@IDATEE and [QUANT]<0),0)');
          quRep1Data.SQL.Add('  ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])');
          quRep1Data.SQL.Add('  ,sClass2=(SElect NAME from [dbo].[CLASSIF] where [ID]=gr2.IGR2)');
          quRep1Data.SQL.Add('  FROM [dbo].[CARDS] ca');
          quRep1Data.SQL.Add('  left join [dbo].[vClassGr2] gr2 on gr2.[ID]=ca.[IDCLASSIF]');
          quRep1Data.SQL.Add('  where [TTOVAR]=0');
          if sPost>'' then quRep1Data.SQL.Add('  and ca.[ID] in ('+sPost+')');
          if sClass>'' then quRep1Data.SQL.Add('  and ca.[IDCLASSIF] in ('+sClass+')');
          quRep1Data.SQL.Add(') t');
          quRep1Data.SQL.Add('where (t.rQB<>0)or(t.rQIn<>0)or(t.rQOut<>0)');
          quRep1Data.SQL.Add('  order by t.IDCLASSIF,t.NAME');

{
Declare @IDATEB int=41867
Declare @IDATEE int=41888

select t.IDCLASSIF,t.ID,t.NAME,t.FULLNAME,t.BARCODE,t.EDIZM,t.RNAC,t.rQB,t.rQIn,t.rQOut,t.sClass
from
 (SELECT ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC]
  ,rQB=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in (8,101) and [ICODE]=ca.[ID] and [IDATE]<@IDATEB),0)
  ,rQIn=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in (8,101) and [ICODE]=ca.[ID] and [IDATE]>=@IDATEB and [IDATE]<=@IDATEE and [QUANT]>0),0)
  ,rQOut=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL] in (8,101) and [ICODE]=ca.[ID] and [IDATE]>=@IDATEB and [IDATE]<=@IDATEE and [QUANT]<0),0)
  ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])
  FROM [dbo].[CARDS] ca
  where [TTOVAR]=0
--  and ca.[ID] in (43,59,203)
  and ca.[IDCLASSIF] in (4,707)) t
  where (t.rQB<>0)or(t.rQIn<>0)or(t.rQOut<>0)
}

          quRep1Data.Active:=True;
        finally
          dsquRep1Data.DataSet:=quRep1Data;
          fmRep1.ViewRep1.EndUpdate;
        end;
        fmRep1.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acRecalcSSExecute(Sender: TObject);
Var iDateB,iDateE,iPointA:Integer;
begin
  //�������� �������������
  if not CanDo('prCalcSS') then begin Showmessage('��� ����.'); end else
  begin
    if MessageDlg('�������� �������� ������������ �� '+fmMainMC.Label3.Caption+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iPointA:=Trunc(Date-1);
      with dmMCS do
      begin
        quSel.Active:=False;
        quSel.SQL.Clear;
        quSel.SQL.Add('SELECT [ISKL],[IDATE] FROM [dbo].[SS_TA] where [ISKL]='+its(fmMainMC.Label3.tag));
        quSel.Active:=True;
        if quSel.RecordCount>0 then  iPointA:=quSel.Fields.FieldByName('IDATE').Value; // ���������� ��������
        quSel.Active:=False;
      end;

      fmPeriod1.cxDateEdit1.Date:=iPointA;
      fmPeriod1.cxDateEdit2.Date:=Trunc(Date);
      fmPeriod1.Label3.Caption:='����� ������������ �� ������� �� '+ds1(iPointA);
      fmPeriod1.ShowModal;
      if fmPeriod1.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        if iDateB<prOpenDate(fmMainMC.Label3.Tag) then
        begin
          fmSt.Memo1.Lines.Add('������ ������.'); delay(10);
        end else
        begin
          fmSt.Memo1.Lines.Add('����� ... ���� ������ ..');  delay(10);

          prRecalcSS(fmMainMC.Label3.tag,iDateB,iDateE,fmSt.Memo1);
          prWrLog(fmMainMC.Label3.tag,31,1,0,' ������������ �� ������ � '+ds1(iDateB)+'('+its(iDateB)+') �� '+ds1(iDateE)+'('+its(iDateE)+')');

          fmSt.Memo1.Lines.Add('������� �������� ��');
        end;  
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmMainMC.acTestPartInExecute(Sender: TObject);
Var iDate:Integer;
    bStart:Boolean;
    iC:INteger;
begin
  //�������� ������������ �������� � ��������� ������� �� ����
  exit; //���� ������ �������

  if not CanDo('prTestPartIn') then begin Showmessage('��� ����.'); end else
  begin
    //�� ���� - ��������� ������� ��� ��� ���

    bStart:=False;

    fmPeriodSingleDate:=tfmPeriodSingleDate.Create(Application);
    fmPeriodSingleDate.cxDateEdit1.Date:=Trunc(Date);
    fmPeriodSingleDate.Label3.Caption:='��������� �������� ���-�� ��������� ������ �� �� '+fmMainMC.Label3.Caption+' ?';
    fmPeriodSingleDate.ShowModal;
    if fmPeriodSingleDate.ModalResult=mrOk then bStart:=True;
    iDate:=Trunc(fmPeriodSingleDate.cxDateEdit1.Date);
    fmPeriodSingleDate.Release;

    if bStart then
    begin
      if iDate<prOpenDate(fmMainMC.Label3.tag) then ShowMessage('������ ����������. ������ ������.')
      else
      begin //������
        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        fmSt.Memo1.Lines.Add(fmt+'����� ... ���� ������������ ������ �������� ..');  delay(10);

        with dmMCS do
        begin
          quCardsMoveList.Active:=False;
          quCardsMoveList.SQL.Clear;
          quCardsMoveList.SQL.Add('SELECT [ICODE] FROM [dbo].[GDSMOVE]');
          quCardsMoveList.SQL.Add('where [ISKL]='+its(fmMainMC.Label3.tag));
          quCardsMoveList.SQL.Add('group by [ICODE]');
          quCardsMoveList.Active:=True;

          fmSt.Memo1.Lines.Add(fmt+'  ������ �� ( '+its(quCardsMoveList.RecordCount)+' ������� )');  delay(10);
          fmSt.Memo1.Lines.Add(fmt+'����� ... ���� ������ ..');  delay(10);

          iC:=0;

          quCardsMoveList.First;
          while not quCardsMoveList.Eof do
          begin
            //�������

            quProc.SQL.Clear;
            quProc.SQL.Add('DECLARE @ISKL int = '+its(fmMainMC.Label3.tag));
            quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
            quProc.SQL.Add('DECLARE @IDCARD int = '+its(quCardsMoveListICODE.AsInteger));
            quProc.SQL.Add('EXECUTE [dbo].[prTestPartIn] @ISKL,@IDATE,@IDCARD');
            try
              quProc.ExecSQL;
            except
              fmSt.Memo1.Lines.Add(fmt+'    ������');
              break;
            end;

            quCardsMoveList.Next;
            inc(iC);
            if iC mod 1000 = 0 then
            begin
              fmSt.Memo1.Lines.Add(fmt+'    - ���������� '+its(iC)); delay(10);
            end;
          end;
          fmSt.Memo1.Lines.Add(fmt+'    - ���������� '+its(iC)); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'������� �������� ��');

          quCardsMoveList.Active:=False;
        end;
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmMainMC.acCreatePartInExecute(Sender: TObject);
Var iDateB,iDateE,iPointA:Integer;
begin
  //�������� �������������
  if not CanDo('prCalcSS') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  begin
    if MessageDlg('�������� �������� ��������� ������ �� '+fmMainMC.Label3.Caption+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iPointA:=Trunc(Date-1);
      with dmMCS do
      begin
        quSel.Active:=False;
        quSel.SQL.Clear;
        quSel.SQL.Add('SELECT [ISKL],[IDATE] FROM [dbo].[SS_TA] where [ISKL]='+its(fmMainMC.Label3.tag));
        quSel.Active:=True;
        if quSel.RecordCount>0 then  iPointA:=quSel.Fields.FieldByName('IDATE').Value; //���������� ��������
        quSel.Active:=False;
      end;

      fmPeriod1.cxDateEdit1.Date:=iPointA;
      fmPeriod1.cxDateEdit2.Date:=Trunc(Date);
      fmPeriod1.Label3.Caption:='����� ������������ �� ������� �� '+ds1(iPointA);
      fmPeriod1.ShowModal;
      if fmPeriod1.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        if iDateB<prOpenDate(fmMainMC.Label3.Tag) then
        begin
          fmSt.Memo1.Lines.Add('������ ������.'); delay(10);
        end else
        begin
          fmSt.Memo1.Lines.Add('����� ... ���� ������ ..');  delay(10);

          prRecalcPartIn(fmMainMC.Label3.tag,iDateB,iDateE,fmSt.Memo1);

          fmSt.Memo1.Lines.Add('������� �������� ��');
        end;
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmMainMC.acRepPribExecute(Sender: TObject);
Var sPost,sClass, sDeps:String;
    StrMH,sMH,sDepName:string;
    iDateB,iDateE:Integer;
    iSh:Integer;
begin
  //����� � �������
  if not CanDo('prReportPrib') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;

    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);

      fmPer1.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;

      quDepsRep.Locate('ID',CurVal.IdDep,[]);

      fmPer1.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer1.ShowModal;
      if fmPer1.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer1.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer1.cxDateEdit2.Date);

        iDateB:=Trunc(fmPer1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPer1.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer1.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer1.cxLookupComboBox2.EditValue;

        fmRepPrib.Memo1.Clear;
        fmRepPrib.Show;
        fmRepPrib.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sPost:='';
        if (fmPer1.cxCheckBox3.Checked=False)and(fmPer1.cxButtonEdit1.Tag>0) then
        begin  //���� ����������� ����������
          fmRepPrib.Memo1.Lines.Add('  - ����������� ����������'); delay(10);
          quCliAss.Active:=False;
          quCliAss.Parameters.ParamByName('ICLI').Value:=fmPer1.cxButtonEdit1.Tag;
          quCliAss.Active:=True;
          quCliAss.First;
          while not quCliAss.Eof do
          begin
            sPost:=sPost+','+its(quCliAssIDCARD.AsInteger);
            quCliAss.Next;
          end;
          if sPost>'' then delete(sPost,1,1); //������ ������� ������
          quCliAss.Active:=False;
        end;

        sClass:='';
        if (fmPer1.cxCheckBox4.Checked=False)and(fmPer1.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ����������
          fmRepPrib.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer1.cxButtonEdit2.Tag)+INtToStr(fmPer1.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer1.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer1.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer1.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer1.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepPrib.Caption:='�������� ������ �� ������ � '+fmPer1.cxDateEdit1.Text+' �� '+fmPer1.cxDateEdit2.Text +' ������ ('+sDeps+') ��������� ('+sPost+') ������ ('+sClass+')';

        fmRepPrib.ViewRepPrib.BeginUpdate;
        dsquRepPribData.DataSet:=nil;

        try
          StrMH:=sDeps;
          CloseTe(teRP);

          while StrMH>'' do
          begin
            while StrMH[1]=',' do Delete(StrMH,1,1);
            if Pos(',',StrMH)>0 then
            begin
              sMH:=Copy(StrMH,1,Pos(',',StrMH)-1);
              Delete(StrMH,1,Pos(',',StrMH)-1);
            end else
            begin
              sMH:=StrMH;
              StrMH:=''
            end;

            sDepName:=fFullName(StrToIntDef(sMH,0));
            iSh:=fShopDep(StrToIntDef(sMH,0));
            //��� ����� ���������  - ���� ����������� ������
            fmRepPrib.Memo1.Lines.Add('  - ��������� ������ �� �� '+sDepName+' ('+sMH+')'); delay(10);

            quRepPribData1.Active:=False;
            quRepPribData1.SQL.Clear;
            quRepPribData1.SQL.Add('');

            quRepPribData1.SQL.Add('declare @IDATEB int = '+its(iDateB-1));
            quRepPribData1.SQL.Add('declare @IDATE0 int        ');
            quRepPribData1.SQL.Add('declare @IDSKL int = '+sMH);
            quRepPribData1.SQL.Add('declare @IDATEE int = '+its(iDateE));

            quRepPribData1.SQL.Add('Set @IDATE0=isnull((Select top 1 IDATEDOC from [dbo].[DOCINV_HEAD] where [IDSKL]=@IDSKL and [IDETAIL]=0 and [IDATEDOC]<=@IDATEB order by [IDATEDOC] desc ),0)');

            quRepPribData1.SQL.Add('Select  ca.[ID],ca.[IDCLASSIF],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC] ');
            quRepPribData1.SQL.Add('        ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])             ');
            quRepPribData1.SQL.Add('        ,sClass2=(SElect NAME from [dbo].[CLASSIF] where [ID]=gr2.IGR2)                  ');
            quRepPribData1.SQL.Add('	    	,sClassGr=(SElect NAME from [dbo].[CLASSIF] where [ID]=(SElect [IDPARENT] from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])) ');
            quRepPribData1.SQL.Add('        ,isnull(abcq.AGR,4) as abcq,isnull(abcp.AGR,4) as abcp');
            quRepPribData1.SQL.Add('        ,(isnull(spinv.QUANT,0)+isnull(spin.QUANTIN,0)-isnull(spout.QUANTOUT,0)-isnull(spvn.QUANTVN,0)+isnull(spinv1.QUANT,0)-isnull(spsale.QUANT,0)) as QUANT_B ');
            quRepPribData1.SQL.Add('		,(isnull(spinv.SUMIN0,0)+isnull(spin.SUMIN0,0)-isnull(spout.SUMIN0,0)-isnull(spvn.SUMIN0,0)+isnull(spinv1.SUMIN0,0)-isnull(spsale.SUMIN0,0))as RSUMIN0_B      ');
            quRepPribData1.SQL.Add('		,(isnull(spinv.SUMIN,0)+isnull(spin.SUMIN,0)-isnull(spout.SUMIN,0)-isnull(spvn.SUMIN,0)+isnull(spinv1.SUMIN,0)-isnull(spsale.SUMIN,0))as RSUMIN_B             ');
            quRepPribData1.SQL.Add('		,(isnull(spinv.SUMR,0)+isnull(spin.SUMR,0)-isnull(spout.SUMR,0)-isnull(spvn.SUMR,0)+isnull(spinv1.SUMR,0)-isnull(spsale.SUMR,0))as RSUMR_B                    ');

            quRepPribData1.SQL.Add('		,isnull(spin1.QUANTIN,0) as QUANTIN,isnull(spin1.SUMIN0,0) as SUMIN0,isnull(spin1.SUMIN,0) as SUMIN,isnull(spin1.SUMR,0) as SUMRIN                            ');
            quRepPribData1.SQL.Add('		,isnull(spout1.QUANTOUT,0) as QUANTOUT,isnull(spout1.SUMIN0,0) as SUMIN0OUT,isnull(spout1.SUMIN,0) as SUMINOUT,isnull(spout1.SUMR,0) as SUMROUT               ');
            quRepPribData1.SQL.Add('		,isnull(spvn1.QUANTVN,0) as QUANTOUTVN,isnull(spvn1.SUMIN0,0) as SUMIN0OUTVN,isnull(spvn1.SUMIN,0) as SUMINOUTVN,isnull(spvn1.SUMR,0) as SUMROUTVN            ');
            quRepPribData1.SQL.Add('		,isnull(spinv2.QUANT,0) as QUANTINV,isnull(spinv2.SUMIN0,0) as SUMIN0INV,isnull(spinv2.SUMIN,0) as SUMININV,isnull(spinv2.SUMR,0) as SUMRINV                  ');
            quRepPribData1.SQL.Add('		,isnull(spsale1.QUANT,0) as QUANTREAL,isnull(spsale1.SUMIN0,0) as SUMIN0REAL,isnull(spsale1.SUMIN,0) as SUMINREAL,isnull(spsale1.SUMR,0) as SUMRREAL          ');

            quRepPribData1.SQL.Add('from [dbo].[CARDS] ca ');
            quRepPribData1.SQL.Add('left join [dbo].[vClassGr2] gr2 on gr2.[ID]=ca.[IDCLASSIF]');

            quRepPribData1.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]=@IDSKL');
            quRepPribData1.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]=@IDSKL');

            quRepPribData1.SQL.Add('left join (  -- �������� ������ �� ������ �������������� ');
            quRepPribData1.SQL.Add('		SELECT sp.[IDCARD],SUM(sp.[QUANTF]) as QUANT,SUM(sp.[SUMINF]) as SUMIN,SUM(sp.[SUMIN0F]) as SUMIN0,SUM(sp.[SUMRF]) as SUMR   ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCINV_SPEC] sp                                                                                                ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCINV_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC=@IDATE0 and hd.IACTIVE=3                                                             ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('		) spinv on spinv.[IDCARD]=ca.[ID]                                                                                            ');
            quRepPribData1.SQL.Add('left join ( -- ������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                       ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTIN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMUCH]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCIN_SPEC] sp                                                                                                 ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCIN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spin on spin.[IDCARD]=ca.[ID]                                                                                            ');
            quRepPribData1.SQL.Add('left join ( -- �������� ����������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                          ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTOUT,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR  ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCOUT_SPEC] sp                                                                                                ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCOUT_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spout on spout.[IDCARD]=ca.[ID]                                                                                          ');
            quRepPribData1.SQL.Add('left join ( -- �������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                      ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTVN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR   ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCVN_SPEC] sp                                                                                                 ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCVN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepPribData1.SQL.Add('			where hd.IDSKLFROM=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spvn on spvn.[IDCARD]=ca.[ID]                                                                                            ');
            quRepPribData1.SQL.Add('left join ( -- �������������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANTF]-sp.[QUANTR]) as QUANT,SUM(sp.[SUMINF]-sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0F]-sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMRF]-sp.[SUMR]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCINV_SPEC] sp                                                                                                ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCINV_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spinv1 on spinv1.[IDCARD]=ca.[ID]                                                                                        ');
            quRepPribData1.SQL.Add('left join ( -- ����� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                         ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANT,SUM(sp.[QUANT]*sp.[PRICEIN]) as SUMIN,SUM(sp.[QUANT]*sp.[PRICEIN0]) as SUMIN0,SUM(sp.[QUANT]*sp.[PRICER]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[PARTOUT] sp                                                                                                    ');
            quRepPribData1.SQL.Add('			where sp.IDSKL=@IDSKL and sp.IDATEDOC>@IDATE0 and sp.IDATEDOC<=@IDATEB and sp.ITYPED=7                                     ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spsale on spsale.[IDCARD]=ca.[ID]                                                                                        ');

            quRepPribData1.SQL.Add('left join ( -- ������� �� ������ � >@IDATEB ��  <=@IDATEE                                                                       ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTIN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMUCH]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCIN_SPEC] sp                                                                                                 ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCIN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATEB and hd.IDATEDOC<=@IDATEE and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spin1 on spin1.[IDCARD]=ca.[ID]                                                                                          ');
            quRepPribData1.SQL.Add('left join ( -- �������� ����������� �� ������ �  >@IDATEB ��  <=@IDATEE                                                         ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTOUT,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR  ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCOUT_SPEC] sp                                                                                                ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCOUT_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATEB and hd.IDATEDOC<=@IDATEE and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spout1 on spout1.[IDCARD]=ca.[ID]                                                                                        ');
            quRepPribData1.SQL.Add('left join ( -- �������� �� ������ �  >@IDATEB ��  <=@IDATEE                                                                     ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTVN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR   ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCVN_SPEC] sp                                                                                                 ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCVN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepPribData1.SQL.Add('			where hd.IDSKLFROM=@IDSKL and hd.IDATEDOC>@IDATEB and hd.IDATEDOC<=@IDATEE and hd.IACTIVE=3                                ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spvn1 on spvn1.[IDCARD]=ca.[ID]                                                                                          ');
            quRepPribData1.SQL.Add('left join ( -- �������������� �� ������  >@IDATEB ��  <=@IDATEE                                                                 ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANTF]-sp.[QUANTR]) as QUANT,SUM(sp.[SUMINF]-sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0F]-sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMRF]-sp.[SUMR]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[DOCINV_SPEC] sp                                                                                                ');
            quRepPribData1.SQL.Add('			left join [dbo].[DOCINV_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepPribData1.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATEB and hd.IDATEDOC<=@IDATEE and hd.IACTIVE=3                                    ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spinv2 on spinv2.[IDCARD]=ca.[ID]                                                                                        ');
            quRepPribData1.SQL.Add('left join ( -- ����� �� ������ �  >@IDATEB ��  <=@IDATEE                                                                        ');
            quRepPribData1.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANT,SUM(sp.[QUANT]*sp.[PRICEIN]) as SUMIN,SUM(sp.[QUANT]*sp.[PRICEIN0]) as SUMIN0,SUM(sp.[QUANT]*sp.[PRICER]) as SUMR ');
            quRepPribData1.SQL.Add('			FROM [dbo].[PARTOUT] sp                                                                                                    ');
            quRepPribData1.SQL.Add('			where sp.IDSKL=@IDSKL and sp.IDATEDOC>@IDATEB and sp.IDATEDOC<=@IDATEE and sp.ITYPED=7                                     ');
            quRepPribData1.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepPribData1.SQL.Add('			) spsale1 on spsale1.[IDCARD]=ca.[ID]                                                                                      ');
            quRepPribData1.SQL.Add('where ca.[ID]>0                                                                                                                  ');
            if sPost>'' then quRepPribData1.SQL.Add('  and ca.[ID] in ('+sPost+')');
            if sClass>'' then quRepPribData1.SQL.Add('  and ca.[IDCLASSIF] in ('+sClass+')');
            quRepPribData1.SQL.Add('and (isnull(spinv.QUANT,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spin.QUANTIN,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spout.QUANTOUT,0)<>0                                                                                                 ');
            quRepPribData1.SQL.Add('	or isnull(spvn.QUANTVN,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spinv1.QUANT,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spsale.QUANT,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spin1.QUANTIN,0)<>0                                                                                                  ');
            quRepPribData1.SQL.Add('	or isnull(spout1.QUANTOUT,0)<>0                                                                                                ');
            quRepPribData1.SQL.Add('	or isnull(spvn1.QUANTVN,0)<>0                                                                                                  ');
            quRepPribData1.SQL.Add('	or isnull(spinv2.QUANT,0)<>0                                                                                                   ');
            quRepPribData1.SQL.Add('	or isnull(spsale1.QUANT,0)<>0                                                                                                  ');
            quRepPribData1.SQL.Add('	)                                                                                                                              ');

            quRepPribData1.Active:=True;

            fmRepPrib.Memo1.Lines.Add('  - ���������� ..'); delay(10);
            quRepPribData1.First;
            while not quRepPribData1.Eof do
            begin
              teRP.Append;
              teRPIDCLASSIF.AsInteger:=quRepPribData1IDCLASSIF.AsInteger;
              teRPID.AsInteger:=quRepPribData1ID.AsInteger;
              teRPNAME.AsString:=quRepPribData1NAME.AsString;
              teRPFULLNAME.AsString:=quRepPribData1FULLNAME.AsString;
              teRPBARCODE.AsString:=quRepPribData1BARCODE.AsString;
              teRPEDIZM.AsInteger:=quRepPribData1EDIZM.AsInteger;
              teRPRNAC.AsFloat:=quRepPribData1RNAC.AsFloat;
              teRPsClass.AsString:=quRepPribData1sClass.AsString;
              teRPsClass2.AsString:=quRepPribData1sClass2.AsString;

              teRPABCP.AsInteger:=quRepPribData1abcp.AsInteger;
              teRPABCQ.AsInteger:=quRepPribData1abcq.AsInteger;

              teRPrQBIn.AsFloat:=0;
              teRPrSBIn.AsFloat:=0;
              teRPrSBIn0.AsFloat:=0;
              teRPrSBInR.AsFloat:=0;
              teRPrQBOut.AsFloat:=0;
              teRPrSBOut.AsFloat:=0;
              teRPrSBOut0.AsFloat:=0;
              teRPrSBOutR.AsFloat:=0;

              teRPrQIn.AsFloat:=quRepPribData1QUANTIN.AsFloat;
              teRPrSIn.AsFloat:=quRepPribData1SUMIN.AsFloat;
              teRPrSIn0.AsFloat:=quRepPribData1SUMIN0.AsFloat;
              teRPrSInR.AsFloat:=quRepPribData1SUMRIN.AsFloat;

              teRPrQRetOut.AsFloat:=quRepPribData1QUANTOUT.AsFloat;
              teRPrSRetOut.AsFloat:=quRepPribData1SUMINOUT.AsFloat;
              teRPrSRetOut0.AsFloat:=quRepPribData1SUMIN0OUT.AsFloat;
              teRPrSRetOutR.AsFloat:=quRepPribData1SUMROUT.AsFloat;

              teRPrQSpisOut.AsFloat:=quRepPribData1QUANTOUTVN.AsFloat;
              teRPrSSpisOut.AsFloat:=quRepPribData1SUMINOUTVN.AsFloat;
              teRPrSSpisOut0.AsFloat:=quRepPribData1SUMIN0OUTVN.AsFloat;
              teRPrSSpisOutR.AsFloat:=quRepPribData1SUMROUTVN.AsFloat;

              teRPrQRealOut.AsFloat:=quRepPribData1QUANTREAL.AsFloat;
              teRPrSRealOut.AsFloat:=quRepPribData1SUMINREAL.AsFloat;
              teRPrSRealOut0.AsFloat:=quRepPribData1SUMIN0REAL.AsFloat;
              teRPrSRealOutR.AsFloat:=quRepPribData1SUMRREAL.AsFloat;

              teRPrQInvItog.AsFloat:=quRepPribData1QUANTINV.AsFloat;
              teRPrSInvItog.AsFloat:=quRepPribData1SUMININV.AsFloat;
              teRPrSInv0Itog.AsFloat:=quRepPribData1SUMIN0INV.AsFloat;
              teRPrSInvRItog.AsFloat:=quRepPribData1SUMRINV.AsFloat;

              teRPrQBeg.AsFloat:=quRepPribData1QUANT_B.AsFloat;
              teRPrSBeg.AsFloat:=quRepPribData1RSUMIN_B.AsFloat;
              teRPrSBeg0.AsFloat:=quRepPribData1RSUMIN0_B.AsFloat;
              teRPrSBegR.AsFloat:=quRepPribData1RSUMR_B.AsFloat;

              teRPrQEnd.AsFloat:=quRepPribData1QUANT_B.AsFloat+quRepPribData1QUANTIN.AsFloat-quRepPribData1QUANTOUT.AsFloat-quRepPribData1QUANTOUTVN.AsFloat-quRepPribData1QUANTREAL.AsFloat+quRepPribData1QUANTINV.AsFloat;
              teRPrSEnd.AsFloat:=quRepPribData1RSUMIN_B.AsFloat+quRepPribData1SUMIN.AsFloat-quRepPribData1SUMINOUT.AsFloat-quRepPribData1SUMINOUTVN.AsFloat-quRepPribData1SUMINREAL.AsFloat+quRepPribData1SUMININV.AsFloat;
              teRPrSEnd0.AsFloat:=quRepPribData1RSUMIN0_B.AsFloat+quRepPribData1SUMIN0.AsFloat-quRepPribData1SUMIN0OUT.AsFloat-quRepPribData1SUMIN0OUTVN.AsFloat-quRepPribData1SUMIN0REAL.AsFloat+quRepPribData1SUMIN0INV.AsFloat;
              teRPrSEndR.AsFloat:=quRepPribData1RSUMR_B.AsFloat+quRepPribData1SUMRIN.AsFloat-quRepPribData1SUMROUT.AsFloat-quRepPribData1SUMROUTVN.AsFloat-quRepPribData1SUMRREAL.AsFloat+quRepPribData1SUMRINV.AsFloat;

              teRPrSRealPrib.AsFloat:=quRepPribData1SUMRREAL.AsFloat-quRepPribData1SUMINREAL.AsFloat;

              teRPIDS.AsInteger:=iSh;
              teRPIDDEP.AsInteger:=StrToIntDef(sMH,0);
              teRPNAMEDEP.AsString:=sDepName;

              teRPrQInvIn.AsFloat:=0;
              teRPrSInvIn.AsFloat:=0;
              teRPrSInvIn0.AsFloat:=0;
              teRPrSInvInR.AsFloat:=0;

              teRPrQInvOut.AsFloat:=0;
              teRPrSInvOut.AsFloat:=0;
              teRPrSInvOut0.AsFloat:=0;
              teRPrSInvOutR.AsFloat:=0;

              teRPsClassGr.AsString:=quRepPribData1sClassGr.AsString;
              teRP.Post;

//  quRepPribDatarQEnd.AsFloat:=quRepPribDatarQBIn.AsFloat-quRepPribDatarQBOut.AsFloat+quRepPribDatarQIn.AsFloat-quRepPribDatarQRetOut.AsFloat-quRepPribDatarQSpisOut.AsFloat-quRepPribDatarQRealOut.AsFloat-quRepPribDatarQInvOut.AsFloat+quRepPribDatarQInvIn.AsFloat;
//  quRepPribDatarSEnd.AsFloat:=quRepPribDatarSBIn.AsFloat-quRepPribDatarSBOut.AsFloat+quRepPribDatarSIn.AsFloat-quRepPribDatarSRetOut.AsFloat-quRepPribDatarSSpisOut.AsFloat-quRepPribDatarSRealOut.AsFloat-quRepPribDatarSInvOut.AsFloat+quRepPribDatarSInvIn.AsFloat;
//  quRepPribDatarSEnd0.AsFloat:=quRepPribDatarSBIn0.AsFloat-quRepPribDatarSBOut0.AsFloat+quRepPribDatarSIn0.AsFloat-quRepPribDatarSRetOut0.AsFloat-quRepPribDatarSSpisOut0.AsFloat-quRepPribDatarSRealOut0.AsFloat-quRepPribDatarSInvOut0.AsFloat+quRepPribDatarSInvIn0.AsFloat;
//  quRepPribDatarSEndR.AsFloat:=quRepPribDatarSBInR.AsFloat-quRepPribDatarSBOutR.AsFloat+quRepPribDatarSInR.AsFloat-quRepPribDatarSRetOutR.AsFloat-quRepPribDatarSSpisOutR.AsFloat-quRepPribDatarSRealOutR.AsFloat-quRepPribDatarSInvOutR.AsFloat+quRepPribDatarSInvInR.AsFloat;


              quRepPribData1.Next;
            end;

            quRepPribData1.Active:=False;
            delay(10);
          end;

          {
          quRepPribData.Active:=False;
          quRepPribData.SQL.Clear;
          quRepPribData.SQL.Add('');
          quRepPribData.SQL.Add('Declare @IDATEB int='+its(Trunc(fmPer1.cxDateEdit1.Date)-1));  // -1 ��� ���� ��� ������� �������
          quRepPribData.SQL.Add('Declare @IDATEE int='+its(Trunc(fmPer1.cxDateEdit2.Date)));

//Declare @IDATEB int=41898
//Declare @IDATEE int=41908

          quRepPribData.SQL.Add('SELECT dep.[IDS],dep.[ID] as IDDEP,dep.[NAME] as NAMEDEP,ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC]');
          quRepPribData.SQL.Add('  ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])');
          quRepPribData.SQL.Add('  ,sClassGr=(SElect NAME from [dbo].[CLASSIF] where [ID]=(SElect [IDPARENT] from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF]))');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- ������� �� ������');
          quRepPribData.SQL.Add('  -- +');
          quRepPribData.SQL.Add('  ,rQBIn=isnull((SELECT SUM([QPART]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBIn=isnull((SELECT SUM([QPART]*[PRICEIN]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBIn0=isnull((SELECT SUM([QPART]*[PRICEIN0]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBInR=isnull((SELECT SUM([QPART]*[PRICER]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- - ');
          quRepPribData.SQL.Add('  ,rQBOut=isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBOut=isnull((SELECT SUM([QUANT]*[PRICEIN]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBOut0=isnull((SELECT SUM([QUANT]*[PRICEIN0]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSBOutR=isnull((SELECT SUM([QUANT]*[PRICER]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- ������ ');
          quRepPribData.SQL.Add('  ,rQIn=isnull((SELECT SUM([QPART]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [ITYPED]=1 and [IDCARD]=ca.[ID]),0) ');
          quRepPribData.SQL.Add('  ,rSIn=isnull((SELECT SUM([QPART]*[PRICEIN]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [ITYPED]=1 and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSIn0=isnull((SELECT SUM([QPART]*[PRICEIN0]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [ITYPED]=1 and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('  ,rSInR=isnull((SELECT SUM([QPART]*[PRICER]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [ITYPED]=1 and [IDCARD]=ca.[ID]),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- ������ ������� ');
          quRepPribData.SQL.Add('  ,rQRetOut=isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=2),0)');
          quRepPribData.SQL.Add('  ,rSRetOut=isnull((SELECT SUM([QUANT]*[PRICEIN]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=2),0)');
          quRepPribData.SQL.Add('  ,rSRetOut0=isnull((SELECT SUM([QUANT]*[PRICEIN0]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=2),0)');
          quRepPribData.SQL.Add('  ,rSRetOutR=isnull((SELECT SUM([QUANT]*[PRICER]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=2),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- ������ ��������');
          quRepPribData.SQL.Add('  ,rQSpisOut=isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=3),0)');
          quRepPribData.SQL.Add('  ,rSSpisOut=isnull((SELECT SUM([QUANT]*[PRICEIN]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=3),0)');
          quRepPribData.SQL.Add('  ,rSSpisOut0=isnull((SELECT SUM([QUANT]*[PRICEIN0]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=3),0)');
          quRepPribData.SQL.Add('  ,rSSpisOutR=isnull((SELECT SUM([QUANT]*[PRICER]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=3),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- ���������� ');
          quRepPribData.SQL.Add('  ,rQRealOut=isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=7),0)');
          quRepPribData.SQL.Add('  ,rSRealOut=isnull((SELECT SUM([QUANT]*[PRICEIN]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=7),0)');
          quRepPribData.SQL.Add('  ,rSRealOut0=isnull((SELECT SUM([QUANT]*[PRICEIN0]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=7),0)');
          quRepPribData.SQL.Add('  ,rSRealOutR=isnull((SELECT SUM([QUANT]*[PRICER]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=7),0)');
          quRepPribData.SQL.Add('   ');
          quRepPribData.SQL.Add('  -- �������������� ������');
          quRepPribData.SQL.Add('  ,rQInvOut=isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0)');
          quRepPribData.SQL.Add('  ,rSInvOut=isnull((SELECT SUM([QUANT]*[PRICEIN]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=20),0)');
          quRepPribData.SQL.Add('  ,rSInvOut0=isnull((SELECT SUM([QUANT]*[PRICEIN0]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=20),0)');
          quRepPribData.SQL.Add('  ,rSInvOutR=isnull((SELECT SUM([QUANT]*[PRICER]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]and [ITYPED]=20),0)');

          quRepPribData.SQL.Add('  -- �������������� ������');

          quRepPribData.SQL.Add('  ,rQInvIn=isnull((SELECT SUM([QPART]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0)             ');
          quRepPribData.SQL.Add('  ,rSInvIn=isnull((SELECT SUM([QPART]*[PRICEIN]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0)   ');
          quRepPribData.SQL.Add('  ,rSInvIn0=isnull((SELECT SUM([QPART]*[PRICEIN0]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0) ');
          quRepPribData.SQL.Add('  ,rSInvInR=isnull((SELECT SUM([QPART]*[PRICER]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0)   ');


          quRepPribData.SQL.Add('  FROM dbo.CARDS ca, dbo.DEPARTS dep');
          quRepPribData.SQL.Add('  where dep.[ISTATUS]=1 and dep.[ID] in ('+sDeps+')');
          quRepPribData.SQL.Add('  and ca.[TTOVAR]=0');
          if sPost>'' then quRepPribData.SQL.Add('  and ca.[ID] in ('+sPost+')');
          if sClass>'' then quRepPribData.SQL.Add('  and ca.[IDCLASSIF] in ('+sClass+')');

          quRepPribData.SQL.Add('   and');
          quRepPribData.SQL.Add('  ( abs(isnull((SELECT SUM([QPART]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]<=@IDATEB and [IDCARD]=ca.[ID]),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QPART]) FROM [dbo].[PARTIN] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID]),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=2),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=3),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=7),0))>0.0001');
          quRepPribData.SQL.Add('    or abs(isnull((SELECT SUM([QUANT]) FROM [dbo].[PARTOUT] where [IDSKL]=dep.[ID] and [IDATEDOC]>@IDATEB and [IDATEDOC]<=@IDATEE and [IDCARD]=ca.[ID] and [ITYPED]=20),0))>0.0001');
          quRepPribData.SQL.Add('  )');

          quRepPribData.SQL.Add('  order by dep.[IDS],dep.[ID],ca.[IDCLASSIF],ca.[NAME]');
          quRepPribData.Active:=True;}
        finally
          dsquRepPribData.DataSet:=teRP;
          fmRepPrib.ViewRepPrib.EndUpdate;
        end;
        fmRepPrib.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acBeginPartInExecute(Sender: TObject);
Var iDate:Integer;
    bStart:Boolean;
    iC:INteger;
begin
  //��������� ��������� �������� ��������� ������ �� ����
  exit; //�� ������� ��� ��������

  if not CanDo('prCreatePartInBegin') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  begin
    //�� ���� - ��������� ������� ��� ��� ���

    bStart:=False;

    fmPeriodSingleDate:=tfmPeriodSingleDate.Create(Application);
    fmPeriodSingleDate.cxDateEdit1.Date:=Trunc(Date);
    fmPeriodSingleDate.Label3.Caption:='��������� �������� ���-�� ��������� ������ �� �� '+fmMainMC.Label3.Caption+' ?';
    fmPeriodSingleDate.ShowModal;
    if fmPeriodSingleDate.ModalResult=mrOk then bStart:=True;
    iDate:=Trunc(fmPeriodSingleDate.cxDateEdit1.Date);
    fmPeriodSingleDate.Release;

    if bStart then
    begin
      if iDate<prOpenDate(fmMainMC.Label3.tag) then ShowMessage('������ ����������. ������ ������.')
      else
      begin //������
        fmSt.cxButton1.Enabled:=False;
        fmSt.Memo1.Clear;
        fmSt.Show;
        fmSt.Memo1.Lines.Add(fmt+'�������� �������������� ������ ..');  delay(10);

        with dmMCS do
        begin
          quD.Active:=False;
          quD.SQL.Clear;
          quD.SQL.Add('delete from [dbo].[PARTIN]');   //������� ������ �� ��������� ������
          quD.SQL.Add('where [IDSKL]='+its(fmMainMC.Label3.tag));
          quD.SQL.Add('and [IDATEDOC]<='+its(iDate));
          quD.SQL.Add('and [IDCARD] not in (SELECT [ICODE] FROM [dbo].[GDSMOVE] where [ISKL]='+its(fmMainMC.Label3.tag)+' and [IDATE]<='+its(iDate)+')');
          quD.ExecSQL;

          quD.Active:=False;
          quD.SQL.Clear;
          quD.SQL.Add('delete from [dbo].[PARTOUT]');  //������� ������ �� ��������� ������ �� ������ ������
          quD.SQL.Add('where [IDSKL]='+its(fmMainMC.Label3.tag));
          quD.SQL.Add('and [IDATEDOC]<='+its(iDate));
          quD.SQL.Add('and [IDCARD] not in (SELECT [ICODE] FROM [dbo].[GDSMOVE] where [ISKL]='+its(fmMainMC.Label3.tag)+' and [IDATE]<='+its(iDate)+')');
          quD.ExecSQL;

          fmSt.Memo1.Lines.Add(fmt+'����� ... ���� ������������ ������ �������� ..');  delay(10);

          quCardsMoveList.Active:=False;
          quCardsMoveList.SQL.Clear;
          quCardsMoveList.SQL.Add('SELECT [ICODE] FROM [dbo].[GDSMOVE]');
          quCardsMoveList.SQL.Add('where [ISKL]='+its(fmMainMC.Label3.tag));
          quCardsMoveList.SQL.Add('group by [ICODE]');
          quCardsMoveList.Active:=True;

          fmSt.Memo1.Lines.Add(fmt+'  ������ �� ( '+its(quCardsMoveList.RecordCount)+' ������� )');  delay(10);
          fmSt.Memo1.Lines.Add(fmt+'����� ... ���� ������ ..');  delay(10);

          iC:=0;

          quCardsMoveList.First;
          while not quCardsMoveList.Eof do
          begin
            //�������

            quProc.SQL.Clear;
            quProc.SQL.Add('DECLARE @ISKL int = '+its(fmMainMC.Label3.tag));
            quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
            quProc.SQL.Add('DECLARE @IDCARD int = '+its(quCardsMoveListICODE.AsInteger));
            quProc.SQL.Add('EXECUTE [dbo].[prCreatePartInBegin] @ISKL,@IDATE,@IDCARD');  //� ��������� ������ ������
            try
              quProc.ExecSQL;
            except
              fmSt.Memo1.Lines.Add(fmt+'    ������');
              break;
            end;

            quCardsMoveList.Next;
            inc(iC);
            if iC mod 1000 = 0 then
            begin
              fmSt.Memo1.Lines.Add(fmt+'    - ���������� '+its(iC)); delay(10);
            end;
          end;
          fmSt.Memo1.Lines.Add(fmt+'    - ���������� '+its(iC)); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'������� �������� ��');

          quCardsMoveList.Active:=False;
        end;
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmMainMC.acRepPostExecute(Sender: TObject);
Var sClass, sDeps:String;
begin
  //������� �� �����������
  if not CanDo('prReportByPost') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;
    if quShops.RecordCount>0 then
    begin
      fmPer2.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;
      fmPer2.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer2.ShowModal;
      if fmPer2.ModalResult=mrOk then
      begin

        fmRepPostRemn.Memo1.Clear;
        fmRepPostRemn.Show;
        fmRepPostRemn.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sClass:='';
        if (fmPer2.cxCheckBox4.Checked=False)and(fmPer2.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ��������������
          fmRepPostRemn.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer2.cxButtonEdit2.Tag)+INtToStr(fmPer2.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer2.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer2.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer2.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer2.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepPostRemn.Caption:='������� �� ����������� �� ���� '+ds1(date);
        //��� ����� ���������  - ���� ����������� ������
        fmRepPostRemn.Memo1.Lines.Add('  - ��������� ������'); delay(10);

        fmRepPostRemn.ViewRepPostRemn.BeginUpdate;
        dsquRepPostRemn.DataSet:=nil;

        try
          quRepPostRemn.Active:=False;
          quRepPostRemn.SQL.Clear;

          quRepPostRemn.SQL.Add('SELECT pin.[IDSKL]                                ');
          quRepPostRemn.SQL.Add('      ,pin.[IDHEAD]                               ');
          quRepPostRemn.SQL.Add('      ,pin.[IDCARD]                               ');
          quRepPostRemn.SQL.Add('      ,pin.[QPART]                                ');
          quRepPostRemn.SQL.Add('      ,pin.[QREMN]                                ');
          quRepPostRemn.SQL.Add('      ,pin.[PRICEIN]                              ');
          quRepPostRemn.SQL.Add('      ,pin.[PRICEIN0]                             ');
          quRepPostRemn.SQL.Add('      ,pin.[PRICER]                               ');
          quRepPostRemn.SQL.Add('      ,(pin.[QREMN]*pin.[PRICEIN]) as SumInRemn   ');
          quRepPostRemn.SQL.Add('      ,(pin.[QREMN]*pin.[PRICEIN0]) as SumIn0Remn ');
          quRepPostRemn.SQL.Add('      ,(pin.[QREMN]*pin.[PRICER]) as SumRRemn     ');
          quRepPostRemn.SQL.Add('      ,hd.[IDATEDOC]                              ');
          quRepPostRemn.SQL.Add('      ,hd.[NUMDOC]                                ');
          quRepPostRemn.SQL.Add('      ,hd.[NUMSF]                                 ');
          quRepPostRemn.SQL.Add('      ,hd.[IDCLI]                                 ');
          quRepPostRemn.SQL.Add('      ,(pin.[QPART]*pin.[PRICEIN]) as SUMIN   ');
          quRepPostRemn.SQL.Add('      ,cli.[Name] as CLINAME                      ');
          quRepPostRemn.SQL.Add('      ,cli.[FullName] as CLIFULLNAME              ');
          quRepPostRemn.SQL.Add('      ,ca.[NAME] as NAME                          ');
          quRepPostRemn.SQL.Add('      ,ca.[FULLNAME]                              ');
          quRepPostRemn.SQL.Add('      ,ca.[BARCODE]                               ');
          quRepPostRemn.SQL.Add('      ,ca.[EDIZM]                                 ');
          quRepPostRemn.SQL.Add('      ,ca.[RNAC]                                   ');
          quRepPostRemn.SQL.Add('      ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF]) ');
          quRepPostRemn.SQL.Add('      ,dep.[NAME] as DEPNAME                       ');
          quRepPostRemn.SQL.Add('  FROM [dbo].[PARTIN] pin                           ');
          quRepPostRemn.SQL.Add('  left join [dbo].[DOCIN_HEAD] hd on hd.[ID]=pin.[IDHEAD] ');
          quRepPostRemn.SQL.Add('  left join [dbo].[CLIENTS] cli on cli.[Id]=hd.[IDCLI]    ');
          quRepPostRemn.SQL.Add('  left join [dbo].[CARDS] ca on ca.[ID]=pin.[IDCARD]      ');
          quRepPostRemn.SQL.Add('  left join [dbo].[DEPARTS] dep on dep.[ID]=pin.[IDSKL]   ');
          quRepPostRemn.SQL.Add('  where                                                   ');
          quRepPostRemn.SQL.Add('  pin.[IDSKL] in ('+sDeps+')                              ');
          quRepPostRemn.SQL.Add('  and pin.[IDATEDOC]<dbo.d2i(getdate())                   ');
          quRepPostRemn.SQL.Add('  and pin.[QREMN]>0                                       ');
          quRepPostRemn.SQL.Add('  and pin.[ITYPED]=1                                      ');

          if (fmPer2.cxCheckBox3.Checked=False)and(fmPer2.cxButtonEdit1.Tag>0) then
          begin  //��������� ���������
            quRepPostRemn.SQL.Add('  and hd.[IDCLI]='+its(fmPer2.cxButtonEdit1.Tag)+'        ');
          end;
          if sClass>'' then quRepPostRemn.SQL.Add('  and ca.[IDCLASSIF] in ('+sClass+')');
          quRepPostRemn.SQL.Add('  order by hd.[IDATEDOC],cli.[Name],pin.[IDSKL],pin.[IDHEAD],pin.[IDCARD] ');
          quRepPostRemn.Active:=True;

        finally
          dsquRepPostRemn.DataSet:=quRepPostRemn;
          fmRepPostRemn.ViewRepPostRemn.EndUpdate;
        end;
        fmRepPostRemn.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acRepRealExecute(Sender: TObject);
Var sPost,sClass, sDeps:String;
begin
  //����� � ����������
  if not CanDo('prReportSale') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;
    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);
      fmPer1.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;
      fmPer1.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer1.ShowModal;
      if fmPer1.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer1.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer1.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer1.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer1.cxLookupComboBox2.EditValue;

        fmRepReal.Memo1.Clear;
        fmRepReal.Show;
        fmRepReal.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sPost:='';
        if (fmPer1.cxCheckBox3.Checked=False)and(fmPer1.cxButtonEdit1.Tag>0) then
        begin  //���� ����������� ����������
          fmRepReal.Memo1.Lines.Add('  - ����������� ����������'); delay(10);
          quCliAss.Active:=False;
          quCliAss.Parameters.ParamByName('ICLI').Value:=fmPer1.cxButtonEdit1.Tag;
          quCliAss.Active:=True;
          quCliAss.First;
          while not quCliAss.Eof do
          begin
            sPost:=sPost+','+its(quCliAssIDCARD.AsInteger);
            quCliAss.Next;
          end;
          if sPost>'' then delete(sPost,1,1); //������ ������� ������
          quCliAss.Active:=False;
        end;

        sClass:='';
        if (fmPer1.cxCheckBox4.Checked=False)and(fmPer1.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ����������
          fmRepReal.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer1.cxButtonEdit2.Tag)+INtToStr(fmPer1.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer1.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer1.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer1.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer1.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepReal.Caption:='���������� ������ �� ������ � '+fmPer1.cxDateEdit1.Text+' �� '+fmPer1.cxDateEdit2.Text +' ������ ('+sDeps+') ��������� ('+sPost+') ������ ('+sClass+')';

        //��� ����� ���������  - ���� ����������� ������
        fmRepReal.Memo1.Lines.Add('  - ��������� ������'); delay(10);

        fmRepReal.ViewRepReal.BeginUpdate;
        dsquRepReal.DataSet:=nil;

        try
          quRepReal.Active:=False;
          quRepReal.SQL.Clear;
          quRepReal.SQL.Add('');
          quRepReal.SQL.Add('Declare @IDATEB int='+its(Trunc(fmPer1.cxDateEdit1.Date)));
          quRepReal.SQL.Add('Declare @IDATEE int='+its(Trunc(fmPer1.cxDateEdit2.Date)));

          quRepReal.SQL.Add('SELECT     hd.Id_Depart, sp.Code, dep.IDS, dep.NAME as DEPNAME, ca.NAME, ca.BARCODE, ca.EDIZM, ca.RNAC,class.NAME as NameCl');
          quRepReal.SQL.Add(',gr2.IGR2,abcq.AGR,abcp.AGR');
          quRepReal.SQL.Add(',sClass2=(SElect NAME from [dbo].[CLASSIF] where [ID]=gr2.IGR2)');
          quRepReal.SQL.Add(',isnull(abcq.AGR,4) as abcq,isnull(abcp.AGR,4) as abcp');
          quRepReal.SQL.Add(', SUM(sp.Quant) AS Quant, SUM(sp.Summa) as RSum, MAX(sp.Price) as PriceR, SUM(DSum) as DSUM');
          quRepReal.SQL.Add('FROM         dbo.cqLines AS sp');
          quRepReal.SQL.Add('LEFT OUTER JOIN dbo.cqHeads AS hd ON hd.Id = sp.IdHead');
          quRepReal.SQL.Add('LEFT OUTER JOIN dbo.DEPARTS AS dep ON dep.ID = hd.Id_Depart');
          quRepReal.SQL.Add('LEFT OUTER JOIN dbo.CARDS ca on ca.ID=sp.Code');
          quRepReal.SQL.Add('LEFT OUTER JOIN dbo.CLASSIF class on class.ID=ca.IDCLASSIF');
          quRepReal.SQL.Add('left join [dbo].[vClassGr2] gr2 on gr2.[ID]=ca.[IDCLASSIF]');
          quRepReal.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]=hd.Id_Depart');
          quRepReal.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]=hd.Id_Depart');

          quRepReal.SQL.Add('where hd.IDate>=@IDATEB');
          quRepReal.SQL.Add('and hd.IDate<=@IDATEE');
          if sPost>'' then quRepReal.SQL.Add('and ca.[ID] in ('+sPost+')');
          if sClass>'' then quRepReal.SQL.Add('and ca.[IDCLASSIF] in ('+sClass+')');
          quRepReal.SQL.Add('and hd.Id_Depart in ('+sDeps+')');
          quRepReal.SQL.Add('GROUP BY hd.Id_Depart, sp.Code, dep.IDS, dep.NAME, ca.NAME, ca.BARCODE, ca.EDIZM, ca.RNAC,class.NAME,gr2.IGR2,abcq.AGR,abcp.AGR');
          quRepReal.SQL.Add('order by dep.IDS,hd.Id_Depart,sp.Code');
          quRepReal.Active:=True;

        finally
          dsquRepReal.DataSet:=quRepReal;
          fmRepReal.ViewRepReal.EndUpdate;
        end;
        fmRepReal.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acRemnsDayExecute(Sender: TObject);
Var sPost,sClass, sDeps:String;
    iC,iCli:INteger;
    rQ,rQRemn:Real;
begin
  if not CanDo('prReportRemnDay') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;
    if quShops.RecordCount>0 then
    begin
      fmPer2.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;
      fmPer2.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer2.ShowModal;
      if fmPer2.ModalResult=mrOk then
      begin

        fmRemnsDay.Memo1.Clear;
        fmRemnsDay.Show;
        fmRemnsDay.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sPost:='';  iCli:=0;
        if (fmPer2.cxCheckBox3.Checked=False)and(fmPer2.cxButtonEdit1.Tag>0) then
        begin  //���� ����������� ����������
          fmRemnsDay.Memo1.Lines.Add('  - ����������� ����������'); delay(10);
          quCliAss.Active:=False;
          quCliAss.Parameters.ParamByName('ICLI').Value:=fmPer2.cxButtonEdit1.Tag;
          quCliAss.Active:=True;
          quCliAss.First;
          while not quCliAss.Eof do
          begin
            sPost:=sPost+','+its(quCliAssIDCARD.AsInteger);
            quCliAss.Next;
          end;
          if sPost>'' then delete(sPost,1,1); //������ ������� ������
          quCliAss.Active:=False;

          iCli:=fmPer2.cxButtonEdit1.Tag;
        end;

        sClass:='';
        if (fmPer2.cxCheckBox4.Checked=False)and(fmPer2.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ����������
          fmRemnsDay.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer2.cxButtonEdit2.Tag)+INtToStr(fmPer2.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer2.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer2.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer2.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer2.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRemnsDay.Caption:='������� �� '+ds1(date)+' ������ ('+sDeps+') ��������� ('+sPost+') ������ ('+sClass+')';

        //��� ����� ���������  - ���� ����������� ������
        fmRemnsDay.Memo1.Lines.Add('  - �����... ���� ��������� ������'); delay(10);

        CloseTe(fmRemnsDay.teRD);
        fmRemnsDay.ViewRemnsDay.BeginUpdate;
        fmRemnsDay.dsteRD.DataSet:=nil;

        try
          quRepRD.Active:=False;
          quRepRD.SQL.Clear;
          quRepRD.SQL.Add('');

          quRepRD.SQL.Add('declare @IDATEB int');
          quRepRD.SQL.Add('declare @IDATEB1 int');
          quRepRD.SQL.Add('declare @IDATEE int');

          quRepRD.SQL.Add('Set @IDATEB=dbo.d2i(getdate())-365');
          quRepRD.SQL.Add('Set @IDATEE=dbo.d2i(getdate())-1');
          quRepRD.SQL.Add('Set @IDATEB1=@IDATEE-30');

          quRepRD.SQL.Add('SELECT CM.[ICODE],CM.[ISKL]');
          quRepRD.SQL.Add(',ca.[IDCLASSIF],ca.[NAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC]');
          quRepRD.SQL.Add(',dep.[NAME] as sDep, gr2.IGR2,abcq.AGR,abcp.AGR');

          quRepRD.SQL.Add(',sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])');
          quRepRD.SQL.Add(',sClass2=(SElect NAME from [dbo].[CLASSIF] where [ID]=gr2.IGR2)');
          quRepRD.SQL.Add(',isnull(abcq.AGR,4) as abcq,isnull(abcp.AGR,4) as abcp');

          quRepRD.SQL.Add(',clia.IDCLI,cli.Name as NameCli,isnull(cli.[DayZ],0) as DayZ,isnull(ca.[AZKOEF],0) as AZKOEF,isnull(ca.[AZSTRAHR],0) as AZSTRAHR');

          quRepRD.SQL.Add(',rQRemn=isnull((Select SUM([QUANT]) from [dbo].[GDSMOVE] where [ISKL]=CM.[ISKL] and [ICODE]=CM.[ICODE] and [IDATE]<@IDATEE),0)');
          quRepRD.SQL.Add(',VSPEED=(Select abs(SUM(t.QUANTD)/isnull(Count(*),1)) from');
          quRepRD.SQL.Add('                 (select di.INUM, isnull(cmv.[QUANT],0) as QUANTD');
          quRepRD.SQL.Add('                 ,isnull((Select SUM(cmremn.[QUANT]) from [dbo].[GDSMOVE] cmremn  where cmremn.[IDATE]<di.INUM and cmremn.[ISKL]=CM.[ISKL] and cmremn.[ICODE]=CM.[ICODE]),0) as REMNB');
          quRepRD.SQL.Add('                 from dbo.DIAP as di');
          quRepRD.SQL.Add('                 left join [dbo].[GDSMOVE] cmv on cmv.[ISKL]=CM.[ISKL] and di.INUM=cmv.[IDATE] and cmv.[ICODE]=CM.[ICODE] and cmv.[ITYPED]=7');
          quRepRD.SQL.Add('                 where di.INUM>=@IDATEB1 and di.INUM<=@IDATEE) as t');
          quRepRD.SQL.Add('               where t.REMNB>0)');

          quRepRD.SQL.Add('FROM [MCRYSTAL].[dbo].[GDSMOVE] CM');
          quRepRD.SQL.Add('left join [dbo].[CARDS] ca on ca.[ID]=CM.[ICODE]');
          quRepRD.SQL.Add('LEFT JOIN dbo.DEPARTS AS dep ON dep.ID = CM.[ISKL]');
          quRepRD.SQL.Add('left join [dbo].[vClassGr2] gr2 on gr2.[ID]=ca.[IDCLASSIF]');
          quRepRD.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]=CM.[ISKL]');
          quRepRD.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]=CM.[ISKL]');
          quRepRD.SQL.Add('left join [dbo].[vCliAss] clia on clia.[IDCARD]=CM.[ICODE] and clia.[IDSKL]=CM.[ISKL]');
          quRepRD.SQL.Add('left join [dbo].[CLIENTS] cli on cli.Id=clia.IDCLI');

          quRepRD.SQL.Add('where CM.[IDATE]>=@IDATEB');
          quRepRD.SQL.Add('and CM.[ISKL] in ('+sDeps+')');
          if sPost>''  then quRepRD.SQL.Add('and CM.[ICODE] in ('+sPost+')');
          if sClass>'' then quRepRD.SQL.Add('and ca.[IDCLASSIF] in ('+sClass+')');
          if iCli>0    then quRepRD.SQL.Add('and clia.IDCLI = '+its(iCli));

          quRepRD.SQL.Add('group by CM.[ISKL],CM.[ICODE],ca.[IDCLASSIF],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC],dep.[NAME],gr2.IGR2,abcq.AGR,abcp.AGR,clia.IDCLI,cli.Name,cli.[DayZ],ca.[AZKOEF],ca.[AZSTRAHR]');
          quRepRD.SQL.Add('order by CM.[ICODE],CM.[ISKL]');

          quRepRD.Active:=True;

          fmRemnsDay.Memo1.Lines.Add('  - �������� '+its(quRepRD.RecordCount)); delay(10);
          fmRemnsDay.Memo1.Lines.Add('  - ����� ... ���� ������ '); delay(10);

          iC:=0;
          quRepRD.First;
          while not quRepRD.Eof do
          begin
            with fmRemnsDay do
            begin
              teRD.Append;
              teRDICODE.AsInteger:=quRepRDICODE.AsInteger;
              teRDISKL.AsInteger:=quRepRDISKL.AsInteger;
              teRDIDCLASSIF.AsInteger:=quRepRDIDCLASSIF.AsInteger;
              teRDNAME.AsString:=quRepRDNAME.AsString;
              teRDBARCODE.AsString:=quRepRDBARCODE.AsString;
              teRDEDIZM.AsInteger:=quRepRDEDIZM.AsInteger;
              teRDRNAC.AsFloat:=quRepRDRNAC.AsFloat;
              teRDsDep.AsString:=quRepRDsDep.AsString;
              teRDsClass.AsString:=quRepRDsClass.AsString;
              teRDsClass2.AsString:=quRepRDsClass2.AsString;
              teRDABCQ.AsInteger:=quRepRDabcq.AsInteger;
              teRDABCP.AsInteger:=quRepRDabcp.AsInteger;
              teRDrQRemn.AsFloat:=quRepRDrQRemn.AsFloat;
//                teRDrSpeed.AsFloat:=prCalcSpeed(quRepRDICODE.AsInteger,quRepRDISKL.AsInteger);
              teRDrSpeed.AsFloat:=quRepRDVSPEED.AsFloat;

              if (teRDrQRemn.AsFloat>0.001) and (teRDrSpeed.AsFloat>0.001) then teRDrDays.AsFloat:=rv(teRDrQRemn.AsFloat/teRDrSpeed.AsFloat)
              else teRDrDays.AsFloat:=0;

              teRDIdCli.AsInteger:=quRepRDIDCLI.AsInteger;
              teRDNameCli.AsString:=quRepRDNameCli.AsString;
              teRDDayZ.AsInteger:=quRepRDDayZ.AsInteger;
              teRDDayZStrah.AsInteger:=quRepRDAZSTRAHR.AsInteger;
              teRDZKoef.AsFloat:=quRepRDAZKOEF.AsFloat;

              rQRemn:=teRDrQRemn.AsFloat;
              if rQRemn<0 then rQRemn:=0;

              if teRDZKoef.AsFloat<>0 then
              begin
                rQ:=(teRDDayZ.AsInteger*teRDrSpeed.AsFloat)*teRDZKoef.AsFloat+(teRDDayZStrah.AsInteger*teRDrSpeed.AsFloat)-rQRemn;
                if rQ<0 then rQ:=0;
                teRDQuantZ.AsFloat:=rQ;
              end else
              begin
                rQ:=(teRDDayZ.AsInteger*teRDrSpeed.AsFloat)+(teRDDayZStrah.AsInteger*teRDrSpeed.AsFloat)-rQRemn;
                if rQ<0 then rQ:=0;
                teRDQuantZ.AsFloat:=rQ;
              end;
              teRD.Post;
            end;

            quRepRD.Next; inc(iC);
            if iC mod 50 = 0 then
            begin
              fmRemnsDay.Memo1.Lines.Add('    - '+its(iC)); delay(10);
            end;
          end;

          fmRemnsDay.Memo1.Lines.Add('    - '+its(iC)); delay(10);
          quRepRD.Active:=False;


        finally
          fmRemnsDay.ViewRemnsDay.EndUpdate;
          fmRemnsDay.dsteRD.DataSet:=fmRemnsDay.teRD;
        end;
        fmRemnsDay.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acLogExecute(Sender: TObject);
Var iDateB,iDateE:Integer;
    sDep:String;
begin
  if not CanDo('prViewLog') then begin Showmessage('��� ����.'); exit; end;

  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
    iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      sDep:='';
      quDeps.Active:=False;
      quDeps.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDeps.Active:=True;

      quDeps.First;
      while not quDeps.Eof do
      begin
        sDep:=sDep+','+quDepsID.AsString;
        quDeps.Next;
      end;
      quDeps.Active:=False;
      if sDep>'' then delete(sDep,1,1);

      dsquGetLog.DataSet:=nil;
      quGetLog.Active:=False;
      quGetLog.SQL.Clear;
      quGetLog.SQL.Add('');

      quGetLog.SQL.Add('SELECT l.[IDATE],l.[ITYPED],l.[IDH],l.[ID],l.[ISKL],l.[IOPER],l.[SPERS],l.[DDATE]');
      quGetLog.SQL.Add(',dt.[NAME] as DOCTYPE');
      quGetLog.SQL.Add(',dep.[NAME] as NAMEDEP');
      quGetLog.SQL.Add('FROM [dbo].[LOG] l');
      quGetLog.SQL.Add('left join [dbo].[DOCTYPE] dt on dt.ITD=l.[ITYPED]');
      quGetLog.SQL.Add('left join [dbo].[DEPARTS] dep on dep.[ID]=l.[ISKL]');
      quGetLog.SQL.Add('where l.[ISKL] in ('+sDep+')');
      quGetLog.SQL.Add('and l.[IDATE]>='+its(iDateB));
      quGetLog.SQL.Add('and l.[IDATE]<='+its(iDateE));
      quGetLog.Active:=True;
      dsquGetLog.DataSet:=quGetLog;

      fmRepLog:=TfmRepLog.Create(Application);
      fmRepLog.ShowModal;
      fmRepLog.Release;
      quGetLog.Active:=False;
    end;
  end;
end;

procedure TfmMainMC.acRepRemnSExecute(Sender: TObject);
Var sPost,sClass, sDeps:String;
    StrMH,sMH,sDepName:string;
    iDateB,iDateE:Integer;
    iSh:Integer;
begin
  //����� �� �������� � ���-�� � ������
  if not CanDo('prReportRemnSum') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;
    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);
      fmPer1.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;
      quDepsRep.Locate('ID',CurVal.IdDep,[]);
      fmPer1.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer1.cxDateEdit1.Date:=Date;

      fmPer1.Label4.Caption:='�� ���� '; //������ �
      fmPer1.Label5.Visible:=False;
      fmPer1.cxDateEdit2.Visible:=False;

      fmPer1.ShowModal;
      if fmPer1.ModalResult=mrOk then
      begin
        fmPer1.Label4.Caption:='������ �'; //������ �
        fmPer1.Label5.Visible:=True;
        fmPer1.cxDateEdit2.Visible:=True;

        CommonSet.DateBeg:=Trunc(fmPer1.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer1.cxDateEdit2.Date);

        iDateB:=Trunc(fmPer1.cxDateEdit1.Date);
        iDateE:=Trunc(fmPer1.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer1.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer1.cxLookupComboBox2.EditValue;
        
        fmRepRemnSum.Memo1.Clear;
        fmRepRemnSum.Show;
        fmRepRemnSum.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sPost:='';
        if (fmPer1.cxCheckBox3.Checked=False)and(fmPer1.cxButtonEdit1.Tag>0) then
        begin  //���� ����������� ����������
          fmRepRemnSum.Memo1.Lines.Add('  - ����������� ����������'); delay(10);
          quCliAss.Active:=False;
          quCliAss.Parameters.ParamByName('ICLI').Value:=fmPer1.cxButtonEdit1.Tag;
          quCliAss.Active:=True;
          quCliAss.First;
          while not quCliAss.Eof do
          begin
            sPost:=sPost+','+its(quCliAssIDCARD.AsInteger);
            quCliAss.Next;
          end;
          if sPost>'' then delete(sPost,1,1); //������ ������� ������
          quCliAss.Active:=False;
        end;

        sClass:='';
        if (fmPer1.cxCheckBox4.Checked=False)and(fmPer1.cxButtonEdit2.Tag>0) then
        begin  //���� ����������� ����������
          fmRepRemnSum.Memo1.Lines.Add('  - ����������� �� ��������������'); delay(10);
          sClass:=fGetSlave(fmPer1.cxButtonEdit2.Tag)+INtToStr(fmPer1.cxButtonEdit2.Tag);
        end;

        sDeps:='';
        if fmPer1.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer1.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer1.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer1.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepRemnSum.Caption:='������� ������ �� ���� '+fmPer1.cxDateEdit1.Text+' ������ ('+sDeps+') ��������� ('+sPost+') ������ ('+sClass+')';

        //��� ����� ���������  - ���� ����������� ������

        fmRepRemnSum.ViewRepRemnS.BeginUpdate;
        dsquRepRemnSum.DataSet:=nil;

        try
          StrMH:=sDeps;
          CloseTe(teRR);

          while StrMH>'' do
          begin
            while StrMH[1]=',' do Delete(StrMH,1,1);
            if Pos(',',StrMH)>0 then
            begin
              sMH:=Copy(StrMH,1,Pos(',',StrMH)-1);
              Delete(StrMH,1,Pos(',',StrMH)-1);
            end else
            begin
              sMH:=StrMH;
              StrMH:=''
            end;

            sDepName:=fFullName(StrToIntDef(sMH,0));
            iSh:=fShopDep(StrToIntDef(sMH,0));
            //��� ����� ���������  - ���� ����������� ������
            fmRepRemnSum.Memo1.Lines.Add('  - ��������� ������ �� �� '+sDepName+' ('+sMH+')'); delay(10);

            quRepRemnSum.Active:=False;
            quRepRemnSum.SQL.Clear;
            quRepRemnSum.SQL.Add('');

            quRepRemnSum.SQL.Add('declare @IDATEB int = '+its(iDateB-1));
            quRepRemnSum.SQL.Add('declare @IDATE0 int        ');
            quRepRemnSum.SQL.Add('declare @IDSKL int = '+sMH);
            quRepRemnSum.SQL.Add('declare @IDATEE int = '+its(iDateE));

            quRepRemnSum.SQL.Add('Set @IDATE0=isnull((Select top 1 IDATEDOC from [dbo].[DOCINV_HEAD] where [IDSKL]=@IDSKL and [IDETAIL]=0 and [IDATEDOC]<=@IDATEB order by [IDATEDOC] desc ),0)');

            quRepRemnSum.SQL.Add('Select  ca.[ID],ca.[IDCLASSIF],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[RNAC] ');
            quRepRemnSum.SQL.Add('        ,sClass=(SElect NAME from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])             ');
            quRepRemnSum.SQL.Add('		,sClassGr=(SElect NAME from [dbo].[CLASSIF] where [ID]=(SElect [IDPARENT] from [dbo].[CLASSIF] where [ID]=ca.[IDCLASSIF])) ');
            quRepRemnSum.SQL.Add('    ,sClass2=(SElect NAME from [dbo].[CLASSIF] where [ID]=gr2.IGR2)');
            quRepRemnSum.SQL.Add('  	,isnull(abcq.AGR,4) as abcq,isnull(abcp.AGR,4) as abcp         ');
            quRepRemnSum.SQL.Add('    ,(isnull(spinv.QUANT,0)+isnull(spin.QUANTIN,0)-isnull(spout.QUANTOUT,0)-isnull(spvn.QUANTVN,0)+isnull(spinv1.QUANT,0)-isnull(spsale.QUANT,0)) as QUANT_B ');
            quRepRemnSum.SQL.Add('		,(isnull(spinv.SUMIN0,0)+isnull(spin.SUMIN0,0)-isnull(spout.SUMIN0,0)-isnull(spvn.SUMIN0,0)+isnull(spinv1.SUMIN0,0)-isnull(spsale.SUMIN0,0))as RSUMIN0_B      ');
            quRepRemnSum.SQL.Add('		,(isnull(spinv.SUMIN,0)+isnull(spin.SUMIN,0)-isnull(spout.SUMIN,0)-isnull(spvn.SUMIN,0)+isnull(spinv1.SUMIN,0)-isnull(spsale.SUMIN,0))as RSUMIN_B             ');
            quRepRemnSum.SQL.Add('		,(isnull(spinv.SUMR,0)+isnull(spin.SUMR,0)-isnull(spout.SUMR,0)-isnull(spvn.SUMR,0)+isnull(spinv1.SUMR,0)-isnull(spsale.SUMR,0))as RSUMR_B                    ');

            quRepRemnSum.SQL.Add('from [dbo].[CARDS] ca ');

            quRepRemnSum.SQL.Add('left join [dbo].[vClassGr2] gr2 on gr2.[ID]=ca.[IDCLASSIF]');
            quRepRemnSum.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]=@IDSKL');
            quRepRemnSum.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]=@IDSKL');

            quRepRemnSum.SQL.Add('left join (  -- �������� ������ �� ������ �������������� ');
            quRepRemnSum.SQL.Add('		SELECT sp.[IDCARD],SUM(sp.[QUANTF]) as QUANT,SUM(sp.[SUMINF]) as SUMIN,SUM(sp.[SUMIN0F]) as SUMIN0,SUM(sp.[SUMRF]) as SUMR   ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[DOCINV_SPEC] sp                                                                                                ');
            quRepRemnSum.SQL.Add('			left join [dbo].[DOCINV_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepRemnSum.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC=@IDATE0 and hd.IACTIVE=3                                                             ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('		) spinv on spinv.[IDCARD]=ca.[ID]                                                                                            ');
            quRepRemnSum.SQL.Add('left join ( -- ������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                       ');
            quRepRemnSum.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTIN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMUCH]) as SUMR ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[DOCIN_SPEC] sp                                                                                                 ');
            quRepRemnSum.SQL.Add('			left join [dbo].[DOCIN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepRemnSum.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('			) spin on spin.[IDCARD]=ca.[ID]                                                                                            ');
            quRepRemnSum.SQL.Add('left join ( -- �������� ����������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                          ');
            quRepRemnSum.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTOUT,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR  ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[DOCOUT_SPEC] sp                                                                                                ');
            quRepRemnSum.SQL.Add('			left join [dbo].[DOCOUT_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepRemnSum.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('			) spout on spout.[IDCARD]=ca.[ID]                                                                                          ');
            quRepRemnSum.SQL.Add('left join ( -- �������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                      ');
            quRepRemnSum.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANTVN,SUM(sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMR]) as SUMR   ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[DOCVN_SPEC] sp                                                                                                 ');
            quRepRemnSum.SQL.Add('			left join [dbo].[DOCVN_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                     ');
            quRepRemnSum.SQL.Add('			where hd.IDSKLFROM=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('			) spvn on spvn.[IDCARD]=ca.[ID]                                                                                            ');
            quRepRemnSum.SQL.Add('left join ( -- �������������� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                ');
            quRepRemnSum.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANTF]-sp.[QUANTR]) as QUANT,SUM(sp.[SUMINF]-sp.[SUMIN]) as SUMIN,SUM(sp.[SUMIN0F]-sp.[SUMIN0]) as SUMIN0,SUM(sp.[SUMRF]-sp.[SUMR]) as SUMR ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[DOCINV_SPEC] sp                                                                                                ');
            quRepRemnSum.SQL.Add('			left join [dbo].[DOCINV_HEAD] hd on hd.[ID]=sp.[IDHEAD]                                                                    ');
            quRepRemnSum.SQL.Add('			where hd.IDSKL=@IDSKL and hd.IDATEDOC>@IDATE0 and hd.IDATEDOC<=@IDATEB and hd.IACTIVE=3                                    ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('			) spinv1 on spinv1.[IDCARD]=ca.[ID]                                                                                        ');
            quRepRemnSum.SQL.Add('left join ( -- ����� �� ������ � >@IDATE0 ��  <=@IDATEB                                                                         ');
            quRepRemnSum.SQL.Add('			SELECT sp.[IDCARD],SUM(sp.[QUANT]) as QUANT,SUM(sp.[QUANT]*sp.[PRICEIN]) as SUMIN,SUM(sp.[QUANT]*sp.[PRICEIN0]) as SUMIN0,SUM(sp.[QUANT]*sp.[PRICER]) as SUMR ');
            quRepRemnSum.SQL.Add('			FROM [dbo].[PARTOUT] sp                                                                                                    ');
            quRepRemnSum.SQL.Add('			where sp.IDSKL=@IDSKL and sp.IDATEDOC>@IDATE0 and sp.IDATEDOC<=@IDATEB and sp.ITYPED=7                                     ');
            quRepRemnSum.SQL.Add('			group by sp.[IDCARD]                                                                                                       ');
            quRepRemnSum.SQL.Add('			) spsale on spsale.[IDCARD]=ca.[ID]                                                                                        ');

            quRepRemnSum.SQL.Add('where ca.[ID]>0                                                                                                                  ');
            if sPost>'' then quRepRemnSum.SQL.Add('  and ca.[ID] in ('+sPost+')');
            if sClass>'' then quRepRemnSum.SQL.Add('  and ca.[IDCLASSIF] in ('+sClass+')');
            quRepRemnSum.SQL.Add('and (isnull(spinv.QUANT,0)<>0                                                                                                   ');
            quRepRemnSum.SQL.Add('	or isnull(spin.QUANTIN,0)<>0                                                                                                   ');
            quRepRemnSum.SQL.Add('	or isnull(spout.QUANTOUT,0)<>0                                                                                                 ');
            quRepRemnSum.SQL.Add('	or isnull(spvn.QUANTVN,0)<>0                                                                                                   ');
            quRepRemnSum.SQL.Add('	or isnull(spinv1.QUANT,0)<>0                                                                                                   ');
            quRepRemnSum.SQL.Add('	or isnull(spsale.QUANT,0)<>0                                                                                                   ');
            quRepRemnSum.SQL.Add('	)                                                                                                                              ');

            quRepRemnSum.Active:=True;

            fmRepRemnSum.Memo1.Lines.Add('  - ���������� ..'); delay(10);
            quRepRemnSum.First;
            while not quRepRemnSum.Eof do
            begin
              teRR.Append;
              teRRIDCLASSIF.AsInteger:=quRepRemnSumIDCLASSIF.AsInteger;
              teRRID.AsInteger:=quRepRemnSumID.AsInteger;
              teRRNAME.AsString:=quRepRemnSumNAME.AsString;
              teRRFULLNAME.AsString:=quRepRemnSumFULLNAME.AsString;
              teRRBARCODE.AsString:=quRepRemnSumBARCODE.AsString;
              teRREDIZM.AsInteger:=quRepRemnSumEDIZM.AsInteger;
              teRRRNAC.AsFloat:=quRepRemnSumRNAC.AsFloat;
              teRRsClass.AsString:=quRepRemnSumsClass.AsString;

              teRRsClass2.AsString:=quRepRemnSumsClass2.AsString;
              teRRABCP.AsInteger:=quRepRemnSumabcp.AsInteger;
              teRRABCQ.AsInteger:=quRepRemnSumabcq.AsInteger;

              teRRrQBeg.AsFloat:=quRepRemnSumQUANT_B.AsFloat;
              teRRrSBeg.AsFloat:=quRepRemnSumRSUMIN_B.AsFloat;
              teRRrSBeg0.AsFloat:=quRepRemnSumRSUMIN0_B.AsFloat;
              teRRrSBegR.AsFloat:=quRepRemnSumRSUMR_B.AsFloat;

              teRRIDS.AsInteger:=iSh;
              teRRIDDEP.AsInteger:=StrToIntDef(sMH,0);
              teRRNAMEDEP.AsString:=sDepName;

              teRRsClassGr.AsString:=quRepRemnSumsClassGr.AsString;
              teRR.Post;

              quRepRemnSum.Next;
            end;

            quRepRemnSum.Active:=False;
          end;
        finally
          dsquRepRemnSum.DataSet:=teRR;
          fmRepRemnSum.ViewRepRemnS.EndUpdate;
        end;
        fmRepRemnSum.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
      fmPer1.Label4.Caption:='������ �'; //������ �
      fmPer1.Label5.Visible:=True;
      fmPer1.cxDateEdit2.Visible:=True;
    end;
  end;
end;

procedure TfmMainMC.acEkonomRepExecute(Sender: TObject);
Var sDeps:String;
begin
  //������������� �����
  if not CanDo('prReportEkonom') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;

    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);

      fmPer3.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;

      quDepsRep.Locate('ID',CurVal.IdDep,[]);

      fmPer3.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer3.ShowModal;
      if fmPer3.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer3.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer3.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer3.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer3.cxLookupComboBox2.EditValue;

        fmRepEconom.Memo1.Clear;
        fmRepEconom.Show;
        fmRepEconom.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sDeps:='';
        if fmPer3.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer3.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer3.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer3.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepEconom.Caption:='������������� ����� �� ������ � '+fmPer3.cxDateEdit1.Text+' �� '+fmPer3.cxDateEdit2.Text +' ������ ('+sDeps+')';

        //��� ����� ���������  - ���� ����������� ������
        fmRepEconom.Memo1.Lines.Add('  - ��������� ������'); delay(10);
        try
          fmRepEconom.ViewRepEconom.BeginUpdate;

          quRepEconom.Active:=False;
          quRepEconom.SQL.Clear;

          quRepEconom.SQL.Add('declare @IDATEB int = '+its(Trunc(CommonSet.DateBeg)));
          quRepEconom.SQL.Add('declare @IDATEE int = '+its(Trunc(CommonSet.DateEnd)));

          quRepEconom.SQL.Add('SELECT dep.[IDS],dep.[ID],dep.[NAME]   ');
          quRepEconom.SQL.Add('      ,(Select SUM([SumInR]+[SumInVnR]+[SumInvR]+[SumAC]-[SumOutR]-[SumOutVnR]-[CashNal]-[CashDNal]-[CashBn]-[CashDBn]) ');
          quRepEconom.SQL.Add('	    from  [dbo].[REPORTS]   ');
          quRepEconom.SQL.Add('		where [ISKL]=dep.[ID]     ');
          quRepEconom.SQL.Add('        and [IDATE]<=@IDATEE ');
          quRepEconom.SQL.Add('		) as REMNR                ');
          quRepEconom.SQL.Add('      ,(Select SUM([SumIn]+[SumInVn]+[SumInv]-[SumOutIn]-[SumOutVn]-[CashSS])');
          quRepEconom.SQL.Add('		from  [dbo].[REPORTS]     ');
          quRepEconom.SQL.Add('		where [ISKL]=dep.[ID]     ');
          quRepEconom.SQL.Add('        and [IDATE]<=@IDATEE ');
          quRepEconom.SQL.Add('	   ) as REMNIN              ');
          quRepEconom.SQL.Add('      ,(Select SUM([CashNal]+[CashBn]) ');
          quRepEconom.SQL.Add('		from  [dbo].[REPORTS]     ');
          quRepEconom.SQL.Add('		where [ISKL]=dep.[ID]     ');
          quRepEconom.SQL.Add('        and [IDATE]>=@IDATEB ');
          quRepEconom.SQL.Add('        and [IDATE]<=@IDATEE ');
          quRepEconom.SQL.Add('	   )as REALR                ');
          quRepEconom.SQL.Add('      ,(Select SUM([CashSS]) ');
          quRepEconom.SQL.Add('		from  [dbo].[REPORTS]     ');
          quRepEconom.SQL.Add('		where [ISKL]=dep.[ID]     ');
          quRepEconom.SQL.Add('        and [IDATE]>=@IDATEB ');
          quRepEconom.SQL.Add('        and [IDATE]<=@IDATEE ');
          quRepEconom.SQL.Add('        )as REALIN           ');
          quRepEconom.SQL.Add('      ,(Select SUM([CashNal]+[CashBn]-[CashSS])');
          quRepEconom.SQL.Add(' 	    from  [dbo].[REPORTS] ');
          quRepEconom.SQL.Add('        where [ISKL]=dep.[ID]');
          quRepEconom.SQL.Add('        and [IDATE]>=@IDATEB ');
          quRepEconom.SQL.Add('        and [IDATE]<=@IDATEE ');
          quRepEconom.SQL.Add('        )as PRIB             ');
          quRepEconom.SQL.Add('  FROM [dbo].[DEPARTS] dep   ');
          quRepEconom.SQL.Add('  where  dep.[ID] in ('+sDeps+') ');

          quRepEconom.Active:=True;

        finally
          fmRepEconom.ViewRepEconom.EndUpdate;
        end;



        fmRepEconom.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acRepPostDayExecute(Sender: TObject);
Var iDate:Integer;
    bStart:Boolean;
//    iC:INteger;
begin
  //������� �� ����������� �� ����
  if not CanDo('prReportByPostDay') then begin Showmessage('��� ����.'); end else
  begin
    bStart:=False;

    fmPeriodSingleDate:=tfmPeriodSingleDate.Create(Application);
    fmPeriodSingleDate.cxDateEdit1.Date:=Trunc(Date);
    fmPeriodSingleDate.Label3.Caption:='������� ��������� �� ����� �� �� '+fmMainMC.Label3.Caption+' ';
    fmPeriodSingleDate.ShowModal;
    if fmPeriodSingleDate.ModalResult=mrOk then bStart:=True;
    iDate:=Trunc(fmPeriodSingleDate.cxDateEdit1.Date);
    fmPeriodSingleDate.Release;

    if bStart then
    begin
      fmRepPostRemnDay.Memo1.Clear;

      fmRepPostRemnDay.Show;
      fmRepPostRemnDay.Memo1.Lines.Add('����� .. ���� ������������ ������.'); Delay(10);

      fmRepPostRemnDay.ViewRepPostRemnDay.BeginUpdate;
      with  fmRepPostRemnDay do
      begin
        quRepPostRemnDay.Active:=False;
        quRepPostRemnDay.SQL.Clear;
        quRepPostRemnDay.SQL.Add('');
        quRepPostRemnDay.SQL.Add('DECLARE @ISKL int = '+its(fmMainMC.Label3.Tag));
        quRepPostRemnDay.SQL.Add('DECLARE @IDATE int = '+its(iDate));
        quRepPostRemnDay.SQL.Add('EXECUTE [dbo].[prRepPostRemnDay] @ISKL,@IDATE');
        quRepPostRemnDay.Active:=True;
      end;

      fmRepPostRemnDay.ViewRepPostRemnDay.EndUpdate;
      fmRepPostRemnDay.Memo1.Lines.Add('������������ ���������.'); Delay(10);
    end;
  end;
end;

procedure TfmMainMC.acRepSaleGrExecute(Sender: TObject);
Var sDeps:String;
begin
  // ����� ���������� �� �������
  if not CanDo('prReportSaleGr') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;

    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);

      fmPer3.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;

      quDepsRep.Locate('ID',CurVal.IdDep,[]);

      fmPer3.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer3.ShowModal;
      if fmPer3.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer3.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer3.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer3.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer3.cxLookupComboBox2.EditValue;

        fmRepSaleGr.Memo1.Clear;
        fmRepSaleGr.Show;
        fmRepSaleGr.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sDeps:='';
        if fmPer3.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer3.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer3.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer3.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepSaleGr.Caption:='���������� �� ������� �� ������ � '+fmPer3.cxDateEdit1.Text+' �� '+fmPer3.cxDateEdit2.Text +' ������ ('+sDeps+')';

        //��� ����� ���������  - ���� ����������� ������
        fmRepSaleGr.Memo1.Lines.Add('  - ��������� ������'); delay(10);
        try
          fmRepSaleGr.ViewRepSaleGr.BeginUpdate;

          quRepSaleGr.Active:=False;
          {
          quRepSaleGr.Parameters.ParamByName('SSKL').Value:=sDeps;
          quRepSaleGr.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
          quRepSaleGr.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateEnd);
          }

          quRepSaleGr.SQL.Clear;
          quRepSaleGr.SQL.Add('');

          quRepSaleGr.SQL.Add('DECLARE @SSKL varchar(900)');
          quRepSaleGr.SQL.Add('DECLARE @IDATEB int');
          quRepSaleGr.SQL.Add('DECLARE @IDATEE int');
          quRepSaleGr.SQL.Add('Set @IDATEB='+its(Trunc(CommonSet.DateBeg)));
          quRepSaleGr.SQL.Add('Set @IDATEE='+its(Trunc(CommonSet.DateEnd)));
          quRepSaleGr.SQL.Add('Set @SSKL='''+sDeps+'''');
          quRepSaleGr.SQL.Add('EXECUTE [dbo].[prRepSaleGr]  @SSKL,@IDATEB,@IDATEE');

          quRepSaleGr.Active:=True;

          fmRepSaleGr.Memo1.Lines.Add('  .. '+its(quRepSaleGr.RecordCount)); delay(10);

        finally
          fmRepSaleGr.ViewRepSaleGr.EndUpdate;
        end;



        fmRepSaleGr.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMC.acRepRealAPExecute(Sender: TObject);
Var sDeps:String;
begin
  //����� �� ���������� ����������� ���������
  if not CanDo('prReportSaleGr') then begin Showmessage('��� ����.'); exit; end;
  with dmMCS do
  begin
    quShops.Active:=False;
    quShops.Parameters.ParamByName('IPERS').Value:=Person.Id;
    quShops.Active:=True;
    quShops.First;

    if quShops.RecordCount>0 then
    begin
      quShops.Locate('ID',CurVal.IdShop,[]);

      fmPer3.cxLookupComboBox1.EditValue:=quShopsID.AsInteger;

      quDepsRep.Active:=False;
      quDepsRep.Parameters.ParamByName('IPERS').Value:=Person.Id;
      quDepsRep.Active:=True;
      quDepsRep.First;

      quDepsRep.Locate('ID',CurVal.IdDep,[]);

      fmPer3.cxLookupComboBox2.EditValue:=quDepsRepID.AsInteger;

      fmPer3.ShowModal;
      if fmPer3.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPer3.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPer3.cxDateEdit2.Date);

        CurVal.IdShop:=fmPer3.cxLookupComboBox1.EditValue;
        CurVal.IdDep:=fmPer3.cxLookupComboBox2.EditValue;

        fmRepSaleAP.Memo1.Clear;
        fmRepSaleAP.Show;
        fmRepSaleAP.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

        sDeps:='';
        if fmPer3.cxCheckBox2.Checked=False then
        begin
          sDeps:=its(fmPer3.cxLookupComboBox2.EditValue);
        end else //�� ���� ������� -  �� ����� ���������?
        begin
          if fmPer3.cxCheckBox1.Checked=False then
          begin //�� ����������� ��������
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 and [IDS]='+its(fmPer3.cxLookupComboBox1.EditValue));
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end else //�� ���� ��������� � ���� �������
          begin
            quSel.Active:=False;
            quSel.SQL.Clear;
            quSel.SQL.Add('SELECT [ID] FROM [dbo].[DEPARTS] where [ISTATUS]>0 ');
            quSel.Active:=True;
            quSel.First;
            while not quSel.Eof do
            begin
              sDeps:=sDeps+','+quSel.FieldByName('ID').AsString;
              quSel.Next;
            end;
            quSel.Active:=False;
            if sDeps>'' then delete(sDeps,1,1);
          end;
        end;

        if sDeps='' then sDeps:='0';

        fmRepSaleAP.Caption:='���������� �� ������� �� ������ � '+fmPer3.cxDateEdit1.Text+' �� '+fmPer3.cxDateEdit2.Text +' ������ ('+sDeps+')';

        fmRepSaleAP.sDeps:=sDeps;
        fmRepSaleAP.iDateB:=Trunc(CommonSet.DateBeg);
        fmRepSaleAP.iDateE:=Trunc(CommonSet.DateEnd);

        //��� ����� ���������  - ���� ����������� ������
        fmRepSaleAP.Memo1.Lines.Add('  - ��������� ������'); delay(10);
        try
          fmRepSaleAP.ViewRepSaleAP.BeginUpdate;

          quRepSaleAP.Active:=False;

          quRepSaleAP.SQL.Clear;
          quRepSaleAP.SQL.Add('');

          quRepSaleAP.SQL.Add('DECLARE @SSKL varchar(900)');
          quRepSaleAP.SQL.Add('DECLARE @IDATEB int');
          quRepSaleAP.SQL.Add('DECLARE @IDATEE int');
          quRepSaleAP.SQL.Add('Set @IDATEB='+its(Trunc(CommonSet.DateBeg)));
          quRepSaleAP.SQL.Add('Set @IDATEE='+its(Trunc(CommonSet.DateEnd)));
          quRepSaleAP.SQL.Add('Set @SSKL='''+sDeps+'''');
          quRepSaleAP.SQL.Add('EXECUTE [dbo].[prRepSaleAP]  @SSKL,@IDATEB,@IDATEE');

          quRepSaleAP.Active:=True;

          fmRepSaleAP.Memo1.Lines.Add('  .. '+its(quRepSaleAP.RecordCount)); delay(10);
        finally
          fmRepSaleAP.ViewRepSaleAP.EndUpdate;
        end;

        fmRepSaleAP.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

end.
