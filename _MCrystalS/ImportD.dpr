program ImportD;

uses
  Forms,
  uImport in 'uImport.pas' {fmImpD},
  Un1 in 'Un1.pas',
  udmTmp in 'udmTmp.pas' {dmTMP: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmImpD, fmImpD);
  Application.CreateForm(TdmTMP, dmTMP);
  Application.Run;
end.
