object fmMainEXp: TfmMainEXp
  Left = 480
  Top = 113
  Width = 901
  Height = 509
  Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' 1'#1057
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 7
    Height = 13
    Caption = 'C'
  end
  object Label2: TLabel
    Left = 168
    Top = 24
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 456
    Width = 893
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Text = '14.01'
        Width = 300
      end>
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 72
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 384
    Width = 893
  end
  object DateEdit1: TcxDateEdit
    Left = 40
    Top = 16
    Style.Shadow = True
    TabOrder = 2
    Width = 113
  end
  object DateEdit2: TcxDateEdit
    Left = 192
    Top = 16
    Style.Shadow = True
    TabOrder = 3
    Width = 105
  end
  object cxCheckBox1: TcxCheckBox
    Left = 316
    Top = 48
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    State = cbsChecked
    TabOrder = 4
    Width = 121
  end
  object cxButton1: TcxButton
    Left = 320
    Top = 16
    Width = 97
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 5
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 40
    Top = 104
  end
  object am1: TActionManager
    Left = 160
    Top = 112
    StyleName = 'XP Style'
    object acStart: TAction
      Caption = 'acStart'
      OnExecute = acStartExecute
    end
  end
  object msConnection: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 'FILE NAME=C:\_MCrystalS\Bin\MCR.udl'
    ConnectionTimeout = 1800
    DefaultDatabase = 'MCRYSTAL'
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 173
  end
  object quShops: TADOQuery
    Connection = msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDS] FROM [dbo].[DEPARTS]'
      'where [IDS] >0'
      'group by [IDS]'
      'order by [IDS] ')
    Left = 116
    Top = 176
    object quShopsIDS: TIntegerField
      FieldName = 'IDS'
    end
  end
  object quSelData: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    Left = 184
    Top = 176
  end
end
