unit Scales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  ComCtrls, cxMemo, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, ActnList, XPStyleActnCtrls, ActnMan, ShellApi;

type
  TfmScale = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewScale: TcxGridDBTableView;
    LevelScale: TcxGridLevel;
    GridScale: TcxGrid;
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    ViewScalePLU: TcxGridDBColumn;
    ViewScaleGOODSITEM: TcxGridDBColumn;
    ViewScaleNAME1: TcxGridDBColumn;
    ViewScaleNAME2: TcxGridDBColumn;
    ViewScalePRICE: TcxGridDBColumn;
    ViewScaleShelfLife: TcxGridDBColumn;
    ViewScaleTareWeight: TcxGridDBColumn;
    ViewScaleStatus: TcxGridDBColumn;
    cxButton4: TcxButton;
    amScale: TActionManager;
    acEditPlu: TAction;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    ViewScaleMessageNo: TcxGridDBColumn;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    procedure cxButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acEditPluExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButton(bSt:Boolean);
  end;

function Transfer_Ethernet(S:PChar):Integer; export; stdcall; far; external 'TransferEth.dll' name 'Transfer_Ethernet';

var
  fmScale: TfmScale;

implementation

uses Un1, u2fdk, EditPlu, MainMC, Dm;

{$R *.dfm}

procedure TfmScale.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmScale.FormCreate(Sender: TObject);
begin
  GridScale.Align:=AlClient;
  FormPlacement1.IniFileName:=sFormIni;
  FormPlacement1.Active:=True;
  ViewScale.RestoreFromIniFile(sGridIni);
//  Memo1.Clear;
end;

procedure TfmScale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewScale.StoreToIniFile(sGridIni,False);
  if not cxButton3.Enabled then Action := caNone;
end;

procedure TfmScale.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmMCS do
  begin
    ViewScale.BeginUpdate;
    if quScaleList.Active then
      if quScaleList.RecordCount>0 then
      begin
        quScaleItems.Active:=False;
        quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
        quScaleItems.Active:=True;
      end;  
    ViewScale.EndUpdate;
  end;
end;

procedure TfmScale.cxButton4Click(Sender: TObject);
begin
  with dmMCS do
  begin
    if (quScaleList.RecordCount>0)and(cxLookupComboBox1.EditValue>0)then
    begin
      ViewScale.BeginUpdate;
      if quScaleList.RecordCount>0 then
      begin
        quScaleItems.Active:=False;
        quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
        quScaleItems.Active:=True;
      end;
      ViewScale.EndUpdate;
    end;
  end;
end;

procedure TfmScale.cxButton1Click(Sender: TObject);
Var n:INteger;
    StrIp:String;
    bErr:Boolean;
begin
  with dmMCS do
  begin
    Memo1.Clear;    //�������� ���������

    quScaleList.Locate('IDAI',cxLookupComboBox1.EditValue,[]);

    ViewScale.BeginUpdate;
    prButton(False);
    bErr:=False;
//    DrvScaleCas

    if (quScaleListSCALETYPE.AsString='CAS_TCP') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (CAS).'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=quScaleListIP1.AsString;
        if n=2 then StrIP:=quScaleListIP2.AsString;
        if n=3 then StrIP:=quScaleListIP3.AsString;
        if n=4 then StrIP:=quScaleListIP4.AsString;
        if n=5 then StrIP:=quScaleListIP5.AsString;

        if StrIp>'' then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+StrIp); delay(10);
          try
            if ScaleCas then
            begin
              try
                DrvScaleCas.ip := StrIP;
                DrvScaleCas.port := 8111;
 	              DrvScaleCas.Connect;
                Memo1.Lines.Add('    ����������� ��');

                quScaleItems.First;
                while not quScaleItems.Eof do
                begin
                  if quScaleItemsStatus.AsInteger<>1 then
                  begin
                    DrvScaleCas.groupcode := 22;
                    DrvScaleCas.ITEM := quScaleItemsGOODSITEM.AsInteger ;
                    DrvScaleCas.LIFE := quScaleItemsShelfLife.AsInteger;
                    DrvScaleCas.MSG := 1;

                    if length(quScaleItemsNAME1.AsString)>0 then DrvScaleCas.MSG1 := quScaleItemsNAME1.AsString else DrvScaleCas.MSG1 := '     ';
                    if length(quScaleItemsNAME2.AsString)>0 then DrvScaleCas.MSG2 := quScaleItemsNAME2.AsString else DrvScaleCas.MSG2 := '     ';

                    DrvScaleCas.NumberLogo := 1;
                    DrvScaleCas.PLUNO := quScaleItemsPLU.AsInteger;
                    DrvScaleCas.PRICE := RoundEx(quScaleItemsPRICE.AsFloat*100);
                    DrvScaleCas.StrLogo := '';
                    DrvScaleCas.TARE := 0;
                    DrvScaleCas.SendPLU;
                    Memo1.Lines.Add('  PLu = '+IntToStr(quScaleItemsPLU.AsInteger)+'  -  Ok');
                    delay(10);
                  end;
                  quScaleItems.Next;
                end;
          	   	DrvScaleCas.Disconnect;
              except
                Memo1.Lines.Add('    ������ ����������� � �����.');
                bErr:=True;
              end;
            end;
          except
          end;
        end; //����� IP
      end;
      if  bErr=False then
      begin
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('UPDATE [dbo].[SCALEPLU]');
        quE.SQL.Add('SET [Status] = 1');
        quE.SQL.Add('WHERE [SCALENUM]='+its(cxLookupComboBox1.EditValue));
        quE.SQL.Add('and [Status]<>1');
        quE.ExecSQL;
      end;
    end;

    quScaleItems.Active:=False;
    quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;

    prButton(True);
    Memo1.Lines.Add('�������� ����� ���������.');
  end;
end;

procedure TfmScale.prButton(bSt:Boolean);
begin
  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  cxButton3.Enabled:=bSt;
  cxButton4.Enabled:=bSt;
  cxButton5.Enabled:=bSt;
  cxButton6.Enabled:=bSt;
  cxButton7.Enabled:=bSt;
  cxButton8.Enabled:=bSt;
end;

procedure TfmScale.cxButton2Click(Sender: TObject);
Var n,i,j:INteger;
    StrIp:String;
    Rec:TcxCustomGridRecord;
    iPlu,iSrok,iItem:INteger;
    Name1,Name2:String;
    rPrice:Real;
    bErr:Boolean;
begin
  with dmMCS do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;

    Memo1.Clear;    //�������� ���������

    ViewScale.BeginUpdate;
    prButton(False);
    bErr:=False;

    quScaleList.Locate('IDAI',cxLookupComboBox1.EditValue,[]);

    if (quScaleListSCALETYPE.AsString='CAS_TCP') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (CAS).'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=quScaleListIP1.AsString;
        if n=2 then StrIP:=quScaleListIP2.AsString;
        if n=3 then StrIP:=quScaleListIP3.AsString;
        if n=4 then StrIP:=quScaleListIP4.AsString;
        if n=5 then StrIP:=quScaleListIP5.AsString;

        if StrIp>'' then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+StrIp); delay(10);
          try
            if ScaleCas then
            begin
              try
                DrvScaleCas.ip := StrIP;
                DrvScaleCas.port := 8111;
 	              DrvScaleCas.Connect;
                Memo1.Lines.Add('    ����������� ��');

                for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
                begin
                  Rec:=ViewScale.Controller.SelectedRecords[i];

                  iPlu:=0;
                  iSrok:=0;
                  iItem:=0;
                  Name1:='  ';
                  Name2:='  ';
                  rPrice:=0;
//                  bFormat:=0;

                  for j:=0 to Rec.ValueCount-1 do
                  begin
                    if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleGOODSITEM' then iItem:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleNAME1' then Name1:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleNAME2' then Name2:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScalePRICE' then rPrice:=Rec.Values[j];
//                    if ViewScale.Columns[j].Name='ViewScaleMessageNo' then bFormat:=Rec.Values[j];
                  end;

                  if iPlu>0 then
                  begin
                    try
                      DrvScaleCas.groupcode := 22;
                      DrvScaleCas.ITEM := iItem;
                      DrvScaleCas.LIFE := iSrok;
                      DrvScaleCas.MSG := 1;

                      if length(Name1)>0 then DrvScaleCas.MSG1 := Name1 else DrvScaleCas.MSG1 := '     ';
                      if length(Name2)>0 then DrvScaleCas.MSG2 := Name2 else DrvScaleCas.MSG2 := '     ';

                      DrvScaleCas.NumberLogo := 1;
                      DrvScaleCas.PLUNO := iPlu;
                      DrvScaleCas.PRICE := RoundEx(rPrice*100);
                      DrvScaleCas.StrLogo := '';
                      DrvScaleCas.TARE := 0;
                      DrvScaleCas.SendPLU;
                      Memo1.Lines.Add('  PLu = '+IntToStr(iPlu)+'  -  Ok');
                      delay(10);
                    except
                      bErr:=True;
                    end;
                  end;
                end;

          	   	DrvScaleCas.Disconnect;
              except
                Memo1.Lines.Add('    ������ ����������� � �����.');
                bErr:=True;
              end;
            end;
          except
          end;
        end; //����� IP
      end;
    end;

    {
    if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �����.'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP+' ���-�� ������� '+INtToStr(ViewScale.Controller.SelectedRecordCount)); delay(10);
          try
            iL:=0;

            for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewScale.Controller.SelectedRecords[i];

              iPlu:=0;
              iSrok:=0;
              sItem:='';
              Name1:='';
              Name2:='';
              rPrice:=0;
              bFormat:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleGoodsItem' then sItem:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleFirstName' then Name1:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleLastName' then Name2:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScalePrice' then rPrice:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleMessageNo' then bFormat:=Rec.Values[j];
              end;

              if iPlu>0 then
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  AddPlu(iL,iPlu,iSrok,sItem,AnsiToOemConvert(Name1),AnsiToOemConvert(Name2),rPrice,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                  delay(10);
                end;
              end;
            end;

            if iL>0 then
            begin
              if LoadScale(CommonSet.sMask+StrIP,iL) then  Memo1.Lines.Add('      ....')
              else
              begin
                bErr:=True;
                Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
              end;
              delay(10);
            end;
          except
            ViewScale.EndUpdate;
          end;
        end; //����� IP
      end;

    end;
    }
    if bErr=False then
    begin //����� ������ ����������
      for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewScale.Controller.SelectedRecords[i];

        iPlu:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewScale.Columns[j].Name='ViewScalePLU' then begin iPlu:=Rec.Values[j]; break; end;
        end;

        if iPlu>0 then
        begin
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('UPDATE [dbo].[SCALEPLU]');
          quE.SQL.Add('Set [Status]=1');
          quE.SQL.Add('WHERE [SCALENUM]='+its(cxLookupComboBox1.EditValue));
          quE.SQL.Add('and [PLU]='+its(iPlu));
          quE.ExecSQL;
        end;
      end;
    end;

    quScaleItems.Active:=False;
    quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;
    prButton(True);
    Memo1.Lines.Add('�������� ����� ���������.');
  end;
end;

procedure TfmScale.acEditPluExecute(Sender: TObject);
begin
  //������������� ���
  with dmMCS do
  begin
    if quScaleItems.RecordCount>0 then
    begin
      fmEditPlu:=tfmEditPlu.Create(Application);
      fmEditPlu.cxSpinEdit1.EditValue:=quScaleItemsPLU.AsInteger;
      fmEditPlu.cxSpinEdit2.EditValue:=quScaleItemsGOODSITEM.AsInteger;
      fmEditPlu.cxTextEdit2.Text:=quScaleItemsNAME1.AsString;
      fmEditPlu.cxTextEdit3.Text:=quScaleItemsNAME2.AsString;
      fmEditPlu.cxSpinEdit3.EditValue:=quScaleItemsShelfLife.AsInteger;
      fmEditPlu.cxCalcEdit1.EditValue:=quScaleItemsPRICE.AsFloat;
      fmEditPlu.cxComboBox1.ItemIndex:=quScaleItemsMessageNo.AsInteger;
      fmEditPlu.ShowModal;
      if fmEditPlu.ModalResult=mrOk then
      begin //���������
        //�������
        quScaleItems.Edit;
        quScaleItemsGOODSITEM.AsInteger:=fmEditPlu.cxSpinEdit2.EditValue;
        quScaleItemsNAME1.AsString:=fmEditPlu.cxTextEdit2.Text;
        quScaleItemsNAME2.AsString:=fmEditPlu.cxTextEdit3.Text;
        quScaleItemsPRICE.AsFloat:=rv(fmEditPlu.cxCalcEdit1.EditValue);
        quScaleItemsShelfLife.AsInteger:=fmEditPlu.cxSpinEdit3.EditValue;
        quScaleItemsTareWeight.Asinteger:=0;
        quScaleItemsStatus.AsInteger:=0;
        quScaleItems.Post;

{
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('[dbo].[SCALEPLU] SET ');
        quE.SQL.Add('[GOODSITEM]='+its(fmEditPlu.cxSpinEdit2.EditValue)+',');
        quE.SQL.Add('[NAME1]='''+fmEditPlu.cxTextEdit2.Text+''',');
        quE.SQL.Add('[NAME2]='''+fmEditPlu.cxTextEdit3.Text+''',');
        quE.SQL.Add('[PRICE]='+its(RoundEx(fmEditPlu.cxCalcEdit1.EditValue*100))+',');
        quE.SQL.Add('[ShelfLife]='+its(fmEditPlu.cxSpinEdit3.EditValue)+',');
        quE.SQL.Add('[MessageNo]='+its(fmEditPlu.cxComboBox1.ItemIndex)+',');
        quE.SQL.Add('[Status]=0');
        quE.SQL.Add('WHERE [SCALENUM]='+its(cxLookupComboBox1.EditValue));
        quE.SQL.Add('and [PLU]='+its(fmEditPlu.cxSpinEdit1.EditValue));
        quE.ExecSQL;}
      end;

      fmEditPlu.Release;
    end;
  end;
end;

procedure TfmScale.cxButton5Click(Sender: TObject);
begin
  prNExportExel6(ViewScale);
end;

procedure TfmScale.cxButton6Click(Sender: TObject);
Var iC,l:Integer;
    sN1,sN2,sN,s:String;
    bE:Boolean;
    iSkl:INteger;
begin
  with dmMCS do
  begin
    Memo1.Clear;    //�������� ���������

    Memo1.Lines.Add('����� ... ���� ���������� ���������� � ������ �� ��������.'); delay(10);
    ViewScale.BeginUpdate;

    prButton(False);

    iSkl:=fDepScale(cxLookupComboBox1.EditValue);

    iC:=0;
    quScaleItems.First;
    while not quScaleItems.Eof do
    begin
      quFCP.Active:=False;
      quFCP.SQL.Clear;
      quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
      quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
      quFCP.SQL.Add('where ca.[ID]='+its(quScaleItemsGOODSITEM.AsInteger));
      quFCP.SQL.Add('and [TTOVAR]=0');
      quFCP.Active:=True;

      if quFCP.RecordCount=1 then
      begin
        sN:=quFCPFULLNAME.AsString;
        sN1:=Copy(sN,1,29);
//        iFormat:=taCardsV09.AsInteger;
        l:=28;
        if (Pos(' ',sN1)>0) and(length(sN)>28) then
        begin
          s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
          while s<>' ' do
          begin
            dec(l);
            s:=sN1[l];
          end;
        end;

        sN1:=Copy(sN,1,l);
        if Length(sN)>l then sN2:=Copy(sN,(l+1),28) else sN2:='';

        bE:=False;

        sN1:=TrimRight(sN1);
        sN2:=TrimRight(sN2);

        if abs(quScaleItemsPRICE.AsFloat-quFCPPRICE.AsFloat)>=0.001 then bE:=True;
        if quFCPSROKGODN.AsInteger<>quScaleItemsShelfLife.AsInteger then bE:=True;
        if sN1<>quScaleItemsNAME1.AsString then bE:=True;
        if sN2<>quScaleItemsNAME2.AsString then bE:=True;

        if bE then
        begin //���� ����������
          quScaleItems.Edit;
          quScaleItemsNAME1.AsString:=sN1;
          quScaleItemsNAME2.AsString:=sN2;
          quScaleItemsPRICE.AsFloat:=quFCPPRICE.AsFloat;
          quScaleItemsShelfLife.AsInteger:=quFCPSROKGODN.AsInteger;
          quScaleItemsStatus.AsInteger:=0;
          quScaleItems.Post;
        end;
      end else
      begin
        quScaleItems.Edit;
        quScaleItemsNAME1.AsString:='�� ��������';
        quScaleItemsNAME2.AsString:='';
        quScaleItemsPRICE.AsFloat:=0;
        quScaleItemsShelfLife.AsInteger:=0;
        quScaleItemsStatus.AsInteger:=0;
        quScaleItems.Post;
        delay(30);
      end;

      quScaleItems.Next; inc(iC);
      if iC mod 100 = 0 then
      begin
        Memo1.Lines.Add('  ���������� - '+its(iC)); delay(100);
      end;
    end;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;

    prButton(True);
    Memo1.Lines.Add('���������� ���������.');
  end;
end;

procedure TfmScale.cxButton7Click(Sender: TObject);
Var i,j:INteger;
    Rec:TcxCustomGridRecord;
    iPlu:INteger;
    bErr:Boolean;
begin
   //�������
  with dmMCS do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;

    if MessageDlg('������� '+its(ViewScale.Controller.SelectedRecordCount)+' ������� �� ��������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;    //�������� ���������
      Memo1.Lines.Add('����� ... ���� ��������.'); delay(10);

      quScaleList.Locate('IDAI',cxLookupComboBox1.EditValue,[]);
      bErr:=False;

    {
    if (quScaleListSCALETYPE.AsString='CAS_TCP') then       // ������� �� ��������� ������� ������� �� ����� , ������ ��������������.
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (CAS).'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=quScaleListIP1.AsString;
        if n=2 then StrIP:=quScaleListIP2.AsString;
        if n=3 then StrIP:=quScaleListIP3.AsString;
        if n=4 then StrIP:=quScaleListIP4.AsString;
        if n=5 then StrIP:=quScaleListIP5.AsString;

        if StrIp>'' then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+StrIp); delay(10);
          try
            if ScaleCas then
            begin
              try
                DrvScaleCas.ip := StrIP;
                DrvScaleCas.port := 8111;
 	              DrvScaleCas.Connect;
                Memo1.Lines.Add('    ����������� ��');

                for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
                begin
                  Rec:=ViewScale.Controller.SelectedRecords[i];

                  iPlu:=0;
                  iSrok:=0;
                  iItem:=0;
                  Name1:='  ';
                  Name2:='  ';
                  rPrice:=0;
//                  bFormat:=0;

                  for j:=0 to Rec.ValueCount-1 do
                  begin
                    if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleGOODSITEM' then iItem:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleNAME1' then Name1:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScaleNAME2' then Name2:=Rec.Values[j];
                    if ViewScale.Columns[j].Name='ViewScalePRICE' then rPrice:=Rec.Values[j];
//                    if ViewScale.Columns[j].Name='ViewScaleMessageNo' then bFormat:=Rec.Values[j];
                  end;

                  if iPlu>0 then
                  begin
                    try
                      DrvScaleCas.groupcode := 22;
                      DrvScaleCas.ITEM := iItem;
                      DrvScaleCas.LIFE := iSrok;
                      DrvScaleCas.MSG := 1;

                      if length(Name1)>0 then DrvScaleCas.MSG1 := Name1 else DrvScaleCas.MSG1 := '     ';
                      if length(Name2)>0 then DrvScaleCas.MSG2 := Name2 else DrvScaleCas.MSG2 := '     ';

                      DrvScaleCas.NumberLogo := 1;
                      DrvScaleCas.PLUNO := iPlu;
                      DrvScaleCas.PRICE := RoundEx(rPrice*100);
                      DrvScaleCas.StrLogo := '';
                      DrvScaleCas.TARE := 0;
                      DrvScaleCas.SendPLU;
                      Memo1.Lines.Add('  PLu = '+IntToStr(iPlu)+'  -  Ok');
                      delay(10);
                    except
                      bErr:=True;
                    end;
                  end;
                end;

          	   	DrvScaleCas.Disconnect;
              except
                Memo1.Lines.Add('    ������ ����������� � �����.');
                bErr:=True;
              end;
            end;
          except
          end;
        end; //����� IP
      end;
    end;
    }

      if bErr=False then
      begin //����� ������ ����������
        for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewScale.Controller.SelectedRecords[i];

          iPlu:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewScale.Columns[j].Name='ViewScalePLU' then begin iPlu:=Rec.Values[j]; break; end;
          end;

          if iPlu>0 then
          begin
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Delete from [dbo].[SCALEPLU]');
            quE.SQL.Add('WHERE [SCALENUM]='+its(cxLookupComboBox1.EditValue));
            quE.SQL.Add('and [PLU]='+its(iPlu));
            quE.ExecSQL;
          end;
        end;
      end;

      quScaleItems.Active:=False;
      quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      ViewScale.EndUpdate;
      ViewScale.Controller.ClearSelection;
      prButton(True);
      Memo1.Lines.Add('�������� ���������.');
    end;

    {if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �� �����.'); delay(10);
      ViewScale.BeginUpdate;
      prButton(False);
      bErr:=False;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP+' ���-�� ������� '+INtToStr(ViewScale.Controller.SelectedRecordCount)); delay(10);
          try
            iL:=0;

            for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewScale.Controller.SelectedRecords[i];

              iPlu:=0;
              iSrok:=0;
              sItem:='';
              Name1:='';
              Name2:='';
              rPrice:=0;
              bFormat:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleGoodsItem' then sItem:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleFirstName' then Name1:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleLastName' then Name2:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScalePrice' then rPrice:=Rec.Values[j];
              end;

              if iPlu>0 then
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  AddPlu(iL,iPlu,iSrok,sItem,AnsiToOemConvert(Name1),AnsiToOemConvert(Name2),rPrice,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                  delay(10);
                end;
              end;
            end;

            if iL>0 then
            begin
              if LoadScale(CommonSet.sMask+StrIP,iL) then  Memo1.Lines.Add('      ....')
              else
              begin
                bErr:=True;
                Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
              end;
              delay(10);
            end;
          except
            ViewScale.EndUpdate;
          end;
        end; //����� IP
      end;
      if bErr=False then
      begin //����� ������ ����������  -  ������� �� ����� �� ������
        for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewScale.Controller.SelectedRecords[i];

          iPlu:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewScale.Columns[j].Name='ViewScalePLU' then begin iPlu:=Rec.Values[j]; break; end;
          end;

          if iPlu>0 then
          begin

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "ScalePLU"');
            quD.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(iPlu));
            quD.ExecSQL;

          end;
        end;
      end;

      quScaleItems.Active:=False;
      quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      ViewScale.EndUpdate;
      ViewScale.Controller.ClearSelection;

      prButton(True);
      Memo1.Lines.Add('�������� ���������.');
    end; }
  end;
end;

procedure TfmScale.cxButton8Click(Sender: TObject);
begin
  with dmMCS do
  begin
    Memo1.Clear;    //�������� �������� �������� � ��������� ����
    {
    quScaleList.Locate('Number',cxLookupComboBox1.EditValue,[]);
    if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �������� � ����.'); delay(10);
      prButton(False);
      bErr:=False;

      quScaleItems.Active:=False;
      quScaleItems.Parameters.ParamByName('INUM').Value:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP); delay(10);
          try
            if FileExists(CurDir+'twswtcp.exe') then
            begin
              Memo1.Lines.Add('  ������� "twswtcp.exe" ������.'); delay(10);
              if FileExists(CommonSet.TrfPath+'f34_1.dat') then
              begin
                Memo1.Lines.Add('  ������ "'+CommonSet.TrfPath+'f34_1.dat'+'" ������.'); delay(10);
                if FileExists(CommonSet.TrfPath+'f34_2.dat') then
                begin
                  Memo1.Lines.Add('  ������ "'+CommonSet.TrfPath+'f34_2.dat'+'" ������.'); delay(10);

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIp+'f34.dat');

                  Memo1.Lines.Add('  �������� "'+CommonSet.TrfPath+'f34_1.dat'+'" � '+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  CopyFile(PChar(CommonSet.TrfPath+'f34_1.dat'),PChar(CurDir+'sm'+StrIp+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIp),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIp+'f34.dat');
                  Memo1.Lines.Add('  �������� "'+CommonSet.TrfPath+'f34_2.dat'+'" � '+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  CopyFile(PChar(CommonSet.TrfPath+'f34_2.dat'),PChar(CurDir+'sm'+StrIp+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIp),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                end else
                begin
                  Memo1.Lines.Add('  ������ - ������ "'+CommonSet.TrfPath+'f34_2.dat'+'" �� ������.'); delay(10);
                  bErr:=True;
                end;
              end else
              begin
                Memo1.Lines.Add('  ������ - ������ "'+CommonSet.TrfPath+'f34_1.dat'+'" �� ������.'); delay(10);
                bErr:=True;
              end;
            end else
            begin
              Memo1.Lines.Add('  ������ - ������� "'+CurDir+'twswtcp.exe" �� ������.'); delay(10);
              bErr:=True;
            end;
          except
            bErr:=True;
          end;
        end; //����� IP
      end;
//      Memo1.Lines.Add('');
      prButton(True);
      if bErr=False then
        Memo1.Lines.Add('�������� �������� �������� ���������.')
      else
        Memo1.Lines.Add('������ ��� ��������� �������� ��������.');
    end;}
  end;
end;




procedure TfmScale.cxButton9Click(Sender: TObject);
begin
  Memo1.Lines.Add('��������');
  {
  //��������� ��������
  try
    if FileExists(curdir+'TransferPLU.exe') then
    begin
      ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','', SW_SHOWNORMAL);
    end else
      showmessage('�� ���� ����� TransferPLU.exe. ���������� � ��������������.');
  finally
  end;}
  Memo1.Lines.Add('�������� ���������');
end;

end.
