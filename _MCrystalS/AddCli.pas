unit AddCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ComCtrls, StdCtrls, cxButtons,
  ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxGraphics,
  cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCalc, cxListView,
  cxGroupBox, DB, ADODB, cxSpinEdit;

type
  TfmAddCli = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxImageComboBox1: TcxImageComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label6: TLabel;
    cxTextEdit5: TcxTextEdit;
    Label7: TLabel;
    cxTextEdit6: TcxTextEdit;
    Label8: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label9: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cxTextEdit9: TcxTextEdit;
    cxTextEdit10: TcxTextEdit;
    cxTextEdit11: TcxTextEdit;
    cxTextEdit12: TcxTextEdit;
    cxTextEdit13: TcxTextEdit;
    cxTextEdit14: TcxTextEdit;
    GroupBox1: TGroupBox;
    cxImageComboBox2: TcxImageComboBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    cxTextEdit18: TcxTextEdit;
    cxTextEdit19: TcxTextEdit;
    cxCheckBox2: TcxCheckBox;
    Label24: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCalcEdit1: TcxCalcEdit;
    Label25: TLabel;
    quEDIProv: TADOQuery;
    dsquEDIProv: TDataSource;
    quEDIProvId: TAutoIncField;
    quEDIProvName: TStringField;
    quEDIProvHostIPEDI: TStringField;
    quEDIProvHostUserEDI: TStringField;
    quEDIProvHostPasswEDI: TStringField;
    quEDIProvDestEDI: TStringField;
    quEDIProvInpEDIOrderSp: TStringField;
    quEDIProvInpEDIRep: TStringField;
    quEDIProvInpEDINacl: TStringField;
    quEDIProvIActive: TSmallintField;
    quEDIProvweblink: TMemoField;
    quEDIProvmailsd: TMemoField;
    quEDIProvphone: TMemoField;
    quEDIProvDestEDIRecadv: TStringField;
    quEDIProvInpEDIESF: TStringField;
    quEDIProvDestEDIESF: TStringField;
    quEDIProvDespEDIBonus: TStringField;
    quEDIProvInpEDIBonus: TStringField;
    quEDIProvDestEDIPretenzia: TStringField;
    quEDIProvInpEDIPretenzia: TStringField;
    quEDIProvDestEDIPricat: TStringField;
    quEDIProvInpEDIPricat: TStringField;
    quEDIProvDestEDIPartin: TStringField;
    quEDIProvInpEDIPartin: TStringField;
    quEDIProvDestEDICoacsu: TStringField;
    quEDIProvInpEDICoacsu: TStringField;
    quEDIProvDestEDIOrderSp: TStringField;
    quEDIProvDestEDINacl: TStringField;
    quEDIProvGLN: TStringField;
    quEDIProvDestEDIStatus: TStringField;
    quEDIProvSendEDIStatus: TSmallintField;
    Label16: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prSetEditCli(iT:SmallInt);
  end;

var
  fmAddCli: TfmAddCli;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmAddCli.prSetEditCli(iT:SmallInt);
begin
  if iT=0 then
  begin
    fmAddCli.Tag:=0;

    quEDIProv.Active:=False;
    quEDIProv.Active:=True;
    quEDIProv.First;

    cxTextEdit1.Text:=''; cxTextEdit1.Properties.ReadOnly:=False;   //��������
    cxTextEdit2.Text:=''; cxTextEdit2.Properties.ReadOnly:=False;    //������ ��������
    cxTextEdit3.Text:=''; cxTextEdit3.Properties.ReadOnly:=False;  //���
    cxTextEdit4.Text:=''; cxTextEdit4.Properties.ReadOnly:=False; //���
    cxTextEdit5.Text:=''; cxTextEdit5.Properties.ReadOnly:=False;  //������
    cxTextEdit6.Text:=''; cxTextEdit6.Properties.ReadOnly:=False;   //�����
    cxTextEdit7.Text:=''; cxTextEdit7.Properties.ReadOnly:=False;  //�����
    cxTextEdit8.Text:=''; cxTextEdit8.Properties.ReadOnly:=False;  //���
    cxTextEdit9.Text:=''; cxTextEdit9.Properties.ReadOnly:=False;   //���������������
    cxTextEdit10.Text:=''; cxTextEdit10.Properties.ReadOnly:=False;  //����� ���������������
    cxTextEdit11.Text:=''; cxTextEdit11.Properties.ReadOnly:=False;  //��������� ����
    cxTextEdit12.Text:=''; cxTextEdit12.Properties.ReadOnly:=False;   //���
    cxTextEdit13.Text:=''; cxTextEdit13.Properties.ReadOnly:=False;  //����
    cxTextEdit14.Text:=''; cxTextEdit14.Properties.ReadOnly:=False;  //��� ����
    cxTextEdit18.Text:=''; cxTextEdit18.Properties.ReadOnly:=False;  //���
    cxTextEdit19.Text:=''; cxTextEdit19.Properties.ReadOnly:=False; //�����-��

    cxImageComboBox1.ItemIndex:=0; cxImageComboBox1.Properties.ReadOnly:=False;  //����������� ����
    cxImageComboBox2.ItemIndex:=0; cxImageComboBox2.Properties.ReadOnly:=False;  //��� ������ ��
    cxLookupComboBox1.EditValue:=quEDIProvId.AsInteger; cxLookupComboBox1.Properties.ReadOnly:=False; //EDI ���������

    cxCheckBox2.Checked:=True; cxCheckBox2.Properties.ReadOnly:=False;
//    cxCheckBox5.Checked:=False; cxCheckBox5.Properties.ReadOnly:=False;
//    cxCheckBox7.Checked:=False; cxCheckBox7.Properties.ReadOnly:=False;

    cxCalcEdit1.Value:=0; cxCalcEdit1.Properties.ReadOnly:=False;
    cxSpinEdit1.Value:=0; cxSpinEdit1.Properties.ReadOnly:=False;
  end;
  if iT=1 then //������������
  begin
    with dmMCS do
    begin
      fmAddCli.Tag:=quClientsId.AsInteger;

      quEDIProv.Active:=False;
      quEDIProv.Active:=True;
      quEDIProv.Locate('Id',quClientsEDIProvider.AsInteger,[]);

      cxTextEdit1.Text:=quClientsName.AsString;      cxTextEdit1.Properties.ReadOnly:=False;   //��������
      cxTextEdit2.Text:=quClientsFullName.AsString;  cxTextEdit2.Properties.ReadOnly:=False;    //������ ��������
      cxTextEdit3.Text:=quClientsINN.AsString;       cxTextEdit3.Properties.ReadOnly:=False;  //���
      cxTextEdit4.Text:=quClientsKPP.AsString;       cxTextEdit4.Properties.ReadOnly:=False; //���
      cxTextEdit5.Text:=quClientsPostIndex.AsString; cxTextEdit5.Properties.ReadOnly:=False;  //������
      cxTextEdit6.Text:=quClientsGorod.AsString;     cxTextEdit6.Properties.ReadOnly:=False;   //�����
      cxTextEdit7.Text:=quClientsStreet.AsString;    cxTextEdit7.Properties.ReadOnly:=False;  //�����
      cxTextEdit8.Text:=quClientsHouse.AsString;     cxTextEdit8.Properties.ReadOnly:=False;  //���
      cxTextEdit9.Text:=quClientsNAMEOTP.AsString;   cxTextEdit9.Properties.ReadOnly:=False;   //���������������
      cxTextEdit10.Text:=quClientsADROTPR.AsString;  cxTextEdit10.Properties.ReadOnly:=False;  //����� ���������������
      cxTextEdit11.Text:=quClientsRSch.AsString;     cxTextEdit11.Properties.ReadOnly:=False;  //��������� ����
      cxTextEdit12.Text:=quClientsBik.AsString;      cxTextEdit12.Properties.ReadOnly:=False;   //���
      cxTextEdit13.Text:=quClientsBank.AsString;     cxTextEdit13.Properties.ReadOnly:=False;  //����
      cxTextEdit14.Text:=quClientsKSch.AsString;     cxTextEdit14.Properties.ReadOnly:=False;  //��� ����
      cxTextEdit18.Text:=quClientsGln.AsString;      cxTextEdit18.Properties.ReadOnly:=False;  //���
      cxTextEdit19.Text:=quClientsEmail.AsString;    cxTextEdit19.Properties.ReadOnly:=False; //�����-��

      cxImageComboBox1.ItemIndex:=quClientsIType.AsInteger;  cxImageComboBox1.Properties.ReadOnly:=False;  //����������� ����
      cxImageComboBox2.ItemIndex:=quClientsZakazT.AsInteger; cxImageComboBox2.Properties.ReadOnly:=False;  //��� ������ ��
      cxLookupComboBox1.EditValue:=quEDIProvId.AsInteger;    cxLookupComboBox1.Properties.ReadOnly:=False; //EDI ���������

      if quClientsPayNDS.AsInteger=0 then cxCheckBox2.Checked:=False else cxCheckBox2.Checked:=True;
      cxCheckBox2.Properties.ReadOnly:=False;  //���������� ���

//      if quClientsNoNaclZ.AsInteger=0 then cxCheckBox5.Checked:=False else cxCheckBox5.Checked:=True;
//      cxCheckBox5.Properties.ReadOnly:=False; // ��� ���������

//      if quClientsNoRecadv.AsInteger=0 then cxCheckBox7.Checked:=False else cxCheckBox7.Checked:=True;
//      cxCheckBox7.Properties.ReadOnly:=False; // �� ���������� RECADV

      cxCalcEdit1.Value:=quClientsMinSumZak.AsFloat; cxCalcEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=quClientsDayZ.AsInteger; cxSpinEdit1.Properties.ReadOnly:=False;
    end;
  end;
  if iT=2 then // ��������
  begin
    with dmMCS do
    begin
      fmAddCli.Tag:=quClientsId.AsInteger;

      quEDIProv.Active:=False;
      quEDIProv.Active:=True;
      quEDIProv.Locate('Id',quClientsEDIProvider.AsInteger,[]);

      cxTextEdit1.Text:=quClientsName.AsString;      cxTextEdit1.Properties.ReadOnly:=True;   //��������
      cxTextEdit2.Text:=quClientsFullName.AsString;  cxTextEdit2.Properties.ReadOnly:=True;    //������ ��������
      cxTextEdit3.Text:=quClientsINN.AsString;       cxTextEdit3.Properties.ReadOnly:=True;  //���
      cxTextEdit4.Text:=quClientsKPP.AsString;       cxTextEdit4.Properties.ReadOnly:=True; //���
      cxTextEdit5.Text:=quClientsPostIndex.AsString; cxTextEdit5.Properties.ReadOnly:=True;  //������
      cxTextEdit6.Text:=quClientsGorod.AsString;     cxTextEdit6.Properties.ReadOnly:=True;   //�����
      cxTextEdit7.Text:=quClientsStreet.AsString;    cxTextEdit7.Properties.ReadOnly:=True;  //�����
      cxTextEdit8.Text:=quClientsHouse.AsString;     cxTextEdit8.Properties.ReadOnly:=True;  //���
      cxTextEdit9.Text:=quClientsNAMEOTP.AsString;   cxTextEdit9.Properties.ReadOnly:=True;   //���������������
      cxTextEdit10.Text:=quClientsADROTPR.AsString;  cxTextEdit10.Properties.ReadOnly:=True;  //����� ���������������
      cxTextEdit11.Text:=quClientsRSch.AsString;     cxTextEdit11.Properties.ReadOnly:=True;  //��������� ����
      cxTextEdit12.Text:=quClientsBik.AsString;      cxTextEdit12.Properties.ReadOnly:=True;   //���
      cxTextEdit13.Text:=quClientsBank.AsString;     cxTextEdit13.Properties.ReadOnly:=True;  //����
      cxTextEdit14.Text:=quClientsKSch.AsString;     cxTextEdit14.Properties.ReadOnly:=True;  //��� ����
      cxTextEdit18.Text:=quClientsGln.AsString;      cxTextEdit18.Properties.ReadOnly:=True;  //���
      cxTextEdit19.Text:=quClientsEmail.AsString;    cxTextEdit19.Properties.ReadOnly:=True; //�����-��

      cxImageComboBox1.ItemIndex:=quClientsIType.AsInteger;  cxImageComboBox1.Properties.ReadOnly:=True;  //����������� ����
      cxImageComboBox2.ItemIndex:=quClientsZakazT.AsInteger; cxImageComboBox2.Properties.ReadOnly:=True;  //��� ������ ��
      cxLookupComboBox1.EditValue:=quEDIProvId.AsInteger;    cxLookupComboBox1.Properties.ReadOnly:=True; //EDI ���������

      if quClientsPayNDS.AsInteger=0 then cxCheckBox2.Checked:=False else cxCheckBox2.Checked:=True;
      cxCheckBox2.Properties.ReadOnly:=True;  //���������� ���

//      if quClientsNoNaclZ.AsInteger=0 then cxCheckBox5.Checked:=False else cxCheckBox5.Checked:=True;
//      cxCheckBox5.Properties.ReadOnly:=True; // ��� ���������

//      if quClientsNoRecadv.AsInteger=0 then cxCheckBox7.Checked:=False else cxCheckBox7.Checked:=True;
//      cxCheckBox7.Properties.ReadOnly:=True; // �� ���������� RECADV

      cxCalcEdit1.Value:=quClientsMinSumZak.AsFloat; cxCalcEdit1.Properties.ReadOnly:=True;
      cxSpinEdit1.Value:=quClientsDayZ.AsInteger; cxSpinEdit1.Properties.ReadOnly:=True;
    end;
  end;
end;

procedure TfmAddCli.cxButton1Click(Sender: TObject);
Var bExit:Boolean;
begin
  bExit:=True;
  if trim(cxTextEdit3.Text)='' then
  begin
   bExit:=False;
   ShowMessage('��������� ���. ���������� ����������!');
   abort;
  end;
  if cxImageComboBox2.EditValue=1 then
  begin
    if Length(cxTextEdit18.Text)<13 then
    begin
      bExit:=False;
      ShowMessage('��������� ���. ���������� ����������!');
      abort;
    end;
   ModalResult:=mrOk;
  end;

  if bExit then  ModalResult:=mrOk else ModalResult:=mrNone;
end;

end.
