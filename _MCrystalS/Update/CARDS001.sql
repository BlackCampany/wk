/*
   25 июня 2014 г.10:46:52
   Пользователь: sa
   Сервер: SYS4
   База данных: MCRYSTAL
   Приложение: 
*/

/* Чтобы предотвратить возможность потери данных, необходимо внимательно просмотреть этот сценарий, прежде чем запускать его вне контекста конструктора баз данных.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CARDS ADD
	SOSTAV varchar(200) NULL,
	GOST varchar(200) NULL
GO
ALTER TABLE dbo.CARDS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
