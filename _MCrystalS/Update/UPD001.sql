USE [MCRYSTAL]
GO

/****** Object:  UserDefinedFunction [dbo].[getNewArt]    Script Date: 06/25/2014 10:54:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<�������� ������ �������������>
-- Create date: <15.05.2014>
-- Description:	<������� ���� ���� � ��������� Id �������>
-- =============================================
CREATE FUNCTION [dbo].[getNewArt] 
(@Id1 int,
 @Id2 int
)
RETURNS int
AS
BEGIN
declare @iItem int
select top 1 @iItem = a.num from dbo.DIAPASON(@Id1,@Id2) a 
left join CARDS g on g.ID = a.num
where g.ID is null
order by num
RETURN @iItem
END


GO


