object fmDeparts: TfmDeparts
  Left = 373
  Top = 233
  Width = 955
  Height = 567
  Caption = #1052#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103' ('#1054#1090#1076#1077#1083#1099')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridDeps: TcxGrid
    Left = 18
    Top = 52
    Width = 901
    Height = 337
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewDeps: TcxGridDBTableView
      OnDblClick = ViewDepsDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquDeps
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLIQUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLIQUANTN'
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANTZAUTO'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLISUMIN'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLISUMIN0'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLISUMIN0N'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'CLISUMINN'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'INSUMIN'
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'INSUMIN0'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'INSUMIN'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'INSUMIN0'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'CLIQUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'CLIQUANTN'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'CLISUMIN'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'CLISUMIN0'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'CLISUMIN0N'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'CLISUMINN'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      object ViewDepsNAMESH: TcxGridDBColumn
        Caption = #1052#1072#1075#1072#1079#1080#1085
        DataBinding.FieldName = 'NAMESH'
        Width = 95
      end
      object ViewDepsID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'ID'
      end
      object ViewDepsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 156
      end
      object ViewDepsFULLNAME: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' ('#1076#1083#1103' '#1094#1077#1085#1085#1080#1082#1086#1074')'
        DataBinding.FieldName = 'FULLNAME'
        Width = 219
      end
      object ViewDepsISTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1040#1082#1090#1080#1074#1085#1099#1081
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1053#1077#1072#1082#1090#1080#1074#1085#1099#1081
            ImageIndex = 0
            Value = 0
          end>
      end
      object ViewDepsGLN: TcxGridDBColumn
        Caption = #1043#1051#1053
        DataBinding.FieldName = 'GLN'
      end
      object ViewDepsISS: TcxGridDBColumn
        Caption = #1062#1077#1085#1099' '#1091#1095#1077#1090#1072
        DataBinding.FieldName = 'ISS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1059#1095#1077#1090' '#1074' '#1087#1088#1086#1076#1072#1078#1085#1099#1093' '#1094#1077#1085#1072#1093
            ImageIndex = 0
            Value = 2
          end
          item
            Description = #1059#1095#1077#1090' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093' ('#1085#1077' '#1087#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057')'
            Value = 0
          end
          item
            Description = #1059#1095#1077#1090' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093' ('#1087#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057')'
            Value = 1
          end>
      end
      object ViewDepsIP: TcxGridDBColumn
        Caption = #1048#1055
        DataBinding.FieldName = 'IP'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1047#1040#1054' ('#1054#1054#1054')'
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1048#1055
            Value = 1
          end>
      end
    end
    object LevelDeps: TcxGridLevel
      GridView = ViewDeps
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 939
    Height = 46
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16763541
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1500100010001000150015001000
        10001F7C1F7C1F7C1F7C1F7C1F7C1F7C15007F2D1F00BF147F2DBF147F2D1F00
        7F2D10001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F00BF141F001F001F21BF14
        1F0015001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF147F2DBF147F2D
        BF1415001F7C1F7C1F7C1F7C1F7C1F7C10007F2DBF147F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C15001F001F001F00BF14BF141F007F2D
        7F2D15001F7C1F7C1F7C1F7C1F7C1F7C7F2D1F001F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C6003C001E00060037F2DBF141F001F001F21BF14BF14
        7F2D10001F7C1F7C1F7CE000F75FF75FE0001F001F00BF141F00BF14BF14BF14
        BF1415001F7C6003C001C001F75FF75FC001E00060037F2DBF147F2DBF147F2D
        7F2D10001F7CC001E313F75FF75FF75FF75F6003E0007F2D1500100010001000
        15001F7C1F7CC001FF67FF67E313F75FE313E313C0011F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CE31360036003ED3BE313C001C001E3131F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C6003ED3BED3BC0011F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7CED3BE313E313ED3B1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acEdit
      BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1000100010001000150015001000
        10001F7C1F7C1F7C0000000000001F7C15007F2D1F007F2D7F2DBF147F2D1F00
        7F2D10001F7C1F7C42082129C241000015001F001F001F21BF141F001F21BF14
        1F0015001F7C1F7C10426277036FC56A4A291F007F2D1F00BF147F2DBF147F2D
        BF1415001F7C1F7CD65AA17B4277E46E00001F001F007F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1042817B2373C46A4A291F007F2DBF14BF141F007F2D
        7F2D15001F7C1F7C1F7CD65AC07F6277036F00001F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C1F7C1042A17B4277E46E4A297F2D1F001F21BF14BF14
        7F2D10001F7C1F7C1F7C1F7CD65AE07F817B237300001F001F00BF14BF14BF14
        BF1415001F7C1F7C1F7C1F7C1F7C1042C07F000000000000BF14BF14BF141F00
        1F0010001F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200001000100010001000
        15001F7C1F7C1F7C1F7C1F7C1F7C1F7C10423967524A10424A291F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042104210421F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Spacing = 1
      Left = 84
      Top = 4
      Visible = True
      OnClick = acEditExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1500100010001000150015001000
        10001F7C1F7C1F7C1F7C1F7C1F7C1F7C15007F2D1F00BF147F2DBF147F2D1F00
        7F2D10001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F00BF141F001F001F21BF14
        1F0015001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF147F2DBF147F2D
        BF1415001F7C1F7C1F7C1F7C1F7C1F7C10007F2DBF147F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF14BF141F007F2D
        7F2D15001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C15001F00BF141F001F001F21BF14BF14
        7F2D10001F7C1F7C1F7C007C007CC67CC67CC67CCE7DBF141F00BF14BF14BF14
        BF1415001F7C1F7C0048C67CCE7DB57EB57EB57ECE7D007CBF147F2DBF147F2D
        7F2D10001F7C1F7C007CCE7DB57EB57EB57ECE7DC67C00481000100010001000
        15001F7C1F7C1F7C1F7CCE7DC67CC67CC67C007C007C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100
      Spacing = 1
      Left = 254
      Top = 4
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Spacing = 1
      Left = 344
      Top = 4
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8000080
        0000800000800000A80000A80000800000800000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA80000FF5B5BFF0000FF2D2DFF5B5BFF2D2DFF5B5BFF00
        00FF5B5B800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF5B5BFF
        0000FF2D2DFF0000FF0000FF4646FF2D2DFF0000A80000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF800000FF0000FF0000FF0000FF2D2DFF5B5BFF2D2DFF5B
        5BFF2D2DA80000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF5B5BFF
        2D2DFF5B5BFF0000FF0000FF0000FF2D2DFF2D2D800000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA80000FF0000FF0000FF0000FF2D2DFF2D2DFF0000FF5B
        5BFF5B5BA80000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B5BFF0000FF
        0000FF0000FF2D2DFF0000FF0000FF0000FF2D2D800000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF800000FF5B5BFF2D2DFF0000FF0000FF4646FF2D2DFF2D
        2DFF5B5B800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF0000FF
        0000FF2D2DFF0000FF2D2DFF2D2DFF2D2DFF2D2DA80000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA80000FF5B5BFF5B5BFF5B5BFF2D2DFF5B5BFF2D2DFF5B
        5BFF5B5B800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000A8
        0000FF5B5BA80000800000800000800000A80000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088
      Spacing = 1
      Left = 144
      Top = 4
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 452
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    Height = 77
    Width = 939
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 304
    Top = 152
  end
  object quDeps: TADOQuery
    Connection = dmMCS.msConnection
    CursorType = ctStatic
    CommandTimeout = 1000
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDS]'
      '      ,[ID]'
      '      ,[NAME]'
      '      ,[FULLNAME]'
      '      ,[ISTATUS]'
      '      ,[IDORG]'
      '      ,[GLN]'
      '      ,[ISS]'
      '      ,[PRIOR]'
      '      ,[PLATNDS]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[DIR1]'
      '      ,[DIR2]'
      '      ,[DIR3]'
      '      ,[GB1]'
      '      ,[GB2]'
      '      ,[GB3]'
      '      ,[CITY]'
      '      ,[STREET]'
      '      ,[HOUSE]'
      '      ,[KORP]'
      '      ,[POSTINDEX]'
      '      ,[PHONE]'
      '      ,[SERLIC]'
      '      ,[NUMLIC]'
      '      ,[ORGAN]'
      '      ,[DATEB]'
      '      ,[DATEE]'
      '      ,[IP]'
      '      ,[IPREG]'
      '      ,[NAMEOTPR]'
      '      ,[ADROPR]'
      '      ,[RSCH]'
      '      ,[KSCH]'
      '      ,[BIK]'
      '      ,[BANK]'
      '      ,NAMESH=(Select Name from [dbo].[SHOPS] where [ID]=[IDS])'
      '  FROM [dbo].[DEPARTS]')
    Left = 208
    Top = 153
    object quDepsIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quDepsID: TIntegerField
      FieldName = 'ID'
    end
    object quDepsNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object quDepsFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 150
    end
    object quDepsISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object quDepsIDORG: TIntegerField
      FieldName = 'IDORG'
    end
    object quDepsGLN: TStringField
      FieldName = 'GLN'
      Size = 15
    end
    object quDepsISS: TSmallintField
      FieldName = 'ISS'
    end
    object quDepsPRIOR: TSmallintField
      FieldName = 'PRIOR'
    end
    object quDepsINN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object quDepsKPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
    object quDepsDIR1: TStringField
      FieldName = 'DIR1'
      Size = 50
    end
    object quDepsDIR2: TStringField
      FieldName = 'DIR2'
      Size = 50
    end
    object quDepsDIR3: TStringField
      FieldName = 'DIR3'
      Size = 50
    end
    object quDepsGB1: TStringField
      FieldName = 'GB1'
      Size = 50
    end
    object quDepsGB2: TStringField
      FieldName = 'GB2'
      Size = 50
    end
    object quDepsGB3: TStringField
      FieldName = 'GB3'
      Size = 50
    end
    object quDepsCITY: TStringField
      FieldName = 'CITY'
      Size = 50
    end
    object quDepsSTREET: TStringField
      FieldName = 'STREET'
      Size = 50
    end
    object quDepsHOUSE: TStringField
      FieldName = 'HOUSE'
      Size = 10
    end
    object quDepsKORP: TStringField
      FieldName = 'KORP'
      Size = 5
    end
    object quDepsPOSTINDEX: TStringField
      FieldName = 'POSTINDEX'
      Size = 10
    end
    object quDepsPHONE: TStringField
      FieldName = 'PHONE'
    end
    object quDepsSERLIC: TStringField
      FieldName = 'SERLIC'
      Size = 50
    end
    object quDepsNUMLIC: TStringField
      FieldName = 'NUMLIC'
      Size = 50
    end
    object quDepsORGAN: TStringField
      FieldName = 'ORGAN'
      Size = 200
    end
    object quDepsDATEB: TDateTimeField
      FieldName = 'DATEB'
    end
    object quDepsDATEE: TDateTimeField
      FieldName = 'DATEE'
    end
    object quDepsIP: TSmallintField
      FieldName = 'IP'
    end
    object quDepsIPREG: TStringField
      FieldName = 'IPREG'
      Size = 200
    end
    object quDepsNAMESH: TStringField
      FieldName = 'NAMESH'
      ReadOnly = True
      Size = 100
    end
    object quDepsPLATNDS: TSmallintField
      FieldName = 'PLATNDS'
    end
    object quDepsNAMEOTPR: TStringField
      FieldName = 'NAMEOTPR'
      Size = 200
    end
    object quDepsADROPR: TStringField
      FieldName = 'ADROPR'
      Size = 200
    end
    object quDepsRSCH: TStringField
      FieldName = 'RSCH'
      Size = 30
    end
    object quDepsKSCH: TStringField
      FieldName = 'KSCH'
      Size = 30
    end
    object quDepsBIK: TStringField
      FieldName = 'BIK'
      Size = 30
    end
    object quDepsBANK: TStringField
      FieldName = 'BANK'
      Size = 200
    end
  end
  object dsquDeps: TDataSource
    DataSet = quDeps
    Left = 208
    Top = 204
  end
  object amDeps: TActionManager
    Left = 304
    Top = 204
    StyleName = 'XP Style'
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ShortCut = 115
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 16430
      OnExecute = acDelExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ShortCut = 114
      OnExecute = acViewExecute
    end
  end
  object quDepId: TADOQuery
    Connection = dmMCS.msConnection
    CursorType = ctStatic
    CommandTimeout = 1000
    Parameters = <
      item
        Name = 'IDMH'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [IDS]'
      '      ,[ID]'
      '      ,[NAME]'
      '      ,[FULLNAME]'
      '      ,[ISTATUS]'
      '      ,[IDORG]'
      '      ,[GLN]'
      '      ,[ISS]'
      '      ,[PRIOR]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[DIR1]'
      '      ,[DIR2]'
      '      ,[DIR3]'
      '      ,[GB1]'
      '      ,[GB2]'
      '      ,[GB3]'
      '      ,[CITY]'
      '      ,[STREET]'
      '      ,[HOUSE]'
      '      ,[KORP]'
      '      ,[POSTINDEX]'
      '      ,[PHONE]'
      '      ,[SERLIC]'
      '      ,[NUMLIC]'
      '      ,[ORGAN]'
      '      ,[DATEB]'
      '      ,[DATEE]'
      '      ,[IP]'
      '      ,[IPREG]'
      '      ,[NAMEOTPR]'
      '      ,[ADROPR]'
      '      ,[RSCH]'
      '      ,[KSCH]'
      '      ,[BIK]'
      '      ,[BANK]'
      '  FROM [dbo].[DEPARTS]'
      'where [ID]=:IDMH')
    Left = 208
    Top = 269
    object quDepIdIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quDepIdID: TIntegerField
      FieldName = 'ID'
    end
    object quDepIdNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object quDepIdFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 150
    end
    object quDepIdISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object quDepIdIDORG: TIntegerField
      FieldName = 'IDORG'
    end
    object quDepIdGLN: TStringField
      FieldName = 'GLN'
      Size = 15
    end
    object quDepIdISS: TSmallintField
      FieldName = 'ISS'
    end
    object quDepIdPRIOR: TSmallintField
      FieldName = 'PRIOR'
    end
    object quDepIdINN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object quDepIdKPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
    object quDepIdDIR1: TStringField
      FieldName = 'DIR1'
      Size = 50
    end
    object quDepIdDIR2: TStringField
      FieldName = 'DIR2'
      Size = 50
    end
    object quDepIdDIR3: TStringField
      FieldName = 'DIR3'
      Size = 50
    end
    object quDepIdGB1: TStringField
      FieldName = 'GB1'
      Size = 50
    end
    object quDepIdGB2: TStringField
      FieldName = 'GB2'
      Size = 50
    end
    object quDepIdGB3: TStringField
      FieldName = 'GB3'
      Size = 50
    end
    object quDepIdCITY: TStringField
      FieldName = 'CITY'
      Size = 50
    end
    object quDepIdSTREET: TStringField
      FieldName = 'STREET'
      Size = 50
    end
    object quDepIdHOUSE: TStringField
      FieldName = 'HOUSE'
      Size = 10
    end
    object quDepIdKORP: TStringField
      FieldName = 'KORP'
      Size = 5
    end
    object quDepIdPOSTINDEX: TStringField
      FieldName = 'POSTINDEX'
      Size = 10
    end
    object quDepIdPHONE: TStringField
      FieldName = 'PHONE'
    end
    object quDepIdSERLIC: TStringField
      FieldName = 'SERLIC'
      Size = 50
    end
    object quDepIdNUMLIC: TStringField
      FieldName = 'NUMLIC'
      Size = 50
    end
    object quDepIdORGAN: TStringField
      FieldName = 'ORGAN'
      Size = 200
    end
    object quDepIdDATEB: TDateTimeField
      FieldName = 'DATEB'
    end
    object quDepIdDATEE: TDateTimeField
      FieldName = 'DATEE'
    end
    object quDepIdIP: TSmallintField
      FieldName = 'IP'
    end
    object quDepIdIPREG: TStringField
      FieldName = 'IPREG'
      Size = 200
    end
    object quDepIdNAMEOTPR: TStringField
      FieldName = 'NAMEOTPR'
      Size = 200
    end
    object quDepIdADROPR: TStringField
      FieldName = 'ADROPR'
      Size = 200
    end
    object quDepIdRSCH: TStringField
      FieldName = 'RSCH'
      Size = 30
    end
    object quDepIdKSCH: TStringField
      FieldName = 'KSCH'
      Size = 30
    end
    object quDepIdBIK: TStringField
      FieldName = 'BIK'
      Size = 30
    end
    object quDepIdBANK: TStringField
      FieldName = 'BANK'
      Size = 200
    end
  end
end
