unit ScalesList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfmScaleList = class(TForm)
    GrSc: TcxGrid;
    ViewSc: TcxGridDBTableView;
    ViewScID: TcxGridDBColumn;
    ViewScSCALETYPE: TcxGridDBColumn;
    ViewScNAME: TcxGridDBColumn;
    ViewScIP1: TcxGridDBColumn;
    ViewScIP2: TcxGridDBColumn;
    ViewScIP3: TcxGridDBColumn;
    ViewScIP4: TcxGridDBColumn;
    ViewScIP5: TcxGridDBColumn;
    LevelSc: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmScaleList: TfmScaleList;

implementation

uses Dm;

{$R *.dfm}

procedure TfmScaleList.FormCreate(Sender: TObject);
begin
  GrSc.Align:=AlClient;
end;

end.
