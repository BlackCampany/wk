unit mFind;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, cxImageComboBox;

type
  TfmFindC = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFind: TcxGridDBTableView;
    LevelFind: TcxGridLevel;
    GridFind: TcxGrid;
    ViewFindID: TcxGridDBColumn;
    ViewFindName: TcxGridDBColumn;
    ViewFindBarCode: TcxGridDBColumn;
    ViewFindEdIzm: TcxGridDBColumn;
    ViewFindPRICE: TcxGridDBColumn;
    ViewFindSubGroupName: TcxGridDBColumn;
    Label1: TLabel;
    ViewFindGroupName: TcxGridDBColumn;
    ViewFindISTATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewFindCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ViewFindDblClick(Sender: TObject);
    procedure ViewFindSelectionChanged(Sender: TcxCustomGridTableView);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindC: TfmFindC;

implementation

uses Dm;

{$R *.dfm}

procedure TfmFindC.FormCreate(Sender: TObject);
begin
  GridFind.Align:=AlClient;
end;

procedure TfmFindC.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmFindC.ViewFindCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;

begin

//  if CountRec1(dmMC.quFindC)=False then exit;

  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewFind.ColumnCount-1 do
  begin
    if ViewFind.Columns[i].Name='ViewFindISTATUS' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType>5  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;
end;

procedure TfmFindC.FormShow(Sender: TObject);
begin
  GridFind.SetFocus;
end;

procedure TfmFindC.ViewFindDblClick(Sender: TObject);
begin
  Label1.Caption:=dmMCS.quFindCID.AsString;
  ModalResult:=mrOk;
end;

procedure TfmFindC.ViewFindSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  Label1.Caption:=dmMCS.quFindCID.AsString;
end;

end.
