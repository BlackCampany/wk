program OperSvodP;

uses
  Forms,
  OpSv in 'OpSv.pas' {fmOpSv},
  DMOPSVOD in 'DMOPSVOD.pas' {dmOPSV: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmOpSv, fmOpSv);
  Application.CreateForm(TdmOPSV, dmOPSV);
  Application.Run;
end.
