unit DocSIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxCheckBox,
  FR_Class, FR_DSet, FR_DBSet, cxCalc, cxRadioGroup, cxGroupBox, dxmdaset,
  cxMemo;

type
  TfmDocsInS = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    FormPlacement1: TFormPlacement;
    dstaSpecIn: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    amDocIn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    Edit1: TEdit;
    acReadBar: TAction;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc1: TcxGrid;
    ViewDoc1: TcxGridDBTableView;
    ViewDoc1Num: TcxGridDBColumn;
    ViewDoc1CodeTovar: TcxGridDBColumn;
    ViewDoc1Name: TcxGridDBColumn;
    ViewDoc1CodeEdIzm: TcxGridDBColumn;
    ViewDoc1BarCode: TcxGridDBColumn;
    ViewDoc1NDSProc: TcxGridDBColumn;
    ViewDoc1NDSSum: TcxGridDBColumn;
    ViewDoc1SumIn0: TcxGridDBColumn;
    ViewDoc1KolMest: TcxGridDBColumn;
    ViewDoc1KolWithMest: TcxGridDBColumn;
    ViewDoc1Kol: TcxGridDBColumn;
    ViewDoc1PriceIn: TcxGridDBColumn;
    ViewDoc1ProcentNac: TcxGridDBColumn;
    ViewDoc1PriceR: TcxGridDBColumn;
    ViewDoc1SumIn: TcxGridDBColumn;
    ViewDoc1SumR: TcxGridDBColumn;
    ViewDoc1BestBefore: TcxGridDBColumn;
    ViewDoc1CodeTara: TcxGridDBColumn;
    ViewDoc1NameTara: TcxGridDBColumn;
    ViewDoc1VesTara: TcxGridDBColumn;
    ViewDoc1CenaTara: TcxGridDBColumn;
    ViewDoc1SumVesTara: TcxGridDBColumn;
    ViewDoc1SumCenaTara: TcxGridDBColumn;
    LevelDoc1: TcxGridLevel;
    ViewDoc1RealPrice: TcxGridDBColumn;
    acSetPrice: TAction;
    cxCheckBox1: TcxCheckBox;
    frRepDIN: TfrReport;
    frtaSpecIn: TfrDBDataSet;
    acPrintDoc: TAction;
    Panel4: TPanel;
    cxLabel7: TcxLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxLabel9: TcxLabel;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    cxRadioButton4: TcxRadioButton;
    cxLabel8: TcxLabel;
    acCalcPrice: TAction;
    cxCheckBox2: TcxCheckBox;
    acPrintCen: TAction;
    ViewDoc1Remn: TcxGridDBColumn;
    acPostav: TAction;
    cxButton3: TcxButton;
    cxRadioGroup1: TcxRadioGroup;
    frtaSp: TfrDBDataSet;
    acTermoP: TAction;
    cxLabel10: TcxLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    PopupMenu1: TPopupMenu;
    acScaleView: TAction;
    N1: TMenuItem;
    acAddToScale: TAction;
    N2: TMenuItem;
    cxButton6: TcxButton;
    cxLabel11: TcxLabel;
    acSetRemn: TAction;
    ViewDoc1Nac1: TcxGridDBColumn;
    ViewDoc1iNac: TcxGridDBColumn;
    ViewDoc1iCat: TcxGridDBColumn;
    cxButton7: TcxButton;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    cxLabel12: TcxLabel;
    acCalcPriceOut: TAction;
    Panel5: TPanel;
    Memo1: TcxMemo;
    Panel6: TPanel;
    Label7: TLabel;
    Panel7: TPanel;
    Label8: TLabel;
    ViewDoc1sTop: TcxGridDBColumn;
    ViewDoc1sNov: TcxGridDBColumn;
    ViewDoc1CenaTovarAvr: TcxGridDBColumn;
    acAddSpis3: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acPrintPassport: TAction;
    tePrintP: TdxMemData;
    tePrintPiNum: TIntegerField;
    tePrintPBarcode: TStringField;
    tePrintPiCode: TIntegerField;
    tePrintPName: TStringField;
    tePrintPDocDate: TStringField;
    tePrintPDocNum: TStringField;
    tePrintPCliName: TStringField;
    tePrintPPrBrak: TStringField;
    tePrintPNameTara: TStringField;
    tePrintPBrakVozm: TStringField;
    frtePrintP: TfrDBDataSet;
    N8: TMenuItem;
    tePrintPEdIzm: TStringField;
    Label16: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxButton8: TcxButton;
    teSpecZ: TdxMemData;
    teSpecZCode: TIntegerField;
    teSpecZName: TStringField;
    teSpecZFullName: TStringField;
    teSpecZEdIzm: TSmallintField;
    teSpecZRemn1: TFloatField;
    teSpecZvReal: TFloatField;
    teSpecZRemnDay: TFloatField;
    teSpecZRemnMin: TFloatField;
    teSpecZRemnMax: TFloatField;
    teSpecZRemn2: TFloatField;
    teSpecZQuant1: TFloatField;
    teSpecZPK: TFloatField;
    teSpecZQuantZ: TFloatField;
    teSpecZQuant2: TFloatField;
    teSpecZQuant3: TFloatField;
    teSpecZPriceIn: TFloatField;
    teSpecZSumIn: TFloatField;
    teSpecZPriceIn0: TFloatField;
    teSpecZSumIn0: TFloatField;
    teSpecZNDSProc: TStringField;
    teSpecZiCat: TIntegerField;
    teSpecZiTop: TIntegerField;
    teSpecZiN: TIntegerField;
    teSpecZBarCode: TStringField;
    teSpecZCliQuant: TFloatField;
    teSpecZCliPrice: TFloatField;
    teSpecZCliPrice0: TFloatField;
    teSpecZCliSumIn: TFloatField;
    teSpecZCliSumIn0: TFloatField;
    teSpecZCliQuantN: TFloatField;
    teSpecZCliPriceN: TFloatField;
    teSpecZCliPrice0N: TFloatField;
    teSpecZCliSumInN: TFloatField;
    teSpecZCliSumIn0N: TFloatField;
    Label17: TLabel;
    teSpecZNNum: TIntegerField;
    cxLabel14: TcxLabel;
    ViewDoc1GTD: TcxGridDBColumn;
    taSpecIn: TdxMemData;
    taSpecInNum: TIntegerField;
    taSpecInCodeTovar: TIntegerField;
    taSpecInBarCode: TStringField;
    taSpecInNDSProc: TFloatField;
    taSpecInNDSSum: TFloatField;
    taSpecInBestBefore: TDateField;
    taSpecInKolMest: TFloatField;
    taSpecInKolEdMest: TFloatField;
    taSpecInKolWithMest: TFloatField;
    taSpecInKolWithNecond: TFloatField;
    taSpecInKol: TFloatField;
    taSpecInName: TStringField;
    taSpecInCodeTara: TIntegerField;
    taSpecInVesTara: TFloatField;
    taSpecInCenaTara: TFloatField;
    taSpecInSumVesTara: TFloatField;
    taSpecInSumCenaTara: TFloatField;
    taSpecInNameTara: TStringField;
    taSpecInTovarType: TIntegerField;
    taSpecInRealPrice: TFloatField;
    taSpecInRemn: TFloatField;
    taSpecInNac1: TFloatField;
    taSpecInNac2: TFloatField;
    taSpecInNac3: TFloatField;
    taSpecIniCat: TSmallintField;
    taSpecInPriceNac: TFloatField;
    taSpecInsTop: TStringField;
    taSpecInsNov: TStringField;
    taSpecInCenaTovarAvr: TFloatField;
    taSpecInGTD: TStringField;
    taSpecIniNac: TFloatField;
    taSpecInCodeEdIzm: TSmallintField;
    taSpecInsMaker: TStringField;
    ViewDoc1sMaker: TcxGridDBColumn;
    acSetGTD: TAction;
    N9: TMenuItem;
    ViewDoc1AVid: TcxGridDBColumn;
    ViewDoc1Vol: TcxGridDBColumn;
    taSpecInAVid: TIntegerField;
    taSpecInVol: TFloatField;
    taSpecInPriceIn: TFloatField;
    taSpecInPriceIn0: TFloatField;
    taSpecInSumIn: TFloatField;
    taSpecInSumIn0: TFloatField;
    taSpecInPriceR: TFloatField;
    taSpecInSumR: TFloatField;
    ViewDoc1PriceIn0: TcxGridDBColumn;
    taSpecInProcentNac: TFloatField;
    taSpecInVolDL: TFloatField;
    ViewDoc1VolDL: TcxGridDBColumn;
    taSp: TdxMemData;
    taSpiNum: TIntegerField;
    taSpBarcode: TStringField;
    taSpiCode: TIntegerField;
    taSpEdIzm: TIntegerField;
    taSpName: TStringField;
    taSpQuant: TFloatField;
    taSpPriceP: TFloatField;
    taSpPriceM: TFloatField;
    taSpScaleNum: TStringField;
    taSpPricePPre: TFloatField;
    taSpecInNewCenaTovar: TFloatField;
    acOprih: TAction;
    acMove: TAction;
    N10: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ViewDoc1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure ViewDoc1DblClick(Sender: TObject);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acSaveDoc1Execute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure taSpecInBeforePost(DataSet: TDataSet);
    procedure taSpecInNDSProcChange(Sender: TField);
    procedure taSpecInNDSSumChange(Sender: TField);
    procedure taSpecInKolWithMestChange(Sender: TField);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure taSpecInCalcFields(DataSet: TDataSet);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure cxLabel14Click(Sender: TObject);
    procedure acTermoPExecute(Sender: TObject);
    procedure cxLabel10Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure taSpecInPriceInChange(Sender: TField);
    procedure taSpecInPriceIn0Change(Sender: TField);
    procedure taSpecInSumInChange(Sender: TField);
    procedure taSpecInPriceRChange(Sender: TField);
    procedure taSpecInSumRChange(Sender: TField);
    procedure taSpecInSumIn0Change(Sender: TField);
    procedure taSpecInProcentNacChange(Sender: TField);
    procedure acSetPriceExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acOprihExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure ViewDoc1DataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prFormSumDoc;
    procedure prSetNac;
    procedure prSetFields1;
    function prRoundPr(rPr:Real):Real;
  end;

Function SaveDocsIn(var IdH,razrub:Integer):boolean;

var
  fmDocsInS: TfmDocsInS;
  iCol,iColT:INteger;

implementation

{uses Un1,DB, Clients, mFind, mCards, mTara, DocsIn, sumprops, MainMC,
  Move, QuantMess, u2fdk, AddInScale, MT, CorrDoc, TBuff, AddDoc4, DocsVn,
  ZakHPost, mMakers, SetGTD, dmPS, SetMatrix, SpInEr;
}
uses Un1,Dm, Clients, mFind, Cards, DocHIn, sumprops, MainMC,
  SelParPrintCen, BarcodeNo, Move;

{$R *.dfm}

Function SaveDocsIn(var IdH,razrub:Integer):boolean;
Var rSumIn,rSumIn0,rSumR,rSumT:Real;
    iSS:INteger;
    bErr:Boolean;
begin
  with dmMCS do
  with fmDocsInS do
  begin
    Result:=False;
    bErr:=False;
    razrub:=0;
    quTTnId.Active:=False;
    IDH:=fmDocsInS.Tag;
    iSS:=fSS(cxLookupComboBox1.EditValue);
    try
      ViewDoc1.BeginUpdate;

      if IDH=0 then //����������
      begin
        IDH:=fGetId(9);
        fmDocsInS.Tag:=IDH;

        quTTnId.Parameters.ParamByName('IDH').Value:=IDH;
        quTTnId.Active:=True;

        quTTnId.Append;
        quTTnIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quTTnIdIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quTTnIdID.AsInteger:=IDH;
        quTTnIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quTTnIdNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quTTnIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
        quTTnIdNUMSF.AsString:=Trim(cxTextEdit2.Text);
        quTTnIdIDCLI.AsInteger:=cxButtonEdit1.tag;
        quTTnIdSUMIN.AsFloat:=0;
        quTTnIdSUMUCH.AsFloat:=0;
        quTTnIdSUMTAR.AsFloat:=0;
        quTTnIdIACTIVE.AsInteger:=1;
        quTTnIdOPRIZN.AsInteger:=0;
        quTTnIdIDZ.AsInteger:=cxTextEdit3.Tag;
        quTTnIdSNUMZ.AsString:=Trim(cxTextEdit3.Text);
        quTTnId.Post;
      end else
      begin
        quTTnId.Parameters.ParamByName('IDH').Value:=IDH;
        quTTnId.Active:=True;
        quTTnId.Edit;
//      quTTnIdID.AsInteger:=IDH;   //�����������
        quTTnIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quTTnIdIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quTTnIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quTTnIdNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quTTnIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
        quTTnIdNUMSF.AsString:=Trim(cxTextEdit2.Text);
        quTTnIdIDCLI.AsInteger:=cxButtonEdit1.tag;
        quTTnIdSUMIN.AsFloat:=0;
        quTTnIdSUMUCH.AsFloat:=0;
        quTTnIdSUMTAR.AsFloat:=0;
        quTTnIdIACTIVE.AsInteger:=1;
        quTTnIdOPRIZN.AsInteger:=0;
        quTTnIdIDZ.AsInteger:=cxTextEdit3.Tag;
        quTTnIdSNUMZ.AsString:=Trim(cxTextEdit3.Text);
        quTTnId.Post;
      end;

      quSpecInRec.Active:=False;
      quSpecInRec.Parameters.ParamByName('IDH').Value:=IDH;
      quSpecInRec.Active:=True;

      quSpecInRec.First;
      while not quSpecInRec.Eof do quSpecInRec.Delete; //���� �������

      rSumIn:=0;
      rSumIn0:=0;
      rSumR:=0;
      rSumT:=0;

      taSpecIn.First;
      while not taSpecIn.Eof do
      begin
        taSpecIn.Edit;
        taSpecInSumIn0.AsFloat:=rv(taSpecInSumIn0.AsFloat);
        taSpecInSumIn.AsFloat:=rv(taSpecInSumIn.AsFloat);
        taSpecInSumR.AsFloat:=rv(taSpecInSumR.AsFloat);
        taSpecInSumCenaTara.AsFloat:=rv(taSpecInSumCenaTara.AsFloat);
        taSpecIn.Post;

        rSumIn:=rSumIn+taSpecInSumIn.AsFloat;
        rSumIn0:=rSumIn0+taSpecInSumIn0.AsFloat;
        rSumR:=rSumR+taSpecInSumR.AsFloat;
        rSumT:=rSumT+taSpecInSumCenaTara.AsFloat;

        quSpecInRec.Append;
        quSpecInRecIDHEAD.AsInteger:=IDH;
        quSpecInRecNUM.AsInteger:=taSpecInNum.AsInteger;
        quSpecInRecIDCARD.AsInteger:=taSpecInCodeTovar.AsInteger;
        quSpecInRecSBAR.AsString:=taSpecInBarCode.AsString;
        quSpecInRecIDM.AsInteger:=taSpecInCodeEdIzm.AsInteger;
        quSpecInRecQUANT.AsFloat:=taSpecInKolWithMest.AsFloat;
        quSpecInRecPRICEIN.AsFloat:=taSpecInPriceIn.AsFloat;
        quSpecInRecSUMIN.AsFloat:=taSpecInSumIn.AsFloat;
        quSpecInRecPRICEIN0.AsFloat:=taSpecInPriceIn0.AsFloat;
        quSpecInRecSUMIN0.AsFloat:=taSpecInSumIn0.AsFloat;
        quSpecInRecPRICEUCH.AsFloat:=taSpecInPriceR.AsFloat;
        quSpecInRecSUMUCH.AsFloat:=taSpecInSumR.AsFloat;
        quSpecInRecNDSPROC.AsFloat:=taSpecInNDSProc.AsFloat;
        quSpecInRecSUMNDS.AsFloat:=taSpecInNDSSum.AsFloat;
        quSpecInRecGTD.AsString:=taSpecInGTD.AsString;
        quSpecInRecSNUMHEAD.AsString:=Trim(cxTextEdit1.Text);
        quSpecInRec.Post;

        taSpecIn.Next;
      end;
      quSpecInRec.Active:=False;

      quTTnId.Edit;
      if iSS=2 then quTTnIdSUMIN.AsFloat:=rSumIn0 else quTTnIdSUMIN.AsFloat:=rSumIn;
      quTTnIdSUMUCH.AsFloat:=rSumR;
      quTTnIdSUMTAR.AsFloat:=rSumT;
      quTTnId.Post;

      bErr:=True;
    finally
      ViewDoc1.EndUpdate;
      if bErr then Result:=True;
    end;
  end;
end;


function TfmDocsInS.prRoundPr(rPr:Real):Real;
Var iR:INteger;
    rPrice:Real;
begin
  iR:=0;  // rPr:=0;rNac:=0;
  if cxRadioButton1.Checked then iR:=1;
  if cxRadioButton2.Checked then iR:=2;
  if cxRadioButton3.Checked then iR:=3;
  if cxRadioButton4.Checked then iR:=4;

  rPrice:=rv(rPr);

  case iR of
  0:begin
      rPrice:=rv(rPr);
      if (cxCheckBox2.Checked)and(rPrice>=0.1) then rPrice:=rPrice-0.01;
    end;
  1:begin
      rPrice:=RoundHi(rPr*100)/100; //��  ������
      if (cxCheckBox2.Checked)and(rPrice>=0.1) then rPrice:=rPrice-0.01;
    end;
  2:begin
      rPrice:=RoundHi(rPr*10)/10; //�� 10 ������
      if (cxCheckBox2.Checked)and(rPrice>=1) then rPrice:=rPrice-0.1;
    end;
  3:begin
      rPrice:=RoundHi(rPr); //�� 1 ���
      if (cxCheckBox2.Checked)and(rPrice>=10) then rPrice:=rPr-1;
    end;
  4:begin
      rPrice:=RoundHi(rPr/10)*10; //�� 10 ���
      if (cxCheckBox2.Checked)and(rPrice>=10) then rPrice:=rPrice-1;
    end;
  end;
  Result:=rPrice;
end;

procedure TfmDocsInS.prSetFields1;
begin
end;

procedure TfmDocsInS.prSetNac;
begin
end;

procedure TfmDocsInS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc1.StoreToIniFile(sGridIni,False);
end;

procedure TfmDocsInS.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=sFormIni;
  FormPlacement1.Active:=True;
  GridDoc1.Align:=alClient;
  ViewDoc1.RestoreFromIniFile(sGridIni);
end;

procedure TfmDocsInS.ViewDoc1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1KolWithMest' then iCol:=1;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1PriceIn' then iCol:=2;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumIn' then iCol:=3;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1PriceR' then iCol:=4;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumR' then iCol:=5;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1ProcentNac' then iCol:=6;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1PriceIn0' then iCol:=7;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SumIn0' then iCol:=8;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NDSProc' then iCol:=9;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NDSSum' then iCol:=10;
end;

procedure TfmDocsInS.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc1.SetFocus;

//  cxCalcEdit1.EditValue:=CommonSet.CurNac;
//  cxCalcEdit1.EditValue:=CommonSet.DefNacIn;

  Memo1.Clear;
{
  if CommonSet.Single=0 then
  begin
    Panel4.Visible:=False;
  end else
  begin
  end;
 } 
end;

procedure TfmDocsInS.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  if cxTextEdit3.Tag=0 then //��������� ��������� ���� �� �����
  begin
    iMax:=1;
    ViewDoc1.BeginUpdate;

    taSpecIn.First;
    while not taSpecIn.Eof do
    begin
      if taSpecInNum.AsInteger>=iMax then  iMax:=taSpecInNum.AsInteger+1;
      taSpecIn.Next;
    end;
                         
    taSpecIn.Append;
    taSpecInNum.AsInteger:=iMax;
    taSpecInCodeTovar.AsInteger:=0;
    taSpecInCodeEdIzm.AsInteger:=1;
    taSpecInBarCode.AsString:='';
    taSpecInNDSProc.AsFloat:=0;
    taSpecInNDSSum.AsFloat:=0;
    taSpecInBestBefore.AsDateTime:=0;
    taSpecInKolMest.AsInteger:=1;
    taSpecInKolEdMest.AsFloat:=0;
    taSpecInKolWithMest.AsFloat:=1;
    taSpecInKolWithNecond.AsFloat:=1;
    taSpecInKol.AsFloat:=1;
    taSpecInPriceIn.AsFloat:=0;
    taSpecInSumIn.AsFloat:=0;
    taSpecInPriceIn0.AsFloat:=0;
    taSpecInSumIn0.AsFloat:=0;
    taSpecInPriceR.AsFloat:=0;
    taSpecInSumR.AsFloat:=0;
    taSpecInProcentNac.AsFloat:=0;
    taSpecInName.AsString:='';
    taSpecInCodeTara.AsInteger:=0;
    taSpecInVesTara.AsFloat:=0;
    taSpecInCenaTara.AsFloat:=0;
    taSpecInSumVesTara.AsFloat:=0;
    taSpecInSumCenaTara.AsFloat:=0;
    taSpecInRealPrice.AsFloat:=0;
    taSpecInRemn.AsFloat:=0;
    taSpecInNac1.AsFloat:=0;
    taSpecInNac2.AsFloat:=0;
    taSpecInNac3.AsFloat:=0;
    taSpecIniCat.AsINteger:=0;
    taSpecInPriceNac.AsFloat:=0;
    taSpecInsMaker.AsString:='';
    taSpecInAVid.AsInteger:=0;
    taSpecInVol.AsFloat:=0;
    taSpecIn.Post;

    ViewDoc1.EndUpdate;
    GridDoc1.SetFocus;

    ViewDoc1CodeTovar.Options.Editing:=True;
    ViewDoc1Name.Options.Editing:=True;

    ViewDoc1Name.Focused:=True;
    ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
  end;
end;

procedure TfmDocsInS.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecIn.RecordCount>0 then
  begin
    taSpecIn.Delete;
  end;
end;

procedure TfmDocsInS.acAddListExecute(Sender: TObject);
begin
  if cxTextEdit3.Tag>0 then
  begin
    Memo1.Lines.Add('��������� (EDI) - ���������� ������ ������ �� ������');
  end else
  begin
    iDirect:=2;
    if cxLookupComboBox1.EditValue<>fmMainMC.Label3.Tag then
    begin
      fmMainMC.Label3.Tag:=cxLookupComboBox1.EditValue;
      CurDep:=Label3.Tag;
      fmMainMC.Label3.Caption:=cxLookupComboBox1.Text;
      try
        fmCards.CardsView.BeginUpdate;
        prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),cxLookupComboBox1.EditValue,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
      finally
        fmCards.CardsView.EndUpdate;
      end;
    end;
    fmCards.Show;
  end;  
end;

procedure TfmDocsInS.acDelAllExecute(Sender: TObject);
begin
  if taSpecIn.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      taSpecIn.First; while not taSpecIn.Eof do taSpecIn.Delete;
    end;
  end;
end;

procedure TfmDocsInS.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmDocsInS.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmDocsInS.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInS.acSaveDocExecute(Sender: TObject);
Var IDH,razrub:INteger;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if SaveDocsIn(IdH,razrub) then
  begin
    with dmMCS do
    begin
      fmDocsInH.ViewDocsIn.BeginUpdate;
      quTTNIn.Requery();
      quTTNIn.Locate('ID',IDH,[]);
      fmDocsInH.ViewDocsIn.EndUpdate;
      fmDocsInH.ViewDocsIn.Controller.FocusRecord(fmDocsInH.ViewDocsIn.DataController.FocusedRowIndex,True);

      Memo1.Lines.Add('���������� ��.'); delay(10);
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocsInS.acExitDocExecute(Sender: TObject);
begin
  if cxButton2.Enabled then cxButton2.Click;
end;

procedure TfmDocsInS.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmDocsInS.acReadBar1Execute(Sender: TObject);
begin
//��
end;

procedure TfmDocsInS.ViewDoc1DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name') then
    begin
      exit; //p
      ViewDoc1Name.Options.Editing:=True;
      ViewDoc1Name.Focused:=True;
    end;
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameTara') then
    begin
{      iDirect:=2;
      PosP.Id:=0; PosP.Name:='';
      delay(10);
      if dmMC.quTara.Active=False then dmMC.quTara.Active:=True;
//      fmTara.ShowModal;
      iDirect:=0;
      if PosP.Id>0 then
      begin
        taSpecIn.Edit;
        taSpecInCodeTara.AsInteger:=PosP.Id;
        taSpecInNameTara.AsString:=PosP.Name;
        taSpecIn.Post;
      end;}
    end;
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1sMaker') then
    begin //����� �������������
//      fmMakers.Show;
    end;
  end;  
end;

procedure TfmDocsInS.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxButton1.SetFocus;
  end;
end;

procedure TfmDocsInS.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmDocsInS.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit2.SetFocus;
  end;
end;

procedure TfmDocsInS.acPrintDocExecute(Sender: TObject);
{Var vPP:TfrPrintPages;
    rSum1,rSum2,rSum3,rN10,rN20:Real;}
begin
  cxButton3.Click;
end;

procedure TfmDocsInS.cxLabel7Click(Sender: TObject);
begin
  GridDoc1.SetFocus;
  acCalcPrice.Execute;
end;

procedure TfmDocsInS.cxButton4Click(Sender: TObject);
begin
  prNExportExel6(ViewDoc1);
end;

procedure TfmDocsInS.ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    StrWk:String;
begin
//  iNac:=0;
  StrWk:='';
  for i:=0 to ViewDoc1.ColumnCount-1 do
  begin
    if ViewDoc1.Columns[i].Name='ViewDoc1iNac' then
//      iNac:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
    if ViewDoc1.Columns[i].Name='ViewDoc1iCat' then
      StrWk:=AViewInfo.GridRecord.DisplayTexts[i];
  end;

  //if (StrWk<>'0') then showMessage(StrWk);

  if (StrWk='M') or (StrWk='S') or (StrWk='O') then
  begin
    if (StrWk='M') or (StrWk='O') then ACanvas.Canvas.Brush.Color := $00FFD1A4; //�������
    if (StrWk='S') then
      begin
        ACanvas.Canvas.Brush.Color := $00AAFFFF;  //������
        ACanvas.Font.Color:=clBlack;
      end
  end;
 {
  if iNac=1 then ACanvas.Canvas.Brush.Color := $00B3B3FF; //��� ��������� �������
  if iNac=2 then ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� ������� �������
  }
end;

procedure TfmDocsInS.acSaveDoc1Execute(Sender: TObject);
begin
  if cxButton7.Enabled then cxButton7.Click;
end;

procedure TfmDocsInS.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmDocsInS.Showing then
  begin
    if fmDocsInS.Tag=0 then cxTextEdit2.Text:=fGetNumDoc(cxLookupComboBox1.EditValue,1);
    GridDoc1.SetFocus;
  end;
end;

procedure TfmDocsInS.taSpecInBeforePost(DataSet: TDataSet);
begin
  iCol:=0;

  if taSpecInCodeTovar.AsInteger<0 then taSpecInCodeTovar.AsInteger:=0;
  if (taSpecInCodeTovar.AsInteger=56573) or (taSpecInCodeTovar.AsInteger=71801) then exit;
  if taSpecInKolMest.AsFloat<0 then taSpecInKolMest.AsFloat:=0;
  if taSpecInKolWithMest.AsFloat<0 then taSpecInKolWithMest.AsFloat:=0;
  if taSpecInNDSProc.AsFloat<0 then taSpecInNDSProc.AsFloat:=0;
  if taSpecInNDSSum.AsFloat<0 then taSpecInNDSSum.AsFloat:=0;
  if taSpecInVesTara.AsFloat<0 then taSpecInVesTara.AsFloat:=0;
  if taSpecInCenaTara.AsFloat<0 then taSpecInCenaTara.AsFloat:=0;
  if taSpecInSumVesTara.AsFloat<0 then taSpecInSumVesTara.AsFloat:=0;
  if taSpecInSumCenaTara.AsFloat<0 then taSpecInSumCenaTara.AsFloat:=0;
  if taSpecInCodeTara.AsInteger<0 then taSpecInCodeTara.AsInteger:=0;
  taSpecInKol.AsFloat:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat;
end;

procedure TfmDocsInS.taSpecInNDSProcChange(Sender: TField);
begin
  if iCol=9 then   //������ ���
  begin
    if taSpecInNDSProc.AsFloat>0 then
    begin
      taSpecInPriceIn.AsFloat:=rv(taSpecInPriceIn.AsFloat);
      taSpecInSumIn.AsFloat:=rv(taSpecInSumIn.AsFloat);
      taSpecInPriceIn0.AsFloat:=taSpecInPriceIn.AsFloat*100/(100+taSpecInNDSProc.AsFloat);
      taSpecInSumIn0.AsFloat:=rv(taSpecInSumIn.AsFloat*100/(100+taSpecInNDSProc.AsFloat));
      taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
    end;
  end;
end;

procedure TfmDocsInS.taSpecInNDSSumChange(Sender: TField);
begin
  // ����� ���
  if iCol=10 then   //����� ���
  begin
    taSpecInSumIn.AsFloat:=rv(taSpecInSumIn.AsFloat);
    taSpecInSumIn0.AsFloat:=taSpecInSumIn.AsFloat-rv(taSpecInNDSSum.AsFloat);
    iCol:=0;
    taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
  end;
end;

procedure TfmDocsInS.taSpecInKolWithMestChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=1 then   //���-�� ��������� ���������
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    taSpecInSumIn.AsFloat:=rv(rQ*taSpecInPriceIn.AsFloat);
    taSpecInSumIn0.AsFloat:=rv(rQ*taSpecInPriceIn0.AsFloat);
    taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
    taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
  end;
end;

procedure TfmDocsInS.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var sBar:String;
    IC,iMax,kNDS:INteger;
begin
  if (Key=$0D) then
  begin

    if cxButton1.Enabled=False then exit;
    if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

    sBar:=Edit1.Text;
    if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
    if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
    with dmMCS do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iC));
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount>0 then
        begin
          if quFCPISTATUS.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������. ���������� ����������.');
          end else
          begin
            if taSpecIn.RecordCount>0 then
              if taSpecIn.Locate('CodeTovar',iC,[]) then
              begin
                ViewDoc1.Controller.FocusRecord(ViewDoc1.DataController.FocusedRowIndex,True);
                GridDoc1.SetFocus;
                exit;
              end;
            iMax:=1;

            fmDocsInS.ViewDoc1.BeginUpdate;
            taSpecIn.First;
            while not taSpecIn.Eof do
            begin
              if taSpecInNum.AsInteger>=iMax then  iMax:=taSpecInNum.AsInteger+1;
              taSpecIn.Next;
            end;
            fmDocsInS.ViewDoc1.EndUpdate;

            kNDS:=Label17.Tag;

            taSpecIn.Append;
            taSpecInNum.AsInteger:=iMax;
            taSpecInCodeTovar.AsInteger:=quFCPID.asinteger;
            taSpecInCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
            taSpecInBarCode.AsString:=sBar;
            taSpecInNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
            taSpecInNDSSum.AsFloat:=0;
            taSpecInBestBefore.AsDateTime:=date;
            taSpecInKolMest.AsInteger:=1;
            taSpecInKolEdMest.AsFloat:=0;
            taSpecInKolWithMest.AsFloat:=1;
            taSpecInKolWithNecond.AsFloat:=1;
            taSpecInKol.AsFloat:=1;
            taSpecInPriceIn.AsFloat:=0;
            taSpecInSumIn.AsFloat:=0;
            taSpecInPriceIn0.AsFloat:=0;
            taSpecInSumIn0.AsFloat:=0;
            taSpecInPriceR.AsFloat:=quFCPPRICE.AsFloat;
            taSpecInSumR.AsFloat:=quFCPPRICE.AsFloat;
            taSpecInProcentNac.AsFloat:=quFCPRNAC.AsFloat;
            taSpecInName.AsString:=quFCPNAME.AsString;
            taSpecInCodeTara.AsInteger:=0;
            taSpecInVesTara.AsFloat:=0;
            taSpecInCenaTara.AsFloat:=0;
            taSpecInSumVesTara.AsFloat:=0;
            taSpecInSumCenaTara.AsFloat:=0;
            taSpecInRealPrice.AsFloat:=quFCPPRICE.AsFloat;
            taSpecInRemn.AsFloat:=0;
            taSpecInNac1.AsFloat:=quFCPRNAC.AsFloat;
            taSpecInNac2.AsFloat:=0;
            taSpecInNac3.AsFloat:=0;
            taSpecIniCat.AsINteger:=0;
            taSpecInPriceNac.AsFloat:=quFCPRNAC.AsFloat;
            taSpecInsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
            taSpecInAVid.AsInteger:=quFCPAVID.AsInteger;
            taSpecInVol.AsFloat:=quFCPVOL.AsFloat;
            taSpecIn.Post;
          end;
        end;
        quFCP.Active:=False;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmDocsInS.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;


procedure TfmDocsInS.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmDocsInS.taSpecInCalcFields(DataSet: TDataSet);
begin
  taSpecInVolDL.AsFloat:=taSpecInVol.AsFloat*taSpecInKol.AsFloat/10; //� ����������
end;

procedure TfmDocsInS.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var i:Integer;
begin
  with dmMCS do
  with fmClients do
  begin
    iDirect:=1; //0 - ������ , 1- �������

    fmClients.Show;

    if cxButtonEdit1.Tag>0 then  //�����-�� ��������� - ���� �� ���� ���������
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
      quFCli.SQL.Add('where cli.[Id]='+its(cxButtonEdit1.Tag));
      quFCli.Active:=True;

      bRefreshCli:=False;
      for i:=0 to CliTree.Items.Count-1 Do
      begin
        if Integer(CliTree.Items[i].Data) = quFCli.FieldByName('IdParent').AsInteger then
        begin
          CliTree.Items[i].Expand(False);
          CliTree.Repaint;
          CliTree.Items[i].Selected:=True;
          try
            CliView.BeginUpdate;
            prRefreshCli(quFCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
            quClients.Locate('ID',quFCli.FieldByName('Id').AsInteger,[]);
          finally
            CliView.EndUpdate;
            CliGr.SetFocus;
            CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCli:=True;
      quFCli.Active:=False;
    end;

  end;
end;

procedure TfmDocsInS.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDocsInS.cxButton1Click(Sender: TObject);
begin
  acSaveDoc.Execute;
end;

procedure TfmDocsInS.acPrintCenExecute(Sender: TObject);
Var i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
//    vPP:TfrPrintPages;
    rPrice,rPriceC:Real;
begin
  //������ ��������
  with dmMCS do
  begin
    if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

    fmSelParCen.ShowModal;
    if fmSelParCen.ModalResult=mrOk then
    begin
      if (fmSelParCen.cxRadioGroup1.ItemIndex=1)or(fmSelParCen.cxRadioGroup1.ItemIndex=2) then ViewDoc1.Controller.SelectAll;

      try
        dstaCen.DataSet:=nil;
        CloseTe(taCen);
        for i:=0 to ViewDoc1.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDoc1.Controller.SelectedRecords[i];

          iNum:=0; rPrice:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then  begin iNum:=Rec.Values[j]; end;
            if ViewDoc1.Columns[j].Name='ViewDoc1PriceR' then  begin rPrice:=Rec.Values[j]; end;
            if (iNum>0)and(rPrice>0) then break;
          end;

          if iNum>0 then
          begin
            quFindC4.Active:=False;
            quFindC4.Parameters.ParamByName('IDEP').Value:=cxLookupComboBox1.EditValue;
            quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
            quFindC4.Active:=True;
            if quFindC4.RecordCount>0 then
            begin
              rPriceC:=quFindC4PRICE.AsFloat;

              if (fmSelParCen.cxRadioGroup1.ItemIndex=2) then rPriceC:=0;

              if abs(rPrice-rPriceC)>0.001 then
              begin
                taCen.Append;
                taCenIdCard.AsInteger:=iNum;
                taCenFullName.AsString:=quFindC4FullName.AsString;
                taCenCountry.AsString:=quFindC4NameCu.AsString;
                taCenPrice1.AsFloat:=RV(rPrice);
                taCenPrice2.AsFloat:=0;
                taCenDiscount.AsFloat:=0;
                taCenBarCode.AsString:=quFindC4BARCODE.AsString;
                taCenEdIzm.AsInteger:=quFindC4EDIZM.AsInteger;
                if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum,cxLookupComboBox1.EditValue) else taCenPluScale.AsString:='';
                taCenReceipt.AsString:=quFindC4SOSTAV.AsString;
                taCenDepName.AsString:=fFullName(cxLookupComboBox1.EditValue);
                taCensDisc.AsString:=prSDisc(iNum,cxLookupComboBox1.EditValue);
                taCenScaleKey.AsInteger:=quFindC4VESBUTTON.AsInteger;
                taCensDate.AsString:=ds1(fmMainMC.cxDEdit1.Date);
//              taCenV02.AsInteger:=quFindC4V02.AsInteger;
//              taCenV12.AsInteger:=quFindC4V12.AsInteger;
//              taCenMaker.AsString:=quFindC4NAMEM.AsString;
                taCen.Post;
              end;
            end;

            quFindC4.Active:=False;
          end;
        end;

        if quLabs.Locate('ID',fmMainMC.cxLookupComboBox1.EditValue,[]) then
        begin
          if FileExists(CommonSet.PathReport+quLabsNameF.AsString) then
          begin
            RepCenn.LoadFromFile(CommonSet.PathReport+quLabsNameF.AsString);
            RepCenn.ReportName:='�������.';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
//        vPP:=frAll;
//        RepCenn.PrintPreparedReport('',1,False,vPP);
          end else showmessage('���� �� ������ : '+CommonSet.PathReport+quLabsNameF.AsString);

        end;
        CloseTe(taCen);
        dstaCen.DataSet:=taCen;
        fmMainMC.cxDEdit1.Date:=Date;
      finally
      end;

    end;
  end;
end;

procedure TfmDocsInS.cxLabel14Click(Sender: TObject);
begin
  acPrintCen.Execute;
end;

procedure TfmDocsInS.acTermoPExecute(Sender: TObject);
Var j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sBar,sCountry,sName,sTehno:String;
//Var SName,sTehno,sBar:String;
begin

  if ViewDoc1.Controller.SelectedRecordCount=0 then exit;
  try
   //���� ������� ���� , ������� �� ���������
    with dmMCS do
    begin
      Rec:=ViewDoc1.Controller.SelectedRecords[0];

      iNum:=0; sBar:='';
      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDoc1.Columns[j].Name='ViewDoc1CodeTovar' then begin  iNum:=Rec.Values[j]; end;
        if ViewDoc1.Columns[j].Name='ViewDoc1BarCode' then begin  sBar:=Rec.Values[j]; end;
        if (iNum>0)and(sBar>'') then break;
      end;

      if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
      begin
        quFindC4.Active:=False;
        quFindC4.Parameters.ParamByName('IDEP').Value:=fmMainMC.Label3.tag;
        quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
//          sBar:=quFindC4BarCode.AsString;
          if pos('22',sBar)=1 then //��� ������� �����
          begin
            while length(sBar)<12 do sBar:=sBar+'0';
            sBar:=ToStandart(sBar);
          end;

          sName:=quFindC4FullName.AsString;
          sTehno:=its(iNum);
          sCountry:=quFindC4NameCu.AsString;

          if FileExists(CurDir + 'LabelBar.frf') then
          begin
            RepCenn.LoadFromFile(CurDir + 'LabelBar.frf');

            frVariables.Variable['SNAME']:=sName;
            frVariables.Variable['SBAR']:=sBar;
            frVariables.Variable['STEHNO']:=sTehno;
            frVariables.Variable['COUNTRY']:=sCountry;

            RepCenn.ReportName:='������ ��������.';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
          end else Showmessage('���� ������� �������� : '+CurDir + 'LabelBar.frf'+' �� ������.');
        end;
        quFindC4.Active:=False;
      end;
    end;
  finally
  end;
end;

procedure TfmDocsInS.cxLabel10Click(Sender: TObject);
begin
  acTermoP.Execute;
end;

procedure TfmDocsInS.cxButton3Click(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum3,rSumOP,rSumOM,rPrPre:Real;
    iNum,iPlNDS:INteger;
    rSumIn,rN10,rN20,rSum10,rSum20,rSum0,rSumR:Real;
    rPrIn0,rPrIn,rPrR:Real;
    sCli:TArrCli;
begin
  try
    prFormSumDoc;
    ViewDoc1.BeginUpdate;
    CloseTe(taSp);

    rSum3:=0; rSumOP:=0; rSumOM:=0;
    iNUm:=0;

    rSumIn:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum0:=0; rSumR:=0;

    taSpecIn.First;
    while not taSpecIN.Eof do
    begin
//      rSum3:=rSum3+rv(taSpecInSumCenaTara.AsFloat);

//      rSumOP:=rSumOP+(taSpecInNecond.AsFloat+taSpecInZemlia.AsFloat+taSpecInOthodi.AsFloat)*taSpecInCenaTovar.AsFloat;
//      rSumOM:=rSumOM+(taSpecInNecond.AsFloat+taSpecInZemlia.AsFloat+taSpecInOthodi.AsFloat)*taSpecInNewCenaTovar.AsFloat;

      if taSpecInCodeEdIzm.AsINteger=2 then
      begin
        inc(iNUm);

        taSp.Append;
        taSpiNum.AsInteger:=iNum;
        taSpBarcode.asstring:=taSpecInBarCode.AsString;
        taSpiCode.AsInteger:=taSpecInCodeTovar.AsInteger;
        taSpEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
        taSpName.AsString:=taSpecInName.AsString;
        taSpQuant.AsFloat:=taSpecInKol.AsFloat;
        taSpPriceP.AsFloat:=taSpecInPriceR.AsFloat;
        taSpPriceM.AsFloat:=taSpecInRealPrice.AsFloat;
        taSpScaleNum.AsString:=prFindPlu(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue);
        taSpPricePPre.AsFloat:=0;
        taSp.Post;
      end;

      rSumIn:=rSumIn+rv(taSpecInSumIn.AsFloat);
      rSumR:=rSumR+rv(taSpecInSumR.AsFloat);

      if taSpecInNDSProc.AsFloat>=15 then
      begin
        rSum20:=rSum20+rv(taSpecInSumIn0.AsFloat);
        rN20:=rN20+rv(taSpecInNDSSum.AsFloat);
      end else
      begin
        if taSpecInNDSProc.AsFloat>=8 then
        begin
          rSum10:=rSum10+rv(taSpecInSumIn0.AsFloat);
          rN10:=rN10+rv(taSpecInNDSSum.AsFloat);
        end else //0
        begin
          rSum0:=rSum0+rv(taSpecInSumIn0.AsFloat);
        end;
      end;

      taSpecIn.Next;
    end;

    frRepDIN.ReportName:='������ ���.';

    prFCli(cxButtonEdit1.Tag,sCli);

    if cxRadioGroup1.ItemIndex=0 then
    begin
      iPlNDS:=1;

      if dmMCS.quDepartsSt.Locate('ID',cxLookupComboBox1.EditValue,[]) then iPlNDS:=dmMCS.quDepartsStPLATNDS.AsInteger;
      if iPlNDS=1 then  //���������� ���
      begin
        frRepDIN.LoadFromFile(CommonSet.PathReport+'reestrin.frf');

        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['CliFrom']:=sCli[2];
        frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;

//        frVariables.Variable['Cli2Name']:=sCli[2];
//        frVariables.Variable['Cli2Inn']:=sCli[3];
//        frVariables.Variable['Cli2Adr']:=sCli[13]+','+sCli[14]+','+sCli[15]+','+sCli[16];
//        frVariables.Variable['Poluch']:=sCli[6];
//        frVariables.Variable['PoluchAdr']:=sCli[8];


        frVariables.Variable['SumPost']:=rSumIn;
        frVariables.Variable['SumMag']:=rSumR;
        frVariables.Variable['SumTara']:=rSum3;
        frVariables.Variable['SumNac']:=rSumR-rSumIn;
        frVariables.Variable['SumNDS10']:=rN10;
        frVariables.Variable['SumNDS20']:=rN20;
        frVariables.Variable['SumNDS']:=rN20+rN10;
        frVariables.Variable['SSumPost']:=MoneyToString(rSumIn,True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
        frVariables.Variable['SSumNac']:=MoneyToString((rSumR-rSumIn),True,False);
        frVariables.Variable['SSumNDS10']:=MoneyToString(rN10,True,False);
        frVariables.Variable['SSumNDS20']:=MoneyToString(rN20,True,False);
        frVariables.Variable['SSumNDS']:=MoneyToString((rN10+rN20),True,False);
        frVariables.Variable['SSumMag']:=MoneyToString(rSumR,True,False);
      end;
      if iPlNDS=0 then //�� ���������� ���
      begin
        frRepDIN.LoadFromFile(CommonSet.PathReport + 'reestrin1.frf');

        frVariables.Variable['DocNum']:=cxTextEdit1.Text;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
        frVariables.Variable['CliFrom']:=sCli[2];
        frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;
        frVariables.Variable['SumPost']:=rSumIn;
        frVariables.Variable['SumMag']:=rSumR;
        frVariables.Variable['SumTara']:=rSum3;
        frVariables.Variable['SumNac']:=rSumR-rSumIn;
        frVariables.Variable['SSumPost']:=MoneyToString(rSumIn,True,False);
        frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
        frVariables.Variable['SSumNac']:=MoneyToString((rSumR-rSumIn),True,False);
        frVariables.Variable['SumOP']:=rSumOP;
        frVariables.Variable['SumOM']:=rSumOM;
        frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
        frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);
      end;
    end;

    if cxRadioGroup1.ItemIndex=1 then
    begin
      frRepDIN.LoadFromFile(CommonSet.PathReport + 'reestrin2.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['CliFrom']:=sCli[2];
      frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;

      frVariables.Variable['SumPost']:=rSumIn;
      frVariables.Variable['SumMag']:=rSumR;
      frVariables.Variable['SumTara']:=rSum3;
      frVariables.Variable['SumNac']:=rSumR-rSumIn;
      frVariables.Variable['SSumPost']:=MoneyToString(rSumIn,True,False);
      frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
      frVariables.Variable['SSumNac']:=MoneyToString((rSumR-rSumIn),True,False);
      frVariables.Variable['SumOP']:=rSumOP;
      frVariables.Variable['SumOM']:=rSumOM;
      frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
      frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);

    end;

    if cxRadioGroup1.ItemIndex=2 then
    begin
      CloseTe(taSp);

      iNUm:=0;

      taSpecIn.First;
      while not taSpecIN.Eof do
      begin
        rPrPre:=prFLP(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue,rPrIn0,rPrIn,rPrR);
        if (rPrPre-taSpecInPriceIn.AsFloat)>=0.5 then
        begin
          inc(iNUm);

          taSp.Append;
          taSpiNum.AsInteger:=iNum;
          taSpBarcode.asstring:=taSpecInBarCode.AsString;
          taSpiCode.AsInteger:=taSpecInCodeTovar.AsInteger;
          taSpEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
          taSpName.AsString:=taSpecInName.AsString;
          taSpQuant.AsFloat:=taSpecInKol.AsFloat;
          taSpPriceP.AsFloat:=taSpecInPriceIn.AsFloat;
          taSpPriceM.AsFloat:=taSpecInRealPrice.AsFloat;
          taSpScaleNum.AsString:='';
          taSpPricePPre.AsFloat:=rPrPre;
          taSp.Post;
        end;
        taSpecIn.Next;
      end;


      frRepDIN.LoadFromFile(CommonSet.PathReport + 'reestrin3.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['CliFrom']:=sCli[2];
      frVariables.Variable['CliTo']:=cxLookupComboBox1.Text;

      frVariables.Variable['SumPost']:=rSumIn;
      frVariables.Variable['SumMag']:=rSumR;
      frVariables.Variable['SumTara']:=rSum3;
      frVariables.Variable['SumNac']:=rSumR-rSumIn;
      frVariables.Variable['SSumPost']:=MoneyToString(rSumIn,True,False);
      frVariables.Variable['SSumTara']:=MoneyToString(rSum3,True,False);
      frVariables.Variable['SSumNac']:=MoneyToString((rSumR-rSumIn),True,False);
      frVariables.Variable['SumOP']:=rSumOP;
      frVariables.Variable['SumOM']:=rSumOM;
      frVariables.Variable['SSumOP']:=MoneyToString(rSumOP,True,False);
      frVariables.Variable['SSumOM']:=MoneyToString(rSumOM,True,False);

    end;

    frRepDIN.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDIN.ShowPreparedReport
    else frRepDIN.PrintPreparedReport('',1,False,vPP);//}
  finally
    ViewDoc1.EndUpdate;
  end;
end;

procedure TfmDocsInS.prFormSumDoc;
var rSumIn,rN10,rN20,rSum10,rSum20,rSum0:Real;
begin
  try
    ViewDoc1.BeginUpdate;
    iCol:=0;
    rSumIn:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum0:=0;
    taSpecIn.First;
    while not taSpecIN.Eof do
    begin
      if (taSpecInCodeTovar.AsInteger>0) then
      begin
        taSpecIn.Edit;
        taSpecInPriceIn.AsFloat:=rv(taSpecInPriceIn.AsFloat);
        taSpecInPriceIn0.AsFloat:=rv(taSpecInPriceIn0.AsFloat);
        taSpecInSumIn.AsFloat:=rv(taSpecInSumIn.AsFloat);
        taSpecInSumIn0.AsFloat:=rv(taSpecInSumIn0.AsFloat);
        taSpecInPriceR.AsFloat:=rv(taSpecInPriceR.AsFloat);
        taSpecInSumR.AsFloat:=rv(taSpecInSumR.AsFloat);
        taSpecInProcentNac.AsFloat:=rv(taSpecInProcentNac.AsFloat);
        taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
        taSpecIn.Post;

        rSumIn:=rSumIn+rv(taSpecInSumIn.AsFloat);

        if taSpecInNDSProc.AsFloat>=15 then
        begin
          rSum20:=rSum20+rv(taSpecInSumIn0.AsFloat);
          rN20:=rN20+rv(taSpecInNDSSum.AsFloat);
        end else
        begin
          if taSpecInNDSProc.AsFloat>=8 then
          begin
            rSum10:=rSum10+rv(taSpecInSumIn0.AsFloat);
            rN10:=rN10+rv(taSpecInNDSSum.AsFloat);
          end else //0
          begin
            rSum0:=rSum0+rv(taSpecInSumIn0.AsFloat);
          end;
        end;
      end;
      taSpecIn.Next;
    end;
  finally
    ViewDoc1.EndUpdate;
  end;
end;

procedure TfmDocsInS.ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  with dmMCS do
  begin
    if (Key=$0D) then
    begin
      kNDS:=0;
      iSS:=fSS(cxLookupComboBox1.EditValue);
      if (iSS=2)and(Label17.Tag=1) then kNDS:=1;

      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
        quFCP.SQL.Add('where ca.[ID]='+its(iCode));
//        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(s1)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          taSpecIn.Edit;
          taSpecInCodeTovar.AsInteger:=iCode;
          taSpecInCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecInBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecInNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecInNDSSum.AsFloat:=0;
          taSpecInBestBefore.AsDateTime:=date;
          taSpecInKolMest.AsInteger:=1;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=1;
          taSpecInKolWithNecond.AsFloat:=1;
          taSpecInKol.AsFloat:=1;
          taSpecInPriceIn.AsFloat:=0;
          taSpecInSumIn.AsFloat:=0;
          taSpecInPriceIn0.AsFloat:=0;
          taSpecInSumIn0.AsFloat:=0;
          taSpecInPriceR.AsFloat:=0;
          taSpecInSumR.AsFloat:=0;
          taSpecInProcentNac.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInName.AsString:=quFCPNAME.AsString;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInVesTara.AsFloat:=0;
          taSpecInCenaTara.AsFloat:=0;
          taSpecInSumVesTara.AsFloat:=0;
          taSpecInSumCenaTara.AsFloat:=0;
          taSpecInRealPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecInRemn.AsFloat:=0;
          taSpecInNac1.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInNac2.AsFloat:=0;
          taSpecInNac3.AsFloat:=0;
          taSpecIniCat.AsINteger:=0;
          taSpecInPriceNac.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
          taSpecInAVid.AsInteger:=quFCPAVID.AsInteger;
          taSpecInVol.AsFloat:=quFCPVOL.AsFloat;
          taSpecIn.Post;
        end;
        quFCP.Active:=False;
      end;
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name' then
      begin
        bAdd:=True;

        sName:=VarAsType(AEdit.EditingValue, varString);
        if Length(sName)>3 then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
            quFindC.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//            quFindC.SQL.Add('and [TTOVAR]=0');
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if quFindC.RecordCount>0 then
            begin
              taSpecIn.Edit;
              taSpecInCodeTovar.AsInteger:=quFindCID.AsInteger;
              taSpecInCodeEdIzm.AsInteger:=quFindCEDIZM.AsInteger;
              taSpecInBarCode.AsString:=quFindCBARCODE.AsString;
              taSpecInNDSProc.AsFloat:=kNDS*quFindCNDS.AsFloat;
              taSpecInNDSSum.AsFloat:=0;
              taSpecInBestBefore.AsDateTime:=date;
              taSpecInKolMest.AsInteger:=1;
              taSpecInKolEdMest.AsFloat:=0;
              taSpecInKolWithMest.AsFloat:=1;
              taSpecInKolWithNecond.AsFloat:=1;
              taSpecInKol.AsFloat:=1;
              taSpecInPriceIn.AsFloat:=0;
              taSpecInSumIn.AsFloat:=0;
              taSpecInPriceIn0.AsFloat:=0;
              taSpecInSumIn0.AsFloat:=0;
              taSpecInPriceR.AsFloat:=0;
              taSpecInSumR.AsFloat:=0;
              taSpecInProcentNac.AsFloat:=quFindCRNAC.AsFloat;
              taSpecInName.AsString:=quFindCNAME.AsString;
              taSpecInCodeTara.AsInteger:=0;
              taSpecInVesTara.AsFloat:=0;
              taSpecInCenaTara.AsFloat:=0;
              taSpecInSumVesTara.AsFloat:=0;
              taSpecInSumCenaTara.AsFloat:=0;
              taSpecInRealPrice.AsFloat:=quFindCPRICE.AsFloat;
              taSpecInRemn.AsFloat:=0;
              taSpecInNac1.AsFloat:=quFindCRNAC.AsFloat;
              taSpecInNac2.AsFloat:=0;
              taSpecInNac3.AsFloat:=0;
              taSpecIniCat.AsINteger:=0;
              taSpecInPriceNac.AsFloat:=quFindCRNAC.AsFloat;
              taSpecInsMaker.AsString:=prFindMaker(quFindCMAKER.AsInteger);
              taSpecInAVid.AsInteger:=quFindCAVID.AsInteger;
              taSpecInVol.AsFloat:=quFindCVOL.AsFloat;
              taSpecIn.Post;

              AEdit.SelectAll;
            end;
          end;
          quFindC.Active:=False;
        end;

        bAdd:=False;
      end;

   //   ViewDoc1KolWithMest.Focused:=True;

    end else
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1CodeTovar' then
        if fTestKey(Key)=False then
          if taSpecIn.State in [dsEdit,dsInsert] then taSpecIn.Cancel;
  end;
end;

procedure TfmDocsInS.ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  if bAdd then exit;
  with dmMCS do
  begin
    if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Name' then
    begin
      sName:=VarAsType(AEdit.EditingValue, varString);
      if Length(sName)>3 then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT TOP 2 * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(fmMainMC.Label3.Tag));
        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          kNDS:=0;
          iSS:=fSS(cxLookupComboBox1.EditValue);
          if (iSS=2)and(Label17.Tag=1) then kNDS:=1;

          ViewDoc1.BeginUpdate;

          taSpecIn.Edit;
          taSpecInCodeTovar.AsInteger:=quFCPID.AsInteger;
          taSpecInCodeEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecInBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecInNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecInNDSSum.AsFloat:=0;
          taSpecInBestBefore.AsDateTime:=date;
          taSpecInKolMest.AsInteger:=1;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=1;
          taSpecInKolWithNecond.AsFloat:=1;
          taSpecInKol.AsFloat:=1;
          taSpecInPriceIn.AsFloat:=0;
          taSpecInSumIn.AsFloat:=0;
          taSpecInPriceIn0.AsFloat:=0;
          taSpecInSumIn0.AsFloat:=0;
          taSpecInPriceR.AsFloat:=0;
          taSpecInSumR.AsFloat:=0;
          taSpecInProcentNac.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInName.AsString:=quFCPNAME.AsString;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInVesTara.AsFloat:=0;
          taSpecInCenaTara.AsFloat:=0;
          taSpecInSumVesTara.AsFloat:=0;
          taSpecInSumCenaTara.AsFloat:=0;
          taSpecInRealPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecInRemn.AsFloat:=0;
          taSpecInNac1.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInNac2.AsFloat:=0;
          taSpecInNac3.AsFloat:=0;
          taSpecIniCat.AsINteger:=0;
          taSpecInPriceNac.AsFloat:=quFCPRNAC.AsFloat;
          taSpecInsMaker.AsString:=prFindMaker(quFCPMAKER.AsInteger);
          taSpecInAVid.AsInteger:=quFCPAVID.AsInteger;
          taSpecInVol.AsFloat:=quFCPVOL.AsFloat;
          taSpecIn.Post;

          ViewDoc1.EndUpdate;

          AEdit.SelectAll;
          ViewDoc1Name.Options.Editing:=False;
          ViewDoc1Name.Focused:=True;
          Key:=#0;
        end;
        quFCP.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsInS.taSpecInPriceInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    taSpecInSumIn.AsFloat:=rv(rQ*taSpecInPriceIn.AsFloat);
    taSpecInPriceIn0.AsFloat:=taSpecInPriceIn.AsFloat*100/(100+taSpecInNDSProc.AsFloat);
    taSpecInSumIn0.AsFloat:=rv(rQ*taSpecInPriceIn0.AsFloat);

    if taSpecInProcentNac.AsFloat>=1 then  taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInProcentNac.AsFloat)/100)
    else taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInNac1.AsFloat)/100);

    if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;

    taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
    taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
  end;
end;

procedure TfmDocsInS.taSpecInPriceIn0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=7 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;

    taSpecInPriceIn.AsFloat:=taSpecInPriceIn0.AsFloat/100*(100+taSpecInNDSProc.AsFloat);
    taSpecInSumIn.AsFloat:=rv(rQ*taSpecInPriceIn.AsFloat);
    taSpecInSumIn0.AsFloat:=rv(rQ*taSpecInPriceIn0.AsFloat);

    if taSpecInProcentNac.AsFloat>=1 then  taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInProcentNac.AsFloat)/100)
    else taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInNac1.AsFloat)/100);

    if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;
    
    taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
    taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
  end;
end;

procedure TfmDocsInS.taSpecInSumInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=3 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecInPriceIn.AsFloat:=taSpecInSumIn.AsFloat/rQ;
      taSpecInPriceIn0.AsFloat:=taSpecInPriceIn.AsFloat*100/(100+taSpecInNDSProc.AsFloat);
      taSpecInSumIn0.AsFloat:=rv(rQ*taSpecInPriceIn0.AsFloat);

      if taSpecInProcentNac.AsFloat>=1 then  taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInProcentNac.AsFloat)/100)
      else taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInNac1.AsFloat)/100);

      if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;

      taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
      taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
    end else
    begin
      taSpecInSumIn.AsFloat:=0;
      taSpecInSumIn0.AsFloat:=0;
      taSpecInSumR.AsFloat:=0;
      taSpecInNDSSum.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsInS.taSpecInPriceRChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=4 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
    if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;
  end;
end;

procedure TfmDocsInS.taSpecInSumRChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=5 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    if abs(rQ)>0 then
    begin
       taSpecInPriceR.AsFloat:=rv(taSpecInSumR.AsFloat/rQ);
      if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;
    end else
    begin
      taSpecInSumIn.AsFloat:=0;
      taSpecInSumIn0.AsFloat:=0;
      taSpecInSumR.AsFloat:=0;
      taSpecInNDSSum.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsInS.taSpecInSumIn0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=8 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
  
    if abs(rQ)>0 then
    begin
      taSpecInPriceIn0.AsFloat:=taSpecInSumIn0.AsFloat/rQ;
      taSpecInPriceIn.AsFloat:=taSpecInPriceIn0.AsFloat/100*(100+taSpecInNDSProc.AsFloat);
      taSpecInSumIn.AsFloat:=rv(rQ*taSpecInPriceIn.AsFloat);

      if taSpecInProcentNac.AsFloat>=1 then  taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInProcentNac.AsFloat)/100)
      else taSpecInPriceR.AsFloat:=prRoundPr(taSpecInPriceIn.AsFloat*(100+taSpecInNac1.AsFloat)/100);

      if taSpecInPriceIn.AsFloat>0 then taSpecInProcentNac.AsFloat:=(taSpecInPriceR.AsFloat-taSpecInPriceIn.AsFloat)/taSpecInPriceIn.AsFloat*100;

      taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
      taSpecInNDSSum.AsFloat:=taSpecInSumIn.AsFloat-taSpecInSumIn0.AsFloat;
    end else
    begin
      taSpecInSumIn.AsFloat:=0;
      taSpecInSumIn0.AsFloat:=0;
      taSpecInSumR.AsFloat:=0;
      taSpecInNDSSum.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsInS.taSpecInProcentNacChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=6 then
  begin
    rQ:=taSpecInKolWithMest.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecInPriceR.AsFloat:=prRoundPr((100+taSpecInProcentNac.AsFloat)/100*taSpecInPriceIn.AsFloat);
      taSpecInSumR.AsFloat:=rv(rQ*taSpecInPriceR.AsFloat);
    end else
    begin
      taSpecInSumIn.AsFloat:=0;
      taSpecInSumIn0.AsFloat:=0;
      taSpecInSumR.AsFloat:=0;
      taSpecInNDSSum.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsInS.acSetPriceExecute(Sender: TObject);
Var rPrIn0,rPrIn,rPrR,rCurPr:Real;
    rRemn:Real;
begin
  //��������� � ����� ���������� �������
  if cxButton1.Enabled then
  begin
    if cxLookupComboBox1.EditValue>0 then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('���������� � ����� ��'); delay(10);
      ViewDoc1.BeginUpdate;
      taSpecIn.First;
      while not taSpecIn.Eof do
      begin
        iCol:=2; //���� ������

        rPrIn:=prFLP(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue,rPrIn0,rPrIn,rPrR);
        rCurPr:=taSpecInRealPrice.AsFloat;
        rPrR:=prRoundPr(rPrR);

        if abs(rPrR-rCurPr)<0.15 then rPrR:=rCurPr; 

        rRemn:=prFindRemnDepD(taSpecInCodeTovar.AsInteger,cxLookupComboBox1.EditValue,Trunc(date));

        taSpecIn.Edit;
        taSpecInPriceR.AsFloat:=rPrR;
        taSpecInPriceIn.AsFloat:=rPrIn;
        taSpecInRemn.AsFloat:=rREmn;
        taSpecIn.Post;

        taSpecIn.Next;
        delay(10);
      end;
      iCol:=0;
      taSpecIn.First;
      ViewDoc1.EndUpdate;
      Memo1.Lines.Add('������� ��������.'); delay(10);
    //prSetNac;
    end else Showmessage('���������� ��');
  end;
end;

procedure TfmDocsInS.cxLabel3Click(Sender: TObject);
begin
  acSetPrice.Execute;
end;

procedure TfmDocsInS.cxButton7Click(Sender: TObject);
begin
  //������������
  acOprih.Execute;
end;

procedure TfmDocsInS.acOprihExecute(Sender: TObject);
Var IDH,razrub:INteger;
begin
  //������������
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if SaveDocsIn(IdH,razrub) then
  begin
    with dmMCS do
    begin
      fmDocsInH.ViewDocsIn.BeginUpdate;
      quTTNIn.Requery();
      quTTNIn.Locate('ID',IDH,[]);
      fmDocsInH.ViewDocsIn.EndUpdate;

      if quTTnIn.Locate('ID',IDH,[])then
      begin
        if quTTNInIACTIVE.AsInteger<=1 then
        begin
          if quTTNInDATEDOC.AsDateTime<prOpenDate(quTTNInIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

          if fTestInv(quTTNInIDSKL.AsInteger,quTTNInIDATEDOC.AsInteger)=False then begin Memo1.Lines.Add('������ ������ (��������������).'); StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

          if OprDocsIn(quTTNInID.AsInteger,Memo1) then
          begin
            if CommonSet.AutoLoadCash=1 then prCashLoadFromDoc(quTTNInID.AsInteger,quTTNInIDSKL.AsInteger,1,Memo1);

            fmDocsInH.prSetValsAddDoc1(2);

            fmDocsInH.ViewDocsIn.BeginUpdate;
            quTTNIn.Requery();
            quTTNIn.Locate('ID',quTTNInID.AsInteger,[]);
            fmDocsInH.ViewDocsIn.EndUpdate;
          end;
        end;
      end;
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocsInS.acMoveExecute(Sender: TObject);
begin
  //�������� ������ - ����� �� ��������� �� �����
  with dmMCS do
  begin
    if taSpecIn.RecordCount>0 then
    begin
      prFormMove(cxLookupComboBox1.EditValue,taSpecInCodeTovar.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),taSpecInName.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmDocsInS.cxButton5Click(Sender: TObject);
Var n:INteger;
    StrIp:String;
    bErr:Boolean;
begin
  //�������� �� ���� ���������
  Memo1.Clear;
  cxButton5.Enabled:=False;
  Memo1.Lines.Add('����� ... ���� ��������� �����.'); delay(10);
  ViewDoc1.BeginUpdate;

  Memo1.Lines.Add('  - �������� ��������� �� �������� ..'); delay(10);
  taSpecIn.First;
  while not taSpecIn.Eof do
  begin
    if taSpecInCodeEdIzm.AsInteger=2 then
    begin
      Memo1.Lines.Add('      '+taSpecInCodeTovar.AsString+' '+taSpecInName.AsString+' ���� - '+fts(taSpecInPriceR.AsFloat)); delay(10);
      with dmMCS do
      begin
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('UPDATE [dbo].[SCALEPLU] Set ');
        quE.SQL.Add('[Status] = 0');
        quE.SQL.Add(',[PRICE] = '+fts(taSpecInPriceR.AsFloat));
        quE.SQL.Add('where [GOODSITEM] = '+taSpecInCodeTovar.AsString);
//        quE.SQL.Add('and [PRICE] <> '+fts(taSpecInPriceR.AsFloat));
        quE.ExecSQL;
       end;
    end;

    taSpecIn.Next;
  end;
  ViewDoc1.EndUpdate;

  Memo1.Lines.Add('  - �������� ��������� �� ���� ..'); delay(10);
  with dmMCS do
  begin
    quScales.Active:=False;
    quScales.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
    quScales.Active:=True;
    quScales.First;
    while not quScales.Eof do
    begin
      quScaleItemsCh.Active:=False;
      quScaleItemsCh.Parameters.ParamByName('INUM').Value:=quScalesIDAI.AsInteger;
      quScaleItemsCh.Active:=True;
      if quScaleItemsCh.RecordCount>0 then
      begin
        Memo1.Lines.Add('       ���� - '+quScalesNAME.AsString); delay(10);
        bErr:=False;

        if (quScalesSCALETYPE.AsString='CAS_TCP') then
        begin
          for n:=1 to 5 do
          begin
            StrIp:='';
            if n=1 then StrIP:=quScalesIP1.AsString;
            if n=2 then StrIP:=quScalesIP2.AsString;
            if n=3 then StrIP:=quScalesIP3.AsString;
            if n=4 then StrIP:=quScalesIP4.AsString;
            if n=5 then StrIP:=quScalesIP5.AsString;

            if StrIp>'' then //���-�� ���������� - ������
            begin
              Memo1.Lines.Add('         �������� - '+StrIp); delay(10);
              try
                if ScaleCas then
                begin
                  try
                    DrvScaleCas.ip := StrIP;
                    DrvScaleCas.port := 8111;
 	                  DrvScaleCas.Connect;
                    Memo1.Lines.Add('         ����������� ��');

                    quScaleItemsCh.First;
                    while not quScaleItemsCh.Eof do
                    begin
                      DrvScaleCas.groupcode := 22;
                      DrvScaleCas.ITEM := quScaleItemsChGOODSITEM.AsInteger ;
                      DrvScaleCas.LIFE := quScaleItemsChShelfLife.AsInteger;
                      DrvScaleCas.MSG := 1;

                      if length(quScaleItemsChNAME1.AsString)>0 then DrvScaleCas.MSG1 := quScaleItemsChNAME1.AsString else DrvScaleCas.MSG1 := '     ';
                      if length(quScaleItemsChNAME2.AsString)>0 then DrvScaleCas.MSG2 := quScaleItemsChNAME2.AsString else DrvScaleCas.MSG2 := '     ';

                      DrvScaleCas.NumberLogo := 1;
                      DrvScaleCas.PLUNO := quScaleItemsChPLU.AsInteger;
                      DrvScaleCas.PRICE := RoundEx(quScaleItemsChPRICE.AsFloat*100);
                      DrvScaleCas.StrLogo := '';
                      DrvScaleCas.TARE := 0;
                      DrvScaleCas.SendPLU;
                      Memo1.Lines.Add('  PLu = '+IntToStr(quScaleItemsChPLU.AsInteger)+'  -  Ok');
                      delay(10);

                      quScaleItemsCh.Next;
                    end;
          	      	DrvScaleCas.Disconnect;
                  except
                    Memo1.Lines.Add('         ������ ����������� � �����.');
                    bErr:=True;
                  end;
                end;
              except
              end;
            end; //����� IP
          end;
          if  bErr=False then
          begin
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('UPDATE [dbo].[SCALEPLU]');
            quE.SQL.Add('SET [Status] = 1');
            quE.SQL.Add('WHERE [SCALENUM]='+its(quScalesIDAI.AsInteger));
            quE.SQL.Add('and [Status]<>1');
            quE.ExecSQL;
          end;
        end;
        quScaleItemsCh.Active:=False;

        if bErr=False then Memo1.Lines.Add('       �������� ����� ���������')
        else Memo1.Lines.Add('       �������� ����� ��������� � ��������.');

        delay(10);
      end;

      quScales.Next;
    end;
    quScales.Active:=False;

  end;
  Memo1.Lines.Add('������� ��������.'); delay(10);
  cxButton5.Enabled:=True;

end;

procedure TfmDocsInS.ViewDoc1DataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  prCalcSumNac(5,4,9,ViewDoc1); //����� ������, ����� �������, ������� �������
end;

end.
