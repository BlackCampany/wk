unit PeriodUni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  Menus, dxfBackGround, cxControls, cxContainer, cxEdit, cxCheckBox;

type
  TfmPeriodUni = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    dxfBackGround1: TdxfBackGround;
    cxCheckBox1: TcxCheckBox;
    procedure cxButton2Click(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriodUni: TfmPeriodUni;

implementation

uses Un1;

{$R *.dfm}

procedure TfmPeriodUni.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPeriodUni.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    DateTimePicker2.Date:=Trunc(Date);
    DateTimePicker2.Visible:=True;
  end else
  begin
    DateTimePicker2.Date:=iMaxDate;
    DateTimePicker2.Visible:=False;
  end;
end;

procedure TfmPeriodUni.FormCreate(Sender: TObject);
begin
  DateTimePicker2.Date:=iMaxDate;
end;

procedure TfmPeriodUni.FormShow(Sender: TObject);
begin
  cxCheckBox1.Checked:=True;
  cxCheckBox1.Properties.ReadOnly:=True;
end;

end.
