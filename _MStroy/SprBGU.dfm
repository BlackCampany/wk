object fmSprBGU: TfmSprBGU
  Left = 379
  Top = 468
  Width = 501
  Height = 465
  Caption = #1041#1077#1083#1082#1080', '#1078#1080#1088#1099', '#1091#1075#1083#1077#1074#1086#1076#1099' - '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 387
    Width = 493
    Height = 44
    Align = alBottom
    BevelInner = bvLowered
    Color = 11599792
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 124
      Top = 12
      Width = 97
      Height = 25
      Caption = 'Ok'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 264
      Top = 12
      Width = 97
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrBGU: TcxGrid
    Left = 12
    Top = 8
    Width = 469
    Height = 373
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewBGU: TcxGridDBTableView
      DragMode = dmAutomatic
      OnDblClick = ViewBGUDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dstaBGU
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object ViewBGUNAMEGR: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'NAMEGR'
        Visible = False
        GroupIndex = 0
        Width = 98
      end
      object ViewBGUNAMEBGU: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMEBGU'
        Width = 154
      end
      object ViewBGUBB: TcxGridDBColumn
        Caption = #1041#1077#1083#1082#1080
        DataBinding.FieldName = 'BB'
      end
      object ViewBGUGG: TcxGridDBColumn
        Caption = #1046#1080#1088#1099
        DataBinding.FieldName = 'GG'
      end
      object ViewBGUU1: TcxGridDBColumn
        Caption = #1059#1075#1083#1077#1074#1086#1076#1099
        DataBinding.FieldName = 'U1'
      end
      object ViewBGUU2: TcxGridDBColumn
        DataBinding.FieldName = 'U2'
        Visible = False
      end
      object ViewBGUEE: TcxGridDBColumn
        Caption = #1069#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1072#1103' '#1094#1077#1085#1086#1089#1090#1100' '#1050#1082#1072#1083
        DataBinding.FieldName = 'EE'
      end
    end
    object LevelBGU: TcxGridLevel
      GridView = ViewBGU
    end
  end
end
