unit PriceType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, ExtCtrls, SpeedBar,
  XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmPriceType = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    ViewPriceType: TcxGridDBTableView;
    LevelPriceType: TcxGridLevel;
    GrPriceType: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    amPriceType: TActionManager;
    acAddPriceType: TAction;
    acEditPriceType: TAction;
    acDelPriceType: TAction;
    ViewPriceTypeID: TcxGridDBColumn;
    ViewPriceTypeNAMEPRICE: TcxGridDBColumn;
    Timer1: TTimer;
    procedure acAddPriceTypeExecute(Sender: TObject);
    procedure acEditPriceTypeExecute(Sender: TObject);
    procedure acDelPriceTypeExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPriceType: TfmPriceType;
  bClearPr:Boolean = False;

implementation

uses dmOffice, AddPriceT;

{$R *.dfm}

procedure TfmPriceType.acAddPriceTypeExecute(Sender: TObject);
Var IdCur,Id:Integer;
begin
  //��������
  if not CanDo('prAddPriceT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    IdCur:=taPriceTId.AsInteger;
    ViewPriceType.BeginUpdate;

    Id:=0;
    taPriceT.First;
    while not taPriceT.Eof do
    begin
      if taPriceTId.AsInteger>Id then Id:=taPriceTId.AsInteger;
      taPriceT.Next;
    end;
    inc(Id);

    fmAddPriceT:=tfmAddPriceT.Create(Application);
    fmAddPriceT.Caption:='���������� ���� ����.';
    fmAddPriceT.cxTextEdit1.Text:='���� � '+IntToStr(Id);

    fmAddPriceT.ShowModal;
    if fmAddPriceT.ModalResult=mrOk then
    begin
      taPriceT.Append;
      taPriceTId.AsInteger:=Id;
      taPriceTNAMEPRICE.AsString:=fmAddPriceT.cxTextEdit1.Text;
      taPriceT.Post;
    end
    else taPriceT.Locate('Id',IdCur,[]);
    fmAddPriceT.Release;
    ViewPriceType.EndUpdate;
  end;
end;

procedure TfmPriceType.acEditPriceTypeExecute(Sender: TObject);
begin
//�������������
  if not CanDo('prEditPriceT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    ViewPriceType.BeginUpdate;
    fmAddPriceT:=tfmAddPriceT.Create(Application);
    fmAddPriceT.Caption:='�������������� ���� ����: '+taPriceTNAMEPRICE.AsString;
    fmAddPriceT.cxTextEdit1.Text:=taPriceTNAMEPRICE.AsString;

    fmAddPriceT.ShowModal;
    if fmAddPriceT.ModalResult=mrOk then
    begin
      taPriceT.Edit;
      taPriceTNAMEPRICE.AsString:=fmAddPriceT.cxTextEdit1.Text;
      taPriceT.Post;
    end;
    fmAddPriceT.Release;
    ViewPriceType.EndUpdate;
  end;
end;

procedure TfmPriceType.acDelPriceTypeExecute(Sender: TObject);
begin
//�������
  if not CanDo('prDelPriceT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ��� ����: "'+taPriceTNAMEPRICE.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewPriceType.BeginUpdate;
      taPriceT.Delete;
      ViewPriceType.EndUpdate;
    end;
  end;
end;

procedure TfmPriceType.FormCreate(Sender: TObject);
begin
  GrPriceType.Align:=AlClient;
end;

procedure TfmPriceType.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPriceType.Timer1Timer(Sender: TObject);
begin
  if bClearPr=True then begin StatusBar1.Panels[0].Text:=''; bClearPr:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearPr:=True;
end;

end.
