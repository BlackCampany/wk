unit AddBuyer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, FIBQuery, pFIBQuery,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmAddBuyer = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    cxTextEdit7: TcxTextEdit;
    cxTextEdit9: TcxTextEdit;
    cxTextEdit12: TcxTextEdit;
    qAddBuyer: TpFIBQuery;
    cxDateEdit1: TcxDateEdit;
    Label4: TLabel;
    cxTextEdit4: TcxTextEdit;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddBuyer: TfmAddBuyer;

implementation

uses dmOffice, Un1, AddClient, FindResult, Goods;

{$R *.dfm}

procedure TfmAddBuyer.cxButton1Click(Sender: TObject);
var
  id:integer;

begin
  if cxButton1.Tag=0 then
    begin
    //add
    qAddBuyer.Close;
    qAddBuyer.SQL.Clear;
    qAddBuyer.SQL.Add('select max(buyer_id) as buyer_id from of_buyers');
    qAddBuyer.Transaction.StartTransaction;
      qAddBuyer.ExecQuery;
      id:=qAddBuyer.FldByName['buyer_id'].AsInteger+1;
    qAddBuyer.Transaction.Commit;

    if cxTextEdit1.Text='' then cxTextEdit1.Text:=' ';
    if cxTextEdit2.Text='' then cxTextEdit2.Text:=' ';
    if cxTextEdit3.Text='' then cxTextEdit3.Text:=' ';
    if cxTextEdit6.Text='' then cxTextEdit6.Text:='0';
    if cxTextEdit12.Text='' then cxTextEdit12.Text:='0';
    if cxTextEdit7.Text='' then cxTextEdit7.Text:=' ';
    //cxDateEdit1.Date='' then
    if cxTextEdit9.Text='' then cxTextEdit9.Text:=' ';
    if cxTextEdit4.Text='' then cxTextEdit4.Text:=' ';


    qAddBuyer.Close;
    qAddBuyer.SQL.Clear;
    qAddBuyer.SQL.Add('insert into of_buyers');
    qAddBuyer.SQL.Add('(buyer_id');
    qAddBuyer.SQL.Add(', buyer_f');
    qAddBuyer.SQL.Add(', buyer_i');
    qAddBuyer.SQL.Add(', buyer_o');
    qAddBuyer.SQL.Add(', buyer_passportseria');
    qAddBuyer.SQL.Add(', buyer_passportnomer');
    qAddBuyer.SQL.Add(', buyer_passportissuer');
    qAddBuyer.SQL.Add(', buyer_passportissuedate');
    qAddBuyer.SQL.Add(', buyer_address');
    qAddBuyer.SQL.Add(', PHONE)');
    qAddBuyer.SQL.Add('values ('''+inttostr(id)+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit1.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit2.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit3.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit6.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit12.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit7.Text+'''');
    qAddBuyer.SQL.Add(', '''+FormatDateTime('yyyy-mm-dd',cxDateEdit1.Date)+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit9.Text+'''');
    qAddBuyer.SQL.Add(', '''+cxTextEdit4.Text+''')');

    //qAddBuyer.SQL.SaveToFile('c:\m.txt');

    qAddBuyer.Transaction.StartTransaction;
    qAddBuyer.ExecQuery;
    qAddBuyer.Transaction.Commit;

    end
    else
    begin

    id:=cxButton1.Tag;
    //ShowMessage(inttostr(id));

    qAddBuyer.Close;
    qAddBuyer.SQL.Clear;
    qAddBuyer.SQL.Add('update of_buyers ');
    qAddBuyer.SQL.Add('set buyer_f = '''+cxTextEdit1.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_i = '''+cxTextEdit2.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_o = '''+cxTextEdit3.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_passportseria = '''+cxTextEdit6.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_passportnomer = '''+cxTextEdit12.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_passportissuer = '''+cxTextEdit7.Text+''', ');
    qAddBuyer.SQL.Add('    buyer_passportissuedate = '''+FormatDateTime('yyyy-mm-dd',cxDateEdit1.Date)+''', ');
    qAddBuyer.SQL.Add('    buyer_address = '''+cxTextEdit9.Text+''', ');
    qAddBuyer.SQL.Add('    PHONE = '''+cxTextEdit4.Text+''' ');
    qAddBuyer.SQL.Add('where (buyer_id = '''+inttostr(id)+''') ');

    //qAddBuyer.SQL.SaveToFile('c:\m.txt');

    qAddBuyer.Transaction.StartTransaction;
    qAddBuyer.ExecQuery;
    qAddBuyer.Transaction.Commit;

    end;

end;

end.
