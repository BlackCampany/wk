object fmXLImport: TfmXLImport
  Left = 257
  Top = 245
  Width = 809
  Height = 496
  Caption = #1048#1084#1087#1086#1088#1090
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 399
    Width = 801
    Height = 44
    Align = alBottom
    BevelInner = bvLowered
    Color = 16763025
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 452
      Top = 12
      Width = 117
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 443
    Width = 801
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 801
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16763025
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100'|'
      Spacing = 1
      Left = 4
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 524
      Top = 4
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 48
    Width = 801
    Height = 351
    Align = alClient
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
    object XLImport: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsXLImport
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object XLImportRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object XLImportName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 258
      end
      object XLImportEXTART: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'EXTART'
      end
      object XLImportStrana: TcxGridDBColumn
        Caption = #1057#1090#1088#1072#1085#1072
        DataBinding.FieldName = 'Strana'
      end
      object XLImportEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'
        DataBinding.FieldName = 'EdIzm'
      end
      object XLImportPrice: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price'
        Width = 110
      end
    end
    object cxGridXLImport: TcxGridLevel
      GridView = XLImport
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 52
    Top = 68
  end
  object teXLImport: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 244
    Top = 160
    object teXLImportName: TStringField
      FieldName = 'Name'
      Size = 200
    end
    object teXLImportEXTART: TStringField
      FieldName = 'EXTART'
      Size = 30
    end
    object teXLImportStrana: TStringField
      FieldName = 'Strana'
      Size = 30
    end
    object teXLImportEdIzm: TStringField
      FieldName = 'EdIzm'
    end
    object teXLImportPrice: TFloatField
      FieldName = 'Price'
    end
  end
  object dsXLImport: TDataSource
    DataSet = teXLImport
    Left = 372
    Top = 168
  end
  object qXLImport: TpFIBQuery
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    Left = 148
    Top = 176
  end
end
