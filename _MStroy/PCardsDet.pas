unit PCardsDet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ExtCtrls, ComCtrls;

type
  TfmPCardsDet = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    ViewPCDet: TcxGridDBTableView;
    LevelPCDet: TcxGridLevel;
    GridPCDet: TcxGrid;
    ViewPCDetNUMTABLE: TcxGridDBColumn;
    ViewPCDetQUESTS: TcxGridDBColumn;
    ViewPCDetENDTIME: TcxGridDBColumn;
    ViewPCDetDISCONT: TcxGridDBColumn;
    ViewPCDetNAME: TcxGridDBColumn;
    ViewPCDetPERCENT: TcxGridDBColumn;
    ViewPCDetTABSUM: TcxGridDBColumn;
    ViewPCDetNAME1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCardsDet: TfmPCardsDet;

implementation

uses Un1, DmRnDisc, Period;

{$R *.dfm}

procedure TfmPCardsDet.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCDet.Align:=AlClient;
  ViewPCDet.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCardsDet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCDet.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCardsDet.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPCardsDet.SpeedItem3Click(Sender: TObject);
begin
//
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=False;
  fmPeriod.DateTimePicker4.Visible:=False;
  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;

      ViewPCDet.BeginUpdate;
{      quDiscDetail.Active:=False;
      quDiscDetail.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quDiscDetail.ParamByName('DateE').AsDateTime:=TrebSel.DateTo+0.9999;
      quDiscDetail.ParamByName('SBAR').AsString:=quDiscSelBARCODE.AsString;
      quDiscDetail.Active:=True;
}      ViewPCDet.EndUpdate;

      fmPCardsDet.Caption:='������������� �� ������ � '+FormatDateTime('dd.mm.yyyy',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy',TrebSel.DateTo);

      exit;
    end;
  end;
  fmPeriod.Release;

end;

end.
