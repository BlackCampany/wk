object fmAddM: TfmAddM
  Left = 306
  Top = 353
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1075#1086' '#1073#1083#1102#1076#1072
  ClientHeight = 573
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 448
    Width = 50
    Height = 13
    Caption = #1041#1069#1050' '#1086#1092#1080#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 260
    Width = 53
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
    Transparent = True
  end
  object Label5: TLabel
    Left = 16
    Top = 296
    Width = 119
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072' '#1084#1086#1076#1080#1092#1080#1082#1072#1090#1086#1088#1086#1074
    Transparent = True
  end
  object Label6: TLabel
    Left = 16
    Top = 320
    Width = 169
    Height = 13
    Caption = #1055#1088#1077#1076#1077#1083#1100#1085#1099#1081' '#1074#1077#1089' '#1084#1086#1076#1080#1092#1080#1082#1072#1090#1086#1088#1086#1074
    Transparent = True
  end
  object Label7: TLabel
    Left = 16
    Top = 360
    Width = 162
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103' ('#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072')'
    Transparent = True
  end
  object Label8: TLabel
    Left = 168
    Top = 56
    Width = 49
    Height = 13
    Caption = #1064#1090#1088#1080#1093#1082#1086#1076
    Transparent = True
  end
  object Label9: TLabel
    Left = 168
    Top = 88
    Width = 24
    Height = 13
    Caption = #1053#1044#1057
    Transparent = True
  end
  object Label10: TLabel
    Left = 104
    Top = 452
    Width = 54
    Height = 13
    Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
    Transparent = True
  end
  object Label11: TLabel
    Left = 104
    Top = 479
    Width = 120
    Height = 13
    Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1087#1077#1088#1077#1076#1072#1095#1080
    Transparent = True
  end
  object Label12: TLabel
    Left = 16
    Top = 420
    Width = 126
    Height = 13
    Caption = #1042#1099#1093#1086#1076' 1 '#1087#1086#1088#1094#1080#1080' '#1087#1086' '#1084#1077#1085#1102
    Transparent = True
  end
  object Label13: TLabel
    Left = 16
    Top = 392
    Width = 129
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1087#1088#1086#1076#1072#1078' '#1073#1083#1102#1076#1072
    Transparent = True
  end
  object Label14: TLabel
    Left = 20
    Top = 120
    Width = 82
    Height = 13
    Caption = #1041#1083#1102#1076#1086' '#1076#1086#1089#1090#1091#1087#1085#1086
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 554
    Width = 368
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 504
    Width = 368
    Height = 50
    Align = alBottom
    BevelInner = bvLowered
    Color = 55040
    TabOrder = 7
    object cxButton1: TcxButton
      Left = 120
      Top = 16
      Width = 91
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      Colors.Default = clWhite
      Colors.Normal = clWhite
      Colors.Hot = 16757721
      Colors.Pressed = 16740279
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 240
      Top = 16
      Width = 91
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      Colors.Hot = 16757721
      Colors.Pressed = 16740279
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfFlat
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 80
    Top = 12
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 0
    Text = 'cxTextEdit1'
    Width = 265
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 240
    Top = 444
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsUltraFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.BorderStyle = ebsFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.BorderStyle = ebsFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 12
    Width = 81
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 88
    Top = 256
    ParentFont = False
    Properties.AutoSelect = False
    Properties.KeyFieldNames = 'SIFR'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        SortOrder = soAscending
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmC.dsCategUp
    Properties.ReadOnly = False
    Style.Color = 16766378
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 4
    Width = 257
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 152
    Top = 292
    ParentFont = False
    Properties.AutoSelect = False
    Properties.KeyFieldNames = 'SIFR'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmC.dsModsGr
    Properties.ReadOnly = False
    Style.Color = 16765695
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Width = 161
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 232
    Top = 84
    Properties.AutoSelect = False
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1057#1090#1072#1074#1082#1072
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmC.dsNalog
    Properties.ReadOnly = False
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 3
    Width = 113
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 240
    Top = 316
    Style.Color = 16765695
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Width = 73
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 80
    Top = 52
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Style.Color = 12058623
    Style.Shadow = True
    TabOrder = 1
    Width = 73
  end
  object cxButton3: TcxButton
    Left = 320
    Top = 292
    Width = 25
    Height = 45
    TabOrder = 9
    TabStop = False
    OnClick = cxButton3Click
    Colors.Default = 16765695
    Colors.Normal = 16765695
    Colors.Pressed = 16742844
    Glyph.Data = {
      66030000424D660300000000000036000000280000000F000000110000000100
      18000000000030030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000FF
      000080000080808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF808080FFFF
      FFFFFFFFFFFFFF000000FFFFFF0000FF000080000080000080808080FFFFFFFF
      FFFFFFFFFF0000FF000080000080808080FFFFFFFFFFFF000000FFFFFF0000FF
      000080000080000080000080808080FFFFFF0000FF0000800000800000800000
      80808080FFFFFF000000FFFFFFFFFFFF0000FF00008000008000008000008080
      8080000080000080000080000080000080808080FFFFFF000000FFFFFFFFFFFF
      FFFFFF0000FF0000800000800000800000800000800000800000800000808080
      80FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF0000FF00008000008000
      0080000080000080000080808080FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF000080000080000080000080000080808080FFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF00008000
      0080000080000080808080FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFF0000FF000080000080000080000080000080808080FFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FF00008000008000008080
      8080000080000080000080808080FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      0000FF000080000080000080808080FFFFFF0000FF0000800000800000808080
      80FFFFFFFFFFFF000000FFFFFFFFFFFF0000FF000080000080808080FFFFFFFF
      FFFFFFFFFF0000FF000080000080000080808080FFFFFF000000FFFFFFFFFFFF
      FFFFFF0000FF000080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000800000
      80000080FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF0000FF0000800000FFFFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000}
    LookAndFeel.Kind = lfStandard
  end
  object cxTextEdit2: TcxButtonEdit
    Left = 232
    Top = 48
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxTextEdit2PropertiesButtonClick
    Style.Shadow = True
    TabOrder = 2
    Width = 113
  end
  object cxLookupComboBox4: TcxLookupComboBox
    Left = 192
    Top = 354
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMESTREAM'
      end>
    Properties.ListSource = dmC.dsMH
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 10
    Width = 153
  end
  object cxCheckBox1: TcxCheckBox
    Left = 16
    Top = 88
    Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1082#1086#1083'-'#1074#1086
    TabOrder = 11
    Transparent = True
    Width = 121
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 240
    Top = 473
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsUltraFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.BorderStyle = ebsFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.BorderStyle = ebsFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 13
    Width = 81
  end
  object cxTextEdit3: TcxTextEdit
    Left = 180
    Top = 418
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 14
    Text = 'cxTextEdit3'
    Width = 169
  end
  object cxLookupComboBox5: TcxLookupComboBox
    Left = 192
    Top = 386
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMECS'
      end>
    Properties.ListSource = dmC.dstaCategSale
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 15
    Width = 153
  end
  object cxCheckBox2: TcxCheckBox
    Left = 132
    Top = 116
    Caption = #1042#1089#1077#1075#1076#1072' / '#1087#1086' '#1075#1088#1072#1092#1080#1082#1091
    Properties.OnChange = cxCheckBox2PropertiesChange
    TabOrder = 16
    Transparent = True
    Width = 201
  end
  object Panel2: TPanel
    Left = 20
    Top = 144
    Width = 329
    Height = 97
    BevelInner = bvLowered
    Color = clWhite
    Enabled = False
    TabOrder = 17
    object Label15: TLabel
      Left = 8
      Top = 16
      Width = 37
      Height = 13
      Caption = #1044#1072#1090#1099' '#1089
    end
    object Label16: TLabel
      Left = 180
      Top = 72
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object Label17: TLabel
      Left = 20
      Top = 44
      Width = 60
      Height = 13
      Caption = #1044#1085#1080' '#1085#1077#1076#1077#1083#1080
    end
    object Label18: TLabel
      Left = 8
      Top = 72
      Width = 45
      Height = 13
      Caption = #1042#1088#1077#1084#1103'  '#1089
    end
    object Label19: TLabel
      Left = 180
      Top = 16
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object cxDateEdit1: TcxDateEdit
      Left = 52
      Top = 12
      TabOrder = 0
      Width = 121
    end
    object cxDateEdit2: TcxDateEdit
      Left = 200
      Top = 12
      TabOrder = 1
      Width = 121
    end
    object cxTimeEdit1: TcxTimeEdit
      Left = 64
      Top = 68
      EditValue = 0.000000000000000000
      Properties.TimeFormat = tfHourMin
      TabOrder = 2
      Width = 101
    end
    object cxTimeEdit2: TcxTimeEdit
      Left = 204
      Top = 68
      EditValue = 0.000000000000000000
      Properties.TimeFormat = tfHourMin
      Properties.UseCtrlIncrement = True
      TabOrder = 3
      Width = 97
    end
    object cxCheckComboBox1: TcxCheckComboBox
      Left = 108
      Top = 40
      Properties.EmptySelectionText = #1053#1080#1082#1086#1075#1076#1072
      Properties.Items = <
        item
          Description = #1055#1085
        end
        item
          Description = #1042#1090
        end
        item
          Description = #1057#1088
        end
        item
          Description = #1063#1090
        end
        item
          Description = #1055#1090
        end
        item
          Description = #1057#1073
        end
        item
          Description = #1042#1089
        end>
      TabOrder = 4
      Width = 193
    end
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 12910532
    BkColor.EndColor = 3538741
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 20
    Top = 252
  end
  object amAddM: TActionManager
    Left = 80
    Top = 252
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      OnExecute = acExitExecute
    end
  end
end
