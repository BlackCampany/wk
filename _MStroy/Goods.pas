unit Goods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls, Placemnt,
  ToolWin, ActnCtrls, ActnMenus, cxControls, cxContainer, cxTreeView,
  ExtCtrls, SpeedBar, cxSplitter, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxImageComboBox,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxTextEdit, StdCtrls, cxCalc,
  Menus, cxLookAndFeelPainters, cxButtons,cxCheckListBox,cxCheckBox,
  cxCurrencyEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox;

type
  TfmCards = class(TForm)
    StatusBar1: TStatusBar;
    amG: TActionManager;
    FormPlacement1: TFormPlacement;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Panel1: TPanel;
    ClassTree: TcxTreeView;
    SpeedBar1: TSpeedBar;
    cxSplitter1: TcxSplitter;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Timer1: TTimer;
    ViewGoods: TcxGridDBTableView;
    LevelGoods: TcxGridLevel;
    GrGoods: TcxGrid;
    ViewGoodsID: TcxGridDBColumn;
    ViewGoodsNAME: TcxGridDBColumn;
    ViewGoodsIACTIVE: TcxGridDBColumn;
    ViewGoodsNAMESHORT: TcxGridDBColumn;
    ViewGoodsNAMENDS: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    acAddGoods: TAction;
    acEditGoods: TAction;
    acDelGoods: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    Action9: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    acTCard: TAction;
    SpeedItem5: TSpeedItem;
    acCardMove: TAction;
    SpeedItem6: TSpeedItem;
    N10: TMenuItem;
    acInputB: TAction;
    Excel1: TMenuItem;
    ViewGoodsLASTPRICEOUT: TcxGridDBColumn;
    acReDel: TAction;
    ViewGoodsCOMMENT: TcxGridDBColumn;
    acCalcTTK: TAction;
    acSetStatus: TAction;
    N12: TMenuItem;
    N13: TMenuItem;
    ViewGoodsIACTIVE1: TcxGridDBColumn;
    N15: TMenuItem;
    Excel2: TMenuItem;
    ViewGoodsLASTPRICEIN: TcxGridDBColumn;
    ViewGoodsEE: TcxGridDBColumn;
    acSetNac: TAction;
    N11: TMenuItem;
    acSetPriceOut: TAction;
    N14: TMenuItem;
    acFindByCode: TAction;
    procedure FormCreate(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGoodsExecute(Sender: TObject);
    procedure acEditGoodsExecute(Sender: TObject);
    procedure acDelGoodsExecute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure ViewGoodsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ClassTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ClassTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewGoodsDblClick(Sender: TObject);
    procedure ViewGoodsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewGoodsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Excel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acReDelExecute(Sender: TObject);
    procedure acCalcTTKExecute(Sender: TObject);
    procedure acSetStatusExecute(Sender: TObject);
    procedure ViewGoodsCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Excel2Click(Sender: TObject);
    procedure acSetNacExecute(Sender: TObject);
    procedure acSetPriceOutExecute(Sender: TObject);
    procedure acFindByCodeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCards: TfmCards;
  bClearClass:Boolean = False;
  bAddG:Boolean = False;

implementation

uses dmOffice, Un1, AddClass, AddGoods, FindResult, AddDoc2, DMOReps, SelPerSkl, SetStatus, XLImport,
  SetNac;

{$R *.dfm}

procedure TfmCards.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;

  ClassTree.Items.BeginUpdate;
  ClassifExpand(nil,ClassTree,dmO.quClassTree,Person.Id,1);
  ClassTree.FullExpand;
  ClassTree.FullCollapse;
  ClassTree.Items.EndUpdate;

  Timer1.Enabled:=True;
  GrGoods.Align:=AlClient;
  ViewGoods.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';


  ViewGoodsCOMMENT.Options.Editing:=CanDo('prEditGoods');
end;

procedure TfmCards.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmCards.Action1Execute(Sender: TObject);
begin
  bAddSpecIn:= False;
  bAddSpecB:= False;
  bAddSpecB1:= False;
  bAddSpecInv:= False;
  bAddSpecCompl:= False;
  bAddTSpec:= False;
  bAddSpecAO:= False;
  bAddSpecVn:=False;
  bAddSpecR:=False;

  close;
end;

procedure TfmCards.Action3Execute(Sender: TObject);
begin
 //�������������
end;

procedure TfmCards.Action5Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
    StrWk:String;
begin
// �������� ��������  ������
  if not CanDo('prAddClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddClass.Caption:='���������� ������.';
  fmAddClass.label4.Caption:='���������� ������.';
  fmAddClass.cxTextEdit1.Text:='';

  fmAddClass.ShowModal;
  if fmAddClass.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('Class');

      taClass.Active:=False;
      taClass.Active:=True;
      taClass.Append;
      taClassID.AsInteger:=iId;
      taClassID_PARENT.AsInteger:=0;
      taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
      taClassITYPE.AsInteger:=1; //������������� �������
      taClass.Post;

      StrWk:=taClass.FieldByName('NAMECL').AsString;

      ClassTree.Items.BeginUpdate;
      TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      ClassTree.Items.AddChildObject(TreeNode,'', nil);
      ClassTree.Items.EndUpdate;

      for i:=0 To ClassTree.Items.Count-1 do
        if Integer(ClassTree.Items[i].Data) = iId then
        begin
          ClassTree.Items[i].Expand(False);
          ClassTree.Items[i].Selected:=True;
          ClassTree.Repaint;
          Break;
        end;

      taClass.Active:=False;
    end;
  end;
end;

procedure TfmCards.Action6Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    CurNode : TTreeNode;
    iId,i,iParent:Integer;
    StrWk:String;
begin
//�������� ���������
  if not CanDo('prAddSubClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ���������� ���������.');
    exit;
  end;

  iParent:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iParent,[]) then
    begin
      fmAddClass.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.label4.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        iId:=GetId('Class');

        taClass.Active:=False;
        taClass.Active:=True;
        taClass.Append;
        taClassID.AsInteger:=iId;
        taClassID_PARENT.AsInteger:=iParent;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        StrWk:=taClass.FieldByName('NAMECL').AsString;

        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(CurNode,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'', nil);
        ClassTree.Items.EndUpdate;

        for i:=0 To ClassTree.Items.Count-1 do
          if Integer(ClassTree.Items[i].Data) = iId then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Items[i].Selected:=True;
            ClassTree.Repaint;
            Break;
          end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmCards.Action7Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iId:Integer;
begin
// �������������
  if not CanDo('prEditClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������������.');
    exit;
  end;

  iId:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iId,[]) then
    begin
      fmAddClass.Caption:='�������������� ������ "'+taClassNAMECL.AsString+'"';
      fmAddClass.label4.Caption:='�������������� ������"'+taClassNAMECL.AsString+'"';
      fmAddClass.cxTextEdit1.Text:=taClassNAMECL.AsString;

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        taClass.Edit;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        CurNode.Text:=fmAddClass.cxTextEdit1.Text;
      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmCards.Action8Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iRes:Integer;
begin
//�������
  if not CanDo('prDelClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������.');
    exit;
  end;

  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ "'+CurNode.Text+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prCanDelClass.ParamByName('IDCL').AsInteger:=Integer(CurNode.Data);
      prCanDelClass.ExecProc;
      iRes:=prCanDelClass.ParamByName('RESULT').AsInteger;

      if iRes=0 then
      begin //�������� ���������
        taClass.Active:=False;
        taClass.Active:=True;
        if taClass.Locate('ID',Integer(CurNode.Data),[]) then
        begin
          iId:=Integer(CurNode.Data);
          taClass.Delete;
          CurNode.Delete;
          ClassTree.Repaint;
        end;
        taClass.Active:=False;
      end;

      if iRes=1 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ���� ���������.');
      end;

      if iRes=2 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ������ �� �����.');
      end;

    end;
  end;
end;

procedure TfmCards.Timer1Timer(Sender: TObject);
begin
  if bClearClass=True then begin StatusBar1.Panels[0].Text:=''; bClearClass:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearClass:=True;
end;

procedure TfmCards.ClassTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if dmO=nil then exit;
  with dmO do
  begin
    ViewGoods.BeginUpdate;
    quCardsSel.Active:=False;
    quCardsSel.ParamByName('PARENTID').AsInteger:=Integer(Node.Data);
    quCardsSel.Active:=True;
    ViewGoods.EndUpdate;
    ClassTree.Tag:=Integer(Node.Data);
  end;
end;

procedure TfmCards.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewGoods.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmCards.acAddGoodsExecute(Sender: TObject);
Var iId:Integer;
    StrSp:String;
begin
// �������� �����
  if not CanDo('prAddGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if ClassTree.Selected=Nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;

  with dmO do
  begin
    iId:=GetId('GD');
    while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
    begin
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then break
      else iId:=GetId('GD');
    end;
    quGdsFind.Active:=False;
    if iId=100000 then
    begin
      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
      exit;
    end;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;

    with fmAddGood do
    begin
      Caption:='���������� ������.';
      Label1.Caption:='���������� ������ � ������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=''; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=''; cxTextEdit2.Properties.ReadOnly:=False;
      cxCEdit1.EditValue:=0; cxCEdit1.Properties.ReadOnly:=False;
      cxCEdit2.EditValue:=0; cxCEdit2.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=False;
      cxLookUpComboBox1.EditValue:=2; cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=2; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=1; cxLookUpComboBox3.Properties.ReadOnly:=False;

      cxCheckBox2.EditValue:=1; cxCheckBox2.Properties.ReadOnly:=False;

      if quCardsSel.RecordCount>0 then
      begin
        cxTextEdit1.Text:=quCardsSelNAME.AsString;
        cxTextEdit2.Text:=quCardsSelCOMMENT.AsString;
        cxCEdit1.EditValue:=quCardsSelLASTPRICEOUT.AsFloat;
        cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;
        cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger;
        cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger;
      end;
    end;

    bAddG:=True; //�������� ����������

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      //��������
      iId:=fmAddGood.cxSpinEdit1.Value;
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then
      begin
        StrSp:='00000000000000000000';

        quCardsSel.Append;
        quCardsSelID.AsInteger:=iId;
        quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
        quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
        quCardsSelCOMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
        quCardsSelLASTPRICEOUT.AsFloat:=rv(fmAddGood.cxCEdit1.EditValue);
        quCardsSelLASTPRICEIN.AsFloat:=rv(fmAddGood.cxCEdit2.EditValue);
        quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
        quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
        quCardsSelIACTIVE.AsInteger:=1;
        quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
        quCardsSelSPISSTORE.AsString:=StrSp;
        quCardsSel.Post;

        quCardsSel.Refresh;
      end else
      begin
        Showmessage('�������� � ����� ����� '+IntToStr(iId)+' ��� ����������, ���������� ����������.');
      end;
      quGdsFind.Active:=False;
    end;

    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
  end;
end;

procedure TfmCards.acEditGoodsExecute(Sender: TObject);
Var IId:Integer;
    StrF:String;
    StrSp:String;
begin
//������������� �����
  if not CanDo('prEditGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    iId:=quCardsSelID.AsInteger;
    StrF:=quCardsSelSPISSTORE.AsString;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;

    with fmAddGood do
    begin
      Caption:='�������������� ������.';
      Label1.Caption:='������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=quCardsSelNAME.AsString; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=quCardsSelCOMMENT.AsString; cxTextEdit2.Properties.ReadOnly:=False;

      cxCEdit1.EditValue:=rv(quCardsSelLASTPRICEOUT.AsFloat); cxCEdit1.Properties.ReadOnly:=False;
      cxCEdit2.EditValue:=rv(quCardsSelLASTPRICEIN.AsFloat); cxCEdit2.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=True;
      cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger; cxLookUpComboBox3.Properties.ReadOnly:=False;

      cxCheckBox2.EditValue:=quCardsSelIACTIVE.AsInteger; cxCheckBox2.Properties.ReadOnly:=False;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      StrSp:='00000000000000000000';

        //����������
      quCardsSel.Edit;
      quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
      quCardsSelCOMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
      quCardsSelLASTPRICEOUT.AsFloat:=rv(fmAddGood.cxCEdit1.EditValue);
      quCardsSelLASTPRICEIN.AsFloat:=rv(fmAddGood.cxCEdit2.EditValue);
      quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
      quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
      quCardsSelIACTIVE.AsInteger:=fmAddGood.cxCheckBox2.EditValue;
      quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
      quCardsSelSPISSTORE.AsString:=StrSp;
      quCardsSel.Post;

      quCardsSel.Refresh;
    end;
    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
  end;
end;

procedure TfmCards.acDelGoodsExecute(Sender: TObject);
Var iRes:Integer;
begin
// ������� �����
  if not CanDo('prDelGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������.');
      exit;
    end;

    if MessageDlg('�� ������������� ������ ������� �����: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� �������� ������: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        prDelCard.ParamByName('SIFR').AsInteger:=quCardsSelID.AsInteger;
        prDelCard.ExecProc;
        iRes:=prDelCard.ParamByName('RESULT').AsInteger;

        if iRes=0 then
        begin //�������� ���������
          quCardsSel.Delete;
          quCardsSel.Refresh;
        end else
          ShowMessage('����� �������������. �������� ����������.');
      end;
    end;
  end;
end;

procedure TfmCards.Action9Execute(Sender: TObject);
begin
// �����������
  showmessage('�������� ����������� ������� ��� �������� � ���������� �� ������ � ������ ������.');
end;

procedure TfmCards.ViewGoodsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if CanDo('prEditDocRet') then begin bDrRet:=True;  end;
  bStartGoods:=True;
end;

procedure TfmCards.ClassTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmCards.ClassTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if not CanDo('prMoveGoods') then begin StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if bDr then
  begin
    bDr:=False;
    sGr:=ClassTree.DropTarget.Text;
    iGr:=Integer(ClassTree.DropTarget.data);
    iCo:=ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              quCardsSel.Edit;
              quCardsSelPARENT.AsInteger:=iGr;
              quCardsSel.Post;
            end;
          end;
          quCardsSel.FullRefresh;
        end;
      end;
    end;
  end;
end;

procedure TfmCards.ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,ClassTree,dmO.quClassTree,Person.Id,1);
  end;
end;

procedure TfmCards.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD,LASTPRICEOUT');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      quFind.Locate('NAME',cxTextEdit1.Text,[loCaseInsensitive, loPartialKey]);
      prRowFocus(fmFind.ViewFind);

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          if quCardsSel.locate('ID',quFindID.AsInteger,[]) then prRowFocus(ViewGoods);

        end;
      end;
//      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmCards.cxButton2Click(Sender: TObject);
//�� ����
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD,LASTPRICEOUT');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where ID ='+IntToStr(iCode));
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          if quCardsSel.locate('ID',quFindID.AsInteger,[]) then prRowFocus(ViewGoods);

        end;
      end;
//      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmCards.ViewGoodsDblClick(Sender: TObject);
Var iMax:INteger;
    S1:String;
begin
  if bAddSpecIn or bAddSpecB or bAddSpecB1 or bAddSpecInv or bAddSpecRet1 or bAddSpecAO or bAddSpecVn or bAddSpecRet or bAddSpecR then
  begin //�������� � ������������ ���������
    with dmO do
    begin
      if bAddSpecIn then
      begin
      end;
      if bAddSpecR then
      begin
      end;
      if bAddSpecRet then
      begin
        if fmAddDoc2.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            //if fmAddDoc2.PageControl1.ActivePageIndex=0 then
            begin
              iMax:=1;
              fmAddDoc2.teDocsSpec.First;
              while not fmAddDoc2.teDocsSpec.Eof do
              begin
                if iMax<fmAddDoc2.teDocsSpecNum.AsInteger+1 then iMax:=fmAddDoc2.teDocsSpecNum.AsInteger+1;
                fmAddDoc2.teDocsSpec.Next;
              end;

              with fmAddDoc2 do
              begin
                ViewDoc2.BeginUpdate;

                teDocsSpec.Append;
                teDocsSpecNum.AsInteger:=iMax;
                teDocsSpeccard_id.AsInteger:=quCardsSelID.AsInteger;
                teDocsSpeccard_name.AsString:=quCardsSelNAME.AsString+' '+quCardsSelNAMESHORT.AsString;
                teDocsSpeccard_count.AsFloat:=1;
                teDocsSpeccard_price.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                teDocsSpeccard_sum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                teDocsSpeccard_pricein.AsFloat:=quCardsSelLASTPRICEIN.AsFloat;
                teDocsSpeccard_sumin.AsFloat:=quCardsSelLASTPRICEIN.AsFloat;
                teDocsSpecpost_id.AsInteger:=prFindMainGroup(quCardsSelParent.AsInteger,S1);
                teDocsSpecpost_name.AsString:=S1;
                teDocsSpecComment.AsString:='';
                teDocsSpecDProc.AsFloat:=0;
                teDocsSpecDSum.AsFloat:=0;
                teDocsSpecRSum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                teDocsSpec.Post;

                ViewDoc2.EndUpdate;
              end;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecRet1 then
      begin
        if fmAddDoc2.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            with fmAddDoc2 do
            begin
              ViewDoc2.BeginUpdate;

              teDocsSpec.Edit;
              teDocsSpeccard_id.AsInteger:=quCardsSelID.AsInteger;
              teDocsSpeccard_name.AsString:=quCardsSelNAME.AsString+' '+quCardsSelNAMESHORT.AsString;
//              teDocsSpeccard_count.AsFloat:=1;
              teDocsSpeccard_price.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
              teDocsSpeccard_sum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat*teDocsSpeccard_count.AsFloat;
              teDocsSpeccard_pricein.AsFloat:=quCardsSelLASTPRICEIN.AsFloat;
              teDocsSpeccard_sumin.AsFloat:=quCardsSelLASTPRICEIN.AsFloat*teDocsSpeccard_count.AsFloat;
              teDocsSpecpost_id.AsInteger:=prFindMainGroup(quCardsSelParent.AsInteger,S1);
              teDocsSpecpost_name.AsString:=S1;
              teDocsSpecComment.AsString:='';
              teDocsSpecDProc.AsFloat:=0;
              teDocsSpecDSum.AsFloat:=0;
              teDocsSpecRSum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
              teDocsSpec.Post;

              ViewDoc2.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecB1 then
      begin
      end;
      if bAddSpecB then
      begin
      end;
      if bAddSpecInv then
      begin
      end;
      if bAddSpecCompl then
      begin
      end;
      if bAddSpecAO then
      begin
      end;
      if bAddSpecVn then
      begin
      end;
    end;
  end else
  begin
    if bAddTSpec then
    begin
    end else
    begin
//      if ViewGoods.Controller.FocusedColumn.Name='ViewGoodsTCARD' then acTCard.Execute
//      else acEditGoods.Execute;
    end;
  end;
end;

procedure TfmCards.ViewGoodsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmCards.ViewGoodsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
{  if bDM then
  begin
    if bStartGoods then begin ResetAddVars; exit;  end;
    ResetAddVars;
  end;}
end;

procedure TfmCards.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewGoods);
end;

procedure TfmCards.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
  if dmO.quCateg.Active=False then dmO.quCateg.Active:=True;
end;

procedure TfmCards.acReDelExecute(Sender: TObject);
Var iRes,iId:INteger;
begin
  // �������������� ������� � ���������
  StatusBar1.Panels[0].Text:='����� ���� ��������������'; Delay(10);
  with dmO do
  with dmORep do
  begin
    ViewGoods.BeginUpdate;
    taDelCard.Active:=False;
    taDelCard.Active:=True;
    taDelCard.First;
    while not taDelCard.Eof do
    begin
      prDelCard.ParamByName('SIFR').AsInteger:=taDelCardSIFR.AsInteger;
      prDelCard.ExecProc;
      iRes:=prDelCard.ParamByName('RESULT').AsInteger;

      if iRes=1 then
      begin //���� ���������������

        iId:=taDelCardSIFR.AsInteger;

        quGdsFind.Active:=False;
        quGdsFind.ParamByName('IID').AsInteger:=iId;
        quGdsFind.Active:=True;
        if quGdsFind.RecordCount=0 then
        begin
          quCardsSel.Append;
          quCardsSelID.AsInteger:=iId;
          quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
          quCardsSelNAME.AsString:=taDelCardNAME.AsString;
          quCardsSelLASTPRICEOUT.AsFloat:=0;
          quCardsSelTTYPE.AsInteger:=taDelCardTTYPE.AsInteger;
          quCardsSelIMESSURE.AsInteger:=taDelCardIMESSURE.AsInteger;
          quCardsSelINDS.AsInteger:=0;
          quCardsSelMINREST.AsFloat:=0;
          quCardsSelIACTIVE.AsInteger:=0;
          quCardsSelCATEGORY.AsInteger:=1;
          quCardsSelSPISSTORE.AsString:='00000000000000000000';
          quCardsSel.Post;
        end;
      end;
      taDelCard.Next;
    end;
    taDelCard.Active:=False;
    quCardsSel.FullRefresh;
    ViewGoods.EndUpdate;
  end;
  StatusBar1.Panels[0].Text:='������ ��'; Delay(10);
end;

procedure TfmCards.acCalcTTKExecute(Sender: TObject);
begin
  //����������� ������������� �� ��������� ���
  with dmO do
  begin
  end;
end;

procedure TfmCards.acSetStatusExecute(Sender: TObject);
Var
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sList:String;
begin
  //���������� ������ ��� ���������� �������
  if ViewGoods.Controller.SelectedRecordCount>0 then
  begin
    fmSetSt:=tfmSetSt.Create(Application);
    fmSetSt.Label1.Caption:='�������� '+IntToStr(ViewGoods.Controller.SelectedRecordCount)+' �������. �������� ������?';
    fmSetSt.ShowModal;

    if fmSetSt.ModalResult=mrOk then
    begin
//      showmessage('Ok');
      sList:='';
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set IACTIVE=1');
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;

      ViewGoods.EndUpdate;
    end;

    if fmSetSt.ModalResult=mrNo then
    begin
//      showmessage('No');
      sList:='';
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set IACTIVE=0');
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;

      ViewGoods.EndUpdate;
    end;

    fmSetSt.Release;

    {
    if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmO do
      begin
        for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewGoods.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
          end;

          iNum:=Rec.Values[j];
          //��� ��� - ����������
    }
  end;
end;

procedure TfmCards.ViewGoodsCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var iType,i:SmallInt;
begin
  iType:=0;
  for i:=0 to ViewGoods.ColumnCount-1 do
  begin
    if ViewGoods.Columns[i].Name='ViewGoodsIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType<1 then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;
end;

procedure TfmCards.Excel2Click(Sender: TObject);
begin
  fmXLImport.ShowModal;
end;

procedure TfmCards.acSetNacExecute(Sender: TObject);
Var
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sList:String;
    rNac:Real;
begin
  //���������� �������
  if ViewGoods.Controller.SelectedRecordCount>0 then
  begin
    fmSetNac:=tfmSetNac.Create(Application);
    fmSetNac.Label1.Caption:='�������� '+IntToStr(ViewGoods.Controller.SelectedRecordCount)+' �������. �������� % �������?';
    fmSetNac.Caption:='���������� �������';
    fmSetNac.Label2.Caption:='���������� % �������';
    fmSetNac.ShowModal;

    if fmSetNac.ModalResult=mrOk then
    begin
//      showmessage('Ok');
      sList:='';
      rNac:=fmSetNac.cxCalcEdit1.Value;
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set EE='+fts(rNac));
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;
      ViewGoods.EndUpdate;
    end;

    fmSetNac.Release;
  end;
end;

procedure TfmCards.acSetPriceOutExecute(Sender: TObject);
Var
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sList:String;
begin
  //����������� ���� �������
  if ViewGoods.Controller.SelectedRecordCount>0 then
  begin
    if MessageDlg('�������� '+IntToStr(ViewGoods.Controller.SelectedRecordCount)+' �������. ����������� ����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      sList:='';
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set LASTPRICEOUT=cast(LASTPRICEIN*(100+EE)/100 as integer)');
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;
      ViewGoods.EndUpdate;
    end;
  end;
end;

procedure TfmCards.acFindByCodeExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

end.
