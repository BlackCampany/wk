object fmInput: TfmInput
  Left = 120
  Top = 195
  BorderStyle = bsDialog
  Caption = #1042#1086#1078#1076#1077#1085#1080#1077' '#1074' '#1073#1083#1102#1076#1072
  ClientHeight = 388
  ClientWidth = 845
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 369
    Width = 845
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object GridF: TcxGrid
    Left = 8
    Top = 16
    Width = 689
    Height = 329
    PopupMenu = PopupMenu1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewF: TcxGridDBTableView
      OnDblClick = ViewFDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquInputF
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewFIDC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
        DataBinding.FieldName = 'IDC'
        Styles.Content = dmO.cxStyle25
        Width = 46
      end
      object ViewFSHORTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'SHORTNAME'
        Styles.Content = dmO.cxStyle15
        Width = 135
      end
      object ViewFRECEIPTNUM: TcxGridDBColumn
        Caption = #8470' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
        DataBinding.FieldName = 'RECEIPTNUM'
        Width = 48
      end
      object ViewFPCOUNT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1088#1094#1080#1081
        DataBinding.FieldName = 'PCOUNT'
        Width = 39
      end
      object ViewFPVES: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1087#1086#1088#1094#1080#1080' '#1074' '#1075#1088#1072#1084#1084#1072#1093
        DataBinding.FieldName = 'PVES'
        Width = 47
      end
      object ViewFNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 44
      end
      object ViewFNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO'
        Styles.Content = dmO.cxStyle1
        Width = 56
      end
      object ViewFSGR: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGR'
        Width = 128
      end
      object ViewFSSGR: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SSGR'
        Width = 126
      end
    end
    object LevelF: TcxGridLevel
      GridView = ViewF
    end
  end
  object Panel1: TPanel
    Left = 700
    Top = 0
    Width = 145
    Height = 369
    Align = alRight
    BevelInner = bvLowered
    Color = clCaptionText
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 20
      Top = 12
      Width = 113
      Height = 25
      Caption = #1055#1077#1088#1077#1081#1090#1080
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 20
      Top = 48
      Width = 113
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object quInputF: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cts.IDC,c.PARENT,ct.SHORTNAME,ct.RECEIPTNUM,ct.PCOUNT,ct.' +
        'PVES,cts.CURMESSURE,me.NAMESHORT,cts.NETTO'
      'FROM OF_CARDSTSPEC cts'
      ''
      'left join OF_CARDST ct on ct.ID=cts.IDT'
      'left join of_messur me on me.ID=cts.CURMESSURE'
      'left join of_cards c on c.ID=cts.IDC'
      ''
      'where cts.IDCARD=:IDC and ct.DATEE>=:CURDATE'
      'and c.PARENT>0')
    OnCalcFields = quInputFCalcFields
    Transaction = dmO.trSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 48
    Top = 56
    poAskRecordCount = True
    object quInputFIDC: TFIBIntegerField
      FieldName = 'IDC'
    end
    object quInputFSHORTNAME: TFIBStringField
      FieldName = 'SHORTNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quInputFRECEIPTNUM: TFIBStringField
      FieldName = 'RECEIPTNUM'
      Size = 30
      EmptyStrToNull = True
    end
    object quInputFPCOUNT: TFIBIntegerField
      FieldName = 'PCOUNT'
    end
    object quInputFPVES: TFIBFloatField
      FieldName = 'PVES'
    end
    object quInputFCURMESSURE: TFIBIntegerField
      FieldName = 'CURMESSURE'
    end
    object quInputFNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quInputFNETTO: TFIBFloatField
      FieldName = 'NETTO'
      DisplayFormat = '0.###'
    end
    object quInputFPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quInputFSGR: TStringField
      FieldKind = fkCalculated
      FieldName = 'SGR'
      Size = 50
      Calculated = True
    end
    object quInputFSSGR: TStringField
      FieldKind = fkCalculated
      FieldName = 'SSGR'
      Size = 50
      Calculated = True
    end
  end
  object dsquInputF: TDataSource
    DataSet = quInputF
    Left = 120
    Top = 56
  end
  object PopupMenu1: TPopupMenu
    Left = 264
    Top = 112
    object Excel1: TMenuItem
      Caption = #1069#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object JN1: TMenuItem
      Action = acOpenTTKFromInput
    end
  end
  object amInput: TActionManager
    Left = 372
    Top = 96
    StyleName = 'XP Style'
    object acOpenTTKFromInput: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1058#1058#1050
      ShortCut = 114
      OnExecute = acOpenTTKFromInputExecute
    end
  end
end
