unit SelPartIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxImageComboBox;

type
  TfmPartIn = class(TForm)
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    Panel2: TPanel;
    ViewPartIn: TcxGridDBTableView;
    LevelPartIn: TcxGridLevel;
    GrPartIn: TcxGrid;
    ViewPartInID: TcxGridDBColumn;
    ViewPartInIDDOC: TcxGridDBColumn;
    ViewPartInDTYPE: TcxGridDBColumn;
    ViewPartInQPART: TcxGridDBColumn;
    ViewPartInQREMN: TcxGridDBColumn;
    ViewPartInPRICEIN: TcxGridDBColumn;
    ViewPartInPRICEOUT: TcxGridDBColumn;
    ViewPartInIDATE: TcxGridDBColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ViewPartInSDATE: TcxGridDBColumn;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewPartInDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPartIn: TfmPartIn;

implementation

uses Un1, dmOffice;

{$R *.dfm}

procedure TfmPartIn.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPartIn.FormCreate(Sender: TObject);
begin
  GrPartIn.Align:=AlClient;
end;

procedure TfmPartIn.ViewPartInDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
