unit DocsOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  cxContainer, cxTextEdit, cxMemo, FR_DSet, FR_DBSet, FR_Class, FIBQuery,
  pFIBQuery, dxmdaset, cxBlobEdit;

type
  TfmDocsOut = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOut: TcxGrid;
    ViewDocsOut: TcxGridDBTableView;
    LevelDocsOut: TcxGridLevel;
    amDocsOut: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acCopy: TAction;
    acPost: TAction;
    Memo1: TcxMemo;
    N3: TMenuItem;
    Excel1: TMenuItem;
    acPrintDO: TAction;
    frRepDocsOD: TfrReport;
    frquSpecOutSel: TfrDBDataSet;
    acPrintSCHF: TAction;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    acExit: TAction;
    pDocs: TpFIBQuery;
    dsDocs: TDataSource;
    teDocs: TdxMemData;
    teDocsdoc_id: TIntegerField;
    teDocsdoc_VERSION: TIntegerField;
    teDocsdoc_number: TStringField;
    teDocsdoc_date: TDateTimeField;
    teDocsdoc_versiondate: TDateTimeField;
    teDocsbuyer_fio: TStringField;
    teDocsdoc_sum: TFloatField;
    teDocsSpec: TdxMemData;
    teDocsdoc_pict: TBlobField;
    ViewDocsOutDOCNUM: TcxGridDBColumn;
    ViewDocsOutDOCDATE: TcxGridDBColumn;
    ViewDocsOutSUMOUT: TcxGridDBColumn;
    ViewDocsOutBUYER_FIO: TcxGridDBColumn;
    SpeedItem9: TSpeedItem;
    ViewDocsOutNAME: TcxGridDBColumn;
    ViewDocsOutSUMIN: TcxGridDBColumn;
    acSumInP: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acPostExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acPrintDOExecute(Sender: TObject);
    procedure acPrintSCHFExecute(Sender: TObject);
    procedure ViewDocsOutCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N6Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure DocRef();
    procedure SpeedItem9Click(Sender: TObject);
    procedure acSumInPExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    Procedure prOn(IDH,IdSkl,IDate,iCli:Integer; Var rSumO,rSumT:Real);
  end;

var
  fmDocsOut: TfmDocsOut;
  bClearDocOut:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc2, SelPartIn1, DMOReps, MainMStroy;

{$R *.dfm}

Procedure TfmDocsOut.prOn(IDH,IdSkl,IDate,iCli:Integer; Var rSumO,rSumT:Real);
begin
  with dmO do
  with dmORep do
  begin
  end;
end;

procedure TfmDocsOut.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  delay(100);
end;


procedure TfmDocsOut.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOut.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOut.Align:=AlClient;
  ViewDocsOut.RestoreFromIniFile(CurDir+GridIni);
  //���� ����� ������� �������� ������ ���
  with dmO do
  begin
    if taNDS.Active=False then taNDS.Active:=True;
    taNDS.First;
    while not taNDS.Eof do
    begin
      //if taNDSID.AsInteger=1 then ViewDocsOutSUMNDS0.Caption:=taNDSNAMENDS.AsString;
      //if taNDSID.AsInteger=2 then ViewDocsOutSUMNDS1.Caption:=taNDSNAMENDS.AsString;
      //if taNDSID.AsInteger=3 then ViewDocsOutSUMNDS2.Caption:=taNDSNAMENDS.AsString;
      taNDS.Next;
    end;
  end;
  StatusBar1.Color:= UserColor.TTnOut;
  SpeedBar1.Color := UserColor.TTnOut;
end;

procedure TfmDocsOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewDocsOut.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsOut.acPeriodExecute(Sender: TObject);
begin
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.ShowModal;

  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;
    DocRef();
  end;
end;

procedure TfmDocsOut.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������

  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  begin
    fmAddDoc2.Caption:='���������: ����� ��������.';
    //fmAddDoc2.cxButton1.tag:=0;
    AddDoc2.IdH:=0;
    fmAddDoc2.Tag:=0;
    fmAddDoc2.cxTextEdit3.Tag:=0;
    fmAddDoc2.cxTextEdit3.Text:=prGetNum(1,0);
    fmAddDoc2.cxTextEdit3.Properties.ReadOnly:=False;
//    if cxTextEdit1.Text=prGetNum(1,0) then prGetNum(1,1); //��������

    //fmAddDoc2.cxTextEdit2.Text:='';
    //fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;

    fmAddDoc2.cxDateEdit1.Date:=Date;
    fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxButtonEdit1.Tag:=0;
    fmAddDoc2.cxButtonEdit1.EditValue:=0;
    fmAddDoc2.cxButtonEdit1.Text:='';
    fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxCurrencyEdit1.Value:=0;
    fmAddDoc2.cxCurrencyEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxTextEdit1.Text:='';
    fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxSpinEdit2.Value:=0;
    fmAddDoc2.cxSpinEdit2.Properties.ReadOnly:=False;

    fmAddDoc2.cxBlobEdit1.Clear;
    fmAddDoc2.cxBlobEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxLabel1.Enabled:=True;
    fmAddDoc2.cxLabel2.Enabled:=True;
    fmAddDoc2.cxLabel3.Enabled:=True;
    fmAddDoc2.cxLabel4.Enabled:=True;
    fmAddDoc2.cxLabel7.Enabled:=True;
    fmAddDoc2.cxLabel8.Enabled:=True;
    fmAddDoc2.cxLabel11.Enabled:=True;
    fmAddDoc2.Label1.Caption:='';

    fmAddDoc2.acAddPos.Enabled:=True;
    fmAddDoc2.acAddList.Enabled:=True;
    fmAddDoc2.acEditPos.Enabled:=True;

    fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
    fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

    fmAddDoc2.cxButton1.Enabled:=True;

    quDV.Active:=False;
    quDV.ParamByName('IDH').AsInteger:=-1;
    quDV.Active:=True;

    CloseTe(fmAddDoc2.teDocsSpec);

    fmAddDoc2.acSave.Enabled:=True;

    fmAddDoc2.ViewDoc2DProc.Visible:=False;
    fmAddDoc2.ViewDoc2DSum.Visible:=False;
    fmAddDoc2.ViewDoc2RSum.Visible:=False;

    fmAddDoc2.ShowModal;
    DocRef();
  end;
end;

procedure TfmDocsOut.acEditDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDH.RecordCount>0 then //���� ��� �������������
    begin
      quDH.Refresh;

      fmAddDoc2.Caption:='���������: ��������������.';
      AddDoc2.IdH:=quDHID.AsInteger;
      fmAddDoc2.Tag:=quDHID.AsInteger;

      fmAddDoc2.cxTextEdit3.Tag:=0;
      fmAddDoc2.cxTextEdit3.Text:=quDHDOCNUM.AsString;
      fmAddDoc2.cxTextEdit3.Properties.ReadOnly:=False;

      fmAddDoc2.cxDateEdit1.Date:=quDHDOCDATE.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.cxButtonEdit1.Tag:=quDHIBUYER.AsInteger;
      fmAddDoc2.cxButtonEdit1.EditValue:=quDHIBUYER.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quDHBUYER_FIO.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.cxLabel1.Enabled:=True;
      fmAddDoc2.cxLabel2.Enabled:=True;
      fmAddDoc2.cxLabel3.Enabled:=True;
      fmAddDoc2.cxLabel4.Enabled:=True;
      fmAddDoc2.cxLabel7.Enabled:=True;
      fmAddDoc2.cxLabel8.Enabled:=True;
      fmAddDoc2.cxLabel11.Enabled:=True;
      fmAddDoc2.Label1.Caption:='( ��� �� '+quDHAKTDATE.AsString+')';

      fmAddDoc2.acAddPos.Enabled:=True;
      fmAddDoc2.acAddList.Enabled:=True;
      fmAddDoc2.acEditPos.Enabled:=True;

      fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

      fmAddDoc2.cxButton1.Enabled:=True;

      fmAddDoc2.cxCurrencyEdit1.Value:=0;
      fmAddDoc2.cxCurrencyEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.cxTextEdit1.Text:='';
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.cxSpinEdit2.Value:=0;
      fmAddDoc2.cxSpinEdit2.Properties.ReadOnly:=False;

      fmAddDoc2.cxBlobEdit1.Clear;
      fmAddDoc2.cxBlobEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.ViewDoc2DProc.Visible:=False;
      fmAddDoc2.ViewDoc2DSum.Visible:=False;
      fmAddDoc2.ViewDoc2RSum.Visible:=False;

      fmAddDoc2.DocOpen(quDHID.AsInteger);
      fmAddDoc2.Tag:=quDHID.AsInteger;


      fmAddDoc2.ShowModal;
      
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsOut.acViewDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDH.RecordCount>0 then //���� ��� ��������
    begin
      quDH.Refresh;
      fmAddDoc2.Caption:='���������: ��������.';
      AddDoc2.IdH:=quDHID.AsInteger;
      fmAddDoc2.Tag:=quDHID.AsInteger;

      fmAddDoc2.cxTextEdit3.Tag:=0;
      fmAddDoc2.cxTextEdit3.Text:=quDHDOCNUM.AsString;
      fmAddDoc2.cxTextEdit3.Properties.ReadOnly:=True;

      fmAddDoc2.cxDateEdit1.Date:=quDHDOCDATE.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=True;

      fmAddDoc2.cxButtonEdit1.Tag:=quDHIBUYER.AsInteger;
      fmAddDoc2.cxButtonEdit1.EditValue:=quDHIBUYER.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quDHBUYER_FIO.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=True;

      fmAddDoc2.cxLabel1.Enabled:=False;
      fmAddDoc2.cxLabel2.Enabled:=False;
      fmAddDoc2.cxLabel3.Enabled:=False;
      fmAddDoc2.cxLabel4.Enabled:=False;
      fmAddDoc2.cxLabel7.Enabled:=False;
      fmAddDoc2.cxLabel8.Enabled:=False;
      fmAddDoc2.cxLabel11.Enabled:=False;
      fmAddDoc2.Label1.Caption:='( ��� �� '+quDHAKTDATE.AsString+')';

      fmAddDoc2.acAddPos.Enabled:=False;
      fmAddDoc2.acAddList.Enabled:=False;
      fmAddDoc2.acEditPos.Enabled:=False;

      fmAddDoc2.ViewDoc2.OptionsData.Editing:=False;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=False;

      fmAddDoc2.cxButton1.Enabled:=False;

      fmAddDoc2.cxCurrencyEdit1.Value:=0;
      fmAddDoc2.cxCurrencyEdit1.Properties.ReadOnly:=True;

      fmAddDoc2.cxTextEdit1.Text:='';
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=True;

      fmAddDoc2.cxSpinEdit2.Value:=0;
      fmAddDoc2.cxSpinEdit2.Properties.ReadOnly:=True;

      fmAddDoc2.cxBlobEdit1.Clear;
      fmAddDoc2.cxBlobEdit1.Properties.ReadOnly:=True;

      fmAddDoc2.ViewDoc2DProc.Visible:=False;
      fmAddDoc2.ViewDoc2DSum.Visible:=False;
      fmAddDoc2.ViewDoc2RSum.Visible:=False;

      fmAddDoc2.DocOpen(quDHID.AsInteger);
      fmAddDoc2.Tag:=quDHID.AsInteger;
      fmAddDoc2.ShowModal;

    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;

end;

procedure TfmDocsOut.ViewDocsOutDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
//    if quDocsOutSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsOut.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
  end;
end;

procedure TfmDocsOut.acOnDoc1Execute(Sender: TObject);
begin
//������������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOnDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    prButtonSet(False);
  end;
  prButtonSet(True);
end;

procedure TfmDocsOut.acOffDoc1Execute(Sender: TObject);
begin
//��������
  with dmO do
  begin
    if not CanDo('prOffDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsOut.Timer1Timer(Sender: TObject);
begin
  if bClearDocOut=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOut:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOut:=True;
end;

procedure TfmDocsOut.acCopyExecute(Sender: TObject);
begin
  //����������
  with dmO do
  with dmORep do
  begin
  end;
end;

procedure TfmDocsOut.acPostExecute(Sender: TObject);
//Var iCType:Integer;
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;
  end;
end;

procedure TfmDocsOut.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  ViewDocsOutSUMIN.Visible:=False;
end;

procedure TfmDocsOut.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsOut);
end;

procedure TfmDocsOut.acPrintDOExecute(Sender: TObject);
begin
  if LevelDocsOut.Visible=False then exit;
  with dmO do
  begin
  end;
end;

procedure TfmDocsOut.acPrintSCHFExecute(Sender: TObject);
begin
  if LevelDocsOut.Visible=False then exit;
  with dmO do
  begin
  end;
end;

procedure TfmDocsOut.ViewDocsOutCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsOut.ColumnCount-1 do
  begin
    if ViewDocsOut.Columns[i].Name='ViewDocsOutIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;
  //if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF;
end;

procedure TfmDocsOut.N6Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnOut := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsOut.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOut.DocRef();
begin
  fmDocsOut.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    ViewDocsOut.BeginUpdate;
    try
      quDH.Active:=False;
      quDH.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDH.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDH.Active:=True;
    finally
      ViewDocsOut.EndUpdate;
    end;
  end;
end;

procedure TfmDocsOut.SpeedItem9Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsOut);
end;

procedure TfmDocsOut.acSumInPExecute(Sender: TObject);
begin
  if ViewDocsOutSUMIN.Visible then ViewDocsOutSUMIN.Visible:=False
  else ViewDocsOutSUMIN.Visible:=True;
end;

end.
