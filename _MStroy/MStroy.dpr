// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program MStroy;

uses
  Forms,
  MainMStroy in 'MainMStroy.pas' {fmMainMStroy},
  dmOffice in 'dmOffice.pas' {dmO: TDataModule},
  Un1 in 'Un1.pas',
  OMessure in 'OMessure.pas' {fmMessure},
  AddMess in 'AddMess.pas' {fmAddMess},
  Goods in 'Goods.pas' {fmCards},
  AddClass in 'AddClass.pas' {fmAddClass},
  PriceType in 'PriceType.pas' {fmPriceType},
  AddPriceT in 'AddPriceT.pas' {fmAddPriceT},
  MH in 'MH.pas' {fmMH},
  AddMH in 'AddMH.pas' {fmAddMH},
  AddGoods in 'AddGoods.pas' {fmAddGood},
  AddBar in 'AddBar.pas' {fmAddBar},
  FindResult in 'FindResult.pas' {fmFind},
  Clients in 'Clients.pas' {fmClients},
  AddClient in 'AddClient.pas' {fmAddClients},
  PeriodUni in 'PeriodUni.pas' {fmPeriodUni},
  CurMessure in 'CurMessure.pas' {fmCurMessure},
  SelPerSkl in 'SelPerSkl.pas' {fmSelPerSkl},
  DocsOut in 'DocsOut.pas' {fmDocsOut},
  AddDoc2 in 'AddDoc2.pas' {fmAddDoc2},
  Message in 'Message.pas' {fmMessage},
  DMOReps in 'DMOReps.pas' {dmORep: TDataModule},
  u2fdk in 'U2FDK.PAS',
  TOSel in 'TOSel.pas' {fmTO},
  FCards in 'FCards.pas' {fmFCards},
  Refer in 'Refer.pas' {fmComm},
  PerA_Office in 'PerA_Office\PerA_Office.pas' {fmPerA_Office},
  ClassSel in 'ClassSel.pas' {fmClassSel},
  ParamSel in 'ParamSel.pas' {fmParamSel},
  of_EditPosMenu in 'of_EditPosMenu.pas' {fmEditPosM},
  of_AvansRep in 'of_AvansRep.pas' {fmRepAvans},
  SetStatus in 'SetStatus.pas' {fmSetSt},
  sumprops in 'SummaProp\sumprops.pas',
  ExcelList in 'ExcelList.pas' {fmExcelList},
  XLImport in 'XLImport.pas' {fmXLImport},
  Buyers in 'Buyers.pas' {fmBuyers},
  AddBuyer in 'AddBuyer.pas' {fmAddBuyer},
  SetNac in 'SetNac.pas' {fmSetNac},
  Shabl in 'Shabl.pas' {fmBuff},
  SetDate in 'SetDate.pas' {fmSetDate},
  Oplata in 'Oplata.pas' {fmOplata};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainMStroy, fmMainMStroy);
  Application.CreateForm(TdmO, dmO);
  Application.CreateForm(TfmMessure, fmMessure);
  Application.CreateForm(TfmAddMess, fmAddMess);
  Application.CreateForm(TfmAddClass, fmAddClass);
  Application.CreateForm(TfmPriceType, fmPriceType);
  Application.CreateForm(TfmMH, fmMH);
  Application.CreateForm(TfmAddGood, fmAddGood);
  Application.CreateForm(TfmFind, fmFind);
  Application.CreateForm(TfmClients, fmClients);
  Application.CreateForm(TfmAddClients, fmAddClients);
  Application.CreateForm(TfmPeriodUni, fmPeriodUni);
  Application.CreateForm(TfmCurMessure, fmCurMessure);
  Application.CreateForm(TfmDocsOut, fmDocsOut);
  Application.CreateForm(TfmAddDoc2, fmAddDoc2);
  Application.CreateForm(TdmORep, dmORep);
  Application.CreateForm(TfmTO, fmTO);
  Application.CreateForm(TfmFCards, fmFCards);
  Application.CreateForm(TfmXLImport, fmXLImport);
  Application.CreateForm(TfmBuyers, fmBuyers);
  Application.CreateForm(TfmAddBuyer, fmAddBuyer);
  Application.CreateForm(TfmBuff, fmBuff);
  Application.CreateForm(TfmSetDate, fmSetDate);
  Application.CreateForm(TfmOplata, fmOplata);
  Application.Run;
end.
