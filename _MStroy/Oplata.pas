unit Oplata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls, ExtCtrls, cxImageComboBox,
  cxCalendar, cxCalc, dxmdaset, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmOplata = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    ViewOplata: TcxGridDBTableView;
    LevOplata: TcxGridLevel;
    GrOplata: TcxGrid;
    teOplata: TdxMemData;
    teOplataNum: TIntegerField;
    teOplataNamePl: TStringField;
    teOplataDatePl: TDateField;
    teOplataSumPl: TFloatField;
    dsteOplata: TDataSource;
    ViewOplataNum: TcxGridDBColumn;
    ViewOplataNamePl: TcxGridDBColumn;
    ViewOplataDatePl: TcxGridDBColumn;
    ViewOplataSumPl: TcxGridDBColumn;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    amPl: TActionManager;
    acAddPl: TAction;
    acDelPl: TAction;
    acSaveOpl: TAction;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure acAddPlExecute(Sender: TObject);
    procedure acDelPlExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveOplExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOplata: TfmOplata;

implementation

uses Un1, dmOffice;

{$R *.dfm}

procedure TfmOplata.FormCreate(Sender: TObject);
begin
  GrOplata.Align:=AlClient;
end;

procedure TfmOplata.acAddPlExecute(Sender: TObject);
Var iMax:Integer;
begin
//�������� ������
  iMax:=1;
  if teOplata.RecordCount>0 then
  begin
    teOplata.Last;
    iMax:=teOplataNum.AsInteger+1;
  end;

  teOplata.Append;
  teOplataNum.AsInteger:=iMax;
  if iMax=1 then teOplataNamePl.AsString:='��������� ������'
  else teOplataNamePl.AsString:='������������� ������';
  teOplataDatePl.AsDateTime:=Date;
  teOplataSumPl.AsFloat:=0;
  teOplata.Post;



end;

procedure TfmOplata.acDelPlExecute(Sender: TObject);
begin
  if teOplata.RecordCount>0 then
  begin
    teOplata.Delete;
  end;
end;

procedure TfmOplata.cxButton2Click(Sender: TObject);
begin
  CloseTe(teOplata);
  Close;
end;

procedure TfmOplata.acSaveOplExecute(Sender: TObject);
Var Idh:INteger;
begin
//����������
  with dmO do
  begin
    IdH:=fmOplata.Tag;

    quPlat.Active:=False;
    quPlat.ParamByName('IDH').AsInteger:=IDH;
    quPlat.Active:=True;

    quPlat.First;
    while not quPlat.Eof do quPlat.Delete;

    teOplata.First;
    while not teOplata.eof do
    begin
      quPlat.Append;
      quPlatIDH.AsInteger:=IdH;
      quPlatNUM.AsInteger:=teOplataNum.AsInteger;
      quPlatNAMEPL.AsString:=teOplataNamePl.AsString;
      quPlatDATEPL.AsDateTime:=Trunc(teOplataDatePl.AsDateTime);
      quPlatSUMPL.AsFloat:=teOplataSumPl.AsFloat;
      quPlat.Post;

      teOplata.Next;
    end;

    quPlat.Active:=False;

    showmessage('���������� ��');
  end;
end;

end.
