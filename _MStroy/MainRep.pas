unit MainRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar, FR_Class, FR_DSet,
  FR_DBSet, DB, DBClient, FR_Desgn, frexpimg, frRtfExp, frXMLExl, frOLEExl,
  frHTMExp, FR_E_HTML2, FR_E_CSV, FR_E_RTF, FR_E_TXT, frTXTExp, FR_BarC,
  FR_Chart;

type
  TfmMainRep = class(TForm)
    StatusBar1: TStatusBar;
    am: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    SpeedBar1: TSpeedBar;
    acExit: TAction;
    acRep1: TAction;
    dsRep1: TfrDBDataSet;
    Timer1: TTimer;
    acCashReal: TAction;
    acSaleWaiters: TAction;
    acSaleCateg: TAction;
    dsRep2: TfrDBDataSet;
    frRepMain2: TfrReport;
    dsRep3: TfrDBDataSet;
    dsRep4: TfrDBDataSet;
    acTabs: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acProtocol: TAction;
    acCaher: TAction;
    dsRep5: TfrDBDataSet;
    acHour: TAction;
    taHour: TClientDataSet;
    taHourNum: TIntegerField;
    taHourPeriod: TStringField;
    taHourChecks: TIntegerField;
    taHourQuests: TIntegerField;
    taHourRSum: TCurrencyField;
    taHourProc: TStringField;
    taHourRProc: TFloatField;
    dsHour: TfrDBDataSet;
    acWeek: TAction;
    acOutGr: TAction;
    acOutKat: TAction;
    acDel: TAction;
    dsRep1_: TfrDBDataSet;
    dsRep2_: TfrDBDataSet;
    dsRepDel: TfrDBDataSet;
    acRealMH: TAction;
    dsRep7: TfrDBDataSet;
    acSaleMH: TAction;
    dsRep8: TfrDBDataSet;
    acRealPerDay: TAction;
    taRDT: TClientDataSet;
    taRDTSifr: TIntegerField;
    taRDTSUMQUANT: TFloatField;
    taRDTSUMSUM: TFloatField;
    taRDTSUMDISC: TFloatField;
    taRDTNAME: TStringField;
    taRDTPARENT: TIntegerField;
    taRDTNAMEGR: TStringField;
    dsRDT: TDataSource;
    frtaRDT: TfrDBDataSet;
    acRealDay: TAction;
    SpeedItem2: TSpeedItem;
    frRepMain1: TfrReport;
    acSaleMHStation: TAction;
    dsRep10: TfrDBDataSet;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acRep1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acCashRealExecute(Sender: TObject);
    procedure acSaleWaitersExecute(Sender: TObject);
    procedure acSaleCategExecute(Sender: TObject);
    procedure acTabsExecute(Sender: TObject);
    procedure acProtocolExecute(Sender: TObject);
    procedure acCaherExecute(Sender: TObject);
    procedure acHourExecute(Sender: TObject);
    procedure acWeekExecute(Sender: TObject);
    procedure acOutGrExecute(Sender: TObject);
    procedure acOutKatExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acRealMHExecute(Sender: TObject);
    procedure acSaleMHExecute(Sender: TObject);
    procedure acRealPerDayExecute(Sender: TObject);
    procedure acRealDayExecute(Sender: TObject);
    procedure acSaleMHStationExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainRep: TfmMainRep;

implementation

uses Dm, Un1, Passw, Period, Tabs, LogSpec, Period2, RepGridDayTime,
  RepRealDays;

{$R *.dfm}

procedure TfmMainRep.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125;
end;

procedure TfmMainRep.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmC do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul4=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA:=TfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA.Release;
  end
  else
  begin
    fmPerA.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRep.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRep.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  StrPre:='';
  TrebSel.DateFrom:=Trunc(Date);
  TrebSel.DateTo:=Trunc(Date)+1;
end;

procedure TfmMainRep.acRep1Execute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� �������
  if not CanDo('SaleGroup') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin

    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep1.Active:=False;
      quRep1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep1.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'SaleGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['CountRec']:=IntToStr(quRep1.RecordCount);

      frRepMain1.ReportName:='���������� �� �������. ('+IntToStr(quRep1.RecordCount)+')';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
//      quRep1.Active:=False;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.Timer1Timer(Sender: TObject);
begin
  if bClearStatusBar=True then begin StatusBar1.Panels[0].Text:=''; Delay(10); bClearStatusBar:=False end;
  if StatusBar1.Panels[0].Text>'' then bClearStatusBar:=True;
end;

procedure TfmMainRep.acCashRealExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ������� �� ������
  if not CanDo('CashReal') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep3.Active:=False;
      quRep3.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep3.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleCash.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������� �� ������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleWaitersExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumSt:Real;
    StrWk,StrWk1:String;
begin
// ���������� �� ����������
  if not CanDo('SaleWaiter') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep4.Active:=False;
      quRep4.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep4.ParamByName('DateE').AsDateTime:=DateEnd;
      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep4.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('���������� �� ����������.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ��������               �����     ������  ';
            //       1234 �������� ������ 18 999.999 9999.99
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          rSum:=0; //����� ������
          rSumSt:=0;  //����� ������
          SelFont(15);

          quRep4.First;
          while not quRep4.Eof do
          begin
            rSum:=rSum+quRep4SUMSUM.AsFloat;
            rSumSt:=rSumSt+quRep4SUMQ.AsFloat;
            //��������
            StrWk1:=Copy(quRep4NAME.AsString,1,25);
            while Length(StrWk1)<25 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //�����
            Str(quRep4SUMSUM.AsFloat:9:2,StrWk1);
            while Length(StrWk1)<9 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //������
            Str(quRep4SUMQ.AsFloat:5,StrWk1);
            while Length(StrWk1)<5 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1;

            PrintStr(StrWk);

            quRep4.Next;
          end;

          PrintStr('');
          SelFont(13);
          Str(rSumSt:8:2,StrWk);
          Str(rSum:8:2,StrWk1);

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          StrWk:='�����               '+StrWk1+' '+StrWk;
          PrintStr(StrWk);
          PrintStr('');

          CutDocPr;
        finally
          quRep4.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������
        frRepMain1.LoadFromFile(CurDir + 'SaleWaiters.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain1.ReportName:='���������� �� ����������.';
        frRepMain1.PrepareReport;
        frRepMain1.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleCategExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ���������
  if not CanDo('SaleCateg') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2.Active:=False;
      quRep2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleCateg.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������� �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acTabsExecute(Sender: TObject);
begin
  with dmC do
  begin
    taTabsAllSel.Active:=False;
    taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
    taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
    taTabsAllSel.Active:=True;
    taTabsAllSel.Last;

    quSelSpecAll.Active:=False;
    quSelSpecAll.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
    quSelSpecAll.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
    quSelSpecAll.Active:=True;
  end;
  fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateTo);
  fmTabs.show;
end;

procedure TfmMainRep.acProtocolExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ����������
  if not CanDo('SaleWaiter') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin //������ ��������� ����������
      quRepLog.Active:=False;
//      quRepLog.ParamByName('DATEB').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateBeg);
//      quRepLog.ParamByName('DATEE').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateEnd);
      quRepLog.ParamByName('DATEB').AsDateTime:=DateBeg;
      quRepLog.ParamByName('DATEE').AsDateTime:=DateEnd;
      quRepLog.Active:=True;

      fmLogSpec.Show;


    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acCaherExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ��������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep5.Active:=False;
      quRep5.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep5.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'SaleCashers.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='���������� �� ��������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acHourExecute(Sender: TObject);
type THStatREc = record
     Checks,Quests:INteger;
     rSum:Currency;
     end;

Var DateBeg,DateEnd:TDateTime;
    HStat:Array[1..24] of THStatrec;
    n:Integer;
    rSumAll:Currency;
    p,i:Real;
    StrWk:String;


Function CalcTimeToH(tt:TDateTime):ShortInt;
begin
  tt:=Frac(tt);
  Result:=StrToINtDef(FormatDateTime('hh',tt),0);
end;

begin
  //��������� ������� - �������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      //������� ������������� ������
      for n:=1 to 24 do
      begin
        hStat[n].Checks:=0; hStat[n].Quests:=0; hStat[n].rSum:=0;
      end;

      quRep6.Active:=False;
      quRep6.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep6.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep6.Active:=True;

      quRep6.First;
      while not quRep6.eof do
      begin
        n:=CalcTimeToH(quRep6CHDATE.AsDateTime)+1; //+1 ���� ���� ����� �������� � ������� �� 1 - ����� �������
        inc(hStat[n].Checks);
        hStat[n].Quests:=hStat[n].Quests+quRep6QUESTS.AsInteger;
        hStat[n].rSum:=hStat[n].rSum+quRep6TABSUM.AsFloat;
        quRep6.Next;
        delay(10);
      end;
      //������ ������ ����� ��� ������������ ��������     ======
      rSumAll:=0;
      for n:=1 to 24 do rSumAll:=rSumAll+hStat[n].rSum;


      //������ ���� �������� - �������� �������
      taHour.Active:=False;
      taHour.CreateDataSet;

      for n:=10 to 24 do //��� �� � ������ ����� ���� ����� ���������
      begin

        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;
      for n:=1 to 9 do //��� �� � ��� ����� � 0 �� 8:00
      begin
        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;



      frRepMain1.LoadFromFile(CurDir + 'SaleHour.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='��������� ����������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acWeekExecute(Sender: TObject);
  //������� �� ���� ������
type THStatREc = record
     Checks,Quests:INteger;
     rSum:Currency;
     end;

Var DateBeg,DateEnd:TDateTime;
    HStat:Array[1..7] of THStatrec;
    n:Integer;
    rSumAll:Currency;
    p,i:Real;
    StrWk,StrWk1:String;


Function CalcDayW(tt:TDateTime):ShortInt;
Var StrW:String;
begin
  StrW:=FormatDateTime('ddd	',tt);
  Result:=1;
  if Pos('��',StrW)>0 then Result:=1;
  if Pos('��',StrW)>0 then Result:=2;
  if Pos('��',StrW)>0 then Result:=3;
  if Pos('��',StrW)>0 then Result:=4;
  if Pos('��',StrW)>0 then Result:=5;
  if Pos('��',StrW)>0 then Result:=6;
  if Pos('��',StrW)>0 then Result:=7;
end;

begin
  //������� �� ���� ������ - �������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      //������� ������������� ������
      for n:=1 to 7 do
      begin
        hStat[n].Checks:=0; hStat[n].Quests:=0; hStat[n].rSum:=0;
      end;

      quRep6.Active:=False;
      quRep6.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep6.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep6.Active:=True;

      quRep6.First;
      while not quRep6.eof do
      begin
        n:=CalcDayW(quRep6CHDATE.AsDateTime);
        inc(hStat[n].Checks);
        hStat[n].Quests:=hStat[n].Quests+quRep6QUESTS.AsInteger;
        hStat[n].rSum:=hStat[n].rSum+quRep6TABSUM.AsFloat;
        quRep6.Next;
        delay(10);
      end;
      //������ ������ ����� ��� ������������ ��������     ======
      rSumAll:=0;
      for n:=1 to 7 do rSumAll:=rSumAll+hStat[n].rSum;


      //������ ���� �������� - �������� �������
      taHour.Active:=False;
      taHour.CreateDataSet;

      for n:=1 to 7 do
      begin

        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        StrWk1:='';
        Case n of
        1:StrWk1:='�����������';
        2:StrWk1:='�������';
        3:StrWk1:='�����';
        4:StrWk1:='�������';
        5:StrWk1:='�������';
        6:StrWk1:='�������';
        7:StrWk1:='�����������';
        end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=StrWk1;
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;

      frRepMain1.LoadFromFile(CurDir + 'SaleWeek.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='������� �� ���� ������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acOutGrExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� �������
  if not CanDo('OutGroup') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep1_.Active:=False;
      quRep1_.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep1_.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'OutGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='������ �� �������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acOutKatExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ���������
  if not CanDo('OutCateg') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2_.Active:=False;
      quRep2_.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2_.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'OutCateg.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������ �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acDelExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
  //���������
  if not CanDo('DelRep') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRepDel.Active:=False;
      quRepDel.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDel.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'DelRep.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������ �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acRealMHExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumSt:Real;
    StrWk,StrWk1:String;
    iStream:Integer;
begin
//������� �� ������ �������� (������������)
  if not CanDo('SaleMH') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep7.Active:=False;
      quRep7.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep7.ParamByName('DateE').AsDateTime:=DateEnd;

      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep7.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('���������� �� ��.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ���      ��������       ���-��   ����� ';
            //       1234 �������� ������ 18 999.999 9999.99
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          rSum:=0;
          rSumSt:=0;
          iStream:=-1; //������ ����� ���� �� ����� ���� �������� ������ � ������ ������
          SelFont(15);

          quRep7.First;
          while not quRep7.Eof do
          begin
            if iStream<>quRep7STREAM.AsInteger then
            begin
              //���� �� ������ ����� -1 �� ������� ����
              if iStream>=0 then
              begin
                PrintStr('');
                SelFont(13);
                Str(rSumSt:8:2,StrWk);
                StrWk:='����� �� �� : '+StrWk;
                PrintStr(StrWk);
                PrintStr('');
                rSumSt:=0;
              end;

              iStream:=quRep7STREAM.AsInteger;
              PrintStr('');
              SelFont(13);
              StrWk:=quRep7NAMESTREAM.AsString;
              PrintStr(StrWk);
              PrintStr('');
              SelFont(15);
            end;

            rSum:=rSum+quRep7SUMSUM.AsFloat;
            rSumSt:=rSumSt+quRep7SUMSUM.AsFloat;
            //���
            StrWk:=IntToStr(quRep7SIFR.AsInteger);
            while Length(StrWk)<5 do StrWk:=' '+StrWk;
            StrWk:=StrWk+' ';//' 0000 '
            //��������
            StrWk1:=Copy(quRep7NAME.AsString,1,18);
            while Length(StrWk1)<18 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //���-��
            Str(quRep7SUMQUANT.AsFloat:7:3,StrWk1);
            while Length(StrWk1)<7 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //�����
            Str(quRep7SUMSUM.AsFloat:7:2,StrWk1);
            while Length(StrWk1)<7 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1;

            PrintStr(StrWk);

            quRep7.Next;
          end;

          PrintStr('');
          SelFont(13);
          Str(rSumSt:8:2,StrWk);
          StrWk:='����� �� �� : '+StrWk;
          PrintStr(StrWk);
          PrintStr('');


          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);
          Str(rSum:8:2,StrWk1);
          StrWk:=' �����                      '+StrWk1+' ���';
          PrintStr(StrWk);

          CutDocPr;
        finally
          quRep7.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������
        frRepMain1.LoadFromFile(CurDir + 'SaleMH.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain1.ReportName:='���������� �� ������ �������� (������������).';
        frRepMain1.PrepareReport;
        frRepMain1.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleMHExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumD:Real;
    StrWk,StrWk1:String;
begin
  //������� �� ������ �������� (������������)
  if not CanDo('CashRealMH') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep8.Active:=False;
      quRep8.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep8.ParamByName('DateE').AsDateTime:=DateEnd;
      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep8.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('������� �� ��.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ����� ��������      �����   � �.�.������ ';
            //   ' ���                99999.99   99999.99   '
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);

          rSum:=0; rSumD:=0;

          quRep8.First;
          while not quRep8.Eof do
          begin

            rSum:=rSum+quRep8SUMSUM.AsFloat;
            rSumD:=rSumD+quRep8SUMDISC.AsFloat;
            //��������
            StrWk:=' '+Copy(quRep8NAMESTREAM.AsString,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';

            //�����
            Str(quRep8SUMSUM.AsFloat:8:2,StrWk1);
            while Length(StrWk1)<8 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';

            //������
            Str(quRep8SUMDISC.AsFloat:8:2,StrWk1);
            while Length(StrWk1)<8 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';

            PrintStr(StrWk);

            quRep8.Next;
          end;

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);
          Str(rSum:8:2,StrWk1);
          StrWk:=' �����              '+StrWk1+' ';
          Str(rSumD:8:2,StrWk1);
          StrWk:=StrWk+StrWk1;

          PrintStr(StrWk);

          CutDocPr;
        finally
          quRep8.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������
        frRepMain2.LoadFromFile(CurDir + 'SaleMHSum.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain2.ReportName:='���������� �� ������ ��������.';
        frRepMain2.PrepareReport;
        frRepMain2.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acRealPerDayExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    iDateb,iDateE,iDateCur:Integer;
    rProc,rQ,rS,rD:Real;
begin
  //���������� �� ������� �� ������ ���
  if not CanDo('SaleDayTime') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod2.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
//  fmPeriod2.DateTimePicker3.Visible:=True;
//  fmPeriod2.DateTimePicker4.Visible:=True;
  fmPeriod2.DateTimePicker3.Time:=TrebSel.TimeFrom;
  fmPeriod2.DateTimePicker4.Time:=TrebSel.TimeTo;

  fmPeriod2.ShowModal; 
  if fmPeriod2.ModalResult=mrOk then
  begin
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);

    with dmC do
    begin
      fmMainrep.taRDT.Active:=False;
      fmMainrep.taRDT.CreateDataSet;
      fmRDTime.Caption:='���������� �� ������ c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd)+'   c '+FormatDateTime('hh:nn',TrebSel.TimeFrom)+' �� '+FormatDateTime('hh:nn',TrebSel.TimeTo);
      fmRDTime.ProgressBar1.Visible:=True;
      fmRDTime.ProgressBar1.Position:=0;
      fmRDTime.Show;
      delay(10);
      fmRDTime.ViewRDT.BeginUpdate;
      iDateB:=Trunc(DateBeg);
      iDateE:=Trunc(DateEnd);
      rProc:=100/(iDateE-iDateB);
      for iDateCur:=iDateB to iDateE do
      begin
        quRep1.Active:=False;
        quRep1.ParamByName('DateB').AsDateTime:=iDateCur+Frac(TrebSel.TimeFrom);
        quRep1.ParamByName('DateE').AsDateTime:=iDateCur+Frac(TrebSel.TimeTo);
        quRep1.Active:=True;
        qurep1.First;
        while not quRep1.Eof do
        begin
          if taRDT.Locate('Sifr',quRep1SIFR.AsInteger,[]) then
          begin //����� ����� ��� ����
            rQ:=taRDTSUMQUANT.AsFloat;
            rS:=taRDTSUMSUM.AsFloat;
            rD:=taRDTSUMDISC.AsFloat;

            taRDT.Edit;
            taRDTSUMQUANT.AsFloat:=rQ+quRep1SUMQUANT.AsFloat;
            taRDTSUMSUM.AsFloat:=rS+quRep1SUMSUM.AsFloat;
            taRDTSUMDISC.AsFloat:=rD+quRep1SUMDISC.AsFloat;
            taRDT.Post;
          end else
          begin  // ������ ��� ���.
            taRDT.Append;
            taRDTSifr.AsInteger:=quRep1SIFR.AsInteger;
            taRDTNAME.AsString:=quRep1NAME.AsString;
            taRDTPARENT.AsInteger:=quRep1PARENT.AsInteger;
            taRDTNAMEGR.AsString:=quRep1NAMEGR.AsString;
            taRDTSUMQUANT.AsFloat:=quRep1SUMQUANT.AsFloat;
            taRDTSUMSUM.AsFloat:=quRep1SUMSUM.AsFloat;
            taRDTSUMDISC.AsFloat:=quRep1SUMDISC.AsFloat;
            taRDT.Post;
          end;
          quRep1.Next;
        end;

        fmRDTime.ProgressBar1.Position:=Round((iDateCur-iDateB)*rProc);
        delay(100);
      end;

      quRep1.Active:=False;
      fmRDTime.ViewRDT.EndUpdate;
      fmRDTime.ProgressBar1.Position:=100;
      delay(200);
      fmRDTime.ProgressBar1.Visible:=False;
    end;
  end;
  fmPeriod2.Release;
end;

procedure TfmMainRep.acRealDayExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    bSt:Boolean;
begin
  //���������� �� ����
  if not CanDo('RealDay') then  begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end; 
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod2.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod2.DateTimePicker3.Visible:=False;
  fmPeriod2.DateTimePicker4.Visible:=False;
  fmPeriod2.Label3.Visible:=False;
  fmPeriod2.Label4.Visible:=False;
//  fmPeriod2.DateTimePicker3.Time:=TrebSel.TimeFrom;
//  fmPeriod2.DateTimePicker4.Time:=TrebSel.TimeTo;

  bSt:=False;
  fmPeriod2.ShowModal;
  if fmPeriod2.ModalResult=mrOk then  bSt:=True;
  fmPeriod2.Release;
  if bSt then
  begin
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);
    fmRealDays.Caption:='���������� �� ���� �� ������ c '+FormatDateTime('dd.mm.yyyy',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',DateEnd);

    with dmC do
    begin
      quRep9.Active:=False;
      quRep9.ParamByName('DATEB').AsDate:=Datebeg;
      quRep9.ParamByName('DATEE').AsDate:=DateEnd; //��� ������ <, ��  TrebSel.DateTo ��� +1
      quRep9.Active:=True;

      fmRealDays.Show;
    end;
  end;
end;

procedure TfmMainRep.acSaleMHStationExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� �� � ��������
  if not CanDo('SaleMHSt') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep10.Active:=False;
      quRep10.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep10.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'SaleMHSt.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='���������� �� ������ ��������, ��������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

end.
