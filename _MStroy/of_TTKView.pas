unit of_TTKView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxButtonEdit, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ComCtrls, cxDropDownEdit, cxCalc,
  cxMaskEdit, cxSpinEdit, cxContainer, cxTextEdit, DBClient, Placemnt;

type
  TfmTTKView = class(TForm)
    StatusBar1: TStatusBar;
    GTSpecV: TcxGrid;
    ViewTSpecV: TcxGridDBTableView;
    ViewTSpecVNum: TcxGridDBColumn;
    ViewTSpecVIDCARD: TcxGridDBColumn;
    ViewTSpecVNAME: TcxGridDBColumn;
    ViewTSpecVIdM: TcxGridDBColumn;
    ViewTSpecVSM: TcxGridDBColumn;
    ViewTSpecVNETTO: TcxGridDBColumn;
    ViewTSpecVBRUTTO: TcxGridDBColumn;
    ViewTSpecVTCard: TcxGridDBColumn;
    LTSpecV: TcxGridLevel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    Label4: TLabel;
    Label5: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxCalcEdit1: TcxCalcEdit;
    FormPlacement1: TFormPlacement;
    taTSpecV: TClientDataSet;
    taTSpecVNum: TIntegerField;
    taTSpecVIdCard: TIntegerField;
    taTSpecVIdM: TIntegerField;
    taTSpecVSM: TStringField;
    taTSpecVKm: TFloatField;
    taTSpecVName: TStringField;
    taTSpecVNetto: TFloatField;
    taTSpecVBrutto: TFloatField;
    taTSpecVKnb: TFloatField;
    taTSpecVTCard: TSmallintField;
    dstaTSpecV: TDataSource;
    taTSpecVPrice: TFloatField;
    taTSpecVPrice1000: TFloatField;
    taTSpecVSumma: TFloatField;
    ViewTSpecVPrice1000: TcxGridDBColumn;
    ViewTSpecVSumma: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ViewTSpecVCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function prCalcSebV:Real;
  end;

var
  fmTTKView: TfmTTKView;

implementation

uses dmOffice, Un1, CalcSeb;

{$R *.dfm}


function TfmTTKView.prCalcSebV:Real;
Var sBeg:String;
    rPrice:Real;
    rMessure:Real;
    rSum:Real;
    iCountB:Integer;
begin
  with dmO do
  begin
    sBeg:='';
    rSum:=0;
    CloseTa(fmCalcSeb.taSpecC);
    //���������� ���-�� �������� ������� �����                                                                           //��� ����� �.�.  ������ ������ �����������
    iCountB:=1;
    fmCalcSeb.prCalcQSeb(sBeg,'',taTSpecVIDCARD.AsInteger,Trunc(Date),1,(taTSpecVNETTO.AsFloat*taTSpecVKM.AsFloat/iCountB),taTSpecVIdM.AsInteger,nil);
    fmCalcSeb.taSpecC.First;
    while not fmCalcSeb.taSpecC.Eof do
    begin
      prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=fmCalcSeb.taSpecCIdGoods.AsInteger;
      prCalcLastPrice1.ExecProc;
      rPrice:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
      rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
      if rMessure<>1 then //���� �������� ��������������
      begin
        if rMessure<>0 then rPrice:=rPrice/rMessure;
      end;
      rSum:=rSum+rPrice*fmCalcSeb.taSpecCQuant.AsFloat;
      fmCalcSeb.taSpecC.Next;
    end;
    fmCalcSeb.taSpecC.Active:=False;
    Result:=rSum;
  end;
end;


procedure TfmTTKView.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmTTKView.ViewTSpecVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
//  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewTSpecV.ColumnCount-1 do
  begin
    if ViewTSpecV.Columns[i].Name='ViewTSpecVTCard' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=1  then
  begin
    ACanvas.Canvas.Font.Color := clHotLight;
    ACanvas.Canvas.Font.Style :=[fsBold];
  end;
end;

end.
