object fmAddGood: TfmAddGood
  Left = 563
  Top = 70
  BorderStyle = bsDialog
  Caption = 'fmAddGood'
  ClientHeight = 369
  ClientWidth = 433
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 148
    Width = 19
    Height = 13
    Caption = #1050#1086#1076
    Transparent = True
  end
  object Label4: TLabel
    Left = 24
    Top = 180
    Width = 75
    Height = 13
    Caption = #1045#1076'. '#1080#1079#1084#1077#1088#1077#1085#1080#1103
    Transparent = True
  end
  object Label5: TLabel
    Left = 24
    Top = 204
    Width = 63
    Height = 13
    Caption = #1057#1090#1072#1074#1082#1072' '#1053#1044#1057
    Transparent = True
  end
  object Label9: TLabel
    Left = 24
    Top = 232
    Width = 19
    Height = 13
    Caption = #1058#1080#1087
    Transparent = True
  end
  object Label11: TLabel
    Left = 24
    Top = 122
    Width = 76
    Height = 13
    Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080'.'
    Transparent = True
  end
  object Label12: TLabel
    Left = 24
    Top = 88
    Width = 70
    Height = 13
    Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    Transparent = True
  end
  object Label6: TLabel
    Left = 224
    Top = 122
    Width = 91
    Height = 13
    Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 350
    Width = 433
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 299
    Width = 433
    Height = 51
    Align = alBottom
    BevelInner = bvLowered
    Color = 16762052
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 64
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 280
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 433
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 8
      Width = 377
      Height = 25
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 88
    Top = 52
    Properties.MaxLength = 200
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    StyleFocused.TextStyle = [fsBold]
    StyleHot.BorderStyle = ebsOffice11
    TabOrder = 3
    Text = 'cxTextEdit1'
    Width = 329
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 104
    Top = 144
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 5
    Width = 89
  end
  object cxCheckBox2: TcxCheckBox
    Left = 20
    Top = 264
    Caption = #1040#1082#1090#1080#1074#1085#1072#1103
    Properties.ValueChecked = 1
    Properties.ValueUnchecked = 0
    State = cbsGrayed
    TabOrder = 9
    Transparent = True
    Width = 89
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 112
    Top = 176
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMESHORT'
      end>
    Properties.ListSource = dmO.dsMess
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 6
    Width = 105
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 112
    Top = 200
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMENDS'
      end>
    Properties.ListSource = dmO.dsNDS
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 7
    Width = 105
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 112
    Top = 228
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMECAT'
      end>
    Properties.ListSource = dmO.dsCateg
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 8
    Width = 105
  end
  object cxCEdit1: TcxCurrencyEdit
    Left = 104
    Top = 116
    EditValue = 0c
    Properties.Alignment.Horz = taRightJustify
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 4
    Width = 89
  end
  object cxTextEdit2: TcxTextEdit
    Left = 104
    Top = 84
    Properties.MaxLength = 200
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    StyleFocused.TextStyle = [fsBold]
    StyleHot.BorderStyle = ebsOffice11
    TabOrder = 10
    Text = 'cxTextEdit2'
    Width = 313
  end
  object cxCEdit2: TcxCurrencyEdit
    Left = 324
    Top = 116
    EditValue = 0c
    Properties.Alignment.Horz = taRightJustify
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 11
    Width = 89
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 344
    Top = 180
  end
end
