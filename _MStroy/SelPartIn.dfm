object fmPartIn: TfmPartIn
  Left = 307
  Top = 229
  BorderStyle = bsDialog
  Caption = #1057#1074#1086#1073#1086#1076#1085#1099#1077' '#1087#1072#1088#1090#1080#1080
  ClientHeight = 305
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 252
    Width = 470
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton2: TcxButton
      Left = 280
      Top = 16
      Width = 83
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 168
      Top = 16
      Width = 83
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 1
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 470
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 32
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 24
      Top = 56
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 120
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 120
      Top = 32
      Width = 39
      Height = 13
      Caption = 'Label5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 120
      Top = 56
      Width = 39
      Height = 13
      Caption = 'Label6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GrPartIn: TcxGrid
    Left = 8
    Top = 88
    Width = 449
    Height = 137
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewPartIn: TcxGridDBTableView
      OnDblClick = ViewPartInDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsSelPartIn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QPART'
          Column = ViewPartInQPART
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QREMN'
          Column = ViewPartInQREMN
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewPartInID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'ID'
        Width = 46
      end
      object ViewPartInIDDOC: TcxGridDBColumn
        DataBinding.FieldName = 'IDDOC'
        Visible = False
      end
      object ViewPartInDTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'DTYPE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1055#1088#1080#1093'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1048#1085#1074#1077#1085#1090'.'
            Value = 3
          end
          item
            Description = #1056#1077#1072#1083'.(Sale)'
            Value = 2
          end
          item
            Description = #1042#1085'.'#1087#1088#1080#1093'.'
            Value = 4
          end
          item
            Description = #1040#1082#1090'.'#1087#1077#1088#1077#1088#1072#1073
            Value = 5
          end
          item
            Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
            Value = 6
          end
          item
            Description = #1042#1086#1079#1074#1088#1072#1090
            Value = 7
          end
          item
            Description = #1056#1077#1072#1083'.'#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
            Value = 8
          end
          item
            Description = #1057#1087#1080#1089#1072#1085#1080#1077
            Value = 9
          end>
      end
      object ViewPartInQPART: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QPART'
      end
      object ViewPartInQREMN: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QREMN'
        Visible = False
      end
      object ViewPartInPRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1080#1093'.'
        DataBinding.FieldName = 'PRICEIN'
      end
      object ViewPartInPRICEOUT: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1091#1095'.'
        DataBinding.FieldName = 'PRICEOUT'
      end
      object ViewPartInIDATE: TcxGridDBColumn
        DataBinding.FieldName = 'IDATE'
        Visible = False
      end
      object ViewPartInSDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'SDATE'
      end
    end
    object LevelPartIn: TcxGridLevel
      GridView = ViewPartIn
    end
  end
end
