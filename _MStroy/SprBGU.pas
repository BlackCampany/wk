unit SprBGU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxCurrencyEdit,
  cxImageComboBox, cxCalc, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxSplitter, cxContainer, cxTreeView, ExtCtrls,
  Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, FIBDataSet,
  pFIBDataSet;

type
  TfmSprBGU = class(TForm)
    Panel2: TPanel;
    GrBGU: TcxGrid;
    ViewBGU: TcxGridDBTableView;
    LevelBGU: TcxGridLevel;
    cxButton1: TcxButton;
    ViewBGUNAMEBGU: TcxGridDBColumn;
    ViewBGUNAMEGR: TcxGridDBColumn;
    ViewBGUBB: TcxGridDBColumn;
    ViewBGUGG: TcxGridDBColumn;
    ViewBGUU1: TcxGridDBColumn;
    ViewBGUU2: TcxGridDBColumn;
    ViewBGUEE: TcxGridDBColumn;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewBGUDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSprBGU: TfmSprBGU;

implementation

uses dmOffice, AddGoods, Un1, Goods;

{$R *.dfm}

procedure TfmSprBGU.cxButton1Click(Sender: TObject);
begin
  with dmO do
  begin
    if bEE then
    begin
      if taBGU.Active and (taBGU.RecordCount>0) then
      begin
        with fmAddGood do
        begin
        end;
      end;
      bEE:=False;
    end;
    close;
  end;
end;

procedure TfmSprBGU.FormCreate(Sender: TObject);
begin
  GrBGU.Align:=AlClient;
end;

procedure TfmSprBGU.FormShow(Sender: TObject);
begin
  with dmO do
  begin
    if taBGU.Active=False then
    begin
      ViewBGU.BeginUpdate;
      taBGU.Active:=True;
      ViewBGU.EndUpdate;
    end;
  end;  
end;

procedure TfmSprBGU.cxButton2Click(Sender: TObject);
begin
  bEE:=False;
  Close;
end;

procedure TfmSprBGU.ViewBGUDblClick(Sender: TObject);
begin
  with dmO do
  begin
    if bEE then
    begin
      if taBGU.Active and (taBGU.RecordCount>0) then
      begin
        with fmAddGood do
        begin
        end;
      end;
      bEE:=False;
    end;
    close;
  end;  
end;

end.
