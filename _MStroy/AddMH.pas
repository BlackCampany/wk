unit AddMH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, dxfBackGround, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxGraphics, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit;

type
  TfmAddMH = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    dxfBackGround1: TdxfBackGround;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel2: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxLookupComboBox1: TcxLookupComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddMH: TfmAddMH;

implementation

uses dmOffice;

{$R *.dfm}

end.
