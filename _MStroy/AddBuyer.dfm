object fmAddBuyer: TfmAddBuyer
  Left = 285
  Top = 109
  Width = 614
  Height = 413
  Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 28
    Width = 49
    Height = 13
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 60
    Width = 22
    Height = 13
    Caption = #1048#1084#1103
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 92
    Width = 47
    Height = 13
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
    Transparent = True
  end
  object Label7: TLabel
    Left = 8
    Top = 130
    Width = 77
    Height = 13
    Caption = #1055#1072#1089#1087#1086#1088#1090' '#1057#1077#1088#1080#1103
    Transparent = True
  end
  object Label8: TLabel
    Left = 16
    Top = 202
    Width = 33
    Height = 13
    Caption = #1042#1099#1076#1072#1083
    Transparent = True
  end
  object Label11: TLabel
    Left = 16
    Top = 230
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
    Transparent = True
  end
  object Label14: TLabel
    Left = 192
    Top = 129
    Width = 34
    Height = 13
    Caption = #1053#1086#1084#1077#1088
    Transparent = True
  end
  object Label15: TLabel
    Left = 16
    Top = 168
    Width = 65
    Height = 13
    Caption = #1042#1099#1076#1072#1085' ('#1076#1072#1090#1072')'
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 262
    Width = 45
    Height = 13
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 326
    Width = 606
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = 14427008
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 128
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 360
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 96
    Top = 24
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 1
    Width = 257
  end
  object cxTextEdit2: TcxTextEdit
    Left = 96
    Top = 56
    Properties.MaxLength = 300
    Style.Shadow = True
    TabOrder = 2
    Width = 256
  end
  object cxTextEdit3: TcxTextEdit
    Left = 96
    Top = 88
    Properties.MaxLength = 300
    Style.Shadow = True
    TabOrder = 3
    Width = 256
  end
  object cxTextEdit6: TcxTextEdit
    Left = 96
    Top = 124
    Properties.MaxLength = 15
    Style.Shadow = True
    TabOrder = 4
    Width = 77
  end
  object cxTextEdit7: TcxTextEdit
    Left = 96
    Top = 193
    Properties.MaxLength = 50
    Style.Shadow = True
    TabOrder = 5
    Width = 449
  end
  object cxTextEdit9: TcxTextEdit
    Left = 96
    Top = 226
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 6
    Width = 449
  end
  object cxTextEdit12: TcxTextEdit
    Left = 246
    Top = 124
    Properties.MaxLength = 20
    Style.Shadow = True
    TabOrder = 7
    Width = 131
  end
  object cxDateEdit1: TcxDateEdit
    Left = 96
    Top = 160
    Style.Shadow = True
    TabOrder = 8
    Width = 121
  end
  object cxTextEdit4: TcxTextEdit
    Left = 96
    Top = 258
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 9
    Width = 449
  end
  object qAddBuyer: TpFIBQuery
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    Left = 400
    Top = 140
  end
end
