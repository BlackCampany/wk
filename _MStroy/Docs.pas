unit Docs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxEdit, cxTextEdit, SpeedBar, cxControls, cxContainer, cxTreeView,
  ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLabel, cxMaskEdit, cxDropDownEdit, cxCalendar, dxmdaset, FIBQuery,
  pFIBQuery,Un1, dmOffice,StrUtils;

type
  TfmDocs = class(TForm)
    Panel1: TPanel;
    BuyersTree: TcxTreeView;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxLabel1: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxTextEdit2: TcxTextEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    teDocHead: TdxMemData;
    cxLabel10: TcxLabel;
    teDocSpec: TdxMemData;
    teDocHeadDOC_ID: TIntegerField;
    teDocHeadDOC_NUMBER: TStringField;
    teDocHeadDOC_PREDOPLATA: TFloatField;
    teDocHeadDOC_WARRANTY: TIntegerField;
    teDocHeadDOC_IZGOTOVLENIE: TIntegerField;
    teDocHeadDOC_VERSION: TIntegerField;
    teDocHeadBUYER_ID: TIntegerField;
    teDocSpecDOC_ID: TIntegerField;
    teDocSpecCARD_ID: TIntegerField;
    teDocSpecCARD_COUNT: TFloatField;
    teDocSpecCARD_PRICE: TFloatField;
    teDocSpecCARD_SUM: TFloatField;
    teDocSpecCARD_NAME: TStringField;
    teDocSpecCARD_POST: TStringField;
    qBuyersDocsTree: TpFIBQuery;
    qQuery: TpFIBQuery;
    teDocHeadDOC_DATE: TDateTimeField;
    teDocHeadDOC_VERSIONDATE: TDateTimeField;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    teDocSpecDOC_VERSION: TIntegerField;
    dsDocSpec: TDataSource;
    cxGrid1DBTableView1RecId: TcxGridDBColumn;
    cxGrid1DBTableView1DOC_ID: TcxGridDBColumn;
    cxGrid1DBTableView1DOC_NUMBER: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_COUNT: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_PRICE: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_SUM: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_NAME: TcxGridDBColumn;
    cxGrid1DBTableView1CARD_POST: TcxGridDBColumn;
    SpeedItem3: TSpeedItem;
    procedure BuyersTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure BuyersTreeChange(Sender: TObject; Node: TTreeNode);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocs: TfmDocs;

implementation

uses Goods;

{$R *.dfm}

procedure TfmDocs.BuyersTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var
  TreeNode : TTreeNode;
begin
  //BuyersTree.Items.AddChildObject(Node, '������� ddd', Pointer(1));


  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    if Node.Level=0 then
      begin
        qBuyersDocsTree.Close;
        qBuyersDocsTree.SQL.Clear;
        qBuyersDocsTree.SQL.Add('select doc_id, doc_number,DOC_DATE from of_dochead where buyer_id='+intToStr(integer(Node.Data))+' group by doc_id,doc_number,DOC_DATE');
        qBuyersDocsTree.Transaction.StartTransaction;
          qBuyersDocsTree.ExecQuery;
          while not qBuyersDocsTree.Eof do
            begin
              TreeNode:=BuyersTree.Items.AddChildObject(Node, '������� '+qBuyersDocsTree.FldByName['doc_number'].AsString+' �� '+FormatDateTime('dd.mm.yyyy',qBuyersDocsTree.FldByName['DOC_DATE'].AsDate), Pointer(qBuyersDocsTree.FldByName['doc_id'].AsInteger));
              //TreeNode.ImageIndex:=8;
              //TreeNode.SelectedIndex:=7;
              BuyersTree.Items.AddChildObject(TreeNode,'', nil);
              qBuyersDocsTree.Next;
            end;
        qBuyersDocsTree.Transaction.Commit;
      end;
    if Node.Level=1 then
      begin
        qBuyersDocsTree.Close;
        qBuyersDocsTree.SQL.Clear;
        qBuyersDocsTree.SQL.Add('select DOC_VERSION,DOC_VERSIONDATE from of_dochead where doc_id='+intToStr(integer(Node.Data))+' order by DOC_VERSION');
        qBuyersDocsTree.Transaction.StartTransaction;
          qBuyersDocsTree.ExecQuery;
          while not qBuyersDocsTree.Eof do
            begin
               TreeNode:=BuyersTree.Items.AddChildObject(Node, '������ '+qBuyersDocsTree.FldByName['DOC_VERSION'].AsString+' �� '+FormatDateTime('dd.mm.yyyy hh:mm',qBuyersDocsTree.FldByName['DOC_VERSIONDATE'].AsDate), Pointer(qBuyersDocsTree.FldByName['DOC_VERSION'].AsInteger));
              //TreeNode.ImageIndex:=8;
              //TreeNode.SelectedIndex:=7;
               //BuyersTree.Items.AddChildObject(TreeNode,'', nil);
               qBuyersDocsTree.Next;
            end;
        qBuyersDocsTree.Transaction.Commit;
      end;

    //
    //ClassifExpand(Node,BuyersTree,dmO.quClassTree,Person.Id,1);
  end;



end;

procedure TfmDocs.FormShow(Sender: TObject);
var
  TreeNode : TTreeNode;
begin
  BuyersTree.Items.Clear;
  qBuyersDocsTree.Close;
  qBuyersDocsTree.SQL.Clear;
  qBuyersDocsTree.SQL.Add('select buyer_id, buyer_f,buyer_i,buyer_o from of_buyers');
  qBuyersDocsTree.Transaction.StartTransaction;
  qBuyersDocsTree.ExecQuery;
    while not qBuyersDocsTree.Eof do
      begin
        TreeNode:=BuyersTree.Items.AddChildObject(nil, qBuyersDocsTree.FldByName['buyer_f'].AsString+' '+qBuyersDocsTree.FldByName['buyer_i'].AsString+' '+qBuyersDocsTree.FldByName['buyer_o'].AsString, Pointer(qBuyersDocsTree.FldByName['buyer_id'].AsInteger));
        //TreeNode.ImageIndex:=8;
        //TreeNode.SelectedIndex:=7;
        BuyersTree.Items.AddChildObject(TreeNode,'', nil);
        qBuyersDocsTree.Next;
      end;
  qBuyersDocsTree.Transaction.Commit;

end;

procedure TfmDocs.SpeedItem1Click(Sender: TObject);
begin
  fmDocs.Close;
end;

procedure TfmDocs.BuyersTreeChange(Sender: TObject; Node: TTreeNode);
var
  NodeParent : TTreeNode;
begin
  if Node.Level<>2 then
    begin
      cxTextEdit1.Text:='';
      cxTextEdit3.Text:='';
      cxTextEdit4.Text:='';
      cxTextEdit5.Text:='';
      cxTextEdit6.Text:='';
      cxDateEdit1.Text:='';
    end;

  if Node.Level=2 then
    begin
      NodeParent:=Node.Parent;

      qQuery.Close;
      qQuery.SQL.Clear;
      qQuery.SQL.Add('select doc_id');
      qQuery.SQL.Add(', doc_number');
      qQuery.SQL.Add(', doc_predoplata');
      qQuery.SQL.Add(', doc_warranty');
      qQuery.SQL.Add(', doc_izgotovlenie');
      qQuery.SQL.Add(', doc_version');
      qQuery.SQL.Add(', buyer_id');
      qQuery.SQL.Add(', doc_date');
      qQuery.SQL.Add(', doc_versiondate');
      qQuery.SQL.Add('from of_dochead where doc_id='+IntToStr(Integer(NodeParent.Data)));
      qQuery.SQL.Add('and doc_version='+IntToStr(integer(Node.Data)));

      qQuery.Transaction.StartTransaction;
        qQuery.ExecQuery;

        CloseTe(teDocHead);

        teDocHead.Insert;
          teDocHeadDOC_ID.AsInteger:=qQuery.FldByName['doc_id'].AsInteger;
          teDocHeadDOC_NUMBER.AsString:=qQuery.FldByName['doc_number'].AsString;
          teDocHeadDOC_PREDOPLATA.AsFloat:=qQuery.FldByName['doc_predoplata'].AsFloat;
          teDocHeadDOC_WARRANTY.AsInteger:=qQuery.FldByName['doc_warranty'].AsInteger;
          teDocHeadDOC_IZGOTOVLENIE.AsInteger:=qQuery.FldByName['doc_izgotovlenie'].AsInteger;
          teDocHeadDOC_VERSION.AsInteger:=qQuery.FldByName['doc_version'].AsInteger;
          teDocHeadBUYER_ID.AsInteger:=qQuery.FldByName['buyer_id'].AsInteger;
          teDocHeadDOC_DATE.AsDateTime:=qQuery.FldByName['doc_date'].AsDateTime;
          teDocHeadDOC_VERSIONDATE.AsDateTime:=qQuery.FldByName['doc_versiondate'].AsDateTime;
        teDocHead.Post;

          cxTextEdit1.Text:=qQuery.FldByName['doc_number'].AsString;
          cxTextEdit3.Text:=qQuery.FldByName['doc_predoplata'].AsString;
          cxTextEdit4.Text:=qQuery.FldByName['doc_warranty'].AsString;
          cxTextEdit5.Text:=qQuery.FldByName['doc_izgotovlenie'].AsString;
          cxTextEdit6.Text:=qQuery.FldByName['doc_version'].AsString;
          cxDateEdit1.Date:=qQuery.FldByName['doc_date'].AsDateTime;

      qQuery.Transaction.Commit;


      qQuery.Close;
      qQuery.SQL.Clear;
      qQuery.SQL.Add('select doc_id, doc_version, card_id, card_count, card_price, c.name');
      qQuery.SQL.Add(', class.namecl, card_count*card_price as card_sum');
      qQuery.SQL.Add('from of_docspec s inner join  of_cards c on s.card_id=c.id');
      qQuery.SQL.Add('left join of_classif class on class.id=c.parent');
      qQuery.SQL.Add('where doc_id='+IntToStr(Integer(NodeParent.Data)));
      qQuery.SQL.Add('and doc_version='+IntToStr(integer(Node.Data)));

      qQuery.Transaction.StartTransaction;
        qQuery.ExecQuery;
        closete(teDocSpec);

        while not qQuery.Eof do
          begin
            teDocSpec.Insert;
              teDocSpecDOC_ID.AsInteger:=qQuery.FldByName['doc_id'].AsInteger;
              teDocSpecDOC_version.AsInteger:=qQuery.FldByName['doc_version'].AsInteger;
              teDocSpecCARD_ID.AsInteger:=qQuery.FldByName['card_id'].AsInteger;
              teDocSpecCARD_COUNT.AsFloat:=qQuery.FldByName['card_count'].AsFloat;
              teDocSpecCARD_PRICE.AsFloat:=qQuery.FldByName['card_price'].AsFloat;
              teDocSpecCARD_SUM.AsFloat:=qQuery.FldByName['card_sum'].AsFloat;
              teDocSpecCARD_NAME.AsString:=qQuery.FldByName['name'].AsString;
              teDocSpecCARD_POST.AsString:=qQuery.FldByName['namecl'].AsString;
            teDocSpec.Post;
            qQuery.Next
         end;

      qQuery.Transaction.Commit;



    end;
end;

procedure TfmDocs.cxButton2Click(Sender: TObject);
var
  NodeParent : TTreeNode;
  Node:TTreeNode;
begin
  Node:=BuyersTree.Selected;
  NodeParent:=Node.Parent;
  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('update of_dochead');
  qQuery.SQL.Add('set doc_predoplata = '+cxTextEdit3.Text);
  qQuery.SQL.Add('    ,doc_warranty = '+cxTextEdit4.Text);
  qQuery.SQL.Add('    ,doc_izgotovlenie = '+cxTextEdit5.Text);
  qQuery.SQL.Add('where doc_id='+IntToStr(Integer(NodeParent.Data)));
  qQuery.SQL.Add('and doc_version='+IntToStr(integer(Node.Data)));
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
  qQuery.Transaction.Commit;

  Node:=BuyersTree.Selected;
  NodeParent:=Node.Parent;
  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('update of_dochead');
  qQuery.SQL.Add('set  doc_date = '''+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+'''');
  qQuery.SQL.Add('    ,doc_number = '''+cxTextEdit1.Text+'''');
  qQuery.SQL.Add('where doc_id='+IntToStr(Integer(NodeParent.Data)));
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
  qQuery.Transaction.Commit;


  teDocSpec.First;
  while not teDocSpec.Eof do
    begin
      qQuery.Close;
      qQuery.SQL.Clear;
      qQuery.SQL.Add('delete from of_docspec');
      qQuery.SQL.Add('where doc_id='+IntToStr(Integer(NodeParent.Data)));
      qQuery.SQL.Add('and doc_version='+IntToStr(integer(Node.Data)));
      
      qQuery.Transaction.StartTransaction;
        qQuery.ExecQuery;

      qQuery.Transaction.Commit;






      qQuery.Close;
      qQuery.SQL.Clear;
      qQuery.SQL.Add('insert into of_docspec (doc_id, doc_version, card_id, card_count, card_price)');
      qQuery.SQL.Add('values ('+teDocSpecDOC_ID.AsString);
      qQuery.SQL.Add('      , '+teDocSpecDOC_VERSION.AsString);
      qQuery.SQL.Add('      , '+teDocSpecCARD_ID.AsString);
      qQuery.SQL.Add('      , '+ANSIReplaceSTR(teDocSpecCARD_COUNT.AsString,',','.'));
      qQuery.SQL.Add('      , '+ANSIReplaceSTR(teDocSpecCARD_PRICE.AsString,',','.'));
      qQuery.SQL.Add('      )');
      qQuery.Transaction.StartTransaction;
        qQuery.ExecQuery;

      qQuery.Transaction.Commit;
      teDocSpec.Next;
    end;








end;

procedure TfmDocs.cxButton1Click(Sender: TObject);
var
  NodeParent : TTreeNode;
  Node:TTreeNode;
  mver:integer;
begin
  Node:=BuyersTree.Selected;
  NodeParent:=Node.Parent;

  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('select max(doc_version) as doc_version from of_dochead where doc_id='+IntToStr(Integer(NodeParent.Data)));
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
    mver:=qQuery.FldByName['doc_version'].AsInteger+1;
  qQuery.Transaction.Commit;


  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('insert into of_dochead (doc_id, doc_number, doc_predoplata, doc_warranty, doc_izgotovlenie, doc_version, buyer_id, doc_date, doc_versiondate)');
  qQuery.SQL.Add('values ('+teDocHeadDOC_ID.AsString);
  qQuery.SQL.Add('      , '+teDocHeadDOC_NUMBER.AsString);
  qQuery.SQL.Add('      , '+teDocHeadDOC_PREDOPLATA.AsString);
  qQuery.SQL.Add('      , '+teDocHeadDOC_WARRANTY.AsString);
  qQuery.SQL.Add('      , '+teDocHeadDOC_IZGOTOVLENIE.AsString);
  qQuery.SQL.Add('      , '+inttostr(mver));
  qQuery.SQL.Add('      , '+teDocHeadBUYER_ID.asstring);
  qQuery.SQL.Add('      , '''+formatdatetime('dd.mm.yyyy',teDocHeadDOC_DATE.AsDateTime)+'''');
  qQuery.SQL.Add('      , '''+formatdatetime('dd.mm.yyyy hh:mm:ss',teDocHeadDOC_VERSIONDATE.AsDateTime)+'''');
  qQuery.SQL.Add('      )');
  //qQuery.SQL.SaveToFile('c:\1.txt');
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
  qQuery.Transaction.Commit;

  teDocSpec.First;
  while not teDocSpec.Eof do
    begin

      qQuery.Close;
      qQuery.SQL.Clear;
      qQuery.SQL.Add('insert into of_docspec (doc_id, doc_version, card_id, card_count, card_price)');
      qQuery.SQL.Add('values ('+teDocSpecDOC_ID.AsString);
      qQuery.SQL.Add('      , '+inttostr(mver));
      qQuery.SQL.Add('      , '+teDocSpecCARD_ID.AsString);
      qQuery.SQL.Add('      , '+ANSIReplaceSTR(teDocSpecCARD_COUNT.AsString,',','.'));
      qQuery.SQL.Add('      , '+ANSIReplaceSTR(teDocSpecCARD_PRICE.AsString,',','.'));
      qQuery.SQL.Add('      )');
      qQuery.Transaction.StartTransaction;
        qQuery.ExecQuery;

      qQuery.Transaction.Commit;
      teDocSpec.Next;
    end;

end;

procedure TfmDocs.N1Click(Sender: TObject);
var
  NodeParent : TTreeNode;
  Node:TTreeNode;
  mver:integer;
begin
  Node:=BuyersTree.Selected;
  NodeParent:=Node.Parent;

  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('select max(DOC_ID) as DOC_ID from of_dochead');
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
    mver:=qQuery.FldByName['DOC_ID'].AsInteger+1;
  qQuery.Transaction.Commit;


  qQuery.Close;
  qQuery.SQL.Clear;
  qQuery.SQL.Add('insert into of_dochead (doc_id, doc_number, doc_predoplata, doc_warranty, doc_izgotovlenie, doc_version, buyer_id, doc_date, doc_versiondate)');
  qQuery.SQL.Add('values ('+inttostr(mver));
  qQuery.SQL.Add('      , 0');
  qQuery.SQL.Add('      , 0');
  qQuery.SQL.Add('      , 0');
  qQuery.SQL.Add('      , 0');
  qQuery.SQL.Add('      , 1');
  qQuery.SQL.Add('      , 1');
  qQuery.SQL.Add('      , '''+formatdatetime('dd.mm.yyyy',Date)+'''');
  qQuery.SQL.Add('      , '''+formatdatetime('dd.mm.yyyy hh:mm:ss',Now())+'''');
  qQuery.SQL.Add('      )');
  //qQuery.SQL.SaveToFile('c:\2.txt');
  qQuery.Transaction.StartTransaction;
    qQuery.ExecQuery;
  qQuery.Transaction.Commit;
end;

procedure TfmDocs.cxLabel1Click(Sender: TObject);
begin
  fmCards.Show;
end;

procedure TfmDocs.cxLabel9Click(Sender: TObject);
begin
  CloseTe(teDocSpec);
end;

end.

