unit ExportFromOf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfProgressBar, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, FIBDataSet, pFIBDataSet, cxCheckBox,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP, cxMemo;

type
  TfmExport = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    ProgressBar1: TdxfProgressBar;
    quExport: TpFIBDataSet;
    quExportIDHEAD: TFIBIntegerField;
    quExportIDNDS: TFIBIntegerField;
    quExportDATEDOC: TFIBDateField;
    quExportNUMDOC: TFIBStringField;
    quExportDATESF: TFIBDateField;
    quExportNUMSF: TFIBStringField;
    quExportIDCLI: TFIBIntegerField;
    quExportIDSKL: TFIBIntegerField;
    quExportNAMECL: TFIBStringField;
    quExportINN: TFIBStringField;
    quExportSUMIN: TFIBFloatField;
    quExportSUMUCH: TFIBFloatField;
    quExportSUMNDS: TFIBFloatField;
    Label4: TLabel;
    quCountD: TpFIBDataSet;
    quCountDCOUNTDOC: TFIBIntegerField;
    quExportEXPNAME: TFIBStringField;
    ProgressBar2: TdxfProgressBar;
    quExpOut: TpFIBDataSet;
    quCountDOut: TpFIBDataSet;
    quCountDOutCOUNTDOC: TFIBIntegerField;
    quExpOutIDHEAD: TFIBIntegerField;
    quExpOutIDNDS: TFIBIntegerField;
    quExpOutDATEDOC: TFIBDateField;
    quExpOutNUMDOC: TFIBStringField;
    quExpOutDATESF: TFIBDateField;
    quExpOutNUMSF: TFIBStringField;
    quExpOutIDCLI: TFIBIntegerField;
    quExpOutIDSKL: TFIBIntegerField;
    quExpOutEXPNAME: TFIBStringField;
    quExpOutNAMECL: TFIBStringField;
    quExpOutINN: TFIBStringField;
    quExpOutSUMIN: TFIBFloatField;
    quExpOutSUMUCH: TFIBFloatField;
    quExpOutSUMNDS: TFIBFloatField;
    quListR: TpFIBDataSet;
    quListRDATEDOC: TFIBDateField;
    quListRIDSKL: TFIBIntegerField;
    quAvIn: TpFIBDataSet;
    quAvOut: TpFIBDataSet;
    quAvInRSUMA: TFIBFloatField;
    quAvOutRSUMA: TFIBFloatField;
    quListREXPNAME: TFIBStringField;
    quCountVn: TpFIBDataSet;
    quCountVnCOUNTDOC: TFIBIntegerField;
    quExportVn: TpFIBDataSet;
    quExportVnID: TFIBIntegerField;
    quExportVnDATEDOC: TFIBDateField;
    quExportVnNUMDOC: TFIBStringField;
    quExportVnIDSKL_FROM: TFIBIntegerField;
    quExportVnEXPNAME: TFIBStringField;
    quExportVnIDSKL_TO: TFIBIntegerField;
    quExportVnEXPNAME1: TFIBStringField;
    quExportVnSUMIN: TFIBFloatField;
    quExportVnSUMUCH: TFIBFloatField;
    quExportVnSUMUCH1: TFIBFloatField;
    quListR1: TpFIBDataSet;
    quListR1DATEDOC: TFIBDateField;
    quListR1IDSKL: TFIBIntegerField;
    quListR1EXPNAME: TFIBStringField;
    quListR1OPER: TFIBStringField;
    quListR1SUMIN: TFIBFloatField;
    quListR1SUMUCH: TFIBFloatField;
    quCountDOutR: TpFIBDataSet;
    quExpOutR: TpFIBDataSet;
    quCountDOutRCOUNTDOC: TFIBIntegerField;
    quExpOutRIDHEAD: TFIBIntegerField;
    quExpOutRIDNDS: TFIBIntegerField;
    quExpOutRDATEDOC: TFIBDateField;
    quExpOutRNUMDOC: TFIBStringField;
    quExpOutRDATESF: TFIBDateField;
    quExpOutRNUMSF: TFIBStringField;
    quExpOutRIDCLI: TFIBIntegerField;
    quExpOutRIDSKL: TFIBIntegerField;
    quExpOutREXPNAME: TFIBStringField;
    quExpOutRNAMECL: TFIBStringField;
    quExpOutRINN: TFIBStringField;
    quExpOutRSUMIN: TFIBFloatField;
    quExpOutRSUMR: TFIBFloatField;
    quExpOutRSUMNDS: TFIBFloatField;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    cxCheckBox4: TcxCheckBox;
    cxCheckBox5: TcxCheckBox;
    cxButton3: TcxButton;
    IdFTP1: TIdFTP;
    Memo1: TcxMemo;
    quExportCATEGORY: TFIBIntegerField;
    quExpOutCATEGORY: TFIBIntegerField;
    quExportVnSUMTAR: TFIBFloatField;
    quListR1COMMENT: TFIBStringField;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmExport: TfmExport;

implementation

uses Un1, u2fdk, dmOffice;

{$R *.dfm}

procedure TfmExport.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmExport.cxButton2Click(Sender: TObject);
Var StrWk,FileN:String;
    F: TextFile;
		SUM_POST,SUM_10,NDS_10,SUM_18,NDS_18,NSP,SUM_SKLAD,SUM_TARA:Real;
    SUM_REALIN,SUM_DELIN,SUM_REAL,SUM_REALBN,SUM_AVOUT,SUM_AVIN,SUM_USLOUT:Real;
    Idh,i,iMax:INteger;
    bW:Boolean;

{		SUM_POST    = ����� ����������;
		SUM_10      = ����� ���������� ��� ��� 10;
		NDS_10      = ��� 10 ����������;
		SUM_18      = ����� ���������� ��� ��� 18;
		NDS_18      = ��� 18 ����������;
		NSP         = 0;
		SUM_SKLAD   = ����� �������;
		SUM_TARA    = ����� ����;

    SUM_REALIN  = ������������� ����������
    SUM_DELIN   = ������������� ���������
    SUM_REAL    = ����� ����������
    SUM_REALBN  = ����� ���������� ������
    SUM_AVOUT   = ����� ������ ���������
    SUM_AVIN   = ����� ������ ����������
    SUM_USLOUT   = ����� ���������� �����

}



begin
  //������� �������
  try
    Application.ProcessMessages;
    FileN:=CommonSet.PathExport+'expout.txt';
    AssignFile(F, FileN);
 //     if Not FileExists(FileN) then Rewrite(F)
//      else                           Append(F);
    Rewrite(F);

    ProgressBar2.Position:=0;
    ProgressBar2.Visible:=True;
    delay(10);

//----������
    if cxCheckBox1.Checked then
    begin
      Label4.Caption:='�����, ���� ��������� ������ (������)..'; Delay(10);
      quCountD.Active:=False;
      quCountD.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quCountD.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quCountD.Active:=True;
      iMax:=quCountDCOUNTDOC.AsInteger;
      quCountD.Active:=False;

      if iMax>0 then
      begin
        quExport.Active:=False;
        quExport.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
        quExport.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
        quExport.Active:=True;

        Label4.Caption:='�����, ���� ������������ ������ (������)..'; Delay(10);
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=True;


        Idh:=0; i:=0;
        SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
        bW:=False;

        quExport.First;
        while not quExport.Eof do
        begin
          if quExportIDHEAD.AsInteger<>Idh then
          begin //��������� ���������
            if Idh<>0 then
            begin //����� ����������
              StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
              WriteLn(F,AnsiToOemConvert(StrWk));
            end;
            SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
            StrWk:='���;'+quExportEXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExportDATEDOC.AsDateTime)+';'+quExportNUMDOC.AsString+';'+quExportNAMECL.AsString+';'+quExportINN.AsString+';'+FormatDateTime('dd.mm.yyyy',quExportDATESF.AsDateTime)+';'+quExportNUMSF.AsString+';';
            IdH:=quExportIDHEAD.AsInteger;
            bW:=True;
            inc(i);
            ProgressBar1.Position:=i*100 div iMax; Delay(10);
          end;
          if quExportCATEGORY.AsInteger<>4 then
          begin
            SUM_POST:=SUM_POST+quExportSUMIN.AsFloat;
            SUM_SKLAD:=SUM_SKLAD+quExportSUMUCH.AsFloat;
            if  quExportIDNDS.AsInteger=2 then //��� 10
            begin
              SUM_10:=SUM_10+quExportSUMIN.AsFloat-quExportSUMNDS.AsFloat;
              NDS_10:=NDS_10+quExportSUMNDS.AsFloat;
            end;
            if  quExportIDNDS.AsInteger=3 then //��� 18
            begin
              SUM_18:=SUM_18+quExportSUMIN.AsFloat-quExportSUMNDS.AsFloat;
              NDS_18:=NDS_18+quExportSUMNDS.AsFloat;
            end;
          end else
          begin
            SUM_TARA:=SUM_TARA+quExportSUMIN.AsFloat;
          end;

          quExport.Next;
        end;
        if bW then
        begin //����� ����������
          StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
          WriteLn(F,AnsiToOemConvert(StrWk));
        end;
        ProgressBar1.Position:=100; delay(100);
        Flush(F);
      end;
    end;

    ProgressBar2.Position:=50; delay(50);

//----������ - �������
    if cxCheckBox2.Checked then
    begin
      Label4.Caption:='�����, ���� ��������� ������ (�������)..'; Delay(10);
      quCountDOut.Active:=False;
      quCountDOut.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quCountDOut.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quCountDOut.Active:=True;
      iMax:=quCountDOutCOUNTDOC.AsInteger;
      quCountDOut.Active:=False;

      if iMax>0 then
      begin

        quExpOut.Active:=False;
        quExpOut.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
        quExpOut.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
        quExpOut.Active:=True;

        Label4.Caption:='�����, ���� ������������ ������ (�������)..'; Delay(10);
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=True;

        Idh:=0; i:=0;
        SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
        bW:=False;

        quExpOut.First;
        while not quExpOut.Eof do
        begin
          if quExpOutIDHEAD.AsInteger<>Idh then
          begin //��������� ���������
            if Idh<>0 then
            begin //����� ����������
              StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
              WriteLn(F,AnsiToOemConvert(StrWk));
            end;
            SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
            StrWk:='���;'+quExpOutEXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutDATEDOC.AsDateTime)+';'+quExpOutNUMDOC.AsString+';'+quExpOutNAMECL.AsString+';'+quExpOutINN.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutDATESF.AsDateTime)+';'+quExpOutNUMSF.AsString+';';
            IdH:=quExpOutIDHEAD.AsInteger;
            bW:=True;
            inc(i);
            ProgressBar1.Position:=i*100 div iMax; Delay(10);
          end;
          if quExpOutCATEGORY.AsInteger<>4 then
          begin
            SUM_POST:=SUM_POST+quExpOutSUMIN.AsFloat;
            SUM_SKLAD:=SUM_SKLAD+quExpOutSUMUCH.AsFloat;
            if  quExpOutIDNDS.AsInteger=2 then //��� 10
            begin
              SUM_10:=SUM_10+quExpOutSUMIN.AsFloat-quExpOutSUMNDS.AsFloat;
              NDS_10:=NDS_10+quExpOutSUMNDS.AsFloat;
            end;
            if  quExpOutIDNDS.AsInteger=3 then //��� 18
            begin
              SUM_18:=SUM_18+quExpOutSUMIN.AsFloat-quExpOutSUMNDS.AsFloat;
              NDS_18:=NDS_18+quExpOutSUMNDS.AsFloat;
            end;
          end else
          begin
            SUM_TARA:=SUM_TARA+quExpOutSUMIN.AsFloat;
          end;
          quExpOut.Next;
        end;
        if bW then
        begin //����� ����������
          StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
          WriteLn(F,AnsiToOemConvert(StrWk));
        end;
        ProgressBar1.Position:=100; delay(100);
        Flush(F);
      end;
    end;
    ProgressBar2.Position:=60; delay(50);

//----������ - ���������� �� �������
    if cxCheckBox3.Checked then
    begin
      Label4.Caption:='�����, ���� ��������� ������ (���������� �� �������)..'; Delay(10);
      quCountDOutR.Active:=False;
      quCountDOutR.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quCountDOutR.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quCountDOutR.Active:=True;
      iMax:=quCountDOutRCOUNTDOC.AsInteger;
      quCountDOutR.Active:=False;

      if iMax>0 then
      begin

        quExpOutR.Active:=False;
        quExpOutR.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
        quExpOutR.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
        quExpOutR.Active:=True;

        Label4.Caption:='�����, ���� ������������ ������ (���������� �� �������)..'; Delay(10);
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=True;

        Idh:=0; i:=0;
        SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
        bW:=False;

        quExpOutR.First;
        while not quExpOutR.Eof do
        begin
          if quExpOutRIDHEAD.AsInteger<>Idh then
          begin //��������� ���������
            if Idh<>0 then
            begin //����� ����������
              StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
              WriteLn(F,AnsiToOemConvert(StrWk));
            end;
            SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
            StrWk:='���;'+quExpOutREXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutRDATEDOC.AsDateTime)+';'+quExpOutRNUMDOC.AsString+';'+quExpOutRNAMECL.AsString+';'+quExpOutRINN.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutRDATESF.AsDateTime)+';'+quExpOutRNUMSF.AsString+';';
            IdH:=quExpOutRIDHEAD.AsInteger;
            bW:=True;
            inc(i);
            ProgressBar1.Position:=i*100 div iMax; Delay(10);
          end;
          SUM_POST:=SUM_POST+quExpOutRSUMIN.AsFloat;
          SUM_SKLAD:=SUM_SKLAD+quExpOutRSUMR.AsFloat;
          if  quExpOutRIDNDS.AsInteger=2 then //��� 10
          begin
            SUM_10:=SUM_10+quExpOutRSUMIN.AsFloat-quExpOutRSUMNDS.AsFloat;
            NDS_10:=NDS_10+quExpOutRSUMNDS.AsFloat;
          end;
          if  quExpOutRIDNDS.AsInteger=3 then //��� 18
          begin
            SUM_18:=SUM_18+quExpOutRSUMIN.AsFloat-quExpOutRSUMNDS.AsFloat;
            NDS_18:=NDS_18+quExpOutRSUMNDS.AsFloat;
          end;
          quExpOutR.Next;
        end;
        if bW then
        begin //����� ����������
          StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
          WriteLn(F,AnsiToOemConvert(StrWk));
        end;
        ProgressBar1.Position:=100; delay(100);
        Flush(F);
      end;
    end;
    ProgressBar2.Position:=70; delay(50);

//----����������
{
    SUM_REALIN  = ������������� ����������
    SUM_DELIN   = ������������� ���������
    SUM_REAL    = ����� ����������
    SUM_REALBN  = ����� ���������� ������
    SUM_AVOUT   = ����� ������ ���������
    SUM_AVIN   = ����� ������ ����������
    SUM_USLOUT   = ����� ���������� �����

}
    if cxCheckBox4.Checked then
    begin
    with dmO do
    begin
      quListR.Active:=False;
      quListR.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quListR.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quListR.Active:=True;
      iMax:=quListR.RecordCount;
      i:=0;
      Label4.Caption:='�����, ���� ������������ ������ (����������)..'; Delay(10);
      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;


      quListR.First;
      while not quListR.Eof do
      begin
        //������� �������� �� ��� �� ������
        SUM_REALIN:=0;
        SUM_DELIN:=0;
        SUM_REAL:=0;
        SUM_REALBN:=0;
        SUM_AVOUT:=0;
        SUM_AVIN:=0;
        SUM_USLOUT:=0;

        //������ �� �����
        taTOOutB.Active:=False;
        taTOOutB.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        taTOOutB.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        taTOOutB.Active:=True;

        taTOOutB.First;
        while not taTOOutB.Eof do
        begin
          if taTOOutBOPER.AsString='Sale' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
            SUM_REAL:=SUM_REAL+taTOOutBSUMUCH.AsFloat;
          end;
          if taTOOutBOPER.AsString='SaleBank' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
            SUM_REALBN:=SUM_REALBN+taTOOutBSUMUCH.AsFloat;
          end;
          if taTOOutBOPER.AsString='Ret' then
          begin
            SUM_REALIN:=SUM_REALIN-abs(taTOOutBSUMIN.AsFloat);
            SUM_REAL:=SUM_REAL-abs(taTOOutBSUMUCH.AsFloat);
          end;
          if taTOOutBOPER.AsString='RetBank' then
          begin
            SUM_REALIN:=SUM_REALIN-abs(taTOOutBSUMIN.AsFloat);
            SUM_REALBN:=SUM_REALBN-abs(taTOOutBSUMUCH.AsFloat);
          end;
          //SalePC ������ ��� ������� ��������
{          if taTOOutBOPER.AsString='SalePC' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
          end;}
          if taTOOutBOPER.AsString='Del' then
          begin
            SUM_DELIN:=SUM_DELIN+taTOOutBSUMIN.AsFloat;
          end;


          taTOOutB.Next;
        end;
        taTOOutB.Active:=False;

        //������ �� ����� - ������
        taTOOutBCat.Active:=False;
        taTOOutBCat.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        taTOOutBCat.ParamByName('ICAT').AsInteger:=2;
        taTOOutBCat.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        taTOOutBCat.Active:=True;

        if (taTOOutBCatRSUM.AsFloat<>0) then
        begin
          SUM_USLOUT:=SUM_USLOUT+taTOOutBCatRSUM.AsFloat;
//          SUM_REAL:=SUM_REAL-SUM_USLOUT;
        end;
        taTOOutBCat.Active:=False;


        //������ �� ����� - ������ ����������
        quAvIn.Active:=False;
        quAvIn.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        quAvIn.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        quAvIn.Active:=True;

        if quAvInRSUMA.AsFloat<>0 then
        begin
          SUM_AVIN:=SUM_AVIN+quAvInRSUMA.AsFloat;
//          SUM_REAL:=SUM_REAL-SUM_AVIN;
        end;
        quAvIn.Active:=True;

        //������ �� ����� - ������ �������
        quAvOut.Active:=False;
        quAvOut.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        quAvOut.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        quAvOut.Active:=True;

        if quAvOutRSUMA.AsFloat<>0 then
        begin
          SUM_AVOut:=SUM_AVOut+(-1)*quAvOutRSUMA.AsFloat;
//          SUM_REAL:=SUM_REAL+SUM_AVOUT;
        end;
        quAvOut.Active:=True;

        SUM_REALIN:=RoundEx(SUM_REALIN*100)/100;
        SUM_DELIN:=RoundEx(SUM_DELIN*100)/100;
        SUM_REAL:=RoundEx(SUM_REAL*100)/100;
        SUM_REALBN:=RoundEx(SUM_REALBN*100)/100;
        SUM_AVOUT:=RoundEx(SUM_AVOUT*100)/100;
        SUM_AVIN:=RoundEx(SUM_AVIN*100)/100;
        SUM_USLOUT:=RoundEx(SUM_USLOUT*100)/100;


        StrWk:='���;'+quListREXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quListRDATEDOC.AsDateTime)+';;;;;;'+FlToS(SUM_REALIN)+';'+FlToS(SUM_DELIN)+';'+FlToS(SUM_REAL)+';'+FlToS(SUM_REALBN)+';'+FlToS(SUM_AVIN)+';'+FlToS(SUM_AVOUT)+';'+FlToS(SUM_USLOUT)+';';
        WriteLn(F,AnsiToOemConvert(StrWk));
        Flush(F);

        quListR.Next;
        inc(i);
        ProgressBar1.Position:=i*100 div iMax; Delay(10);
      end;
      quListR.Active:=False;

      quListR1.Active:=False;
      quListR1.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quListR1.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quListR1.Active:=True;

      quListR1.First;
      while not quListR1.Eof do
      begin
        StrWk:=quListR1OPER.AsString+';'+quListR1EXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quListR1DATEDOC.AsDateTime)+';'+FlToS(quListR1SUMIN.AsFloat)+';'+FlToS(quListR1SUMUCH.AsFloat)+';'+quListR1COMMENT.AsString+';';

        WriteLn(F,AnsiToOemConvert(StrWk));
        Flush(F);

        quListR1.Next;
      end;
      quListR1.Active:=False;
    end;
    end;

    ProgressBar2.Position:=90; delay(50);


//----���������� ���������


    if cxCheckBox5.Checked then
    begin
      quCountVn.Active:=False;
      quCountVn.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quCountVn.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quCountVn.Active:=True;
      iMax:=quCountVnCOUNTDOC.AsInteger;
      quCountVn.Active:=False;

      ProgressBar2.Position:=0;
      ProgressBar2.Visible:=True;

      if iMax>0 then
      begin

        quExportVn.Active:=False;
        quExportVn.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
        quExportVn.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
        quExportVn.Active:=True;

        Label4.Caption:='�����, ���� ������������ ������ (�����������)..'; Delay(10);
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=True;

        i:=0;
        quExportVn.First;
        while not quExportVn.Eof do
        begin
          StrWk:='���;'+quExportVnEXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExportVnDATEDOC.AsDateTime)+';'+quExportVnNUMDOC.AsString+';'+quExportVnEXPNAME1.asString+';;;;'+FlToS(quExportVnSUMIN.AsFloat)+';'+FlToS(quExportVnSUMUCH.AsFloat)+';'+FlToS(quExportVnSUMUCH1.AsFloat)+';'+FlToS(quExportVnSUMTAR.AsFloat)+';';
          WriteLn(F,AnsiToOemConvert(StrWk));

          quExportVn.Next;
          inc(i);
          ProgressBar1.Position:=i*100 div iMax; Delay(10);
        end;
        ProgressBar1.Position:=100; delay(100);
        Flush(F);
      end;
    end;

    ProgressBar2.Position:=100; delay(50);

  finally
    ProgressBar1.Position:=100; Delay(10);
    ProgressBar2.Position:=100;
    Label4.Caption:='������������ ������ ���������.'; Delay(10);
    quExport.Active:=False;
    quExpOut.Active:=False;
    quExpOutR.Active:=False;
    quExportVn.Active:=False;
    CloseFile(F);
  end;
end;

procedure TfmExport.FormCreate(Sender: TObject);
begin
  Label4.Caption:='';
  Label1.Caption:='������� ������������ � ���� '+CommonSet.PathExport+'expout.txt';
end;

procedure TfmExport.cxButton3Click(Sender: TObject);
var
  FList,Dest,PList,DList: TStrings;
  i,k,m:INteger;
  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Memo1.Clear;
  Memo1.Lines.Add('  �������� �������� ������ �� FTP �������� ��������.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    StrWk:=CommonSet.PathExport+'expout.txt';
    
    if FileExists(StrWk) then
    begin
      FList.Add(StrWk); // ���������� � ������ �������� ����� ��� �������� � ������
      While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
//      if CommonSet.Compress=1 then StrWk:=StrWk+TrExt;
      DList.Add(StrWk); //���������� �������� ���  �����
    end;

    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
    //3. ����������
    //4. ��������
    //5. ��� ������ ���������� � �����
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      Memo1.Lines.Add(' ���� - '+FList.Strings[i]);
      {$I-}
      Assignfile(f,FList.Strings[i]);
      FileMode := 2;  {Set file access to read/Write }
      Reset(F);
      CloseFile(F);
      {$I+}
      if IOResult = 0 then
      begin
        Memo1.Lines.Add(' ������ - ��.');
        try
{          if CommonSet.Compress=1 then
          begin
            fs := TECLFileStream.Create(CurDir+DList.Strings[i],fmCreate,odInac,levelValue[9]);
            fs.LoadFromFile(FList.Strings[i]);
            fs.Free;
            Memo1.Lines.Add(' ������ - ��.');
          end;
}
          For k:=0 to PList.Count-1 do
          begin
            StrWk:=PList.Strings[k];
            m:=0;
            BadPath:=False;
            While StrWk>'' do
            begin
              if Pos('\',strwk)>0 then
              begin
                StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                Delete(StrWk,1,Pos('\',strwk));
              end else
              begin
                StrWk1:=StrWk;
                StrWk:='';
              end;
              if strwk1>'' then
              begin
                Memo1.Lines.Add('    ���� ��� �������� - '+StrWk1);
                try
                  IdFTP1.ChangeDir(StrWk1);
                  inc(m);
                  Memo1.Lines.Add('    ���� ��.'+StrWk1);
                except
                  Memo1.Lines.Add('    ������ ��� �������� - '+StrWk1);
                  BadPath:=True;
                end;
              end;
            end;

            if not BadPath then //���� ���������� ����.
            begin
              try
                Memo1.Lines.Add('      �������� ����� - '+DList.Strings[i]);
                if CommonSet.Compress=1 then
                begin
                  IDFtp1.Put(DList.Strings[i], DList.Strings[i], False );
                end
                else
                begin
                  StrWk:=FList.Strings[i];
                  While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
                  IDFtp1.Put(FList.Strings[i], StrWk, False );
                end;
//                Memo1.Lines.Add('      �������� Ok.');
              except
                Memo1.Lines.Add('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
              end;
            end;

            While m>0 do
            begin
              IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
              m:=m-1;
            end;
          end;

          //������� ������
{          if CommonSet.Compress=1 then
          begin
            Assignfile(f,CurDir+DList.Strings[i]);
            erase(f);
          end;
}
{     //���������� ������

           if TrGood then //���������� ���� � �����
            MoveFile(CurDir+FList.Strings[i], CommonSet.PathArh+FList.Strings[i]);
}
        except
//          Memo1.Lines.Add(' ������ ��� ������.');
        end;
      end
      else
      begin
        Memo1.Lines.Add(' � ������� ��������.');
      end;
      delay(100);
    end;
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
    Memo1.Lines.Add('  �������� �������� ���������.');
  end;
end;

{
Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmMainFrom.Memo1.Lines.Count > 500 then fmMainFrom.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmMainFrom.Memo1.Lines.Add(StrWk);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;
}

procedure TfmExport.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

end.
