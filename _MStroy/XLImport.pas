unit XLImport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ComCtrls, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxmdaset, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComObj, Un1, StrUtils, dmOffice,
  FIBQuery, pFIBQuery;

type
  TfmXLImport = class(TForm)
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    XLImport: TcxGridDBTableView;
    cxGridXLImport: TcxGridLevel;
    cxGrid1: TcxGrid;
    teXLImport: TdxMemData;
    teXLImportName: TStringField;
    teXLImportEXTART: TStringField;
    teXLImportStrana: TStringField;
    teXLImportEdIzm: TStringField;
    teXLImportPrice: TFloatField;
    dsXLImport: TDataSource;
    XLImportRecId: TcxGridDBColumn;
    XLImportName: TcxGridDBColumn;
    XLImportEXTART: TcxGridDBColumn;
    XLImportStrana: TcxGridDBColumn;
    XLImportEdIzm: TcxGridDBColumn;
    XLImportPrice: TcxGridDBColumn;
    cxButton1: TcxButton;
    SpeedItem2: TSpeedItem;
    qXLImport: TpFIBQuery;
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmXLImport: TfmXLImport;

implementation

uses Goods;

{$R *.dfm}

procedure TfmXLImport.SpeedItem2Click(Sender: TObject);
begin
  fmXLImport.Close;
end;

procedure TfmXLImport.SpeedItem1Click(Sender: TObject);
var
    ExcelApp, Workbook, ISheet: Variant;
    i:integer;
    //q:integer;
begin

  if OpenDialog1.Execute then
    begin
      ExcelApp := CreateOleObject('Excel.Application');
      ExcelApp.Application.EnableEvents := false;
      Workbook := ExcelApp.WorkBooks.Add(OpenDialog1.fileName);
      ISheet := Workbook.Worksheets.Item[1];

      CloseTe(teXLImport);
      i:=2;
      //teXLImport.Active:=True;

      //showmessage(ISheet.Cells.Item[i, 4].Value);
      //memo1.Lines.Add('���� �������� �����. �����');
        While ISheet.Cells.Item[i, 1].Value<>'' do
          begin
            teXLImport.Append;
              teXLImportName.AsString:=String(ISheet.Cells.Item[i, 1].Value);
              teXLImportEXTART.AsString:=String(ISheet.Cells.Item[i, 2].Value);
              teXLImportStrana.AsString:=String(ISheet.Cells.Item[i, 3].Value);
              teXLImportEdIzm.AsString:=String(ISheet.Cells.Item[i, 4].Value);
              teXLImportPrice.AsFloat:=strtofloat(AnsiReplaceStr(string(ISheet.Cells.Item[i, 5].Value),'.',','));
            teXLImport.Post;
            inc(i);
          end;
        //memo1.Lines.Add('�������� ���������');
        ExcelApp.Quit;
        ExcelApp:=Unassigned;
        teXLImport.First;
      end;
end;


procedure TfmXLImport.cxButton1Click(Sender: TObject);
var idh:integer;
    imessure:integer;
    rc:integer;
    sName:String;
begin
  teXLImport.First;
{  qXLImport.Close;
  qXLImport.SQL.Clear;
  qXLImport.SQL.Add('select max(id) as idh from of_cards');
  qXLImport.ExecQuery;
  idh:=qXLImport.FldByName['idh'].AsInteger;
  }

  While not teXLImport.Eof do
  begin
//      inc(idh);
    qXLImport.Close;
    qXLImport.SQL.Clear;
    qXLImport.SQL.Add('select id from of_messur where nameshort like ''%'+teXLImportEdIzm.AsString+'%'' ');
    qXLImport.Transaction.StartTransaction;
    qXLImport.ExecQuery;
      imessure:=qXLImport.FldByName['id'].AsInteger;
    qXLImport.Transaction.Commit;

    sName:=trim(teXLImportEXTART.AsString);
    if sName>'' then sName:=teXLImportName.AsString+' ('+trim(teXLImportEXTART.AsString)+')'
    else sName:=teXLImportName.AsString;

    qXLImport.Close;
    qXLImport.SQL.Clear;
    qXLImport.SQL.Add('select count(id) as rc from of_cards');
    qXLImport.SQL.Add('where name like ''%'+sName+'%'' and parent='+inttostr(integer(fmCards.ClassTree.Selected.Data)));
    qXLImport.Transaction.StartTransaction;
    qXLImport.ExecQuery;
      rc:=qXLImport.FldByName['rc'].AsInteger;
    qXLImport.Transaction.Commit;

    //showmessage(inttostr(rc));

    if rc=0 then
    begin
        idh:=GetId('GD');

        with dmO do
        begin
          while iId<1000000 do   //��� ���� ��� ����� ����� ������ 1000000 �������� ��������� ����� � 0, ������ ��� ��������
          begin
            quGdsFind.Active:=False;
            quGdsFind.ParamByName('IID').AsInteger:=iId;
            quGdsFind.Active:=True;
            if quGdsFind.RecordCount=0 then break
            else iId:=GetId('GD');
          end;
          quGdsFind.Active:=False;
          if iId=100000 then
          begin
            showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
            exit;
          end;
        end;

        qXLImport.Close;
        qXLImport.SQL.Clear;
        qXLImport.SQL.Add('insert into of_cards');
        qXLImport.SQL.Add('(  id ');
        qXLImport.SQL.Add(', parent ');
        qXLImport.SQL.Add(', name ');
        qXLImport.SQL.Add(', ttype ');
        qXLImport.SQL.Add(', imessure ');
        qXLImport.SQL.Add(', inds ');
        qXLImport.SQL.Add(', lastpricein ');
        qXLImport.SQL.Add(', iactive ');
        qXLImport.SQL.Add(', category ');
        qXLImport.SQL.Add(', spisstore ');
        qXLImport.SQL.Add(', rcategory ');
        qXLImport.SQL.Add(', strana) ');
        qXLImport.SQL.Add('values ');
        qXLImport.SQL.Add('(  '+inttostr(idh));
        qXLImport.SQL.Add(', '+inttostr(integer(fmCards.ClassTree.Selected.Data)));
        qXLImport.SQL.Add(', '''+sName+'''  ');
        qXLImport.SQL.Add(', 0  ');
        qXLImport.SQL.Add(', '+inttostr(imessure));
        qXLImport.SQL.Add(', 2  ');
        qXLImport.SQL.Add(', '+AnsiReplaceStr(teXLImportPrice.AsString,',','.')+' ');
        qXLImport.SQL.Add(', 1 ');
        qXLImport.SQL.Add(', 1  ');
        qXLImport.SQL.Add(', ''00000000000000000000'' ');
        qXLImport.SQL.Add(', 1 ');
        qXLImport.SQL.Add(', '''+teXLImportStrana.AsString+''')');

        //qXLImport.SQL.SaveToFile('c:\zi.txt');

        qXLImport.Transaction.StartTransaction;
        qXLImport.ExecQuery;
        qXLImport.Transaction.Commit;
      end
      else
      begin
        qXLImport.Close;
        qXLImport.SQL.Clear;
        qXLImport.SQL.Add('update of_cards set');
        qXLImport.SQL.Add('lastpricein ='''+AnsiReplaceStr(teXLImportPrice.AsString,',','.')+''',');
        qXLImport.SQL.Add('imessure ='+intToStr(imessure));
        qXLImport.SQL.Add('where name like ''%'+sName+'%'' and parent='''+inttostr(integer(fmCards.ClassTree.Selected.Data))+'''');

        //qXLImport.SQL.SaveToFile('c:\zu.txt');

        qXLImport.Transaction.StartTransaction;
        qXLImport.ExecQuery;
        qXLImport.Transaction.Commit;
      end;

    teXLImport.Next;
  end;
  delay(10);
  dmO.quCardsSel.FullRefresh;
  delay(10);
  ShowMessage('���������� ��.');
end;

end.
