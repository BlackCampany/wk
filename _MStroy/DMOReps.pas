unit DMOReps;

interface

uses
  SysUtils, Classes, DB, DBClient, frexpimg, frRtfExp, frTXTExp, frXMLExl,
  frOLEExl, FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT,
  FR_DSet, FR_DBSet, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  FIBQuery, pFIBQuery, pFIBStoredProc,Dialogs, dxmdaset;

type
  TdmORep = class(TDataModule)
    taPartTest: TClientDataSet;
    taPartTestNum: TIntegerField;
    taPartTestIdGoods: TIntegerField;
    taPartTestNameG: TStringField;
    taPartTestIM: TIntegerField;
    taPartTestSM: TStringField;
    taPartTestQuant: TFloatField;
    taPartTestPrice1: TCurrencyField;
    taPartTestiRes: TSmallintField;
    dsPartTest: TDataSource;
    taCalc: TClientDataSet;
    taCalcArticul: TIntegerField;
    taCalcName: TStringField;
    taCalcIdm: TIntegerField;
    taCalcsM: TStringField;
    taCalcKm: TFloatField;
    taCalcQuant: TFloatField;
    taCalcQuantFact: TFloatField;
    taCalcQuantDiff: TFloatField;
    dsCalc: TDataSource;
    frRep1: TfrReport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBMPExport1: TfrBMPExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    taCalcSumUch: TFloatField;
    taCalcSumIn: TFloatField;
    taCalcB: TClientDataSet;
    taCalcBCODEB: TIntegerField;
    taCalcBNAMEB: TStringField;
    taCalcBQUANT: TFloatField;
    taCalcBPRICEOUT: TFloatField;
    taCalcBSUMOUT: TFloatField;
    taCalcBID: TIntegerField;
    taCalcBIDCARD: TIntegerField;
    taCalcBNAMEC: TStringField;
    taCalcBQUANTC: TFloatField;
    taCalcBPRICEIN: TFloatField;
    taCalcBSUMIN: TFloatField;
    taCalcBIM: TIntegerField;
    taCalcBSM: TStringField;
    dsCalcB: TDataSource;
    frdsCalcB: TfrDBDataSet;
    taCalcBSB: TStringField;
    taTORep: TClientDataSet;
    dsTORep: TDataSource;
    taTORepsType: TStringField;
    taTORepsDocType: TStringField;
    taTORepsCliName: TStringField;
    taTORepsDate: TStringField;
    taTORepsDocNum: TStringField;
    taTOReprSum: TCurrencyField;
    taTOReprSum1: TCurrencyField;
    frdsTORep: TfrDBDataSet;
    taTOReprSumO: TCurrencyField;
    taTOReprSumO1: TCurrencyField;
    taCMove: TClientDataSet;
    dstaCMove: TDataSource;
    taCMoveARTICUL: TIntegerField;
    taCMoveIDATE: TIntegerField;
    taCMoveIDSTORE: TIntegerField;
    taCMoveNAMEMH: TStringField;
    taCMoveRB: TFloatField;
    taCMovePOSTIN: TFloatField;
    taCMovePOSTOUT: TFloatField;
    taCMoveVNIN: TFloatField;
    taCMoveVNOUT: TFloatField;
    taCMoveINV: TFloatField;
    taCMoveQREAL: TFloatField;
    taCMoveRE: TFloatField;
    trSelRep: TpFIBTransaction;
    quDocOutBPrint: TpFIBDataSet;
    quDocOutBPrintIDB: TFIBIntegerField;
    quDocOutBPrintCODEB: TFIBIntegerField;
    quDocOutBPrintQUANT: TFIBFloatField;
    quDocOutBPrintSUMOUT: TFIBFloatField;
    quDocOutBPrintIDSKL: TFIBIntegerField;
    quDocOutBPrintSUMIN: TFIBFloatField;
    quDocOutBPrintNAMEB: TFIBStringField;
    quDocOutBPrintPRICER: TFIBFloatField;
    quSelInSum: TpFIBDataSet;
    quSelNamePos: TpFIBDataSet;
    quSelNamePosNAMECL: TFIBStringField;
    quSelNamePosPARENT: TFIBIntegerField;
    quSelNamePosNAME: TFIBStringField;
    quSelNamePosSM: TFIBStringField;
    quSelNamePosKOEF: TFIBFloatField;
    quSelNamePosIMESSURE: TFIBIntegerField;
    quSelOutSum: TpFIBDataSet;
    quSelNameCl: TpFIBDataSet;
    quSelNameClPARENT: TFIBIntegerField;
    quSelNameClNAMECL: TFIBStringField;
    quDocsCompl: TpFIBDataSet;
    dsquDocsCompl: TDataSource;
    quDocsComplID: TFIBIntegerField;
    quDocsComplDATEDOC: TFIBDateField;
    quDocsComplNUMDOC: TFIBStringField;
    quDocsComplIDSKL: TFIBIntegerField;
    quDocsComplSUMIN: TFIBFloatField;
    quDocsComplSUMUCH: TFIBFloatField;
    quDocsComplSUMTAR: TFIBFloatField;
    quDocsComplPROCNAC: TFIBFloatField;
    quDocsComplIACTIVE: TFIBIntegerField;
    quDocsComplOPER: TFIBStringField;
    quDocsComplNAMEMH: TFIBStringField;
    quDocsComlRec: TpFIBDataSet;
    quSpecCompl: TpFIBDataSet;
    quSpecComplC: TpFIBDataSet;
    quSpecComplCB: TpFIBDataSet;
    quDocsComlRecID: TFIBIntegerField;
    quDocsComlRecDATEDOC: TFIBDateField;
    quDocsComlRecNUMDOC: TFIBStringField;
    quDocsComlRecIDSKL: TFIBIntegerField;
    quDocsComlRecSUMIN: TFIBFloatField;
    quDocsComlRecSUMUCH: TFIBFloatField;
    quDocsComlRecSUMTAR: TFIBFloatField;
    quDocsComlRecPROCNAC: TFIBFloatField;
    quDocsComlRecIACTIVE: TFIBIntegerField;
    quDocsComlRecOPER: TFIBStringField;
    quSpecComplIDHEAD: TFIBIntegerField;
    quSpecComplID: TFIBIntegerField;
    quSpecComplIDCARD: TFIBIntegerField;
    quSpecComplQUANT: TFIBFloatField;
    quSpecComplIDM: TFIBIntegerField;
    quSpecComplKM: TFIBFloatField;
    quSpecComplPRICEIN: TFIBFloatField;
    quSpecComplSUMIN: TFIBFloatField;
    quSpecComplPRICEINUCH: TFIBFloatField;
    quSpecComplSUMINUCH: TFIBFloatField;
    quSpecComplTCARD: TFIBSmallIntField;
    quSpecComplNAMESHORT: TFIBStringField;
    quSpecComplNAME: TFIBStringField;
    quSpecComplCIDHEAD: TFIBIntegerField;
    quSpecComplCID: TFIBIntegerField;
    quSpecComplCIDCARD: TFIBIntegerField;
    quSpecComplCQUANT: TFIBFloatField;
    quSpecComplCIDM: TFIBIntegerField;
    quSpecComplCKM: TFIBFloatField;
    quSpecComplCPRICEIN: TFIBFloatField;
    quSpecComplCSUMIN: TFIBFloatField;
    quSpecComplCPRICEINUCH: TFIBFloatField;
    quSpecComplCSUMINUCH: TFIBFloatField;
    quSpecComplCNAMESHORT: TFIBStringField;
    quSpecComplCNAME: TFIBStringField;
    quSpecComplCBIDHEAD: TFIBIntegerField;
    quSpecComplCBIDB: TFIBIntegerField;
    quSpecComplCBID: TFIBIntegerField;
    quSpecComplCBCODEB: TFIBIntegerField;
    quSpecComplCBNAMEB: TFIBStringField;
    quSpecComplCBQUANT: TFIBFloatField;
    quSpecComplCBIDCARD: TFIBIntegerField;
    quSpecComplCBNAMEC: TFIBStringField;
    quSpecComplCBQUANTC: TFIBFloatField;
    quSpecComplCBPRICEIN: TFIBFloatField;
    quSpecComplCBSUMIN: TFIBFloatField;
    quSpecComplCBIM: TFIBIntegerField;
    quSpecComplCBSM: TFIBStringField;
    quSpecComplCBSB: TFIBStringField;
    taHeadDoc: TClientDataSet;
    taSpecDoc: TClientDataSet;
    taHeadDocIType: TSmallintField;
    taHeadDocDateDoc: TIntegerField;
    taHeadDocNumDoc: TStringField;
    taHeadDocIdCli: TIntegerField;
    taHeadDocNameCli: TStringField;
    taHeadDocIdSkl: TIntegerField;
    taHeadDocNameSkl: TStringField;
    taHeadDocSumIN: TFloatField;
    taHeadDocSumUch: TFloatField;
    taHeadDocId: TIntegerField;
    taSpecDocIType: TSmallintField;
    taSpecDocIdHead: TIntegerField;
    taSpecDocNum: TIntegerField;
    taSpecDocIdCard: TIntegerField;
    taSpecDocQuant: TFloatField;
    taSpecDocPriceIn: TFloatField;
    taSpecDocSumIn: TFloatField;
    taSpecDocPriceUch: TFloatField;
    taSpecDocSumUch: TFloatField;
    taSpecDocIdNds: TIntegerField;
    taSpecDocSumNds: TFloatField;
    taSpecDocNameC: TStringField;
    taSpecDocSm: TStringField;
    taSpecDocIdM: TIntegerField;
    dsHeadDoc: TDataSource;
    dsSpecDoc: TDataSource;
    quDocsActs: TpFIBDataSet;
    dsquDocsActs: TDataSource;
    quDocsActsId: TpFIBDataSet;
    quDocsActsID2: TFIBIntegerField;
    quDocsActsDATEDOC: TFIBDateField;
    quDocsActsNUMDOC: TFIBStringField;
    quDocsActsIDSKL: TFIBIntegerField;
    quDocsActsSUMIN: TFIBFloatField;
    quDocsActsSUMUCH: TFIBFloatField;
    quDocsActsIACTIVE: TFIBIntegerField;
    quDocsActsOPER: TFIBStringField;
    quDocsActsIdID: TFIBIntegerField;
    quDocsActsIdDATEDOC: TFIBDateField;
    quDocsActsIdNUMDOC: TFIBStringField;
    quDocsActsIdIDSKL: TFIBIntegerField;
    quDocsActsIdSUMIN: TFIBFloatField;
    quDocsActsIdSUMUCH: TFIBFloatField;
    quDocsActsIdIACTIVE: TFIBIntegerField;
    quDocsActsIdOPER: TFIBStringField;
    quDocsActsNAMEMH: TFIBStringField;
    quDocsVnSel: TpFIBDataSet;
    dsDocsVnSel: TDataSource;
    quSpecVnSel: TpFIBDataSet;
    quDocsVnId: TpFIBDataSet;
    quDocsVnSelID: TFIBIntegerField;
    quDocsVnSelDATEDOC: TFIBDateField;
    quDocsVnSelNUMDOC: TFIBStringField;
    quDocsVnSelIDSKL_FROM: TFIBIntegerField;
    quDocsVnSelIDSKL_TO: TFIBIntegerField;
    quDocsVnSelSUMIN: TFIBFloatField;
    quDocsVnSelSUMUCH: TFIBFloatField;
    quDocsVnSelSUMUCH1: TFIBFloatField;
    quDocsVnSelSUMTAR: TFIBFloatField;
    quDocsVnSelPROCNAC: TFIBFloatField;
    quDocsVnSelIACTIVE: TFIBIntegerField;
    quDocsVnSelNAMEMH: TFIBStringField;
    quDocsVnSelNAMEMH1: TFIBStringField;
    quDocsVnIdID: TFIBIntegerField;
    quDocsVnIdDATEDOC: TFIBDateField;
    quDocsVnIdNUMDOC: TFIBStringField;
    quDocsVnIdIDSKL_FROM: TFIBIntegerField;
    quDocsVnIdIDSKL_TO: TFIBIntegerField;
    quDocsVnIdSUMIN: TFIBFloatField;
    quDocsVnIdSUMUCH: TFIBFloatField;
    quDocsVnIdSUMUCH1: TFIBFloatField;
    quDocsVnIdSUMTAR: TFIBFloatField;
    quDocsVnIdPROCNAC: TFIBFloatField;
    quDocsVnIdIACTIVE: TFIBIntegerField;
    quSpecVnSelIDHEAD: TFIBIntegerField;
    quSpecVnSelID: TFIBIntegerField;
    quSpecVnSelNUM: TFIBIntegerField;
    quSpecVnSelIDCARD: TFIBIntegerField;
    quSpecVnSelQUANT: TFIBFloatField;
    quSpecVnSelPRICEIN: TFIBFloatField;
    quSpecVnSelSUMIN: TFIBFloatField;
    quSpecVnSelPRICEUCH: TFIBFloatField;
    quSpecVnSelSUMUCH: TFIBFloatField;
    quSpecVnSelPRICEUCH1: TFIBFloatField;
    quSpecVnSelSUMUCH1: TFIBFloatField;
    quSpecVnSelIDM: TFIBIntegerField;
    quSpecVnSelNAMEC: TFIBStringField;
    quSpecVnSelSM: TFIBStringField;
    quSpecVnSelKM: TFIBFloatField;
    taSpecDocKm: TFloatField;
    taSpecDocPriceUch1: TFloatField;
    taSpecDocSumUch1: TFloatField;
    quSpecAO: TpFIBDataSet;
    quSpecAOIDHEAD: TFIBIntegerField;
    quSpecAOID: TFIBIntegerField;
    quSpecAOIDCARD: TFIBIntegerField;
    quSpecAOQUANT: TFIBFloatField;
    quSpecAOIDM: TFIBIntegerField;
    quSpecAOKM: TFIBFloatField;
    quSpecAOPRICEIN: TFIBFloatField;
    quSpecAOSUMIN: TFIBFloatField;
    quSpecAOPRICEINUCH: TFIBFloatField;
    quSpecAOSUMINUCH: TFIBFloatField;
    quSpecAOTCARD: TFIBSmallIntField;
    quSpecAONAMESHORT: TFIBStringField;
    quSpecAONAME: TFIBStringField;
    quSpecAI: TpFIBDataSet;
    quSpecAIIDHEAD: TFIBIntegerField;
    quSpecAIID: TFIBIntegerField;
    quSpecAIIDCARD: TFIBIntegerField;
    quSpecAIQUANT: TFIBFloatField;
    quSpecAIIDM: TFIBIntegerField;
    quSpecAIKM: TFIBFloatField;
    quSpecAIPRICEIN: TFIBFloatField;
    quSpecAISUMIN: TFIBFloatField;
    quSpecAIPRICEINUCH: TFIBFloatField;
    quSpecAISUMINUCH: TFIBFloatField;
    quSpecAITCARD: TFIBSmallIntField;
    quSpecAINAMESHORT: TFIBStringField;
    quSpecAINAME: TFIBStringField;
    quSpecAIPROCPRICE: TFIBFloatField;
    taSpecB: TClientDataSet;
    taSpecBID: TIntegerField;
    taSpecBIDCARD: TIntegerField;
    taSpecBQUANT: TFloatField;
    taSpecBIDM: TIntegerField;
    taSpecBSIFR: TIntegerField;
    taSpecBNAMEB: TStringField;
    taSpecBCODEB: TStringField;
    taSpecBKB: TFloatField;
    taSpecBPRICER: TFloatField;
    taSpecBDSUM: TCurrencyField;
    taSpecBRSUM: TCurrencyField;
    taSpecBNAME: TStringField;
    taSpecBNAMESHORT: TStringField;
    taSpecBTCARD: TSmallintField;
    taSpecBKM: TFloatField;
    dstaSpecB: TDataSource;
    quDocOutBPrintNAME: TFIBStringField;
    quDocsActsCOMMENT: TFIBStringField;
    quDocsActsIdCOMMENT: TFIBStringField;
    quOperT: TpFIBDataSet;
    quOperTABR: TFIBStringField;
    quOperTID: TFIBIntegerField;
    quOperTDOCTYPE: TFIBSmallIntField;
    quOperTNAMEOPER: TFIBStringField;
    quOperTNAMEDOC: TFIBStringField;
    dsquOperT: TDataSource;
    taDocType: TpFIBDataSet;
    taDocTypeID: TFIBSmallIntField;
    taDocTypeNAMEDOC: TFIBStringField;
    dstaDocType: TDataSource;
    taOperT: TpFIBDataSet;
    taOperTABR: TFIBStringField;
    taOperTID: TFIBIntegerField;
    taOperD: TpFIBDataSet;
    taOperDABR: TFIBStringField;
    taOperDID: TFIBIntegerField;
    taOperDDOCTYPE: TFIBSmallIntField;
    taOperDNAMEOPER: TFIBStringField;
    dstaOperD: TDataSource;
    taSpecDoc1: TClientDataSet;
    taSpecDoc1IType: TSmallintField;
    taSpecDoc1IdHead: TIntegerField;
    taSpecDoc1Num: TIntegerField;
    taSpecDoc1IdCard: TIntegerField;
    taSpecDoc1Quant: TFloatField;
    taSpecDoc1PriceIn: TFloatField;
    taSpecDoc1SumIn: TFloatField;
    taSpecDoc1PriceUch: TFloatField;
    taSpecDoc1SumUch: TFloatField;
    taSpecDoc1IdNDS: TIntegerField;
    taSpecDoc1SumNds: TFloatField;
    taSpecDoc1NameC: TStringField;
    taSpecDoc1SM: TStringField;
    taSpecDoc1IDM: TIntegerField;
    taSpecDoc1Km: TFloatField;
    taSpecDoc1PriceUch1: TFloatField;
    taSpecDoc1SumUch1: TFloatField;
    taSpecDocProcPrice: TFloatField;
    quSpecVnGrPos: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    FIBFloatField5: TFIBFloatField;
    FIBFloatField6: TFIBFloatField;
    FIBFloatField7: TFIBFloatField;
    FIBIntegerField5: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField8: TFIBFloatField;
    prTestSpecPart: TpFIBStoredProc;
    prResetSum: TpFIBStoredProc;
    quCardPO: TpFIBDataSet;
    quCardPOQUANT: TFIBFloatField;
    quCardPORSUM: TFIBFloatField;
    prCalcSumRemn: TpFIBStoredProc;
    taHeadDocComment: TStringField;
    taDelCard: TpFIBDataSet;
    taDelCardID: TFIBIntegerField;
    taDelCardSIFR: TFIBIntegerField;
    taDelCardNAME: TFIBStringField;
    taDelCardTTYPE: TFIBIntegerField;
    taDelCardIMESSURE: TFIBIntegerField;
    prRECALCGDS: TpFIBStoredProc;
    trD: TpFIBTransaction;
    quClass: TpFIBDataSet;
    quClassID: TFIBIntegerField;
    quClassNAME: TFIBStringField;
    quClassPARENT: TFIBIntegerField;
    quClassIMESSURE: TFIBIntegerField;
    quClassNAMECL: TFIBStringField;
    quFindPart: TpFIBDataSet;
    quFindPartID: TFIBIntegerField;
    quFindPartIDSTORE: TFIBIntegerField;
    quFindPartIDDOC: TFIBIntegerField;
    quFindPartARTICUL: TFIBIntegerField;
    quFindPartIDCLI: TFIBIntegerField;
    quFindPartDTYPE: TFIBIntegerField;
    quFindPartQPART: TFIBFloatField;
    quFindPartQREMN: TFIBFloatField;
    quFindPartPRICEIN: TFIBFloatField;
    quFindPartIDATE: TFIBIntegerField;
    quFindPartNAMECL: TFIBStringField;
    quMainClass: TpFIBDataSet;
    quMainClassID: TFIBIntegerField;
    quFindPartNAMEMH: TFIBStringField;
    pr_CalcLastPrice: TpFIBStoredProc;
    quMovePer: TpFIBDataSet;
    quMovePerPOSTIN: TFIBFloatField;
    quMovePerPOSTOUT: TFIBFloatField;
    quMovePerVNIN: TFIBFloatField;
    quMovePerVNOUT: TFIBFloatField;
    quMovePerINV: TFIBFloatField;
    quMovePerQREAL: TFIBFloatField;
    quSelInSumIDATE: TFIBIntegerField;
    quSelInSumQPART: TFIBFloatField;
    quSelInSumPRICEIN: TFIBFloatField;
    quSelOutSumIDDATE: TFIBIntegerField;
    quSelOutSumQUANT: TFIBFloatField;
    quSelOutSumPRICEIN: TFIBFloatField;
    quSelOutSumSUMOUT: TFIBFloatField;
    quFindSebReal: TpFIBDataSet;
    quFindSebRealIDM: TFIBIntegerField;
    quFindSebRealKM: TFIBFloatField;
    quFindSebRealPRICEIN: TFIBFloatField;
    dsquFindSebReal: TDataSource;
    quFindCl: TpFIBDataSet;
    quFindClID_PARENT: TFIBIntegerField;
    quFindClNAMECL: TFIBStringField;
    quFindMassa: TpFIBDataSet;
    quFindMassaPOUTPUT: TFIBStringField;
    quOperTTPRIB: TFIBSmallIntField;
    quOperTTSPIS: TFIBSmallIntField;
    quFindTSpis: TpFIBDataSet;
    quFindTSpisTSPIS: TFIBSmallIntField;
    quDB: TpFIBDataSet;
    quDBID: TFIBIntegerField;
    quDBNAMEDB: TFIBStringField;
    quDBPATHDB: TFIBStringField;
    dsquDB: TDataSource;
    quClosePartIn: TpFIBQuery;
    trU: TpFIBTransaction;
    quPODoc: TpFIBDataSet;
    quPODocSUMIN: TFIBFloatField;
    quFindCli: TpFIBDataSet;
    quFindCliID: TFIBIntegerField;
    quFindCliNAMECL: TFIBStringField;
    taCardM: TdxMemData;
    taCardMIdDoc: TIntegerField;
    taCardMTypeD: TIntegerField;
    taCardMDateD: TDateField;
    taCardMsFrom: TStringField;
    taCardMsTo: TStringField;
    taCardMiM: TIntegerField;
    taCardMsM: TStringField;
    taCardMQuant: TFloatField;
    taCardMQRemn: TFloatField;
    taCardMComment: TStringField;
    dstaCardM: TDataSource;
    quTestInput: TpFIBDataSet;
    dsquTestInput: TDataSource;
    quTestInputID: TFIBIntegerField;
    quTestInputDATEDOC: TFIBDateField;
    quTestInputNUMDOC: TFIBStringField;
    quTestInputIDCLI: TFIBIntegerField;
    quTestInputIDSKL: TFIBIntegerField;
    quTestInputSUMIN: TFIBFloatField;
    quTestInputIACTIVE: TFIBIntegerField;
    quTestInputOPER: TFIBStringField;
    quTestInputCOMMENT: TFIBStringField;
    quTestInputNAMEMH: TFIBStringField;
    quSpecVnSelCATEGORY: TFIBIntegerField;
    taTOReprSumPr: TFloatField;
    taTORepPriceType: TStringField;
    taTORepsSumPr: TStringField;
    quDocsComplIDSKLTO: TFIBIntegerField;
    quDocsComplIDFROM: TFIBIntegerField;
    quDocsComplTOREAL: TFIBSmallIntField;
    quDocsComplNAMEMH1: TFIBStringField;
    quDocsComlRecIDFROM: TFIBIntegerField;
    quDocsComlRecTOREAL: TFIBSmallIntField;
    quDocsComlRecIDSKLTO: TFIBIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
function fRecalcSumDoc(Idh,dType,ResT:Integer):Real;
procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch:Real);
procedure prFind2group(IdCl:Integer;Var S1,S2:String);
function prFindMassaTC(IdCard:Integer):String;
function FindTSpis(sOp:String):Integer;
Function prFindMainGroup(Id:Integer;Var S1:String):Integer;

var
  dmORep: TdmORep;


implementation

uses dmOffice, TOSel, Un1;

{$R *.dfm}

Function prFindMainGroup(Id:Integer;Var S1:String):Integer;
Var IdPar,IdPost:INteger;
begin
  with dmORep do
  begin
    S1:=''; IdPar:=Id;
    IdPost:=IdPar;
    while IdPar<>0 do
    begin
      quFindCl.Active:=False;
      quFindCl.ParamByName('IDCL').AsInteger:=IdPar;
      quFindCl.Active:=True;
      S1:=Copy(quFindClNAMECL.AsString,1,50);
      IdPost:=IdPar;
      IdPar:=quFindClID_PARENT.AsInteger;
      quFindCl.Active:=False;
    end;
  end;
  Result:=IdPost;
end;


function FindTSpis(sOp:String):Integer;
begin
  Result:=1;
  with dmORep do
  begin
    quFindTSpis.Active:=False;
    quFindTSpis.ParamByName('ABR').AsString:=sOp;
    quFindTSpis.Active:=True;

    if quFindTSpis.RecordCount>0 then Result:=quFindTSpisTSPIS.AsInteger;

    quFindTSpis.Active:=False;
  end;
end;


function prFindMassaTC(IdCard:Integer):String;
begin
  Result:='';
  with dmORep do
  begin
    quFindMassa.Active:=False;
    quFindMassa.ParamByName('IDCARD').AsInteger:=IdCard;
    quFindMassa.Active:=True;

    if quFindMassa.RecordCount>0 then Result:=quFindMassaPOUTPUT.AsString;

    quFindMassa.Active:=False;
  end;
end;

procedure prFind2group(IdCl:Integer;Var S1,S2:String);
Var Id:INteger;
begin
  with dmORep do
  begin
    S1:=''; S2:=''; Id:=IdCl;
    while iD<>0 do
    begin
      quFindCl.Active:=False;
      quFindCl.ParamByName('IDCL').AsInteger:=Id;
      quFindCl.Active:=True;
      S2:=S1;
      S1:=Copy(quFindClNAMECL.AsString,1,50);
      Id:=quFindClID_PARENT.AsInteger;
      quFindCl.Active:=False;
    end;
  end;
end;  

procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch:Real);
begin
  with dmORep do
  begin
//    EXECUTE PROCEDURE PR_CALCLASTPRICE2 (?IDGOOD, ?IDSKL, ?IDATE)
    pr_CalcLastPrice.ParamByName('IDGOOD').AsInteger:=IdCard;
    pr_CalcLastPrice.ParamByName('IDSKL').AsInteger:=IdSkl;
    pr_CalcLastPrice.ParamByName('IDATE').AsInteger:=IDate;
    pr_CalcLastPrice.ExecProc;
    PriceIn:=pr_CalcLastPrice.ParamByName('PRICEIN').AsFloat;
    PriceUch:=pr_CalcLastPrice.ParamByName('PRICEUCH').AsFloat;

    //���� � ������� ��
  end;
end;

function fRecalcSumDoc(Idh,dType,ResT:Integer):Real;
begin
  Result:=0;
  with dmORep do
  with dmO do
  begin
    if ResT=2 then
    begin //����������� ������ � ����������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
    if ResT=1 then //����������� �� ����������� ����� �� �������������
    begin
      if dType=1 then //������
      begin //������ �� ����� �.�. ��� ������������ ������������ � ��������� �������
            { ����� ���� ��������� ���������� ������� � ���������� ���-��� � �� ����� ����
            , � ��� ��� ��������� ������ �� ������ ������� ��� �� � ���������� ����������� ������.
            ���� ���� ������� � 0 � ���-��, � -900 � �����, � 0=KM
            ����� �������� ������ �������� �������� � �����������
            }
      end;
      if dType=2 then //����������
      begin

      end;
      if dType=4 then //�����
      begin

      end;
      if dType=5 then //��� �����������
      begin

      end;
      if dType=6 then //������������
      begin

      end;
      if dType=7 then //�������
      begin
  {      quSpecOutSel.Active:=False;
        quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecOutSel.Active:=True;

        quSpecOutSel.First;
        while not quSpecOutSel.Eof do
        begin



          quSpecOutSel.Next;
        end;
        quSpecOutSel.Active:=False;
        }
      end;


      //��� ���� �� ������ ������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
  end;
end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
begin
  with dmORep do
  begin
    prTestSpecPart.ParamByName('IDDOC').AsInteger:=IDH;
    prTestSpecPart.ParamByName('DTYPE').AsInteger:=dType;
    prTestSpecPart.ParamByName('DSUM').AsFloat:=rSum;
    prTestSpecPart.ExecProc;
    Result:=prTestSpecPart.ParamByName('RESULT').AsInteger;
  end;
end;


end.
