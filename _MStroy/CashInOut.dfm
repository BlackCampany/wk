object fmCashInOut: TfmCashInOut
  Left = 385
  Top = 215
  BorderStyle = bsDialog
  Caption = #1048#1079#1098#1103#1090#1080#1077' '#1076#1077#1085#1077#1075'.'
  ClientHeight = 155
  ClientWidth = 317
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 96
    Width = 317
    Height = 59
    Align = alBottom
    BevelInner = bvLowered
    Color = 6618980
    TabOrder = 0
    object Button1: TcxButton
      Left = 24
      Top = 6
      Width = 97
      Height = 45
      Caption = #1048#1079#1098#1103#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Default = 8454016
      Colors.Normal = 10485663
      Colors.Pressed = clGreen
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 192
      Top = 6
      Width = 97
      Height = 45
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      Colors.Default = 8454016
      Colors.Normal = 10485663
      Colors.Pressed = clGreen
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
  object cxLabel1: TcxLabel
    Left = 12
    Top = 20
    Caption = #1057#1091#1084#1084#1072' '#1085#1072#1083#1080#1095#1085#1099#1093' '#1074' '#1082#1072#1089#1089#1077
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxLabel2: TcxLabel
    Left = 12
    Top = 56
    Caption = #1057#1091#1084#1084#1072' '#1082' '#1080#1079#1098#1103#1090#1080#1102
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 180
    Top = 18
    TabStop = False
    EditValue = 1000.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 3
    Width = 105
  end
  object cxCurrencyEdit2: TcxCurrencyEdit
    Left = 180
    Top = 54
    TabStop = False
    EditValue = 500.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 4
    OnClick = cxCurrencyEdit2Click
    Width = 105
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 6618980
    BkColor.EndColor = 59904
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 32
    Top = 4
  end
end
