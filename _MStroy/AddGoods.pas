unit AddGoods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxCheckBox, cxGraphics, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCalc, DBClient, cxImageComboBox,
  cxCheckListBox, cxCurrencyEdit;

type
  TfmAddGood = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxCheckBox2: TcxCheckBox;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    Timer1: TTimer;
    Label9: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    Label11: TLabel;
    cxCEdit1: TcxCurrencyEdit;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label6: TLabel;
    cxCEdit2: TcxCurrencyEdit;
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddGood: TfmAddGood;
  bClearAddG:Boolean = False;

implementation

uses dmOffice, Un1, Goods;

{$R *.dfm}

procedure TfmAddGood.Timer1Timer(Sender: TObject);
begin
  if bClearAddG=True then begin StatusBar1.Panels[0].Text:=''; bClearAddG:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearAddG:=True;
end;


procedure TfmAddGood.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bAddG:=False;
end;

procedure TfmAddGood.FormShow(Sender: TObject);
begin
  if bAddG then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else
  begin
    cxCEdit1.SetFocus;
    cxCEdit1.SelectAll;
  end;
end;

end.
