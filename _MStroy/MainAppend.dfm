object fmMainAppend: TfmMainAppend
  Left = 678
  Top = 115
  BorderStyle = bsDialog
  Caption = 'Appender'
  ClientHeight = 447
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 321
    Height = 345
    Align = alTop
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object cxButton1: TcxButton
    Left = 200
    Top = 408
    Width = 99
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 1
    OnClick = cxButton1Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object ProgressBar1: TProgressBar
    Left = 16
    Top = 412
    Width = 150
    Height = 16
    Position = 5
    TabOrder = 2
    Visible = False
  end
  object cxButton2: TcxButton
    Left = 200
    Top = 368
    Width = 99
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100
    TabOrder = 3
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxDateEdit1: TcxDateEdit
    Left = 16
    Top = 370
    TabOrder = 4
    Width = 145
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 256
    Top = 16
  end
  object dmC: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA'
      'user_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 0
    Left = 32
    Top = 16
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 88
    Top = 16
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 144
    Top = 16
  end
  object taCheck: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'ACheck.DB'
    Left = 104
    Top = 88
    object taCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taCheckLogicDate: TDateField
      FieldName = 'LogicDate'
    end
    object taCheckRealDate: TDateField
      FieldName = 'RealDate'
    end
    object taCheckOpenTime: TStringField
      FieldName = 'OpenTime'
      Size = 5
    end
    object taCheckCloseTime: TStringField
      FieldName = 'CloseTime'
      Size = 5
    end
    object taCheckCover: TSmallintField
      FieldName = 'Cover'
    end
    object taCheckCashier: TSmallintField
      FieldName = 'Cashier'
    end
    object taCheckWaiter: TSmallintField
      FieldName = 'Waiter'
    end
    object taCheckUnit: TStringField
      FieldName = 'Unit'
      Size = 2
    end
    object taCheckDepart: TStringField
      FieldName = 'Depart'
      Size = 2
    end
    object taCheckTotal: TFloatField
      FieldName = 'Total'
    end
    object taCheckBaseKurs: TFloatField
      FieldName = 'BaseKurs'
    end
    object taCheckDeleted: TSmallintField
      FieldName = 'Deleted'
    end
    object taCheckManager: TSmallintField
      FieldName = 'Manager'
    end
    object taCheckCharge: TFloatField
      FieldName = 'Charge'
    end
    object taCheckTable: TStringField
      FieldName = 'Table'
      Size = 4
    end
    object taCheckOpenDate: TDateField
      FieldName = 'OpenDate'
    end
    object taCheckCount: TSmallintField
      FieldName = 'Count'
    end
    object taCheckNacKurs: TFloatField
      FieldName = 'NacKurs'
    end
    object taCheckTotalR: TFloatField
      FieldName = 'TotalR'
    end
    object taCheckCoverR: TSmallintField
      FieldName = 'CoverR'
    end
    object taCheckTaxSum: TFloatField
      FieldName = 'TaxSum'
    end
    object taCheckTaxSumR: TFloatField
      FieldName = 'TaxSumR'
    end
    object taCheckTaxRate: TFloatField
      FieldName = 'TaxRate'
    end
    object taCheckTaxRateR: TFloatField
      FieldName = 'TaxRateR'
    end
    object taCheckBonus: TFloatField
      FieldName = 'Bonus'
    end
    object taCheckBonusCard: TFloatField
      FieldName = 'BonusCard'
    end
  end
  object Session1: TSession
    Active = True
    AutoSessionName = True
    NetFileDir = 'C:\'
    Left = 32
    Top = 88
  end
  object taRCheck: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'ARcheck.DB'
    Left = 168
    Top = 88
    object taRCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taRCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taRCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taRCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taRCheckQnt: TFloatField
      FieldName = 'Qnt'
    end
    object taRCheckPrice: TFloatField
      FieldName = 'Price'
    end
    object taRCheckComp: TStringField
      FieldName = 'Comp'
      Size = 1
    end
    object taRCheckRealPrice: TFloatField
      FieldName = 'RealPrice'
    end
    object taRCheckQntR: TFloatField
      FieldName = 'QntR'
    end
    object taRCheckNalog: TFloatField
      FieldName = 'Nalog'
    end
  end
  object taDCheck: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'ADCheck.DB'
    Left = 240
    Top = 88
    object taDCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taDCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taDCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taDCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taDCheckSum: TFloatField
      FieldName = 'Sum'
    end
    object taDCheckCardCod: TFloatField
      FieldName = 'CardCod'
    end
    object taDCheckPerson: TSmallintField
      FieldName = 'Person'
    end
    object taDCheckSumR: TFloatField
      FieldName = 'SumR'
    end
  end
  object taPCheck: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'APcheck.DB'
    Left = 104
    Top = 152
    object taPCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taPCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taPCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taPCheckCurency: TSmallintField
      FieldName = 'Curency'
    end
    object taPCheckBaseSumEqw: TFloatField
      FieldName = 'BaseSumEqw'
    end
    object taPCheckOriginalSum: TFloatField
      FieldName = 'OriginalSum'
    end
    object taPCheckKurs: TFloatField
      FieldName = 'Kurs'
    end
    object taPCheckDisc: TFloatField
      FieldName = 'Disc'
    end
    object taPCheckExtra: TStringField
      FieldName = 'Extra'
    end
    object taPCheckBaseSumR: TFloatField
      FieldName = 'BaseSumR'
    end
    object taPCheckOrigSumR: TFloatField
      FieldName = 'OrigSumR'
    end
    object taPCheckIsTax: TSmallintField
      FieldName = 'IsTax'
    end
  end
  object taVCheck: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'AVCheck.DB'
    Left = 168
    Top = 152
    object taVCheckUNI: TAutoIncField
      FieldName = 'UNI'
      ReadOnly = True
    end
    object taVCheckLogicDate: TDateField
      FieldName = 'LogicDate'
    end
    object taVCheckRealDate: TDateField
      FieldName = 'RealDate'
    end
    object taVCheckTime: TStringField
      FieldName = 'Time'
      Size = 5
    end
    object taVCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taVCheckComp: TSmallintField
      FieldName = 'Comp'
    end
    object taVCheckQnt: TFloatField
      FieldName = 'Qnt'
    end
    object taVCheckPrice: TFloatField
      FieldName = 'Price'
    end
    object taVCheckReason: TSmallintField
      FieldName = 'Reason'
    end
    object taVCheckManager: TSmallintField
      FieldName = 'Manager'
    end
    object taVCheckWaiter: TSmallintField
      FieldName = 'Waiter'
    end
    object taVCheckTable: TStringField
      FieldName = 'Table'
      Size = 4
    end
    object taVCheckUnit: TStringField
      FieldName = 'Unit'
      Size = 2
    end
    object taVCheckDepart: TStringField
      FieldName = 'Depart'
      Size = 2
    end
    object taVCheckTabNum: TFloatField
      FieldName = 'TabNum'
    end
  end
  object taCashSail: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    C.CASHNUM,C.ZNUM,C.CHECKNUM,C.TAB_ID,C.CASHERID,C.WAITERID,'
      '    T.NUMTABLE,T.QUESTS,T.DISCONT,'
      
        '    SUM(C.TABSUM) as TABSUM,MAX(C.CHDATE) as CHDATE,MAX(T.BEGTIM' +
        'E) as BEGTIME, max(C.ID) as ID'
      'FROM'
      '    CASHSAIL C'
      ''
      'LEFT JOIN TABLES_ALL T ON T.ID=C.TAB_ID'
      ''
      
        'WHERE C.CHDATE>=:DBEG and C.CHDATE<:DEND and (C.TABSUM>=0.01 or ' +
        'C.TABSUM<=-0.01)'
      ''
      
        'group by C.CASHNUM,C.ZNUM,C.CHECKNUM,C.TAB_ID,C.CASHERID,C.WAITE' +
        'RID,'
      '    T.NUMTABLE,T.QUESTS,T.DISCONT'
      '         '
      'ORDER BY C.CASHNUM,C.ZNUM,C.CHECKNUM')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 24
    Top = 288
    poAskRecordCount = True
    object taCashSailCASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object taCashSailZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object taCashSailCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object taCashSailTAB_ID: TFIBIntegerField
      FieldName = 'TAB_ID'
    end
    object taCashSailCASHERID: TFIBIntegerField
      FieldName = 'CASHERID'
    end
    object taCashSailWAITERID: TFIBIntegerField
      FieldName = 'WAITERID'
    end
    object taCashSailNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taCashSailQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object taCashSailDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taCashSailTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object taCashSailCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object taCashSailBEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
    end
    object taCashSailID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer2Timer
    Left = 256
    Top = 160
  end
  object taSpec: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID_TAB,'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    SIFR,'
      '    PRICE,'
      '    QUANTITY,'
      '    DISCOUNTPROC,'
      '    DISCOUNTSUM,'
      '    SUMMA,'
      '    ISTATUS,'
      '    ITYPE'
      'FROM'
      '    SPEC_ALL '
      'WHERE'
      '    ID_TAB=:IDHEAD'
      ''
      'ORDER BY ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 88
    Top = 288
    object taSpecID_TAB: TFIBIntegerField
      FieldName = 'ID_TAB'
    end
    object taSpecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taSpecID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taSpecNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taSpecSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taSpecPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taSpecQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taSpecDISCOUNTPROC: TFIBFloatField
      FieldName = 'DISCOUNTPROC'
    end
    object taSpecDISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object taSpecSUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object taSpecISTATUS: TFIBIntegerField
      FieldName = 'ISTATUS'
    end
    object taSpecITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
  end
  object taDel: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD'
      'FROM'
      '    TABLES_ALL '
      ''
      'where ENDTIME>=:DBEG and ENDTIME<:DEND'
      'and OPERTYPE='#39'Del'#39
      ''
      'order by ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 136
    Top = 272
    object taDelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDelID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taDelNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taDelQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object taDelTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object taDelBEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
    end
    object taDelENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taDelDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taDelOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object taDelCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object taDelSKLAD: TFIBSmallIntField
      FieldName = 'SKLAD'
    end
  end
  object taMenu: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'Menu.DB'
    Left = 24
    Top = 216
    object taMenuSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taMenuName: TStringField
      FieldName = 'Name'
      Size = 25
    end
    object taMenuPrice: TFloatField
      FieldName = 'Price'
    end
    object taMenuCode: TStringField
      FieldName = 'Code'
      Size = 4
    end
    object taMenuTreeType: TStringField
      FieldName = 'TreeType'
      Size = 1
    end
    object taMenuLimitPrice: TFloatField
      FieldName = 'LimitPrice'
    end
    object taMenuCateg: TSmallintField
      FieldName = 'Categ'
    end
    object taMenuParent: TSmallintField
      FieldName = 'Parent'
    end
    object taMenuLink: TSmallintField
      FieldName = 'Link'
    end
    object taMenuStream: TSmallintField
      FieldName = 'Stream'
    end
    object taMenuLack: TSmallintField
      FieldName = 'Lack'
    end
    object taMenuDesignSifr: TSmallintField
      FieldName = 'DesignSifr'
    end
    object taMenuAltName: TStringField
      FieldName = 'AltName'
      Size = 25
    end
    object taMenuNalog: TFloatField
      FieldName = 'Nalog'
    end
    object taMenuBarcode: TStringField
      FieldName = 'Barcode'
      Size = 13
    end
    object taMenuImage: TSmallintField
      FieldName = 'Image'
    end
    object taMenuConsumma: TFloatField
      FieldName = 'Consumma'
    end
    object taMenuMinRest: TSmallintField
      FieldName = 'MinRest'
    end
    object taMenuPrnRest: TSmallintField
      FieldName = 'PrnRest'
    end
    object taMenuCookTime: TSmallintField
      FieldName = 'CookTime'
    end
    object taMenuDispenser: TSmallintField
      FieldName = 'Dispenser'
    end
    object taMenuDispKoef: TSmallintField
      FieldName = 'DispKoef'
    end
    object taMenuAccess: TSmallintField
      FieldName = 'Access'
    end
  end
  object taMenuDel: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'Menu_.DB'
    Left = 88
    Top = 216
    object taMenuDelSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taMenuDelName: TStringField
      FieldName = 'Name'
      Size = 25
    end
    object taMenuDelCode: TStringField
      FieldName = 'Code'
      Size = 4
    end
    object taMenuDelCateg: TSmallintField
      FieldName = 'Categ'
    end
    object taMenuDelReplace: TSmallintField
      FieldName = 'Replace'
    end
  end
  object taMod: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'Modyfy.DB'
    Left = 152
    Top = 216
    object taModSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taModName: TStringField
      FieldName = 'Name'
      Size = 25
    end
    object taModParent: TSmallintField
      FieldName = 'Parent'
    end
    object taModPrice: TFloatField
      FieldName = 'Price'
    end
    object taModRealPrice: TFloatField
      FieldName = 'RealPrice'
    end
  end
  object quMenu: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    SIFR,'
      '    NAME,'
      '    PRICE,'
      '    CODE,'
      '    TREETYPE,'
      '    LIMITPRICE,'
      '    CATEG,'
      '    PARENT,'
      '    LINK,'
      '    STREAM,'
      '    LACK,'
      '    DESIGNSIFR,'
      '    ALTNAME,'
      '    NALOG,'
      '    BARCODE,'
      '    IMAGE,'
      '    CONSUMMA,'
      '    MINREST,'
      '    PRNREST,'
      '    COOKTIME,'
      '    DISPENSER,'
      '    DISPKOEF,'
      '    ACCESS,'
      '    FLAGS,'
      '    TARA,'
      '    CNTPRICE,'
      '    BACKBGR,'
      '    FONTBGR,'
      '    IACTIVE,'
      '    IEDIT'
      'FROM'
      '    MENU ')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 216
    Top = 288
    object quMenuSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quMenuNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quMenuPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quMenuCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quMenuTREETYPE: TFIBStringField
      FieldName = 'TREETYPE'
      Size = 1
      EmptyStrToNull = True
    end
    object quMenuLIMITPRICE: TFIBFloatField
      FieldName = 'LIMITPRICE'
    end
    object quMenuCATEG: TFIBSmallIntField
      FieldName = 'CATEG'
    end
    object quMenuPARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object quMenuLINK: TFIBSmallIntField
      FieldName = 'LINK'
    end
    object quMenuSTREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object quMenuLACK: TFIBSmallIntField
      FieldName = 'LACK'
    end
    object quMenuDESIGNSIFR: TFIBSmallIntField
      FieldName = 'DESIGNSIFR'
    end
    object quMenuALTNAME: TFIBStringField
      FieldName = 'ALTNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quMenuNALOG: TFIBFloatField
      FieldName = 'NALOG'
    end
    object quMenuBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object quMenuIMAGE: TFIBSmallIntField
      FieldName = 'IMAGE'
    end
    object quMenuCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quMenuMINREST: TFIBSmallIntField
      FieldName = 'MINREST'
    end
    object quMenuPRNREST: TFIBSmallIntField
      FieldName = 'PRNREST'
    end
    object quMenuCOOKTIME: TFIBSmallIntField
      FieldName = 'COOKTIME'
    end
    object quMenuDISPENSER: TFIBSmallIntField
      FieldName = 'DISPENSER'
    end
    object quMenuDISPKOEF: TFIBSmallIntField
      FieldName = 'DISPKOEF'
    end
    object quMenuACCESS: TFIBSmallIntField
      FieldName = 'ACCESS'
    end
    object quMenuFLAGS: TFIBSmallIntField
      FieldName = 'FLAGS'
    end
    object quMenuTARA: TFIBSmallIntField
      FieldName = 'TARA'
    end
    object quMenuCNTPRICE: TFIBSmallIntField
      FieldName = 'CNTPRICE'
    end
    object quMenuBACKBGR: TFIBFloatField
      FieldName = 'BACKBGR'
    end
    object quMenuFONTBGR: TFIBFloatField
      FieldName = 'FONTBGR'
    end
    object quMenuIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quMenuIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
  end
  object quMod: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    SIFR,'
      '    NAME,'
      '    PARENT,'
      '    PRICE,'
      '    REALPRICE,'
      '    IACTIVE,'
      '    IEDIT'
      'FROM'
      '    MODIFY ')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 280
    Top = 288
    object quModSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quModNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quModPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quModPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quModREALPRICE: TFIBFloatField
      FieldName = 'REALPRICE'
    end
    object quModIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quModIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
  end
  object taCateg: TTable
    DatabaseName = 'C:\_CasherRn\BIN\RkData\'
    SessionName = 'Session1_2'
    TableName = 'Categ.DB'
    Left = 216
    Top = 216
    object taCategSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taCategName: TStringField
      FieldName = 'Name'
      Size = 15
    end
    object taCategImage: TGraphicField
      FieldName = 'Image'
      BlobType = ftGraphic
      Size = 200
    end
    object taCategTaxGroup: TSmallintField
      FieldName = 'TaxGroup'
    end
  end
  object quCateg: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    SIFR,'
      '    NAME,'
      '    TAX_GROUP,'
      '    IACTIVE,'
      '    IEDIT'
      'FROM'
      '    CATEGORY ')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 280
    Top = 232
    object quCategSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quCategNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quCategTAX_GROUP: TFIBIntegerField
      FieldName = 'TAX_GROUP'
    end
    object quCategIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quCategIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
  end
  object taFis: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT sp.ID_TAB,sp.ID,sp.NUMTABLE,sp.SIFR,sp.PRICE,sp.QUANTITY,' +
        'sp.DISCOUNTPROC,'
      
        '       sp.DISCOUNTSUM,sp.SUMMA,sp.ITYPE,sp.QUANTITY1,hd.ENDTIME,' +
        'sp.ID_PERSONAL'
      'FROM SPEC_ALL sp'
      'left join TABLES_ALL hd on hd.ID=sp.ID_TAB'
      'where sp.INEED=1'
      'and sp.ID_TAB=:IDHEAD'
      '')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 24
    Top = 160
    poAskRecordCount = True
    object taFisID_TAB: TFIBIntegerField
      FieldName = 'ID_TAB'
    end
    object taFisID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taFisNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taFisSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taFisPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taFisQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taFisDISCOUNTPROC: TFIBFloatField
      FieldName = 'DISCOUNTPROC'
    end
    object taFisDISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object taFisSUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object taFisITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taFisQUANTITY1: TFIBFloatField
      FieldName = 'QUANTITY1'
    end
    object taFisENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taFisID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
  end
  object taPC: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD'
      'FROM'
      '    TABLES_ALL '
      ''
      'where ENDTIME>=:DBEG and ENDTIME<:DEND'
      'and OPERTYPE='#39'SalePC'#39
      ''
      'order by ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 168
    Top = 312
    object taPCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taPCID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taPCNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taPCQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object taPCTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object taPCBEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
    end
    object taPCENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taPCDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taPCOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object taPCCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object taPCSKLAD: TFIBSmallIntField
      FieldName = 'SKLAD'
    end
  end
end
