unit dmOffice;

interface

uses
  SysUtils, Classes, frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl,
  FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT, FIBQuery,
  pFIBQuery, pFIBStoredProc, DB, FIBDataSet, ImgList, Controls, cxStyles,
  pFIBDataSet, FIBDatabase, pFIBDatabase,DBClient,cxMemo,Variants,dxmdaset,
  Dialogs;

type
  TdmO = class(TDataModule)
    OfficeRnDb: TpFIBDatabase;
    //trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxRepos1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    imState: TImageList;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    prGetId: TpFIBStoredProc;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quMessTree: TpFIBDataSet;
    taMess: TpFIBDataSet;
    taMessID: TFIBIntegerField;
    taMessID_PARENT: TFIBIntegerField;
    taMessNAMESHORT: TFIBStringField;
    taMessNAMEFULL: TFIBStringField;
    taMessKOEF: TFIBFloatField;
    prCanDelMess: TpFIBStoredProc;
    quClassTree: TpFIBDataSet;
    taClass: TpFIBDataSet;
    taClassID: TFIBIntegerField;
    taClassID_PARENT: TFIBIntegerField;
    taClassITYPE: TFIBSmallIntField;
    taClassNAMECL: TFIBStringField;
    prCanDelClass: TpFIBStoredProc;
    taPriceT: TpFIBDataSet;
    taPriceTID: TFIBIntegerField;
    taPriceTNAMEPRICE: TFIBStringField;
    dsPriceT: TDataSource;
    quMHTree: TpFIBDataSet;
    taMH: TpFIBDataSet;
    taMHID: TFIBIntegerField;
    taMHPARENT: TFIBIntegerField;
    taMHITYPE: TFIBIntegerField;
    taMHNAMEMH: TFIBStringField;
    taMHDEFPRICE: TFIBIntegerField;
    quMHSel: TpFIBDataSet;
    quMHSelID: TFIBIntegerField;
    quMHSelPARENT: TFIBIntegerField;
    quMHSelITYPE: TFIBIntegerField;
    quMHSelNAMEMH: TFIBStringField;
    quMHSelDEFPRICE: TFIBIntegerField;
    dsMHSel: TDataSource;
    quMHSelNAMEPRICE: TFIBStringField;
    quMHParent: TpFIBDataSet;
    quPriceTSel: TpFIBDataSet;
    quPriceTSelID: TFIBIntegerField;
    quPriceTSelNAMEPRICE: TFIBStringField;
    dsPriceTSel: TDataSource;
    quCardsSel: TpFIBDataSet;
    dsCardsSel: TDataSource;
    quCardsSelID: TFIBIntegerField;
    quCardsSelPARENT: TFIBIntegerField;
    quCardsSelNAME: TFIBStringField;
    quCardsSelTTYPE: TFIBIntegerField;
    quCardsSelIMESSURE: TFIBIntegerField;
    quCardsSelINDS: TFIBIntegerField;
    quCardsSelMINREST: TFIBFloatField;
    quCardsSelLASTPRICEIN: TFIBFloatField;
    quCardsSelLASTPRICEOUT: TFIBFloatField;
    quCardsSelLASTPOST: TFIBIntegerField;
    quCardsSelIACTIVE: TFIBIntegerField;
    quCardsSelNAMESHORT: TFIBStringField;
    quCardsSelNAMENDS: TFIBStringField;
    quCardsSelPROC: TFIBFloatField;
    taNDS: TpFIBDataSet;
    taNDSID: TFIBIntegerField;
    taNDSNAMENDS: TFIBStringField;
    taNDSPROC: TFIBFloatField;
    dsMess: TDataSource;
    dsNDS: TDataSource;
    quGdsFind: TpFIBDataSet;
    quBarFind: TpFIBDataSet;
    quBarFindBAR: TFIBStringField;
    quBarFindGOODSID: TFIBIntegerField;
    quBars: TpFIBDataSet;
    quBarsBAR: TFIBStringField;
    quBarsGOODSID: TFIBIntegerField;
    quBarsQUANT: TFIBFloatField;
    quBarsBARFORMAT: TFIBSmallIntField;
    quBarsPRICE: TFIBFloatField;
    trUpdCards: TpFIBTransaction;
    trUpdBars: TpFIBTransaction;
    quGoodsEU: TpFIBDataSet;
    quGoodsEUGOODSID: TFIBIntegerField;
    quGoodsEUIDATEB: TFIBIntegerField;
    quGoodsEUIDATEE: TFIBIntegerField;
    quGoodsEUTO100GRAMM: TFIBFloatField;
    dsGoodsEU: TDataSource;
    trUpdEU: TpFIBTransaction;
    quFind: TpFIBDataSet;
    dsFind: TDataSource;
    quFindID: TFIBIntegerField;
    quFindPARENT: TFIBIntegerField;
    quFindNAME: TFIBStringField;
    taClients: TpFIBDataSet;
    taClientsID: TFIBIntegerField;
    taClientsNAMECL: TFIBStringField;
    taClientsFULLNAMECL: TFIBStringField;
    taClientsINN: TFIBStringField;
    taClientsPHONE: TFIBStringField;
    taClientsMOL: TFIBStringField;
    taClientsCOMMENT: TFIBStringField;
    taClientsINDS: TFIBIntegerField;
    taClientsIACTIVE: TFIBSmallIntField;
    dsClients: TDataSource;
    cxStyle26: TcxStyle;
    trDocsSel: TpFIBTransaction;
    trDocsUpd: TpFIBTransaction;
    quCardsSelTCARD: TFIBIntegerField;
    quFindEU: TpFIBDataSet;
    quFindEUGOODSID: TFIBIntegerField;
    quFindEUIDATEB: TFIBIntegerField;
    quFindEUIDATEE: TFIBIntegerField;
    quFindEUTO100GRAMM: TFIBFloatField;
    trSElMessure: TpFIBTransaction;
    quMessureSel: TpFIBDataSet;
    dsMessureSel: TDataSource;
    quMessureSelID: TFIBIntegerField;
    quMessureSelID_PARENT: TFIBIntegerField;
    quMessureSelNAMESHORT: TFIBStringField;
    quMessureSelKOEF: TFIBFloatField;
    taClientsKPP: TFIBStringField;
    taClientsADDRES: TFIBStringField;
    quCardsSel1: TpFIBDataSet;
    dsCardsSel1: TDataSource;
    quCardsSel1ID: TFIBIntegerField;
    quCardsSel1PARENT: TFIBIntegerField;
    quCardsSel1NAME: TFIBStringField;
    quCardsSel1TTYPE: TFIBIntegerField;
    quCardsSel1IMESSURE: TFIBIntegerField;
    quCardsSel1INDS: TFIBIntegerField;
    quCardsSel1MINREST: TFIBFloatField;
    quCardsSel1LASTPRICEIN: TFIBFloatField;
    quCardsSel1LASTPRICEOUT: TFIBFloatField;
    quCardsSel1LASTPOST: TFIBIntegerField;
    quCardsSel1IACTIVE: TFIBIntegerField;
    quCardsSel1TCARD: TFIBIntegerField;
    quCardsSel1NAMESHORT: TFIBStringField;
    quCardsSel1NAMENDS: TFIBStringField;
    quCardsSel1PROC: TFIBFloatField;
    quS: TpFIBDataSet;
    quSTCARD: TFIBIntegerField;
    quST: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    trSel: TpFIBTransaction;
    prDelPart: TpFIBStoredProc;
    prAddPartIn: TpFIBStoredProc;
    prFindPartOut: TpFIBStoredProc;
    trSel1: TpFIBTransaction;
    prPartInDel: TpFIBStoredProc;
    taParams: TpFIBDataSet;
    taParamsID: TFIBIntegerField;
    taParamsIDATEB: TFIBIntegerField;
    taParamsIDATEE: TFIBIntegerField;
    taParamsIDSTORE: TFIBIntegerField;
    prCalcLastPrice: TpFIBStoredProc;
    prAddPartOut: TpFIBStoredProc;
    prDelPartOut: TpFIBStoredProc;
    prCalcLastPrice1: TpFIBStoredProc;
    quFindCard: TpFIBDataSet;
    quFindCardID: TFIBIntegerField;
    quFindCardPARENT: TFIBIntegerField;
    quFindCardNAME: TFIBStringField;
    quFindCardTTYPE: TFIBIntegerField;
    quFindCardIMESSURE: TFIBIntegerField;
    quFindCardINDS: TFIBIntegerField;
    quFindCardMINREST: TFIBFloatField;
    quFindCardLASTPRICEIN: TFIBFloatField;
    quFindCardLASTPRICEOUT: TFIBFloatField;
    quFindCardLASTPOST: TFIBIntegerField;
    quFindCardIACTIVE: TFIBIntegerField;
    quFindCardTCARD: TFIBIntegerField;
    quFindTCard: TpFIBDataSet;
    quFindTCardID: TFIBIntegerField;
    quFindTCardSHORTNAME: TFIBStringField;
    quFindTCardRECEIPTNUM: TFIBStringField;
    quFindTCardPOUTPUT: TFIBStringField;
    quFindTCardPCOUNT: TFIBIntegerField;
    quFindTCardPVES: TFIBFloatField;
    trSel2: TpFIBTransaction;
    quMHAll: TpFIBDataSet;
    quMHAllID: TFIBIntegerField;
    quMHAllPARENT: TFIBIntegerField;
    quMHAllITYPE: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    quMHAllDEFPRICE: TFIBIntegerField;
    quMHAllNAMEPRICE: TFIBStringField;
    dsMHAll: TDataSource;
    taMessITYPE: TFIBSmallIntField;
    taCateg: TpFIBDataSet;
    taCategID: TFIBIntegerField;
    taCategNAMECAT: TFIBStringField;
    dsCateg: TDataSource;
    quCardsSelCATEGORY: TFIBIntegerField;
    quCardsSel1CATEGORY: TFIBIntegerField;
    quTODel: TpFIBQuery;
    quSelCardsCat: TpFIBDataSet;
    quSelCardsCatCATEGORY: TFIBIntegerField;
    quRemnDate: TpFIBDataSet;
    quCPartIn: TpFIBDataSet;
    quCPartOut: TpFIBDataSet;
    dsquCPartIn: TDataSource;
    dsquCPartOut: TDataSource;
    quCPartInID: TFIBIntegerField;
    quCPartInIDSTORE: TFIBIntegerField;
    quCPartInNAMEMH: TFIBStringField;
    quCPartInIDDOC: TFIBIntegerField;
    quCPartInARTICUL: TFIBIntegerField;
    quCPartInIDCLI: TFIBIntegerField;
    quCPartInNAMECL: TFIBStringField;
    quCPartInDTYPE: TFIBIntegerField;
    quCPartInQPART: TFIBFloatField;
    quCPartInQREMN: TFIBFloatField;
    quCPartInPRICEIN: TFIBFloatField;
    quCPartInPRICEOUT: TFIBFloatField;
    quCPartInIDATE: TFIBIntegerField;
    quCPartOutARTICUL: TFIBIntegerField;
    quCPartOutIDDATE: TFIBIntegerField;
    quCPartOutIDSTORE: TFIBIntegerField;
    quCPartOutNAMEMH: TFIBStringField;
    quCPartOutIDPARTIN: TFIBIntegerField;
    quCPartOutIDDOC: TFIBIntegerField;
    quCPartOutIDCLI: TFIBIntegerField;
    quCPartOutNAMECL: TFIBStringField;
    quCPartOutDTYPE: TFIBIntegerField;
    quCPartOutQUANT: TFIBFloatField;
    quCPartOutPRICEIN: TFIBFloatField;
    quCPartOutSUMOUT: TFIBFloatField;
    quCPartInSUMIN: TFIBFloatField;
    quCPartInSUMREMN: TFIBFloatField;
    quCPartOutSUMIN: TFIBFloatField;
    quFindUse: TpFIBDataSet;
    quFindUseMAXDATE: TFIBDateField;
    quFCard: TpFIBDataSet;
    quFCardID: TFIBIntegerField;
    quFCardIMESSURE: TFIBIntegerField;
    quFCardNAME: TFIBStringField;
    dsFCard: TDataSource;
    quFCardPARENT: TFIBIntegerField;
    quFCardTCARD: TFIBIntegerField;
    quFindGrName: TpFIBDataSet;
    quFindGrNameID: TFIBIntegerField;
    quFindGrNameID_PARENT: TFIBIntegerField;
    quFindGrNameITYPE: TFIBSmallIntField;
    quFindGrNameNAMECL: TFIBStringField;
    quAllCards: TpFIBDataSet;
    quAllCardsID: TFIBIntegerField;
    quAllCardsNAME: TFIBStringField;
    quAllCardsIMESSURE: TFIBIntegerField;
    quAllCardsTCARD: TFIBIntegerField;
    quAllCardsPARENT: TFIBIntegerField;
    quAllCardsNAMESHORT: TFIBStringField;
    quAllCardsKOEF: TFIBFloatField;
    quFCardINDS: TFIBIntegerField;
    prDelPartInv: TpFIBStoredProc;
    prAddPartIn1: TpFIBStoredProc;
    quFindTCARD2: TFIBIntegerField;
    taNums: TpFIBDataSet;
    taNumsIT: TFIBIntegerField;
    taNumsSPRE: TFIBStringField;
    taNumsCURNUM: TFIBIntegerField;
    prAddPartIn2: TpFIBStoredProc;
    quGDS: TpFIBDataSet;
    quGDSARTICUL: TFIBIntegerField;
    quGDSIDATE: TFIBIntegerField;
    quGDSIDSTORE: TFIBIntegerField;
    quGDSPOSTIN: TFIBFloatField;
    quGDSPOSTOUT: TFIBFloatField;
    quGDSVNIN: TFIBFloatField;
    quGDSVNOUT: TFIBFloatField;
    quGDSINV: TFIBFloatField;
    quGDSQREAL: TFIBFloatField;
    quCardsSelKOEF: TFIBFloatField;
    trUpdDel: TpFIBTransaction;
    quCReal: TpFIBDataSet;
    dsquCReal: TDataSource;
    quCRealDATEDOC: TFIBDateField;
    quCRealCODEB: TFIBIntegerField;
    quCRealNAMEB: TFIBStringField;
    quCRealQUANT: TFIBFloatField;
    quCRealPRICEOUT: TFIBFloatField;
    quCRealSUMOUT: TFIBFloatField;
    quCRealQUANTC: TFIBFloatField;
    quCRealPRICEIN: TFIBFloatField;
    quCRealSUMIN: TFIBFloatField;
    quCRealIM: TFIBIntegerField;
    quCRealSM: TFIBStringField;
    quCRealQB: TFloatField;
    quCPartInNUMDOCIN: TFIBStringField;
    quCPartInNUMDOCINV: TFIBStringField;
    quCPartInNUMDOCVN: TFIBStringField;
    quCPartInNUMDOCACTS: TFIBStringField;
    quCPartInNUMDOCCOMPL: TFIBStringField;
    quCPartInNUMDOC: TStringField;
    quCPartOutNDOB: TFIBStringField;
    quCPartOutNDI: TFIBStringField;
    quCPartOutNDV: TFIBStringField;
    quCPartOutNDA: TFIBStringField;
    quCPartOutNDC: TFIBStringField;
    quCPartOutNDO: TFIBStringField;
    quCPartOutNDR: TFIBStringField;
    quCPartOutNUMDOC: TStringField;
    quCanEdit: TpFIBDataSet;
    quCanEditID: TFIBIntegerField;
    quCanEditDATEDOC: TFIBDateField;
    quCanEditIDP: TFIBIntegerField;
    quCanEditVALEDIT: TFIBDateTimeField;
    quMHSpis: TpFIBDataSet;
    quMHSpisID: TFIBIntegerField;
    quMHSpisNAMEMH: TFIBStringField;
    quMHSpisNUMSPIS: TFIBSmallIntField;
    quCardsSelSPISSTORE: TFIBStringField;
    quCardsSel1SPISSTORE: TFIBStringField;
    quFCardSpis: TpFIBDataSet;
    quFCardSpisSPISSTORE: TFIBStringField;
    quFCardLASTPRICEOUT: TFIBFloatField;
    quCardsSelRemn: TFloatField;
    quMHr: TpFIBDataSet;
    dsquMHr: TDataSource;
    quMHrID: TFIBIntegerField;
    quMHrPARENT: TFIBIntegerField;
    quMHrITYPE: TFIBIntegerField;
    quMHrNAMEMH: TFIBStringField;
    quMHrDEFPRICE: TFIBIntegerField;
    quMHrNAMEPRICE: TFIBStringField;
    quFCardRemn: TFloatField;
    dsMHr1: TDataSource;
    prDelCard: TpFIBStoredProc;
    quMH_: TpFIBDataSet;
    quMH_ID: TFIBIntegerField;
    quCPartInNDR: TFIBStringField;
    quFindUse1: TpFIBDataSet;
    quFindUse1MAXDATE: TFIBDateField;
    quMH_NAMEMH: TFIBStringField;
    cxStyle27: TcxStyle;
    quTC: TpFIBDataSet;
    quTCIDCARD: TFIBIntegerField;
    quTCID: TFIBIntegerField;
    quTCDATEB: TFIBDateField;
    quTCDATEE: TFIBDateField;
    quTCSHORTNAME: TFIBStringField;
    quTCRECEIPTNUM: TFIBStringField;
    quTCPOUTPUT: TFIBStringField;
    quTCPCOUNT: TFIBIntegerField;
    quTCPVES: TFIBFloatField;
    quTCTEHNO: TFIBStringField;
    quTCOFORM: TFIBStringField;
    quFindGROUP: TStringField;
    quFindSGROUP: TStringField;
    quFindCli: TpFIBDataSet;
    dsquFindCli: TDataSource;
    quFindCliID: TFIBIntegerField;
    quFindCliNAMECL: TFIBStringField;
    taClientsSROKPLAT: TFIBIntegerField;
    quCardsSelCOMMENT: TFIBStringField;
    quCardsSel1COMMENT: TFIBStringField;
    prAddKb: TpFIBStoredProc;
    quCardsSel1BB: TFIBFloatField;
    quCardsSel1GG: TFIBFloatField;
    quCardsSel1U1: TFIBFloatField;
    quCardsSel1U2: TFIBFloatField;
    quCardsSel1EE: TFIBFloatField;
    quCardsSelBB: TFIBFloatField;
    quCardsSelGG: TFIBFloatField;
    quCardsSelU1: TFIBFloatField;
    quCardsSelU2: TFIBFloatField;
    quCardsSelEE: TFIBFloatField;
    quFCardGR: TStringField;
    quFCardSGR: TStringField;
    quTSpecL: TpFIBDataSet;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBIntegerField5: TFIBIntegerField;
    FIBIntegerField6: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    FIBIntegerField7: TFIBIntegerField;
    FIBFloatField5: TFIBFloatField;
    FIBStringField3: TFIBStringField;
    FIBIntegerField8: TFIBIntegerField;
    quRemnDateID: TFIBIntegerField;
    quRemnDatePARENT: TFIBIntegerField;
    quRemnDateNAME: TFIBStringField;
    quRemnDateTTYPE: TFIBIntegerField;
    quRemnDateIMESSURE: TFIBIntegerField;
    quRemnDateIACTIVE: TFIBIntegerField;
    quRemnDateTCARD: TFIBIntegerField;
    quRemnDateNAMESHORT: TFIBStringField;
    quRemnDateKOEF: TFIBFloatField;
    quE: TpFIBQuery;
    dstaTermoObr: TDataSource;
    quTSpecLNETTO2: TFIBFloatField;
    dstaTermoObr2: TDataSource;
    quSetTCard: TpFIBQuery;
    quUpd: TpFIBQuery;
    quF: TpFIBDataSet;
    taClientsNAMEOTP: TFIBStringField;
    taClientsADROTPR: TFIBStringField;
    taClientsRSCH: TFIBStringField;
    taClientsKSCH: TFIBStringField;
    taClientsBANK: TFIBStringField;
    taClientsBIK: TFIBStringField;
    quFCardCATEGORY: TFIBIntegerField;
    quAllCardsCATEGORY: TFIBIntegerField;
    ColorDialog1: TColorDialog;
    quRemnDateCATEGORY: TFIBIntegerField;
    quCateg: TpFIBDataSet;
    dsquCateg: TDataSource;
    quCategID: TFIBIntegerField;
    quCategNAMECAT: TFIBStringField;
    quUseCateg: TpFIBDataSet;
    quUseCategID: TFIBIntegerField;
    quCardsSelRCATEGORY: TFIBIntegerField;
    quLastCli: TpFIBDataSet;
    trSel3: TpFIBTransaction;
    quLastCliIDHEAD: TFIBIntegerField;
    quLastCliNAMECL: TFIBStringField;
    quLastCliIDCARD: TFIBIntegerField;
    quLastCliQUANT: TFIBFloatField;
    quLastCliNAMESHORT: TFIBStringField;
    quLastCliPRICEIN: TFIBFloatField;
    quLastCliSUMIN: TFIBFloatField;
    quLastCliIDM: TFIBIntegerField;
    quLastCliKM: TFIBFloatField;
    quLastCliDATEDOC: TFIBDateField;
    quLastCliNUMDOC: TFIBStringField;
    quLastCliIDCLI: TFIBIntegerField;
    dsquLastCli: TDataSource;
    quM: TpFIBDataSet;
    quMID: TFIBIntegerField;
    quMID_PARENT: TFIBIntegerField;
    quMNAMESHORT: TFIBStringField;
    quMKOEF: TFIBFloatField;
    quMITYPE: TFIBSmallIntField;
    quDocsId: TpFIBDataSet;
    quDocsH: TpFIBQuery;
    quDH: TpFIBDataSet;
    quDHID: TFIBIntegerField;
    quDHDOCNUM: TFIBStringField;
    quDHIBUYER: TFIBIntegerField;
    quDHDOCDATE: TFIBDateField;
    quDHSUMIN: TFIBFloatField;
    quDHSUMOUT: TFIBFloatField;
    dsquDH: TDataSource;
    quDHBUYER_F: TFIBStringField;
    quDHBUYER_I: TFIBStringField;
    quDHBUYER_O: TFIBStringField;
    quDHBUYER_FIO: TStringField;
    quDHNAME: TFIBStringField;
    quDV: TpFIBDataSet;
    dsquDV: TDataSource;
    quDVIDH: TFIBIntegerField;
    quDVIDV: TFIBIntegerField;
    quDVPREDOPLATA: TFIBFloatField;
    quDVIZGOTOVLENIE: TFIBIntegerField;
    quDVWARRANTY: TFIBStringField;
    quDVVPICT: TFIBBlobField;
    quDVVDATE: TFIBDateTimeField;
    quDS: TpFIBDataSet;
    quDSIDH: TFIBIntegerField;
    quDSIDV: TFIBIntegerField;
    quDSNUM: TFIBIntegerField;
    quDSCODE: TFIBIntegerField;
    quDSQUANT: TFIBFloatField;
    quDSPRICEIN: TFIBFloatField;
    quDSPRICEOUT: TFIBFloatField;
    quDSSUMIN: TFIBFloatField;
    quDSSUMOUT: TFIBFloatField;
    quFCardLASTPRICEIN: TFIBFloatField;
    quDocsIdID: TFIBIntegerField;
    quDocsIdDOCNUM: TFIBStringField;
    quDocsIdIBUYER: TFIBIntegerField;
    quDocsIdDOCDATE: TFIBDateField;
    quDocsIdSUMIN: TFIBFloatField;
    quDocsIdSUMOUT: TFIBFloatField;
    quDocsIdIDPERS: TFIBIntegerField;
    quDSPARENT: TFIBIntegerField;
    quDSNAME: TFIBStringField;
    quDSIPOST: TFIBIntegerField;
    quDSNAMECL: TFIBStringField;
    quByuerId: TpFIBDataSet;
    quByuerIdBUYER_ID: TFIBIntegerField;
    quByuerIdBUYER_F: TFIBStringField;
    quByuerIdBUYER_I: TFIBStringField;
    quByuerIdBUYER_O: TFIBStringField;
    quByuerIdBUYER_PASSPORTSERIA: TFIBIntegerField;
    quByuerIdBUYER_PASSPORTNOMER: TFIBIntegerField;
    quByuerIdBUYER_PASSPORTISSUER: TFIBStringField;
    quByuerIdBUYER_PASSPORTISSUEDATE: TFIBDateField;
    quByuerIdBUYER_ADDRESS: TFIBStringField;
    quByuerIdPHONE: TFIBStringField;
    quDSCOMMENT: TFIBStringField;
    quDSDPROC: TFIBFloatField;
    quDSDSUM: TFIBFloatField;
    quDSNAMESHORT: TFIBStringField;
    quFindLASTPRICEOUT: TFIBFloatField;
    taShablH: TpFIBDataSet;
    taShablHIDH: TFIBIntegerField;
    taShablHNAMESH: TFIBStringField;
    taShablHPREDOPLATA: TFIBFloatField;
    taShablHIZGOTOVLENIE: TFIBIntegerField;
    taShablHWARRANTY: TFIBStringField;
    taShablS: TpFIBDataSet;
    taShablSIDH: TFIBIntegerField;
    taShablSNUM: TFIBIntegerField;
    taShablSCODE: TFIBIntegerField;
    taShablSNAME: TFIBStringField;
    taShablSQUANT: TFIBFloatField;
    taShablSPRICEOUT: TFIBFloatField;
    dstaShablH: TDataSource;
    dstaShablS: TDataSource;
    taShablSRSUM: TFloatField;
    quShablSId: TpFIBDataSet;
    quShablSIdIDH: TFIBIntegerField;
    quShablSIdNUM: TFIBIntegerField;
    quShablSIdCODE: TFIBIntegerField;
    quShablSIdQUANT: TFIBFloatField;
    quShablSIdPRICEOUT: TFIBFloatField;
    quDocsIdAKTDATE: TFIBDateField;
    quDHAKTDATE: TFIBDateField;
    quPlat: TpFIBDataSet;
    quPlatIDH: TFIBIntegerField;
    quPlatID: TFIBIntegerField;
    quPlatNUM: TFIBIntegerField;
    quPlatNAMEPL: TFIBStringField;
    quPlatDATEPL: TFIBDateField;
    quPlatSUMPL: TFIBFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taDobSpecQUANTChange(Sender: TField);
    procedure taDobSpecPRICERChange(Sender: TField);
    procedure quCRealCalcFields(DataSet: TDataSet);
    procedure quCPartInCalcFields(DataSet: TDataSet);
    procedure quCPartOutCalcFields(DataSet: TDataSet);
    procedure quCardsSelCalcFields(DataSet: TDataSet);
    procedure quFCardCalcFields(DataSet: TDataSet);
    procedure quFindCalcFields(DataSet: TDataSet);
    procedure quDHCalcFields(DataSet: TDataSet);
    procedure taShablSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
Function CanEdit(IDate:Integer):Boolean;

Function GetId(ta:String):Integer;
procedure prSelPartIn(Articul,IdStore,iCli:Integer;rPrice:Real);
procedure prSelPartInT(Articul,IdStore,iCli:Integer);
procedure prSelPartInRet(Articul,IdStore,iCli:Integer);

function prFindKM(Idm:Integer):Real; //���� �����������
function prFindKNM(Idm:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��
//function prFindCardKNM(IdC:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��

function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������
function prFindSM1(Idm:Integer):String; //�������� ��. ���
function prFindMT(Idm:Integer):Integer;
Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������
Procedure prFindCl(Idc:Integer;Var S1,S2,S3,S4,S5,S6,s7,s8,s9,s10,s11,s12:String); //�������� ������� �� ��� ID
Procedure prFindBy(Idc:Integer;Var S1,S2,S3,S4,S5,S6,s7,s8,s9,s10:String); //�������� ���������� �� ��� ID

Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis:Integer);

//��� ���� ��� �� �����
Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Var rSum:Real);
Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Memo1:TcxMemo;Var rSumOut:Real);

Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum,rSumT:Real):Integer;
Function prTOFind(iDateB,IdSkl:Integer):Integer;
Procedure prTODel(iDateB,IdSkl:Integer);
Function prTOPostIn(iD,IdSkl:Integer; Var rSumT:Real):Real;
Function prTOPostOut(iD,IdSkl:Integer; Var rSumT:Real):Real;
Function prTOVnIn(iD,IdSkl:Integer; Var rSumT:Real):Real;
Function prTOVnOut(iD,IdSkl:Integer; Var rSumT:Real):Real;
//Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real):Real;
Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real; Var iType:SmallInt):Real;

Function prTOReal(iD,IdSkl:Integer):Real;
Function prTypeC(IdC:Integer):Integer;
procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String;iType:Integer);
procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
Function prFindGrN(IdC:INteger):String;
Function prCalcRemnSum(iCode,iDate,iSkl:Integer):Real; //��� �������� ����
Function prCalcRemnSumF(iCode,iDate,iSkl:Integer;rQ:Real):Real; //��� �������������� ����
Function prCalcRemnSpeed(iCode,iDate,iSkl:Integer;Var rSpeedDay,rRemnDay:Real):Real;

Function prTypeTC(IdC:Integer):Integer;
Function prGetNum(iT,iAdd:INteger):String;
Function fCalcBlasCard(IdSkl,iCode:Integer):Boolean;
Procedure prSetTC(IdC:Integer;bTC:Boolean);

procedure prFinfBGU(idc:Integer;var bb,gg,uu:Real);
procedure prFindObr(iobr:INteger;var bo,go,uo:Real; var so:String);
procedure prFinfBGUE(idc:Integer;var bb,gg,uu,ee:Real);

var
  dmO: TdmO;
  rQb,rQe:Real;

implementation

uses Un1, DMOReps, TOSel;

{$R *.dfm}

procedure prFindObr(iobr:INteger;var bo,go,uo:Real; var so:String);
begin
  bo:=0; go:=0; uo:=0; so:='';
  if iobr>0 then
  begin
    with dmO do
    begin
      quF.Active:=False;
      quF.SelectSQL.Clear;
      quF.SelectSQL.Add('SELECT NAMEOBR,BB,GG,U1,U2');
      quF.SelectSQL.Add('FROM OF_BGUOBR');
      quF.SelectSQL.Add('where ID ='+IntToStr(iobr));
      quF.Active:=True;
      if quF.RecordCount>0 then
      begin
        so:=quF.fieldbyname('NAMEOBR').AsString;
        bo:=quF.fieldbyname('BB').AsFloat;
        go:=quF.fieldbyname('GG').AsFloat;
        uo:=quF.fieldbyname('U1').AsFloat;
      end;
      quFind.Active:=False;
    end;
  end;
end;

procedure prFinfBGUE(idc:Integer;var bb,gg,uu,ee:Real);
begin
  bb:=0; gg:=0; uu:=0; ee:=0;
  with dmO do
  begin
    quF.Active:=False;
    quF.SelectSQL.Clear;
    quF.SelectSQL.Add('SELECT BB,GG,U1,U2,EE');
    quF.SelectSQL.Add('FROM OF_CARDS');
    quF.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quF.Active:=True;
    if quF.RecordCount>0 then
    begin
      bb:=quF.fieldbyname('BB').AsFloat;
      gg:=quF.fieldbyname('GG').AsFloat;
      uu:=quF.fieldbyname('U1').AsFloat;
      ee:=quF.fieldbyname('EE').AsFloat;
    end;
    quFind.Active:=False;
  end;
end;


procedure prFinfBGU(idc:Integer;var bb,gg,uu:Real);
begin
  bb:=0; gg:=0; uu:=0;
  with dmO do
  begin
    quF.Active:=False;
    quF.SelectSQL.Clear;
    quF.SelectSQL.Add('SELECT BB,GG,U1,U2');
    quF.SelectSQL.Add('FROM OF_CARDS');
    quF.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quF.Active:=True;
    if quF.RecordCount>0 then
    begin
      bb:=quF.fieldbyname('BB').AsFloat;
      gg:=quF.fieldbyname('GG').AsFloat;
      uu:=quF.fieldbyname('U1').AsFloat;
    end;
    quFind.Active:=False;
  end;
end;

Procedure prSetTC(IdC:Integer;bTC:Boolean);
Var iTC:INteger;
begin
  with dmO do
  begin
    if bTc then iTc:=1 else iTc:=0;
    if quCardsSel.locate('ID',IdC,[]) then
    begin
      quCardsSel.Edit;
      quCardsSelTCARD.AsInteger:=iTC;
      quCardsSel.Post;
      quCardsSel.Refresh;
    end else
    begin
      quSetTCard.SQL.Clear;
      quSetTCard.SQL.Add('Update OF_CARDS Set TCARD='+IntToStr(iTC)+' where ID='+IntToStr(IdC));
      quSetTCard.ExecQuery;
    end;
  end;
end;

Procedure prFindCl(Idc:Integer;Var S1,S2,S3,S4,s5,s6,s7,s8,s9,s10,s11,s12:String); //�������� ������� �� ��� ID
begin
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    if taClients.Locate('ID',Idc,[]) then
    begin
      S1:=taClientsNAMECL.AsString;
      S2:=taClientsADDRES.AsString;
      S3:=taClientsINN.AsString;
      S4:=taClientsKPP.AsString;
      if taClientsNAMEOTP.AsString>'' then  S5:=taClientsNAMEOTP.AsString else S5:='';
      if taClientsADROTPR.AsString>'' then  S6:=taClientsADROTPR.AsString else S6:='';
      s7:=taClientsRSCH.AsString;
      s8:=taClientsKSCH.AsString;
      s9:=taClientsBANK.AsString;
      s10:=taClientsBIK.AsString;
      s11:=taClientsPHONE.AsString;
      s12:=taClientsMOL.AsString;

    end else
    begin
      S1:='';
      S2:='';
      S3:='';
      S4:='';
      S5:='';
      S6:='';
      S7:='';
      S8:='';
      S9:='';
      S10:='';
      S11:='';
      S12:='';
    end;
  end;
end;

Procedure prFindBy(Idc:Integer;Var S1,S2,S3,S4,S5,S6,s7,s8,s9,s10:String); //�������� ���������� �� ��� ID
Var StrWk,StrWk1:String;
begin
  with dmO do
  begin
    quByuerId.Active:=False;
    quByuerId.ParamByName('IDB').AsInteger:=Idc;
    quByuerId.Active:=True;
    quByuerId.First;
    if quByuerId.RecordCount>0 then
    begin
      StrWk:=quByuerIdBUYER_F.AsString;

      StrWk1:=quByuerIdBUYER_I.AsString;
      if Length(StrWk1)>0 then StrWk:=StrWk+' '+StrWk1[1]+'.';

      StrWk1:=quByuerIdBUYER_O.AsString;
      if Length(StrWk1)>0 then StrWk:=StrWk+' '+StrWk1[1]+'.';

      S1:=StrWk;
      S2:=quByuerIdBUYER_PASSPORTSERIA.AsString;
      S3:=quByuerIdBUYER_PASSPORTNOMER.AsString;
      S4:=quByuerIdBUYER_PASSPORTISSUER.AsString;
      S5:=quByuerIdBUYER_PASSPORTISSUEDATE.AsString;
      S6:=quByuerIdBUYER_ADDRESS.AsString;
      s7:=quByuerIdPHONE.AsString;
      s8:=quByuerIdBUYER_F.AsString;
      s9:=quByuerIdBUYER_I.AsString;
      s10:=quByuerIdBUYER_O.AsString;

    end else
    begin
      S1:='';
      S2:='';
      S3:='';
      S4:='';
      S5:='';
      S6:='';
      S7:='';
      S8:='';
      S9:='';
      S10:='';
    end;
    quByuerId.Active:=False;
  end;
end;



Function fCalcBlasCard(IdSkl,iCode:Integer):Boolean;
Var StrWk:String;
    iNum:Integer;
begin
  Result:=False;
  with dmO do
  begin
    quFCardSpis.Active:=False;
    quFCardSpis.ParamByName('ICODE').AsInteger:=iCode;
    quFCardSpis.Active:=True;
    StrWk:=quFCardSpisSPISSTORE.AsString;
    if length(StrWk)<20 then StrWk:='00000000000000000000';
    quFCardSpis.Active:=False;

    iNum:=0;
    quMHSpis.Active:=False;
    quMHSpis.Active:=True;
    quMHSpis.First;
    while not quMHSpis.Eof do
    begin
      if quMHSpisID.AsInteger=IdSkl then
      begin
        iNum:=quMHSpisNUMSPIS.AsInteger;
        Break;
      end;
      quMHSpis.Next;
    end;
    quMHSpis.Active:=False;

    if (iNum>0) and (iNum<20) and (StrWk>'') then
    begin
      if StrWk[iNum]='1' then Result:=True;
    end;
  end;
end;

Function prGetNum(iT,iAdd:INteger):String;
var iNum:Integer;
    sPre:String;
begin
  Result:='';
  with dmO do
  begin
    if taNums.Active=False then taNums.Active:=True;
    if taNums.Locate('IT',iT,[]) then
    begin
      iNum:=taNumsCURNUM.AsInteger+1;
      sPre:=taNumsSPRE.AsString;
      TrimStr(sPre);
      Result:=sPre+INtToStr(iNum);
      if iAdd=1 then
      begin
        taNums.Edit;
        taNumsCURNUM.AsInteger:=iNum;
        taNums.Post;
      end;
    end;
  end;
end;

Function prTypeTC(IdC:Integer):Integer;
begin
  Result:=0;
  with dmO do
  begin
    quFind.Active:=False;
    quFind.SelectSQL.Clear;
    quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD,LASTPRICEOUT');
    quFind.SelectSQL.Add('FROM OF_CARDS');
    quFind.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quFind.Active:=True;
    if quFind.RecordCount>0 then
    begin
      Result:=quFind.fieldbyname('TCARD').AsInteger;
    end;
    quFind.Active:=False;
  end;
end;


Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������
begin
  with dmORep do
  begin
    Id_Group:=0;
    NameGr:='';

    quSelNameCl.Active:=False;
    quSelNameCl.ParamByName('IDC').AsInteger:=Idc;
    quSelNameCl.Active:=True;

    quSelNameCl.First;
    if quSelNameCl.RecordCount>0 then
    begin
      Id_Group:=quSelNameClPARENT.AsInteger;
      NameGr:=quSelNameClNAMECL.AsString;
    end;

    quSelNameCl.Active:=False;
  end;
end;

Function prCalcRemnSum(iCode,iDate,iSkl:Integer):Real;
begin // � ���� �������� �������� ���� ������ ��������
  with dmORep do
  begin
    prCalcSumRemn.ParamByName('IDCARD').AsInteger:=iCode;
    prCalcSumRemn.ParamByName('IDSKL').AsInteger:=iSkl;
    prCalcSumRemn.ParamByName('DDATE').AsDate:=iDate;
    prCalcSumRemn.ExecProc;
    Result:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;
  end;
end;


Function prCalcRemnSumF(iCode,iDate,iSkl:Integer;rQ:Real):Real; //rQ ������ ���� � ��������, �.�. ���� � �������� ��
begin
  Result:=0;
end;



Function prFindGrN(IdC:INteger):String;
begin
  with dmO do
  begin
    quFindGrName.Active:=False;
    quFindGrName.ParamByName('IDCL').AsInteger:=IdC;
    quFindGrName.Active:=True;
    if quFindGrName.RecordCount>0 then Result:=quFindGrNameNAMECL.AsString
    else Result:='';
    quFindGrName.Active:=False;
  end;
end;

procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
begin
  with dmO do
  begin
  end;
  iDate:=0;
  iC:=0;
end;

procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String;iType:Integer);
begin
end;


Function prTypeC(IdC:Integer):Integer;
begin
  Result:=1;
  with dmO do
  begin
    quSelCardsCat.Active:=False;
    quSelCardsCat.ParamByName('IDCARD').AsInteger:=IdC;
    quSelCardsCat.Active:=True;
    quSelCardsCat.First;
    if quSelCardsCat.RecordCount>0 then
    begin
      Result:=quSelCardsCatCATEGORY.AsInteger;
    end;
    quSelCardsCat.Active:=False;
  end;
end;

Function prTOReal(iD,IdSkl:Integer):Real;
begin
  Result:=0;
end;

Function prTOVnIn(iD,IdSkl:Integer; Var rSumT:Real):Real;
begin
  Result:=0;
end;

Function prTOVnOut(iD,IdSkl:Integer; Var rSumT:Real):Real;
begin
  Result:=0;
end;

Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real;Var iType:SmallInt):Real;
begin       //��������������
  Result:=0;
end;

Function prTOPostOut(iD,IdSkl:Integer; Var rSumT:Real):Real;
begin
  Result:=0;
end;


Function prTOPostIn(iD,IdSkl:Integer; Var rSumT:Real):Real;
begin
  Result:=0;
end;

Procedure prTODel(iDateB,IdSkl:Integer);
begin
  with dmO do
  begin
    WriteHistory('�������� �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+'. ��������: '+Person.Name);
    quTODel.ParamByName('DATEB').AsInteger:=iDateB;
    quTODel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTODel.ExecQuery;
  end;
end;


Function prTOFind(iDateB,IdSkl:Integer):Integer;
begin
  with dmO do
  begin
    Result:=0;
  end;
end;


Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum,rSumT:Real):Integer;
begin
  with dmO do
  begin
    Result:=1;
  end;
end;

Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Memo1:TcxMemo;Var rSumOut:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    IdC,iMain:INteger;
    sM,StrWk,StrWk1,StrWk2,StrWk3,StrWk4:String;
    rSum:Real;
begin
  sBeg:=sBeg+'    ';
  with dmO do
  begin
    //���
    rSumOut:=0; MassaB:=1;
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end else
    begin
      inc(iErr);
      sErr:=sErr+IntToStr(iCode)+';';
      Memo1.Lines.Add(SBeg+'������: �� �� �������.')
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM; // 0,050

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������   //0,050
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� ������� //0,050
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������ rQ/����� ������ 0,050*0,6/0,05

        StrWk:=QT1.FieldByName('NAME').AsString;
        While Length(StrWk)<20 do StrWk:=StrWk+' ';
        StrWk1:=QT1.FieldByName('IDCARD').AsString;
        While Length(StrWk1)<5 do StrWk1:=' '+StrWk1;
        Str((RoundEx(rQ1*1000)/1000):8:3,StrWk2);

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��

          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ����. ���-��: '+StrWk2+' '+sM);
          prCalcBlPrice1(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,Memo1,rSum);
        end else
        begin //��� ��
          rSum:=0;
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then
              rSum:=taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
          Str((RoundEx(rSum*100)/100):8:2,StrWk3);
          Str((RoundEx(rSum/rQ1*100)/100):8:2,StrWk4);
          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ���.  ���-��: '+StrWk2+' '+sM+'    �����: '+StrWk3+ '�'+'    ����: '+StrWk4+ '�');
        end;
        rSumOut:=rSumOut+rSum;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;



Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Var rSum:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM:Real;
    IdC,iMain:INteger;
    sM:String;
    rSum1,MassaB:Real;
begin
  rSum:=0; MassaB:=1;
  with dmO do
  begin
    //���
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM;

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��
//          prCalcBl(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ,taCalc,Memo1);
          prCalcBlPrice(QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,rSum1);
          rSum:=rSum+rSum1;
        end else
        begin //��� ��
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then rSum:=rSum+taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
        end;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;

Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis:Integer);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    bCalc:Boolean;
    rQRemn,rQbs:Real;

Procedure prCalcSave;
Var rQb:Real;
begin
  if taCalcB.Active = False then taCalcB.Active:=True;
  if taCalc.Active = False then taCalc.Active:=True;

  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQbs;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    taCalcB.Append;
    taCalcB.fieldByName('ID').AsInteger:=IdPos;
    taCalcB.fieldByName('CODEB').AsInteger:=taSpecB.fieldByName('IDCARD').AsInteger;
    taCalcB.fieldByName('NAMEB').AsString:=taSpecB.fieldByName('NAME').AsString;
    taCalcB.fieldByName('QUANT').AsFloat:=taSpecB.fieldByName('QUANT').AsFloat;
    taCalcB.fieldByName('PRICEOUT').AsFloat:=taSpecB.fieldByName('PRICER').AsFloat;
    taCalcB.fieldByName('SUMOUT').AsFloat:=taSpecB.fieldByName('RSUM').AsFloat;
    taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
    taCalcB.fieldByName('NAMEC').AsString:=sName;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQbs;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQb:=taCalc.fieldByName('Quant').AsFloat+rQbs;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQb;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQbs;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;
end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    if bNeedMemo then Memo1.Lines.Add(SBeg+'��� - '+IntToStr(iCode));

    rQbs:=rQ;

    if iSpis=1 then //�� �������� ������������
    begin
      if iTCard=1 then // ����� � ��������
      begin
        if fCalcBlasCard(IdSkl,iCode) then bCalc:=False //���� �� ������������ �� ��� �����
        else
        begin
          //����� ��������� � ���� �� ��� ������ �� ���� � �� ����� ���� ������ �����
          quFindTCard.Active:=False;
          quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
          quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
          quFindTCard.Active:=True;
          if quFindTCard.RecordCount=0 then bCalc:=False
          else
          begin
            bCalc:=True;
            // ����� ��������� �������, ����� ����, ���� ����� ������� �������
            rQremn:=prCalcRemn(iCode,iDate,IdSkl);
            if (rQRemn>0)and(rQ>0) then //����� �������� ���������� �������� ��� ��������� � �������������� ����������
            begin                       // � ��� ������� �� ������� ������� ����������
              bCalc:=False; //����� ���� ��� ������� ����� ���� ����� �� �������
            //���� �������� �� ������� �� �������� � ��� �������� ������ � ��� ����� � ����������
              if rQRemn<rQ then  rQbs:=rQRemn;
            end;
          end;
          quFindTCard.Active:=False;
        end;
      end else bCalc:=False;
    end else  // �� �������� �� ������������
    begin
       bCalc:=False; //����� ���� ��� ������� ����� �� �������� � rQbs:=rQ;
    end;

    if bCalc=False then // ������������ �� ���� � ����� rQbs
    begin
        //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQbs:=rQbs*kM;              //������ ������� - ��� ������ �� �����������

      if bNeedMemo then Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). ��� �����.  ���-��: '+FloatToStr(RoundEx(rQbs*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;

      if ((rQ-rQbs)>0.00001) and (iSpis=1) then  //��� ���-�� ��������  -  ���� ������������
      begin
        rQbs:=rQ-rQbs;
        bCalc:=True;
      end;
    end;

    if bCalc then //���� ��� ����� � ��� ���� ������������
    begin
      //����������� ���� ������������ � �����  rQbs ������ rQ

      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        if (iPCount=0) or (MassaB=0) then
        begin
          iTC:=0;
          inc(iErr);
          sErr:=sErr+IntToStr(iCode)+' '+sName+';';
          if bNeedMemo then Memo1.Lines.Add(SBeg+'������: ������������ ��������� � ��.');
        end else
        begin
          if bNeedMemo then Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQbs*1000)/1000)+' '+sM);
        end;
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        if bNeedMemo then Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������

          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQbs/MassaB; //��� ����� �� ��� ���-�� ������

          prCalcBl(sBeg,QT1.FieldByName('NAME').AsString,IdSkl,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taSpecB,taCalc,taCalcB,Memo1,bNeedMemo,iSpis);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end;


  end;
end;



{  //������ ����������� ������� ��� ������� ���-�� ��������������

Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    bCalc:Boolean;

Procedure prCalcSave;
Var rQb:Real;
begin
  if taCalcB.Active = False then taCalcB.Active:=True;
  if taCalc.Active = False then taCalc.Active:=True;

  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    taCalcB.Append;
    taCalcB.fieldByName('ID').AsInteger:=IdPos;
    taCalcB.fieldByName('CODEB').AsInteger:=taSpecB.fieldByName('IDCARD').AsInteger;
    taCalcB.fieldByName('NAMEB').AsString:=taSpecB.fieldByName('NAME').AsString;
    taCalcB.fieldByName('QUANT').AsFloat:=taSpecB.fieldByName('QUANT').AsFloat;
    taCalcB.fieldByName('PRICEOUT').AsFloat:=taSpecB.fieldByName('PRICER').AsFloat;
    taCalcB.fieldByName('SUMOUT').AsFloat:=taSpecB.fieldByName('RSUM').AsFloat;
    taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
    taCalcB.fieldByName('NAMEC').AsString:=sName;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQ:=taCalc.fieldByName('Quant').AsFloat+rQ;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;
end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    Memo1.Lines.Add(SBeg+'��� - '+IntToStr(iCode));
    if iTCard=1 then bCalc:=True else bCalc:=False;
    if fCalcBlasCard(IdSkl,iCode) then bCalc:=False; //���� �� ������������ �� ��� �����

    if bCalc then //���� ��� ����� � ��� ���� ������������
    begin
      // ����� ��������� �������, ����� ����, ���� ����� ������� �������



      //����������� ����

      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        if (iPCount=0) or (MassaB=0) then
        begin
          iTC:=0;
          inc(iErr);
          sErr:=sErr+IntToStr(iCode)+' '+sName+';';
          Memo1.Lines.Add(SBeg+'������: ������������ ��������� � ��.');
        end else
        begin
          Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
        end;
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin

        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������

          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBl(sBeg,QT1.FieldByName('NAME').AsString,IdSkl,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taSpecB,taCalc,taCalcB,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);                         
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;
    end;
  end;
end;

}

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmO do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all
    if ta='MESS' then iType:=100; //������� ���������
    if ta='Class' then iType:=101; //������������� ������� � �����
    if ta='MH' then iType:=102; //����� �������� � ������������
    if ta='GD' then iType:=103; //�������
    if ta='Cli' then iType:=104; //�����������
    if ta='DocIn' then iType:=105; //��������� ��������� ����������
    if ta='SpecIn' then iType:=106; //������������ ��������� ����������
    if ta='TCards' then iType:=107; //��������������� �����
    if ta='PartIn' then iType:=108; //��������� ������
    if ta='InvH' then iType:=109; //��������� ��������������
    if ta='InvS' then iType:=110; //������������ ��������������
    if ta='Pars' then iType:=111; //��������� ������
    if ta='DocOut' then iType:=112; //��������� ��������� ����������
    if ta='SpecOut' then iType:=113; //������������ ��������� ����������
    if ta='DocOutB' then iType:=114; //��������� ��������� ���������� ����
    if ta='SpecOutB' then iType:=115; //������������ ��������� ���������� ����
    if ta='SpecOutC' then iType:=116; //������������ ��������� ���������� ��������
    if ta='HeadCompl' then iType:=117; //��������� ������������
    if ta='DocAct' then iType:=118; //��������� ����� �����������
    if ta='DocVn' then iType:=119; //��������� ����������
    if ta='SpecVn' then iType:=120; //��������� ����������
    if ta='DocR' then iType:=121; //��������� ���������� �� �������
    if ta='Shabl' then iType:=122; //��������� ���������� �� �������

    prGetId.ParamByName('ITYPE').Value:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmO.DataModuleCreate(Sender: TObject);
begin
  OfficeRnDb.Connected:=False;
  OfficeRnDb.DBName:=CommonSet.OfficeDb;
  try
    OfficeRnDb.Open;
  except
  end;
end;

procedure TdmO.DataModuleDestroy(Sender: TObject);
begin
  try
    OfficeRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmO do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then
     Result:=False;
  end;
end;

Function CanEdit(IDate:Integer):Boolean;
begin
  with dmO do
  begin
    quCanEdit.Active:=False;
    quCanEdit.Active:=True;
    Result:=True;
    quCanEdit.First;
    if quCanEdit.RecordCount>0 then
    begin
      if Trunc(quCanEditDATEDOC.AsDateTime)>=IDate then Result:=False;
    end;
  end;
end;



procedure prSelPartInT(Articul,IdStore,iCli:Integer);
begin
end;

procedure prSelPartIn(Articul,IdStore,iCli:Integer;rPrice:Real);
begin
end;

procedure prSelPartInRet(Articul,IdStore,iCli:Integer);
begin
end;


function prFindMT(Idm:Integer):Integer;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMITYPE.AsInteger;  //
  end;
end;


function prFindKM(Idm:Integer):Real;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMKOEF.AsFloat;
  end;
end;

function prFindKNM(Idm:Integer;Var km:Real):String;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    Km:=quMKOEF.AsFloat;
    Result:=quMNAMESHORT.AsString;
  end;
end;


function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Var iDate:Integer;
begin
  with dmO do
  begin
    Result:=0;
    try
      iDate:=prDateToI(rDate);
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=iDc;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        Result:=quFindEUTO100GRAMM.AsFloat;
      end;
    finally
      quFindEU.Active:=False;
    end;
  end;
end;

function prFindSM1(Idm:Integer):String; //�������� ��. ���
begin
  with dmO do
  begin
    try
      Result:='';
      quM.Active:=False;
      quM.ParamByName('IDM').AsInteger:=Idm;
      quM.Active:=True;
      if quM.RecordCount=1 then Result:=quMNAMESHORT.asstring
    finally
      quM.Active:=False;
    end;
  end;
end;

Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������
begin
  IM:=Idm;
  with dmO do
  begin
    try
      quM.Active:=False;
      quM.ParamByName('IDM').AsInteger:=IM;
      quM.Active:=True;
      if quMID_PARENT.AsInteger=0 then SM:=quMNAMESHORT.asstring
      else begin
        IM:=quMID_PARENT.AsInteger;
        quM.Active:=False;
        quM.ParamByName('IDM').AsInteger:=IM;
        quM.Active:=True;
        SM:=quMNAMESHORT.asstring;
      end;
    finally
      quM.Active:=False;
    end;
  end;
end;

Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
begin
  Result:=0;
end;

Function prCalcRemnSpeed(iCode,iDate,iSkl:Integer;Var rSpeedDay,rRemnDay:Real):Real;
begin
  Result:=0;
end;



procedure TdmO.taDobSpecQUANTChange(Sender: TField);
begin
//  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

procedure TdmO.taDobSpecPRICERChange(Sender: TField);
begin
//  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

procedure TdmO.quCRealCalcFields(DataSet: TDataSet);
begin
  //
  quCRealQB.AsFloat:=0;
  if quCRealQUANT.AsFloat<>0 then quCRealQB.AsFloat:=quCRealQUANTC.AsFloat/quCRealQUANT.AsFloat;
end;

procedure TdmO.quCPartInCalcFields(DataSet: TDataSet);
begin
  if quCPartInDTYPE.AsInteger=1 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCIN.AsString;
  if quCPartInDTYPE.AsInteger=3 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCINV.AsString;
  if quCPartInDTYPE.AsInteger=4 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCVN.AsString;
  if quCPartInDTYPE.AsInteger=5 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCACTS.AsString;
  if quCPartInDTYPE.AsInteger=6 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCCOMPL.AsString;
  if quCPartInDTYPE.AsInteger=8 then quCPartInNUMDOC.AsString:=quCPartInNDR.AsString;
end;

procedure TdmO.quCPartOutCalcFields(DataSet: TDataSet);
begin
  if quCPartOutDTYPE.AsInteger=2 then quCPartOutNUMDOC.AsString:=quCPartOutNDOB.AsString;
  if quCPartOutDTYPE.AsInteger=3 then quCPartOutNUMDOC.AsString:=quCPartOutNDI.AsString;
  if quCPartOutDTYPE.AsInteger=4 then quCPartOutNUMDOC.AsString:=quCPartOutNDV.AsString;
  if quCPartOutDTYPE.AsInteger=5 then quCPartOutNUMDOC.AsString:=quCPartOutNDA.AsString;
  if quCPartOutDTYPE.AsInteger=6 then quCPartOutNUMDOC.AsString:=quCPartOutNDC.AsString;
  if quCPartOutDTYPE.AsInteger=7 then quCPartOutNUMDOC.AsString:=quCPartOutNDO.AsString;
  if quCPartOutDTYPE.AsInteger=8 then quCPartOutNUMDOC.AsString:=quCPartOutNDR.AsString;

end;

procedure TdmO.quCardsSelCalcFields(DataSet: TDataSet);
begin
  if CommonSet.CalcRemns=1 then quCardsSelRemn.AsFloat:=prCalcRemn(quCardsSelID.AsInteger,Trunc(Date),CurVal.IdMH1)
  else quCardsSelRemn.AsFloat:=0;
end;

procedure TdmO.quFCardCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  quFCardRemn.AsFloat:=prCalcRemn(quFCardID.AsInteger,Trunc(Date),CurVal.IdMH2);
  prFind2group(quFCardPARENT.AsInteger,S1,S2);
  quFCardGR.AsString:=S1;
  quFCardSGR.AsString:=S2;
end;

procedure TdmO.quFindCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  prFind2group(quFindPARENT.AsInteger,S1,S2);
  quFindGROUP.AsString:=S1;
  quFindSGROUP.AsString:=S2;
end;

procedure TdmO.quDHCalcFields(DataSet: TDataSet);
begin
  quDHBUYER_FIO.asString:=quDHBUYER_F.asString+' '+quDHBUYER_I.asString+' '+quDHBUYER_O.asString;
end;

procedure TdmO.taShablSCalcFields(DataSet: TDataSet);
begin
  taShablSRSUM.AsFloat:=rv(taShablSQUANT.AsFloat*taShablSPRICEOUT.AsFloat);
end;

end.
