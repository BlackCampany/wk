unit uBuyers;

interface

uses
  TrDispCall,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxImageComboBox, cxContainer, cxTextEdit,
  StdCtrls, Menus, cxLookAndFeelPainters, cxButtons, FR_DSet, FR_DBSet,
  FR_Class, ADODB, ComCtrls, Excel2000, OleServer, ExcelXP, ComObj,StrUtils,
  ShellApi,cxProgressBar;

type
  TfmClients1 = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    amCli: TActionManager;
    acMove: TAction;
    acExit: TAction;
    ViewCli: TcxGridDBTableView;
    LevelCli: TcxGridLevel;
    GridCli: TcxGrid;
    ViewCliId: TcxGridDBColumn;
    ViewCliINN: TcxGridDBColumn;
    ViewCliName: TcxGridDBColumn;
    ViewCliFullName: TcxGridDBColumn;
    ViewCliIType: TcxGridDBColumn;
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TcxTextEdit;
    Button1: TcxButton;
    acTovar: TAction;
    SpeedItem3: TSpeedItem;
    acTestChangePriceIn: TAction;
    SpeedItem4: TSpeedItem;
    acPrintCli: TAction;
    SpeedItem5: TSpeedItem;
    frRepCli: TfrReport;
    frtaClients: TfrDBDataSet;
    quClients: TADOQuery;
    quClientsId: TAutoIncField;
    quClientsINN: TStringField;
    quClientsName: TStringField;
    quClientsFullName: TStringField;
    quClientsIType: TSmallintField;
    acTestChangePriceInOne: TAction;
    SpeedItem6: TSpeedItem;
    acAddPost: TAction;
    acEditClients: TAction;
    acDelPost: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ViewCliIActiveT: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    acOnPost: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    acTrList: TAction;
    ViewCliKPP: TcxGridDBColumn;
    ViewCliPostIndex: TcxGridDBColumn;
    ViewCliGorod: TcxGridDBColumn;
    ViewCliStreet: TcxGridDBColumn;
    ViewCliHouse: TcxGridDBColumn;
    ViewCliIActive: TcxGridDBColumn;
    SpeedItem7: TSpeedItem;
    PBar1: TcxProgressBar;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    ViewCliHasSpec: TcxGridDBColumn;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N21: TMenuItem;
    procedure acExitExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acTovarExecute(Sender: TObject);
    procedure acTestChangePriceInExecute(Sender: TObject);
    procedure acPrintCliExecute(Sender: TObject);
    procedure frtaClientsClose(Sender: TObject);
    procedure acTestChangePriceInOneExecute(Sender: TObject);
    procedure acAddPostExecute(Sender: TObject);
    procedure acEditClientsExecute(Sender: TObject);
    procedure acDelPostExecute(Sender: TObject);
    procedure acOnPostExecute(Sender: TObject);
    procedure ViewCliCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acTrListExecute(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
    procedure SpeedItem8Click(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClients1: TfmClients1;

implementation

uses Dm, uDocs, Un1, uCardCli, uPeriod, uCliChPr, AddPost, uPeriod1;

{$R *.dfm}

procedure TfmClients1.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmClients1.acMoveExecute(Sender: TObject);
begin
  with dmC do
  begin
    if not taClients.Eof then
    begin
      fmDocs.ViewDocs.BeginUpdate;
      quDocs.Active:=False;
      quDocs.SQL.Clear;
      //quDocs.SQL.Add('Select');
      //quDocs.SQL.Add('d.Id_Shop, d.Id_Depart, d.Id,  d.DocDate, d.Id_Client, d.DocNum, d.SFNum, d.SumIn, d.SumOut, SumNac=d.SumOut-d.SumIn, d.NoNDS, d.NDS1, d.NDS2, d.DocType,');
      //quDocs.SQL.Add('NameSh=s.Name, NameDep=dep.Name , NameCli=cl.Name, SumInSp=d.SumIn, SumOutSp=d.SumOut');
      //quDocs.SQL.Add('from TTN d');


     quDocs.SQL.Add('select');
     quDocs.SQL.Add('  d.Id_Shop');
     quDocs.SQL.Add(', d.Id_Depart');
     quDocs.SQL.Add(', d.Id');
     quDocs.SQL.Add(', d.DocDate');
     quDocs.SQL.Add(', d.Id_Client');
     quDocs.SQL.Add(', d.DocNum');
     quDocs.SQL.Add(', d.SFNum');

     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.SumIn else d.SumIn end as SumIn');
     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.SumOut else d.SumOut end as SumOut');
     quDocs.SQL.Add(', case when d.DocType=2 then -1*(d.SumOut-d.SumIn) else (d.SumOut-d.SumIn) end as SumNac');
     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.NoNDS else d.NoNDS end as NoNDS');
     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.NDS1 else d.NDS1 end as NDS1');
     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.NDS2 else d.NDS2 end as NDS2');

     quDocs.SQL.Add(', d.DocType');
     quDocs.SQL.Add(', NameSh=s.Name');
     quDocs.SQL.Add(', NameDep=dep.Name');
     quDocs.SQL.Add(', NameCli=cl.Name');

     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.SumIn else d.SumIn end as SumInSp');
     quDocs.SQL.Add(', case when d.DocType=2 then -1* d.SumOut else d.SumOut end  as SumOutSp');
     quDocs.SQL.Add('from TTN d');

      quDocs.SQL.Add('Left join Shops s on s.Id=d.Id_Shop');
      quDocs.SQL.Add('Left join Departs dep on ((dep.Id_Shop=d.Id_Shop)and(dep.Id=d.Id_Depart))');
      quDocs.SQL.Add('Left join Clients cl on cl.Id=d.Id_Client');
      quDocs.SQL.Add('where DocDate>='''+FormatDateTime('yyyymmdd',CommonSet.DateBeg)+'''');
      quDocs.SQL.Add('and DocDate<'''+FormatDateTime('yyyymmdd',CommonSet.DateEnd+1)+'''');
      quDocs.SQL.Add('and d.Id_Client='+IntToStr(taClientsId.AsInteger));
      quDocs.SQL.Add('Order by cl.NameCli, d.DocDate, s.Name');

      quDocs.Active:=True;
      fmDocs.ViewDocs.EndUpdate;
      fmDocs.ViewDocs.Tag:=taClientsId.AsInteger;

      fmDocs.Caption:='��������� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+'     �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd);
      fmDocs.Show;
    end;
  end;
end;

procedure TfmClients1.Button1Click(Sender: TObject);
Var StrWk:String;
begin
  with dmC do
  begin
    if Edit1.Text>'' then
    begin
      quFindCli.Active:=False;
      quFindCli.SQL.Clear;
      quFindCli.SQL.Add('SElect INN from Clients Cl');
      quFindCli.SQL.Add('where Cl."Name" like ''%'+Edit1.Text+'%''');
      quFindCli.Active:=True;

      if quFindCli.RecordCount>0 then
      begin
        StrWk:=quFindCliINN.AsString;
        taClients.Locate('INN',strwk,[]);
        GridCli.SetFocus;
      end;
      quFindCli.Active:=False;
    end;
  end;
end;

procedure TfmClients1.acTovarExecute(Sender: TObject);
begin
  //������ ����������
  with dmC do
  begin
    fmCardCli.ViewCardCli.BeginUpdate;
    quCardCli.Active:=False;
    quCardCli.Parameters.ParamByName('IDCL').Value:=taClientsId.AsInteger;
    quCardCli.Parameters.ParamByName('DATEB').Value:=CommonSet.DateBeg;
    quCardCli.Parameters.ParamByName('DateE').Value:=CommonSet.DateEnd+1;
    quCardCli.Parameters.ParamByName('ART1').Value:=1;
    quCardCli.Parameters.ParamByName('ART2').Value:=1000000;
    quCardCli.Active:=True;
    fmCardCli.ViewCardCli.EndUpdate;

    fmCardCli.Caption:=taClientsName.AsString+'. �������� ������� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
    sCurPost:=taClientsName.AsString;

    fmCardCli.Show;
  end;
end;

procedure TfmClients1.acTestChangePriceInExecute(Sender: TObject);
begin
 //�������� ��������� ���������� ����
  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit2.Date);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);
    with dmC do
    begin

      quChangeViewPriceIn.Active:=False;
      quChangeViewPriceIn.SQL.Clear;
      quChangeViewPriceIn.SQL.Add('ALTER VIEW dbo.vPriceIn');
      quChangeViewPriceIn.SQL.Add('AS');
      quChangeViewPriceIn.SQL.Add('SELECT     DocType, Articul, Id_Client, PriceIn');
      quChangeViewPriceIn.SQL.Add('FROM         dbo.TTNLn sp');
      quChangeViewPriceIn.SQL.Add('WHERE DocType=1 and (DocDate >= '''+FormatDateTime('yyyymmdd',CommonSet.DateBeg)+ ''') AND (DocDate < '''+FormatDateTime('yyyymmdd',CommonSet.DateEnd+1)+''')');
      quChangeViewPriceIn.SQL.Add('GROUP BY DocType, Articul, Id_Client, PriceIn');
      quChangeViewPriceIn.ExecSQL;

      delay(100);

      quSelClientsChPr.Active:=False;
      quSelClientsChPr.Active:=True;

      fmCliChPr.Caption:='���������� ���������� ���������� ���� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      fmCliChPr.Tag:=0;
      fmCliChPr.Show;

    end;
  end;
end;

procedure TfmClients1.acPrintCliExecute(Sender: TObject);
begin
//������
  quClients.Active:=False;
  quClients.Active:=True;

  frRepCli.LoadFromFile(CurDir + 'Clients.frf');

  frRepCli.ReportName:='������ �����������.';
  frRepCli.PrepareReport;
  frRepCli.ShowPreparedReport;

{
      frRep1.LoadFromFile(CurDir + 'SaleGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRep1.ReportName:='���������� �� �������.';
      frRep1.PrepareReport;
      frRep1.ShowPreparedReport;
}



end;

procedure TfmClients1.frtaClientsClose(Sender: TObject);
begin
  quClients.Active:=False;
end;

procedure TfmClients1.acTestChangePriceInOneExecute(Sender: TObject);
Var IdCl:INteger;
begin
//�������� �� ������ ����������
 //�������� ��������� ���������� ����
  fmPeriod1:=TfmPeriod1.Create(Application);

  fmPeriod1.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit2.Date);
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    IdCl:=dmC.taClientsId.AsInteger;
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);
    with dmC do
    begin

      quChangeViewPriceIn.Active:=False;
      quChangeViewPriceIn.SQL.Clear;
      quChangeViewPriceIn.SQL.Add('ALTER VIEW dbo.vPriceIn');
      quChangeViewPriceIn.SQL.Add('AS');
      quChangeViewPriceIn.SQL.Add('SELECT     DocType, Articul, Id_Client, PriceIn');
      quChangeViewPriceIn.SQL.Add('FROM         dbo.TTNLn sp');
      quChangeViewPriceIn.SQL.Add('WHERE DocType=1 and (DocDate >= '''+FormatDateTime('yyyymmdd',CommonSet.DateBeg)+ ''') AND (DocDate < '''+FormatDateTime('yyyymmdd',CommonSet.DateEnd+1)+''')');
      quChangeViewPriceIn.SQL.Add('and Id_Client='+IntToStr(IdCl));

      if fmPeriod1.CheckBox5.Checked=False then
        quChangeViewPriceIn.SQL.Add('and Id_Shop='+IntToStr(fmPeriod1.cxLookupComboBox4.EditValue));

      quChangeViewPriceIn.SQL.Add('GROUP BY DocType, Articul, Id_Client, PriceIn');
      quChangeViewPriceIn.ExecSQL;

      delay(100);

      quSelClientsChPr.Active:=False;
      quSelClientsChPr.Active:=True;

      if fmPeriod1.CheckBox5.Checked then
      begin
        fmCliChPr.Caption:='���������:'+dmC.taClientsName.AsString+' ��������� ���������� ���� �� ������ '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
        fmCliChPr.GridCardsChPr.Tag:=0;
      end
      else
      begin
        fmCliChPr.Caption:='���������:'+dmC.taClientsName.AsString+' ��������� ���������� ���� �� ������ '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd)+' �� �������� '+PeriodPar.Shop;
        fmCliChPr.GridCardsChPr.Tag:=fmPeriod1.cxLookupComboBox4.EditValue;
      end;

      fmCliChPr.Tag:=IdCl;
      fmCliChPr.Show;
    end;
    fmPeriod1.Release;
  end;
end;

procedure TfmClients1.acAddPostExecute(Sender: TObject);
Var StrWk:String;
begin
//�������� ����������
  if not CanDo('prAddClients') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmC do
  begin
    fmAddPost:=tfmAddPost.Create(Application);
    with fmAddPost do
    begin
      Caption:='���������� ����������.';
      cxTextEdit1.Text:='����� ���������';
      cxTextEdit2.Text:='����� ���������';
      cxTextEdit3.Text:='';
      cxImageComboBox1.EditValue:=1;
      cxTextEdit4.Text:=''; //���
      cxTextEdit5.Text:='620000'; //
      cxTextEdit6.Text:='������������';
      cxTextEdit7.Text:='';
      cxTextEdit8.Text:='';

      cxTextEdit9.Text:='';
      cxTextEdit10.Text:='';
      cxTextEdit11.Text:='';
      cxTextEdit12.Text:='';
      cxTextEdit13.Text:='';
      cxTextEdit14.Text:='';

      cxTextEdit15.Text:='';
      cxDateEdit1.Date:=strtodate('01.01.2010');
      cxCheckBox1.Checked:=False;


    end;
    fmAddPost.ShowModal;
    if fmAddPost.ModalResult=mrOk then
    begin
      //��� ���� ��������� � ���������
      //1.��������
      ViewCli.BeginUpdate;
      StrWk:=fmAddPost.cxTextEdit3.Text;
      While Pos(' ',strwk)>0 do Delete(StrWk,Pos(' ',strwk),1);
      If StrWk>'' then
      begin
        if taClients.Locate('INN',StrWk,[]) then
        begin
          showmessage('��������� � ��� '+StrWk+' ��� ���� - '+taClientsName.AsString+'. ���������� ����������.');
        end else
        begin
          //��� ����� ��������
          taClients.Append;
          taClientsINN.AsString:=StrWk;
          taClientsName.AsString:=fmAddPost.cxTextEdit1.Text;
          taClientsFullName.AsString:=fmAddPost.cxTextEdit2.Text;
          taClientsIType.AsInteger:=fmAddPost.cxImageComboBox1.EditValue;
          taClientsIActive.AsInteger:=1;
          taClientsKPP.AsString:=fmAddPost.cxTextEdit4.Text;
          taClientsPostIndex.AsString:=fmAddPost.cxTextEdit5.Text;
          taClientsGorod.AsString:=fmAddPost.cxTextEdit6.Text;
          taClientsStreet.AsString:=fmAddPost.cxTextEdit7.Text;
          taClientsHouse.AsString:=fmAddPost.cxTextEdit8.Text;

          taClientsNAMEOTP.AsString:=fmAddPost.cxTextEdit9.text;
          taClientsADROTPR.AsString:=fmAddPost.cxTextEdit10.text;
          taClientsRSch.AsString:=fmAddPost.cxTextEdit11.text;
          taClientsKSch.AsString:=fmAddPost.cxTextEdit14.text;
          taClientsBank.AsString:=fmAddPost.cxTextEdit13.text;
          taClientsBik.AsString:=fmAddPost.cxTextEdit12.text;

          taClientsDogNum.AsString:=fmAddPost.cxTextEdit15.Text;
          taClientsDogDate.AsDateTime:=Trunc(fmAddPost.cxDateEdit1.Date);
          taClientsNotUsePriceDifferentRepot.AsBoolean:=fmAddPost.cxCheckBox1.Checked;

          taClients.Post;

          writepr(taClientsId.AsInteger,taClientsINN.AsString,'Clients',0); //���������� ����������

        end;
      end else
      begin
        ShowMessage('���� ��� ������ ���� ���������. ���������� ���������!')
      end;
      ViewCli.EndUpdate;
    end;
    fmAddPost.Release;
  end;
end;

procedure TfmClients1.acEditClientsExecute(Sender: TObject);
Var StrWk:String;
    Id:INteger;
    sInn:String;
begin
//������������� ����������
  if not CanDo('prEditClients') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmC do
  begin
    if taClients.Eof then exit;
    Id:=taClientsId.AsInteger;
    sInn:=taClientsINN.AsString;

    fmAddPost:=tfmAddPost.Create(Application);
    with fmAddPost do
    begin
      Caption:='�������������� ����������.';
      cxTextEdit1.Text:=taClientsName.AsString;
      cxTextEdit2.Text:=taClientsFullName.AsString;
      cxTextEdit3.Text:=taClientsINN.AsString;
      cxImageComboBox1.EditValue:=taClientsIType.AsInteger;
      cxTextEdit4.Text:=taClientsKPP.AsString; //���
      cxTextEdit5.Text:=taClientsPostIndex.AsString; //
      cxTextEdit6.Text:=taClientsGorod.AsString;
      cxTextEdit7.Text:=taClientsStreet.AsString;
      cxTextEdit8.Text:=taClientsHouse.AsString;

      cxTextEdit9.Text:=taClientsNAMEOTP.AsString;
      cxTextEdit10.Text:=taClientsADROTPR.AsString;
      cxTextEdit11.Text:=taClientsRSch.AsString;
      cxTextEdit14.Text:=taClientsKSch.AsString;
      cxTextEdit13.Text:=taClientsBank.AsString;
      cxTextEdit12.Text:=taClientsBik.AsString;
      cxTextEdit15.Text:=taClientsDogNum.AsString;
      if formatdatetime('dd.mm.yyyy',taClientsDogDate.AsDateTime)<>'30.12.1899' then cxDateEdit1.Date:=taClientsDogDate.AsDateTime else cxDateEdit1.Date:=strtodate('01.01.2010');
      cxCheckBox1.Checked:=taClientsNotUsePriceDifferentRepot.AsBoolean;


    end;
    fmAddPost.ShowModal;
    if fmAddPost.ModalResult=mrOk then
    begin
      //��� ���� ��������� � ���������
      //1.��������
      ViewCli.BeginUpdate;
      StrWk:=fmAddPost.cxTextEdit3.Text;
      While Pos(' ',strwk)>0 do Delete(StrWk,Pos(' ',strwk),1);
      If StrWk>'' then
      begin
        if StrWk<>sInn then
        begin
          if taClients.Locate('INN',StrWk,[]) then
          begin
            showmessage('��������� � ��� '+StrWk+' ��� ���� - '+taClientsName.AsString+'. ��������� ����������.');
            taClients.Locate('Id',Id,[]);
          end else
          begin
            //��� ����� ���������
            taClients.Edit;
            taClientsINN.AsString:=StrWk;
            taClientsName.AsString:=fmAddPost.cxTextEdit1.Text;
            taClientsFullName.AsString:=fmAddPost.cxTextEdit2.Text;
            taClientsIType.AsInteger:=fmAddPost.cxImageComboBox1.EditValue;
            taClientsKPP.AsString:=fmAddPost.cxTextEdit4.Text;
            taClientsPostIndex.AsString:=fmAddPost.cxTextEdit5.Text;
            taClientsGorod.AsString:=fmAddPost.cxTextEdit6.Text;
            taClientsStreet.AsString:=fmAddPost.cxTextEdit7.Text;
            taClientsHouse.AsString:=fmAddPost.cxTextEdit8.Text;

            taClientsNAMEOTP.AsString:=fmAddPost.cxTextEdit9.Text;
            taClientsADROTPR.AsString:=fmAddPost.cxTextEdit10.Text;
            taClientsRSch.AsString:=fmAddPost.cxTextEdit11.Text;
            taClientsKSch.AsString:=fmAddPost.cxTextEdit14.Text;
            taClientsBank.AsString:=fmAddPost.cxTextEdit13.Text;
            taClientsBik.AsString:=fmAddPost.cxTextEdit12.Text;

            taClientsDogNum.AsString:=fmAddPost.cxTextEdit15.Text;
            taClientsDogDate.AsDateTime:=Trunc(fmAddPost.cxDateEdit1.Date);
            taClientsNotUsePriceDifferentRepot.AsBoolean:=fmAddPost.cxCheckBox1.Checked;

            taClients.Post;

            writepr(taClientsId.AsInteger,taClientsINN.AsString,'Clients',1); //�������������� ����������
          end;
        end else //��� �� ���������, ������ ���������
        begin
          taClients.Edit;
          taClientsINN.AsString:=StrWk;
          taClientsName.AsString:=fmAddPost.cxTextEdit1.Text;
          taClientsFullName.AsString:=fmAddPost.cxTextEdit2.Text;
          taClientsIType.AsInteger:=fmAddPost.cxImageComboBox1.EditValue;
          taClientsKPP.AsString:=fmAddPost.cxTextEdit4.Text;
          taClientsPostIndex.AsString:=fmAddPost.cxTextEdit5.Text;
          taClientsGorod.AsString:=fmAddPost.cxTextEdit6.Text;
          taClientsStreet.AsString:=fmAddPost.cxTextEdit7.Text;
          taClientsHouse.AsString:=fmAddPost.cxTextEdit8.Text;

          taClientsNAMEOTP.AsString:=fmAddPost.cxTextEdit9.Text;
          taClientsADROTPR.AsString:=fmAddPost.cxTextEdit10.Text;
          taClientsRSch.AsString:=fmAddPost.cxTextEdit11.Text;
          taClientsKSch.AsString:=fmAddPost.cxTextEdit14.Text;
          taClientsBank.AsString:=fmAddPost.cxTextEdit13.Text;
          taClientsBik.AsString:=fmAddPost.cxTextEdit12.Text;

          taClientsDogNum.AsString:=fmAddPost.cxTextEdit15.Text;
          taClientsDogDate.AsDateTime:=trunc(fmAddPost.cxDateEdit1.Date);
          taClientsNotUsePriceDifferentRepot.AsBoolean:=fmAddPost.cxCheckBox1.Checked;

          taClients.Post;

          writepr(taClientsId.AsInteger,taClientsINN.AsString,'Clients',1); //�������������� ����������
        end;
      end else
      begin
        ShowMessage('���� ��� ������ ���� ���������. ���������� ���������!')
      end;
      ViewCli.EndUpdate;
    end;
    fmAddPost.Release;
  end;
end;

procedure TfmClients1.acDelPostExecute(Sender: TObject);
begin
//������� ����������
  if not CanDo('prOffClients') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmC do
  begin
    if taClients.Eof then exit;
    if taClientsIActive.AsInteger=0 then exit;
    ViewCli.BeginUpdate;
    taClients.Edit;
    taClientsIActive.AsInteger:=0;
    taClients.Post;

    writepr(taClientsId.AsInteger,taClientsINN.AsString,'Clients',2); //�������������� ����������
    ViewCli.EndUpdate;
  end;
end;

procedure TfmClients1.acOnPostExecute(Sender: TObject);
begin
//������������ ����������
  if not CanDo('prOnClients') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmC do
  begin
    if taClients.Eof then exit;
    if taClientsIActive.AsInteger=1 then exit;
    ViewCli.BeginUpdate;
    taClients.Edit;
    taClientsIActive.AsInteger:=1;
    taClients.Post;
    writepr(taClientsId.AsInteger,taClientsINN.AsString,'Clients',0); //�������������� ����������
    ViewCli.EndUpdate;
  end;
end;

procedure TfmClients1.ViewCliCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);

Var i:Integer;
    sType:String;

begin
//����������
  sType:='1';
  for i:=0 to ViewCli.ColumnCount-1 do
  begin
    if ViewCli.Columns[i].Name='ViewCliIActive' then
    begin
      try
//        sType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
        sType:=AViewInfo.GridRecord.DisplayTexts[i];
        break;
      except
      end;  
    end;
  end;

  if sType='0'  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmClients1.acTrListExecute(Sender: TObject);
var i,j:Integer;
    CardN:String;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if not CanDo('prTransListCl') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  //�������� �������
  with dmC do
  begin
    if MessageDlg('��������: '+INtToStr(ViewCli.Controller.SelectedRecordCount)+' ����� ��� ��������. �������� ?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      for i:=0 to ViewCli.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewCli.Controller.SelectedRecords[i];

        iNum:=0; CardN:='';
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewCli.Columns[j].Name='ViewCliId' then iNum :=Rec.Values[j]; //��� ��� - ����������
          if ViewCli.Columns[j].Name='ViewCliINN' then CardN :=Rec.Values[j]; //��� ���
        end;

        if iNum>0 then WritePr(iNum,CardN,'Clients',1);
      end;
      Showmessage('�������� ��������� �������.');
      StatusBar1.Panels[0].Text:='';
    end;
  end;
end;

procedure TfmClients1.SpeedItem7Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    StrWk:String;
    iCode:Integer;
    iC,iM:Integer;
    rPr1,rPr2:Real;
begin
  fmPeriod1:=TfmPeriod1.Create(Application);

  fmPeriod1.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit2.Date);
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.DateEdit2.Date);

    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');

    ExcelApp.Application.EnableEvents := false;
  //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;
    //WB:= ExcelApp.WorkBooks.Add;
    //XL.Connect;
    //WB.ConnectTo(ExcelApp.Workbooks.Add(EmptyParam,EmptyParam));
    //ASheet.ConnectTo(WB.ActiveSheet as _WorkSheet);

    with dmC do
    begin
      quSelClientsSpec.Active:=False;

      quSelClientsSpec.SQL.Clear;
      quSelClientsSpec.SQL.Add('SELECT tt.Articul, gd.BarCode, gd.Name, gd.FullName ');
      quSelClientsSpec.SQL.Add(', isnull(br.NameBrand,''���'') as NameBrand');
      quSelClientsSpec.SQL.Add(', case when isnull(gd.IsTender,0)=1 then ''������''');
      quSelClientsSpec.SQL.Add('  else case when gd.EdIzm=1 then ''��.'' else ''��.'' end end EdIzm ');
      quSelClientsSpec.SQL.Add(', c.Name as NameCountry');
      quSelClientsSpec.SQL.Add(', gd.NDS,0.00 as Price');
      quSelClientsSpec.SQL.Add('FROM	dbo.TTNLn tt');
      quSelClientsSpec.SQL.Add('left join dbo.Goods gd on tt.Articul=gd.Id');
      quSelClientsSpec.SQL.Add('left join dbo.Brands br on gd.Brand=br.Id');
      quSelClientsSpec.SQL.Add('left join dbo.Country c on gd.Country=c.Id');
      quSelClientsSpec.SQL.Add('WHERE	(tt.DocType = 1)');
      quSelClientsSpec.SQL.Add('and tt.Articul!=0 AND (tt.DocDate >= '''+FormatDateTime('yyyymmdd',CommonSet.DateBeg)+''')');
      quSelClientsSpec.SQL.Add('AND (tt.DocDate < '''+FormatDateTime('yyyymmdd',CommonSet.DateEnd)+''')');
      quSelClientsSpec.SQL.Add('AND (tt.Id_Client = '+taClientsId.AsString+')');
      if fmPeriod1.CheckBox5.Checked=False  then
        quSelClientsSpec.SQL.Add('AND (tt.Id_Shop = '+IntToStr(fmPeriod1.cxLookupComboBox4.EditValue)+')');
      quSelClientsSpec.SQL.Add('GROUP BY tt.Articul, gd.BarCode, gd.Name, gd.FullName, br.NameBrand, gd.EdIzm, c.Name, gd.NDS, gd.IsTender, gd.IsTender');
      quSelClientsSpec.SQL.Add('order by gd.Name');

      quSelClientsSpec.Active:=True;

      iRow:=quSelClientsSpec.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+8, 1, 19], varVariant);


      quSelClientsSpec2.Active:=False;
      quSelClientsSpec2.SQL.Clear;

      quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
      quSelClientsSpec2.SQL.Add('declare @Id_Client int');
      quSelClientsSpec2.SQL.Add('set @Id_Client='+taClientsId.AsString);

      quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'' '' FROM [dbo].[TTN] t');
      quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
      quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
      quSelClientsSpec2.SQL.Add('    and [DocType]=1');
      quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
      quSelClientsSpec2.SQL.Add('  group by d.Name');
      quSelClientsSpec2.SQL.Add('  order by d.Name');

      quSelClientsSpec2.SQL.Add('select ''(''+@str+'')'' as Departs,isnull(DogNum,''___________'') as DogNum,DogDate from dbo.Clients where Id=@Id_Client');
      quSelClientsSpec2.Active:=True;

      //ArrayData[1,2]:='���������� �1';
      //ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      //ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������"';
      //ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';
      ArrayData[1,2]:='���������� �1';
      if (FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime))<>'30.12.1899'
        then ArrayData[2,2]:='������������ � �������� � '+taClientsDogNum.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime)+' �.'
        else ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������" '+quSelClientsSpec2.FieldByName('Departs').AsString;
      ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';

      quSelClientsSpec2.Active:=False;


      {ArrayData[6,1]:='���� ���������� �������';
      ArrayData[6,2]:='��� ������ (�� ��)';
      ArrayData[6,3]:='��������';
      ArrayData[6,4]:='������������';
      ArrayData[6,5]:='������ ������������';      ///!!!!!!
      ArrayData[6,6]:='���/ ������ ��-�� ������';
      ArrayData[6,7]:='��. ���.';
      ArrayData[6,8]:='�����';
      ArrayData[6,9]:='������ ������������� (�����)';
      ArrayData[6,10]:='��� ������� �������� ��-�� ������';
      ArrayData[6,11]:='���� ������ � ���';
      ArrayData[6,12]:='���� �����';
      ArrayData[6,13]:='% ���������';
      ArrayData[6,14]:='������ ���,%';
      ArrayData[6,15]:='�����, ��';
      ArrayData[6,16]:='������ (�������), ��.';
      ArrayData[6,17]:='������, ��.';
      ArrayData[6,18]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,19]:='������� ������ �� ����� ��������';}

      ArrayData[6,1]:='��� ������ (�� ��)';
      ArrayData[6,2]:='��������';
      ArrayData[6,3]:='������������';      ///!!!!!!
      ArrayData[6,4]:='��. ���.';
      ArrayData[6,5]:='�����';
      ArrayData[6,6]:='������ ������������� (�����)';
      ArrayData[6,7]:='��� ������� �������� ��-�� ������';
      ArrayData[6,8]:='���� ������ � ���';
      ArrayData[6,9]:='���� �����';
      ArrayData[6,10]:='% ���������';
      ArrayData[6,11]:='������ ���,%';
      ArrayData[6,12]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,13]:='������� ������ �� ����� ��������';
      ArrayData[6,14]:='���� ������������� ��';



      ArrayData[iRow+8,2]:='���������';
      ArrayData[iRow+8,11]:='����������';

      fmClients.PBar1.Position:=0;
      fmClients.PBar1.Visible:=True; delay(10);
      iM:=Trunc(iRow/20);
      if iM=0 then iM:=1;
      iC:=0;

      quSelClientsSpec.First;
      while not quSelClientsSpec.Eof do
      begin
        iCode:=quSelClientsSpecArticul.AsInteger;
        if fmPeriod1.CheckBox5.Checked=False then prFindLPrSpec1(iCode,dmC.taClientsId.AsInteger,fmPeriod1.cxLookupComboBox4.EditValue,rPr1,rPr2,StrWk)
          else prFindLPrSpec1(iCode,dmC.taClientsId.AsInteger,0,rPr1,rPr2,StrWk);


        {ArrayData[iC+7,1] := StrWk;
        ArrayData[iC+7,2] := quSelClientsSpecArticul.AsInteger;
        ArrayData[iC+7,3] :='"'+ quSelClientsSpecBarCode.AsString+'"';
        ArrayData[iC+7,4] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecFullName.AsString;
        ArrayData[iC+7,7] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,8] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,9] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,11] := FloatToStr(rPr1);
        ArrayData[iC+7,14] := quSelClientsSpecNDS.AsInteger;}

        //ArrayData[iC+7,1] := StrWk;
        ArrayData[iC+7,1] := quSelClientsSpecArticul.AsInteger;
        ArrayData[iC+7,2] := quSelClientsSpecBarCode.AsString;
        //ArrayData[iC+7,4] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,3] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,4] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,6] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,8] := FloatToStr(rPr1);
        ArrayData[iC+7,11] := quSelClientsSpecNDS.AsInteger;




        quSelClientsSpec.Next; inc(iC);
        if iC mod iM = 0 then
        begin
          fmClients.PBar1.Position:=Trunc((iC/(iM*20))*100);
          Delay(10);
        end;
      end;
      quSelClientsSpec.Active:=False;
      fmClients.PBar1.Position:=100;
    //delay(300);
      fmClients.PBar1.Visible:=False;
    end;





    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    Cell1 := WorkBook.WorkSheets[1].Cells[7,10];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,10];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Formula:='=IF(I7>0,(I7-H7)/H7,"")';
    //Range.Se := '0.00%';

    Cell1 := WorkBook.WorkSheets[1].Cells[7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    Cell1 := WorkBook.WorkSheets[1].Cells[iRow+7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

    WorkBook.WorkSheets[1].Cells[1,3].Font.Size := 8;

    WorkBook.WorkSheets[1].Cells[2,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[3,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[3,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[4,2].Font.Size := 8;
    WorkBook.WorkSheets[1].Cells[5,2].Font.Size := 8;


    //WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=8;
    WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=10;
    WorkBook.WorkSheets[1].Columns[2].NumberFormat :='#';
    //WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=30;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=30;

    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[5].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[7].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[8].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[9].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0,00%';
    WorkBook.WorkSheets[1].Columns[11].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[15].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[16].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[17].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[12].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[13].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[14].ColumnWidth:=7;
    WorkBook.WorkSheets[1].Columns[14].NumberFormat := '��.��.����';


    WorkBook.WorkSheets[1].Rows[6].RowHeight:=50;

    Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.WrapText:=True;
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=3;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    ExcelApp.Visible := true;
  end;
  fmPeriod1.Release;

end;


procedure TfmClients1.SpeedItem8Click(Sender: TObject);
begin
  prNExportExel4(ViewCli);
end;

procedure TfmClients1.SpeedItem9Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    StrWk:String;
    iCode:Integer;
    iC,iM:Integer;
    rPr1,rPr2:Real;
begin
{  fmPeriod1:=TfmPeriod1.Create(Application);

  fmPeriod1.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit2.Date);
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then}
  begin
{    CommonSet.DateBeg:=Trunc(fmPeriod1.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.DateEdit2.Date);}

    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');

    ExcelApp.Application.EnableEvents := false;
  //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    with dmC do
    begin
      quSelClientsSpec.Active:=False;

      quSelClientsSpec.SQL.Clear;
      quSelClientsSpec.SQL.Add('select ');
      quSelClientsSpec.SQL.Add('         t.Articul, gd.BarCode, gd.Name, gd.FullName');
      quSelClientsSpec.SQL.Add('       , isnull(br.NameBrand,''���'') as NameBrand');
      quSelClientsSpec.SQL.Add('       , case when isnull(gd.IsTender,0)=1 then ''������''');
      quSelClientsSpec.SQL.Add('         else case when gd.EdIzm=1 then ''��.'' else ''��.'' end end EdIzm ');
      quSelClientsSpec.SQL.Add('       , c.Name as NameCountry');
      quSelClientsSpec.SQL.Add('       , gd.NDS');
      quSelClientsSpec.SQL.Add('       ,(Select top 1 cast(Price as Decimal(10,2)) as Price from dbo.ReestrDoc l inner join  dbo.ReestrD h on l.ReestrD_Id=h.ID');
      quSelClientsSpec.SQL.Add('         where h.Id_Client=t.Id_Client /*and h.DateB=t.[DateB]*/ and l.Price!=0 and l.Articul=t.Articul order by h.DateB desc) as Price');
      quSelClientsSpec.SQL.Add('       from (');
      quSelClientsSpec.SQL.Add('  SELECT');
      quSelClientsSpec.SQL.Add('       max([DateB]) as [DateB]');
      quSelClientsSpec.SQL.Add('      ,l.Articul');
      quSelClientsSpec.SQL.Add('      ,h.Id_Client');
      quSelClientsSpec.SQL.Add('  FROM [dbo].[ReestrD] h inner join dbo.ReestrDoc l on h.ID=l.ReestrD_Id');
      quSelClientsSpec.SQL.Add('  where h.Id_Client='+taClientsId.AsString+' and [iActive] in (1) and l.DatePriceOut is null');
      quSelClientsSpec.SQL.Add('  group by l.Articul,h.Id_Client) t');
      quSelClientsSpec.SQL.Add('    left join dbo.Goods gd on t.Articul=gd.Id');
      quSelClientsSpec.SQL.Add('    left join dbo.Brands br on gd.Brand=br.Id');
      quSelClientsSpec.SQL.Add('    left join dbo.Country c on gd.Country=c.Id');
      quSelClientsSpec.SQL.Add('    order by gd.Name ');

      //quSelClientsSpec.SQL.SaveToFile('c:\1.sql');

      quSelClientsSpec.Active:=True;

      iRow:=quSelClientsSpec.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+8, 1, 19], varVariant);


      quSelClientsSpec2.Active:=False;
      quSelClientsSpec2.SQL.Clear;

      quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
      quSelClientsSpec2.SQL.Add('declare @Id_Client int');
      quSelClientsSpec2.SQL.Add('set @Id_Client='+taClientsId.AsString);

      quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'' '' FROM [dbo].[TTN] t');
      quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
      quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
      quSelClientsSpec2.SQL.Add('    and [DocType]=1');
      quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
      quSelClientsSpec2.SQL.Add('  group by d.Name');
      quSelClientsSpec2.SQL.Add('  order by d.Name');

      quSelClientsSpec2.SQL.Add('select ''(''+@str+'')'' as Departs,isnull(DogNum,''___________'') as DogNum,DogDate from dbo.Clients where Id=@Id_Client');
      quSelClientsSpec2.Active:=True;

      ArrayData[1,2]:='���������� �1';
      if (FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime))<>'30.12.1899'
        then ArrayData[2,2]:='������������ � �������� � '+taClientsDogNum.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime)+' �.'
        else ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������" '+quSelClientsSpec2.FieldByName('Departs').AsString;
      ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';

      quSelClientsSpec2.Active:=False;

      ArrayData[6,1]:='��� ������ (�� ��)';
      ArrayData[6,2]:='��������';
      ArrayData[6,3]:='������������';      ///!!!!!!
      ArrayData[6,4]:='��. ���.';
      ArrayData[6,5]:='�����';
      ArrayData[6,6]:='������ ������������� (�����)';
      ArrayData[6,7]:='��� ������� �������� ��-�� ������';
      ArrayData[6,8]:='���� ������ � ���';
      ArrayData[6,9]:='���� �����';
      ArrayData[6,10]:='% ���������';
      ArrayData[6,11]:='������ ���,%';
      ArrayData[6,12]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,13]:='������� ������ �� ����� ��������';
      ArrayData[6,14]:='���� ������������� ��';



      ArrayData[iRow+8,2]:='���������';
      ArrayData[iRow+8,11]:='����������';

      fmClients.PBar1.Position:=0;
      fmClients.PBar1.Visible:=True; delay(10);
      iM:=Trunc(iRow/20);
      if iM=0 then iM:=1;
      iC:=0;

      quSelClientsSpec.First;
      while not quSelClientsSpec.Eof do
      begin
        iCode:=quSelClientsSpecArticul.AsInteger;

        ArrayData[iC+7,1] := quSelClientsSpecArticul.AsInteger;
        //ArrayData[iC+7,2] :='"'+ quSelClientsSpecBarCode.AsString+'"';
        ArrayData[iC+7,2] := quSelClientsSpecBarCode.AsString;
        ArrayData[iC+7,3] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,4] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,6] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,8] := quSelClientsSpecPrice.AsString;
        ArrayData[iC+7,11] := quSelClientsSpecNDS.AsInteger;

        quSelClientsSpec.Next; inc(iC);
        if iC mod iM = 0 then
        begin
          fmClients.PBar1.Position:=Trunc((iC/(iM*20))*100);
          Delay(10);
        end;
      end;
      quSelClientsSpec.Active:=False;
      fmClients.PBar1.Position:=100;
    //delay(300);
      fmClients.PBar1.Visible:=False;
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    Cell1 := WorkBook.WorkSheets[1].Cells[7,10];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,10];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Formula:='=IF(I7>0,(I7-H7)*100/H7,"")';
    //Range.Se := '0.00%';

    Cell1 := WorkBook.WorkSheets[1].Cells[7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    Cell1 := WorkBook.WorkSheets[1].Cells[iRow+7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

    WorkBook.WorkSheets[1].Cells[1,3].Font.Size := 8;

    WorkBook.WorkSheets[1].Cells[2,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[3,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[3,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[4,2].Font.Size := 8;
    WorkBook.WorkSheets[1].Cells[5,2].Font.Size := 8;


    //WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=8;
    WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=10;
    WorkBook.WorkSheets[1].Columns[2].NumberFormat :='#';
    //WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=30;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=30;

    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[5].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[7].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[8].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[9].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0,00%';
    WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0.00';
    WorkBook.WorkSheets[1].Columns[11].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[15].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[16].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[17].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[12].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[13].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[14].ColumnWidth:=7;
    WorkBook.WorkSheets[1].Columns[14].NumberFormat := '��.��.����';


    WorkBook.WorkSheets[1].Rows[6].RowHeight:=50;

    Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.WrapText:=True;
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=3;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    ExcelApp.Visible := true;
  end;

end;

procedure TfmClients1.N9Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    StrWk:String;
    iCode:Integer;
    iC,iM:Integer;
    rPr1,rPr2:Real;
begin
{  fmPeriod1:=TfmPeriod1.Create(Application);

  fmPeriod1.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit2.Date);
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then}
  begin
{    CommonSet.DateBeg:=Trunc(fmPeriod1.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.DateEdit2.Date);}

    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');

    ExcelApp.Application.EnableEvents := false;
  //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    with dmC do
    begin
      quSelClientsSpec.Active:=False;

      quSelClientsSpec.SQL.Clear;
      quSelClientsSpec.SQL.Add('select');
      quSelClientsSpec.SQL.Add('         t.Articul');
      quSelClientsSpec.SQL.Add('         ,b.BarCode');
      quSelClientsSpec.SQL.Add('         ,gd.Name, gd.FullName');
      quSelClientsSpec.SQL.Add('       , isnull(br.NameBrand,''���'') as NameBrand');
      quSelClientsSpec.SQL.Add('       , case when isnull(gd.IsTender,0)=1 then ''������''');
      quSelClientsSpec.SQL.Add('         else case when gd.EdIzm=1 then ''��.'' else ''��.'' end end EdIzm');
      quSelClientsSpec.SQL.Add('       , c.Name as NameCountry');
      quSelClientsSpec.SQL.Add('       , gd.NDS');
      quSelClientsSpec.SQL.Add('       ,(Select top 1 cast(Price as Decimal(10,2)) as Price from dbo.ReestrDoc l inner join  dbo.ReestrD h on l.ReestrD_Id=h.ID');
      quSelClientsSpec.SQL.Add('         where h.Id_Client=t.Id_Client /*and h.DateB=t.[DateB]*/ and l.Price!=0 and l.Articul=t.Articul order by h.DateB desc) as Price');
      quSelClientsSpec.SQL.Add('       from (');
      quSelClientsSpec.SQL.Add('  SELECT');
      quSelClientsSpec.SQL.Add('       max([DateB]) as [DateB]');
      quSelClientsSpec.SQL.Add('      ,l.Articul');
      quSelClientsSpec.SQL.Add('      ,h.Id_Client');
      quSelClientsSpec.SQL.Add('  FROM [dbo].[ReestrD] h inner join dbo.ReestrDoc l on h.ID=l.ReestrD_Id');
      quSelClientsSpec.SQL.Add('  where h.Id_Client='+taClientsId.AsString+' and [iActive] in (1) and l.DatePriceOut is null');

      quSelClientsSpec.SQL.Add('  group by l.Articul,h.Id_Client) t');
      quSelClientsSpec.SQL.Add('    left join dbo.Goods gd on t.Articul=gd.Id');
      quSelClientsSpec.SQL.Add('    left join dbo.Brands br on gd.Brand=br.Id');
      quSelClientsSpec.SQL.Add('    left join dbo.Country c on gd.Country=c.Id');
      quSelClientsSpec.SQL.Add('    left join (select BarCode, Articul from (');
      quSelClientsSpec.SQL.Add('                    select * from (');
      quSelClientsSpec.SQL.Add('                        SELECT [Bar] as BarCode,[GoodsId] as Articul FROM [dbo].[BarCode]');
      quSelClientsSpec.SQL.Add('                        union all');
      quSelClientsSpec.SQL.Add('                        select BarCode,Id as Articul from dbo.Goods');
      quSelClientsSpec.SQL.Add('                        ) t group by BarCode,Articul) tt) b on b.Articul=t.Articul');
      quSelClientsSpec.SQL.Add('    order by gd.Name');

      //quSelClientsSpec.SQL.SaveToFile('c:\1.sql');

      quSelClientsSpec.Active:=True;

      iRow:=quSelClientsSpec.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+8, 1, 19], varVariant);


      quSelClientsSpec2.Active:=False;
      quSelClientsSpec2.SQL.Clear;

      quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
      quSelClientsSpec2.SQL.Add('declare @Id_Client int');
      quSelClientsSpec2.SQL.Add('set @Id_Client='+taClientsId.AsString);

      quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'' '' FROM [dbo].[TTN] t');
      quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
      quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
      quSelClientsSpec2.SQL.Add('    and [DocType]=1');
      quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
      quSelClientsSpec2.SQL.Add('  group by d.Name');
      quSelClientsSpec2.SQL.Add('  order by d.Name');

      quSelClientsSpec2.SQL.Add('select ''(''+@str+'')'' as Departs,isnull(DogNum,''___________'') as DogNum,DogDate from dbo.Clients where Id=@Id_Client');
      quSelClientsSpec2.Active:=True;

      ArrayData[1,2]:='���������� �1';
      if (FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime))<>'30.12.1899'
        then ArrayData[2,2]:='������������ � �������� � '+taClientsDogNum.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime)+' �.'
        else ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������" '+quSelClientsSpec2.FieldByName('Departs').AsString;
      ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';

      quSelClientsSpec2.Active:=False;

      ArrayData[6,1]:='��� ������ (�� ��)';
      ArrayData[6,2]:='��������';
      ArrayData[6,3]:='������������';      ///!!!!!!
      ArrayData[6,4]:='��. ���.';
      ArrayData[6,5]:='�����';
      ArrayData[6,6]:='������ ������������� (�����)';
      ArrayData[6,7]:='��� ������� �������� ��-�� ������';
      ArrayData[6,8]:='���� ������ � ���';
      ArrayData[6,9]:='���� �����';
      ArrayData[6,10]:='% ���������';
      ArrayData[6,11]:='������ ���,%';
      ArrayData[6,12]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,13]:='������� ������ �� ����� ��������';
      ArrayData[6,14]:='���� ������������� ��';



      ArrayData[iRow+8,2]:='���������';
      ArrayData[iRow+8,11]:='����������';

      fmClients.PBar1.Position:=0;
      fmClients.PBar1.Visible:=True; delay(10);
      iM:=Trunc(iRow/20);
      if iM=0 then iM:=1;
      iC:=0;

      quSelClientsSpec.First;
      while not quSelClientsSpec.Eof do
      begin
        iCode:=quSelClientsSpecArticul.AsInteger;

        ArrayData[iC+7,1] := quSelClientsSpecArticul.AsInteger;
        //ArrayData[iC+7,2] :='"'+ quSelClientsSpecBarCode.AsString+'"';
        ArrayData[iC+7,2] := quSelClientsSpecBarCode.AsString;
        ArrayData[iC+7,3] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,4] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,6] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,8] := quSelClientsSpecPrice.AsString;
        ArrayData[iC+7,11] := quSelClientsSpecNDS.AsInteger;

        quSelClientsSpec.Next; inc(iC);
        if iC mod iM = 0 then
        begin
          fmClients.PBar1.Position:=Trunc((iC/(iM*20))*100);
          Delay(10);
        end;
      end;
      quSelClientsSpec.Active:=False;
      fmClients.PBar1.Position:=100;
    //delay(300);
      fmClients.PBar1.Visible:=False;
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    Cell1 := WorkBook.WorkSheets[1].Cells[7,10];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,10];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Formula:='=IF(I7>0,(I7-H7)/H7,"")';
    //Range.Se := '0.00%';

    Cell1 := WorkBook.WorkSheets[1].Cells[7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    Cell1 := WorkBook.WorkSheets[1].Cells[iRow+7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

    WorkBook.WorkSheets[1].Cells[1,3].Font.Size := 8;

    WorkBook.WorkSheets[1].Cells[2,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[3,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[3,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[4,2].Font.Size := 8;
    WorkBook.WorkSheets[1].Cells[5,2].Font.Size := 8;


    //WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=8;
    WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=10;
    WorkBook.WorkSheets[1].Columns[2].NumberFormat :='#';
    //WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=30;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=30;

    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[5].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[7].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[8].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[9].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0,00%';
    WorkBook.WorkSheets[1].Columns[11].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[15].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[16].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[17].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[12].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[13].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[14].ColumnWidth:=7;
    WorkBook.WorkSheets[1].Columns[14].NumberFormat := '��.��.����';


    WorkBook.WorkSheets[1].Rows[6].RowHeight:=50;

    Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.WrapText:=True;
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=3;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    ExcelApp.Visible := true;
  end;
end;

procedure TfmClients1.N10Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    StrWk:String;
    iCode:Integer;
    iC,iM:Integer;
    rPr1,rPr2:Real;
begin
  fmPeriod1:=TfmPeriod1.Create(Application);

  fmPeriod1.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.DateEdit2.Date);
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.DateEdit2.Date);

    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');

    ExcelApp.Application.EnableEvents := false;
  //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;
    //WB:= ExcelApp.WorkBooks.Add;
    //XL.Connect;
    //WB.ConnectTo(ExcelApp.Workbooks.Add(EmptyParam,EmptyParam));
    //ASheet.ConnectTo(WB.ActiveSheet as _WorkSheet);

    with dmC do
    begin
      quSelClientsSpec.Active:=False;

      quSelClientsSpec.SQL.Clear;
      quSelClientsSpec.SQL.Add('SELECT tt.Articul, b.BarCode, gd.Name, gd.FullName ');
      quSelClientsSpec.SQL.Add(', isnull(br.NameBrand,''���'') as NameBrand');
      quSelClientsSpec.SQL.Add(', case when isnull(gd.IsTender,0)=1 then ''������''');
      quSelClientsSpec.SQL.Add('  else case when gd.EdIzm=1 then ''��.'' else ''��.'' end end EdIzm ');
      quSelClientsSpec.SQL.Add(', c.Name as NameCountry');
      quSelClientsSpec.SQL.Add(', gd.NDS,0.00 as Price');
      quSelClientsSpec.SQL.Add('FROM	dbo.TTNLn tt');
      quSelClientsSpec.SQL.Add('left join dbo.Goods gd on tt.Articul=gd.Id');
      quSelClientsSpec.SQL.Add('left join dbo.Brands br on gd.Brand=br.Id');
      quSelClientsSpec.SQL.Add('left join dbo.Country c on gd.Country=c.Id');

      quSelClientsSpec.SQL.Add('left join (select BarCode, Articul from (');
      quSelClientsSpec.SQL.Add('                  select * from (');
      quSelClientsSpec.SQL.Add('                        SELECT [Bar] as BarCode,[GoodsId] as Articul FROM [dbo].[BarCode]');
      quSelClientsSpec.SQL.Add('                        union all');
      quSelClientsSpec.SQL.Add('                        select BarCode,Id as Articul from dbo.Goods');
      quSelClientsSpec.SQL.Add('                        ) t group by BarCode,Articul) tt) b on b.Articul=tt.Articul');

      quSelClientsSpec.SQL.Add('WHERE	(tt.DocType = 1)');
      quSelClientsSpec.SQL.Add('and tt.Articul!=0 AND (tt.DocDate >= '''+FormatDateTime('yyyymmdd',CommonSet.DateBeg)+''')');
      quSelClientsSpec.SQL.Add('AND (tt.DocDate < '''+FormatDateTime('yyyymmdd',CommonSet.DateEnd)+''')');
      quSelClientsSpec.SQL.Add('AND (tt.Id_Client = '+taClientsId.AsString+')');
      if fmPeriod1.CheckBox5.Checked=False  then
        quSelClientsSpec.SQL.Add('AND (tt.Id_Shop = '+IntToStr(fmPeriod1.cxLookupComboBox4.EditValue)+')');
      quSelClientsSpec.SQL.Add('GROUP BY tt.Articul, b.BarCode, gd.Name, gd.FullName, br.NameBrand, gd.EdIzm, c.Name, gd.NDS, gd.IsTender, gd.IsTender');
      quSelClientsSpec.SQL.Add('order by gd.Name');

      quSelClientsSpec.Active:=True;

      iRow:=quSelClientsSpec.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+8, 1, 19], varVariant);


      quSelClientsSpec2.Active:=False;
      quSelClientsSpec2.SQL.Clear;

      quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
      quSelClientsSpec2.SQL.Add('declare @Id_Client int');
      quSelClientsSpec2.SQL.Add('set @Id_Client='+taClientsId.AsString);

      quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'' '' FROM [dbo].[TTN] t');
      quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
      quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
      quSelClientsSpec2.SQL.Add('    and [DocType]=1');
      quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
      quSelClientsSpec2.SQL.Add('  group by d.Name');
      quSelClientsSpec2.SQL.Add('  order by d.Name');

      quSelClientsSpec2.SQL.Add('select ''(''+@str+'')'' as Departs,isnull(DogNum,''___________'') as DogNum,DogDate from dbo.Clients where Id=@Id_Client');
      quSelClientsSpec2.Active:=True;

      //ArrayData[1,2]:='���������� �1';
      //ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      //ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������"';
      //ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';
      ArrayData[1,2]:='���������� �1';
      if (FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime))<>'30.12.1899'
        then ArrayData[2,2]:='������������ � �������� � '+taClientsDogNum.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime)+' �.'
        else ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������" '+quSelClientsSpec2.FieldByName('Departs').AsString;
      ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';

      quSelClientsSpec2.Active:=False;


      {ArrayData[6,1]:='���� ���������� �������';
      ArrayData[6,2]:='��� ������ (�� ��)';
      ArrayData[6,3]:='��������';
      ArrayData[6,4]:='������������';
      ArrayData[6,5]:='������ ������������';      ///!!!!!!
      ArrayData[6,6]:='���/ ������ ��-�� ������';
      ArrayData[6,7]:='��. ���.';
      ArrayData[6,8]:='�����';
      ArrayData[6,9]:='������ ������������� (�����)';
      ArrayData[6,10]:='��� ������� �������� ��-�� ������';
      ArrayData[6,11]:='���� ������ � ���';
      ArrayData[6,12]:='���� �����';
      ArrayData[6,13]:='% ���������';
      ArrayData[6,14]:='������ ���,%';
      ArrayData[6,15]:='�����, ��';
      ArrayData[6,16]:='������ (�������), ��.';
      ArrayData[6,17]:='������, ��.';
      ArrayData[6,18]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,19]:='������� ������ �� ����� ��������';}

      ArrayData[6,1]:='��� ������ (�� ��)';
      ArrayData[6,2]:='��������';
      ArrayData[6,3]:='������������';      ///!!!!!!
      ArrayData[6,4]:='��. ���.';
      ArrayData[6,5]:='�����';
      ArrayData[6,6]:='������ ������������� (�����)';
      ArrayData[6,7]:='��� ������� �������� ��-�� ������';
      ArrayData[6,8]:='���� ������ � ���';
      ArrayData[6,9]:='���� �����';
      ArrayData[6,10]:='% ���������';
      ArrayData[6,11]:='������ ���,%';
      ArrayData[6,12]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,13]:='������� ������ �� ����� ��������';
      ArrayData[6,14]:='���� ������������� ��';



      ArrayData[iRow+8,2]:='���������';
      ArrayData[iRow+8,11]:='����������';

      fmClients.PBar1.Position:=0;
      fmClients.PBar1.Visible:=True; delay(10);
      iM:=Trunc(iRow/20);
      if iM=0 then iM:=1;
      iC:=0;

      quSelClientsSpec.First;
      while not quSelClientsSpec.Eof do
      begin
        iCode:=quSelClientsSpecArticul.AsInteger;
        if fmPeriod1.CheckBox5.Checked=False then prFindLPrSpec1(iCode,dmC.taClientsId.AsInteger,fmPeriod1.cxLookupComboBox4.EditValue,rPr1,rPr2,StrWk)
          else prFindLPrSpec1(iCode,dmC.taClientsId.AsInteger,0,rPr1,rPr2,StrWk);


        {ArrayData[iC+7,1] := StrWk;
        ArrayData[iC+7,2] := quSelClientsSpecArticul.AsInteger;
        ArrayData[iC+7,3] :='"'+ quSelClientsSpecBarCode.AsString+'"';
        ArrayData[iC+7,4] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecFullName.AsString;
        ArrayData[iC+7,7] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,8] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,9] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,11] := FloatToStr(rPr1);
        ArrayData[iC+7,14] := quSelClientsSpecNDS.AsInteger;}

        //ArrayData[iC+7,1] := StrWk;
        ArrayData[iC+7,1] := quSelClientsSpecArticul.AsInteger;
        ArrayData[iC+7,2] := quSelClientsSpecBarCode.AsString;
        //ArrayData[iC+7,4] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,3] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,4] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,6] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,8] := FloatToStr(rPr1);
        ArrayData[iC+7,11] := quSelClientsSpecNDS.AsInteger;




        quSelClientsSpec.Next; inc(iC);
        if iC mod iM = 0 then
        begin
          fmClients.PBar1.Position:=Trunc((iC/(iM*20))*100);
          Delay(10);
        end;
      end;
      quSelClientsSpec.Active:=False;
      fmClients.PBar1.Position:=100;
    //delay(300);
      fmClients.PBar1.Visible:=False;
    end;





    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    Cell1 := WorkBook.WorkSheets[1].Cells[7,10];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,10];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Formula:='=IF(I7>0,(I7-H7)/H7,"")';
    //Range.Se := '0.00%';

    Cell1 := WorkBook.WorkSheets[1].Cells[7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    Cell1 := WorkBook.WorkSheets[1].Cells[iRow+7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

    WorkBook.WorkSheets[1].Cells[1,3].Font.Size := 8;

    WorkBook.WorkSheets[1].Cells[2,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[3,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[3,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[4,2].Font.Size := 8;
    WorkBook.WorkSheets[1].Cells[5,2].Font.Size := 8;


    //WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=8;
    WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=10;
    WorkBook.WorkSheets[1].Columns[2].NumberFormat :='#';
    //WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=30;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=30;

    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[5].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[7].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[8].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[9].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0,00%';
    WorkBook.WorkSheets[1].Columns[11].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[15].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[16].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[17].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[12].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[13].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[14].ColumnWidth:=7;
    WorkBook.WorkSheets[1].Columns[14].NumberFormat := '��.��.����';


    WorkBook.WorkSheets[1].Rows[6].RowHeight:=50;

    Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.WrapText:=True;
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=3;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    ExcelApp.Visible := true;
  end;
  fmPeriod1.Release;
end;

procedure TfmClients1.N12Click(Sender: TObject);
var
  f1,f2:textfile;
  str:widestring;
begin
  dmc.quSelClientsSpec2.Active:=False;
  dmc.quSelClientsSpec2.SQL.Clear;

  dmc.quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
  dmc.quSelClientsSpec2.SQL.Add('declare @Id_Client int');
  dmc.quSelClientsSpec2.SQL.Add('set @Id_Client='+dmc.taClientsId.AsString);

  dmc.quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'', '' FROM [dbo].[TTN] t');
  dmc.quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
  dmc.quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
  dmc.quSelClientsSpec2.SQL.Add('    and [DocType]=1');
  dmc.quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
  dmc.quSelClientsSpec2.SQL.Add('  group by d.Name');
  dmc.quSelClientsSpec2.SQL.Add('  order by d.Name');

  dmc.quSelClientsSpec2.SQL.Add('select SUBSTRING(@str,0,len(@str)) as Departs,DogNum,DogDate from dbo.Clients where Id=@Id_Client');
  dmc.quSelClientsSpec2.Active:=True;

  //����� �� �������, ������
  AssignFile(f1, ExtractFilePath(Application.ExeName)+'doc\ClientLetter1.rtf');
  Reset(f1);

  AssignFile(f2, ExtractFilePath(Application.ExeName)+'temp.rtf');
  Rewrite(f2);

  while not eof(f1) do
    begin
      readln(f1,str);
      str:=AnsiReplaceStr(str, 'ClientFullName1', dmc.taClientsFullName.asString);
      str:=AnsiReplaceStr(str, 'Departs1', dmc.quSelClientsSpec2.FieldByName('Departs').AsString);
      str:=AnsiReplaceStr(str, 'Date1', FormatDateTime('dd.mm.yyyy',Date()));
      str:=AnsiReplaceStr(str, 'DogDate2', FormatDateTime('dd.mm.yyyy',dmc.taClientsDogDate.AsDateTime));
      writeln(f2,str);
    end;
  CloseFile(f1);
  CloseFile(f2);
  dmc.quSelClientsSpec2.Active:=False;

  //ShowMessage(PChar(ExtractFilePath(Application.ExeName)+'temp.rtf'));
  ShellExecute(handle,'open',PChar(ExtractFilePath(Application.ExeName)+'temp.rtf'),nil,nil,SW_SHOWNORMAL);
end;

procedure TfmClients1.N21Click(Sender: TObject);
var
  f1,f2:textfile;
  str:widestring;
begin
  (*
  dmc.quSelClientsSpec2.Active:=False;
  dmc.quSelClientsSpec2.SQL.Clear;

  dmc.quSelClientsSpec2.SQL.Add('declare @str varchar(250)');
  dmc.quSelClientsSpec2.SQL.Add('declare @Id_Client int');
  dmc.quSelClientsSpec2.SQL.Add('set @Id_Client='+dmc.taClientsId.AsString);

  dmc.quSelClientsSpec2.SQL.Add('SELECT @str=isnull(@str,'''')+isnull(d.Name,'''')+'', '' FROM [dbo].[TTN] t');
  dmc.quSelClientsSpec2.SQL.Add('  inner join dbo.Departs d on t.[Id_Depart]=d.Id');
  dmc.quSelClientsSpec2.SQL.Add('  where [Id_Client]=@Id_Client');
  dmc.quSelClientsSpec2.SQL.Add('    and [DocType]=1');
  dmc.quSelClientsSpec2.SQL.Add('    and [DocDate] between (getdate()-180) and getdate()');
  dmc.quSelClientsSpec2.SQL.Add('  group by d.Name');
  dmc.quSelClientsSpec2.SQL.Add('  order by d.Name');

  dmc.quSelClientsSpec2.SQL.Add('select SUBSTRING(@str,0,len(@str)) as Departs,DogNum,DogDate from dbo.Clients where Id=@Id_Client');
  dmc.quSelClientsSpec2.Active:=True;
  *)

  //����� �� �������, ������
  AssignFile(f1, ExtractFilePath(Application.ExeName)+'doc\ClientLetter2.rtf');
  Reset(f1);

  AssignFile(f2, ExtractFilePath(Application.ExeName)+'temp2.rtf');
  Rewrite(f2);

  while not eof(f1) do
    begin
      readln(f1,str);
      str:=AnsiReplaceStr(str, 'ClientFullName1', dmc.taClientsFullName.asString);
      //str:=AnsiReplaceStr(str, 'Departs1', dmc.quSelClientsSpec2.FieldByName('Departs').AsString);
      str:=AnsiReplaceStr(str, 'Date1', FormatDateTime('dd.mm.yyyy',Date()));
      str:=AnsiReplaceStr(str, 'DogDate2', FormatDateTime('dd.mm.yyyy',dmc.taClientsDogDate.AsDateTime));
      writeln(f2,str);
    end;
  CloseFile(f1);
  CloseFile(f2);
  //dmc.quSelClientsSpec2.Active:=False;

  //ShowMessage(PChar(ExtractFilePath(Application.ExeName)+'temp2.rtf'));
  ShellExecute(handle,'open',PChar(ExtractFilePath(Application.ExeName)+'temp2.rtf'),nil,nil,SW_SHOWNORMAL);
end;

end.
