unit TransMenuBack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, FIBDataSet, pFIBDataSet,
  FIBDatabase, pFIBDatabase, ImgList, cxLookAndFeelPainters, cxButtons,
  cxContainer, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTreeView;

type
  TfmMenuCr = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    MenuTree: TTreeView;
    RxSplitter1: TRxSplitter;
    ViewMenuCr: TcxGridDBTableView;
    LevelMenuCr: TcxGridLevel;
    GridMenuCr: TcxGrid;
    ViewMenuCrSIFR: TcxGridDBColumn;
    ViewMenuCrNAME: TcxGridDBColumn;
    ViewMenuCrPRICE: TcxGridDBColumn;
    ViewMenuCrCODE: TcxGridDBColumn;
    ViewMenuCrLIMITPRICE: TcxGridDBColumn;
    ViewMenuCrCATEG: TcxGridDBColumn;
    ViewMenuCrPARENT: TcxGridDBColumn;
    ViewMenuCrLINK: TcxGridDBColumn;
    ViewMenuCrSTREAM: TcxGridDBColumn;
    ViewMenuCrLACK: TcxGridDBColumn;
    ViewMenuCrALTNAME: TcxGridDBColumn;
    ViewMenuCrNALOG: TcxGridDBColumn;
    ViewMenuCrBARCODE: TcxGridDBColumn;
    ViewMenuCrIMAGE: TcxGridDBColumn;
    ViewMenuCrCONSUMMA: TcxGridDBColumn;
    ViewMenuCrMINREST: TcxGridDBColumn;
    ViewMenuCrPRNREST: TcxGridDBColumn;
    ViewMenuCrCOOKTIME: TcxGridDBColumn;
    ViewMenuCrDISPENSER: TcxGridDBColumn;
    ViewMenuCrDISPKOEF: TcxGridDBColumn;
    ViewMenuCrACCESS: TcxGridDBColumn;
    ViewMenuCrFLAGS: TcxGridDBColumn;
    ViewMenuCrTARA: TcxGridDBColumn;
    ViewMenuCrCNTPRICE: TcxGridDBColumn;
    ViewMenuCrBACKBGR: TcxGridDBColumn;
    ViewMenuCrFONTBGR: TcxGridDBColumn;
    ViewMenuCrIACTIVE: TcxGridDBColumn;
    ViewMenuCrIEDIT: TcxGridDBColumn;
    am3: TActionManager;
    acExit: TAction;
    Timer1: TTimer;
    ActionMainMenuBar1: TActionMainMenuBar;
    ViewMenuCrNAMECA: TcxGridDBColumn;
    ViewMenuCrNAMEMO: TcxGridDBColumn;
    ViewMenuCrIACTIVE1: TcxGridDBColumn;
    CasherRnDb: TpFIBDatabase;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quMenuTree: TpFIBDataSet;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelTREETYPE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelCATEG: TFIBSmallIntField;
    quMenuSelPARENT: TFIBSmallIntField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelLACK: TFIBSmallIntField;
    quMenuSelDESIGNSIFR: TFIBSmallIntField;
    quMenuSelALTNAME: TFIBStringField;
    quMenuSelNALOG: TFIBFloatField;
    quMenuSelBARCODE: TFIBStringField;
    quMenuSelIMAGE: TFIBSmallIntField;
    quMenuSelCONSUMMA: TFIBFloatField;
    quMenuSelMINREST: TFIBSmallIntField;
    quMenuSelPRNREST: TFIBSmallIntField;
    quMenuSelCOOKTIME: TFIBSmallIntField;
    quMenuSelDISPENSER: TFIBSmallIntField;
    quMenuSelDISPKOEF: TFIBSmallIntField;
    quMenuSelACCESS: TFIBSmallIntField;
    quMenuSelFLAGS: TFIBSmallIntField;
    quMenuSelTARA: TFIBSmallIntField;
    quMenuSelCNTPRICE: TFIBSmallIntField;
    quMenuSelBACKBGR: TFIBFloatField;
    quMenuSelFONTBGR: TFIBFloatField;
    quMenuSelIACTIVE: TFIBSmallIntField;
    quMenuSelIEDIT: TFIBSmallIntField;
    quMenuSelNAMECA: TFIBStringField;
    quMenuSelNAMEMO: TFIBStringField;
    dsMenuSel: TDataSource;
    quMenuFindParent: TpFIBDataSet;
    quMenuFindParentCOUNTREC: TFIBIntegerField;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    quMaxIdMe: TpFIBDataSet;
    quMaxIdMeMAXID: TFIBIntegerField;
    imState: TImageList;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    quFindB: TpFIBDataSet;
    dsFindB: TDataSource;
    quFindBSIFR: TFIBIntegerField;
    quFindBNAME: TFIBStringField;
    quFindBPRICE: TFIBFloatField;
    quFindBCODE: TFIBStringField;
    quFindBPARENT: TFIBSmallIntField;
    quFindBCONSUMMA: TFIBFloatField;
    Label2: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxButton3: TcxButton;
    quModGrMo: TpFIBDataSet;
    quModGrMoSIFR: TFIBIntegerField;
    quModGrMoNAME: TFIBStringField;
    quModGrMoPARENT: TFIBIntegerField;
    quModGrMoPRICE: TFIBFloatField;
    quModGrMoREALPRICE: TFIBFloatField;
    quModGrMoIACTIVE: TFIBSmallIntField;
    quModGrMoIEDIT: TFIBSmallIntField;
    quMods: TpFIBDataSet;
    quModsSIFR: TFIBIntegerField;
    quModsNAME: TFIBStringField;
    quModsPARENT: TFIBIntegerField;
    quModsPRICE: TFIBFloatField;
    quModsREALPRICE: TFIBFloatField;
    quModsIACTIVE: TFIBSmallIntField;
    quModsIEDIT: TFIBSmallIntField;
    quModsCODEB: TFIBIntegerField;
    quModsKB: TFIBFloatField;
    dsMods: TDataSource;
    LevelMods: TcxGridLevel;
    ViewMods: TcxGridDBTableView;
    ViewModsSIFR: TcxGridDBColumn;
    ViewModsNAME: TcxGridDBColumn;
    ViewModsCODEB: TcxGridDBColumn;
    ViewModsKB: TcxGridDBColumn;
    quMaxIdMod: TpFIBDataSet;
    quMaxIdModMAXID: TFIBIntegerField;
    acEditM: TAction;
    quStream: TpFIBDataSet;
    quStreamID: TFIBIntegerField;
    quStreamNAMESTREAM: TFIBStringField;
    dsquStream: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure MenuTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMenuCrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViewMenuCrStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ViewMenuCrDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMenuCrDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewModsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewModsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewModsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure acEditMExecute(Sender: TObject);
    procedure quMenuSelAfterPost(DataSet: TDataSet);
    procedure quModsAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure RefreshMods;
    Procedure RefreshModTree;
  end;

var
  fmMenuCr: TfmMenuCr;
//  bDrM:Boolean=False;
  bClearMenu:Boolean = False;
  bCreate:Boolean = False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, dmOffice, Goods, uTransM, FindResult,
  DMOReps, of_EditPosMenu;

{$R *.dfm}

Procedure TfmMenuCr.RefreshMods;
begin
  quMods.Active:=False;
  quMods.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
  quMods.Active:=True;
  quMods.First;
end;

Procedure TfmMenuCr.RefreshModTree;
Var TreeNode : TTreeNode;
begin
  MenuTree.Items.Clear;

  quModGrMo.Active:=False;
  quModGrMo.Active:=True;
  quModGrMo.First;
  while not quModGrMo.Eof do
  begin
    TreeNode:=MenuTree.Items.AddChildObject(nil, quModGrMoNAME.AsString, Pointer(quModGrMoSIFR.AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;

    quModGrMo.Next;
  end;
  if MenuTree.Items.Count>0 then
  begin
    MenuTree.Items[0].Selected:=True;
    MenuTree.Repaint;
    delay(10);
  end;
end;



procedure TfmMenuCr.FormCreate(Sender: TObject);
begin
  bCreate:=True;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMenuCr.Align:=AlClient;
  ViewMenuCr.RestoreFromIniFile(CurDir+GridIni);

  cxTextEdit1.Text:='';

  with dmORep do
  begin
    if quDB.Active then
    begin
      cxLookupComboBox1.EditValue:=quDBID.AsInteger;
      cxLookupComboBox1.Text:=quDBNAMEDB.AsString;
      DBName:=dmORep.quDBPATHDB.AsString;
    end;
  end;

  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;

    bMenuList:=False;
    MenuTree.Items.Clear;
    CardsExpandLevel(nil,MenuTree,quMenuTree);

    MenuTree.FullExpand;
    MenuTree.FullCollapse;

    quMenuTree.First;
    bMenuList:=True;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;
  except
    CasherRnDb.Connected:=False;
  end;
  bCreate:=False;
end;

procedure TfmMenuCr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CasherRnDb.Connected:=False;
  ViewMenuCr.StoreToIniFile(CurDir+GridIni,False);
  Release;
end;

procedure TfmMenuCr.MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,MenuTree,quMenuTree);
  end;
end;

procedure TfmMenuCr.MenuTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuList then
  begin
    if cxButton3.Tag=0 then
    begin
      ViewMenuCr.BeginUpdate;

      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMenuSel.Active:=True;

      ViewMenuCr.EndUpdate;
    end;
    if cxButton3.Tag=1 then
    begin
      ViewMods.BeginUpdate;
      RefreshMods;
      ViewMods.EndUpdate;
    end;
  end;
end;

procedure TfmMenuCr.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuCr.Timer1Timer(Sender: TObject);
begin
  if bClearMenu=True then begin StatusBar1.Panels[0].Text:=''; bClearMenu:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMenu:=True;
end;

procedure TfmMenuCr.ViewMenuCrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewMenuCr.ColumnCount-1 do
  begin
    if ViewMenuCr.Columns[i].Name='ViewMenuCrIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmMenuCr.ViewMenuCrStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prTransMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDM:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iD:INteger;
    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmCards.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          ViewMenuCr.BeginUpdate;
          for i:=0 to fmCards.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  quMaxIdMe.Active:=False;
                  quMaxIdMe.Active:=True;
                  Id:=quMaxIdMeMAXID.AsInteger+1;
                  quMaxIdMe.Active:=False;

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);

                  trUpdM.StartTransaction;
                  quMenusel.Append;
                  quMenuSelSIFR.AsInteger:=Id;
                  quMenuSelNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quMenuSelPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                 //��� ����
                  quMenuSelCODE.AsString:=IntToStr(iNum);
                  quMenuSelCONSUMMA.AsFloat:=1;
                  quMenuSelTREETYPE.AsString:='F';
                  quMenuSelLIMITPRICE.AsFloat:=0;

                  quMenuSelCATEG.AsInteger:=0;
                  quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
                  quMenuSelLINK.AsInteger:=0;
                  quMenuSelSTREAM.AsInteger:=0;
                  quMenuSelLACK.AsInteger:=0;
                  quMenuSelDESIGNSIFR.AsInteger:=0;
                  quMenuSelALTNAME.AsString:='';
                  quMenuSelNALOG.AsFloat:=0;
                  quMenuSelBARCODE.AsString:=sBar;
                  quMenuSelIMAGE.AsInteger:=0;
                  quMenuSelMINREST.AsFloat:=0;
                  quMenuSelPRNREST.AsInteger:=0;
                  quMenuSelCOOKTIME.AsInteger:=0;
                  quMenuSelDISPENSER.AsInteger:=0;
                  quMenuSelDISPKOEF.AsInteger:=0;
                  quMenuSelACCESS.AsInteger:=0;
                  quMenuSelFLAGS.AsInteger:=0;
                  quMenuSelTARA.AsInteger:=0;
                  quMenuSelCNTPRICE.AsInteger:=0;
                  quMenuSelBACKBGR.AsFloat:=0;
                  quMenuSelFONTBGR.AsFloat:=0;
                  quMenuSelIACTIVE.AsInteger:=1;
                  quMenuSelIEDIT.AsInteger:=0;
                  quMenuSel.Post;
                  trUpdM.Commit;

                  with dmO do
                  begin
                  // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                    prAddKb.ParamByName('ITYPE').AsInteger:=1;
                    prAddKb.ParamByName('SIFR').AsInteger:=Id;
                    prAddKb.ParamByName('CODEB').AsInteger:=iNum;
                    prAddKb.ParamByName('KB').AsFloat:=1;
                    prAddKb.ExecProc;
                  end;

                except
                end;
                quMenuSel.Refresh;
              end;
            end;
          end;
          ViewMenuCr.EndUpdate;
        end;
      end;
    end;
    if iCo=1 then //������ ���� ��������� �������
    begin
      if Sender is TcxGridSite then
      begin
        bAdd:=False;
        k:=1;
        AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
        if AHitTest.HitTestCode=107 then //������
        begin
          Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
          fmTransM:=tfmTransM.Create(Application);
          for j:=0 to Rec1.ValueCount-1 do
          begin
            if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
          end;
          iNum:=Rec1.Values[j];
          if quMenuSel.Locate('SIFR',iNum,[]) then
          begin
            with fmTransM do
            begin
              cxTextEdit1.Text:=quMenuSelNAME.AsString;
              cxCalcEdit1.EditValue:=StrToINtDef(quMenuSelCODE.AsString,0);
              cxCalcEdit2.EditValue:=quMenuSelCONSUMMA.AsFloat;

              cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
              cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
              cxCalcEdit4.EditValue:=1;
            end;
            fmTransM.ShowModal;
            if fmTransM.ModalResult=mrYes then
            begin //������
              trUpdM.StartTransaction;
              quMenusel.Edit;
              //��� ����
              quMenuSelNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
              quMenuSelCODE.AsString:=INtToStr(Trunc(fmTransM.cxCalcEdit3.EditValue));
              quMenuSelCONSUMMA.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
              quMenuSel.Post;
              trUpdM.Commit;


              with dmO do
              begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                prAddKb.ParamByName('ITYPE').AsInteger:=1;
                prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
                prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                prAddKb.ExecProc;
              end;

              quMenuSel.Refresh;
            end;
            if fmTransM.ModalResult=mrOk then //����������
            begin
              bAdd:=True;
              k:=fmTransM.cxCalcEdit4.EditValue;
            end;
          end else bAdd:=True;
          fmTransM.Release;
        end;
        if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
        if bAdd then   //����������
        begin
          quMaxIdMe.Active:=False;
          quMaxIdMe.Active:=True;
          Id:=quMaxIdMeMAXID.AsInteger+1;
          quMaxIdMe.Active:=False;

          sBar:=IntToStr(iD);
          while length(sBar)<10 do sBar:='0'+sBar;
          sBar:='25'+sBar;
          sBar:=ToStandart(sBar);

          trUpdM.StartTransaction;
          quMenusel.Append;
          quMenuSelSIFR.AsInteger:=Id;
          quMenuSelNAME.AsString:=Copy(dmO.quCardsSelNAME.AsString,1,100);
          quMenuSelPRICE.AsFloat:=dmO.quCardsSelLASTPRICEOUT.AsFloat;
          //��� ����
          quMenuSelCODE.AsString:=IntToStr(dmO.quCardsSelID.AsInteger);
          quMenuSelCONSUMMA.AsFloat:=K;
          quMenuSelTREETYPE.AsString:='F';
          quMenuSelLIMITPRICE.AsFloat:=0;

          quMenuSelCATEG.AsInteger:=0;
          quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
          quMenuSelLINK.AsInteger:=0;
          quMenuSelSTREAM.AsInteger:=0;
          quMenuSelLACK.AsInteger:=0;
          quMenuSelDESIGNSIFR.AsInteger:=0;
          quMenuSelALTNAME.AsString:='';
          quMenuSelNALOG.AsFloat:=0;
          quMenuSelBARCODE.AsString:=sBar;
          quMenuSelIMAGE.AsInteger:=0;
          quMenuSelMINREST.AsFloat:=0;
          quMenuSelPRNREST.AsInteger:=0;
          quMenuSelCOOKTIME.AsInteger:=0;
          quMenuSelDISPENSER.AsInteger:=0;
          quMenuSelDISPKOEF.AsInteger:=0;
          quMenuSelACCESS.AsInteger:=0;
          quMenuSelFLAGS.AsInteger:=0;
          quMenuSelTARA.AsInteger:=0;
          quMenuSelCNTPRICE.AsInteger:=0;
          quMenuSelBACKBGR.AsFloat:=0;
          quMenuSelFONTBGR.AsFloat:=0;
          quMenuSelIACTIVE.AsInteger:=1;
          quMenuSelIEDIT.AsInteger:=0;
          quMenuSel.Post;
          trUpdM.Commit;

          with dmO do
          begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=1;
            prAddKb.ParamByName('SIFR').AsInteger:=Id;
            prAddKb.ParamByName('CODEB').AsInteger:=dmO.quCardsSelID.AsInteger;
            prAddKb.ParamByName('KB').AsFloat:=K;
            prAddKb.ExecProc;
          end;

          quMenuSel.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfmMenuCr.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    quFindB.Active:=False;
    quFindB.SelectSQL.Clear;
    quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
    quFindB.SelectSQL.Add('FROM MENU ');
    quFindB.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
    quFindB.Active:=True;

    fmFind.LevelFind.Visible:=False;
    fmFind.LevelFMenu.Visible:=True;

    fmFind.ShowModal;
    if fmFind.ModalResult=mrOk then
    begin
      if quFindB.RecordCount>0 then
      begin
        bMenuList:=False;
        ViewMenuCr.BeginUpdate;

        for i:=0 to MenuTree.Items.Count-1 Do
        if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
        begin
          MenuTree.Items[i].Expand(False);
          MenuTree.Repaint;
          MenuTree.Items[i].Selected:=True;
          Break;
        End;
        delay(10);

        quMenuSel.Active:=False;
        quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
        quMenuSel.Active:=True;

        quMenuSel.First;
        quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

        ViewMenuCr.EndUpdate;
        bMenuList:=True;
      end;
    end;

    fmFind.LevelFind.Visible:=True;
    fmFind.LevelFMenu.Visible:=False;

    cxTextEdit1.Text:='';
    delay(10);
    GridMenuCr.SetFocus;
  end;
end;

procedure TfmMenuCr.cxButton2Click(Sender: TObject);
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFindB.Active:=False;
      quFindB.SelectSQL.Clear;
      quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
      quFindB.SelectSQL.Add('FROM MENU ');
      quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCode));
      quFindB.Active:=True;

      fmFind.LevelFind.Visible:=False;
      fmFind.LevelFMenu.Visible:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFindB.RecordCount>0 then
        begin
          bMenuList:=False;
          ViewMenuCr.BeginUpdate;

          for i:=0 to MenuTree.Items.Count-1 Do
          if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
          begin
            MenuTree.Items[i].Expand(False);
            MenuTree.Repaint;
            MenuTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);

          quMenuSel.Active:=False;
          quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
          quMenuSel.Active:=True;

          quMenuSel.First;
          quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

          ViewMenuCr.EndUpdate;
          bMenuList:=True;
        end;
      end;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;

      cxTextEdit1.Text:='';
      delay(10);
      GridMenuCr.SetFocus;
    end;
  end;
end;

procedure TfmMenuCr.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
//
  if bCreate then exit;
  StatusBar1.Panels[0].Text:='�����, ���� �������� ���� ...'; delay(10);
  cxButton3.Caption:='�� ������������';
  cxButton3.Tag:=0;
  fmMenuCr.Caption:='���� (�����)';

  LevelMenuCr.Visible:=True;
  LevelMods.Visible:=False;

  ViewMenuCr.BeginUpdate;

  if dmORep.quDB.Locate('ID',cxLookupComboBox1.EditValue,[]) then
    DBName:=dmORep.quDBPATHDB.AsString;

  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;

    bMenuList:=False;

    MenuTree.Visible:=False;
    MenuTree.Items.Clear;
    CardsExpandLevel(nil,MenuTree,quMenuTree);
    MenuTree.FullExpand;
    MenuTree.FullCollapse;
    MenuTree.Visible:=True;

    quMenuTree.First;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;

    bMenuList:=True;

  except
    CasherRnDb.Connected:=False;
  end;

  ViewMenuCr.EndUpdate;
  StatusBar1.Panels[0].Text:='��.'; delay(10);
end;

procedure TfmMenuCr.cxButton3Click(Sender: TObject);
begin
  if not CasherRnDb.Connected then exit;
  StatusBar1.Panels[0].Text:='����� ���� ������������ ������ ...'; delay(10);
  if cxButton3.Tag=0 then
  begin
    //������������ �� ������������
    cxButton3.Caption:='�� �����';
    cxButton3.Tag:=1;
    fmMenuCr.Caption:='���� (������������)';

    LevelMenuCr.Visible:=False;
    LevelMods.Visible:=True;

    ViewMods.BeginUpdate;
    try
      bMenuList:=False;
      MenuTree.Items.Clear;

      RefreshModTree;
      if quModGrMo.RecordCount>0 then  RefreshMods;

      bMenuList:=True;
    finally
      ViewMods.EndUpdate;
    end;

  end else
  begin
    //������������ ������� �� �����
    cxButton3.Caption:='�� ������������';
    cxButton3.Tag:=0;
    fmMenuCr.Caption:='���� (�����)';

    LevelMenuCr.Visible:=True;
    LevelMods.Visible:=False;

    ViewMenuCr.BeginUpdate;
    try
      bMenuList:=False;

      MenuTree.Items.Clear;
      CardsExpandLevel(nil,MenuTree,quMenuTree);
      MenuTree.Visible:=False;
      MenuTree.FullExpand;
      MenuTree.FullCollapse;
      MenuTree.Visible:=True;

      quMenuTree.First;

      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
      quMenuSel.Active:=True;

      bMenuList:=True;
    finally
      ViewMenuCr.EndUpdate;
    end;
  end;
  StatusBar1.Panels[0].Text:='��.'; delay(10);
end;

procedure TfmMenuCr.ViewModsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewModsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iD:INteger;
    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmCards.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� (������������) ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          ViewMods.BeginUpdate;
          for i:=0 to fmCards.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  quMaxIdMod.Active:=False;
                  quMaxIdMod.Active:=True;
                  Id:=quMaxIdModMAXID.AsInteger+1;
                  quMaxIdMod.Active:=False;

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);
                                         

                  trUpdM.StartTransaction;
                  quMods.Append;
                  quModsSIFR.AsInteger:=Id;
                  quModsNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quModsPRICE.AsFloat:=0;
                  quModsREALPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                 //��� ����
                  quModsCODEB.AsInteger:=iNum;
                  quModsKB.AsFloat:=1;
                  quModsPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
                  quModsIACTIVE.AsInteger:=1;
                  quModsIEDIT.AsInteger:=0;
                  quMods.Post;
                  trUpdM.Commit;


                  with dmO do
                  begin
                    // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                    prAddKb.ParamByName('ITYPE').AsInteger:=2;
                    prAddKb.ParamByName('SIFR').AsInteger:=Id;
                    prAddKb.ParamByName('CODEB').AsInteger:=iNum;
                    prAddKb.ParamByName('KB').AsFloat:=1;
                    prAddKb.ExecProc;
                  end;

                except
                end;
                quMods.Refresh;
              end;
            end;
          end;
          ViewMods.EndUpdate;
        end;
      end;
    end;
    if iCo=1 then //������ ���� ��������� �������
    begin
      if Sender is TcxGridSite then
      begin
        bAdd:=False;
        k:=1;
        AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
        if AHitTest.HitTestCode=107 then //������
        begin
          Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
          fmTransM:=tfmTransM.Create(Application);
          for j:=0 to Rec1.ValueCount-1 do
          begin
            if ViewMods.Columns[j].Name='ViewModsSIFR' then break;
          end;
          iNum:=Rec1.Values[j];
          if quMods.Locate('SIFR',iNum,[]) then
          begin
            with fmTransM do
            begin
              cxTextEdit1.Text:=quModsNAME.AsString;
              cxCalcEdit1.EditValue:=StrToINtDef(quModsCODEB.AsString,0);
              cxCalcEdit2.EditValue:=quModsKB.AsFloat;

              cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
              cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
              cxCalcEdit4.EditValue:=1;
            end;
            fmTransM.ShowModal;
            if fmTransM.ModalResult=mrYes then
            begin //������
              trUpdM.StartTransaction;
              quMods.Edit;
              //��� ����
              quModsNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
              quModsCODEB.AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
              quModsKB.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
              quMods.Post;
              trUpdM.Commit;
              quMods.Refresh;

              with dmO do
              begin
                // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                prAddKb.ParamByName('ITYPE').AsInteger:=2;
                prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
                prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                prAddKb.ExecProc;
              end;

            end;
            if fmTransM.ModalResult=mrOk then //����������
            begin
              bAdd:=True;
              k:=fmTransM.cxCalcEdit4.EditValue;
            end;
          end else bAdd:=True;
          fmTransM.Release;
        end;
        if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
        if bAdd then   //����������
        begin
          quMaxIdMod.Active:=False;
          quMaxIdMod.Active:=True;
          Id:=quMaxIdModMAXID.AsInteger+1;
          quMaxIdMod.Active:=False;

          sBar:=IntToStr(iD);
          while length(sBar)<10 do sBar:='0'+sBar;
          sBar:='25'+sBar;
          sBar:=ToStandart(sBar);

          trUpdM.StartTransaction;
          quMods.Append;
          quModsSIFR.AsInteger:=Id;
          quModsNAME.AsString:=Copy(dmO.quCardsSelNAME.AsString,1,100);
          quModsPRICE.AsFloat:=0;
          quModsREALPRICE.AsFloat:=dmO.quCardsSelLASTPRICEOUT.AsFloat;
          quModsCODEB.AsInteger:=dmO.quCardsSelID.AsInteger;
          quModsKB.AsFloat:=K;
          quModsPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
          quModsIACTIVE.AsInteger:=1;
          quModsIEDIT.AsInteger:=0;
          quMods.Post;

          trUpdM.Commit;
          quMods.Refresh;

          with dmO do
          begin
                    // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=2;
            prAddKb.ParamByName('SIFR').AsInteger:=Id;
            prAddKb.ParamByName('CODEB').AsInteger:=dmO.quCardsSelID.AsInteger;
            prAddKb.ParamByName('KB').AsFloat:=K;
            prAddKb.ExecProc;
          end;

        end;
      end;
    end;
  end;
end;

procedure TfmMenuCr.ViewModsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prTransMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDM:=True;
end;

procedure TfmMenuCr.acEditMExecute(Sender: TObject);
begin
// ����������� ������� ����
  if LevelMenuCr.Visible then
  begin
    if quMenuSel.RecordCount>0 then
    begin
      try
        fmEditPosM:=TfmEditPosM.Create(Application);
        with fmEditPosM do
        begin
          quStream.Active:=False;
          quStream.Active:=True;
          cxTextEdit1.Text:=quMenuSelNAME.AsString;
          cxCurrencyEdit1.Value:=quMenuSelPRICE.AsFloat;
          cxLookupComboBox1.EditValue:=quMenuSelSTREAM.AsInteger;
          cxCalcEdit1.Value:=StrToIntDef(quMenuSelCODE.AsString,0);
          cxCalcEdit2.Value:=quMenuSelCONSUMMA.AsFloat;
        end;
        fmEditPosM.ShowModal;
        if fmEditPosM.ModalResult=mrOk then
        begin
          trUpdM.StartTransaction;
          quMenuSel.Edit;
          quMenuSelNAME.AsString:=fmEditPosM.cxTextEdit1.Text;
          quMenuSelPRICE.AsFloat:=fmEditPosM.cxCurrencyEdit1.Value;
          quMenuSelSTREAM.AsInteger:=fmEditPosM.cxLookupComboBox1.EditValue;
          quMenuSelCODE.AsString:=IntToStr(Trunc(fmEditPosM.cxCalcEdit1.Value));
          quMenuSelCONSUMMA.AsFloat:=fmEditPosM.cxCalcEdit2.Value;
          quMenuSel.Post;
          trUpdM.Commit;

          with dmO do
          begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=1;
            prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
            prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmEditPosM.cxCalcEdit1.Value);
            prAddKb.ParamByName('KB').AsFloat:=fmEditPosM.cxCalcEdit2.Value;
            prAddKb.ExecProc;
          end;

          quMenuSel.Refresh;
        end;

      finally
        quStream.Active:=False;
        fmEditPosM.Release;
      end;
    end;
  end;
  if LevelMods.Visible then
  begin
    if quMods.RecordCount>0 then
    begin
      try
        fmEditPosM:=TfmEditPosM.Create(Application);
        with fmEditPosM do
        begin
          quStream.Active:=False;
          quStream.Active:=True;
          cxTextEdit1.Text:=quModsNAME.AsString;
          cxCurrencyEdit1.Value:=0;
          cxLookupComboBox1.EditValue:=0;
          cxCalcEdit1.Value:=StrToIntDef(quModsCODEB.AsString,0);
          cxCalcEdit2.Value:=quModsKB.AsFloat;
        end;
        fmEditPosM.ShowModal;
        if fmEditPosM.ModalResult=mrOk then
        begin
          trUpdM.StartTransaction;
          quMods.Edit;
          quModsNAME.AsString:=fmEditPosM.cxTextEdit1.Text;
          quModsCODEB.AsString:=IntToStr(Trunc(fmEditPosM.cxCalcEdit1.Value));
          quModsKB.AsFloat:=fmEditPosM.cxCalcEdit2.Value;
          quMods.Post;
          trUpdM.Commit;

          quMods.Refresh;

          with dmO do
          begin
            // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=2;
            prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
            prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmEditPosM.cxCalcEdit1.Value);
            prAddKb.ParamByName('KB').AsFloat:=fmEditPosM.cxCalcEdit2.Value;
            prAddKb.ExecProc;
          end;

        end;

      finally
        quStream.Active:=False;
        fmEditPosM.Release;
      end;
    end;
  end;

end;

procedure TfmMenuCr.quMenuSelAfterPost(DataSet: TDataSet);
begin
{  //����� ���������� ������
  with dmO do
  begin
// EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
    prAddKb.ParamByName('ITYPE').AsInteger:=1;
    prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSIFR.AsInteger;
    prAddKb.ParamByName('CODEB').AsInteger:=StrToIntDef(quMenuSelCODE.AsString,0);
    prAddKb.ParamByName('KB').AsFloat:=quMenuSelCONSUMMA.AsFloat;
    prAddKb.ExecProc;
  end;
  }
end;

procedure TfmMenuCr.quModsAfterPost(DataSet: TDataSet);
begin
{
  with dmO do
  begin
// EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
    prAddKb.ParamByName('ITYPE').AsInteger:=2;
    prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
    prAddKb.ParamByName('CODEB').AsInteger:=quModsCODEB.AsInteger;
    prAddKb.ParamByName('KB').AsFloat:=quModsKB.AsFloat;
    prAddKb.ExecProc;
  end;
}
end;

end.
