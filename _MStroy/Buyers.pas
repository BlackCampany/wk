unit Buyers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Placemnt, ExtCtrls, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ActnList,
  ActnMan, SpeedBar, cxContainer, cxTextEdit, cxCheckBox,
  cxImageComboBox, XPStyleActnCtrls, dxmdaset, FIBQuery, pFIBQuery;

type
  TfmBuyers = class(TForm)
    StatusBar1: TStatusBar;
    ViewCl: TcxGridDBTableView;
    LevelCl: TcxGridLevel;
    GridCl: TcxGrid;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    amCli: TActionManager;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton3: TcxButton;
    acExit: TAction;
    acAddCli: TAction;
    acEditCli: TAction;
    acDelCli: TAction;
    SpeedItem2: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acDelSel: TAction;
    acActSel: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    acReDel: TAction;
    N8: TMenuItem;
    SpeedItem5: TSpeedItem;
    qBuyers: TpFIBQuery;
    teBuyers: TdxMemData;
    teBuyersBUYER_ID: TIntegerField;
    teBuyersBUYER_FIO: TStringField;
    teBuyersBUYER_ADDRESS: TStringField;
    dsBuyers: TDataSource;
    teBuyersBUYER_F: TStringField;
    teBuyersBUYER_I: TStringField;
    teBuyersBUYER_O: TStringField;
    teBuyersBUYER_PASSPORTSERIA: TIntegerField;
    teBuyersBUYER_PASSPORTNOMER: TIntegerField;
    teBuyersBUYER_PASSPORTISSUEDATE: TDateField;
    teBuyersBUYER_PASSPORTISSUER: TStringField;
    ViewClBUYER_ID: TcxGridDBColumn;
    ViewClBUYER_FIO: TcxGridDBColumn;
    ViewClBUYER_ADDRESS: TcxGridDBColumn;
    ViewClBUYER_F: TcxGridDBColumn;
    ViewClBUYER_I: TcxGridDBColumn;
    ViewClBUYER_O: TcxGridDBColumn;
    ViewClBUYER_PASSPORTSERIA: TcxGridDBColumn;
    ViewClBUYER_PASSPORTNOMER: TcxGridDBColumn;
    ViewClBUYER_PASSPORTISSUER: TcxGridDBColumn;
    ViewClBUYER_PASSPORTISSUEDATE: TcxGridDBColumn;
    teBuyersPHONE: TStringField;
    ViewClPHONE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddCliExecute(Sender: TObject);
    procedure acEditCliExecute(Sender: TObject);
    procedure acDelCliExecute(Sender: TObject);
    procedure acDelSelExecute(Sender: TObject);
    procedure acActSelExecute(Sender: TObject);
    procedure acReDelExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewClCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewClKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuyersRefr();
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prClearFmAddCli;
  end;

var
  fmBuyers: TfmBuyers;
  bClearCli:Boolean = False;

implementation

uses dmOffice, Un1, AddClient, FindResult, Goods, AddBuyer;

{$R *.dfm}


Procedure TfmBuyers.prClearFmAddCli;
begin
  with fmAddClients do
  begin
    cxTextEdit1.Text:='';
    cxTextEdit2.Text:='';
    cxTextEdit3.Text:='';
    cxTextEdit4.Text:='';
    cxTextEdit5.Text:='';
    cxTextEdit6.Text:='';
    cxTextEdit7.Text:='';
    cxTextEdit8.Text:='';
    cxTextEdit9.Text:='';
    cxTextEdit10.Text:='';
    cxTextEdit11.Text:='';
    cxTextEdit12.Text:='';
    cxTextEdit13.Text:='';
    cxMemo1.Clear;
    cxCheckBox1.EditValue:=1;
    cxSpinEdit1.Value:=0;
  end;
end;

procedure TfmBuyers.FormCreate(Sender: TObject);
begin
  GridCl.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  ViewCl.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';

end;

procedure TfmBuyers.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCl.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmBuyers.Timer1Timer(Sender: TObject);
begin
  if bClearCli=True then begin StatusBar1.Panels[0].Text:=''; bClearCli:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearCli:=True;
end;

procedure TfmBuyers.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmBuyers.acAddCliExecute(Sender: TObject);
//Var iId:Integer;
//    TreeNode : TTreeNode;
//    i:Integer;
//    StrWk:String;
begin
// �������� �����������
  if not CanDo('prAddCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    fmAddBuyer.Caption:='���������� ����������.';
    fmAddBuyer.cxButton1.Tag:=0;
    fmAddBuyer.cxTextEdit1.Text:='';
    fmAddBuyer.cxTextEdit2.Text:='';
    fmAddBuyer.cxTextEdit3.Text:='';
    fmAddBuyer.cxTextEdit6.Text:='0';
    fmAddBuyer.cxTextEdit12.Text:='0';
    fmAddBuyer.cxTextEdit7.Text:='';
    fmAddBuyer.cxTextEdit9.Text:='';
    fmAddBuyer.cxTextEdit4.Text:='';

    fmAddBuyer.cxDateEdit1.Date:=Date;
    fmAddBuyer.ShowModal;
    BuyersRefr();
  end;
end;

procedure TfmBuyers.acEditCliExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if teBuyers.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;

    fmAddBuyer.Caption:='�������������� ����������� - '+taClientsNAMECL.AsString;
    with fmAddBuyer do
    begin
      cxButton1.Tag:=teBuyersBUYER_ID.AsInteger;
      cxTextEdit1.Text:=fmBuyers.teBuyersBUYER_F.AsString;
      cxTextEdit2.Text:=fmBuyers.teBuyersBUYER_I.AsString;
      cxTextEdit3.Text:=fmBuyers.teBuyersBUYER_O.AsString;
      cxTextEdit6.Text:=fmBuyers.teBuyersBUYER_PASSPORTSERIA.AsString;
      cxTextEdit12.Text:=fmBuyers.teBuyersBUYER_PASSPORTNOMER.AsString;
      cxTextEdit7.Text:=fmBuyers.teBuyersBUYER_PASSPORTISSUER.AsString;
      cxTextEdit9.Text:=fmBuyers.teBuyersBUYER_ADDRESS.AsString;
      cxDateEdit1.Date:=fmBuyers.teBuyersBUYER_PASSPORTISSUEDATE.AsDateTime;
      cxTextEdit4.Text:=fmBuyers.teBuyersPHONE.AsString;;
    end;

    fmAddBuyer.ShowModal;
    BuyersRefr();
  end;
end;

procedure TfmBuyers.acDelCliExecute(Sender: TObject);
begin
  //�������
  if not CanDo('prDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if teBuyers.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;
    if MessageDlg('�� ������������� ������ ������� ����������� - '+teBuyersBUYER_FIO.AsString,
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewCl.BeginUpdate;
      //taClients.Edit;
      //taClientsIACTIVE.AsInteger:=0;
      //taClients.Post;
      //delay(10);
      //taClients.FullRefresh;
      qBuyers.Close;
      qBuyers.SQL.Clear;
      qBuyers.SQL.Add('delete from of_buyers where (buyer_id = '+teBuyersBUYER_ID.AsString+')');
      qBuyers.Transaction.StartTransaction;
        qBuyers.ExecQuery;
      qBuyers.Transaction.Commit;
      BuyersRefr();


      ViewCl.EndUpdate;
    end;
  end;
end;

procedure TfmBuyers.acDelSelExecute(Sender: TObject);
begin
  if not CanDo('prViewDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    ViewCl.BeginUpdate;
    taClients.Active:=False;
    taClients.SelectSQL.Clear;
    taClients.SelectSQL.Add('SELECT ID,NAMECL,FULLNAMECL,INN,PHONE,MOL,COMMENT,INDS,IACTIVE,KPP,ADDRES,SROKPLAT');
    taClients.SelectSQL.Add('FROM OF_CLIENTS');
    taClients.SelectSQL.Add('Order by NAMECL');
    taClients.Active:=True;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmBuyers.acActSelExecute(Sender: TObject);
begin
  with dmO do
  begin
    ViewCl.BeginUpdate;
    taClients.Active:=False;
    taClients.SelectSQL.Clear;
    taClients.SelectSQL.Add('SELECT ID,NAMECL,FULLNAMECL,INN,PHONE,MOL,COMMENT,INDS,IACTIVE,KPP,ADDRES,SROKPLAT');
    taClients.SelectSQL.Add('FROM OF_CLIENTS');
    taClients.SelectSQL.Add('WHERE IACTIVE=1');
    taClients.SelectSQL.Add('Order by NAMECL');
    taClients.Active:=True;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmBuyers.acReDelExecute(Sender: TObject);
begin
//������������
  if not CanDo('prReDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taClients.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;
    ViewCl.BeginUpdate;
    taClients.Edit;
    taClientsIACTIVE.AsInteger:=1;
    taClients.Post;
    delay(10);
    taClients.Refresh;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmBuyers.cxButton3Click(Sender: TObject);
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
// ����� ��� ���������� �� �������� ���������� � ����������
      quFindCli.Active:=False;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT ID,NAMECL ');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFindCli.Active:=True;

      fmFind.LevelFind.Visible:=False;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFindCli.RecordCount>0 then
        begin
          taClients.locate('ID',quFindCliID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GridCl.SetFocus;
    end;
  end;
end;

procedure TfmBuyers.ViewClCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

procedure TfmBuyers.ViewClKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
    if bSelCli then
    begin
      bSelCli:=False;
      ModalResult:=mrOk;
    end;
end;

procedure TfmBuyers.SpeedItem5Click(Sender: TObject);
begin
  prNExportExel5(ViewCl);
end;

procedure TfmBuyers.BuyersRefr();
begin
  ViewCl.BeginUpdate;
  try
    qBuyers.Close;
    qBuyers.sql.Clear;
    qBuyers.SQL.Add('select buyer_id');
    qBuyers.SQL.Add('     , buyer_f');
    qBuyers.SQL.Add('     , buyer_i');
    qBuyers.SQL.Add('     , buyer_o');
    qBuyers.SQL.Add('     , buyer_passportseria');
    qBuyers.SQL.Add('     , buyer_passportnomer');
    qBuyers.SQL.Add('     , buyer_passportissuer');
    qBuyers.SQL.Add('     , buyer_passportissuedate');
    qBuyers.SQL.Add('     , buyer_address');
    qBuyers.SQL.Add('     , PHONE');
    qBuyers.SQL.Add('from of_buyers order by buyer_f, buyer_i, buyer_o');

    qBuyers.Transaction.StartTransaction;
    CloseTe(fmBuyers.teBuyers);
    qBuyers.ExecQuery;
      While NOT fmBuyers.qBuyers.Eof do
        begin
          fmBuyers.teBuyers.Append;
            fmBuyers.teBuyersBUYER_ID.AsInteger:=fmBuyers.qBuyers.FldByName['buyer_id'].AsInteger;
            fmBuyers.teBuyersBUYER_FIO.AsString:=fmBuyers.qBuyers.FldByName['buyer_f'].AsString+
                                    ' '+fmBuyers.qBuyers.FldByName['buyer_i'].AsString+
                                    ' '+fmBuyers.qBuyers.FldByName['buyer_o'].AsString;
            fmBuyers.teBuyersBUYER_F.AsString:=fmBuyers.qBuyers.FldByName['buyer_f'].AsString;
            fmBuyers.teBuyersBUYER_I.AsString:=fmBuyers.qBuyers.FldByName['buyer_i'].AsString;
            fmBuyers.teBuyersBUYER_O.AsString:=fmBuyers.qBuyers.FldByName['buyer_o'].AsString;
            fmBuyers.teBuyersBUYER_PASSPORTSERIA.AsInteger:=fmBuyers.qBuyers.FldByName['buyer_passportseria'].AsInteger;
            fmBuyers.teBuyersBUYER_PASSPORTNOMER.AsInteger:=fmBuyers.qBuyers.FldByName['buyer_passportnomer'].AsInteger;
            fmBuyers.teBuyersBUYER_PASSPORTISSUER.AsString:=fmBuyers.qBuyers.FldByName['buyer_passportissuer'].AsString;
            fmBuyers.teBuyersBUYER_PASSPORTISSUEDATE.AsDateTime:=fmBuyers.qBuyers.FldByName['buyer_passportissuedate'].AsDateTime;
            fmBuyers.teBuyersBUYER_ADDRESS.AsString:=fmBuyers.qBuyers.FldByName['buyer_address'].AsString;
            fmBuyers.teBuyersPHONE.AsString:=fmBuyers.qBuyers.FldByName['PHONE'].AsString;
          fmBuyers.teBuyers.Post;
          fmBuyers.qBuyers.Next;
        end;
    fmBuyers.qBuyers.Transaction.Commit;
  finally
    ViewCl.EndUpdate;
  end;
end;


procedure TfmBuyers.FormShow(Sender: TObject);
begin
  BuyersRefr();
end;

end.
