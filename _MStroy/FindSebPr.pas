unit FindSebPr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCurrencyEdit;

type
  TfmFindSebPr = class(TForm)
    VSPr: TcxGridDBTableView;
    LSPr: TcxGridLevel;
    GrSPr: TcxGrid;
    VSPrPRICEIN: TcxGridDBColumn;
    procedure VSPrDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindSebPr: TfmFindSebPr;

implementation

uses DMOReps;

{$R *.dfm}

procedure TfmFindSebPr.VSPrDblClick(Sender: TObject);
begin
  Modalresult:=mrOk;
end;

end.
