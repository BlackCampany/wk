unit MainDiscount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar, FR_DSet, FR_DBSet,
  FR_Class;

type
  TfmMainDiscount = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    acDiscCards: TAction;
    acPlatCards: TAction;
    acTypePl: TAction;
    acDiscRep: TAction;
    acRealPCRep: TAction;
    frRep1: TfrReport;
    acPCCli: TAction;
    acEditBar: TAction;
    acEditIndex: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDiscCardsExecute(Sender: TObject);
    procedure acPlatCardsExecute(Sender: TObject);
    procedure acTypePlExecute(Sender: TObject);
    procedure acDiscRepExecute(Sender: TObject);
    procedure acRealPCRepExecute(Sender: TObject);
    procedure acPCCliExecute(Sender: TObject);
    procedure acEditBarExecute(Sender: TObject);
    procedure acEditIndexExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainDiscount: TfmMainDiscount;

implementation

uses Un1, dmRnDisc, Discount, TypePl, PCard, Period, PCDetail,
  PerA_Disc, RepPCCli, repDiscCli;

{$R *.dfm}

procedure TfmMainDiscount.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainDiscount.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  TrebSel.DateFrom:=Trunc(Date-1);
  TrebSel.DateTo:=Trunc(Date);
end;

procedure TfmMainDiscount.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainDiscount.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmCDisc do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
//���  fmPerA_Disc:=TfmPerA_Disc.Create(Application);
  fmPerA_Disc:=TfmPerA_Disc.Create(Application);
  fmPerA_Disc.ShowModal;
  if fmPerA_Disc.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA_Disc.Release;

    bMenuList:=False;
    fmRnDisc:=tfmRnDisc.Create(Application);

    fmRnDisc.Show;
    bMenuList:=True;

  end
  else
  begin
    fmPerA_Disc.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainDiscount.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainDiscount.acDiscCardsExecute(Sender: TObject);
begin
  fmRnDisc.Show;
//���������� �����
end;

procedure TfmMainDiscount.acPlatCardsExecute(Sender: TObject);
begin
//��������� �����
  with dmCDisc do
  begin
    taPCard.Active:=False;
    taPCard.Active:=True;
  end;
  fmPCard.Show;
end;

procedure TfmMainDiscount.acTypePlExecute(Sender: TObject);
begin
//���� ��������� ����
  fmTypePl.Show;
end;

procedure TfmMainDiscount.acDiscRepExecute(Sender: TObject);
begin
  //������ �� ���������� ������
//  if not CanDo('SaleGroup') then  StatusBar1.Panels[0].Text:='��� ����.';

  with dmCDisc do
  begin
    quRep1.Active:=False;
    quRep1.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quRep1.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quRep1.Active:=True;
  end;

  fmDiscCli.Caption:='������ �� �� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmDiscCli.Show;



{  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);
  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin

    with dmCDisc do
    begin
      quRep1.Active:=False;
      quRep1.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quRep1.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;


      frRep1.LoadFromFile(CurDir + 'DiscSum.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
//      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRep1.ReportName:='������ �� ���������� ������.';
      frRep1.PrepareReport;
      frRep1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;             }

end;

procedure TfmMainDiscount.acRealPCRepExecute(Sender: TObject);
begin
  with dmCDisc do
  begin
    quPCDet.Active:=False;
    quPCDet.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quPCDet.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quPCDet.Active:=True;
  end;

  fmPCDet.Caption:='��������� ����� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmPCDet.Show;
end;

procedure TfmMainDiscount.acPCCliExecute(Sender: TObject);
begin
  with dmCDisc do
  begin
    quPCCli.Active:=False;
    quPCCli.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quPCCli.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quPCCli.Active:=True;
  end;

  fmPCCli.Caption:='���������� �� �� �� �������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmPCCli.Show;
end;

procedure TfmMainDiscount.acEditBarExecute(Sender: TObject);
Var StrWk:String;
begin
  //�������� � �������� ��������� ������ = ����� 3-��� � 11-��� �����
  with dmCDisc do
  begin
    if MessageDlg('�������� = ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quDiscAll.Active:=False;
      quDiscAll.Active:=True;

      quDiscAll.First;
      trUpdate.StartTransaction;
      while not quDiscAll.Eof do
      begin
        StrWk:=quDiscAllBARCODE.AsString;
        if pos('=',StrWk)=0 then
        begin
          if Length(StrWk)>11 then
          begin
            insert('=',StrWk,12);
            insert('=',StrWk,4);

            quDiscAll.Edit;
            quDiscAllBARCODE.AsString:=StrWk;
            quDiscAll.Post;
          end;
        end;

        quDiscAll.Next;
      end;
      trUpdate.Commit;
      quDiscAll.Active:=False;
    end;
  end;
end;

procedure TfmMainDiscount.acEditIndexExecute(Sender: TObject);
Var StrWk:String;
    iCode:INteger;
begin
  //�������� � �������� ��������� ������ = ����� 3-��� � 11-��� �����
  with dmCDisc do
  begin
    if MessageDlg('�������� ClientIndex ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quDiscAll.Active:=False;
      quDiscAll.Active:=True;

      quDiscAll.First;
      trUpdate.StartTransaction;
      while not quDiscAll.Eof do
      begin
        if quDiscAllCLIENTINDEX.AsInteger=0 then
        begin
          StrWk:=quDiscAllBARCODE.AsString;
          while pos('=',StrWk)>0 do delete(StrWk,1,pos('=',StrWk));

          iCode:=StrToIntDef(StrWk,0);
          if iCode>0 then
          begin
            quDiscAll.Edit;
            quDiscAllCLIENTINDEX.AsInteger:=iCode;
            quDiscAll.Post;
          end;
        end;
        quDiscAll.Next;
      end;
      trUpdate.Commit;
      quDiscAll.Active:=False;
    end;
  end;
end;

end.
