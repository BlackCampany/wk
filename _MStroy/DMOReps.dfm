object dmORep: TdmORep
  OldCreateOrder = False
  Left = 65526
  Top = 102
  Height = 730
  Width = 935
  object taPartTest: TClientDataSet
    Aggregates = <>
    FileName = 'PartTest.cds'
    Params = <>
    Left = 24
    Top = 16
    object taPartTestNum: TIntegerField
      FieldName = 'Num'
    end
    object taPartTestIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taPartTestNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taPartTestIM: TIntegerField
      FieldName = 'IM'
    end
    object taPartTestSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taPartTestQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taPartTestPrice1: TCurrencyField
      FieldName = 'Price1'
      DisplayFormat = ',0.00;-,0.00'
      EditFormat = ',0.00;-,0.00'
    end
    object taPartTestiRes: TSmallintField
      FieldName = 'iRes'
    end
  end
  object dsPartTest: TDataSource
    DataSet = taPartTest
    Left = 24
    Top = 64
  end
  object taCalc: TClientDataSet
    Aggregates = <>
    FileName = 'Calc.cds'
    Params = <>
    Left = 89
    Top = 16
    object taCalcArticul: TIntegerField
      FieldName = 'Articul'
    end
    object taCalcName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taCalcIdm: TIntegerField
      FieldName = 'Idm'
    end
    object taCalcsM: TStringField
      FieldName = 'sM'
    end
    object taCalcKm: TFloatField
      FieldName = 'Km'
    end
    object taCalcQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCalcQuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taCalcQuantDiff: TFloatField
      FieldName = 'QuantDiff'
      DisplayFormat = '0.000'
    end
    object taCalcSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taCalcSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
  end
  object dsCalc: TDataSource
    DataSet = taCalc
    Left = 89
    Top = 64
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 312
    Top = 16
    ReportForm = {19000000}
  end
  object frTextExport1: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 560
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 504
  end
  object frCSVExport1: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = ';'
    Left = 968
    Top = 344
  end
  object frHTMExport1: TfrHTMExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 200
  end
  object frHTML2Export1: TfrHTML2Export
    Scale = 1.000000000000000000
    Navigator.Position = []
    Navigator.Font.Charset = DEFAULT_CHARSET
    Navigator.Font.Color = clWindowText
    Navigator.Font.Height = -11
    Navigator.Font.Name = 'MS Sans Serif'
    Navigator.Font.Style = []
    Navigator.InFrame = False
    Navigator.WideInFrame = False
    Left = 968
    Top = 248
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    HighQuality = False
    CellsAlign = False
    CellsBorders = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    CellsMerged = False
    CellsWrapWords = False
    ExportPictures = False
    PageBreaks = False
    AsText = False
    Left = 968
    Top = 448
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 968
    Top = 400
  end
  object frTextAdvExport1: TfrTextAdvExport
    ScaleWidth = 1.000000000000000000
    ScaleHeight = 1.000000000000000000
    Borders = True
    Pseudogrpahic = False
    PageBreaks = True
    OEMCodepage = False
    EmptyLines = True
    LeadSpaces = True
    PrintAfter = False
    PrinterDialog = True
    UseSavedProps = True
    Left = 968
    Top = 296
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    Left = 968
    Top = 152
  end
  object frBMPExport1: TfrBMPExport
    Left = 968
    Top = 8
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 968
    Top = 56
  end
  object frTIFFExport1: TfrTIFFExport
    Left = 968
    Top = 104
  end
  object taCalcB: TClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    FileName = 'CalcB.cds'
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'CODEB'
        DataType = ftInteger
      end
      item
        Name = 'NAMEB'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'QUANT'
        DataType = ftFloat
      end
      item
        Name = 'PRICEOUT'
        DataType = ftFloat
      end
      item
        Name = 'SUMOUT'
        DataType = ftFloat
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'NAMEC'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'QUANTC'
        DataType = ftFloat
      end
      item
        Name = 'PRICEIN'
        DataType = ftFloat
      end
      item
        Name = 'SUMIN'
        DataType = ftFloat
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <
      item
        Name = 'taCalcBIndex'
        Fields = 'ID'
        Options = [ixPrimary]
      end>
    IndexFieldNames = 'ID'
    Params = <>
    StoreDefs = True
    Left = 161
    Top = 16
    object taCalcBID: TIntegerField
      FieldName = 'ID'
    end
    object taCalcBCODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object taCalcBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 30
    end
    object taCalcBQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taCalcBNAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 30
    end
    object taCalcBQUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object taCalcBIM: TIntegerField
      FieldName = 'IM'
    end
    object taCalcBSM: TStringField
      FieldName = 'SM'
    end
    object taCalcBSB: TStringField
      FieldName = 'SB'
      Size = 10
    end
  end
  object dsCalcB: TDataSource
    DataSet = taCalcB
    Left = 160
    Top = 64
  end
  object frdsCalcB: TfrDBDataSet
    DataSource = dsCalcB
    Left = 368
    Top = 16
  end
  object taTORep: TClientDataSet
    Aggregates = <>
    FileName = 'to.cds'
    FieldDefs = <
      item
        Name = 'sType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sDocType'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'sCliName'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'sDate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'sDocNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rSum'
        DataType = ftCurrency
      end
      item
        Name = 'rSum1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumPr'
        DataType = ftFloat
      end
      item
        Name = 'PriceType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sSumPr'
        DataType = ftString
        Size = 15
      end>
    IndexDefs = <
      item
        Name = 'taTORepIndex1'
        Fields = 'sType;sDocType'
      end>
    IndexName = 'taTORepIndex1'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 128
    object taTORepsType: TStringField
      FieldName = 'sType'
    end
    object taTORepsDocType: TStringField
      FieldName = 'sDocType'
      Size = 50
    end
    object taTORepsCliName: TStringField
      FieldName = 'sCliName'
      Size = 200
    end
    object taTORepsDate: TStringField
      FieldName = 'sDate'
      Size = 10
    end
    object taTORepsDocNum: TStringField
      FieldName = 'sDocNum'
    end
    object taTOReprSum: TCurrencyField
      FieldName = 'rSum'
    end
    object taTOReprSum1: TCurrencyField
      FieldName = 'rSum1'
    end
    object taTOReprSumO: TCurrencyField
      FieldName = 'rSumO'
    end
    object taTOReprSumO1: TCurrencyField
      FieldName = 'rSumO1'
    end
    object taTOReprSumPr: TFloatField
      FieldName = 'rSumPr'
    end
    object taTORepPriceType: TStringField
      FieldName = 'PriceType'
    end
    object taTORepsSumPr: TStringField
      FieldName = 'sSumPr'
      Size = 15
    end
  end
  object dsTORep: TDataSource
    DataSet = taTORep
    Left = 24
    Top = 184
  end
  object frdsTORep: TfrDBDataSet
    DataSource = dsTORep
    Left = 328
    Top = 72
  end
  object taCMove: TClientDataSet
    Aggregates = <>
    FileName = 'CMove.cds'
    Params = <>
    Left = 80
    Top = 128
    object taCMoveARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object taCMoveIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object taCMoveIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object taCMoveNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 200
    end
    object taCMoveRB: TFloatField
      FieldName = 'RB'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTIN: TFloatField
      FieldName = 'POSTIN'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTOUT: TFloatField
      FieldName = 'POSTOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveVNIN: TFloatField
      FieldName = 'VNIN'
      DisplayFormat = '0.000'
    end
    object taCMoveVNOUT: TFloatField
      FieldName = 'VNOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveINV: TFloatField
      FieldName = 'INV'
      DisplayFormat = '0.000'
    end
    object taCMoveQREAL: TFloatField
      FieldName = 'QREAL'
      DisplayFormat = '0.000'
    end
    object taCMoveRE: TFloatField
      FieldName = 'RE'
      DisplayFormat = '0.000'
    end
  end
  object dstaCMove: TDataSource
    DataSet = taCMove
    Left = 80
    Top = 184
  end
  object trSelRep: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 232
    Top = 120
  end
  object quDocOutBPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT vr.IDB,vr.CODEB,vr.QUANT,vr.SUMOUT,vr.IDSKL,vr.SUMIN,ds.N' +
        'AMEB,ds.PRICER,ca.NAME'
      'FROM OF_VREALB_DOC vr'
      
        'left join of_docspecoutb ds on ((ds.IDHEAD=:IDH)and(ds.IDCARD=vr' +
        '.CODEB)and(ds.ID=vr.IDB))'
      'left join of_Cards ca on ca.ID=ds.IDCARD')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 384
    Top = 192
    poAskRecordCount = True
    object quDocOutBPrintIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quDocOutBPrintCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quDocOutBPrintQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocOutBPrintSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quDocOutBPrintIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocOutBPrintSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocOutBPrintNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocOutBPrintPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quDocOutBPrintNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSelInSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT IDATE,QPART,PRICEIN'
      'FROM OF_PARTIN'
      
        'where IDATE>=:DATEB and IDATE<:DATEE and IDSTORE=:IDSKL and ARTI' +
        'CUL=:IDGOOD and DTYPE=:ITYPE'
      'Order by IDATE DESC')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 432
    Top = 136
    object quSelInSumIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quSelInSumQPART: TFIBFloatField
      FieldName = 'QPART'
    end
    object quSelInSumPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
  end
  object quSelNamePos: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cl.NAMECL,ca.PARENT,ca.NAME,ME.NAMESHORT as SM,ME.KOEF,ca' +
        '.IMESSURE'
      'FROM OF_CARDS ca'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'left join OF_MESSUR ME on ME.ID=ca.IMESSURE'
      'where ca.ID=:IDCARD')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 504
    Top = 136
    poAskRecordCount = True
    object quSelNamePosNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelNamePosPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNamePosNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelNamePosSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSelNamePosKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
    object quSelNamePosIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object quSelOutSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT IDDATE,QUANT,PRICEIN,SUMOUT'
      'FROM OF_PARTOUT'
      
        'where IDDATE>=:DATEB and IDDATE<=:DATEE and IDSTORE=:IDSKL and A' +
        'RTICUL=:IDGOOD and DTYPE=1'
      'order by IDDATE desc')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 464
    Top = 200
    poAskRecordCount = True
    object quSelOutSumIDDATE: TFIBIntegerField
      FieldName = 'IDDATE'
    end
    object quSelOutSumQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelOutSumPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSelOutSumSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
  end
  object quSelNameCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ca.PARENT, cl.NAMECL FROM OF_CARDS ca'
      'left join OF_Classif cl on cl.ID=ca.PARENT'
      'where ca.ID=:IDC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 576
    Top = 136
    poAskRecordCount = True
    object quSelNameClPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNameClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quDocsCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDSKLTO = :IDSKLTO,'
      '    IDFROM = :IDFROM,'
      '    TOREAL = :TOREAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDSKLTO,'
      '    IDFROM,'
      '    TOREAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDSKLTO,'
      '    :IDFROM,'
      '    :TOREAL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH,'
      'mh1.NAMEMH as NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH,'
      'mh1.NAMEMH as NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<=:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 552
    Top = 200
    poAskRecordCount = True
    object quDocsComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComplDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComplNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComplIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
      DisplayFormat = '0.00'
    end
    object quDocsComplPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
      DisplayFormat = '0.0'
    end
    object quDocsComplIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComplOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsComplNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsComplIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
    object quDocsComplIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsComplTOREAL: TFIBSmallIntField
      FieldName = 'TOREAL'
    end
    object quDocsComplNAMEMH1: TFIBStringField
      FieldName = 'NAMEMH1'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquDocsCompl: TDataSource
    DataSet = quDocsCompl
    Left = 536
    Top = 256
  end
  object quDocsComlRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDFROM = :IDFROM,'
      '    TOREAL = :TOREAL,'
      '    IDSKLTO = :IDSKLTO'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDFROM,'
      '    TOREAL,'
      '    IDSKLTO'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDFROM,'
      '    :TOREAL,'
      '    :IDSKLTO'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDFROM,TOREAL,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADCOMPL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDFROM,TOREAL,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 632
    Top = 200
    poAskRecordCount = True
    object quDocsComlRecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComlRecDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComlRecNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComlRecIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComlRecSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsComlRecSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsComlRecSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsComlRecPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsComlRecIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComlRecOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsComlRecIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsComlRecTOREAL: TFIBSmallIntField
      FieldName = 'TOREAL'
    end
    object quDocsComlRecIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
  end
  object quSpecCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLB'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLB('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH'
      'order by s.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 780
    Top = 124
    poAskRecordCount = True
    object quSpecComplIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecComplNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecComplC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLC'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLC('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  s.IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 780
    Top = 224
    poAskRecordCount = True
    object quSpecComplCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplCPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplCSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecComplCB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLCB'
      'SET '
      '    CODEB = :CODEB,'
      '    NAMEB = :NAMEB,'
      '    QUANT = :QUANT,'
      '    IDCARD = :IDCARD,'
      '    NAMEC = :NAMEC,'
      '    QUANTC = :QUANTC,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    IM = :IM,'
      '    SM = :SM,'
      '    SB = :SB'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLCB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLCB('
      '    IDHEAD,'
      '    IDB,'
      '    ID,'
      '    CODEB,'
      '    NAMEB,'
      '    QUANT,'
      '    IDCARD,'
      '    NAMEC,'
      '    QUANTC,'
      '    PRICEIN,'
      '    SUMIN,'
      '    IM,'
      '    SM,'
      '    SB'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :IDB,'
      '    :ID,'
      '    :CODEB,'
      '    :NAMEB,'
      '    :QUANT,'
      '    :IDCARD,'
      '    :NAMEC,'
      '    :QUANTC,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :IM,'
      '    :SM,'
      '    :SB'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECCOMPLCB.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECCOMPLCB.IDB = :OLD_IDB'
      '    and OF_DOCSPECCOMPLCB.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where IDHEAD=:IDH'
      '')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 776
    Top = 172
    poAskRecordCount = True
    object quSpecComplCBIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCBIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quSpecComplCBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCBCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecComplCBNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCBIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCBNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANTC: TFIBFloatField
      FieldName = 'QUANTC'
    end
    object quSpecComplCBPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCBIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecComplCBSM: TFIBStringField
      FieldName = 'SM'
      EmptyStrToNull = True
    end
    object quSpecComplCBSB: TFIBStringField
      FieldName = 'SB'
      EmptyStrToNull = True
    end
  end
  object taHeadDoc: TClientDataSet
    Aggregates = <>
    FileName = 'HeadDoc.cds'
    Params = <>
    Left = 32
    Top = 296
    object taHeadDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taHeadDocId: TIntegerField
      FieldName = 'Id'
    end
    object taHeadDocDateDoc: TIntegerField
      FieldName = 'DateDoc'
    end
    object taHeadDocNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 15
    end
    object taHeadDocIdCli: TIntegerField
      FieldName = 'IdCli'
    end
    object taHeadDocNameCli: TStringField
      FieldName = 'NameCli'
      Size = 70
    end
    object taHeadDocIdSkl: TIntegerField
      FieldName = 'IdSkl'
    end
    object taHeadDocNameSkl: TStringField
      FieldName = 'NameSkl'
      Size = 70
    end
    object taHeadDocSumIN: TFloatField
      FieldName = 'SumIN'
      DisplayFormat = '0.00'
    end
    object taHeadDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taHeadDocComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
  end
  object taSpecDoc: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 100
    Top = 288
    object taSpecDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDocIdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDocNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDocIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDocQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDocPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocIdNds: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDocSumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDocNameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDocSm: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDocIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDocKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecDocPriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDocSumUch1: TFloatField
      FieldName = 'SumUch1'
    end
    object taSpecDocProcPrice: TFloatField
      FieldName = 'ProcPrice'
    end
  end
  object dsHeadDoc: TDataSource
    DataSet = taHeadDoc
    Left = 32
    Top = 340
  end
  object dsSpecDoc: TDataSource
    DataSet = taSpecDoc
    Left = 104
    Top = 340
  end
  object quDocsActs: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,mh.NAMEMH,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,mh.NAMEMH,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 240
    Top = 264
    poAskRecordCount = True
    object quDocsActsID2: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsActsSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsActsIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsActsNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsActsCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsquDocsActs: TDataSource
    DataSet = quDocsActs
    Left = 240
    Top = 312
  end
  object quDocsActsId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.ID=:IDH'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 320
    Top = 280
    poAskRecordCount = True
    object quDocsActsIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsActsIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsActsIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsIdOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsActsIdCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quDocsVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 368
    poAskRecordCount = True
    object quDocsVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnSelIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnSelIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnSelPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsVnSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsVnSelNAMEMH1: TFIBStringField
      FieldName = 'NAMEMH1'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsDocsVnSel: TDataSource
    DataSet = quDocsVnSel
    Left = 304
    Top = 424
  end
  object quSpecVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECVN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    PRICEUCH1 = :PRICEUCH1,'
      '    SUMUCH1 = :SUMUCH1,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECVN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECVN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    PRICEUCH1,'
      '    SUMUCH1,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :PRICEUCH1,'
      '    :SUMUCH1,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM, C.CATEGORY'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where SP.IDHEAD=:IDHD'
      ''
      'ORDER BY SP.NUM')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 472
    object quSpecVnSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecVnSelNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecVnSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecVnSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecVnSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecVnSelPRICEUCH: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object quSpecVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecVnSelPRICEUCH1: TFIBFloatField
      FieldName = 'PRICEUCH1'
    end
    object quSpecVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quSpecVnSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecVnSelNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecVnSelSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecVnSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecVnSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
  end
  object quDocsVnId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADVN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 232
    Top = 368
    poAskRecordCount = True
    object quDocsVnIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnIdIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnIdIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsVnIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsVnIdSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quDocsVnIdSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnIdPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quSpecAO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSOUT'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSOUT'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSOUT('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 256
    poAskRecordCount = True
    object quSpecAOIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAOIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAOQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAOIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAOKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAOPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAOSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAOPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAOSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAOTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAONAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAONAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecAI: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSIN'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD,'
      '    PROCPRICE = :PROCPRICE'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSIN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSIN('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD,'
      '    PROCPRICE'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD,'
      '    :PROCPRICE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME,so.PROCPRICE'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME,so.PROCPRICE'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 312
    poAskRecordCount = True
    object quSpecAIIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAIID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAIIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAIQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAIIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAIKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAIPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAISUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAIPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAISUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAITCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAINAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAINAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecAIPROCPRICE: TFIBFloatField
      FieldName = 'PROCPRICE'
    end
  end
  object taSpecB: TClientDataSet
    Aggregates = <>
    FileName = 'SpecB.cds'
    Params = <>
    Left = 232
    Top = 16
    object taSpecBID: TIntegerField
      FieldName = 'ID'
    end
    object taSpecBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taSpecBQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object taSpecBIDM: TIntegerField
      FieldName = 'IDM'
    end
    object taSpecBSIFR: TIntegerField
      FieldName = 'SIFR'
    end
    object taSpecBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 100
    end
    object taSpecBCODEB: TStringField
      FieldName = 'CODEB'
      Size = 10
    end
    object taSpecBKB: TFloatField
      FieldName = 'KB'
    end
    object taSpecBPRICER: TFloatField
      FieldName = 'PRICER'
    end
    object taSpecBDSUM: TCurrencyField
      FieldName = 'DSUM'
    end
    object taSpecBRSUM: TCurrencyField
      FieldName = 'RSUM'
    end
    object taSpecBNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taSpecBNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
    end
    object taSpecBTCARD: TSmallintField
      FieldName = 'TCARD'
    end
    object taSpecBKM: TFloatField
      FieldName = 'KM'
    end
  end
  object dstaSpecB: TDataSource
    DataSet = taSpecB
    Left = 232
    Top = 64
  end
  object quOperT: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER,'
      '    TPRIB = :TPRIB,'
      '    TSPIS = :TSPIS'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER,'
      '    TPRIB,'
      '    TSPIS'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER,'
      '    :TPRIB,'
      '    :TSPIS'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,'
      'd.NAMEDOC,op.TPRIB,op.TSPIS'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,'
      'd.NAMEDOC,op.TPRIB,op.TSPIS'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where op.ID>0')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 456
    Top = 264
    object quOperTABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object quOperTID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quOperTDOCTYPE: TFIBSmallIntField
      FieldName = 'DOCTYPE'
    end
    object quOperTNAMEOPER: TFIBStringField
      FieldName = 'NAMEOPER'
      Size = 30
      EmptyStrToNull = True
    end
    object quOperTNAMEDOC: TFIBStringField
      FieldName = 'NAMEDOC'
      Size = 30
      EmptyStrToNull = True
    end
    object quOperTTPRIB: TFIBSmallIntField
      FieldName = 'TPRIB'
    end
    object quOperTTSPIS: TFIBSmallIntField
      FieldName = 'TSPIS'
    end
  end
  object dsquOperT: TDataSource
    DataSet = quOperT
    Left = 456
    Top = 320
  end
  object taDocType: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCTYPE'
      'SET '
      '    NAMEDOC = :NAMEDOC'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCTYPE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCTYPE('
      '    ID,'
      '    NAMEDOC'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMEDOC'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDOC'
      'FROM'
      '    OF_DOCTYPE '
      ''
      ' WHERE '
      '        OF_DOCTYPE.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDOC'
      'FROM'
      '    OF_DOCTYPE ')
    Transaction = dmO.trSel2
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 800
    Top = 292
    object taDocTypeID: TFIBSmallIntField
      FieldName = 'ID'
    end
    object taDocTypeNAMEDOC: TFIBStringField
      FieldName = 'NAMEDOC'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dstaDocType: TDataSource
    DataSet = taDocType
    Left = 800
    Top = 344
  end
  object taOperT: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,d.NAMEDOC'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID'
      'FROM OF_OPER op'
      'where op.ABR=:ABR'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 528
    Top = 312
    poAskRecordCount = True
    object taOperTABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object taOperTID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object taOperD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,d.NAMEDOC'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER'
      'FROM OF_OPER op'
      'where op.DOCTYPE=:IDT'
      'order by op.NAMEOPER')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 584
    Top = 312
    object taOperDABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object taOperDID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taOperDDOCTYPE: TFIBSmallIntField
      FieldName = 'DOCTYPE'
    end
    object taOperDNAMEOPER: TFIBStringField
      FieldName = 'NAMEOPER'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dstaOperD: TDataSource
    DataSet = taOperD
    Left = 584
    Top = 368
  end
  object taSpecDoc1: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc1.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 152
    Top = 316
    object taSpecDoc1IType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDoc1IdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDoc1Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDoc1IdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDoc1Quant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDoc1PriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1SumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1PriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1SumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1IdNDS: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDoc1SumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDoc1NameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDoc1SM: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDoc1IDM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDoc1Km: TFloatField
      FieldName = 'Km'
    end
    object taSpecDoc1PriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDoc1SumUch1: TFloatField
      FieldName = 'SumUch1'
    end
  end
  object quSpecVnGrPos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECVN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    PRICEUCH1 = :PRICEUCH1,'
      '    SUMUCH1 = :SUMUCH1,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECVN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECVN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    PRICEUCH1,'
      '    SUMUCH1,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :PRICEUCH1,'
      '    :SUMUCH1,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where SP.IDHEAD=:IDHD'
      ''
      'ORDER BY SP.NUM')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 528
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object FIBIntegerField2: TFIBIntegerField
      FieldName = 'ID'
    end
    object FIBIntegerField3: TFIBIntegerField
      FieldName = 'NUM'
    end
    object FIBIntegerField4: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object FIBFloatField1: TFIBFloatField
      FieldName = 'QUANT'
    end
    object FIBFloatField2: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object FIBFloatField3: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object FIBFloatField4: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object FIBFloatField5: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object FIBFloatField6: TFIBFloatField
      FieldName = 'PRICEUCH1'
    end
    object FIBFloatField7: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object FIBIntegerField5: TFIBIntegerField
      FieldName = 'IDM'
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object FIBFloatField8: TFIBFloatField
      FieldName = 'KM'
    end
  end
  object prTestSpecPart: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_TESTSPECPART (?IDDOC, ?DTYPE, ?DSUM)')
    StoredProcName = 'PR_TESTSPECPART'
    Left = 32
    Top = 412
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prResetSum: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RESETSUM (?IDDOC, ?DTYPE)')
    StoredProcName = 'PR_RESETSUM'
    Left = 32
    Top = 468
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quCardPO: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(QUANT)as QUANT,SUM(PRICEIN*QUANT)as RSUM'
      'FROM OF_PARTOUT'
      'where ARTICUL=:IDCARD and IDDOC=:IDH and DTYPE=:DT'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 112
    Top = 412
    poAskRecordCount = True
    object quCardPOQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quCardPORSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
  end
  object prCalcSumRemn: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_CALCREMNSUM (?IDCARD, ?IDSKL, ?DDATE)')
    StoredProcName = 'PR_CALCREMNSUM'
    Left = 112
    Top = 468
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object taDelCard: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID,SIFR,NAME,TTYPE,IMESSURE'
      'FROM'
      '    OF_CARDS_DEL ')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 32
    Top = 532
    poAskRecordCount = True
    object taDelCardID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDelCardSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taDelCardNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taDelCardTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object taDelCardIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object prRECALCGDS: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCGDSMOVE (?DDATE, ?IDATE)')
    StoredProcName = 'PR_RECALCGDSMOVE'
    Left = 184
    Top = 532
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trD: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 112
    Top = 532
  end
  object quClass: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ca.ID,ca.NAME,ca.PARENT,ca.IMESSURE,cl.NAMECL'
      'FROM OF_CARDS ca'
      'left join OF_CLASSIF cl on (cl.ID=ca.PARENT)and(cl.ITYPE=1)'
      ''
      'where PARENT=:IPARENT and CATEGORY<2')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 632
    Top = 432
    poAskRecordCount = True
    object quClassID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quClassNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quClassPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quClassIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quClassNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quFindPart: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT pi.ID,pi.IDSTORE,pi.IDDOC,pi.ARTICUL,pi.IDCLI,pi.DTYPE,'
      'pi.QPART,pi.QREMN,pi.PRICEIN,pi.IDATE,Cli.NAMECL,mh.NAMEMH'
      'FROM OF_PARTIN pi'
      'left join OF_CLIENTS cli on pi.IDCLI=Cli.ID'
      'left join OF_MH mh on pi.IDSTORE=mh.ID'
      ''
      'where ARTICUL=:IDCARD and pi.QREMN>0.001 and pi.IDSTORE=:IDSTORE'
      'Order by pi.IDATE DESC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 704
    Top = 432
    poAskRecordCount = True
    object quFindPartID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindPartIDSTORE: TFIBIntegerField
      FieldName = 'IDSTORE'
    end
    object quFindPartIDDOC: TFIBIntegerField
      FieldName = 'IDDOC'
    end
    object quFindPartARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quFindPartIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quFindPartDTYPE: TFIBIntegerField
      FieldName = 'DTYPE'
    end
    object quFindPartQPART: TFIBFloatField
      FieldName = 'QPART'
      DisplayFormat = '0.000'
    end
    object quFindPartQREMN: TFIBFloatField
      FieldName = 'QREMN'
      DisplayFormat = '0.000'
    end
    object quFindPartPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object quFindPartIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quFindPartNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quFindPartNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quMainClass: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID FROM OF_CLASSIF'
      'WHERE ID_PARENT=0')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 632
    Top = 496
    poAskRecordCount = True
    object quMainClassID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object pr_CalcLastPrice: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_CALCLASTPRICE2 (?IDGOOD, ?IDSKL, ?IDATE)')
    StoredProcName = 'PR_CALCLASTPRICE2'
    Left = 200
    Top = 440
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quMovePer: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      'SUM(POSTIN)as POSTIN,'
      'SUM(POSTOUT)as POSTOUT,'
      'SUM(VNIN)as VNIN,'
      'SUM(VNOUT)as VNOUT,'
      'SUM(INV)as INV,'
      'SUM(QREAL)as QREAL'
      'FROM OF_GDSMOVE'
      
        'where ARTICUL=:IDGOOD and IDATE>=:DATEB and IDATE<:DATEE and IDS' +
        'TORE=:IDSTORE')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 704
    Top = 496
    object quMovePerPOSTIN: TFIBFloatField
      FieldName = 'POSTIN'
    end
    object quMovePerPOSTOUT: TFIBFloatField
      FieldName = 'POSTOUT'
    end
    object quMovePerVNIN: TFIBFloatField
      FieldName = 'VNIN'
    end
    object quMovePerVNOUT: TFIBFloatField
      FieldName = 'VNOUT'
    end
    object quMovePerINV: TFIBFloatField
      FieldName = 'INV'
    end
    object quMovePerQREAL: TFIBFloatField
      FieldName = 'QREAL'
    end
  end
  object quFindSebReal: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT first 5 IDM,KM,PRICEIN'
      'FROM OF_DOCSPECOUTR'
      'where IDCARD=:IDCARD and QUANT>0'
      'order by IDHEAD desc')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 792
    Top = 432
    poAskRecordCount = True
    object quFindSebRealIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quFindSebRealKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quFindSebRealPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
  end
  object dsquFindSebReal: TDataSource
    DataSet = quFindSebReal
    Left = 788
    Top = 492
  end
  object quFindCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID_PARENT,NAMECL'
      'FROM OF_CLASSIF'
      'where ITYPE=1'
      'and ID=:IDCL')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 152
    Top = 128
    poAskRecordCount = True
    object quFindClID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object quFindClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quFindMassa: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT first 1 POUTPUT'
      'FROM OF_CARDST'
      'where IDCARD=:IDCARD'
      'order by DATEB desc')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 152
    Top = 184
    poAskRecordCount = True
    object quFindMassaPOUTPUT: TFIBStringField
      FieldName = 'POUTPUT'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object quFindTSpis: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT TSPIS FROM OF_OPER'
      'where ABR=:ABR'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 232
    Top = 184
    poAskRecordCount = True
    object quFindTSpisTSPIS: TFIBSmallIntField
      FieldName = 'TSPIS'
    end
  end
  object quDB: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDB,'
      '    PATHDB'
      'FROM'
      '    OF_DB ')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 400
    Top = 552
    poAskRecordCount = True
    object quDBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDBNAMEDB: TFIBStringField
      FieldName = 'NAMEDB'
      Size = 30
      EmptyStrToNull = True
    end
    object quDBPATHDB: TFIBStringField
      FieldName = 'PATHDB'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object dsquDB: TDataSource
    DataSet = quDB
    Left = 472
    Top = 552
  end
  object quClosePartIn: TpFIBQuery
    Transaction = trU
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'update OF_PARTIN'
      'Set QREMN=0'
      'where ARTICUL=:IDCARD'
      'and IDSTORE=:IDSKL'
      'and IDATE<=:IDATE')
    Left = 304
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trU: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 328
    Top = 192
  end
  object quPODoc: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(QUANT*PRICEIN)as SUMIN '
      'FROM OF_PARTOUT'
      'where ARTICUL=:IDCARD'
      'and DTYPE=:IDTYPE'
      'and IDDOC=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 608
    Top = 568
    poAskRecordCount = True
    object quPODocSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object quFindCli: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT first 2 ID,NAMECL'
      'FROM OF_CLIENTS'
      'where UPPER(NAMECL) like '#39'%'#1040#1064'%'#39)
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 184
    Top = 240
    poAskRecordCount = True
    object quFindCliID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindCliNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object taCardM: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 24
    Top = 239
    object taCardMIdDoc: TIntegerField
      FieldName = 'IdDoc'
    end
    object taCardMTypeD: TIntegerField
      FieldName = 'TypeD'
    end
    object taCardMDateD: TDateField
      FieldName = 'DateD'
    end
    object taCardMsFrom: TStringField
      FieldName = 'sFrom'
      Size = 50
    end
    object taCardMsTo: TStringField
      FieldName = 'sTo'
      Size = 50
    end
    object taCardMiM: TIntegerField
      FieldName = 'iM'
    end
    object taCardMsM: TStringField
      FieldName = 'sM'
      Size = 10
    end
    object taCardMQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCardMQRemn: TFloatField
      FieldName = 'QRemn'
      DisplayFormat = '0.000'
    end
    object taCardMComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
  end
  object dstaCardM: TDataSource
    DataSet = taCardM
    Left = 80
    Top = 239
  end
  object quTestInput: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTB'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTB('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dhb.ID,dhb.DATEDOC,dhb.NUMDOC,dhb.IDCLI,dhb.IDSKL,mh.NAME' +
        'MH,dhb.SUMIN,dhb.IACTIVE,dhb.OPER,dhb.COMMENT'
      'FROM OF_DOCHEADOUTB dhb'
      'left join of_mh mh on mh.ID=dhb.IDSKL'
      'where(  dhb.DATEDOC=:DDATE and dhb.IDCLI=:IDB'
      '     ) and (     DHB.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dhb.ID,dhb.DATEDOC,dhb.NUMDOC,dhb.IDCLI,dhb.IDSKL,mh.NAME' +
        'MH,dhb.SUMIN,dhb.IACTIVE,dhb.OPER,dhb.COMMENT'
      'FROM OF_DOCHEADOUTB dhb'
      'left join of_mh mh on mh.ID=dhb.IDSKL'
      'where dhb.DATEDOC=:DDATE and dhb.IDCLI=:IDB')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 400
    Top = 608
    poAskRecordCount = True
    object quTestInputID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quTestInputDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quTestInputNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quTestInputIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quTestInputIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quTestInputNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quTestInputSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quTestInputIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quTestInputOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quTestInputCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquTestInput: TDataSource
    DataSet = quTestInput
    Left = 472
    Top = 608
  end
end
