unit PCDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, ComCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, FR_Class,
  FR_DSet, FR_DBSet, ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmPCDet = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    GridPCDet: TcxGrid;
    ViewPCDet: TcxGridDBTableView;
    LevelPCDet: TcxGridLevel;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    ViewPCDetNUMTABLE: TcxGridDBColumn;
    ViewPCDetQUESTS: TcxGridDBColumn;
    ViewPCDetENDTIME: TcxGridDBColumn;
    ViewPCDetDISCONT: TcxGridDBColumn;
    ViewPCDetCLINAME: TcxGridDBColumn;
    ViewPCDetTABSUM: TcxGridDBColumn;
    ViewPCDetNAME: TcxGridDBColumn;
    ViewPCDetSIFR: TcxGridDBColumn;
    ViewPCDetPRICE: TcxGridDBColumn;
    ViewPCDetQUANTITY: TcxGridDBColumn;
    ViewPCDetDISCOUNTPROC: TcxGridDBColumn;
    ViewPCDetDISCOUNTSUM: TcxGridDBColumn;
    ViewPCDetSUMMA: TcxGridDBColumn;
    ViewPCDetNAME1: TcxGridDBColumn;
    SpeedItem1: TSpeedItem;
    frRep1: TfrReport;
    frquPCDet: TfrDBDataSet;
    ViewPCDetNAMETYPE: TcxGridDBColumn;
    ViewPCDetCOMMENT: TcxGridDBColumn;
    ViewPCDetDATETO: TcxGridDBColumn;
    ViewPCDetDCNAME: TcxGridDBColumn;
    SpeedItem2: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmPCDet: TfmPCDet;

implementation

uses Un1, DmRnDisc, Period, MainDiscount;

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// L��� CLSID OLE-������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // +���� ������
    Result := true
  else
    Result := false;
end;


procedure TfmPCDet.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCDet.Align:=AlClient;
  ViewPCDet.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCDet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCDet.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCDet.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPCDet.SpeedItem3Click(Sender: TObject);
begin
//
//
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);

  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;

      ViewPCDet.BeginUpdate;
      quPCDet.Active:=False;
      quPCDet.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quPCDet.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quPCDet.Active:=True;
      ViewPCDet.EndUpdate;

      fmPCDet.Caption:='��������� ����� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      exit;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmPCDet.SpeedItem1Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  with dmCDisc do
  begin
    quPCDet.Filter:=ViewPCDet.DataController.Filter.FilterText;
    quPCDet.Filtered:=True;

    frRep1.LoadFromFile(CurDir + 'PCDet.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);

    StrWk:=ViewPCDet.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRep1.ReportName:='��������� ����� (���������������� �����).';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;

    quPCDet.Filter:='';
    quPCDet.Filtered:=False;
  end;

end;

procedure TfmPCDet.SpeedItem2Click(Sender: TObject);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow:Integer;
    i:Integer;
    NameF:String;
begin
// ����� � Excel
  with dmCDisc do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    ViewPCDet.BeginUpdate;

    dsPCDet.DataSet:=nil;

    quPCDet.Filter:=ViewPCDet.DataController.Filter.FilterText;
    quPCDet.Filtered:=True;

    //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=ViewPCDet.VisibleColumnCount;
    iRow:=quPCDet.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := ViewPCDet.VisibleColumns[i-1].Caption;
    end;

    quPCDet.First;
    iRow:=2;
    While not quPCDet.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=ViewPCDet.VisibleColumns[i-1].Name;
        delete(NameF,1,9); //������ �� �������� ������� ViewPCDet
        ArrayData[iRow,i] := quPCDet.Fields.FieldByName(NameF).Value;
      end;
      quPCDet.Next; //Delay(10);
      inc(iRow);
    end;

    StatusBar1.Panels[0].Text:='���� ������� ������.'; delay(10);

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+quPCDet.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    quPCDet.Filter:='';
    quPCDet.Filtered:=False;

    quPCDet.First;

    dsPCDet.DataSet:=quPCDet;

    ViewPCDet.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ ��.';

    ExcelApp.Visible := true;
  end;
end;

{

Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow:Integer;
    i:Integer;
    NameF:String;
begin
//
//������� � Excel
  with dmCRep do
  begin


//    Range.Borders.LineStyle := xlDouble;
//    Range.Borders.Color := RGB(0,0,0);

  end;
end;


}
end.
