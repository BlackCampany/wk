unit FCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ActnList, XPStyleActnCtrls, ActnMan,
  cxImageComboBox, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, ExtCtrls;

type
  TfmFCards = class(TForm)
    ViewFCards: TcxGridDBTableView;
    LevelFCards: TcxGridLevel;
    GridFCards: TcxGrid;
    ViewFCardsID: TcxGridDBColumn;
    ViewFCardsNAME: TcxGridDBColumn;
    ActionManager1: TActionManager;
    Action1: TAction;
    ViewFCardsRemn: TcxGridDBColumn;
    ViewFCardsGR: TcxGridDBColumn;
    ViewFCardsSGR: TcxGridDBColumn;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure ViewFCardsCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFCards: TfmFCards;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmFCards.cxButton1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmFCards.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmFCards.FormShow(Sender: TObject);
begin
  with dmO do
  begin
    if CurVal.IdMH2<>0 then
    begin
      cxLookupComboBox1.EditValue:=CurVal.IdMH2;
      cxLookupComboBox1.Text:=CurVal.NAMEMH2;
    end else
    begin
      if quMHr.Active=False then quMHr.Active:=True;
      quMHr.FullRefresh;
      quMHr.First;
      if quMHr.RecordCount>0 then
      begin
        CurVal.IdMH2:=quMHrID.AsInteger;
        CurVal.NAMEMH2:=quMHrNAMEMH.AsString;
        cxLookupComboBox1.EditValue:=CurVal.IdMH2;
        cxLookupComboBox1.Text:=CurVal.NAMEMH2;
      end;
    end;
  end;

  Label1.Caption:='����� - '+INtToStr(dmO.quFCard.RecordCount);
  GridFCards.SetFocus;
end;

procedure TfmFCards.Action1Execute(Sender: TObject);
begin
  ModalResult:=mrNone;
  Close;
end;

procedure TfmFCards.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
  CurVal.IdMH2:=cxLookupComboBox1.EditValue;
  CurVal.NAMEMH2:=cxLookupComboBox1.Text;
  with dmO do
  begin
    try
      ViewFCards.BeginUpdate;
      quFCard.FullRefresh;
    finally
      ViewFCards.EndUpdate;
    end;
  end;
end;

procedure TfmFCards.ViewFCardsCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    rQ:Real;
begin
  if AViewInfo.GridRecord.Selected then exit;

  rQ:=0;

  for i:=0 to ViewFCards.ColumnCount-1 do
  begin
    if ViewFCards.Columns[i].Name='ViewFCardsRemn' then
    begin
      rQ:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varDouble);
      break;
    end;
  end;

  if rQ<0 then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clRed;
  end;

end;

end.
