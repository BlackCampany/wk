unit AddDoc2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc, ActnList,
  XPStyleActnCtrls, ActnMan, cxImageComboBox, dxmdaset, StrUtils,Excel2000, OleServer, ExcelXP,ComObj,
  cxRadioGroup, FR_DSet, FR_DBSet, FR_Class, cxBlobEdit, cxSpinEdit;

type
  TfmAddDoc2 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    FormPlacement1: TFormPlacement;
    taSpecOut: TClientDataSet;
    dsSpec: TDataSource;
    taSpecOutNum: TIntegerField;
    taSpecOutIdGoods: TIntegerField;
    taSpecOutNameG: TStringField;
    taSpecOutIM: TIntegerField;
    taSpecOutSM: TStringField;
    taSpecOutQuant: TFloatField;
    taSpecOutPrice1: TCurrencyField;
    taSpecOutSum1: TCurrencyField;
    taSpecOutPrice2: TCurrencyField;
    taSpecOutSum2: TCurrencyField;
    taSpecOutINds: TIntegerField;
    taSpecOutSNds: TStringField;
    taSpecOutRNds: TCurrencyField;
    taSpecOutSumNac: TCurrencyField;
    taSpecOutProcNac: TFloatField;
    Image7: TImage;
    amDocOut: TActionManager;
    acSave: TAction;
    acExit: TAction;
    acAddPos: TAction;
    taSpecOutKM: TFloatField;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    acMovePos: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    taSpecOutCType: TSmallintField;
    Panel4: TPanel;
    cxLabel8: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel1: TcxLabel;
    Panel5: TPanel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxTextEdit3: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    teDocsSpec: TdxMemData;
    GridDoc2: TcxGrid;
    ViewDoc2: TcxGridDBTableView;
    LevelDoc2: TcxGridLevel;
    teDocsSpeccard_id: TIntegerField;
    teDocsSpeccard_count: TFloatField;
    teDocsSpeccard_price: TFloatField;
    teDocsSpeccard_sum: TFloatField;
    teDocsSpeccard_name: TStringField;
    teDocsSpecpost_name: TStringField;
    dsDocSpec: TDataSource;
    ViewDoc2card_id: TcxGridDBColumn;
    ViewDoc2card_count: TcxGridDBColumn;
    ViewDoc2card_price: TcxGridDBColumn;
    ViewDoc2card_sum: TcxGridDBColumn;
    ViewDoc2card_name: TcxGridDBColumn;
    ViewDoc2post_name: TcxGridDBColumn;
    teDocsSpecNum: TIntegerField;
    ViewDoc2Num: TcxGridDBColumn;
    qDocEdit: TpFIBQuery;
    cxButton3: TcxButton;
    teDocsSpecpost_id: TIntegerField;
    cxButton4: TcxButton;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    quDelVer: TpFIBQuery;
    Rep1: TfrReport;
    frteDocsSpec: TfrDBDataSet;
    cxBlobEdit1: TcxBlobEdit;
    cxLabel5: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxSpinEdit2: TcxSpinEdit;
    ViewVer: TcxGridDBTableView;
    LevelVer: TcxGridLevel;
    GrVer: TcxGrid;
    ViewVerIDV: TcxGridDBColumn;
    ViewVerPREDOPLATA: TcxGridDBColumn;
    ViewVerIZGOTOVLENIE: TcxGridDBColumn;
    ViewVerVPICT: TcxGridDBColumn;
    ViewVerVDATE: TcxGridDBColumn;
    teDocsSpeccard_pricein: TFloatField;
    teDocsSpeccard_sumin: TFloatField;
    cxTextEdit1: TcxTextEdit;
    PopupMenu2: TPopupMenu;
    N3: TMenuItem;
    acSetVer: TAction;
    teSp: TdxMemData;
    dxMemData2: TdxMemData;
    teSpCode: TIntegerField;
    teSpQuant: TFloatField;
    teSpName: TStringField;
    teSpIPost: TIntegerField;
    tePr: TdxMemData;
    tePrCode: TIntegerField;
    tePrQuant: TFloatField;
    tePrName: TStringField;
    teDocsSpecComment: TStringField;
    ViewDoc2Comment: TcxGridDBColumn;
    ViewDoc2card_pricein: TcxGridDBColumn;
    ViewDoc2card_sumin: TcxGridDBColumn;
    acSumIn: TAction;
    teDocsSpecDProc: TFloatField;
    teDocsSpecDSum: TFloatField;
    teDocsSpecRSum: TFloatField;
    ViewDoc2DProc: TcxGridDBColumn;
    ViewDoc2DSum: TcxGridDBColumn;
    ViewDoc2RSum: TcxGridDBColumn;
    acSetDisc: TAction;
    cxLabel11: TcxLabel;
    N4: TMenuItem;
    acEditPos: TAction;
    acAddShablon: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    acSetShabl: TAction;
    N7: TMenuItem;
    cxRadioButton3: TcxRadioButton;
    Label1: TLabel;
    cxButton5: TcxButton;
    frquPlat: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecOutQuantChange(Sender: TField);
    procedure taSpecOutPrice1Change(Sender: TField);
    procedure taSpecOutSum1Change(Sender: TField);
    procedure taSpecOutPrice2Change(Sender: TField);
    procedure taSpecOutSum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
//    procedure cxLabel5Click(Sender: TObject);
//    procedure cxLabel6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel8Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure ViewDoc2SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButtonEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure MenuItem1Click(Sender: TObject);
    procedure teDocsSpecBeforePost(DataSet: TDataSet);
    procedure cxTextEdit7KeyPress(Sender: TObject; var Key: Char);
    procedure DocOpen(idh:integer);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ExcelZakup();
    procedure cxButton3Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acSetVerExecute(Sender: TObject);
    procedure ViewVerDblClick(Sender: TObject);
    procedure ViewVerSelectionChanged(Sender: TcxCustomGridTableView);
    procedure ViewVerCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewVerCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acSumInExecute(Sender: TObject);
    procedure acSetDiscExecute(Sender: TObject);
    procedure acEditPosExecute(Sender: TObject);
    procedure cxLabel11Click(Sender: TObject);
    procedure acAddShablonExecute(Sender: TObject);
    procedure acSetShablExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc2: TfmAddDoc2;
  iCol:Integer = 0;
  iColT:Integer = 0;
  bAdd:Boolean = False;
  IdH,IdV:Integer;

implementation

uses Un1, dmOffice, Clients, Goods, SelPartIn, SelPartIn1, DMOReps,
  DocsOut, FCards, OMessure, SelPerSkl, Buyers, sumprops, SetNac, AddClass,
  Shabl, SetDate, Oplata;

{$R *.dfm}

procedure TfmAddDoc2.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  //PageControl1.Align:=alClient;
  //PageControl1.ActivePageIndex:=0;
  ViewDoc2.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc2.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin

(*  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;
  end;*)
end;

procedure TfmAddDoc2.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  fmBuyers.ShowModal;
  //ShowMessage(fmBuyers.teBuyersBUYER_ID.asstring);
  if fmBuyers.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=fmBuyers.teBuyersBUYER_ID.AsInteger;
    cxButtonEdit1.Text:=fmBuyers.teBuyersBUYER_FIO.AsString;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;
end;

procedure TfmAddDoc2.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  acAddPos.Execute;
end;

procedure TfmAddDoc2.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrRet then  Accept:=True;
end;

procedure TfmAddDoc2.ViewDoc2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    S1:String;
begin
//
  if bDrRet then
  begin
    ResetAddVars;

    iCo:=fmCards.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewDoc2.BeginUpdate;
        try
          iMax:=1;
          fmAddDoc2.teDocsSpec.First;
          while not fmAddDoc2.teDocsSpec.Eof do
          begin
            if iMax<fmAddDoc2.teDocsSpecNum.AsInteger+1 then iMax:=fmAddDoc2.teDocsSpecNum.AsInteger+1;
            fmAddDoc2.teDocsSpec.Next;
          end;

          with dmO do
          begin
            for i:=0 to fmCards.ViewGoods.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmCards.ViewGoods.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmCards.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
              end;

              iNum:=Rec.Values[j];
            //��� ��� - ����������

              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                with fmAddDoc2 do
                begin
                  teDocsSpec.Append;
                  teDocsSpecNum.AsInteger:=iMax;
                  teDocsSpeccard_id.AsInteger:=quCardsSelID.AsInteger;
                  teDocsSpeccard_name.AsString:=quCardsSelNAME.AsString+' '+quCardsSelNAMESHORT.AsString;
                  teDocsSpeccard_count.AsFloat:=1;
                  teDocsSpeccard_price.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                  teDocsSpeccard_sum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                  teDocsSpeccard_pricein.AsFloat:=quCardsSelLASTPRICEIN.AsFloat;
                  teDocsSpeccard_sumin.AsFloat:=quCardsSelLASTPRICEIN.AsFloat;
                  teDocsSpecpost_id.AsInteger:=prFindMainGroup(quCardsSelParent.AsInteger,S1);
                  teDocsSpecpost_name.AsString:=S1;
                  teDocsSpecComment.AsString:='';
                  teDocsSpecDProc.AsFloat:=0;
                  teDocsSpecDSum.AsFloat:=0;
                  teDocsSpecRSum.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                  teDocsSpec.Post;

                  inc(iMax);
                end;
              end;
            end;
          end;
        finally
          ViewDoc2.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
//  taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
//  taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if abs(taSpecOutSum1.AsFloat)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin

//  taSpecSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice2.AsFloat:=RoundEx(taSpecOutSum2.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.cxLabel3Click(Sender: TObject);
Var rPrice,rPrice1:Currency;
    rMessure,kSp,k:Real;
begin
  //����������� ����  � ����� ���������� �������
  //if PageControl1.ActivePageIndex=0 then
  begin
    if cxButtonEdit1.Tag=0 then
    begin
      showmessage('�������� ������� �����������.');
      exit;
    end else
    begin
      iCol:=0;
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        with dmO do
        begin
          prCalcLastPrice.ParamByName('IDGOOD').AsInteger:=taSpecOutIdGoods.AsInteger;
          prCalcLastPrice.ParamByName('IDCLI').AsFloat:=cxButtonEdit1.Tag;
          prCalcLastPrice.ExecProc;

          rPrice:=prCalcLastPrice.ParamByName('PRICEIN').AsFloat;
          rPrice1:=prCalcLastPrice.ParamByName('PRICEUCH').AsFloat;
          rMessure:=prCalcLastPrice.ParamByName('KOEF').AsFloat;

          quM.Active:=False;
          quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
          quM.Active:=True;
          kSp:=quMKOEF.AsFloat;
          quM.Active:=False;

          if kSp<>rMessure then
          begin
            k:=rMessure/kSp;
          end else k:=1;

          taSpecOut.Edit;
          taSpecOutPrice1.AsFloat:=rPrice*k;
          taSpecOutPrice2.AsFloat:=rPrice1*k;
          taSpecOutSum1.AsFloat:=rPrice*k*taSpecOutQuant.AsFloat;
          taSpecOutSum2.AsFloat:=rPrice1*k*taSpecOutQuant.AsFloat;
          taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
          if taSpecOutSum1.AsFloat>0.01 then
          begin
            taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
          end else taSpecOutProcNac.AsFloat:=0;
          taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;

          taSpecOut.Post;

          taSpecOut.Next;
        end;
      end;
      taSpecOut.First;
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc2.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  //if PageControl1.ActivePageIndex=0 then
  begin
    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      taSpecOut.Edit;
      taSpecOutPrice2.AsFloat:=taSpecOutPrice1.AsFloat;
      taSpecOutSum2.AsFloat:=taSpecOutSum1.AsFloat;
      taSpecOutSumNac.AsFloat:=0;
      taSpecOutProcNac.AsFloat:=0;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    taSpecOut.First;
    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.ViewDoc2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Quant' then iCol:=1;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price1' then iCol:=2;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum1' then iCol:=3;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price2' then iCol:=4;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum2' then iCol:=5;
//  Label16.Caption:=IntToStr(iCol); Delay(10);
end;

procedure TfmAddDoc2.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  //PageControl1.ActivePageIndex:=0;

  ViewDoc2card_name.Options.Editing:=False;
  ViewDoc2card_pricein.Visible:=False;
  ViewDoc2card_sumin.Visible:=False;
end;
(*
procedure TfmAddDoc2.cxLabel5Click(Sender: TObject);
var sMessure:String;
    iM:Integer;
    rPrice,rPrice1,k:Real;
begin
  //������ ������
  if PageControl1.ActivePageIndex=0 then
  begin
    fmPartIn:=tfmPartIn.Create(Application);
    if cxLookupComboBox1.EditValue>0 then
    begin
      if not taSpecOut.Eof then prSelPartInRet(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag);
      with dmO do
      begin
        quM.Active:=false;
        quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
        quM.Active:=True;
        if quMID_PARENT.AsInteger=0 then sMessure:=quMNAMESHORT.AsString
        else
        begin
          iM:=quMID_PARENT.AsInteger;
          quM.Active:=false;
          quM.ParamByName('IDM').AsInteger:=iM;
          quM.Active:=True;
          sMessure:=quMNAMESHORT.AsString;
        end;
        quM.Active:=false;
      end;
      fmPartIn.Label4.Caption:=taSpecOutNameG.AsString+' ('+sMessure+')';
      fmPartIn.Label5.Caption:=cxLookupComboBox1.Text;
      if cxButtonEdit1.Tag=0 then fmPartIn.Label6.Caption:='�� ���������'
                           else fmPartIn.Label6.Caption:=cxButtonEdit1.Text;
      fmPartIn.ShowModal;
      if fmPartIn.ModalResult=mrOk then
      begin //������������ ����
        rPrice:=dmO.quSelPartInPRICEIN.AsFloat; // � ��������
        rPrice1:=dmO.quSelPartInPRICEOUT.AsFloat;
        k:=prFindKM(taSpecOutIM.AsInteger);

        taSpecOut.Edit;
        taSpecOutPrice1.AsFloat:=rPrice*k;
        taSpecOutPrice2.AsFloat:=rPrice1*k;
        taSpecOutSum1.AsFloat:=rPrice*k*taSpecOutQuant.AsFloat;
        taSpecOutSum2.AsFloat:=rPrice1*k*taSpecOutQuant.AsFloat;
        taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
        if taSpecOutSum1.AsFloat>0.01 then
        begin
          taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
        end else taSpecOutProcNac.AsFloat:=0;
        taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
        taSpecOut.Post;
      end;
    end else showmessage('�������� ����� ��������.');
    fmPartIn.Release;
  end;
end;
*)
(*
procedure TfmAddDoc2.cxLabel6Click(Sender: TObject);
Var iYes:Integer;
    rQP,rQs,PriceSp,k:Real;
begin
//�������� ������
  if PageControl1.ActivePageIndex=0 then
  begin
    fmPartIn1:=tfmPartIn1.Create(Application);
    if cxLookupComboBox1.EditValue>0 then
    begin
      with dmO do
      with dmORep do
      begin
        iCol:=0;
        CloseTa(taPartTest);
        taSpecOut.First;
        while not taSpecOut.Eof do
        begin
          //��� ��� ������
          rQP:=0;
          k:=prFindKM(taSpecOutIM.AsInteger);
          rQs:=taSpecOutQuant.AsFloat*k; //20*0,5=10
          PriceSp:=taSpecOutPrice1.AsFloat/k;

          prSelPartIn(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag,0);
          with dmO do
          begin
            quSelPartIn.First;
            while not quSelPartIn.Eof do
            begin
            //���� �� ���� ������� ���� �����, ��������� �������� ���
              if RoundVal(quSelPartInPRICEIN.AsFloat)=RoundVal(PriceSp) then rQp:=rQp+quSelPartInQREMN.AsFloat;
              quSelPartIn.Next;
            end;
            quSelPartIn.Active:=False;
          end;

          if rQp>=rQs then iYes:=1 else iYes:=0;

          taPartTest.Append;
          taPartTestNum.AsInteger:=taSpecOutNum.AsInteger;
          taPartTestIdGoods.AsInteger:=taSpecOutIdGoods.AsInteger;
          taPartTestNameG.AsString:=taSpecOutNameG.AsString;
          taPartTestIM.AsInteger:=taSpecOutIM.AsInteger;
          taPartTestSM.AsString:=taSpecOutSM.AsString;
          taPartTestQuant.AsFloat:=taSpecOutQuant.AsFloat;
          taPartTestPrice1.AsFloat:=taSpecOutPrice1.AsFloat;
          taPartTestiRes.AsInteger:=iYes;
          taPartTest.Post;

          taSpecOut.Next;
        end;

        fmPartIn1.Label5.Caption:=cxLookupComboBox1.Text;
        if cxButtonEdit1.Tag=0 then fmPartIn1.Label6.Caption:='�� ���������'
                               else fmPartIn1.Label6.Caption:=cxButtonEdit1.Text;
        fmPartIn1.ShowModal;

        taPartTest.Active:=False;
      end;
    end else showmessage('�������� ����� ��������.');
    fmPartIn1.Release;
  end;
end;
*)
procedure TfmAddDoc2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  taSpecOut.Active:=False;
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewDoc2.StoreToIniFile(CurDir+GridIni,False);
      taSpecOut.Active:=False;
      Action := caHide;
    end
    else  Action := caNone;
  end;
end;

procedure TfmAddDoc2.cxButton1Click(Sender: TObject);
Var rSumIn,rSumOut:Real;
    iDv:INteger;
begin
  //��������
  //ShowMessage(IntToStr(IdH));
  with dmO do
  begin
    if fmAddDoc2.Tag=0 then //��������
    begin
      qDocEdit.Close;
      qDocEdit.SQL.Clear;
      qDocEdit.SQL.Add('select max(ID) as ID from of_doch');
      qDocEdit.Transaction.StartTransaction;
      qDocEdit.ExecQuery;
      IdH:=qDocEdit.FldByName['ID'].AsInteger+1;
      qDocEdit.Transaction.Commit;

      try
        ViewDoc2.BeginUpdate;
        ViewVer.BeginUpdate;

        rSumIn:=0;
        rSumOut:=0;

        teDocsSpec.First;
        while not teDocsSpec.Eof do
        begin
          rSumIn:=rSumIn+rv(teDocsSpeccard_sumin.AsFloat);
          rSumOut:=rSumOut+rv(teDocsSpeccard_sum.AsFloat);
          teDocsSpec.Next;
        end;

        quDocsId.Active:=False;
        quDocsID.ParamByName('IDH').AsInteger:=IdH;
        quDocsId.Active:=True;

        quDocsId.Append;
        quDocsIdID.AsInteger:=IDH;
        quDocsIdDOCNUM.AsString:=cxTextEdit3.Text;
        quDocsIdIBUYER.AsInteger:=cxButtonEdit1.Tag;
        quDocsIdDOCDATE.AsDateTime:=cxDateEdit1.Date;
        quDocsIdSUMIN.AsFloat:=rSumIn;
        quDocsIdSUMOUT.AsFloat:=rSumOut;
        quDocsIdIDPERS.AsInteger:=Person.Id;
        quDocsId.Post;

        delay(10);

        if cxTextEdit3.Text=prGetNum(1,0) then prGetNum(1,1); //��������� �����
        fmAddDoc2.Tag:=IDH;

        quDV.Active:=False;
        quDV.ParamByName('IDH').AsInteger:=IdH;
        quDV.Active:=True;

        quDV.Append;
        quDVIDH.AsInteger:=IDH;
        quDVIDV.AsInteger:=1;
        quDVPREDOPLATA.AsFloat:=rv(cxCurrencyEdit1.Value);
        quDVIZGOTOVLENIE.AsInteger:=cxSpinEdit2.Value;
        quDVWARRANTY.AsString:=cxTextEdit1.Text;
        quDVVPICT.AsVariant:=cxBlobEdit1.EditValue;
        quDVVDATE.AsDateTime:=Now;
        quDV.Post;

        delay(10);

        quDS.Active:=False;
        quDS.ParamByName('IDH').AsInteger:=IdH;
        quDS.ParamByName('IDV').AsInteger:=1;
        quDS.Active:=True;

        teDocsSpec.First;
        while not teDocsSpec.Eof do
        begin
          quDS.Append;
          quDSIDH.AsInteger:=IDH;
          quDSIDV.AsInteger:=1;
          quDSNUM.AsInteger:=teDocsSpecNum.AsInteger;
          quDSCODE.AsInteger:=teDocsSpeccard_id.AsInteger;
          quDSQUANT.AsFloat:=teDocsSpeccard_count.AsFloat;
          quDSPRICEIN.AsFloat:=teDocsSpeccard_pricein.AsFloat;
          quDSPRICEOUT.AsFloat:=teDocsSpeccard_price.AsFloat;
          quDSSUMIN.AsFloat:=teDocsSpeccard_sumin.AsFloat;
          quDSSUMOUT.AsFloat:=teDocsSpeccard_sum.AsFloat;
          quDSIPOST.AsInteger:=teDocsSpecpost_id.AsInteger;
          quDSComment.AsString:=teDocsSpecComment.AsString;
          quDSDPROC.AsFloat:=teDocsSpecDProc.AsFloat;
          quDSDSUM.AsFloat:=teDocsSpecDSum.AsFloat;
          quDS.Post;

          teDocsSpec.Next; delay(10);
        end;
      finally
        ViewDoc2.EndUpdate;
        ViewVer.EndUpdate;
        prRowFocus(ViewVer);
      end;
    end
    else
    begin
      try
        ViewDoc2.BeginUpdate;
        ViewVer.BeginUpdate;

        rSumIn:=0;
        rSumOut:=0;

        teDocsSpec.First;
        while not teDocsSpec.Eof do
        begin
          rSumIn:=rSumIn+rv(teDocsSpeccard_sumin.AsFloat);
          rSumOut:=rSumOut+rv(teDocsSpeccard_sum.AsFloat);
          teDocsSpec.Next;
        end;

        IDH:=fmAddDoc2.Tag;

        quDocsId.Active:=False;
        quDocsID.ParamByName('IDH').AsInteger:=IdH;
        quDocsId.Active:=True;

        if quDocsId.RecordCount=1 then
        begin
          quDocsId.First;

          quDocsId.Edit;
          quDocsIdDOCNUM.AsString:=cxTextEdit3.Text;
          quDocsIdIBUYER.AsInteger:=cxButtonEdit1.Tag;
          quDocsIdDOCDATE.AsDateTime:=cxDateEdit1.Date;
          quDocsIdSUMIN.AsFloat:=rSumIn;
          quDocsIdSUMOUT.AsFloat:=rSumOut;
          quDocsIdIDPERS.AsInteger:=Person.Id;
          quDocsId.Post;

          quDV.Active:=False;
          quDV.ParamByName('IDH').AsInteger:=IdH;
          quDV.Active:=True;

          iDV:=1;
          quDV.First;
          while not quDV.Eof do
          begin
            if (quDVIDV.AsInteger+1)>iDv then iDv:=quDVIDV.AsInteger+1;

            quDV.Next;
          end;

          quDV.Append;
          quDVIDH.AsInteger:=IDH;
          quDVIDV.AsInteger:=iDv;
          quDVPREDOPLATA.AsFloat:=rv(cxCurrencyEdit1.Value);
          quDVIZGOTOVLENIE.AsInteger:=cxSpinEdit2.Value;
          quDVWARRANTY.AsString:=cxTextEdit1.Text;
          quDVVPICT.AsVariant:=cxBlobEdit1.EditValue;
          quDVVDATE.AsDateTime:=Now;
          quDV.Post;

          delay(10);

          quDS.Active:=False;
          quDS.ParamByName('IDH').AsInteger:=IdH;
          quDS.ParamByName('IDV').AsInteger:=iDv;
          quDS.Active:=True;

          teDocsSpec.First;
          while not teDocsSpec.Eof do
          begin
            quDS.Append;
            quDSIDH.AsInteger:=IDH;
            quDSIDV.AsInteger:=iDv;
            quDSNUM.AsInteger:=teDocsSpecNum.AsInteger;
            quDSCODE.AsInteger:=teDocsSpeccard_id.AsInteger;
            quDSQUANT.AsFloat:=teDocsSpeccard_count.AsFloat;
            quDSPRICEIN.AsFloat:=teDocsSpeccard_pricein.AsFloat;
            quDSPRICEOUT.AsFloat:=teDocsSpeccard_price.AsFloat;
            quDSSUMIN.AsFloat:=teDocsSpeccard_sumin.AsFloat;
            quDSSUMOUT.AsFloat:=teDocsSpeccard_sum.AsFloat;
            quDSIPOST.AsInteger:=teDocsSpecpost_id.AsInteger;
            quDSComment.AsString:=teDocsSpecComment.AsString;
            quDSDPROC.AsFloat:=teDocsSpecDProc.AsFloat;
            quDSDSUM.AsFloat:=teDocsSpecDSum.AsFloat;
            quDS.Post;
            delay(10);

            teDocsSpec.Next;
          end;
        end;
      finally
        ViewDoc2.EndUpdate;
        ViewVer.EndUpdate;
        prRowFocus(ViewVer);
      end;
      quDH.Refresh;
    end;
  end;
  ShowMessage('���������� ���������');
end;

procedure TfmAddDoc2.acSaveExecute(Sender: TObject);
begin
  if cxButton1.Enabled then cxButton1.Click;
end;

procedure TfmAddDoc2.acExitExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc2.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmAddDoc2.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  ViewDoc2.BeginUpdate;
  try
    iMax:=1;
    teDocsSpec.First;
    while not teDocsSpec.Eof do
    begin
      if iMax<teDocsSpecNum.AsInteger+1 then iMax:=teDocsSpecNum.AsInteger+1;
      teDocsSpec.Next;
    end;

    teDocsSpec.Append;
    teDocsSpecNum.AsInteger:=iMax;
    teDocsSpeccard_id.AsInteger:=0;
    teDocsSpeccard_count.AsFloat:=0;
    teDocsSpeccard_price.AsFloat:=0;
    teDocsSpeccard_sum.AsFloat:=0;
    teDocsSpeccard_pricein.AsFloat:=0;
    teDocsSpeccard_sumin.AsFloat:=0;
    teDocsSpeccard_name.AsString:='';
    teDocsSpecpost_id.Asinteger:=0;
    teDocsSpecpost_name.AsString:='';
    teDocsSpecComment.AsString:='';
    teDocsSpecDProc.AsFloat:=0;
    teDocsSpecDSum.AsFloat:=0;
    teDocsSpecRSum.AsFloat:=0;
    teDocsSpec.Post;
  finally
    ViewDoc2.EndUpdate;
  end;
  GridDoc2.SetFocus;

  ViewDoc2card_name.Options.Editing:=True;
  ViewDoc2card_name.Focused:=True;
end;

procedure TfmAddDoc2.acAddListExecute(Sender: TObject);
begin
  bAddSpecRet:=True;
  fmCards.Show;
end;

procedure TfmAddDoc2.cxLabel7Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc2.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  //if PageControl1.ActivePageIndex=0 then
  begin
    if teDocsSpec.RecordCount>0 then
    begin
      teDocsSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc2.ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    S1:String;

begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2card_id' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,LASTPRICEIN,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc2.BeginUpdate;

          teDocsSpec.Edit;
          teDocsSpeccard_id.AsInteger:=iCode;
          teDocsSpeccard_count.AsFloat:=1;
          teDocsSpeccard_price.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpeccard_sum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpeccard_pricein.AsFloat:=quFCardLASTPRICEIN.AsFloat;
          teDocsSpeccard_sumin.AsFloat:=quFCardLASTPRICEIN.AsFloat;
          teDocsSpeccard_name.AsString:=quFCardNAME.AsString+' '+prFindSM1(quFCardIMESSURE.AsInteger);
          teDocsSpecpost_id.AsInteger:=prFindMainGroup(quFCardPARENT.AsInteger,S1);
          teDocsSpecpost_name.AsString:=S1;
          teDocsSpecComment.AsString:='';
          teDocsSpecDProc.AsFloat:=0;
          teDocsSpecDSum.AsFloat:=0;
          teDocsSpecRSum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpec.Post;

          ViewDoc2.EndUpdate;
        end;
      end;
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2card_name' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,LASTPRICEIN,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc2.BeginUpdate;
            teDocsSpec.Edit;
            teDocsSpeccard_id.AsInteger:=quFCardID.AsInteger;
            teDocsSpeccard_count.AsFloat:=1;
            teDocsSpeccard_price.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            teDocsSpeccard_sum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            teDocsSpeccard_pricein.AsFloat:=quFCardLASTPRICEIN.AsFloat;
            teDocsSpeccard_sumin.AsFloat:=quFCardLASTPRICEIN.AsFloat;
            teDocsSpeccard_name.AsString:=quFCardNAME.AsString+' '+prFindSM1(quFCardIMESSURE.AsInteger);
            teDocsSpecpost_id.AsInteger:=prFindMainGroup(quFCardPARENT.AsInteger,S1);
            teDocsSpecpost_name.AsString:=S1;
            teDocsSpecDProc.AsFloat:=0;
            teDocsSpecDSum.AsFloat:=0;
            teDocsSpecRSum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            teDocsSpec.Post;

            ViewDoc2.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;

      ViewDoc2card_count.Focused:=True;
    end else
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2card_id' then
        if fTestKey(Key)=False then
          if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Cancel;
  end;
end;


procedure TfmAddDoc2.ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var sName,S1:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2card_name') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,LASTPRICEIN,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc2.BeginUpdate;

          teDocsSpec.Edit;
          teDocsSpeccard_id.AsInteger:=quFCardID.AsInteger;
          teDocsSpeccard_count.AsFloat:=1;
          teDocsSpeccard_price.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpeccard_sum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpeccard_pricein.AsFloat:=quFCardLASTPRICEIN.AsFloat;
          teDocsSpeccard_sumin.AsFloat:=quFCardLASTPRICEIN.AsFloat;
          teDocsSpeccard_name.AsString:=quFCardNAME.AsString+' '+prFindSM1(quFCardIMESSURE.AsInteger);
          teDocsSpecpost_id.AsInteger:=prFindMainGroup(quFCardPARENT.AsInteger,S1);
          teDocsSpecpost_name.AsString:=S1;
          teDocsSpecDProc.AsFloat:=0;
          teDocsSpecDSum.AsFloat:=0;
          teDocsSpecRSum.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          teDocsSpec.Post;

          ViewDoc2.EndUpdate;
          AEdit.SelectAll;
          ViewDoc2card_name.Options.Editing:=False;
          ViewDoc2card_name.Focused:=True;
          Key:=#0;
        end;
      end;
    end;

  end;//}
end;

procedure TfmAddDoc2.cxLabel8Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddDoc2.acDelAllExecute(Sender: TObject);
begin
//  if PageControl1.ActivePageIndex=0 then
  begin
    if teDocsSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        teDocsSpec.First; while not teDocsSpec.Eof do teDocsSpec.Delete;
      end;
    end;
  end;  
end;

procedure TfmAddDoc2.ViewDoc2SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecOutIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpecOut.Edit;
        taSpecOutIM.AsInteger:=iM;
        taSpecOutKm.AsFloat:=rK;
        taSpecOutSM.AsString:=Sm;
        taSpecOut.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
(*  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  if cxButtonEdit2.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit2.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit2.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit2.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit2.Tag:=0;
    cxButtonEdit2.Text:='';
  end;

  *)
end;

procedure TfmAddDoc2.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc2.cxButtonEdit2KeyPress(Sender: TObject; var Key: Char);
begin
(*  sName:=cxButtonEdit2.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit2.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit2.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
  *)
end;

procedure TfmAddDoc2.MenuItem1Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc2);
end;

procedure TfmAddDoc2.teDocsSpecBeforePost(DataSet: TDataSet);
begin
  teDocsSpecDSum.AsFloat:=rv(teDocsSpeccard_count.AsFloat*teDocsSpeccard_price.AsFloat/100*teDocsSpecDProc.AsFloat);
  teDocsSpeccard_sum.AsFloat:=teDocsSpeccard_count.AsFloat*teDocsSpeccard_price.AsFloat-teDocsSpecDSum.AsFloat;
  teDocsSpeccard_sumin.AsFloat:=teDocsSpeccard_count.AsFloat*teDocsSpeccard_pricein.AsFloat;
  teDocsSpecRSum.AsFloat:=teDocsSpeccard_sum.AsFloat+teDocsSpecDSum.AsFloat;
end;

procedure TfmAddDoc2.cxTextEdit7KeyPress(Sender: TObject; var Key: Char);
begin
if Key=',' then Key:='.';
if key in ['0'..'9','.',#8,#13] then Key:=Key else Key:=#0;
end;

procedure TfmAddDoc2.DocOpen(idh:integer);
Var rDSum:Real;
begin
  with dmO do
  begin
    ViewVer.BeginUpdate;
    ViewDoc2.BeginUpdate;
    rDSum:=0;

    try
      quDV.Active:=False;
      quDV.parambyname('IDH').AsInteger:=Idh;
      quDV.Active:=True;

      quDV.Last;

      CloseTe(teDocsSpec);
      if quDV.RecordCount>0 then
      begin
        fmAddDoc2.cxCurrencyEdit1.Value:=quDVPREDOPLATA.AsFloat;
        fmAddDoc2.cxTextEdit1.Text:=quDVWARRANTY.AsString;
        fmAddDoc2.cxSpinEdit2.Value:=quDVIZGOTOVLENIE.AsInteger;
        fmAddDoc2.cxBlobEdit1.EditValue:=quDVVPICT.AsVariant;

        quDS.Active:=False;
        quDS.ParamByName('IDH').AsInteger:=Idh;
        quDS.ParamByName('IDV').AsInteger:=quDVIDV.AsInteger;
        quDS.Active:=True;

        quDS.First;
        while not quDS.Eof do
        begin
          teDocsSpec.Append;
          teDocsSpecNum.AsInteger:=quDSNUM.AsInteger;
          teDocsSpeccard_id.AsInteger:=quDSCODE.AsInteger;
          teDocsSpeccard_count.AsFloat:=quDSQUANT.AsFloat;
          teDocsSpeccard_price.AsFloat:=quDSPRICEOUT.AsFloat;
          teDocsSpeccard_sum.AsFloat:=quDSSUMOUT.AsFloat;
          teDocsSpeccard_name.asstring:=quDSNAME.AsString+' '+quDSNAMESHORT.AsString;
          teDocsSpecpost_name.AsString:=quDSNAMECL.AsString;
          teDocsSpecpost_id.AsInteger:=quDSIPOST.AsInteger;
          teDocsSpecComment.AsString:=quDSComment.AsString;
          teDocsSpeccard_pricein.AsFloat:=quDSPRICEIN.AsFloat;
          teDocsSpeccard_sumin.AsFloat:=quDSSUMIN.AsFloat;
          teDocsSpecDProc.AsFloat:=quDSDPROC.AsFloat;
          teDocsSpecDSum.AsFloat:=quDSDSUM.AsFloat;
          teDocsSpecRSum.AsFloat:=quDSSUMOUT.AsFloat+quDSDSUM.AsFloat;
          teDocsSpec.Post;

//          teDocsSpecpost_id.AsInteger:=prFindMainGroup(quCardsSelParent.AsInteger,S1);
//          teDocsSpecpost_name.AsString:=S1;

          rDSum:=rDSum+quDSDSUM.AsFloat;

          quDS.Next;
        end;
        quDS.Active:=False;
      end;

    finally
      ViewVer.EndUpdate;
      ViewDoc2.EndUpdate;

      if abs(rDSum)>0.001 then
      begin
        ViewDoc2DProc.Visible:=True;
        ViewDoc2DSum.Visible:=True;
        ViewDoc2RSum.Visible:=True;
      end else
      begin
        ViewDoc2DProc.Visible:=False;
        ViewDoc2DSum.Visible:=False;
        ViewDoc2RSum.Visible:=False;
      end;

    end;
    delay(10);
  end;
end;

procedure TfmAddDoc2.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
//var
  //i:integer;
begin
  //i:=strtoint(item.caption);
  //ShowMessage(item.caption);
  //DocOpen(ListView1.Tag,i);
end;

procedure TfmAddDoc2.ExcelZakup();
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
//    StrWk:String;
//    iCode:Integer;
//    iC,iM:Integer;
//    rPr1,rPr2:Real;
    i:integer;
begin
  begin
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');

    ExcelApp.Application.EnableEvents := false;
  //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

        iRow:=teDocsSpec.RecordCount;
        ArrayData := VarArrayCreate([1,iRow+15, 1, 3], varVariant);

        ArrayData[1,1]:='����������';
        ArrayData[2,1]:='�����';
        ArrayData[3,1]:='�������';
        ArrayData[4,1]:='��������';

        ArrayData[1,2]:='��� "��������"';
        ArrayData[2,2]:='������������, �������� 39';
        ArrayData[3,2]:='8(343)340000, 8902878987';
        ArrayData[4,2]:='������ ����';

        ArrayData[6,1]:='���������';
        ArrayData[7,1]:='�����';
        ArrayData[8,1]:='�������';
        ArrayData[9,1]:='��������';
        ArrayData[10,1]:='����';

        ArrayData[6,2]:='��� "��������"';
        ArrayData[7,2]:='������������, �.��������� 24';
        ArrayData[8,2]:='8(343)3455555, 89028767876';
        ArrayData[9,2]:='������� �����';
        ArrayData[10,2]:='18.03.2011';

        ArrayData[12,1]:='������ �� �������������';

        ArrayData[14,1]:='������������';
        ArrayData[14,2]:='�������';
        ArrayData[14,3]:='���-��';

        teDocsSpec.First;i:=0;
          while not teDocsSpec.Eof do
            begin
              ArrayData[15+i,1]:=teDocsSpeccard_name.AsString;
              ArrayData[15+i,2]:=teDocsSpeccard_id.AsString;
              ArrayData[15+i,3]:=teDocsSpeccard_count.AsString;
              teDocsSpec.Next;inc(i);
            end;


        Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
        Cell2 := WorkBook.WorkSheets[1].Cells[iRow+15,3];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        Range.Value := ArrayData;

        WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=35;
        WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=30;
        WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=15;

        WorkBook.WorkSheets[1].Cells[14,1].Font.Bold := True;
        WorkBook.WorkSheets[1].Cells[14,2].Font.Bold := True;
        WorkBook.WorkSheets[1].Cells[14,3].Font.Bold := True;

        Cell1 := WorkBook.WorkSheets[1].Cells[14,1];
        Cell2 := WorkBook.WorkSheets[1].Cells[14,3];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        Range.HorizontalAlignment:=3;
        Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;

        Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
        Cell2 := WorkBook.WorkSheets[1].Cells[4,2];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;

        Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
        Cell2 := WorkBook.WorkSheets[1].Cells[10,2];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;


        Cell1 := WorkBook.WorkSheets[1].Cells[14,1];
        Cell2 := WorkBook.WorkSheets[1].Cells[14+i,1];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;


        Cell1 := WorkBook.WorkSheets[1].Cells[14,2];
        Cell2 := WorkBook.WorkSheets[1].Cells[14+i,2];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        Range.HorizontalAlignment:=3;
        Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;


        Cell1 := WorkBook.WorkSheets[1].Cells[14,3];
        Cell2 := WorkBook.WorkSheets[1].Cells[14+i,3];
        Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
        Range.Borders.LineStyle:=1;



        ExcelApp.Visible := true;










    //with dmC do
    //begin
{

      iRow:=quSelClientsSpec.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+8, 1, 19], varVariant);


      ArrayData[1,2]:='���������� �1';
      if (FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime))<>'30.12.1899'
        then ArrayData[2,2]:='������������ � �������� � '+taClientsDogNum.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taClientsDogDate.AsDateTime)+' �.'
        else ArrayData[2,2]:='������������ � �������� � ___________ �� ___________ 20___ �.';
      ArrayData[3,2]:='����� '+taClientsName.AsString+' � �� "������" '+quSelClientsSpec2.FieldByName('Departs').AsString;
      ArrayData[4,1]:='���� ���������� ��������� � ���� ______________';

      quSelClientsSpec2.Active:=False;

      ArrayData[6,1]:='��� ������ (�� ��)';
      ArrayData[6,2]:='��������';
      ArrayData[6,3]:='������������';      ///!!!!!!
      ArrayData[6,4]:='��. ���.';
      ArrayData[6,5]:='�����';
      ArrayData[6,6]:='������ ������������� (�����)';
      ArrayData[6,7]:='��� ������� �������� ��-�� ������';
      ArrayData[6,8]:='���� ������ � ���';
      ArrayData[6,9]:='���� �����';
      ArrayData[6,10]:='% ���������';
      ArrayData[6,11]:='������ ���,%';
      ArrayData[6,12]:='���-�� ������ � ����� ������ �����, ��.';
      ArrayData[6,13]:='������� ������ �� ����� ��������';
      ArrayData[6,14]:='���� ������������� ��';



      ArrayData[iRow+8,2]:='���������';
      ArrayData[iRow+8,11]:='����������';

      fmClients.PBar1.Position:=0;
      fmClients.PBar1.Visible:=True; delay(10);
      iM:=Trunc(iRow/20);
      if iM=0 then iM:=1;
      iC:=0;

      quSelClientsSpec.First;
      while not quSelClientsSpec.Eof do
      begin
        iCode:=quSelClientsSpecArticul.AsInteger;

        ArrayData[iC+7,1] := quSelClientsSpecArticul.AsInteger;
        //ArrayData[iC+7,2] :='"'+ quSelClientsSpecBarCode.AsString+'"';
        ArrayData[iC+7,2] := quSelClientsSpecBarCode.AsString;
        ArrayData[iC+7,3] := quSelClientsSpecName.AsString;
        ArrayData[iC+7,4] := quSelClientsSpecEdIzm.AsString;
        ArrayData[iC+7,5] := quSelClientsSpecNameBrand.AsString;
        ArrayData[iC+7,6] := quSelClientsSpecNameCountry.AsString;
        ArrayData[iC+7,8] := quSelClientsSpecPrice.AsString;
        ArrayData[iC+7,11] := quSelClientsSpecNDS.AsInteger;

        quSelClientsSpec.Next; inc(iC);
        if iC mod iM = 0 then
        begin
          fmClients.PBar1.Position:=Trunc((iC/(iM*20))*100);
          Delay(10);
        end;
      end;
      quSelClientsSpec.Active:=False;
      fmClients.PBar1.Position:=100;
    //delay(300);
      fmClients.PBar1.Visible:=False;
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    Cell1 := WorkBook.WorkSheets[1].Cells[7,10];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,10];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Formula:='=IF(I7>0,(I7-H7)*100/H7,"")';
    //Range.Se := '0.00%';

    Cell1 := WorkBook.WorkSheets[1].Cells[7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    Cell1 := WorkBook.WorkSheets[1].Cells[iRow+7,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+8,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

    WorkBook.WorkSheets[1].Cells[1,3].Font.Size := 8;

    WorkBook.WorkSheets[1].Cells[2,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[3,3].Font.Size := 12;
    WorkBook.WorkSheets[1].Cells[3,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Cells[4,2].Font.Size := 8;
    WorkBook.WorkSheets[1].Cells[5,2].Font.Size := 8;


    //WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=8;
    WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=10;
    WorkBook.WorkSheets[1].Columns[2].NumberFormat :='#';
    //WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=30;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=30;

    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[4].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[5].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[6].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[7].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[8].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[9].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0,00%';
    WorkBook.WorkSheets[1].Columns[10].NumberFormat := '0.00';
    WorkBook.WorkSheets[1].Columns[11].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[15].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[16].ColumnWidth:=6;
    //WorkBook.WorkSheets[1].Columns[17].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[12].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[13].ColumnWidth:=6;
    WorkBook.WorkSheets[1].Columns[14].ColumnWidth:=7;
    WorkBook.WorkSheets[1].Columns[14].NumberFormat := '��.��.����';


    WorkBook.WorkSheets[1].Rows[6].RowHeight:=50;

    Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[6,14];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.WrapText:=True;
    Range.Font.Size := 7;
    Range.HorizontalAlignment:=3;
    Range.VerticalAlignment:=1;
    Range.Borders.LineStyle:=1;

    ExcelApp.Visible := true;}
    ;
  end;

end;

procedure TfmAddDoc2.cxButton3Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    iPost:Integer;
    i:integer;
    s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12:String;

begin
  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');

  ExcelApp.Application.EnableEvents := false;

  ViewDoc2.BeginUpdate;
  try
    CloseTe(teSp);
    teDocsSpec.First;
    while not teDocsSpec.Eof do
    begin
      teSp.Append;
      teSpCode.AsInteger:=teDocsSpeccard_id.AsInteger;
      teSpQuant.asFloat:=teDocsSpeccard_count.AsFloat;
      if length(teDocsSpecComment.AsString)>0 then
        teSpName.AsString:=teDocsSpeccard_name.AsString+' /'+teDocsSpecComment.AsString+'/'
      else
        teSpName.AsString:=teDocsSpeccard_name.AsString;
      teSpIPost.AsInteger:=teDocsSpecpost_id.AsInteger;
      teSp.Post;

      teDocsSpec.Next;
    end;

    //�������� ������������ ������ ����� �� �����������
    while teSp.RecordCount>0 do
    begin
      CloseTe(tePr);

      teSp.First;
      iPost:=teSpIPost.AsInteger;
      while not teSp.Eof do
      begin
        if teSpIPost.AsInteger=iPost then
        begin
          tePr.Append;
          tePrCode.AsInteger:=teSpCode.AsInteger;
          tePrQuant.AsFloat:=teSpQuant.AsFloat;
          tePrName.AsString:=teSpName.AsString;
          tePr.Post;

          teSp.Delete;
        end else teSp.Next;
      end;

      //���� ��������
      //� ������� �����
      Workbook := ExcelApp.WorkBooks.Add;


      iRow:=tePr.RecordCount;
      ArrayData := VarArrayCreate([1,iRow+15, 1, 3], varVariant);

      ArrayData[1,1]:='����������';
      ArrayData[2,1]:='�����';
      ArrayData[3,1]:='�������';
      ArrayData[4,1]:='��������';

      prFindCl(CommonSet.MainFrom,S1,S2,S3,S4,s5,s6,s7,s8,s9,s10,s11,s12);

      ArrayData[1,2]:=s1;
      ArrayData[2,2]:=s2;
      ArrayData[3,2]:=s11;
      ArrayData[4,2]:=Person.Name;

      ArrayData[6,1]:='���������';
      ArrayData[7,1]:='�����';
      ArrayData[8,1]:='�������';
      ArrayData[9,1]:='��������';
      ArrayData[10,1]:='����';

      prFindCl(iPost,S1,S2,S3,S4,s5,s6,s7,s8,s9,s10,s11,s12);

      ArrayData[6,2]:=s1;
      ArrayData[7,2]:=s2;
      ArrayData[8,2]:=s11;
      ArrayData[9,2]:=s12;
      ArrayData[10,2]:=cxDateEdit1.Text;

      ArrayData[12,1]:='������ �� �������������';

      ArrayData[14,1]:='������������';
      ArrayData[14,2]:='�������';
      ArrayData[14,3]:='���-��';

      tePr.First;i:=0;
      while not tePr.Eof do
      begin
        ArrayData[15+i,1]:=tePrName.AsString;
        ArrayData[15+i,2]:=tePrCode.AsString;
        ArrayData[15+i,3]:=tePrQuant.AsString;
        tePr.Next;inc(i);
      end;


      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[iRow+15,3];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      WorkBook.WorkSheets[1].Columns[1].ColumnWidth:=35;
      WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=30;
      WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=15;

      WorkBook.WorkSheets[1].Cells[14,1].Font.Bold := True;
      WorkBook.WorkSheets[1].Cells[14,2].Font.Bold := True;
      WorkBook.WorkSheets[1].Cells[14,3].Font.Bold := True;

      Cell1 := WorkBook.WorkSheets[1].Cells[14,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[14,3];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
      Range.HorizontalAlignment:=3;
      Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;

      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[4,2];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;

      Cell1 := WorkBook.WorkSheets[1].Cells[6,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[10,2];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;


      Cell1 := WorkBook.WorkSheets[1].Cells[14,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[14+i,1];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;


      Cell1 := WorkBook.WorkSheets[1].Cells[14,2];
      Cell2 := WorkBook.WorkSheets[1].Cells[14+i,2];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
      Range.HorizontalAlignment:=3;
      Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;


      Cell1 := WorkBook.WorkSheets[1].Cells[14,3];
      Cell2 := WorkBook.WorkSheets[1].Cells[14+i,3];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
        //Range.WrapText:=True;
        //Range.Font.Size := 7;
        //Range.HorizontalAlignment:=3;
        //Range.VerticalAlignment:=1;
      Range.Borders.LineStyle:=1;
    end;

    ExcelApp.Visible := true;

  finally
    ViewDoc2.EndUpdate;
    teSp.Active:=False;
    tePr.Active:=False;
  end;

//  ExcelZakup();
end;

procedure TfmAddDoc2.N3Click(Sender: TObject);
begin
  with dmO do
  begin
    if cxButton1.Enabled=False then exit;

    ViewVer.BeginUpdate;
    ViewDoc2.BeginUpdate;
    try
      if quDV.RecordCount>0 then
      begin
        if MessageDlg('������������� ������ ������� ������ '+its(quDVIDV.AsInteger)+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDV.Delete;
          CloseTe(teDocsSpec);
          fmAddDoc2.cxCurrencyEdit1.Value:=0;
          fmAddDoc2.cxTextEdit1.Text:='';
          fmAddDoc2.cxSpinEdit2.Value:=0;
          fmAddDoc2.cxBlobEdit1.Clear;

          delay(10);
          if quDV.RecordCount>0 then
          begin
            fmAddDoc2.cxCurrencyEdit1.Value:=quDVPREDOPLATA.AsFloat;
            fmAddDoc2.cxTextEdit1.Text:=quDVWARRANTY.AsString;
            fmAddDoc2.cxSpinEdit2.Value:=quDVIZGOTOVLENIE.AsInteger;
            fmAddDoc2.cxBlobEdit1.EditValue:=quDVVPICT.AsVariant;

            quDS.Active:=False;
            quDS.ParamByName('IDH').AsInteger:=Idh;
            quDS.ParamByName('IDV').AsInteger:=quDVIDV.AsInteger;
            quDS.Active:=True;

            quDS.First;
            while not quDS.Eof do
            begin
              teDocsSpec.Append;
              teDocsSpecNum.AsInteger:=quDSNUM.AsInteger;
              teDocsSpeccard_id.AsInteger:=quDSCODE.AsInteger;
              teDocsSpeccard_count.AsFloat:=quDSQUANT.AsFloat;
              teDocsSpeccard_price.AsFloat:=quDSPRICEOUT.AsFloat;
              teDocsSpeccard_sum.AsFloat:=quDSSUMOUT.AsFloat;
              teDocsSpeccard_name.asstring:=quDSNAME.AsString+' '+quDSNAMESHORT.AsString;
              teDocsSpecpost_name.AsString:=quDSNAMECL.AsString;
              teDocsSpecpost_id.AsInteger:=quDSIPOST.AsInteger;
              teDocsSpecComment.AsString:=quDSComment.AsString;
              teDocsSpecDProc.AsFloat:=quDSDPROC.AsFloat;
              teDocsSpecDSum.AsFloat:=quDSDSUM.AsFloat;
              teDocsSpecRSum.AsFloat:=quDSSUMOUT.AsFloat+quDSDSUM.AsFloat;
              teDocsSpec.Post;

              quDS.Next;
            end;
            quDS.Active:=False;
          end;
        end;
      end;
    finally
      ViewVer.EndUpdate;
      ViewDoc2.EndUpdate;
    end;
    delay(10);
  end;
end;

procedure TfmAddDoc2.cxButton4Click(Sender: TObject);
Var S1,S2,S3,S4,S5,S6,s7,s8,s9,s10:String;
    rSumOut:Real;
    StrWk:String;
    AktDate:TDateTime;
    IdH:INteger;
begin

  prFindBy(cxButtonEdit1.Tag,S1,S2,S3,S4,S5,S6,s7,s8,s9,s10);

  rSumOut:=0;

  try
    ViewDoc2.BeginUpdate;


    teDocsSpec.First;
    while not teDocsSpec.Eof do
    begin
      rSumOut:=rSumOut+rv(teDocsSpeccard_sum.AsFloat);
      teDocsSpec.Next;
    end;
  finally
    ViewDoc2.EndUpdate;
  end;

  if cxRadioButton1.Checked then
  begin
    Rep1.LoadFromFile(CurDir + 'Predlojenie.frf');

    frVariables.Variable['FIO']:=S1;
    frVariables.Variable['Adres']:=S6;
    frVariables.Variable['Phone']:=S7;
    frVariables.Variable['DocDate']:=cxDateEdit1.Date;
    frVariables.Variable['Sum1']:=rSumOut;
    frVariables.Variable['Person']:=Person.Name;
    frVariables.Variable['Picture2']:=cxBlobEdit1.EditValue;

    Rep1.ReportName:='������������ �����������.';
    Rep1.PrepareReport;
    Rep1.ShowPreparedReport;
  end;
  if cxRadioButton2.Checked then
  begin
    Rep1.LoadFromFile(CurDir + 'dogovor.frf');

    IDH:=fmAddDoc2.Tag;

    dmO.quPlat.Active:=False;
    dmO.quPlat.ParamByName('IDH').AsInteger:=IDH;
    dmO.quPlat.Active:=True;

    frVariables.Variable['DOC_NUMBER']:=cxTextEdit3.Text;
    frVariables.Variable['DOC_DATE']:=cxDateEdit1.Date;
    frVariables.Variable['BUYER_F']:=s8;
    frVariables.Variable['BUYER_I']:=s9;
    frVariables.Variable['BUYER_O']:=s10;
    frVariables.Variable['BUYER_PASSPORTSERIA']:=s2;
    frVariables.Variable['BUYER_PASSPORTNOMER']:=s3;
    frVariables.Variable['BUYER_PASSPORTISSUER']:=s4;
    frVariables.Variable['BUYER_PASSPORTISSUEDATE']:=s5;
    frVariables.Variable['BUYER_ADDRESS']:=s6;

    Str(rSumOut:9:2,StrWk);
    frVariables.Variable['DOC_SUM']:=Trim(StrWk);

    Str(cxCurrencyEdit1.EditValue:9:2,StrWk);
    frVariables.Variable['DOC_PREDOPLATA']:=Trim(StrWk);

    frVariables.Variable['DOC_WARRANTY']:=cxTextEdit1.Text;
    frVariables.Variable['DOC_IZGOTOVLENIE']:=cxSpinEdit2.EditValue;
    frVariables.Variable['SSUM']:=MoneyToString(rSumOut,True,False);
    frVariables.Variable['SPRE']:=MoneyToString(cxCurrencyEdit1.EditValue,True,False);

    Rep1.ReportName:='�������.';
    Rep1.PrepareReport;
    Rep1.ShowPreparedReport;
  end;
  if cxRadioButton3.Checked then
  begin
    if fmAddDoc2.Tag>0 then
    begin
      fmSetDate.DateTimePicker1.Date:=Date;
      fmSetDate.ShowModal;
      if fmSetDate.ModalResult=mrOk then
      begin
        AktDate:=fmSetDate.DateTimePicker1.Date;
        Label1.Caption:='( ��� �� '+FormatDateTime('dd.mm.yyyy',AktDate)+')';

        //��������
        try
          IDH:=fmAddDoc2.Tag;
          with dmO do
          begin
            quDocsId.Active:=False;
            quDocsID.ParamByName('IDH').AsInteger:=IdH;
            quDocsId.Active:=True;

            if quDocsId.RecordCount=1 then
            begin
              quDocsId.First;

              quDocsId.Edit;
              quDocsIdAKTDATE.AsDateTime:=Trunc(AktDate);
              quDocsId.Post;
            end;
          end;  
        except
        end;

        Rep1.LoadFromFile(CurDir + 'Akt.frf');

        frVariables.Variable['DOC_NUMBER']:=cxTextEdit3.Text;
        frVariables.Variable['DOC_DATE']:=cxDateEdit1.Date;
        frVariables.Variable['SAKT_DATE']:=FormatDateTime('dd.mm.yyyy',AktDate);
        frVariables.Variable['AKT_DATE']:=AktDate;
        frVariables.Variable['BUYER_F']:=s8;
        frVariables.Variable['BUYER_I']:=s9;
        frVariables.Variable['BUYER_O']:=s10;
        frVariables.Variable['BUYER_PASSPORTSERIA']:=s2;
        frVariables.Variable['BUYER_PASSPORTNOMER']:=s3;
        frVariables.Variable['BUYER_PASSPORTISSUER']:=s4;
        frVariables.Variable['BUYER_PASSPORTISSUEDATE']:=s5;
        frVariables.Variable['BUYER_ADDRESS']:=s6;

        Str(rSumOut:9:2,StrWk);
        frVariables.Variable['DOC_SUM']:=Trim(StrWk);

        Str(cxCurrencyEdit1.EditValue:9:2,StrWk);
        frVariables.Variable['DOC_PREDOPLATA']:=Trim(StrWk);

        frVariables.Variable['DOC_WARRANTY']:=cxTextEdit1.Text;
        frVariables.Variable['DOC_IZGOTOVLENIE']:=cxSpinEdit2.EditValue;
        frVariables.Variable['SSUM']:=MoneyToString(rSumOut,True,False);
        frVariables.Variable['SPRE']:=MoneyToString(cxCurrencyEdit1.EditValue,True,False);

        Rep1.ReportName:='��� ����� �����.';
        Rep1.PrepareReport;
        Rep1.ShowPreparedReport;
      end;
    end else
      showmessage('��� ���������� ���� ��������� ��������.');
  end;
end;

procedure TfmAddDoc2.acSetVerExecute(Sender: TObject);
Var rDSum:Real;
begin
  with dmO do
  begin
    ViewVer.BeginUpdate;
    ViewDoc2.BeginUpdate;

    rDSum:=0;
    try
      if quDV.RecordCount>0 then
      begin
        CloseTe(teDocsSpec);


        fmAddDoc2.cxCurrencyEdit1.Value:=quDVPREDOPLATA.AsFloat;
        fmAddDoc2.cxTextEdit1.Text:=quDVWARRANTY.AsString;
        fmAddDoc2.cxSpinEdit2.Value:=quDVIZGOTOVLENIE.AsInteger;
        fmAddDoc2.cxBlobEdit1.EditValue:=quDVVPICT.AsVariant;

        quDS.Active:=False;
        quDS.ParamByName('IDH').AsInteger:=Idh;
        quDS.ParamByName('IDV').AsInteger:=quDVIDV.AsInteger;
        quDS.Active:=True;

        quDS.First;
        while not quDS.Eof do
        begin
          teDocsSpec.Append;
          teDocsSpecNum.AsInteger:=quDSNUM.AsInteger;
          teDocsSpeccard_id.AsInteger:=quDSCODE.AsInteger;
          teDocsSpeccard_count.AsFloat:=quDSQUANT.AsFloat;
          teDocsSpeccard_price.AsFloat:=quDSPRICEOUT.AsFloat;
          teDocsSpeccard_sum.AsFloat:=quDSSUMOUT.AsFloat;
          teDocsSpeccard_name.asstring:=quDSNAME.AsString+' '+quDSNAMESHORT.AsString;
          teDocsSpecpost_name.AsString:=quDSNAMECL.AsString;
          teDocsSpecpost_id.AsInteger:=quDSIPOST.AsInteger;
          teDocsSpecComment.AsString:=quDSComment.AsString;
          teDocsSpecDProc.AsFloat:=quDSDPROC.AsFloat;
          teDocsSpecDSum.AsFloat:=quDSDSUM.AsFloat;
          teDocsSpecRSum.AsFloat:=quDSSUMOUT.AsFloat+quDSDSUM.AsFloat;
          teDocsSpec.Post;

          rDSum:=rDSum+quDSDSUM.AsFloat;

//          teDocsSpecpost_id.AsInteger:=prFindMainGroup(quCardsSelParent.AsInteger,S1);
//          teDocsSpecpost_name.AsString:=S1;

          quDS.Next;
        end;
        quDS.Active:=False;
      end;

    finally
      ViewVer.EndUpdate;
      ViewDoc2.EndUpdate;

      if abs(rDSum)>0.001 then
      begin
        ViewDoc2DProc.Visible:=True;
        ViewDoc2DSum.Visible:=True;
        ViewDoc2RSum.Visible:=True;
      end else
      begin
        ViewDoc2DProc.Visible:=False;
        ViewDoc2DSum.Visible:=False;
        ViewDoc2RSum.Visible:=False;
      end;
    end;
    delay(10);
  end;
end;

procedure TfmAddDoc2.ViewVerDblClick(Sender: TObject);
begin
  acSetVer.Execute;
end;

procedure TfmAddDoc2.ViewVerSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  acSetVer.Execute;
end;

procedure TfmAddDoc2.ViewVerCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acSetVer.Execute;
end;

procedure TfmAddDoc2.ViewVerCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acSetVer.Execute;
end;

procedure TfmAddDoc2.acSumInExecute(Sender: TObject);
begin
  //
  ViewDoc2card_pricein.Visible:=True;
  ViewDoc2card_sumin.Visible:=True;
end;

procedure TfmAddDoc2.acSetDiscExecute(Sender: TObject);
  //��������� ������ �� ���������� �������
Var
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    rNac:Real;
begin
  //���������� �������
  if ViewDoc2.Controller.SelectedRecordCount>0 then
  begin
    if cxButton1.Enabled=False then
    begin
      showmessage('�������� � ������ ��������� �������������� ���������...');
      exit;
    end;

    fmSetNac:=tfmSetNac.Create(Application);
    fmSetNac.Label1.Caption:='�������� '+IntToStr(ViewDoc2.Controller.SelectedRecordCount)+' �������. �������� % ������?';
    fmSetNac.Caption:='���������� ������';
    fmSetNac.Label2.Caption:='���������� % ������';
    fmSetNac.ShowModal;

    if fmSetNac.ModalResult=mrOk then
    begin
//      showmessage('Ok');
      rNac:=fmSetNac.cxCalcEdit1.Value;
      ViewDoc2.BeginUpdate;
      for i:=0 to ViewDoc2.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc2.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc2.Columns[j].Name='ViewDoc2Num' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        if teDocsSpec.Locate('Num',iNum,[]) then
        begin
          teDocsSpec.Edit;
          teDocsSpecDProc.AsFloat:=rNac;
          teDocsSpecDSum.AsFloat:=rv(teDocsSpeccard_count.AsFloat*teDocsSpeccard_price.AsFloat/100*teDocsSpecDProc.AsFloat);
          teDocsSpeccard_sum.AsFloat:=teDocsSpeccard_count.AsFloat*teDocsSpeccard_price.AsFloat-teDocsSpecDSum.AsFloat;
          teDocsSpecRSum.AsFloat:=teDocsSpeccard_sum.AsFloat+teDocsSpecDSum.AsFloat;
          teDocsSpec.Post;
        end;


      end;
      ViewDoc2.EndUpdate;

      ViewDoc2DProc.Visible:=True;
      ViewDoc2DSum.Visible:=True;
      ViewDoc2RSum.Visible:=True;
    end;
    fmSetNac.Release;
  end;
end;

procedure TfmAddDoc2.acEditPosExecute(Sender: TObject);
begin
//�������������� �������
  if teDocsSpec.RecordCount>0 then
  begin
    bAddSpecRet1:=True;
    fmCards.Show;
  end;
end;

procedure TfmAddDoc2.cxLabel11Click(Sender: TObject);
begin
  acEditPos.Execute;
end;

procedure TfmAddDoc2.acAddShablonExecute(Sender: TObject);
Var Idh:INteger;
begin
 //�������� � �������
  if not CanDo('prAddShablon') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if quDV.RecordCount>0 then
    begin
      fmAddClass.Caption:='���������� �������.';
      fmAddClass.label4.Caption:='���������� �������.';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        try
          ViewDoc2.BeginUpdate;

          IdH:=GetId('Shabl')+1;
          if taShablH.Active=False then taShablH.Active:=True
          else taShablH.FullRefresh;

          if taShablS.Active=False then taShablS.Active:=True
          else taShablS.FullRefresh;

          taShablH.Append;
          taShablHIDH.AsInteger:= IdH;
          taShablHNAMESH.AsString:=fmAddClass.cxTextEdit1.Text;
          taShablHPREDOPLATA.AsFloat:=cxCurrencyEdit1.EditValue;
          taShablHIZGOTOVLENIE.AsInteger:=cxSpinEdit2.EditValue;
          taShablHWARRANTY.AsString:=cxTextEdit1.Text;
          taShablH.Post;

          teDocsSpec.First;
          while not teDocsSpec.Eof do
          begin
            taShablS.Append;
            taShablSIDH.AsInteger:=Idh;
            taShablSNUM.AsInteger:=teDocsSpecNum.AsInteger;
            taShablSCODE.AsInteger:=teDocsSpeccard_id.AsInteger;
            taShablSNAME.AsString:=teDocsSpeccard_name.AsString;
            taShablSQUANT.AsFloat:=teDocsSpeccard_count.AsFloat;
            taShablSPRICEOUT.AsFloat:=teDocsSpeccard_price.AsFloat;
            taShablS.Post;

            teDocsSpec.Next;
          end;
        finally
          ViewDoc2.EndUpdate;
        end;
        showmessage('�������� ������� � �������.');

      end;
    end;
  end;
end;

procedure TfmAddDoc2.acSetShablExecute(Sender: TObject);
Var iMax:Integer;
    S1:String;
begin
 //������� ������
  with dmO do
  begin
    if taShablH.Active=False then taShablH.Active:=True
    else taShablH.FullRefresh;

    if taShablS.Active=False then taShablS.Active:=True
    else taShablS.FullRefresh;

    taShablH.First;

    fmBuff.ShowModal;
    if fmBuff.ModalResult=mrOk then
    begin
      if cxButton1.Enabled=False then
      begin
        showmessage('�������� �������� ������ �� ��������. �������������� ���������.');
        exit;
      end;
      if taShablH.RecordCount>0 then
      begin
        cxCurrencyEdit1.EditValue:=taShablHPREDOPLATA.AsFloat;
        cxSpinEdit2.EditValue:=taShablHIZGOTOVLENIE.AsInteger;
        cxTextEdit1.Text:=taShablHWARRANTY.AsString;

        try
          ViewDoc2.BeginUpdate;

          quShablSId.Active:=False;
          quShablSId.ParamByName('IDH').AsInteger:=taShablHIDH.AsInteger;
          quShablSId.Active:=True;

          CloseTe(teDocsSpec);

          iMax:=1;

          quShablSId.First;
          while not quShablSId.EOF do
          begin

            quFCard.Active:=False;
            quFCard.SelectSQL.Clear;
            quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,LASTPRICEIN,CATEGORY');
            quFCard.SelectSQL.Add('FROM OF_CARDS');
            quFCard.SelectSQL.Add('where ID='+IntToStr(quShablSIdCODE.AsInteger));
            quFCard.SelectSQL.Add('and IACTIVE>0');
            quFCard.Active:=True;

            if quFCard.RecordCount=1 then
            begin
              teDocsSpec.Append;
              teDocsSpecNum.AsInteger:=iMax;
              teDocsSpeccard_id.AsInteger:=quShablSIdCODE.AsInteger;
              teDocsSpeccard_count.AsFloat:=quShablSIdQUANT.AsFloat;
              teDocsSpeccard_price.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
              teDocsSpeccard_sum.AsFloat:=rv(quFCardLASTPRICEOUT.AsFloat*quShablSIdQUANT.AsFloat);
              teDocsSpeccard_pricein.AsFloat:=quFCardLASTPRICEIN.AsFloat;
              teDocsSpeccard_sumin.AsFloat:=rv(quFCardLASTPRICEIN.AsFloat*quShablSIdQUANT.AsFloat);
              teDocsSpeccard_name.AsString:=quFCardNAME.AsString+' '+prFindSM1(quFCardIMESSURE.AsInteger);
              teDocsSpecpost_id.AsInteger:=prFindMainGroup(quFCardPARENT.AsInteger,S1);
              teDocsSpecpost_name.AsString:=S1;
              teDocsSpecComment.AsString:='';
              teDocsSpecDProc.AsFloat:=0;
              teDocsSpecDSum.AsFloat:=0;
              teDocsSpecRSum.AsFloat:=rv(quFCardLASTPRICEOUT.AsFloat*quShablSIdQUANT.AsFloat);
              teDocsSpec.Post;
            end;

            quShablSId.Next; inc(iMax);
          end;

          quShablSId.Active:=False;
        finally
          ViewDoc2.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxButton5Click(Sender: TObject);
Var IdH:INteger;
    rSum:Real;
begin
  with dmO do
  begin
    if fmAddDoc2.Tag=0 then
    begin
      showmessage('��������� ������� ����� ��������� ������� ��������.');
    end else
    begin
      IdH:=fmAddDoc2.Tag;
      rSum:=0;

      ViewDoc2.BeginUpdate;
      teDocsSpec.First;
      while not teDocsSpec.Eof do
      begin
        rSum:=rSum+teDocsSpeccard_sum.AsFloat;

        teDocsSpec.Next;
      end;

      ViewDoc2.EndUpdate;

      fmOplata.Label1.Caption:='����� �� �������� - '+fts(rSum);

      with fmOplata do
      begin
        Closete(teOplata);



        quPlat.Active:=False;
        quPlat.ParamByName('IDH').AsInteger:=IDH;
        quPlat.Active:=True;
        quPlat.First;
        while not quPlat.Eof do
        begin
          teOplata.Append;
          teOplataNum.AsInteger:=quPlatNUM.AsInteger;
          teOplataNamePl.AsString:=quPlatNAMEPL.AsString;
          teOplataDatePl.AsDateTime:=Trunc(quPlatDATEPL.AsDateTime);
          teOplataSumPl.AsFloat:=quPlatSUMPL.AsFloat;

          quPlat.Next;
        end;

        quPlat.Active:=False;

        if fmAddDoc2.cxButton1.Enabled then
        begin
          fmOplata.cxButton1.Enabled:=True;
          fmOplata.cxButton3.Enabled:=True;
          fmOplata.cxButton4.Enabled:=True;
          fmOplata.ViewOplata.OptionsData.Editing:=True;
          fmOplata.acAddPl.Enabled:=True;
          fmOplata.acDelPl.Enabled:=True;
          fmOplata.acSaveOpl.Enabled:=True;
        end else
        begin
          fmOplata.cxButton1.Enabled:=False;
          fmOplata.cxButton3.Enabled:=False;
          fmOplata.cxButton4.Enabled:=False;
          fmOplata.ViewOplata.OptionsData.Editing:=False;

          fmOplata.acAddPl.Enabled:=False;
          fmOplata.acDelPl.Enabled:=False;
          fmOplata.acSaveOpl.Enabled:=False;
        end;


      end;
      fmOplata.Tag:=IDH;
      fmOplata.Show;
    end;
  end;
end;

end.
