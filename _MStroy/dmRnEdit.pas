unit dmRnEdit;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  cxStyles, ImgList, Controls, FIBQuery, pFIBQuery, pFIBStoredProc,
  frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl, FR_E_HTML2, FR_E_HTM,
  FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT;

type
  TdmC = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    imState: TImageList;
    quMenuTree: TpFIBDataSet;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelTREETYPE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelCATEG: TFIBSmallIntField;
    quMenuSelPARENT: TFIBSmallIntField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelLACK: TFIBSmallIntField;
    quMenuSelDESIGNSIFR: TFIBSmallIntField;
    quMenuSelALTNAME: TFIBStringField;
    quMenuSelNALOG: TFIBFloatField;
    quMenuSelBARCODE: TFIBStringField;
    quMenuSelIMAGE: TFIBSmallIntField;
    quMenuSelCONSUMMA: TFIBFloatField;
    quMenuSelMINREST: TFIBSmallIntField;
    quMenuSelPRNREST: TFIBSmallIntField;
    quMenuSelCOOKTIME: TFIBSmallIntField;
    quMenuSelDISPENSER: TFIBSmallIntField;
    quMenuSelDISPKOEF: TFIBSmallIntField;
    quMenuSelACCESS: TFIBSmallIntField;
    quMenuSelFLAGS: TFIBSmallIntField;
    quMenuSelTARA: TFIBSmallIntField;
    quMenuSelCNTPRICE: TFIBSmallIntField;
    quMenuSelBACKBGR: TFIBFloatField;
    quMenuSelFONTBGR: TFIBFloatField;
    quMenuSelIACTIVE: TFIBSmallIntField;
    quMenuSelIEDIT: TFIBSmallIntField;
    dsMenuSel: TDataSource;
    quCateg: TpFIBDataSet;
    dsCateg: TDataSource;
    quCategSIFR: TFIBIntegerField;
    quCategNAME: TFIBStringField;
    quCategTAX_GROUP: TFIBIntegerField;
    quCategIACTIVE: TFIBSmallIntField;
    quCategIEDIT: TFIBSmallIntField;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    quMaxIdCateg: TpFIBDataSet;
    quMaxIdCategMAXID: TFIBIntegerField;
    quModGrMo: TpFIBDataSet;
    quModGrMoSIFR: TFIBIntegerField;
    quModGrMoNAME: TFIBStringField;
    quModGrMoPARENT: TFIBIntegerField;
    quModGrMoPRICE: TFIBFloatField;
    quModGrMoREALPRICE: TFIBFloatField;
    quModGrMoIACTIVE: TFIBSmallIntField;
    quModGrMoIEDIT: TFIBSmallIntField;
    quMaxIdGrMo: TpFIBDataSet;
    quMaxIdGrMoMAXID: TFIBIntegerField;
    quMods: TpFIBDataSet;
    dsMods: TDataSource;
    quModsSIFR: TFIBIntegerField;
    quModsNAME: TFIBStringField;
    quModsPARENT: TFIBIntegerField;
    quModsPRICE: TFIBFloatField;
    quModsREALPRICE: TFIBFloatField;
    quModsIACTIVE: TFIBSmallIntField;
    quModsIEDIT: TFIBSmallIntField;
    quDelMod: TpFIBDataSet;
    trUpd1: TpFIBTransaction;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    taMenu: TpFIBDataSet;
    trUpd2: TpFIBTransaction;
    trSel2: TpFIBTransaction;
    taMenuSIFR: TFIBIntegerField;
    taMenuNAME: TFIBStringField;
    taMenuPRICE: TFIBFloatField;
    taMenuCODE: TFIBStringField;
    taMenuTREETYPE: TFIBStringField;
    taMenuLIMITPRICE: TFIBFloatField;
    taMenuCATEG: TFIBSmallIntField;
    taMenuPARENT: TFIBSmallIntField;
    taMenuLINK: TFIBSmallIntField;
    taMenuSTREAM: TFIBSmallIntField;
    taMenuLACK: TFIBSmallIntField;
    taMenuDESIGNSIFR: TFIBSmallIntField;
    taMenuALTNAME: TFIBStringField;
    taMenuNALOG: TFIBFloatField;
    taMenuBARCODE: TFIBStringField;
    taMenuIMAGE: TFIBSmallIntField;
    taMenuCONSUMMA: TFIBFloatField;
    taMenuMINREST: TFIBSmallIntField;
    taMenuPRNREST: TFIBSmallIntField;
    taMenuCOOKTIME: TFIBSmallIntField;
    taMenuDISPENSER: TFIBSmallIntField;
    taMenuDISPKOEF: TFIBSmallIntField;
    taMenuACCESS: TFIBSmallIntField;
    taMenuFLAGS: TFIBSmallIntField;
    taMenuTARA: TFIBSmallIntField;
    taMenuCNTPRICE: TFIBSmallIntField;
    taMenuBACKBGR: TFIBFloatField;
    taMenuFONTBGR: TFIBFloatField;
    taMenuIACTIVE: TFIBSmallIntField;
    taMenuIEDIT: TFIBSmallIntField;
    prAddRClassif: TpFIBStoredProc;
    trUpdProc: TpFIBTransaction;
    quMenuFindParent: TpFIBDataSet;
    quMenuFindParentCOUNTREC: TFIBIntegerField;
    prDelRClassif: TpFIBStoredProc;
    prEditRClassif: TpFIBStoredProc;
    quCategUp: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBSmallIntField1: TFIBSmallIntField;
    FIBSmallIntField2: TFIBSmallIntField;
    dsCategUp: TDataSource;
    trSel3: TpFIBTransaction;
    quModsGr: TpFIBDataSet;
    FIBIntegerField3: TFIBIntegerField;
    FIBStringField2: TFIBStringField;
    FIBIntegerField4: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBSmallIntField3: TFIBSmallIntField;
    FIBSmallIntField4: TFIBSmallIntField;
    dsModsGr: TDataSource;
    quNalog: TpFIBDataSet;
    dsNalog: TDataSource;
    quNalogID: TFIBIntegerField;
    quNalogPRIOR: TFIBSmallIntField;
    quNalogNAME: TFIBStringField;
    quMenuSelNAMECA: TFIBStringField;
    quMenuSelNAMEMO: TFIBStringField;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    prGetId: TpFIBStoredProc;
    quMaxIdMe: TpFIBDataSet;
    quMaxIdMeMAXID: TFIBIntegerField;
    taMenuPrint: TpFIBDataSet;
    taMenuPrintSIFR: TFIBIntegerField;
    taMenuPrintNAME: TFIBStringField;
    taMenuPrintPRICE: TFIBFloatField;
    taMenuPrintCODE: TFIBStringField;
    taMenuPrintTREETYPE: TFIBStringField;
    taMenuPrintLIMITPRICE: TFIBFloatField;
    taMenuPrintCATEG: TFIBSmallIntField;
    taMenuPrintPARENT: TFIBSmallIntField;
    taMenuPrintLINK: TFIBSmallIntField;
    taMenuPrintSTREAM: TFIBSmallIntField;
    taMenuPrintLACK: TFIBSmallIntField;
    taMenuPrintDESIGNSIFR: TFIBSmallIntField;
    taMenuPrintALTNAME: TFIBStringField;
    taMenuPrintNALOG: TFIBFloatField;
    taMenuPrintBARCODE: TFIBStringField;
    taMenuPrintIMAGE: TFIBSmallIntField;
    taMenuPrintCONSUMMA: TFIBFloatField;
    taMenuPrintMINREST: TFIBSmallIntField;
    taMenuPrintPRNREST: TFIBSmallIntField;
    taMenuPrintCOOKTIME: TFIBSmallIntField;
    taMenuPrintDISPENSER: TFIBSmallIntField;
    taMenuPrintDISPKOEF: TFIBSmallIntField;
    taMenuPrintACCESS: TFIBSmallIntField;
    taMenuPrintFLAGS: TFIBSmallIntField;
    taMenuPrintTARA: TFIBSmallIntField;
    taMenuPrintCNTPRICE: TFIBSmallIntField;
    taMenuPrintBACKBGR: TFIBFloatField;
    taMenuPrintFONTBGR: TFIBFloatField;
    taMenuPrintIACTIVE: TFIBSmallIntField;
    taMenuPrintIEDIT: TFIBSmallIntField;
    taMenuPrintNAMEGR: TFIBStringField;
    dsMenuPrint: TDataSource;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quModList: TpFIBDataSet;
    quModListSIFR: TFIBIntegerField;
    quModListNAME: TFIBStringField;
    quModListPARENT: TFIBIntegerField;
    quModListNAMEGR: TFIBStringField;
    taMH: TpFIBDataSet;
    trMH: TpFIBTransaction;
    dsMH: TDataSource;
    taMHID: TFIBIntegerField;
    taMHNAMESTREAM: TFIBStringField;
    quMenuSelNAMESTREAM: TFIBStringField;
    taMESSAGE: TpFIBDataSet;
    taMESSAGEID: TFIBIntegerField;
    taMESSAGEMESSAG: TFIBStringField;
    dstaMESSAGE: TDataSource;
    quMenuDayList: TpFIBDataSet;
    quMenuDayListID: TFIBSmallIntField;
    quMenuDayListNAME: TFIBStringField;
    quMenuDayListMDATE: TFIBDateField;
    quMenuDayListIACTIVE: TFIBIntegerField;
    dsquMenuDayList: TDataSource;
    quMDL_Id: TpFIBDataSet;
    quMDL_IdID: TFIBSmallIntField;
    quMDL_IdNAME: TFIBStringField;
    quMDL_IdMDATE: TFIBDateField;
    quMDL_IdIACTIVE: TFIBIntegerField;
    quMenuDay: TpFIBDataSet;
    quMenuDayIDH: TFIBIntegerField;
    quMenuDayID: TFIBIntegerField;
    quMenuDayNAME: TFIBStringField;
    quMenuDayPRIOR: TFIBIntegerField;
    quMenuDayPARENT: TFIBIntegerField;
    quMaxMDG: TpFIBDataSet;
    quMaxMDGID: TFIBIntegerField;
    quMDGID: TpFIBDataSet;
    quMDGIDIDH: TFIBIntegerField;
    quMDGIDID: TFIBIntegerField;
    quMDGIDNAMEGR: TFIBStringField;
    quMDGIDPRIOR: TFIBIntegerField;
    quMDGIDPARENT: TFIBIntegerField;
    quMDS: TpFIBDataSet;
    dsquMDS: TDataSource;
    quMDSIDH: TFIBIntegerField;
    quMDSIDGR: TFIBIntegerField;
    quMDSSIFR: TFIBIntegerField;
    quMDSNAME: TFIBStringField;
    quMDSPRICE: TFIBFloatField;
    quMDFindParent: TpFIBDataSet;
    quMDFindParentCOUNTREC: TFIBIntegerField;
    quMDRep: TpFIBDataSet;
    quMDRepIDH: TFIBIntegerField;
    quMDRepIDGR: TFIBIntegerField;
    quMDRepSIFR: TFIBIntegerField;
    quMDRepNAMEGR: TFIBStringField;
    quMDRepPRIOR: TFIBIntegerField;
    quMDRepNAME: TFIBStringField;
    quMDRepPRICE: TFIBFloatField;
    prSetMDL: TpFIBStoredProc;
    trSet: TpFIBTransaction;
    quD: TpFIBQuery;
    quMDSALTNAME: TFIBStringField;
    quMDRepALTNAME: TFIBStringField;
    quMDGroups: TpFIBDataSet;
    quMDGroupsIDH: TFIBIntegerField;
    quMDGroupsID: TFIBIntegerField;
    quMDGroupsNAMEGR: TFIBStringField;
    quMDGroupsPRIOR: TFIBIntegerField;
    quMDGroupsPARENT: TFIBIntegerField;
    quMDSPRI: TFIBIntegerField;
    quMDRepPRI: TFIBIntegerField;
    quFindB: TpFIBDataSet;
    quFindBSIFR: TFIBIntegerField;
    quFindBNAME: TFIBStringField;
    quFindBPRICE: TFIBFloatField;
    quFindBCODE: TFIBStringField;
    quFindBPARENT: TFIBSmallIntField;
    quFindBCONSUMMA: TFIBFloatField;
    dsFindB: TDataSource;
    quModsCODEB: TFIBIntegerField;
    quModsKB: TFIBFloatField;
    taTabsAll: TpFIBDataSet;
    taTabsAllID: TFIBIntegerField;
    taTabsAllID_PERSONAL: TFIBIntegerField;
    taTabsAllNUMTABLE: TFIBStringField;
    taTabsAllQUESTS: TFIBIntegerField;
    taTabsAllTABSUM: TFIBFloatField;
    taTabsAllBEGTIME: TFIBDateTimeField;
    taTabsAllENDTIME: TFIBDateTimeField;
    taTabsAllDISCONT: TFIBStringField;
    taTabsAllOPERTYPE: TFIBStringField;
    taTabsAllCHECKNUM: TFIBIntegerField;
    taTabsAllSKLAD: TFIBSmallIntField;
    taTabsAllDISCONT1: TFIBStringField;
    taTabsAllSTATION: TFIBIntegerField;
    taTabsAllNUMZ: TFIBIntegerField;
    taDelPar: TpFIBDataSet;
    dstaDelPar: TDataSource;
    taDelParID: TFIBIntegerField;
    taDelParNAMEDEL: TFIBStringField;
    taDelParBTYPE: TFIBSmallIntField;
    taCategSale: TpFIBDataSet;
    dstaCategSale: TDataSource;
    taCategSaleID: TFIBIntegerField;
    taCategSaleNAMECS: TFIBStringField;
    quMenuSelDATEB: TFIBIntegerField;
    quMenuSelDATEE: TFIBIntegerField;
    quMenuSelDAYWEEK: TFIBStringField;
    quMenuSelTIMEB: TFIBTimeField;
    quMenuSelTIMEE: TFIBTimeField;
    quMenuSelALLTIME: TFIBSmallIntField;
    quMenuSelTIMEBB: TStringField;
    quMenuSelTIMEEE: TStringField;
    trUpdMenu: TpFIBTransaction;
    quMenuID: TpFIBDataSet;
    quMenuIDSIFR: TFIBIntegerField;
    quMenuIDNAME: TFIBStringField;
    quMenuIDPRICE: TFIBFloatField;
    quMenuIDCODE: TFIBStringField;
    quMenuIDTREETYPE: TFIBStringField;
    quMenuIDLIMITPRICE: TFIBFloatField;
    quMenuIDCATEG: TFIBSmallIntField;
    quMenuIDPARENT: TFIBSmallIntField;
    quMenuIDLINK: TFIBSmallIntField;
    quMenuIDSTREAM: TFIBSmallIntField;
    quMenuIDLACK: TFIBSmallIntField;
    quMenuIDDESIGNSIFR: TFIBSmallIntField;
    quMenuIDALTNAME: TFIBStringField;
    quMenuIDNALOG: TFIBFloatField;
    quMenuIDBARCODE: TFIBStringField;
    quMenuIDIMAGE: TFIBSmallIntField;
    quMenuIDCONSUMMA: TFIBFloatField;
    quMenuIDMINREST: TFIBSmallIntField;
    quMenuIDPRNREST: TFIBSmallIntField;
    quMenuIDCOOKTIME: TFIBSmallIntField;
    quMenuIDDISPENSER: TFIBSmallIntField;
    quMenuIDDISPKOEF: TFIBSmallIntField;
    quMenuIDACCESS: TFIBSmallIntField;
    quMenuIDFLAGS: TFIBSmallIntField;
    quMenuIDTARA: TFIBSmallIntField;
    quMenuIDCNTPRICE: TFIBSmallIntField;
    quMenuIDBACKBGR: TFIBFloatField;
    quMenuIDFONTBGR: TFIBFloatField;
    quMenuIDIACTIVE: TFIBSmallIntField;
    quMenuIDIEDIT: TFIBSmallIntField;
    quMenuIDDATEB: TFIBIntegerField;
    quMenuIDDATEE: TFIBIntegerField;
    quMenuIDDAYWEEK: TFIBStringField;
    quMenuIDTIMEB: TFIBTimeField;
    quMenuIDTIMEE: TFIBTimeField;
    quMenuIDALLTIME: TFIBSmallIntField;
    taMESSAGEISERV: TFIBSmallIntField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quMenuSelCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;

var
  dmC: TdmC;

implementation

uses Un1;

{$R *.dfm}

procedure TdmC.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
  except
  end;
end;

procedure TdmC.DataModuleDestroy(Sender: TObject);
begin
  try
    CasherRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmC do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;


procedure TdmC.quMenuSelCalcFields(DataSet: TDataSet);
begin
  if quMenuSelALLTIME.AsInteger<>1 then
  begin
    quMenuSelTIMEBB.AsString:=FormatDateTime('hh:nn',quMenuSelTIMEB.AsDateTime);
    quMenuSelTIMEEE.AsString:=FormatDateTime('hh:nn',quMenuSelTIMEE.AsDateTime);
  end;  
end;

end.
