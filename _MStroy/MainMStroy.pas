unit MainMStroy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  ActnList, ExtCtrls, SpeedBar, XPStyleActnCtrls, StdCtrls, pFIBDataSet,
  Menus;

type
  TfmMainMStroy = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acGoods: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem5: TSpeedItem;
    acMessure: TAction;
    acPriceType: TAction;
    acMHP: TAction;
    acClients: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    acOut: TAction;
    SpeedItem4: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    acRepPost: TAction;
    Action1: TAction;
    TimerClose: TTimer;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acCli: TAction;
    SpeedItem12: TSpeedItem;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMessureExecute(Sender: TObject);
    procedure acGoodsExecute(Sender: TObject);
    procedure acPriceTypeExecute(Sender: TObject);
    procedure acMHPExecute(Sender: TObject);
    procedure acClientsExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure acMoveDateExecute(Sender: TObject);
    procedure acTovRepExecute(Sender: TObject);
    procedure acRepPribExecute(Sender: TObject);
    procedure acObVedExecute(Sender: TObject);
    procedure acInDocsExecute(Sender: TObject);
    procedure acActsExecute(Sender: TObject);
    procedure amOperTExecute(Sender: TObject);
    procedure acRealExecute(Sender: TObject);
    procedure acRecalcPerExecute(Sender: TObject);
    procedure acRepPostExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure acPartRemnExecute(Sender: TObject);
    procedure acAvansExecute(Sender: TObject);
    procedure TimerCloseTimer(Sender: TObject);
    procedure acBGUExecute(Sender: TObject);
    procedure acTermoObrExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure acCategRExecute(Sender: TObject);
    procedure acRemnSpeedRealExecute(Sender: TObject);
    procedure acCliExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

procedure prFindPartRemn(IdGr:Integer);
procedure prAllViewOff;
procedure prAllViewOn;


var
  fmMainMStroy: TfmMainMStroy;

implementation

uses Un1, dmOffice, OMessure, Goods, PriceType, MH, Clients,
  SelPerSkl, DocsOut, TOSel, DMOReps, RepPrib, RepObSa, OperType, PerA_Office, ClassSel,
  ParamSel, RepPost, AddDoc2, PeriodUni, rCategory, SelPerSkl2, Docs, Buyers;

{$R *.dfm}

procedure prAllViewOff;
begin
  fmAddDoc2.ViewDoc2.BeginUpdate;
end;

procedure prAllViewOn;
begin
  fmAddDoc2.ViewDoc2.EndUpdate;
end;

procedure prFindPartRemn(IdGr:Integer);
Var sM:String;
    iM:Integer;
    QT1:TpFIBDataSet;
    iDate:Integer;
    rQ,rQrec:Real;
    rSum:Real;
begin
  with dmO do
  with dmORep do
  begin
    quClass.Active:=False;
    quClass.ParamByName('IPARENT').AsInteger:=IdGr;
    quClass.Active:=True;

    iDate:=Trunc(Date);

    quClass.First;    //��� �� �������
    while not quClass.Eof do
    begin
      with fmRepPost do
      begin
        quMh_.First;
        while not quMH_.Eof do
        begin
          rQ:=prCalcRemn(quClassID.AsInteger,iDate,quMH_ID.AsInteger);

          if rQ>0.001 then //������� ������������� ����������� �� �������, ����� �������������
          begin
            quFindPart.Active:=False;
            quFindPart.ParamByName('IDCARD').AsInteger:=quClassID.AsInteger;
            quFindPart.ParamByName('IDSTORE').AsInteger:=quMH_ID.AsInteger;
            quFindPart.Active:=True;
{
SELECT pi.ID,pi.IDSTORE,pi.IDDOC,pi.ARTICUL,pi.IDCLI,pi.DTYPE,
pi.QPART,pi.QREMN,pi.PRICEIN,pi.IDATE,Cli.NAMECL,mh.NAMEMH
FROM OF_PARTIN pi
left join OF_CLIENTS cli on pi.IDCLI=Cli.ID
left join OF_MH mh on pi.IDSTORE=mh.ID

where ARTICUL=:IDCARD and pi.QREMN>0.001 and pi.IDSTORE=:IDSTORE
Order by pi.IDATE DESC
}
            quFindPart.First; //������ ������

            //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

            while (quFindPart.Eof=False)and(rQ>0.001) do
            begin
              if rQ>=quFindPartQREMN.AsFloat then
              begin
                rQRec:=quFindPartQREMN.AsFloat;
                rQ:=rQ-rQRec;
              end else
              begin
                rQRec:=rQ;
                rQ:=0;
              end;

              taPost.Append;
              taPostIdCard.AsInteger:=quClassID.AsInteger;
              taPostNameC.AsString:=quClassNAME.AsString;
              prFindSM(quClassIMESSURE.AsInteger,sM,iM);
              taPostsM.AsString:=sM;
              taPostiM.AsInteger:=iM;
              taPostrQ.AsFloat:= rQRec;
              taPostrPrice.AsFloat:=quFindPartPRICEIN.AsFloat;
              taPostrSum.AsFloat:=rQRec*quFindPartPRICEIN.AsFloat;
              taPostiPost.AsInteger:=quFindPartIDCLI.AsInteger;
              taPostNameP.AsString:=quFindPartNAMECL.AsString;
              taPostiGr.AsInteger:=quClassPARENT.AsInteger;
              taPostNameG.AsString:=quClassNAMECL.asstring;
              taPostiDoc.AsInteger:=quFindPartIDDOC.AsInteger;
              taPostiTDoc.AsInteger:=quFindPartDTYPE.AsInteger;
              taPostsDoc.AsString:=FormatDateTime('dd.mm.yyyy',quFindPartIDATE.AsInteger);
              taPostNameMH.AsString:=quFindPartNAMEMH.AsString;
              taPost.Post;

              quFindPart.Next; Delay(10);
            end;
            quFindPart.Active:=False;
          end else
          begin
            if rQ<=(-0.001) then
            begin
              rSum:=prCalcRemnSumF(quClassID.AsInteger,iDate,quMH_ID.AsInteger,rQ);
             //rSum - ����� ������� (������������ � - ), �.�. �������������� �� ��������� �������

              taPost.Append;
              taPostIdCard.AsInteger:=quClassID.AsInteger;
              taPostNameC.AsString:=quClassNAME.AsString;
              prFindSM(quClassIMESSURE.AsInteger,sM,iM);
              taPostsM.AsString:=sM;
              taPostiM.AsInteger:=iM;
              taPostrQ.AsFloat:= rQ;
              taPostrPrice.AsFloat:=rSum/rQ;
              taPostrSum.AsFloat:=rSum;
              taPostiPost.AsInteger:=-1;
              taPostNameP.AsString:='';
              taPostiGr.AsInteger:=quClassPARENT.AsInteger;
              taPostNameG.AsString:=quClassNAMECL.asstring;
              taPostiDoc.AsInteger:=-1;
              taPostiTDoc.AsInteger:=-1;
              taPostsDoc.AsString:='';
              taPostNameMH.AsString:=quMH_NAMEMH.AsString;
              taPost.Post;
            end;
          end;

          quMH_.Next;
        end;
      end;
      quClass.Next;Delay(10);
    end;
    quClass.Active:=False;

    QT1:=TpFIBDataSet.Create(Owner);
    QT1.Active:=False;
    QT1.Database:=OfficeRnDb;
    QT1.Transaction:=trSel;
    QT1.SelectSQL.Clear;
    QT1.SelectSQL.Add('SELECT ID FROM OF_CLASSIF');
    QT1.SelectSQL.Add('WHERE ID_PARENT='+IntToStr(IdGr));
    QT1.Active:=True;

    QT1.First;
    while not QT1.Eof do
    begin
      prFindPartRemn(QT1.FieldByName('ID').AsInteger);
      QT1.Next; Delay(10);
    end;
    QT1.Active:=False;
    QT1.Free;
  end;
end;


procedure TfmMainMStroy.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainMStroy.FormCreate(Sender: TObject);
Var WkDir:String;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  WkDir:=GetCurrentDir;
  if CurDir<>WkDir then CurDir:=WkDir;

//  SetWorkingDirectory(PChar(ExtractFilePath(FileName)));

  if CurDir[Length(CurDir)]<>'\' then CurDir:=CurDir+'\';

  StatusBar1.Panels[1].Text:=CurDir;

  ReadIni;
  CommonSet.DateFrom:=Trunc(Date-20);
  CommonSet.DateTo:=Trunc(Date+1);
  CurVal.IdMH:=0;
  CurVal.NAMEMH:='';
//  if FileExists(CurDir+'Prib.cds') then DeleteFile(CurDir+'Prib.cds');
//  if FileExists(CurDir+'SpecC.cds') then DeleteFile(CurDir+'SpecC.cds');
//  if FileExists(CurDir+'SpecCInv.cds') then DeleteFile(CurDir+'SpecCInv.cds');
//  if FileExists(CurDir+'ttkspec.cds') then DeleteFile(CurDir+'ttkspec.cds');

  SpeedBar1.Color := UserColor.Main;
end;

procedure TfmMainMStroy.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainMStroy.FormShow(Sender: TObject);
Var StrC:String;
begin
  Left:=0;
  Top:=0;
//  Width:=1024;

  StrC:=CommonSet.DepartName;
  if SpecVal.DateTo<StrToDate('01.01.2100') then
    StrC:=StrC+' ��������� �� '+FormatDateTime('dd.mm.yyyy',SpecVal.DateTo);
  if SpecVal.CountStarts>=0 then
    StrC:=StrC+' �������� '+INtToStr(SpecVal.CountStarts)+' �������� ���������.';
  if SpecVal.CountStarts=-1 then
    StrC:=StrC+' �������� 0 �������� ���������.';

  Caption:=StrC;

  with dmO do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA_Office:=TfmPerA_Office.Create(Application);

  fmPerA_Office.StatusBar1.Color:=UserColor.Main;
  fmPerA_Office.GroupBox1.Color:=UserColor.Main;

  fmPerA_Office.ShowModal;
  if fmPerA_Office.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA_Office.Release;

    bMenuList:=False;
    bMenuListMo:=False;

    fmCards:=tfmCards.Create(Application);

    bMenuList:=True;
    bMenuListMo:=True;

    if (SpecVal.CountStarts=-1)or(SpecVal.DateTo<Date) then TimerClose.Enabled:=True;
    if dmO.quCateg.Active=False then dmO.quCateg.Active:=True;

  end
  else
  begin
    fmPerA_Office.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainMStroy.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainMStroy.acMessureExecute(Sender: TObject);
begin
  //������� ���������
  //������� , ��������  - ����� ����� �������� ��� ������� ����� ����
  bAddSpec:=False;
  fmMessure.ShowModal;
  
end;

procedure TfmMainMStroy.acGoodsExecute(Sender: TObject);
begin
  bAddSpecRet:= False;
  bAddSpecRet1:= False;
  bAddSpecIn:= False;
  bAddSpecB:= False;
  bAddSpecB1:= False;
  bAddSpecInv:= False;
  bAddSpecCompl:= False;
  bAddTSpec:= False;
  bAddSpecAO:= False;
  bAddSpecVn:=False;
  bAddSpecR:=False;

  fmCards.Show;
end;

procedure TfmMainMStroy.acPriceTypeExecute(Sender: TObject);
begin
  //���� ���
  with dmO do
  begin
    taPriceT.Active:=False;
    taPriceT.Active:=True;

    fmPriceType.ShowModal;
    taPriceT.Active:=False;
  end;
end;

procedure TfmMainMStroy.acMHPExecute(Sender: TObject);
begin
  fmMH.TreeMH.Items.Clear;
  ClassifEx(nil,fmMH.TreeMH,dmO.quMHTree,0,'NAMEMH');
  fmMH.ShowModal;
end;

procedure TfmMainMStroy.acClientsExecute(Sender: TObject);
begin
//�����������
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    fmClients.Show;
  end;
end;

procedure TfmMainMStroy.acMoveExecute(Sender: TObject);
begin
//
end;

procedure TfmMainMStroy.acMoveDateExecute(Sender: TObject);
begin
//�������� �� ������ �� ������
end;

procedure TfmMainMStroy.acTovRepExecute(Sender: TObject);
begin
 //�������� �����
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    quMHAll1.First;
    if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainMStroy.acRepPribExecute(Sender: TObject);
begin
  //����� �� �������
  if not CanDo('prRepPrib') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl2:=tfmSelPerSkl2.Create(Application);
  with fmSelPerSkl2 do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False; quMHAll1.Active:=True; quMHAll1.First;
    if Prib.MHId>0 then cxLookupComboBox1.EditValue:=Prib.MHId else cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    CheckBox1.Checked:=Prib.MHAll;
    CheckBox11.Checked:=Prib.MHUnion;

    quMProd.Active:=False; quMProd.Active:=True;  quMProd.First;
    if Prib.MProd>0 then cxLookupComboBox2.EditValue:=Prib.MProd else cxLookupComboBox2.EditValue:=quMProdID.AsInteger;
    CheckBox2.Checked:=Prib.MProdAll;
    CheckBox22.Checked:=Prib.MProdUnion;

    quCateg.Active:=False; quCateg.Active:=True; quCateg.First;
    if Prib.Cat>0 then cxLookupComboBox3.EditValue:=Prib.Cat else cxLookupComboBox3.EditValue:=quCategID.AsInteger;
    CheckBox3.Checked:=Prib.CatAll;
    CheckBox33.Checked:=Prib.CatUnion;

    quSalet.Active:=False; quSalet.Active:=True; quSalet.First;
    cxLookupComboBox4.EditValue:=quSaletSalet.AsInteger;
    CheckBox4.Checked:=Prib.SaletAll;
    CheckBox44.Checked:=Prib.SaletUnion;

    cxLookupComboBox5.EditValue:=Prib.Oper; 
    CheckBox5.Checked:=Prib.OperAll;
    CheckBox55.Checked:=Prib.OperUnion;
  end;
  fmSelPerSkl2.ShowModal;
  if fmSelPerSkl2.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl2.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl2.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl2.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl2.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

    fmRepPrib.teMH.Active:=False;
    fmRepPrib.teMH.Active:=True;

    fmSelPerSkl2.quMHAll1.First;
    while not fmSelPerSkl2.quMHAll1.Eof do
    begin
      fmRepPrib.teMH.Append;
      fmRepPrib.teMHID.AsInteger:=fmSelPerSkl2.quMHAll1ID.AsInteger;
      fmRepPrib.teMHNAMEMH.AsString:=fmSelPerSkl2.quMHAll1NAMEMH.AsString;
      fmRepPrib.teMH.Post;

      fmSelPerSkl2.quMHAll1.Next;
    end;

    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;


    fmRepPrib.Show;  delay(10);//}
    fmRepPrib.prFormPrib;
  end else
  begin
    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;
  end;
end;

procedure TfmMainMStroy.acObVedExecute(Sender: TObject);
type TRSum = record
     qRemn,qIn,qRet,qVnIn,qVnOut,qOut,qInv,qInvIn,qInvOut,qRes:Real;
     sRemn,sIn,sRet,sVnIn,sVnOut,sOut,sInv,sInvIn,sInvOut,sRes:Real;
     end;

Var  bAdd:Boolean;
     iCount:Integer;
     arSum:TRSum;
     rQ,rQRec,rPr,rPrU:Real;
     S1,S2:String;

begin
//��������� ���������
  if not CanDo('prRepObVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    with dmO do
    with dmORep do
    begin
      fmRepOb.Show;  delay(10);//}
      with fmRepOb do
      begin
        Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
        ViewObVed.BeginUpdate;

        CloseTe(fmRepOb.teObVed);

        quAllCards.Active:=False;
        quAllCards.Active:=True;

        dsquAllCards.DataSet:=dmO.quAllCards;
        PBar1.Properties.Min:=0;
        PBar1.Properties.Max:=quAllCards.RecordCount;
        iCount:=0;
        PBar1.Position:=iCount;
        PBar1.Visible:=True;

        quAllCards.First;
        while not quAllCards.Eof do
        begin
          bAdd:=False;
          //� �������
          arSum.qRemn:=prCalcRemn(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore);

          quMovePer.Active:=False;
          quMovePer.ParamByName('IDGOOD').AsInteger:=quAllCardsID.AsInteger;
          quMovePer.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
          quMovePer.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
          quMovePer.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
          quMovePer.Active:=True;

          arSum.qIn:=round(quMovePerPOSTIN.AsFloat*1000)/1000;
          arSum.qRet :=round(quMovePerPOSTOUT.AsFloat*1000)/1000;
          arSum.qVnIn :=round(quMovePerVNIN.AsFloat*1000)/1000;
          arSum.qVnOut :=round(quMovePerVNOUT.AsFloat*1000)/1000;
          arSum.qOut:=round(quMovePerQREAL.AsFloat*1000)/1000;
          arSum.qInv:=round(quMovePerINV.AsFloat*1000)/1000;

          if abs(arSum.qRemn)>=0.001 then bAdd:=True;
          if abs(arSum.qIn)>=0.001 then bAdd:=True;
          if abs(arSum.qRet)>=0.001 then bAdd:=True;
          if abs(arSum.qVnIn)>=0.001 then bAdd:=True;
          if abs(arSum.qVnOut)>=0.001 then bAdd:=True;
          if abs(arSum.qOut)>=0.001 then bAdd:=True;
          if abs(arSum.qInv)>=0.001 then bAdd:=True;

          quMovePer.Active:=False;

          if bAdd then
          begin
            //������������ �� ��������� �������
            //��� � ��������
            arSum.sRemn:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore,arSum.qRemn);
            //��� � �������
            if quAllCardsKOEF.AsFloat<>0 then arSum.qRemn:=arSum.qRemn/quAllCardsKOEF.AsFloat; //�� �������� ����� � �������

            if quAllCardsKOEF.AsFloat<>0 then
            begin
             arSum.qIn:=arSum.qIn/quAllCardsKOEF.AsFloat;
             arSum.qRet:=arSum.qRet/quAllCardsKOEF.AsFloat;
             arSum.qVnIn:=arSum.qVnIn/quAllCardsKOEF.AsFloat;
             arSum.qVnOut:=arSum.qVnOut/quAllCardsKOEF.AsFloat;
             arSum.qOut:=arSum.qOut/quAllCardsKOEF.AsFloat;
             arSum.qInv:=arSum.qInv/quAllCardsKOEF.AsFloat;
            end;

            if arSum.qInv>=0 then begin arSum.qInvIn:=arSum.qInv; arSum.qInvOut:=0; end;
            if arSum.qInv<0 then begin arSum.qInvIn:=0; arSum.qInvOut:=(-1)*arSum.qInv; end;

            arSum.sIn:=0;arSum.sVnIn:=0;arSum.sInvIn:=0;
            arSum.sRet:=0;arSum.sVnOut:=0;arSum.sInvOut:=0;arSum.sOut:=0;
            rPr:=0; rPrU:=0;

            //������

            if arSum.qVnIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE<>1 and DTYPE<>3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qVnIn;
              arSum.sVnIn:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnIn:=arSum.sVnIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU);
                arSum.sVnIn:=arSum.sVnIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qInvIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qInvIn;
              arSum.sInvIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvIn:=arSum.sInvIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU);
                arSum.sInvIn:=arSum.sInvIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=1');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');
              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qIn;
              arSum.sIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sIn:=arSum.sIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU);
                arSum.sIn:=arSum.sIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;
              quSelInSum.Active:=False;
            end;




{
SELECT IDDATE,QUANT,PRICEIN,SUMOUT
FROM OF_PARTOUT
where IDDATE>=:DATEB and IDDATE<=:DATEE and IDSTORE=:IDSKL and ARTICUL=:IDGOOD and DTYPE=1
order by IDDATE desc
}

// ������

            if arSum.qRet>0 then  //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=7');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qRet; arSum.sRet:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sRet:=arSum.sRet+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sRet:=arSum.sRet+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qOut>0 then //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=2 or DTYPE=8)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qOut; arSum.sOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sOut:=arSum.sOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sOut:=arSum.sOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qInvOut>0 then //��������������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qInvOut; arSum.sInvOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvOut:=arSum.sInvOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sInvOut:=arSum.sInvOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qVnOut>0 then //���������� ������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=4 or DTYPE=5 or DTYPE=6)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qVnOut; arSum.sVnOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnOut:=arSum.sVnOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sVnOut:=arSum.sVnOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            arSum.QRes:=arSum.qRemn+(arSum.qIn+arSum.qVnIn+arSum.qInvIn)-(arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut);
            //����� ��� � ������ ��������

            arSum.SRes:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateTo-1),CommonSet.IdStore,(arSum.QRes*quAllCardsKOEF.AsFloat));

            prFind2group(quAllCardsPARENT.AsInteger,S1,S2);

            teObVed.Append;
            teObVedIdCode.AsInteger:=quAllCardsID.AsInteger;
            teObVedNameC.AsString:=quAllCardsNAME.AsString;
            teObVediM.AsInteger:=quAllCardsIMESSURE.AsInteger;
            teObVedSm.AsString:=quAllCardsNAMESHORT.AsString;
            teObVedIdGroup.AsInteger:=quAllCardsPARENT.AsInteger;
            teObVedNameGr1.AsString:=S1;
            teObVedNameGr2.AsString:=S2;
            teObVedQBeg.AsFloat:=arSum.qRemn;
            teObVedSBeg.AsFloat:=arSum.sRemn;
            teObVedQIn.AsFloat:=arSum.qIn+arSum.qVnIn+arSum.qInvIn;
            teObVedSIn.AsFloat:=arSum.sIn+arSum.sVnIn+arSum.sInvIn;
            teObVedQOut.AsFloat:=arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut;
            teObVedSOut.AsFloat:=arSum.sRet+arSum.sVnOut+arSum.sOut+arSum.sInvOut;
            teObVedQRes.AsFloat:=arSum.QRes;
            teObVedSRes.AsFloat:=arSum.SRes;
            teObVedPrice.AsFloat:=rPr;
            teObVedKm.AsFloat:=quAllCardsKOEF.AsFloat;
            teObVed.Post;

          end;

          quAllCards.Next;
          inc(iCount);
          PBar1.Position:=iCount; delay(10);
        end;

        dsquAllCards.DataSet:=Nil;
        PBar1.Visible:=False;

        ViewObVed.EndUpdate;
      end;
      fmRepOb.Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

//      fmRepOb.Memo1.Lines.Add('  ������� �� ������.'); delay(10);

{      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom-1); //-1 �.�. ���� ������
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateFrom-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quRemnDate.Active:=False;
      quRemnDate.Active:=True;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      fmRepOb.ViewObVed.BeginUpdate;
//      fmRepOb.taObVed.Active:=False;
      CloseTa(fmRepOb.taObVed);

      iCount:=0;
      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
        with fmRepOb do
        begin
          bAdd:=False;
//          if quRemnDateTCARD.AsInteger=1 then  //��� �����
//          begin
          if abs(quRemnDateREMN.AsFloat)>0.0001 then bAdd:=True; //�� ������� ���� !!!
//          end
//          else bAdd:=True;

          if bAdd then
          begin
            //��� ��� ������� ����� ������� - ����� ���� �� ������ ���������
            //Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;
            //MOV.REMN/ME.KOEF
            rSum:=prCalcRemnSum(quRemnDateID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore);

            taObVed.Append;
            taObVedIdCode.AsInteger:=quRemnDateID.AsInteger;
            taObVedNameC.AsString:=quRemnDateNAME.AsString;
            taObVediM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            taObVedSm.AsString:=quRemnDateNAMEMESSURE.AsString;
            taObVedIdGroup.AsInteger:=quRemnDatePARENT.AsInteger;
            taObVedNameClass.AsString:=quRemnDateNAMECL.AsString;
            taObVedQBeg.AsFloat:=quRemnDateREMN.AsFloat;
            taObVedSBeg.AsFloat:=rSum;
            taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=0;
            taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=0;
            taObVedPrice.AsFloat:=0;
            taObVedKm.AsFloat:=quRemnDateKOEF.AsFloat;
            taObVed.Post;
          end;
        end;
        quRemnDate.Next; delay(10); inc(iCount);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;

      quRemnDate.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelInSum.Active:=False;
      quSelInSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelInSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelInSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelInSum.Active:=True;

      iCount:=0;

      quSelInSum.First;
      while not quSelInSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelInSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm
            else taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelInSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelInSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;

              if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm;
              taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;

              taObVedQOut.AsFloat:=0;
              taObVedSOut.AsFloat:=0;
              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelInSum.Next;  inc(iCount); delay(10);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;
      quSelInSum.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelOutSum.Active:=False;
      quSelOutSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelOutSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelOutSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelOutSum.Active:=True;

      iCount:=0;

      quSelOutSum.First;
      while not quSelOutSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelOutSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm
            else taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=quSelOutSumSUMOUT.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelOutSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelOutSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;
              taObVedSIn.AsFloat:=0;

              taObVedQOut.AsFloat:=0;
              if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm;
              taObVedSOut.AsFloat:=quSelOutSumSUMOut.AsFloat;

              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelOutSum.Next; inc(iCount); delay(10);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;
      quSelOutSum.Active:=False;

      fmRepOb.ViewObVed.EndUpdate;
      }



procedure TfmMainMStroy.acInDocsExecute(Sender: TObject);
begin
  //���������� ���������
end;

procedure TfmMainMStroy.acActsExecute(Sender: TObject);
begin
//���� �����������
end;

procedure TfmMainMStroy.amOperTExecute(Sender: TObject);
begin
  //��������
  //���� ���
  with dmORep do
  begin
    quOperT.Active:=False;
    quOperT.Active:=True;

    fmOperType:=TfmOperType.Create(Application);
    fmOperType.ShowModal;
    fmOperType.Release;
    quOperT.Active:=False;
  end;
end;

procedure TfmMainMStroy.acRealExecute(Sender: TObject);
begin
  (*fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.ShowModal;

  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;*)

  fmDocsOut.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with fmDocsOut do
  with dmO do
  begin
    ViewDocsOut.BeginUpdate;
    try
      quDH.Active:=False;
      quDH.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDH.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDH.Active:=True;
    finally
      ViewDocsOut.EndUpdate;
    end;
  end;
  (*end;*)

  fmDocsOut.Show;
end;

procedure TfmMainMStroy.acRecalcPerExecute(Sender: TObject);
begin
//������������ ������
end;

procedure TfmMainMStroy.acRepPostExecute(Sender: TObject);
begin
  //������� �� �����������
end;

procedure TfmMainMStroy.Action1Execute(Sender: TObject);
begin
  //
  fmClassSel:=TfmClassSel.Create(Application);
  fmClassSel.ShowModal;
  fmClassSel.Release;
end;

procedure TfmMainMStroy.acPartRemnExecute(Sender: TObject);
begin
//������������ ������
end;

procedure TfmMainMStroy.acAvansExecute(Sender: TObject);
begin
  //����� �� ������� � �������
end;

procedure TfmMainMStroy.TimerCloseTimer(Sender: TObject);
begin
  Close;
end;

procedure TfmMainMStroy.acBGUExecute(Sender: TObject);
begin
  //����� ���� �������� - ����������
end;

procedure TfmMainMStroy.acTermoObrExecute(Sender: TObject);
begin
  //���� ���������
end;

procedure TfmMainMStroy.N1Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
//    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.Main:=dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmMainMStroy.acCategRExecute(Sender: TObject);
begin
//��������� ���� � �������
  //���� ���������
  fmRCategory.ViewCateg.BeginUpdate;
  with dmO do
  begin
    quCateg.Active:=False;
    quCateg.Active:=True;
  end;
  fmRCategory.ViewCateg.EndUpdate;
  fmRCategory.Show;
end;

procedure TfmMainMStroy.acRemnSpeedRealExecute(Sender: TObject);
begin
  //������� �������� �������
end;

procedure TfmMainMStroy.acCliExecute(Sender: TObject);
begin
  fmBuyers.Show;
end;

end.
