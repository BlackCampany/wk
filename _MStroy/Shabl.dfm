object fmBuff: TfmBuff
  Left = 367
  Top = 245
  BorderStyle = bsDialog
  Caption = #1057#1087#1080#1089#1086#1082' '#1096#1072#1073#1083#1086#1085#1086#1074
  ClientHeight = 463
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GridTH: TcxGrid
    Left = 8
    Top = 8
    Width = 497
    Height = 433
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewTH: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dstaShablH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewTHIDH: TcxGridDBColumn
        Caption = #8470' '
        DataBinding.FieldName = 'IDH'
        Width = 25
      end
      object ViewTHNAMESH: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMESH'
        Width = 176
      end
      object ViewTHPREDOPLATA: TcxGridDBColumn
        Caption = #1055#1088#1077#1076#1086#1087#1083#1072#1090#1072
        DataBinding.FieldName = 'PREDOPLATA'
      end
      object ViewTHIZGOTOVLENIE: TcxGridDBColumn
        Caption = #1048#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'IZGOTOVLENIE'
      end
      object ViewTHWARRANTY: TcxGridDBColumn
        Caption = #1043#1072#1088#1072#1085#1090#1080#1103
        DataBinding.FieldName = 'WARRANTY'
        Width = 124
      end
    end
    object ViewTS: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dstaShablS
      DataController.DetailKeyFieldNames = 'IDH'
      DataController.KeyFieldNames = 'NUM'
      DataController.MasterKeyFieldNames = 'IDH'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewTSNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Width = 50
      end
      object ViewTSCODE: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'CODE'
        Width = 50
      end
      object ViewTSNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 150
      end
      object ViewTSQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        Width = 50
      end
      object ViewTSPRICEOUT: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICEOUT'
        Width = 50
      end
      object ViewTSRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUM'
        Width = 50
      end
    end
    object ViewDSpec: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsSpecDoc
      DataController.DetailKeyFieldNames = 'IType;IdHead'
      DataController.KeyFieldNames = 'IType;IdHead'
      DataController.MasterKeyFieldNames = 'IType;Id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewDSpecIType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IType'
        Visible = False
      end
      object ViewDSpecIdHead: TcxGridDBColumn
        Caption = #1042#1085'.'#1082#1086#1076' '#1076#1086#1082'.'
        DataBinding.FieldName = 'IdHead'
        Visible = False
      end
      object ViewDSpecNum: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087'.'
        DataBinding.FieldName = 'Num'
        Width = 40
      end
      object ViewDSpecNameC: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameC'
        Width = 120
      end
      object ViewDSpecIdCard: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IdCard'
        Width = 40
      end
      object ViewDSpecQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
      end
      object ViewDSpecPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewDSpecSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
        DataBinding.FieldName = 'SumIn'
      end
      object ViewDSpecPriceUch: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1091#1095'.'
        DataBinding.FieldName = 'PriceUch'
      end
      object ViewDSpecSumUch: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1091#1095'.'
        DataBinding.FieldName = 'SumUch'
      end
      object ViewDSpecIdNds: TcxGridDBColumn
        DataBinding.FieldName = 'IdNds'
        Visible = False
      end
      object ViewDSpecSumNds: TcxGridDBColumn
        DataBinding.FieldName = 'SumNds'
        Visible = False
      end
      object ViewDSpecSm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'Sm'
      end
      object ViewDSpecIdM: TcxGridDBColumn
        DataBinding.FieldName = 'IdM'
        Visible = False
      end
    end
    object LevelTH: TcxGridLevel
      GridView = ViewTH
      object LevelTS: TcxGridLevel
        GridView = ViewTS
      end
    end
  end
  object cxButton1: TcxButton
    Left = 520
    Top = 16
    Width = 81
    Height = 25
    Caption = #1042#1089#1090#1072#1074#1080#1090#1100
    ModalResult = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 520
    Top = 56
    Width = 81
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    ModalResult = 2
    TabOrder = 2
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 520
    Top = 168
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 520
    Top = 136
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 4
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 444
    Width = 611
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
end
