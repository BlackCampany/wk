unit AddM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxGraphics,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxMaskEdit, cxCalc, cxSpinEdit, cxCurrencyEdit, dxfBackGround,
  cxButtonEdit, ActnList, XPStyleActnCtrls, ActnMan, cxCheckBox,
  cxGroupBox, cxRadioGroup, cxCalendar, cxTimeEdit, cxCheckComboBox;

type
  TfmAddM = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    Label9: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    cxSpinEdit1: TcxSpinEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    dxfBackGround1: TdxfBackGround;
    cxButton3: TcxButton;
    cxTextEdit2: TcxButtonEdit;
    amAddM: TActionManager;
    acExit: TAction;
    cxLookupComboBox4: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    Label10: TLabel;
    Label11: TLabel;
    cxCalcEdit3: TcxCalcEdit;
    Label12: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label13: TLabel;
    cxLookupComboBox5: TcxLookupComboBox;
    cxCheckBox2: TcxCheckBox;
    Label14: TLabel;
    Panel2: TPanel;
    Label15: TLabel;
    cxDateEdit1: TcxDateEdit;
    Label16: TLabel;
    cxDateEdit2: TcxDateEdit;
    Label17: TLabel;
    Label18: TLabel;
    cxTimeEdit1: TcxTimeEdit;
    Label19: TLabel;
    cxTimeEdit2: TcxTimeEdit;
    cxCheckComboBox1: TcxCheckComboBox;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxTextEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acExitExecute(Sender: TObject);
    procedure cxCheckBox2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddM: TfmAddM;

implementation

uses dmRnEdit, Un1;

{$R *.dfm}

procedure TfmAddM.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmAddM.cxButton3Click(Sender: TObject);
begin
  cxLookupComboBox2.EditValue:=Null;
  cxSpinEdit1.EditValue:=0;
end;

procedure TfmAddM.cxTextEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var sBar:String;
    iCode:Integer;
begin
  //���������� ��
  with dmC do
  begin
    prGetId.ParamByName('ITYPE').Value:=8;  //��� ����� � ��
    prGetId.ExecProc;
    iCode:=prGetId.ParamByName('RESULT').Value;
    sBar:=IntToStr(iCode);
    while length(sBar)<10 do sBar:='0'+sBar;
    sBar:='25'+sBar;
    sBar:=ToStandart(sBar);

    cxTextEdit2.Text:=sBar;
  end;
end;

procedure TfmAddM.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmAddM.cxCheckBox2PropertiesChange(Sender: TObject);
begin
  if cxCheckBox2.Checked then
  begin
    Panel2.Enabled:=False;
    if cxCheckBox2.Tag=0 then
    begin
      cxDateEdit1.EditValue:=null;
      cxDateEdit2.EditValue:=null;

      cxCheckComboBox1.States[0]:=cbsUnChecked;
      cxCheckComboBox1.States[1]:=cbsUnChecked;
      cxCheckComboBox1.States[2]:=cbsUnChecked;
      cxCheckComboBox1.States[3]:=cbsUnChecked;
      cxCheckComboBox1.States[4]:=cbsUnChecked;
      cxCheckComboBox1.States[5]:=cbsUnChecked;
      cxCheckComboBox1.States[6]:=cbsUnChecked;

      cxTimeEdit1.EditValue:=null;
      cxTimeEdit2.EditValue:=null;
    end;
  end
  else
  begin
    Panel2.Enabled:=True;
    if cxCheckBox2.Tag=0 then
    begin
      cxDateEdit1.Date:=Date;
      cxDateEdit2.Date:=Date+30;

      cxCheckComboBox1.States[0]:=cbsChecked;
      cxCheckComboBox1.States[1]:=cbsChecked;
      cxCheckComboBox1.States[2]:=cbsChecked;
      cxCheckComboBox1.States[3]:=cbsChecked;
      cxCheckComboBox1.States[4]:=cbsChecked;
      cxCheckComboBox1.States[5]:=cbsChecked;
      cxCheckComboBox1.States[6]:=cbsChecked;

      cxTimeEdit1.Time:=0;
      cxTimeEdit2.Time:=0.999999;
    end;
  end;
end;

end.
