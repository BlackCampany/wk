object fmMH: TfmMH
  Left = 306
  Top = 186
  Width = 477
  Height = 393
  Caption = #1052#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 340
    Width = 469
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 469
    Height = 27
    UseSystemFont = False
    ActionManager = amMH
    Caption = 'ActionMainMenuBar1'
    ColorMap = XPColorMap1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object GridMH: TcxGrid
    Left = 184
    Top = 40
    Width = 273
    Height = 281
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewMH: TcxGridDBTableView
      DragMode = dmAutomatic
      PopupMenu = PopupMenu1
      OnStartDrag = ViewMHStartDrag
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsMHSel
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewMHID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 40
      end
      object ViewMHNAMEMH: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMEMH'
        Width = 115
      end
      object ViewMHNAMEPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1072
        DataBinding.FieldName = 'NAMEPRICE'
        Width = 116
      end
    end
    object LevelMH: TcxGridLevel
      GridView = ViewMH
    end
  end
  object TreeMH: TcxTreeView
    Left = 0
    Top = 27
    Width = 177
    Height = 313
    Align = alLeft
    PopupMenu = PopupMenu2
    TabOrder = 3
    OnDragDrop = TreeMHDragDrop
    OnDragOver = TreeMHDragOver
    Images = dmO.imState
    ReadOnly = True
    OnChange = TreeMHChange
    OnExpanded = TreeMHExpanded
  end
  object amMH: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = acExit
            ImageIndex = 10
          end
          item
            Items = <
              item
                Items = <
                  item
                    Action = acGroupAdd
                    ImageIndex = 28
                  end
                  item
                    Action = scSGroupAdd
                    ImageIndex = 28
                  end
                  item
                    Action = acGroupEdit
                    ImageIndex = 27
                  end
                  item
                    Caption = '-'
                  end
                  item
                    Action = acGroupDel
                    ImageIndex = 29
                  end>
                Action = acGroup
                ImageIndex = 9
              end
              item
                Items = <
                  item
                    Action = acMHAdd
                    ImageIndex = 32
                  end
                  item
                    Action = acMHEdit
                    ImageIndex = 30
                  end
                  item
                    Caption = '-'
                  end
                  item
                    Action = acMHDel
                    ImageIndex = 31
                  end>
                Action = acMH
                ImageIndex = 11
              end>
            Action = acD
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Images = dmO.imState
    Left = 384
    Top = 176
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 10
      OnExecute = acExitExecute
    end
    object acD: TAction
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      OnExecute = acDExecute
    end
    object acGroup: TAction
      Caption = #1043#1088#1091#1087#1087#1099' '#1052#1061
      ImageIndex = 9
      OnExecute = acGroupExecute
    end
    object acMH: TAction
      Caption = #1052#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
      ImageIndex = 11
      OnExecute = acMHExecute
    end
    object acGroupAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 28
      OnExecute = acGroupAddExecute
    end
    object scSGroupAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1075#1088#1091#1087#1087#1091
      ImageIndex = 28
      OnExecute = scSGroupAddExecute
    end
    object acGroupEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 27
      OnExecute = acGroupEditExecute
    end
    object acGroupDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 29
      OnExecute = acGroupDelExecute
    end
    object acMHAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 32
      OnExecute = acMHAddExecute
    end
    object acMHEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 30
      OnExecute = acMHEditExecute
    end
    object acMHDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 31
      OnExecute = acMHDelExecute
    end
  end
  object XPColorMap1: TXPColorMap
    HighlightColor = 15660791
    BtnSelectedColor = clBtnFace
    UnusedColor = 15660791
    Left = 384
    Top = 120
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 248
    Top = 72
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 256
    Top = 128
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 280
    Top = 208
    object N6: TMenuItem
      Action = acMHAdd
    end
    object N7: TMenuItem
      Action = acMHEdit
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Action = acMHDel
    end
  end
  object PopupMenu2: TPopupMenu
    Images = dmO.imState
    Left = 56
    Top = 176
    object N1: TMenuItem
      Action = acGroupAdd
    end
    object N2: TMenuItem
      Action = scSGroupAdd
    end
    object N3: TMenuItem
      Action = acGroupEdit
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = acGroupDel
    end
  end
end
