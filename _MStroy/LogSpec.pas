unit LogSpec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, ExtCtrls, SpeedBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxDataStorage, StdCtrls, ActnList,
  XPStyleActnCtrls, ActnMan, FR_Class, FR_DSet, FR_DBSet;

type
  TfmLogSpec = class(TForm)
    StatusBar1: TStatusBar;
    ViewLog: TcxGridDBTableView;
    LevelLog: TcxGridLevel;
    GridLog: TcxGrid;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    ViewLogDATEOP: TcxGridDBColumn;
    ViewLogID_PERSONAL: TcxGridDBColumn;
    ViewLogNAMEOP: TcxGridDBColumn;
    ViewLogCONTENT: TcxGridDBColumn;
    ViewLogNAMEP: TcxGridDBColumn;
    ViewLogOPERNAME: TcxGridDBColumn;
    ViewLogCONTNAME: TcxGridDBColumn;
    ActionManager1: TActionManager;
    acPrint: TAction;
    SpeedItem2: TSpeedItem;
    frRepLog: TfrReport;
    dsRep6: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLogSpec: TfmLogSpec;

implementation

uses Un1, Dm, MainRep;

{$R *.dfm}

procedure TfmLogSpec.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewLog.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmLogSpec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taSpecAllSel.Active:=False;
  ViewLog.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmLogSpec.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmLogSpec.acPrintExecute(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  //������
{  StrWk:=ViewLog.DataController.Filter.FilterText;}

  ViewLog.BeginUpdate;
  with dmC do
  begin
    quRepLog.Filter:=ViewLog.DataController.Filter.FilterText;
    quRepLog.Filtered:=True;
    frRepLog.LoadFromFile(CurDir + 'PersLog.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateTo);
    StrWk:=ViewLog.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRepLog.ReportName:='�������� �������� ���������.';
    frRepLog.PrepareReport;
    frRepLog.ShowPreparedReport;

    quRepLog.Filter:='';
    quRepLog.Filtered:=False;
  end;
  ViewLog.EndUpdate;
end;

procedure TfmLogSpec.Button1Click(Sender: TObject);
begin
  showmessage(ViewLog.DataController.Filter.FilterText);
end;

end.
