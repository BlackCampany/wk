unit Shabl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxCalendar, cxImageComboBox, ComCtrls;

type
  TfmBuff = class(TForm)
    ViewTH: TcxGridDBTableView;
    LevelTH: TcxGridLevel;
    GridTH: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    LevelTS: TcxGridLevel;
    ViewTS: TcxGridDBTableView;
    ViewDSpec: TcxGridDBTableView;
    ViewDSpecIType: TcxGridDBColumn;
    ViewDSpecIdHead: TcxGridDBColumn;
    ViewDSpecNum: TcxGridDBColumn;
    ViewDSpecIdCard: TcxGridDBColumn;
    ViewDSpecQuant: TcxGridDBColumn;
    ViewDSpecPriceIn: TcxGridDBColumn;
    ViewDSpecSumIn: TcxGridDBColumn;
    ViewDSpecPriceUch: TcxGridDBColumn;
    ViewDSpecSumUch: TcxGridDBColumn;
    ViewDSpecIdNds: TcxGridDBColumn;
    ViewDSpecSumNds: TcxGridDBColumn;
    ViewDSpecNameC: TcxGridDBColumn;
    ViewDSpecSm: TcxGridDBColumn;
    ViewDSpecIdM: TcxGridDBColumn;
    ViewTHIDH: TcxGridDBColumn;
    ViewTHNAMESH: TcxGridDBColumn;
    ViewTHPREDOPLATA: TcxGridDBColumn;
    ViewTHIZGOTOVLENIE: TcxGridDBColumn;
    ViewTHWARRANTY: TcxGridDBColumn;
    ViewTSNUM: TcxGridDBColumn;
    ViewTSCODE: TcxGridDBColumn;
    ViewTSNAME: TcxGridDBColumn;
    ViewTSQUANT: TcxGridDBColumn;
    ViewTSPRICEOUT: TcxGridDBColumn;
    ViewTSRSUM: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBuff: TfmBuff;

implementation

uses Un1, dmOffice;

{$R *.dfm}

procedure TfmBuff.cxButton4Click(Sender: TObject);
begin
  //�������
  if not CanDo('prDelShablon') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taShablH.RecordCount>0 then
    begin
      if MessageDlg('������� ������ - '+taShablHNAMESH.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          ViewTH.BeginUpdate;
          ViewTS.BeginUpdate;

          taShablH.Delete;

        finally
          ViewTS.EndUpdate;
          ViewTH.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmBuff.cxButton3Click(Sender: TObject);
begin
//��������
  if not CanDo('prDelShablon') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taShablH.RecordCount>0 then
    begin
      if MessageDlg('������� ��� ������� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          ViewTH.BeginUpdate;
          ViewTS.BeginUpdate;

          taShablH.First;
          while not taShablH.Eof do taShablH.Delete;

        finally
          ViewTS.EndUpdate;
          ViewTH.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmBuff.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
