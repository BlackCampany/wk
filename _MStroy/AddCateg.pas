unit AddCateg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfBackGround, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxDropDownEdit, cxCalc;

type
  TfmAddCateg = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddCateg: TfmAddCateg;

implementation

{$R *.dfm}

procedure TfmAddCateg.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
