unit TOSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  ExtCtrls, Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  ComCtrls, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxCalendar,
  cxCurrencyEdit, cxContainer, cxMemo, FR_Class;

type
  TfmTO = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    StatusBar1: TStatusBar;
    GridTO: TcxGrid;
    ViewTO: TcxGridDBTableView;
    LevelTO: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    amTO: TActionManager;
    acExit: TAction;
    acPerSkl: TAction;
    acExportEx: TAction;
    Memo1: TcxMemo;
    acCreate: TAction;
    SpeedItem4: TSpeedItem;
    acPrintTO: TAction;
    SpeedItem5: TSpeedItem;
    acTODelPer: TAction;
    SpeedItem6: TSpeedItem;
    ViewTOIDATE: TcxGridDBColumn;
    ViewTOREMNIN: TcxGridDBColumn;
    ViewTOREMNOUT: TcxGridDBColumn;
    ViewTOPOSTIN: TcxGridDBColumn;
    ViewTOPOSTOUT: TcxGridDBColumn;
    ViewTOVNIN: TcxGridDBColumn;
    ViewTOVNOUT: TcxGridDBColumn;
    ViewTOINV: TcxGridDBColumn;
    ViewTOQREAL: TcxGridDBColumn;
    ViewTONAMEMH: TcxGridDBColumn;
    ViewTOREMNINT: TcxGridDBColumn;
    ViewTOPOSTINT: TcxGridDBColumn;
    ViewTOPOSTOUTT: TcxGridDBColumn;
    ViewTOVNINT: TcxGridDBColumn;
    ViewTOVNOUTT: TcxGridDBColumn;
    ViewTOINVT: TcxGridDBColumn;
    ViewTOREMNOUTT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPerSklExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acCreateExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTO: TfmTO;

implementation

uses Un1, dmOffice, SelPerSkl, DMOReps, Message;

{$R *.dfm}

procedure TfmTO.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridTO.Align:=AlClient;
  ViewTO.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTO.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmTO.acExportExExecute(Sender: TObject);
begin
  // ������� � ������
  prNExportExel5(ViewTO);
end;

procedure TfmTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTO.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTO.acPerSklExecute(Sender: TObject);
begin
//������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmTO.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmTO.acCreateExecute(Sender: TObject);
Var iDateB,iDateE,IdSkl,iR,iD:INteger;
    rSumB:Real;
    rSumBT:Real;
    StrWk:String;
begin
  //������������ �� �� ������
  if not CanDo('prCreateTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    // ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=CommonSet.DateFrom;
      quMHAll1.Active:=False;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date);
      IdSkl:=fmSelPerSkl.cxLookupComboBox1.EditValue;

      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
      CommonSet.DateFrom:=iDateB;
      CommonSet.DateTo:=iDateE+1;

      if not CanEdit(iDateB) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

      WriteHistory('������������ �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE)+'. ��������: '+Person.Name);

      if iDateE>=iMaxDate then  iDateE:=Trunc(Date+1);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      //�������
      Memo1.Clear;
      prWH('������ �������.  ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE),Memo1); Delay(10);

      for iD:=iDateB to iDateE do
      begin
        //������� �� ������
        prWH('  ������� �� '+FormatDateTime('dd.mm.yyyy',iD),Memo1); Delay(10);
        iR:=prTOFindRemB(iD,IdSkl,rSumB,rSumBT);

        if iR<>0 then
          if MessageDlg('������� �� ������ �� ���������. ���������� ������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then exit;

        Str(rSumB:12:2,StrWk);
        prWH('    ������� �� ������: '+StrWk,Memo1); Delay(10);

        //�������� �� ��������
        iR:=prTOFind(iD,IdSkl);
        if iR<>0 then //���� ������� ���������
        begin
          StrWk:='    �� �� '+FormatDateTime('dd.mm.yyyy',iD)+' ��� ����. ������� ��� �� � '+FormatDateTime('dd.mm.yyyy',iD)+'?';
          if MessageDlg(StrWk,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            if not CanDo('prDelTO') then
            begin
              prWH('��� ���� �� �������� ��. ������ �� ��������.',Memo1); Delay(10);
              exit;
            end;
            //��������
            prWH('    ���� �������� �� � '+FormatDateTime('dd.mm.yyyy',iD),Memo1); Delay(10);
            prTODel(iD,IdSkl);
            prWH('    �������� ��.',Memo1); Delay(10);
          end else
          begin
            prWH('������ �� ��������.',Memo1); Delay(10);
            exit;
          end;
        end;

        //������
        prWH('    ������.',Memo1); Delay(10);
        //������
        prWH('    �������.',Memo1); Delay(10);
        //�� ������
        prWH('    ��. ������.',Memo1); Delay(10);
        //�� ������
        prWH('    ��. ������.',Memo1); Delay(10);
        //����������
        prWH('    ����������.',Memo1); Delay(10);

        //��������������
        prWH('    ��������������.',Memo1); Delay(10);

      end;
      prWH('������ ��������.',Memo1); Delay(10);

    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmTO.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

end.
