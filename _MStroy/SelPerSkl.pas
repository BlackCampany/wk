unit SelPerSkl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox,
  dxfProgressBar, cxRadioGroup;

type
  TfmSelPerSkl = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxCheckBox1: TcxCheckBox;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl: TfmSelPerSkl;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmSelPerSkl.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl.FormCreate(Sender: TObject);
begin
  cxDateEdit2.Date:=iMaxDate;
end;

procedure TfmSelPerSkl.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxDateEdit2.Date:=Trunc(Date);
    cxDateEdit2.Visible:=True;
  end else
  begin
    cxDateEdit2.Date:=iMaxDate;
    cxDateEdit2.Visible:=False;
  end;
end;

procedure TfmSelPerSkl.cxButton1Click(Sender: TObject);
begin
  Cursor:=crHourGlass; Delay(10);
end;

end.
