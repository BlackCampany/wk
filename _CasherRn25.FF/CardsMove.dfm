object fmCardsMove: TfmCardsMove
  Left = 249
  Top = 217
  Width = 703
  Height = 412
  Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 152
    Top = 232
    Width = 137
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object PageControl1: TPageControl
    Left = 80
    Top = 8
    Width = 593
    Height = 289
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1055#1088#1080#1093#1086#1076' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      object GridPartIn: TcxGrid
        Left = 0
        Top = 0
        Width = 585
        Height = 261
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewPartIn: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquCPartIn
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QPART'
              Column = ViewPartInQPART
            end
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QREMN'
              Column = ViewPartInQREMN
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMIN'
              Column = ViewPartInSUMIN
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMREMN'
              Column = ViewPartInSUMREMN
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QPART'
              Column = ViewPartInQPART
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QREMN'
              Column = ViewPartInQREMN
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMIN'
              Column = ViewPartInSUMIN
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMREMN'
              Column = ViewPartInSUMREMN
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewPartInARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
            Width = 48
          end
          object ViewPartInID: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'ID'
          end
          object ViewPartInNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 150
          end
          object ViewPartInIDDOC: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'IDDOC'
            Visible = False
          end
          object ViewPartInNUMDOC: TcxGridDBColumn
            Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'NUMDOC'
          end
          object ViewPartInIDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewPartInIDCLI: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
            DataBinding.FieldName = 'IDCLI'
            Visible = False
          end
          object ViewPartInNAMECL: TcxGridDBColumn
            Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            DataBinding.FieldName = 'NAMECL'
            Width = 149
          end
          object ViewPartInDTYPE: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'DTYPE'
          end
          object ViewPartInQPART: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'QPART'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInQREMN: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'QREMN'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInPRICEIN: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PRICEIN'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInPRICEOUT: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'PRICEOUT'
          end
          object ViewPartInSUMIN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SUMIN'
            Styles.Content = dmO.cxStyle15
          end
          object ViewPartInSUMREMN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1072' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093
            DataBinding.FieldName = 'SUMREMN'
            Styles.Content = dmO.cxStyle15
          end
        end
        object LevelPartIn: TcxGridLevel
          GridView = ViewPartIn
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1072#1089#1093#1086#1076' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      ImageIndex = 1
      object GridPartOut: TcxGrid
        Left = 0
        Top = 0
        Width = 585
        Height = 261
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewPartOut: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquCPartOut
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QUANT'
              Column = ViewPartOutQUANT
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMOUT'
              Column = ViewPartOutSUMOUT
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QUANT'
              Column = ViewPartOutQUANT
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMOUT'
              Column = ViewPartOutSUMOUT
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewPartOutARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
          end
          object ViewPartOutIDDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewPartOutIDSTORE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1052#1061
            DataBinding.FieldName = 'IDSTORE'
            Visible = False
          end
          object ViewPartOutNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 103
          end
          object ViewPartOutIDPARTIN: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1080#1093#1086#1076#1085#1086#1081' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'IDPARTIN'
          end
          object ViewPartOutIDDOC: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'IDDOC'
            Visible = False
          end
          object ViewPartOutNUMDOC: TcxGridDBColumn
            Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'NUMDOC'
          end
          object ViewPartOutIDCLI: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
            DataBinding.FieldName = 'IDCLI'
            Visible = False
          end
          object ViewPartOutNAMECL: TcxGridDBColumn
            Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            DataBinding.FieldName = 'NAMECL'
            Width = 148
          end
          object ViewPartOutDTYPE: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'DTYPE'
          end
          object ViewPartOutQUANT: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANT'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartOutPRICEIN: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PRICEIN'
          end
          object ViewPartOutSUMOUT: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SUMOUT'
            Styles.Content = dmO.cxStyle25
          end
        end
        object LevelPartOut: TcxGridLevel
          GridView = ViewPartOut
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077
      ImageIndex = 2
      object GridMove: TcxGrid
        Left = 0
        Top = 0
        Width = 585
        Height = 261
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewMove: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmORep.dstaCMove
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'POSTIN'
              Column = ViewMovePOSTIN
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'POSTOUT'
              Column = ViewMovePOSTOUT
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'VNOUT'
              Column = ViewMoveVNIN
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'VNOUT'
              Column = ViewMoveVNOUT
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'INV'
              Column = ViewMoveINV
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QREAL'
              Column = ViewMoveQREAL
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewMoveARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
          end
          object ViewMoveIDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewMoveIDSTORE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1052#1061
            DataBinding.FieldName = 'IDSTORE'
            Visible = False
          end
          object ViewMoveNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 146
          end
          object ViewMoveRB: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095#1072#1083#1086
            DataBinding.FieldName = 'RB'
          end
          object ViewMovePOSTIN: TcxGridDBColumn
            Caption = #1055#1088#1080#1093#1086#1076
            DataBinding.FieldName = 'POSTIN'
          end
          object ViewMovePOSTOUT: TcxGridDBColumn
            Caption = #1042#1086#1079#1074#1088#1072#1090
            DataBinding.FieldName = 'POSTOUT'
          end
          object ViewMoveVNIN: TcxGridDBColumn
            Caption = #1042#1085'. '#1087#1088#1080#1093#1086#1076
            DataBinding.FieldName = 'VNIN'
          end
          object ViewMoveVNOUT: TcxGridDBColumn
            Caption = #1042#1085'. '#1088#1072#1089#1093#1086#1076
            DataBinding.FieldName = 'VNOUT'
          end
          object ViewMoveINV: TcxGridDBColumn
            Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
            DataBinding.FieldName = 'INV'
          end
          object ViewMoveQREAL: TcxGridDBColumn
            Caption = #1056#1072#1089#1093#1086#1076
            DataBinding.FieldName = 'QREAL'
          end
          object ViewMoveRE: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094
            DataBinding.FieldName = 'RE'
            Styles.Content = dmO.cxStyle25
          end
        end
        object LevelMove: TcxGridLevel
          GridView = ViewMove
        end
      end
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 66
    Height = 378
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Align = alLeft
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 60
    BtnHeight = 40
    Color = 11206570
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acPeriod
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1080#1086#1076
      Spacing = 1
      Left = 3
      Top = 11
      Visible = True
      OnClick = acPeriodExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 3
      Top = 155
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
  end
  object amCM: TActionManager
    Left = 348
    Top = 120
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      OnExecute = acExitExecute
    end
    object acPeriod: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      OnExecute = acPeriodExecute
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 480
    Top = 304
  end
end
