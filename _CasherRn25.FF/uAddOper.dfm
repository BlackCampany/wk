object fmAddOper: TfmAddOper
  Left = 349
  Top = 233
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
  ClientHeight = 339
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 50
    Height = 13
    Caption = #1054#1087#1077#1088#1072#1094#1080#1103
  end
  object Label2: TLabel
    Left = 16
    Top = 120
    Width = 66
    Height = 13
    Caption = #1050#1086#1076' '#1082#1083#1072#1074#1080#1096#1080
  end
  object Label3: TLabel
    Left = 16
    Top = 80
    Width = 42
    Height = 13
    Caption = #1057#1080#1084#1074#1086#1083' '
  end
  object Label4: TLabel
    Left = 16
    Top = 184
    Width = 103
    Height = 13
    Caption = #1056#1077#1078#1080#1084' '#1087#1088#1080#1084#1077#1085#1077#1085#1080#1103' '
  end
  object Bevel1: TBevel
    Left = 8
    Top = 48
    Width = 345
    Height = 9
  end
  object Bevel2: TBevel
    Left = 8
    Top = 152
    Width = 345
    Height = 9
  end
  object Bevel3: TBevel
    Left = 8
    Top = 216
    Width = 89
    Height = 9
  end
  object Bevel4: TBevel
    Left = 88
    Top = 216
    Width = 9
    Height = 57
  end
  object Label5: TLabel
    Left = 120
    Top = 224
    Width = 92
    Height = 13
    Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1082#1083#1102#1095#1072
  end
  object Label6: TLabel
    Left = 120
    Top = 256
    Width = 41
    Height = 13
    Caption = 'BarCode'
  end
  object Panel1: TPanel
    Left = 0
    Top = 287
    Width = 367
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 0
    object Button1: TButton
      Left = 72
      Top = 16
      Width = 83
      Height = 25
      Caption = #1054#1082
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 176
      Top = 16
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 272
      Top = 16
      Width = 75
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Enabled = False
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object Edit1: TEdit
    Left = 104
    Top = 64
    Width = 137
    Height = 37
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnChange = ComboBox1Change
  end
  object Edit2: TEdit
    Left = 104
    Top = 104
    Width = 137
    Height = 37
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnChange = ComboBox1Change
  end
  object BitBtn1: TBitBtn
    Left = 264
    Top = 72
    Width = 75
    Height = 65
    Caption = #1054#1087#1088#1077#1076#1077#1083#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn1Click
  end
  object ComboBox3: TComboBox
    Left = 232
    Top = 216
    Width = 97
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = 'ComboBox3'
    OnChange = ComboBox1Change
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5')
  end
  object Edit3: TEdit
    Left = 232
    Top = 248
    Width = 97
    Height = 21
    TabOrder = 5
    Text = 'Edit3'
    OnChange = ComboBox1Change
  end
  object ComboBox1: TcxLookupComboBox
    Left = 96
    Top = 16
    Width = 241
    Height = 21
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmC.dsOperations
    Properties.ReadOnly = False
    TabOrder = 6
  end
  object ComboBox2: TcxLookupComboBox
    Left = 144
    Top = 176
    Width = 185
    Height = 21
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmC.dsStates
    Properties.ReadOnly = False
    Properties.OnChange = ComboBox2PropertiesChange
    TabOrder = 7
  end
end
