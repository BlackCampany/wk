unit uPressKey;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus;

type
  TfmPressKey = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPressKey: TfmPressKey;
  vSet:Boolean;

implementation

Uses Un1;

{$R *.dfm}

procedure TfmPressKey.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var bCh:Byte;
    iShift:Integer;
begin
  bCh:=ord(Key);
  //121,27,18
  iShift:=0;
  if Shift=[ssShift]  then iShift:=1;
  if Shift=[ssCtrl]   then iShift:=2;

  if (bch in [27,121,13,16,17,18])or (Shift=[ssAlt]) then
  begin
    if bch in [27,121] then //������� � ����������� � ���
    begin
      if bch=27 then vSet:=True;
      close;
    end
    else
    begin
      Label1.Font.Color:=clRed;
      label1.Caption:='������ ������� ������������ ������.';
      edit1.Text:='';
      edit2.Text:='';

      CommonSEt.Key_Char:='';
      CommonSet.Key_Code:=0;
      CommonSet.Key_Shift:=0;
    end;
  end
  else
  begin
    Label1.Font.Color:=clBlack;
    label1.Caption:='������� ������� '+ShortCuttoText(ShortCut(Key,shift))+' ��� '+IntToStr(bCh);
    edit1.Text:=ShortCuttoText(ShortCut(Key,shift));
    edit2.Text:=IntToStr(Key);

    CommonSEt.Key_Char:=ShortCuttoText(ShortCut(Key,shift));
    CommonSet.Key_Code:=bCh;
    CommonSet.Key_Shift:=iShift;

  end;
end;

end.
