unit RepPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class;

type
  TfmRepPost = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridPost: TcxGrid;
    ViewPost: TcxGridDBTableView;
    LevelPost: TcxGridLevel;
    dsPost: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepPost: TfrReport;
    frtaPost: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    taPost: TClientDataSet;
    taPostIdCard: TIntegerField;
    taPostNameC: TStringField;
    taPostiM: TIntegerField;
    taPostsM: TStringField;
    taPostrQ: TFloatField;
    taPostrPrice: TCurrencyField;
    taPostrSum: TCurrencyField;
    taPostiPost: TIntegerField;
    taPostNameP: TStringField;
    taPostiGr: TIntegerField;
    taPostNameG: TStringField;
    ViewPostIdCard: TcxGridDBColumn;
    ViewPostNameC: TcxGridDBColumn;
    ViewPostsM: TcxGridDBColumn;
    ViewPostrQ: TcxGridDBColumn;
    ViewPostrPrice: TcxGridDBColumn;
    ViewPostrSum: TcxGridDBColumn;
    ViewPostiPost: TcxGridDBColumn;
    ViewPostNameP: TcxGridDBColumn;
    ViewPostNameG: TcxGridDBColumn;
    taPostiDoc: TIntegerField;
    taPostiTDoc: TSmallintField;
    taPostsDoc: TStringField;
    ViewPostsDoc: TcxGridDBColumn;
    taPostNameMH: TStringField;
    ViewPostNameMH: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPost: TfmRepPost;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps, ParamSel, MainRnOffice;

{$R *.dfm}

procedure TfmRepPost.FormCreate(Sender: TObject);
begin
  GridPost.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewPost.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepPost.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPost.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepPost.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepPost.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPost.SpeedItem2Click(Sender: TObject);
begin
  //����� �� �����������
  fmParamSel:=TfmParamSel.Create(Application);
  fmParamSel.cxButtonEdit1.Text:=CurVal.SCLASS;
  fmParamSel.cxButtonEdit1.Tag:=CurVal.ICLASS;
  if CurVal.ICLALL=1 then fmParamSel.cxCheckBox1.Checked:=True else fmParamSel.cxCheckBox1.Checked:=False;
  fmParamSel.ShowModal;
  if fmParamSel.ModalResult=mrOk then
  begin
    CurVal.SCLASS:=fmParamSel.cxButtonEdit1.Text;
    CurVal.ICLASS:=fmParamSel.cxButtonEdit1.Tag;
    if fmParamSel.cxCheckBox1.Checked then CurVal.ICLALL:=1 else CurVal.ICLALL:=0;

    fmParamSel.Release;

//    fmRepPost.Show;  delay(10);//}
    Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
    Memo1.Lines.Add('  ������� �� �����������.'); delay(10);
    Memo1.Lines.Add('  ��������� ������.'); delay(10);

    ViewPost.BeginUpdate;

    dmO.quMH_.Active:=False;
    dmO.quMH_.Active:=True;

    CloseTa(taPost);
    if CurVal.ICLALL=0 then  //�� ����� ������
    begin
      prFindPartRemn(CurVal.ICLASS);
    end else  //�� ���� ������
    begin
      with dmORep do
      begin
        quMainClass.Active:=False;
        quMainClass.Active:=True;
        quMainClass.First;
        while not quMainClass.Eof do
        begin
          prFindPartRemn(quMainClassID.AsInteger);
          quMainClass.Next;
        end;
        quMainClass.Active:=False;
      end;
    end;
    dmO.quMH_.Active:=False;

    ViewPost.EndUpdate;

    Memo1.Lines.Add('������������ ��.'); delay(10);
  end else
   fmParamSel.Release;
end;

procedure TfmRepPost.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel4(ViewPost);
end;

procedure TfmRepPost.SpeedItem4Click(Sender: TObject);
//Var StrWk:String;
//    i:Integer;
begin
//������
  ViewPost.BeginUpdate;
{  taPost.Filter:=ViewPost.DataController.Filter.FilterText;
  taPost.Filtered:=True;

  frRepPost.LoadFromFile(CurDir + 'Post.frf');

  if CommonSet.DateTo>=iMaxDate then frVariables.Variable['sPeriod']:=' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewPost.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepPost.ReportName:='����� � �������.';
  frRepPost.PrepareReport;
  frRepPost.ShowPreparedReport;

  taPost.Filter:='';
  taPost.Filtered:=False;}
  ViewPost.EndUpdate;
end;

end.
