unit OperType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, ExtCtrls, SpeedBar,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox;

type
  TfmOperType = class(TForm)
    StatusBar1: TStatusBar;
    ViewOperType: TcxGridDBTableView;
    LevelOperType: TcxGridLevel;
    GrOperType: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    amOperType: TActionManager;
    acAddOperType: TAction;
    acEditOperType: TAction;
    acDelOperType: TAction;
    Timer1: TTimer;
    ViewOperTypeABR: TcxGridDBColumn;
    ViewOperTypeDOCTYPE: TcxGridDBColumn;
    ViewOperTypeNAMEOPER: TcxGridDBColumn;
    ViewOperTypeNAMEDOC: TcxGridDBColumn;
    ViewOperTypeTPRIB: TcxGridDBColumn;
    ViewOperTypeTSPIS: TcxGridDBColumn;
    procedure acAddOperTypeExecute(Sender: TObject);
    procedure acEditOperTypeExecute(Sender: TObject);
    procedure acDelOperTypeExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOperType: TfmOperType;
  bClearPr:Boolean = False;

implementation

uses dmOffice, DMOReps, AddOperT;

{$R *.dfm}

procedure TfmOperType.acAddOperTypeExecute(Sender: TObject);
Var Id:Integer;
begin
  //��������
  if not CanDo('prAddOperT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    ViewOperType.BeginUpdate;

    taDocType.Active:=False;
    taDocType.Active:=True;
    taDocType.First;

    fmAddOper:=tfmAddOper.Create(Application);
    fmAddOper.Caption:='���������� ��������.';
    fmAddOper.cxTextEdit1.Text:='';
    fmAddOper.cxTextEdit1.Tag:=0;
    fmAddOper.cxTextEdit2.Text:='';
    fmAddOper.cxLookupComboBox1.EditValue:=taDocTypeID.AsInteger;
    fmAddOper.cxLookupComboBox1.Text:=taDocTypeNAMEDOC.AsString;
    fmAddOper.cxImageComboBox1.EditValue:=2;
    fmAddOper.cxImageComboBox2.EditValue:=1;

    fmAddOper.ShowModal;
    if fmAddOper.ModalResult=mrOk then
    begin

      Id:=0;
      quOperT.First;
      while not quOperT.Eof do
      begin
        if quOperTId.AsInteger>Id then Id:=quOperTId.AsInteger;
        quOperT.Next;
      end;
      inc(Id);

      quOperT.Append;
      quOperTABR.AsString:=fmAddOper.cxTextEdit1.Text;
      quOperTId.AsInteger:=Id;
      quOperTDOCTYPE.AsInteger:=fmAddOper.cxLookupComboBox1.EditValue;
      quOperTNAMEOPER.AsString:=fmAddOper.cxTextEdit2.Text;
      quOperTTPRIB.AsInteger:=fmAddOper.cxImageComboBox1.EditValue;
      quOperTTSPIS.AsInteger:=fmAddOper.cxImageComboBox2.EditValue;
      quOperT.Post;
      quOperT.Refresh;
    end;
    fmAddOper.Release;
    
    taDocType.Active:=False;
    ViewOperType.EndUpdate;
  end;
end;

procedure TfmOperType.acEditOperTypeExecute(Sender: TObject);
begin
//�������������
  if not CanDo('prEditOperT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    ViewOperType.BeginUpdate;

    taDocType.Active:=False;
    taDocType.Active:=True;
    taDocType.First;
    taDocType.Locate('ID',quOperTDOCTYPE.AsInteger,[]);

    fmAddOper:=tfmAddOper.Create(Application);
    fmAddOper.Caption:='�������������� ��������.';
    fmAddOper.cxTextEdit1.Text:=quOperTABR.AsString;
    fmAddOper.cxTextEdit1.Tag:=quOperTID.AsInteger;
    fmAddOper.cxTextEdit2.Text:=quOperTNAMEOPER.AsString;
    fmAddOper.cxLookupComboBox1.EditValue:=quOperTDOCTYPE.AsInteger;
    fmAddOper.cxLookupComboBox1.Text:=taDocTypeNAMEDOC.AsString;
    fmAddOper.cxImageComboBox1.EditValue:=quOperTTPRIB.AsInteger;
    fmAddOper.cxImageComboBox2.EditValue:=quOperTTSPIS.AsInteger;

    fmAddOper.ShowModal;
    if fmAddOper.ModalResult=mrOk then
    begin

      quOperT.Edit;
      quOperTABR.AsString:=fmAddOper.cxTextEdit1.Text;
      quOperTDOCTYPE.AsInteger:=fmAddOper.cxLookupComboBox1.EditValue;
      quOperTNAMEOPER.AsString:=fmAddOper.cxTextEdit2.Text;
      quOperTTPRIB.AsInteger:=fmAddOper.cxImageComboBox1.EditValue;
      quOperTTSPIS.AsInteger:=fmAddOper.cxImageComboBox2.EditValue;
      quOperT.Post;
      quOperT.Refresh;
    end;
    fmAddOper.Release;
    
    taDocType.Active:=False;
    ViewOperType.EndUpdate;

  end;
end;

procedure TfmOperType.acDelOperTypeExecute(Sender: TObject);
begin
//�������
  if not CanDo('prDelOperT') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    if MessageDlg('�� ������������� ������ ��������: "'+quOperTNAMEOPER.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewOperType.BeginUpdate;
      quOperT.Delete;
      ViewOperType.EndUpdate;
    end;
  end;
end;

procedure TfmOperType.FormCreate(Sender: TObject);
begin
  GrOperType.Align:=AlClient;
end;

procedure TfmOperType.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmOperType.Timer1Timer(Sender: TObject);
begin
  if bClearPr=True then begin StatusBar1.Panels[0].Text:=''; bClearPr:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearPr:=True;
end;

end.
