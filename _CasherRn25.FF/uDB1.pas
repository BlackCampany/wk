unit uDB1;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, DB,
  Windows, FIBDataSet, pFIBDataSet, pFIBStoredProc, WinSpool, VaComm,
  VaSystem, VaClasses;

type
  TdmC1 = class(TDataModule)
    CasherRnDb1: TpFIBDatabase;
    quPrintQu: TpFIBDataSet;
    quPrintQuIDQUERY: TFIBIntegerField;
    quPrintQuID: TFIBIntegerField;
    quPrintQuSTREAM: TFIBIntegerField;
    quPrintQuPTYPE: TFIBStringField;
    quPrintQuFTYPE: TFIBSmallIntField;
    quPrintQuSTR: TFIBStringField;
    quPrintQuPAGECODE: TFIBSmallIntField;
    quPrintQuCTYPE: TFIBSmallIntField;
    quPrintQuBRING: TFIBSmallIntField;
    quDelQu: TpFIBQuery;
    trQuPrint: TpFIBTransaction;
    trDelQu: TpFIBTransaction;
    trPrintSel: TpFIBTransaction;
    trPrintUpd: TpFIBTransaction;
    prGetId1: TpFIBStoredProc;
    trSelGetId: TpFIBTransaction;
    prSetRefresh: TpFIBStoredProc;
    trRefresh: TpFIBTransaction;
    quRefresh: TpFIBDataSet;
    taNumZ: TpFIBDataSet;
    trSelNumZ: TpFIBTransaction;
    trUpdNumZ: TpFIBTransaction;
    taNumZID: TFIBSmallIntField;
    taNumZCURDATE: TFIBDateTimeField;
    taNumZNUM: TFIBIntegerField;
    quRSum: TpFIBDataSet;
    quRSumPAYTYPE: TFIBSmallIntField;
    quRSumSUMR: TFIBFloatField;
    trS1: TpFIBTransaction;
    quRSumRet: TpFIBDataSet;
    quRSumRetPAYTYPE: TFIBSmallIntField;
    quRSumRetSUMR: TFIBFloatField;
    quMaxMin: TpFIBDataSet;
    quMaxMinIBEG: TFIBIntegerField;
    quMaxMinIEND: TFIBIntegerField;
    quDSum: TpFIBDataSet;
    quDSumDSUM: TFIBFloatField;
    quSums: TpFIBDataSet;
    quSumsID: TFIBIntegerField;
    quSumsSUM1: TFIBFloatField;
    quSumsSUM2: TFIBFloatField;
    quSumsSUM3: TFIBFloatField;
    taU1: TpFIBTransaction;
    quTabFind: TpFIBDataSet;
    quTabFindID: TFIBIntegerField;
    quWaitersSum: TpFIBDataSet;
    quWaitersSumID_PERSONAL: TFIBIntegerField;
    quWaitersSumNAME: TFIBStringField;
    quWaitersSumSUMQU: TFIBBCDField;
    quWaitersSumSUMR: TFIBFloatField;
    quPrint: TpFIBDataSet;
    quPrintIDQUERY: TFIBIntegerField;
    quPrintID: TFIBIntegerField;
    quPrintSTREAM: TFIBIntegerField;
    quPrintPTYPE: TFIBStringField;
    quPrintFTYPE: TFIBSmallIntField;
    quPrintSTR: TFIBStringField;
    quPrintPAGECODE: TFIBSmallIntField;
    quPrintCTYPE: TFIBSmallIntField;
    quPrintBRING: TFIBSmallIntField;
    quPrintQH: TpFIBDataSet;
    quAddPQH: TpFIBQuery;
    quPrintQHSTREAM: TFIBIntegerField;
    quPrintQHIDH: TFIBIntegerField;
    quPrintQHCOMMENT: TFIBStringField;
    quSumTalons: TpFIBDataSet;
    quSumTalonsSIFR: TFIBIntegerField;
    quSumTalonsNAME: TFIBStringField;
    quSumTalonsPRICE: TFIBFloatField;
    quSumTalonsQUANT: TFIBFloatField;
    quSumTalonsRSUM: TFIBFloatField;
    quFBar: TpFIBDataSet;
    quFBarBARCODE: TFIBStringField;
    quFBarSIFR: TFIBIntegerField;
    quFBarQUANT: TFIBFloatField;
    quFBarPRICE: TFIBFloatField;
    devCas: TVaComm;
    VaWaitMessageCas: TVaWaitMessage;
    procedure devCasRxBuf(Sender: TObject; Data: PVaData; Count: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure PrintDBStr(iQ,iStream:INteger;sPt,PrintS:String;iFont,iRing:Integer);
    Function GetNumZ:INteger;
    procedure prPrint;
    Procedure prDelQu; //������ �� �������
//    Procedure prSetFont(StrP:String;iFont:INteger); //��������� �����
//    Procedure prPrintStr(StrP,S:String); //��������� �����
  end;

Function GetId2(ta:String):Integer;
Procedure SetRefresh;
Function  GetRefresh:Boolean;
Function FindTab(ID:Integer):Boolean;

Function prOpenPrinter(PName:String):Boolean;
Procedure prWritePrinter(S:String);
Procedure prClosePrinter(PName:String);
procedure prAddPrintQH(STREAM,ID:INteger;S:String);
function prFindBar(sBar:String):INteger;
function prFindBarQ(sBar:String;Var rQ:Real):INteger;


var
  dmC1: TdmC1;

{  iCount: Integer;}
  bPrintOpen: Boolean = False; //��� LPT
  PrintHandle:THandle; //��� LPT
  iReadCount:Integer = 0;
  sScanEv:String;

implementation

uses Un1, Dm;

{$R *.dfm}

function prFindBar(sBar:String):INteger;
Var sB:String;
begin
  Result:=0;
  with dmC1 do
  begin
    sB:=sBar;
    if (pos('22',sB)=1)and(length(sB)>=7) then sB:=Copy(sB,1,7);  //������� �����

    quFBar.Active:=False;
    quFBar.ParamByName('SBAR').AsString:=sB;
    quFBar.Active:=True;
    if quFBar.RecordCount=1 then Result:=quFBarSIFR.AsInteger;

    quFBar.Active:=False;
  end;
end;


function prFindBarQ(sBar:String;Var rQ:Real):INteger;
Var sB:String;
    sQ:String;
begin
  Result:=0;
  rQ:=1;
  with dmC1 do
  begin
    sB:=sBar;
    if (pos('22',sB)=1)and(length(sB)=13) then
    begin
      sQ:=Copy(sB,8,5);
      rQ:=StrToINtDef(sQ,0);     // ��� � �
      if rQ>0 then rQ:=rQ/1000;  // ��� � ��
      sB:=Copy(sB,1,7);  //������� �����
    end;

    quFBar.Active:=False;
    quFBar.ParamByName('SBAR').AsString:=sB;
    quFBar.Active:=True;
    if quFBar.RecordCount=1 then Result:=quFBarSIFR.AsInteger;

    quFBar.Active:=False;
  end;
end;


procedure prAddPrintQH(STREAM,ID:INteger;S:String);
begin
  with dmC1 do
  begin
    quAddPQH.SQL.Clear;
    quAddPQH.SQL.Add('insert into PRINTQH values('+INtToStr(STREAM)+','+INtToStr(ID)+','''+S+''')');
    quAddPQH.ExecQuery;
  end;
end;


Function prOpenPrinter(PName:String):Boolean;
Var  DocInfo1: TDocInfo1;
begin
  prWriteLog('   �������� ������� - '+PName);
  Result:=False;
  if OpenPrinter(PChar(PName), PrintHandle, nil) then
  begin
    Result:=True;
    with DocInfo1 do begin
      pDocName := PChar('Tmp_doc');
      pOutputFile := nil;
      pDataType := 'RAW';
    end;
    StartDocPrinter(PrintHandle, 1, @DocInfo1);
    StartPagePrinter(PrintHandle);
  end
  else prWriteLog('   ������� �� ���� - '+PName);
end;

Procedure prClosePrinter(PName:String);
begin
  prWriteLog('   �������� ������� - '+PName);
  EndPagePrinter(PrintHandle);
  EndDocPrinter(PrintHandle);
  ClosePrinter(PrintHandle);
  bPrintOpen:=False;
end;

Procedure prWritePrinter(S:String);
Var  N: DWORD;
begin
  WritePrinter(PrintHandle, PChar(S), Length(S), N);
end;


Procedure TdmC1.prDelQu; //������ �� ������� - ����� ��������� ����� �.�. ������ ������ ����� �������������� ������� PS
begin
  if quPrintQH.Active then
  begin
    prWriteLogPS(' ������ �������.');
    quPrintQH.First;
    while not quPrintQH.Eof do //� ������� �������� ���� ������ �� ���������� ������
    begin
      quDelQu.ParamByName('IDQ').AsInteger:=quPrintQHIDH.AsInteger;
      quDelQu.ExecQuery;
      delay(30);
      quPrintQH.Delete;
    end;
  end;
end;

procedure TdmC1.prPrint;
Var n:INteger;
    StrP,StrPN,StrDev:String;
    bSpec:Boolean;
begin
  //������� �� �������

  for n:=1 to 5 do
  begin
    StrP:='0';
    Case n of
    1: begin StrDev:=CommonSet.Qgroup1; StrPN:=CommonSet.Pgroup1N; end;
    2: begin StrDev:=CommonSet.Qgroup2; StrPN:=CommonSet.Pgroup2N; end;
    3: begin StrDev:=CommonSet.Qgroup3; StrPN:=CommonSet.Pgroup3N; end;
    4: begin StrDev:=CommonSet.Qgroup4; StrPN:=CommonSet.Pgroup4N; end;
    5: begin StrDev:=CommonSet.Qgroup5; StrPN:=CommonSet.Pgroup5N; end;
    end;
    //��������  StrP= COM1|COM2|COM3  - ���� ���������� �� 3-� ��������
    //��� ���� ��� ��������� �����
    if (StrDev<>'0')and(StrDev<>'') then  //������ ����� ��������� �� ������ ������� ��� ������
    begin
      quPrintQH.Active:=False;
      quPrintQH.ParamByName('ISTREAM').AsInteger:=n;
      quPrintQH.Active:=True;
      if quPrintQH.RecordCount>0 then
      begin
        while StrDev>'' do
        begin
          if pos('|',StrDev)>0 then
          begin
            StrP:=Copy(StrDev,1,pos('|',StrDev)-1);
            delete(StrDev,1,pos('|',StrDev));
//        prWriteLog('   ������� ������ 1 - '+StrP);
          end else
          begin
            StrP:=StrDev;
            StrDev:='';
          end;
          with dmC do
          if (StrP<>'0')and(StrP<>'') then
          begin
            prDevOpen(StrP,1); //�������� ����������

            quPrintQH.First;
            while not quPrintQH.Eof do
            begin
              quPrintQu.Active:=False;
              quPrintQu.ParamByName('IDQ').AsInteger:=quPrintQHIDH.AsInteger;
              quPrintQu.Active:=True;

              if quPrintQu.RecordCount>0 then //���� ��� �������� � ���� �������
              begin
                prWriteLogPS(' '+IntToStr(n)+'('+StrP+') ����� ������� ������� - '+IntToStr(quPrintQHIDH.AsInteger));
                prWriteLogPS(' '+IntToStr(n)+'('+StrP+') ���-�� ������� - '+IntToStr(quPrintQu.RecordCount));

                if quPrintQuBRING.AsInteger=1 then prRing(StrP); //������ �����

                bSpec:=False;
                quPrintQu.First;
                while not quPrintQu.Eof do
                begin
                  prSetFont(StrP,quPrintQuFTYPE.AsInteger,1);
                  prPrintStr(StrP,quPrintQuSTR.AsString);
                  if Pos('Check',quPrintQuPTYPE.AsString)>0 then bSpec:=True;

                  quPrintQu.Next;
                  delay(10);
                end;
                if bSpec then CutDocPrSpec //������ ��� �������� ��������� - �����
                else prCutDoc(StrP,1); //�������
              end;
              quPrintQu.Active:=False;
              Delay(CommonSet.CheckDelaySec); //�������� ����� ������� ������ ������� �� ����� ��������

              quPrintQH.Next;
            end;
            prDevClose(StrP,1);//�������� ����������
          end;
        end;
        prDelQu; //�������� ��� ������� �� ������� quPrintQH.
      end;
      quPrintQH.Active:=False;
    end;
  end;
end;


Function FindTab(ID:Integer):Boolean;
begin
  Result:=False;
  with dmC1 do
  begin
    quTabFind.Active:=False;
    quTabFind.ParamByName('ID').AsInteger:=ID;
    quTabFind.Active:=True;
    if quTabFind.RecordCount>0 then Result:=True;
    quTabFind.Active:=False;
  end;
end;


Function TdmC1.GetNumZ:INteger;
Var iNum:INteger;
begin

  if taNumZ.Active=False then taNumZ.Active:=True;
  taNumZ.FullRefresh;
  if taNumZ.RecordCount=0 then
  begin
    taNumZ.Append;
    taNumZID.AsInteger:=1;
    taNumZCURDATE.AsDateTime:=Trunc(Date)+1+StrToTimeDef(CommonSet.ZTimeShift,0);
    taNumZNUM.AsInteger:=1;
    taNumZ.Post;

    Result:=1;
  end else
  begin
    taNumZ.First;
    if taNumZCURDATE.AsDateTime>now then
    begin
      iNum:=taNumZNUM.AsInteger+1;
      taNumZ.Edit;
      taNumZNUM.AsInteger:=iNum;
      taNumZ.Post;

      Result:=iNum;
    end else
    begin
      taNumZ.Edit;
      taNumZCURDATE.AsDateTime:=Trunc(Date)+1+StrToTimeDef(CommonSet.ZTimeShift,0);
      taNumZNUM.AsInteger:=1;
      taNumZ.Post;

      Result:=1;
    end;
  end;
end;

Function  GetRefresh:Boolean;
begin
  with dmC1 do
  begin
    result:=False;



  end;
end;


Procedure SetRefresh;
begin
  with dmC1 do
  begin
    prSetRefresh.ParamByName('STATION').AsInteger:=Commonset.Station;
    prSetRefresh.ExecProc;
  end;
end;


Procedure TdmC1.PrintDBStr(iQ,iStream:INteger;sPt,PrintS:String;iFont,iRing:Integer);
Var iQs:Integer;
begin
//  Delay(20);
  iQs:=GetId('PQS');

//  Delay(20);
  quPrint.Append;
  quPrintIDQUERY.AsInteger:=iQ;
  quPrintID.AsInteger:=iQs;
  quPrintSTREAM.AsInteger:=iStream;
  quPrintPTYPE.AsString:=sPt;
  quPrintFTYPE.AsInteger:=iFont;
  quPrintSTR.AsString:=PrintS;
  quPrintCTYPE.AsInteger:=0;
  quPrintBRING.AsInteger:=iRing;
  quPrint.Post;
end;

Function GetId2(ta:String):Integer;
Var iType:Integer;
begin
  with dmC1 do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all

    if ta='PQH' then iType:=9; //����� ������� ������
    if ta='PQS' then iType:=10; //����� ������ ������
    if ta='PQH0' then iType:=11; //����� ������� ��� � ���������

    delay(10);
    prGetId1.ParamByName('ITYPE').Value:=iType;
    prGetId1.ExecProc;
    result:=prGetId1.ParamByName('RESULT').Value;
  end;
end;


procedure TdmC1.devCasRxBuf(Sender: TObject; Data: PVaData;
  Count: Integer);
var P: Integer;
begin
  if Count >0 then
  begin
    for P:=0 to Count-1 do
    begin
      sScanEv:=sScanEv + char(Data[P]);
    end;
  end;
  iReadCount:=iReadCount+Count;
end;

end.
