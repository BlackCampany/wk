unit DocOutR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, DBClient;

type
  TfmDocsReal = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsR: TcxGrid;
    ViewDocsR: TcxGridDBTableView;
    LevelDocsR: TcxGridLevel;
    ViewDocsRID: TcxGridDBColumn;
    ViewDocsRDATEDOC: TcxGridDBColumn;
    ViewDocsRNUMDOC: TcxGridDBColumn;
    ViewDocsRNAMECL: TcxGridDBColumn;
    ViewDocsRIDSKL_FROM: TcxGridDBColumn;
    ViewDocsRIDSKL_TO: TcxGridDBColumn;
    ViewDocsRSUMIN: TcxGridDBColumn;
    ViewDocsRSUMUCH: TcxGridDBColumn;
    ViewDocsRSUMTAR: TcxGridDBColumn;
    ViewDocsRPROCNAC: TcxGridDBColumn;
    ViewDocsRNAMEMH: TcxGridDBColumn;
    ViewDocsRIACTIVE: TcxGridDBColumn;
    amDocsReal: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsVn: TcxGridLevel;
    ViewCardsR: TcxGridDBTableView;
    ViewCardsRNAME: TcxGridDBColumn;
    ViewCardsRNAMESHORT: TcxGridDBColumn;
    ViewCardsRIDCARD: TcxGridDBColumn;
    ViewCardsRQUANT: TcxGridDBColumn;
    ViewCardsRPRICEIN: TcxGridDBColumn;
    ViewCardsRSUMIN: TcxGridDBColumn;
    ViewCardsRPRICEUCH: TcxGridDBColumn;
    ViewCardsRSUMUCH: TcxGridDBColumn;
    ViewCardsRIDNDS: TcxGridDBColumn;
    ViewCardsRSUMNDS: TcxGridDBColumn;
    ViewCardsRDATEDOC: TcxGridDBColumn;
    ViewCardsRNUMDOC: TcxGridDBColumn;
    ViewCardsRNAMECL: TcxGridDBColumn;
    ViewCardsRNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsReal: TfrReport;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    frquSpecRSel: TfrDBDataSet;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    acPrintTTN13: TAction;
    N5: TMenuItem;
    taSpecRPrint: TClientDataSet;
    taSpecRPrintNum: TIntegerField;
    taSpecRPrintIdGoods: TIntegerField;
    taSpecRPrintNameG: TStringField;
    taSpecRPrintIM: TIntegerField;
    taSpecRPrintSM: TStringField;
    taSpecRPrintQuant: TFloatField;
    taSpecRPrintPriceIn: TCurrencyField;
    taSpecRPrintSumIn: TCurrencyField;
    taSpecRPrintPriceR: TCurrencyField;
    taSpecRPrintSumR: TCurrencyField;
    taSpecRPrintSumNac: TCurrencyField;
    taSpecRPrintProcNac: TFloatField;
    taSpecRPrintKm: TFloatField;
    taSpecRPrintTCard: TIntegerField;
    taSpecRPrintMassa: TStringField;
    N6: TMenuItem;
    acAddDocs: TAction;
    N7: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsRDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acPrintTTN13Execute(Sender: TObject);
    procedure acAddDocsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prOpen(IDH:Integer);
    procedure prOn(IdH,IdSkl,iDate,iCli:Integer;var rSum1,rSum2:Real);
  end;

var
  fmDocsReal: TfmDocsReal;
  bClearDocR:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc3, DMOReps, TBuff, AddDoc4,
  MainRnOffice;

{$R *.dfm}

procedure TfmDocsReal.prOn(IdH,IdSkl,iDate,iCli:Integer;var rSum1,rSum2:Real);
Var PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure,rQRemn:Real;
begin
  with dmO do
  with dmORep do
  begin
    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    rSum1:=0;

    quSpecRSel.First;
    while not quSpecRSel.Eof do
    begin

      PriceSp:=0;
      rSumIn:=0;
//            rSumUch:=0;
      rQs:=quSpecRSelQUANT.AsFloat*quSpecRSelKM.AsFloat; //�������� � ��������

      if rQs>0 then
      begin //����������� ������
        prSelPartIn(quSpecRSelIDCARD.AsInteger,IdSkl,0,0);

        quSelPartIn.First;
        if rQs>0 then
        begin
          while (not quSelPartIn.Eof) and (rQs>0) do
          begin
           //���� �� ���� ������� ���� �����, ��������� �������� ���
            rQp:=quSelPartInQREMN.AsFloat;
            if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                        else  rQ:=rQp;
            rQs:=rQs-rQ;

            PriceSp:=quSelPartInPRICEIN.AsFloat;
            rSumIn:=rSumIn+RoundVal(PriceSp*rQ);
            rSum1:=rSum1+RoundVal(PriceSp*rQ); //����� ���������

            prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecRSelIDCARD.AsInteger;
            prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
            prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
            prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
            prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
            prAddPartOut.ParamByName('DTYPE').AsInteger:=8;
            prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
            prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
            prAddPartOut.ParamByName('SUMOUT').AsFloat:=RoundVal(quSpecRSelPRICER.AsFloat*rQ);
            prAddPartout.ExecProc;

            quSelPartIn.Next;
          end;

          if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
          begin
            if PriceSp=0 then
            begin //��� ���� ���������� ������� � ���������� ����������
              prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecRSelIDCARD.AsInteger;
              prCalcLastPrice1.ExecProc;
              PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
              rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
              if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
            end;

            prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecRSelIDCARD.AsInteger;
            prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
            prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
            prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
            prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
            prAddPartOut.ParamByName('DTYPE').AsInteger:=8;
            prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
            prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
            prAddPartOut.ParamByName('SUMOUT').AsFloat:=quSpecRSelPRICER.AsFloat*rQs;
            prAddPartout.ExecProc;

            rSumIn:=rSumIn+RoundVal(PriceSp*rQs);
//                rSumUch:=rSumUch+RoundVal(PriceUch*rQs);
            rSum1:=rSum1+RoundVal(PriceSp*rQs); //����� ���������
          end;
        end;
        quSelPartIn.Active:=False;

      //�������� ���������
        quSpecRSel.Edit;
        if quSpecRSelQUANT.AsFloat<>0 then
        begin
          quSpecRSelSUMIN.AsFloat:=rSumIn;
          quSpecRSelPRICEIN.AsFloat:=rSumIn/quSpecRSelQUANT.AsFloat;
        end else
        begin
          quSpecRSelSUMIN.AsFloat:=0;
          quSpecRSelPRICEIN.AsFloat:=0;
        end;
        quSpecRSel.Post;

      end else  //������������� ����� - ��� ������� ��������� ������  (�������)
      begin
        //������ - �������� �������� ����� �������� �� ���� �.�. �� ����� �����

        //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
        rQRemn:=prCalcRemn(quSpecRSelIDCARD.AsInteger,iDate-1,IdSkl);
        if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
        begin
          quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecRSelIDCARD.AsInteger;
          quClosePartIn.ParamByName('IDSKL').AsInteger:=IdSkl;
          quClosePartIn.ParamByName('IDATE').AsInteger:=iDate-1;
          quClosePartIn.ExecQuery;
        end;

        prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl;
        prAddPartIn1.ParamByName('IDDOC').AsInteger:=IdH;
        prAddPartIn1.ParamByName('DTYPE').AsInteger:=8;
        prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
        prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecRSelIDCARD.AsInteger;
        prAddPartIn1.ParamByName('IDCLI').AsInteger:=iCli;
        prAddPartIn1.ParamByName('QUANT').AsFloat:=rQs*(-1); //������ ����

        PriceSp:=0;
        PriceUch:=0;
        if quSpecRSelKM.AsFloat<>0 then
        begin
          PriceSp:=quSpecRSelPRICEIN.AsFloat/quSpecRSelKM.AsFloat;   //���� � ������� ��
          PriceUch:=quSpecRSelPRICER.AsFloat/quSpecRSelKM.AsFloat; //���� � ������� ��
        end;

        prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
        prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
        prAddPartIn1.ExecProc;

        rSum1:=rSum1+quSpecRSelSUMIN.AsFloat; //����� ���������
      end;

      quSpecRSel.Next;
      delay(10);
    end;

    quSpecRSel.Active:=False;
          //�� ����

    quTaraVn.Active:=False;
    quTaraVn.ParamByName('IDH').AsInteger:=IDH;
    quTaraVn.Active:=True;

    rSum2:=0;

    quTaraVn.First;
    while not quTaraVn.Eof do
    begin

      PriceSp:=0;
      PriceUch:=0;
      rSumIn:=0;
      rSumUch:=0;
      rQs:=quTaraVnQUANT.AsFloat*quTaraVnKM.AsFloat; //�������� � ��������
      prSelPartInT(quTaraVnIDCARD.AsInteger,IdSkl,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
               //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=rSumIn+RoundVal(PriceSp*rQ);
          rSumUch:=rSumUch+RoundVal(PriceUch*rQ);
          rSum2:=rSum2+RoundVal(PriceSp*rQ);

          prAddPartOutT.ParamByName('ARTICUL').AsInteger:=quTaraVnIDCARD.AsInteger;
          prAddPartOutT.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOutT.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOutT.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOutT.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOutT.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOutT.ParamByName('DTYPE').AsInteger:=8;
          prAddPartOutT.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartOutT.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOutT.ParamByName('SUMOUT').AsFloat:=rSumUch;
          prAddPartoutT.ExecProc;

          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quTaraVnIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;

          prAddPartOutT.ParamByName('ARTICUL').AsInteger:=quTaraVnIDCARD.AsInteger;
          prAddPartOutT.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOutT.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOutT.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOutT.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOutT.ParamByName('IDCLI').AsInteger:=0;
          prAddPartOutT.ParamByName('DTYPE').AsInteger:=8;
          prAddPartOutT.ParamByName('QUANT').AsFloat:=rQs;
          prAddPartOutT.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOutT.ParamByName('SUMOUT').AsFloat:=PriceUch*rQs;
          prAddPartoutT.ExecProc;

          rSumIn:=rSumIn+(PriceSp*rQs);
          rSumUch:=rSumUch+(PriceUch*rQs);
          rSum2:=rSum2+(PriceSp*rQs);
        end;
      end;
      quSelPartIn.Active:=False;

      //�������� ���������
      quTaraVn.Edit;
      if quTaraVnQUANT.AsFloat<>0 then
      begin
        quTaraVnSUMIN.AsFloat:=RoundVal(rSumIn);
        quTaraVnSUMUCH.AsFloat:=RoundVal(rSumUch);
        quTaraVnPRICEIN.AsFloat:=RoundVal(rSumIn/quTaraVnQUANT.AsFloat);
        quTaraVnPRICEUCH.AsFloat:=RoundVal(rSumUch/quTaraVnQUANT.AsFloat);
      end else
      begin
        quTaraVnSUMIN.AsFloat:=0;
        quTaraVnSUMUCH.AsFloat:=0;
        quTaraVnPRICEIN.AsFloat:=0;
        quTaraVnPRICEUCH.AsFloat:=0;
      end;
      quTaraVn.Post;

      quTaraVn.Next;
      delay(10);
    end;

    quTaraVn.Active:=False;
  end;
end;

procedure TfmDocsReal.prOpen(IDH:Integer);
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    quSpecRSel.First;
    while not quSpecRSel.Eof do
    begin
      with fmAddDoc4 do
      begin
        taSpec.Append;
        taSpecNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpecIdGoods.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpecNameG.AsString:=quSpecRSelNAMEC.AsString;
        taSpecIM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpecSM.AsString:=quSpecRSelSM.AsString;
        taSpecQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpecPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpecSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpecPriceR.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecSumR.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecINds.AsInteger:=quSpecRSelIDNDS.AsInteger;
        taSpecSNds.AsString:=quSpecRSelNAMENDS.AsString;
        taSpecRNds.AsFloat:=quSpecRSelSUMNDS.AsFloat;
        taSpecSumNac.AsFloat:=quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat;
        taSpecProcNac.AsFloat:=0;
        if quSpecRSelSUMIN.AsFloat<>0 then
          taSpecProcNac.AsFloat:=RoundEx((quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat)/quSpecRSelSUMIN.AsFloat*10000)/100;
        if quSpecRSelKM.AsFloat>0 then taSpecKM.AsFloat:=quSpecRSelKM.AsFloat
        else
        begin
          taSpecKM.AsFloat:=prFindKM(quSpecRSelIDM.AsInteger);
        end;
        taSpecTCard.AsInteger:=quSpecRSelTCard.AsInteger;
        taSpecOper.AsInteger:=quSpecRSelOPER.AsInteger;
        taSpec.Post;
      end;
      quSpecRSel.Next;
    end;

    quTaraR.Active:=False;
    quTaraR.ParamByName('IDH').AsInteger:=IDH;
    quTaraR.Active:=True;

    quTaraR.First;
    while not quTaraR.Eof do
    begin
      with fmAddDoc4 do
      begin
        taTara.Append;
        taTaraNum.AsInteger:=quTaraRNUM.AsInteger;
        taTaraIdCard.AsInteger:=quTaraRIDCARD.AsInteger;
        taTaraNameG.AsString:=quTaraRNAMEC.AsString;
        taTaraQuant.AsFloat:=quTaraRQUANT.AsFloat;
        taTaraPriceIn.AsFloat:=quTaraRPRICEIN.AsFloat;
        taTaraSumIn.AsFloat:=quTaraRSUMIN.AsFloat;
        taTaraPriceUch.AsFloat:=quTaraRPRICEUCH.AsFloat;
        taTaraSumUch.AsFloat:=quTaraRSUMUCH.AsFloat;
        taTaraIM.AsInteger:=quTaraRIDM.AsInteger;
        taTaraKM.AsFloat:=quTaraRKM.asfloat;
        taTaraSM.AsString:=quTaraRSM.asString;
        taTaraSumNac.AsFloat:=quTaraRSUMUCH.AsFloat-quTaraRSUMIN.AsFloat;
        if abs(quTaraRSUMIN.AsFloat)>0.01 then
        begin
          taTaraProcNac.AsFloat:=(quTaraRSUMUCH.AsFloat-quTaraRSUMIN.AsFloat)/quTaraRSUMIN.AsFloat*100;
        end;

        taTara.Post;
      end;
      quTaraR.Next;
    end;

    prAllViewOn;
  end;
end;

procedure TfmDocsReal.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  delay(100);
end;


procedure TfmDocsReal.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsReal.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsR.Align:=AlClient;
  ViewDocsR.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsR.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00DFDFBD;
end;

procedure TfmDocsReal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsR.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsR.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsReal.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsR.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsReal.Caption:='���������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsR.BeginUpdate;
        quDocsRSel.Active:=False;
        quDocsRSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRSel.Active:=True;
        ViewDocsR.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsReal.Caption:='���������� �� ������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsR.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsR.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsReal.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddDoc4.Caption:='��������� �� ����������: ����� ��������.';
    fmAddDoc4.cxTextEdit1.Text:=prGetNum(8,0);
    fmAddDoc4.cxTextEdit1.Tag:=0;
    fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddDoc4.cxTextEdit2.Text:='';
    fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddDoc4.cxDateEdit1.Date:=Date;
    fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddDoc4.cxDateEdit2.Date:=Date;
    fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
    fmAddDoc4.cxCalcEdit1.EditValue:=0;
    fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=False;

    if taNDS.Active=False then taNDS.Active:=True;
    taNds.FullRefresh;;

    fmAddDoc4.cxButtonEdit1.Tag:=0;
    fmAddDoc4.cxButtonEdit1.EditValue:=0;
    fmAddDoc4.cxButtonEdit1.Text:='';
    fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;
    fmAddDoc4.cxButtonEdit1.Enabled:=True;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddDoc4.cxLookupComboBox1.EditValue:=0;
    fmAddDoc4.cxLookupComboBox1.Text:='';
    fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddDoc4.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddDoc4.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddDoc4.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddDoc4.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddDoc4.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc4.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddDoc4.Label15.Caption:='��. ����: ';
      fmAddDoc4.Label15.Tag:=0;
    end;

    fmAddDoc4.cxLabel1.Enabled:=True;
    fmAddDoc4.cxLabel2.Enabled:=True;
    fmAddDoc4.cxLabel3.Enabled:=True;
    fmAddDoc4.cxLabel4.Enabled:=True;
    fmAddDoc4.cxLabel5.Enabled:=True;
    fmAddDoc4.cxLabel6.Enabled:=True;
    fmAddDoc4.N1.Enabled:=True;

    fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
    fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;

    fmAddDoc4.cxButton1.Enabled:=True;
    CloseTa(fmAddDoc4.taSpec);

    fmAddDoc4.ViewTara.OptionsData.Editing:=True;
    fmAddDoc4.ViewTara.OptionsData.Deleting:=True;

    CloseTa(fmAddDoc4.taTara);

    fmAddDoc4.acSaveDoc.Enabled:=True;

    fmAddDoc4.Show;
  end;
end;

procedure TfmDocsReal.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        fmAddDoc4.Caption:='��������� �� ���������� : ��������������.';
        fmAddDoc4.cxTextEdit1.Text:=quDocsRSelNUMDOC.AsString;
        fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc4.cxTextEdit1.Tag:=quDocsRSelID.AsInteger;

        fmAddDoc4.cxTextEdit2.Text:=quDocsRSelNUMSF.AsString;
        fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
        fmAddDoc4.cxDateEdit1.Date:=quDocsRSelDATEDOC.AsDateTime;
        fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
        fmAddDoc4.cxDateEdit2.Date:=quDocsRSelDATESF.AsDateTime;
        fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
        fmAddDoc4.cxButtonEdit1.Enabled:=True;
        fmAddDoc4.cxCalcEdit1.EditValue:=quDocsRSelPROCNAC.AsFloat;
        fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=False;

        if taNDS.Active=False then taNDS.Active:=True;
        taNds.FullRefresh;

        fmAddDoc4.cxButtonEdit1.Tag:=quDocsRSelIDCLI.AsInteger;
        fmAddDoc4.cxButtonEdit1.EditValue:=quDocsRSelIDCLI.AsInteger;
        fmAddDoc4.cxButtonEdit1.Text:=quDocsRSelNAMECL.AsString;
        fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddDoc4.cxLookupComboBox1.EditValue:=quDocsRSelIDSKL.AsInteger;
        fmAddDoc4.cxLookupComboBox1.Text:=quDocsRSelNAMEMH.AsString;
        fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsRSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsRSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddDoc4.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc4.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddDoc4.Label15.Caption:='��. ����: ';
          fmAddDoc4.Label15.Tag:=0;
        end;

        fmAddDoc4.cxLabel1.Enabled:=True;
        fmAddDoc4.cxLabel2.Enabled:=True;
        fmAddDoc4.cxLabel3.Enabled:=True;
        fmAddDoc4.cxLabel4.Enabled:=True;
        fmAddDoc4.cxLabel5.Enabled:=True;
        fmAddDoc4.cxLabel6.Enabled:=True;
        fmAddDoc4.cxButton1.Enabled:=True;
        fmAddDoc4.N1.Enabled:=True;

        fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
        fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;
        fmAddDoc4.ViewTara.OptionsData.Editing:=True;
        fmAddDoc4.ViewTara.OptionsData.Deleting:=True;

        CloseTa(fmAddDoc4.taSpec);
        CloseTa(fmAddDoc4.taTara);

        fmAddDoc4.acSaveDoc.Enabled:=True;

        IDH:=quDocsRSelID.AsInteger;

        prOpen(IDH);

        fmAddDoc4.Show;

      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsReal.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      prAllViewOff;

      fmAddDoc4.Caption:='��������� �� ����������: ��������.';
      fmAddDoc4.cxTextEdit1.Text:=quDocsRSelNUMDOC.AsString;
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxTextEdit2.Text:=quDocsRSelNUMSF.AsString;
      fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc4.cxDateEdit1.Date:=quDocsRSelDATEDOC.AsDateTime;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxDateEdit2.Date:=quDocsRSelDATESF.AsDateTime;
      fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc4.cxCalcEdit1.EditValue:=quDocsRSelPROCNAC.AsFloat;
      fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=True;

      if taNDS.Active=False then taNDS.Active:=True;
      taNds.FullRefresh;

      fmAddDoc4.cxButtonEdit1.Tag:=quDocsRSelIDCLI.AsInteger;
      fmAddDoc4.cxButtonEdit1.EditValue:=quDocsRSelIDCLI.AsInteger;
      fmAddDoc4.cxButtonEdit1.Text:=quDocsRSelNAMECL.AsString;
      fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxButtonEdit1.Enabled:=False;

      if quMHAll.Active=False then quMHAll.Active:=True;
      quMHAll.FullRefresh;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quDocsRSelIDSKL.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quDocsRSelNAMEMH.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsRSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsRSelNAMEMH.AsString;

      if quMHAll.Locate('ID',CurVal.IdMH,[]) then
      begin
        fmAddDoc4.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc4.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end else
      begin
        fmAddDoc4.Label15.Caption:='��. ����: ';
        fmAddDoc4.Label15.Tag:=0;
      end;

      fmAddDoc4.cxLabel1.Enabled:=False;
      fmAddDoc4.cxLabel2.Enabled:=False;
      fmAddDoc4.cxLabel3.Enabled:=False;
      fmAddDoc4.cxLabel4.Enabled:=False;
      fmAddDoc4.cxLabel5.Enabled:=False;
      fmAddDoc4.cxLabel6.Enabled:=False;
      fmAddDoc4.N1.Enabled:=False;
      fmAddDoc4.cxButton1.Enabled:=False;

      fmAddDoc4.ViewDoc4.OptionsData.Editing:=False;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;
      fmAddDoc4.ViewTara.OptionsData.Editing:=False;
      fmAddDoc4.ViewTara.OptionsData.Deleting:=False;

      CloseTa(fmAddDoc4.taSpec);
      CloseTa(fmAddDoc4.taTara);

      fmAddDoc4.acSaveDoc.Enabled:=False;

      IDH:=quDocsRSelID.AsInteger;

      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecRSel.Active:=True;

      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        with fmAddDoc4 do
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=quSpecRSelID.AsInteger;
          taSpecIdGoods.AsInteger:=quSpecRSelIDCARD.AsInteger;
          taSpecNameG.AsString:=quSpecRSelNAMEC.AsString;
          taSpecIM.AsInteger:=quSpecRSelIDM.AsInteger;
          taSpecSM.AsString:=quSpecRSelSM.AsString;
          taSpecQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
          taSpecPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
          taSpecSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
          taSpecPriceR.AsFloat:=quSpecRSelPRICER.AsFloat;
          taSpecSumR.AsFloat:=quSpecRSelSUMR.AsFloat;
          taSpecINds.AsInteger:=quSpecRSelIDNDS.AsInteger;
          taSpecSNds.AsString:=quSpecRSelNAMENDS.AsString;
          taSpecRNds.AsFloat:=quSpecRSelSUMNDS.AsFloat;
          taSpecSumNac.AsFloat:=quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat;
          taSpecProcNac.AsFloat:=0;
          if quSpecRSelSUMIN.AsFloat<>0 then
            taSpecProcNac.AsFloat:=RoundEx((quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat)/quSpecRSelSUMIN.AsFloat*10000)/100;
          if quSpecRSelKM.AsFloat>0 then taSpecKM.AsFloat:=quSpecRSelKM.AsFloat
          else
          begin
            taSpecKM.AsFloat:=prFindKM(quSpecRSelIDM.AsInteger);
          end;
          taSpecTCard.AsInteger:=quSpecRSelTCard.AsInteger;
          taSpecOper.AsInteger:=quSpecRSelOPER.AsInteger;
          taSpec.Post;
        end;
        quSpecRSel.Next;
      end;

      quSpecRSel.Active:=False;

      quTaraR.Active:=False;
      quTaraR.ParamByName('IDH').AsInteger:=IDH;
      quTaraR.Active:=True;

      quTaraR.First;
      while not quTaraR.Eof do
      begin
        with fmAddDoc4 do
        begin
          taTara.Append;
          taTaraNum.AsInteger:=quTaraRNUM.AsInteger;
          taTaraIdCard.AsInteger:=quTaraRIDCARD.AsInteger;
          taTaraNameG.AsString:=quTaraRNAMEC.AsString;
          taTaraQuant.AsFloat:=quTaraRQUANT.AsFloat;
          taTaraPriceIn.AsFloat:=quTaraRPRICEIN.AsFloat;
          taTaraSumIn.AsFloat:=quTaraRSUMIN.AsFloat;
          taTaraPriceUch.AsFloat:=quTaraRPRICEUCH.AsFloat;
          taTaraSumUch.AsFloat:=quTaraRSUMUCH.AsFloat;
          taTaraIM.AsInteger:=quTaraRIDM.AsInteger;
          taTaraKM.AsFloat:=quTaraRKM.asfloat;
          taTaraSM.AsString:=quTaraRSM.asString;
          taTaraSumNac.AsFloat:=quTaraRSUMUCH.AsFloat-quTaraRSUMIN.AsFloat;
          if abs(quTaraRSUMIN.AsFloat)>0.01 then
          begin
            taTaraProcNac.AsFloat:=(quTaraRSUMUCH.AsFloat-quTaraRSUMIN.AsFloat)/quTaraRSUMIN.AsFloat*100;
          end;
          taTara.Post;
        end;
        quTaraR.Next;
      end;
      quTaraR.Active:=False;

      prAllViewOn;

      fmAddDoc4.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsReal.ViewDocsRDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsRSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsReal.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsRSel.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsReal.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumInDoc,rSumTDoc:Real;
begin
//������������
  with dmORep do
  with dmO do
  begin
    if not CanDo('prOnDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsRSelDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsRSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsRSelNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

          //������� ��� ������ ���� ���� �� ������ ������ �� ������� ��������� � �� ������� � �� �������, ������� ��� ���� �� �����

          IDH:=quDocsRSelID.AsInteger;

          //����� �������������� ����� ��������� �� - � ������� ����� - �.�. ��� ������.

          quSpecRSel.Active:=False;
          quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecRSel.Active:=True;

          quSpecRSel.First;
          while not quSpecRSel.Eof do
          begin
            if quSpecRSelQUANT.AsFloat<0 then
            begin //������
              if quSpecRSelPRICEIN.AsFloat<0.01 then
              begin
                showmessage('������������ �������� ���������� - ������������ ����. (��� '+IntToStr(quSpecRSelIDCARD.AsInteger)+')');
                quSpecRSel.Active:=False;
                prButtonSet(True);
                exit;
              end;
            end;
            quSpecRSel.Next;
          end;
          quSpecRSel.Active:=False;

          prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPart.ParamByName('DTYPE').AsInteger:=8;
          prDelPart.ExecProc;

          //����������� ��� ������
          prOn(IDH,quDocsRSelIDSKL.AsInteger,TRUNC(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDCLI.AsInteger,rSumInDoc,rSumTDoc);

          //�������� ������
          quDocsRSel.Edit;
          quDocsRSelSUMIN.AsFloat:=rSumInDoc;
          quDocsRSelSUMTAR.AsFloat:=rSumTDoc;
          quDocsRSelIACTIVE.AsInteger:=1;
          quDocsRSel.Post;
          quDocsRSel.Refresh;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsReal.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOffDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsRSelDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsRSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsRSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin

          if prTOFind(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsRSelNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=8;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('DTYPE').AsInteger:=8;
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
            prDelPartOut.ExecProc;

            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=8;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            // 4 - �������� ������
            quDocsRSel.Edit;
            quDocsRSelIACTIVE.AsInteger:=0;
            quDocsRSel.Post;
            quDocsRSel.Refresh;
          end;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsReal.Timer1Timer(Sender: TObject);
begin
  if bClearDocR=True then begin StatusBar1.Panels[0].Text:=''; bClearDocR:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocR:=True;
end;

procedure TfmDocsReal.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
   { if LevelDocsR.Visible then
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='����������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsReal.Caption:='����������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsR.Visible:=False;
      LevelCardsVn.Visible:=True;

      ViewCardsR.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInCard.Active:=True;
      ViewCardsR.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsReal.Caption:='���������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsR.Visible:=True;
      LevelCardsVn.Visible:=False;

      ViewDocsR.BeginUpdate;
      quDocsRSel.Active:=False;
      quDocsRSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsRSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsRSel.Active:=True;
      ViewDocsR.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocsReal.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsReal.acPrint1Execute(Sender: TObject);
begin
//������ �������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      frquSpecRSel.DataSet:=dmORep.quSpecRSel;

      frRepDocsReal.LoadFromFile(CurDir + 'ttn12.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsReal.ReportName:='��������� - ���������� �� �������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.acCopyExecute(Sender: TObject);
var Par:Variant;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=4;
    par[1]:=quDocsRSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=8;
      taHeadDocId.AsInteger:=quDocsRSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsRSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsRSelIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsRSelNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsRSelIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsRSelNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsRSelSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsRSelSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=8;
        taSpecDocIdHead.AsInteger:=quSpecRSelIDHEAD.AsInteger;
        taSpecDocNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecRSelIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecRSelSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecRSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecRSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpecDocKm.AsFloat:=quSpecRSelKM.AsFloat;
        taSpecDocPriceUch1.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecDoc.Post;
      
        quSpecRSel.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsReal.acInsertDExecute(Sender: TObject);
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocR') then
        begin
          prAllViewOff;

          fmAddDoc4.Caption:='��������� �� ���������� : ��������������.';
          fmAddDoc4.cxTextEdit1.Text:=prGetNum(8,0);
          fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxTextEdit1.Tag:=0;

          fmAddDoc4.cxTextEdit2.Text:='';
          fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit1.Date:=Date;
          fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit2.Date:=Date;
          fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxCalcEdit1.EditValue:=0;
          fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=False;

          if taNDS.Active=False then taNDS.Active:=True;
            taNds.FullRefresh;;

          fmAddDoc4.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc4.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
          fmAddDoc4.cxButtonEdit1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxButtonEdit1.Enabled:=True;

          if quMHAll.Active=False then quMHAll.Active:=True;
            quMHAll.FullRefresh;

          fmAddDoc4.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc4.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
            fmAddDoc4.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc4.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc4.Label15.Caption:='��. ����: ';
            fmAddDoc4.Label15.Tag:=0;
          end;

          fmAddDoc4.cxLabel1.Enabled:=True;
          fmAddDoc4.cxLabel2.Enabled:=True;
          fmAddDoc4.cxLabel3.Enabled:=True;
          fmAddDoc4.cxLabel4.Enabled:=True;
          fmAddDoc4.cxLabel5.Enabled:=True;
          fmAddDoc4.cxLabel6.Enabled:=True;
          fmAddDoc4.N1.Enabled:=True;

          fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
          fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;

          fmAddDoc4.cxButton1.Enabled:=True;
          CloseTa(fmAddDoc4.taSpec);

          fmAddDoc4.ViewTara.OptionsData.Editing:=True;
          fmAddDoc4.ViewTara.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc4.taTara);

          fmAddDoc4.acSaveDoc.Enabled:=True;


          taSpecDoc.First;

          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecKM.AsFloat:=taSpecDocKm.AsFloat;
                taSpecQuant.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecPriceIn.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSumIn.AsFloat:=RoundVal(taSpecDocSumIn.AsFloat);
                taSpecPriceR.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSumR.AsFloat:=RoundVal(taSpecDocSumUch.AsFloat);
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=RoundVal(taSpecDocSumUch.AsFloat)-RoundVal(taSpecDocSumIn.AsFloat);
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                  taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpecTCard.AsInteger:=prTypeTC(taSpecDocIdCard.AsInteger);
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddDoc4.Show;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsReal.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsReal.Excel1Click(Sender: TObject);
begin
//������� � ������
  prNExportExel4(ViewDocsR);
end;

procedure TfmDocsReal.acPrintTTN13Execute(Sender: TObject);
begin
//������ ��� ��.��������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      CloseTa(taSpecRPrint);
      
      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        taSpecRPrint.Append;
        taSpecRPrintNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpecRPrintIdGoods.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpecRPrintNameG.AsString:=quSpecRSelNAMEC.AsString;
        taSpecRPrintIM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpecRPrintSM.AsString:=quSpecRSelSM.AsString;
        taSpecRPrintQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpecRPrintPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpecRPrintSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpecRPrintPriceR.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecRPrintSumR.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecRPrintSumNac.AsFloat:=quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat;
        taSpecRPrintProcNac.AsFloat:=0;
        if quSpecRSelSUMIN.AsFloat<>0 then
          taSpecRPrintProcNac.AsFloat:=RoundEx((quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat)/quSpecRSelSUMIN.AsFloat*10000)/100;
        if quSpecRSelKM.AsFloat>0 then taSpecRPrintKM.AsFloat:=quSpecRSelKM.AsFloat
        else
        begin
          taSpecRPrintKM.AsFloat:=prFindKM(quSpecRSelIDM.AsInteger);
        end;
        taSpecRPrintTCard.AsInteger:=quSpecRSelTCard.AsInteger;

        if quSpecRSelTCard.AsInteger=1 then
        taSpecRPrintMassa.AsString:=prFindMassaTC(quSpecRSelIDCARD.AsInteger);

        taSpecRPrint.Post;

        quSpecRSel.Next;
      end;

      frquSpecRSel.DataSet:=taSpecRPrint;

      frRepDocsReal.LoadFromFile(CurDir + 'ttn13_1.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsReal.ReportName:='��������� - ���������� �� �������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.acAddDocsExecute(Sender: TObject);
Var IDH,IDS:INteger;
    rSum:Real;
begin
// ������������ ��������� ��������� - �������� � �����������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;
    if MessageDlg('������������ ��������� �� �������� � �����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quSpecRSel1.Active:=False;
      quSpecRSel1.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel1.ParamByName('OPER').AsInteger:=1; //��������
      quSpecRSel1.Active:=True;
      if quSpecRSel1.RecordCount>0 then
      begin
        //��������� �������� ��������
        IDH:=GetId('DocOutB');
        quDOBHEAD.Active:=False;
        quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
        quDOBHEAD.Active:=True;

        quDOBHEAD.Append;
        quDOBHEADID.AsInteger:=IDH;
        quDOBHEADDATEDOC.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDOBHEADNUMDOC.AsString:=quDocsRSelNUMDOC.AsString;
        quDOBHEADDATESF.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDOBHEADNUMSF.AsString:='';
        quDOBHEADIDCLI.AsInteger:=0;
        quDOBHEADIDSKL.AsInteger:=quDocsRSelIDSKL.AsInteger;
        quDOBHEADSUMIN.AsFloat:=0;
        quDOBHEADSUMUCH.AsFloat:=0;
        quDOBHEADSUMTAR.AsFloat:=0;
        quDOBHEADSUMNDS0.AsFloat:=0;
        quDOBHEADSUMNDS1.AsFloat:=0;
        quDOBHEADSUMNDS2.AsFloat:=0;
        quDOBHEADPROCNAC.AsFloat:=0;
        quDOBHEADIACTIVE.AsInteger:=0;
        quDOBHEADOPER.AsString:='��';
        quDOBHEADCOMMENT.AsString:='�������� �������� � ����������.';
        quDOBHEAD.Post;

        quDOBSpec.Active:=False;
        quDobSpec.ParamByName('IDH').AsInteger:=IDH;
        quDobSpec.Active:=True;

        rSum:=0;
        IDS:=1;

        quSpecRSel1.First;
        while not quSpecRSel1.Eof do
        begin
          if quSpecRSel1QUANT.AsFloat<0 then
          begin

            quDobSpec.Append;
            quDOBSPECIDHEAD.AsInteger:=IDH;
            quDOBSPECID.AsInteger:=IDS;
            quDOBSPECSIFR.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quDOBSPECNAMEB.AsString:=quSpecRSel1NAMEC.AsString;
            quDOBSPECCODEB.AsString:='';
            quDOBSPECKB.AsFloat:=1;
            quDOBSPECDSUM.AsFloat:=0;
            quDOBSPECRSUM.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quDOBSPECQUANT.AsFloat:=quSpecRSel1QUANT.AsFloat*(-1);
            quDOBSPECIDCARD.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quDOBSPECIDM.AsInteger:=quSpecRSel1IDM.AsInteger;
            quDOBSPECKM.AsFloat:=quSpecRSel1KM.AsFloat;
            quDOBSPECPRICER.AsFloat:=0;
            quDobSpec.Post;

            inc(IDS);

            rSum:=rSum+quSpecRSel1SUMIN.AsFloat*(-1);
          end;

          quSpecRSel1.Next;
        end;

        quDobHead.Edit;
        quDOBHEADSUMIN.AsFloat:=rSum;
        quDOBHEADSUMUCH.AsFloat:=rSum;
        quDobHead.Post;

        quDobHead.Active:=False;
        quDOBSpec.Active:=False;
      end;
      quSpecRSel1.Active:=False;

      quSpecRSel1.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel1.ParamByName('OPER').AsInteger:=2; //�����������
      quSpecRSel1.Active:=True;
      if quSpecRSel1.RecordCount>0 then
      begin
        IDH:=GetId('DocAct');

        quDocsActsId.Active:=False;
        quDocsActsId.ParamByName('IDH').AsInteger:=IDH;
        quDocsActsId.Active:=True;

        quDocsActsId.Append;
        quDocsActsIdID.AsInteger:=IDH;
        quDocsActsIdDATEDOC.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDocsActsIdNUMDOC.AsString:=quDocsRSelNUMDOC.AsString;
        quDocsActsIdIDSKL.AsInteger:=quDocsRSelIDSKL.AsInteger;
        quDocsActsIdSUMIN.AsFloat:=0;
        quDocsActsIdSUMUCH.AsFloat:=0;
        quDocsActsIdOPER.AsString:='��';
        quDocsActsIdIACTIVE.AsInteger:=0;
        quDocsActsIdCOMMENT.AsString:='����������� �������� � ����������.';
        quDocsActsId.Post;

        quSpecAO.Active:=False;
        quSpecAO.ParamByName('IDH').AsInteger:=IDH;
        quSpecAO.Active:=True;

        quSpecAO.First;
        while not quSpecAO.Eof do quSpecAO.Delete;

        rSum:=0;
        IDS:=1;

        quSpecRSel1.First;
        while not quSpecRSel1.Eof do
        begin
          if quSpecRSel1QUANT.AsFloat<0 then
          begin
            quSpecAO.Append;
            quSpecAOIDHEAD.AsInteger:=IDH;
            quSpecAOID.AsInteger:=IDS;
            quSpecAOIDCARD.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quSpecAOQUANT.AsFloat:=quSpecRSel1QUANT.AsFloat*(-1);
            quSpecAOIDM.AsInteger:=quSpecRSel1IDM.AsInteger;
            quSpecAOKM.AsFloat:=quSpecRSel1KM.AsFloat;
            quSpecAOPRICEIN.AsFloat:=quSpecRSel1PRICEIN.AsFloat;
            quSpecAOSUMIN.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quSpecAOPRICEINUCH.AsFloat:=quSpecRSel1PRICEIN.AsFloat;
            quSpecAOSUMINUCH.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quSpecAOTCARD.AsInteger:=quSpecRSel1TCARD.AsInteger;
            quSpecAO.Post;

            inc(IDS);

            rSum:=rSum+quSpecRSel1SUMIN.AsFloat*(-1);
          end;
          quSpecRSel1.Next;
        end;

        quDocsActsId.Edit;
        quDocsActsIdSUMIN.AsFloat:=rSum;
        quDocsActsIdSUMUCH.AsFloat:=rSum;
        quDocsActsId.Post;


        quSpecAO.Active:=False;
        quDocsActsId.Active:=False;
      end;
    end;
  end;
end;

end.
