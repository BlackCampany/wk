unit PreFF1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, RxGIF, Menus, cxLookAndFeelPainters, cxButtons, dxfBackGround,
  dxfShapedForm;

type
  TfmPre_Shape = class(TForm)
    Edit1: TcxTextEdit;
    Panel2: TPanel;
    cxButton10: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton14: TcxButton;
    ShapedForm: TdxfShapedForm;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPre_Shape: TfmPre_Shape;

implementation

uses Un1, Dm, MainFF;

{$R *.dfm}

procedure TfmPre_Shape.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var bCh:Byte;
    iShift:Integer;
begin
  bCh:=ord(Key);
  //121,27,18
  iShift:=0;
  if Shift=[ssShift]  then iShift:=1;
  if Shift=[ssCtrl]   then iShift:=2;
  if Shift=[ssAlt]  then iShift:=3;

  if iShift>0 then StrPre:='' else
  begin
    if bCh = 13 then
    begin //���� ����������
      if StrPre='' then StrPre:=SOnlyDigit(Edit1.Text);
      If FindPers(StrPre) then
      begin
        //��� ������ ������������ ����������
        fmMainFF.CreateViewPers;
        StrPre:='';
        close;
      end
      else
      begin
        StrPre:='';
        Person.Id:=0;
        Person.Name:='';
      end;
      Edit1.Text:='';
    end
    else //�����
      StrPre:=StrPre+Chr(bCh);
  end;
end;

procedure TfmPre_Shape.FormCreate(Sender: TObject);
begin
  Edit1.Text:='';
  ShapedForm.Picture.LoadFromFile(CurDir+'Pre.bmp');
  ShapedForm.Visible:=True;

{  if CommonSet.MReader=1 then
  begin
    Panel1.Visible:=False;
    fmPre.ClientHeight:=300;
  end else
  begin
    Panel1.Visible:=True;
    fmPre.ClientHeight:=404;
  end;}
end;

procedure TfmPre_Shape.cxButton15Click(Sender: TObject);
begin
  Edit1.Text:='';
end;

procedure TfmPre_Shape.cxButton14Click(Sender: TObject);
begin
//��
  if StrPre='' then StrPre:=SOnlyDigit(Edit1.Text);
  If FindPers(StrPre) then
  begin
        //��� ������ ������������ ����������
    fmMainFF.CreateViewPers;
    StrPre:='';
    close;
  end
  else
  begin
    Person.Id:=0;
    Person.Name:='';
  end;
  StrPre:='';
  Edit1.Text:='';
  Edit1.SetFocus;
end;

procedure TfmPre_Shape.cxButton10Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'0';
end;

procedure TfmPre_Shape.cxButton1Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'1';
end;

procedure TfmPre_Shape.cxButton2Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'2';
end;

procedure TfmPre_Shape.cxButton3Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'1';
end;

procedure TfmPre_Shape.cxButton4Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'2';
end;

procedure TfmPre_Shape.cxButton5Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'3';
end;

procedure TfmPre_Shape.cxButton6Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'4';
end;

procedure TfmPre_Shape.cxButton7Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'5';
end;

procedure TfmPre_Shape.cxButton8Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'6';
end;

procedure TfmPre_Shape.cxButton9Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'7';
end;

procedure TfmPre_Shape.cxButton11Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'8';
end;

procedure TfmPre_Shape.cxButton12Click(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'9';
end;

procedure TfmPre_Shape.FormShow(Sender: TObject);
begin
  StrPre:='';
  Person.Id:=0;
  Person.Name:='';
  Edit1.Text:='';
  Edit1.SetFocus;
end;

end.
