unit Goods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls, Placemnt,
  ToolWin, ActnCtrls, ActnMenus, cxControls, cxContainer, cxTreeView,
  ExtCtrls, SpeedBar, cxSplitter, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxImageComboBox,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxTextEdit, StdCtrls, cxCalc,
  Menus, cxLookAndFeelPainters, cxButtons;

type
  TfmGoods = class(TForm)
    StatusBar1: TStatusBar;
    amG: TActionManager;
    FormPlacement1: TFormPlacement;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Panel1: TPanel;
    ClassTree: TcxTreeView;
    SpeedBar1: TSpeedBar;
    cxSplitter1: TcxSplitter;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Timer1: TTimer;
    ViewGoods: TcxGridDBTableView;
    LevelGoods: TcxGridLevel;
    GrGoods: TcxGrid;
    ViewGoodsID: TcxGridDBColumn;
    ViewGoodsPARENT: TcxGridDBColumn;
    ViewGoodsNAME: TcxGridDBColumn;
    ViewGoodsTTYPE: TcxGridDBColumn;
    ViewGoodsIMESSURE: TcxGridDBColumn;
    ViewGoodsINDS: TcxGridDBColumn;
    ViewGoodsMINREST: TcxGridDBColumn;
    ViewGoodsLASTPRICEIN: TcxGridDBColumn;
    ViewGoodsLASTPRICEOUT: TcxGridDBColumn;
    ViewGoodsLASTPOST: TcxGridDBColumn;
    ViewGoodsIACTIVE: TcxGridDBColumn;
    ViewGoodsNAMESHORT: TcxGridDBColumn;
    ViewGoodsNAMENDS: TcxGridDBColumn;
    ViewGoodsPROC: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    acAddGoods: TAction;
    acEditGoods: TAction;
    acDelGoods: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    Action9: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewGoodsTCARD: TcxGridDBColumn;
    acTCard: TAction;
    SpeedItem5: TSpeedItem;
    acCardMove: TAction;
    SpeedItem6: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGoodsExecute(Sender: TObject);
    procedure acEditGoodsExecute(Sender: TObject);
    procedure acDelGoodsExecute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure ViewGoodsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ClassTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ClassTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewGoodsDblClick(Sender: TObject);
    procedure acTCardExecute(Sender: TObject);
    procedure acCardMoveExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGoods: TfmGoods;
  bClearClass:Boolean = False;
  bDr:Boolean = False;
implementation

uses dmOffice, Un1, AddClass, AddGoods, FindResult, AddDoc1, TCard,
  GoodsSel, AddDoc2, CardsMove, AddInv;

{$R *.dfm}

procedure TfmGoods.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ClassifExpand(nil,ClassTree,dmO.quClassTree,Person.Id,1);
  ClassTree.FullExpand;
  ClassTree.FullCollapse;
  Timer1.Enabled:=True;
  GrGoods.Align:=AlClient;
  ViewGoods.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';
end;

procedure TfmGoods.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmGoods.Action1Execute(Sender: TObject);
begin
  bAddSpec:=False;
  bAddSpecB:=False;
  bAddSpecB1:=False;
  bAddTSpec:=False;
  close;
end;

procedure TfmGoods.Action3Execute(Sender: TObject);
begin
 //�������������
end;

procedure TfmGoods.Action5Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
    StrWk:String;
begin
// �������� ��������  ������
  if not CanDo('prAddClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddClass.Caption:='���������� ������.';
  fmAddClass.label4.Caption:='���������� ������.';
  fmAddClass.cxTextEdit1.Text:='';

  fmAddClass.ShowModal;
  if fmAddClass.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('Class');

      taClass.Active:=False;
      taClass.Active:=True;
      taClass.Append;
      taClassID.AsInteger:=iId;
      taClassID_PARENT.AsInteger:=0;
      taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
      taClassITYPE.AsInteger:=1; //������������� �������
      taClass.Post;

      StrWk:=taClass.FieldByName('NAMECL').AsString;

      ClassTree.Items.BeginUpdate;
      TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      ClassTree.Items.AddChildObject(TreeNode,'', nil);
      ClassTree.Items.EndUpdate;

      with fmGoodsSel do
      begin
        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'',nil);
        ClassTree.Items.EndUpdate;
        ClassTree.Repaint;
        delay(10);
      end;

      for i:=0 To ClassTree.Items.Count-1 do
        if Integer(ClassTree.Items[i].Data) = iId then
        begin
          ClassTree.Items[i].Expand(False);
          ClassTree.Items[i].Selected:=True;
          ClassTree.Repaint;
          Break;
        end;

      taClass.Active:=False;
    end;
  end;
end;

procedure TfmGoods.Action6Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    CurNode : TTreeNode;
    iId,i,iParent:Integer;
    StrWk:String;
begin
//�������� ���������
  if not CanDo('prAddSubClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ���������� ���������.');
    exit;
  end;

  iParent:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iParent,[]) then
    begin
      fmAddClass.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.label4.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        iId:=GetId('Class');

        taClass.Active:=False;
        taClass.Active:=True;
        taClass.Append;
        taClassID.AsInteger:=iId;
        taClassID_PARENT.AsInteger:=iParent;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        StrWk:=taClass.FieldByName('NAMECL').AsString;

        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(CurNode,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'', nil);
        ClassTree.Items.EndUpdate;

        with fmGoodsSel do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) =iParent  then
            begin
              ClassTree.Items.BeginUpdate;
              TreeNode:=ClassTree.Items.AddChildObject(ClassTree.Items[i],StrWk,Pointer(iId));
              TreeNode.ImageIndex:=8;
              TreeNode.SelectedIndex:=7;
              ClassTree.Items.AddChildObject(TreeNode,'', nil);
              ClassTree.Items.EndUpdate;

              ClassTree.Repaint;
              Break;
            end;
        end;


        for i:=0 To ClassTree.Items.Count-1 do
          if Integer(ClassTree.Items[i].Data) = iId then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Items[i].Selected:=True;
            ClassTree.Repaint;
            Break;
          end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoods.Action7Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iId,i:Integer;
begin
// �������������
  if not CanDo('prEditClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������������.');
    exit;
  end;

  iId:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iId,[]) then
    begin
      fmAddClass.Caption:='�������������� ������ "'+taClassNAMECL.AsString+'"';
      fmAddClass.label4.Caption:='�������������� ������"'+taClassNAMECL.AsString+'"';
      fmAddClass.cxTextEdit1.Text:=taClassNAMECL.AsString;

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        taClass.Edit;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        CurNode.Text:=fmAddClass.cxTextEdit1.Text;

        with fmGoodsSel do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) = iId then
            begin
              ClassTree.Items[i].Text:=fmAddClass.cxTextEdit1.Text;
              ClassTree.Repaint;
              Break;
            end;
        end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoods.Action8Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iRes:Integer;
    i,iId:Integer;
begin
//�������
  if not CanDo('prDelClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������.');
    exit;
  end;

  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ "'+CurNode.Text+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prCanDelClass.ParamByName('IDCL').AsInteger:=Integer(CurNode.Data);
      prCanDelClass.ExecProc;
      iRes:=prCanDelClass.ParamByName('RESULT').AsInteger;

      if iRes=0 then
      begin //�������� ���������
        taClass.Active:=False;
        taClass.Active:=True;
        if taClass.Locate('ID',Integer(CurNode.Data),[]) then
        begin
          iId:=Integer(CurNode.Data);
          taClass.Delete;
          CurNode.Delete;
          ClassTree.Repaint;

          with fmGoodsSel do
          begin
            for i:=0 To ClassTree.Items.Count-1 do
              if Integer(ClassTree.Items[i].Data)=iId then
              begin
                ClassTree.Items[i].Delete;
                ClassTree.Repaint;
                Break;
              end;
          end;
        end;
        taClass.Active:=False;
      end;

      if iRes=1 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ���� ���������.');
      end;

      if iRes=2 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ������ �� �����.');
      end;

    end;
  end;
end;

procedure TfmGoods.Timer1Timer(Sender: TObject);
begin
  if bClearClass=True then begin StatusBar1.Panels[0].Text:=''; bClearClass:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearClass:=True;
end;

procedure TfmGoods.ClassTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if dmO=nil then exit;
  with dmO do
  begin
    quCardsSel.Active:=False;
    quCardsSel.ParamByName('PARENTID').AsInteger:=Integer(Node.Data);
    quCardsSel.Active:=True;
  end;
end;

procedure TfmGoods.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewGoods.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmGoods.acAddGoodsExecute(Sender: TObject);
Var iId:Integer;
begin
// �������� �����
  if not CanDo('prAddGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if ClassTree.Selected=Nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;

  with dmO do
  begin
    iId:=GetId('GD');
    while iId<100000 do
    begin
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then break
      else iId:=GetId('GD');
    end;
    quGdsFind.Active:=False;
    if iId=100000 then
    begin
      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
      exit;
    end;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;

    with fmAddGood do
    begin
      Caption:='���������� ������.';
      Label1.Caption:='���������� ������ � ������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=''; cxTextEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=False;
      cxLookUpComboBox1.EditValue:=2; cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=2; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=1; cxLookUpComboBox3.Properties.ReadOnly:=False;

      cxCalcEdit1.Value:=0; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=0; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=1; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=False;

      tBar.Active:=False;
      tBar.CreateDataSet;

      tEU.Active:=False;
      tEU.CreateDataSet;

      if quCardsSel.Eof=False then
      begin
        cxTextEdit1.Text:=quCardsSelNAME.AsString;
        cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;
        cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger;
        cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger;
        cxCalcEdit1.Value:=quCardsSelMINREST.AsFloat;
        cxCheckBox1.EditValue:=quCardsSelTTYPE.AsInteger;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
        quGoodsEU.Active:=True;
        quGoodsEu.First;
        while not quGoodsEU.Eof do
        begin
          tEu.Append;
          tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
          tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
          tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
          tEU.Post;

          quGoodsEU.Next;
        end;
      end;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      //��������
      iId:=fmAddGood.cxSpinEdit1.Value;
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then
      begin
        quCardsSel.Append;
        quCardsSelID.AsInteger:=iId;
        quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
        quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
        quCardsSelTTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
        quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
        quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
        quCardsSelMINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
        quCardsSelIACTIVE.AsInteger:=1;
        quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
        quCardsSel.Post;

        with fmAddGood do
        begin
          quBars.Active:=False;
          quBars.ParamByName('IID').AsInteger:=iId;
          quBars.Active:=True;

          tBar.First;
          while not tBar.Eof do
          begin
            if tBariStatus.AsInteger=1 then
            begin
              quBars.Append;
              quBarsBAR.AsString:=tBarBarNew.AsString;
              quBarsGOODSID.AsInteger:=iId;
              quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
              quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
              quBarsPRICE.AsFloat:=0;
              quBars.Post;
            end;
            tBar.Next;
          end;
          quBars.Active:=False;

          quGoodsEU.Active:=False;
          quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
          quGoodsEU.Active:=True;
          quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

          tEU.first;
          while not tEu.Eof do
          begin
            quGoodsEU.Append;
            quGoodsEUGOODSID.AsInteger:=iId;
            quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
            quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
            quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
            quGoodsEU.Post;

            tEU.next;
          end;
          tEu.Active:=False;
        end;
        quCardsSel.Refresh;
      end else
      begin
        Showmessage('�������� � ����� ����� '+IntToStr(iId)+' ��� ����������, ���������� ����������.');
      end;
      quGdsFind.Active:=False;
    end;

    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoods.acEditGoodsExecute(Sender: TObject);
Var IId:Integer;
begin
//������������� �����
  if not CanDo('prEditGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    iId:=quCardsSelID.AsInteger;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;

    with fmAddGood do
    begin
      Caption:='�������������� ������.';
      Label1.Caption:='������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=quCardsSelNAME.AsString; cxTextEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=True;
      cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger;
      cxCalcEdit1.Value:=quCardsSelMINREST.AsFloat; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=quCardsSelTTYPE.AsInteger; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=quCardsSelIACTIVE.AsInteger; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=True;

      tBar.Active:=False;
      tBar.CreateDataSet;

      tEu.Active:=False;
      tEU.CreateDataSet;

      quBars.Active:=False;
      quBars.ParamByName('IID').AsInteger:=iID;
      quBars.Active:=True;
      quBars.First;
      while not quBars.Eof do
      begin
        tBar.Append;
        tBarBarNew.AsString:=quBarsBAR.AsString;
        tBarBarOld.AsString:=quBarsBAR.AsString;
        tBarQuant.AsFloat:=quBarsQUANT.AsFloat;
        tBariStatus.AsInteger:=1;
        tBar.Post;

        quBars.Next;
      end;

      quBars.Active:=False;

      quGoodsEU.Active:=False;
      quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
      quGoodsEU.Active:=True;
      quGoodsEu.First;
      while not quGoodsEU.Eof do
      begin
        tEu.Append;
        tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
        tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
        tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
        tEU.Post;

        quGoodsEU.Next;
      end;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
        //����������
      quCardsSel.Edit;
      quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
      quCardsSelTTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
      quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
      quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
      quCardsSelMINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
      quCardsSelIACTIVE.AsInteger:=fmAddGood.cxCheckBox2.EditValue;
      quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
      quCardsSel.Post;

      with fmAddGood do
      begin
        quBars.Active:=False;
        quBars.ParamByName('IID').AsInteger:=iId;
        quBars.Active:=True;

        quBars.First;
        while not quBars.Eof do quBars.Delete;

        tBar.First;
        while not tBar.Eof do
        begin
          if tBariStatus.AsInteger=1 then
          begin
            quBars.Append;
            quBarsBAR.AsString:=tBarBarNew.AsString;
            quBarsGOODSID.AsInteger:=iId;
            quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
            quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
            quBarsPRICE.AsFloat:=0;
            quBars.Post;
          end;
          tBar.Next;
        end;
        quBars.Active:=False;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
        quGoodsEU.Active:=True;
        quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

        tEU.first;
        while not tEu.Eof do
        begin
          quGoodsEU.Append;
          quGoodsEUGOODSID.AsInteger:=iId;
          quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
          quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
          quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
          quGoodsEU.Post;

          tEU.next;
        end;
        tEu.Active:=False;
      end;

      quCardsSel.Refresh;
    end;
    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoods.acDelGoodsExecute(Sender: TObject);
begin
// ������� �����
  if not CanDo('prDelGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    if MessageDlg('�� ������������� ������ ������� �����: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� �������� ������: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
        quGoodsEU.Active:=True;
        quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

        quCardsSel.Delete;
        quCardsSel.Refresh;

      end;
    end;
  end;
end;

procedure TfmGoods.Action9Execute(Sender: TObject);
begin
// �����������
  showmessage('�������� ����������� ������� ��� �������� � ���������� �� ������ � ������ ������.');
end;

procedure TfmGoods.ViewGoodsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if CanDo('prMoveGoods') then
  begin bDr:=True;  end;
  if CanDo('prTransMenu') then
  begin bDM:=True;  end;
  if CanDo('prEditInvSpec') then
  begin bDInv:=True;  end;
end;

procedure TfmGoods.ClassTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmGoods.ClassTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if not CanDo('prMoveGoods') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if bDr then
  begin
    bDr:=False;
    sGr:=ClassTree.DropTarget.Text;
    iGr:=Integer(ClassTree.DropTarget.data);
    iCo:=ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              quCardsSel.Edit;
              quCardsSelPARENT.AsInteger:=iGr;
              quCardsSel.Post;
            end;
          end;
          quCardsSel.FullRefresh;
        end;
      end;
    end;
  end;
end;

procedure TfmGoods.ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,ClassTree,dmO.quClassTree,Person.Id,1);
  end;
end;

procedure TfmGoods.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where NAME like ''%'+cxTextEdit1.Text+'%''');
      quFind.Active:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          quCardsSel.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.cxButton2Click(Sender: TObject);
//�� ����
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where ID ='+IntToStr(iCode));
      quFind.Active:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          quCardsSel.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.cxButton3Click(Sender: TObject);
Var i:INteger;
begin
{
SELECT c.ID,c.PARENT,c.NAME
FROM OF_CARDS c
left Join OF_BARCODE b on b.GOODSID=c.ID
where b.BAR like '%2222%'
}
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT c.ID,c.PARENT,c.NAME');
      quFind.SelectSQL.Add('FROM OF_CARDS c');
      quFind.SelectSQL.Add('left Join OF_BARCODE b on b.GOODSID=c.ID');
      quFind.SelectSQL.Add('where b.BAR like ''%'+cxTextEdit1.Text+'%''');
      quFind.Active:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          quCardsSel.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.ViewGoodsDblClick(Sender: TObject);
Var iMax,iM:INteger;
    sM:String;
    kM:Real;
//    kBrutto:Real;
//    iCurDate:Integer;
begin
  if bAddSpec or bAddSpecB or bAddSpecB1 or bAddSpecInv then
  begin //�������� � ������������ ���������
    with dmO do
    begin
      if bAddSpec then
      begin
        if fmAddDoc1.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            iMax:=1;
            fmAddDoc1.taSpec.First;
            if not fmAddDoc1.taSpec.Eof then
            begin
              fmAddDoc1.taSpec.Last;
              iMax:=fmAddDoc1.taSpecNum.AsInteger+1;
            end;

            with fmAddDoc1 do
            begin
              ViewDoc1.BeginUpdate;
              taSpec.Append;
              taSpecNum.AsInteger:=iMax;
              taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
              taSpecNameG.AsString:=quCardsSelNAME.AsString;
              taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
              taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
              taSpecQuant.AsFloat:=1;
              taSpecPrice1.AsCurrency:=0;
              taSpecSum1.AsCurrency:=0;
              taSpecPrice2.AsCurrency:=0;
              taSpecSum2.AsCurrency:=0;
              taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
              taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
              taSpecRNds.AsCurrency:=0;
              taSpecSumNac.AsCurrency:=0;
              taSpecProcNac.AsFloat:=0;
              taSpec.Post;
              ViewDoc1.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
        if fmAddDoc2.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            iMax:=1;
            fmAddDoc2.taSpecOut.First;
            if not fmAddDoc2.taSpecOut.Eof then
            begin
              fmAddDoc2.taSpecOut.Last;
              iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
            end;

            with fmAddDoc2 do
            begin
              ViewDoc2.BeginUpdate;
              taSpecOut.Append;
              taSpecOutNum.AsInteger:=iMax;
              taSpecOutIdGoods.AsInteger:=quCardsSelID.AsInteger;
              taSpecOutNameG.AsString:=quCardsSelNAME.AsString;
              taSpecOutIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
              taSpecOutSM.AsString:=quCardsSelNAMESHORT.AsString;
              taSpecOutQuant.AsFloat:=1;
              taSpecOutPrice1.AsCurrency:=0;
              taSpecOutSum1.AsCurrency:=0;
              taSpecOutPrice2.AsCurrency:=0;
              taSpecOutSum2.AsCurrency:=0;
              taSpecOutINds.AsInteger:=quCardsSelINDS.AsInteger;
              taSpecOutSNds.AsString:=quCardsSelNAMENDS.AsString;
              taSpecOutRNds.AsCurrency:=0;
              taSpecOutSumNac.AsCurrency:=0;
              taSpecOutProcNac.AsFloat:=0;
              taSpecOut.Post;
              ViewDoc2.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecB1 then
      begin
        if (taDobSpec.Active=True) and (taDobSpec.RecordCount>0) then
        begin
          prFindSM(quCardsSelIMESSURE.AsInteger,sM,iM); //����� � ��������

          taDobSpec.Edit;
          taDOBSPECCODEB.AsString:=quCardsSelID.AsString;
          taDOBSPECKB.AsFloat:=1;
          taDOBSPECIDCARD.AsInteger:=quCardsSelID.AsInteger;
          taDOBSPECIDM.AsInteger:=iM;
          taDOBSPECKM.AsFloat:=1;
          taDobSpec.Post;

          taDobSpec.Refresh;

          bAddSpecB1:=False;
          Close;
        end;
      end;
      if bAddSpecB then
      begin
        if taDobSpec.Active=True then
        begin
          prFindSM(quCardsSelIMESSURE.AsInteger,sM,iM); //����� � ��������

          taDobSpec.Append;
          taDobSPECIDHEAD.AsInteger:=quDocsOutBID.AsInteger;
          taDobSPECID.AsInteger:=GetId('SpecOutB');
          taDobSPECSIFR.AsInteger:=0;
          taDobSPECNAMEB.AsString:='';
          taDobSPECCODEB.AsString:=quCardsSelID.AsString;
          taDobSPECKB.AsFloat:=1;
          taDobSPECDSUM.AsFloat:=0;
          taDobSPECRSUM.AsFloat:=0;
          taDobSPECQUANT.AsFloat:=1;
          taDobSPECIDCARD.AsInteger:=quCardsSelID.AsInteger;
          taDobSPECIDM.AsInteger:=iM;
          taDobSPECKM.AsFloat:=1;
          taDobSPECPRICER.AsFloat:=0;
          taDobSpec.Post;

          taDobSpec.Refresh;
        end;
      end;
      if bAddSpecInv then
      begin
        if fmAddInv.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            iMax:=1;
            fmAddInv.taSpec.First;
            if not fmAddInv.taSpec.Eof then
            begin
              fmAddInv.taSpec.Last;
              iMax:=fmAddInv.taSpecNum.AsInteger+1;
            end;

            with fmAddInv do
            begin
              ViewInv.BeginUpdate;
              if taSpec.Locate('IdGoods',quCardsSelID.AsInteger,[])=False then
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                taSpecQuant.AsFloat:=0;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceUch.AsFloat:=0;
                taSpecSumUch.AsFloat:=0;
                taSpecQuantFact.AsFloat:=0;
                taSpecPriceInF.AsFloat:=0;
                taSpecSumInF.AsFloat:=0;
                taSpecPriceUchF.AsFloat:=0;
                taSpecSumUchF.AsFloat:=0;
                taSpecKm.AsFloat:=Km;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpecId_Group.AsInteger:=quCardsSelPARENT.AsInteger;
                taSpecNameGr.AsString:=prFindGrN(quCardsSelPARENT.AsInteger);
                taSpec.Post;
               end; 
              ViewInv.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
    end;
  end else
  begin
    if bAddTSpec then
    begin
      //������� ������� ��������� ����� � ������������ ��
      //��� ������� �� ���� �� ������� ��� ����� ������
     { with dmO do
      begin
        kBrutto:=0;
        iCurDate:=prDateToI(Date);
        quFindEU.Active:=False;
        quFindEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
        quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
        quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
        quFindEU.Active:=True;

        if quFindEU.RecordCount>0 then
        begin
          quFindEU.First;
          kBrutto:=quFindEUTO100GRAMM.AsFloat;
        end;

        quFindEU.Active:=False;
        fmTCard.ViewTSpec.BeginUpdate;
        quTSpec.Append;
        quTSpecIDC.AsInteger:=quTCardsIDCARD.AsInteger;
        quTSpecIDT.AsInteger:=quTCardsID.AsInteger;
        quTSpecIDCARD.AsInteger:=quCardsSelID.AsInteger;
        quTSpecCURMESSURE.AsInteger:=quCardsSelIMESSURE.AsInteger;
        quTSpecNETTO.AsFloat:=100;
        quTSpecBRUTTO.AsFloat:=100+kBrutto;
        quTSpecKNB.AsFloat:=kBrutto;
        quTSpec.Post;
        quTSpec.FullRefresh;
        quTSpec.Locate('IDCARD',quCardsSelID.AsInteger,[]);
        fmTCard.ViewTSpec.EndUpdate;
      end; }
    end else
    begin
      if ViewGoods.Controller.FocusedColumn.Name='ViewGoodsTCARD' then acTCard.Execute
      else acEditGoods.Execute;
    end;
  end;
end;

procedure TfmGoods.acTCardExecute(Sender: TObject);
Var iCurDate:INteger;
    kBrutto,rN:Real;
begin
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if not quCardsSel.Eof then
    begin
      quTCards.Active:=False;
      quTCards.ParamByName('IDGOOD').AsInteger:=quCardsSelID.AsInteger;
      quTCards.Active:=True;

      if quTCards.RecordCount=0 then
      begin
        fmTCard.Panel3.Visible:=False;
        quTSpec.Active:=False;
      end  else
      begin
        fmTCard.Panel3.Visible:=True;

        quTCards.Last;
        fmTCard.cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
        fmTCard.cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
        fmTCard.cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
        fmTCard.cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
        fmTCard.cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

        quTSpec.Active:=False;
        quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
        quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
        quTSpec.Active:=True;

        //������������� ������
        iCurDate:=prDateToI(Date);
        fmTCard.ViewTSpec.BeginUpdate;

        quTSpec.First;
        while not quTSpec.Eof do
        begin
          kBrutto:=0;
          quFindEU.Active:=False;
          quFindEU.ParamByName('GOODSID').AsInteger:=quTSpecIDCARD.AsInteger;
          quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
          quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
          quFindEU.Active:=True;

          if quFindEU.RecordCount>0 then
          begin
            quFindEU.First;
            kBrutto:=quFindEUTO100GRAMM.AsFloat;
          end;

          rN:=quTSpecNETTO.AsFloat;
          quFindEU.Active:=False;
          quTSpec.Edit;
          quTSpecBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
          quTSpecKNB.AsFloat:=kBrutto;
          quTSpec.Post;
          quTSpec.Next;
        end;
        quTSpec.FullRefresh;
        quTSpec.First;

        fmTCard.ViewTSpec.EndUpdate;
      end;

      fmTCard.Label6.Caption:= IntToStr(quCardsSelID.AsInteger);
      fmTCard.cxTextEdit1.Text:=quCardsSelNAME.AsString;
      fmTCard.cxTextEdit1.Tag:=quCardsSelID.AsInteger;

      fmTCard.ViewTSpec.OptionsData.Editing:=False;
      fmTCard.cxLabel1.Enabled:=False;
      fmTCard.cxLabel2.Enabled:=False;
      fmTCard.Image1.Enabled:=False;
      fmTCard.Image2.Enabled:=False;

      TK.Add:=False;
      TK.Edit:=False;



      fmTCard.Caption:='��������������� �����. '+IntToStr(quCardsSelID.AsInteger)+' '+quCardsSelNAME.AsString;
      fmTCard.Show;
    end else
    begin
      showmessage('�������� ������ ��� �������������� !!');
    end;
  end;
end;

procedure TfmGoods.acCardMoveExecute(Sender: TObject);
begin
  //�������� �� ������
  with dmO do if not quCardsSel.Eof then prPreShowMove(quCardsSelID.AsInteger,CommonSet.DateFrom,CommonSet.DateTo,CommonSet.IdStore,quCardsSelNAME.AsString);
  fmCardsMove.PageControl1.ActivePageIndex:=2;
  fmCardsMove.ShowModal;
end;

end.
