unit DiscDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ExtCtrls, ComCtrls, cxCurrencyEdit;

type
  TfmDiscDet = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    ViewDCDet: TcxGridDBTableView;
    LevelDCDet: TcxGridLevel;
    GridDCDet: TcxGrid;
    ViewDCDetNUMTABLE: TcxGridDBColumn;
    ViewDCDetQUESTS: TcxGridDBColumn;
    ViewDCDetENDTIME: TcxGridDBColumn;
    ViewDCDetDISCONT: TcxGridDBColumn;
    ViewDCDetNAME: TcxGridDBColumn;
    ViewDCDetPERCENT: TcxGridDBColumn;
    ViewDCDetTABSUM: TcxGridDBColumn;
    ViewDCDetNAME1: TcxGridDBColumn;
    ViewDCDetSUMPOS: TcxGridDBColumn;
    ViewDCDetITSUM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDiscDet: TfmDiscDet;

implementation

uses Un1, DmRnDisc, Period;

{$R *.dfm}

procedure TfmDiscDet.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDCDet.Align:=AlClient;
  ViewDCDet.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmDiscDet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDCDet.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDiscDet.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDiscDet.SpeedItem3Click(Sender: TObject);
begin
//
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);
  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;

      ViewDCDet.BeginUpdate;
      quDiscDetail.Active:=False;
      quDiscDetail.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quDiscDetail.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quDiscDetail.ParamByName('SBAR').AsString:=quDiscSelBARCODE.AsString;
      quDiscDetail.Active:=True;
      ViewDCDet.EndUpdate;

      fmDiscDet.Caption:='�������� �� ����� "'+quDiscSelBARCODE.AsString+'" �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);

      exit;
    end;
  end;
  fmPeriod.Release;

end;

end.
