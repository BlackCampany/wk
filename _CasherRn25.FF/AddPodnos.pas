unit AddPodnos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxCalc, cxImageComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, dxmdaset;

type
  TfmAddP = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GridSpecP: TcxGrid;
    ViewSpecP: TcxGridDBBandedTableView;
    LevelSpecP: TcxGridLevel;
    teSp: TdxMemData;
    dsteSp: TDataSource;
    teSpSIFR: TIntegerField;
    teSpNAME: TStringField;
    teSpPRICE: TFloatField;
    teSpQUANTITY: TFloatField;
    teSpSUMMA: TFloatField;
    ViewSpecPRecId: TcxGridDBBandedColumn;
    ViewSpecPSIFR: TcxGridDBBandedColumn;
    ViewSpecPNAME: TcxGridDBBandedColumn;
    ViewSpecPPRICE: TcxGridDBBandedColumn;
    ViewSpecPQUANTITY: TcxGridDBBandedColumn;
    ViewSpecPSUMMA: TcxGridDBBandedColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddP: TfmAddP;

implementation

uses Dm;

{$R *.dfm}

procedure TfmAddP.FormCreate(Sender: TObject);
begin
  GridSpecP.Align:=AlClient;
end;

end.
