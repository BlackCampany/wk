unit ModifFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ActnList, XPStyleActnCtrls, ActnMan, Menus, cxDataStorage;

type
  TfmModifFF = class(TForm)
    Panel1: TPanel;
    FormPlacement1: TFormPlacement;
    ViewMo: TcxGridDBTableView;
    LevelMo: TcxGridLevel;
    GridMo: TcxGrid;
    Button3: TcxButton;
    cxButton1: TcxButton;
    ViewMoSIFR: TcxGridDBColumn;
    ViewMoNAME: TcxGridDBColumn;
    ViewMoPARENT: TcxGridDBColumn;
    ViewMoPRICE: TcxGridDBColumn;
    ViewMoREALPRICE: TcxGridDBColumn;
    ViewMoIACTIVE: TcxGridDBColumn;
    ViewMoIEDIT: TcxGridDBColumn;
    Panel2: TPanel;
    Label1: TLabel;
    amMod: TActionManager;
    acSelMod: TAction;
    acExitMod: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewMoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitModExecute(Sender: TObject);
    procedure acSelModExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmModifFF: TfmModifFF;

implementation

uses Dm, Un1, MainFF;

{$R *.dfm}

procedure TfmModifFF.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewMo.RestoreFromIniFile(CurDir+GridIni,False);
end;

procedure TfmModifFF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMo.StoreToIniFile(CurDir+GridIni);
end;

procedure TfmModifFF.ViewMoCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
begin
  if (AViewInfo is TcxGridTableDataCellViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;
  ACanvas.Canvas.Brush.Bitmap := ABitmap;

  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmModifFF.cxButton1Click(Sender: TObject);
begin
  acSelMod.Execute;
end;

procedure TfmModifFF.Button3Click(Sender: TObject);
begin
  acExitMod.Execute;
end;

procedure TfmModifFF.FormShow(Sender: TObject);
begin
  GridMo.SetFocus;
end;

procedure TfmModifFF.acExitModExecute(Sender: TObject);
begin
  close;
end;

procedure TfmModifFF.acSelModExecute(Sender: TObject);
begin
  if (MaxMod-CountMod)>0 then
  begin
    with dmC do
    begin
      quCurSpecMod.Append;
      quCurSpecModSTATION.AsInteger:=CommonSet.Station;
      quCurSpecModID_TAB.AsInteger:=Tab.Id;
      quCurSpecModId_Pos.AsInteger:=quCurSpecID.AsInteger;
      quCurSpecModId.AsInteger:=Check.Max+1;
      quCurSpecModSifr.AsInteger:=quModifSIFR.AsInteger;
      quCurSpecModName.AsString:=quModifName.AsString;
      quCurSpecModQuantity.AsFloat:=quCurSpecQUANTITY.AsFloat;
      quCurSpecMod.Post;

      inc(Check.Max);
    end;
    inc(CountMod);
  end;
  Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';
  GridMo.SetFocus;
  if (MaxMod-CountMod)=0 then close;
end;

end.
