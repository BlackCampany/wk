unit BnSber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  ComObj, ActiveX, OleServer, cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit;

type
  TfmSber = class(TForm)
    Memo1: TcxMemo;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    Label1: TLabel;
    cxButton1: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;
Procedure prBnPrint;

var
  fmSber: TfmSber;
  Drv: OleVariant;
  vCh: Variant;
//  iMoneyBn:Integer;

implementation

uses Un1, UnCash, Dm;

{$R *.dfm}

procedure prBnPrint;
Var StrP:String;
    kDelay:Integer;
begin
//����� ����� ������ ������ ����� ���������� �� ����������
  if  BnStr>'' then
  begin
    if CommonSet.CashNum>0 then
    begin
      OpenNFDoc;
      while BnStr>'' do
      begin
        if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
        else
        begin
          if ord(BnStr[1])=$01 then
          begin
            try
              if CloseNFDoc=False then
              begin
                TestFp('PrintNFStr');
                CloseNFDoc;
              end;

              if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
              begin
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                CutDoc;
              end;

              delay(33);

              if OpenNFDoc=false then
              begin
                TestFp('PrintNFStr');
                OpenNFDoc;
              end;
            finally
              Delete(BnStr,1,1);
            end;
          end
          else
          begin
            if Pos(Chr($0D),BnStr)>0 then
            begin
              StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
              Delete(BnStr,1,Pos(Chr($0D),BnStr));
            end
            else
            begin
              if Pos(Chr($01),BnStr)>0 then
              begin
                StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
              end
              else
              begin
                StrWk:=BnStr;
                BnStr:='';
              end;
            end;
            PrintNFStr(StrWk);
          end;
        end;
      end;
      CloseNFDoc;

      if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
      begin
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        CutDoc;
      end;

      delay(33);
    end else
    begin
      if (pos('COM',CommonSet.PrePrintPort)>0)or(pos('LPT',CommonSet.PrePrintPort)>0) then //������ ������������ ����� �� ��������� �������
      begin
        with dmC do
        begin
          prDevOpen(CommonSet.PrePrintPort,0);

          BufPr.iC:=0;
          StrP:=CommonSet.PrePrintPort;
//          SelFont(10);

          while BnStr>'' do
          begin
            if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
            else
            begin
              if ord(BnStr[1])=$01 then
              begin
              //�������� �������� ������

                kDelay:=(BufPr.iC div 40)-10;
                if kDelay<0 then kDelay:=0;

                prWrBuf(StrP);
                prCutDoc(StrP,0);   //}
                delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));

                prDevClose(StrP,0);


                prDevOpen(CommonSet.PrePrintPort,0);
                BufPr.iC:=0;

//              CloseNFDoc;
//              CutDoc;
//              OpenNFDoc;

                Delete(BnStr,1,1);
              end
              else
              begin
                if Pos(Chr($0D),BnStr)>0 then
                begin
                  StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
                  Delete(BnStr,1,Pos(Chr($0D),BnStr));
                end
                else
                begin
                  if Pos(Chr($01),BnStr)>0 then
                  begin
                    StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                    Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
                  end
                  else
                  begin
                    StrWk:=BnStr;
                    BnStr:='';
                  end;
                end;
//              PrintNFStr(StrWk);
                PrintStrDev(StrP,StrWk);
              end;
            end;
          end;

//        CloseNFDoc;
          if BufPr.iC>0 then
          begin
            kDelay:=(BufPr.iC div 40)-10;
            if kDelay<0 then kDelay:=0;
            prWrBuf(StrP);
            prCutDoc(StrP,0);   //}
            delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
          end;
          prDevClose(StrP,0);
        end;
      end;
    end;
  end;
end;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmSber.FormShow(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    StrWk:String;
begin
//�������
  Memo1.Clear;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������.');
  tTime:=now;

  if IsOLEObjectInstalled('SBRFSRV.Server') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� �� ���������� !!!'+StrT);
    exit;
  end;

{  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ��������.');
  tTime:=now;
  Drv := CreateOleObject('SBRFSRV.Server');}
  Drv.Clear;
{  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);           }
  Str((iMoneyBn/100):9:2,StrWk); StrWk:=TrimStr(StrWk);
  Label1.Caption:='����� � ������ '+StrWk+'�.';
end;

procedure TfmSber.cxButton2Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk,StrWkBn:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��� (��������� ������) 4000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);

  iRes:=Drv.NFun(4000);
//  iRes:=0;

  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
//  StrWk:='111222333444555';

  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
//  StrWk:='01.01.2009';

  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');

  vCh:=Drv.GParam('Cheque');
{  vCh:='       ������� "������� ���"'+chr($0D)+chr($0A)+'      ������,��.��������,�.19,      '+
'           ���. 123-4567            '+chr($0D)+chr($0A)+
'                                    '+chr($0D)+chr($0A)+
'                                    '+chr($0D)+chr($0A)+
'29.02.08                       09:12'+chr($0D)+chr($0A)+
'           ������ ������            '+chr($0D)+chr($0A)+
'��������:                   00001603'+chr($0D)+chr($0A)+
'����� ������������:     164444444455'+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'����� ������� � ����                '+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'*********  ����� ��������  *********'+chr($0D)+chr($0A)+
'===================================='+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+chr($01);
}
  BnStr:= vCh;
  Memo1.Lines.Add(CommonSet.TypeFis);
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    StrWkBn:=BnStr;
    if Pos(Chr($01),StrWkBn)>0 then StrWkBn[Pos(Chr($01),StrWkBn)]:='`';
    Memo1.Lines.Add(StrWkBn);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton4Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ������� ������� 4006');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4006);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton3Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��������   1000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
//  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton5Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ���   4002');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton6Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �������� �� ���   4003 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4003);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;

procedure TfmSber.cxButton7Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �� ��������   1002 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;

end;


procedure TfmSber.cxButton9Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('�������� ��� �� ��������� � ����������� ������� (6000) ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(6000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton10Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ��������   3001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(3001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton11Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �����    7000 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton12Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ���������  7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmSber.cxButton1Click(Sender: TObject);
begin
  Close;
end;

end.
