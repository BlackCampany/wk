unit UnitBN;

interface

uses
  Windows, Messages, SysUtils, Classes;

Type TBNAnswer = record
     iErr,PointNum,IdTr:Integer;
     sErr,bnBar,sDate,sTime,sId,sAuthCode,sCardType,sNumTerm,sErrCode,sErrCode26:String;
     end;

Procedure prStartBnTr(CashNum,PointNum,IdTr:Integer;bnSum:Real;bnPath,bnBar:String; Var iErr:Integer; Var sErr:String);
Procedure prDelBnTr(bnPath:String;CashNum:INteger);
Procedure prTestAnswerBN(CashNum:Integer;bnPath:String;Var iErr,PointNum,IdTr:Integer; Var sErr,bnBar,sDate,sTime,sId,sAuthCode,sCardType,sNumTerm,sErrCode,sErrCode26:String);
Procedure prDelBnTrO(bnPath:String;CashNum:INteger);
Procedure prClearAnsw;


Var  Answ:TBNAnswer;

implementation

uses Un1;

Procedure prClearAnsw;
begin
  Answ.iErr:=0;
  Answ.PointNum:=0;
  Answ.IdTr:=0;
  Answ.sErr:='';
  Answ.bnBar:='';
  Answ.sDate:='';
  Answ.sTime:='';
  Answ.sId:='';
  Answ.sAuthCode:='';
  Answ.sCardType:='';
  Answ.sNumTerm:='';
  Answ.sErrCode:='';
  Answ.sErrCode26:='';
end;


Procedure prTestAnswerBN(CashNum:Integer;bnPath:String;Var iErr,PointNum,IdTr:Integer; Var sErr,bnBar,sDate,sTime,sId,sAuthCode,sCardType,sNumTerm,sErrCode,sErrCode26:String);
Var StrWk:String;
    f:TextFile;
    Buf:array[1..200] of char;
    Ch:Char;
    i:Integer;
begin
  for i:=1 to 200 do Buf[i]:=#$20;
  iErr:=0; sErr:='';
  if bnPath[length(bnPath)]<>'\' then bnPath:=bnPath+'\';
  StrWk:=IntToStr(CashNum);
  while length(StrWk)<3 do StrWk:='0'+StrWk;
  if FileExists(bnPath+'$int__o$.'+StrWk) then
  begin
    try
      assignfile(f,bnPath+'$int__o$.'+StrWk);
      Reset(f);
      for i:=1 to 197 do
      begin
        if EOF(f) then break;
        Read(f,Ch);
        Buf[i]:=Ch;
      end;
      CloseFile(f);

      //��� � ����� ������� - �����������

      //f2 - ���������� ��� ����������
      StrWk:='';
      for i:=2 to 5 do StrWk:=StrWk+Buf[i];
      IdTr:=StrToIntDef(StrWk,0);

      //f4 - ��� �����
      StrWk:='';
      for i:=7 to 25 do StrWk:=StrWk+Buf[i];
      bnBar:=StrWk;

      //f8 - ���� ��
      StrWk:='';
      for i:=82 to 87 do StrWk:=StrWk+Buf[i];
      sDate:=StrWk;

      //f9 - ����� ��
      StrWk:='';
      for i:=88 to 93 do StrWk:=StrWk+Buf[i];
      sTime:=StrWk;

      //f11 - 11	TransID	94	2	AS	ID ����������, ��� ������ (��. ���3)
      StrWk:='';
      for i:=95 to 96 do StrWk:=StrWk+Buf[i];
      sId:=StrWk;

      //f12 - 12	AuthCode	96	9	AS K	��� �����������, ��� ������ ��� offline ����������
      StrWk:='';
      for i:=97 to 105 do StrWk:=StrWk+Buf[i];
      sAuthCode:=StrWk;

      //f13 - 13	Msg	105	..16
      StrWk:='';
      for i:=106 to 121 do StrWk:=StrWk+Buf[i];
      sErr:=StrWk;

      //f14 - CardType	121	2
      StrWk:='';
      for i:=122 to 123 do StrWk:=StrWk+Buf[i];
      sCardType:=StrWk;

      //f17 - TrmID	127	8	AS	����� ���������
      StrWk:='';
      for i:=128 to 135 do StrWk:=StrWk+Buf[i];
      sNumTerm:=StrWk;

      //f19 - MerchN	139	..15	AS	����� ����� ������������
      StrWk:='';
      for i:=140 to 154 do StrWk:=StrWk+Buf[i];
      PointNum:=StrToIntDef(StrWk,0);

      //f20 - ErrorCode	154	1	AS	��� ������ (��.���5)
      StrWk:=Buf[155];
      sErrCode:=StrWk;

      //f26 - ErrorCode	154	1	AS	��� ������ (��.���5)
      StrWk:=Buf[196]+Buf[197];
      sErrCode26:=StrWk;


      if Buf[1]='A' then
      begin //��� ������ , ����� ��������� ���������
        iErr:=1;
      end;

      if Buf[1]='N' then
      begin //��� �����
        iErr:=2;
      end;

//sId,sAuthCode,sCardType,sNumTerm,sErrCode
    except
      CloseFile(f);
    end;
  end;
end;

Procedure prDelBnTrO(bnPath:String;CashNum:INteger);
Var StrWk:String;
begin
  if bnPath[length(bnPath)]<>'\' then bnPath:=bnPath+'\';
  StrWk:=IntToStr(CashNum);
  while length(StrWk)<3 do StrWk:='0'+StrWk;
  if FileExists(bnPath+'$int__o$.'+StrWk) then
  begin
    DeleteFile(bnPath+'$int__o$.'+StrWk);
  end;
end;

Procedure prDelBnTr(bnPath:String;CashNum:INteger);
Var StrWk:String;
begin
  if bnPath[length(bnPath)]<>'\' then bnPath:=bnPath+'\';
  StrWk:=IntToStr(CashNum);
  while length(StrWk)<3 do StrWk:='0'+StrWk;
  if FileExists(bnPath+'$int__i$.'+StrWk) then
  begin
    DeleteFile(bnPath+'$int__i$.'+StrWk);
  end;
end;

Procedure prStartBnTr(CashNum,PointNum,IdTr:Integer;bnSum:Real;bnPath,bnBar:String; Var iErr:Integer; Var sErr:String);
Var StrWk:String;
    iPos:Integer;
    bnDate,curDate:Integer;
    f:TextFile;
    Buf:array[1..200] of char;
    n,i:Integer;
    C,K:byte;
begin
  //��������� ������������ ����� ABG ����������
  //1 �������� ���� �������� �����, ������� bnBar
  prDelBnTr(bnPath,CashNum);
  prDelBnTrO(bnPath,CashNum);
  iErr:=0;
  sErr:='��� ��.';
  if bnBar[1]=';' then delete(bnBar,1,1);
  iPos:=Pos('?',bnBar);
  if iPos>0 then delete(bnBar,iPos,Length(bnBar)-iPos+1);
//  fmTestBn.Memo1.Lines.Add('('+bnBar+')');
  iPos:=Pos('=',bnBar)+1;
  StrWk:=Copy(bnBar,iPos,4);
  bnDate:=StrToIntDef(StrWk,0);
  if bnDate=0 then begin iErr:=1; sErr:='�������������� ���� �������� �����.'; exit; end;
  StrWk:=FormatDateTime('yymm',date);
  curDate:=StrToIntDef(StrWk,0);
  if bnDate<curDate then begin iErr:=2; sErr:='���� �������� ����� �����.'; exit; end;
  //�� ���� ����� ��� ���������, ���� ������
  if bnPath[length(bnPath)]<>'\' then bnPath:=bnPath+'\';
  if not DirectoryExists(bnPath) then begin iErr:=3; sErr:='����������� �� �������.'; exit; end;
  assignfile(f,bnPath+'bn.tmp');
  try
    Rewrite(f);
    for n:=1 to 198 do Buf[n]:=#$20; Buf[199]:=#$0D; Buf[200]:=#$0A;
    //��� ����������
    Buf[2]:='0';Buf[3]:='0';Buf[4]:='0';Buf[5]:='0';

    Buf[6]:='R'; //('K' - key entry /'R' - stripe reader

    //c 30-��    	Track2Data
    n:=Length(bnBar);
    if n>40 then n:=40;
    for i:=1 to n do Buf[29+i]:=bnBar[i];

    //�����
    StrWk:=IntToStr(RoundEx(bnSum*100));
    n:=Length(StrWk);
    if n>12 then n:=12;
    for i:=1 to n do Buf[69+i]:=StrWk[i];

    if Operation=0 then   //�������
    begin
      //��� 0 - sale
      Buf[94]:='0';
    end;

    if Operation=1 then   //�������
    begin
      //��� 4 - �������
      Buf[94]:='4';
    end;

    //����� �����
    StrWk:=IntToStr(CashNum);
    while length(StrWk)<3 do StrWk:='0'+StrWk;
    for i:=1 to 3 do Buf[123+i]:=StrWk[i];

    //
    Buf[127]:=#$70;
    Buf[156]:='1';

    //����������� ������
    C:=0;
    for i:=1 to 197 do
    begin
      k:=Ord(Buf[i]);
      C:=C XOR (K or((i-1) mod 7));
    end;
//    Label1.Caption:='���1 - '+IntToStr(C);
    StrWk:=IntToStr(C);
    Buf[198]:=StrWk[1];

    write(f,Buf);
    CloseFile(f);

    StrWk:=IntToStr(CashNum);
    while length(StrWk)<3 do StrWk:='0'+StrWk;

    RenameFile(bnPath+'bn.tmp',bnPath+'$int__i$.'+StrWk);

  Except
    CloseFile(f);
  end;
end;


end.
 