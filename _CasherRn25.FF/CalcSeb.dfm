object fmCalcSeb: TfmCalcSeb
  Left = 401
  Top = 216
  Width = 546
  Height = 381
  Caption = #1057#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 538
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 33
      Height = 13
      Caption = #1041#1083#1102#1076#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 24
      Top = 32
      Width = 74
      Height = 13
      Caption = #1041#1088#1091#1090#1090#1086' '#1085#1072' '#1076#1072#1090#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 128
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 128
      Top = 32
      Width = 39
      Height = 13
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 432
      Top = 40
      Width = 83
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 432
      Top = 8
      Width = 83
      Height = 25
      Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100
      TabOrder = 1
      OnClick = cxButton1Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrSeb: TcxGrid
    Left = 19
    Top = 112
    Width = 510
    Height = 185
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewSeb: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsSeb
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUM1'
          Column = ViewSebSUM1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSebIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCARD'
        Width = 38
      end
      object ViewSebNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 154
      end
      object ViewSebNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 49
      end
      object ViewSebCURMESSURE: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CURMESSURE'
        Visible = False
      end
      object ViewSebNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO'
      end
      object ViewSebBRUTTO: TcxGridDBColumn
        Caption = #1041#1088#1091#1090#1090#1086
        DataBinding.FieldName = 'BRUTTO'
      end
      object ViewSebKOEF: TcxGridDBColumn
        DataBinding.FieldName = 'KOEF'
        Visible = False
      end
      object ViewSebKNB: TcxGridDBColumn
        DataBinding.FieldName = 'KNB'
        Visible = False
      end
      object ViewSebPRICE1: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1083'.'#1087#1088#1080#1093#1086#1076#1072
        DataBinding.FieldName = 'PRICE1'
      end
      object ViewSebSUM1: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1094#1077#1085#1072#1084' '#1087'.'#1087'.'
        DataBinding.FieldName = 'SUM1'
        Styles.Content = dmO.cxStyle25
        Width = 63
      end
      object ViewSebPRICE2: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'PRICE2'
        Visible = False
      end
      object ViewSebSUM2: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'SUM2'
        Visible = False
      end
    end
    object LevelSeb: TcxGridLevel
      GridView = ViewSeb
    end
  end
  object taSeb: TClientDataSet
    Aggregates = <>
    FileName = 'CalcSeb.cds'
    Params = <>
    Left = 216
    Top = 144
    object taSebIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taSebCURMESSURE: TIntegerField
      FieldName = 'CURMESSURE'
    end
    object taSebNETTO: TFloatField
      FieldName = 'NETTO'
      DisplayFormat = '0.000'
    end
    object taSebBRUTTO: TFloatField
      FieldName = 'BRUTTO'
      DisplayFormat = '0.000'
    end
    object taSebNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taSebNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Size = 50
    end
    object taSebKOEF: TFloatField
      FieldName = 'KOEF'
    end
    object taSebKNB: TFloatField
      FieldName = 'KNB'
    end
    object taSebPRICE1: TCurrencyField
      FieldName = 'PRICE1'
      DisplayFormat = '0.00'
    end
    object taSebSUM1: TCurrencyField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object taSebPRICE2: TCurrencyField
      FieldName = 'PRICE2'
      DisplayFormat = '0.00'
    end
    object taSebSUM2: TCurrencyField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
  end
  object dsSeb: TDataSource
    DataSet = taSeb
    Left = 216
    Top = 200
  end
  object quMHAll: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 296
    Top = 144
    object quMHAllID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAllPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAllITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAllNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAllDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAllNAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsMHAll: TDataSource
    DataSet = quMHAll
    Left = 296
    Top = 200
  end
end
