object fmEditPosM: TfmEditPosM
  Left = 311
  Top = 283
  BorderStyle = bsDialog
  Caption = 'fmEditPosM'
  ClientHeight = 272
  ClientWidth = 371
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 16
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
  end
  object Label2: TLabel
    Left = 32
    Top = 40
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
  end
  object Label3: TLabel
    Left = 32
    Top = 64
    Width = 99
    Height = 13
    Caption = #1052#1061' '#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 253
    Width = 371
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 96
    Width = 329
    Height = 89
    Caption = #1041#1101#1082' '#1086#1092#1080#1089
    TabOrder = 1
    object Label4: TLabel
      Left = 64
      Top = 24
      Width = 69
      Height = 13
      Caption = #1050#1086#1076' '#1087#1077#1088#1077#1076#1072#1095#1080
    end
    object Label5: TLabel
      Left = 64
      Top = 56
      Width = 80
      Height = 13
      Caption = #1050#1086#1101#1092'. '#1087#1077#1088#1077#1076#1072#1095#1080
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 168
      Top = 20
      EditValue = 0
      Style.Shadow = True
      TabOrder = 0
      Width = 113
    end
    object cxCalcEdit2: TcxCalcEdit
      Left = 168
      Top = 52
      EditValue = 0
      Style.Shadow = True
      TabOrder = 1
      Width = 113
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 200
    Width = 371
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 136
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 248
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 96
    Top = 12
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit1'
    Width = 257
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 96
    Top = 36
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Style.Shadow = True
    TabOrder = 4
    Width = 73
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 192
    Top = 60
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMESTREAM'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = fmMenuCr.dsquStream
    Style.Shadow = True
    TabOrder = 5
    Width = 145
  end
end
