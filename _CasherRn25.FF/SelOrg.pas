unit SelOrg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, StdCtrls, cxButtons,
  ExtCtrls;

type
  TfmSelOrg = class(TForm)
    Panel1: TPanel;
    cxButton17: TcxButton;
    ViSelOrg: TcxGridDBTableView;
    LeSelOrg: TcxGridLevel;
    GrSelOrg: TcxGrid;
    ViSelOrgCASHNUM: TcxGridDBColumn;
    ViSelOrgFIS: TcxGridDBColumn;
    ViSelOrgIORG: TcxGridDBColumn;
    ViSelOrgDEPARTNAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelOrg: TfmSelOrg;

implementation

{$R *.dfm}

end.
