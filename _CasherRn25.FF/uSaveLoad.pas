unit uSaveLoad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ToolEdit, ComCtrls;

type
  TfmSaveLoad = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    FilenameEdit1: TFilenameEdit;
    ProgressBar1: TProgressBar;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSaveLoad: TfmSaveLoad;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmSaveLoad.BitBtn1Click(Sender: TObject);
var k:Integer;
    f:TextFile;
    strwk1:String;
begin
  if bsave then
  begin

  k:=0;
  AssignFile(f,FilenameEdit1.Text);

  try
  Rewrite(f);
  with dmC do
  begin
    taCf_Operations.First;
    ProgressBar1.Max:=taCf_Operations.RecordCount;
    ProgressBar1.Visible:=True;
    while not taCf_Operations.Eof do
    begin
      strwk:=IntToStr(taCF_OperationsKEY_CODE.AsInteger);
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsKEY_STATUS.AsInteger);
      StrWk:=StrWk+';'+taCF_OperationsKEY_CHAR.AsString;
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsID_OPERATIONS.AsInteger);
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsID_STATES.AsInteger);
      StrWk:=StrWk+';'+taCF_OperationsBARCODE.AsString;
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsKEY_POSITION.AsInteger);
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsID_CLASSIF.AsInteger);
      StrWk:=StrWk+';'+taCF_OperationsARTICUL.AsString;
      StrWk:=StrWk+';'+taCF_OperationsBAR.AsString;
      StrWk:=StrWk+';'+IntToStr(taCF_OperationsID_DEPARTS.AsInteger);
      StrWk:=StrWk+';';
      while pos(' ',strwk)>0 do delete(strwk,pos(' ',strwk),1);


      WriteLn(f,strwk);

      taCf_Operations.Next;
      inc(k);
      ProgressBar1.Position:=k;
      delay(10);
    end;
    delay(1000);
    ProgressBar1.Visible:=False;
  end;
  finally
    Flush(f);
    CloseFile(f);
  end;
  end
  else
  begin
    with dmC do
    begin
      ProgressBar1.Position:=0;
      taCf_Operations.Active:=False;
      taCf_Operations.Active:=True;
      delay(10);

      while not taCf_Operations.Eof do taCf_Operations.Delete;

      AssignFile(f,FilenameEdit1.Text);
      try
        Reset(f);
        while not eof(f) do
        begin
          readln(f,strwk);

          taCf_Operations.Append;

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsKEY_CODE.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsKEY_STATUS.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsKEY_CHAR.AsString:=strwk1;

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsID_OPERATIONS.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsID_STATES.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsBARCODE.AsString:=strwk1;

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsKEY_POSITION.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsID_CLASSIF.AsInteger:=StrToIntDef(strwk1,0);

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsARTICUL.AsString:=strwk1;

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsBAR.AsString:=strwk1;

          strwk1:=copy(strwk,1,pos(';',strwk)-1);
          delete(strwk,1,Pos(';',strwk));
          taCF_OperationsID_DEPARTS.AsInteger:=StrToIntDef(strwk1,0);

          taCf_Operations.Post;
          delay(10);
        end;
      finally
        taCf_Operations.Active:=False;
        taCf_Operations.Active:=True;
        delay(10);
{        if quCF_Classif.Active=True then
        begin
          quCF_Classif.Active:=False;
          delay(10);
          quCF_Classif.Active:=True;
          delay(10);
        end;
        if quCF_CardScla.Active=True then
        begin
          quCF_CardScla.Active:=False;
          delay(10);
          quCF_CardScla.Active:=True;
          delay(10);
        end;
        if quCF_Depart.Active=True then
        begin
          quCF_Depart.Active:=False;
          delay(10);
          quCF_Depart.Active:=True;
          delay(10);
        end;
        if quCF_Operations.Active=True then
        begin
          quCF_Operations.Active:=False;
          delay(10);
          quCF_Operations.Active:=True;
          delay(10);
        end;}
        showmessage('Загрузка завершена.');
        CloseFile(f);
      end
    end;
  end;
end;

end.
