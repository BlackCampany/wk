object fmMainRZD: TfmMainRZD
  Tag = 17
  Left = 377
  Top = -18
  Align = alTop
  BorderStyle = bsNone
  Caption = #1052#1086#1076#1091#1083#1100' '#1088#1072#1079#1076#1072#1095#1080
  ClientHeight = 956
  ClientWidth = 1272
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 564
    Top = 644
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Visible = False
    Height = 13
    Width = 473
  end
  object Panel1: TPanel
    Left = 8
    Top = 4
    Width = 781
    Height = 145
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 25
      Height = 11
      Caption = 'Label1'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
    end
    object GridP: TcxGrid
      Left = 2
      Top = 36
      Width = 777
      Height = 107
      Align = alBottom
      BevelKind = bkFlat
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object ViewP: TcxGridDBCardView
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = ViewPCellDblClick
        DataController.DataSource = dmR.dsquTabP
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.CardSizing = False
        OptionsCustomize.RowFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.CaptionSeparator = #0
        OptionsView.CardBorderWidth = 3
        OptionsView.CardWidth = 100
        OptionsView.CellTextMaxLineCount = 2
        OptionsView.RowCaptionAutoHeight = True
        OptionsView.RowCaptionEndEllipsis = True
        OptionsView.SeparatorColor = clWhite
        Styles.CardBorder = dmR.cxStyle23
        object ViewPID_TAB: TcxGridDBCardViewRow
          Caption = #8470' '
          DataBinding.FieldName = 'ID_TAB'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Styles.Content = dmR.cxStyle5
        end
        object ViewPSUMP: TcxGridDBCardViewRow
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'SUMP'
          PropertiesClassName = 'TcxCurrencyEditProperties'
        end
      end
      object LevelP: TcxGridLevel
        GridView = ViewP
      end
    end
  end
  object Panel2: TPanel
    Left = 1040
    Top = 612
    Width = 217
    Height = 397
    BevelInner = bvLowered
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label9: TLabel
      Left = 16
      Top = 28
      Width = 93
      Height = 20
      Alignment = taCenter
      Caption = #8470' '#1087#1086#1076#1085#1086#1089#1072
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 16
      Top = 188
      Width = 57
      Height = 49
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = cxButton2Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 80
      Top = 188
      Width = 57
      Height = 49
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 144
      Top = 188
      Width = 57
      Height = 49
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 16
      Top = 132
      Width = 57
      Height = 49
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton6: TcxButton
      Left = 80
      Top = 132
      Width = 57
      Height = 49
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 144
      Top = 132
      Width = 57
      Height = 49
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 16
      Top = 76
      Width = 57
      Height = 49
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton9: TcxButton
      Left = 80
      Top = 76
      Width = 57
      Height = 49
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton10: TcxButton
      Left = 144
      Top = 76
      Width = 57
      Height = 49
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton11: TcxButton
      Left = 16
      Top = 244
      Width = 57
      Height = 48
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton12: TcxButton
      Left = 80
      Top = 244
      Width = 57
      Height = 48
      Caption = ','
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      Visible = False
      OnClick = cxButton12Click
      Colors.Default = 12621940
      Colors.Normal = 14600630
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton13: TcxButton
      Left = 16
      Top = 348
      Width = 89
      Height = 41
      Caption = 'C'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = cxButton13Click
      Colors.Default = 8454143
      Colors.Normal = 13434879
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      LookAndFeel.Kind = lfFlat
    end
    object cxButton14: TcxButton
      Left = 16
      Top = 300
      Width = 89
      Height = 41
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = cxButton14Click
      Colors.Default = 8454143
      Colors.Normal = 13434879
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      Glyph.Data = {
        12040000424D12040000000000003600000028000000190000000D0000000100
        180000000000DC030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000FFFFFFFFFF
        FF00FFFFFF000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfFlat
    end
    object cxButton15: TcxButton
      Left = 128
      Top = 300
      Width = 81
      Height = 89
      Caption = 'Ok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      TabStop = False
      OnClick = cxButton15Click
      Colors.Default = 16750591
      Colors.Normal = 16764159
      Colors.Hot = 16763647
      Colors.Pressed = 16718591
      LookAndFeel.Kind = lfFlat
    end
    object CEdit4: TcxCalcEdit
      Left = 128
      Top = 20
      TabStop = False
      EditValue = 1
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Properties.QuickClose = True
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 8552960
      Style.Font.Height = -19
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.Shadow = True
      Style.ButtonTransparency = ebtHideInactive
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 14
      Width = 73
    end
  end
  object GridHK: TcxGrid
    Left = 12
    Top = 612
    Width = 1017
    Height = 398
    BevelInner = bvLowered
    BevelOuter = bvRaised
    BevelKind = bkFlat
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object ViewHK: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewHKCellClick
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.DataRowHeight = 45
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.Header = False
    end
    object LevelHK: TcxGridLevel
      GridView = ViewHK
    end
  end
  object Panel3: TPanel
    Left = 792
    Top = 2
    Width = 476
    Height = 57
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object Button1: TcxButton
      Left = 8
      Top = 5
      Width = 73
      Height = 44
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = Button1Click
      Colors.Default = 14600630
      Colors.Normal = 14600630
      Colors.Pressed = 15854048
      Glyph.Data = {
        22090000424D2209000000000000360400002800000023000000230000000100
        080000000000EC04000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFC
        FCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
        FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF000000000000FCFCFCFC
        FCFCFF000000000000FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF00FCFCFCFCFC
        FCFCFCFCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00
        FCFCFCFCFCFCFCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFF00FCFCFCFCFCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFF0400FFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFF0400FF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FCFF
        0400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00}
      LookAndFeel.Kind = lfFlat
    end
    object Button2: TcxButton
      Left = 88
      Top = 5
      Width = 73
      Height = 44
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button2Click
      Colors.Default = 14600630
      Colors.Normal = 14600630
      Colors.Pressed = 15854048
      Glyph.Data = {
        22090000424D2209000000000000360400002800000023000000230000000100
        080000000000EC04000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00FFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFF00
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFC
        FCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
        FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFF00FCFCFCFCFCFCFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFF000000000000FCFCFCFCFCFCFF000000000000FFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFF00FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFF0400FFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFCFCFCFCFCFCFCFF0400FFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFCFCFCFCFCFF0400FF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFCFCFCFCFCFF
        0400FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF00FCFCFCFCFC
        FCFCFF0400FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
        FCFCFCFCFCFF0400FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFFFF00FCFCFCFF0400FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FCFF0400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00}
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 272
      Top = 4
      Width = 100
      Height = 48
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1043#1050
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton1Click
      Colors.Default = 14600630
      Colors.Normal = 14600630
      Colors.Pressed = 14600630
      LookAndFeel.Kind = lfFlat
    end
    object RxClock2: TRxClock
      Left = 387
      Top = 4
      Width = 81
      Height = 49
      Color = 14138790
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = RxClock1Click
    end
  end
  object GridM: TcxGrid
    Left = 792
    Top = 62
    Width = 473
    Height = 543
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    LookAndFeel.Kind = lfFlat
    object ViewM: TcxGridDBCardView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewMCellClick
      OnCustomDrawCell = ViewMCustomDrawCell
      DataController.DataSource = dmR.dsMenu
      DataController.Options = []
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.CardSizing = False
      OptionsCustomize.RowFiltering = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.HideSelection = True
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.CaptionSeparator = #0
      OptionsView.CardBorderWidth = 3
      OptionsView.CardWidth = 145
      OptionsView.CellTextMaxLineCount = 2
      OptionsView.SeparatorColor = 8080936
      Styles.Background = dmR.cxStyle6
      Styles.Selection = dmR.cxStyle1
      Styles.CardBorder = dmR.cxStyle10
      object ViewMINFO: TcxGridDBCardViewRow
        DataBinding.FieldName = 'INFO'
        PropertiesClassName = 'TcxTextEditProperties'
        IsCaptionAssigned = True
      end
      object ViewMTREETYPE: TcxGridDBCardViewRow
        DataBinding.FieldName = 'TREETYPE'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.Focusing = False
        Options.IncSearch = False
        Options.ShowEditButtons = isebNever
        Options.ShowCaption = False
      end
      object ViewMSPRICE: TcxGridDBCardViewRow
        DataBinding.FieldName = 'SPRICE'
        PropertiesClassName = 'TcxTextEditProperties'
        IsCaptionAssigned = True
      end
    end
    object LevelM: TcxGridLevel
      GridView = ViewM
    end
  end
  object Panel6: TPanel
    Left = 12
    Top = 152
    Width = 773
    Height = 413
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 5
    object Panel7: TPanel
      Left = 646
      Top = 2
      Width = 125
      Height = 409
      Align = alRight
      Color = 14145495
      TabOrder = 0
      Visible = False
      object cxButton16: TcxButton
        Left = 10
        Top = 16
        Width = 109
        Height = 49
        Caption = #1047#1072#1082#1088#1099#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        TabStop = False
        OnClick = cxButton16Click
        Colors.Default = 12621940
        Colors.Normal = 14930369
        Colors.Pressed = 10843723
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
      end
      object cxButton17: TcxButton
        Left = 10
        Top = 76
        Width = 109
        Height = 49
        Caption = #1059#1076#1072#1083#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        TabStop = False
        OnClick = cxButton17Click
        Colors.Default = 12621940
        Colors.Normal = 14930369
        Colors.Pressed = 10843723
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
      end
      object cxButton18: TcxButton
        Left = 10
        Top = 136
        Width = 109
        Height = 49
        Caption = #1048#1079#1084'. '#1082#1086#1083'-'#1074#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        TabStop = False
        OnClick = cxButton18Click
        Colors.Default = 12621940
        Colors.Normal = 14930369
        Colors.Pressed = 10843723
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
      end
      object cxButton19: TcxButton
        Left = 6
        Top = 320
        Width = 109
        Height = 73
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        TabStop = False
        OnClick = cxButton19Click
        Colors.Default = 12621940
        Colors.Normal = 14930369
        Colors.Pressed = 10843723
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 2
      Width = 643
      Height = 409
      Align = alLeft
      BevelInner = bvLowered
      Color = 10517080
      TabOrder = 1
      Visible = False
      object Panel5: TPanel
        Left = 2
        Top = 36
        Width = 639
        Height = 371
        Align = alBottom
        BevelInner = bvLowered
        Color = clWhite
        TabOrder = 0
        object GridSpec: TcxGrid
          Left = 2
          Top = 2
          Width = 635
          Height = 367
          Align = alClient
          BevelKind = bkFlat
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object ViewSpec: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            FilterBox.CustomizeDialog = False
            DataController.DataSource = dsteSpec
            DataController.KeyFieldNames = 'ID'
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMMA'
                Column = ViewSpecSUMMA
              end
              item
                Kind = skSum
                FieldName = 'DSUM'
                Column = ViewSpecDSUM
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.UnselectFocusedRecordOnExit = False
            OptionsView.ExpandButtonsForEmptyDetails = False
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.FixedBandSeparatorWidth = 0
            Styles.Footer = dmR.cxStyle4
            Styles.Header = dmR.cxStyle4
            Styles.BandBackground = dmR.cxStyle23
            Styles.BandHeader = dmR.cxStyle23
            Bands = <
              item
                Width = 279
              end
              item
                Width = 309
              end>
            object ViewSpecSIFR: TcxGridDBBandedColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'SIFR'
              Options.Editing = False
              Options.Sorting = False
              Styles.Content = dmR.cxStyle30
              Width = 37
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object ViewSpecNAME: TcxGridDBBandedColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NAME'
              Options.Editing = False
              Options.Sorting = False
              Styles.Content = dmR.cxStyle30
              Width = 215
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object ViewSpecPRICE: TcxGridDBBandedColumn
              Caption = #1062#1077#1085#1072
              DataBinding.FieldName = 'PRICE'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Options.Editing = False
              Options.Sorting = False
              Styles.Content = dmR.cxStyle30
              Width = 109
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object ViewSpecQUANTITY: TcxGridDBBandedColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANTITY'
              PropertiesClassName = 'TcxCalcEditProperties'
              Options.Editing = False
              Styles.Content = dmR.cxStyle30
              Width = 74
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object ViewSpecSUMMA: TcxGridDBBandedColumn
              Caption = #1057#1091#1084#1084#1072
              DataBinding.FieldName = 'SUMMA'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Options.Editing = False
              Styles.Content = dmR.cxStyle30
              Width = 126
              Position.BandIndex = 1
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object ViewSpecDPROC: TcxGridDBBandedColumn
              Caption = #1055#1088#1086#1094#1077#1085#1090' '#1089#1082#1080#1076#1082#1080
              DataBinding.FieldName = 'DPROC'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = ',0.##%;-,0.##%'
              Visible = False
              Options.Editing = False
              Styles.Content = dmR.cxStyle30
              Width = 35
              Position.BandIndex = 1
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object ViewSpecDSUM: TcxGridDBBandedColumn
              Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
              DataBinding.FieldName = 'DSUM'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Visible = False
              Options.Editing = False
              Styles.Content = dmR.cxStyle30
              Width = 63
              Position.BandIndex = 1
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object ViewSpecISTATUS: TcxGridDBBandedColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'ISTATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmR.imState
              Properties.Items = <
                item
                  Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
                  ImageIndex = 2
                  Value = 0
                end
                item
                  Description = #1079#1072#1082#1072#1079#1072#1085#1086
                  ImageIndex = 0
                  Value = 1
                end>
              Visible = False
              Options.Editing = False
              Styles.Content = dmR.cxStyle30
              Width = 190
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 1
            end
          end
          object ViewModif: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = dsteMod
            DataController.DetailKeyFieldNames = 'ID_POS'
            DataController.KeyFieldNames = 'ID'
            DataController.MasterKeyFieldNames = 'ID'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GridLineColor = clBlack
            OptionsView.GridLines = glNone
            OptionsView.GroupByBox = False
            OptionsView.Header = False
            object ViewModifID_TAB: TcxGridDBColumn
              DataBinding.FieldName = 'ID_TAB'
              Visible = False
            end
            object ViewModifID_POS: TcxGridDBColumn
              DataBinding.FieldName = 'ID_POS'
              Visible = False
            end
            object ViewModifID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object ViewModifSIFR: TcxGridDBColumn
              DataBinding.FieldName = 'SIFR'
              Visible = False
            end
            object ViewModifNAME: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NAME'
              Width = 300
            end
            object ViewModifQUANTITY: TcxGridDBColumn
              DataBinding.FieldName = 'QUANTITY'
              Visible = False
            end
          end
          object LevelSpec: TcxGridLevel
            GridView = ViewSpec
            object LevelModif: TcxGridLevel
              GridView = ViewModif
            end
          end
        end
      end
      object cxTextEdit1: TcxTextEdit
        Left = 4
        Top = 4
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -16
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 1
        Text = 'cxTextEdit1'
        Width = 585
      end
    end
  end
  object Panel8: TPanel
    Left = 16
    Top = 568
    Width = 769
    Height = 37
    BevelInner = bvLowered
    Color = 10517080
    TabOrder = 6
    object Label2: TLabel
      Left = 604
      Top = 20
      Width = 12
      Height = 13
      Caption = 'L2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 8
      Top = 8
      Width = 15
      Height = 22
      Caption = 'l3'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object acRZD: TActionManager
    Left = 576
    Top = 84
    StyleName = 'XP Style'
    object acOpenSelMenu: TAction
      Caption = 'acOpenSelMenu'
      OnExecute = acOpenSelMenuExecute
    end
    object acModify: TAction
      Caption = 'acModify'
      OnExecute = acModifyExecute
    end
  end
  object teSpec: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 106
    Top = 324
    object teSpecID: TIntegerField
      FieldName = 'ID'
    end
    object teSpecSIFR: TIntegerField
      FieldName = 'SIFR'
    end
    object teSpecPRICE: TFloatField
      FieldName = 'PRICE'
      DisplayFormat = '0.00'
    end
    object teSpecQUANTITY: TFloatField
      FieldName = 'QUANTITY'
      DisplayFormat = '0.000'
    end
    object teSpecSUMMA: TFloatField
      FieldName = 'SUMMA'
      DisplayFormat = '0.00'
    end
    object teSpecNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object teSpecISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object teSpecLIMITM: TIntegerField
      FieldName = 'LIMITM'
    end
    object teSpecLINKM: TIntegerField
      FieldName = 'LINKM'
    end
    object teSpecITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
  end
  object dsteSpec: TDataSource
    DataSet = teSpec
    Left = 106
    Top = 376
  end
  object teMod: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 198
    Top = 324
    object teModID_POS: TIntegerField
      FieldName = 'ID_POS'
    end
    object teModID: TSmallintField
      FieldName = 'ID'
    end
    object teModSIFR: TIntegerField
      FieldName = 'SIFR'
    end
    object teModNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object teModQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
  end
  object dsteMod: TDataSource
    DataSet = teMod
    Left = 198
    Top = 376
  end
  object tiP: TTimer
    Interval = 5000
    OnTimer = tiPTimer
    Left = 500
    Top = 100
  end
  object TimerVes: TTimer
    Enabled = False
    OnTimer = TimerVesTimer
    Left = 336
    Top = 100
  end
end
