unit CashEnd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, ExtCtrls, Placemnt,
  ActnList, XPStyleActnCtrls, ActnMan, dxfBackGround, dxfLabel, cxMaskEdit,
  cxDropDownEdit, cxCalc, Menus, cxLabel, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid;

type
  TfmCash = class(TForm)
    cEdit1: TcxCurrencyEdit;
    CEdit2: TcxCurrencyEdit;
    CEdit3: TcxCurrencyEdit;
    Button5: TcxButton;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    am2: TActionManager;
    acExit: TAction;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    Label4: TLabel;
    cxButton15: TcxButton;
    CEdit4: TcxCalcEdit;
    cxButton16: TcxButton;
    CEdit5: TcxCurrencyEdit;
    cxButton17: TcxButton;
    Label9: TLabel;
    Button8: TcxButton;
    CEdit6: TcxCurrencyEdit;
    Label1: TcxLabel;
    Label2: TcxLabel;
    cxLabel1: TcxLabel;
    Label3: TcxLabel;
    Label8: TcxLabel;
    Gr2Ch: TcxGrid;
    Vi2Ch: TcxGridDBTableView;
    Vi2ChIORG: TcxGridDBColumn;
    Vi2ChNAME: TcxGridDBColumn;
    Vi2ChRSUM: TcxGridDBColumn;
    Le2Ch: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure CEdit5PropertiesEditValueChanged(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TSalePC = record
     BarCode:String;
     rBalans,rBalansDay,DLimit:Real;
     CliName,CliType:String;
     end;

var
  fmCash: TfmCash;
  bFirst:Boolean;
  SalePC:TSalePC;
  bPressB5Cash:Boolean = False;

implementation

uses Un1, CredCards, BnSber, UnCash, Dm, RaschotWarn, fmDiscountShape,
  prdb, UnPrizma, Vtb24un;

{$R *.dfm}

procedure TfmCash.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
{  fmCash.ClientWidth:=730;
  if not CommonSet.bTouch then
  begin
    Panel1.Visible:=False;
    fmCash.ClientWidth:=500;
  end;
  if CommonSet.BNButton=0 then cxButton16.Enabled:=False else cxButton16.Enabled:=True;} 
end;

procedure TfmCash.Timer1Timer(Sender: TObject);
begin
  if MessClear then
  begin
    Label4.Caption:='';
    MessClear:=False;
  end;
  if Label4.Caption>'' then MessClear:=True;
end;

procedure TfmCash.cxCurrencyEdit2PropertiesEditValueChanged(
  Sender: TObject);
begin
//��� ��� ������������ ���������
  CEdit3.EditValue:=roundEx((CEdit2.EditValue+CEdit5.EditValue+CEdit6.EditValue-CEdit1.EditValue)*100)/100;
  if CEdit3.EditValue<0 then
  begin
    Label4.Caption:='��������� ����� ������������ !';
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
      CEdit2.SelectAll;
      bPressB5Cash:=True;
      Button5.Enabled:=False;
    end;
  end
  else
  begin
    Button5.Enabled:=True;
    bPressB5Cash:=False;
    if fmCash.Visible then Button5.SetFocus;
  end;
end;

procedure TfmCash.Button5Click(Sender: TObject);
begin
  if bPressB5Cash then exit;
  bPressB5Cash:=True;
  Button5.Enabled:=False;
  cEdit3.EditValue:=cEdit2.EditValue+cEdit5.EditValue+CEdit6.EditValue-CEdit1.EditValue;
  if CEdit3.EditValue<0 then
  begin
    Label4.Caption:='��������� ����� ������������ !';
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
      CEdit2.SelectAll;
    end;
  end
  else
  begin
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
    end;
//    iCashType:=0; //������ ��������

    if Prizma then
    begin
      prWriteLog('!!Raschot');
      Event_RegEx(37,Nums.iCheckNum,0,0,'','',0,0,0);
    end;

    if CommonSet.RaschotWarn=1 then
    begin
      fmRaschotWarn.cxCurrencyEdit1.EditValue:=cEdit1.EditValue;
      fmRaschotWarn.cxCurrencyEdit2.EditValue:=cEdit5.EditValue;
      fmRaschotWarn.cxCurrencyEdit3.EditValue:=cEdit2.EditValue;
      fmRaschotWarn.cxCurrencyEdit4.EditValue:=cEdit3.EditValue;
      fmRaschotWarn.cxCurrencyEdit5.EditValue:=cEdit6.EditValue;

      fmRaschotWarn.ShowModal;
      delay(100);
      if fmRaschotWarn.ModalResult=mrOk then ModalResult:=mrOk
      else
      begin
        bPressB5Cash:=False;
        Button5.Enabled:=True;
      end;
    end else
      ModalResult:=mrOk;
  end;
end;

procedure TfmCash.acExitExecute(Sender: TObject);
begin
  if fmCash.Visible then
  begin
    CEdit2.SetFocus;
  end;
  ModalResult:=mrCancel;
end;

procedure TfmCash.cxButton2Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=1
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+1
    else CEdit4.Text:=CEdit4.Text+'1';
  bFirst:=False;
end;

procedure TfmCash.cxButton11Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=0
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10
    else CEdit4.Text:=CEdit4.Text+'0';
  bFirst:=False;
end;

procedure TfmCash.cxButton3Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=2
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+2
    else CEdit4.Text:=CEdit4.Text+'2';
  bFirst:=False;
end;

procedure TfmCash.cxButton4Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=3
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+3
    else CEdit4.Text:=CEdit4.Text+'3';
  bFirst:=False;
end;

procedure TfmCash.cxButton5Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=4
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+4
    else CEdit4.Text:=CEdit4.Text+'4';
  bFirst:=False;
end;

procedure TfmCash.cxButton6Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=5
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+5
    else CEdit4.Text:=CEdit4.Text+'5';
  bFirst:=False;
end;

procedure TfmCash.cxButton7Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=6
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+6
    else CEdit4.Text:=CEdit4.Text+'6';
  bFirst:=False;
end;

procedure TfmCash.cxButton8Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=7
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+7
    else CEdit4.Text:=CEdit4.Text+'7';
  bFirst:=False;
end;

procedure TfmCash.cxButton9Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=8
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+8
    else CEdit4.Text:=CEdit4.Text+'8';
  bFirst:=False;
end;

procedure TfmCash.cxButton10Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=9
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+9
    else CEdit4.Text:=CEdit4.Text+'9';
  bFirst:=False;
end;

procedure TfmCash.cxButton13Click(Sender: TObject);
begin
  CEdit4.Value:=0;
end;

procedure TfmCash.cxButton15Click(Sender: TObject);
begin
  if (cEdit4.EditValue <> 0)and(cEdit4.EditValue < 100000)  then
  begin
    cEdit2.EditValue:=cEdit4.EditValue;
  end else
  begin
    cEdit4.EditValue:=0;
  end;
end;

procedure TfmCash.FormShow(Sender: TObject);
begin
  cEdit4.EditValue:=0;
  cEdit5.EditValue:=0;
  cEdit6.EditValue:=0;

  SalePC.BarCode:='';
  SalePC.rBalans:=0;
  SalePC.rBalansDay:=0;
  SalePC.DLimit:=0;
  SalePC.CliName:='';
  SalePC.CliType:='';
  Tab.PBar:='';

  iPayType:=0;
  bFirst:=True;
  Button5.Enabled:=True;
  bPressB5Cash:=False;
end;

procedure TfmCash.cxButton14Click(Sender: TObject);
begin
  cEdit4.EditValue:=Trunc(cEdit4.EditValue*100/10)/100;
end;

procedure TfmCash.cxButton12Click(Sender: TObject);
begin
  if pos(',',CEdit4.Text)=0 then CEdit4.Text:=CEdit4.Text+',';
  bFirst:=False;
end;

procedure TfmCash.cxButton16Click(Sender: TObject);
Var rMoney,rBn:Real;
    StrP,StrTmp:String;
    iMB:INteger;
begin
//    rMoney = ������ ���������� ����� � ������ � ������
    iPayType:=0;
    rMoney:=cEdit1.Value-cEdit6.Value;
    if rMoney<0.01 then
    begin
      showmessage('����������� �� �����. �������� ��������.');
      exit; //����������� - ������ 3 ������ �������� �� ��������
    end;

    if Prizma then
    begin
      prWriteLog('!!RaschotBN');
      Event_RegEx(38,Nums.iCheckNum,0,0,'','',0,0,0);
    end;

    if (CommonSet.BNManual=1)or(CommonSet.BNManual=11) then //������ ����� �������
    begin
      //������� ���� ������ ���� ����� � �����.
      try
        fmCredCards:=TfmCredCards.Create(Application);
        fmCredCards.CurrencyEdit1.EditValue:=rMoney;
        fmCredCards.CurrencyEdit1.Tag:=RoundEx(rMoney*100);
        fmCredCards.CurrencyEdit1.Properties.ReadOnly:=True;
        fmCredCards.CurrencyEdit1.Enabled:=True;

        fmCredCards.Panel2.Visible:=False;
        fmCredCards.Edit1.Visible:=False;
        if CommonSet.BNManual=11 then fmCredCards.Edit1.Enabled:=False;

        fmCredCards.GridBN.Visible:=True;
        fmCredCards.Button1.Visible:=True;
        fmCredCards.ShowModal;
        if fmCredCards.ModalResult<>mrOk then rMoney:=0 //���� ������ �� �����=0
        else rMoney:=rv(fmCredCards.CurrencyEdit1.EditValue);

      finally
        fmCredCards.Release;
      end;

      if rMoney<>0 then
      begin


        iCashType:=1; //������ �����������
        cEdit5.EditValue:=rMoney;
        cEdit2.EditValue:=cEdit1.Value-rMoney-cEdit6.EditValue;

        //���������� ������ �� ������������
        //aCheckBn[i]:=0
        rBn:=rMoney;

        with dmC do
        begin
          quCheckOrgItog.First;
          while not quCheckOrgItog.Eof do
          begin
            if rBn<quCheckOrgItogRSUM.AsFloat then
            begin
              aCheckBn[quCheckOrgItogIORG.AsInteger]:=rBn;
              rBn:=0;
            end
            else
            begin
              aCheckBn[quCheckOrgItogIORG.AsInteger]:=quCheckOrgItogRSUM.AsFloat;
              rBn:=rBn-quCheckOrgItogRSUM.AsFloat;
            end;

            quCheckOrgItog.Next;
          end;
        end;

        iPayType:=dmC.taCredCardID.AsInteger;
        ModalResult:=mrOk;
      end;
    end
    else  //�������������� ����� ������� �� ���������� ������������ ���� �� ��������
    begin
      if CommonSet.BNManual=0 then  //���
      begin
        //������� ���� ���������� ����� � �����.
        //������� ���� ������ ���� ����� � �����.
        try
          fmCredCards:=TfmCredCards.Create(Application);
          fmCredCards.CurrencyEdit1.EditValue:=rMoney;
          fmCredCards.CurrencyEdit1.Enabled:=False;
          fmCredCards.Panel2.Visible:=True;
          fmCredCards.Edit1.Visible:=True;
          fmCredCards.GridBN.Visible:=False;
          fmCredCards.Button1.Visible:=False;
          bnBar:='';
          BnStr:='';

          fmCredCards.ShowModal;
          if fmCredCards.ModalResult<>mrOk then rMoney:=0; //���� ������ �� �����=0

        finally
          fmCredCards.Release;
        end;

        if rMoney<>0 then
        begin
          iCashType:=1; //������ �����������
          cEdit5.EditValue:=rMoney;
          cEdit2.EditValue:=cEdit1.Value-rMoney-cEdit6.EditValue;

          iPayType:=1;
          ModalResult:=mrOk;
        end;
      end;
      if CommonSet.BNManual=2 then
      begin //��� ����
        try
//          fmSber:=tFmSber.Create(Application);

          iMoneyBn:=RoundEx(rMoney*100); //� ��������

          fmSber.Label1.Visible:=True;
          fmSber.cxButton9.Enabled:=False;
          fmSber.cxButton10.Enabled:=False;
          fmSber.cxButton11.Enabled:=False;
          fmSber.cxButton6.Enabled:=False;

          if Operation=0 then //�������
          begin
            fmSber.cxButton2.Enabled:=True;
            fmSber.cxButton3.Enabled:=False;
            fmSber.cxButton4.Enabled:=False;
            fmSber.cxButton5.Enabled:=False;
            fmSber.cxButton7.Enabled:=False;

          end;
          if Operation=1 then //�������
          begin
            fmSber.cxButton2.Enabled:=False;
            fmSber.cxButton3.Enabled:=False;
            fmSber.cxButton4.Enabled:=False;
            fmSber.cxButton5.Enabled:=True;
            fmSber.cxButton7.Enabled:=True;
          end;

          BnStr:='';

          fmSber.ShowModal;

//����� ����� ������ ������ ����� ���������� �� ����������
//��� - �� ����� ����� �.�. � ���������� ��������� ����� ���������  , � ��������� ���� ��� ��������� ����� ����������� ����.

          prBnPrint;

          if fmSber.ModalResult<>mrOk then
          begin
//            rMoney:=0; //���� ������ �� �����=0
            iMoneyBn:=0;
          end;
          if iMoneyBn<>0 then
          begin
            iCashType:=1; //������ �����������
//            rMoney:=iMoneyBn/100;
            cEdit5.EditValue:=rMoney;
            cEdit2.EditValue:=cEdit1.Value-rMoney-cEdit6.EditValue;

            iPayType:=1;

            ModalResult:=mrOk;
          end;

        finally
//          fmSber.Release;
        end;
      end;

      if CommonSet.BNManual=3 then //�������������� ����� ������� ���
      begin
        try
//        fmSber:=tFmSber.Create(Application);

          iMB:=Trunc(RoundEx(rMoney*100)); //� ��������

          iMoneyBn:=iMB; //� ��������

          fmVTB.Label1.Visible:=True;
          fmVTB.cxButton9.Enabled:=False;
          fmVTB.cxButton6.Enabled:=False;
          fmVTB.cxButton8.Enabled:=False;
          fmVTB.cxButton8.TabStop:=False;


          if Operation=0 then //�������
          begin

            SberTabStop:=1;

            fmVTB.cxButton2.Enabled:=True;
            fmVTB.cxButton2.TabOrder:=1;
            fmVTB.cxButton2.TabStop:=True;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0) then
            begin
              fmVTB.cxButton8.Enabled:=True;
              fmVTB.cxButton8.TabOrder:=2;
              fmVTB.cxButton8.TabStop:=True;

              fmVTB.cxButton1.TabOrder:=3;
            end else
            begin
              fmVTB.cxButton8.Enabled:=False;
              fmVTB.cxButton8.TabStop:=False;

              fmVTB.cxButton1.TabOrder:=2;
            end;

            fmVTB.cxButton5.Enabled:=False;
            fmVTB.cxButton5.TabStop:=False;

            fmVTB.cxButton13.Enabled:=False;
            fmVTB.cxButton13.TabStop:=False;

          end;
          if Operation=1 then //�������
          begin
            SberTabStop:=2;

            fmVTB.cxButton2.Enabled:=False;
            fmVTB.cxButton2.TabStop:=False;

            fmVTB.cxButton5.Enabled:=True;
            fmVTB.cxButton5.TabOrder:=1;
            fmVTB.cxButton5.TabStop:=True;

            fmVTB.cxButton1.TabOrder:=3;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0) then
            begin
              fmVTB.cxButton8.Enabled:=True;
              fmVTB.cxButton8.TabOrder:=3;
              fmVTB.cxButton8.TabStop:=True;

              fmVTB.cxButton1.TabOrder:=4;
            end else
            begin
              fmVTB.cxButton8.Enabled:=False;
              fmVTB.cxButton8.TabStop:=False;

              fmVTB.cxButton1.TabOrder:=3;
            end;

            fmVTB.cxButton13.Enabled:=False;
            fmVTB.cxButton13.TabStop:=False;

          end;

          bnBar:='';
          BnStr:='';

          prWriteLog('~!fmVTB.ShowModal');

          fmVTB.ShowModal;

          prWriteLog('');
          prWriteLog('������ ��� ����� ���� ���');
          prWriteLog('-----------------');
          prWriteLog(BnStr);
          prWriteLog('-----------------');
          prWriteLog('');

          StrTmp:=BnStr;
          prBnPrint;
          BnStr:=StrTmp;
          prBnPrint;

          if fmVTB.ModalResult<>mrOk then
          begin
//            rMoney:=0; //���� ������ �� �����=0
            iMoneyBn:=0;
          end;
          if iMoneyBn<>0 then
          begin
            rMoney:=iMoneyBn/100;

            iCashType:=1; //������ �����������
//            rMoney:=iMoneyBn/100;
            cEdit5.EditValue:=rMoney;
            cEdit2.EditValue:=cEdit1.Value-rMoney-cEdit6.EditValue;

            iPayType:=1;

            ModalResult:=mrOk;
          end;

        finally
        end;
      end;
    end;
end;

procedure TfmCash.CEdit5PropertiesEditValueChanged(Sender: TObject);
begin
  CEdit3.EditValue:=rv(CEdit2.EditValue+CEdit5.EditValue+cEdit6.EditValue-CEdit1.EditValue);
  if CEdit3.EditValue<0 then
  begin
    Label4.Caption:='��������� ����� ������������ !';
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
      CEdit2.SelectAll;
      Button5.Enabled:=False;
      bPressB5Cash:=True;
    end;
  end
  else
  begin
    Button5.Enabled:=True;
    bPressB5Cash:=False;
    if fmCash.Visible then Button5.SetFocus;
  end;
end;

procedure TfmCash.cxButton17Click(Sender: TObject);
begin
  if CommonSet.CashNum>0 then CashDriver
  else
  begin
    if CommonSet.SpecChar=1 then
    begin
      if pos('fis',CommonSet.SpecBox)=0 then dmC.OpenMoneyBox
      else
      begin
        try
          StrWk:='COM'+IntToStr(CommonSet.CashPort);
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
          CashOpen(PChar(StrWk));
          CashDriver;
          CashClose;
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
        except
        end;
      end;
    end else dmC.OpenMoneyBox;
    if Prizma then
    begin
      prWriteLog('!!OpenDriver');
      Event_RegEx(40,Nums.iCheckNum,0,0,'','',0,0,0);
    end;
  end;
end;

procedure TfmCash.Button8Click(Sender: TObject);
var rSumPC,rSumN,rSumBN:Real;
begin
  if not CanDo('prPrintBNCheck') then exit;
  with dmC do
  begin
    rSumN:=rv(fmCash.cEdit1.Value);
    rSumBN:=rv(fmCash.cEdit5.Value);

    fmDiscount_Shape.cxTextEdit1.Text:='';
    fmDiscount_Shape.Caption:='��������� ��������� �����';
    fmDiscount_Shape.Label1.Caption:='��������� ��������� �����';
    fmDiscount_Shape.cxButton13.Visible:=True;
    fmDiscount_Shape.cxCheckBox1.Checked:=False;
    fmDiscount_Shape.cxCheckBox1.Visible:=True;

    if not CanDo('prPrintPCCheck') then
    begin
      fmDiscount_Shape.cxButton13.Visible:=False;
      fmDiscount_Shape.cxCheckBox1.Checked:=False;
      fmDiscount_Shape.cxCheckBox1.Enabled:=True;
    end;

    fmDiscount_Shape.ShowModal;
    if fmDiscount_Shape.ModalResult=mrOk then
    begin
      if fmDiscount_Shape.cxCheckBox1.Checked=False then
      begin
        //������ �� ��������� �����
        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          SalePC.BarCode:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          prFindPCardBalans(SalePC.BarCode,SalePC.rBalans,SalePC.rBalansDay,SalePC.DLimit,SalePC.CliName,SalePC.CliType);

          rSumPC:=rv(SalePC.rBalansDay);

          if rSumPC>0 then
          begin
            if rSumPC>(rSumN-rSumBN) then rSumPC:=rv(rSumN-rSumBN);

            fmCash.cEdit6.Value:=rSumPC;
            fmCash.cEdit2.Value:=rSumN-rSumBN-rSumPC;
            fmCash.cEdit3.Value:=0;

            Tab.PBar:=SalePC.BarCode;

          end;
        end else
        begin
          SalePC.BarCode:='';
          SalePC.rBalans:=0;
          SalePC.rBalansDay:=0;
          SalePC.DLimit:=0;
          SalePC.CliName:='';
          SalePC.CliType:='';
          Tab.PBar:='';

          fmCash.cEdit6.Value:=0;
          fmCash.cEdit2.Value:=rSumN-rSumBN;
          fmCash.cEdit3.Value:=0;
        end;
      end else
      begin
        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          SalePC.BarCode:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          prFindPCardBalans(SalePC.BarCode,SalePC.rBalans,SalePC.rBalansDay,SalePC.DLimit,SalePC.CliName,SalePC.CliType);

          Showmessage('     ��������  '+SalePC.CliName+#$0D+'     ���  '+SalePC.CliType+#$0D+'     ������  '+fts(rv(SalePC.rBalans))+#$0D+'     ������ �������  '+fts(rv(SalePC.rBalansDay))+#$0D+'     ������� �����  '+fts(rv((-1)*SalePC.DLimit)));
        end;
      end;
    end;
  end;
end;

end.
