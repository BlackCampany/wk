object fmMainCashRn: TfmMainCashRn
  Left = 130
  Top = 48
  Width = 894
  Height = 690
  Align = alTop
  BorderIcons = []
  Caption = #1050#1072#1089#1089#1086#1074#1099#1081' '#1084#1086#1076#1091#1083#1100
  Color = clGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxButton6: TcxButton
    Left = 784
    Top = 347
    Width = 209
    Height = 35
    Caption = #1042#1089#1077' '#1079#1072#1082#1072#1079#1099' ('#1086#1073#1085#1086#1074#1080#1090#1100')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    Visible = False
    WordWrap = True
    OnClick = cxButton6Click
    Colors.Default = 8453888
    Colors.Normal = 8453888
    Colors.Pressed = clGreen
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel1: TPanel
    Left = 784
    Top = 8
    Width = 209
    Height = 191
    BevelInner = bvLowered
    Color = 2434341
    TabOrder = 0
    object Label8: TLabel
      Left = 16
      Top = 16
      Width = 8
      Height = 13
      Caption = 'l8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 16
      Top = 40
      Width = 8
      Height = 13
      Caption = 'l9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 16
      Top = 64
      Width = 8
      Height = 13
      Caption = 'l2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 16
      Top = 112
      Width = 8
      Height = 13
      Caption = 'l3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 16
      Top = 88
      Width = 8
      Height = 13
      Caption = 'l4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 16
      Top = 160
      Width = 8
      Height = 13
      Caption = 'l5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 16
      Top = 136
      Width = 8
      Height = 13
      Caption = 'l6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 784
    Top = 216
    Width = 209
    Height = 41
    BevelInner = bvLowered
    Color = 12303291
    TabOrder = 1
    OnClick = Panel2Click
    object Label13: TLabel
      Left = 11
      Top = 13
      Width = 189
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = Panel2Click
    end
  end
  object RxClock1: TRxClock
    Left = 872
    Top = 656
    Width = 129
    Height = 41
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 14927276
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = RxClock1Click
  end
  object Panel3: TPanel
    Left = 16
    Top = 8
    Width = 745
    Height = 577
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 93
      Height = 13
      Caption = #1054#1090#1082#1088#1099#1090#1099#1077' '#1079#1072#1082#1072#1079#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object GridTab: TcxGrid
      Left = 8
      Top = 24
      Width = 729
      Height = 545
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PersView: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmC.dsPers
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.GroupByBox = False
        Styles.Header = dmC.cxStyle10
        object PersViewNAME: TcxGridDBColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'NAME'
          Styles.Content = dmC.cxStyle3
          Width = 136
        end
        object PersViewsCountTab: TcxGridDBColumn
          Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          DataBinding.FieldName = 'sCountTab'
          Styles.Content = dmC.cxStyle3
          Width = 314
        end
      end
      object TabView: TcxGridDBCardView
        Synchronization = False
        OnDblClick = TabViewDblClick
        NavigatorButtons.ConfirmDelete = False
        FilterBox.CustomizeDialog = False
        OnCustomDrawCell = TabViewCustomDrawCell
        DataController.DataSource = dmC.dsTabs
        DataController.DetailKeyFieldNames = 'ID_PERSONAL'
        DataController.KeyFieldNames = 'ID'
        DataController.MasterKeyFieldNames = 'ID_PERSONAL'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        LayoutDirection = ldVertical
        OptionsCustomize.RowFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.CardWidth = 130
        OptionsView.RowCaptionAutoHeight = True
        OptionsView.RowCaptionEndEllipsis = True
        OptionsView.SeparatorColor = 16713471
        OptionsView.SeparatorWidth = 5
        Styles.Background = dmC.cxStyle11
        Styles.CaptionRow = dmC.cxStyle12
        Styles.CardBorder = dmC.cxStyle3
        object TabViewSSTAT: TcxGridDBCardViewRow
          Caption = #1057#1090#1086#1083
          DataBinding.FieldName = 'SSTAT'
          Styles.Content = dmC.cxStyle13
        end
        object TabViewQUESTS: TcxGridDBCardViewRow
          Caption = #1043#1086#1089#1090#1077#1081
          DataBinding.FieldName = 'QUESTS'
        end
        object TabViewNAME: TcxGridDBCardViewRow
          Caption = #1043#1086#1089#1090#1100
          DataBinding.FieldName = 'NAME'
          Styles.Content = dmC.cxStyle26
        end
        object TabViewTABSUM: TcxGridDBCardViewRow
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'TABSUM'
          Styles.Content = dmC.cxStyle12
        end
        object TabViewSTIME: TcxGridDBCardViewRow
          Caption = #1054#1090#1082#1088#1099#1090
          DataBinding.FieldName = 'STIME'
        end
      end
      object PersLevel: TcxGridLevel
        GridView = PersView
        object TabLevel: TcxGridLevel
          GridView = TabView
        end
      end
    end
  end
  object Button1: TcxButton
    Left = 24
    Top = 602
    Width = 89
    Height = 60
    Caption = #1057#1086#1079#1076#1072#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = False
    OnClick = Button1Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button2: TcxButton
    Left = 120
    Top = 602
    Width = 89
    Height = 60
    Caption = #1054#1090#1082#1088#1099#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = False
    OnClick = Button2Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button3: TcxButton
    Left = 216
    Top = 602
    Width = 89
    Height = 60
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = False
    OnClick = Button3Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button4: TcxButton
    Left = 312
    Top = 602
    Width = 89
    Height = 60
    Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = False
    OnClick = Button4Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button5: TcxButton
    Left = 784
    Top = 522
    Width = 209
    Height = 60
    Caption = #1056#1072#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    Visible = False
    OnClick = Button5Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button6: TcxButton
    Left = 408
    Top = 602
    Width = 89
    Height = 60
    Action = acRetPre
    Caption = #1054#1090#1084#1077#1085#1072' '#1055#1088#1063
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton1: TcxButton
    Left = 784
    Top = 290
    Width = 209
    Height = 36
    Caption = #1050#1072#1089#1089#1086#1074#1099#1077' '#1086#1090#1095#1077#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Visible = False
    OnClick = cxButton1Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel4: TPanel
    Left = 774
    Top = 333
    Width = 225
    Height = 166
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 10
    Visible = False
    object cxButton2: TcxButton
      Left = 8
      Top = 13
      Width = 209
      Height = 36
      Caption = 'X-'#1086#1090#1095#1077#1090' ('#1086#1090#1095#1077#1090' '#1086' '#1074#1099#1088#1091#1095#1082#1077')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 8
      Top = 61
      Width = 209
      Height = 36
      Caption = 'Z-'#1086#1090#1095#1077#1090' ('#1079#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton3Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 16
      Top = 120
      Width = 193
      Height = 25
      Caption = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1090#1100' '#1095#1077#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
      OnClick = cxButton4Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 16
      Top = 120
      Width = 193
      Height = 25
      Caption = #1054#1092#1086#1088#1084#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = cxButton5Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
  object Button7: TcxButton
    Left = 504
    Top = 602
    Width = 89
    Height = 60
    Action = acPrintPre
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    Visible = False
    WordWrap = True
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button8: TcxButton
    Left = 600
    Top = 602
    Width = 89
    Height = 60
    Action = acCashPCard
    Caption = #1054#1087#1083#1072#1090#1072' '#1041#1053
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    Visible = False
    WordWrap = True
    Colors.Default = 9943551
    Colors.Normal = 9943551
    Colors.Pressed = 4227327
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 322
    Top = 136
  end
  object amRn: TActionManager
    Left = 282
    Top = 253
    StyleName = 'XP Style'
    object acMenu1: TAction
      Caption = 'acMenu1'
    end
    object acCreateTab: TAction
      Caption = 'acCreateTab'
      ShortCut = 107
      OnExecute = acCreateTabExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1082#1072#1079
      ShortCut = 109
      OnExecute = acDelExecute
    end
    object acMove: TAction
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
      ShortCut = 117
      OnExecute = acMoveExecute
    end
    object acRetPre: TAction
      Caption = 'acRetPre'
      ShortCut = 120
      OnExecute = acRetPreExecute
    end
    object acCashEnd: TAction
      Caption = 'acCashEnd'
      ShortCut = 116
      OnExecute = acCashEndExecute
    end
    object acCashRep: TAction
      Caption = #1050#1072#1089#1089#1086#1074#1099#1077' '#1086#1090#1095#1077#1090#1099
      ShortCut = 118
      OnExecute = acCashRepExecute
    end
    object acExit: TAction
      Caption = 'Exit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acOpen: TAction
      Caption = 'acOpen'
      ShortCut = 115
      OnExecute = acOpenExecute
    end
    object acPrintPre: TAction
      Caption = #1054#1092#1086#1088#1084#1080#1090#1100' '#1057#1095#1077#1090
      ShortCut = 113
      OnExecute = acPrintPreExecute
    end
    object acCashPCard: TAction
      Caption = 'acCashPCard'
      OnExecute = acCashPCardExecute
    end
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 10921638
    BkColor.EndColor = clBlack
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 304
    Top = 360
  end
  object frRepMain: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    ShowPrintDialog = False
    ShowProgress = False
    RebuildPrinter = False
    Left = 95
    Top = 29
    ReportForm = {19000000}
  end
  object frquCheck: TfrDBDataSet
    DataSet = dmC.quCheck
    Left = 178
    Top = 29
  end
  object TimerPrintQ: TTimer
    Enabled = False
    Left = 418
    Top = 141
  end
  object TimerDelayPrintQ: TTimer
    Enabled = False
    Left = 418
    Top = 197
  end
  object TiRefresh: TTimer
    Enabled = False
    Left = 418
    Top = 253
  end
end
