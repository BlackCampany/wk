unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmInput = class(TForm)
    StatusBar1: TStatusBar;
    ViewF: TcxGridDBTableView;
    LevelF: TcxGridLevel;
    GridF: TcxGrid;
    quInputF: TpFIBDataSet;
    dsquInputF: TDataSource;
    quInputFIDC: TFIBIntegerField;
    quInputFSHORTNAME: TFIBStringField;
    quInputFRECEIPTNUM: TFIBStringField;
    quInputFPCOUNT: TFIBIntegerField;
    quInputFPVES: TFIBFloatField;
    quInputFCURMESSURE: TFIBIntegerField;
    quInputFNAMESHORT: TFIBStringField;
    quInputFNETTO: TFIBFloatField;
    ViewFIDC: TcxGridDBColumn;
    ViewFSHORTNAME: TcxGridDBColumn;
    ViewFRECEIPTNUM: TcxGridDBColumn;
    ViewFPCOUNT: TcxGridDBColumn;
    ViewFPVES: TcxGridDBColumn;
    ViewFNAMESHORT: TcxGridDBColumn;
    ViewFNETTO: TcxGridDBColumn;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    quInputFPARENT: TFIBIntegerField;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    procedure cxButton2Click(Sender: TObject);
    procedure ViewFDblClick(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInput: TfmInput;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmInput.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmInput.ViewFDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmInput.Excel1Click(Sender: TObject);
begin
  prNExportExel4(ViewF);
end;

end.
