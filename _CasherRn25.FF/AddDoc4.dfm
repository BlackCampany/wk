object fmAddDoc4: TfmAddDoc4
  Left = 277
  Top = 213
  Width = 783
  Height = 582
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 129
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 33
      Height = 13
      Caption = #1057#1063#1060#1050
      Transparent = True
    end
    object Label3: TLabel
      Left = 248
      Top = 40
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label4: TLabel
      Left = 24
      Top = 72
      Width = 58
      Height = 13
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 96
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label15: TLabel
      Left = 264
      Top = 96
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object Label6: TLabel
      Left = 432
      Top = 16
      Width = 54
      Height = 13
      Caption = #1057#1082#1080#1076#1082#1072' (%)'
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Properties.OnChange = cxDateEdit1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxTextEdit2: TcxTextEdit
      Left = 112
      Top = 36
      Properties.MaxLength = 15
      TabOrder = 2
      Text = 'cxTextEdit2'
      Width = 121
    end
    object cxDateEdit2: TcxDateEdit
      Left = 272
      Top = 36
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 112
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 4
      Text = 'cxButtonEdit1'
      OnKeyPress = cxButtonEdit1KeyPress
      Width = 281
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 92
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 145
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 496
      Top = 12
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 6
      Width = 89
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 529
    Width = 775
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 488
    Width = 775
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 121
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 8
      Width = 137
      Height = 25
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 129
    Width = 153
    Height = 359
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 136
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 7
      Top = 160
      Cursor = crHandPoint
      Caption = #1051#1101#1081#1073#1083' '#1085#1072' '#1079#1072#1087#1072#1089
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      Visible = False
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
  end
  object PageControl1: TPageControl
    Left = 160
    Top = 136
    Width = 553
    Height = 329
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1058#1086#1074#1072#1088#1099
      object GridDoc4: TcxGrid
        Left = 0
        Top = 0
        Width = 545
        Height = 298
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewDoc4: TcxGridDBTableView
          OnDragDrop = ViewDoc4DragDrop
          OnDragOver = ViewDoc4DragOver
          NavigatorButtons.ConfirmDelete = False
          OnEditing = ViewDoc4Editing
          OnEditKeyDown = ViewDoc4EditKeyDown
          OnEditKeyPress = ViewDoc4EditKeyPress
          DataController.DataSource = dsSpec
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'RNds'
              Column = ViewDoc4RNds
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumIn'
              Column = ViewDoc4SumIn
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumR'
              Column = ViewDoc4SumR
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewDoc4SumNac
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
              Column = ViewDoc4ProcNac
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object ViewDoc4Num: TcxGridDBColumn
            Caption = #8470' '#1087#1087
            DataBinding.FieldName = 'Num'
            Options.Editing = False
            Width = 51
          end
          object ViewDoc4IdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdGoods'
            Width = 50
          end
          object ViewDoc4NameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 179
          end
          object ViewDoc4IM: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'IM'
            Options.Editing = False
          end
          object ViewDoc4SM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.ReadOnly = True
            Properties.OnButtonClick = ViewDoc4SMPropertiesButtonClick
            Width = 57
          end
          object ViewDoc4Quant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
          end
          object ViewDoc4PriceIn: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PriceIn'
          end
          object ViewDoc4SumIn: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SumIn'
          end
          object ViewDoc4PriceR: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1085'.'
            DataBinding.FieldName = 'PriceR'
          end
          object ViewDoc4SumR: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1085'.'
            DataBinding.FieldName = 'SumR'
          end
          object ViewDoc4INds: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1053#1044#1057
            DataBinding.FieldName = 'INds'
            Options.Editing = False
          end
          object ViewDoc4SNds: TcxGridDBColumn
            Caption = #1053#1044#1057
            DataBinding.FieldName = 'SNds'
            Options.Editing = False
            Width = 48
          end
          object ViewDoc4RNds: TcxGridDBColumn
            Caption = #1053#1044#1057' '#1074' '#1090'.'#1095'.'
            DataBinding.FieldName = 'RNds'
            Options.Editing = False
          end
          object ViewDoc4SumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'SumNac'
            Options.Editing = False
          end
          object ViewDoc4ProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'ProcNac'
            Options.Editing = False
          end
          object ViewDoc4Km: TcxGridDBColumn
            Caption = #1050#1086#1077#1092'.'#1077#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'Km'
          end
          object ViewDoc4TCard: TcxGridDBColumn
            Caption = #1058#1050
            DataBinding.FieldName = 'TCard'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmO.imState
            Properties.Items = <
              item
                Description = #1085#1077#1090
                Value = 0
              end
              item
                Description = #1090#1082
                ImageIndex = 11
                Value = 1
              end>
            Width = 50
          end
          object ViewDoc4Oper: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'Oper'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
                ImageIndex = 0
                Value = 0
              end
              item
                Description = #1085#1072' '#1089#1087#1080#1089#1072#1085#1080#1077
                Value = 1
              end
              item
                Description = #1085#1072' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1091
                Value = 2
              end>
          end
        end
        object LevelDoc4: TcxGridLevel
          GridView = ViewDoc4
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1058#1072#1088#1072
      ImageIndex = 1
      object GrTara: TcxGrid
        Left = 0
        Top = 0
        Width = 545
        Height = 298
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewTara: TcxGridDBTableView
          OnDragDrop = ViewTaraDragDrop
          OnDragOver = ViewTaraDragOver
          NavigatorButtons.ConfirmDelete = False
          OnEditing = ViewTaraEditing
          OnEditKeyDown = ViewTaraEditKeyDown
          OnEditKeyPress = ViewTaraEditKeyPress
          DataController.DataSource = dstaTara
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.###'
              Kind = skSum
              FieldName = 'Quant'
              Column = ViewTaraQuant
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumIn'
              Column = ViewTaraSumIn
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumUch'
              Column = ViewTaraSumUch
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewTaraSumNac
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object ViewTaraNum: TcxGridDBColumn
            Caption = #8470' '#1087'.'#1087'.'
            DataBinding.FieldName = 'Num'
            Options.Editing = False
            Width = 49
          end
          object ViewTaraIdCard: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdCard'
            Width = 52
          end
          object ViewTaraNameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 119
          end
          object ViewTaraIM: TcxGridDBColumn
            Caption = #1050#1086#1076'.'#1045#1076'.'#1080#1079#1084
            DataBinding.FieldName = 'IM'
            Options.Editing = False
            Width = 40
          end
          object ViewTaraKM: TcxGridDBColumn
            Caption = #1050#1086#1101#1092'.'#1077#1076'.'#1080#1079#1084
            DataBinding.FieldName = 'KM'
            Options.Editing = False
          end
          object ViewTaraSM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = ViewTaraInSMPropertiesButtonClick
            Width = 50
          end
          object ViewTaraQuant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
          end
          object ViewTaraPriceIn: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PriceIn'
          end
          object ViewTaraSumIn: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SumIn'
          end
          object ViewTaraPriceUch: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'PriceUch'
          end
          object ViewTaraSumUch: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'SumUch'
          end
          object ViewTaraSumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094'.'
            DataBinding.FieldName = 'SumNac'
            Options.Editing = False
            Styles.Content = dmO.cxStyle1
          end
          object ViewTaraProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094'.'
            DataBinding.FieldName = 'ProcNac'
            Options.Editing = False
            Styles.Content = dmO.cxStyle1
          end
        end
        object LevelTara: TcxGridLevel
          GridView = ViewTara
        end
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 480
    Top = 200
  end
  object taSpec: TClientDataSet
    Aggregates = <>
    FileName = 'SpecR.cds'
    Params = <>
    Left = 296
    Top = 200
    object taSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecQuantChange
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpecPriceIn: TCurrencyField
      FieldName = 'PriceIn'
      OnChange = taSpecPriceInChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecSumIn: TCurrencyField
      FieldName = 'SumIn'
      OnChange = taSpecSumInChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecPriceR: TCurrencyField
      FieldName = 'PriceR'
      OnChange = taSpecPriceRChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecSumR: TCurrencyField
      FieldName = 'SumR'
      OnChange = taSpecSumRChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecINds: TIntegerField
      FieldName = 'INds'
    end
    object taSpecSNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpecRNds: TCurrencyField
      FieldName = 'RNds'
      EditFormat = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
    end
    object taSpecSumNac: TCurrencyField
      FieldName = 'SumNac'
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = ',0.0%;-,0.0%'
    end
    object taSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecTCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpecOper: TSmallintField
      FieldName = 'Oper'
    end
  end
  object dsSpec: TDataSource
    DataSet = taSpec
    Left = 296
    Top = 248
  end
  object amDocR: TActionManager
    Left = 192
    Top = 200
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 424
    Top = 240
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
  object taTara: TClientDataSet
    Aggregates = <>
    FileName = 'TaraR.cds'
    Params = <>
    Left = 548
    Top = 211
    object taTaraNum: TIntegerField
      FieldName = 'Num'
    end
    object taTaraIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taTaraNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taTaraIM: TIntegerField
      FieldName = 'IM'
      Visible = False
    end
    object taTaraKM: TFloatField
      FieldName = 'KM'
      Visible = False
      DisplayFormat = '0.###'
    end
    object taTaraSM: TStringField
      FieldName = 'SM'
    end
    object taTaraQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taTaraQuantChange
      DisplayFormat = '0.000'
    end
    object taTaraPriceIn: TFloatField
      FieldName = 'PriceIn'
      OnChange = taTaraPriceInChange
      DisplayFormat = '0.00'
    end
    object taTaraSumIn: TFloatField
      FieldName = 'SumIn'
      OnChange = taTaraSumInChange
      DisplayFormat = '0.00'
    end
    object taTaraPriceUch: TFloatField
      FieldName = 'PriceUch'
      OnChange = taTaraPriceUchChange
      DisplayFormat = '0.00'
    end
    object taTaraSumUch: TFloatField
      FieldName = 'SumUch'
      OnChange = taTaraSumUchChange
      DisplayFormat = '0.00'
    end
    object taTaraSumNac: TFloatField
      FieldName = 'SumNac'
      DisplayFormat = '0.00'
    end
    object taTaraProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = '0.0 '#39'%'#39
    end
  end
  object dstaTara: TDataSource
    DataSet = taTara
    Left = 548
    Top = 267
  end
  object CasherRnDb1: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\OFFICERN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelCash
    DefaultUpdateTransaction = trSelCash
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 20
    Left = 208
    Top = 312
  end
  object trSelCash: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 204
    Top = 371
  end
  object quFindPrice: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT PRICE,'
      '       CODE,'
      '       CONSUMMA,'
      '       IACTIVE'
      'FROM MENU'
      'where CODE=:IDCARD and CONSUMMA=1'
      'order by IACTIVE')
    Transaction = trSelCash
    Database = CasherRnDb1
    Left = 292
    Top = 323
    poAskRecordCount = True
    object quFindPricePRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quFindPriceCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quFindPriceCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quFindPriceIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
end
