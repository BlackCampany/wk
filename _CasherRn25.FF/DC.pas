unit DC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, dxfBackGround, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxMaskEdit, cxDropDownEdit, cxCalc, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCheckBox, cxCalendar;

type
  TfmAddDC = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    dxfBackGround1: TdxfBackGround;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxCalcEdit1: TcxCalcEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CheckBox1: TcxCheckBox;
    Label4: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label5: TLabel;
    cxDateEdit1: TcxDateEdit;
    Label6: TLabel;
    cxTextEdit4: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDC: TfmAddDC;

implementation

{$R *.dfm}

procedure TfmAddDC.FormCreate(Sender: TObject);
begin
  cxTextEdit1.Text:='';
  cxTextEdit2.Text:='';
  cxCalcEdit1.Value:=0;
end;

end.
