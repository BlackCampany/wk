unit UnCash;

interface
uses
//  ������� ��� ��-101 ���

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ComObj, ActiveX, OleServer, StdCtrls;

Function  InspectSt(Var sMess:String):Integer;
Function  GetSt(Var sMess:String):INteger;
Function  OpenZ:Boolean;  //��������
Function  GetSerial:Boolean;
Function  GetNums:Boolean;
Function  GetRes:Boolean;
Function  GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;

Function   CashOpen(sPort:PChar):Boolean;
Function   CashClose:Boolean;
Function   CashDate(var sDate:String):Boolean;
Function   GetX:Boolean;
Function   ZOpen:Boolean; //������ �����
Function   ZClose:Boolean;
Function   CheckStart:Boolean;
Function   CheckRetStart:Boolean;
Function   CheckAddPos(Var iSum:Integer):Boolean;
Function   CheckTotal(Var iTotal:Integer):Boolean;
Function   CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Function   CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Function   CheckClose:Boolean;
Procedure  CheckCancel;
function  InCass(rSum:Real):Boolean;//����������
Function  GetCashReg(Var rSum:Real):Boolean;//���������� � �����

Function    CashDriver:Boolean;
Procedure   TestPosCh;

Function   OpenNFDoc:Boolean;
Function   CloseNFDoc:Boolean;
Function   PrintNFStr(StrPr:String):Boolean;
Function   SelectF(iNum:Integer):Boolean;
Function   CutDoc:Boolean;
Function   CutDoc1:Boolean;

procedure   WriteHistoryFR(Strwk_: string);
Function    TestStatus(StrOp:String; Var SMess:String):Boolean;

function   IsOLEObjectInstalled(Name: String): boolean;
function   More24H(Var iSt:INteger):Boolean;
Function   CheckOpenFis:Boolean;

Function   GetNumCh:Boolean;

Function   GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean;
Function   SetDP(Str1,Str2:String):Boolean;
Function   CheckOpen:Boolean;

procedure prBnPrint;
Procedure TestFp(StrOp:String);


Type
     TStatusFR = record
     St1:String[2];
     St2:String[4];
     St3:String[4];
     St4:String[10];
     end;

     TNums = record
     bOpen:Boolean;
     ZNum:Integer;
     SerNum:String[11];
     RegNum:string[10];
     CheckNum:string[4];
     iCheckNum:Integer;
     iRet:INteger;
     sRet:String;
     sDate:String;
     ZYet:Integer;
     sDateTimeBeg:String;
     end;


     TCurPos = record
     Articul:String;
     Bar:String;
     Name:String;
     EdIzm:String;
     TypeIzm:INteger;
     Quant:Real;
     Price:Real;
     DProc:Real;
     DSum:Real;
     Depart:SmallInt;
     end;

     TPos = record
     Name:String;
     Code:String;
     AddName:String;
     Price:Integer;
     Count:Integer;
     Stream: Integer;
     Sum: Real;
     Bar:String;
     end;


var
  CommandsArray : array [1..32000] of Char;
  HeapStatus    : THeapStatus;
  PressCount    : Byte;
  StatusFr      : TStatusFr;
  Nums          : TNums;
  CurPos        :TCurPos;
  SelPos        :TCurPos;
  sMessage      :String;
  PosCh         : TPos;
  Drv: OleVariant;
  iRfr:SmallInt;
  rSumR,rSumD:Real;

Const iPassw:Integer = 30;

implementation

uses Un1, Dm, MainFF, Attention;

Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;


procedure prBnPrint;
begin
//����� ����� ������ ������ ����� ���������� �� ����������
  try
//      with fmMainCasher do
//      begin
          if  BnStr>'' then
          begin
            if OpenNFDoc=false then
            begin
              TestFp('PrintNFStr');
              delay(100);
              OpenNFDoc;
            end;

            while BnStr>'' do
            begin
              if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
              else
              begin
                if ord(BnStr[1])=$01 then
                begin
                  try
                    if CloseNFDoc=False then
                    begin
                      TestFp('PrintNFStr');
                      CloseNFDoc;
                    end;

                    if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
                    begin
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      CutDoc;
                    end;

                    delay(33);

                    if OpenNFDoc=false then
                    begin
                      TestFp('PrintNFStr');
                      OpenNFDoc;
                    end;
                  finally
                    Delete(BnStr,1,1);
                  end;
                end
                else
                begin
                  if Pos(Chr($0D),BnStr)>0 then
                  begin
                    StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
                    Delete(BnStr,1,Pos(Chr($0D),BnStr));
                  end
                  else
                  begin
                    if Pos(Chr($01),BnStr)>0 then
                    begin
                      StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                      Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
                    end
                    else
                    begin
                      StrWk:=BnStr;
                      BnStr:='';
                    end;
                  end;
                  if pos('�����',StrWk)>0 then PrintNFStr('')
                  else
                  if PrintNFStr(StrWk)=False then
                  begin
                     TestFp('PrintNFStr');
                     PrintNFStr(StrWk);
                  end;
                end;
              end;
            end;
            if CloseNFDoc=False then
            begin
              TestFp('PrintNFStr');
              CloseNFDoc;
            end;
       //   end;
      end;
  except
    prWriteLog('ErrPrint');
  end;
end;


Function CheckOpen:Boolean;
//Var iSt:INteger;
begin
  if Commonset.CashNum<=0 then begin Result:=False; exit; end;
  result:=False;
//  exit;
{ ���� �� ��� ��������� - ��� ������������� ������ �������

  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  iSt:=Drv.ECRMode;
  if iSt=8 then
  begin
    Result:=True;
  end;}
end;


Function SetDP(Str1,Str2:String):Boolean;
begin
  if (Commonset.CashNum<=0) then begin Result:=True; exit; end;
  iRfr:=Drv.EEJShowClientDisplayText(Str1,Str2);
  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean;
Var cSum1,cSum2,cSum3,cSum4:Currency;
begin
  rSum1:=0; rSum2:=0; rSum3:=0; rSum4:=0;
  if (Commonset.CashNum<=0) then begin inc(Nums.iCheckNum); Result:=True; exit; end;

  iRfr:=Drv.GetDiscountAndExtraSums(cSum1,cSum2,cSum3,cSum4);
  rSum1:=cSum1;
  rSum2:=cSum2;
  rSum3:=cSum3;
  rSum4:=cSum4;

  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetNumCh:Boolean;
Var iNum:SmallINt;
begin
  if (Commonset.CashNum<=0)    then begin inc(Nums.iCheckNum); Result:=True; exit; end;
  iNum:=0;
  iRfr:=Drv.GetCurrentReceiptNum(iNum); //����� ���������� ����
  Nums.iCheckNum:=iNum;
  Nums.CheckNum:=IntToStr(Nums.iCheckNum);

  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����
Var cSum:Currency;
begin
  rSum:=-1;
  if (Commonset.CashNum<=0)    then begin inc(Nums.iCheckNum); Result:=True; exit; end;

  iRfr:=Drv.GetCashInDrawer(cSum);
  rSum:=cSum;

  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function InCass(rSum:Real):Boolean;
var cSum,cSum1:Currency;
begin
  Result:=True;
  if (Commonset.CashNum<=0)    then begin exit; end;

  cSum:=rv(abs(rSum));
  if rSum>0 then  //�������� �����
  begin
    if iRfr=0 then iRfr:=Drv.OpenReceipt(4, 1, Person.Name, Nums.iCheckNum);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CashInOut('����� ',cSum,cSum1);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CloseReceipt;
  end else //������
  begin
    if iRfr=0 then iRfr:=Drv.OpenReceipt(5, 1, Person.Name, Nums.iCheckNum);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CashInOut('����� ',cSum,cSum1);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CloseReceipt;
  end;

  if iRfr=0 then Result:=True
  else Result:=False;
end;



Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
begin
  result:=True;
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  rSum1:=0;
  rSum2:=0;
  rSum3:=0;
end;


function More24H(Var iSt:INteger):Boolean;
begin
  result:=False;
  if (Commonset.CashNum<=0)    then begin Result:=False; exit; end;
//  Drv.GetECRStatus;
//  iSt:=Drv.ECRMode;
//  if Drv.ECRMode=3 then Result:=True;
end;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


Function GetSt(Var sMess:String):Integer;
begin
  if (Commonset.CashNum<=0)    then begin Result:=0; exit; end;

  result:=0;
  sMess:='';
end;



Function TestStatus(StrOp:String; Var SMess:String):Boolean;
Var St1,St2,St3:ShortINt;
begin
  Result:=True;
  sMess:='';
  if (Commonset.CashNum<=0)    then exit;

  St1:=0; St2:=0; St3:=0;

  if iRfr=0 then iRfr:=Drv.GetStatus(St1,St2,St3);

  Drv.GetErrorMesageRU(iRfr,sMess);

  if (iRfr<>0)or(St1<>0)or((St2<>2)and(St2<>0))or(St3<>0) then
  begin
    prWriteLog('--'+StrOp+';'+its(iRfr)+';'+its(St1)+';'+its(St2)+';'+its(St3)+';'+sMess);
    Result:=False; //���� ���������� �.�. ����� ����������� ������� �������, �������� ������� ����
  end;
end;

procedure WriteHistoryFR(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='fr'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Function CutDoc:Boolean;
begin
  if (Commonset.CashNum<=0)    then
  begin

    dmC.prCutDoc(CommonSet.PortCash,0);

    Result:=True;
    exit;
  end;
  iRfr:=Drv.CutAndPrint;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function CutDoc1:Boolean;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  iRfr:=Drv.CutAndPrint;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function SelectF(iNum:Integer):Boolean;
begin
  Result:=True;
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
end;


Function PrintNFStr(StrPr:String):Boolean;
begin
  Result:=True;
  if (Commonset.CashNum>0) then
  begin
    if Length(StrPr)>40 then StrPr:=copy(StrPr,1,40);
    iRfr:=Drv.PrintString(StrPr);
    if iRfr=0 then Result:=True
    else Result:=False;
    delay(30);
  end;
end;


Function OpenNFDoc:Boolean;
begin
  if (Commonset.CashNum<=0)    then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);

    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(1, 1, Person.Name, Nums.iCheckNum);  //1 - ������������ ��������
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(30);
end;

Function CloseNFDoc:Boolean;
begin
  if (Commonset.CashNum<=0)    then
  begin
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;
  iRfr:=Drv.CloseReceiptEx(True); //c �������
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(30);
end;


Function CashDriver:Boolean;
begin
  iRfr:=Drv.OpenCashDrawer;
  if iRfr=0 then Result:=True else Result:=False;
end;


Function CheckClose:Boolean;
begin
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
  iRfr:=Drv.CloseReceipt;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Var rSumSd:Currency;
//    rCli:Real;
//    Str1,Str2:String;
    ValName:String;
begin
  iDopl:=0;
  ValName:='';
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
  if iPayType=0 then //���
  begin
    iRfr:=Drv.Payment(0,iClient/100,rSumSd);
    iDopl:=RoundEx(rSumSd*100);
  end else
  begin
    iRfr:=Drv.Payment(1,iClient/100,rSumSd);
    iDopl:=RoundEx(rSumSD*100);
  end;
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);

end;

Function CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Var iDiscount:Integer;
    rDiscCurr,rItog:Currency;
    sDisc:String;
begin
  sDisc:='������';
  iDiscount:=RoundEx(rDiscount*100);
  iDisc:=iDiscount;
  iItog:=0;
  if (Commonset.CashNum<=0) then begin Result:=True; exit; end;
  if iDiscount>=0 then
  begin //������
    rDiscCurr:=iDiscount/100;
    iRfr:=Drv.DiscountTotal(sDisc,rDiscCurr,rItog);
  end else
  begin
  end;
  if iRfr=0 then
  begin
    iItog:=RoundEx(rItog*100);
    Result:=True;
  end
  else Result:=False;
  delay(100);
end;

Function CheckTotal(Var iTotal:Integer):Boolean;
Var rSum:Currency;
//    Str1,Str2:String;
//    rDiscDop:Real;
begin
  iTotal:=0;
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
  iRfr:=Drv.SubTotal(rSum);

  if iRfr=0 then
  begin
    iTotal:=RoundEx(rSum*100);
    Result:=True;
  end
  else Result:=False;

  delay(100);

end;


Procedure CheckCancel;
begin
  if (Commonset.CashNum<=0)    then
  begin
    exit;
  end;
  iRfr:=Drv.BreakReceipt;
end;

Procedure TestPosCh;
begin
//  if Length(PosCh.Name)>36 then PosCh.Name:=Copy(PosCh.Name,1,36);
//  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
//  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);

  if Length(PosCh.Name)>32 then PosCh.Name:=Copy(PosCh.Name,1,32)+' ';
  if Length(PosCh.Code)>5 then PosCh.Code:=Copy(PosCh.Code,1,5);
end;


Function CheckAddPos(Var iSum:Integer):Boolean;
Var rSum,rPr:Currency;
    rQ:Real;
//    Str1,Str2:String;
begin
  iSum:=RoundEx(rv(PosCh.Price)*RoundEx(PosCh.Count*1000)/1000 *100);
  TestPosCh;
  rPr:=RoundEx(PosCh.Price)/100;

  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;


  prWriteLog('~~FisPos;'+IntToStr((Nums.iCheckNum))+';'+PosCh.Code+';'+PosCh.Bar+';'+FloatToStr(PosCh.Count)+';'+FloatToStr(rPr)+';');

  if PosCh.Count>=0 then
  begin
    rQ:=RoundEx(PosCh.Count)/1000;
    iRfr:=Drv.AddArticle(PosCh.Name,PosCh.Code, rQ, rPr, 1, rSum);

    if iRfr=0 then
    begin
      iSum:=RoundEx(rSum*100);
      Result:=True;
    end
    else Result:=False;

  end else
  begin
    rQ:=RoundEx(PosCh.Count)/1000*(-1);
    iRfr:=Drv.StornoArticle(PosCh.Name,PosCh.Code, rQ, rPr, 1, rSum);

    if iRfr=0 then
    begin
      iSum:=RoundEx(rSum*100)*(-1);
      Result:=True;
    end
    else Result:=False;

  end;

  delay(100);
end;


Function CheckStart:Boolean;
//Var StrWk:String;
begin
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(2, 1, Person.Name, Nums.iCheckNum);  //��� �� ������� - 2, ������ -1
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);
end;

Function CheckRetStart:Boolean;
begin
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(3, 1, Person.Name, Nums.iCheckNum);  //��� �� ������� - 3, ������-1
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);
end;


Function ZClose:Boolean;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  Result:=False;
  iRfr:=Drv.ZReport('');
  if iRfr=0 then
  begin
    Result:=True;
  end;
end;


Function ZOpen:Boolean;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  Result:=True;
end;

Function GetX:Boolean;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  Result:=False;
  iRfr:=Drv.XReport('');
  if iRfr=0 then
  begin
    Result:=True;
  end;
end;

Function CashDate(var sDate:String):Boolean;
Var iRet:Integer;
    iDD,iMM,iYY,iSS,iHH,iMI:Smallint;
begin
//
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  Result:=False;
  sDate:='';
  iRet:=Drv.GetCurrentDateTime(iDD,iMM,iYY,iHH,iMI,iSS);
  if iRet<>0 then
  begin
    Exit;
  end
  else
  begin
    sDate:=its(iDD)+'.'+its(iMM)+'.'+its(iYY)+':'+its(iHH)+':'+its(iMI)+':'+its(iSS);
    Result:=true;
  end;
end;


Function CashClose:Boolean;
Var iRet:Integer;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  Result:=False;
  iRet:=Drv.Disconnect;
  if iRet=0 then result:=True;
end;

Function CashOpen(sPort:PChar):Boolean;
Var StrWk:String;
    iP:INteger;
    sMess:String;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;

  CommonSet.TypeFis:='sp101';

  if not IsOLEObjectInstalled('SP101FRKLib.SP101FRObject') then
  begin
    showmessage('������� ����� �� ����������. !!!');
    Result:=False;
    exit;
  end;
  try
   Drv := CreateOleObject('SP101FRKLib.SP101FRObject');
  except
    showmessage('���������� ������� ������ "SP101FRKLib.SP101FRObject". !!!');
    Result:=False;
    exit;
  end;

  StrWk:=String(sPort);
  if pos('COM',StrWk)>0 then delete(StrWk,pos('COM',StrWk),3);
  iP:=StrToINtDef(StrWk,2);
  iRfr:=Drv.Connect(iP);
  if iRfr=0 then
  begin
    iRfr:=Drv.Init(Now); //������������� ���������� �������
    if iRfr=0 then result:=True else
    begin
      Drv.GetErrorMesageRU(iRfr,sMess);
      showmessage('������ ����� - '+sMess+' ���������� ������ ����������. ��c�� ���������� ������������� - ������������� �����.');
      result:=False;
    end;
  end else result:=False;
end;

Function GetNums:Boolean;
Var iNum,iZ,iKL:SmallINt;
begin
  if (Commonset.CashNum<=0)    then
  begin
    Result:=True;
    exit;
  end;

  Result:=True;

  iRfr:=Drv.GetCurrentReceiptNum(iNum); //����� ���������� ����
  if iRfr=0 then
  begin
    iRfr:=Drv.GetCurrentShift(iZ); //����� ������� �����
    if iRfr=0 then
    begin
      iRfr:=Drv.GetEEJStatus(iKl);
      if iRfr=0 then Result:=True else Result:=False;

      Nums.iCheckNum:=iNum;
      Nums.CheckNum:=IntToStr(Nums.iCheckNum);
      Nums.ZNum:=iZ;
      Nums.ZYet:=iKl;
    end;
  end;
end;

Function GetRes:Boolean;
begin
  Result:=True;
end;

Function GetSerial:Boolean;
Var sNum:String;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;

  iRfr:=Drv.GetFiscNumber(sNum);
  if iRfr=0 then Nums.SerNum:=sNum
  else Nums.SerNum:='';

  result:=True;
end;


Function OpenZ:Boolean;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  result:=True;
end;



Function InspectSt(Var sMess:String):Integer;
Var  St1,St2,St3:ShortINt;
begin
  sMess:='��� ��.';
  if (Commonset.CashNum<=0)    then begin Result:=0; exit; end;
  St1:=0; St2:=2; St3:=0;

  iRfr:=Drv.GetStatus(St1,St2,St3);
//���� ��� ������� ��� ��������
//  Drv.GetErrorMessageRU(iRfr,sMess);
  if (iRfr<>0)or(St1<>0)or((St2<>2)and(St2<>0))or(St3<>0) then
    prWriteLog('--GetStatus;'+Its(iRfr)+':'+its(St1)+';'+its(St2)+';'+its(St3)+';');

  result:=iRfr;
end;

Function CheckOpenFis:Boolean;
//Var iSt:INteger;
begin
  if (Commonset.CashNum<=0)    then begin Result:=True; exit; end;
  result:=True;
end;



end.
