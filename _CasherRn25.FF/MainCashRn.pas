unit MainCashRn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RXClock, StdCtrls, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxEdit, DB, cxDBData, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxGrid, cxGridCardView, cxGridDBCardView,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  dxfBackGround, cxDataStorage, Menus, FR_Class, FR_DSet, FR_DBSet, Grids,
  DBGrids;

type
  TfmMainCashRn = class(TForm)
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Panel2: TPanel;
    Label13: TLabel;
    RxClock1: TRxClock;
    Timer1: TTimer;
    Panel3: TPanel;
    Label1: TLabel;
    GridTab: TcxGrid;
    PersLevel: TcxGridLevel;
    PersView: TcxGridDBTableView;
    PersViewNAME: TcxGridDBColumn;
    PersViewsCountTab: TcxGridDBColumn;
    TabLevel: TcxGridLevel;
    TabView: TcxGridDBCardView;
    TabViewQUESTS: TcxGridDBCardViewRow;
    TabViewTABSUM: TcxGridDBCardViewRow;
    TabViewSSTAT: TcxGridDBCardViewRow;
    TabViewSTIME: TcxGridDBCardViewRow;
    Button1: TcxButton;
    Button2: TcxButton;
    Button3: TcxButton;
    Button4: TcxButton;
    Button5: TcxButton;
    amRn: TActionManager;
    acMenu1: TAction;
    acCreateTab: TAction;
    acDel: TAction;
    acMove: TAction;
    Button6: TcxButton;
    acRetPre: TAction;
    acCashEnd: TAction;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxButton1: TcxButton;
    Panel4: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    acCashRep: TAction;
    acExit: TAction;
    cxButton5: TcxButton;
    Label5: TLabel;
    dxfBackGround1: TdxfBackGround;
    Label6: TLabel;
    acOpen: TAction;
    Button7: TcxButton;
    acPrintPre: TAction;
    acCashPCard: TAction;
    Button8: TcxButton;
    frRepMain: TfrReport;
    frquCheck: TfrDBDataSet;
    TimerPrintQ: TTimer;
    TimerDelayPrintQ: TTimer;
    TabViewNAME: TcxGridDBCardViewRow;
    TiRefresh: TTimer;
    cxButton6: TcxButton;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel2Click(Sender: TObject);
    procedure TabViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acCreateTabExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure acRetPreExecute(Sender: TObject);
    procedure acCashEndExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acCashRepExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure TabViewDblClick(Sender: TObject);
    procedure acPrintPreExecute(Sender: TObject);
    procedure acCashPCardExecute(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure CreateViewPers(bCh:Boolean);
    Procedure DestrViewPers;
    procedure AssignBrush(ABrush: TBrush; ABitMap: TBitMap);
    Procedure prDelQu; //������ �� �������

    { Public declarations }
  end;

Procedure WriteStatus;

var
  fmMainCashRn: TfmMainCashRn;
  TableImage:TBitmap;
  FGridBrush:TBrush;
  bOpenTable:Boolean=False; //true � �������� ��������, ���� ������ �������� �������
  arQ:Array[1..100] of Integer;


implementation

uses uPre, Un1, Dm, Menu, Calc, CreateTab, MessDel, Spec, CashEnd, UnCash,
  Attention, Discont, fmDiscountShape, uDB1;

{$R *.dfm}


Procedure TfmMainCashRn.prDelQu; //������ �� �������
Var i:Integer;
begin
  for i:=1 to 100 do
  if arQ[i]>0 then
  begin
    dmC1.quDelQu.ParamByName('IDQ').AsInteger:=arQ[i];
    dmC1.quDelQu.ExecQuery;
    delay(30);
  end;
end;

Procedure WriteStatus;
begin
  with fmMainCashRn do
  begin

    label8.caption:='����� � '+IntToStr(CommonSet.CashNum);
    Label2.Caption:='����: '+Nums.sDate;
    Label4.Caption:='����� �'+IntToStr(Nums.ZNum)+'  ��� � '+IntToStr(Nums.iCheckNum);
    Label3.Caption:='������ ���: ������ ('+IntToStr(Nums.iRet)+')';
    Label9.Caption:='���.�����  '+Nums.SerNum;
    Label6.Caption:='�������� (����) '+INtToStr(Nums.ZYet);

    Case Operation of
    0: begin
         Label5.Caption:='�������� - ������� ��������.';
       end;
    1: begin
         Label5.Caption:='�������� - �������.';
       end;
    end;


    if Nums.iRet=0 then Label3.Caption:='������ ���: '+Nums.sRet;
    if (Nums.iRet=2)or(Nums.iRet=22) then Label3.Caption:='������ ���: ���������� ������� �����';


    if CommonSet.CashNum=0 then label8.caption:='������� ���������.'
    else
    begin
      if CommonSet.CashNum>0 then label8.caption:='����� � '+intToStr(CommonSet.CashNum);
      if CommonSet.CashNum<0 then
      begin
        label8.caption:='������� �������.';
        Label2.Caption:='����: '+FormatDateTime('dd.mm.yyyy',date);
        Label4.Caption:='����� �'+IntToStr(Nums.ZNum)+'  ��� � '+IntToStr(Nums.iCheckNum);
      end;
    end;
  end;
end;



Procedure TfmMainCashRn.DestrViewPers;
begin
  Label13.Caption:='';
  Person.Id:=0;
  Person.Name:='';
  Panel3.Visible:=False;
  Panel4.Visible:=False;
  Button1.Visible:=False;
  Button2.Visible:=False;
  Button3.Visible:=False;
  Button4.Visible:=False;
  Button5.Visible:=False;
  Button6.Visible:=False;
  Button7.Visible:=False;
  Button8.Visible:=False;
  cxButton1.Visible:=False;
  cxButton6.Visible:=False;

  acOpen.Enabled:=False;
  dxfBackGround1.BkColor.BeginColor:=$00E8CDB7; //��� ������� �����
  dxfBackGround1.BkColor.EndColor:=$0058371D;

//  dxfBackGround1.BkColor.BeginColor:=$00A6A6A6; //��� ����� �����
//  dxfBackGround1.BkColor.EndColor:=$00000000;
//  Panel1.Color:=$00252525;
//  Panel2.Color:=$00BBBBBB;
//  RxClock1.Color:=$00000000;
end;


Procedure TfmMainCashRn.CreateViewPers(bCh:Boolean);
Var TiShift:TDateTime;
begin

  if bCh=False then exit; //���� ��������� ������� ������ �� ������ �������
  bChangeView:=False; //������� ������ �� ������ ������ ���������� ��� ������.
  TiShift:=Now;
  TiShift:=TiShift-(CommonSet.HourShift/24);

  Label13.Caption:=Person.Name;
  with dmC do
  begin

    PersView.BeginUpdate;
    quPers.Active:=False;
    dsPers.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end
    else
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quPers.SelectSQL.Add('and t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end;

    quPers.Active:=True;
    dsPers.DataSet:=quPers;
    PersView.EndUpdate;


    TabView.BeginUpdate;
    quTabs.Active:=False;
    dsTabs.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMTABLE');
    end
    else
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quTabs.SelectSQL.Add('and t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMTABLE');
    end;

//t.BEGTIME>'27.01.2008 01:00'

    quTabs.Active:=True;
    dsTabs.DataSet:=quTabs;
    TabView.EndUpdate;

    PersView.Focused:=True;
    quPers.Last;
//    delay(10);

    while not quPers.Bof do
    begin
      PersView.Controller.FocusedRow.Expand(True);
      quPers.Prior;
//      delay(10);
    end;

    if CanDo('prAddAll')or CanDo('prAdd') then
    begin
      Button1.Visible:=True;
      acCreateTab.Enabled:=True;
    end
    else acCreateTab.Enabled:=False;
    if CanDo('prEditAll')or CanDo('prEdit') then
    begin
      Button2.Visible:=True;
      acOpen.Enabled:=True;
    end
    else acOpen.Enabled:=False;
    if CanDo('prDelAll')or CanDo('prDel') then
    begin
      Button3.Visible:=True;
      acDel.Enabled:=True;
    end
    else acDel.Enabled:=False;
    if CanDo('prMovAll') then
    begin
      Button4.Visible:=True;
      acMove.Enabled:=True;
    end
    else acMove.Enabled:=False;

    if CanDo('prPrintCheck') then
    begin
      if CommonSet.CashNum<>0 then
      begin
        Button5.Visible:=True; //��� =0 - ������� ���������;
        acCashEnd.Enabled:=True;
      end
      else acCashEnd.Enabled:=False;
    end
    else acCashEnd.Enabled:=False;

    if CanDo('prRetPre') then
    begin
      Button6.Visible:=True;
      acRetPre.Enabled:=True;
    end
    else acRetPre.Enabled:=False;

    Button7.Visible:=True; //�������� ����

    if CanDo('prPrintBNCheck') then
    begin
      Button8.Visible:=True;
      acCashPCard.Enabled:=True;
    end
    else acCashPCard.Enabled:=False;


    if CanDo('prCashRep') then
    begin
      cxButton1.Visible:=True;
      acCashRep.Enabled:=True;
    end
    else acCashRep.Enabled:=False;

    cxButton6.Visible:=True;
    Label1.Caption:='�������� ������ �� ��������� '+IntToStr(CommonSet.HourShift)+' �.';

  end;
end;

procedure TfmMainCashRn.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=768;
end;

procedure TfmMainCashRn.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  Person.Id:=0;
  Person.Name:='';
  fmPre.ShowModal;
end;

procedure TfmMainCashRn.FormShow(Sender: TObject);
//Var MaxDate:TDateTime;
//Const WarnMess:String = '����� �������� ��������� �������.';
begin
//
//  delay(3000);

{  StrWk:=#$33+#$30+#$2E+#$31+#$31+#$2E+#$32+#$30+#$30+#$37;
  MaxDate:=StrToDate(StrWk);
  if MaxDate<Date then
  begin
    ShowMessage(WarnMess);
//    close;
    delay(300000);
  end;

  StrWk:=#$33+#$30+#$2E+#$31+#$31+#$2E+#$32+#$30+#$30+#$37;
  MaxDate:=StrToDate(StrWk);
  if MaxDate<Date then
  begin
    ShowMessage(WarnMess);
    close;
//    delay(300000);
  end;
}

  Timer1.Enabled:=True;
  with dmC do
  begin
    try
      CasherRnDb.Connected:=False;
      CasherRnDb.DatabaseName:=DBName;
      CasherRnDb.Connected:=True;

      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=0; //������
      quMenu.Active:=True;

    except
      ShowMessage('������ �������� ���� - '+DBName);
      Close;
    end;
  end;
  with dmC1 do
  begin
    try
      CasherRnDb1.Connected:=False;
      CasherRnDb1.DatabaseName:=DBName;
      CasherRnDb1.Connected:=True;
    except
      Close;
    end;
  end;
end;

procedure TfmMainCashRn.FormCreate(Sender: TObject);
Var StrWk:String;
begin

  CurDir := ExtractFilePath(ParamStr(0));
  Label8.Caption:='';
  Label9.Caption:='';
  Label2.Caption:='';
  Label3.Caption:='';
  Label4.Caption:='';

  Operation:=0; //�������� �������
  Label5.Caption:='�������� - ������� ��������.';

  ReadIni;

  dxfBackGround1.BkColor.BeginColor:=$00E8CDB7; //��� ������� �����
  dxfBackGround1.BkColor.EndColor:=$0058371D;

//  dxfBackGround1.BkColor.BeginColor:=$00A6A6A6; //��� ����� �����
//  dxfBackGround1.BkColor.EndColor:=$00000000;
//  Panel1.Color:=$00252525;
//  Panel2.Color:=$00BBBBBB;
//  RxClock1.Color:=$00000000;


//��������� �������� Nums

  Nums.ZNum:=CommonSet.CashZ;
  Nums.SerNum:='0';
  Nums.RegNum:='0';
  Nums.CheckNum:=INtToStr(CommonSet.CashChNum);
  Nums.iCheckNum:=CommonSet.CashChNum;
  Nums.iRet:=0;
  Nums.sRet:='���';
  Nums.sDate:=FormatDateTime(sFormatDate,Date);


  TimerDelayPrintQ.Interval:=CommonSet.iStartDelaySec;
  TimerDelayPrintQ.Enabled:=True;


  // ���������� �������

//SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_HIDEWINDOW );

  //�������� ����� ����� ��� ����������� �������
  if CommonSet.CashNum>0 then
  begin
    try
      if CommonSet.CashPort>0 then
      begin
        StrWk:='COM'+IntToStr(CommonSet.CashPort);
        if not CashOpen(PChar(StrWk)) then   //dll �������
        begin
          CommonSet.CashNum:=0;
          showmessage('������ �������� �����.');
        end;
      end
      else
      begin
        CommonSet.CashNum:=0;
      end;
    except
      CommonSet.CashNum:=0;
    end;
  end;

  if CommonSet.CashNum>0 then //��������� ���� ������ - �������� �����
  begin
//    Label9.Caption:='���.�����  '+Nums.SerNum;

    Nums.iRet:=InspectSt(Nums.sRet);
    if (Nums.iRet=0) or (Nums.iRet=2)or(Nums.iRet=22)or(Nums.iRet=21)or(Nums.iRet=1) then Nums.bOpen:=True;
    if CheckOpen then
    begin
      CheckCancel;
      Nums.iRet:=InspectSt(Nums.sRet);
    end;
    GetSerial;
    CashDate(Nums.sDate);
    GetNums; //������
    GetRes; //�������

{    if (Nums.iRet<>1) then //�������� ������� ���� ������� �����
    begin
      GetSerial;
      CashDate(Nums.sDate);
      GetNums; //������
      GetRes; //�������
    end;}
  end;

  WriteStatus;

  StrPre:='';
  Person.Id:=0;
  Person.Name:='';

  TableImage:=TBitMap.Create;
  TableImage.LoadFromFile(CurDir+'Pict1.bmp');
  FGridBrush := TBrush.Create;

  if CommonSet.iQuestVisible=1 then
  begin
    TabViewQUESTS.Visible:=True;
    TabViewNAME.Visible:=False;
  end;
  if CommonSet.iQuestVisible=2 then
  begin
    TabViewQUESTS.Visible:=False;
    TabViewNAME.Visible:=True;
  end;
  if CommonSet.iQuestVisible=3 then
  begin
    TabViewQUESTS.Visible:=True;
    TabViewNAME.Visible:=True;
  end;

  Label1.Caption:='�������� ������ �� ��������� '+IntToStr(CommonSet.HourShift)+' �.';
end;

procedure TfmMainCashRn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_SHOWWINDOW);

  if CommonSet.CashNum>0 then CashClose;

  TableImage.Free;
  FGridBrush.Free;
  Panel3.Visible:=False;
  dmC.quTabs.Active:=False;
  dmC.quPers.Active:=False;
  dmC.CasherRnDb.Connected:=False;
//  delay(500);
end;

procedure TfmMainCashRn.Panel2Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmMainCashRn.AssignBrush(ABrush: TBrush; ABitMap: TBitMap);
begin
  FGridBrush.Bitmap := ABitMap;
  ABrush.Assign(FGridBrush);
end;


procedure TfmMainCashRn.TabViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);

var
  ARect: TRect;
  ATextToDraw: String;

  procedure SetTextToDraw;
  begin
    if (AViewInfo is TcxGridCardRowDataViewInfo) then
    begin
      ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index];
    end
    else
      ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);
  end;

begin
{ remove/add the closing brace on this line to disable/enable the following code}

  ARect := AViewInfo.Bounds;
//  ACanvas.Canvas.Font.Assign(FFont); ===
  SetTextToDraw;

  AssignBrush(ACanvas.Canvas.Brush, TableImage);

  ACanvas.Canvas.FillRect(ARect);

  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
 // SetBkMode(ACanvas.Canvas.Handle, 50); ===

  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0);
  ADone := true;
end;

procedure TfmMainCashRn.Button2Click(Sender: TObject);
Var sMessage:String;
begin
//  acOpenTab.Execute;
  delay(10);
  if bPrintCheck then
  begin
    sMessage:='���������� ��������� ��������� ����.';
    fmAttention.Label1.Caption:=sMessage;
    prWriteLog('~~AttentionShow;'+sMessage);
    fmAttention.ShowModal;
    exit;
  end;
  acOpen.Execute;
end;

procedure TfmMainCashRn.Button5Click(Sender: TObject);
begin
  if bPrintCheck then
  begin
    sMessage:='���������� ��������� ��������� ����.';
    fmAttention.Label1.Caption:=sMessage;
    prWriteLog('~~AttentionShow;'+sMessage);
    fmAttention.ShowModal;
    exit;
  end;
  acCashEnd.Execute;
end;

procedure TfmMainCashRn.Button6Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainCashRn.Button1Click(Sender: TObject);
begin
  acCreateTab.Execute;
end;

procedure TfmMainCashRn.acCreateTabExecute(Sender: TObject);
Var StrWk,SCapt:String;
begin
  if  bPrintCheck then exit;
  if not CanDo('prAdd') then exit;
  fmCreateTab:=TfmCreateTab.Create(Application);
  with fmCreateTab do
  begin
    TextEdit1.Text:='0';
    CalcEdit1.EditValue:=0;

    dmC.quPersonal1.Active:=True;
    fmCreateTab.ComboBox1.EditValue:=Person.Id;
    if not CanDo('prEditAll') then
    begin
      fmCreateTab.ComboBox1.Properties.ReadOnly:=True;
    end;
    if CommonSet.IncNumTab>0 then
    begin
      StrWk:=IntToStr(CommonSet.IncNumTab);
      while Length(StrWk)<3 do StrWk:='0'+StrWk;
      fmCreateTab.TextEdit1.Text:=StrWk;
      inc(CommonSet.IncNumTab); WriteTabNum;
    end;
    fmCreateTab.ShowModal;
    if fmCreateTab.ModalResult=mrOk then
    begin
      Tab.Id_Personal:=fmCreateTab.ComboBox1.EditValue;
      Tab.Name:=fmCreateTab.ComboBox1.Text;
      Tab.OpenTime:=Now;
      Tab.NumTable:=fmCreateTab.TextEdit1.Text;
      Tab.Quests:=fmCreateTab.CalcEdit1.EditValue;
      Tab.iStatus:=0; //������� �� �����
      Tab.DBar:='';
      Tab.Id:=GetId('TabH');
      Tab.Summa:=0;
      Check.Max:=0;

      FormLog('CreateTab',IntToStr(Tab.Id_Personal)+' '+Tab.NumTable);

      with dmC do
      begin
        quCurSpec.Active:=False;
        quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpec.Active:=True;
        quCurSpec.First;

        quCurMod.Active:=False;
        quCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurMod.ParamByName('IDP').AsInteger:=0;
        quCurMod.Active:=True;
      end;

      fmSpec.SetStatus(SCapt);

      fmSpec.Label7.Caption:=Tab.Name;
      fmSpec.BEdit1.Text:=Tab.NumTable;
      if Tab.Quests>0 then fmSpec.SEdit1.EditValue:=Tab.Quests else fmSpec.SEdit1.EditValue:=1;
      fmSpec.Label10.Caption:=FormatdateTime('dd.mm.yyyy hh:nn',Tab.OpenTime);
      fmSpec.Label11.Caption:=SCapt;
      fmSpec.Label17.Caption:='';

      fmSpec.ViewSpecDPROC.Visible:=False;
      fmSpec.ViewSpecDSum.Visible:=False;

      dmC.quPersonal1.Active:=False;
      fmCreateTab.Release;

      fmSpec.ShowModal;
    end
    else
    begin
      dmC.quPersonal1.Active:=False;
      fmCreateTab.Release;
    end;
  end;
  delay(100);
  CreateViewPers(bChangeView);
end;

procedure TfmMainCashRn.acDelExecute(Sender: TObject);
Var StrWk:String;
begin
  if not CanDo('prDel') then exit;
  if dmC.quTabs.Eof then exit;

  fmMessDel:=TfmMessDel.Create(Application);
  if not CanDo('prDelNoSkl') then fmMessDel.cxButton1.Enabled:=False;

  with dmC do
  begin
    fmMessDel.Label1.Caption:='����  '+quTabsNUMTABLE.AsString;
    Str(quTabsTABSUM.AsCurrency:10:2,StrWk);
    fmMessDel.Label2.Caption:='����� '+StrWk;

    fmMessDel.ShowModal;

    if fmMessDel.ModalResult=mrYes then
    begin
      FormLog('DelTab0',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);

      prSaveToAll.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
      prSaveToAll.ParamByName('OPERTYPE').AsString:='Del';
      prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
      prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
      prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
      prSaveToAll.ExecProc;

//       ������ ����
      quDelTab.Active:=False;
      quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

      trDel.StartTransaction;
      quDelTab.Active:=True;
      trDel.Commit;

      fmMainCashRn.CreateViewPers(True);
    end;
    if fmMessDel.ModalResult=mrNo then
    begin
      FormLog('DelTab1',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);

      prSaveToAll.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
      prSaveToAll.ParamByName('OPERTYPE').AsString:='Del';
      prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
      prSaveToAll.ParamByName('SKLAD').AsInteger:=1; //�������� �� �����������
      prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
      prSaveToAll.ExecProc;
//       ������ ����
      quDelTab.Active:=False;
      quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

      trDel.StartTransaction;
      quDelTab.Active:=True;
      trDel.Commit;

      fmMainCashRn.CreateViewPers(True);
    end;
  end;
  fmMessDel.Release;
end;

procedure TfmMainCashRn.Button3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmMainCashRn.acMoveExecute(Sender: TObject);
  //����������� �� ������� ���������
begin
  if not CanDo('prMoveAll') then exit;
  if dmC.quTabs.Eof then exit;

  fmCreateTab:=TfmCreateTab.Create(Application);
  with fmCreateTab do
  begin
    TextEdit1.Text:=dmC.quTabsNUMTABLE.AsString;
    CalcEdit1.EditValue:=dmC.quTabsQUESTS.AsInteger;

    dmC.quPersonal1.Active:=True;
    fmCreateTab.ComboBox1.EditValue:=Person.Id;
    Label1.Caption:='���� ��������� �����?';
    fmCreateTab.Caption:='������� ������.';


    fmCreateTab.ShowModal;
    if fmCreateTab.ModalResult=mrOk then
    begin
      with dmC do
      begin

        Tab.Id:=quTabsID.AsInteger;
        Tab.Id_Personal:=fmCreateTab.ComboBox1.EditValue;
        Tab.NumTable:=fmCreateTab.TextEdit1.Text;
        Tab.Quests:=fmCreateTab.CalcEdit1.EditValue;

        FormLog('MoveTab',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+intToStr(Tab.Id_Personal)+' '+Tab.NumTable);

        TabView.BeginUpdate;

//        trUpdate.StartTransaction;
        quTabs.Edit;
        quTabsID_PERSONAL.AsInteger:=Tab.Id_Personal;
        quTabsNUMTABLE.AsString:=Tab.NumTable;
        quTabsQUESTS.AsInteger:=Tab.Quests;
        quTabs.Post;
//        trUpdate.Commit;
        TabView.EndUpdate;
        quTabs.Locate('ID',Tab.Id,[]);

        fmMainCashRn.CreateViewPers(True);
      end;
    end;
    dmC.quPersonal1.Active:=False;
    fmCreateTab.Release;
  end;
end;

procedure TfmMainCashRn.Button4Click(Sender: TObject);
begin
  acMove.Execute;
end;

procedure TfmMainCashRn.acRetPreExecute(Sender: TObject);
begin
  if not CanDo('prRetPre') then exit;
  if dmC.quTabs.Eof then exit;
  with dmC do
  begin
    if quTabsISTATUS.AsInteger=1 then // � ��������� ��������
    begin
      Tab.Id:=quTabsID.AsInteger;

      FormLog('RetPre',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);

      TabView.BeginUpdate;

//      trUpdate.StartTransaction;
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=0;
      quTabs.Post;
//      trUpdate.Commit;
      TabView.EndUpdate;
      quTabs.Locate('ID',Tab.Id,[]);

//      fmMainCashRn.CreateViewPers;
      quTabs.Refresh;
    end;
  end;
end;

procedure TfmMainCashRn.acCashEndExecute(Sender: TObject);
Var strWk,StrWk1:String;
    iRet,IdH,IdC:Integer;
    bCheckOk:Boolean;
    rDiscont,rSum,rDProc:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    iAvans,iSumA,iQ:INteger;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;

begin
  if not CanDo('prPrintCheck') then begin  exit;end;
  if PrintQu then
  begin
    showmessage('������� �����, ��������� �������.');
    prWriteLog('������� �����, ��������� �������.');
    exit;
  end;
  with dmC do
  begin
    if quTabs.Eof then begin  exit; end;

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      prWriteLog('~~AttentionShow;'+'������ ����� 24 �����. ���������� ������� �����.');
      fmAttention.ShowModal;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    TabCh.Id:=quTabsID.AsInteger;

    //���� ��������� �������� �� ������ �������� iAvans
    // 1 ����� ������� ������ ����� ����  ---- ������������ ������
    // 2 ����� ������ = ����� ����  ----- TabCh.Summa=0
    // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    rSum:=0; iSumA:=0;
    iAvans:=0;
    quCheck.First;
    while not quCheck.Eof do
    begin
      rSum:=rSum+quCheckSUMMA.AsFloat;
      if quCheckPRICE.AsFloat<0 then iSumA:=RoundEx(quCheckSUMMA.AsFloat*100);
      quCheck.Next;
    end;
    if iSumA<0 then //������ �� ���� � ������
    begin
      if rSum>0 then iAvans:=3; // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���
      if rSum=0 then iAvans:=2; // 2 ����� ������ = ����� ����
      if rSum<0 then iAvans:=1; // 1 ����� ������� ������ ����� ����  ---- ������������ ������
      if not CanDo('prAvansCheck') then
      begin
        fmAttention.Label1.Caption:='��� ���� ��� ������ � �������� !!!.';
        prWriteLog('~~AttentionShow;'+'��� ���� ��� ������ � �������� !!!.');
        fmAttention.ShowModal;

        exit;
      end;
    end;

    TabView.BeginUpdate;
    quTabs.Refresh; //���� ������� ��� �� ������� �� ������ ������� - ������ ���-�� �����������
    TabView.EndUpdate;
    delay(10);

    TabCh.Id_Personal:=quTabsID_PERSONAL.AsInteger;
    taPersonal.Active:=true;
    taPersonal.Locate('Id',TabCh.Id_Personal,[]);
    TabCh.Name:=taPersonalNAME.AsString;
    Tab.Name:=TabCh.Name; //����� ��� ������ ��������� �� ���� � �������
    taPersonal.Active:=False;

    TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
    TabCh.NumTable:=quTabsNUMTABLE.AsString;
    TabCh.Quests:=quTabsQUESTS.AsInteger;
    TabCh.iStatus:=quTabsISTATUS.AsInteger;
    TabCh.DBar:=quTabsDISCONT.AsString;
    TabCh.Summa:=quTabsTABSUM.AsFloat;

    if TabCh.Summa<>rSum then
    begin  //������������ ���������� �� ��������� - ��� ������� ��-�� ����������� �������
           //���� �������� ���������
      TabView.BeginUpdate;
      quTabs.Refresh; //���� ������� ��� �� ������� �� ������ ������� - ������ ���-�� �����������
      TabView.EndUpdate;
      delay(10);

      TabCh.Summa:=rSum;
    end;

    if TabCh.Summa<0.01 then
    begin
      if iAvans<2 then  //��������, ��� �������� ������
      begin
        fmAttention.Label1.Caption:='��� � ������� � ������������� ������ �������� ������ !!!.';
        prWriteLog('~~AttentionShow;'+'��� � ������� � ������������� ������ �������� ������ !!!.');
        fmAttention.ShowModal;
        Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      end;
    end;

    if (Operation=1) and (iAvans>1) then
    begin
      fmAttention.Label1.Caption:='������ �������� ��� �������� (������)!!!.';
      prWriteLog('~~AttentionShow;'+'������ �������� ��� �������� (������)!!!.');
      fmAttention.ShowModal;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    taModif.Active:=True;

    if iAvans=2 then // 2 ����� ������ = ����� ����  ������� ������ ������������ ��������
    begin
      PosCh.Name:='';
      PosCh.Code:='';
      PosCh.Price:=0; //� ��������
      PosCh.Count:=0; //� �������
      PosCh.Sum:=0;

      try
        if CommonSet.PrePrintPort='fisprim' then
        begin
          OpenNFDoc;

          SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
          SelectF(14); PrintNFStr('       ������');
          SelectF(13); PrintNFStr('');
          PrintNFStr('��������: '+TabCh.Name);
          PrintNFStr('����: '+TabCh.NumTable);
          SelectF(13);PrintNFStr('������: '+IntToStr(TabCh.Quests));
          PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15);StrWk:=' ��������          ���-��   ����   ����� ';
          PrintNFStr(StrWk);
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15); PrintNFStr(' ');PrintNFStr(' ');

          iSumA:=iSumA*(-1);
          Str((rSum+iSumA/100):10:2,StrWk);
          StrWk:='����� ����� �����:    '+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          Str((iSumA/100):10:2,StrWk);
          StrWk:='������ ����� �� �����:'+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          StrWk:='����� �� �����       :      0,00 ���';
          PrintNFStr(StrWk); PrintNFStr(' ');

          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(11);
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CloseNFDoc;
          CutDoc;
        end;
        if pos('DB',CommonSet.PrePrintPort)>0 then
        begin
        //��������� �������
          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId('PQH0'); //��� ���������� ���������� - ����������� � ����� ������

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'       ������',14,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
            end;
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);


            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            StrWk:='����� ����� �����:    '+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            Str((iSumA/100):10:2,StrWk);
            StrWk:='������ ����� �� �����:'+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            StrWk:='����� �� �����       :      0,00 ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

            GetId('PQH'); //����������� ������� �� 1-�
            quPrint.Active:=False;
          end;
        end;

      finally

       //�������� � ������ ����� ��� ���������� ��������� - ��� �� �������

        prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;
        prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
        prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
        prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
        prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
        prSaveToAll.ExecProc;

         // ������ ����
        quDelTab.Active:=False;
        quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

        trDel.StartTransaction;
        quDelTab.Active:=True;
        trDel.Commit;
        //��� ������ �� ���� �.�. ����� =0
      end;
//      quTabs.Refresh;
      fmMainCashRn.CreateViewPers(True);
      exit;
    end;

    //������ �������� ������  iAvans:=3; ����� ������ < ����� ����
    with fmCash do
    begin
      Caption:='�������� ������';
      Label5.Caption:='���. '+TabCh.Name+'  ���� - '+TabCh.NumTable;
      Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      cEdit1.EditValue:=TabCh.Summa;
      cEdit2.EditValue:=TabCh.Summa;
      cEdit3.EditValue:=0;
      cEdit2.SelectAll;

      if CommonSet.CashNum>0 then
      begin
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          Label7.Caption:='������ ���: ��� ��.';
        end
        else //��� ������  - ����� ���� ������ ��� ���������� ���
        begin
          Label7.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

          fmAttention.Label1.Caption:=StrWk;
          prWriteLog('~~AttentionShow;'+StrWk);
          fmAttention.ShowModal;

          while TestStatus('InspectSt',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            Exit;   //�������  �  ���� �� ������ �� ���������� ������ �� ����
          end;
        end;
        //���� ����� �� ���� ��� � �����

        Label7.Caption:='������ ���: ��� ��.';
        fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
      end;
    end;

    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      try
        bCheckOk:=False;
        bPrintCheck:=False; //��� �� 2-��� �������
        bPrintCheck:=True;
        PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

      //������� ���
        if CommonSet.CashNum>0 then
        begin

       //�������� ������� �������� �����
          CashDriver;

          Case Operation of
          0: begin
//             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';');
               prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CheckStart;
               while TestStatus('CheckStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          1: begin
               prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CheckRetStart;
               while TestStatus('CheckRetStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          end;


          //�������

          rDiscont:=0;
          if iAvans=0 then
          begin
            rSum:=0;
            quCheck.First;
            while not quCheck.Eof do
            begin      //��������� �������
              PosCh.Name:=quCheckNAME.AsString;
              PosCh.Code:=quCheckSIFR.AsString;
              PosCh.AddName:='';
              PosCh.Price:=RoundEx(quCheckPRICE.AsFloat*100);
              PosCh.Count:=RoundEx(quCheckQUANTITY.AsFloat*1000);
              PosCh.Sum:=RoundEx(quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat*100)/100;

              rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
              rSum:=rSum+quCheckSUMMA.AsFloat;

              quCheck.Next;
              if not quCheck.Eof then
              begin
                while quCheckITYPE.AsInteger=1 do
                begin  //������������
                  if quCheck.Eof then break;
                  if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                    PosCh.AddName:=PosCh.AddName+'|   '+taModifNAME.AsString;
                  quCheck.Next;
                end;
                delete(PosCh.AddName,1,1);
              end;

              prWriteLog('~!CheckAddPos; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
              if PosCh.Sum>=0.01 then   //������� ������� �������� ������
              begin
                CheckAddPos(iSumPos);
                while TestStatus('CheckAddPos',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;
              end else prWriteLog('~!CheckAddPosBad; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
            end;
          end else //���� ���� ������ �� ��� ����� �������
          begin
            PosCh.Name:='� ������ �� �����';
            PosCh.Code:='';
            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            PosCh.AddName:='|����� ����� �����:    '+StrWk+'|������ ����� �� �����:';
            Str((iSumA/100):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+StrWk+'| ';
            PosCh.Price:=RoundEx(rSum*100); //� ��������
            PosCh.Count:=1000; //� �������
            PosCh.Sum:=RoundEx(rSum*100)/100;

            CheckAddPos(iSumPos);
            while TestStatus('CheckAddPos',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

          //������������ ���������� - ������ ����, ������, ������;
          CheckTotal(iSumTotal);
          prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');
          while TestStatus('CheckTotal',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
          end;

          iSumIt:=0; iSumDisc:=0;
          if rDiscont>0 then
          begin
            CheckDiscount(rDiscont,iSumDisc,iSumIt); //� ������ iSumIt=0 ������
            prWriteLog('!!AfterCheckDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
            while TestStatus('CheckDiscount',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

//        ��������, ���� ���� ����������� - ��������� ���
        //�������� iSumTotal
          if iSumIt=0 then iSumIt:=iSumTotal-iSumDisc;
          if abs(rSum-(iSumIt/100))>0.01 then
//          if abs(rSum-(iSumIt/100))>10000000 then
            begin
            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+'; ����. ����� '+FloatToStr(rSum)+'; ����. ����� '+INtToStr(iSumIt)+';');
            prClearCheck;
          end
          else
          begin
            if iCashType=0 then CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',iSumIt) //���
            else CheckRasch(1,RoundEx(fmCash.cEdit1.EditValue*100),'',iSumIt); //������

            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(fmCash.cEdit2.EditValue)+';'+INtToStr(iSumIt)+';');
            while TestStatus('CheckRasch',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

            prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');
            CheckClose;
            while TestStatus('CheckClose',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

          //�������� ������ ����� ������
            iRet:=InspectSt(StrWk);
            if iRet=0 then
            begin
              Label3.Caption:='������ ���: ��� ��.';
              if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
              if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
          //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
              WriteCheckNum;
            end
            else
            begin
              Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

              while TestStatus('InspectSt',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;
            end;

            bCheckOk:=True;
          end;
        end else //����� ������������, �� ��� �� �������� ����� .. ������� c ��������� ������
        begin
          if CommonSet.PrePrintPort<>'0' then
        //�� ����������� ��� STAR
          begin
            try
              prOpenDevPrint(CommonSet.PrePrintPort);

              SelFont(13);PrintStr(' '+CommonSet.DepartName); PrintStr('');

              SelFont(14); PrintStr('       ������');
              SelFont(13); PrintStr('');
              PrintStr('��������: '+TabCh.Name);
              PrintStr('����: '+TabCh.NumTable);
              SelFont(13);PrintStr('������: '+IntToStr(TabCh.Quests));
              PrintStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

              SelFont(13);StrWk:='                                        ';
              PrintStr(StrWk);
              SelFont(15);StrWk:=' ��������      ���-��   ����   �����';
              PrintStr(StrWk);
              SelFont(13);StrWk:='                                        ';
              PrintStr(StrWk);

             //�������
              rDiscont:=0;

              taStreams.Active:=False;
              taStreams.Active:=True;
              taStreams.First;
              while not taStreams.Eof do
              begin
                rSum:=0;

                quCheck.First;
                while not quCheck.Eof do
                begin      //��������� �������

                  PosCh.Name:=quCheckNAME.AsString;
                  PosCh.Code:=quCheckCODE.AsString;
                  PosCh.AddName:='';
                  PosCh.Stream:=quCheckSTREAM.AsInteger;

                  if PosCh.Stream=taStreamsID.AsInteger then
                  begin
                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
                    rSum:=rSum+quCheckSUMMA.AsFloat;

                    StrWk:= Copy(PosCh.Name,1,18);
                    while Length(StrWk)<18 do StrWk:=StrWk+' ';
                    Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'�';
                    SelFont(15);
                    PrintStr(StrWk);


                    quCheck.Next;
                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          PrintStr('   '+Copy(taModifNAME.AsString,1,29));
                        end;
                        quCheck.Next;
                      end;
                    end;
                  end else
                  quCheck.Next;
                end;
                if rSum<>0 then
                begin
                  SelFont(13);
                  StrWk:='                    ';
                  PrintStr(StrWk);
                  //�������� ������� �� ������
                  Str(rSum:8:2,StrWk1);
                  StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';

                  SelFont(15);
                  PrintStr(StrWk);
                  PrintStr('');
                end;


                taStreams.Next;
              end;

              SelFont(13);
              StrWk:='                                          ';
              PrintStr(StrWk);

              SelFont(15);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                    '+StrWk1+' ���';
              PrintStr(StrWk);

              if rDiscont>0.02 then
              begin
                SelFont(15);
                PrintStr('');
                SelFont(10);
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:2,StrWk1);
                Str(rDiscont:8:2,StrWk);
//                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                StrWk:=' � �.�. ������ - '+StrWk+'�.';
                PrintStr(StrWk);
              end;
              CutDocPr;
            finally
              taStreams.Active:=False;
              Delay(100);
              DevPrint.Close;
            end;
          end;
        end;
        taModif.Active:=False;

        if CommonSet.CashNum<=0 then bCheckOk:=True;
        if bCheckOk then
        begin
          inc(CommonSet.CashChNum); //����� �������� 1-��
          WriteCheckNum;

          prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;

          Case Operation of
        0:begin
            if iCashType=0 then
            begin
              FormLog('Sale',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
            end else
            begin
              FormLog('SaleBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='SaleBank';
            end;
          end;
        1:begin
            if iCashType=0 then
            begin
              FormLog('Ret',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='Ret';
            end else
            begin
              FormLog('RetBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='RetBank';
             end;
            end;
          end;

          if CommonSet.CashNum<=0 then
          begin
            Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
            prSaveToAll.ParamByName('CHECKNUM').AsInteger:=CommonSet.CashChNum;
          end
          else prSaveToAll.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;

          prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
          prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
          prSaveToAll.ExecProc;

          IdH:=prSaveToAll.ParamByName('ID_H').AsInteger;

         // ������ ����
          quDelTab.Active:=False;
          quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

          trDel.StartTransaction;
          quDelTab.Active:=True;
          trDel.Commit;

        //���� ���
          taCashSail.Active:=True;
//        trUpdCS.StartTransaction;

          idC:=GetId('CS');
          taCashSail.Append;
          taCashSailID.AsInteger:=IdC;
          taCashSailCASHNUM.AsInteger:=CommonSet.CashNum;
          if CommonSet.CashNum<0 then
          begin
            taCashSailZNUM.AsInteger:=CommonSet.CashZ;
            taCashSailCHECKNUM.AsInteger:=CommonSet.CashChNum;
          end
          else
          begin
            taCashSailZNUM.AsInteger:=Nums.ZNum;
            taCashSailCHECKNUM.AsInteger:=Nums.iCheckNum;
          end;

          taCashSailTAB_ID.AsInteger:=IdH;

          Case Operation of
          0: taCashSailTABSUM.AsFloat:=TabCh.Summa;
          1: taCashSailTABSUM.AsFloat:=(-1)*TabCh.Summa;
          end;

          taCashSailCLIENTSUM.AsFloat:=fmCash.cEdit2.EditValue;
          taCashSailCHDATE.AsDateTime:=now;
          taCashSailCASHERID.AsInteger:=Person.Id;
          taCashSailWAITERID.AsInteger:=TabCh.Id_Personal;
          taCashSailPAYTYPE.AsInteger:=iCashType;

          taCashSail.Post;

//        trUpdCS.Commit;
          taCashSail.Active:=False;
        end;
      finally
        bPrintCheck:=False; //��� �� �������� �������
        PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
      end;
    end;
    if bExitPers then
    begin
      bExitPers:=False;
      Operation:=0;
      Label5.Caption:='�������� - ������� ��������.';
      PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
      DestrViewPers;
      fmPre.ShowModal;
    end
    else
    begin
      fmMainCashRn.CreateViewPers(True);
      Operation:=0;
      Label5.Caption:='�������� - ������� ��������.';
      PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
    end;
  end;
end;

procedure TfmMainCashRn.cxButton2Click(Sender: TObject);
Var StrWk:String;
    iRet:Integer;
begin
  if OpenZ then
  begin
    if GetX=False then Showmessage('������ ��� ��������� X-������.');
  end
  else  //�������� �����
  begin
    iRet:=InspectSt(StrWk);
    if iRet=0 then
    begin
      if ZOpen=False then Showmessage('������ ��� �������� �����.')
      else
      begin
        if GetX=False then Showmessage('������ ��� ��������� X-������.');
      end;
    end
    else
    begin
      Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
      Showmessage('��������� ������ ��� - '+''''+StrWk+''''+'  ������ ('+IntToStr(iRet)+')');
    end;
  end;
end;

procedure TfmMainCashRn.cxButton3Click(Sender: TObject);
Var iRet:INteger;
begin
  //�������� �����
  if CommonSet.CashNum>0 then
  begin
    if OpenZ then //��� �������� ����� � �������� ������� �� �����
    begin
      //�������� ��� �������
      ZClose;
      iRet:=InspectSt(StrWk);
      if iRet=0 then
      begin
        Label3.Caption:='������ ���: ��� ��.';
        if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
        inc(CommonSet.CashZ);

        GetSerial;
        CashDate(Nums.sDate);
        GetNums;
        GetRes; //�������

        WriteStatus;
        if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
        WriteCheckNum;
      end
      else
      begin
        Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
      end;
    end
    else
    begin
      Showmessage('����� ��� �������.');
    end;
  end;
  If CommonSet.CashNum<0 then
  begin
    inc(CommonSet.CashZ);
    CommonSet.CashChNum:=0;
    CommonSet.PreCheckNum:=1;
    if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
    WriteCheckNum;
    Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
  end;
end;

procedure TfmMainCashRn.cxButton4Click(Sender: TObject);
begin
 CheckCancel;
end;

procedure TfmMainCashRn.acCashRepExecute(Sender: TObject);
begin
  if Panel4.Visible then
  Panel4.Visible:=False
  else
  begin
    Panel4.Visible:=True;
    cxButton2.SetFocus;
  end;
end;

procedure TfmMainCashRn.cxButton1Click(Sender: TObject);
begin
  acCashRep.Execute;
end;

procedure TfmMainCashRn.acExitExecute(Sender: TObject);
begin
  if bPrintCheck then
  begin
    bExitPers:=True;
    prWriteLog('������� ������ ��� ������ ����.');
    exit;
  end;
  bPrintCheck:=False;
  DestrViewPers;
  fmPre.ShowModal;
end;

procedure TfmMainCashRn.cxButton5Click(Sender: TObject);
begin
  if not CanDo('prRetCheck') then exit;

  if Operation =0 then Operation:=1
  else Operation := 0;

  Case Operation of
  0: begin
       Label5.Caption:='�������� - ������� ��������.';
     end;
  1: begin
       Label5.Caption:='�������� - �������.';
     end;
  end;
end;

procedure TfmMainCashRn.RxClock1Click(Sender: TObject);
begin
  if CanDo('prExit') then
    if MessageDlg('�� ������������� ������ �������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FormLog('Close',IntToStr(Tab.Id_Personal));
      Close;
    end;
end;

procedure TfmMainCashRn.acOpenExecute(Sender: TObject);
Var StrPers, StrStatus:String;
    DSum,rSum:Real;
begin
  //���������� ������������ �����
  if bOpenTable then exit;
  with dmC do
  begin
    bOpenTable:=True; // ��������� �����

    if not CanDo('prView') then begin bOpenTable:=False; exit; end;

    if quTabs.Eof then  begin bOpenTable:=False; exit; end;

    Tab.Id_Personal:=quTabsID_PERSONAL.AsInteger;

    quFindPers.Active:=False;
    quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
    quFindPers.Active:=True;
    Tab.Name:=quFindPersNAME.AsString;
    quFindPers.Active:=False;

    Tab.OpenTime:=quTabsBEGTIME.AsDateTime;
    Tab.NumTable:=quTabsNUMTABLE.AsString;
    Tab.Quests:=quTabsQUESTS.AsInteger;
    Tab.iStatus:=quTabsISTATUS.AsInteger;
    Tab.DBar:=quTabsDISCONT.AsString;
    Tab.DBar1:=quTabsDISCONT.AsString;
    Tab.Id:=quTabsID.AsInteger;
    Tab.Summa:=quTabsTABSUM.AsFloat;

    //�������� ���������� ������

    //������� �� ������ ������
    prWriteLog('--OpenTab;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');


    prOpenTab.ParamByName('ID_TAB').AsInteger:=Tab.Id;
    prOpenTab.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
    prOpenTab.ExecProc;
    DSUM:=prOpenTab.ParamByName('DSUMALL').AsDouble;
    Check.Max:=prOpenTab.ParamByName('IDMAX').AsInteger;


    prWriteLog('--SelSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');

    rSum:=0;
    quCurSpec.Active:=False;
    quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
    quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurSpec.Active:=True;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSUMMA.AsFloat;
      prWriteLog('-----pos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');
      quCurSpec.Next;
    end;
    prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');

    quCurMod.Active:=False;
    quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
    quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
    quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurMod.Active:=True;

    quCurSpec.First;
    quCurMod.First;

    //����� �������� �������� �� ������������ �������� ����� �� �����
    if RoundEx(rSum*100)<>RoundEx(Tab.Summa*100) then
    begin
      //����� �� ��������
      prWriteLog('--SumSpec; New-'+IntToStr(RoundEx(rSum*100))+';Old-'+IntToStr(RoundEx(Tab.Summa*100))+';');
      if MessageDlg('���������� ����������� ���� ��������� � ������������ ������. �� ��������� �����?',
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
//        showmessage('������ �������� ������.');
        bOpenTable:=False; // ���� ������� �����
        exit;
      end;
    end;

    with fmSpec do
    begin
      StrPers:=Tab.Name;
      StrStatus:='';

      SetStatus(StrStatus);

      Label7.Caption:=StrPers;
      BEdit1.Text:=Tab.NumTable;
      if Tab.Quests>0 then SEdit1.EditValue:=Tab.Quests else SEdit1.EditValue:=1;
      Label10.Caption:=FormatdateTime('dd.mm.yyyy hh:nn',Tab.OpenTime);
      Label11.Caption:=StrStatus;
      Label17.Caption:='';

      If FindDiscount(Tab.DBar) then
      begin
        // ��������� ������
        Str(Tab.DPercent:5:2,StrWk);
        Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
      end;

      if DSum<>0 then
      begin
        ViewSpecDPROC.Visible:=True;
        ViewSpecDSum.Visible:=True;
      end
      else
      begin
        ViewSpecDPROC.Visible:=False;
        ViewSpecDSum.Visible:=False;
      end;

    end;

    bOpenTable:=False; // ���� ������� �����

    fmSpec.ShowModal;
  end;
  delay(100);
  CreateViewPers(bChangeView); //�������� ��������� ���-���� ����� ��������� ������ �������� �������
end;

procedure TfmMainCashRn.TabViewDblClick(Sender: TObject);
begin
//Button2.SetFocus;
//Button2.OnClick(Self);
end;

procedure TfmMainCashRn.acPrintPreExecute(Sender: TObject);
  //�������� ����
Var strWk,StrWk1:String;
    rDiscont,rDProc:Real;
    TabCh:TTab;
    iQ:Integer;
begin
  with dmC do
  begin
    if quTabs.Eof then exit;
    if (quTabsISTATUS.AsInteger=1) and (CommonSet.PreCheckCount=1) then
    begin
      showmessage('��������� ������ ����� ���������.');
      exit;
    end;

    inc(CommonSet.PreCheckNum);
    WriteCheckNum;

    TabCh.Id_Personal:=quTabsID_PERSONAL.AsInteger;
    taPersonal.Active:=true;
    taPersonal.Locate('Id',TabCh.Id_Personal,[]);
    TabCh.Name:=taPersonalNAME.AsString;
    taPersonal.Active:=False;

    TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
    TabCh.NumTable:=quTabsNUMTABLE.AsString;
    TabCh.Quests:=quTabsQUESTS.AsInteger;
    TabCh.iStatus:=quTabsISTATUS.AsInteger;
    TabCh.DBar:=quTabsDISCONT.AsString;
    TabCh.Id:=quTabsID.AsInteger;
    TabCh.Summa:=quTabsTABSUM.AsFloat;

    FindDiscount(TabCh.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    taModif.Active:=True;

    bPrintCheck:=False;
    bPrintCheck:=True;

    if (CommonSet.PrePrintPort<>'0')and(CommonSet.PrePrintPort<>'G')and(Pos('DB',CommonSet.PrePrintPort)=0)and(Pos('fis',CommonSet.PrePrintPort)=0) then
    begin //���������� ���� COM1 ��������
      try
        prOpenDevPrint(CommonSet.PrePrintPort);

        SelFont(13); PrintStr(' '+CommonSet.DepartName); PrintStr(' ');
        SelFont(14);PrintStr('       ����');
        SelFont(13);PrintStr(' ');
        PrintStr('��������: '+TabCh.Name);
        PrintStr('����: '+TabCh.NumTable);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintStr('�����: '+Tab.DName+' ('+StrWk+'%)');
        end;
        SelFont(13);
        PrintStr('������: '+IntToStr(TabCh.Quests));
        PrintStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

        SelFont(13); StrWk:='                                          '; PrintStr(StrWk);

        SelFont(15); StrWk:=' ��������      ���-��   ����   �����'; PrintStr(StrWk);
        SelFont(13); StrWk:='                                          '; PrintStr(StrWk);

        rDiscont:=0;

        quCheck.First;
        while not quCheck.Eof do
        begin      //��������� �������

          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          StrWk:= Copy(PosCh.Name,1,19);
          while Length(StrWk)<19 do StrWk:=StrWk+' ';
          Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckPRICE.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
          StrWk:=StrWk+StrWk1+'�';
          SelFont(15);
          PrintStr(StrWk);

          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
              begin
                PrintStr('   '+Copy(taModifNAME.AsString,1,29));
              end;
              quCheck.Next;
            end;
          end;
        end;

        SelFont(13);
        StrWk:='                                          ';
        PrintStr(StrWk);

        SelFont(15);
        Str(TabCh.Summa:8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        PrintStr(StrWk);

        if rDiscont>0.02 then
        begin
          SelFont(15);
          PrintStr('');
          SelFont(10);
          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
          Str(rDProc:5:2,StrWk1);
          Str(rDiscont:8:2,StrWk);
//          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          StrWk:=' � �.�. ������ - '+StrWk+'�.';
          PrintStr(StrWk);
        end;
        CutDocPr;
      finally
        Delay(100);
        DevPrint.Close;
      end;
    end;
    if CommonSet.PrePrintPort='G' then
    begin

      quCheck.Filtered:=False;
      quCheck.Filter:='ITYPE=0';
      quCheck.Filtered:=True;

      rDiscont:=0;

      quCheck.First;
      while not quCheck.Eof do
      begin      //��������� �������
        rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
        quCheck.Next;
      end;
      quCheck.First;


      frRepMain.LoadFromFile(CurDir + 'PreCheck1.frf');

      frVariables.Variable['Waiter']:=TabCh.Name;
      frVariables.Variable['TabNum']:=TabCh.NumTable;
      frVariables.Variable['OpenTime']:=FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime);
      frVariables.Variable['Quests']:=IntToStr(TabCh.Quests);
      frVariables.Variable['ZNum']:=IntToStr(TabCh.Id);
      frVariables.Variable['PreNum']:=IntToStr(CommonSet.PreCheckNum-1);

      if rDiscont>0.2 then
      begin
        str(rDiscont:8:2,StrWk);
        frVariables.Variable['Discount']:='� �.�. ������ '+StrWk;
      end else frVariables.Variable['Discount']:='';
      frRepMain.ReportName:='����.';
      frRepMain.PrepareReport;
//      frRepMain.ShowPreparedReport;
      frRepMain.PrintPreparedReportDlg;
    end;
    if pos('DB',CommonSet.PrePrintPort)>0 then
    begin
        //��������� �������
      with dmC1 do
      begin
        quPrint.Active:=False;
        quPrint.Active:=True;
        iQ:=GetId('PQH0'); //��� ���������� ���������� - ����������� � ����� ������

        PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'       ����',14,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
        end;
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

        rDiscont:=0;

        quCheck.First;
        while not quCheck.Eof do
        begin      //��������� �������

          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          if pos('DBfis2',CommonSet.PrePrintPort)>0 then
          begin  //�� ���������� �����   - ��� �� 2-� ������ �� 36 �������� - ���� �������� �������
            StrWk:= Copy(PosCh.Name,1,36);
            while Length(StrWk)<36 do StrWk:=StrWk+' ';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

            Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
            StrWk:=' '+StrWk1;
            Str(quCheckPRICE.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1+'�';
            while Length(StrWk)<36 do StrWk:=' '+StrWk;
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

          end else //��� �� ����������� ������ 40 ��������
          begin
            StrWk:= Copy(PosCh.Name,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';
            Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCheckPRICE.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
            StrWk:=StrWk+StrWk1+'�';

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          end;
          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
              begin
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
              end;
              quCheck.Next;
            end;
          end;
        end;

        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

        Str(TabCh.Summa:8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

        if rDiscont>0.02 then
        begin
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
          Str(rDProc:5:2,StrWk1);
          Str(rDiscont:8:2,StrWk);
          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,10,0);
        end;

        GetId('PQH'); //����������� ������� �� 1-�
        quPrint.Active:=False;
      end;  
    end;
    if CommonSet.PrePrintPort='fisshtrih' then
    begin
      PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');

      PrintNFStr('             ����');
      PrintNFStr(' ');
      PrintNFStr('��������: '+Tab.Name);
      PrintNFStr('����: '+Tab.NumTable);
      if Tab.DBar>'' then
      begin
        Str(Tab.DPercent:5:2,StrWk);
        PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
      end;
      PrintNFStr('������: '+IntToStr(Tab.Quests));
      PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

      StrWk:='-----------------------------------';
      PrintNFStr(StrWk);
      StrWk:=' ��������     ���-��  ����   ����� ';
      PrintNFStr(StrWk);
      StrWk:='-----------------------------------';
      PrintNFStr(StrWk);

      rDiscont:=0;

      quCheck.First;
      while not quCheck.Eof do
      begin      //��������� �������

        PosCh.Name:=quCheckNAME.AsString;
        PosCh.Code:=quCheckCODE.AsString;
        PosCh.AddName:='';

        StrWk:= Copy(PosCh.Name,1,36);
        while Length(StrWk)<36 do StrWk:=StrWk+' ';
        PrintNFStr(StrWk); //������� ����� �������� - �������� �������

        rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

        Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
        StrWk:='          '+StrWk1;
        Str(quCheckPRICE.AsFloat:7:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1;
        Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1+'���';
        PrintNFStr(StrWk);

        quCheck.Next;
        if not quCheck.Eof then
        begin
          while quCheckITYPE.AsInteger=1 do
          begin  //������������
            if quCheck.Eof then break;
            if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
            begin
              StrWk:='   '+Copy(taModifNAME.AsString,1,29);
              PrintNFStr(StrWk);
            end;
            quCheck.Next;
          end;
        end;
      end;

      StrWk:='-----------------------------------';
      PrintNFStr(StrWk);
      Str(TabCh.Summa:8:2,StrWk1);
      StrWk:=' �����                '+StrWk1+' ���';
      PrintNFStr(StrWk);

      if rDiscont>0.02 then
      begin
        PrintNFStr('');
        rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
        Str(rDProc:5:2,StrWk1);
        Str(rDiscont:8:2,StrWk);
        StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
        PrintNFStr(StrWk);
      end;

      PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');
      CutDoc;
    end;
    if CommonSet.PrePrintPort='fisprim' then
    begin
      OpenNFDoc;
      SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
      PrintNFStr('');
      SelectF(14); PrintNFStr('        ����');
      SelectF(13); PrintNFStr(' '); PrintNFStr('��������: '+Tab.Name);
      SelectF(3); PrintNFStr('����: '+Tab.NumTable);
      if Tab.DBar>'' then
      begin
        Str(Tab.DPercent:5:2,StrWk);
        PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
      end;
      PrintNFStr('������: '+IntToStr(Tab.Quests));
      PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

      SelectF(3); StrWk:='                                                       ';
      PrintNFStr(StrWk);
      SelectF(15); StrWk:=' ��������     ���-��  ����   ����� ';
      PrintNFStr(StrWk);
      SelectF(3); StrWk:='                                                       ';
      PrintNFStr(StrWk);

      rDiscont:=0;

      quCheck.First;
      while not quCheck.Eof do
      begin      //��������� �������

        PosCh.Name:=quCheckNAME.AsString;
        PosCh.Code:=quCheckCODE.AsString;
        PosCh.AddName:='';

        StrWk:= Copy(PosCh.Name,1,29);
        while Length(StrWk)<29 do StrWk:=StrWk+' ';
        Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
        StrWk:=StrWk+' '+StrWk1;
        Str(quCheckPRICE.AsFloat:7:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1;
        Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1+'���';
        SelectF(3);
        PrintNFStr(StrWk);

        rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

        quCheck.Next;
        if not quCheck.Eof then
        begin
          while quCheckITYPE.AsInteger=1 do
          begin  //������������
            if quCheck.Eof then break;
            if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
            begin
              StrWk:='   '+Copy(taModifNAME.AsString,1,29);
              SelectF(0); PrintNFStr(StrWk);
            end;
            quCheck.Next;
          end;
        end;
      end;

      SelectF(3); StrWk:='                                                       ';
      PrintNFStr(StrWk);
      SelectF(10); PrintNFStr(' ');
      Str(TabCh.Summa:8:2,StrWk1);
      StrWk:=' �����                '+StrWk1+' ���';
      SelectF(15); PrintNFStr(StrWk);

      if rDiscont>0.02 then
      begin
        SelectF(10); PrintNFStr(' ');
        rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
        Str(rDProc:5:2,StrWk1);
        Str(rDiscont:8:2,StrWk);
        StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
        PrintNFStr(StrWk);
      end;

      PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
      CloseNFDoc;
      CutDoc;
    end;


   //  ��������� ������ ����� �����

    TabView.BeginUpdate;
 //   trUpdate.StartTransaction;
    quTabs.Edit;
    quTabsISTATUS.AsInteger:=1;
    quTabs.Post;
//    trUpdate.Commit;
    TabView.EndUpdate;
    quTabs.Locate('ID',Tab.Id,[]);
//      fmMainCashRn.CreateViewPers;
    quTabs.Refresh;



    taModif.Active:=False;

    quCheck.Filtered:=False;
    quCheck.Active:=False;

    bPrintCheck:=False;
    if bExitPers then
    begin
      bExitPers:=False;
      DestrViewPers;
      fmPre.ShowModal;
      exit;
    end;

  end;
end;


procedure TfmMainCashRn.acCashPCardExecute(Sender: TObject);
//Var StrWk:String;
//Var rSum:Real;
begin
//  fmDiscount_Shape.ShowModal;
  if not CanDo('prPrintBNCheck') then exit;
  with dmC do
  begin
    if quTabs.Eof then exit;

    fmDiscount_Shape.cxTextEdit1.Text:='';
    fmDiscount_Shape.Caption:='��������� ��������� �����';
    fmDiscount_Shape.ShowModal;
    if fmDiscount_Shape.ModalResult=mrOk then
    begin

        //������� ��������� ������ - ��������� ����� - �.�. �� ������
//      showmessage('');

      if fmDiscount_Shape.cxTextEdit1.Text>'' then
      begin
        DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
        If FindPCard(DiscountBar) then
        begin
          bPrintCheck:=True; //������� ������ ����

{          taPersonal.Active:=False;
          taPersonal.Active:=true;
          taPersonal.Locate('Id',quTabsID_PERSONAL.AsInteger,[]);
          Tab.Name:=taPersonalNAME.AsString;
          taPersonal.Active:=False;
}
{          TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
          TabCh.NumTable:=quTabsNUMTABLE.AsString;
          TabCh.Quests:=quTabsQUESTS.AsInteger;
          TabCh.iStatus:=quTabsISTATUS.AsInteger;
          TabCh.DBar:=quTabsDISCONT.AsString;
          TabCh.Id:=quTabsID.AsInteger;
          TabCh.Summa:=quTabsTABSUM.AsFloat;
}
          FormLog('SalePC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);

          prSaveToAllPC.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
          prSaveToAllPC.ParamByName('OPERTYPE').AsString:='SalePC';
          prSaveToAllPC.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;
          prSaveToAllPC.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
          prSaveToAllPC.ParamByName('SDISCOUNT').AsString:=DiscountBar; //��� �����
          prSaveToAllPC.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
          prSaveToAllPC.ExecProc;

         // ������ ����
          quDelTab.Active:=False;
          quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

          trDel.StartTransaction;
          quDelTab.Active:=True;
          trDel.Commit;

          //��c��� ��� � cashsail �� ���� �.�. ��� ������ ����
          CreateViewPers(True);


          bPrintCheck:=False; //������� ������ ����
        end else
        begin
          //��������� ������� ��������� ������
          FormLog('BadPC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
        end;
      end;
    end;
  end;
end;

procedure TfmMainCashRn.cxButton6Click(Sender: TObject);
begin
  //��� ������
with dmC do
  begin
    Label1.Caption:='��� �������� ������.';

    PersView.BeginUpdate;
    quPers.Active:=False;
    dsPers.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end
    else
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end;

    quPers.Active:=True;
    dsPers.DataSet:=quPers;
    PersView.EndUpdate;


    TabView.BeginUpdate;
    quTabs.Active:=False;
    dsTabs.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMTABLE');
    end
    else
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMTABLE');
    end;

//t.BEGTIME>'27.01.2008 01:00'

    quTabs.Active:=True;
    dsTabs.DataSet:=quTabs;
    TabView.EndUpdate;

    PersView.Focused:=True;
    quPers.Last;
//    delay(10);

    while not quPers.Bof do
    begin
      PersView.Controller.FocusedRow.Expand(True);
      quPers.Prior;
//      delay(10);
    end;
  end;
end;

end.
