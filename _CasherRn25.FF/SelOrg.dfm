object fmSelOrg: TfmSelOrg
  Left = 577
  Top = 310
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1102
  ClientHeight = 204
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 149
    Width = 404
    Height = 55
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 0
    object cxButton17: TcxButton
      Left = 152
      Top = 8
      Width = 97
      Height = 41
      Caption = #1054#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      TabStop = False
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
  end
  object GrSelOrg: TcxGrid
    Left = 8
    Top = 8
    Width = 385
    Height = 136
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViSelOrg: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsquOrgsSt
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViSelOrgCASHNUM: TcxGridDBColumn
        Caption = #8470' '#1050#1072#1089#1089#1099
        DataBinding.FieldName = 'CASHNUM'
      end
      object ViSelOrgIORG: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'IORG'
      end
      object ViSelOrgDEPARTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'DEPARTNAME'
        Width = 177
      end
      object ViSelOrgFIS: TcxGridDBColumn
        Caption = #1060#1080#1089#1082#1072#1083#1100#1085#1099#1081' '#1088#1077#1078#1080#1084
        DataBinding.FieldName = 'FIS'
      end
    end
    object LeSelOrg: TcxGridLevel
      GridView = ViSelOrg
    end
  end
end
