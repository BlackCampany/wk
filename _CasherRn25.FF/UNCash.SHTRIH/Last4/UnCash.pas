unit UnCash;

interface
uses
//  ������� ������

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ComObj, ActiveX, OleServer, StdCtrls;


Function InspectSt(Var sMess:String):Integer;
Function GetSt(Var sMess:String):INteger;
Function OpenZ:Boolean;  //��������
Function GetSerial:Boolean;
Function GetNums:Boolean;
Function GetRes:Boolean;
Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;

Function CashOpen(sPort:PChar):Boolean;
Function CashClose:Boolean;
Function CashDate(var sDate:String):Boolean;
Function GetX:Boolean;
Function ZOpen:Boolean; //������ �����
Function ZClose:Boolean;
Function CheckStart:Boolean;
Function CheckRetStart:Boolean;
Function CheckAddPos(Var iSum:Integer):Boolean;
Function CheckTotal(Var iTotal:Integer):Boolean;
Function CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Function CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Function CheckClose:Boolean;
Procedure CheckCancel;
Procedure InCass(rSum:Real);//����������
Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����

Function CashDriver:Boolean;
Procedure TestPosCh;

Function OpenNFDoc:Boolean;
Function CloseNFDoc:Boolean;
Function PrintNFStr(StrPr:String):Boolean;
Function SelectF(iNum:Integer):Boolean;
Function CutDoc:Boolean;
Function CutDoc1:Boolean;

procedure WriteHistoryFR(Strwk_: string);
Function TestStatus(StrOp:String; Var SMess:String):Boolean;

function IsOLEObjectInstalled(Name: String): boolean;
function More24H(Var iSt:INteger):Boolean;
Function CheckOpen:Boolean;
Function SetDP(Str1,Str2:String):Boolean;


Type
     TStatusFR = record
     St1:String[2];
     St2:String[4];
     St3:String[4];
     St4:String[10];
     end;

     TNums = record
     bOpen:Boolean;
     ZNum:Integer;
     SerNum:String[11];
     RegNum:string[10];
     CheckNum:string[4];
     iCheckNum:Integer;
     iRet:INteger;
     sRet:String;
     sDate:String;
     ZYet:Integer;
     sDateTimeBeg:String;
     end;

     TCurPos = record
     Articul:String;
     Bar:String;
     Name:String;
     EdIzm:String;
     Quant:Real;
     Price:Currency;
     DProc:Real;
     DSum:Currency;
     Depart:SmallInt;
     end;

     TPos = record
     Name:String;
     Code:String;
     AddName:String;
     Price:Integer;
     Count:Integer;
     Stream: Integer;
     Sum: Real;
     end;

var
  CommandsArray : array [1..32000] of Char;
  HeapStatus    : THeapStatus;
  PressCount    : Byte;
  StatusFr      : TStatusFr;
  Nums          : TNums;
//  CurPos        :TCurPos;
//  SelPos        :TCurPos;
  sMessage      :String;
  PosCh         : TPos;
  Drv: OleVariant;

Const iPassw:Integer = 30;

implementation

uses Un1;

Function SetDP(Str1,Str2:String):Boolean;
begin
  Result:=True;

  { �� 101frf
  if (Commonset.CashNum<=0) then begin Result:=True; exit; end;
  iRfr:=Drv.EEJShowClientDisplayText(Str1,Str2);
  if iRfr=0 then Result:=True
  else Result:=False;}
end;


Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����
Var iSt:Integer;
begin
  result:=False;
  rSum:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Drv.Password:=iPassw;
  Drv.RegisterNumber:=241;
  iSt:=Drv.GetCashReg;
  if iSt=0 then
  begin
    rSum:=Drv.ContentsOfCashRegister;
    Result:=True;
  end;
end;

Procedure InCass(rSum:Real);
begin
  if Commonset.CashNum<=0 then begin exit; end;

  Drv.Password:=iPassw;

  if Drv.ResultCode=88 then
  begin
    Drv.ContinuePrint;
  end;

  Drv.Summ1 := rSum;
  Drv.CashOutCome;

end;



Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
begin
  result:=True;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  rSum1:=0;
  rSum2:=0;
  rSum3:=0;
end;


function More24H(Var iSt:INteger):Boolean;
begin
  result:=False;
  if Commonset.CashNum<=0 then begin Result:=False; exit; end;
  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  iSt:=Drv.ECRMode;
  if Drv.ECRMode=3 then Result:=True;
end;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


Function GetSt(Var sMess:String):Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=0; exit; end;

  result:=0;
  sMess:='';
end;



Function TestStatus(StrOp:String; Var SMess:String):Boolean;
Var bWr:Boolean;
    iR:Integer;

begin
  Result:=True;
  sMess:='';
  bWr:=False;

  if Commonset.CashNum<=0 then exit;

  iR:=Drv.ResultCode;
  if iR<>0 then
  begin
    if iR=88 then
    begin
      Drv.ContinuePrint;
    end;
    //�� ��������� ���������� 0 ��� ���������� ������� ��� ������ ������������
    iR:=0;

    if StrOp='CheckAddPos' then iR:=Drv.Sale; //�������� ��������� �������� - ������ ��� ������� - ������ ������ ��������������
    if StrOp='CheckTotal' then iR:=Drv.CheckSubTotal; //�������� ��������� �������� - ������ ��� ������� - ������ ������ ��������������
    if StrOp='CheckDiscount' then iR:=Drv.Discount; //�������� ��������� �������� - ������ ��� ������� - ������ ������ ��������������
    if StrOp='CheckRasch' then iR:=Drv.CloseCheck; //�������� ��������� �������� - ������ ��� ������� - ������ ������ ��������������

  end;

  if iR<>0 then
  begin
    bWr:=True;
    SMess:=Drv.ResultCodeDescription;
    Result:=False;
  end else Result:=True;

  if bWr then WriteHistoryFr(StrOp+';'+INtToStr(iR)+';'+sMess);
end;

procedure WriteHistoryFR(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='fr'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Function CutDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
  Drv.Password:=iPassw;
  Drv.CutType:=True;
  Drv.CutCheck;
end;

Function CutDoc1:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
  Drv.Password:=iPassw;
  Drv.CutType:=True;
  Drv.CutCheck;
end;

Function SelectF(iNum:Integer):Boolean;
begin
  Result:=True;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
end;


Function PrintNFStr(StrPr:String):Boolean;
Var iR:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Drv.Password:=iPassw;
  Drv.StringForPrinting:=Copy(StrPr,1,36);

  if iR=88 then
  begin
    Drv.ContinuePrint;
  end;

  iR:=Drv.PrintString;

  while iR<>0 do
  begin
    if iR=88 then Drv.ContinuePrint;

    delay(3000);
    iR:=Drv.PrintString;
  end;

  iR:=Drv.ResultCode;
  if iR=0 then Result:=True
  else Result:=False;
end;


Function OpenNFDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;

Function CloseNFDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;


Function CashDriver:Boolean;
Var n:ShortINt;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
//  exit;
{  for n:=0 to 5 do
  begin
    Drv.Password:=iPassw;
    Drv.DrawerNumber:=n;
    Drv.OpenDrawer;
  end;}
  n:=0;
  Drv.Password:=iPassw;
  Drv.DrawerNumber:=n;
  Drv.OpenDrawer;

end;


Function CheckClose:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;

Function CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Var iR:INteger;

begin
  Result:=False;
//  if Comment='' then Comment:='-------------------';
  iDopl:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  if iPayType=0 then //���
  begin
    Drv.Password:=iPassw;
    Drv.Summ1:=iClient/100;
    Drv.Summ2:=0;
    Drv.Summ3:=0;
    Drv.Summ4:=0;
    Drv.DiscountOnCheck:=0;
    Drv.Tax1:=0;
    Drv.Tax2:=0;
    Drv.Tax3:=0;
    Drv.Tax4:=0;
    Drv.StringForPrinting:=Comment;

    if Drv.ResultCode=88 then
    begin
      Drv.ContinuePrint;
    end;

    iR:=Drv.CloseCheck;

{    while iR<>0 do
    begin
      if iR=88 then
      begin
        Drv.ContinuePrint;
      end;
      delay(3000);
      iR:=Drv.CloseCheck;
    end;}

    iDopl:=Trunc(Drv.Change*100);
    iR:=Drv.ResultCode;
    if iR=0 then Result:=True
    else Result:=False;
  end;
  if iPayType=1 then //������
  begin
 //    if Comment='' then Comment:='-------------------------';
    Drv.Password:=iPassw;
    Drv.Summ1:=0;
    Drv.Summ2:=0;
    Drv.Summ3:=0;
    Drv.Summ4:=iClient/100;
    Drv.DiscountOnCheck:=0;
    Drv.Tax1:=0;
    Drv.Tax2:=0;
    Drv.Tax3:=0;
    Drv.Tax4:=0;
    Drv.StringForPrinting:=Comment;

    if Drv.ResultCode=88 then
    begin
      Drv.ContinuePrint;
    end;

    iR:=Drv.CloseCheck;

{    while iR<>0 do
    begin
      if iR=88 then
      begin
        Drv.ContinuePrint;
      end;
      delay(3000);
      iR:=Drv.CloseCheck;
    end;}

    iDopl:=Trunc(Drv.Change*100);
    iR:=Drv.ResultCode;
    if iR=0 then Result:=True
    else Result:=False;

  end;
end;

Function CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Var iDiscount:Integer;
    iR:Integer;
    StrWk:String;
begin
  iDiscount:=RoundEx(rDiscount*100);
  iDisc:=0;
  iItog:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  if iDiscount>=0 then
  begin //������
    Drv.Summ1:=iDiscount/100;
    Drv.Tax1:=0;
    Drv.Tax2:=0;
    Drv.Tax3:=0;
    Drv.Tax4:=0;
    Drv.Password:=iPassw;
    StrWk:='----------------';
    if length(StrWk)>40 then StrWk:=copy(StrWk,1,40);
    Drv.StringForPrinting:=StrWk;

    if Drv.ResultCode=88 then
    begin
      Drv.ContinuePrint;
    end;

    iR:=Drv.Discount;
    while iR<>0 do
    begin
      if iR=88 then
      begin
        Drv.ContinuePrint;
      end;
      delay(3000);
      iR:=Drv.Discount;
    end;
    iDisc:=iDiscount;
    iItog:=0;
  end else
  begin
//    Drv.Summ1:=iDiscount/100*(-1);
    Drv.Summ1:=iDiscount/100;
    Drv.Tax1:=0;
    Drv.Tax2:=0;
    Drv.Tax3:=0;
    Drv.Tax4:=0;
    Drv.Password:=iPassw;
    StrWk:='----------------';
    if length(StrWk)>40 then StrWk:=copy(StrWk,1,40);
    Drv.StringForPrinting:=StrWk;

    if Drv.ResultCode=88 then
    begin
      Drv.ContinuePrint;
    end;

    iR:=Drv.Charge;
    while iR<>0 do
    begin
      if iR=88 then
      begin
        Drv.ContinuePrint;
      end;
      delay(3000);
      iR:=Drv.Charge;
    end;
    iDisc:=iDiscount;
    iItog:=0;
  end;
  iR:=Drv.ResultCode;
  if iR=0 then Result:=True
  else Result:=False;
end;

Function CheckTotal(Var iTotal:Integer):Boolean;
Var iR:Integer;
begin
  iTotal:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Drv.Summ1:=0;
  Drv.Password:=iPassw;

  if Drv.ResultCode=88 then
  begin
    Drv.ContinuePrint;
  end;

  iR:=Drv.CheckSubTotal;

{  while iR<>0 do
  begin
    if iR=88 then
    begin
      Drv.ContinuePrint;
    end;
    delay(3000);
    iR:=Drv.CheckSubTotal;
  end;}

  iTotal:=Trunc(Drv.Summ1*100);
  iR:=Drv.ResultCode;
  if iR=0 then Result:=True
  else Result:=False;
  PrintNFStr('--------------------');
  PrintNFStr(Tab.Name);
end;


Procedure CheckCancel;
begin
  if Commonset.CashNum<=0 then begin exit; end;

  Drv.Password:=iPassw;

  if Drv.ResultCode=88 then
  begin
    Drv.ContinuePrint;
  end;

  Drv.CancelCheck;
end;

Procedure TestPosCh;
begin
  if Length(PosCh.Name)>36 then PosCh.Name:=Copy(PosCh.Name,1,36);
  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);
end;


Function CheckAddPos(Var iSum:Integer):Boolean;
Var iR:INteger;
begin
  iSum:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;

  TestPosCh;

  if Drv.ResultCode=88 then
  begin
    Drv.ContinuePrint;
  end;

  Drv.Password:=iPassw;
  Drv.Quantity:=PosCh.Count/1000;
  Drv.Price:=PosCh.Price/100;
  Drv.Summ1:=0;
  Drv.Department:=1;
  Drv.Tax1:=0;
  Drv.Tax2:=0;
  Drv.Tax3:=0;
  Drv.Tax4:=0;
  Drv.StringForPrinting:=PosCh.Name;

  Case Operation of
  0: iR:=Drv.Sale;
  1: iR:=Drv.ReturnSale;
  end;
{  while iR<>0 do
  begin
    if iR=88 then
    begin
      Drv.ContinuePrint;
    end;
    delay(3000);
    iR:=Drv.Sale;
  end;
}
  iSum:=RoundEx(PosCh.Count/1000*PosCh.Price);
  iR:=Drv.ResultCode;
  if iR=0 then Result:=True
  else Result:=False;
end;


Function CheckStart:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;

Function CheckRetStart:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;


Function ZClose:Boolean;
Var iR:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  Drv.Password:=iPassw;
  iR:=Drv.PrintReportWithCleaning;
  if iR=0 then
  begin
    Result:=True;
    CutDoc;
  end;
end;


Function ZOpen:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
end;

Function GetX:Boolean;
Var iR:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  Drv.Password:=iPassw;
  iR:=Drv.PrintReportWithoutCleaning;
  if iR=0 then
  begin
    Result:=True;
  end;
end;

Function CashDate(var sDate:String):Boolean;
Var iRet:Integer;
begin
//
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  sDate:='';
  Drv.Password:=iPassw;
  iRet:=Drv.GetECRStatus;
  if iRet<>0 then
  begin
    Exit;
  end
  else
  begin
    sDate:=FormatDateTime('dd.mm.yyyy',Drv.Date);
    Result:=true;
  end;
end;


Function CashClose:Boolean;
Var iRet:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  Drv.UnlockPort;
  iRet:=Drv.Disconnect;
  if iRet=0 then result:=True;
end;

Function CashOpen(sPort:PChar):Boolean;
Var iR:Integer;
    StrWk:String;
    iP,iSpeed:INteger;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;

  if not IsOLEObjectInstalled('Addin.DrvFR') then
  begin
    showmessage('������� ����� �� ����������. !!!');
    Result:=False;
    exit;
  end;
  try
   Drv := CreateOleObject('Addin.DrvFR');
  except
    showmessage('���������� ������� ������ "Addin.DrvFR". !!!');
    Result:=False;
    exit;
  end;

  StrWk:=String(sPort);
  if pos('COM',StrWk)>0 then delete(StrWk,pos('COM',StrWk),3);
  iP:=StrToINtDef(StrWk,2);

  for iSpeed:=1 to 6 do
  begin
    Drv.Password:=iPassw;
    Drv.ComNumber:=iP; //
    Drv.BaudRate:=iSpeed; //4800
    Drv.TimeOut:=CommonSet.ComDelay;
    iR:=Drv.Connect;
    if iR=0 then
    begin
      iR:=Drv.LockPort;
      Break;
    end;
  end;
  if iR=0 then result:=True else result:=False;

end;

Function GetNums:Boolean;
begin
  if Commonset.CashNum<=0 then begin inc(Nums.iCheckNum); inc(CommonSet.CashChNum);  Result:=True; exit; end;

  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  Nums.iCheckNum:=Drv.OpenDocumentNumber;
  Nums.CheckNum:=IntToStr(Nums.iCheckNum);
  CommonSet.CashChNum:=Nums.iCheckNum;
  Nums.ZNum:=Drv.SessionNumber+1;
  Nums.ZYet:=Drv.FreeRecordInFM;

  Result:=True;
end;

Function GetRes:Boolean;
begin
  Result:=True;
end;

Function GetSerial:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;

  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  Nums.SerNum:=Copy(Drv.SerialNumber,1,11);

  result:=True;
end;


Function OpenZ:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  result:=True;
end;



Function InspectSt(Var sMess:String):Integer;
Var iSt:INteger;
begin
  sMess:='��� ��.';
  if Commonset.CashNum<=0 then begin Result:=0; exit; end;
  result:=0;
//  exit;
  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  iSt:=Drv.ECRMode;
  if iSt=3 then
  begin
    sMess:='������� ������: '+IntToStr(iSt)+' ���������� ������� �����.';
    Result:=22;
    exit;
  end;
{  if iSt=8 then
  begin
    sMess:='������� ������: '+IntToStr(iSt)+' ���������� ��� �� ������.';
    Result:=21;
    exit;
  end;
}
end;

Function CheckOpen:Boolean;
Var iSt:INteger;
begin
  if Commonset.CashNum<=0 then begin Result:=False; exit; end;
  result:=False;
//  exit;
  Drv.Password:=iPassw;
  Drv.GetECRStatus;
  iSt:=Drv.ECRMode;
  if iSt=8 then
  begin
    Result:=True;
  end;
end;



end.
