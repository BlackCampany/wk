object fmMainConfig: TfmMainConfig
  Left = 318
  Top = 122
  Width = 669
  Height = 467
  HelpContext = 1
  Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  HelpFile = 'casher.hlp'
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 402
    Width = 661
    Height = 19
    Panels = <>
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 185
    Height = 354
    Align = alLeft
    Caption = #1056#1072#1079#1076#1077#1083#1099
    TabOrder = 1
    object Tree1: TTreeView
      Left = 2
      Top = 15
      Width = 181
      Height = 337
      HelpType = htKeyword
      HelpKeyword = #1055#1088#1086#1075#1088#1072#1084#1084#1080#1088#1086#1074#1072#1085#1080#1077' '#1082#1083#1072#1074#1080#1072#1090#1091#1088#1099
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvRaised
      BevelKind = bkFlat
      BorderStyle = bsNone
      Images = dmC.imState
      Indent = 19
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      OnChange = Tree1Change
      Items.Data = {
        020000002D0000000000000000000000FFFFFFFFFFFFFFFF0000000004000000
        14CDE0F1F2F0EEE9EAE820EAEBE0E2E8E0F2F3F0FB2A00000000000000000000
        00FFFFFFFFFFFFFFFF000000000000000011CAE0F1F1EEE2FBE520EEEFE5F0E0
        F6E8E8270000000900000009000000FFFFFFFFFFFFFFFF00000000000000000E
        C3F0F3EFEFFB20F2EEE2E0F0EEE21F0000000000000000000000FFFFFFFFFFFF
        FFFF000000000000000006D2EEE2E0F0FB1F0000000000000000000000FFFFFF
        FFFFFFFFFF000000000000000006CEF2E4E5EBFB250000000000000000000000
        FFFFFFFFFFFFFFFF00000000010000000CCEE1EEF0F3E4EEE2E0EDE8E5200000
        000000000000000000FFFFFFFFFFFFFFFF000000000000000007D1EAE0EDE5F0
        FB}
    end
  end
  object Panel2: TPanel
    Left = 185
    Top = 0
    Width = 476
    Height = 354
    Align = alClient
    BevelInner = bvLowered
    Color = clSkyBlue
    TabOrder = 2
    object Panel3: TPanel
      Left = 232
      Top = 56
      Width = 105
      Height = 271
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 0
      Visible = False
      object Grid1: TcxGrid
        Left = 32
        Top = 16
        Width = 337
        Height = 145
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object View1: TcxGridDBTableView
          DataController.DataSource = dmC.dsCF_Operations
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          object View1NAME: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            Styles.Content = dmC.cxStyle8
            Width = 136
            DataBinding.FieldName = 'NAME'
          end
          object View1KEY_CODE: TcxGridDBColumn
            Caption = #1050#1086#1076
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CODE'
          end
          object View1KEY_CHAR: TcxGridDBColumn
            Caption = #1050#1083#1072#1074#1080#1096#1072
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CHAR'
          end
          object View1NAME1: TcxGridDBColumn
            Caption = #1056#1077#1078#1080#1084
            Styles.Content = dmC.cxStyle6
            Width = 77
            DataBinding.FieldName = 'NAME1'
          end
          object View1KEY_POSITION: TcxGridDBColumn
            Caption = #1050#1083#1102#1095'.'#1087#1086#1083'.'
            Styles.Content = dmC.cxStyle6
            Width = 65
            DataBinding.FieldName = 'KEY_POSITION'
          end
          object View1BARCODE: TcxGridDBColumn
            Caption = #1064#1050
            Styles.Content = dmC.cxStyle6
            Width = 93
            DataBinding.FieldName = 'BARCODE'
          end
        end
        object Level1: TcxGridLevel
          GridView = View1
        end
      end
    end
    object Panel4: TPanel
      Left = 336
      Top = 42
      Width = 129
      Height = 309
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 1
      object TreeClassif: TTreeView
        Left = 8
        Top = 8
        Width = 417
        Height = 81
        Images = dmC.imState
        Indent = 19
        ReadOnly = True
        TabOrder = 0
        OnExpanding = TreeClassifExpanding
      end
      object GridCl: TcxGrid
        Left = 8
        Top = 120
        Width = 250
        Height = 176
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        object ViewCl: TcxGridDBTableView
          DataController.DataSource = dmC.dsCF_Operations
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.GroupByBox = False
          object ViewClNAMECL: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Styles.Content = dmC.cxStyle2
            Width = 184
            DataBinding.FieldName = 'NAMECL'
          end
          object ViewClKEY_CODE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1082#1083#1072#1074#1080#1096#1080
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CODE'
          end
          object ViewClKEY_CHAR: TcxGridDBColumn
            Caption = #1050#1083#1072#1074#1080#1096#1072
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CHAR'
          end
        end
        object Level2: TcxGridLevel
          GridView = ViewCl
        end
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 48
      Width = 231
      Height = 295
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object GridGoods: TcxGrid
        Left = 0
        Top = 0
        Width = 417
        Height = 151
        TabOrder = 0
        object ViewGoods: TcxGridDBTableView
          DataController.DataSource = dmC.dsMenuSelKey
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          object ViewGoodsSIFR: TcxGridDBColumn
            Caption = #1042#1085'.'#1050#1086#1076
            Styles.Content = dmC.cxStyle1
            DataBinding.FieldName = 'SIFR'
          end
          object ViewGoodsCODE: TcxGridDBColumn
            Caption = #1050#1086#1076
            Styles.Content = dmC.cxStyle3
            DataBinding.FieldName = 'CODE'
          end
          object ViewGoodsNAME: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Styles.Content = dmC.cxStyle4
            Width = 200
            DataBinding.FieldName = 'NAME'
          end
          object ViewGoodsPRICE: TcxGridDBColumn
            Caption = #1062#1077#1085#1072
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Styles.Content = dmC.cxStyle10
            DataBinding.FieldName = 'PRICE'
          end
        end
        object Level3: TcxGridLevel
          GridView = ViewGoods
        end
      end
      object GridCFGoods: TcxGrid
        Left = -3
        Top = 160
        Width = 250
        Height = 135
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        object ViewCFGoods: TcxGridDBTableView
          DataController.DataSource = dmC.dsCF_Operations
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.GroupByBox = False
          object ViewCFGoodsDBColumn1: TcxGridDBColumn
            Caption = #1050#1086#1076
            Styles.Content = dmC.cxStyle3
            DataBinding.FieldName = 'BAR'
          end
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Styles.Content = dmC.cxStyle2
            Width = 184
            DataBinding.FieldName = 'NAMECS'
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1082#1083#1072#1074#1080#1096#1080
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CODE'
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = #1050#1083#1072#1074#1080#1096#1072
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CHAR'
          end
        end
        object Level4: TcxGridLevel
          GridView = ViewCFGoods
        end
      end
    end
    object SpeedBar1: TSpeedBar
      Left = 2
      Top = 2
      Width = 472
      Height = 48
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
      BtnOffsetHorz = 4
      BtnOffsetVert = 4
      BtnWidth = 60
      BtnHeight = 40
      BevelInner = bvLowered
      TabOrder = 4
      InternalVer = 1
      object SpeedbarSection1: TSpeedbarSection
        Caption = 'Untitled (0)'
      end
      object SpeedItem1: TSpeedItem
        Action = acAdd
        BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        Hint = #1044#1086#1073#1072#1074#1080#1090#1100
        Spacing = 1
        Left = 14
        Top = 4
        Visible = True
        OnClick = acAddExecute
        SectionName = 'Untitled (0)'
      end
      object SpeedItem2: TSpeedItem
        Action = acEdit
        BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
        Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
        Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
        Spacing = 1
        Left = 74
        Top = 4
        Visible = True
        OnClick = acEditExecute
        SectionName = 'Untitled (0)'
      end
      object SpeedItem3: TSpeedItem
        Action = acDel
        BtnCaption = #1059#1076#1072#1083#1080#1090#1100
        Caption = #1059#1076#1072#1083#1080#1090#1100
        Hint = #1059#1076#1072#1083#1080#1090#1100
        Spacing = 1
        Left = 174
        Top = 4
        Visible = True
        OnClick = acDelExecute
        SectionName = 'Untitled (0)'
      end
      object SpeedItem4: TSpeedItem
        BtnCaption = #1042#1099#1093#1086#1076
        Caption = #1042#1099#1093#1086#1076
        Glyph.Data = {
          5E040000424D5E04000000000000360000002800000012000000130000000100
          18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
          CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
          D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
          D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
          D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
          8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
          0000CED3D6848684848684848684848684848684848684848684848684848684
          848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
          7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
          FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
          00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
          D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
          75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
          0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
          FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
          CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
          007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
          00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
          D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
          75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
          0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
          FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
          CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
          00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
          D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
          D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
          494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
          0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
          00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
          CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
          D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
          D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
          D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
          D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
          0000}
        Hint = #1042#1099#1093#1086#1076'|'
        Spacing = 1
        Left = 364
        Top = 4
        Visible = True
        OnClick = SpeedItem4Click
        SectionName = 'Untitled (0)'
      end
    end
    object Panel6: TPanel
      Left = 152
      Top = 48
      Width = 209
      Height = 295
      BevelOuter = bvLowered
      TabOrder = 3
      Visible = False
      object GridDep: TcxGrid
        Left = 16
        Top = 8
        Width = 194
        Height = 97
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object ViewDep: TcxGridDBTableView
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          object ViewDepID: TcxGridDBColumn
            Caption = #1050#1086#1076
            Styles.Content = dmC.cxStyle2
            Width = 53
            DataBinding.FieldName = 'ID'
          end
          object ViewDepNAME: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Styles.Content = dmC.cxStyle3
            DataBinding.FieldName = 'NAME'
          end
        end
        object Level5: TcxGridLevel
          GridView = ViewDep
        end
      end
      object GridCFDep: TcxGrid
        Left = 16
        Top = 136
        Width = 425
        Height = 136
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        object ViewCFDep: TcxGridDBTableView
          DataController.DataSource = dmC.dsCF_Operations
          DataController.Filter.Criteria = {FFFFFFFF0000000000}
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.GroupByBox = False
          object ViewCFDepID_DEPARTS: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
            Styles.Content = dmC.cxStyle2
            DataBinding.FieldName = 'ID_DEPARTS'
          end
          object ViewCFDepNAMEDEP: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Styles.Content = dmC.cxStyle3
            Width = 209
            DataBinding.FieldName = 'NAMEDEP'
          end
          object ViewCFDepKEY_CODE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1082#1083#1072#1074#1080#1096#1080
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CODE'
          end
          object ViewCFDepKEY_CHAR: TcxGridDBColumn
            Caption = #1050#1083#1072#1074#1080#1096#1072
            Styles.Content = dmC.cxStyle7
            DataBinding.FieldName = 'KEY_CHAR'
          end
        end
        object Level6: TcxGridLevel
          GridView = ViewCFDep
        end
      end
    end
  end
  object Memo1: TRichEdit
    Left = 0
    Top = 354
    Width = 661
    Height = 48
    Align = alBottom
    HideScrollBars = False
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object MainMenu1: TMainMenu
    Left = 32
    Top = 144
    object N1: TMenuItem
      Caption = #1057#1080#1089#1090#1077#1084#1072
      object N2: TMenuItem
        Caption = '-'
      end
      object N3: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N3Click
      end
    end
    object N4: TMenuItem
      Caption = #1050#1083#1072#1074#1080#1072#1090#1091#1088#1072
      object N5: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
        OnClick = N6Click
      end
    end
  end
  object AM1: TActionManager
    Left = 72
    Top = 208
    StyleName = 'XP Style'
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelExecute
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 112
    Top = 144
  end
end
