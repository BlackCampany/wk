unit RecalcPer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ComCtrls, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase,
  pFIBDatabase, DB, FIBDataSet, pFIBDataSet, cxCheckBox;

type
  TfmRecalcPer = class(TForm)
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    cxButton2: TcxButton;
    prDelPer: TpFIBStoredProc;
    trS: TpFIBTransaction;
    trD: TpFIBTransaction;
    trU: TpFIBTransaction;
    prTestPart: TpFIBStoredProc;
    quDIn: TpFIBDataSet;
    quDInID: TFIBIntegerField;
    quDInIDCLI: TFIBIntegerField;
    quDInIDSKL: TFIBIntegerField;
    quDInSUMIN: TFIBFloatField;
    quDInSUMUCH: TFIBFloatField;
    quDInSUMTAR: TFIBFloatField;
    quDInSUMNDS0: TFIBFloatField;
    quDInSUMNDS1: TFIBFloatField;
    quDInSUMNDS2: TFIBFloatField;
    quDInPROCNAC: TFIBFloatField;
    quDCompl: TpFIBDataSet;
    quDAct: TpFIBDataSet;
    quDComplID: TFIBIntegerField;
    quDComplDATEDOC: TFIBDateField;
    quDComplNUMDOC: TFIBStringField;
    quDComplIDSKL: TFIBIntegerField;
    quDComplSUMIN: TFIBFloatField;
    quDComplSUMUCH: TFIBFloatField;
    quDComplSUMTAR: TFIBFloatField;
    quDComplPROCNAC: TFIBFloatField;
    quDComplIACTIVE: TFIBIntegerField;
    quDComplOPER: TFIBStringField;
    quDActID: TFIBIntegerField;
    quDActDATEDOC: TFIBDateField;
    quDActNUMDOC: TFIBStringField;
    quDActIDSKL: TFIBIntegerField;
    quDActSUMIN: TFIBFloatField;
    quDActSUMUCH: TFIBFloatField;
    quDActIACTIVE: TFIBIntegerField;
    quDActOPER: TFIBStringField;
    quDActCOMMENT: TFIBStringField;
    quDVn: TpFIBDataSet;
    quDVnID: TFIBIntegerField;
    quDVnDATEDOC: TFIBDateField;
    quDVnNUMDOC: TFIBStringField;
    quDVnIDSKL_FROM: TFIBIntegerField;
    quDVnIDSKL_TO: TFIBIntegerField;
    quDVnSUMIN: TFIBFloatField;
    quDVnSUMUCH: TFIBFloatField;
    quDVnSUMUCH1: TFIBFloatField;
    quDVnSUMTAR: TFIBFloatField;
    quDVnPROCNAC: TFIBFloatField;
    quDVnIACTIVE: TFIBIntegerField;
    quDRet: TpFIBDataSet;
    quDRetID: TFIBIntegerField;
    quDRetDATEDOC: TFIBDateField;
    quDRetNUMDOC: TFIBStringField;
    quDRetDATESF: TFIBDateField;
    quDRetNUMSF: TFIBStringField;
    quDRetIDCLI: TFIBIntegerField;
    quDRetIDSKL: TFIBIntegerField;
    quDRetSUMIN: TFIBFloatField;
    quDRetSUMUCH: TFIBFloatField;
    quDRetSUMTAR: TFIBFloatField;
    quDRetSUMNDS0: TFIBFloatField;
    quDRetSUMNDS1: TFIBFloatField;
    quDRetSUMNDS2: TFIBFloatField;
    quDRetPROCNAC: TFIBFloatField;
    quDRetIACTIVE: TFIBIntegerField;
    quDOutB: TpFIBDataSet;
    quDOutBID: TFIBIntegerField;
    quDOutBDATEDOC: TFIBDateField;
    quDOutBNUMDOC: TFIBStringField;
    quDOutBDATESF: TFIBDateField;
    quDOutBNUMSF: TFIBStringField;
    quDOutBIDCLI: TFIBIntegerField;
    quDOutBIDSKL: TFIBIntegerField;
    quDOutBSUMIN: TFIBFloatField;
    quDOutBSUMUCH: TFIBFloatField;
    quDOutBSUMTAR: TFIBFloatField;
    quDOutBSUMNDS0: TFIBFloatField;
    quDOutBSUMNDS1: TFIBFloatField;
    quDOutBSUMNDS2: TFIBFloatField;
    quDOutBPROCNAC: TFIBFloatField;
    quDOutBIACTIVE: TFIBIntegerField;
    quDOutBOPER: TFIBStringField;
    quDInv: TpFIBDataSet;
    quDInvID: TFIBIntegerField;
    quDInvDATEDOC: TFIBDateField;
    quDInvNUMDOC: TFIBStringField;
    quDInvIDSKL: TFIBIntegerField;
    quDInvIACTIVE: TFIBIntegerField;
    quDInvITYPE: TFIBIntegerField;
    quDInvSUM1: TFIBFloatField;
    quDInvSUM11: TFIBFloatField;
    quDInvSUM2: TFIBFloatField;
    quDInvSUM21: TFIBFloatField;
    prSetSpecActive: TpFIBStoredProc;
    quDInIACTIVE: TFIBIntegerField;
    quDR: TpFIBDataSet;
    quDRID: TFIBIntegerField;
    quDRDATEDOC: TFIBDateField;
    quDRNUMDOC: TFIBStringField;
    quDRDATESF: TFIBDateField;
    quDRNUMSF: TFIBStringField;
    quDRIDCLI: TFIBIntegerField;
    quDRIDSKL: TFIBIntegerField;
    quDRSUMIN: TFIBFloatField;
    quDRSUMUCH: TFIBFloatField;
    quDRSUMTAR: TFIBFloatField;
    quDRSUMNDS0: TFIBFloatField;
    quDRSUMNDS1: TFIBFloatField;
    quDRSUMNDS2: TFIBFloatField;
    quDRPROCNAC: TFIBFloatField;
    quDRIACTIVE: TFIBIntegerField;
    cxCheckBox1: TcxCheckBox;
    quC: TpFIBDataSet;
    quCID: TFIBIntegerField;
    PRNORMPARTIN: TpFIBStoredProc;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    trS1: TpFIBTransaction;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(StrWk:String);
  end;

var
  fmRecalcPer: TfmRecalcPer;

implementation

uses Un1, dmOffice, DMOReps, DocCompl, AddCompl, ActPer, DocsVn, DocsOut,
  DocOutB, DOBSpec, DocInv, DocOutR;

{$R *.dfm}

procedure TfmRecalcPer.prWr(StrWk:String);
Var StrWk1:String;
begin
  StrWk1:=FormatDateTime('mm.dd hh:nn:sss',now)+' '+StrWk;
  Memo1.Lines.Add(StrWk1);
end;


procedure TfmRecalcPer.cxButton1Click(Sender: TObject);
Var iDateB,iDateE,iDateCur:Integer;
    iE:INteger;
    rSum1,rSum2:Real;
    rSum11,rSum22:Real;
    iC:INteger;
    rQ:Real;
begin
  //�������� �������
  Memo1.Clear;
  iDateB:=Trunc(cxDateEdit1.Date);
  iDateE:=Trunc(Date);

  if not CanEdit(iDateB) then begin prWr('������ ������.'); exit; end;

  prWr('�������� ��������.');

  prWr('  ������ ������.');
  prDelPer.ParamByName('IDATE').AsInteger:=iDateB;
  prDelPer.ExecProc;
  prWr('  --- ������� ��.'); delay(10);

  prWr('  ���� ����������� �����.');
  prTestPart.ExecProc;
  iE:=prTestPart.ParamByName('RESULT').AsInteger;
  prWr('  --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

  if cxCheckBox1.Checked then
  begin
    prWr('  �������� ����������� �����.');
    prWr('    ����� ... ');

    try
      quC.Active:=False;
      quC.Active:=True;

      quMHall1.Active:=False;
      quMHall1.Active:=True;

      iC:=0;
      quC.First;
      while not quC.Eof do
      begin

        quMHAll1.First;
        while not quMHAll1.Eof do
        begin
          //������� ��� �� ���� - �� �� ����� ����������� ���

          rQ:=prCalcRemn(quCID.AsInteger,iDateB-1,quMHAll1ID.AsInteger);

//      EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT)

          PRNORMPARTIN.ParamByName('IDSKL').AsInteger:=quMHAll1ID.AsInteger;
          PRNORMPARTIN.ParamByName('IDATE').AsInteger:=iDateB-1;
          PRNORMPARTIN.ParamByName('IDCARD').AsInteger:=quCID.AsInteger;
          PRNORMPARTIN.ParamByName('QUANT').AsFloat:=rQ;
          PRNORMPARTIN.ExecProc;
          delay(10);

          quMHAll1.Next;
        end;

        quC.Next; inc(iC);
        if iC mod 100 = 0 then
        begin
          prWr('    ����� ... ');
        end;
      end;
    finally
      quC.Active:=False;
      quMHall1.Active:=False;
    end;

    prWr('  --- ������� ��.'); delay(10);
  end;

  prWr('  ����������� ������ ����������.(2)');
  prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
  prSetSpecActive.ParamByName('ATYPE').AsInteger:=2;
  prSetSpecActive.ExecProc;
  prWr('  --- ������� ��. '); delay(10);

  with dmO do
  with dmORep do
  begin
    for iDateCur:=iDateB to iDateE do
    begin
      prWr('  ��������� - '+FormatDateTime('dd.mm.yyyy',iDateCur)+' ('+IntToStr(iDateCur)+')');

      delay(10); //�������� 10 ��� �� ������ ������ - ����� �������� ������ ��������

      bPrintMemo:=False;

      prWr('    ��������� ���������.');
      iE:=0;
      quDIn.Active:=False;
      quDIn.ParamByName('DDATE').AsDate:=iDateCur;
      quDIn.Active:=True;
      quDIn.First;
      while not quDIn.Eof do
      begin
        prAddPartIn.ParamByName('IDDOC').AsInteger:=quDInID.AsInteger;
        prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
        prAddPartIn.ParamByName('IDSKL').AsInteger:=quDInIDSKL.AsInteger;
        prAddPartIn.ParamByName('IDCLI').AsInteger:=quDInIDCLI.AsInteger;
        prAddPartIn.ParamByName('IDATE').AsInteger:=iDateCur;
        prAddPartIn.ExecProc;

        quDIn.Edit;
        quDInIACTIVE.AsInteger:=1;
        quDIn.Post;

        quDIn.Next; delay(10); inc(iE);
      end;
      quDIn.Active:=False;
      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

      prWr('    ������������.');

      iE:=0;
      quDCompl.Active:=False;
      quDCompl.ParamByName('DDATE').AsDate:=iDateCur;
      quDCompl.Active:=True;
      quDCompl.First;
      while not quDCompl.Eof do
      begin
        fmDocsCompl.prOpenSpec(quDComplID.AsInteger); delay(100);
        fmAddCompl.prCalcC(iDateCur,quDComplIDSKL.AsInteger); delay(100);
        fmAddCompl.prCalcPr(quDComplIDSKL.AsInteger); delay(100);
        fmAddCompl.prSave(quDComplID.AsInteger,rSum1,rSum2); delay(100);
        fmDocsCompl.prOn(quDComplID.AsInteger,quDComplIDSKL.AsInteger,iDateCur,rSum1,rSum2);

        quDCompl.Edit;
        quDComplIACTIVE.AsInteger:=1;
        quDComplSUMIN.AsFloat:=rSum1;
        quDComplSUMUCH.AsFloat:=rSum2;
        quDCompl.Post;

        quDCompl.Next; delay(10);inc(iE);
      end;
      quDCompl.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

      prWr('    ���������� �����������.');

      iE:=0;
      quDVn.Active:=False;
      quDVn.ParamByName('DDATE').AsDate:=iDateCur;
      quDVn.Active:=True;
      quDVn.First;
      while not quDVn.Eof do
      begin
        fmDocsVn.prPart(quDVnID.AsInteger,quDVnIDSKL_FROM.AsInteger,quDVnIDSKL_TO.AsInteger,iDateCur,rSum1,rSum2);

        quDVn.Edit;
        quDVnIACTIVE.AsInteger:=1;
        quDVnSUMIN.AsFloat:=RoundVal(rSum1);
        quDVnSUMTAR.AsFloat:=RoundVal(rSum2);
        quDVn.Post;

        quDVn.Next; delay(10);inc(iE);
      end;
      quDVn.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


      prWr('    ���������� �� �������.');

      iE:=0;
      quDR.Active:=False;
      quDR.ParamByName('DDATE').AsDate:=iDateCur;
      quDR.Active:=True;
      quDR.First;
      while not quDR.Eof do
      begin
        fmDocsReal.prOn(quDRID.AsInteger,quDRIDSKL.AsInteger,iDateCur,quDRIDCLI.AsInteger,rSum1,rSum2);

        quDR.Edit;
        quDRIACTIVE.AsInteger:=1;
        quDRSUMIN.AsFloat:=RoundVal(rSum1);
        quDRSUMTAR.AsFloat:=RoundVal(rSum2);
        quDR.Post;

        quDR.Next; delay(10);inc(iE);
      end;
      quDR.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


      prWr('    ���� �����������.');

      iE:=0;
      quDAct.Active:=False;
      quDAct.ParamByName('DDATE').AsDate:=iDateCur;
      quDAct.Active:=True;
      quDAct.First;
      while not quDAct.Eof do
      begin
        fmDocsActs.prPriceOut(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur); delay(100);//�������� ������ � ������������ ���� ������
        fmDocsActs.prPriceIn(quDActID.AsInteger); delay(100);//�������� ���� ������� � �������
        fmDocsActs.prPartIn(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur,rSum1);delay(100); // ������������ ��������� ������

        rSum2:=rSum1; //����

        quDAct.Edit;
        quDActIACTIVE.AsInteger:=1;
        quDActSUMIN.AsFloat:=rSum1;
        quDActSUMUCH.AsFloat:=rSum2;
        quDAct.Post;

        quDAct.Next; delay(10);inc(iE);
      end;
      quDAct.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


      prWr('    ������� ����������.');

      iE:=0;
      quDRet.Active:=False;
      quDRet.ParamByName('DDATE').AsDate:=iDateCur;
      quDRet.Active:=True;
      quDRet.First;
      while not quDRet.Eof do
      begin
//        fmDocsVn.prPart(quDRetID.AsInteger,quDRetIDSKL_FROM.AsInteger,quDRetIDSKL_TO.AsInteger,iDateCur,rSum1,rSum2);
        fmDocsOut.prOn(quDRetID.AsInteger,quDRetIDSKL.AsInteger,iDateCur,quDRetIDCLI.AsInteger,rSum1,rSum2);

        quDRet.Edit;
        quDRetIACTIVE.AsInteger:=1;
        quDRetSUMIN.AsFloat:=RoundVal(rSum1);
        quDRetSUMTAR.AsFloat:=RoundVal(rSum2);
        quDRet.Post;

        quDRet.Next; delay(10);inc(iE);
      end;
      quDRet.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

      prWr('    ����������.');

      iE:=0;
      quDOutB.Active:=False;
      quDOutB.ParamByName('DDATE').AsDate:=iDateCur;
      quDOutB.Active:=True;
      quDOutB.First;
      while not quDOutB.Eof do
      begin
        prWr('        ���. �'+quDOutBNUMDOC.AsString+' '+FloatToStr(quDOutBSUMUCH.AsFloat)+' ����. '+quDOutBOPER.AsString);
//        prWr('          prOpen');
        fmDocsOutB.prOpen(quDOutBID.AsInteger,1,Memo1);  //1 - ������ ��������, �� ��������� ������������ ������� � �������
//        prWr('          prCalcQuant');
        fmDOBSpec.prCalcQuant(quDOutBIDSKL.AsInteger,iDateCur,False,FindTSpis(quDOutBOPER.AsString));                                           // ��� �������� �������
//        prWr('          prSave');
        fmDOBSpec.prSave(quDOutBID.AsInteger,rSum1,rSum2);
//        prWr('          prOn');
        fmDocsOutB.prOn(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,iDateCur,rSum2,rSum2);

//        prWr('          end');
        quDOutB.Edit;
        quDOutBIACTIVE.AsInteger:=1;
        quDOutBSUMUCH.AsFloat:=RoundVal(rSum1);
        quDOutBSUMIN.AsFloat:=RoundVal(rSum2);
        quDOutB.Post;

        quDOutB.Next; delay(10);inc(iE);
      end;
      quDOutB.Active:=False;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


      prWr('    ��������������.');

      iE:=0;
      quDInv.Active:=False;
      quDInv.ParamByName('DDATE').AsDate:=iDateCur;
      quDInv.Active:=True;
      quDInv.First;
      while not quDInv.Eof do
      begin
        prWr('        ���. �'+quDInvNUMDOC.AsString+'����� ����.'+FloatToStr(RoundVal(quDInvSUM1.AsFloat))+' ����.'+FloatToStr(RoundVal(quDInvSUM2.AsFloat)));
        fmDocsInv.prCalcRemnInv(quDInvID.AsInteger,iDateCur,quDInvIDSKL.AsInteger); //�������� ���������� ���-��
        //����������
        fmDocsInv.prOn(quDInvId.AsInteger,iDateCur,quDInvIDSKL.AsInteger,rSum1,rSum2,rSum11,rSum22);


        quDInv.Edit;
        quDInvSUM1.AsFloat:=RoundVal(rSum1);
        quDInvSUM11.AsFloat:=RoundVal(rSum11);
        quDInvSUM2.AsFloat:=RoundVal(rSum2);
        quDInvSUM21.AsFloat:=RoundVal(rSum22);
        quDInvIACTIVE.AsInteger:=1;
        quDInv.Post;

        quDInv.Next; delay(10);inc(iE);
      end;
      quDInv.Active:=False;

      bPrintMemo:=True;

      prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

    end;
  end;

  prWr('  ����������� ������ ����������.(1)');
  prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
  prSetSpecActive.ParamByName('ATYPE').AsInteger:=1;
  prSetSpecActive.ExecProc;
  prWr('  --- ������� ��. '); delay(10);

  prWr('�������� ��������.');
end;

procedure TfmRecalcPer.cxButton2Click(Sender: TObject);
begin
  close;
end;

end.
