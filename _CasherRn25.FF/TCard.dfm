object fmTCard: TfmTCard
  Left = 290
  Top = 117
  Width = 585
  Height = 557
  Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1072#1103' '#1082#1072#1088#1090#1072
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 436
    Top = 297
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D617076010000424D760100000000000036000000280000000A00
      00000A000000010018000000000040010000C40E0000C40E0000000000000000
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
      0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
      0000FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966
      0000CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99
      0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF
      0000}
    Proportional = True
    Transparent = True
    OnClick = cxLabel1Click
  end
  object Image2: TImage
    Left = 436
    Top = 321
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D617076010000424D760100000000000036000000280000000D00
      0000080000000100180000000000400100000000000000000000000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633A
      F3633AF3633AF3633AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3
      A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFF
      FF00FFFFFF633AF3C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FF
      C3A7FFA777E8FFFFFF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A7
      77E8A777E8A777E8A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00}
    Proportional = True
    Transparent = True
    OnClick = cxLabel2Click
  end
  object Label7: TLabel
    Left = 456
    Top = 352
    Width = 74
    Height = 13
    Cursor = crHandPoint
    Caption = #1041#1088#1091#1090#1090#1086' '#1085#1072' '#1076#1072#1090#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label7Click
  end
  object Image3: TImage
    Left = 436
    Top = 265
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D61709A020000424D9A0200000000000036000000280000000B00
      0000110000000100180000000000640200000000000000000000000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000000000000000000000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000FF
      6600000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000
      FF6600FF9966FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
      FFFFFFFF000000FF6600FF9966000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFF000000FF9966FF6600FFFFFF000000FFFFFFFFFFFFFFFFFF
      FFFFFF000000FFFFFFFFFFFFFFFFFF000000FF9966FF6600000000000000FFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FF6600FF9966FFFFFF00
      0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF6600
      000000000000000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF0000
      000000000000BD00FFFF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF0000000000BD00FFFF00FFFF000000FFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000000000BD0000BD0000BD000000FFFFFF000000FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF00
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF000000}
    Proportional = True
    Transparent = True
    OnClick = cxLabel1Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 257
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label6: TLabel
      Left = 208
      Top = 16
      Width = 49
      Height = 13
      AutoSize = False
      Caption = #1050#1086#1076' '
    end
    object GTCards: TcxGrid
      Left = 8
      Top = 40
      Width = 193
      Height = 209
      PopupMenu = PopupMenuTCard
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object ViewTCards: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        OnFocusedRecordChanged = ViewTCardsFocusedRecordChanged
        DataController.DataSource = dmO.dsTCards
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViewTCardsIDCARD: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1082#1072#1088#1090#1099
          DataBinding.FieldName = 'IDCARD'
          Visible = False
        end
        object ViewTCardsID: TcxGridDBColumn
          Caption = #1050#1086#1076' '
          DataBinding.FieldName = 'ID'
          Width = 39
        end
        object ViewTCardsDATEB: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1085#1072#1095'.'
          DataBinding.FieldName = 'DATEB'
        end
        object ViewTCardsDATEE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095'.'
          DataBinding.FieldName = 'DATEE'
          Visible = False
          Width = 68
        end
        object ViewTCardsSHORTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'SHORTNAME'
          Visible = False
        end
        object ViewTCardsRECEIPTNUM: TcxGridDBColumn
          DataBinding.FieldName = 'RECEIPTNUM'
          Visible = False
        end
        object ViewTCardsPOUTPUT: TcxGridDBColumn
          DataBinding.FieldName = 'POUTPUT'
          Visible = False
        end
        object ViewTCardsPCOUNT: TcxGridDBColumn
          DataBinding.FieldName = 'PCOUNT'
          Visible = False
        end
        object ViewTCardsPVES: TcxGridDBColumn
          DataBinding.FieldName = 'PVES'
          Visible = False
        end
        object ViewTCardsSDATEE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095'.'
          DataBinding.FieldName = 'SDATEE'
          Width = 67
        end
      end
      object LTCards: TcxGridLevel
        GridView = ViewTCards
      end
    end
    object cxButton4: TcxButton
      Left = 8
      Top = 8
      Width = 41
      Height = 22
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1058#1050
      TabOrder = 1
      OnClick = cxButton4Click
      Glyph.Data = {
        76010000424D760100000000000036000000280000000A0000000A0000000100
        18000000000040010000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FF6600FF6600
        FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF6600FF6600
        FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF9966FF9966
        FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF99660000CCCC99CCCC99
        CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC990000FFFFFFFFFFFF
        FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton5: TcxButton
      Left = 96
      Top = 8
      Width = 22
      Height = 22
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1058#1050
      TabOrder = 2
      OnClick = cxButton5Click
      Glyph.Data = {
        76010000424D760100000000000036000000280000000D000000080000000100
        1800000000004001000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633AF3633AF3633AF363
        3AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3A777E8A777E8A777
        E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFFFF00FFFFFF633AF3
        C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFA777E8FFFF
        FF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8
        A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfOffice11
    end
    object Panel3: TPanel
      Left = 208
      Top = 40
      Width = 361
      Height = 209
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 99
        Height = 13
        Caption = #1050#1086#1088#1086#1090#1082#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 91
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
      end
      object Label3: TLabel
        Left = 8
        Top = 72
        Width = 117
        Height = 13
        Caption = #1042#1099#1093#1086#1076' 1 '#1087#1086#1088#1094'. '#1087#1086' '#1084#1077#1085#1102
      end
      object Label4: TLabel
        Left = 8
        Top = 112
        Width = 217
        Height = 13
        Caption = #1063#1080#1089#1083#1086' '#1087#1086#1088#1094'. '#1085#1072' '#1082#1086#1090#1086#1088#1086#1077' '#1089#1076#1077#1083#1072#1085#1072' '#1079#1072#1082#1083#1072#1076#1082#1072
      end
      object Label5: TLabel
        Left = 8
        Top = 136
        Width = 119
        Height = 13
        Caption = #1052#1072#1089#1089#1072' '#1086#1076#1085#1086#1081' '#1087#1086#1088#1094#1080#1080'  '#1075'.'
      end
      object Label8: TLabel
        Left = 16
        Top = 168
        Width = 32
        Height = 13
        Caption = 'Label8'
        Visible = False
      end
      object cxTextEdit2: TcxTextEdit
        Left = 128
        Top = 12
        ParentFont = False
        Properties.MaxLength = 100
        Properties.OnChange = cxTextEdit2PropertiesChange
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.Shadow = True
        Style.IsFontAssigned = True
        TabOrder = 0
        Text = 'cxTextEdit2'
        Width = 225
      end
      object cxTextEdit3: TcxTextEdit
        Left = 152
        Top = 44
        Properties.OnChange = cxTextEdit3PropertiesChange
        Style.Shadow = True
        TabOrder = 1
        Text = 'cxTextEdit3'
        Width = 113
      end
      object cxTextEdit4: TcxTextEdit
        Left = 152
        Top = 68
        Properties.OnChange = cxTextEdit4PropertiesChange
        Style.Shadow = True
        TabOrder = 2
        Text = 'cxTextEdit4'
        Width = 113
      end
      object cxSpinEdit1: TcxSpinEdit
        Left = 232
        Top = 108
        Properties.OnChange = cxSpinEdit1PropertiesChange
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        Value = 1
        Width = 89
      end
      object cxCalcEdit1: TcxCalcEdit
        Left = 232
        Top = 132
        EditValue = 0.000000000000000000
        Properties.OnChange = cxCalcEdit1PropertiesChange
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 4
        Width = 89
      end
      object cxButton1: TcxButton
        Left = 120
        Top = 168
        Width = 113
        Height = 25
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        Enabled = False
        TabOrder = 5
        OnClick = cxButton1Click
        LookAndFeel.Kind = lfOffice11
      end
    end
    object cxTextEdit1: TcxTextEdit
      Left = 264
      Top = 9
      TabStop = False
      ParentFont = False
      Properties.MaxLength = 200
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 4
      Text = 'cxTextEdit1'
      Width = 305
    end
    object cxButton6: TcxButton
      Left = 56
      Top = 8
      Width = 22
      Height = 22
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1058#1050
      TabOrder = 5
      Visible = False
      OnClick = cxButton6Click
      Glyph.Data = {
        9A020000424D9A0200000000000036000000280000000B000000110000000100
        1800000000006402000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF000000FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFF000000000000000000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000FF6600000000000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF6600FF9966FFFF
        FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000FF
        6600FF9966000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        000000FF9966FF6600FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFF000000FF9966FF6600000000000000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FF6600FF9966FFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF66000000000000000000
        00FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000BD00
        FFFF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
        0000BD00FFFF00FFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF0000000000BD0000BD0000BD000000FFFFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton7: TcxButton
      Left = 160
      Top = 8
      Width = 22
      Height = 22
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1058#1050
      TabOrder = 6
      Visible = False
      OnClick = cxButton7Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 504
    Width = 577
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 257
    Width = 425
    Height = 247
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 2
    object GTSpec: TcxGrid
      Left = 2
      Top = 2
      Width = 421
      Height = 243
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object ViewTSpec: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmO.dsTSpec
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'BRUTTO'
            Column = ViewTSpecBRUTTO
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'NETTO'
            Column = ViewTSpecNETTO
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object ViewTSpecIDCARD: TcxGridDBColumn
          Caption = #1050#1086#1076' '
          DataBinding.FieldName = 'IDCARD'
          Options.Editing = False
          Width = 45
        end
        object ViewTSpecNAME: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NAME'
          Options.Editing = False
          Width = 137
        end
        object ViewTSpecCURMESSURE: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'CURMESSURE'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Visible = False
          Width = 50
        end
        object ViewTSpecNAMESHORT: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'NAMESHORT'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = ViewTSpecNAMESHORTPropertiesButtonClick
          Width = 51
        end
        object ViewTSpecNETTO: TcxGridDBColumn
          Caption = #1053#1077#1090#1090#1086
          DataBinding.FieldName = 'NETTO'
          Width = 68
        end
        object ViewTSpecBRUTTO: TcxGridDBColumn
          Caption = #1041#1088#1091#1090#1090#1086
          DataBinding.FieldName = 'BRUTTO'
          Width = 70
        end
      end
      object LTSpec: TcxGridLevel
        GridView = ViewTSpec
      end
    end
  end
  object cxButton2: TcxButton
    Left = 440
    Top = 464
    Width = 113
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 3
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxLabel1: TcxLabel
    Left = 456
    Top = 296
    Cursor = crHandPoint
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
    Enabled = False
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    Transparent = True
    OnClick = cxLabel1Click
  end
  object cxLabel2: TcxLabel
    Left = 456
    Top = 320
    Cursor = crHandPoint
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
    Enabled = False
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    Transparent = True
    OnClick = cxLabel2Click
  end
  object cxButton3: TcxButton
    Left = 440
    Top = 424
    Width = 113
    Height = 25
    Caption = #1057#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100
    TabOrder = 6
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxDateEdit1: TcxDateEdit
    Left = 440
    Top = 376
    TabOrder = 7
    Width = 121
  end
  object cxLabel3: TcxLabel
    Left = 456
    Top = 264
    Cursor = crHandPoint
    Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    Transparent = True
    OnClick = cxLabel3Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 96
    Top = 296
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 344
    Top = 320
  end
  object PopupMenuTCard: TPopupMenu
    Left = 152
    Top = 72
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      OnClick = N2Click
    end
  end
  object TCH: TClientDataSet
    Aggregates = <>
    FileName = 'TCH.cds'
    Params = <>
    Left = 24
    Top = 72
    object TCHIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object TCHID: TIntegerField
      FieldName = 'ID'
    end
    object TCHDATEB: TDateField
      FieldName = 'DATEB'
    end
    object TCHDATEE: TDateField
      FieldName = 'DATEE'
    end
    object TCHSHORTNAME: TStringField
      FieldName = 'SHORTNAME'
      Size = 100
    end
    object TCHRECEIPTNUM: TStringField
      FieldName = 'RECEIPTNUM'
      Size = 30
    end
    object TCHPOUTPUT: TStringField
      FieldName = 'POUTPUT'
      Size = 30
    end
    object TCHPCOUNT: TIntegerField
      FieldName = 'PCOUNT'
    end
    object TCHPVES: TFloatField
      FieldName = 'PVES'
    end
  end
  object TCS: TClientDataSet
    Aggregates = <>
    FileName = 'TCS.cds'
    FieldDefs = <
      item
        Name = 'IDC'
        DataType = ftInteger
      end
      item
        Name = 'IDT'
        DataType = ftInteger
      end
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'CURMESSURE'
        DataType = ftInteger
      end
      item
        Name = 'NETTO'
        DataType = ftFloat
      end
      item
        Name = 'BRUTTO'
        DataType = ftFloat
      end
      item
        Name = 'KNB'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'TCSIndex1'
        Fields = 'IDC;IDT;ID'
      end>
    IndexFieldNames = 'IDC;IDT;ID'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 136
    object TCSIDC: TIntegerField
      FieldName = 'IDC'
    end
    object TCSIDT: TIntegerField
      FieldName = 'IDT'
    end
    object TCSID: TIntegerField
      FieldName = 'ID'
    end
    object TCSIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object TCSCURMESSURE: TIntegerField
      FieldName = 'CURMESSURE'
    end
    object TCSNETTO: TFloatField
      FieldName = 'NETTO'
    end
    object TCSBRUTTO: TFloatField
      FieldName = 'BRUTTO'
    end
    object TCSKNB: TFloatField
      FieldName = 'KNB'
    end
  end
  object dsTCH: TDataSource
    DataSet = TCH
    Left = 88
    Top = 72
  end
  object dsTCS: TDataSource
    DataSet = TCS
    Left = 88
    Top = 136
  end
end
