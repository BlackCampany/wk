unit Period3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox, cxSpinEdit,
  cxTimeEdit, FIBDatabase, pFIBDatabase;

type
  TfmSelPerSkl1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxTimeEdit1: TcxTimeEdit;
    Label2: TLabel;
    cxTimeEdit2: TcxTimeEdit;
    Label4: TLabel;
    CasherRnDb: TpFIBDatabase;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quSpecAll: TpFIBDataSet;
    quSpecAllOPERTYPE: TFIBStringField;
    quSpecAllSIFR: TFIBIntegerField;
    quSpecAllNAME: TFIBStringField;
    quSpecAllCODE: TFIBStringField;
    quSpecAllCONSUMMA: TFIBFloatField;
    quSpecAllQSUM: TFIBFloatField;
    quSpecAllDSUM: TFIBFloatField;
    quSpecAllRSUM: TFIBFloatField;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl1: TfmSelPerSkl1;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmSelPerSkl1.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl1.cxButton1Click(Sender: TObject);
Var CurDate:TDateTime;
    DateB,DateE,iCurDate:Integer;
    sOper:String;
    IDH,IDS:Integer;
    rSum,Km:Real;
    iCode,iM:INteger;
    bHead:Boolean;
begin
  CommonSet.DateFrom:=Trunc(cxDateEdit1.Date);
  CommonSet.DateTo:=Trunc(cxDateEdit2.Date)+1;
  CommonSet.IdStore:=cxLookupComboBox1.EditValue;
  CommonSet.NameStore:=cxLookupComboBox1.Text;

  Label4.Caption:='���������� ����� ������.';delay(10);
  with dmO do
  begin
    CasherRnDb.Connected:=False;
    CasherRnDb.DatabaseName:=DBNAME;
    try
      CasherRnDb.Connected:=True;
    except
      ShowMessage('������������ ����: '+DBNAME+'. ����� ������ ����������.');
    end;

    if CasherRnDb.Connected then
    begin
      //��������� ������
//      sOper:=''; //��������� ��������
      DateB:=Trunc(cxDateEdit1.Date);
      DateE:=Trunc(cxDateEdit2.Date);
      for iCurDate:=DateB to DateE-1 do
      begin
        CurDate:=iCurDate+cxTimeEdit1.Time;
        rSum:=0; IdH:=0; bHead:=False; sOper:='';
        quSpecAll.Active:=False;
        quSpecAll.ParamByName('DATEB').AsDateTime:=CurDate;
        quSpecAll.ParamByName('DATEE').AsDateTime:=CurDate+1;
        quSpecAll.Active:=True;

        quSpecAll.First;
        while not quSpecAll.Eof do
        begin
          if quSpecAllOPERTYPE.AsString<>sOper then
          begin //��������� ��������� � ��������� sOper
            if bHead then
            begin //��������� ��� ���� ����� ��������� �����
              if abs(rSum)>0 then
              begin
                quDobHead.Edit;
                quDOBHEADSUMUCH.AsCurrency:=rSum;
                quDobHead.Post;
              end;
            end;

            sOper:=quSpecAllOPERTYPE.AsString;

            IDH:=GetId('DocOutB');
            quDOBHEAD.Active:=False;
            quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
            quDOBHEAD.Active:=True;

            quDOBHEAD.Append;
            quDOBHEADID.AsInteger:=IDH;
            quDOBHEADDATEDOC.AsDateTime:=iCurDate;
            quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
            quDOBHEADDATESF.AsDateTime:=iCurDate;
            quDOBHEADNUMSF.AsString:=IntToStr(IDH);
            quDOBHEADIDCLI.AsInteger:=0;
            quDOBHEADIDSKL.AsInteger:=CommonSet.IdStore;
            quDOBHEADSUMIN.AsCurrency:=0;
            quDOBHEADSUMUCH.AsCurrency:=0;
            quDOBHEADSUMTAR.AsCurrency:=0;
            quDOBHEADSUMNDS0.AsCurrency:=0;
            quDOBHEADSUMNDS1.AsCurrency:=0;
            quDOBHEADSUMNDS2.AsCurrency:=0;
            quDOBHEADPROCNAC.AsFloat:=0;
            quDOBHEADIACTIVE.AsInteger:=0;
            quDOBHEADOPER.AsString:=sOper;
            quDOBHEAD.Post;

            quDOBSpec.Active:=False;
            quDobSpec.ParamByName('IDH').AsInteger:=IDH;
            quDobSpec.Active:=True;

            bHead:=True;
            rSum:=0; //��������� �����
          end;
          //�������� ������������
          IDS:=GetId('SpecOutB');

          iM:=0; Km:=0;
          iCode:=StrToIntDef(quSpecAllCODE.AsString,0);
          if iCode>0 then
          begin
            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=iCode;
            quFindCard.Active:=True;
            if quFindCard.RecordCount>0 then
            begin
              iM:=quFindCardIMESSURE.AsInteger;
              if iM>0 then
              begin
                quM.Active:=False;
                quM.ParamByName('IDM').AsInt64:=iM;
                quM.Active:=True;
                if quM.RecordCount>0 then KM:=quMKOEF.AsFloat;
                quM.Active:=False;
              end;
            end else iCode:=0;
            quFindCard.Active:=False;
          end;


          quDobSpec.Append;
          quDOBSPECIDHEAD.AsInteger:=IDH;
          quDOBSPECID.AsInteger:=IDS;
          quDOBSPECSIFR.AsInteger:=quSpecAllSIFR.AsInteger;
          quDOBSPECNAMEB.AsString:=quSpecAllNAME.AsString;
          quDOBSPECCODEB.AsString:=quSpecAllCODE.AsString;
          quDOBSPECKB.AsFloat:=quSpecAllCONSUMMA.AsFloat;
          quDOBSPECDSUM.AsFloat:=quSpecAllDSUM.AsFloat;
          quDOBSPECRSUM.AsFloat:=quSpecAllRSUM.AsFloat;
          quDOBSPECQUANT.AsFloat:=quSpecAllQSUM.AsFloat;
          quDOBSPECIDCARD.AsInteger:=iCode;
          quDOBSPECIDM.AsInteger:=iM;
          quDOBSPECKM.AsFloat:=kM;
          quDOBSPECPRICER.AsFloat:=0;
          if abs(quSpecAllQSUM.AsFloat)>0 then quDOBSPECPRICER.AsFloat:=RoundEx(quSpecAllRSUM.AsFloat/quSpecAllQSUM.AsFloat*100)/100;
          quDobSpec.Post;

          rSum:=rSum+quSpecAllRSUM.AsFloat;

          quSpecAll.Next;
        end;
        if bHead then
        begin //��������� ��� ���� ����� ��������� �����
          if abs(rSum)>0 then
          begin
            quDobHead.Edit;
            quDOBHEADSUMUCH.AsCurrency:=rSum;
            quDobHead.Post;
          end;
        end;
        quDobHead.Active:=False;
        quDobSpec.Active:=False;
      end;
    end;
  end;
  Label4.Caption:='����� ������ ��������.';Delay(500);
  ModalResult:=mrOk;
end;

end.
