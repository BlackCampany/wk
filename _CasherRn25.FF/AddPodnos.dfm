object fmAddP: TfmAddP
  Left = 911
  Top = 315
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1085#1086#1089' '#1074' '#1090#1077#1082#1091#1097#1080#1081' '#1095#1077#1082'?'
  ClientHeight = 491
  ClientWidth = 514
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 399
    Width = 514
    Height = 92
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 52
      Top = 9
      Width = 141
      Height = 72
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1095#1077#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Default = 14600630
      Colors.Normal = 14600630
      Colors.Pressed = 15854048
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 315
      Top = 9
      Width = 142
      Height = 72
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      Colors.Default = 14600630
      Colors.Normal = 14600630
      Colors.Pressed = 15854048
      LookAndFeel.Kind = lfFlat
    end
  end
  object GridSpecP: TcxGrid
    Left = 16
    Top = 6
    Width = 477
    Height = 383
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object ViewSpecP: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      FilterBox.CustomizeDialog = False
      DataController.DataSource = dsteSp
      DataController.KeyFieldNames = 'SIFR'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoGroupsAlwaysExpanded]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
        end
        item
          Kind = skSum
          FieldName = 'DSUM'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.FixedBandSeparatorWidth = 0
      Styles.Selection = dmC.cxStyle16
      Styles.Footer = dmC.cxStyle4
      Styles.Header = dmC.cxStyle4
      Styles.BandBackground = dmC.cxStyle23
      Styles.BandHeader = dmC.cxStyle23
      Bands = <
        item
          Width = 454
        end>
      object ViewSpecPRecId: TcxGridDBBandedColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewSpecPSIFR: TcxGridDBBandedColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'SIFR'
        Styles.Content = dmC.cxStyle5
        Width = 34
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewSpecPNAME: TcxGridDBBandedColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle5
        Width = 211
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewSpecPPRICE: TcxGridDBBandedColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        Styles.Content = dmC.cxStyle5
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewSpecPQUANTITY: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Styles.Content = dmC.cxStyle5
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ViewSpecPSUMMA: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SUMMA'
        Styles.Content = dmC.cxStyle5
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
    end
    object LevelSpecP: TcxGridLevel
      GridView = ViewSpecP
    end
  end
  object teSp: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 164
    Top = 96
    object teSpSIFR: TIntegerField
      FieldName = 'SIFR'
    end
    object teSpNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object teSpPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object teSpQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
    object teSpSUMMA: TFloatField
      FieldName = 'SUMMA'
      DisplayFormat = '0.00'
    end
  end
  object dsteSp: TDataSource
    DataSet = teSp
    Left = 168
    Top = 152
  end
end
