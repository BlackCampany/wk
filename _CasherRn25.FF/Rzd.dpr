program Rzd;

uses
  Forms,
  MainRZD in 'MainRZD.pas' {fmMainRZD},
  Un1 in 'Un1.pas',
  DmRZD in 'DmRZD.pas' {dmR: TDataModule},
  ModifFF1 in 'ModifFF1.pas' {fmModifFF1},
  Calc1 in 'Calc1.pas' {fmCalc1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainRZD, fmMainRZD);
  Application.CreateForm(TdmR, dmR);
  Application.CreateForm(TfmCalc1, fmCalc1);
  Application.Run;
end.
