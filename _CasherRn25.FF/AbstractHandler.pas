unit AbstractHandler;
// ������ 2.0 - ��������� ��������� ������� � DLL
// version 1.01b - ���������� ���������� GetVMTEnd
interface
type
  // ���������� � ����������� ������
  TMethodInfoRec = record
    ClassType: TClass; // �����, � ������� ������������ �����
    VMTIndex: cardinal; // ����� ����� � VMT
  end;
  TMIRArray = array of TMethodInfoRec;

  TAbstractHandler = class
  private
    // ������ ������������ ����������� ����������� �������
    procedure HandleAbstract;
    // ���������� ��� ��������� ������, ����������� � VMT
    class procedure AbstractProc; virtual;abstract;
    // ����� ������-������, � ������� ��� ������������ �������� ����������� �����
    class function GetFirstDeclarator(AClass: TClass; VMTIndex: integer): TClass;
  protected
    // ��������������� ������� ��� ���������� ������ ����������
    // ��� �������������� ��� ������� ������ � FormatStr �������������:
    // - ������: ��� ������
    // - �����: ����� ����� VMT
    // ���������� ������ ���������������.
    class function FormatAbstractInfos(const Abstracts: Array of TMethodInfoRec;
      const FormatStr: String = 'Introduced in: %s; VMT: %d'#10#13): String;
  public
    // ���������� ��� ������, � ������� ������������ ��������� �����
    class function GetClassPackageName(AClass: TClass): String;
    // ���������� ��� �����, � ������� ������������ ��������� �����
    // ���������� Unknown, ���� � ������ ��� RTTI ���������� (��. ���� �� $M)
    class function GetClassUnitName(AClass: TClass): String;
    // ���������� �������� ����� �������, ��������������� �� DLL/BPL
    // ���� Addr ��������� �� ���, � �� �� ������, �� ���������� Addr ��� ���������
    class function UnThunkImport(Addr: pointer): pointer;
    // ���������� �����, ������������ � VMT ��� ����������� �������
    class function AbsProcAddress: Pointer;
    // ���������, �������� �� ��������� ����� �����������
    class function IsMethodAbstract(Method: Pointer): Boolean;
    // ���� ����������� ������ � VMT ��������� ������. ���������� ������,
    // ���� ���� ���� ������. ������ ��������� ������� ������������ � Abstracts
    class function DetectAbstracts(AClass: TClass; out Abstracts: TMIRArray): boolean;
    // ������� ����������, ���� � ������ ���� ����������� ������
    class procedure AssertNonAbstract(AClass: TClass);
  end;


implementation
uses SysUtils, TypInfo, Windows;
var
  AbsProc: Pointer;
type PPointer = ^Pointer;

class function TAbstractHandler.AbsProcAddress: Pointer;
var
  TAP: procedure of object;
begin
  if not Assigned(AbsProc)
  then begin
    TAP:= self.AbstractProc;
    AbsProc:= UnThunkImport(TMethod(TAP).Code);
  end;
  Result:= AbsProc;
end;

class procedure TAbstractHandler.AssertNonAbstract(AClass: TClass);
var
  Abstracts: TMIRArray;
begin
   if DetectAbstracts(AClass, Abstracts)
    then raise EAbstractError.CreateFMT('Class %s (Unit: %s; package: %s) contains the following abstract methods:'#10#13
     +FormatAbstractInfos(Abstracts), [AClass.ClassName, GetClassUnitName(AClass), GetClassPackageName(AClass)]);
end;

// ���� ����� ��������� ������� � VMT
function GetVMTEnd(AClass: TClass): Pointer;
var
  VMT, Start, Finish: PPointer;
begin
  TClass(VMT):= AClass;
  Start:= VMT; Inc(Start, vmtIntfTable shr 2);
  Finish:= VMT; Inc(Finish,vmtClassName shr 2);
  Result:= Ptr($7FFFFFFF);

  while Integer(Start) <= Integer(Finish) do
  begin
    if (Integer(Start^)>Integer(VMT)) and (Integer(Start^) < Integer(Result))
      then Result:=Start^;
    Inc(Start);
  end;
end;
// ���� �����-������, � ������� ������� �������� �������� ���� VMT
class function TAbstractHandler.GetFirstDeclarator(AClass: TClass; VMTIndex: integer): TClass;
var
  VMTEntry: PPointer;
begin
  Result:= AClass;
  while True do
  begin
    TClass(VMTEntry):= Result.ClassParent;
    Inc(VMTEntry, VMTIndex);
    // ���� � ������ ���� ���� ��� ��������, �� �� ����
    // �������� AbsProcAddress:
    if (VMTEntry^)=AbsProcAddress
      then Result:= Result.ClassParent
      else Exit;
  end;
end;

class function TAbstractHandler.DetectAbstracts(AClass: TClass;
  out Abstracts: TMIRArray): boolean;
var
  VMT: PPointer;
  VMTEnd: Pointer;
begin
  TClass(VMT):= AClass;
  VMTEnd:=GetVMTEnd(AClass);

  SetLength(Abstracts, 0);
  while (VMT<>VMTEnd) // ��������� VMT
  do begin
    if IsMethodAbstract(VMT^)
    then begin
      SetLength(Abstracts, Length(Abstracts)+1); // ��������� ������
      with Abstracts[High(Abstracts)] do
      begin
        VMTIndex:= (Integer(VMT)-Integer(AClass)) shr 2; // ������ ����� - 4 �����
        ClassType:= GetFirstDeclarator(AClass, VMTIndex);
      end;
    end;
    Inc(VMT);
  end;
  Result:= Length(Abstracts)>0; // �������������, ������� �� �����.
end;

class function TAbstractHandler.FormatAbstractInfos(
  const Abstracts: array of TMethodInfoRec;
  const FormatStr: String): String;
var
  i: integer;
begin
  Result:='';
  for i:= Low(Abstracts) to High(Abstracts) do
    with Abstracts[i] do
      Result:= Result+Format(FormatStr, [ClassType.ClassName, VMTIndex]);
end;
// ��� ���������� ��������� �������������� ���� ����������� �������
procedure TAbstractHandler.HandleAbstract;
begin
  AssertNonAbstract(ClassType);
end;

class function TAbstractHandler.IsMethodAbstract(Method: Pointer): Boolean;
begin
  result:= UnThunkImport(Method)=AbsProcAddress;
end;

class function TAbstractHandler.UnThunkImport(Addr: pointer): pointer;
begin
  Result:=Addr;
  if Word(Addr^) = $25FF // ��� ������� ���������� jmp
    then Result:= PPointer(PPointer(Integer(Addr)+2)^)^;
end;

class function TAbstractHandler.GetClassPackageName(
  AClass: TClass): String;
var
  M: TMemoryBasicInformation;
begin
  // ���������� ����� DLL, ������� ������� �������
  VirtualQuery(AClass, M, sizeof(M));
  SetLength(Result, MAX_PATH+1);
  if HMODULE(M.AllocationBase) <> HInstance // ���� ��� �� ������� ���������
  then begin
    GetModuleFileName(HMODULE(M.AllocationBase), PChar(Result), MAX_PATH);
    SetLength(Result, StrLen(Pchar(Result)));
    Result:= ExtractFileName(Result);
  end
  else
    Result:= 'Main Program';
end;

class function TAbstractHandler.GetClassUnitName(AClass: TClass): String;
var
  C: Pointer;
begin
  Result:= 'Unknown';
  C:= AClass.ClassInfo;
  if Assigned(C)
    then Result:= GetTypeData(C).UnitName;
end;

initialization
  AbsProc:= Nil; // ��� ���������� ����������� ��� �������������� �����������
  // ������������� ��� ����������:
  AbstractErrorProc:= Addr(TAbstractHandler.HandleAbstract);
  // �������������� ��������� - ����� ����� �����
  TAbstractHandler.AbsProcAddress;
end.