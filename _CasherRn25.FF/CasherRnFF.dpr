// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program CasherRnFF;

uses
  Windows,
  Forms,
  MainFF in 'MainFF.pas' {fmMainFF},
  Un1 in 'Un1.pas',
  Dm in 'Dm.pas' {dmC: TDataModule},
  UnCash in 'UnCash.pas',
  PreFF in 'PreFF.pas' {fmPreFF},
  ModifFF in 'ModifFF.pas' {fmModifFF},
  CashEnd in 'CashEnd.pas' {fmCash},
  Calc in 'Calc.pas' {fmCalc},
  Attention in 'Attention.pas' {fmAttention},
  CredCards in 'CredCards.pas' {fmCredCards},
  UnitBN in 'UnitBN.pas',
  fmDiscountShape in 'fmDiscountShape.pas' {fmDiscount_Shape},
  UnDP2300 in 'UnDP2300.pas',
  u2fdk in 'U2FDK.PAS',
  BnSber in 'BnSber.pas' {fmSber},
  TabsFF in 'TabsFF.pas' {fmTabsFF},
  PreFF1 in 'PreFF1.pas' {fmPre_Shape},
  uDB1 in 'uDB1.pas' {dmC1: TDataModule},
  PCardList in 'PCardList.pas' {fmPCardList},
  prdb in 'prdb.pas' {dmPC: TDataModule},
  RaschotWarn in 'RaschotWarn.pas' {fmRaschotWarn},
  uDMTR in 'uDMTR.pas' {dmTR: TDataModule},
  Shutd in 'Shutd.pas' {fmShutd},
  UnInOutM in 'UnInOutM.pas' {fmInOutM},
  CopyCh in 'CopyCh.pas' {fmCopyCh},
  UnPrizma in 'UnPrizma.pas',
  Vtb24un in 'Vtb24un.pas' {fmVtb},
  MessBN in 'MessBN.pas' {fmMessBn},
  AddPodnos in 'AddPodnos.pas' {fmAddP},
  SelOrg in 'SelOrg.pas' {fmSelOrg},
  Modif in 'Modif.pas' {fmModif};

{$R *.res}

Const AppID='SoftUr';

Var Handle:THandle;

begin
  // ������� � ���������� ������ 1-�������� "����" � ����������
  // ������ AppID, ���������� ��� � ���� �������� ������������
  // � ���������, ��� �� �� ������ ��� ������ ������.
  Handle:=CreateFileMapping($FFFFFFFF,Nil,PAGE_READONLY,0,1,AppID);
  If GetLastError=ERROR_ALREADY_EXISTS then MessageBox(0,'������ ������ ����� ��������� ����������.',AppID,MB_OK or MB_ICONSTOP or MB_TOPMOST)
  else
  begin
    Application.Initialize;
    Application.CreateForm(TfmMainFF, fmMainFF);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmPreFF, fmPreFF);
  Application.CreateForm(TfmCash, fmCash);
  Application.CreateForm(TfmCalc, fmCalc);
  Application.CreateForm(TfmAttention, fmAttention);
  Application.CreateForm(TfmDiscount_Shape, fmDiscount_Shape);
  Application.CreateForm(TfmTabsFF, fmTabsFF);
  Application.CreateForm(TfmPre_Shape, fmPre_Shape);
  Application.CreateForm(TdmC1, dmC1);
  Application.CreateForm(TfmPCardList, fmPCardList);
  Application.CreateForm(TdmPC, dmPC);
  Application.CreateForm(TfmRaschotWarn, fmRaschotWarn);
  Application.CreateForm(TdmTR, dmTR);
  Application.CreateForm(TfmShutd, fmShutd);
  Application.CreateForm(TfmInOutM, fmInOutM);
  Application.CreateForm(TfmCopyCh, fmCopyCh);
  Application.CreateForm(TfmVtb, fmVtb);
  Application.CreateForm(TfmSber, fmSber);
  Application.CreateForm(TfmMessBn, fmMessBn);
  Application.CreateForm(TfmAddP, fmAddP);
  Application.CreateForm(TfmSelOrg, fmSelOrg);
  Application.Run;
  end;
  // ����������� ������ � ��� ����� ��������� ��������� ������.
  CloseHandle(Handle)
end.
