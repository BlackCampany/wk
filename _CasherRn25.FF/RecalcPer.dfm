object fmRecalcPer: TfmRecalcPer
  Left = 425
  Top = 185
  BorderStyle = bsDialog
  Caption = #1055#1077#1088#1077#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1079#1072' '#1087#1077#1088#1080#1086#1076
  ClientHeight = 446
  ClientWidth = 369
  Color = 16728736
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 61
    Height = 13
    Caption = #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1089
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 427
    Width = 369
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object cxDateEdit1: TcxDateEdit
    Left = 80
    Top = 12
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 113
  end
  object cxButton1: TcxButton
    Left = 200
    Top = 10
    Width = 73
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 2
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object Memo1: TcxMemo
    Left = 8
    Top = 68
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 349
    Width = 353
  end
  object cxButton2: TcxButton
    Left = 288
    Top = 10
    Width = 73
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 4
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 16
    Top = 40
    Caption = #1089' '#1082#1086#1085#1090#1088#1086#1083#1077#1084' '#1087#1072#1088#1090#1080#1086#1085#1085#1086#1075#1086' '#1091#1095#1077#1090#1072
    TabOrder = 5
    Transparent = True
    Width = 201
  end
  object prDelPer: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_DELPER (?IDATE)')
    StoredProcName = 'PR_DELPER'
    Left = 48
    Top = 72
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trS: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 136
  end
  object trD: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 192
  end
  object trU: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 248
  end
  object prTestPart: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCPARTREMN ')
    StoredProcName = 'PR_RECALCPARTREMN'
    Left = 120
    Top = 72
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDIn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADIN'
      'SET '
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADIN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADIN('
      '    ID,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,IDCLI,IDSKL,SUMIN,SUMUCH,SUMTAR,SUMNDS0,SUMNDS1,SUMNDS' +
        '2,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADIN'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADIN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,IDCLI,IDSKL,SUMIN,SUMUCH,SUMTAR,SUMNDS0,SUMNDS1,SUMNDS' +
        '2,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADIN'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 72
    object quDInID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDInIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDInIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDInSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDInSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDInSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDInSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDInSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDInSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDInPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDInIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quDCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER'
      'FROM OF_DOCHEADCOMPL'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADCOMPL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER'
      'FROM OF_DOCHEADCOMPL'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      ''
      ''
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 128
    object quDComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDComplDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDComplNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDComplIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDComplSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDComplSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDComplPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDComplIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDComplOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
  end
  object quDAct: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,IACTIVE,OPER,COMMENT'
      'FROM OF_DOCHEADACTS '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADACTS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,IACTIVE,OPER,COMMENT'
      'FROM OF_DOCHEADACTS '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 184
    object quDActID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDActDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDActNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDActIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDActSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDActSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDActIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDActOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDActCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quDVn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL_FROM,IDSKL_TO,SUMIN,SUMUCH,SUMUCH' +
        '1,SUMTAR,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADVN '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADVN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL_FROM,IDSKL_TO,SUMIN,SUMUCH,SUMUCH' +
        '1,SUMTAR,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADVN '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 240
    object quDVnID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDVnDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDVnNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDVnIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDVnIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDVnSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDVnSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDVnSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quDVnSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDVnPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDVnIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quDRet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUT'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUT('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,S' +
        'UMTAR,SUMNDS0,'
      '    SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUT '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,S' +
        'UMTAR,SUMNDS0,'
      '    SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUT '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 296
    object quDRetID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDRetDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDRetNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRetDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDRetNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRetIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDRetIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDRetSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDRetSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDRetSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDRetSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDRetSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDRetSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDRetPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDRetIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quDOutB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTB'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTB('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      'FROM'
      '    OF_DOCHEADOUTB'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTB.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      'FROM'
      '    OF_DOCHEADOUTB'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by OPER desc'
      ''
      ' ')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 352
    object quDOutBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDOutBDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDOutBNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDOutBDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDOutBNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDOutBIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDOutBIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDOutBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDOutBSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDOutBSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDOutBSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDOutBSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDOutBSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDOutBPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDOutBIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDOutBOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
  end
  object quDInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      'FROM'
      '    OF_DOCHEADINV '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADINV.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      'FROM'
      '    OF_DOCHEADINV '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 280
    Top = 72
    object quDInvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDInvDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDInvNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDInvIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDInvIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDInvITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDInvSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quDInvSUM11: TFIBFloatField
      FieldName = 'SUM11'
    end
    object quDInvSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quDInvSUM21: TFIBFloatField
      FieldName = 'SUM21'
    end
  end
  object prSetSpecActive: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_SETSPECACTIVE (?DDATE, ?ATYPE)')
    StoredProcName = 'PR_SETSPECACTIVE'
    Left = 120
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDR: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 280
    Top = 128
    object quDRID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDRDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDRNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDRNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDRIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDRSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDRSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDRSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDRSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDRSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDRSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDRPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDRIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID'
      'FROM OF_CARDS'
      'order by ID')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 36
    Top = 356
    poAskRecordCount = True
    object quCID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object PRNORMPARTIN: TpFIBStoredProc
    Transaction = trU
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT' +
        ')')
    StoredProcName = 'PR_NORMPARTIN'
    Left = 124
    Top = 320
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quMHAll1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = trS1
    Database = dmO.OfficeRnDb
    Left = 124
    Top = 264
    object quMHAll1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAll1PARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAll1ITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAll1NAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAll1DEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAll1NAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object trS1: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 36
    Top = 304
  end
end
