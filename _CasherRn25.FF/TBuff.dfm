object fmTBuff: TfmTBuff
  Left = 338
  Top = 136
  BorderStyle = bsDialog
  Caption = #1041#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
  ClientHeight = 354
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GridTH: TcxGrid
    Left = 8
    Top = 8
    Width = 369
    Height = 337
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewTH: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = fmTCard.dsTCH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewTHDATEB: TcxGridDBColumn
        Caption = 'C'
        DataBinding.FieldName = 'DATEB'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 42
      end
      object ViewTHDATEE: TcxGridDBColumn
        Caption = #1087#1086
        DataBinding.FieldName = 'DATEE'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 62
      end
      object ViewTHSHORTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'SHORTNAME'
        Width = 223
      end
      object ViewTHRECEIPTNUM: TcxGridDBColumn
        Caption = #8470' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
        DataBinding.FieldName = 'RECEIPTNUM'
        Width = 79
      end
      object ViewTHPOUTPUT: TcxGridDBColumn
        Caption = #1042#1099#1093#1086#1076' 1 '#1087#1086#1088#1094#1080#1080' '#1087#1086' '#1084#1077#1085#1102
        DataBinding.FieldName = 'POUTPUT'
        Width = 55
      end
      object ViewTHPCOUNT: TcxGridDBColumn
        Caption = #1053#1072' '#1082#1086#1083'-'#1074#1086' '#1087#1086#1088#1094#1080#1081
        DataBinding.FieldName = 'PCOUNT'
      end
      object ViewTHPVES: TcxGridDBColumn
        Caption = #1052#1072#1089#1089#1072' 1 '#1087#1086#1088#1094#1080#1080
        DataBinding.FieldName = 'PVES'
      end
    end
    object ViewTS: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = fmTCard.dsTCS
      DataController.DetailKeyFieldNames = 'IDT'
      DataController.KeyFieldNames = 'ID'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewTSIDC: TcxGridDBColumn
        DataBinding.FieldName = 'IDC'
        Visible = False
      end
      object ViewTSIDT: TcxGridDBColumn
        DataBinding.FieldName = 'IDT'
        Visible = False
      end
      object ViewTSID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object ViewTSIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCARD'
      end
      object ViewTSCURMESSURE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CURMESSURE'
      end
      object ViewTSNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO'
      end
      object ViewTSBRUTTO: TcxGridDBColumn
        Caption = #1041#1088#1091#1090#1090#1086
        DataBinding.FieldName = 'BRUTTO'
      end
      object ViewTSKNB: TcxGridDBColumn
        DataBinding.FieldName = 'KNB'
        Visible = False
      end
    end
    object LevelTH: TcxGridLevel
      GridView = ViewTH
      object GridTHLevelTS: TcxGridLevel
        GridView = ViewTS
      end
    end
  end
  object cxButton1: TcxButton
    Left = 392
    Top = 16
    Width = 81
    Height = 25
    Caption = #1042#1089#1090#1072#1074#1080#1090#1100
    ModalResult = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 392
    Top = 56
    Width = 81
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 392
    Top = 304
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 392
    Top = 272
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 4
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
end
