unit CurMessure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfmCurMessure = class(TForm)
    VM: TcxGridDBTableView;
    LM: TcxGridLevel;
    GM: TcxGrid;
    VMID: TcxGridDBColumn;
    VMID_PARENT: TcxGridDBColumn;
    VMNAMESHORT: TcxGridDBColumn;
    VMKOEF: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure VMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCurMessure: TfmCurMessure;

implementation

uses dmOffice;

{$R *.dfm}

procedure TfmCurMessure.FormCreate(Sender: TObject);
begin
  GM.Align:=AlClient;
end;

procedure TfmCurMessure.VMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

end.
