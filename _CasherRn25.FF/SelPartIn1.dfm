object fmPartIn1: TfmPartIn1
  Left = 283
  Top = 199
  Width = 504
  Height = 389
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 302
    Width = 496
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton2: TcxButton
      Left = 208
      Top = 16
      Width = 83
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 65
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 24
      Top = 40
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 120
      Top = 16
      Width = 39
      Height = 13
      Caption = 'Label5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 120
      Top = 40
      Width = 39
      Height = 13
      Caption = 'Label6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GrPartIn: TcxGrid
    Left = 8
    Top = 88
    Width = 473
    Height = 145
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewPartIn: TcxGridDBTableView
      OnDblClick = ViewPartInDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsPartTest
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QPART'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QREMN'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewPartInNum: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087
        DataBinding.FieldName = 'Num'
        Width = 31
      end
      object ViewPartInIdGoods: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'IdGoods'
        Width = 50
      end
      object ViewPartInNameG: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameG'
        Width = 123
      end
      object ViewPartInIM: TcxGridDBColumn
        DataBinding.FieldName = 'IM'
        Visible = False
      end
      object ViewPartInSM: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084
        DataBinding.FieldName = 'SM'
        Width = 52
      end
      object ViewPartInQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
      end
      object ViewPartInPrice1: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price1'
      end
      object ViewPartIniRes: TcxGridDBColumn
        Caption = #1055#1072#1088#1090#1080#1103
        DataBinding.FieldName = 'iRes'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            ImageIndex = 35
            Value = 0
          end
          item
            ImageIndex = 36
            Value = 1
          end>
      end
    end
    object LevelPartIn: TcxGridLevel
      GridView = ViewPartIn
    end
  end
end
