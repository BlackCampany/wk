object fmCashFBLoad: TfmCashFBLoad
  Left = 872
  Top = 274
  BorderStyle = bsDialog
  Caption = #1055#1086#1083#1085#1072#1103' '#1087#1088#1086#1075#1088#1091#1079#1082#1072' FB '#1082#1072#1089#1089
  ClientHeight = 340
  ClientWidth = 449
  Color = 16758380
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 24
    Width = 55
    Height = 13
    Caption = #1055#1091#1090#1100' '#1082' '#1041#1044' '
    Transparent = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 88
    Top = 20
    Style.Shadow = True
    TabOrder = 0
    Text = 'localhost:C:\_CasherFb\DB\CASHERDB.GDB'
    Width = 293
  end
  object cxCheckBox1: TcxCheckBox
    Left = 28
    Top = 52
    Caption = #1058#1086#1083#1100#1082#1086' '#1072#1082#1090#1080#1074#1085#1099#1077
    State = cbsChecked
    TabOrder = 1
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 16
    Top = 132
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    Height = 193
    Width = 225
  end
  object cxProgressBar1: TcxProgressBar
    Left = 16
    Top = 100
    ParentColor = False
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clGreen
    Properties.EndColor = 9830293
    Style.BorderStyle = ebsOffice11
    Style.Color = clWhite
    Style.Shadow = True
    TabOrder = 3
    Width = 225
  end
  object cxButton1: TcxButton
    Left = 272
    Top = 100
    Width = 149
    Height = 45
    Caption = #1055#1091#1089#1082
    TabOrder = 4
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 272
    Top = 156
    Width = 149
    Height = 45
    Caption = #1057#1090#1086#1087
    TabOrder = 5
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 272
    Top = 276
    Width = 149
    Height = 45
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 6
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox2: TcxCheckBox
    Left = 28
    Top = 68
    Caption = '+ '#1040#1083#1082#1086#1087#1086#1083#1103
    State = cbsChecked
    TabOrder = 7
    Width = 121
  end
end
