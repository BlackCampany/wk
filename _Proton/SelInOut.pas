unit SelInOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxTextEdit, cxMaskEdit, cxButtonEdit, cxControls, cxContainer,
  cxEdit, cxLabel, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ExtCtrls;

type
  TfmInOutSel = class(TForm)
    cxLabel5: TcxLabel;
    cxButtonEdit2: TcxButtonEdit;
    cxLabel1: TcxLabel;
    cxButtonEdit1: TcxButtonEdit;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInOutSel: TfmInOutSel;

implementation

uses Un1;

{$R *.dfm}

procedure TfmInOutSel.FormCreate(Sender: TObject);
begin
  cxButtonEdit1.Text:=CurDir+'scanout.dat';
  cxButtonEdit2.Text:=CurDir+'Inventry.dat';
end;

procedure TfmInOutSel.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog2.Execute;
  cxButtonEdit2.Text:=OpenDialog2.FileName;
end;

procedure TfmInOutSel.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

end.
