unit RVed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmRVed = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridRVed: TcxGrid;
    LevelRVed: TcxGridLevel;
    dsObVed: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepOb: TfrReport;
    frtaObVed: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    teRVed: TdxMemData;
    teRVedIdCode1: TIntegerField;
    teRVedNameC: TStringField;
    teRVediM: TSmallintField;
    teRVedIdGroup: TIntegerField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teRVedNameGr1: TStringField;
    teRVedNameGr2: TStringField;
    teRVedIdSGroup: TIntegerField;
    ViewRVed: TcxGridDBTableView;
    ViewRVedRecId: TcxGridDBColumn;
    ViewRVedIdCode1: TcxGridDBColumn;
    ViewRVedNameC: TcxGridDBColumn;
    ViewRVediM: TcxGridDBColumn;
    ViewRVedIdGroup: TcxGridDBColumn;
    ViewRVedIdSGroup: TcxGridDBColumn;
    ViewRVedQR: TcxGridDBColumn;
    ViewRVedNameGr1: TcxGridDBColumn;
    ViewRVedNameGr2: TcxGridDBColumn;
    teRVediBrand: TIntegerField;
    teRVediCat: TSmallintField;
    ViewRVediCat: TcxGridDBColumn;
    amRepOb: TActionManager;
    acMoveRepOb: TAction;
    teRVedNameGr3: TStringField;
    ViewRVedNameGr3: TcxGridDBColumn;
    teRVedQR: TFloatField;
    teRVedPrice: TFloatField;
    ViewRVedPrice: TcxGridDBColumn;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teRVedsTop: TStringField;
    teRVedsNov: TStringField;
    ViewRVedsTop: TcxGridDBColumn;
    ViewRVedsNov: TcxGridDBColumn;
    teRVedRSum: TFloatField;
    ViewRVedRSum: TcxGridDBColumn;
    teRVedDayN: TIntegerField;
    ViewRVedDayN: TcxGridDBColumn;
    teRVedQRemVoz: TFloatField;
    ViewRVedQRemVoz: TcxGridDBColumn;
    teRVedQNVoz: TFloatField;
    ViewRVedQNVoz: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewRVedSelectionChanged(Sender: TcxCustomGridTableView);
    procedure teRVedCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRVed: TfmRVed;

implementation

uses Un1, MDB, MT, Move, dmPS;

{$R *.dfm}

procedure TfmRVed.FormCreate(Sender: TObject);
begin
  GridRVed.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewRVed.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmRVed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewRVed.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  teRVed.Active:=False;
end;

procedure TfmRVed.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRVed.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRVed.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewRVed);
end;

procedure TfmRVed.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewRVed.BeginUpdate;
  teRVed.Filter:=ViewRVed.DataController.Filter.FilterText;
  teRVed.Filtered:=True;

  frRepOb.LoadFromFile(CurDir + 'RVed.frf');

  frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

//  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewRVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepOb.ReportName:='��������� ���������.';
  frRepOb.PrepareReport;
  frRepOb.ShowPreparedReport;

  teRVed.Filter:='';
  teRVed.Filtered:=False;
  ViewRVed.EndUpdate;
end;

procedure TfmRVed.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepRVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmRVed.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRVed.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmRVed.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewRVed.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teRVedIdCode1.AsInteger;
      prFillMove(teRVedIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teRVedNameC.AsString+'('+teRVedIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmRVed.ViewRVedSelectionChanged(Sender: TcxCustomGridTableView);
begin
  if ViewRVed.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teRVedIdCode1.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmRVed.teRVedCalcFields(DataSet: TDataSet);
begin
  teRVedRSum.AsFloat:=rv(teRVedQR.AsFloat*teRVedPrice.AsFloat);

  with dmP do
  begin
    quAllRem.Active:=false;
    quAllRem.ParamByName('CODE').AsInteger:= teRVedIdCode1.AsInteger;
    quAllRem.Active:=true;
    teRVedQRemVoz.AsFloat:=quAllRemQRem.AsFloat;
    quAllRem.Active:=false;
    teRVedQNVoz.AsFloat:=teRVedQR.AsFloat-teRVedQRemVoz.AsFloat;
  end;

end;

end.
