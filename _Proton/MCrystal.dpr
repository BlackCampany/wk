// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program MCrystal;
uses
  Forms,
  MainMCryst in 'MainMCryst.pas' {fmMainMCryst},
  MDB in 'MDB.pas' {dmMC: TDataModule},
  Un1 in 'Un1.pas',
  PasswMainCard in 'PasswMainCard.pas' {fmPerA},
  mCards in 'mCards.pas' {fmCards},
  mCountry in 'mCountry.pas' {fmCountry},
  AddSingle in 'AddSingle.pas' {fmAddSingle},
  mBrands in 'mBrands.pas' {fmBrands},
  mMakers in 'mMakers.pas' {fmMakers},
  mDeparts in 'mDeparts.pas' {fmDeparts},
  mEu in 'mEu.pas' {fmEU},
  AddEU in 'AddEU.pas' {fmAddEU},
  mAddCard in 'mAddCard.pas' {fmAddCard},
  mFind in 'mFind.pas' {fmFindC},
  u2fdk in 'U2FDK.PAS',
  AddClassif in 'AddClassif.pas' {fmAddClassif},
  Bar in 'Bar.pas' {fmBar},
  ClasSel in 'ClasSel.pas' {fmClassSel},
  DocsIn in 'DocsIn.pas' {fmDocs1},
  Period1 in 'Period1.pas' {fmPeriod1},
  AddDoc1 in 'AddDoc1.pas' {fmAddDoc1},
  Clients in 'Clients.pas' {fmClients},
  mTara in 'mTara.pas' {fmTara},
  DocsOut in 'DocsOut.pas' {fmDocs2},
  AddCli in 'AddCli.pas' {fmAddCli},
  AddDoc2 in 'AddDoc2.pas' {fmAddDoc2},
  sumprops in 'SummaProp\Sumprops.pas',
  DocsAC in 'DocsAC.pas' {fmDocs3},
  AddDoc3 in 'AddDoc3.pas' {fmAddDoc3},
  DocsVn in 'DocsVn.pas' {fmDocs4},
  AddDoc4 in 'AddDoc4.pas' {fmAddDoc4},
  BufPrice in 'BufPrice.pas' {fmBufPrice},
  PXDB in 'PXDB.pas' {dmPx: TDataModule},
  CashList in 'CashList.pas' {fmCashList},
  Status in 'Status.pas' {fmSt},
  Remn in 'Remn.pas' {fmRemn},
  Move in 'Move.pas' {fmMove},
  CashRep in 'CashRep.pas' {fmCashRep},
  Gens in 'Gens.pas' {fmGens},
  ToRep in 'ToRep.pas' {fmTORep},
  CG in 'CG.pas' {fmCG},
  Period in 'Period.pas' {fmPeriod},
  ObMove in 'ObMove.pas' {fmObVed},
  DocsInv in 'DocsInv.pas' {fmDocs5},
  AddDoc5 in 'AddDoc5.pas' {fmAddDoc5},
  TermLoad in 'TermLoad.pas' {fmMainLoader},
  InvExport in 'InvExport.pas' {fmInvExport},
  Labels in 'Labels.pas' {fmLabels},
  TermoPr in 'TermoPr.pas' {fmTermoPr},
  QuantMess in 'QuantMess.pas' {fmQuant},
  ScaleSpr in 'ScaleSpr.pas' {fmScaleSpr},
  Scales in 'Scales.pas' {fmScale},
  Func in 'Func.pas',
  AddInScale in 'AddInScale.pas' {fmAddInScale},
  EditPlu in 'EditPlu.pas' {fmEditPlu},
  DepPars in 'DepPars.pas' {fmDepPars},
  MT in 'MT.pas' {dmMT: TDataModule},
  Corr in 'Corr.pas' {fmCorr},
  TBuff in 'TBuff.pas' {fmTBuff},
  CashDep in 'CashDep.pas' {fmCashDep},
  Receipt in 'Receipt.pas' {fmReceipt},
  RepReal in 'RepReal.pas' {fmRepReal},
  Checks in 'Checks.pas' {fmChecks},
  CorrDoc in 'CorrDoc.pas' {fmDocCorr},
  ObMoveTara in 'ObMoveTara.pas' {fmObVedTara},
  DStop in 'DStop.pas' {fmDStop},
  CliPars in 'CliPars.pas' {fmCliPars},
  RVed in 'RVed.pas' {fmRVed},
  PeriodR in 'PeriodR.pas' {fmPeriodR},
  BarcodeNo in 'BarcodeNo.pas' {fmBarMessage},
  RepTovarPeriod in 'RepTovarPeriod.pas' {fmRepTovarPeriod},
  uCateg in 'uCateg.pas' {fmCateg},
  uAddDouble in 'uAddDouble.pas' {fmAddDouble},
  RepRDay in 'RepRDay.pas' {fmRDayVed},
  RepHour in 'RepHour.pas' {fmRealH},
  uGraf in 'uGraf.pas' {fmGraf1},
  uGraf2 in 'uGraf2.pas' {fmGraf2},
  uGraf3 in 'uGraf3.pas' {fmGraf3},
  ChangeT in 'ChangeT.pas' {fmChangeT},
  Zadan in 'Zadan.pas' {fmZadan},
  ZadMessge in 'ZadMessge.pas' {fmZadMessage},
  GdsLoad in 'GdsLoad.pas' {fmGdsLoad},
  MFB in 'MFB.pas' {dmFB: TDataModule},
  Prib1 in 'Prib1.pas' {fmPrib1},
  SortPar in 'SortPar.pas' {fmSortF},
  Akciya in 'Akciya.pas' {fmAkciya},
  DocObm in 'DocObm.pas' {fmDocs6},
  AddDoc6 in 'AddDoc6.pas' {fmAddDoc6},
  DocZ in 'DocZ.pas' {fmDocs7},
  ChangeB in 'ChangeB.pas' {fmChangeB},
  Parts in 'Parts.pas' {fmParts},
  DocsVnSel in 'DocsVnSel.pas' {fmSelTVn},
  uTermElisey in 'uTermElisey.pas' {Service1: TService},
  CheckEdit in 'CheckEdit.pas' {fmCheckEdit},
  SelInOut in 'SelInOut.pas' {fmInOutSel},
  DepSel in 'DepSel.pas' {fmDepSel},
  OborRep in 'OborRep.pas' {fmOborRep},
  SetCardsParams in 'SetCardsParams.pas' {fmSetCardsParams},
  AssortPost in 'AssortPost.pas' {fmAssPost},
  AddDoc7 in 'AddDoc7.pas' {fmAddDoc7},
  SetStatusBZ in 'SetStatusBZ.pas' {fmSetStatus},
  CorrTO in 'CorrTO.pas' {fmCorrTO},
  SetNewDate in 'SetNewDate.pas' {fmSetNewDate},
  ChangePost in 'ChangePost.pas' {fmChangePost},
  RDayVedWide in 'RDayVedWide.pas' {fmRDayVedWide},
  AvSpeed in 'AvSpeed.pas' {fmAvSpeed},
  CashFBLoad in 'CashFBLoad.pas' {fmCashFBLoad},
  ChangeT1 in 'ChangeT1.pas' {fmChangeT1},
  Cassirs in 'Cassirs.pas' {fmCassirs},
  PrintAddDocs in 'PrintAddDocs.pas' {fmPrintAdd},
  ZakHPost in 'ZakHPost.pas' {fmZakHPost},
  GrafPost in 'GrafPost.pas' {fmGrafPost},
  GrafPostavok in 'GrafPostavok.pas' {fmGrafPostavok},
  PreInvView in 'PreInvView.pas' {fmPreInvView},
  RepVoz in 'RepVoz.pas' {fmRepVoz},
  CatDisc in 'CatDisc.pas' {fmCatDisc},
  AddMaker in 'AddMaker.pas' {fmAddMaker},
  AddCliLic in 'AddCliLic.pas' {fmAddCliLic},
  PreAlg in 'PreAlg.pas' {fmPreAlg},
  RepAlg in 'RepAlg.pas' {fmAlg},
  AlgClass in 'AlgClass.pas' {fmAlgClass},
  Kadr in 'Kadr.pas' {fmKadr},
  AddCassir in 'AddCassir.pas' {fmAddCassir},
  DocHVoz in 'DocHVoz.pas' {fmDocs8},
  dmPS in 'dmPS.pas' {dmP},
  ReasonsV in 'ReasonsV.pas' {fmReasonVoz},
  AddDoc8 in 'AddDoc8.pas' {fmAddDoc8},
  ResTest in 'ResTest.pas' {fmRes},
  AlcoHd in 'AlcoHd.pas' {fmAlcoE},
  DBAlg in 'DBAlg.pas' {dmAlg: TDataModule},
  AlcoSp in 'AlcoSp.pas' {fmAddAlco},
  DelayV in 'DelayV.pas' {fmDelayV},
  PostGd in 'PostGd.pas' {fmPD},
  TypeVoz in 'TypeVoz.pas' {fmTypeVoz},
  InvVoz in 'InvVoz.pas' {fmInvVoz},
  PostGood in 'PostGood.pas' {fmP},
  GrafVozvrat in 'GrafVozvrat.pas' {fmGrafVozvrat},
  SetGTD in 'SetGTD.pas' {fmSetGTD},
  SetTypeDocOut in 'SetTypeDocOut.pas' {fmSetTypeDocOut},
  AltPost in 'AltPost.pas' {fmAltPost},
  SetMatrix in 'SetMatrix.pas' {fmSetMatrix},
  SpInEr in 'SpInEr.pas' {fmSpInEr},
  ObrPredz in 'ObrPredz.pas' {fmObrPredz},
  ShowPost in 'ShowPost.pas' {fmShowPost},
  SelDep in 'SelDep.pas' {fmSelDep},
  DateP in 'DateP.pas' {fmDateP},
  FindKassir in 'FindKassir.pas' {fmFindKassir},
  SInvVoz in 'SInvVoz.pas' {fmSInvVoz},
  SpecInvVoz in 'SpecInvVoz.pas' {fmSpecInvVoz},
  AddCash in 'AddCash.pas' {fmAddCash},
  HistoryOper in 'HistoryOper.pas' {fmHistOp},
  CliLicense in 'CliLicense.pas' {fmCliLic},
  dbe in 'dbe.pas' {dmE: TDataModule},
  UpLica in 'UpLica.pas' {fmUpLica},
  AddUPL in 'AddUPL.pas' {fmAddUPL},
  SyncCash in 'SyncCash.pas' {fmSyncCash},
  dmFbPro in 'dmFbPro.pas' {dmPro: TDataModule},
  PrintInSCHF in 'PrintInSCHF.pas' {fmPrintInSCHF},
  SelPost in 'SelPost.pas' {fmSelPost},
  PreRealAlco in 'PreRealAlco.pas' {fmPreRealAlco},
  RepRealAP in 'RepRealAP.pas' {fmRepSaleAP},
  LoadCashAVid in 'LoadCashAVid.pas' {fmCashFBLoadAVid};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainMCryst, fmMainMCryst);
  Application.CreateForm(TfmDocCorr, fmDocCorr);
  Application.CreateForm(TfmCashDep, fmCashDep);
  Application.CreateForm(TfmObVedTara, fmObVedTara);
  Application.CreateForm(TdmMC, dmMC);
  Application.CreateForm(TdmMT, dmMT);
  Application.CreateForm(TfmCards, fmCards);
  Application.CreateForm(TfmCountry, fmCountry);
  Application.CreateForm(TfmAddSingle, fmAddSingle);
  Application.CreateForm(TfmBrands, fmBrands);
  Application.CreateForm(TfmDeparts, fmDeparts);
  Application.CreateForm(TfmEU, fmEU);
  Application.CreateForm(TfmAddEU, fmAddEU);
  Application.CreateForm(TfmAddCard, fmAddCard);
  Application.CreateForm(TfmFindC, fmFindC);
  Application.CreateForm(TfmBar, fmBar);
  Application.CreateForm(TfmClassSel, fmClassSel);
  Application.CreateForm(TfmDocs1, fmDocs1);
  Application.CreateForm(TfmPeriod1, fmPeriod1);
  Application.CreateForm(TfmAddDoc1, fmAddDoc1);
  Application.CreateForm(TfmClients, fmClients);
  Application.CreateForm(TfmTara, fmTara);
  Application.CreateForm(TfmDocs2, fmDocs2);
  Application.CreateForm(TfmAddCli, fmAddCli);
  Application.CreateForm(TfmAddDoc2, fmAddDoc2);
  Application.CreateForm(TfmDocs3, fmDocs3);
  Application.CreateForm(TfmAddDoc3, fmAddDoc3);
  Application.CreateForm(TfmDocs4, fmDocs4);
  Application.CreateForm(TfmAddDoc4, fmAddDoc4);
  Application.CreateForm(TfmBufPrice, fmBufPrice);
  Application.CreateForm(TdmPx, dmPx);
  Application.CreateForm(TfmCashList, fmCashList);
  Application.CreateForm(TfmSt, fmSt);
  Application.CreateForm(TfmRemn, fmRemn);
  Application.CreateForm(TfmMove, fmMove);
  Application.CreateForm(TfmCashRep, fmCashRep);
  Application.CreateForm(TfmGens, fmGens);
  Application.CreateForm(TfmCG, fmCG);
  Application.CreateForm(TfmTORep, fmTORep);
  Application.CreateForm(TfmObVed, fmObVed);
  Application.CreateForm(TfmDocs5, fmDocs5);
  Application.CreateForm(TfmAddDoc5, fmAddDoc5);
  Application.CreateForm(TfmMainLoader, fmMainLoader);
  Application.CreateForm(TfmInvExport, fmInvExport);
  Application.CreateForm(TfmCashDep, fmCashDep);
  Application.CreateForm(TfmRepReal, fmRepReal);
  Application.CreateForm(TfmChecks, fmChecks);
  Application.CreateForm(TfmRVed, fmRVed);
  Application.CreateForm(TfmRepTovarPeriod, fmRepTovarPeriod);
  Application.CreateForm(TfmCateg, fmCateg);
  Application.CreateForm(TfmAddDouble, fmAddDouble);
  Application.CreateForm(TfmRDayVed, fmRDayVed);
  Application.CreateForm(TfmRealH, fmRealH);
  Application.CreateForm(TfmGraf1, fmGraf1);
  Application.CreateForm(TfmGraf2, fmGraf2);
  Application.CreateForm(TfmGraf3, fmGraf3);
  Application.CreateForm(TfmChangeT, fmChangeT);
  Application.CreateForm(TfmZadan, fmZadan);
  Application.CreateForm(TfmZadMessage, fmZadMessage);
  Application.CreateForm(TdmFB, dmFB);
  Application.CreateForm(TfmPrib1, fmPrib1);
  Application.CreateForm(TfmSortF, fmSortF);
  Application.CreateForm(TfmAkciya, fmAkciya);
  Application.CreateForm(TfmDocs6, fmDocs6);
  Application.CreateForm(TfmAddDoc6, fmAddDoc6);
  Application.CreateForm(TfmDocs7, fmDocs7);
  Application.CreateForm(TfmChangeB, fmChangeB);
  Application.CreateForm(TfmParts, fmParts);
  Application.CreateForm(TfmSelTVn, fmSelTVn);
  Application.CreateForm(TService1, Service1);
  Application.CreateForm(TfmDepSel, fmDepSel);
  Application.CreateForm(TfmOborRep, fmOborRep);
  Application.CreateForm(TfmSetCardsParams, fmSetCardsParams);
  Application.CreateForm(TfmAssPost, fmAssPost);
  Application.CreateForm(TfmAddDoc7, fmAddDoc7);
  Application.CreateForm(TfmSetStatus, fmSetStatus);
  Application.CreateForm(TfmRDayVedWide, fmRDayVedWide);
  Application.CreateForm(TfmAvSpeed, fmAvSpeed);
  Application.CreateForm(TfmCashFBLoad, fmCashFBLoad);
  Application.CreateForm(TfmChangeT1, fmChangeT1);
  Application.CreateForm(TfmCassirs, fmCassirs);
  Application.CreateForm(TfmZakHPost, fmZakHPost);
  Application.CreateForm(TfmPreInvView, fmPreInvView);
  Application.CreateForm(TfmRepVoz, fmRepVoz);
  Application.CreateForm(TfmCatDisc, fmCatDisc);
  Application.CreateForm(TfmMakers, fmMakers);
  Application.CreateForm(TfmAddMaker, fmAddMaker);
  Application.CreateForm(TfmAddCliLic, fmAddCliLic);
  Application.CreateForm(TfmPreAlg, fmPreAlg);
  Application.CreateForm(TfmAlg, fmAlg);
  Application.CreateForm(TfmDocs8, fmDocs8);
  Application.CreateForm(TdmP, dmP);
  Application.CreateForm(TfmReasonVoz, fmReasonVoz);
  Application.CreateForm(TfmAddDoc8, fmAddDoc8);
  Application.CreateForm(TfmRes, fmRes);
  Application.CreateForm(TfmAlcoE, fmAlcoE);
  Application.CreateForm(TdmAlg, dmAlg);
  Application.CreateForm(TfmAddAlco, fmAddAlco);
  Application.CreateForm(TfmDelayV, fmDelayV);
  Application.CreateForm(TfmPD, fmPD);
  Application.CreateForm(TfmTypeVoz, fmTypeVoz);
  Application.CreateForm(TfmInvVoz, fmInvVoz);
  Application.CreateForm(TfmP, fmP);
  Application.CreateForm(TfmGrafVozvrat, fmGrafVozvrat);
  Application.CreateForm(TfmSetTypeDocOut, fmSetTypeDocOut);
  Application.CreateForm(TfmAltPost, fmAltPost);
  Application.CreateForm(TfmSetMatrix, fmSetMatrix);
  Application.CreateForm(TfmSpInEr, fmSpInEr);
  Application.CreateForm(TfmObrPredz, fmObrPredz);
  Application.CreateForm(TfmShowPost, fmShowPost);
  Application.CreateForm(TfmDateP, fmDateP);
  Application.CreateForm(TfmSInvVoz, fmSInvVoz);
  Application.CreateForm(TfmSpecInvVoz, fmSpecInvVoz);
  Application.CreateForm(TfmAddCash, fmAddCash);
  Application.CreateForm(TfmHistOp, fmHistOp);
  Application.CreateForm(TfmCliLic, fmCliLic);
  Application.CreateForm(TdmE, dmE);
  Application.CreateForm(TfmUpLica, fmUpLica);
  Application.CreateForm(TdmPro, dmPro);
  Application.CreateForm(TfmPrintInSCHF, fmPrintInSCHF);
  Application.CreateForm(TfmPreRealAlco, fmPreRealAlco);
  Application.CreateForm(TfmRepSaleAP, fmRepSaleAP);
  Application.CreateForm(TfmCashFBLoadAVid, fmCashFBLoadAVid);
  Application.Run;
end.
