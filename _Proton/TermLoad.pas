unit TermLoad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxPC, cxControls, cxContainer, cxEdit, cxLabel, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxGraphics, cxDropDownEdit, cxMemo, VaClasses, VaComm,
  VaSystem, DB, dxmdaset, cxSpinEdit, cxRadioGroup, pvtables, sqldataset;

type
  TfmMainLoader = class(TForm)
    OpenDialog1: TOpenDialog;
    DevPrint: TVaComm;
    Memo1: TcxMemo;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    Page1: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    cxLabel1: TcxLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButton2: TcxButton;
    cxLabel2: TcxLabel;
    cxComboBox1: TcxComboBox;
    Page2: TcxTabSheet;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxLabel3: TcxLabel;
    taArt: TdxMemData;
    taArtCode: TIntegerField;
    Label3: TLabel;
    cxLabel4: TcxLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxRButton1: TcxRadioButton;
    cxRButton2: TcxRadioButton;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxButtonEdit2: TcxButtonEdit;
    cxComboBox2: TcxComboBox;
    cxLabel8: TcxLabel;
    cxSpinEdit2: TcxSpinEdit;
    cxLabel7: TcxLabel;
    cxButton3: TcxButton;
    Label6: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    quCTerm: TPvQuery;
    quCTermGoodsID: TIntegerField;
    quCTermID: TStringField;
    quCTermName: TStringField;
    quCTermCena: TFloatField;
    Label7: TLabel;
    cxComboBox3: TcxComboBox;
    quCTermEdIzm: TSmallintField;
    quCTermBarCode: TStringField;
    cxButton6: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxComboBox3PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(S:String);
  end;

procedure Delay(MSecs: Longint);

var
  fmMainLoader: TfmMainLoader;

const
  iTimeOut:Integer=50; //��������

implementation

uses MDB, u2fdk, Un1, SelInOut, MT;

{$R *.dfm}

procedure TfmMainLoader.prWr(S:String);
begin
  Memo1.Lines.Add(FormatDateTime('hh:nn ss:zzz ',now)+S); Delay(10);
end;


procedure TfmMainLoader.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainLoader.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  cxPageControl1.Align:=AlClient;
  OpenDialog1.InitialDir:=CurDir;
  cxButtonEdit1.Text:=CurDir+'Referenc.dat';
  cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  Memo1.Align:=AlClient;
  Memo1.Clear;
  cxPageControl1.ActivePageIndex:=0;
end;

procedure TfmMainLoader.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

procedure TfmMainLoader.cxButton2Click(Sender: TObject);
Var bErr:Boolean;
    f:TextFile;
    StrWk:String;
    iC,iCode,n,iD,iErr,i:INteger;
    sCode:String;
    Buff:Array[1..50] of Char;
    BuffR:Array[1..5] of Char;
begin
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  cxButton5.Enabled:=False;
  iD:=cxSpinEdit1.Value;
  Memo1.Clear;
  prWr('������ ��������');
  iC:=0;
  Label2.Caption:=INtToStr(iC);
  Label3.Caption:='0';

  bErr:=False;
  try
    prWr('  ��������� ���� COM'+IntToStr(cxComboBox1.ItemIndex+1));
    DevPrint.DeviceName:='COM'+IntToStr(cxComboBox1.ItemIndex+1);
    try
      DevPrint.Open;
    except
      prWr('  ������ �������� �����.');
      bErr:=True;
      try
        DevPrint.Close;
      except
      end;
    end;

    if not bErr then
    begin
      prWr('  ���� ��');
      prWr('  ��������� ���� - '+cxButtonEdit1.Text);
      if cxRButton1.Checked then
      begin
        if FileExists(cxButtonEdit1.Text) then
        begin
          prWr('  ���� - '+cxButtonEdit1.Text+'  ����������.');
          assignfile(f,cxButtonEdit1.Text);
          try
            Reset(f);
            prWr('');
            prWr('  ������������� � ���������� ...');
            taArt.Active:=False;
            taArt.Active:=True;

            iErr:=0;

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=11; end;
            end;

            Buff[1]:=#$30;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=12; end;
            end;

            Buff[1]:=#$00;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=13; end;
            end;

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=14; end;
            end;

            prWr('    ����� ... ���� ��������.');

            Buff[1]:=#$04;
//          DevPrint.WriteBuf(Buff,1); delay(iD);
            for n:=2 to 49 do Buff[n]:=chr(n-1);
            DevPrint.WriteBuf(Buff,49); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$22 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$22 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=22; end;
            end;

            while (EOF(f)=False)and(bErr=False) do
            begin
              inc(iC); Label2.Caption:=INtToStr(iC); Delay(10);
              ReadLn(f,StrWk);
              if Length(StrWk)>=49 then
              begin
                Label3.Caption:='0';
                Buff[1]:=#$01;
                DevPrint.WriteBuf(Buff,1); delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]<>#$02 then
                begin
                  for i:=1 to iTimeOut do
                  begin
                    delay(iD);
                    DevPrint.ReadBuf(BuffR,1);
                    if BuffR[1]=#$02 then break;
                    Label3.Caption:=IntToStr(i);
                  end;
                  if i=iTimeOut then  begin bErr:=True; iErr:=2; end;
                end;
                if not bErr then
                begin
                  Buff[1]:=#$04;
                  for n:=2 to 49 do Buff[n]:= StrWk[n-1];
                  DevPrint.WriteBuf(Buff,49); delay(iD);
                  DevPrint.ReadBuf(BuffR,1);
                  if BuffR[1]<>#$22 then
                  begin
                    for i:=1 to iTimeOut do
                    begin
                      delay(iD);
                      DevPrint.ReadBuf(BuffR,1);
                      if BuffR[1]=#$22 then break;
                      Label3.Caption:=IntToStr(i);
                    end;
                    if i=iTimeOut then  begin bErr:=True; iErr:=22; end;
                  end;
                end;

                sCode:=Copy(StrWk,14,5); while pos(' ',sCode)>0 do delete(sCode,pos(' ',sCode),1);
                iCode:=StrToINtDef(sCode,0);
                if iCode>0 then
                begin
                  if taArt.Locate('Code',iCode,[])=False then
                  begin
                    taArt.Append;
                    taArtCode.AsInteger:=iCode;
                    taArt.Post;
                  end;
                end else
                begin
                  prWr('      - ������ �������������� ���. '+IntToStr(iC)+' ('+Copy(StrWk,1,18)+')');
                end;  
              end;
            end;

            if bErr then
            begin
              prWr('      ����� �� ������ ('+IntToStr(iErr)+')');
            end;

            Buff[1]:=#$01;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
            end;

            Buff[1]:=#$0F;
            Buff[3]:=#$00;
            Buff[2]:=#$30;
//          DevPrint.WriteBuf(Buff,3); delay(iD);
            for n:=4 to 49 do Buff[n]:=chr(n+2);
            DevPrint.WriteBuf(Buff,49); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$22 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$22 then break;
                Label3.Caption:=IntToStr(i);
              end;
            end;

            if iErr>0 then
              prWr('      ������ '+INtToStr(iErr));

            prWr('    ����� ��������� '+INtToStr(iC)+'- �� '+IntToStr(taArt.RecordCount)+'- ��������.');
            taArt.Active:=False;
            prWr('');
            CloseFile(f);
          except
            prWr('  ������ ������ � ������.');
          end;
        end else
          prWr('  ���� - '+cxButtonEdit1.Text+'  �� ������.');
      end;
      if cxRButton2.Checked then
      begin
        Buff[1]:=#$01;
        BuffR[1]:=#$00;
        DevPrint.WriteBuf(Buff,1); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$02 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$02 then break;
            Label3.Caption:=IntToStr(i);
          end;
          if i=iTimeOut then  begin bErr:=True; end;
        end;

        Buff[1]:=#$05;
        BuffR[1]:=#$00;
        DevPrint.WriteBuf(Buff,1); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$02 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$02 then break;
            Label3.Caption:=IntToStr(i);
          end;
          if i=iTimeOut then  begin bErr:=True; end;
        end;

        Buff[1]:=#$00;
        BuffR[1]:=#$00;
        DevPrint.WriteBuf(Buff,1); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$02 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$02 then break;
            Label3.Caption:=IntToStr(i);
          end;
          if i=iTimeOut then  begin bErr:=True; end;
        end;

        Buff[1]:=#$01;
        BuffR[1]:=#$00;
        DevPrint.WriteBuf(Buff,1); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$02 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$02 then break;
            Label3.Caption:=IntToStr(i);
          end;
          if i=iTimeOut then  begin bErr:=True;  end;
        end;

        prWr('    ����� ... ���� ��������.');

        Buff[1]:=#$04;
        for n:=2 to 6 do Buff[n]:=chr(n-1);
        DevPrint.WriteBuf(Buff,6); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$22 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$22 then break;
            Label3.Caption:=IntToStr(i);
          end;
          if i=iTimeOut then  begin bErr:=True; end;
        end;

{21W7
22W7
24W13
25W13
26W13}



//        while (EOF(f)=False)and(bErr=False) do
        iC:=0;
        while (iC<5)and(bErr=False) do
        begin
//          ReadLn(f,StrWk);
          if iC=0 then StrWk:='21W7 ';
          if iC=1 then StrWk:='22W7 ';
          if iC=2 then StrWk:='24W13';
          if iC=3 then StrWk:='25W13';
          if iC=4 then StrWk:='26W13';

          inc(iC); Label2.Caption:=INtToStr(iC); Delay(10);

          if (Length(StrWk)>=4) and (Length(StrWk)<6) then
          begin

            if Length(StrWk)<5 then StrWk:=StrWk+' ';

            Buff[1]:=#$01;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; end;
            end;

            if not bErr then
            begin
              Buff[1]:=#$04;
              for n:=2 to 6 do Buff[n]:= StrWk[n-1];
              DevPrint.WriteBuf(Buff,6); delay(iD);
              DevPrint.ReadBuf(BuffR,1);
              if BuffR[1]<>#$22 then
              begin
                for i:=1 to iTimeOut do
                begin
                  delay(iD);
                  DevPrint.ReadBuf(BuffR,1);
                  if BuffR[1]=#$22 then break;
                  Label3.Caption:=IntToStr(i);
                end;
                if i=iTimeOut then  begin bErr:=True;  end;
              end;
            end;


          end;
        end;

        Buff[1]:=#$01;
        DevPrint.WriteBuf(Buff,1); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$02 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$02 then break;
            Label3.Caption:=IntToStr(i);
          end;
        end;

        Buff[1]:=#$0F;
        Buff[2]:=#$05;
        Buff[3]:=#$00;
//          DevPrint.WriteBuf(Buff,3); delay(iD);
        for n:=4 to 6 do Buff[n]:=chr(n-1);
        DevPrint.WriteBuf(Buff,6); delay(iD);
        DevPrint.ReadBuf(BuffR,1);
        if BuffR[1]<>#$22 then
        begin
          for i:=1 to iTimeOut do
          begin
            delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]=#$22 then break;
            Label3.Caption:=IntToStr(i);
          end;
        end;

        if bErr then  prWr('      -- ������ ��� ��������.');
        prWr('    ����� ��������� '+INtToStr(iC)+'- ����� �� .');
      end;

    end;
  finally
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton5.Enabled:=True;
    DevPrint.Close;
    prWr('�������� ���������.');
  end;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TfmMainLoader.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit2.Text:=OpenDialog1.FileName;
end;

procedure TfmMainLoader.cxButton3Click(Sender: TObject);
//�������
Var bErr:Boolean;
    f:TextFile;
    StrWk:String;
    iC,n,iD,iErr,i:INteger;
    Buff:Array[1..5] of Char;
    BuffR:Array[1..50] of Char;
    rSum:Real;
    sSum,sQ:String;
    FileN:String;

  procedure prClBuf;
  var i:SmallINt;
  begin
    for i:=1 to 27 do BuffR[i]:='0';
  end;

begin
  cxButton3.Enabled:=False;
  cxButton4.Enabled:=False;
  cxButton2.Enabled:=False;
  cxButton6.Enabled:=False;

  iD:=cxSpinEdit2.Value;
  Memo1.Clear;
  prWr('������ �������');
  iC:=0;
  Label5.Caption:=INtToStr(iC);

  iErr:=0;
  bErr:=False;
  try
    prWr('  ��������� ���� COM'+IntToStr(cxComboBox2.ItemIndex+1));
    DevPrint.DeviceName:='COM'+IntToStr(cxComboBox2.ItemIndex+1);
    try
      DevPrint.Open;
    except
      prWr('  ������ �������� �����.');
      bErr:=True;
      try
        DevPrint.Close;
      except
      end;
    end;

    if not bErr then
    begin
      prWr('  ���� ��');

{      if FileExists(cxButtonEdit2.Text) then
      begin //���� ���������� - ������ ����������
        prWr('  ���� - '+cxButtonEdit2.Text+' ����������. ������� ����������.');
      end else
      begin //����� ��� ����� ��������� ������    }

        rSum:=0;

        try
          assignfile(f,CommonSet.TmpPath+'Inv.tmp');
          rewrite(f);
          try
            prWr('  ���� ������ ��');
            prWr('');
            prWr('  �������������...');

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=11; end;
            end;

            Buff[1]:=#$00;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=12; end;
            end;

            Buff[1]:=#$1A;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1); delay(iD);
            if BuffR[1]<>#$02 then
            begin
              for i:=1 to iTimeOut do
              begin
                delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]=#$02 then break;
                Label3.Caption:=IntToStr(i);
              end;
              if i=iTimeOut then  begin bErr:=True; iErr:=13; end;
            end;

            BuffR[1]:=#$00;
            DevPrint.ReadBuf(BuffR,1); delay(10);
            if BuffR[1]=#$01 then
            begin          //��� � ��� �������������
              Buff[1]:=#$02;
              DevPrint.WriteBuf(Buff,1); delay(iD);
            end else bErr:=True;

            prWr('    ����� ... ���� ��������.');


            if not bErr then
            begin
              prClBuf;
              DevPrint.ReadBuf(BuffR,27); delay(10); //������ ��� ������
              if BuffR[1]<>#$04 then bErr:=True
              else
              begin
                while (BuffR[1]<>#$0F)and(bErr=False) do
                begin
                  //������� 22 ������������� ���������� ��������
                  Buff[1]:=#$22;
                  DevPrint.WriteBuf(Buff,1); delay(iD);

                  //��� 01
                  prClBuf;
                  DevPrint.ReadBuf(BuffR,1); delay(10); //������ ��� ������

                  if BuffR[1]=#$01 then  //������
                  begin //������� 02 �� �����������
                    Buff[1]:=#$02;
                    DevPrint.WriteBuf(Buff,1); delay(iD);

                    prClBuf;
                    DevPrint.ReadBuf(BuffR,27); delay(10); //������ ��� ������
                    if BuffR[1]=#$04 then //����� ��� �����
                    begin
                      inc(iC); Label5.Caption:=INtToStr(iC); Delay(10);
                      StrWk:='';
                      for n:=2 to 27 do StrWk:=StrWk+BuffR[n];
                      StrWk:=StrWk+'1';
//                      StrWk:=IntToStr(iC);
                      writeLn(f,StrWk);
                      sSum:=Copy(StrWk,8,9);
                      sQ:=Copy(StrWk,17,9);

                      while pos(' ',sSum)>0 do delete(sSum,pos(' ',sSum),1);
                      while pos('.',sSum)>0 do sSum[pos('.',sSum)]:=',';
                      while pos(' ',sQ)>0 do delete(sQ,pos(' ',sQ),1);
                      while pos('.',sQ)>0 do sQ[pos('.',sQ)]:=',';

                      rSum:=rSum+(StrToFloatDef(sSum,0)*StrToFloatDef(sQ,1));

                    end;
                  end else
                  begin
                    bErr:=True;
                  end;
                end;
              end;
              //��� ��������� ���������
              if bErr=False then
              begin
                Buff[1]:=#$22;
                DevPrint.WriteBuf(Buff,1); delay(iD);
              end else
              begin
                prWr('    ������ ��� ������� ������ ('+INtToStr(iErr)+')');
              end;
            end;
            prWr('');
          finally
            CloseFile(f);
          end;

          //���� ��������� ������� - ��������� � �����������
          Str(rSum:8:2,sSum);
          while pos('.',sSum)>0 do sSum[pos('.',sSum)]:='-';
          while pos(',',sSum)>0 do sSum[pos(',',sSum)]:='-';

          FileN:=cxButtonEdit2.Text;
          while pos('.',FileN)>0 do delete(FileN,Length(FileN),1);
          FileN:=FileN+'_'+sSum+'.dat';

          prWr('');
          if FileExists(FileN) then
          begin
            try
              prWr('������ - '+FileN);
              assignfile(f,FileN);
              erase(f);
              prWr('�������� ��.');
            except
              prWr('������ ��� ��������.');
            end;
          end;

          CopyFile(PChar(CommonSet.TmpPath+'Inv.tmp'),PChar(FileN),False); delay(10);

        except
          prWr('  ������ �������� �����. ������� ����������.');
        end;
    end;
  finally
    cxButton3.Enabled:=True;
    cxButton4.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton6.Enabled:=True;

    DevPrint.Close;
    prWr('�������� ���������.');
  end;
end;

procedure TfmMainLoader.cxButton4Click(Sender: TObject);
Var f:TextFile;
begin
  prWr('');
  prWr('������ - '+cxButtonEdit2.Text);
  if FileExists(cxButtonEdit2.Text) then
  begin
    try
      assignfile(f,cxButtonEdit2.Text);
      erase(f);
      prWr('�������� ��.');
    except
      prWr('������ ��� ��������.');
    end;
  end else
  begin
    prWr('����� ��� - ������� ������.');
  end;
end;

procedure TfmMainLoader.cxButton5Click(Sender: TObject);
Var f:TextFile;
    Str1,Str2:String;
begin
//����������� ����
  Memo1.Clear;
  prWr('����� ... ���� ������������ ����� ��������.');
  try
    cxButton1.Enabled:=False;
    cxButton2.Enabled:=False;
    cxButton5.Enabled:=False;

    with dmMc do
    begin
      assignfile(f,cxButtonEdit1.Text);
      try
        rewrite(f);
        prWr('  ��������� ������.');
        quCTerm.Active:=False;
        quCTerm.Active:=True;
        prWr('  C����� ��.');

        prWr('  ��������� ����.');

        quCTerm.First;
        while not quCTerm.Eof do
        begin
          if cxComboBox3.ItemIndex=0 then
          begin
            Str1:=quCTermID.AsString;
            while Length(Str1)<13 do Str1:=Str1+' ';
            Str1:=Str1+quCTermGoodsID.AsString;
            while Length(Str1)<18 do Str1:=Str1+' ';

            Str2:=quCTermCena.AsString;
            while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
            Str1:=Str1+Str2;
            while Length(Str1)<27 do Str1:=Str1+' ';

            Str1:=Str1+Copy(AnsiToOemConvert(quCTermName.AsString),1,22);
            while Length(Str1)<49 do Str1:=Str1+' ';
          end;

          if cxComboBox3.ItemIndex=1 then
          begin
            Str1:='P;'+quCTermID.AsString+';'; //��
            Str1:=Str1+quCTermName.AsString+';'; //��������

            Str2:=quCTermCena.AsString;
            while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
            Str1:=Str1+Str2+';'; //����
            Str1:=Str1+'1;'; //���-��
            if (quCTermBarCode.AsString<>quCTermID.AsString) then Str1:=Str1+quCTermBarCode.AsString+';' //�������� ��
            else Str1:=Str1+';';


            Str1:=Str1+';'; //�����
            if quCTermEdIzm.AsInteger=1 then Str1:=Str1+'��.' else Str1:=Str1+'��.'
          end;
          writeln(f,Str1);

          quCTerm.Next;
        end;
        prWr('  ���� ��.');
      finally
        quCTerm.Active:=False;
        closefile(f);
      end;
    end;
  finally
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton5.Enabled:=True;
    prWr('���� �������� ��.');
  end;
end;

procedure TfmMainLoader.cxComboBox3PropertiesChange(Sender: TObject);
begin
  if cxComboBox3.ItemIndex=1 then cxButtonEdit1.Text:=CurDir+'scanin.dat';
  
end;

procedure TfmMainLoader.cxButton6Click(Sender: TObject);
Var FileFrom, FileTo:String;
    bErr:Boolean;
    f,f1:TextFile;
    Str1,Str2,sBar,sQ,sSum:String;
    rQ,rSum:Real;
    iC,iC1:Integer;
begin
  iC:=0; iC1:=0;
  try
    fmInOutSel:=TfmInOutSel.Create(Application);
    fmInOutSel.ShowModal;
    if fmInOutSel.ModalResult=mrOk then
    begin
      //������� ���������������
      Memo1.Clear;
      FileFrom:=fmInOutSel.cxButtonEdit1.Text;
      FileTo:=fmInOutSel.cxButtonEdit2.Text;


      if FileExists(FileFrom) then
      begin
        prWr('�������� - '+FileFrom);

        bErr:=False;

        if FileExists(FileTo) then
        begin
          try
            prWr('������ - '+FileTo);
            assignfile(f,FileTo);
            erase(f);
            prWr('�������� ��.');
            prWr('�������� - '+FileTo);
          except
            prWr('������ ��� ��������.');
            bErr:=True;
          end;
        end else prWr('�������� - '+FileTo);

        if not bErr then
        begin
          try
            if dmMT.ptBar.Active=False then dmMT.ptBar.Active:=True;
            dmMT.ptBar.Refresh;
            dmMT.ptBar.IndexFieldNames:='ID';

            assignfile(f1,FileFrom);
            Reset(f1);

            assignfile(f,FileTo);
            rewrite(f);
            rSum:=0;

            while not EOF(f1) do
            begin
              ReadLn(f1,Str1); //P;4605246004896;3.000
              inc(iC);

              if (Length(Str1)>5)and(Str1[1]='P') then
              begin
                bErr:=False;
                rQ:=0;

                delete(Str1,1,Pos(';',Str1));
                if Pos(';',Str1)>0 then
                begin
                  sBar:=Copy(Str1,1,Pos(';',Str1)-1);
                  delete(Str1,1,Pos(';',Str1));

                  sQ:=Str1;
                  while pos(' ',sQ)>0 do delete(sQ,pos(' ',sQ),1);
                  while pos('.',sQ)>0 do sQ[pos('.',sQ)]:=',';
                  rQ:=StrToFloatDef(sQ,0);
                end else bErr:=True;

                if not bErr then
                begin
                  with dmMT do
                  begin
                    if ptBar.FindKey([sBar]) then
                    begin
                      if taCards.FindKey([ptBarGoodsID.AsInteger]) then
                      begin
                        //��� ����� - ���� ������
                        Str1:='01'; //�� �� ���� �����

                        Str1:=Str1+ptBarGoodsID.AsString; //��� ������
                        while length(Str1)<7 do Str1:=Str1+' ';

                        Str2:=taCardsCena.AsString; //����
                        while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                        Str1:=Str1+Str2;
                        while length(Str1)<16 do Str1:=Str1+' ';

                        Str2:=fs(rQ); //���-��
                        while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                        Str1:=Str1+Str2;
                        while length(Str1)<26 do Str1:=Str1+' ';

                        Str1:=Str1+'1'; //������ - ����� �� ����� ����

                        writeln(f,Str1); inc(iC1);

                        rSum:=rSum+taCardsCena.AsFloat*rQ;

                      end else
                      begin
                        prWr('   ������ - ��� "'+its(ptBarGoodsID.AsInteger)+'" �� ������ � ����.');
                      end;
                    end else
                    begin
                      prWr('   ������ - �������� "'+sBar+'" �� ������ � ����.');
                    end;
                  end;
                end;
              end;
            end;

          finally
            closefile(f1);
            closefile(f);
          end;


          //���� ��������� ������� - ��������� � �����������
          Str(rSum:8:2,sSum);
          while pos('.',sSum)>0 do sSum[pos('.',sSum)]:='-';
          while pos(',',sSum)>0 do sSum[pos(',',sSum)]:='-';

          FileFrom:=FileTo;
          while pos('.',FileTo)>0 do delete(FileTo,Length(FileTo),1);
          FileTo:=FileTo+'_'+sSum+'.dat';

          prWr('');
          if FileExists(FileTo) then
          begin
            try
              prWr('������ - '+FileTo);
              assignfile(f,FileTo);
              erase(f);
              prWr('�������� ��.');
            except
              prWr('������ ��� ��������.');
            end;
          end;

          CopyFile(PChar(FileFrom),PChar(FileTo),False); delay(10);

        end;
      end else prWr('�������� - '+FileFrom+ ' - �� ������. ������� ����������.');
    end;
  finally
    fmInOutSel.Release;
    prWr('  ������� - '+its(iC)+'  ������� - '+its(iC1));
    Label5.Caption:=its(iC1);
    Label6.Caption:=its(iC-iC1);
    prWr('������� ���������.');
  end;
end;

end.
