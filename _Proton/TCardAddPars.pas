unit TCardAddPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, dxmdaset, ExtCtrls, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmTCardsAddPars = class(TForm)
    Panel1: TPanel;
    teAddPars: TdxMemData;
    ViewAddPars: TcxGridDBTableView;
    LevelAddPars: TcxGridLevel;
    GridAddPars: TcxGrid;
    teAddParsNum: TIntegerField;
    teAddParsNamePar: TStringField;
    teAddParsValPar: TStringField;
    dsteAddPars: TDataSource;
    ViewAddParsRecId: TcxGridDBColumn;
    ViewAddParsNamePar: TcxGridDBColumn;
    ViewAddParsValPar: TcxGridDBColumn;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTCardsAddPars: TfmTCardsAddPars;

implementation

{$R *.dfm}

procedure TfmTCardsAddPars.FormCreate(Sender: TObject);
begin
  GridAddPars.Align:=AlClient;
end;

end.
