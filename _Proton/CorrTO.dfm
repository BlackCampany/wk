object fmCorrTO: TfmCorrTO
  Left = 310
  Top = 603
  BorderStyle = bsDialog
  Caption = #1050#1086#1088#1088#1077#1082#1094#1080#1103' '#1058#1054
  ClientHeight = 291
  ClientWidth = 337
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 80
    Height = 13
    Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095'.'
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 44
    Width = 40
    Height = 13
    Caption = #1055#1088#1080#1093#1086#1076' '
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 72
    Width = 109
    Height = 13
    Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1072#1089#1089#1077
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 100
    Width = 60
    Height = 13
    Caption = #1056#1072#1089#1093#1086#1076' '#1076#1086#1087'.'
    Transparent = True
  end
  object Label5: TLabel
    Left = 16
    Top = 128
    Width = 99
    Height = 13
    Caption = #1054#1089#1090'. '#1085#1072' '#1082#1086#1085#1077#1094' '#1088#1072#1089#1095'.'
    Transparent = True
  end
  object Label6: TLabel
    Left = 16
    Top = 156
    Width = 123
    Height = 13
    Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094' '#1087#1086' '#1058#1054
    Transparent = True
  end
  object Label7: TLabel
    Left = 16
    Top = 196
    Width = 107
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 229
    Width = 337
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = 16767152
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 56
      Top = 16
      Width = 75
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 200
      Top = 16
      Width = 75
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 180
    Top = 12
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 129
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 180
    Top = 40
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Width = 129
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 180
    Top = 68
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 3
    Width = 129
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 180
    Top = 96
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 4
    Width = 129
  end
  object cxCalcEdit5: TcxCalcEdit
    Left = 180
    Top = 124
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Width = 129
  end
  object cxCalcEdit6: TcxCalcEdit
    Left = 180
    Top = 152
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Properties.OnEditValueChanged = cxCalcEdit6PropertiesEditValueChanged
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Width = 129
  end
  object cxCalcEdit7: TcxCalcEdit
    Left = 180
    Top = 188
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.00'
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 7
    Width = 129
  end
end
