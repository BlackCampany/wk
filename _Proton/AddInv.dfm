object fmAddInv: TfmAddInv
  Left = 56
  Top = 160
  Width = 1044
  Height = 797
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 161
    Top = 125
    Width = 875
    Height = 578
    Align = alClient
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 504
      Width = 871
      Height = 72
      Align = alBottom
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 0
      object Memo1: TcxMemo
        Left = 2
        Top = 2
        TabStop = False
        Align = alClient
        Lines.Strings = (
          'Memo1')
        ParentFont = False
        Properties.OEMConvert = True
        Properties.ReadOnly = True
        Properties.ScrollBars = ssVertical
        Properties.WordWrap = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Pitch = fpFixed
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        OnDblClick = Memo1DblClick
        Height = 68
        Width = 867
      end
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 871
      Height = 502
      ActivePage = TabSheet2
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = #1058#1086#1074#1072#1088#1099' '#1080' '#1073#1083#1102#1076#1072
        object GridInv: TcxGrid
          Left = 0
          Top = 0
          Width = 863
          Height = 471
          Align = alClient
          PopupMenu = PopupMenu1
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewInv: TcxGridDBTableView
            OnDragDrop = ViewInvDragDrop
            OnDragOver = ViewInvDragOver
            NavigatorButtons.ConfirmDelete = False
            OnEditing = ViewInvEditing
            OnEditKeyDown = ViewInvEditKeyDown
            OnEditKeyPress = ViewInvEditKeyPress
            DataController.DataSource = dsSpec
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'Quant'
                Column = ViewInvQuant
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QuantDif'
                Column = ViewInvQuantDif
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QuantFact'
                Column = ViewInvQuantFact
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn'
                Column = ViewInvSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInDif'
                Column = ViewInvSumInDif
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInF'
                Column = ViewInvSumInF
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn0'
                Column = ViewInvSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInF0'
                Column = ViewInvSumInF0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInDif0'
                Column = ViewInvSumInDif0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInTO'
                Column = ViewInvSumInTO
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInTODif'
                Column = ViewInvSumInTODif
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'Quant'
                Column = ViewInvQuant
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'QuantDif'
                Column = ViewInvQuantDif
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'QuantFact'
                Column = ViewInvQuantFact
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewInvSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInDif'
                Column = ViewInvSumInDif
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInF'
                Column = ViewInvSumInF
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn0'
                Column = ViewInvSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInF0'
                Column = ViewInvSumInF0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInDif0'
                Column = ViewInvSumInDif0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInTO'
                Column = ViewInvSumInTO
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInTODif'
                Column = ViewInvSumInTODif
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewInvNum: TcxGridDBColumn
              Caption = #8470' '#1087#1087
              DataBinding.FieldName = 'Num'
              Width = 34
            end
            object ViewInvIdGoods: TcxGridDBColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'IdGoods'
              Width = 57
            end
            object ViewInvNameG: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NameG'
              Width = 142
            end
            object ViewInvCType: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
              DataBinding.FieldName = 'CType'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Items = <
                item
                  Description = #1058#1086#1074#1072#1088
                  ImageIndex = 0
                  Value = 1
                end
                item
                  Description = #1059#1089#1083#1091#1075#1080
                  Value = 2
                end
                item
                  Description = #1040#1074#1072#1085#1089
                  Value = 3
                end
                item
                  Description = #1058#1072#1088#1072
                  Value = 4
                end>
              Options.Editing = False
            end
            object ViewInvTCard: TcxGridDBColumn
              Caption = #1058#1050
              DataBinding.FieldName = 'TCard'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmO.imState
              Properties.Items = <
                item
                  Value = 0
                end
                item
                  ImageIndex = 11
                  Value = 1
                end>
              Options.Editing = False
              Styles.Content = dmO.cxStyle5
            end
            object ViewInvNoCalc: TcxGridDBColumn
              Caption = #1053#1077' '#1088#1072#1089#1082#1083#1072#1076#1099#1074#1072#1090#1100
              DataBinding.FieldName = 'NoCalc'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = 1
              Properties.ValueUnchecked = 0
            end
            object ViewInvIdGroup: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099
              DataBinding.FieldName = 'IdGroup'
              Options.Editing = False
            end
            object ViewInvNameGr: TcxGridDBColumn
              Caption = #1043#1088#1091#1087#1087#1072
              DataBinding.FieldName = 'NameGr'
              Options.Editing = False
              Styles.Content = dmO.cxStyle15
              Width = 159
            end
            object ViewInvNameGr2: TcxGridDBColumn
              Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
              DataBinding.FieldName = 'NameGr2'
              Options.Editing = False
              Width = 140
            end
            object ViewInvIM: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'IM'
              Width = 35
            end
            object ViewInvSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Options.Editing = False
              Width = 60
            end
            object ViewInvQuant: TcxGridDBColumn
              Caption = #1056#1072#1089#1095'. '#1082#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'Quant'
              Options.Editing = False
              Styles.Content = dmO.cxStyle7
            end
            object ViewInvPriceIn: TcxGridDBColumn
              Caption = #1056#1072#1089#1095'. '#1094#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
              DataBinding.FieldName = 'PriceIn'
              Options.Editing = False
              Styles.Content = dmO.cxStyle7
            end
            object ViewInvSumIn: TcxGridDBColumn
              Caption = #1056#1072#1089#1095'. '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
              DataBinding.FieldName = 'SumIn'
              Options.Editing = False
              Styles.Content = dmO.cxStyle7
            end
            object ViewInvQuantFact: TcxGridDBColumn
              Caption = #1060#1072#1082#1090' '#1082#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QuantFact'
              Styles.Content = dmO.cxStyle25
            end
            object ViewInvPriceInF: TcxGridDBColumn
              Caption = #1060#1072#1082#1090' '#1094#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
              DataBinding.FieldName = 'PriceInF'
            end
            object ViewInvSumInF: TcxGridDBColumn
              Caption = #1060#1072#1082#1090' '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
              DataBinding.FieldName = 'SumInF'
            end
            object ViewInvQuantDif: TcxGridDBColumn
              Caption = #1056#1072#1079#1085#1080#1094#1072' '#1082#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QuantDif'
              Options.Editing = False
              Styles.Content = dmO.cxStyle25
            end
            object ViewInvSumInDif: TcxGridDBColumn
              Caption = #1056#1072#1079#1085#1080#1094#1072' '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
              DataBinding.FieldName = 'SumInDif'
              Options.Editing = False
              Styles.Content = dmO.cxStyle12
            end
            object ViewInvSumInTO: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1058#1054' ('#1094#1077#1085#1099' '#1079#1072#1082#1091#1087#1072')'
              DataBinding.FieldName = 'SumInTO'
              Styles.Content = dmO.cxStyle1
            end
            object ViewInvSumInTODif: TcxGridDBColumn
              Caption = #1056#1072#1079#1085#1080#1094#1072' '#1058#1054' ('#1094#1077#1085#1099' '#1079#1072#1082#1091#1087#1072')'
              DataBinding.FieldName = 'SumInTODif'
              Options.Editing = False
              Styles.Content = dmO.cxStyle25
            end
            object ViewInvPriceIn0: TcxGridDBColumn
              Caption = #1056#1072#1089#1095'. '#1094#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079'.'#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn0'
            end
            object ViewInvSumIn0: TcxGridDBColumn
              Caption = #1056#1072#1089#1095'. '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn0'
            end
            object ViewInvPriceInF0: TcxGridDBColumn
              Caption = #1060#1072#1082#1090' '#1094#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceInF0'
            end
            object ViewInvSumInF0: TcxGridDBColumn
              Caption = #1060#1072#1082#1090' '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumInF0'
            end
            object ViewInvSumInDif0: TcxGridDBColumn
              Caption = #1056#1072#1079#1085#1080#1094#1072' '#1089#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumInDif0'
            end
            object ViewInvNDSProc: TcxGridDBColumn
              Caption = #1053#1044#1057
              DataBinding.FieldName = 'NDSProc'
            end
            object ViewInvSumNDS: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057
              DataBinding.FieldName = 'SumNDS'
            end
          end
          object LevelInv: TcxGridLevel
            GridView = ViewInv
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1058#1086#1074#1072#1088#1099
        ImageIndex = 1
        object GridInvC: TcxGrid
          Left = 0
          Top = 0
          Width = 863
          Height = 471
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewInvC: TcxGridDBTableView
            OnDragDrop = ViewInvDragDrop
            OnDragOver = ViewInvDragOver
            NavigatorButtons.ConfirmDelete = False
            OnEditing = ViewInvEditing
            OnEditKeyDown = ViewInvEditKeyDown
            OnEditKeyPress = ViewInvEditKeyPress
            DataController.DataSource = dsSpecC
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'Quant'
                Column = ViewInvCQuant
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QuantDif'
                Column = ViewInvCQuantDif
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QuantFact'
                Column = ViewInvCQuantFact
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn'
                Column = ViewInvCSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInDif'
                Column = ViewInvCSumInDif
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInF'
                Column = ViewInvCSumInF
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInTO'
                Column = ViewInvCSumInTO
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn0'
                Column = ViewInvCSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInDif0'
                Column = ViewInvCSumInDif0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumInF0'
                Column = ViewInvCSumInF0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumNDS'
                Column = ViewInvCSumNDS
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'Quant'
                Column = ViewInvCQuant
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'QuantDif'
                Column = ViewInvCQuantDif
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'QuantFact'
                Column = ViewInvCQuantFact
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewInvCSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInDif'
                Column = ViewInvCSumInDif
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInF'
                Column = ViewInvCSumInF
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInTO'
                Column = ViewInvCSumInTO
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInTODif'
                Column = ViewInvCSumInTODif
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn0'
                Column = ViewInvCSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInF0'
                Column = ViewInvCSumInF0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumInDif0'
                Column = ViewInvCSumInDif0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumNDS'
                Column = ViewInvCSumNDS
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewInvCNum: TcxGridDBColumn
              Caption = #8470
              DataBinding.FieldName = 'Num'
              Options.Editing = False
              Width = 43
            end
            object ViewInvCIdGoods: TcxGridDBColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'IdGoods'
              Options.Editing = False
              Width = 37
            end
            object ViewInvCNameG: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NameG'
              Options.Editing = False
              Width = 158
            end
            object ViewInvCCType: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
              DataBinding.FieldName = 'CType'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Items = <
                item
                  Description = #1058#1086#1074#1072#1088
                  ImageIndex = 0
                  Value = 1
                end
                item
                  Description = #1059#1089#1083#1091#1075#1080
                  ImageIndex = 0
                  Value = 2
                end
                item
                  Description = #1040#1074#1072#1085#1089
                  ImageIndex = 0
                  Value = 3
                end
                item
                  Description = #1058#1072#1088#1072
                  ImageIndex = 0
                  Value = 4
                end>
              Options.Editing = False
              Styles.Content = dmO.cxStyle5
            end
            object ViewInvCIdGroup: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099
              DataBinding.FieldName = 'IdGroup'
              Options.Editing = False
            end
            object ViewInvCNameGr: TcxGridDBColumn
              Caption = #1043#1088#1091#1087#1087#1072
              DataBinding.FieldName = 'NameGr'
              Options.Editing = False
              Width = 150
            end
            object ViewInvCIM: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'IM'
              Options.Editing = False
            end
            object ViewInvCSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              Options.Editing = False
            end
            object ViewInvCQuant: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1095'.'
              DataBinding.FieldName = 'Quant'
              Options.Editing = False
              Styles.Content = dmO.cxStyle25
            end
            object ViewInvCPriceIn: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1088#1072#1089#1095'.'
              DataBinding.FieldName = 'PriceIn'
              Options.Editing = False
            end
            object ViewInvCSumIn: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1095'.'
              DataBinding.FieldName = 'SumIn'
              Options.Editing = False
            end
            object ViewInvCQuantFact: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1092#1072#1082#1090
              DataBinding.FieldName = 'QuantFact'
              Options.Editing = False
              Styles.Content = dmO.cxStyle25
            end
            object ViewInvCPriceInF: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1092#1072#1082#1090
              DataBinding.FieldName = 'PriceInF'
              Options.Editing = False
            end
            object ViewInvCSumInF: TcxGridDBColumn
              Caption = 'C'#1091#1084#1084#1072' '#1092#1072#1082#1090
              DataBinding.FieldName = 'SumInF'
              Options.Editing = False
            end
            object ViewInvCQuantDif: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1079#1085#1080#1094#1072
              DataBinding.FieldName = 'QuantDif'
              Options.Editing = False
            end
            object ViewInvCSumInDif: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1072#1079#1085#1080#1094#1072
              DataBinding.FieldName = 'SumInDif'
              Options.Editing = False
              Styles.Content = dmO.cxStyle12
            end
            object ViewInvCTCard: TcxGridDBColumn
              Caption = #1058#1050
              DataBinding.FieldName = 'TCard'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmO.imState
              Properties.Items = <
                item
                  Description = #1053#1077#1090
                  Value = 0
                end
                item
                  Description = #1045#1089#1090#1100
                  ImageIndex = 11
                  Value = 1
                end>
              Options.Editing = False
            end
            object ViewInvCSumInTO: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1058#1054' ('#1094#1077#1085#1099' '#1079#1072#1082#1091#1087#1072')'
              DataBinding.FieldName = 'SumInTO'
            end
            object ViewInvCSumInTODif: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1072#1079#1085#1080#1094#1072' '#1089' '#1058#1054
              DataBinding.FieldName = 'SumInTODif'
              Options.Editing = False
            end
            object ViewInvCPriceIn0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1088#1072#1089#1095'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn0'
              Options.Editing = False
            end
            object ViewInvCSumIn0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1095'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn0'
              Options.Editing = False
            end
            object ViewInvCPriceInF0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1092#1072#1082#1090' ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceInF0'
              Options.Editing = False
            end
            object ViewInvCSumInF0: TcxGridDBColumn
              Caption = 'C'#1091#1084#1084#1072' '#1092#1072#1082#1090' ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumInF0'
              Options.Editing = False
            end
            object ViewInvCSumInDif0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1072#1079#1085#1080#1094#1072' ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumInDif0'
              Options.Editing = False
            end
            object ViewInvCNDSProc: TcxGridDBColumn
              Caption = #1053#1044#1057
              DataBinding.FieldName = 'NDSProc'
              Options.Editing = False
            end
            object ViewInvCSumNDS: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057
              DataBinding.FieldName = 'SumNDS'
              Options.Editing = False
            end
          end
          object LevelInvC: TcxGridLevel
            GridView = ViewInvC
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1050#1072#1083#1100#1082#1091#1083#1103#1094#1080#1103
        ImageIndex = 2
        object GridBC: TcxGrid
          Left = 0
          Top = 0
          Width = 863
          Height = 471
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewBC: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = dmORep.dsCalcB
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SUMIN0'
                Column = ViewBCSUMIN0
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMIN0'
                Column = ViewBCSUMIN0
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewBCID: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1087#1086#1079#1080#1094#1080#1080
              DataBinding.FieldName = 'ID'
            end
            object ViewBCCODEB: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'CODEB'
            end
            object ViewBCNAMEB: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'NAMEB'
              Width = 200
            end
            object ViewBCQUANT: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'QUANT'
            end
            object ViewBCIDCARD: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'IDCARD'
            end
            object ViewBCNAMEC: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'NAMEC'
              Width = 200
            end
            object ViewBCSB: TcxGridDBColumn
              Caption = #1058#1080#1087
              DataBinding.FieldName = 'SB'
            end
            object ViewBCQUANTC: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANTC'
            end
            object ViewBCPRICEIN: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
              DataBinding.FieldName = 'PRICEIN'
            end
            object ViewBCPRICEIN0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PRICEIN0'
            end
            object ViewBCSUMIN: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
              DataBinding.FieldName = 'SUMIN'
            end
            object ViewBCIM: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'IM'
            end
            object ViewBCSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              Width = 50
            end
            object ViewBCSUMIN0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SUMIN0'
            end
          end
          object LevelBC: TcxGridLevel
            GridView = ViewBC
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 703
    Width = 1036
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label2: TLabel
      Left = 336
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Label2'
      Visible = False
    end
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 113
      Height = 25
      Action = acSaveInv
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'    Ctrl+S'
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 184
      Top = 8
      Width = 113
      Height = 25
      Action = acExit
      Caption = #1042#1099#1093#1086#1076'   F10'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 744
    Width = 1036
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1036
    Height = 125
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 44
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label15: TLabel
      Left = 264
      Top = 44
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object Label7: TLabel
      Left = 24
      Top = 72
      Width = 63
      Height = 13
      Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077
      Transparent = True
    end
    object Label8: TLabel
      Left = 24
      Top = 100
      Width = 67
      Height = 13
      Caption = #1044#1077#1090#1072#1083#1100#1085#1086#1089#1090#1100
      Transparent = True
    end
    object Label9: TLabel
      Left = 764
      Top = 56
      Width = 236
      Height = 13
      Caption = #1055#1077#1095#1072#1090#1100' '#1076#1086#1089#1090#1091#1087#1085#1072' '#1090#1086#1083#1100#1082#1086' '#1074' '#1088#1077#1078#1080#1084#1077' '#1087#1088#1086#1089#1084#1086#1090#1088#1072'.'
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 121
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 40
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 145
    end
    object cxButton3: TcxButton
      Left = 760
      Top = 4
      Width = 113
      Height = 41
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 3
      OnClick = cxButton3Click
      Glyph.Data = {
        86070000424D86070000000000003600000028000000180000001A0000000100
        1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
        B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
        A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
        85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
        C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
        A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
        F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
        C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
        FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
        ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
        C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
        FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
        D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
        87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
        E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
        B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
        F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
        C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
        C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
        E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
        FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
        E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
        B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
        D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
        B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
        D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
        B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
        FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
        F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
        C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
        FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
        F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
        D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
    object cxRadioButton1: TcxRadioButton
      Left = 568
      Top = 12
      Width = 113
      Height = 17
      Caption = #1048#1085#1074'. '#1074#1077#1076#1086#1084#1086#1089#1090#1100
      Checked = True
      TabOrder = 4
      TabStop = True
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
    object cxRadioButton2: TcxRadioButton
      Left = 568
      Top = 44
      Width = 177
      Height = 17
      Caption = #1048#1085#1074'. '#1074#1077#1076#1086#1084#1086#1089#1090#1100' ('#1087#1091#1089#1090#1086#1081' '#1092#1072#1082#1090')'
      TabOrder = 5
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
    object cxRadioButton3: TcxRadioButton
      Left = 568
      Top = 60
      Width = 113
      Height = 17
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      TabOrder = 6
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
    object cxTextEdit2: TcxTextEdit
      Left = 112
      Top = 66
      Properties.MaxLength = 150
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 7
      Text = 'cxTextEdit2'
      Width = 441
    end
    object cxCheckBox2: TcxCheckBox
      Left = 432
      Top = 12
      Caption = #1057#1091#1084#1084#1086#1074#1086#1081' '#1091#1095#1077#1090
      TabOrder = 8
      Width = 105
    end
    object cxComboBox1: TcxComboBox
      Left = 112
      Top = 94
      Properties.Items.Strings = (
        #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1086
        #1042#1089#1103' '#1082#1072#1088#1090#1086#1090#1077#1082#1072)
      Style.Shadow = True
      TabOrder = 9
      Text = #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1086
      Width = 149
    end
    object cxRadioButton4: TcxRadioButton
      Left = 568
      Top = 28
      Width = 113
      Height = 17
      Caption = #1048#1085#1074'. '#1091#1085#1080#1092'. '#1092#1086#1088#1084#1072
      TabOrder = 10
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
    object cxRadioButton5: TcxRadioButton
      Left = 568
      Top = 76
      Width = 165
      Height = 17
      Caption = #1057#1083#1080#1095#1080#1090#1077#1083#1100#1085#1072#1103' '#1048#1053#1042'-19'
      TabOrder = 11
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 125
    Width = 161
    Height = 578
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label3: TLabel
      Left = 8
      Top = 144
      Width = 107
      Height = 13
      Cursor = crHandPoint
      Caption = '1. '#1056#1072#1089#1095#1080#1090#1072#1090#1100' '#1086#1089#1090#1072#1090#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label3Click
    end
    object Label4: TLabel
      Left = 8
      Top = 216
      Width = 123
      Height = 13
      Cursor = crHandPoint
      Caption = #1059#1088#1072#1074#1085#1103#1090#1100' ('#1092#1072#1082#1090'='#1088#1072#1089#1095#1077#1090')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label4Click
    end
    object Label6: TLabel
      Left = 8
      Top = 232
      Width = 75
      Height = 13
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1072#1082#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label6Click
    end
    object cxLabel1: TcxLabel
      Left = 8
      Top = 8
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 80
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'   F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel7: TcxLabel
      Left = 8
      Top = 24
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'  Ctrl+Ins '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel7Click
    end
    object cxLabel8: TcxLabel
      Left = 8
      Top = 48
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1089#1077' ('#1086#1089#1090#1072#1083#1100#1085#1099#1077')'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel8Click
    end
    object cxLabel9: TcxLabel
      Left = 8
      Top = 96
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100'     Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel9Click
    end
    object cxCheckBox1: TcxCheckBox
      Left = 8
      Top = 168
      Caption = #1087#1077#1088#1077#1089#1095'-'#1090#1100' '#1094#1077#1085#1099' '#1092#1072#1082#1090#1072
      TabOrder = 5
      Width = 137
    end
    object PBar1: TcxProgressBar
      Left = 12
      Top = 384
      ParentColor = False
      Position = 100.000000000000000000
      Properties.BarBevelOuter = cxbvRaised
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = clBlue
      Properties.EndColor = 16745090
      Properties.PeakValue = 100.000000000000000000
      Style.Color = clWhite
      TabOrder = 6
      Visible = False
      Width = 133
    end
    object cxTextEdit3: TcxTextEdit
      Left = 12
      Top = 280
      BeepOnEnter = False
      Properties.OnChange = cxTextEdit3PropertiesChange
      TabOrder = 7
      Text = 'cxTextEdit3'
      OnKeyDown = cxTextEdit3KeyDown
      Width = 137
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 384
    Top = 176
  end
  object taSpec: TClientDataSet
    Aggregates = <>
    FileName = 'SpecInv.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecIndex1'
        Fields = 'NameGr;NameG'
        Options = [ixCaseInsensitive]
      end
      item
        Name = 'taSpecIndex2'
        Fields = 'Num'
      end
      item
        Name = 'taSpecIndex3'
        Fields = 'IdGoods'
      end>
    IndexName = 'taSpecIndex2'
    Params = <>
    StoreDefs = True
    BeforePost = taSpecBeforePost
    Left = 296
    Top = 176
    object taSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecSM: TStringField
      FieldName = 'SM'
    end
    object taSpecQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpecPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecQuantFact: TFloatField
      FieldName = 'QuantFact'
      OnChange = taSpecQuantFactChange
      DisplayFormat = '0.000'
    end
    object taSpecPriceInF: TFloatField
      FieldName = 'PriceInF'
      OnChange = taSpecPriceInFChange
      DisplayFormat = '0.00'
    end
    object taSpecSumInF: TFloatField
      FieldName = 'SumInF'
      OnChange = taSpecSumInFChange
      DisplayFormat = '0.00'
    end
    object taSpecPriceUchF: TFloatField
      FieldName = 'PriceUchF'
      OnChange = taSpecPriceUchFChange
      DisplayFormat = '0.00'
    end
    object taSpecSumUchF: TFloatField
      FieldName = 'SumUchF'
      OnChange = taSpecSumUchFChange
      DisplayFormat = '0.00'
    end
    object taSpecQuantDif: TFloatField
      FieldName = 'QuantDif'
      DisplayFormat = '0.000'
    end
    object taSpecSumInDif: TFloatField
      FieldName = 'SumInDif'
      DisplayFormat = '0.00'
    end
    object taSpecSumUchDif: TFloatField
      FieldName = 'SumUchDif'
      DisplayFormat = '0.00'
    end
    object taSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecTCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpecId_Group: TIntegerField
      FieldName = 'Id_Group'
    end
    object taSpecNoCalc: TSmallintField
      FieldName = 'NoCalc'
    end
    object taSpecSumInTO: TFloatField
      FieldName = 'SumInTO'
      OnChange = taSpecSumInTOChange
      DisplayFormat = '0.00'
    end
    object taSpecSumInTODif: TFloatField
      FieldName = 'SumInTODif'
      DisplayFormat = '0.00'
    end
    object taSpecNameGr: TStringField
      FieldName = 'NameGr'
      Size = 50
    end
    object taSpecNameGr2: TStringField
      FieldName = 'NameGr2'
      Size = 50
    end
    object taSpecCType: TSmallintField
      FieldName = 'CType'
    end
    object taSpecPriceIn0: TFloatField
      FieldName = 'PriceIn0'
    end
    object taSpecSumIn0: TFloatField
      FieldName = 'SumIn0'
    end
    object taSpecPriceInF0: TFloatField
      FieldName = 'PriceInF0'
    end
    object taSpecSumInF0: TFloatField
      FieldName = 'SumInF0'
    end
    object taSpecSumInDif0: TFloatField
      FieldName = 'SumInDif0'
    end
    object taSpecNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object taSpecSumNDS: TFloatField
      FieldName = 'SumNDS'
    end
  end
  object dsSpec: TDataSource
    DataSet = taSpec
    Left = 296
    Top = 232
  end
  object amInv: TActionManager
    Left = 196
    Top = 324
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acSaveInv: TAction
      Caption = 'acSaveInv'
      ShortCut = 16467
      OnExecute = acSaveInvExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acCalc1: TAction
      Caption = 'acCalc1'
      ShortCut = 16505
      OnExecute = acCalc1Execute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acEquals: TAction
      Caption = 'acEquals'
      ShortCut = 49233
      OnExecute = acEqualsExecute
    end
    object acRefreshGr: TAction
      Caption = 'acRefreshGr'
      ShortCut = 49234
      OnExecute = acRefreshGrExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
    object acFindWord: TAction
      Caption = 'acFindWord'
      ShortCut = 16454
      OnExecute = acFindWordExecute
    end
    object acFastSave: TAction
      Caption = 'acFastSave'
      ShortCut = 16449
      OnExecute = acFastSaveExecute
    end
  end
  object taSpecC: TClientDataSet
    Aggregates = <>
    FileName = 'SpecCInv.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecCIndex1'
        Fields = 'NameGr;NameG'
        Options = [ixCaseInsensitive]
      end
      item
        Name = 'taSpecCIndex2'
        Fields = 'Num'
      end>
    IndexName = 'taSpecCIndex1'
    Params = <>
    StoreDefs = True
    BeforePost = taSpecCBeforePost
    Left = 472
    Top = 176
    object taSpecCNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecCIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecCNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecCIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecCSM: TStringField
      FieldName = 'SM'
    end
    object taSpecCQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpecCPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecCSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecCPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecCSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecCQuantFact: TFloatField
      FieldName = 'QuantFact'
      OnChange = taSpecCQuantFactChange
      DisplayFormat = '0.000'
    end
    object taSpecCPriceInF: TFloatField
      FieldName = 'PriceInF'
      DisplayFormat = '0.00'
    end
    object taSpecCSumInF: TFloatField
      FieldName = 'SumInF'
      DisplayFormat = '0.00'
    end
    object taSpecCPriceUchF: TFloatField
      FieldName = 'PriceUchF'
      DisplayFormat = '0.00'
    end
    object taSpecCSumUchF: TFloatField
      FieldName = 'SumUchF'
      DisplayFormat = '0.00'
    end
    object taSpecCQuantDif: TFloatField
      FieldName = 'QuantDif'
      DisplayFormat = '0.000'
    end
    object taSpecCSumInDif: TFloatField
      FieldName = 'SumInDif'
      DisplayFormat = '0.00'
    end
    object taSpecCSumUchDif: TFloatField
      FieldName = 'SumUchDif'
      DisplayFormat = '0.00'
    end
    object taSpecCKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecCTCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpecCId_Group: TIntegerField
      FieldName = 'Id_Group'
    end
    object taSpecCNameGr: TStringField
      FieldName = 'NameGr'
      Size = 100
    end
    object taSpecCSumInTO: TFloatField
      FieldName = 'SumInTO'
      OnChange = taSpecCSumInTOChange
      DisplayFormat = '0.00'
    end
    object taSpecCSumInTODif: TFloatField
      FieldName = 'SumInTODif'
      DisplayFormat = '0.00'
    end
    object taSpecCCType: TSmallintField
      FieldName = 'CType'
    end
    object taSpecCPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      DisplayFormat = '0.00'
    end
    object taSpecCSumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
    object taSpecCPriceInF0: TFloatField
      FieldName = 'PriceInF0'
      DisplayFormat = '0.00'
    end
    object taSpecCSumInF0: TFloatField
      FieldName = 'SumInF0'
      DisplayFormat = '0.00'
    end
    object taSpecCSumInDif0: TFloatField
      FieldName = 'SumInDif0'
      DisplayFormat = '0.00'
    end
    object taSpecCNDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.0'
    end
    object taSpecCSumNDS: TFloatField
      FieldName = 'SumNDS'
      DisplayFormat = '0.00'
    end
  end
  object dsSpecC: TDataSource
    DataSet = taSpecC
    Left = 472
    Top = 232
  end
  object RepInv: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    OnUserFunction = RepInvUserFunction
    Left = 191
    Top = 182
    ReportForm = {19000000}
  end
  object frdsSpec: TfrDBDataSet
    DataSource = dsSpec
    Left = 191
    Top = 238
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 555
    Top = 194
    object N1: TMenuItem
      Caption = #1042#1089#1077' '#1085#1077' '#1088#1072#1089#1082#1083#1072#1076#1099#1074#1072#1090#1100' '#1085#1072' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1080#1077
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1089#1077' '#1088#1072#1089#1082#1083#1072#1076#1099#1074#1072#1090#1100' '#1085#1072' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1080#1077
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
  object frdsSpecC: TfrDBDataSet
    DataSource = dsSpecC
    Left = 399
    Top = 238
  end
  object taR: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'Code'
    Left = 235
    Top = 326
    object taRCode: TIntegerField
      FieldName = 'Code'
    end
    object taRPrice: TFloatField
      FieldName = 'Price'
    end
    object taRPrice0: TFloatField
      FieldName = 'Price0'
    end
  end
  object taSpec1: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'Num'
    BeforePost = taSpec1BeforePost
    Left = 295
    Top = 290
    object taSpec1Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpec1IdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpec1NameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpec1IM: TSmallintField
      FieldName = 'IM'
    end
    object taSpec1SM: TStringField
      FieldName = 'SM'
    end
    object taSpec1Quant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpec1PriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpec1SumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpec1PriceIn0: TFloatField
      FieldName = 'PriceIn0'
      DisplayFormat = '0.00'
    end
    object taSpec1SumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
    object taSpec1PriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpec1SumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpec1QuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taSpec1PriceInF: TFloatField
      FieldName = 'PriceInF'
      OnChange = taSpec1PriceInFChange
      DisplayFormat = '0.00'
    end
    object taSpec1SumInF: TFloatField
      FieldName = 'SumInF'
      OnChange = taSpec1SumInFChange
      DisplayFormat = '0.00'
    end
    object taSpec1PriceInF0: TFloatField
      FieldName = 'PriceInF0'
      OnChange = taSpec1PriceInF0Change
      DisplayFormat = '0.00'
    end
    object taSpec1SumInF0: TFloatField
      FieldName = 'SumInF0'
      OnChange = taSpec1SumInF0Change
      DisplayFormat = '0.00'
    end
    object taSpec1PriceUchF: TFloatField
      FieldName = 'PriceUchF'
      DisplayFormat = '0.00'
    end
    object taSpec1SumUchF: TFloatField
      FieldName = 'SumUchF'
      DisplayFormat = '0.00'
    end
    object taSpec1QuantDif: TFloatField
      FieldName = 'QuantDif'
      DisplayFormat = '0.000'
    end
    object taSpec1SumInDif: TFloatField
      FieldName = 'SumInDif'
      DisplayFormat = '0.00'
    end
    object taSpec1SumInDif0: TFloatField
      FieldName = 'SumInDif0'
      DisplayFormat = '0.00'
    end
    object taSpec1SumUchDif: TFloatField
      FieldName = 'SumUchDif'
      DisplayFormat = '0.00'
    end
    object taSpec1Km: TFloatField
      FieldName = 'Km'
      DisplayFormat = '0.000'
    end
    object taSpec1TCard: TSmallintField
      FieldName = 'TCard'
    end
    object taSpec1NoCalc: TSmallintField
      FieldName = 'NoCalc'
    end
    object taSpec1SumInTO: TFloatField
      FieldName = 'SumInTO'
      DisplayFormat = '0.00'
    end
    object taSpec1SumInTODif: TFloatField
      FieldName = 'SumInTODif'
      DisplayFormat = '0.00'
    end
    object taSpec1CType: TSmallintField
      FieldName = 'CType'
    end
    object taSpec1NDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.00'
    end
    object taSpec1SumNDS: TFloatField
      FieldName = 'SumNDS'
      DisplayFormat = '0.00'
    end
    object taSpec1IdGroup: TIntegerField
      FieldName = 'IdGroup'
    end
  end
  object taSpecC1: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'Num'
    BeforePost = taSpecC1BeforePost
    Left = 475
    Top = 290
    object taSpecC1Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpecC1IdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecC1NameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecC1IM: TSmallintField
      FieldName = 'IM'
    end
    object taSpecC1SM: TStringField
      FieldName = 'SM'
    end
    object taSpecC1Quant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpecC1PriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecC1PriceIn0: TFloatField
      FieldName = 'PriceIn0'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
    object taSpecC1PriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecC1QuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taSpecC1PriceInF: TFloatField
      FieldName = 'PriceInF'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumInF: TFloatField
      FieldName = 'SumInF'
      DisplayFormat = '0.00'
    end
    object taSpecC1PriceInF0: TFloatField
      FieldName = 'PriceInF0'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumInF0: TFloatField
      FieldName = 'SumInF0'
      DisplayFormat = '0.00'
    end
    object taSpecC1PriceUchF: TFloatField
      FieldName = 'PriceUchF'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumUchF: TFloatField
      FieldName = 'SumUchF'
      DisplayFormat = '0.00'
    end
    object taSpecC1QuantDif: TFloatField
      FieldName = 'QuantDif'
      DisplayFormat = '0.000'
    end
    object taSpecC1SumInDif: TFloatField
      FieldName = 'SumInDif'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumInDif0: TFloatField
      FieldName = 'SumInDif0'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumUchDif: TFloatField
      FieldName = 'SumUchDif'
      DisplayFormat = '0.00'
    end
    object taSpecC1Km: TFloatField
      FieldName = 'Km'
      DisplayFormat = '0.000'
    end
    object taSpecC1TCard: TSmallintField
      FieldName = 'TCard'
    end
    object taSpecC1SumInTO: TFloatField
      FieldName = 'SumInTO'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumInTODif: TFloatField
      FieldName = 'SumInTODif'
      DisplayFormat = '0.00'
    end
    object taSpecC1CType: TFloatField
      FieldName = 'CType'
    end
    object taSpecC1NDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.00'
    end
    object taSpecC1SumNDS: TFloatField
      FieldName = 'SumNDS'
      DisplayFormat = '0.00'
    end
    object taSpecC1IdGroup: TIntegerField
      FieldName = 'IdGroup'
    end
  end
end
