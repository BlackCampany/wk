unit DocsVn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocs4 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsVn: TcxGrid;
    ViewDocsVn: TcxGridDBTableView;
    LevelDocsVn: TcxGridLevel;
    amDocsVn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc4: TAction;
    acEditDoc4: TAction;
    acViewDoc4: TAction;
    acDelDoc4: TAction;
    acOnDoc4: TAction;
    acOffDoc4: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewDocsVnDepart: TcxGridDBColumn;
    ViewDocsVnDateInvoice: TcxGridDBColumn;
    ViewDocsVnNumber: TcxGridDBColumn;
    ViewDocsVnFlowDepart: TcxGridDBColumn;
    ViewDocsVnSummaTovarMove: TcxGridDBColumn;
    ViewDocsVnSummaTara: TcxGridDBColumn;
    ViewDocsVnNameD1: TcxGridDBColumn;
    ViewDocsVnNameD2: TcxGridDBColumn;
    ViewDocsVnAcStatusP: TcxGridDBColumn;
    ViewDocsVnSummaTovarNew: TcxGridDBColumn;
    acAdd1: TAction;
    ViewDocsVnProvodTypeP: TcxGridDBColumn;
    acViewPrihoh: TAction;
    N5: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc4Execute(Sender: TObject);
    procedure acEditDoc4Execute(Sender: TObject);
    procedure acViewDoc4Execute(Sender: TObject);
    procedure ViewDocsVnDblClick(Sender: TObject);
    procedure acDelDoc4Execute(Sender: TObject);
    procedure acOnDoc4Execute(Sender: TObject);
    procedure acOffDoc4Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acAdd1Execute(Sender: TObject);
    procedure ViewDocsVnCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acViewPrihohExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc4(iT,iTd:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs4: TfmDocs4;
  bClearDocVn:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, AddDoc4, MT, DocsVnSel, u2fdk, DocsIn;

{$R *.dfm}

procedure TfmDocs4.prSetValsAddDoc4(iT,iTd:SmallINt); //0-����������, 1-��������������, 2-��������, 3-����� �������������� � ����������
Var iNum:INteger;
begin
  with dmMC do     //iTd - ��� ������������� ��������� - ��������� 3  ��������� ���� = 0
  begin
    bOpen:=True;

    fmAddDoc4.ViewDoc4Cena.Options.Editing:=False;
    fmAddDoc4.ViewDoc4SumCena.Options.Editing:=False;
    fmAddDoc4.ViewDoc4NewCena.Options.Editing:=False;
    fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=False;

    if (iT<2)and(iTd<>3) then
    begin
      if CommonSet.Single=1 then
      begin
        fmAddDoc4.ViewDoc4Cena.Options.Editing:=True;
        fmAddDoc4.ViewDoc4SumCena.Options.Editing:=True;
        fmAddDoc4.ViewDoc4NewCena.Options.Editing:=True;
        fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=True;
      end;
    end;

    if iT=0 then
    begin
      iNum:=prFindNum(3)+1;

      fmAddDoc4.Caption:='���������: ����������.';

      if iNum>0 then
      begin
        fmAddDoc4.cxTextEdit1.Text:=its(iNum);
        fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc4.cxTextEdit1.Tag:=0;
        fmAddDoc4.Label1.Tag:=iNum;
      end else
      begin
        fmAddDoc4.cxTextEdit1.Text:='';
        fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc4.cxTextEdit1.Tag:=0;
      end;

      fmAddDoc4.cxTextEdit10.Text:='';
      fmAddDoc4.cxTextEdit10.Tag:=0;
      fmAddDoc4.cxDateEdit10.Date:=0;
      fmAddDoc4.cxDateEdit10.Tag:=0;

      fmAddDoc4.cxDateEdit1.Date:=date;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True; quDeparts.First;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quDepartsID.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quDepartsName.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc4.cxLookupComboBox2.EditValue:=quDepartsID.AsInteger;
      fmAddDoc4.cxLookupComboBox2.Text:=quDepartsName.AsString;
      fmAddDoc4.cxLookupComboBox2.Properties.ReadOnly:=False;

      fmAddDoc4.cxLabel1.Enabled:=True;
      fmAddDoc4.cxLabel2.Enabled:=True;
      fmAddDoc4.cxLabel3.Enabled:=True;
      fmAddDoc4.cxLabel4.Enabled:=True;
      fmAddDoc4.cxButton1.Enabled:=True;

      fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;
      fmAddDoc4.ViewDoc4Kol.Options.Editing:=True;

      fmAddDoc4.ViewDoc4NewCena.Options.Editing:=False;
      if CommonSet.Single=1 then fmAddDoc4.ViewDoc4NewCena.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=False;
      if CommonSet.Single=1 then fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=True;

      if (iTd=3) then   //��� ����������� ����������
      begin
        fmAddDoc4.acAddPos.Enabled:=False;
        fmAddDoc4.acAddList.Enabled:=False;
        fmAddDoc4.acReadBar.Enabled:=False;
        fmAddDoc4.acReadBar1.Enabled:=False;
        fmAddDoc4.acSetPrice.Enabled:=False;
        fmAddDoc4.acAddPosIn.Enabled:=False;
        fmAddDoc4.acAddPosOut.Enabled:=False;

        fmAddDoc4.cxLabel5.Enabled:=False;
        fmAddDoc4.cxLabel7.Enabled:=False;
        fmAddDoc4.cxLabel10.Enabled:=False;
        fmAddDoc4.cxLabel11.Enabled:=False;
        fmAddDoc4.cxLabel6.Enabled:=False;


        if pos('Torg',Person.Name)>0 then fmAddDoc4.ViewDoc4QuantM.Options.Editing:=False
        else fmAddDoc4.ViewDoc4QuantM.Options.Editing:=True;

        if pos('Torg',Person.Name)>0 then fmAddDoc4.ViewDoc4Kol.Options.Editing:=True
        else fmAddDoc4.ViewDoc4Kol.Options.Editing:=False;

        fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;

      end else
      begin
        fmAddDoc4.acAddPos.Enabled:=True;
        fmAddDoc4.acAddList.Enabled:=True;
        fmAddDoc4.acReadBar.Enabled:=True;
        fmAddDoc4.acReadBar1.Enabled:=True;
        fmAddDoc4.acSetPrice.Enabled:=True;
        fmAddDoc4.acAddPosIn.Enabled:=True;
        fmAddDoc4.acAddPosOut.Enabled:=True;

        fmAddDoc4.cxLabel5.Enabled:=True;
        fmAddDoc4.cxLabel7.Enabled:=True;
        fmAddDoc4.cxLabel10.Enabled:=True;
        fmAddDoc4.cxLabel11.Enabled:=True;
        fmAddDoc4.cxLabel6.Enabled:=True;
      end;

    end;
    if iT=1 then
    begin
      fmAddDoc4.Caption:='���������: ��������������.';
      fmAddDoc4.cxTextEdit1.Text:=quTTNVnNumber.AsString;
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc4.cxTextEdit1.Tag:=quTTnVnID.AsInteger;

      fmAddDoc4.cxTextEdit10.Text:=quTTnVnNumber.AsString;
      fmAddDoc4.cxTextEdit10.Tag:=quTTnVnID.AsInteger;
      fmAddDoc4.cxDateEdit10.Date:=quTTnVnDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit10.Tag:=quTTnVnDepart.AsInteger;

      fmAddDoc4.cxDateEdit1.Date:=quTTnVnDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quTTnVnDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quTTnVnNameD1.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc4.cxLookupComboBox2.EditValue:=quTTnVnFlowDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox2.Text:=quTTnVnNameD2.AsString;
      fmAddDoc4.cxLookupComboBox2.Properties.ReadOnly:=False;

      fmAddDoc4.cxLabel1.Enabled:=True;
      fmAddDoc4.cxLabel2.Enabled:=True;
      fmAddDoc4.cxLabel4.Enabled:=True;
      fmAddDoc4.cxLabel3.Enabled:=True;
      fmAddDoc4.cxButton1.Enabled:=True;

      fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;
      fmAddDoc4.ViewDoc4Kol.Options.Editing:=True;

      fmAddDoc4.ViewDoc4NewCena.Options.Editing:=False;
      if CommonSet.Single=1 then fmAddDoc4.ViewDoc4NewCena.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=False;
      if CommonSet.Single=1 then fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=True;

      if (iTd=3) then   //��� ����������� ����������
      begin
        fmAddDoc4.acAddPos.Enabled:=False;
        fmAddDoc4.acAddList.Enabled:=False;
        fmAddDoc4.acReadBar.Enabled:=False;
        fmAddDoc4.acReadBar1.Enabled:=False;
        fmAddDoc4.acSetPrice.Enabled:=False;
        fmAddDoc4.acAddPosIn.Enabled:=False;
        fmAddDoc4.acAddPosOut.Enabled:=False;

        fmAddDoc4.cxLabel5.Enabled:=False;
        fmAddDoc4.cxLabel7.Enabled:=False;
        fmAddDoc4.cxLabel10.Enabled:=False;
        fmAddDoc4.cxLabel11.Enabled:=False;
        fmAddDoc4.cxLabel6.Enabled:=False;

        if pos('Torg',Person.Name)>0 then fmAddDoc4.ViewDoc4QuantM.Options.Editing:=False
        else fmAddDoc4.ViewDoc4QuantM.Options.Editing:=True;

        if pos('Torg',Person.Name)>0 then fmAddDoc4.ViewDoc4Kol.Options.Editing:=True
        else fmAddDoc4.ViewDoc4Kol.Options.Editing:=False;

        fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;
      end else
      begin
        fmAddDoc4.acAddPos.Enabled:=True;
        fmAddDoc4.acAddList.Enabled:=True;
        fmAddDoc4.acReadBar.Enabled:=True;
        fmAddDoc4.acReadBar1.Enabled:=True;
        fmAddDoc4.acSetPrice.Enabled:=True;
        fmAddDoc4.acAddPosIn.Enabled:=True;
        fmAddDoc4.acAddPosOut.Enabled:=True;

        fmAddDoc4.cxLabel5.Enabled:=True;
        fmAddDoc4.cxLabel7.Enabled:=True;
        fmAddDoc4.cxLabel10.Enabled:=True;
        fmAddDoc4.cxLabel11.Enabled:=True;
        fmAddDoc4.cxLabel6.Enabled:=True;
      end;
    end;
    if iT=2 then
    begin
      fmAddDoc4.Caption:='���������: ��������.';

        fmAddDoc4.acAddPos.Enabled:=False;
        fmAddDoc4.acAddList.Enabled:=False;
        fmAddDoc4.acReadBar.Enabled:=False;
        fmAddDoc4.acReadBar1.Enabled:=False;
        fmAddDoc4.acSetPrice.Enabled:=False;
        fmAddDoc4.acAddPosIn.Enabled:=False;
        fmAddDoc4.acAddPosOut.Enabled:=False;

        fmAddDoc4.cxLabel5.Enabled:=False;
        fmAddDoc4.cxLabel7.Enabled:=False;
        fmAddDoc4.cxLabel10.Enabled:=False;
        fmAddDoc4.cxLabel11.Enabled:=False;
        fmAddDoc4.cxLabel6.Enabled:=False;


      fmAddDoc4.cxTextEdit1.Text:=quTTNVnNumber.AsString;
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxTextEdit1.Tag:=quTTnVnID.AsInteger;

      fmAddDoc4.cxTextEdit10.Text:=quTTnVnNumber.AsString;
      fmAddDoc4.cxTextEdit10.Tag:=quTTnVnID.AsInteger;
      fmAddDoc4.cxDateEdit10.Date:=quTTnVnDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit10.Tag:=quTTnVnDepart.AsInteger;

      fmAddDoc4.cxDateEdit1.Date:=quTTnVnDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=True;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quTTnVnDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quTTnVnNameD1.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc4.cxLookupComboBox2.EditValue:=quTTnVnFlowDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox2.Text:=quTTnVnNameD2.AsString;
      fmAddDoc4.cxLookupComboBox2.Properties.ReadOnly:=True;

      fmAddDoc4.cxLabel1.Enabled:=False;
      fmAddDoc4.cxLabel2.Enabled:=False;
      fmAddDoc4.cxLabel3.Enabled:=False;
      fmAddDoc4.cxLabel4.Enabled:=False;
      fmAddDoc4.cxButton1.Enabled:=False;

      fmAddDoc4.ViewDoc4.OptionsData.Editing:=False;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;

      fmAddDoc4.ViewDoc4NewCena.Options.Editing:=False;
      fmAddDoc4.ViewDoc4SumNewCena.Options.Editing:=False;
    end;

    bOpen:=False;
  end;
end;

procedure TfmDocs4.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs4.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs4.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsVn.Align:=AlClient;
  ViewDocsVn.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsVn.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs4.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelDocsVn.Visible then
      begin
        fmDocs4.Caption:='��������� �����������  �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs4.ViewDocsVn.BeginUpdate;
        try
          quTTNVn.Active:=False;
          quTTNVn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNVn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTNVn.Active:=True;
        finally
          fmDocs4.ViewDocsVn.EndUpdate;
        end;

      end else
      begin
        fmDocs4.Caption:='����������� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        ViewCards.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
        quDocsInCard.Active:=True;}
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;            

procedure TfmDocs4.acAddDoc4Execute(Sender: TObject);
Var iTp:INteger;
//    rSum1,rSum2:Real;
    iDepSpis:Integer;
begin
  //�������� ��������

  if not CanDo('prAddDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMt do
  begin
    fmSelTVn.ShowModal;
    if fmSelTVn.ModalResult=mrOk then
    begin
      prSetValsAddDoc4(0,0); //0-����������, 1-��������������, 2-��������
      iTp:=1;
      if fmSelTVn.cxRadioButton1.Checked then iTp:=1;
      if fmSelTVn.cxRadioButton2.Checked then iTp:=2;

      if iTp=1 then
      begin
          fmAddDoc4.Label3.Caption:='������������';
      end;
      if iTp=2 then
      begin
        fmAddDoc4.Label3.Caption:='��������';
        if ptDep.Active=False then ptDep.Active:=True;
        ptDep.First;  iDepSpis:=0;
        while not ptDep.Eof do
        begin
          if pos('����',OemToAnsiConvert(ptDepName.AsString))>0 then  iDepSpis:=ptDepID.AsInteger;
          ptDep.Next;
        end;
        if iDepSpis>0 then fmAddDoc4.cxLookupComboBox2.EditValue:=iDepSpis;
      end;

      fmAddDoc4.Tag:=iTp;

      CloseTe(fmAddDoc4.taSpecVn);
      fmAddDoc4.acSaveDoc.Enabled:=True;
      fmAddDoc4.Show;
    end;
  end;
end;

procedure TfmDocs4.acEditDoc4Execute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnVn.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnVnAcStatusP.AsInteger=0 then
      begin
        if quTTnVnDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnVnDepart.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;



        if quTTnVnProvodTypeP.AsInteger=1 then
        begin
          fmAddDoc4.Label3.Caption:='������������';
          fmAddDoc4.Tag:=1;
          prSetValsAddDoc4(1,0); //0-����������, 1-��������������, 2-��������
        end;

        if quTTnVnProvodTypeP.AsInteger=2 then
        begin
          fmAddDoc4.Label3.Caption:='��������';
          fmAddDoc4.Tag:=2;
          prSetValsAddDoc4(1,0); //0-����������, 1-��������������, 2-��������
        end;

        if quTTnVnProvodTypeP.AsInteger=3 then
        begin
          fmAddDoc4.Label3.Caption:='�������� � �������';
          fmAddDoc4.Tag:=3;
          prSetValsAddDoc4(1,3); //0-����������, 1-��������������, 2-��������
        end;

        fmAddDoc4.Tag:=quTTnVnProvodTypeP.AsInteger;

        CloseTe(fmAddDoc4.taSpecVn);

        fmAddDoc4.acSaveDoc.Enabled:=True;

//        IDH:=quTTnVnID.AsInteger;

        quSpecVn.Active:=False;
        quSpecVn.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
        quSpecVn.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
        quSpecVn.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
        quSpecVn.Active:=True;

        quSpecVn.First;
//        iC:=1;
        iCol:=0;
        while not quSpecVn.Eof do
        begin
          with fmAddDoc4 do
          begin
            taSpecVn.Append;
            taSpecVnNum.AsInteger:=quSpecVnCodeGroup.AsInteger;
            taSpecVnId.AsInteger:=quSpecVnCodePodgr.AsInteger;
            taSpecVnCodeTovar.AsInteger:=quSpecVnCodeTovar.AsInteger;
            taSpecVnName.AsString:=quSpecVnName.AsString;
            taSpecVnFullName.AsString:=quSpecVnFullName.AsString;
            taSpecVnCodeEdIzm.AsInteger:=quSpecVnCodeEdIzm.AsInteger;
            taSpecVnBarCode.AsString:=quSpecVnBarCode.AsString;
//            taSpecVnKolMest.AsInteger:=quSpecVnKolMest.AsInteger;
//            taSpecVnKolWithMest.AsFloat:=quSpecVnKolWithMest.AsFloat; //quSpecVnKolEdMest.AsFloat;
            taSpecVnKol.AsFloat:=quSpecVnKol.AsFloat;
            taSpecVnNewCena.AsFloat:=quSpecVnCenaTovarMove.AsFloat;
            taSpecVnCena.AsFloat:=quSpecVnCenaTovarSpis.AsFloat;
            taSpecVnProc.AsFloat:=quSpecVnProcent.AsFloat;
            taSpecVnSumNewCena.AsFloat:=rv(quSpecVnKol.AsFloat*quSpecVnCenaTovarMove.AsFloat);
            taSpecVnSumCena.AsFloat:=rv(quSpecVnSumCenaTovarSpis.AsFloat);
            taSpecVnSumRazn.AsFloat:=rv(quSpecVnKol.AsFloat*quSpecVnCenaTovarMove.AsFloat)-quSpecVnSumCenaTovarSpis.AsFloat;

            taSpecVnCenaSS.AsFloat:=quSpecVnCenaTovarSpis.AsFloat;
            taSpecVnSumCenaSS.AsFloat:=quSpecVnSumCenaTovarSpis.AsFloat;
            taSpecVnNDSProc.AsFloat:=quSpecVnNDS.AsFloat;

            taSpecVnQuantN.AsFloat:=quSpecVnSumVesTara.AsFloat;
            if quSpecVnSumVesTara.AsFloat>0 then
              taSpecVnProcBrak.AsFloat:=rv(quSpecVnKol.AsFloat/quSpecVnSumVesTara.AsFloat*100)
            else
              taSpecVnProcBrak.AsFloat:=0;

            taSpecVnQuantM.AsFloat:=quSpecVnKolMest.AsInteger/1000;

            taSpecVn.Post;
          end;
          quSpecVn.Next;
        end;
        fmAddDoc4.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs4.acViewDoc4Execute(Sender: TObject);
begin
  //��������
  if not CanDo('prViewDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnVn.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc4(2,0); //0-����������, 1-��������������, 2-��������

      fmAddDoc4.ViewDoc4.BeginUpdate;

      if quTTnVnProvodTypeP.AsInteger=1 then
      begin
        fmAddDoc4.Label3.Caption:='������������';
        fmAddDoc4.Tag:=1;
      end;
      if quTTnVnProvodTypeP.AsInteger=2 then
      begin
        fmAddDoc4.Label3.Caption:='��������';
        fmAddDoc4.Tag:=2;
      end;

      if quTTnVnProvodTypeP.AsInteger=3 then
      begin
        fmAddDoc4.Label3.Caption:='�������� � �������';
        fmAddDoc4.Tag:=3;
      end;

      CloseTe(fmAddDoc4.taSpecVn);
      CloseTe(fmAddDoc4.teSpecIn);

      fmAddDoc4.acSaveDoc.Enabled:=True;

//        IDH:=quTTnVnID.AsInteger;

      quSpecVn.Active:=False;
      quSpecVn.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
      quSpecVn.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
      quSpecVn.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
      quSpecVn.Active:=True;

      quSpecVn.First;
      iCol:=0;

      while not quSpecVn.Eof do
      begin
        with fmAddDoc4 do
        begin
          taSpecVn.Append;
          taSpecVnNum.AsInteger:=quSpecVnCodeGroup.AsInteger;
          taSpecVnId.AsInteger:=quSpecVnCodePodgr.AsInteger;
          taSpecVnCodeTovar.AsInteger:=quSpecVnCodeTovar.AsInteger;
          taSpecVnName.AsString:=quSpecVnName.AsString;
          taSpecVnFullName.AsString:=quSpecVnFullName.AsString;
          taSpecVnCodeEdIzm.AsInteger:=quSpecVnCodeEdIzm.AsInteger;
          taSpecVnBarCode.AsString:=quSpecVnBarCode.AsString;
//          taSpecVnKolMest.AsInteger:=quSpecVnKolMest.AsInteger;
//          taSpecVnKolWithMest.AsFloat:=quSpecVnKolWithMest.AsFloat; //+quSpecVnKolEdMest.AsFloat;
          taSpecVnKol.AsFloat:=quSpecVnKol.AsFloat;
          taSpecVnNewCena.AsFloat:=quSpecVnCenaTovarMove.AsFloat;
          taSpecVnCena.AsFloat:=quSpecVnCenaTovarSpis.AsFloat;
          taSpecVnProc.AsFloat:=quSpecVnProcent.AsFloat;
          taSpecVnSumNewCena.AsFloat:=rv(quSpecVnKol.AsFloat*quSpecVnCenaTovarMove.AsFloat);
          taSpecVnSumCena.AsFloat:=rv(quSpecVnSumCenaTovarSpis.AsFloat);
          taSpecVnSumRazn.AsFloat:=rv(quSpecVnKol.AsFloat*quSpecVnCenaTovarMove.AsFloat)-quSpecVnSumCenaTovarSpis.AsFloat;

          taSpecVnCenaSS.AsFloat:=quSpecVnCenaTovarSpis.AsFloat;
          taSpecVnSumCenaSS.AsFloat:=quSpecVnSumCenaTovarSpis.AsFloat;
          taSpecVnNDSProc.AsFloat:=quSpecVnNDS.AsFloat;

          taSpecVnQuantN.AsFloat:=quSpecVnSumVesTara.AsFloat;
          if quSpecVnSumVesTara.AsFloat>0 then
            taSpecVnProcBrak.AsFloat:=rv(quSpecVnKol.AsFloat/quSpecVnSumVesTara.AsFloat*100)
          else
            taSpecVnProcBrak.AsFloat:=0;

          taSpecVnQuantM.AsFloat:=quSpecVnKolMest.AsInteger/1000;

          taSpecVn.Post;

        end;
        quSpecVn.Next; 
      end;

      fmAddDoc4.ViewDoc4.EndUpdate;
      if fmAddDoc4.teSpecIn.RecordCount>0 then fmAddDoc4.ViewDoc4.ViewData.Expand(True);

      fmAddDoc4.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs4.ViewDocsVnDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quTTnVnAcStatusP.AsInteger=0 then acEditDoc4.Execute //��������������
    else acViewDoc4.Execute; //��������}
  end;
end;

procedure TfmDocs4.acDelDoc4Execute(Sender: TObject);
begin
  if not CanDo('prDelDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnVn.RecordCount>0 then //���� ��� �������
    begin
      if quTTnVnAcStatusP.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTnVnNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(3,quTTnVnID.AsInteger,trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNSelfLn"');
          quD.SQL.Add('where Depart='+its(quTTnVnDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnVnDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnVnNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNSelf"');
          quD.SQL.Add('where Depart='+its(quTTnVnDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnVnDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnVnNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          prRefrID(quTTnVn,ViewDocsVn,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs4.acOnDoc4Execute(Sender: TObject);
Var //rPr:Real;
    bOpr:Boolean;
begin
//������������
  with dmMC do
  begin
    if not CanDo('prOnDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnVn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnVnAcStatusP.AsInteger=0 then
      begin
//      if pos('Torg',Person.Name)>0 then

        if quTTnVnDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnVnDepart.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if MessageDlg('������������ �������� �'+quTTnVnNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {if prTOFind(Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnDepart.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnVnNameDep.AsString+' � '+FormatDateTime('dd.mm.yyyy',quTTnVnDateInvoice.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnDepart.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem7.Enabled:=True;
              exit;
            end;
          end;}
          // ��������� �� 2010-06-16----- ������
           {     quA.Active:=false;
                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>'''+formatDateTime('yyyy-mm-dd',quTTnVnDateInvoice.AsDateTime)+'''');
                quA.SQL.Add('and Detail=0');
                quA.SQL.Add('and Depart='+quTTnVnDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                  if (quA.RecordCount>0) then
                    begin
                      ShowMessage('������������� �� ��������, ��������� ������ ��������������');
                      quA.Active:=false;
                      exit;
                    end;
                quA.Active:=false;

                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>='''+formatDateTime('yyyy-mm-dd',quTTnVnDateInvoice.AsDateTime)+'''');
                quA.SQL.Add('and Detail>0');
                quA.SQL.Add('and Depart='+quTTnVnDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                CloseTe(dmMT.teInventryArt);
                dmMT.teInventryArt.Active:=true;
                  while not quA.Eof do
                    begin
                      quB.Active:=False;
                      quB.SQL.Clear;
                      quB.SQL.Add('select Item from "InventryLine"');
                      quB.SQL.Add('where Inventry ='+quA.FieldByName('Inventry').AsString);
                      quB.SQL.Add('and Depart='+quTTnVnDepart.AsString);

                      quB.Active:=True;
                      while not quB.Eof do
                        begin
                          dmMT.teInventryArt.Append;
                          dmMT.teInventryArtArticul.AsInteger:=quB.fieldbyName('Item').AsInteger;
                          dmMT.teInventryArt.Post;
                          quB.Next
                        end;
                      quB.Active:=False;
                      quA.Next;
                    end;

                quA.Active:=false;

                quSpecVn1.Active:=False;
                quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
                quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
                quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
                quSpecVn1.Active:=True;

                quSpecVn1.First;
                while not quSpecVn1.Eof do
                  begin
                    if dmMT.teInventryArt.Locate('Articul',quSpecVn1.fieldbyname('CodeTovar').AsInteger,[]) then
                      begin
                        showMessage('������������� �� ��������, ��������� ��������� ��������������');
                        exit;
                      end;
                    quSpecVn1.Next;
                  end;

                quSpecVn1.Active:=False;
                dmMT.teInventryArt.Active:=false;
          //----------------------------- ������
            }
          bOpr:=True;
          if quTTnVnProvodTypeP.AsInteger=3 then
            if pos('Torg',Person.Name)=0 then
            begin
              prWH('��� ���� �� ������������� ������� ���� ���������.',Memo1);
              bOpr:=False; //������������ ������  ���� 3-�� ���
            end;

          if bOpr then
          begin

            prButtonSet(False);

            prAddHist(3,quTTnVnID.AsInteger,trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnNumber.AsString,'�����.',1);

            Memo1.Clear;
            prWH('����� ... ���� ������.',Memo1);

            quSpecVn1.Active:=False;
            quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
            quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
            quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
            quSpecVn1.Active:=True;


//          prClearBuf(quTTnVnID.AsInteger,3,Trunc(quTTnVnDateInvoice.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quTTnVnDateInvoice.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTnVnID.AsInteger)*100+4;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNVN;ON;'+its(quTTnVnDepart.AsInteger)+r+its(quTTnVnFlowDepart.AsInteger)+r+its(quTTnVnID.AsInteger)+r+ds(quTTnVnDateInvoice.AsDateTime)+r+quTTnVnNumber.AsString+r+fs(quTTnVnSummaTovarSpis.AsFloat)+r+fs(quTTnVnSummaTovarMove.AsFloat)+r+fs(quTTnVnSummaTovarNew.AsFloat)+r+fs(quTTnVnSummaTara.AsFloat)+r+fs(quTTnVnProvodTypeP.AsINteger)+r);

                quSpecVn1.First;
                while not quSpecVn1.Eof do
                begin
                  StrPost:='TTNVNLN;ON;'+its(quTTnVnDepart.AsInteger)+r+its(quTTnVnFlowDepart.AsInteger)+r+its(quTTnVnID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecVn1CodeTovar.asINteger)+r+its(quSpecVn1CodeTara.asINteger)+r+fs(quSpecVn1Kol.asfloat)+r+fs(quSpecVn1SumCenaTovarSpis.asfloat)+r+fs(quSpecVn1SumCenaTovarMove.AsFloat)+r+fs(quSpecVn1SumCenaTara.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecVn1.Next;
                end;
              finally
                closefile(fPost);
              end;

                //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin

              quSpecVn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecVn1.Eof do
              begin
              //���� ���-�� ����� �� ���� ������ - �������������� ����� ���� ���� � ���� ��� � �����
                if quSpecVn1Kol.AsFloat<0 then
                begin //���� ������
//                rPr:=prFC(quSpecVn1CodeTovar.AsInteger);
//                if abs(quSpecVn1NewCena.AsFloat-rPr)>=0.01 then //
//                  prTPriceBuf(quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quTTnVnNumber.AsString,rPr,quSpecVn1NewCena.AsFloat);
                end;

                prDelTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(-1)*quSpecVn1Kol.AsFloat);
                prAddTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(-1)*quSpecVn1Kol.AsFloat);

                if (quTTnVnDepart.AsInteger<>quTTnVnFlowDepart.AsInteger) and (pos('��������',quTTnVnNameD2.AsString)=0) then //������ ������ - ����� ������
                begin
                  prDelTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);
                  prAddTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);
                end;

                quSpecVn1.Next;
              end;
              quSpecVn1.Active:=False;

              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNSelf" Set AcStatusP=3, AcStatusR=3, Oprihod=1,  ChekBuhP=1,  ChekBuhR=1');
              quE.SQL.Add('where Depart='+its(quTTnVnDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTnVnDateInvoice.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTnVnNumber.AsString+'''');
              quE.ExecSQL;

              prRefrID(quTTnVn,ViewDocsVn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTnVnDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnVnDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);

            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

            prButtonSet(True);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmDocs4.acOffDoc4Execute(Sender: TObject);
Var  bOpr:Boolean;

begin
//��������
  with dmMC do
  begin
    if not CanDo('prOffDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTnVnDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    if quTTnVn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnVnAcStatusP.AsInteger=3 then
      begin
        if quTTnVnDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnVnDepart.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if MessageDlg('�������� �������� �'+quTTnVnNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          prAddHist(3,quTTnVnID.AsInteger,trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnNumber.AsString,'�����.',0);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);

          quSpecVn1.Active:=False;
          quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
          quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
          quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
          quSpecVn1.Active:=True;

          bOpr:=True;
          if NeedPost(trunc(quTTnVnDateInvoice.AsDateTime)) then
          begin
            iNumPost:=PostNum(quTTnVnID.AsInteger)*100+4;
            prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'TTNVN;OF;'+its(quTTnVnDepart.AsInteger)+r+its(quTTnVnFlowDepart.AsInteger)+r+its(quTTnVnID.AsInteger)+r+ds(quTTnVnDateInvoice.AsDateTime)+r+quTTnVnNumber.AsString+r+fs(quTTnVnSummaTovarSpis.AsFloat)+r+fs(quTTnVnSummaTovarMove.AsFloat)+r+fs(quTTnVnSummaTovarNew.AsFloat)+r+fs(quTTnVnSummaTara.AsFloat)+r+fs(quTTnVnProvodTypeP.AsINteger)+r);

              quSpecVn1.First;
              while not quSpecVn1.Eof do
              begin
                StrPost:='TTNVNLN;OF;'+its(quTTnVnDepart.AsInteger)+r+its(quTTnVnFlowDepart.AsInteger)+r+its(quTTnVnID.AsInteger);
                StrPost:=StrPost+r+its(quSpecVn1CodeTovar.asINteger)+r+its(quSpecVn1CodeTara.asINteger)+r+fs(quSpecVn1Kol.asfloat)+r+fs(quSpecVn1SumCenaTovarSpis.asfloat)+r+fs(quSpecVn1SumCenaTovarMove.AsFloat)+r+fs(quSpecVn1SumCenaTara.asfloat);
                writeln(fPost,StrPost);
                quSpecVn1.Next;
              end;
            finally
              closefile(fPost);
            end;

              //������ ����� - ���� ��������
            bOpr:=prTr(sNumPost(iNumPost),Memo1);
          end;

          if bOpr then
          begin

          quSpecVn1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecVn1.Eof do
          begin
            prDelTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(quSpecVn1Kol.AsFloat*-1));

            if quTTnVnDepart.AsInteger<>quTTnVnFlowDepart.AsInteger then //������ ������ - ����� ������
              prDelTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);

            quSpecVn1.Next;
          end;
          quSpecVn1.Active:=False;

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNSelf" Set AcStatusP=0, AcStatusR=0, Oprihod=0');
          quE.SQL.Add('where Depart='+its(quTTnVnDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTnVnDateInvoice.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTnVnNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTnVn,ViewDocsVn,0);

          //�������� ��
          prWh('  �������� ��.',Memo1);
          if (trunc(Date)-trunc(quTTnVnDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnVnDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

          prWh('������ ��.',Memo1);
          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs4.Timer1Timer(Sender: TObject);
begin
  if bClearDocVn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocVn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocVn:=True;
end;

procedure TfmDocs4.acVidExecute(Sender: TObject);
begin
  //���
  with dmMC do
  begin
    {if LevelDocsVn.Visible then
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs4.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs4.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsVn.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs4.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs4.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsVn.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsVn.BeginUpdate;
      quTTnVn.Active:=False;
      quTTnVn.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quTTnVn.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quTTnVn.Active:=True;
      ViewDocsVn.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocs4.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs4.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsVn.Visible=False then exit;
  with dmMC do
  begin
    if quTTnVn.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnVnID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocVnReestr.frf');

      frVariables.Variable['CliName']:=quTTnVnNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnVnNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnVnDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnVnNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs4.acCopyExecute(Sender: TObject);
//var Par:Variant;
begin
  //����������
  with dmMC do
  begin
    {if quTTnVn.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnVnID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnVnID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnVnDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnVnNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quTTnVnIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnVnNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnVnIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnVnNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnVnSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnVnSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnVnID.AsInteger;
      quSpecIn.Active:=True;

      quSpecIn.First;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnVnID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecInNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecInIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecInQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecInIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecInSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecInIDM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecInIDM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDoc.Post;

        quSpecIn.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocs4.acInsertDExecute(Sender: TObject);
{Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;}
begin
  // ��������
  with dmMC do
  begin
    {taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocVn') then
        begin
          prAllViewOff;

          fmAddDoc4.Caption:='���������: ����� ��������.';
          fmAddDoc4.cxTextEdit1.Text:=prGetNum(1,0);
          fmAddDoc4.cxTextEdit1.Tag:=0;
          fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxTextEdit2.Text:='';
          fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit1.Date:=Date;
          fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit2.Date:=Date;
          fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc4.cxCurrencyEdit2.EditValue:=0;

          if taHeadDocIType.AsInteger=1 then
          begin
            fmAddDoc4.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc4.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc4.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc4.cxButtonEdit1.Tag:=0;
            fmAddDoc4.cxButtonEdit1.EditValue:=0;
            fmAddDoc4.cxButtonEdit1.Text:='';
          end;

          fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc4.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc4.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quDeparts.Active=False then quDeparts.Active:=True;
          quDeparts.FullRefresh;

          if quDeparts.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc4.Label15.Caption:='��. ����: '+quDepartsNAMEPRICE.AsString;
            fmAddDoc4.Label15.Tag:=quDepartsDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc4.Label15.Caption:='��. ����: ';
            fmAddDoc4.Label15.Tag:=0;
          end;

          fmAddDoc4.cxLabel1.Enabled:=True;
          fmAddDoc4.cxLabel2.Enabled:=True;
          fmAddDoc4.cxLabel3.Enabled:=True;
          fmAddDoc4.cxLabel4.Enabled:=True;
          fmAddDoc4.cxLabel5.Enabled:=True;
          fmAddDoc4.cxLabel6.Enabled:=True;
          fmAddDoc4.N1.Enabled:=True;

          fmAddDoc4.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc4.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc4.cxButton1.Enabled:=True;

          CloseTa(fmAddDoc4.taSpec);

          fmAddDoc4.ViewTara.OptionsData.Editing:=True;
          fmAddDoc4.ViewTara.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc4.taTaraS);

          fmAddDoc4.acSaveDoc.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecVnds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

     //     fmAddDoc4.ShowModal;
          fmAddDoc4.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocs4.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsVn.Visible then
  begin
    prNExportExel5(ViewDocsVn);
  end else
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocs4.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs4.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsVn);
end;

procedure TfmDocs4.acAdd1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc4(0,0); //0-����������, 1-��������������, 2-��������

//    CloseTa(fmAddDoc4.taSpecVn);
{    if FileExists(CurDir+'SpecVn.cds') then fmAddDoc4.taSpecVn.Active:=True
    else fmAddDoc4.taSpecVn.CreateDataSet;}
    CloseTe(fmAddDoc4.taSpecVn);

    fmAddDoc4.acSaveDoc.Enabled:=True;
    fmAddDoc4.Show;
  end;
end;

procedure TfmDocs4.ViewDocsVnCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsVN.ColumnCount-1 do
  begin
    if ViewDocsVN.Columns[i].Name='ViewDocsVnAcStatusP' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;
end;

procedure TfmDocs4.acViewPrihohExecute(Sender: TObject);
Var sNum:String;
    dDate:TDateTime;
    iDep,iDate,IdH:INteger;
begin
 //������� ������
  with dmMC do
  with dmMt do
  begin
    if quTTnVn.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnVnProvodTypeP.AsInteger=3 then
      begin
        sNum:=quTTnVnNumber.AsString;
        dDate:=quTTnVnDateInvoice.AsDateTime;
        iDep:=quTTnVnDepart.AsInteger;

        if ptTTNIn.Active=False then ptTTNIn.Active:=True;
        ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';

        iDate:=0;

        if ptTTnIn.FindKey([iDep,dDate,sNum]) then iDate:=Trunc(ptTTnInDateInvoice.AsDateTime);
        if iDate=0 then
          if ptTTnIn.FindKey([iDep,dDate-1,sNum]) then iDate:=Trunc(ptTTnInDateInvoice.AsDateTime);

        if iDate>0 then
        begin
          iDh:=ptTTnInId.AsInteger;
          fmDocs1.LevelDocsIn.Visible:=True;
          fmDocs1.LevelCards.Visible:=False;

          fmDocs1.ViewDocsIn.BeginUpdate;
          try
            quTTNIn.Active:=False;
            quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
            quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd); // ��� <=
            quTTNIn.Active:=True;
          finally
            fmDocs1.ViewDocsIn.EndUpdate;
          end;

          fmDocs1.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

          if quTTNIn.Locate('Id',iDh,[]) then
            fmDocs1.acViewDoc1.Execute;

        end else showmessage('��������� �������� �� ������.');
      end else showmessage('�������� �� ���� ����.');
    end else showmessage('�������� ��������.');
  end;
end;

end.
