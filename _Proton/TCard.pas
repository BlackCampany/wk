unit TCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus,
  cxLookAndFeelPainters, cxMaskEdit, cxSpinEdit, StdCtrls, cxContainer,
  cxTextEdit, cxButtons, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ExtCtrls, cxDropDownEdit, cxCalc, cxLabel,
  Placemnt, cxDBLookupComboBox, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, cxButtonEdit, cxCalendar, DBClient, cxBlobEdit, FR_DSet,
  FR_DBSet, FR_Class, ActnList, XPStyleActnCtrls, ActnMan, cxCheckBox,
  cxLookupEdit, cxDBLookupEdit;

type
  TTK = Record
  Add,Edit:Boolean
  end;

  TfmTCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    GTCards: TcxGrid;
    ViewTCards: TcxGridDBTableView;
    LTCards: TcxGridLevel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    Panel2: TPanel;
    Panel3: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label4: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label5: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    GTSpec: TcxGrid;
    ViewTSpec: TcxGridDBTableView;
    LTSpec: TcxGridLevel;
    ViewTCardsIDCARD: TcxGridDBColumn;
    ViewTCardsID: TcxGridDBColumn;
    ViewTCardsDATEB: TcxGridDBColumn;
    ViewTCardsDATEE: TcxGridDBColumn;
    ViewTCardsSHORTNAME: TcxGridDBColumn;
    ViewTCardsRECEIPTNUM: TcxGridDBColumn;
    ViewTCardsPOUTPUT: TcxGridDBColumn;
    ViewTCardsPCOUNT: TcxGridDBColumn;
    ViewTCardsPVES: TcxGridDBColumn;
    ViewTSpecIDCARD: TcxGridDBColumn;
    ViewTSpecNETTO: TcxGridDBColumn;
    ViewTSpecBRUTTO: TcxGridDBColumn;
    ViewTSpecNAME: TcxGridDBColumn;
    ViewTSpecSM: TcxGridDBColumn;
    cxButton6: TcxButton;
    Timer1: TTimer;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    ViewTSpecIdM: TcxGridDBColumn;
    ViewTCardsSDATEE: TcxGridDBColumn;
    cxButton7: TcxButton;
    PopupMenuTCard: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    TCS: TClientDataSet;
    TCH: TClientDataSet;
    TCHIDCARD: TIntegerField;
    TCHID: TIntegerField;
    TCHDATEB: TDateField;
    TCHDATEE: TDateField;
    TCHSHORTNAME: TStringField;
    TCHRECEIPTNUM: TStringField;
    TCHPOUTPUT: TStringField;
    TCHPCOUNT: TIntegerField;
    TCHPVES: TFloatField;
    TCSIDC: TIntegerField;
    TCSIDT: TIntegerField;
    TCSID: TIntegerField;
    TCSIDCARD: TIntegerField;
    TCSCURMESSURE: TIntegerField;
    TCSNETTO: TFloatField;
    TCSBRUTTO: TFloatField;
    TCSKNB: TFloatField;
    dsTCH: TDataSource;
    dsTCS: TDataSource;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    frRepTTK: TfrReport;
    frtaTTK: TfrDBDataSet;
    taTTK: TClientDataSet;
    taTTKNum: TIntegerField;
    taTTKName: TStringField;
    taTTKSM: TStringField;
    taTTKQuantB: TFloatField;
    taTTKQuantN: TFloatField;
    taTTKQuantN10: TFloatField;
    taTTKQuantN20: TFloatField;
    amTCard: TActionManager;
    acIns: TAction;
    acEdit: TAction;
    acDel: TAction;
    taTSpec: TClientDataSet;
    taTSpecNum: TIntegerField;
    taTSpecIdCard: TIntegerField;
    taTSpecIdM: TIntegerField;
    taTSpecSM: TStringField;
    taTSpecKm: TFloatField;
    taTSpecName: TStringField;
    taTSpecNetto: TFloatField;
    taTSpecBrutto: TFloatField;
    taTSpecKnb: TFloatField;
    dstaTSpec: TDataSource;
    ViewTSpecNum: TcxGridDBColumn;
    acSave: TAction;
    TCSNAME: TStringField;
    taTTKCode: TIntegerField;
    taTSpecTCard: TSmallintField;
    ViewTSpecTCard: TcxGridDBColumn;
    acTTKView: TAction;
    taTSpecNetto1: TFloatField;
    taTSpecComment: TStringField;
    ViewTSpecNetto1: TcxGridDBColumn;
    ViewTSpecComment: TcxGridDBColumn;
    Panel4: TPanel;
    Image3: TImage;
    cxLabel3: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    Image1: TImage;
    Image2: TImage;
    Label7: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxCheckBox1: TcxCheckBox;
    cxButton10: TcxButton;
    cxButton2: TcxButton;
    taTSpecPr1: TFloatField;
    ViewTSpecPr1: TcxGridDBColumn;
    TCSNETTO1: TFloatField;
    TCSCOMMENT: TStringField;
    cxButton11: TcxButton;
    taTTKComment: TStringField;
    taTSpecNetto2: TFloatField;
    taTSpecPr2: TFloatField;
    ViewTSpecNetto2: TcxGridDBColumn;
    ViewTSpecPr2: TcxGridDBColumn;
    cxButton12: TcxButton;
    ViewTSpecIObr: TcxGridDBColumn;
    taTSpecIObr: TIntegerField;
    cxLookupComboBox1: TcxLookupComboBox;
    Label9: TLabel;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton3: TcxButton;
    cxButton15: TcxButton;
    acTestUse: TAction;
    Label8: TLabel;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxTextEdit2PropertiesChange(Sender: TObject);
    procedure cxTextEdit3PropertiesChange(Sender: TObject);
    procedure cxTextEdit4PropertiesChange(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxCalcEdit1PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure ViewTCardsFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure ViewTSpecEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure acSaveExecute(Sender: TObject);
    procedure taTSpecNettoChange(Sender: TField);
    procedure ViewTSpecSMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton10Click(Sender: TObject);
    procedure ViewTSpecCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acTTKViewExecute(Sender: TObject);
    procedure ViewTSpecDblClick(Sender: TObject);
    procedure ViewTSpecEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taTSpecNetto1Change(Sender: TField);
    procedure taTSpecPr1Change(Sender: TField);
    procedure cxButton11Click(Sender: TObject);
    procedure taTSpecNetto2Change(Sender: TField);
    procedure taTSpecPr2Change(Sender: TField);
    procedure cxButton12Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure ViewTSpecCommentPropertiesChange(Sender: TObject);
    procedure ViewTSpecIObrPropertiesChange(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure  prSetVal;
    procedure  prSetOn;
    procedure  prSaveTForce;
    procedure prTestAndSave;
    procedure prColsEnables(bEn:Boolean);

  end;

var
  fmTCard: TfmTCard;
  TK: TTK;
  bClearTK:Boolean = False;
  iColTC:ShortINt;

implementation

uses dmOffice, Un1, PeriodUni, Goods, FindResult, CurMessure, GoodsSel,
  CalcSeb, TBuff, Refer, FCards, OMessure, of_TTKView, CalcCal,
  TCardAddPars;

{$R *.dfm}

procedure TfmTCard.prColsEnables(bEn:Boolean);
begin
  ViewTSpecNum.Options.Editing:=bEn;
  ViewTSpecIDCARD.Options.Editing:=bEn;
  ViewTSpecNAME.Options.Editing:=bEn;
  ViewTSpecIdM.Options.Editing:=bEn;
  ViewTSpecSM.Options.Editing:=bEn;
  ViewTSpecNETTO.Options.Editing:=bEn;
  ViewTSpecPr1.Options.Editing:=bEn;
  ViewTSpecNetto1.Options.Editing:=bEn;
  ViewTSpecPr2.Options.Editing:=bEn;
  ViewTSpecNetto2.Options.Editing:=bEn;
  ViewTSpecTCard.Options.Editing:=bEn;
end;

procedure TfmTCard.prTestAndSave;
Var bSave:Boolean;
    StrWk:String;
    k:Integer;
begin
  bSave:=False;
  with dmO do
  begin
    ViewTSpec.BeginUpdate;

    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;

    if quTSpec.RecordCount<>taTSpec.RecordCount then bSave:=True;

    if bSave=False then
    begin
      quTSpec.First; taTSpec.First;  k:=0;
      while not quTSpec.Eof do
      begin
        if taTSpecIdCard.AsInteger<>quTSpecIdCard.AsInteger then begin bSave:=True; k:=1; end;
        if taTSpecIdM.AsInteger<>quTSpecCURMESSURE.AsInteger then begin bSave:=True; k:=2; end;
//        if taTSpecKm.AsFloat<>quTSpecKNB.AsFloat then begin bSave:=True; k:=3; end;
        if taTSpecNetto.AsFloat<>quTSpecNETTO.AsFloat then begin bSave:=True; k:=4; end;

        if bSave then
        begin
          StrWk:=taTSpecIdCard.AsString+' '+taTSpecName.AsString+' _'+IntToStr(k);
          break;
        end;

        quTSpec.Next; taTSpec.Next;
      end;
    end;
    
    quTSpec.Active:=False;
    taTSpec.Last;

    ViewTSpec.EndUpdate;
  end;

  if bSave then
    if MessageDlg('�� ����������('+StrWk+'), ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then prSaveTForce;
end;

procedure  TfmTCard.prSetOn;
begin
  cxButton1.Enabled:=True;
end;

procedure  TfmTCard.prSetVal;
var kBrutto:Real;
    iDate:Integer;
    iMax:INteger;
begin
  if bSet=False then exit;
  with dmO do
  begin
//    showmessage('��������� ������������?');

    cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
    cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
    cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
    cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
    cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

    CloseTa(taTSpec);

    ViewTSpec.BeginUpdate;

    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;


    iMax:=1;
    quTSpec.First;
    while not quTSpec.Eof do
    begin
      taTSpec.Append;
      taTSpecNum.AsInteger:=iMax;
      taTSpecIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
      taTSpecIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
      taTSpecSM.AsString:=quTSpecNAMESHORT.AsString;
      taTSpecKm.AsFloat:=quTSpecKOEF.AsFloat;
      taTSpecName.AsString:=quTSpecNAME.AsString;
      taTSpecNetto.AsFloat:=quTSpecNETTO.AsFloat;
      taTSpecBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
      taTSpecKnb.AsFloat:=0;
      taTSpecTCard.AsInteger:=quTSpecTCARD.AsInteger;
      taTSpecNetto1.AsFloat:=quTSpecNETTO1.AsFloat;
      taTSpecComment.AsString:=quTSpecComment.AsString;
      taTSpecPr1.AsFloat:=0;
      if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;
      taTSpecNetto2.AsFloat:=quTSpecNETTO2.AsFloat;
      taTSpecPr2.AsFloat:=0;
      if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;
      taTSpecIOBR.AsInteger:=quTSpecIOBR.AsInteger;
      taTSpec.Post;

      quTSpec.Next;   inc(iMax);
    end;
    quTSpec.Active:=False;

    ViewTSpecBRUTTO.Caption:='������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    iDate:=prDateToI(cxDateEdit1.Date);

    taTSpec.First;
    while not taTSpec.Eof do
    begin
      //���������� ������
      kBrutto:=0;
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        kBrutto:=quFindEUTO100GRAMM.AsFloat;
      end;

      quFindEU.Active:=False;

      taTSpec.Edit;
      taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+kBrutto)/100;
      taTSpecKNB.AsFloat:=kBrutto;
      taTSpec.Post;

      taTSpec.Next;
    end;

    ViewTSpec.EndUpdate;
    cxButton1.Enabled:=False;

//    ViewTSpec.OptionsData.Editing:=False;
    prColsEnables(False);

    cxLabel1.Enabled:=False;
    cxCheckBox1.Enabled:=False;
    cxLabel2.Enabled:=False;
    Image1.Enabled:=False;
    Image2.Enabled:=False;
    cxButton10.Enabled:=False;

    cxSpinEdit1.Properties.ReadOnly:=True;
    cxCalcEdit1.Properties.ReadOnly:=True;
    cxButton12.Enabled:=False;
    cxButton1.Enabled:=False;
//    cxButton13.Enabled:=False;
  end;
end;

procedure TfmTCard.cxButton2Click(Sender: TObject);
begin
  prTestAndSave;
  close;
end;

procedure TfmTCard.Timer1Timer(Sender: TObject);
begin
  if bClearTK=True then begin StatusBar1.Panels[0].Text:='';bClearTK:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearTK:=True;
end;

procedure TfmTCard.cxButton4Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate:Integer;
    sVes:String;
    iVes,iPers:INteger;
begin
  //���������� ��
  if not CanDo('prAddTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    //��� ���� �������� �� ���������� �������
    if quTCards.RecordCount>0 then if cxButton10.Enabled then prTestAndSave;
    fmPeriodUni.DateTimePicker1.Date:=Date;
    fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
    fmPeriodUni.ShowModal;
    if fmPeriodUni.ModalResult=mrOk then
    begin
      cxLookupComboBox1.EditValue:=0;

      sVes:='1000';  //���
      iVes:=1000;    //��� � �������
      iPers:=1;      //���-�� ������

      bSet:=False;
      DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
      DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

      //�������� �� �������������
//      prTestUseTC(cxTextEdit1.Tag,iDate,iC);

      sCards:=its(cxTextEdit1.Tag);
      prFindInputCards(cxTextEdit1.Tag,Trunc(DateB));
      prTestUseTC1(iDate);

      if Trunc(DateB)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
//        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). ������� ������ ������ �������� !!!');
        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). ������� ������ ������ �������� !!!');
        exit;
      end;

      if quTCards.RecordCount>0 then
      begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
        quTCards.Last;

        sVes:=quTCardsPOUTPUT.AsString;  //���
        iVes:=quTCardsPVES.AsInteger;    //��� � �������
        iPers:=quTCardsPCOUNT.AsInteger;      //���-�� ������

//        prSetVal;
        if quTCardsDATEB.AsDateTime>=DateB then
        begin
          showmessage('������ ������ ��� ���������, ���������� ����������.');
          exit;
        end;
        if quTCardsDATEE.AsDateTime>=iMaxDate then
        begin
          quTCards.Edit;
          quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
          quTCards.Post;
        end;
      end;

      iId:=GetId('TCards');

      quTCards.Append;
      quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
      quTCardsID.AsInteger:=IId;
      quTCardsDATEB.AsDateTime:=Trunc(DateB);
      quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
      quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
      quTCardsRECEIPTNUM.AsString:=IntToStr(iId);
      quTCardsPOUTPUT.AsString:=sVes;
      quTCardsPVES.AsFloat:=iVes;
      quTCardsPCOUNT.AsInteger:=iPers;
      quTCards.Post;

      prSetTC(cxTextEdit1.Tag,True);

      if Panel3.Visible=False then Panel3.Visible:=True;
      bSet:=True;

      prSetVal;
    end;
  end;
end;

procedure TfmTCard.cxButton1Click(Sender: TObject);
Var bErr:Boolean;
begin
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  cxButton1.Enabled:=False;
  if cxCalcEdit1.EditValue<1 then
  begin
    Showmessage('������!!!  ����� ����� ������ �� ����� ���� ������ 1 ...');
    cxButton1.Enabled:=True;
    exit;
  end;

  ViewTSpec.BeginUpdate;
  try
    bErr:=False;
    taTSpec.First;
    while not taTSpec.Eof do
    begin
      if taTSpecIdCard.AsInteger=0 then bErr:=True;
      if taTSpecNetto.AsFloat=0 then bErr:=True;
      if taTSpecNetto1.AsFloat=0 then bErr:=True;
      if taTSpecNetto2.AsFloat=0 then bErr:=True;
      taTSpec.Next;
    end;
  finally
    ViewTSpec.EndUpdate;
  end;

  if bErr then
  begin
    showmessage('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
    cxButton1.Enabled:=True;
    exit;
  end;

  prSaveTForce;
  if cxLookupComboBox1.Text='' then
  begin
    cxLookupComboBox1.EditValue:=0;
    cxLookupComboBox1.Text:='���';
  end;

  with dmO do
  begin
    quTCards.Edit;
    quTCardsSHORTNAME.AsString:=cxTextEdit2.Text;
    quTCardsRECEIPTNUM.AsString:=cxTextEdit3.Text;
    quTCardsPOUTPUT.AsString:=cxTextEdit4.Text;
    quTCardsPCOUNT.AsInteger:=cxSpinEdit1.EditValue;
    quTCardsPVES.AsFloat:=cxCalcEdit1.EditValue;
    quTCardsIOBR.AsInteger:=cxLookupComboBox1.EditValue;
    quTCards.Post;
  end;

end;

procedure TfmTCard.cxTextEdit2PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit3PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit4PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxCalcEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxButton6Click(Sender: TObject);
Var iDate,iDateB,iDateE,iT,iDatePre:Integer;
    bEdit,bPer:Boolean;
begin
  // ������������� ������
  if not CanDo('prEditTKPeriod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
//    CommonSet.DateFrom:=Trunc(quTCardsDATEB.AsDateTime);
//    CommonSet.DateTo:=Trunc(quTCardsDATEE.AsDateTime)+1;
    if quTCards.RecordCount>0 then
    begin
      fmPeriodUni.DateTimePicker1.Date:=quTCardsDATEB.AsDateTime;
      fmPeriodUni.DateTimePicker2.Visible:=False;
      fmPeriodUni.cxCheckBox1.Visible:=False;
      fmPeriodUni.ShowModal;
      if fmPeriodUni.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
        iDateE:=Trunc(quTCardsDATEE.AsDateTime);
        bPer:=True;
        if iDateB>iDateE then
        begin  //������ �������� �� ����� ���� ����� �����
          showmessage('����������� ������ ������ - ���� ������ �������� ������ ���� ������ ���� ��������� ��������. �������������� ���������.');
          bPer:=False;
        end;

        if quTCards.RecordCount>1 then
        begin
          bSet:=False;
          ViewTCards.BeginUpdate;
          ViewTSpec.BeginUpdate;
          iT:=quTCardsID.AsInteger;
          try
            quTCards.Prior;
            if not quTCards.Bof then  //���� �� ������
            begin
              iDatePre:=Trunc(quTCardsDATEB.AsDateTime);

              if iDateB<=iDatePre then
              begin
                showmessage('����������� ������ ������ - ����������� ������� �������� � ���������� ������. �������������� ���������.');
                bPer:=False;
              end;
            end;  
          except
          end;

          quTCards.Locate('ID',iT,[]);
          ViewTSpec.EndUpdate;
          ViewTCards.EndUpdate;
          bSet:=True;
        end;

        if bPer then
        begin
          bEdit:=True;

          if iDateB<Trunc(Date) then
          begin
//            prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC)
            sCards:=its(cxTextEdit1.Tag);
            prFindInputCards(cxTextEdit1.Tag,iDateB);
            prTestUseTC1(iDate);
          end
          else iDate:=Trunc(iDateB-1);

          if iDateB<=iDate then
          begin //� ������ �������� ���� �������������
            if not CanDo('prRecalcTCard') then
            begin
              Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). �������������� ���������� !!!');
              bEdit:=False;
            end else
            begin
              if MessageDlg('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). ����������� �������� ����������. ������������� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              begin
                bEdit:=True;
              end else
              begin
                bEdit:=False;
              end;
            end;
          end;
          if bEdit then
          begin
            quTCards.Edit;
            quTCardsDATEB.AsDateTime:=iDateB;
            quTCards.Post;

            if quTCards.RecordCount>1 then
            begin
              bSet:=False;
              ViewTCards.BeginUpdate;
              ViewTSpec.BeginUpdate;
              iT:=quTCardsID.AsInteger;
              try
                quTCards.Prior;
                if not quTCards.Bof then  //���� �� ������
                begin
                  quTCards.Edit;
                  quTCardsDATEE.AsDateTime:=Trunc(iDateB-1);
                  quTCards.Post;
                end;
              except
              end;

              quTCards.Locate('ID',iT,[]);
              ViewTSpec.EndUpdate;
              ViewTCards.EndUpdate;
              bSet:=True;
            end;
          end;
        end; //bPer
      end;
      fmPeriodUni.cxCheckBox1.Visible:=True;
    end;
  end;
end;

procedure TfmTCard.ViewTCardsFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
//  showmessage('1');
  if Visible then prSetVal;
end;

procedure TfmTCard.cxButton5Click(Sender: TObject);
Var iDate:Integer;
begin
  //������� ��
  if not CanDo('prDelTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
      //�������� �� ��������� ��������
      if Trunc(quTCardsDATEB.AsDateTime)<Trunc(Date) then  //���� ������� �� � ����� ���������
      begin
//        prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);

        sCards:=its(cxTextEdit1.Tag);
        prFindInputCards(cxTextEdit1.Tag,Trunc(quTCardsDATEB.AsDateTime));
        prTestUseTC1(iDate);

        if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
        begin //� ������ �������� ���� ������������� - �������� ����������
          Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). �������� ���������� !!!');
          exit;
        end;
      end;  

    if MessageDlg('�� ������������� ������ ������� �� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quTCards.Delete;
      if quTCards.RecordCount=0 then
      begin

        if taTSpec.Active then
        begin
          taTSpec.First;
          while not taTSpec.Eof do taTSpec.Delete;
        end;

        Panel3.Visible:=False;
        //����� ������� ���� ����� �������� � ��������� ��
        prSetTC(cxTextEdit1.Tag,False);
      end else
      begin
        quTCards.Last;

        quTCards.Edit;
        quTCardsDATEE.AsDateTime:=iMaxDate;
        quTCards.Post;

        quTCards.Refresh;

        prSetVal;
      end;
    end;
    end;
  end;
end;

procedure TfmTCard.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTSpec.RestoreFromIniFile(CurDir+GridIni);

  if UserPars.CheckGram=1 then cxCheckBox1.Checked:=True
  else cxCheckBox1.Checked:=False;

end;

procedure TfmTCard.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
      bAddTSpec:=True;
      quCardsSel1.Active:=False;
      quCardsSel1.Active:=True;
      fmGoodsSel.Show;
    end else showmessage('�������� ������� ��.');
  end;
end;

procedure TfmTCard.FormShow(Sender: TObject);
begin
  cxDateEdit1.Date:=Date;
  cxLabel3.Tag:=0;
end;

procedure TfmTCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (cxButton10.Enabled=True) or (cxButton1.Enabled=True) then
  begin
    if MessageDlg('�������� � ������ ��������������, �����?',  mtConfirmation, [mbYes, mbNo], 0,) = mrYes then
    begin
      Action := caHide;
      ViewTSpec.StoreToIniFile(CurDir+GridIni,False);
      if cxCheckBox1.Checked then UserPars.CheckGram:=1 else UserPars.CheckGram:=0;
      WriteParams;
    end
    else Action := caNone;
  end else
  begin
    Action := caHide;
    ViewTSpec.StoreToIniFile(CurDir+GridIni,False);
    if cxCheckBox1.Checked then UserPars.CheckGram:=1 else UserPars.CheckGram:=0;
    WriteParams;
  end;
end;

procedure TfmTCard.ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
//Var rN,rB,rK,rKOld:Real;
//    Id:Integer;
begin
{  with dmO do
  begin
    if quTSpec.RecordCount>0 then
    begin
      quMessureSel.Active:=False;
      quMessureSel.ParamByName('IDM').AsInteger:=quTSpecCURMESSURE.AsInteger;
      quMessureSel.Active:=True;

      quMessureSel.Locate('ID',quTSpecCURMESSURE.AsInteger,[]);

      fmCurMessure.ShowModal;
      if fmCurMessure.ModalResult=mrOk then
      begin
        rN:=quTSpecNETTO.AsFloat;
        rB:=quTSpecBRUTTO.AsFloat;
        rK:=quMessureSelKOEF.AsFloat;
        rKOld:=quTSpecKOEF.AsFloat;
        Id:=quTSpecID.AsInteger;

        quTSpec.Edit;
        quTSpecCURMESSURE.AsInteger:=quMessureSelID.AsInteger;
        if (rK=0) or (rN=0) then
        begin
          quTSpecNETTO.AsFloat:=0;
          quTSpecBRUTTO.AsFloat:=0;
        end else
        begin
          quTSpecNETTO.AsFloat:=RoundEx(rN*rKOld/rK*1000)/1000;
          quTSpecBRUTTO.AsFloat:=(rB/rN)*RoundEx(rN*rKOld/rK*1000)/1000;
        end;
//        quTSpecNAMESHORT.AsString:=quMessureSelNAMESHORT.AsString;
        quTSpec.Post;
        quTSpec.FullRefresh;
        quTSpec.Locate('ID',ID,[]);
      end;
      quMessureSel.Active:=False;
    end;
  end;}
end;

procedure TfmTCard.cxLabel2Click(Sender: TObject);
begin
  with dmO do
  begin
    if taTSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� "'+taTSpecNAME.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taTSpec.Delete;
      end;
    end;
  end;
end;

procedure TfmTCard.Label7Click(Sender: TObject);
Var kBrutto:Real;
    iDate:INteger;
begin
//  showmessage('2');
//  prSetVal;
  with dmO do
  begin
    ViewTSpec.BeginUpdate;

    ViewTSpecBRUTTO.Caption:='������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    iDate:=prDateToI(cxDateEdit1.Date);

    taTSpec.First;
    while not taTSpec.Eof do
    begin
      //���������� ������
      kBrutto:=0;
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        kBrutto:=quFindEUTO100GRAMM.AsFloat;
      end;

      quFindEU.Active:=False;

      taTSpec.Edit;
      taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+kBrutto)/100;
      taTSpecKNB.AsFloat:=kBrutto;
      taTSpec.Post;

      taTSpec.Next;
    end;

    ViewTSpec.EndUpdate;
  end;
end;

procedure TfmTCard.cxButton3Click(Sender: TObject);
begin
//�������������
//  fmCalcSeb:=tfmCalcSeb.Create(Application);
  fmCalcSeb.Label4.Caption:=cxTextEdit1.Text;
  fmCalcSeb.Label6.Caption:=Label6.Caption;
  fmCalcSeb.Label1.Caption:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
  fmCalcSeb.ShowModal;
  fmCalcSeb.quMHAll.Active:=False;
  fmCalcSeb.taSeb.Active:=False;
//  fmCalcSeb.Release;
end;

procedure TfmTCard.cxButton7Click(Sender: TObject);
Var iDate:Integer;
begin
  //�������� ���������
//  prTestUseTC(dmO.quTCardsIDCARD.AsInteger,iDate,iC);

  sCards:=dmO.quTCardsIDCARD.AsString;
  prFindInputCards(dmO.quTCardsIDCARD.AsInteger,Trunc(dmO.quTCardsDATEB.AsDateTime));
  prTestUseTC1(iDate);

  Label8.Caption:=sCards;
  Label8.Visible:=True;

//  Label8.Caption:=IntToStr(iDate)+'   '+IntToStr(iC);
end;

procedure TfmTCard.cxLabel3Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmTCard.N1Click(Sender: TObject);
begin
  //���������� ��
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    TCH.Active:=False;
    TCH.FileName:=CurDir+'TCH.cds';
    if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
    else TCH.CreateDataSet;

    TCS.Active:=False;
    TCS.FileName:=CurDir+'TCS.cds';
    if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
    else TCS.CreateDataSet;

    if TCH.Locate('ID',quTCardsID.AsInteger,[])=False
    then
    begin
      TCH.Append;
      TCHIDCARD.AsInteger:=quTCardsIDCARD.AsInteger;
      TCHID.AsInteger:=quTCardsID.AsInteger;
      TCHDATEB.AsDateTime:=quTCardsDATEB.AsDateTime;
      TCHDATEE.AsDateTime:=quTCardsDATEE.AsDateTime;
      TCHSHORTNAME.AsString:=quTCardsSHORTNAME.AsString;
      TCHRECEIPTNUM.AsString:=quTCardsRECEIPTNUM.AsString;
      TCHPOUTPUT.AsString:=quTCardsPOUTPUT.AsString;
      TCHPCOUNT.AsInteger:=quTCardsPCOUNT.AsInteger;
      TCHPVES.AsFloat:=quTCardsPVES.AsFloat;
      TCH.Post;

      taTSpec.First;
      while not taTSpec.Eof do
      begin
        TCS.Append;
        TCSIDC.AsInteger:=quTCardsIDCARD.AsInteger;
        TCSIDT.AsInteger:=quTCardsID.AsInteger;
        TCSID.AsInteger:=taTSpecNum.AsInteger;
        TCSIDCARD.AsInteger:=taTSpecIDCARD.AsInteger;
        TCSCURMESSURE.AsInteger:=taTSpecIdM.AsInteger;
        TCSNETTO.AsFloat:=taTSpecNETTO.AsFloat;
        TCSBRUTTO.AsFloat:=taTSpecBRUTTO.AsFloat;
        TCSKNB.AsFloat:=taTSpecKm.AsFloat;
        TCSNAME.AsString:=taTSpecName.AsString;
        TCSNETTO1.AsFloat:=taTSpecNETTO1.AsFloat;
        TCSCOMMENT.AsString:=taTSpecComment.AsString;
        TCS.Post;

        taTSpec.Next;
      end;

    end else
    begin
      showmessage('�� "'+quTCardsSHORTNAME.AsString+'"  ��� ���� � ������.');
    end;
  end;

  TCH.Active:=False;
  TCS.Active:=False;
end;

procedure TfmTCard.N2Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;
    Km:Real;
    iMax:Integer;
begin
  // ��������
  TCH.Active:=False;
  TCH.FileName:=CurDir+'TCH.cds';
  if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
  else TCH.CreateDataSet;

  TCS.Active:=False;
  TCS.FileName:=CurDir+'TCS.cds';
  if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
  else TCS.CreateDataSet;

  fmTBuff:=TfmTBuff.Create(Application);

  fmTBuff.LevelTH.Visible:=True;
  fmTBuff.LevelTS.Visible:=True;
  fmTBuff.LevelD.Visible:=False;
  fmTBuff.LevelDSpec.Visible:=False;

  fmTBuff.ShowModal;
  if fmTBuff.ModalResult=mrOk then
  begin //���������
    if TCH.RecordCount>0 then
    begin
      if CanDo('prAddTK') then
      begin
        with dmO do
        begin
          fmPeriodUni.DateTimePicker1.Date:=TCHDATEB.AsDateTime;
          fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
          fmPeriodUni.ShowModal;
          if fmPeriodUni.ModalResult=mrOk then
          begin
            DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
            DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

           //�������� �� �������������

            if trunc(DateB)<Trunc(Date) then
            begin
//              prTestUseTC(cxTextEdit1.Tag,iDate,iC)

              sCards:=its(cxTextEdit1.Tag);
              prFindInputCards(cxTextEdit1.Tag,Trunc(DateB));
              prTestUseTC1(iDate);
            end
            else iDate:=Trunc(DateB-1);

            if Trunc(DateB)>iDate then
            begin //� ������ �������� �� ���� �������������

              bAdd:=True;
              if quTCards.RecordCount>0 then
              begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
                quTCards.Last;

                if quTCardsDATEB.AsDateTime<DateB then
                begin
                  if quTCardsDATEE.AsDateTime>=iMaxDate then
                  begin
                    quTCards.Edit;
                    quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
                    quTCards.Post;
                  end else
                  begin
                    if quTCardsDATEE.AsDateTime>=DateB then
                    begin
                      showmessage('������ ������ ��� ���������, ���������� ����������.');
                      bAdd:=False;
                    end;
                  end;
                end else
                begin
                  showmessage('������ ������ ��� ���������, ���������� ����������.');
                  bAdd:=False;
                end;
              end;

              if bAdd then
              begin
                iId:=GetId('TCards');

                quTCards.Append;
                quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
                quTCardsID.AsInteger:=IId;
                quTCardsDATEB.AsDateTime:=Trunc(DateB);
                quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
                quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
                quTCardsRECEIPTNUM.AsString:=IntToStr(iId);
                quTCardsPOUTPUT.AsString:=IntToStr(Trunc(TCHPVES.AsFloat));
                quTCardsPCOUNT.AsInteger:=TCHPCOUNT.AsInteger;
                quTCardsPVES.AsFloat:=TCHPVES.AsFloat;
                quTCards.Post;

                prSetTC(cxTextEdit1.Tag,True); //TCard - 1

                if Panel3.Visible=False then Panel3.Visible:=True;

                cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
                cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
                cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
                cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
                cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

                //�������� ��� ������������  �� TCS
                ViewTSpec.BeginUpdate;

                CloseTa(taTSpec);

                iMax:=0;
                TCS.First;
                while not TCS.Eof do
                begin
                  if TCSIDT.AsInteger=TCHID.AsInteger then
                  begin

                  //��� ������� �� ���� �� ������� ��� ����� ������
                 //���� ������� ��������� ���� � ��������� ������ � ��� �� ������ ���������
                    if prCanSel(cxTextEdit1.Tag,TCSIDCARD.asInteger) then
                    begin
                      kBrutto:=0;
                      iCurDate:=prDateToI(cxDateEdit1.Date);
                      quFindEU.Active:=False;
                      quFindEU.ParamByName('GOODSID').AsInteger:=TCSIDCARD.asInteger;
                      quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
                      quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
                      quFindEU.Active:=True;

                      if quFindEU.RecordCount>0 then
                      begin
                        quFindEU.First;
                        kBrutto:=quFindEUTO100GRAMM.AsFloat;
                      end;

                      quFindEU.Active:=False;

                      iMax:=iMax+1;
                      taTSpec.Append;
                      taTSpecNum.AsInteger:=iMax;
                      taTSpecIDCARD.AsInteger:=TCSIDCARD.asInteger;
                      taTSpecIDM.AsInteger:=TCSCURMESSURE.AsInteger;
                      taTSpecSM.AsString:=prFindKNM(TCSCURMESSURE.AsInteger,Km);
                      taTSpecKm.AsFloat:=km;
                      taTSpecNETTO.AsFloat:=TCSNETTO.AsFloat;
                      taTSpecBRUTTO.AsFloat:=TCSNETTO.AsFloat*(100+kBrutto)/100;
                      taTSpecKNB.AsFloat:=kBrutto;
                      taTSpecName.AsString:=TCSNAME.AsString;
                      taTSpecTCARD.AsInteger:=prTypeTC(TCSIDCARD.asInteger);//
                      taTSpecNETTO1.AsFloat:=TCSNETTO1.AsFloat;
                      taTSpecComment.AsString:=TCSCOMMENT.AsString;

                      taTSpecPr1.AsFloat:=0;
                      if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((TCSNETTO.AsFloat-TCSNETTO1.AsFloat)/TCSNETTO.AsFloat*10000)/100;

                      taTSpecNetto2.AsFloat:=TCSNETTO1.AsFloat; //���� ���������� �����, ��� ����� ����� �������
                      taTSpecPr2.AsFloat:=0;
                      if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;
                      taTSpecIOBR.AsInteger:=0;

                      taTSpec.Post;
                    end;
{                    else  //������ �� ��������  ��� ��������������
                    begin
                      showmessage('���������� ����������: ����������� ������.');
                    end; }

                  end;
                  TCS.Next;
                end;
                taTSpec.Last;
                ViewTSpec.EndUpdate;

                acEdit.Execute; //����� � ����� ��������������
              end;
            end else
            begin //� ������ �������� ���� ������������� - �������� ����������
              Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). ������� ������ ������ �������� !!!');
              exit;
            end;
          end;
        end;
      end else showmessage('��� ����.');
    end;
  end;
  fmTBuff.Release;

  TCH.Active:=False;
  TCS.Active:=False;
end;

procedure TfmTCard.cxButton9Click(Sender: TObject);
begin
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;
    fmComm:=TfmComm.Create(Application);
    fmComm.Memo1.Text:=quTCardsTEHNO.AsString;
    fmComm.Memo2.Text:=quTCardsOFORM.AsString;
    fmComm.ShowModal;
    if fmComm.ModalResult=mrOk then
    begin
      quTCards.Edit;
      quTCardsTEHNO.AsString:=fmComm.Memo1.Text;
      quTCardsOFORM.AsString:=fmComm.Memo2.Text;
      quTCards.Post;
    end;
    fmComm.Release;
  end;
end;

procedure TfmTCard.cxButton8Click(Sender: TObject);
Var kp,iMax:Integer;
    bb,gg,uu,ee:Real;
begin
  //������ ���
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    CloseTa(taTTK);

    kp:=cxSpinEdit1.EditValue; if kp=0 then kp:=1;
    iMax:=1;
    
    taTSpec.First;
    while not taTSpec.Eof do
    begin
      taTTK.Append;
      taTTKNum.AsInteger:=iMax;
      taTTKCode.AsInteger:=taTSpecIdCard.AsInteger;
      taTTKName.AsString:=taTSpecNAME.AsString;
      if taTSpecIdM.AsInteger=1 then
      begin //��������� � ������ ��������� ���� � �����������
        taTTKSM.AsString:='�.';
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp*1000; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO1.AsFloat/kp*1000;
      end else
      begin
        taTTKSM.AsString:=taTSpecSM.AsString;
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO1.AsFloat/kp;
      end;
      taTTKQuantN10.AsFloat:=taTSpecNETTO1.AsFloat/kp*taTSpecKm.AsFloat*10; //���� � �������� � �� 10 ������
      taTTKQuantN20.AsFloat:=taTSpecNETTO1.AsFloat/kp*taTSpecKm.AsFloat*20; //���� � �������� � �� 20 ������
      taTTKComment.AsString:=taTSpecComment.AsString;
      taTTK.Post;

      taTSpec.Next;
      inc(iMax);
    end;

    frRepTTK.LoadFromFile(CurDir + 'ttk.frf');

    prFinfBGUE(cxTextEdit1.Tag,bb,gg,uu,ee);

    frVariables.Variable['Num']:=quTCardsID.AsString;
    frVariables.Variable['Name']:=cxTextEdit1.Text;
    frVariables.Variable['CodeB']:=Label6.Caption;
    frVariables.Variable['MBlud']:=cxCalcEdit1.EditValue;
    frVariables.Variable['TP']:=quTCardsTEHNO.AsString;
    frVariables.Variable['TREB']:=quTCardsOFORM.AsString;
    frVariables.Variable['BB']:=bb;
    frVariables.Variable['GG']:=gg;
    frVariables.Variable['UU']:=uu;
    frVariables.Variable['EE']:=ee;

    frVariables.Variable['P1']:=quTCardsPAR1.AsString;
    frVariables.Variable['P2']:=quTCardsPAR2.AsString;
    frVariables.Variable['P3']:=quTCardsPAR3.AsString;
    frVariables.Variable['P4']:=quTCardsPAR4.AsString;
    frVariables.Variable['P5']:=quTCardsPAR5.AsString;
    frVariables.Variable['P6']:=quTCardsPAR6.AsString;
    frVariables.Variable['P7']:=quTCardsPAR7.AsString;
    frVariables.Variable['P8']:=quTCardsPAR8.AsString;
    frVariables.Variable['P9']:=quTCardsPAR9.AsString;
    frVariables.Variable['P10']:=quTCardsPAR10.AsString;
    frVariables.Variable['P11']:=quTCardsPAR11.AsString;
    frVariables.Variable['P12']:=quTCardsPAR12.AsString;
    frVariables.Variable['P13']:=quTCardsPAR13.AsString;
    frVariables.Variable['P14']:=quTCardsPAR14.AsString;
    frVariables.Variable['P15']:=quTCardsPAR15.AsString;

    frRepTTK.ReportName:='���.';
    frRepTTK.PrepareReport;
    frRepTTK.ShowPreparedReport;

    taTTK.Active:=False;
  end;
end;

procedure TfmTCard.acInsExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
//  if ViewTSpec.OptionsData.Editing then
  if ViewTSpecIDCARD.Options.Editing then
  begin //��������� ��������� ������
    iMax:=1;

    taTSpec.First;
    if not taTSpec.Eof then
    begin
      taTSpec.Last;
      iMax:=taTSpecNum.AsInteger+1;
    end;

    taTSpec.Append;
    taTSpecNum.AsInteger:=iMax;
    taTSpecIDCARD.AsInteger:=0;
    taTSpecIdM.AsInteger:=-1;
    taTSpecSM.AsString:='';
    taTSpecKm.AsFloat:=0;
    taTSpecName.AsString:='';
    taTSpecNETTO.AsFloat:=0;

    taTSpecNETTO1.AsFloat:=0;
    taTSpecComment.AsString:='';
    taTSpecPr1.AsFloat:=0;

    taTSpecNETTO2.AsFloat:=0;
    taTSpecPr2.AsFloat:=0;

    taTSpecBRUTTO.AsFloat:=0;
    taTSpecKNB.AsFloat:=0;
    taTSpecTCard.AsInteger:=0;
    taTSpecIOBR.AsInteger:=0;
    taTSpec.Post;
  end;
  GTSpec.SetFocus;
  ViewTSpecNAME.Focused:=True;
end;

procedure TfmTCard.acEditExecute(Sender: TObject);
Var iDate:Integer;
    bEdit:Boolean;
begin
  //�������� �� ����������� ��������������
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    bEdit:=True;

    if Trunc(quTCardsDATEB.AsDateTime)<Trunc(Date) then  //��������� ���� ������ ������ �������
    begin
//      prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);

      sCards:=its(cxTextEdit1.Tag);
      prFindInputCards(cxTextEdit1.Tag,Trunc(quTCardsDATEB.AsDateTime));
      prTestUseTC1(iDate);

      if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
        if not CanDo('prRecalcTCard') then
        begin
          Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). �������������� ���������� !!!');
          bEdit:=False;
        end else
        begin
           if MessageDlg('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+'). ����������� �������� ����������. ������������� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
           begin
             bEdit:=True;
           end else
           begin
             bEdit:=False;
           end;
        end;
      end;
    end;

    if bEdit then
    begin
//      fmTCard.ViewTSpec.OptionsData.Editing:=True;
      prColsEnables(True);

      cxLabel1.Enabled:=True;
      cxCheckBox1.Enabled:=True;
      cxLabel2.Enabled:=True;
      Image1.Enabled:=True;
      Image2.Enabled:=True;
      StatusBar1.Panels[1].Text:='��������������, ��� ���������� Ctrl+S';
      cxButton10.Enabled:=True;
      cxSpinEdit1.Properties.ReadOnly:=False;
      cxCalcEdit1.Properties.ReadOnly:=False;
      cxButton12.Enabled:=True;
//      cxButton13.Enabled:=True;
      prSetOn;
    end;
  end;
end;

procedure TfmTCard.acDelExecute(Sender: TObject);
begin
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
//  if ViewTSpec.OptionsData.Editing then //��������� ��������� ������
  if ViewTSpecIDCARD.Options.Editing then
  with dmO do
  begin
    if taTSpec.RecordCount>0 then
    begin
      taTSpec.Delete;
    end;
  end;
end;

procedure TfmTCard.ViewTSpecEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecIDCARD' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          if prCanSel(cxTextEdit1.Tag,iCode) then
          begin
            taTSpec.Edit;
            taTSpecIDCARD.AsInteger:=iCode;
            taTSpecName.AsString:=quFCardNAME.AsString;
            taTSpecNETTO.AsFloat:=0;
            taTSpecBRUTTO.AsFloat:=0;
            taTSpecNETTO1.AsFloat:=0;
            taTSpecPr1.AsFloat:=0;
            taTSpecNETTO2.AsFloat:=0;
            taTSpecPr2.AsFloat:=0;
            taTSpecComment.AsString:='';
            taTSpecKNB.AsFloat:=0;

            if cxCheckBox1.Checked then
            begin
              taTSpecIdM.AsInteger:=4;
              taTSpecSM.AsString:='�.';
              taTSpecKm.AsFloat:=0.001;
            end else
            begin
              taTSpecIdM.AsInteger:=quFCardIMESSURE.AsInteger;
              taTSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
              taTSpecKm.AsFloat:=Km;
            end;
            taTSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
            taTSpec.Post;

          end else
          begin
            Showmessage('����������� ������, ���������� ����������.');
          end;
        end;
      end;
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecNAME' then
      begin
        if AEdit.EditingValue <> null then
        begin
          sName:=VarAsType(AEdit.EditingValue, varString);

          fmFCards.ViewFCards.BeginUpdate;
          quFCard.Active:=False;
          quFCard.SelectSQL.Clear;
          quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
          quFCard.SelectSQL.Add('FROM OF_CARDS');
          quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
          quFCard.SelectSQL.Add('and IACTIVE>0');
          quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
          quFCard.SelectSQL.Add('Order by NAME');

          quFCard.Active:=True;

//        bAdd:=True;
        //�������� ����� ������ � ����� ������

          quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
          prRowFocus(fmFCards.ViewFCards);
          fmFCards.ViewFCards.EndUpdate;

          fmFCards.ShowModal;
          if fmFCards.ModalResult=mrOk then
          begin
            if quFCard.RecordCount>0 then
            begin
              if prCanSel(cxTextEdit1.Tag,quFCardID.AsInteger) then
              begin
                taTSpec.Edit;
                taTSpecIDCARD.AsInteger:=quFCardID.AsInteger;

                if cxCheckBox1.Checked then
                begin
                  taTSpecIdM.AsInteger:=4;
                  taTSpecSM.AsString:='�.';
                  taTSpecKm.AsFloat:=0.001;
                end else
                begin
                  taTSpecIdM.AsInteger:=quFCardIMESSURE.AsInteger;
                  taTSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
                  taTSpecKm.AsFloat:=Km;
                end;

                taTSpecName.AsString:=quFCardNAME.AsString;
                taTSpecNETTO.AsFloat:=0;
                taTSpecBRUTTO.AsFloat:=0;
                taTSpecNETTO1.AsFloat:=0;
                taTSpecPr1.AsFloat:=0;

                taTSpecNETTO2.AsFloat:=0;
                taTSpecPr2.AsFloat:=0;

                taTSpecComment.AsString:='';
                taTSpecKNB.AsFloat:=0;
                taTSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
                taTSpeciOBR.AsInteger :=0;
                taTSpec.Post;
              end else
              begin
                Showmessage('����������� ������, ���������� ����������.');
              end;
              AEdit.SelectAll;
            end;
          end;
//        bAdd:=False;
        end;
      end;
    end else
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecIDCARD' then
        if fTestKey(Key)=False then
          if taTSpec.State in [dsEdit,dsInsert] then taTSpec.Post;
  end;
end;

procedure TfmTCard.acSaveExecute(Sender: TObject);
begin
  //���������� ��
  prSaveTForce;
end;

procedure  TfmTCard.prSaveTForce;
Var bErr:Boolean;
begin
  cxButton10.Enabled:=False;

  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

//  if ViewTSpec.OptionsData.Editing then //��������� ��������� ������
  if ViewTSpecIDCARD.Options.Editing then
  with dmO do
  begin
    ViewTSpec.BeginUpdate;
    try
      bErr:=False;
      if cxCalcEdit1.EditValue=0 then bErr:=True;
      taTSpec.First;
      while not taTSpec.Eof do
      begin
        if taTSpecIdCard.AsInteger=0 then bErr:=True;
        if taTSpecNetto.AsFloat=0 then bErr:=True;
        if taTSpecNetto1.AsFloat=0 then bErr:=True;
        if taTSpecNetto2.AsFloat=0 then bErr:=True;
        taTSpec.Next;
      end;
    finally
      ViewTSpec.EndUpdate;
    end;

    if bErr then
    begin
      showmessage('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
      cxButton10.Enabled:=True;
      exit;
    end;

    ViewTSpec.BeginUpdate;
    try
      quTSpec.Active:=False;
      quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
      quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
      quTSpec.Active:=True;

      while not quTSpec.Eof do quTSpec.Delete;

      if taTSpec.State in [dsEdit,dsInsert] then taTSpec.Post;

      taTSpec.First;
      while not taTSpec.eof do
      begin
        quTSpec.Append;
        quTSpecIDC.AsInteger:=quTCardsIDCARD.AsInteger;
        quTSpecIDT.AsInteger:=quTCardsID.AsInteger;
        quTSpecID.AsInteger:=taTSpecNum.AsInteger;
        quTSpecIDCARD.AsInteger:=taTSpecIdCard.AsInteger;
        quTSpecCURMESSURE.AsInteger:=taTSpecIdM.AsInteger;
        quTSpecNETTO.AsFloat:=taTSpecNetto.AsFloat;
        quTSpecBRUTTO.AsFloat:=taTSpecBrutto.AsFloat;
        //��� ����� ��������� km �.�. ��� ����� ����� ��� kbrutto
        quTSpecKNB.AsFloat:=taTSpecKm.AsFloat; //���� ���� ����� �� ���������
        quTSpecNetto1.AsFloat:=taTSpecNETTO1.AsFloat;
        quTSpecComment.AsString:=taTSpecComment.AsString;
        quTSpecNetto2.AsFloat:=taTSpecNETTO2.AsFloat;
        quTSpecIOBR.AsInteger:=taTSpecIOBR.AsInteger;
        quTSpec.Post;

        taTSpec.Next;
      end;

      StatusBar1.Panels[1].Text:='C��������� ��.';
    finally
      ViewTSpec.EndUpdate;
      cxButton10.Enabled:=True;
    end;
  end;
end;

procedure TfmTCard.taTSpecNettoChange(Sender: TField);
begin
  if iColTC=1 then
  begin
    taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+taTSpecKNB.AsFloat)/100;

    taTSpecNETTO1.AsFloat:=taTSpecNETTO.AsFloat;
    taTSpecPr1.AsFloat:=0;
    if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;

    taTSpecNETTO2.AsFloat:=(1-taTSpecPr2.AsFloat/100)*taTSpecNETTO1.AsFloat

  end;
end;

procedure TfmTCard.ViewTSpecSMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taTSpecIDM.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taTSpec.Edit;
        taTSpecIdM.AsInteger:=iM;
        taTSpecKm.AsFloat:=rK;
        taTSpecSM.AsString:=Sm;
        taTSpec.Post;
      end;
    end;
  end;
end;

procedure TfmTCard.cxButton10Click(Sender: TObject);
begin
  prSaveTForce;
end;

procedure TfmTCard.ViewTSpecCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;
begin
//  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewTSpec.ColumnCount-1 do
  begin
    if ViewTSpec.Columns[i].Name='ViewTSpecTCard' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=1 then
  begin
    ACanvas.Canvas.Font.Color := clHotLight;
    ACanvas.Canvas.Font.Style :=[fsBold];
  end;
end;

procedure TfmTCard.acTTKViewExecute(Sender: TObject);
Var iCurDate:INteger;
    kBrutto,rN:Real;
    iMax:Integer;
begin
//�������� ���
  if (taTSpec.RecordCount>0)and(taTSpec.Active=True)and(taTSpecTCard.AsInteger=1) then
  begin
    //������� ��� �� ��������
    with dmO do
    begin
      quTC.Active:=False;
      quTC.ParamByName('IDGOOD').AsInteger:=taTSpecIdCard.AsInteger;
      quTC.Active:=True;

      CloseTa(fmTTKView.taTSpecV);
//      fmTCard.taTSpec.First; while not fmTCard.taTSpec.Eof do fmTCard.taTSpec.delete;

      if quTC.RecordCount=0 then
      begin
        fmTTKView.taTSpecV.Active:=False;
        quTC.Active:=False;
        showmessage('��� ����� �� �������.');
      end  else
      begin
        quTC.Last;

        fmTTKView.Caption:=taTSpecName.AsString+' ( c '+FormatDateTime('dd.mm.yyyy',quTCDATEB.AsDateTime)+')';

        fmTTKView.cxTextEdit3.Text:=quTCRECEIPTNUM.AsString;
        fmTTKView.cxTextEdit4.Text:=quTCPOUTPUT.AsString;
        fmTTKView.cxSpinEdit1.EditValue:=quTCPCOUNT.AsInteger;
        fmTTKView.cxCalcEdit1.EditValue:=quTCPVES.AsFloat;

        fmTTKView.ViewTSpecV.BeginUpdate;

        quTSpec.Active:=False;
        quTSpec.ParamByName('IDC').AsInteger:=quTCIDCARD.AsInteger;
        quTSpec.ParamByName('IDTC').AsInteger:=quTCID.AsInteger;
        quTSpec.Active:=True;

        iMax:=1;
        quTSpec.First;
        while not quTSpec.Eof do
        begin
          fmTTKView.taTSpecV.Append;
          fmTTKView.taTSpecVNum.AsInteger:=iMax;
          fmTTKView.taTSpecVIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
          fmTTKView.taTSpecVIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
          fmTTKView.taTSpecVSM.AsString:=quTSpecNAMESHORT.AsString;
          fmTTKView.taTSpecVKm.AsFloat:=quTSpecKOEF.AsFloat;
          fmTTKView.taTSpecVName.AsString:=quTSpecNAME.AsString;
          fmTTKView.taTSpecVNetto.AsFloat:=quTSpecNETTO.AsFloat;
          fmTTKView.taTSpecVBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
          fmTTKView.taTSpecVKnb.AsFloat:=0;
          fmTTKView.taTSpecVTCard.AsInteger:=quTSpecTCARD.AsInteger;
          fmTTKView.taTSpecVPrice.AsFloat:=0;
          fmTTKView.taTSpecVPrice1000.AsFloat:=0;
          fmTTKView.taTSpecVSumma.AsFloat:=0;
          fmTTKView.taTSpecV.Post;

          quTSpec.Next;   inc(iMax);
        end;
        quTSpec.Active:=False;

        //������������� ������
        iCurDate:=prDateToI(Date);

        fmTTKView.taTSpecV.First;
        while not fmTTKView.taTSpecV.Eof do
        begin
          kBrutto:=0;
          quFindEU.Active:=False;
          quFindEU.ParamByName('GOODSID').AsInteger:=fmTTKView.taTSpecVIDCARD.AsInteger;
          quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
          quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
          quFindEU.Active:=True;

          if quFindEU.RecordCount>0 then
          begin
            quFindEU.First;
            kBrutto:=quFindEUTO100GRAMM.AsFloat;
          end;

          rN:=fmTTKView.taTSpecVNETTO.AsFloat;
          quFindEU.Active:=False;

          fmTTKView.taTSpecV.Edit;
          fmTTKView.taTSpecVBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
          fmTTKView.taTSpecVKNB.AsFloat:=kBrutto;
          fmTTKView.taTSpecV.Post;
          fmTTKView.taTSpecV.Next;
        end;
        fmTTKView.taTSpecV.First;
        quTC.Active:=False;
        fmTTKView.ViewTSpecV.EndUpdate;

        fmTTKView.ViewTSpecVPrice1000.Visible:=False;
        fmTTKView.ViewTSpecVSumma.Visible:=False;

        fmTTKView.ShowModal;
        fmTTKView.taTSpecV.Active:=False;
      end;
    end;
  end;
end;

procedure TfmTCard.ViewTSpecDblClick(Sender: TObject);
begin
  acTTKView.Execute;
end;

procedure TfmTCard.ViewTSpecEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iColTC:=0;
  if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecNETTO' then iColTC:=1;
  if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecNetto1' then iColTC:=2;
  if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecPr1' then iColTC:=3;
  if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecNetto2' then iColTC:=4;
  if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecPr2' then iColTC:=5;
end;

procedure TfmTCard.taTSpecNetto1Change(Sender: TField);
begin
  if iColTC=2 then
  begin
    if taTSpecNETTO1.AsFloat>taTSpecNETTO.AsFloat then
    begin
      taTSpecNETTO.AsFloat:=taTSpecNETTO1.AsFloat;
      taTSpecBRUTTO.AsFloat:=taTSpecNETTO1.AsFloat*(100+taTSpecKNB.AsFloat)/100;
    end;
    taTSpecPr1.AsFloat:=0;
    if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;

    taTSpecNETTO2.AsFloat:=(1-taTSpecPr2.AsFloat/100)*taTSpecNETTO1.AsFloat

  end;
end;

procedure TfmTCard.taTSpecPr1Change(Sender: TField);
begin
  if iColTC=3 then
  begin
    taTSpecNetto1.AsFloat:=(1-taTSpecPr1.AsFloat/100)*taTSpecNetto.AsFloat;
    taTSpecNETTO2.AsFloat:=(1-taTSpecPr2.AsFloat/100)*taTSpecNETTO1.AsFloat
  end;
end;

procedure TfmTCard.cxButton11Click(Sender: TObject);
Var kp,iMax:Integer;
begin
  //������ ���
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    CloseTa(taTTK);

    kp:=cxSpinEdit1.EditValue; if kp=0 then kp:=1;
    iMax:=1;
    
    taTSpec.First;
    while not taTSpec.Eof do
    begin
      taTTK.Append;
      taTTKNum.AsInteger:=iMax;
      taTTKCode.AsInteger:=taTSpecIdCard.AsInteger;
      taTTKName.AsString:=taTSpecNAME.AsString;
      if taTSpecIdM.AsInteger=1 then
      begin //��������� � ������ ��������� ���� � �����������
        taTTKSM.AsString:='�.';
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp*1000; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO1.AsFloat/kp*1000;
      end else
      begin
        taTTKSM.AsString:=taTSpecSM.AsString;
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO1.AsFloat/kp;
      end;
      taTTKQuantN10.AsFloat:=taTSpecNETTO1.AsFloat/kp*taTSpecKm.AsFloat*10; //���� � �������� � �� 10 ������
      taTTKQuantN20.AsFloat:=taTSpecNETTO1.AsFloat/kp*taTSpecKm.AsFloat*20; //���� � �������� � �� 20 ������
      taTTKComment.AsString:=taTSpecComment.AsString;
      taTTK.Post;

      taTSpec.Next;
      inc(iMax);
    end;

    frRepTTK.LoadFromFile(CurDir + 'ttk1.frf');

    frVariables.Variable['Num']:=quTCardsID.AsString;
    frVariables.Variable['Name']:=cxTextEdit1.Text;
    frVariables.Variable['CodeB']:=Label6.Caption;
    frVariables.Variable['MBlud']:=cxCalcEdit1.EditValue;
    frVariables.Variable['TP']:=quTCardsTEHNO.AsString;
    frVariables.Variable['TREB']:=quTCardsOFORM.AsString;

    frRepTTK.ReportName:='���.';
    frRepTTK.PrepareReport;
    frRepTTK.ShowPreparedReport;

    taTTK.Active:=False;
  end;
end;

procedure TfmTCard.taTSpecNetto2Change(Sender: TField);
begin
  if iColTC=4 then
  begin
    taTSpecPr2.AsFloat:=0;
    if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;
  end;
end;

procedure TfmTCard.taTSpecPr2Change(Sender: TField);
begin
  if iColTC=5 then
  begin
    taTSpecNETTO2.AsFloat:=(1-taTSpecPr2.AsFloat/100)*taTSpecNETTO1.AsFloat
  end;
end;

procedure TfmTCard.cxButton12Click(Sender: TObject);
Var rVes:Real;
    iPor:Integer;
begin
  ViewTSpec.BeginUpdate;
  rVes:=0;
  taTSpec.First;
  while not taTSpec.Eof do
  begin
    rVes:=rVes+taTSpecNetto2.AsFloat*taTSpecKm.AsFloat; //� ��������
    taTSpec.Next;
  end;
  iPor:=1;
  if cxSpinEdit1.Value>0 then iPor:=cxSpinEdit1.Value;
  cxCalcEdit1.Value:=RoundEx(rVes*1000/iPor);//��������� ������ � ������ �� ������
  ViewTSpec.EndUpdate;
end;

procedure TfmTCard.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  cxButton13.Enabled:=True;
end;

procedure TfmTCard.ViewTSpecCommentPropertiesChange(Sender: TObject);
begin
  cxButton13.Enabled:=True;
end;

procedure TfmTCard.ViewTSpecIObrPropertiesChange(Sender: TObject);
begin
  cxButton13.Enabled:=True;
end;

procedure TfmTCard.cxButton13Click(Sender: TObject);
begin
  with dmO do
  begin
    quUpd.SQL.Clear;
    quUpd.SQL.Add('Update OF_CARDST Set IOBR='+its(cxLookupComboBox1.EditValue)+' where ID='+its(quTCardsID.AsInteger));
    quUpd.ExecQuery;

    ViewTSpec.BeginUpdate;

{    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;
}
    if taTSpec.State in [dsEdit,dsInsert] then taTSpec.Post;

    taTSpec.First;
    while not taTSpec.Eof do
    begin
 //         quTSpecIOBR.AsInteger:=taTSpecIOBR.AsInteger;
      if taTSpecIOBR.AsInteger>=0 then
      begin
        quUpd.SQL.Clear;
        quUpd.SQL.Add('Update OF_CARDSTSPEC Set IOBR='+its(taTSpecIOBR.AsInteger)+' where IDT='+its(quTCardsID.AsInteger)+' and ID='+its(taTSpecNum.AsInteger));
        quUpd.ExecQuery;
      end;

      taTSpec.Next;
    end;
    StatusBar1.Panels[1].Text:='C��������� ��.';

    taTSpec.First;
    ViewTSpec.EndUpdate;
  end;
end;

procedure TfmTCard.cxButton14Click(Sender: TObject);
Var bo,go,uo,bb,gg,uu:Real;
    so:String;
    rQ,rQ1:Real;
begin
  fmCalcCal:=tfmCalcCal.Create(Application);
  with fmCalcCal do
  begin
    CloseTe(taCal);
    ViewTSpec.BeginUpdate;
    ViewCal.BeginUpdate;

    if taTSpec.State in [dsEdit,dsInsert] then taTSpec.Post;

    taTSpec.First;
    while not taTSpec.Eof do
    begin
      prFinfBGU(taTSpecIdCard.AsInteger,bb,gg,uu);
      prFindObr(taTSpecIObr.AsInteger,bo,go,uo,so);
      rQ:=taTSpecNetto1.AsFloat/cxSpinEdit1.Value; //�� ���� ������
      rQ1:=taTSpecNetto2.AsFloat;

      taCal.Append;
      taCalNum.AsInteger:=taTSpecNum.AsInteger;
      taCalIdCard.AsInteger:=taTSpecIdCard.AsInteger;
      taCalName.AsString:=taTSpecName.AsString;
      taCalObr.AsString:=so;
      taCalMassa.AsFloat:=rQ;
      taCalMassa1.AsFloat:=rQ1;
      taCalb1.AsFloat:=rQ/100*bb;
      taCalb2.AsFloat:=rQ/100*bb - rQ/100*bb*bo/100;
      taCalg1.AsFloat:=rQ/100*gg;
      taCalg2.AsFloat:=rQ/100*gg - rQ/100*gg*go/100;
      taCalu1.AsFloat:=rQ/100*uu;
      taCalu2.AsFloat:=rQ/100*uu - rQ/100*uu*uo/100;
      taCalkb.AsFloat:=taTSpecKm.AsFloat;
      taCalim.AsInteger:=taTSpecIdM.AsInteger;
      taCalsm.AsString:=taTSpecSM.AsString;
      taCal.Post;

      taTSpec.Next;
    end;

    bb:=0; gg:=0; uu:=0; //rQ1:=0;

    try
      taTSpec.First;

      taCal.First;
      while not taCal.Eof do
      begin //��� ��������� � �� (�������� ���� � 1-�);
        bb:=bb+taCalb2.AsFloat*taCalkb.AsFloat;
        gg:=gg+taCalg2.AsFloat*taCalkb.AsFloat;
        uu:=uu+taCalu2.AsFloat*taCalkb.AsFloat;
//      rQ:=rQ+taCalMassa.AsFloat*taCalkb.AsFloat;
//      rQ1:=rQ1+taCalMassa1.AsFloat*taCalkb.AsFloat; //����� �������� �����

        taCal.Next;
      end;

      //��������� ��� � ������
      bb:=bb*1000;
      gg:=gg*1000;
      uu:=uu*1000;

//    rQ:=rQ*1000;
//    rQ1:=rQ1*1000;

    //��������� �������� ��������� �� ����� ���������
      rQ:=fmTCard.cxCalcEdit1.EditValue;
      prFindObr(cxLookupComboBox1.EditValue,bo,go,uo,so);

      bb:=bb-bb*bo/100;
      gg:=gg-gg*go/100;
      uu:=uu-uu*uo/100;

    //���������� ���-�� �� 100�
      bb:=bb*100/rQ;
      gg:=gg*100/rQ;
      uu:=uu*100/rQ;
    except
    end;
    cxCalcEdit1.EditValue:=4*(bb+uu)+9*gg;
    cxCalcEdit2.EditValue:=bb;
    cxCalcEdit3.EditValue:=gg;
    cxCalcEdit4.EditValue:=uu;

    cxCalcEdit1.Tag:=cxTextEdit1.Tag;

    ViewTSpec.EndUpdate;
    ViewCal.EndUpdate;


  end;
  fmCalcCal.ShowModal;
  fmCalcCal.taCal.Active:=False;
  fmCalcCal.Release;
end;

procedure TfmTCard.cxButton15Click(Sender: TObject);
Var iC:Integer;
begin
  //�������������� ���������
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;
    try
      fmTCardsAddPars:=TfmTCardsAddPars.Create(Application);
      with fmTCardsAddPars do
      begin
        closete(teAddPars);
        ViewAddPars.BeginUpdate;
        iC:=1;

        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='�������� ���� ����� �������'; teAddParsValPar.AsString:=quTCardsPAR1.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='�������� ���� ����'; teAddParsValPar.AsString:=quTCardsPAR2.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='�������� ���� ������'; teAddParsValPar.AsString:=quTCardsPAR3.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='����� �� � 1 � �� �����'; teAddParsValPar.AsString:=quTCardsPAR4.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='����'; teAddParsValPar.AsString:=quTCardsPAR5.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='�������� ���� ������'; teAddParsValPar.AsString:=quTCardsPAR6.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='���������������������� �����������'; teAddParsValPar.AsString:=quTCardsPAR7.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='���������� ��������������, � �.�. �����������'; teAddParsValPar.AsString:=quTCardsPAR8.AsString; teAddPars.Post; inc(iC);
//9
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='������� ���'; teAddParsValPar.AsString:=quTCardsPAR9.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='����'; teAddParsValPar.AsString:=quTCardsPAR10.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='������������'; teAddParsValPar.AsString:=quTCardsPAR11.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='����'; teAddParsValPar.AsString:=quTCardsPAR12.AsString; teAddPars.Post; inc(iC);
        teAddPars.Append; teAddParsNum.AsInteger:=iC; teAddParsNamePar.AsString:='�����'; teAddParsValPar.AsString:=quTCardsPAR13.AsString; teAddPars.Post; 


        ViewAddPars.EndUpdate;
      end;

      fmTCardsAddPars.ShowModal;
      if fmTCardsAddPars.ModalResult=mrOk then
      begin
        delay(10);

        with fmTCardsAddPars do
        begin
          ViewAddPars.BeginUpdate;
          dsteAddPars.DataSet:=nil;

          quTCards.Edit;
          teAddPars.First;
          while not teAddPars.Eof do
          begin
            quTCards.FieldByName('PAR'+its(teAddParsNum.AsInteger)).AsString:=teAddParsValPar.AsString;
            teAddPars.Next;
          end;
          quTCards.Post;

          ViewAddPars.EndUpdate;

          teAddPars.Active:=False;
        end;
      end;
    finally
      fmTCardsAddPars.Release;
    end;
  end;
end;


end.
