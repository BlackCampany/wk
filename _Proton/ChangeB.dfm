object fmChangeB: TfmChangeB
  Left = 454
  Top = 212
  BorderStyle = bsDialog
  Caption = #1057#1090#1072#1090#1091#1089' '#1086#1087#1083#1072#1090#1099
  ClientHeight = 174
  ClientWidth = 358
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 358
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = 16760962
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 48
      Top = 16
      Width = 101
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 204
      Top = 16
      Width = 99
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxRadioButton1: TcxRadioButton
    Left = 36
    Top = 20
    Width = 113
    Height = 17
    Caption = #1085#1077' '#1086#1087#1083#1072#1095#1077#1085
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton2: TcxRadioButton
    Left = 36
    Top = 48
    Width = 113
    Height = 17
    Caption = #1086#1087#1083#1072#1095#1077#1085' '#1095#1072#1089#1090#1080#1095#1085#1086
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton3: TcxRadioButton
    Left = 36
    Top = 76
    Width = 113
    Height = 17
    Caption = #1086#1087#1083#1072#1095#1077#1085
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
  end
end
