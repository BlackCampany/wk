unit SetCardsParams1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxDropDownEdit, cxCalc, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, StdCtrls, ExtCtrls, Menus,
  cxLookAndFeelPainters, cxButtons, cxCheckBox, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit;

type
  TfmSetCardsParams1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Label4: TLabel;
    cxCheckBox6: TcxCheckBox;
    cxButtonEdit1: TcxButtonEdit;
    Label5: TLabel;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Label5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetCardsParams1: TfmSetCardsParams1;

implementation

uses MDB, mMakers;

{$R *.dfm}

procedure TfmSetCardsParams1.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  fmMakers.Show; 
end;

procedure TfmSetCardsParams1.FormCreate(Sender: TObject);
begin
  fmSetCardsParams1.cxButtonEdit1.Tag:=0;
  fmSetCardsParams1.cxButtonEdit1.Text:='';
end;

procedure TfmSetCardsParams1.Label5Click(Sender: TObject);
begin
  fmSetCardsParams1.cxButtonEdit1.Tag:=0;
  fmSetCardsParams1.cxButtonEdit1.Text:='';
end;

end.
