object fmAddSingle: TfmAddSingle
  Left = 420
  Top = 380
  BorderStyle = bsDialog
  Caption = 'fmAddSingle'
  ClientHeight = 189
  ClientWidth = 384
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 133
    Width = 384
    Height = 56
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 64
      Top = 16
      Width = 97
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 200
      Top = 16
      Width = 97
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072'  Esc'
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLabel1: TcxLabel
    Left = 12
    Top = 24
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 112
    Top = 22
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Text = 'cxTextEdit1'
    Width = 193
  end
  object cxLabel2: TcxLabel
    Left = 24
    Top = 88
    Caption = #1057#1090#1072#1090#1091#1089
    Transparent = True
    Visible = False
  end
  object cxCheckBox1: TcxCheckBox
    Left = 88
    Top = 82
    Caption = #1040#1082#1090#1080#1074#1085#1099#1081
    TabOrder = 4
    Visible = False
    Width = 121
  end
  object cxLabel3: TcxLabel
    Left = 12
    Top = 48
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
    Visible = False
  end
  object cxTextEdit2: TcxTextEdit
    Left = 112
    Top = 46
    Properties.MaxLength = 100
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Text = 'cxTextEdit2'
    Visible = False
    Width = 245
  end
  object amAddSingle: TActionManager
    Left = 264
    Top = 72
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      OnExecute = acExitExecute
    end
  end
end
