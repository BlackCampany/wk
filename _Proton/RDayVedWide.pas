unit RDayVedWide;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmRDayVedWide = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    dsRDayVedW: TDataSource;
    FormPlacement1: TFormPlacement;
    SpeedItem4: TSpeedItem;
    teRDayVedW: TdxMemData;
    teRDayVedWIdCode1: TIntegerField;
    teRDayVedWNameC: TStringField;
    teRDayVedWiM: TSmallintField;
    teRDayVedWIdGroup: TIntegerField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    teRDayVedWNameGr1: TStringField;
    teRDayVedWNameGr2: TStringField;
    teRDayVedWIdSGroup: TIntegerField;
    teRDayVedWiBrand: TIntegerField;
    teRDayVedWiCat: TSmallintField;
    amRepDayOb: TActionManager;
    acMoveRepOb: TAction;
    teRDayVedWNameGr3: TStringField;
    teRDayVedWQR: TFloatField;
    teRDayVedWPrice: TFloatField;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teRDayVedWAvrSpeed: TFloatField;
    teRDayVedWQRDay: TFloatField;
    teRDayVedWsTop: TStringField;
    teRDayVedWsNov: TStringField;
    ViewRDayVedW: TcxGridDBTableView;
    LevelRDayVedW: TcxGridLevel;
    GridRDayVedW: TcxGrid;
    teRDayVedWDayN: TIntegerField;
    teRDayVedWD01: TFloatField;
    teRDayVedWD02: TFloatField;
    teRDayVedWD03: TFloatField;
    teRDayVedWD04: TFloatField;
    teRDayVedWD05: TFloatField;
    teRDayVedWD06: TFloatField;
    teRDayVedWD07: TFloatField;
    teRDayVedWD08: TFloatField;
    teRDayVedWD09: TFloatField;
    teRDayVedWD10: TFloatField;
    teRDayVedWD11: TFloatField;
    teRDayVedWD12: TFloatField;
    teRDayVedWD13: TFloatField;
    teRDayVedWD14: TFloatField;
    teRDayVedWD15: TFloatField;
    teRDayVedWD16: TFloatField;
    teRDayVedWD17: TFloatField;
    teRDayVedWD18: TFloatField;
    teRDayVedWD19: TFloatField;
    teRDayVedWD20: TFloatField;
    teRDayVedWD21: TFloatField;
    teRDayVedWD22: TFloatField;
    teRDayVedWD23: TFloatField;
    teRDayVedWD24: TFloatField;
    teRDayVedWD25: TFloatField;
    teRDayVedWD26: TFloatField;
    teRDayVedWD27: TFloatField;
    teRDayVedWD28: TFloatField;
    teRDayVedWD29: TFloatField;
    teRDayVedWD30: TFloatField;
    ViewRDayVedWIdCode1: TcxGridDBColumn;
    ViewRDayVedWNameC: TcxGridDBColumn;
    ViewRDayVedWiM: TcxGridDBColumn;
    ViewRDayVedWQR: TcxGridDBColumn;
    ViewRDayVedWNameGr1: TcxGridDBColumn;
    ViewRDayVedWNameGr2: TcxGridDBColumn;
    ViewRDayVedWNameGr3: TcxGridDBColumn;
    ViewRDayVedWiCat: TcxGridDBColumn;
    ViewRDayVedWPrice: TcxGridDBColumn;
    ViewRDayVedWAvrSpeed: TcxGridDBColumn;
    ViewRDayVedWQRDay: TcxGridDBColumn;
    ViewRDayVedWsTop: TcxGridDBColumn;
    ViewRDayVedWsNov: TcxGridDBColumn;
    ViewRDayVedWDayN: TcxGridDBColumn;
    ViewRDayVedWD01: TcxGridDBColumn;
    ViewRDayVedWD02: TcxGridDBColumn;
    ViewRDayVedWD03: TcxGridDBColumn;
    ViewRDayVedWD04: TcxGridDBColumn;
    ViewRDayVedWD05: TcxGridDBColumn;
    ViewRDayVedWD06: TcxGridDBColumn;
    ViewRDayVedWD07: TcxGridDBColumn;
    ViewRDayVedWD08: TcxGridDBColumn;
    ViewRDayVedWD09: TcxGridDBColumn;
    ViewRDayVedWD10: TcxGridDBColumn;
    ViewRDayVedWD11: TcxGridDBColumn;
    ViewRDayVedWD12: TcxGridDBColumn;
    ViewRDayVedWD13: TcxGridDBColumn;
    ViewRDayVedWD14: TcxGridDBColumn;
    ViewRDayVedWD15: TcxGridDBColumn;
    ViewRDayVedWD16: TcxGridDBColumn;
    ViewRDayVedWD17: TcxGridDBColumn;
    ViewRDayVedWD18: TcxGridDBColumn;
    ViewRDayVedWD19: TcxGridDBColumn;
    ViewRDayVedWD20: TcxGridDBColumn;
    ViewRDayVedWD21: TcxGridDBColumn;
    ViewRDayVedWD22: TcxGridDBColumn;
    ViewRDayVedWD23: TcxGridDBColumn;
    ViewRDayVedWD24: TcxGridDBColumn;
    ViewRDayVedWD25: TcxGridDBColumn;
    ViewRDayVedWD26: TcxGridDBColumn;
    ViewRDayVedWD27: TcxGridDBColumn;
    ViewRDayVedWD28: TcxGridDBColumn;
    ViewRDayVedWD29: TcxGridDBColumn;
    ViewRDayVedWD30: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewRDayVedWSelectionChanged(Sender: TcxCustomGridTableView);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ptDaysColsW;
  end;

var
  fmRDayVedWide: TfmRDayVedWide;

implementation

uses Un1, MDB, MT, Move;

{$R *.dfm}

procedure TfmRDayVedWide.ptDaysColsW;
Var dDate:TDateTime;
begin
  dDate:=Date;

  ViewRDayVedWD30.Caption:=ds1(dDate-1);
  if dw(dDate-1)>5  then ViewRDayVedWD30.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD30.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD29.Caption:=ds1(dDate-2);
  if dw(dDate-2)>5  then ViewRDayVedWD29.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD29.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD28.Caption:=ds1(dDate-3);
  if dw(dDate-3)>5  then ViewRDayVedWD28.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD28.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD27.Caption:=ds1(dDate-4);
  if dw(dDate-4)>5  then ViewRDayVedWD27.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD27.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD26.Caption:=ds1(dDate-5);
  if dw(dDate-5)>5  then ViewRDayVedWD26.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD26.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD25.Caption:=ds1(dDate-6);
  if dw(dDate-6)>5  then ViewRDayVedWD25.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD25.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD24.Caption:=ds1(dDate-7);
  if dw(dDate-7)>5  then ViewRDayVedWD24.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD24.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD23.Caption:=ds1(dDate-8);
  if dw(dDate-8)>5  then ViewRDayVedWD23.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD23.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD22.Caption:=ds1(dDate-9);
  if dw(dDate-9)>5  then ViewRDayVedWD22.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD22.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD21.Caption:=ds1(dDate-10);
  if dw(dDate-10)>5  then ViewRDayVedWD21.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD21.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD20.Caption:=ds1(dDate-11);
  if dw(dDate-11)>5  then ViewRDayVedWD20.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD20.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD19.Caption:=ds1(dDate-12);
  if dw(dDate-12)>5  then ViewRDayVedWD19.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD19.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD18.Caption:=ds1(dDate-13);
  if dw(dDate-13)>5  then ViewRDayVedWD18.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD18.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD17.Caption:=ds1(dDate-14);
  if dw(dDate-14)>5  then ViewRDayVedWD17.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD17.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD16.Caption:=ds1(dDate-15);
  if dw(dDate-15)>5  then ViewRDayVedWD16.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD16.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD15.Caption:=ds1(dDate-16);
  if dw(dDate-16)>5  then ViewRDayVedWD15.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD15.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD14.Caption:=ds1(dDate-17);
  if dw(dDate-17)>5  then ViewRDayVedWD14.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD14.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD13.Caption:=ds1(dDate-18);
  if dw(dDate-18)>5  then ViewRDayVedWD13.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD13.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD12.Caption:=ds1(dDate-19);
  if dw(dDate-19)>5  then ViewRDayVedWD12.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD12.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD11.Caption:=ds1(dDate-20);
  if dw(dDate-20)>5  then ViewRDayVedWD11.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD11.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD10.Caption:=ds1(dDate-21);
  if dw(dDate-21)>5  then ViewRDayVedWD10.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD10.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD09.Caption:=ds1(dDate-22);
  if dw(dDate-22)>5  then ViewRDayVedWD09.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD09.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD08.Caption:=ds1(dDate-23);
  if dw(dDate-23)>5  then ViewRDayVedWD08.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD08.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD07.Caption:=ds1(dDate-24);
  if dw(dDate-24)>5  then ViewRDayVedWD07.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD07.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD06.Caption:=ds1(dDate-25);
  if dw(dDate-25)>5  then ViewRDayVedWD06.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD06.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD05.Caption:=ds1(dDate-26);
  if dw(dDate-26)>5  then ViewRDayVedWD05.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD05.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD04.Caption:=ds1(dDate-27);
  if dw(dDate-27)>5  then ViewRDayVedWD04.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD04.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD03.Caption:=ds1(dDate-28);
  if dw(dDate-28)>5  then ViewRDayVedWD03.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD03.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD02.Caption:=ds1(dDate-29);
  if dw(dDate-29)>5  then ViewRDayVedWD02.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD02.Styles.Content:=dmMC.cxStyle1;
  ViewRDayVedWD01.Caption:=ds1(dDate-30);
  if dw(dDate-30)>5  then ViewRDayVedWD01.Styles.Content:=dmMC.cxStyle25  else ViewRDayVedWD01.Styles.Content:=dmMC.cxStyle1;

end;



procedure TfmRDayVedWide.FormCreate(Sender: TObject);
begin
  GridRDayVedW.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmRDayVedWide.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  teRDayVedW.Active:=False;
end;

procedure TfmRDayVedWide.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRDayVedWide.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRDayVedWide.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewRDayVedW);
end;

procedure TfmRDayVedWide.SpeedItem4Click(Sender: TObject);
begin
//������
end;

procedure TfmRDayVedWide.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepRVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmRDayVedWide.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewRDayVedW.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teRDayVedWIdCode1.AsInteger;
      prFillMove(teRDayVedWIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teRDayVedWNameC.AsString+'('+teRDayVedWIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;


procedure TfmRDayVedWide.ViewRDayVedWSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewRDayVedW.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teRDayVedWIdCode1.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

end.
