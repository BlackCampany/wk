unit PreAlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxRadioGroup,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxButtons, ExtCtrls, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, DB, pvtables, sqldataset;

type
  TfmPreAlg = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    Label3: TLabel;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    cxRadioButton4: TcxRadioButton;
    Label4: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    quDeps: TPvQuery;
    quDepsID: TSmallintField;
    quDepsName: TStringField;
    dsquDeps: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPreAlg: TfmPreAlg;

implementation

uses MDB;

{$R *.dfm}

procedure TfmPreAlg.FormCreate(Sender: TObject);
begin
  fmPreAlg.cxDateEdit1.Date:=Date;
  fmPreAlg.cxDateEdit2.Date:=Date;
end;

end.
