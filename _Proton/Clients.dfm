object fmClients: TfmClients
  Left = 360
  Top = 368
  Width = 829
  Height = 661
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 604
    Width = 813
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 449
    Width = 813
    Height = 155
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 161
      Height = 13
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
    end
    object cxButton2: TcxButton
      Left = 104
      Top = 102
      Width = 69
      Height = 29
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      TabStop = False
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxTextEdit1: TcxTextEdit
      Left = 16
      Top = 32
      Properties.OnChange = cxTextEdit1PropertiesChange
      TabOrder = 1
      Text = 'cxTextEdit1'
      OnKeyDown = cxTextEdit1KeyDown
      OnKeyPress = cxTextEdit1KeyPress
      Width = 161
    end
    object cxButton3: TcxButton
      Left = 17
      Top = 102
      Width = 72
      Height = 29
      Caption = 'OK'
      TabOrder = 2
      TabStop = False
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 60
      Width = 161
      Height = 25
      Caption = #1053#1072#1081#1090#1080' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1102
      TabOrder = 3
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object GridFindCli: TcxGrid
      Left = 220
      Top = 2
      Width = 591
      Height = 151
      Align = alRight
      TabOrder = 4
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object ViewFindCli: TcxGridDBTableView
        OnDblClick = ViewFindCliDblClick
        OnKeyPress = ViewFindCliKeyPress
        NavigatorButtons.ConfirmDelete = False
        OnCustomDrawCell = ViewFindCliCustomDrawCell
        DataController.DataSource = dmMC.dsquFindCli
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object ViewFindCliVendor: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'Vendor'
          Width = 53
        end
        object ViewFindCliName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1082#1088#1072#1090#1082#1086#1077')'
          DataBinding.FieldName = 'Name'
          Width = 178
        end
        object ViewFindCliINN: TcxGridDBColumn
          Caption = #1048#1053#1053
          DataBinding.FieldName = 'INN'
          Width = 115
        end
        object ViewFindCliFullName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1087#1086#1083#1085#1086#1077')'
          DataBinding.FieldName = 'FullName'
          Width = 166
        end
        object ViewFindCliPhone1: TcxGridDBColumn
          Caption = #1090#1077#1083#1077#1092#1086#1085
          DataBinding.FieldName = 'Phone1'
        end
        object ViewFindCliOKPO: TcxGridDBColumn
          Caption = #1050#1055#1055
          DataBinding.FieldName = 'OKPO'
        end
        object ViewFindCliPhone2: TcxGridDBColumn
          Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
          DataBinding.FieldName = 'Phone2'
        end
        object ViewFindCliFax: TcxGridDBColumn
          Caption = 'E-Mail'
          DataBinding.FieldName = 'Fax'
        end
        object ViewFindCliCodeUchet: TcxGridDBColumn
          Caption = 'GLN ('#1043#1051#1053')'
          DataBinding.FieldName = 'CodeUchet'
        end
        object ViewFindCliStatus: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1062#1041
          DataBinding.FieldName = 'Status'
        end
        object ViewFindCliStatusByte: TcxGridDBColumn
          Caption = #1058#1080#1087
          DataBinding.FieldName = 'StatusByte'
        end
        object ViewFindCliRaion: TcxGridDBColumn
          Caption = #1047#1072#1082#1072#1079
          DataBinding.FieldName = 'Raion'
        end
      end
      object LevelFindCli: TcxGridLevel
        Caption = #1055#1086#1080#1089#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        GridView = ViewFindCli
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 145
    Height = 449
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 12
      Top = 20
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100'    Ctrl+Ins'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clGreen
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 12
      Top = 48
      Cursor = crHandPoint
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100'           F4'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 12
      Top = 176
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100'      Ctrl+Del'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 12
      Top = 72
      Cursor = crHandPoint
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088'           F3'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel4Click
    end
    object cxLabel5: TcxLabel
      Left = 12
      Top = 100
      Cursor = crHandPoint
      Caption = #1044#1086#1087'. '#1087#1072#1088#1072#1084#1077#1090#1088#1099
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.TextColor = clBlue
      StyleHot.BorderStyle = ebsNone
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.TextColor = clBlue
      Properties.Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC6BEBDA5B69C84AE8484AA8494A694B5AEA5FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5C3AD5AAE5A29AE2921
        BA3129C33931BE4A52B26394A694FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB5C3B5429E3110A20821B2218CCB9494D39C31CB5229CB4A42BA5294A6
        94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6C7C663A25A109200189E0829AE29AD
        C7ADB5CFBD39CB5229C74A21C73952AE5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        BDC3B5318E18219208299E1839AE31A5BAA5ADC3B542C75231C74A29C73929BA
        3194A68CFFFFFFFFFFFFFFFFFFFFFFFFBDC3B55AA2429CC794C6D7C6BDCBB5BD
        BEBDB5BAB5ADBEADB5C7B58CCB9421BA217BAA7BFFFFFFFFFFFFFFFFFFFFFFFF
        C6C7BD9CC38CD6E3CEDEE7DEC6D7C6CECFCEBDC3BDA5BAA5A5BEA584C38418B6
        1884AE7BFFFFFFFFFFFFFFFFFFFFFFFFC6C7C6ADCBA5A5C7948CBE7B73B663D6
        DFCEC6D7C64AAE3939AE2931B22931AE29A5B69CFFFFFFFFFFFFFFFFFFFFFFFF
        D6CBCEBDCBBDADCFA59CC7949CC78CEFF3EFE7EFE773BE635AB64A4AB23973B6
        6BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C7C6BDCFBDB5CFA5ADCB9CDE
        EBD6D6E7CE84C3736BBA5A73B66BBDC7B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC6C7C6C6CBBDB5CFADADCFA59CC79494BE849CBE94C6CBBDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6CFCEC6C7C6C6
        CBC6C6CBBDC6CBC6D6CFCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Properties.Orientation = cxoLeftTop
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 12
      Top = 136
      Cursor = crHandPoint
      Caption = #1043#1088#1072#1092#1080#1082'  '#1087#1086#1089#1090'.   Alt+G'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxButton4: TcxButton
      Left = 12
      Top = 224
      Width = 105
      Height = 41
      TabOrder = 6
      TabStop = False
      OnClick = cxButton4Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridCli: TcxGrid
    Left = 152
    Top = 8
    Width = 657
    Height = 437
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    OnFocusedViewChanged = GridCliFocusedViewChanged
    object ViewCli: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewCliDblClick
      OnKeyPress = ViewCliKeyPress
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewCliCustomDrawCell
      DataController.DataSource = dmMC.dsquCli
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewCliVendor: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Vendor'
        Width = 51
      end
      object ViewCliName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1082#1088#1072#1090#1082#1086#1077')'
        DataBinding.FieldName = 'Name'
        Styles.Content = dmMC.cxStyle5
        Width = 156
      end
      object ViewCliINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'INN'
      end
      object ViewCliFullName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1087#1086#1083#1085#1086#1077')'
        DataBinding.FieldName = 'FullName'
        Width = 185
      end
      object ViewCliPhone1: TcxGridDBColumn
        Caption = #1090#1077#1083#1077#1092#1086#1085
        DataBinding.FieldName = 'Phone1'
      end
      object ViewCliOKPO: TcxGridDBColumn
        Caption = #1050#1055#1055
        DataBinding.FieldName = 'OKPO'
      end
      object ViewCliPhone2: TcxGridDBColumn
        Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
        DataBinding.FieldName = 'Phone2'
      end
      object ViewCliFax: TcxGridDBColumn
        Caption = 'E-Mail'
        DataBinding.FieldName = 'Fax'
      end
      object ViewCliCodeUchet: TcxGridDBColumn
        Caption = 'GLN ('#1043#1051#1053')'
        DataBinding.FieldName = 'CodeUchet'
      end
      object ViewCliStatus: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1062#1041
        DataBinding.FieldName = 'Status'
      end
      object ViewCliStatusByte: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'StatusByte'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1070#1088'.'#1083#1080#1094#1086
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1048#1055
            Value = 1
          end>
      end
      object ViewCliRaion: TcxGridDBColumn
        Caption = #1047#1072#1082#1072#1079
        DataBinding.FieldName = 'Raion'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1053#1077#1090
            ImageIndex = 0
            Value = 0
          end
          item
            Description = 'EDI'
            Value = 1
          end
          item
            Description = 'E-Mail'
            Value = 2
          end
          item
            Description = 'EDI-Test'
            Value = 3
          end
          item
            Description = #1048#1085#1086#1081
            Value = 4
          end
          item
            Description = 'EDI '#1087#1088#1080#1085#1080#1084#1072#1090#1100' '#1085#1072#1082#1083'.'
            Value = 5
          end
          item
            Description = #1057#1074#1086#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
            Value = 6
          end>
      end
      object ViewCliPhone4: TcxGridDBColumn
        Caption = #8470' '#1083#1080#1094#1077#1085#1079#1080#1080
        DataBinding.FieldName = 'Phone4'
      end
      object ViewCliMailVoz: TcxGridDBColumn
        Caption = 'E-Mail '#1074#1086#1079#1074
        DataBinding.FieldName = 'EMAILVOZ'
        Width = 151
      end
      object ViewCliMolVoz: TcxGridDBColumn
        Caption = #1054#1090#1074'.'#1083#1080#1094#1086' '#1087#1086' '#1074#1086#1079#1074#1088'.'
        DataBinding.FieldName = 'MOLVOZ'
        Width = 176
      end
      object ViewCliPhoneVoz: TcxGridDBColumn
        Caption = #1058#1077#1083'. '#1087#1086' '#1074#1086#1079#1074#1088#1072#1090#1072#1084
        DataBinding.FieldName = 'PHONEVOZ'
        Width = 134
      end
      object ViewCliTypeV: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088'.'
        DataBinding.FieldName = 'TYPEVOZ'
        Width = 158
      end
      object ViewCliBlockV: TcxGridDBColumn
        Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085' '#1087#1086' '#1074#1086#1079#1074#1088'.'
        DataBinding.FieldName = 'STBLOCK'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        Width = 144
      end
    end
    object ViewIP: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewIPDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquIP
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewIPCode: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Code'
        Width = 44
      end
      object ViewIPName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1082#1088#1072#1090#1082#1086#1077')'
        DataBinding.FieldName = 'Name'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewIPINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'INN'
      end
      object ViewIPFullName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' ('#1087#1086#1083#1085#1086#1077')'
        DataBinding.FieldName = 'FullName'
        Width = 204
      end
      object ViewIPCountry: TcxGridDBColumn
        Caption = #1057#1090#1088#1072#1085#1072
        DataBinding.FieldName = 'Country'
        Width = 116
      end
      object ViewIPPhone4: TcxGridDBColumn
        Caption = #8470' '#1083#1080#1094#1077#1085#1079#1080#1080
        DataBinding.FieldName = 'Phone4'
      end
    end
    object LevelCli: TcxGridLevel
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      GridView = ViewCli
    end
    object LevelIP: TcxGridLevel
      Caption = #1055#1088#1077#1076#1087#1088#1080#1085#1080#1084#1072#1090#1077#1083#1080
      GridView = ViewIP
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 448
    Top = 172
  end
  object amCli: TActionManager
    Left = 340
    Top = 144
    StyleName = 'XP Style'
    object acAddCli: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 16429
      OnExecute = acAddCliExecute
    end
    object acEditCli: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ShortCut = 115
      OnExecute = acEditCliExecute
    end
    object acDelCli: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 16430
      OnExecute = acDelCliExecute
    end
    object acViewCli: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ShortCut = 114
      OnExecute = acViewCliExecute
    end
    object acFindCli: TAction
      Caption = #1055#1086#1080#1089#1082
      OnExecute = acFindCliExecute
    end
    object acUpCli: TAction
      Caption = 'acUpCli'
      ShortCut = 38
    end
    object acDownCli: TAction
      Caption = 'acDownCli'
      ShortCut = 40
    end
    object acRecalcAss: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      OnExecute = acRecalcAssExecute
    end
    object acGrafPost: TAction
      Caption = #1043#1088#1072#1092#1080#1082' '#1087#1086#1089#1090#1072#1074#1086#1082
      ShortCut = 32839
      OnExecute = acGrafPostExecute
    end
    object acEditLic: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1083#1080#1094#1077#1085#1079#1080#1081
      OnExecute = acEditLicExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 536
    Top = 144
    object N3: TMenuItem
      Action = acGrafPost
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = acRecalcAss
    end
    object N4: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnClick = N4Click
    end
  end
end
