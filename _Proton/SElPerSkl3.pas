unit SElPerSkl3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox,
  dxfProgressBar, cxRadioGroup;

type
  TfmSelPerSkl3 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    Label2: TLabel;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl3: TfmSelPerSkl3;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmSelPerSkl3.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl3.cxButton1Click(Sender: TObject);
begin
  Cursor:=crHourGlass; Delay(10);
end;

end.
