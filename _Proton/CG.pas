unit CG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox;

type
  TfmCG = class(TForm)
    FormPlacement1: TFormPlacement;
    ViewCG: TcxGridDBTableView;
    LevelCG: TcxGridLevel;
    GridCG: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ViewCGCARD: TcxGridDBColumn;
    ViewCGITEM: TcxGridDBColumn;
    ViewCGDEPART: TcxGridDBColumn;
    ViewCGName: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCG: TfmCG;

implementation

uses Un1, MDB, Period1;

{$R *.dfm}

procedure TfmCG.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridCG.Align:=AlClient;
end;

procedure TfmCG.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmCG.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCG.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(ViewCG);
end;

procedure TfmCG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmMC.quCG2.Active:=False;
  fmCG.Release;
end;

end.
