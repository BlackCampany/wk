object fmRes: TfmRes
  Left = 403
  Top = 427
  Width = 648
  Height = 522
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 53
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 464
      Top = 24
      Width = 6
      Height = 13
      Caption = '0'
    end
    object cxButton1: TcxButton
      Left = 12
      Top = 4
      Width = 61
      Height = 45
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 124
      Top = 4
      Width = 73
      Height = 45
      Caption = #1060#1080#1082#1089'.'#1094#1077#1085#1099
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 200
      Top = 4
      Width = 73
      Height = 45
      Caption = #1053#1072#1094#1077#1085#1082#1072
      TabOrder = 2
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton4: TcxButton
      Left = 276
      Top = 4
      Width = 73
      Height = 45
      Caption = #1057#1090#1072#1090#1091#1089
      TabOrder = 3
      OnClick = cxButton4Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton5: TcxButton
      Left = 352
      Top = 4
      Width = 73
      Height = 45
      Caption = 'DiscStop'
      TabOrder = 4
      OnClick = cxButton5Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridTestA: TcxGrid
    Left = 12
    Top = 64
    Width = 613
    Height = 309
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewTestA1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteFP
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object ViewTestA1RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewTestA1iCode: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'iCode'
      end
      object ViewTestA1iName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'iName'
      end
      object ViewTestA1FPA: TcxGridDBColumn
        Caption = #1060#1080#1082#1089'.'#1094#1077#1085#1072' '#1074' '#1040#1082#1094#1080#1080
        DataBinding.FieldName = 'FPA'
      end
      object ViewTestA1FPC: TcxGridDBColumn
        Caption = #1060#1080#1082#1089'. '#1094#1077#1085#1072' '#1074' '#1082#1072#1088#1090#1086#1095#1082#1077
        DataBinding.FieldName = 'FPC'
        Width = 68
      end
      object ViewTestA1FRC: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1074' '#1074' '#1082#1072#1088#1090#1086#1095#1082#1077
        DataBinding.FieldName = 'FRC'
      end
      object ViewTestA1AC: TcxGridDBColumn
        Caption = #1045#1089#1090#1100' '#1074' '#1040#1082#1094#1080#1080
        DataBinding.FieldName = 'AC'
      end
      object ViewTestA1Remn: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Remn'
      end
    end
    object ViewTestA2: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object ViewTestA3: TcxGridDBTableView
      PopupMenu = PopupMenu3
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteST
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object ViewTestA3RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewTestA3iCode: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'iCode'
      end
      object ViewTestA3AC: TcxGridDBColumn
        Caption = #1042' '#1040#1082#1094#1080#1080
        DataBinding.FieldName = 'AC'
      end
      object ViewTestA3STA: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1040#1082#1094#1080#1080
        DataBinding.FieldName = 'STA'
      end
      object ViewTestA3CTC: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1082#1072#1088#1090#1086#1095#1082#1080
        DataBinding.FieldName = 'CTC'
      end
    end
    object ViewTestA4: TcxGridDBTableView
      PopupMenu = PopupMenu2
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteDP
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object ViewTestA4RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewTestA4iCode: TcxGridDBColumn
        DataBinding.FieldName = 'iCode'
      end
      object ViewTestA4iName: TcxGridDBColumn
        DataBinding.FieldName = 'iName'
      end
      object ViewTestA4AC: TcxGridDBColumn
        DataBinding.FieldName = 'AC'
      end
      object ViewTestA4DSA: TcxGridDBColumn
        DataBinding.FieldName = 'DSA'
      end
      object ViewTestA4DSC: TcxGridDBColumn
        DataBinding.FieldName = 'DSC'
      end
    end
    object LevelTestA1: TcxGridLevel
      Caption = #1060#1062
      GridView = ViewTestA1
    end
    object LevelTestA2: TcxGridLevel
      Caption = #1053#1040#1062
      GridView = ViewTestA2
    end
    object LevelTestA3: TcxGridLevel
      Caption = #1057#1058
      GridView = ViewTestA3
    end
    object LevelTestA4: TcxGridLevel
      Caption = #1057#1051
      GridView = ViewTestA4
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 399
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    Height = 89
    Width = 640
  end
  object teFP: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 120
    Top = 148
    object teFPiCode: TIntegerField
      FieldName = 'iCode'
    end
    object teFPiName: TStringField
      FieldName = 'iName'
      Size = 30
    end
    object teFPFPA: TFloatField
      FieldName = 'FPA'
      DisplayFormat = '0.00'
    end
    object teFPFRC: TFloatField
      FieldName = 'FRC'
      DisplayFormat = '0.00'
    end
    object teFPAC: TSmallintField
      FieldName = 'AC'
    end
    object teFPFPC: TFloatField
      FieldName = 'FPC'
      DisplayFormat = '0.00'
    end
    object teFPRemn: TFloatField
      FieldName = 'Remn'
    end
  end
  object dsteFP: TDataSource
    DataSet = teFP
    Left = 120
    Top = 204
  end
  object PopupMenu1: TPopupMenu
    Left = 116
    Top = 268
    object N1: TMenuItem
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1092#1080#1082#1089'.'#1094#1077#1085#1099' '#1091' '#1090#1086#1074#1072#1088#1072' '#1074#1085#1077' '#1072#1082#1094#1080#1080' '#1080' '#1073#1077#1079' '#1086#1089#1090#1072#1090#1082#1072
      OnClick = N1Click
    end
    object N4: TMenuItem
      Caption = #1051#1077#1074#1099#1077' '#1092#1080#1082#1089#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1094#1077#1085#1099' '#1074' '#1087#1088#1080#1093#1086#1076#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
      OnClick = N4Click
    end
    object N3: TMenuItem
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1092#1080#1082#1089'.'#1094#1077#1085#1099' '#1074#1085#1077' '#1072#1082#1094#1080#1080' '#1089' '#1086#1089#1090#1072#1090#1082#1086#1084
      OnClick = N3Click
    end
  end
  object teDP: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 196
    Top = 152
    object teDPDSA: TIntegerField
      FieldName = 'DSA'
    end
    object teDPDSC: TIntegerField
      FieldName = 'DSC'
    end
    object teDPiCode: TIntegerField
      FieldName = 'iCode'
    end
    object teDPAC: TSmallintField
      FieldName = 'AC'
    end
    object teDPName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsteDP: TDataSource
    DataSet = teDP
    Left = 196
    Top = 204
  end
  object PopupMenu2: TPopupMenu
    Left = 348
    Top = 160
    object N2: TMenuItem
      Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1080#1088#1086#1074#1072#1090#1100' '#1089#1090#1086#1087#1089#1082#1080#1076#1082#1080' '#1089' '#1072#1082#1094#1080#1103#1084#1080
      OnClick = N2Click
    end
  end
  object teST: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 264
    Top = 156
    object teSTiCode: TIntegerField
      FieldName = 'iCode'
    end
    object teSTAC: TSmallintField
      FieldName = 'AC'
    end
    object teSTSTA: TIntegerField
      FieldName = 'STA'
    end
    object teSTCTC: TIntegerField
      FieldName = 'CTC'
    end
  end
  object dsteST: TDataSource
    DataSet = teST
    Left = 264
    Top = 208
  end
  object PopupMenu3: TPopupMenu
    Left = 312
    Top = 288
    object N001: TMenuItem
      Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1089#1090#1072#1090#1091#1089' '#1074#1089#1077#1093' '#1082#1072#1088#1090#1086#1095#1077#1082' '#1074' 00 '#1080' '#1091#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1084#1072#1103#1082#1080' '#1087#1086' '#1040#1082#1094#1080#1103#1084
      OnClick = N001Click
    end
  end
end
