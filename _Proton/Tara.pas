unit Tara;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls;

type
  TfmTara = class(TForm)
    StatusBar1: TStatusBar;
    ViTara: TcxGridDBTableView;
    LeTara: TcxGridLevel;
    GrTara: TcxGrid;
    ViTaraID: TcxGridDBColumn;
    ViTaraNAME: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ViTaraDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTara: TfmTara;

implementation

uses DMOReps, AddDoc4, dmOffice;

{$R *.dfm}

procedure TfmTara.FormCreate(Sender: TObject);
begin
  GrTara.Align:=AlClient;
end;

procedure TfmTara.ViTaraDblClick(Sender: TObject);
Var iMax:INteger;
    PriceIn,Km:Real;
begin
  //������� �������
  with dmORep do
  begin
    if quTara.RecordCount=0 then exit;

    if fmAddDoc4.Visible then
    begin
      if fmAddDoc4.cxButton1.Enabled then
      begin
        with fmAddDoc4 do
        begin
          iMax:=1;
//          iCard:=quTaraID.AsInteger;

          ViewDoc4.BeginUpdate;

          taSpec.First;
          if not fmAddDoc4.taSpec.Eof then
          begin
            taSpec.Last;
            iMax:=taSpecNum.AsInteger+1;
          end;

//          prCalcLastPricePos(iCard,fmAddDoc4.cxLookupComboBox1.EditValue,Trunc(fmAddDoc4.cxDateEdit1.Date),PriceIn,PriceUch,PriceIn0);

          PriceIn:=quTaraLASTPRICEOUT.AsFloat;

          taSpec.Append;
          taSpecNum.AsInteger:=iMax;
          taSpecIdGoods.AsInteger:=quTaraID.AsInteger;
          taSpecNameG.AsString:=quTaraNAME.AsString;
          taSpecIM.AsInteger:=quTaraIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quTaraIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=1;
          taSpecPriceIn.AsFloat:=PriceIn;
          taSpecSumIn.AsFloat:=PriceIn;
          taSpecPriceR.AsFloat:=PriceIn;
          taSpecSumR.AsFloat:=PriceIn;
          taSpecINds.AsInteger:=1;
          taSpecSNds.AsString:='0';
          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecTCard.AsInteger:=0;
          taSpecOper.AsInteger:=0;
          taSpecCType.AsInteger:=quTaraCATEGORY.AsInteger;
          taSpecPriceIn0.AsFloat:=PriceIn;
          taSpecSumIn0.AsFloat:=PriceIn;
          taSpecSumOut0.AsFloat:=PriceIn;
          taSpecRNdsOut.AsFloat:=0;
          taSpecBZQUANTR.AsFloat:=0;
          taSpecBZQUANTF.AsFloat:=0;
          taSpecBZQUANTS.AsFloat:=1;
          taSpec.Post;

          ViewDoc4.EndUpdate;

          fmTara.close;
        end;
      end;
    end;
  end;
end;

end.
