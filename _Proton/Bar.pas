unit Bar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls,
  SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxContainer, cxTextEdit, cxDataStorage,
  cxImageComboBox;

type
  TfmBar = class(TForm)
    FormPlacementBar: TFormPlacement;
    StatusBar1: TStatusBar;
    amBar: TActionManager;
    acExit: TAction;
    SpeedBar1: TSpeedBar;
    acAddBar: TAction;
    acDelBar: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    BarView: TcxGridDBTableView;
    BarLevel: TcxGridLevel;
    BarGr: TcxGrid;
    BarViewBar: TcxGridDBColumn;
    BarViewGoodsId: TcxGridDBColumn;
    BarViewQuant: TcxGridDBColumn;
    BarViewBarFormat: TcxGridDBColumn;
    BarViewBarStatus: TcxGridDBColumn;
    BarViewCost: TcxGridDBColumn;
    BarViewStatus: TcxGridDBColumn;
    Panel1: TPanel;
    acAddBarCode: TAction;
    TextEdit1: TcxTextEdit;
    acCancBar: TAction;
    acMakeMain: TAction;
    SpeedItem4: TSpeedItem;
    Timer2: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddBarExecute(Sender: TObject);
    procedure acDelBarExecute(Sender: TObject);
    procedure TextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acAddBarCodeExecute(Sender: TObject);
    procedure acCancBarExecute(Sender: TObject);
    procedure acMakeMainExecute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBar: TfmBar;

implementation

uses Un1, MDB, mCards;

{$R *.dfm}

procedure TfmBar.FormCreate(Sender: TObject);
begin
  FormPlacementBar.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacementBar.Active:=True;
end;

procedure TfmBar.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmBar.acAddBarExecute(Sender: TObject);
begin
  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  TextEdit1.Visible:=True;
  TextEdit1.SetFocus;
end;

procedure TfmBar.acDelBarExecute(Sender: TObject);
Var iNum:Integer;
    sBar:String;
begin
//�������
  if not CanDo('prDelBar') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quBars.Eof then
    begin
      showmessage('������� ������.');
      exit;
    end;
    if MessageDlg('�� ������������� ������ ������� �������� "'+quBarsID.AsString+'" �� ����.',
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iNum:=quBarsGoodsId.AsInteger;
      sBar:=quBarsID.AsString;

      if quCardsBarCode.AsString<>sBar then
      begin
        BarView.BeginUpdate;

        quD.SQL.Clear;
        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from "BarCode"' );
        quD.SQL.Add('where  ID='''+sBar+'''');
        quD.ExecSQL;


        quBars.Active:=False;
        quBars.ParamByName('GOODSID').Value:=iNum;
        quBars.Active:=True;

        BarView.EndUpdate;
      end else
      begin
        showmessage('������ �� �������� �������� � �������� - �������� ����������.');
      end;
    end;
  end;
end;

procedure TfmBar.TextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var bCh:Byte;
    iC:INteger;
begin

  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  bCh:=ord(Key);

  if bCh = 13 then
  begin
    with dmMC do
    begin
      if TextEdit1.Text='' then
      begin
        delay(10);
        TextEdit1.Text:='';
        TextEdit1.Visible:=False;
        exit;
      end;

      if not prFindBar(TextEdit1.Text,-1,iC) then  //�.�. ��� ��� ���
      begin
        //���������
        bAddB:=True;

        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "BarCode" values (');
        quA.SQL.Add(its(quCardsId.AsInteger)+',');//GoodsID
        quA.SQL.Add(''''+TextEdit1.Text+''''+',');//ID
        quA.SQL.Add('1,');//Quant
        if quCardsTovarType.AsInteger=0 then   quA.SQL.Add('0,') //BarFormat
        else quA.SQL.Add('2,'); //BarFormat
        quA.SQL.Add(''''+ds(date)+''''+',');//DateChange
        quA.SQL.Add('0,');//BarStatus
        quA.SQL.Add('0,');//Cost
        quA.SQL.Add('0');//Status
        quA.SQL.Add(')');
        quA.ExecSQL;

{
        taBar.Append;
        taBarBar.AsString:=TextEdit1.Text;
        taBarGoodsId.AsInteger:=quCardSelId.AsInteger;
        taBarQuant.AsFloat:=1;
        if quCardSelTovarType.AsInteger=0 then taBarBarFormat.AsInteger:=0 //�������
        else taBarBarFormat.AsInteger:=2; //�������
        taBarBarStatus.AsInteger:=0;
        taBarCost.AsFloat:=0;
        taBarStatus.AsInteger:=0;
        taBar.Post;     }

        quBars.Active:=False;
        quBars.ParamByName('GOODSID').Value:=quCardsId.AsInteger;
        quBars.Active:=True;

        quBars.Locate('ID',TextEdit1.Text,[]);

      end
      else
      begin
        showmessage('�������� "'+TextEdit1.Text+'" ��� ���� � ��c���� ('+INtToStr(iC)+').');
      end;

      delay(10);
      TextEdit1.Text:='';
      TextEdit1.Visible:=False;
      //}
    end;
  end;
end;

procedure TfmBar.acAddBarCodeExecute(Sender: TObject);
begin
  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  TextEdit1.Visible:=True;
  TextEdit1.SetFocus;
end;

procedure TfmBar.acCancBarExecute(Sender: TObject);
begin
  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  TextEdit1.Text:='';
  TextEdit1.Visible:=False;
end;

procedure TfmBar.acMakeMainExecute(Sender: TObject);
Var iC:Integer;
begin
//������� ��������
  if CommonSet.Single=0 then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if not CanDo('prSetMainBar') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quBars.RecordCount=0  then exit;

    if MessageDlg('�� ������������� ������ ������� �� '+quBarsID.AsString+' ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update Goods Set ');
      quE.SQL.Add('BarCode='''+quBarsID.AsString+'''');
      quE.SQL.Add('where ID='+its(quCardsID.AsInteger));
      quE.ExecSQL;

      iC:=quCardsID.AsInteger;
      try
        fmCards.CardsView.BeginUpdate;
        prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),fmCards.cxCheckBox1.Checked);
        quCards.Locate('ID',iC,[]);
      finally
        fmCards.CardsView.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmBar.Timer2Timer(Sender: TObject);
begin
  if bClearStatusBar=True then begin StatusBar1.Panels[0].Text:=''; Delay(10); end;
  if StatusBar1.Panels[0].Text>'' then bClearStatusBar:=True;
end;

end.
