object fmAddMaker: TfmAddMaker
  Left = 387
  Top = 439
  BorderStyle = bsDialog
  Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1080
  ClientHeight = 117
  ClientWidth = 644
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 16
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel
    Left = 12
    Top = 48
    Width = 24
    Height = 13
    Caption = #1048#1053#1053
  end
  object Label3: TLabel
    Left = 12
    Top = 80
    Width = 23
    Height = 13
    Caption = #1050#1055#1055
  end
  object Panel1: TPanel
    Left = 526
    Top = 0
    Width = 118
    Height = 117
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 104
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 405
  end
  object cxTextEdit2: TcxTextEdit
    Left = 104
    Top = 44
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 169
  end
  object cxTextEdit3: TcxTextEdit
    Left = 104
    Top = 76
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 165
  end
end
