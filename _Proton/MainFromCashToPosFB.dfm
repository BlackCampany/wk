object fmMainCashOut: TfmMainCashOut
  Left = 457
  Top = 111
  Width = 660
  Height = 313
  Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1095#1077#1082#1086#1074' '#1089' '#1082#1072#1089#1089#1099'.'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 103
    Height = 13
    Caption = #1055#1091#1090#1100' '#1076#1086' '#1073#1072#1079#1099' '#1082#1072#1089#1089#1099
  end
  object Label2: TLabel
    Left = 24
    Top = 100
    Width = 61
    Height = 13
    Caption = #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1089
  end
  object Label3: TLabel
    Left = 236
    Top = 100
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Label4: TLabel
    Left = 24
    Top = 44
    Width = 109
    Height = 13
    Caption = #1055#1091#1090#1100' '#1076#1086' '#1073#1072#1079#1099' '#1086#1073#1084#1077#1085#1072
  end
  object Label5: TLabel
    Left = 28
    Top = 72
    Width = 46
    Height = 13
    Caption = #8470' '#1082#1072#1089#1089#1099
  end
  object cxTextEdit1: TcxTextEdit
    Left = 148
    Top = 12
    TabOrder = 0
    Text = 'cxTextEdit1'
    Width = 393
  end
  object cxDateEdit1: TcxDateEdit
    Left = 100
    Top = 96
    TabOrder = 1
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 260
    Top = 96
    TabOrder = 2
    Width = 121
  end
  object cxButton1: TcxButton
    Left = 572
    Top = 24
    Width = 75
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1041#1044
    TabOrder = 3
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 572
    Top = 56
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100' '#1095#1077#1082#1080
    TabOrder = 4
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 572
    Top = 88
    Width = 75
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 5
    Visible = False
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 572
    Top = 120
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100' '#1041#1044
    TabOrder = 6
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object Memo1: TcxMemo
    Left = 4
    Top = 124
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Height = 153
    Width = 537
  end
  object cxTextEdit2: TcxTextEdit
    Left = 148
    Top = 40
    TabOrder = 8
    Text = 'cxTextEdit2'
    Width = 393
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 100
    Top = 68
    TabOrder = 9
    Value = 1
    Width = 121
  end
  object CasherDb: TpFIBDatabase
    DBName = '192.168.11.101:E:\_Casher\DB\CASHERDB.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherFb'
    WaitForRestoreConnect = 20
    Left = 64
    Top = 136
  end
  object Cash: TpFIBDatabase
    DBName = '192.168.0.44:C:\PosFb\cash.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelectCash
    DefaultUpdateTransaction = trUpdateCash
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'Cash'
    WaitForRestoreConnect = 20
    Left = 308
    Top = 136
  end
  object trSelectCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 308
    Top = 184
  end
  object trUpdateCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 308
    Top = 232
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = CasherDb
    TimeoutAction = TARollback
    Left = 64
    Top = 196
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = CasherDb
    TimeoutAction = TARollback
    Left = 116
    Top = 196
  end
  object quSumTr: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT Operation,Sum(TOTALRUB-DSUM) as SumOp  FROM CASHSAIL'
      'WHERE'
      'SHOPINDEX=1'
      'and CASHNUMBER=2'
      'and ZNUMBER=2'
      'Group by Operation'
      'Order by Operation')
    Transaction = trSelect
    Database = CasherDb
    UpdateTransaction = trUpdateCash
    Left = 178
    Top = 136
    object quSumTrOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
    object quSumTrSUMOP: TFIBFloatField
      FieldName = 'SUMOP'
    end
  end
  object prDelCashZ: TpFIBStoredProc
    Transaction = trUpdateCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE DELCASHSZDAY (?CASHNUMBER, ?ZNUM, ?ZDATEB, ?ZD' +
        'ATEE)')
    StoredProcName = 'DELCASHSZDAY'
    Left = 368
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quCashPay: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,CHECKSUM,' +
        'PAYSUM,DSUM,DBAR,COUNTPOS,CHDATE,PAYMENT'
      'FROM CASHPAY '
      'where DSUM<>0'
      'and CASHNUMBER=1'
      'and ZNUMBER=1'
      '')
    Transaction = trSelect
    Database = CasherDb
    UpdateTransaction = trUpdate
    Left = 172
    Top = 200
    poAskRecordCount = True
    object quCashPaySHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCashPayCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCashPayZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCashPayCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCashPayCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCashPayCHECKSUM: TFIBFloatField
      FieldName = 'CHECKSUM'
    end
    object quCashPayPAYSUM: TFIBFloatField
      FieldName = 'PAYSUM'
    end
    object quCashPayDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCashPayDBAR: TFIBStringField
      FieldName = 'DBAR'
      Size = 22
      EmptyStrToNull = True
    end
    object quCashPayCOUNTPOS: TFIBSmallIntField
      FieldName = 'COUNTPOS'
    end
    object quCashPayCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCashPayPAYMENT: TFIBSmallIntField
      FieldName = 'PAYMENT'
    end
  end
  object prAddCashP: TpFIBStoredProc
    Transaction = trSelectCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE ADDCASHP (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?' +
        'CHECKNUMBER, ?CASHER, ?CHECKSUM, ?PAYSUM, ?DSUM, ?DBAR, ?COUNTPO' +
        'S, ?CHDATE, ?PAYMENT)')
    StoredProcName = 'ADDCASHP'
    Left = 392
    Top = 188
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prAddCashS: TpFIBStoredProc
    Transaction = trSelectCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE ADDCASHS (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?' +
        'CHECKNUMBER, ?ID, ?CHDATE, ?ARTICUL, ?BAR, ?CARDSIZE, ?QUANTITY,' +
        ' ?PRICERUB, ?DPROC, ?DSUM, ?TOTALRUB, ?CASHER, ?DEPART, ?OPERATI' +
        'ON, ?SECPOS, ?ECHECK)')
    StoredProcName = 'ADDCASHS'
    Left = 472
    Top = 188
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object fbCashSail: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    cs.SHOPINDEX,'
      '    cs.CASHNUMBER,'
      '    cs.ZNUMBER,'
      '    cs.CHECKNUMBER,'
      '    cs.ID,'
      '    cs.CHDATE,'
      '    cs.ARTICUL,'
      '    cs.BAR,'
      '    cs.CARDSIZE,'
      '    cs.QUANTITY,'
      '    cs.PRICERUB,'
      '    cs.DPROC,'
      '    cs.DSUM,'
      '    cs.TOTALRUB,'
      '    cs.CASHER,'
      '    cs.DEPART,'
      '    cs.OPERATION,'
      '    cs.SECPOS,'
      '    cs.ECHECK'
      ''
      'FROM'
      '    CASHSAIL cs')
    Transaction = trSelect
    Database = CasherDb
    UpdateTransaction = trUpdate
    Left = 238
    Top = 135
    object fbCashSailSHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object fbCashSailCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object fbCashSailZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object fbCashSailCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object fbCashSailID: TFIBIntegerField
      FieldName = 'ID'
    end
    object fbCashSailCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object fbCashSailARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object fbCashSailBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object fbCashSailCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object fbCashSailQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object fbCashSailPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object fbCashSailDPROC: TFIBFloatField
      FieldName = 'DPROC'
    end
    object fbCashSailDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object fbCashSailTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
    end
    object fbCashSailCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object fbCashSailDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object fbCashSailOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
    object fbCashSailSECPOS: TFIBSmallIntField
      FieldName = 'SECPOS'
    end
    object fbCashSailECHECK: TFIBSmallIntField
      FieldName = 'ECHECK'
    end
  end
  object quZListDet2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ZLISTDET'
      'SET '
      '    RSUM = :RSUM,'
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE'
      'WHERE'
      '    CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ZLISTDET'
      'WHERE'
      '        CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ZLISTDET('
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      ')'
      'VALUES('
      '    :CASHNUM,'
      '    :ZNUM,'
      '    :INOUT,'
      '    :ITYPE,'
      '    :RSUM,'
      '    :IDATE,'
      '    :DDATE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where(  CASHNUM=:CNUM'
      'and ZNUM=:ZNUM'
      '     ) and (     ZLISTDET.CASHNUM = :OLD_CASHNUM'
      '    and ZLISTDET.ZNUM = :OLD_ZNUM'
      '    and ZLISTDET.INOUT = :OLD_INOUT'
      '    and ZLISTDET.ITYPE = :OLD_ITYPE'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where CASHNUM=:CNUM'
      'and ZNUM=:ZNUM')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    AutoCommit = True
    Left = 572
    Top = 188
    poAskRecordCount = True
    object quZListDet2CASHNUM: TFIBSmallIntField
      FieldName = 'CASHNUM'
    end
    object quZListDet2ZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object quZListDet2INOUT: TFIBSmallIntField
      FieldName = 'INOUT'
    end
    object quZListDet2ITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quZListDet2RSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quZListDet2IDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quZListDet2DDATE: TFIBDateTimeField
      FieldName = 'DDATE'
    end
  end
  object quZListDet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ZLISTDET'
      'SET '
      '    RSUM = :RSUM,'
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE'
      'WHERE'
      '    CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ZLISTDET'
      'WHERE'
      '        CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ZLISTDET('
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      ')'
      'VALUES('
      '    :CASHNUM,'
      '    :ZNUM,'
      '    :INOUT,'
      '    :ITYPE,'
      '    :RSUM,'
      '    :IDATE,'
      '    :DDATE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where(  CASHNUM=:CNUM'
      'and ZNUM=:ZNUM'
      '     ) and (     ZLISTDET.CASHNUM = :OLD_CASHNUM'
      '    and ZLISTDET.ZNUM = :OLD_ZNUM'
      '    and ZLISTDET.INOUT = :OLD_INOUT'
      '    and ZLISTDET.ITYPE = :OLD_ITYPE'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where CASHNUM=:CNUM'
      'and ZNUM=:ZNUM')
    Transaction = trSelect
    Database = CasherDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 232
    Top = 200
    poAskRecordCount = True
    object quZListDetCASHNUM: TFIBSmallIntField
      FieldName = 'CASHNUM'
    end
    object quZListDetZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object quZListDetINOUT: TFIBSmallIntField
      FieldName = 'INOUT'
    end
    object quZListDetITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quZListDetRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quZListDetIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quZListDetDDATE: TFIBDateTimeField
      FieldName = 'DDATE'
    end
  end
end
