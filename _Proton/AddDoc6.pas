unit AddDoc6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxSpinEdit,
  cxCheckBox, FR_DSet, FR_DBSet, FR_Class, dxmdaset;

type
  TfmAddDoc6 = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    amDocObm1: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    acPrintCen: TAction;
    acPostTov: TAction;
    cxButton8: TcxButton;
    acSetRemn: TAction;
    acTermoP: TAction;
    acSetNac: TAction;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxComboBox1: TcxComboBox;
    ViewDoc6: TcxGridDBTableView;
    LevelDoc6: TcxGridLevel;
    GridDoc6: TcxGrid;
    teSpecObm: TdxMemData;
    teSpecObmCode: TIntegerField;
    teSpecObmName: TStringField;
    teSpecObmiM: TSmallintField;
    teSpecObmsType: TSmallintField;
    teSpecObmQuant: TFloatField;
    teSpecObmPriceIn: TFloatField;
    teSpecObmPriceOut: TFloatField;
    teSpecObmSumIn: TFloatField;
    teSpecObmSumOut: TFloatField;
    teSpecObmQRemn: TFloatField;
    teSpecObmPriceM: TFloatField;
    teSpecObmFixPrice: TFloatField;
    teSpecObmGdsNac: TFloatField;
    teSpecObmNac: TFloatField;
    teSpecObmNum: TIntegerField;
    teSpecObmsBar: TStringField;
    dsteSpecObm: TDataSource;
    ViewDoc6Num: TcxGridDBColumn;
    ViewDoc6Code: TcxGridDBColumn;
    ViewDoc6sBar: TcxGridDBColumn;
    ViewDoc6Name: TcxGridDBColumn;
    ViewDoc6iM: TcxGridDBColumn;
    ViewDoc6sType: TcxGridDBColumn;
    ViewDoc6Quant: TcxGridDBColumn;
    ViewDoc6PriceIn: TcxGridDBColumn;
    ViewDoc6Nac: TcxGridDBColumn;
    ViewDoc6PriceOut: TcxGridDBColumn;
    ViewDoc6SumIn: TcxGridDBColumn;
    ViewDoc6SumOut: TcxGridDBColumn;
    ViewDoc6QRemn: TcxGridDBColumn;
    ViewDoc6PriceM: TcxGridDBColumn;
    ViewDoc6FixPrice: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure cxLabel2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure ViewDoc2DblClick(Sender: TObject);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acPostTovExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure ViewDoc6DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc6DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewDoc6Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure teSpecObmQuantChange(Sender: TField);
    procedure teSpecObmPriceInChange(Sender: TField);
    procedure teSpecObmSumInChange(Sender: TField);
    procedure teSpecObmPriceOutChange(Sender: TField);
    procedure teSpecObmSumOutChange(Sender: TField);
    procedure teSpecObmNacChange(Sender: TField);
    procedure ViewDoc6EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtons(bSt:Boolean);
  end;

var
  fmAddDoc6: TfmAddDoc6;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, sumprops, Move, MT, QuantMess,
  u2fdk, MainMCryst, TBuff;

{$R *.dfm}

procedure TfmAddDoc6.prButtons(bSt:Boolean);
begin
//  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;

  with dmMc do
  begin
//    if quDepartsSt.Active=False then quDepartsSt.Active:=True;

    quDepartsSt.Locate('ID',cxLookupComboBox1.EditValue,[]);

    if quDepartsStStatus3.AsInteger=1 then
    begin
    end else
    begin
    end;
  end;

end;


procedure TfmAddDoc6.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc6.Align:=alClient;
  ViewDoc6.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);

end;

procedure TfmAddDoc6.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S:String;
begin
  if fmclients.Showing then fmclients.Close;
  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  if Label4.Tag<=1 then //����������
  begin
    fmClients.LevelCli.Active:=True;
    fmClients.LevelIP.Active:=False;
  end;
  if Label4.Tag=2 then //���������������
  begin
    fmClients.LevelCli.Active:=False;
    fmClients.LevelIP.Active:=True;
  end;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      Label4.Tag:=1;
      cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
      cxButtonEdit1.Text:=dmMC.quCliName.AsString;
    end else
    begin
      Label4.Tag:=2;
      cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
      cxButtonEdit1.Text:=dmMC.quIPName.AsString;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;

  bOpen:=False;
  iDirect:=0;
end;

procedure TfmAddDoc6.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc6.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=2 then  Accept:=True;
end;

procedure TfmAddDoc6.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc6.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  bDrOut:=False;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc6.SetFocus;

//  ViewDoc6NameG.Options.Editing:=False;
//  ViewTaraNameG.Options.Editing:=False;
end;

procedure TfmAddDoc6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      bDrOut:=False;
      ViewDoc6.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    bDrOut:=False;
    ViewDoc6.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  end;
end;

procedure TfmAddDoc6.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc6.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  iMax:=1;
  ViewDoc6.BeginUpdate;

  teSpecObm.First;
  if not teSpecObm.Eof then
  begin
    teSpecObm.Last;
    iMax:=teSpecObmNum.AsInteger+1;
  end;

  iCol:=0;

  teSpecObm.Append;
  teSpecObmNum.AsInteger:=iMax;
  teSpecObmCode.AsInteger:=0;
  teSpecObmsBar.AsString:='';
  teSpecObmName.AsString:='';
  teSpecObmiM.AsInteger:=0;
  teSpecObmsType.AsInteger:=0;
  teSpecObmQuant.AsFloat:=0;
  teSpecObmPriceIn.AsFloat:=0;
  teSpecObmNac.AsFloat:=0;
  teSpecObmPriceOut.AsFloat:=0;
  teSpecObmSumIn.AsFloat:=0;
  teSpecObmSumOut.AsFloat:=0;
  teSpecObmQRemn.AsFloat:=0;
  teSpecObmPriceM.AsFloat:=0;
  teSpecObmFixPrice.AsFloat:=0;
  teSpecObmGdsNac.AsFloat:=0;
  teSpecObm.Post;

  ViewDoc6.EndUpdate;
  GridDoc6.SetFocus;

  ViewDoc6.Controller.FocusRecord(ViewDoc6.DataController.FocusedRowIndex,True);
  ViewDoc6Code.Options.Editing:=True;
  ViewDoc6Name.Options.Editing:=True;
  ViewDoc6Name.Focused:=True;
end;

procedure TfmAddDoc6.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if teSpecObm.RecordCount>0 then
  begin
    teSpecObm.Delete;
  end;
end;

procedure TfmAddDoc6.acAddListExecute(Sender: TObject);
begin
  iDirect:=20;
  fmCards.Show;
end;

procedure TfmAddDoc6.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rInSumIn,rInSumOut:Real;
    rOutSumIn,rOutSumOut:Real;
    ChSet: set of Char;
begin
  //��������
  with dmMC do
  with dmMT do
  begin
    if cxDateEdit1.Date<=prOpenDate then begin  ShowMessage('������ ������.');  exit; end;
    if fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False then begin ShowMessage('������ ������ (��������������).'); exit; end;

    ChSet:=['0','1','2','3','4','5','6','7','8','9'];

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton8.Enabled:=False;

    IdH:=cxTextEdit1.Tag;

    if teSpecObm.State in [dsEdit,dsInsert] then teSpecObm.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    //�� ������������ ������ ��������� �� ���� - ������

    try
      ViewDoc6.BeginUpdate;

      rInSumIn:=0;
      rInSumOut:=0;
      rOutSumIn:=0;
      rOutSumOut:=0;
      iCol:=0;

      //��������� �����
      teSpecObm.First;
      while not teSpecObm.Eof do
      begin
        if teSpecObmQuant.AsFloat>=0 then
        begin
          rInSumIn:=rInSumIn+rv(teSpecObmSumIn.AsFloat);
          rInSumOut:=rInSumOut+rv(teSpecObmSumOut.AsFloat);;
        end else
        begin
          rOutSumIn:=rOutSumIn+rv(teSpecObmSumIn.AsFloat);
          rOutSumOut:=rOutSumOut+rv(teSpecObmSumOut.AsFloat);;
        end;

        teSpecObm.Edit;
        teSpecObmSumIn.AsFloat:=rv(teSpecObmSumIn.AsFloat);
        teSpecObmSumOut.AsFloat:=rv(teSpecObmSumOut.AsFloat);
        teSpecObm.Post;

        teSpecObm.Next;
      end;

      if ptDExchH.Active=False then ptDExchH.Active:=True;
      if IDH=0 then //����������
      begin
        ptDExchH.Append;
        ptDExchHDOCDATE.AsDateTime:=cxDateEdit1.Date;
        ptDExchHDOCNUM.AsString:=AnsiToOemConvert(cxTextEdit1.Text);
        ptDExchHDEPART.AsInteger:=cxLookupComboBox1.EditValue;
        ptDExchHITYPE.AsInteger:=cxComboBox1.ItemIndex;
        ptDExchHINSUMIN.AsFloat:=rInSumIn;
        ptDExchHINSUMOUT.AsFloat:=rInSumOut;
        ptDExchHOUTSUMIN.AsFloat:=rOutSumIn;
        ptDExchHOUTSUMOUT.AsFloat:=rOutSumOut;
        ptDExchHCLITYPE.AsInteger:=Label4.Tag;
        ptDExchHCLICODE.AsInteger:=cxButtonEdit1.Tag;
        ptDExchHIACTIVE.AsInteger:=0;
        ptDExchH.Post;
        delay(20);

        ptDExchH.Refresh;
        IDH:=ptDExchHID.AsInteger;
        cxTextEdit1.Tag:=IdH;

      end else
      begin
        if ptDExchH.FindKey([IDH]) then
        begin
          ptDExchH.Edit;
          ptDExchHDOCDATE.AsDateTime:=cxDateEdit1.Date;
          ptDExchHDOCNUM.AsString:=AnsiToOemConvert(cxTextEdit1.Text);
          ptDExchHDEPART.AsInteger:=cxLookupComboBox1.EditValue;
          ptDExchHITYPE.AsInteger:=cxComboBox1.ItemIndex;
          ptDExchHINSUMIN.AsFloat:=rInSumIn;
          ptDExchHINSUMOUT.AsFloat:=rInSumOut;
          ptDExchHOUTSUMIN.AsFloat:=rOutSumIn;
          ptDExchHOUTSUMOUT.AsFloat:=rOutSumOut;
          ptDExchHCLITYPE.AsInteger:=Label4.Tag;
          ptDExchHCLICODE.AsInteger:=cxButtonEdit1.Tag;
          ptDExchHIACTIVE.AsInteger:=0;
          ptDExchH.Post;
          delay(20);
        end;
      end;

      if ptDExchS.Active=False then ptDExchS.Active:=True;
      ptDExchS.CancelRange;
      ptDExchS.SetRange([IDH],[IDH]);
      ptDExchS.First;
      while not ptDExchS.Eof do ptDExchS.Delete;
      delay(10);

      teSpecObm.First;
      while not teSpecObm.Eof do
      begin
        ptDExchS.Append;
        ptDExchSIDH.AsInteger:=IDH;
        ptDExchSSTYPE.AsInteger:=teSpecObmsType.AsInteger;
        ptDExchSCODE.AsInteger:=teSpecObmCode.AsInteger;
        ptDExchSQUANT.AsFloat:=teSpecObmQuant.AsFloat;
        ptDExchSPRICEIN.AsFloat:=teSpecObmPriceIn.AsFloat;
        ptDExchSPRICEOUT.AsFloat:=teSpecObmPriceOut.AsFloat;
        ptDExchSSUMIN.AsFloat:=teSpecObmSumIn.AsFloat;
        ptDExchSSUMOUT.AsFloat:=teSpecObmSumOut.AsFloat;
        ptDExchSNAC.AsFloat:=teSpecObmNac.AsFloat;
        ptDExchS.Post;

        teSpecObm.Next;
      end;
    finally
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      ViewDoc6.EndUpdate;
      quObmH.Active:=False; quObmH.Active:=True;
      quObmH.Locate('ID',IDH,[]);
    end;
  end;
end;

procedure TfmAddDoc6.acDelAllExecute(Sender: TObject);
begin
  if teSpecObm.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      teSpecObm.First;
      while not teSpecObm.Eof do teSpecObm.Delete;
    end;
  end;
end;

procedure TfmAddDoc6.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc6.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc6.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc6.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc6.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrOut then  Accept:=True;
end;

procedure TfmAddDoc6.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc6.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc6.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmMC do
    begin
     { quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;}
    end;
  end;
end;

procedure TfmAddDoc6.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc6.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC,iMax:INteger;
    rPrIn0:Real;
begin
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if teSpecObm.RecordCount>0 then
    if teSpecObm.Locate('sBar',sBar,[]) then
    begin
      ViewDoc6.Controller.FocusRecord(ViewDoc6.DataController.FocusedRowIndex,True);
      GridDoc6.SetFocus;
      exit;
    end;

  if prFindBarC(sBar,0,iC) then
  begin
    iMax:=1;
    if teSpecObm.RecordCount>0 then begin teSpecObm.Last; iMax:=teSpecObmNum.AsInteger+1; end;

    with dmMt do
    begin
      if taCards.FindKey([iC]) then
      begin
        teSpecObm.Append;
        teSpecObmNum.AsInteger:=iMax;
        teSpecObmCode.AsInteger:=taCardsID.AsInteger;
        teSpecObmsBar.AsString:=sBar;
        teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
        teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
        teSpecObmsType.AsInteger:=0;
        teSpecObmQuant.AsFloat:=0;
        teSpecObmPriceIn.AsFloat:=prFLPriceDateT(taCardsID.AsInteger,RoundEx(Date),rPrIn0);
        teSpecObmNac.AsFloat:=0;
        teSpecObmPriceOut.AsFloat:=taCardsCena.AsFloat;
        teSpecObmSumIn.AsFloat:=0;
        teSpecObmSumOut.AsFloat:=0;
        teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
        teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
        teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
        teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
        teSpecObm.Post;
      end;

      ViewDoc6.Controller.FocusRecord(ViewDoc6.DataController.FocusedRowIndex,True);
      GridDoc6.SetFocus;
    end;
  end else
    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc6.ViewDoc2DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
    if (ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Name') then
    begin
      ViewDoc6Name.Options.Editing:=True;
      ViewDoc6Name.Focused:=True;
    end;
end;

procedure TfmAddDoc6.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxButton1.SetFocus;
  end;
end;

procedure TfmAddDoc6.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc6.cxButtonEdit1PropertiesChange(Sender: TObject);
Var S:String;
begin
  if bOpen then exit;
  S:=cxButtonEdit1.Text;
  if Length(S)>2 then
  begin
    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount=1 then
      begin
        Label4.Tag:=1;
        cxButtonEdit1.Tag:=dmMC.quFCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quFCliName.AsString;
        cxLookupComboBox1.SetFocus;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount=1 then
        begin
          Label4.Tag:=2;
          cxButtonEdit1.Tag:=dmMC.quFIPCode.AsInteger;
          cxButtonEdit1.Text:=dmMC.quFIPName.AsString;
          cxLookupComboBox1.SetFocus;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;
end;

procedure TfmAddDoc6.cxButton6Click(Sender: TObject);
begin //��� �������� ����� ��������
end;


procedure TfmAddDoc6.cxButton7Click(Sender: TObject);
begin //��� �������� ����������
end;

procedure TfmAddDoc6.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc6.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc6.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc6.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc6.Columns[j].Name='ViewDoc6Code' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc6.acPostTovExecute(Sender: TObject);
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if teSpecObm.RecordCount>0 then
    begin
      bDrObm:=True;
      
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=teSpecObmCode.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=teSpecObmCode.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=teSpecObmName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc6.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc6);
end;

procedure TfmAddDoc6.ViewDoc6DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=20 then  Accept:=True;
end;

procedure TfmAddDoc6.ViewDoc6DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    rPr,rNac,rPrIn0:Real;
begin
  if iDirect=20 then
  begin
    iDirect:=0; //������ ���������
    iCol:=0;

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        iMax:=1;
        fmAddDoc6.teSpecObm.First;
        if not fmAddDoc6.teSpecObm.Eof then
        begin
          fmAddDoc6.teSpecObm.Last;
          iMax:=fmAddDoc6.teSpecObmNum.AsInteger+1;
        end;

        with dmMT do
        begin
          ViewDoc6.BeginUpdate;
          try

          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            if taCards.FindKey([iNum]) then
            begin

              rPr:=prFLPriceDateT(taCardsID.AsInteger,RoundEx(Date),rPrIn0);

              rNac:=0;
              if rPr<>0 then
              begin
                rNac:=(taCardsCena.AsFloat-rPr)/rPr*100;
              end;

              teSpecObm.Append;
              teSpecObmNum.AsInteger:=iMax;
              teSpecObmCode.AsInteger:=taCardsID.AsInteger;
              teSpecObmsBar.AsString:=taCardsBarCode.AsString;
              teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
              teSpecObmsType.AsInteger:=0;
              teSpecObmQuant.AsFloat:=0;
              teSpecObmPriceIn.AsFloat:=rPr;
              teSpecObmNac.AsFloat:=rNac;
              teSpecObmPriceOut.AsFloat:=taCardsCena.AsFloat;
              teSpecObmSumIn.AsFloat:=0;
              teSpecObmSumOut.AsFloat:=0;
              teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
              teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
              teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
              teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
              teSpecObm.Post;

              inc(iMax);
            end;
          end;

          finally
            ViewDoc6.EndUpdate;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfmAddDoc6.ViewDoc6Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Quant' then iCol:=1;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6PriceIn' then iCol:=2;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6SumIn' then iCol:=3;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6PriceOut' then iCol:=4;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6SumOut' then iCol:=5;
  if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Nac' then iCol:=6;
end;

procedure TfmAddDoc6.teSpecObmQuantChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=1 then //���-��
  begin
    rQ:=teSpecObmQuant.AsFloat;
    teSpecObmSumIn.AsFloat:=rQ*teSpecObmPriceIn.AsFloat;
    teSpecObmSumOut.AsFloat:=rQ*teSpecObmPriceOut.AsFloat;
    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;
    rNac:=0;
    if teSpecObmSumOut.AsFloat<>0 then
    begin
      if rv(teSpecObmSumIn.AsFloat)<>0 then
        rNac:=(teSpecObmSumOut.AsFloat-rv(teSpecObmSumIn.AsFloat))/rv(teSpecObmSumIn.AsFloat)*100;
    end;
    teSpecObmNac.AsFloat:=rNac;
  end;
end;

procedure TfmAddDoc6.teSpecObmPriceInChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=2 then
  begin
    rQ:=teSpecObmQuant.AsFloat;
    teSpecObmSumIn.AsFloat:=rQ*teSpecObmPriceIn.AsFloat;
    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;

    rNac:=0;
    if teSpecObmSumOut.AsFloat<>0 then
    begin
      if rv(teSpecObmSumIn.AsFloat)<>0 then
        rNac:=(teSpecObmSumOut.AsFloat-rv(teSpecObmSumIn.AsFloat))/rv(teSpecObmSumIn.AsFloat)*100;
    end;
    teSpecObmNac.AsFloat:=rNac;
  end;
end;

procedure TfmAddDoc6.teSpecObmSumInChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=3 then
  begin
    rQ:=teSpecObmQuant.AsFloat;
    if rQ<>0 then
    begin
      teSpecObmPriceIn.AsFloat:=rv(teSpecObmSumIn.AsFloat)/rQ;
    end;

    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;

    rNac:=0;
    if teSpecObmSumOut.AsFloat<>0 then
    begin
      if rv(teSpecObmSumIn.AsFloat)<>0 then
        rNac:=(teSpecObmSumOut.AsFloat-rv(teSpecObmSumIn.AsFloat))/rv(teSpecObmSumIn.AsFloat)*100;
    end;
    teSpecObmNac.AsFloat:=rNac;
  end;
end;

procedure TfmAddDoc6.teSpecObmPriceOutChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=4 then
  begin
    rQ:=teSpecObmQuant.AsFloat;
    teSpecObmSumOut.AsFloat:=rQ*teSpecObmPriceOut.AsFloat;
    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;

    rNac:=0;
    if teSpecObmSumOut.AsFloat<>0 then
    begin
      if rv(teSpecObmSumIn.AsFloat)<>0 then
        rNac:=(teSpecObmSumOut.AsFloat-rv(teSpecObmSumIn.AsFloat))/rv(teSpecObmSumIn.AsFloat)*100;
    end;
    teSpecObmNac.AsFloat:=rNac;
  end;
end;

procedure TfmAddDoc6.teSpecObmSumOutChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=5 then
  begin
    rQ:=teSpecObmQuant.AsFloat;
    if rQ<>0 then
    begin
      teSpecObmPriceOut.AsFloat:=rv(teSpecObmSumOut.AsFloat)/rQ;
    end;

    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;

    rNac:=0;
    if teSpecObmSumOut.AsFloat<>0 then
    begin
      if rv(teSpecObmSumIn.AsFloat)<>0 then
        rNac:=(teSpecObmSumOut.AsFloat-rv(teSpecObmSumIn.AsFloat))/rv(teSpecObmSumIn.AsFloat)*100;
    end;
    teSpecObmNac.AsFloat:=rNac;
  end;
end;

procedure TfmAddDoc6.teSpecObmNacChange(Sender: TField);
Var rQ,rNac:Real;
begin
  if iCol=6 then
  begin
    rQ:=teSpecObmQuant.AsFloat;
    rNac:=teSpecObmNac.AsFloat;
    teSpecObmPriceOut.AsFloat:=rv(teSpecObmPriceIn.AsFloat*rNac/100);
    teSpecObmSumOut.AsFloat:=teSpecObmPriceOut.AsFloat*rQ;

    if rQ>=0 then teSpecObmsType.AsInteger:=1 else teSpecObmsType.AsInteger:=2;
  end;
end;

procedure TfmAddDoc6.ViewDoc6EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    bAdd:Boolean;
    rPr,rNac,rPrIn0:Real;
begin
  with dmMC do
  with dmMT do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Code' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCode));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          bAdd:=True;
          if quFindCStatus.AsInteger>100 then
          begin
            bAdd:=False;
            if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
          end;
          if bAdd then
          begin
            if taCards.FindKey([iCode]) then
            begin
              rPr:=prFLPriceDateT(taCardsID.AsInteger,RoundEx(Date),rPrIn0);

              rNac:=0;
              if rPr<>0 then
              begin
                rNac:=(taCardsCena.AsFloat-rPr)/rPr*100;
              end;

              teSpecObm.Edit;
              teSpecObmCode.AsInteger:=taCardsID.AsInteger;
              teSpecObmsBar.AsString:=taCardsBarCode.AsString;
              teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
              teSpecObmsType.AsInteger:=0;
              teSpecObmQuant.AsFloat:=0;
              teSpecObmPriceIn.AsFloat:=rPr;
              teSpecObmNac.AsFloat:=rNac;
              teSpecObmPriceOut.AsFloat:=taCardsCena.AsFloat;
              teSpecObmSumIn.AsFloat:=0;
              teSpecObmSumOut.AsFloat:=0;
              teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
              teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
              teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
              teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
              teSpecObm.Post;
            end;
          end;
          GridDoc6.SetFocus;
          ViewDoc6Quant.Focused:=True;
        end;
      end;
      if ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Name' then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if sName>'' then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if CountRec1(quFindC) then
            begin
              bAdd:=True;
              if quFindCStatus.AsInteger>100 then
              begin
                bAdd:=False;
                if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
              end;
              if bAdd then
              begin
                if taCards.FindKey([quFindCID.AsInteger]) then
                begin
                  rPr:=prFLPriceDateT(taCardsID.AsInteger,RoundEx(Date),rPrIn0);

                  rNac:=0;
                  if rPr<>0 then
                  begin
                    rNac:=(taCardsCena.AsFloat-rPr)/rPr*100;
                  end;

                  teSpecObm.Edit;
                  teSpecObmCode.AsInteger:=taCardsID.AsInteger;
                  teSpecObmsBar.AsString:=taCardsBarCode.AsString;
                  teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                  teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
                  teSpecObmsType.AsInteger:=0;
                  teSpecObmQuant.AsFloat:=0;
                  teSpecObmPriceIn.AsFloat:=rPr;
                  teSpecObmNac.AsFloat:=rNac;
                  teSpecObmPriceOut.AsFloat:=taCardsCena.AsFloat;
                  teSpecObmSumIn.AsFloat:=0;
                  teSpecObmSumOut.AsFloat:=0;
                  teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
                  teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
                  teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
                  teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
                  teSpecObm.Post;
                end;
              end;
              GridDoc6.SetFocus;
              ViewDoc6Quant.Focused:=True;
            end;
          end;
        end;
      end;
    end else
      if (ViewDoc6.Controller.FocusedColumn.Name='ViewDoc6Code') then
        if fTestKey(Key)=False then
          if teSpecObm.State in [dsEdit,dsInsert] then teSpecObm.Cancel;
  end;
end;

end.
