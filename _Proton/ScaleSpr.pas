unit ScaleSpr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxmdaset, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, cxContainer, cxTextEdit, cxMaskEdit,
  cxSpinEdit;

type
  TfmScaleSpr = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GrSc: TcxGrid;
    ViewSc: TcxGridDBTableView;
    LevelSc: TcxGridLevel;
    taSc: TdxMemData;
    dstaSc: TDataSource;
    cxSEdit1: TcxSpinEdit;
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    Label2: TLabel;
    taScModel: TStringField;
    taScNumber: TStringField;
    taScName: TStringField;
    taScPluCount: TIntegerField;
    taScIp1: TStringField;
    taScIp2: TStringField;
    taScIp3: TStringField;
    taScIp4: TStringField;
    taScIp5: TStringField;
    ViewScRecId: TcxGridDBColumn;
    ViewScNumber: TcxGridDBColumn;
    ViewScModel: TcxGridDBColumn;
    ViewScName: TcxGridDBColumn;
    ViewScPluCount: TcxGridDBColumn;
    ViewScIp1: TcxGridDBColumn;
    ViewScIp2: TcxGridDBColumn;
    ViewScIp3: TcxGridDBColumn;
    ViewScIp4: TcxGridDBColumn;
    ViewScIp5: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmScaleSpr: TfmScaleSpr;

implementation

uses Un1;

{$R *.dfm}

procedure TfmScaleSpr.FormCreate(Sender: TObject);
begin
  GrSc.Align:=AlClient;
end;

procedure TfmScaleSpr.Button1Click(Sender: TObject);
begin
  Label1.Caption:=IntToIp(cxSEdit1.Value);
end;

procedure TfmScaleSpr.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmScaleSpr.Button2Click(Sender: TObject);
begin
  Label2.Caption:=INtToStr(StrToIp(Label1.Caption));
end;

end.
