unit ResTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, cxContainer, cxTextEdit, cxMemo, dxmdaset;

type
  TfmRes = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    ViewTestA1: TcxGridDBTableView;
    LevelTestA1: TcxGridLevel;
    GridTestA: TcxGrid;
    LevelTestA2: TcxGridLevel;
    LevelTestA3: TcxGridLevel;
    LevelTestA4: TcxGridLevel;
    ViewTestA2: TcxGridDBTableView;
    ViewTestA3: TcxGridDBTableView;
    ViewTestA4: TcxGridDBTableView;
    Memo1: TcxMemo;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    teFP: TdxMemData;
    teFPiCode: TIntegerField;
    teFPiName: TStringField;
    teFPFPA: TFloatField;
    teFPFRC: TFloatField;
    teFPAC: TSmallintField;
    dsteFP: TDataSource;
    ViewTestA1RecId: TcxGridDBColumn;
    ViewTestA1iCode: TcxGridDBColumn;
    ViewTestA1iName: TcxGridDBColumn;
    ViewTestA1FPA: TcxGridDBColumn;
    ViewTestA1FRC: TcxGridDBColumn;
    ViewTestA1AC: TcxGridDBColumn;
    teFPFPC: TFloatField;
    ViewTestA1FPC: TcxGridDBColumn;
    Label1: TLabel;
    teFPRemn: TFloatField;
    ViewTestA1Remn: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    teDP: TdxMemData;
    dsteDP: TDataSource;
    teDPDSA: TIntegerField;
    teDPDSC: TIntegerField;
    ViewTestA4RecId: TcxGridDBColumn;
    ViewTestA4iCode: TcxGridDBColumn;
    ViewTestA4iName: TcxGridDBColumn;
    ViewTestA4AC: TcxGridDBColumn;
    ViewTestA4DSA: TcxGridDBColumn;
    ViewTestA4DSC: TcxGridDBColumn;
    teDPiCode: TIntegerField;
    teDPAC: TSmallintField;
    teDPName: TStringField;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    teST: TdxMemData;
    teSTiCode: TIntegerField;
    teSTAC: TSmallintField;
    teSTSTA: TIntegerField;
    teSTCTC: TIntegerField;
    dsteST: TDataSource;
    ViewTestA3RecId: TcxGridDBColumn;
    ViewTestA3iCode: TcxGridDBColumn;
    ViewTestA3AC: TcxGridDBColumn;
    ViewTestA3STA: TcxGridDBColumn;
    ViewTestA3CTC: TcxGridDBColumn;
    PopupMenu3: TPopupMenu;
    N001: TMenuItem;
    N4: TMenuItem;
    N3: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure N001Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRes: TfmRes;

implementation

uses Un1, MT, MDB, u2fdk, AddDoc1, DocsIn;

{$R *.dfm}

procedure TfmRes.FormCreate(Sender: TObject);
begin
  GridTestA.Align:=AlClient;
end;

procedure TfmRes.cxButton1Click(Sender: TObject);
begin
{  if LevelTestA1.Active then Memo1.Lines.Add('1');
  if LevelTestA2.Active then Memo1.Lines.Add('2');
  if LevelTestA3.Active then Memo1.Lines.Add('3');
  if LevelTestA4.Active then Memo1.Lines.Add('4');}


  if LevelTestA1.Active then prNExportExel5(ViewTestA1);
  if LevelTestA2.Active then prNExportExel5(ViewTestA2);
  if LevelTestA3.Active then prNExportExel5(ViewTestA3);
  if LevelTestA4.Active then prNExportExel5(ViewTestA4);
end;

procedure TfmRes.cxButton2Click(Sender: TObject);
Var iC:INteger;
begin
  LevelTestA1.Active:=True;
  with dmMt do
  with dmMC do
  begin
    Memo1.Clear;
    Memo1.Lines.Add('������ ��������.');
    CloseTe(teFP);
    ViewTestA1.BeginUpdate;
    Memo1.Lines.Add('  ������������ ������.'); delay(10);
    quAkciya.First;
    while not quAkciya.Eof do
    begin
      if quAkciyaAType.AsInteger=1 then
      begin
        teFp.Append;
        teFPiCode.AsInteger:=quAkciyaCode.AsInteger;
        teFPiName.AsString:='';
        teFPFPA.AsFloat:=quAkciyaVal1.AsFloat;
        teFPFRC.AsFloat:=0;
        teFPFPC.AsFloat:=0;
        teFPAC.AsInteger:=1;
        teFP.Post;
      end;

      quAkciya.Next;
    end;

    Memo1.Lines.Add('  �������� ��������.'); delay(10);
    iC:=0;

    if taCards.Active=False then taCards.Active:=True;
    taCards.First;
    while not taCards.Eof do
    begin
      if taCardsFixPrice.AsFloat>0 then
      begin
        if taCardsStatus.AsInteger>=100 then
        begin
          taCards.Edit;
          taCardsFixPrice.AsFloat:=0;
          taCards.Post;
        end else
        begin
          if teFP.Locate('iCode',taCardsID.AsInteger,[]) then
          begin
            teFP.Edit;
            teFPiName.AsString:=OemToAnsiConvert(taCardsName.AsString);
            teFPFRC.AsFloat:=taCardsCena.AsFloat;
            teFPFPC.AsFloat:=taCardsFixPrice.AsFloat;
            teFPRemn.AsFloat:=taCardsReserv1.AsFloat;
            teFp.Post;
          end else
          begin
            teFP.Append;
            teFPiCode.AsInteger:=taCardsID.AsInteger;
            teFPiName.AsString:=OemToAnsiConvert(taCardsName.AsString);
            teFPFRC.AsFloat:=taCardsCena.AsFloat;
            teFPFPC.AsFloat:=taCardsFixPrice.AsFloat;
            teFPFPA.AsFloat:=0;
            teFPAC.AsInteger:=0;
            teFPRemn.AsFloat:=taCardsReserv1.AsFloat;
            teFp.Post;
          end;
        end;
      end;

      taCards.Next;   inc(iC);
      if iC mod 100 = 0 then
      begin
        Label1.Caption:=its(iC);
        delay(10);
      end;
    end;

    Memo1.Lines.Add('  ������ ������.'); delay(10);
    teFP.First;
    while not teFP.Eof do
    begin
      if teFPAC.AsInteger=1 then
      begin
        if (abs(teFPFPA.AsFloat-teFPFPC.AsFloat)<0.01) then
        begin
          if (abs(teFPFPA.AsFloat-teFPFRC.AsFloat)<0.01) then
          begin
            teFP.Delete; //��� �� - ������������� �� �����
          end else teFP.Next;
        end else teFP.Next;
      end else teFP.Next;
    end;

    ViewTestA1.EndUpdate;

    Memo1.Lines.Add('������� ��������.'); delay(10);
  end;
end;

procedure TfmRes.N1Click(Sender: TObject);
begin
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;

    teFp.First;
    while not teFp.Eof do
    begin
      if teFpAC.AsInteger=0 then
      begin
        if abs(teFPRemn.AsFloat)<0.01 then
        begin
          if taCards.FindKey([teFPiCode.AsInteger]) then
          begin
            taCards.Edit;
            taCardsFixPrice.AsFloat:=0;
            taCards.Post;
          end;
        end;
      end;

      teFp.Next;
    end;
  end;
end;

procedure TfmRes.cxButton5Click(Sender: TObject);
begin
  LevelTestA4.Active:=True;
  CloseTe(teDP);
  with dmMT do
  with dmMC do
  begin
    ptDStop.Active:=False;
    ptDStop.Active:=True;

    Memo1.Clear;
    Memo1.Lines.Add('������ ��������.');
    ViewTestA4.BeginUpdate;
    Memo1.Lines.Add('  ������������ ������.'); delay(10);
    quAkciya.First;
    while not quAkciya.Eof do
    begin
      if quAkciyaAType.AsInteger=2 then
      begin
        if quAkciyaDStop.AsInteger=0 then
        begin
          teDP.Append;
          teDPiCode.AsInteger:=quAkciyaCode.AsInteger;
          teDPName.AsString:='';
          teDPAC.AsInteger:=1;
          teDPDSA.AsInteger:=0;
          if ptDStop.FindKey([quAkciyaCode.AsInteger]) then
          begin
            if ptDStopPercent.AsFloat>99 then //100
            begin
              teDPDSC.AsInteger:=0;
            end else
            begin
              teDPDSC.AsInteger:=2;
            end;
          end else
            teDPDSC.AsInteger:=1;
          teDP.Post;
        end;
      end;
      quAkciya.Next;
    end;

    Memo1.Lines.Add('  �������� ������.'); delay(10);

    ptDStop.First;
    while not ptDStop.Eof do
    begin
      if ptDStopPercent.AsFloat>99.5 then //100
      begin //������ �� ���������

          if teDP.Locate('iCode',ptDStopCode.AsInteger,[]) then
          begin
            teDP.Edit;
            teDPDSC.AsInteger:=0;
            teDP.Post;
          end else
          begin
            teDP.Append;
            teDPiCode.AsInteger:=ptDStopCode.AsInteger;
            teDPName.AsString:='';
            teDPAC.AsInteger:=0;
            teDPDSA.AsInteger:=1;
            teDPDSC.AsInteger:=0;
            teDP.Post;
          end;

      end;
      ptDStop.Next;
    end;

    ViewTestA4.EndUpdate;
  end;
end;

procedure TfmRes.N2Click(Sender: TObject);
begin
  with dmMt do
  begin
    if ptDStop.Active=False then ptDStop.Active:=True;

    teDP.First;
    while not teDP.Eof do
    begin
      if ptDStop.FindKey([teDPiCode.AsInteger]) then
      begin
        if teDPAC.AsInteger=0 then   //� ����� ��� - ������ ������ �����������
        begin
          ptDStop.Edit;
          ptDStopPercent.AsFloat:=0; //������ ���������
          ptDStop.Post;
        end else
        begin
          if teDPDSA.AsInteger=0 then  //���� � ����� � ������ �� ���������
          begin
            ptDStop.Edit;
            ptDStopPercent.AsFloat:=100; //������ �� ���������
            ptDStop.Post;
          end else
          begin
            ptDStop.Edit;
            ptDStopPercent.AsFloat:=0; //������ ���������
            ptDStop.Post;
          end;
        end;
      end else //� ������ ���� � ��������� ���
      begin
        if teDPDSA.AsInteger=0 then  //���� � ����� � ������ �� ���������
        begin
          ptDStop.Append;
          ptDStopCode.AsInteger:=teDPiCode.AsInteger;
          ptDStopPercent.AsFloat:=100; //������ �� ���������
          ptDStopStatus.AsInteger:=1;
          ptDStopStatus_1.AsInteger:=0;
          ptDStop.Post;
        end;
      end;
      teDP.Next;
    end;
  end;

end;

procedure TfmRes.cxButton4Click(Sender: TObject);
Var iC:INteger;
begin
  LevelTestA3.Active:=True;
  with dmMt do
  with dmMC do
  begin
    if taCards.Active=False then taCards.Active:=True;

    Memo1.Clear;
    Memo1.Lines.Add('������ ��������.');
    CloseTe(teST);
    ViewTestA3.BeginUpdate;
    Memo1.Lines.Add('  ������������ ������.'); delay(10);
    quAkciya.First;
    while not quAkciya.Eof do
    begin
      if quAkciyaAType.AsInteger=3 then
      begin
        teST.Append;
        teSTiCode.AsInteger:=quAkciyaCode.AsInteger;
        teSTAC.AsInteger:=1;
        teSTSTA.AsInteger:=quAkciyaDStop.AsInteger;
        if taCards.FindKey([quAkciyaCode.AsInteger]) then teSTCTC.AsInteger:=taCardsV02.AsInteger
        else teSTCTC.AsInteger:=-1;
        teST.Post;
      end;

      quAkciya.Next;
    end;

    Memo1.Lines.Add('  �������� ��������.'); delay(10);
    iC:=0;

    if taCards.Active=False then taCards.Active:=True;
    taCards.First;
    while not taCards.Eof do
    begin
      if taCardsV02.AsFloat>=0 then //��������
      begin
        if taCardsStatus.AsInteger>=100 then
        begin
          taCards.Edit;
          taCardsV02.AsINteger:=1;
          taCards.Post;
        end else
        begin
          if teST.Locate('iCode',taCardsID.AsInteger,[]) then
          begin
            teST.Edit;
            teSTCTC.AsInteger:=taCardsV02.AsINteger;
            teST.Post;
          end else
          begin
            if taCardsV02.AsFloat>1 then //������ ������ 00
            begin
              teST.Append;
              teSTiCode.AsInteger:=taCardsID.AsInteger;
              teSTSTA.AsInteger:=0;
              teSTCTC.AsInteger:=taCardsV02.AsINteger;
              teSTAC.AsInteger:=0;
              teST.Post;
            end;
          end;
        end;
      end;

      taCards.Next;   inc(iC);
      if iC mod 100 = 0 then
      begin
        Label1.Caption:=its(iC);
        delay(10);
      end;
    end;

    ViewTestA3.EndUpdate;
  end;
end;

procedure TfmRes.N001Click(Sender: TObject);
Var iC:INteger;
begin
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;
    iC:=0;
    taCards.First;
    while not taCards.Eof do
    begin
      if taCardsV02.AsFloat>=2 then
      begin
        taCards.Edit;
        taCardsV02.AsINteger:=1;
        taCards.Post;
      end;

      taCards.Next;   inc(iC);
      if iC mod 100 = 0 then
      begin
        Label1.Caption:=its(iC);
        delay(10);
      end;
    end;

    teST.First;
    while not teST.Eof do
    begin
      if teSTAC.AsInteger=1 then
      begin
        if teSTSTA.AsInteger=2 then
        begin
          if taCards.FindKey([teSTiCode.AsInteger]) then
          begin
            taCards.Edit;
            taCardsV02.AsFloat:=2;
            taCards.Post;
          end;
        end;
      end;

      teST.Next;
    end;
  end;
end;

procedure TfmRes.N4Click(Sender: TObject);
Var  rn1,rn2,rn3,rPr,rPrA:Real;
     iMax:INteger;
begin
  with dmMC do
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;
    fmDocs1.prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc1.taSpecIn);
    fmAddDoc1.acSaveDoc.Enabled:=True;

    iMax:=1;
    
    teFp.First;
    while not teFp.Eof do
    begin
      if teFpAC.AsInteger=0 then
      begin
        if abs(teFPRemn.AsFloat)>0.01 then
        begin
          if taCards.FindKey([teFPiCode.AsInteger]) then
          begin
{           taCards.Edit;
            taCardsFixPrice.AsFloat:=0;
            taCards.Post;}

            with fmAddDoc1 do
            begin
              prCalcPriceOut(taCardsID.AsInteger,Trunc(Date),0,0,rn1,rn2,rn3,rPr,rPrA);

              taSpecIn.Append;
              taSpecInNum.AsInteger:=iMax;
              taSpecInCodeTovar.AsInteger:=taCardsID.AsInteger;
              taSpecInCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              taSpecInBarCode.AsString:=taCardsBarCode.AsString;
              taSpecInNDSProc.AsFloat:=taCardsNDS.AsFloat;
              taSpecInNDSSum.AsFloat:=0;
              taSpecInOutNDSSum.AsFloat:=0;
              taSpecInBestBefore.AsDateTime:=Date;
              taSpecInKolMest.AsInteger:=1;
              taSpecInKolEdMest.AsFloat:=0;
              taSpecInKolWithMest.AsFloat:=1;
              taSpecInKolWithNecond.AsFloat:=0;
              taSpecInKol.AsFloat:=0;
              taSpecInCenaTovar.AsFloat:=0;
              taSpecInProcent.AsFloat:=0;
              taSpecInNewCenaTovar.AsFloat:=rPr;
              taSpecInSumCenaTovarPost.AsFloat:=0;
              taSpecInSumCenaTovar.AsFloat:=rPr;
              taSpecInProcentN.AsFloat:=0;
              taSpecInNecond.AsFloat:=0;
              taSpecInCenaNecond.AsFloat:=0;
              taSpecInSumNecond.AsFloat:=0;
              taSpecInProcentZ.AsFloat:=0;
              taSpecInZemlia.AsFloat:=0;
              taSpecInProcentO.AsFloat:=0;
              taSpecInOthodi.AsFloat:=0;
              taSpecInName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              taSpecInCodeTara.AsInteger:=0;
              taSpecInVesTara.AsFloat:=0;
              taSpecInCenaTara.AsFloat:=0;
              taSpecInSumCenaTara.AsFloat:=0;
              taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
              taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
              taSpecInNac1.AsFloat:=rn1;
              taSpecInNac2.AsFloat:=rn2;
              taSpecInNac3.AsFloat:=rn3;
              taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
              taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
              if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
              if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
              taSpecInCenaTovarAvr.AsFloat:=rPrA;
              taSpecInsMaker.AsString:='';
              taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
              taSpecIn.Post;   inc(iMax);
            end;

          end;
        end;
      end;

      teFp.Next;
    end;

    fmAddDoc1.Show;
  end;
end;

procedure TfmRes.N3Click(Sender: TObject);
begin
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;

    teFp.First;
    while not teFp.Eof do
    begin
      if teFpAC.AsInteger=0 then
      begin
        if abs(teFPRemn.AsFloat)>=0.01 then
        begin
          if taCards.FindKey([teFPiCode.AsInteger]) then
          begin
            taCards.Edit;
            taCardsFixPrice.AsFloat:=0;
            taCards.Post;
          end;
        end;
      end;

      teFp.Next;
    end;
  end;
end;

end.

