unit AlcoHd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, SpeedBar,
  ExtCtrls, Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ActnList, XPStyleActnCtrls, ActnMan, cxImageComboBox;

type
  TfmAlcoE = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    Memo1: TcxMemo;
    FormPlacement1: TFormPlacement;
    ViewAlco: TcxGridDBTableView;
    LevelAlco: TcxGridLevel;
    GridAlco: TcxGrid;
    ViewAlcoIdOrg: TcxGridDBColumn;
    ViewAlcoId: TcxGridDBColumn;
    ViewAlcoDDateB: TcxGridDBColumn;
    ViewAlcoDDateE: TcxGridDBColumn;
    ViewAlcoIT1: TcxGridDBColumn;
    ViewAlcoSPers: TcxGridDBColumn;
    ViewAlcoName: TcxGridDBColumn;
    amAlco: TActionManager;
    acPeriod: TAction;
    acAddAlco: TAction;
    acEditAlco: TAction;
    acViewAlco: TAction;
    acDelAlco: TAction;
    ViewAlcoIT2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddAlcoExecute(Sender: TObject);
    procedure acEditAlcoExecute(Sender: TObject);
    procedure acDelAlcoExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAlcoE: TfmAlcoE;

implementation

uses Un1, MDB, DBAlg, Period1, AlcoSp, MT;

{$R *.dfm}

procedure TfmAlcoE.FormCreate(Sender: TObject);
begin
  GridAlco.Align:=AlClient;
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Memo1.Clear;
end;

procedure TfmAlcoE.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAlcoE.acPeriodExecute(Sender: TObject);
begin
  //
  with dmAlg do
  begin

    fmPeriod1.cxDateEdit1.Date:=CommonSet.ADateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.ADateEnd;
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.ADateBeg:=fmPeriod1.cxDateEdit1.Date;
      CommonSet.ADateEnd:=fmPeriod1.cxDateEdit2.Date;

      fmAlcoE.Caption:='������ ����������� ��������� �� ������ � '+ds1(CommonSet.ADateBeg)+' �� '+ds1(CommonSet.ADateEnd);
      fmAlcoE.Memo1.Clear;

      fmAlcoE.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

      fmAlcoE.ViewAlco.BeginUpdate;
      quAlcoDH.Active:=False;
      quAlcoDH.ParamByName('IDATEB').Value:=Trunc(CommonSet.ADateBeg);
      quAlcoDH.ParamByName('IDATEE').Value:=Trunc(CommonSet.ADateEnd);
      quAlcoDH.Active:=True;
      fmAlcoE.ViewAlco.EndUpdate;

      fmAlcoE.Memo1.Lines.Add('������������ ������ ��.'); delay(10);
    end;
  end;
end;

procedure TfmAlcoE.acAddAlcoExecute(Sender: TObject);
begin
  //�������� ����������
  if not CanDo('prAddAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmAlg do
    begin

      quOrgList.Active:=False;
      quOrgList.Active:=True;
      quOrgList.First;
      fmAddAlco.cxLookupComboBox1.EditValue:=quOrgListID.AsInteger;
      fmAddAlco.cxLookupComboBox1.Tag:=0;

      fmAddAlco.cxDateEdit1.Date:=Date-1;
      fmAddAlco.cxDateEdit2.Date:=Date;

      CloseTe(fmAddAlco.teAlcoOb);
      CloseTe(fmAddAlco.teAlcoIn);

      fmAddAlco.cxButton1.Enabled:=True;
    end;
    fmAddAlco.ShowModal;
  end;
end;

procedure TfmAlcoE.acEditAlcoExecute(Sender: TObject);
Var IDH:INteger;
    Sinn,Skpp:String;
    iCliCB:INteger;
begin
  //�������������
  if not CanDo('prEditAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmAlg do
    with dmMt do
    with dmMC do
    begin
      if quAlcoDH.RecordCount>0 then
      begin
        quOrgList.Active:=False;
        quOrgList.Active:=True;
        quOrgList.First;

        quOrgList.Locate('ID',quAlcoDHIdOrg.AsInteger,[]);

        fmAddAlco.cxLookupComboBox1.EditValue:=quAlcoDHIdOrg.AsInteger;
        fmAddAlco.cxLookupComboBox1.Tag:=quAlcoDHId.AsInteger;

        fmAddAlco.cxDateEdit1.Date:=quAlcoDHDDateB.AsDateTime;
        fmAddAlco.cxDateEdit2.Date:=quAlcoDHDDateE.AsDateTime;

        fmAddAlco.cxComboBox1.ItemIndex:=quAlcoDHIT1.AsInteger;
        fmAddAlco.cxComboBox2.ItemIndex:=quAlcoDHIT2.AsInteger;

        IDH:=quAlcoDHId.AsInteger;

        CloseTe(fmAddAlco.teAlcoOb);
        CloseTe(fmAddAlco.teAlcoIn);

        fmAddAlco.cxButton1.Enabled:=True;

        fmAddAlco.ViAlco1.BeginUpdate;
        fmAddAlco.ViAlco2.BeginUpdate;

        fmAddAlco.dsteAlcoOb.DataSet:=nil;
        fmAddAlco.dsteAlcoIn.DataSet:=nil;

        quAlcoDS1.Active:=False;
        quAlcoDS1.ParamByName('IDH').Value:=IDH;
        quAlcoDS1.Active:=True;

        quAlcoDS1.First;
        while not quAlcoDS1.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoOb.Append;
            teAlcoObiDep.AsInteger:=quAlcoDS1iDep.AsInteger;
            teAlcoObsDep.AsString:=quAlcoDS1NameShop.AsString;
            teAlcoObiVid.AsInteger:=quAlcoDS1iVid.AsInteger;
            teAlcoObsVid.AsString:=quAlcoDS1iVid.AsString;
            teAlcoObsVidName.AsString:=quAlcoDS1NameVid.AsString;
            teAlcoObiProd.AsInteger:=quAlcoDS1iProd.AsInteger;
            teAlcoObsProd.AsString:=quAlcoDS1NameProducer.AsString;
            teAlcoObsProdInn.AsString:=quAlcoDS1INN.AsString;
            teAlcoObsProdKPP.AsString:=quAlcoDS1KPP.AsString;
            teAlcoObrQb.AsFloat:=quAlcoDS1rQb.AsFloat;
            teAlcoObrQIn1.AsFloat:=quAlcoDS1rQIn1.AsFloat;
            teAlcoObrQIn2.AsFloat:=quAlcoDS1rQIn2.AsFloat;
            teAlcoObrQIn3.AsFloat:=quAlcoDS1rQIn3.AsFloat;
            teAlcoObrQInIt1.AsFloat:=quAlcoDS1rQInIt1.AsFloat;
            teAlcoObrQIn4.AsFloat:=quAlcoDS1rQIn4.AsFloat;
            teAlcoObrQIn5.AsFloat:=quAlcoDS1rQIn5.AsFloat;
            teAlcoObrQIn6.AsFloat:=quAlcoDS1rQIn6.AsFloat;
            teAlcoObrQInIt.AsFloat:=quAlcoDS1rQInIt.AsFloat;
            teAlcoObrQOut1.AsFloat:=quAlcoDS1rQOut1.AsFloat;
            teAlcoObrQOut2.AsFloat:=quAlcoDS1rQOut2.AsFloat;
            teAlcoObrQOut3.AsFloat:=quAlcoDS1rQOut3.AsFloat;
            teAlcoObrQOut4.AsFloat:=quAlcoDS1rQOut4.AsFloat;
            teAlcoObrQOutIt.AsFloat:=quAlcoDS1rQOutIt.AsFloat;
            teAlcoObrQe.AsFloat:=quAlcoDS1rQe.AsFloat;
            teAlcoOb.Post;
          end;
          quAlcoDS1.Next;
        end;
        quAlcoDS1.Active:=False;

        quAlcoDS2.Active:=False;
        quAlcoDS2.ParamByName('IDH').Value:=IDH;
        quAlcoDS2.Active:=True;

        quAlcoDS2.First;
        while not quAlcoDS2.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoIn.Append;
            teAlcoIniDep.AsInteger:=quAlcoDS2iDep.AsInteger;
            teAlcoInsDep.AsString:=quAlcoDS2NameShop.AsString;
            teAlcoIniVid.AsInteger:=quAlcoDS2iVid.AsInteger;
            teAlcoInsVid.AsString:=quAlcoDS2iVid.AsString;
            teAlcoInsVidName.AsString:=quAlcoDS2NameVid.AsString;
            teAlcoIniProd.AsInteger:=quAlcoDS2iProd.AsInteger;
            teAlcoInsProd.AsString:=quAlcoDS2NameProducer.AsString;
            teAlcoInsProdInn.AsString:=quAlcoDS2INN.AsString;
            teAlcoInsProdKPP.AsString:=quAlcoDS2KPP.AsString;
            teAlcoIniPost.AsInteger:=quAlcoDS2iPost.AsInteger;
            teAlcoInsPost.AsString:=prFindCliName2(1,quAlcoDS2iPost.AsInteger,Sinn,Skpp,iCliCB);
            teAlcoInsPostInn.AsString:=Sinn;
            teAlcoInsPostKpp.AsString:=Skpp;
            teAlcoIniLic.AsInteger:=quAlcoDS2iLic.AsInteger;
            teAlcoInsLicNumber.AsString:=quAlcoDS2LicNum.AsString;
            teAlcoInLicDateB.AsDateTime:=quAlcoDS2LicDateB.AsDateTime;
            teAlcoInLicDateE.AsDateTime:=quAlcoDS2LicDateE.AsDateTime;
            teAlcoInLicOrg.AsString:=quAlcoDS2LicOrg.AsString;
            teAlcoInDocDate.AsDateTime:=quAlcoDS2DocDate.AsDateTime;
            teAlcoInDocNum.AsString:=quAlcoDS2DocNum.AsString;
            teAlcoInGTD.AsString:=quAlcoDS2GTD.AsString;
            teAlcoInrQIn.AsFloat:=quAlcoDS2rQIn.AsFloat;
            teAlcoInDocId.AsInteger:=quAlcoDS2IdHdr.AsInteger;
            teAlcoIn.Post;
          end;
          quAlcoDS2.Next;
        end;
        quAlcoDS2.Active:=False;

        fmAddAlco.dsteAlcoOb.DataSet:=fmAddAlco.teAlcoOb;
        fmAddAlco.dsteAlcoIn.DataSet:=fmAddAlco.teAlcoIn;

        fmAddAlco.ViAlco1.EndUpdate;
        fmAddAlco.ViAlco2.EndUpdate;


        fmAddAlco.ShowModal;
      end;
    end;
  end;
end;

procedure TfmAlcoE.acDelAlcoExecute(Sender: TObject);
Var IDH:INteger;
begin
//�������
  if not CanDo('prDelAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmAlg do
    begin
      if MessageDlg('����� ������ ������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if quAlcoDH.RecordCount>0 then
        begin
          IDH:=quAlcoDHId.AsInteger;
          Memo1.Clear;
          Memo1.Lines.Add('����� ���� �������� ���������.'); delay(10);
          Memo1.Lines.Add('  - �������������.'); delay(10);

          quDelDS.SQL.Clear;
          quDelDS.SQL.Add('Delete from "A_ALCGDS1"');
          quDelDS.SQL.Add('where IdH='+its(IDH));
          quDelDS.ExecSQL;

          delay(100);

          Memo1.Lines.Add('  - �������������..'); delay(10);

          quDelDS.SQL.Clear;
          quDelDS.SQL.Add('Delete from "A_ALCGDS2"');
          quDelDS.SQL.Add('where IdH='+its(IDH));
          quDelDS.ExecSQL;

          delay(100);

          Memo1.Lines.Add('  - ���������'); delay(10);

          quDelDS.SQL.Clear;
          quDelDS.SQL.Add('Delete from "A_ALCGDH"');
          quDelDS.SQL.Add('where Id='+its(IDH));
          quDelDS.ExecSQL;
          delay(100);

          Memo1.Lines.Add('�������� ��'); delay(10);

          Memo1.Lines.Add('����������'); delay(10);

          fmAlcoE.ViewAlco.BeginUpdate;
          quAlcoDH.Active:=False;
          quAlcoDH.ParamByName('IDATEB').Value:=Trunc(CommonSet.ADateBeg);
          quAlcoDH.ParamByName('IDATEE').Value:=Trunc(CommonSet.ADateEnd);
          quAlcoDH.Active:=True;
          fmAlcoE.ViewAlco.EndUpdate;

          quAlcoDH.First;

          Memo1.Lines.Add('���������� ��'); delay(10);
        end;
      end;
    end;
  end;
end;

end.
