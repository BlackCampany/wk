unit SpInEr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, dxmdaset, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCheckBox, cxImageComboBox, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls;

type
  TfmSpInEr = class(TForm)
    ViewSpEr: TcxGridDBTableView;
    LevSpEr: TcxGridLevel;
    cxGrSpEr: TcxGrid;
    taSpEr: TdxMemData;
    taSpErNum: TIntegerField;
    taSpErCodeTovar: TIntegerField;
    taSpErCodeEdIzm: TSmallintField;
    taSpErNameCode: TStringField;
    taSpErTender: TIntegerField;
    taSpErkolRaz: TFloatField;
    taSpErkolZ: TFloatField;
    taSpErkolSp: TFloatField;
    taSpErCenaZ: TFloatField;
    taSpErCenaSp: TFloatField;
    taSpErCenaRaz: TFloatField;
    ViewSpErNum: TcxGridDBColumn;
    ViewSpErCodeTovar: TcxGridDBColumn;
    ViewSpErTender: TcxGridDBColumn;
    ViewSpErCodeEdIzm: TcxGridDBColumn;
    ViewSpErkolZ: TcxGridDBColumn;
    ViewSpErkolSp: TcxGridDBColumn;
    ViewSpErkolRaz: TcxGridDBColumn;
    ViewSpErCenaZ: TcxGridDBColumn;
    ViewSpErCenaSp: TcxGridDBColumn;
    ViewSpErCenaRaz: TcxGridDBColumn;
    ViewSpErNameCode: TcxGridDBColumn;
    dstaSpEr: TDataSource;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSpInEr: TfmSpInEr;

implementation

uses Un1, AddDoc1, DocsIn;

{$R *.dfm}

procedure TfmSpInEr.cxButton1Click(Sender: TObject);
begin
prNExportExel5(ViewSpEr);
end;

procedure TfmSpInEr.cxButton2Click(Sender: TObject);
begin
 close;
 taSpEr.Active:=false;
 if fmAddDoc1.Showing then fmAddDoc1.Show
 else fmDocs1.Show; 
end;

end.
