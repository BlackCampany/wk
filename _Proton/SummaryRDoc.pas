unit SummaryRDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, cxClasses, cxGraphics, cxCustomData, cxStyles, cxEdit,
  cxControls, cxCustomPivotGrid, cxDBPivotGrid, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  dxPScxPivotGrid2Lnk, ActnList, XPStyleActnCtrls, ActnMan, cxImageComboBox,
  cxDBLookupComboBox;

type
  TfmSummary1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    PivotGrid1: TcxDBPivotGrid;
    PrintExp3: TdxComponentPrinter;
    PrintExp3Link1: TcxPivotGridReportLink;
    am1: TActionManager;
    acPrintTab: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    PrintExp3Link2: TcxPivotGridReportLink;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    PivotGrid1CATEGORY: TcxDBPivotGridField;
    PivotGrid1DATEDOC: TcxDBPivotGridField;
    PivotGrid1DAYWEEK: TcxDBPivotGridField;
    PivotGrid1IDCARD: TcxDBPivotGridField;
    PivotGrid1KVART: TcxDBPivotGridField;
    PivotGrid1MHFROM: TcxDBPivotGridField;
    PivotGrid1NAME: TcxDBPivotGridField;
    PivotGrid1NAMECL: TcxDBPivotGridField;
    PivotGrid1NAMESHORT: TcxDBPivotGridField;
    PivotGrid1QUANT: TcxDBPivotGridField;
    PivotGrid1RNac: TcxDBPivotGridField;
    PivotGrid1SUMIN: TcxDBPivotGridField;
    PivotGrid1SUMR: TcxDBPivotGridField;
    PivotGrid1WEEKOFM: TcxDBPivotGridField;
    PivotGrid2: TcxDBPivotGrid;
    cxDBPivotGridField1: TcxDBPivotGridField;
    cxDBPivotGridField2: TcxDBPivotGridField;
    cxDBPivotGridField3: TcxDBPivotGridField;
    cxDBPivotGridField4: TcxDBPivotGridField;
    cxDBPivotGridField5: TcxDBPivotGridField;
    cxDBPivotGridField6: TcxDBPivotGridField;
    cxDBPivotGridField7: TcxDBPivotGridField;
    cxDBPivotGridField8: TcxDBPivotGridField;
    cxDBPivotGridField9: TcxDBPivotGridField;
    cxDBPivotGridField10: TcxDBPivotGridField;
    cxDBPivotGridField11: TcxDBPivotGridField;
    cxDBPivotGridField12: TcxDBPivotGridField;
    cxDBPivotGridField13: TcxDBPivotGridField;
    cxDBPivotGridField14: TcxDBPivotGridField;
    PivotGrid1OPER: TcxDBPivotGridField;
    procedure FormCreate(Sender: TObject);
    procedure acPrintTabExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSummary1: TfmSummary1;

implementation

uses Un1, RepPrib, SortPar, ExcelList, DMOReps;

{$R *.dfm}

procedure TfmSummary1.FormCreate(Sender: TObject);
begin
  PivotGrid1.Align:=alClient;
  PivotGrid2.Align:=alClient;
  PivotGrid1.Visible:=True;
  PivotGrid2.Visible:=False;
  PivotGrid1.RestoreFromIniFile(CurDir+GridIni);
  PivotGrid2.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmSummary1.acPrintTabExecute(Sender: TObject);
begin
  if PivotGrid1.Visible then
  begin
    PrintExp3.CurrentLink:=PrintExp3Link1;
    //������ �������
    PrintExp3Link1.ReportTitle.Text:=fmSummary1.Caption;
    PrintExp3.Preview(True,nil);
  end;
  if PivotGrid2.Visible then
  begin
    PrintExp3.CurrentLink:=PrintExp3Link2;
    PrintExp3Link2.ReportTitle.Text:=fmSummary1.Caption;

    PrintExp3.Preview(True,nil);
  end;
end;

procedure TfmSummary1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PivotGrid1.StoreToIniFile(CurDir+GridIni,False);
  PivotGrid2.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmSummary1.cxButton3Click(Sender: TObject);
Var i:INteger;
    StrWk:String;
    fField:TcxPivotGridField;
begin
  if PivotGrid1.Visible then
  begin
    try
      fmSortF:=tfmSortF.Create(Application);
      with fmSortF do
      begin
        CloseTe(taRowF);
        CloseTe(taColVal);

        for i:=0 to PivotGrid1.FieldCount-1 do
        begin
          if PivotGrid1.Fields[i].Area=faRow then
          begin
            taRowF.Append;
            taRowFId.AsInteger:=i;
            taRowFCapt.AsString:=PivotGrid1.Fields[i].Caption;
            StrWk:=PivotGrid1.Fields[i].Name;
//          Delete(StrWk,1,10);
            taRowFNameF.AsString:=StrWk;
            taRowF.Post;
          end;

          if PivotGrid1.Fields[i].Area=faData then
          begin
            taColVal.Append;
            taColValId.AsInteger:=i;
            taColValCapt.AsString:=PivotGrid1.Fields[i].Caption;
            StrWk:=PivotGrid1.Fields[i].Name;
//          Delete(StrWk,1,10);
            taColValNameF.AsString:=StrWk;
            taColVal.Post;
          end;
        end;

        taRowF.Last;
        if taRowF.RecordCount>0 then
        begin
          cxLookupComboBox1.EditValue:=taRowFNameF.AsString;
          cxLookupComboBox1.Text:=taRowFCapt.AsString;
          cxLookupComboBox1.Enabled:=True;
        end else
        begin
          cxLookupComboBox1.EditValue:='';
          cxLookupComboBox1.Text:='��� ��������';
          cxLookupComboBox1.Enabled:=False;
        end;


        taColVal.First;
        if taColVal.RecordCount>0 then
        begin
          cxLookupComboBox2.EditValue:=taColValNameF.AsString;
          cxLookupComboBox2.Text:=taColValCapt.AsString;
          cxLookupComboBox2.Enabled:=True;
        end else
        begin
          cxLookupComboBox2.EditValue:='';
          cxLookupComboBox2.Text:='��� ��������';
          cxLookupComboBox2.Enabled:=False;
        end;
      end;
      fmSortF.ShowModal;
      if fmSortF.ModalResult=mrOk then
      begin
        // sync settings with selected field
        PivotGrid1.BeginUpdate;
        fField:=PivotGrid1.GetFieldByName(fmSortF.cxLookupComboBox1.Text);
        if fmSortF.cxRadioButton1.Checked then fField.SortOrder:=soDescending;
        if fmSortF.cxRadioButton2.Checked then fField.SortOrder:=soAscending;
        fField.SortBySummaryInfo.Field := PivotGrid1.GetFieldByName(fmSortF.cxLookupComboBox2.Text);

        if fmSortF.cxCheckBox1.Checked then
        begin
          fField.TopValueCount:=0;
          fField.TopValueShowOthers:=True;
        end
        else
        begin
          fField.TopValueCount:=fmSortF.cxSpinEdit1.EditValue;
          fField.TopValueShowOthers:=fmSortF.CheckBox1.Checked;
        end;

        PivotGrid1.EndUpdate;
      end;
    finally
      fmSortF.Release;
    end;
  end;
end;

procedure TfmSummary1.cxButton4Click(Sender: TObject);
var
  PG:TcxDBPivotGrid;
begin
  //������� � ������
  fmExcelList:=tfmExcelList.Create(Application);
  with fmExcelList do
  begin
    GridEx.Align:=AlClient;
    ViewEx.BeginUpdate;

    dsSummary.PivotGrid:=PivotGrid1;
    PG:=PivotGrid1;

    dsSummary.CreateData;

    try
      ViewEx.ClearItems;
      ViewEx.DataController.CreateAllItems;
      CreateFooterSummary(ViewEx,PG);
    finally
      ViewEx.EndUpdate;
    end;
  end;
  fmExcelList.ShowModal;
  fmExcelList.dsSummary.PivotGrid:=nil;
  fmExcelList.Release;

end;

end.
