object fmCashList: TfmCashList
  Left = 302
  Top = 363
  Width = 742
  Height = 408
  Caption = #1050#1072#1089#1089#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridCashList: TcxGrid
    Left = 156
    Top = 12
    Width = 549
    Height = 333
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewCashList: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquCashList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewCashListNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088
        DataBinding.FieldName = 'Number'
      end
      object ViewCashListName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
      end
      object ViewCashListLoad: TcxGridDBColumn
        Caption = #1043#1088#1091#1079#1080#1090#1100
        DataBinding.FieldName = 'Load'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmMC.ImageList1
        Properties.Items = <
          item
            Description = #1044#1072
            ImageIndex = 18
            Value = 1
          end
          item
            Description = #1053#1077#1090
            Value = 0
          end>
      end
      object ViewCashListTake: TcxGridDBColumn
        Caption = #1057#1085#1080#1084#1072#1090#1100
        DataBinding.FieldName = 'Take'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmMC.ImageList1
        Properties.Items = <
          item
            Description = #1044#1072
            ImageIndex = 18
            Value = 1
          end
          item
            Description = #1053#1077#1090
            Value = 0
          end>
      end
      object ViewCashListRezerv: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'Rezerv'
      end
      object ViewCashListPATH: TcxGridDBColumn
        Caption = #1055#1091#1090#1100
        DataBinding.FieldName = 'PATH'
        Options.Editing = False
        Width = 261
      end
    end
    object LevelCashList: TcxGridLevel
      GridView = ViewCashList
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 355
    Width = 734
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 141
    Height = 355
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxLabel1: TcxLabel
      Left = 12
      Top = 20
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100'    Ctrl+Ins'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clGreen
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel2: TcxLabel
      Left = 12
      Top = 48
      Cursor = crHandPoint
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100'           F4'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 12
      Top = 104
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100'      Ctrl+Del'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel4: TcxLabel
      Left = 12
      Top = 72
      Cursor = crHandPoint
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088'           F3'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object Label10: TcxLabel
      Left = 16
      Top = 160
      Cursor = crHandPoint
      Caption = #1042#1099#1093#1086#1076'  F10'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.TextColor = clBlue
      StyleHot.BorderStyle = ebsNone
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.TextColor = clBlue
      Properties.Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC6BEBDA5B69C84AE8484AA8494A694B5AEA5FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5C3AD5AAE5A29AE2921
        BA3129C33931BE4A52B26394A694FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB5C3B5429E3110A20821B2218CCB9494D39C31CB5229CB4A42BA5294A6
        94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6C7C663A25A109200189E0829AE29AD
        C7ADB5CFBD39CB5229C74A21C73952AE5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        BDC3B5318E18219208299E1839AE31A5BAA5ADC3B542C75231C74A29C73929BA
        3194A68CFFFFFFFFFFFFFFFFFFFFFFFFBDC3B55AA2429CC794C6D7C6BDCBB5BD
        BEBDB5BAB5ADBEADB5C7B58CCB9421BA217BAA7BFFFFFFFFFFFFFFFFFFFFFFFF
        C6C7BD9CC38CD6E3CEDEE7DEC6D7C6CECFCEBDC3BDA5BAA5A5BEA584C38418B6
        1884AE7BFFFFFFFFFFFFFFFFFFFFFFFFC6C7C6ADCBA5A5C7948CBE7B73B663D6
        DFCEC6D7C64AAE3939AE2931B22931AE29A5B69CFFFFFFFFFFFFFFFFFFFFFFFF
        D6CBCEBDCBBDADCFA59CC7949CC78CEFF3EFE7EFE773BE635AB64A4AB23973B6
        6BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C7C6BDCFBDB5CFA5ADCB9CDE
        EBD6D6E7CE84C3736BBA5A73B66BBDC7B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC6C7C6C6CBBDB5CFADADCFA59CC79494BE849CBE94C6CBBDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6CFCEC6C7C6C6
        CBC6C6CBBDC6CBC6D6CFCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Properties.Orientation = cxoLeftTop
      Transparent = True
      OnClick = Label10Click
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 292
    Top = 36
  end
  object amCashList: TActionManager
    Left = 280
    Top = 148
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acEditCash: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1089#1089#1091
      ShortCut = 115
      OnExecute = acEditCashExecute
    end
  end
end
