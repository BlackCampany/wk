object Form1: TForm1
  Left = 414
  Top = 167
  Width = 485
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 12
    Top = 64
    Width = 449
    Height = 361
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object cxDateEdit1: TcxDateEdit
    Left = 40
    Top = 16
    TabOrder = 1
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 192
    Top = 16
    TabOrder = 2
    Width = 121
  end
  object cxButton1: TcxButton
    Left = 360
    Top = 12
    Width = 101
    Height = 25
    Caption = #1042#1087#1077#1088#1077#1076
    TabOrder = 3
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object PvSQL: TPvSqlDatabase
    AliasName = 'MCRYSTAL'
    DatabaseName = 'PSQL'
    SessionName = 'PvSqlDefault'
    Left = 36
    Top = 92
  end
  object quCq: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.' +
        'Ck_Card, '
      'SUM(cq.Quant*cq.Price-cq.Summa)as CorSum'
      ', sum(cq.Summa) as RSum '
      ', SUM(if(gd.NDS=10,(cq.Quant*cq.Price-cq.Summa),0))as CorSum10'
      ', sum(if(gd.NDS=10,cq.Summa,0))as RSum10'
      ', SUM(if(gd.NDS=18,(cq.Quant*cq.Price-cq.Summa),0))as CorSum18'
      ', sum(if(gd.NDS=18,cq.Summa,0))as RSum18'
      ''
      'from "cq201001" cq'
      'left join Goods gd on gd.ID=cq.Code'
      'where cq.DateOperation=:IDATE'
      
        'group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, c' +
        'q.Ck_Card'
      'order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card')
    Params = <
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 88
    Top = 92
    object quCqDateOperation: TDateField
      FieldName = 'DateOperation'
    end
    object quCqGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quCqCassir: TStringField
      FieldName = 'Cassir'
      Size = 10
    end
    object quCqCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quCqCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quCqCorSum: TFloatField
      FieldName = 'CorSum'
    end
    object quCqRSum: TFloatField
      FieldName = 'RSum'
    end
    object quCqCorSum10: TFloatField
      FieldName = 'CorSum10'
    end
    object quCqRSum10: TFloatField
      FieldName = 'RSum10'
    end
    object quCqCorSum18: TFloatField
      FieldName = 'CorSum18'
    end
    object quCqRSum18: TFloatField
      FieldName = 'RSum18'
    end
  end
  object quD: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'delete from "SaleByDepart" where "DateSale"='#39'2009-12-30'#39)
    Params = <>
    Left = 36
    Top = 160
  end
  object quA: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '')
    Params = <>
    Left = 88
    Top = 160
  end
end
