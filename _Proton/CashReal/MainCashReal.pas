unit MainCashReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  DB, pvtables, sqldataset, pvsqltables;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxButton1: TcxButton;
    PvSQL: TPvSqlDatabase;
    quCq: TPvQuery;
    quD: TPvQuery;
    quCqDateOperation: TDateField;
    quCqGrCode: TSmallintField;
    quCqCassir: TStringField;
    quCqCash_Code: TIntegerField;
    quCqCk_Card: TIntegerField;
    quCqCorSum: TFloatField;
    quCqRSum: TFloatField;
    quCqCorSum10: TFloatField;
    quCqRSum10: TFloatField;
    quCqCorSum18: TFloatField;
    quCqRSum18: TFloatField;
    quA: TPvQuery;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Un1;

{$R *.dfm}

procedure TForm1.cxButton1Click(Sender: TObject);
var iDate:Integer;
begin
  Memo1.Clear;
  Memo1.Lines.Add('������');
  Memo1.Lines.Add(' �������� ����.');

  try
    PvSQL.Connected:=False;
//      PvSQL.AliasName:=CommonSet.PathCryst;
    PvSQL.Connected:=True;
    if PvSQL.Connected=False then Memo1.Lines.Add('�� ������� ������������ � ���� ������.');
  except
    Memo1.Lines.Add('������ ��� ����������� � ���� ������.');
    exit;
  end;

  Memo1.Lines.Add(' ���� ��.');

  for iDate:=Trunc(cxDateEdit1.Date) to Trunc(cxDateEdit2.Date) do
  begin
    Memo1.Lines.Add('  ��������� ���� - '+FormatDateTime('dd.mm.yyyy',iDate));
    Memo1.Lines.Add('    �����...');
    quD.SQL.Clear;
    quD.SQL.Add('delete from "SaleByDepart" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iDate)+'''');
    quD.ExecSQL;
    quD.SQL.Clear;
    quD.SQL.Add('delete from "SaleByDepartBN" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iDate)+'''');
    quD.ExecSQL;

    Memo1.Lines.Add('    ������������...');

    quCq.Active:=False;
    quCq.ParamByName('IDATE').AsDate:=iDate;
    quCq.Active:=True;

    Memo1.Lines.Add('    ����������...');
    quCq.First;
    while not quCq.Eof do
    begin
      quA.SQL.Clear;
      if quCqCk_Card.AsInteger=0 then //���
      begin
        quA.SQL.Add('INSERT into "SaleByDepart" values (');
      end else //������
      begin
        quA.SQL.Add('INSERT into "SaleByDepartBN" values (');
      end;
      quA.SQL.Add(''''+ds(iDate)+''','); //DateSale
      quA.SQL.Add(its(quCqCash_Code.AsInteger)+',');  //CassaNumber
      quA.SQL.Add(its(quCqGrCode.AsInteger)+',');//Otdel
      quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat))+',');//Summa
      quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat))+',');//SummaCor
      quA.SQL.Add(its(quCqCassir.AsInteger)+',');//Employee
      quA.SQL.Add(fs(roundVal(quCqRSum10.AsFloat))+',');//SummaNDSx10
      quA.SQL.Add(fs(roundVal(quCqRSum18.AsFloat))+',');//SummaNDSx20
      quA.SQL.Add(fs(roundVal(quCqCorSum10.AsFloat))+',');//CorNDSx10
      quA.SQL.Add(fs(roundVal(quCqCorSum18.AsFloat))+',');//CorNDSx20
      quA.SQL.Add('0,');//SummaNSPx10
      quA.SQL.Add('0,');//SummaNSPx20
      quA.SQL.Add('0,');//SummaOblNSP
      quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum10.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx0
      quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat-quCqCorSum10.AsFloat-quCqCorSum18.AsFloat))+',');//CorNDSx0
      quA.SQL.Add('0,');//SummaNSP0
      quA.SQL.Add('0,');//NDSx10
      quA.SQL.Add('0,');//NDSx20
      quA.SQL.Add('0,');//CorNSPx0
      quA.SQL.Add('0,');//CorNSPx10
      quA.SQL.Add('0,');//CorNSPx20
      quA.SQL.Add('0,');//Rezerv
      quA.SQL.Add('3,');//ShopIndex
      quA.SQL.Add('0,');//RezervSumma
      quA.SQL.Add('0,');//RezervSumNSP
      quA.SQL.Add('0,');//RezervSkid
      quA.SQL.Add('0,');//RezervSkidNSP
      quA.SQL.Add('0,');//SenderSumma
      quA.SQL.Add('0,');//SenderSumNSP
      quA.SQL.Add('0,');//SenderSkid
      quA.SQL.Add('0');//SenderSkidNSP
      quA.SQL.Add(')');
      quA.ExecSQL;

      quCq.Next;
    end;

    Memo1.Lines.Add('  Ok');
  end;
  Memo1.Lines.Add('Ok');
end;

end.
