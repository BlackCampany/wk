object fmAddDoc8: TfmAddDoc8
  Left = 290
  Top = 315
  Width = 962
  Height = 600
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 548
    Width = 954
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 440
    Width = 954
    Height = 108
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 56
      Top = 38
      Width = 153
      Height = 41
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080' '#1074#1099#1081#1090#1080'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 264
      Top = 38
      Width = 137
      Height = 41
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object Gr1: TcxGrid
      Left = 456
      Top = 2
      Width = 496
      Height = 104
      Align = alRight
      TabOrder = 2
      LookAndFeel.Kind = lfOffice11
      object Vi1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = Vi1CellDblClick
        DataController.DataSource = dmP.dsquPost1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object Vi1NameCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'NamePost'
          Width = 154
        end
        object Vi1Name: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'NameDep'
          Width = 106
        end
        object Vi1DateInvoice: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'DateInvoice'
          Width = 65
        end
        object Vi1CenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
          DataBinding.FieldName = 'PriceIn'
          Width = 81
        end
        object Vi1NewCenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'Rprice'
          Width = 80
        end
      end
      object Vi2: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMC.dsquPost7
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object Vi2DateInvoice: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'DateInvoice'
          Width = 60
        end
        object Vi2Kol: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Kol'
          Width = 45
        end
        object Vi2CenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1057#1057
          DataBinding.FieldName = 'CenaTovar'
        end
        object Vi2CenaTovarSpis: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
          DataBinding.FieldName = 'CenaTovarSpis'
        end
        object Vi2NDSProc: TcxGridDBColumn
          Caption = #1053#1044#1057
          DataBinding.FieldName = 'NDSProc'
          Width = 43
        end
        object Vi2NDSSum: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057
          DataBinding.FieldName = 'NDSSum'
        end
        object Vi2Number: TcxGridDBColumn
          Caption = #1053#1086#1084#1077#1088
          DataBinding.FieldName = 'Number'
        end
        object Vi2NameCli: TcxGridDBColumn
          Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
          DataBinding.FieldName = 'NameCli'
          Width = 79
        end
        object Vi2Name: TcxGridDBColumn
          Caption = #1052#1061
          DataBinding.FieldName = 'Name'
          Width = 100
        end
      end
      object Lev1: TcxGridLevel
        GridView = Vi1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 121
    Width = 153
    Height = 319
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 954
    Height = 121
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object Label12: TLabel
      Left = 302
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label3: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label1: TLabel
      Left = 24
      Top = 56
      Width = 70
      Height = 13
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      Transparent = True
    end
    object cxDateEdit1: TcxDateEdit
      Left = 320
      Top = 12
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 137
    end
    object cxTextEdit1: TcxTextEdit
      Left = 136
      Top = 12
      Properties.AutoSelect = False
      Properties.MaxLength = 10
      Properties.ReadOnly = True
      TabOrder = 1
      Text = 'cxTextEdit1'
      Width = 137
    end
    object cxButton8: TcxButton
      Left = 512
      Top = 12
      Width = 45
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton8Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxTextEdit2: TcxTextEdit
      Left = 136
      Top = 52
      Properties.MaxLength = 100
      Properties.ReadOnly = True
      TabOrder = 3
      Text = 'cxTextEdit2'
      Width = 321
    end
  end
  object Panel4: TPanel
    Left = 153
    Top = 121
    Width = 801
    Height = 319
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 4
    object Edit1: TEdit
      Left = 176
      Top = 260
      Width = 113
      Height = 21
      TabOrder = 0
    end
    object GridDoc8: TcxGrid
      Left = 2
      Top = 2
      Width = 797
      Height = 295
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      object ViewDoc8: TcxGridDBTableView
        DragMode = dmAutomatic
        NavigatorButtons.ConfirmDelete = False
        OnCellClick = ViewDoc8CellClick
        OnCustomDrawCell = ViewDoc8CustomDrawCell
        OnEditing = ViewDoc8Editing
        OnEditKeyDown = ViewDoc8EditKeyDown
        OnSelectionChanged = ViewDoc8SelectionChanged
        DataController.DataSource = dstaSpecVoz
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Styles.Footer = dmMC.cxStyle5
        object ViewDoc8DOCDATE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          DataBinding.FieldName = 'DOCDATE'
          Visible = False
        end
        object ViewDoc8Number: TcxGridDBColumn
          Caption = #8470
          DataBinding.FieldName = 'number'
          Options.Editing = False
          Width = 43
        end
        object ViewDoc8Depart: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1086#1090#1076'.'
          DataBinding.FieldName = 'Depart'
          Options.Editing = False
          Width = 72
        end
        object ViewDoc8DepartName: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'DepartName'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'Name'
          Properties.ListColumns = <
            item
              FieldName = 'Name'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dmP.dsquDepVoz
          Properties.MaxLength = 30
          Options.Editing = False
          Width = 86
        end
        object ViewDoc8Code: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1090#1086#1074'.'
          DataBinding.FieldName = 'Code'
          Options.Editing = False
          Width = 67
        end
        object ViewDoc8NameCode: TcxGridDBColumn
          Caption = #1058#1086#1074#1072#1088
          DataBinding.FieldName = 'NameCode'
          Options.Editing = False
          Width = 105
        end
        object ViewDoc8idcli: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1087#1086#1089#1090'.'
          DataBinding.FieldName = 'idcli'
          Options.Editing = False
          Width = 55
        end
        object ViewDoc8NameCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'namecli'
          Options.Editing = False
          Width = 110
        end
        object ViewDoc8Quant: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1076#1083#1103' '#1074#1086#1079#1074'.'
          DataBinding.FieldName = 'quant'
          Options.Editing = False
          Width = 100
        end
        object ViewDoc8QuantOut: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1074#1077#1088#1085#1091#1083#1080
          DataBinding.FieldName = 'quantout'
          Options.Editing = False
          Width = 103
        end
        object ViewDoc8QRem: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
          DataBinding.FieldName = 'qrem'
          Options.Editing = False
          Width = 92
        end
        object ViewDoc8QALLREM: TcxGridDBColumn
          Caption = #1054#1073#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'QAllRem'
          Options.Editing = False
          Styles.Content = dmMC.cxStyle23
          Width = 85
        end
        object ViewDoc8PriceIn0: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
          DataBinding.FieldName = 'PriceIn0'
          Options.Editing = False
          Width = 119
        end
        object ViewDoc8PriceIn: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
          DataBinding.FieldName = 'PriceIn'
          Options.Editing = False
          Width = 118
        end
        object ViewDoc8RPrice: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'. '#1089' '#1053#1044#1057
          DataBinding.FieldName = 'RPrice'
          Options.Editing = False
          Width = 100
        end
        object ViewDoc8RSumIn0: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
          DataBinding.FieldName = 'RSumIn0'
          Options.Editing = False
          Width = 112
        end
        object ViewDoc8RSumIn: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
          DataBinding.FieldName = 'RSumIn'
          Options.Editing = False
          Width = 113
        end
        object ViewDoc8RsumNDS: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076'. '#1089' '#1053#1044#1057
          DataBinding.FieldName = 'RSumNDS'
          Options.Editing = False
          Width = 139
        end
        object ViewDoc8Status: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'Status'
          Options.Editing = False
          Width = 77
        end
        object ViewDoc8DateVoz: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088'.'
          DataBinding.FieldName = 'datevoz'
          PropertiesClassName = 'TcxDateEditProperties'
          Options.Editing = False
        end
        object ViewDoc8Reason: TcxGridDBColumn
          Caption = #1055#1088#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'Reason'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'id'
          Properties.ListColumns = <
            item
              FieldName = 'Reason'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dmP.dsquReasV
        end
      end
      object VD8: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object VD8DOCDATE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        end
        object VD8Number: TcxGridDBColumn
          Caption = #8470
        end
        object VD8Depart: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
        end
        object VD8Code: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1090#1086#1074'.'
        end
        object VD8NameCode: TcxGridDBColumn
          Caption = #1058#1086#1074#1072#1088
        end
        object VD8idcli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        end
        object VD8Quant: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
        end
        object VD8QuantOut: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1074#1077#1088#1085#1091#1083#1080
        end
        object VD8QRem: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090#1072#1083#1086#1089#1100
        end
        object VD8QALLREM: TcxGridDBColumn
          Caption = #1054#1073#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
        end
        object VD8PriceIn0: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        end
        object VD8PriceIn: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
        end
        object VD8RPrice: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'. '#1089' '#1053#1044#1057
        end
        object VD8RSumIn0: TcxGridDBColumn
        end
        object VD8RSumIn: TcxGridDBColumn
        end
        object VD8RsumNDS: TcxGridDBColumn
        end
        object VD8Status: TcxGridDBColumn
        end
        object VD8DateVoz: TcxGridDBColumn
        end
        object VD8Reason: TcxGridDBColumn
        end
      end
      object LevelDoc8: TcxGridLevel
        GridView = ViewDoc8
      end
      object LVD8: TcxGridLevel
      end
    end
    object PBar1: TcxProgressBar
      Left = 2
      Top = 296
      Align = alBottom
      ParentColor = False
      Position = 100.000000000000000000
      Properties.BarBevelOuter = cxbvRaised
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = clBlue
      Properties.EndColor = 16745090
      Properties.PeakValue = 100.000000000000000000
      Style.Color = clWhite
      TabOrder = 2
      Visible = False
      Width = 797
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 336
    Top = 184
  end
  object dstaSpecVoz: TDataSource
    DataSet = taSpecVoz
    Left = 256
    Top = 240
  end
  object amDocVoz: TActionManager
    Left = 184
    Top = 188
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
    object acReadBar: TAction
      Caption = 'acReadBar'
      ShortCut = 16449
      OnExecute = acReadBarExecute
    end
    object acReadBar1: TAction
      Caption = 'acReadBar1'
      ShortCut = 16450
      OnExecute = acReadBar1Execute
    end
    object acPostTov: TAction
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32884
      SecondaryShortCuts.Strings = (
        'Ctrl+F5')
      OnExecute = acPostTovExecute
    end
  end
  object taSpecVoz: TdxMemData
    Indexes = <>
    SortOptions = []
    OnCalcFields = taSpecVozCalcFields
    Left = 256
    Top = 184
    object taSpecVoznumber: TIntegerField
      FieldName = 'number'
    end
    object taSpecVozID: TIntegerField
      FieldName = 'ID'
    end
    object taSpecVozDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object taSpecVozCode: TIntegerField
      FieldName = 'Code'
    end
    object taSpecVozNameCode: TStringField
      DisplayWidth = 40
      FieldName = 'NameCode'
      Size = 100
    end
    object taSpecVozDepart: TIntegerField
      FieldName = 'Depart'
    end
    object taSpecVozDepartName: TStringField
      FieldName = 'DepartName'
      OnChange = taSpecVozDepartNameChange
    end
    object taSpecVozidcli: TIntegerField
      FieldName = 'idcli'
    end
    object taSpecVoznamecli: TStringField
      DisplayWidth = 50
      FieldName = 'namecli'
      Size = 100
    end
    object taSpecVozquant: TFloatField
      DisplayWidth = 15
      FieldName = 'quant'
      OnChange = taSpecVozquantChange
      DisplayFormat = '0.000'
    end
    object taSpecVozquantout: TFloatField
      DisplayWidth = 15
      FieldName = 'quantout'
      DisplayFormat = '0.000'
    end
    object taSpecVozqrem: TFloatField
      DisplayWidth = 15
      FieldName = 'qrem'
      DisplayFormat = '0.000'
    end
    object taSpecVozQAllRem: TFloatField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'QAllRem'
      DisplayFormat = '0.000'
      Calculated = True
    end
    object taSpecVozPriceIn0: TFloatField
      DisplayWidth = 15
      FieldName = 'PriceIn0'
      OnChange = taSpecVozPriceIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecVozPriceIn: TFloatField
      DisplayWidth = 15
      FieldName = 'PriceIn'
      OnChange = taSpecVozPriceInChange
      DisplayFormat = '0.00'
    end
    object taSpecVozRPrice: TFloatField
      DisplayWidth = 15
      FieldName = 'RPrice'
      OnChange = taSpecVozRPriceChange
      DisplayFormat = '0.00'
    end
    object taSpecVozRSumIn0: TFloatField
      DisplayWidth = 15
      FieldName = 'RSumIn0'
      DisplayFormat = '0.00'
    end
    object taSpecVozRSumIn: TFloatField
      DisplayWidth = 15
      FieldName = 'RSumIn'
      DisplayFormat = '0.00'
    end
    object taSpecVozRSumNDS: TFloatField
      DisplayWidth = 15
      FieldName = 'RSumNDS'
      DisplayFormat = '0.00'
    end
    object taSpecVozdatevoz: TIntegerField
      FieldName = 'datevoz'
    end
    object taSpecVozStatus: TStringField
      FieldName = 'Status'
    end
    object taSpecVozReason: TIntegerField
      FieldName = 'Reason'
    end
  end
end
