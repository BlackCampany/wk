unit SelDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ExtCtrls, DB, pvtables, sqldataset, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox;

type
  TfmSelDep = class(TForm)
    Label4: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    quDeps: TPvQuery;
    quDepsID: TSmallintField;
    quDepsName: TStringField;
    dsquDeps: TDataSource;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelDep: TfmSelDep;

implementation

{$R *.dfm}

end.
