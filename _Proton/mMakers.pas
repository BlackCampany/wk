unit mMakers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmMakers = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewMakers: TcxGridDBTableView;
    LevelMakers: TcxGridLevel;
    GridMakers: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    Timer1: TTimer;
    amMakers: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    ViewMakersID: TcxGridDBColumn;
    ViewMakersNAMEM: TcxGridDBColumn;
    ViewMakersINNM: TcxGridDBColumn;
    ViewMakersKPPM: TcxGridDBColumn;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure ViewMakersDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMakers: TfmMakers;
  bClearMakers:Boolean = False;

implementation

uses Un1, MDB, AddSingle, MT, AddMaker, u2fdk, SetCardsParams, AddDoc1,
  SetCardsParams1;

{$R *.dfm}

procedure TfmMakers.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmMakers.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmMakers.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmMakers.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmMakers.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.FormCreate(Sender: TObject);
begin
  GridMakers.Align:=AlClient;
end;

procedure TfmMakers.Timer1Timer(Sender: TObject);
begin
  if bClearMakers=True then begin StatusBar1.Panels[0].Text:=''; bClearMakers:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMakers:=True;
end;

procedure TfmMakers.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMakers.acDelExecute(Sender: TObject);
Var iCode:Integer;
begin            //�������
  with dmMC do
  with dmMT do
  begin
    if quMakers.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quMakersNAMEM.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iCode:=quMakersID.AsInteger;
        try
          ViewMakers.BeginUpdate;

          if ptMakers.Active=False then ptMakers.Active:=True;

          if ptMakers.FindKey([iCode]) then ptMakers.Delete;

          quMakers.Active:=False;
          quMakers.Active:=True;
          quMakers.First;

        finally
          ViewMakers.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmMakers.acEditExecute(Sender: TObject);
Var iCode:Integer;
begin                  //�������������
  with dmMC do
  with dmMT do
  begin
    if quMakers.RecordCount>0 then
    begin
      iCode:=quMakersID.AsInteger;
      fmAddMaker:=tfmAddMaker.Create(Application);
      try
        fmAddMaker.cxTextEdit1.Text:=quMakersNAMEM.AsString;
        fmAddMaker.cxTextEdit2.Text:=quMakersINNM.AsString;
        fmAddMaker.cxTextEdit3.Text:=quMakersKPPM.AsString;
        fmAddMaker.ShowModal;

        if fmAddMaker.ModalResult=mrOk then
        begin
          try
            ViewMakers.BeginUpdate;

            if ptMakers.Active=False then ptMakers.Active:=True;

            if ptMakers.FindKey([iCode]) then
            begin
              ptMakers.Edit;
              ptMakersNAMEM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit1.Text);
              ptMakersINNM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit2.Text);
              ptMakersKPPM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit3.Text);
              ptMakers.Post;
            end;
            quMakers.Active:=False;
            quMakers.Active:=True;

            quMakers.Locate('ID',iCode,[]);
          finally
            ViewMakers.EndUpdate;
          end;
        end;
      finally
        fmAddMaker.Release;
      end;
    end;
  end;
end;

procedure TfmMakers.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin                //��������
  with dmMC do
  with dmMT do
  begin
    fmAddMaker:=tfmAddMaker.Create(Application);
    try
      fmAddMaker.cxTextEdit1.Text:='';
      fmAddMaker.cxTextEdit2.Text:='';
      fmAddMaker.cxTextEdit3.Text:='';
      fmAddMaker.ShowModal;
      if fmAddMaker.ModalResult=mrOk then
      begin
        try
          ViewMakers.BeginUpdate;

          iMax:=prMax('Maker')+1;
          if ptMakers.Active=False then ptMakers.Active:=True;

          ptMakers.Append;
          ptMakersID.AsInteger:=iMax;
          ptMakersNAMEM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit1.Text);
          ptMakersINNM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit2.Text);
          ptMakersKPPM.AsString:=AnsiToOemConvert(fmAddMaker.cxTextEdit3.Text);
          ptMakers.Post;

          quMakers.Active:=False;
          quMakers.Active:=True;

          quMakers.Locate('ID',iMax,[]);
        finally
          ViewMakers.EndUpdate;
        end;
      end;
    finally
      fmAddMaker.Release;
    end;
  end;
end;

procedure TfmMakers.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmMakers.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmMakers.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmMakers.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmMakers.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmMakers.ViewMakersDblClick(Sender: TObject);
begin
  with dmMC do
  with dmMT do
  begin
    if quMakers.RecordCount>0 then
    begin
      if fmSetCardsParams.Showing then
      begin
        fmSetCardsParams.cxButtonEdit1.Tag:=quMakersID.AsInteger;
        fmSetCardsParams.cxButtonEdit1.Text:=quMakersNAMEM.AsString;
        Close;
      end;
      if fmSetCardsParams1.Showing then
      begin
        fmSetCardsParams1.cxButtonEdit1.Tag:=quMakersID.AsInteger;
        fmSetCardsParams1.cxButtonEdit1.Text:=quMakersNAMEM.AsString;
        Close;
      end;
      if fmAddDoc1.Showing then
      begin
        with fmAddDoc1 do
        begin
          taSpecIn.Edit;
          taSpecInSumVesTara.AsFloat:=quMakersID.AsInteger;
          taSpecInsMaker.AsString:=quMakersNAMEM.AsString;
          taSpecIn.Post;
        end;
        Close;
      end;
    end;
  end;
end;

end.
