object fmAddDoc4: TfmAddDoc4
  Left = 411
  Top = 69
  Width = 1098
  Height = 776
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' : '#1074#1085'. '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit10: TcxDateEdit
    Left = 824
    Top = 176
    TabStop = False
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    TabOrder = 7
    Width = 177
  end
  object cxTextEdit10: TcxTextEdit
    Left = 824
    Top = 148
    TabStop = False
    Properties.MaxLength = 15
    Properties.ReadOnly = True
    TabOrder = 5
    Text = #1055#1077#1088#1074#1086#1085#1072#1095#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1080' '#1082#1086#1076
    Width = 177
  end
  object Edit1: TEdit
    Left = 656
    Top = 216
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Edit1'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1090
    Height = 93
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 44
      Width = 39
      Height = 13
      Caption = #1054#1090' '#1082#1086#1075#1086
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label2: TLabel
      Left = 24
      Top = 68
      Width = 26
      Height = 13
      Caption = #1050#1086#1084#1091
      Transparent = True
    end
    object Label3: TLabel
      Left = 412
      Top = 16
      Width = 48
      Height = 16
      Caption = 'Label3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 10
      TabOrder = 0
      Text = 'cxTextEdit1'
      OnKeyDown = cxTextEdit1KeyDown
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 40
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'Name'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmMC.dsquDeparts
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 217
    end
    object cxButton3: TcxButton
      Left = 620
      Top = 8
      Width = 105
      Height = 45
      Action = acPrintDoc
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 3
      Glyph.Data = {
        160B0000424D160B00000000000036040000280000002C000000280000000100
        080000000000E006000000000000000000000001000000000000E3E1DF00CBC8
        C300FDFDFD00ECEAE500E2DFDD00FAF9F600F5F5F50056B62700A39971008279
        5500736A4A00E5E4E200BBB9B500D3CDB900BAB4AB00BBB39500C0B89C00C8C2
        A9001961EF00C4BDA300F2F1EC00C6752200F8F8F800F0EFED00F7D4AB001649
        A900E8A76600B6AD8C0097928E00F4F4F300F1F1F000FCC88700A2E9AC0069BD
        9F00DCD9D600D0CCC800EBEAEA00F3B97900DEDCD800FEE2B600C1BBB300EEED
        EC00A49D8D00B1ADA800ADA9A300AAA49F008C878400B3A8990089845A00D3D0
        CC00D8D6D300D68F4A0075716F00FFF3D400CAC4BE00D5D3D000DAD5C300FDFD
        FA00E1B68900CA966D00DDD9CA00968A6200C4C1BD00E8E6E400E0DDCF00AFA5
        8400B4590400EEECE900A9A07A00948E89009A947C009C989600BD670C00CFCA
        B500ECEAE800FFD79D00FFFBF4009D936700FBFBFA00D4BDA900FFEAC400DAE9
        9800CCC6AF00F4DBD100EEB37300F3F3F100FFFFED00DAD4CF00E7E4DA00E8C7
        9D00E3E0D400D1823800A3857600918B6E00BA713600FEF4E600E29C5900D7D2
        C0008F531B00D6CFBD00F7F6F500FEEDE00082C8CE00FFD08E00837F7C006861
        4300F9F9F900FFFBE000EBE9DF0097B88B00E2A468006D67C500E7E5F600C5C3
        E300B394A00099BF2C00FFF4EA00DECFD800EDAE6E00EFBCA600FDFAFA00F7C0
        80004537EA00AECD4C00AC7F5100E9E9E900FFFFFF00FAFAFA00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242622242424242424242424242424242424242424242424242424242424
        2424242424242424242424242422372301363624242424242424242424242424
        24242424242424242424242424242424242424242424243231012C470E0E0E28
        2824242424242424242424242424242424242424242424242424242426573123
        242301360E2E682E682E2E2E2C0E282424242424242424242424242424242424
        242424242424225723363E2828282B2E6847524F1C1C1C45681C2B0E24242424
        24242424242424242424242424242424223723284534342A2C452E1C3E4C7E7E
        502F1C1C1C45682D282424242424242424242424242424242424223723284534
        2E2D47682E1C2A537E7E7E207E560D2A1C1C1C1C0E3624242424242424242424
        242424242657232845342C240B042D1C453B537E4C7E212021207E35101C1C2E
        2C36572424242424242424242424030437282E683E7E7E7E06042C3D6E7E653A
        4F19211D200707517E272F1C1C23223F242424242424242429290B012E2E2339
        7E7E7E7E1E042B4F56335E5C19192166660707077B7E56592E0C004324242424
        2424242455001C45227F7E7E7E7E7E7E17040C3B424219191212121207070773
        51397E4F2B004A2424242424242424241E472D7E7F4E7F7F7F7F7F7F430B3E2F
        4262191912121212757E7E7E7E5F0E3100432424242424242424242464687F06
        061D1D1D1D1D1D55433F23282F5E62191919127277657E7E2C2C3E0003242424
        2424242424242424641C1E1E1717171717171717294A32013E0C444862625E3B
        747E531C0E363224242424242424242424242424642B4A4A4A4A4A4A4A4A4A4A
        1717003231010C0E7C423A7E185C1C340E222424242424242424242424242424
        642B3F3F3F0B0B0B0B0B0B4A5555173F2637013E0C2C497C0A341C340E242424
        242424242424242424242424642B00000000000404040455647E7E385A003231
        360C0E2C2E341C3428242424242424242424242424242424162B040404040404
        04044A02027E59424233402637013E0C2B2C4734282424242424242424242424
        242424246A2E040404040404043F7E7E7E5F15484242423A003231360C0E2C34
        282424242424242424242424242424247F2E370404040404067E7E7E7E6E5B15
        4848424215612637013E0C3428242424242424242424242424242424163E0C29
        1717787E7E7E3049181A60335B15484242423B04322336453624242424242424
        2424242424242424241647327E7E7E706F3F6D3A1F257660335B154848424248
        4F26376801242424242424242424242424242424247F3E3E0C370B717A722579
        791F1F541A60335B15484842154A014731242424242424242424242424242424
        244E2B372331106E6E1A765425791F1F797660335B1515483A05470E22242424
        24242424242424242424242424022B0157274B1F76601A76762579791F1F251A
        60335B5B7E5568570024242424242424242424242424242424241E0E0D7E2767
        671F1A6E1A765425791F1F79541A605F7E473604242424242424242424242424
        242424242424241D0D7E2767676767791A1A1A765425791F1F795F7E232C0424
        2424242424242424242424242424242424242464637E504B4B4B676767251A1A
        76542579675F7E012B0024242424242424242424242424242424242424242416
        637E35505027274B4B4B67546E1A541843432B0E3F2424242424242424242424
        24242424242424242424247F637E6B566B3535505027271B344122040C472229
        242424242424242424242424242424242424242424242439287E7E397E395656
        6B35352F2E2C2D2B322924242424242424242424242424242424242424242424
        24242402320F567E4C7E7E7E7E394C0E45365700242424242424242424242424
        24242424242424242424242424242424024E01137E7E397E7E7E7E0C2D312424
        242424242424242424242424242424242424242424242424242424242424391E
        0E3C7E7E397E7E0C0E2224242424242424242424242424242424242424242424
        242424242424242424242424243F2F657E39390C3E0B24242424242424242424
        2424242424242424242424242424242424242424242424242424243113397E5C
        2217242424242424242424242424242424242424242424242424242424242424
        2424242424242424242828221D64242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424}
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox1: TcxCheckBox
      Left = 620
      Top = 64
      Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
      TabOrder = 4
      Width = 193
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 112
      Top = 64
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'Name'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmMC.dsquDeparts1
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 217
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 723
    Width = 1090
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 582
    Width = 1090
    Height = 141
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 20
      Top = 12
      Width = 121
      Height = 37
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 152
      Top = 12
      Width = 137
      Height = 37
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 308
      Top = 12
      Width = 45
      Height = 37
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton8Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object GrPVn: TcxGrid
      Left = 475
      Top = 2
      Width = 613
      Height = 137
      Align = alRight
      TabOrder = 3
      LookAndFeel.Kind = lfOffice11
      object ViewPVn: TcxGridDBTableView
        OnDblClick = ViewPVnDblClick
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dstePartIn1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfAlwaysVisible
        Styles.Background = dmMC.cxStyle18
        object ViewPVnARTICUL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1091#1083
          DataBinding.FieldName = 'ARTICUL'
          Width = 45
        end
        object ViewPVnIDATE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'IDATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Styles.Content = dmMC.cxStyle1
        end
        object ViewPVnQPART: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
          DataBinding.FieldName = 'QPART'
          Styles.Content = dmMC.cxStyle1
        end
        object ViewPVnQREMN: TcxGridDBColumn
          Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
          DataBinding.FieldName = 'QREMN'
          Styles.Content = dmMC.cxStyle1
        end
        object ViewPVnPRICEIN0: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' '#1073#1077#1079' '#1053#1044#1057
          DataBinding.FieldName = 'PRICEIN0'
          Styles.Content = dmMC.cxStyle13
        end
        object ViewPVnPRICEIN: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
          DataBinding.FieldName = 'PRICEIN'
          Styles.Content = dmMC.cxStyle1
        end
        object ViewPVnSSTORE: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'SSTORE'
          Width = 60
        end
        object ViewPVnSCLI: TcxGridDBColumn
          Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
          DataBinding.FieldName = 'SCLI'
          Width = 80
        end
        object ViewPVnID: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1055#1055
          DataBinding.FieldName = 'ID'
        end
        object ViewPVnIDSTORE: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
          DataBinding.FieldName = 'IDSTORE'
          Visible = False
        end
        object ViewPVnIDDOC: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1076#1086#1082'.'
          DataBinding.FieldName = 'IDDOC'
        end
        object ViewPVnDTYPE: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1055#1055
          DataBinding.FieldName = 'DTYPE'
        end
        object ViewPVnPRICEOUT: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'PRICEOUT'
        end
        object ViewPVnSINNCLI: TcxGridDBColumn
          Caption = #1048#1053#1053
          DataBinding.FieldName = 'SINNCLI'
        end
      end
      object LePVn: TcxGridLevel
        GridView = ViewPVn
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 93
    Width = 153
    Height = 489
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 2
      Top = 69
      Width = 149
      Height = 88
      Align = alTop
      Caption = #1055#1088#1080#1093#1086#1076
      TabOrder = 4
      object cxLabel10: TcxLabel
        Left = 12
        Top = 20
        Cursor = crHandPoint
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' '
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = 4227072
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsUnderline]
        Style.IsFontAssigned = True
        Properties.Orientation = cxoLeftTop
        Properties.PenWidth = 3
        Transparent = True
        OnClick = cxLabel10Click
      end
      object cxLabel11: TcxLabel
        Left = 12
        Top = 36
        Cursor = crHandPoint
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = 4227072
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsUnderline]
        Style.IsFontAssigned = True
        Properties.Orientation = cxoLeftTop
        Properties.PenWidth = 3
        Transparent = True
        OnClick = cxLabel11Click
      end
    end
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 12
      Top = 180
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel6: TcxLabel
      Left = 12
      Top = 232
      Cursor = crHandPoint
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1094#1077#1085#1099' Ctrl+F3'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object GroupBox2: TGroupBox
      Left = 2
      Top = 2
      Width = 149
      Height = 67
      Align = alTop
      Caption = ' '#1056#1072#1089#1093#1086#1076' '
      TabOrder = 5
    end
    object cxLabel5: TcxLabel
      Left = 12
      Top = 20
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel7: TcxLabel
      Left = 12
      Top = 36
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel4: TcxLabel
      Left = 12
      Top = 204
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
  end
  object GridDoc4: TcxGrid
    Left = 164
    Top = 100
    Width = 909
    Height = 477
    PopupMenu = PopupMenu1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewDoc4: TcxGridDBTableView
      OnDblClick = ViewDoc4DblClick
      OnDragDrop = ViewDoc4DragDrop
      OnDragOver = ViewDoc4DragOver
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewDoc4CustomDrawCell
      OnEditing = ViewDoc4Editing
      OnEditKeyDown = ViewDoc4EditKeyDown
      OnEditKeyPress = ViewDoc4EditKeyPress
      OnSelectionChanged = ViewDoc4SelectionChanged
      DataController.DataSource = dstaSpecVn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'KolMest'
          Column = ViewDoc4QuantM
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'KolWithMest'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Kol'
          Column = ViewDoc4Kol
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumCena'
          Column = ViewDoc4SumCena
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumNewCena'
          Column = ViewDoc4SumNewCena
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRazn'
          Column = ViewDoc4SumRazn
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumCena0'
          Column = ViewDoc4SumCena0
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.Indicator = True
      Styles.Selection = dmMC.cxStyle5
      Styles.Footer = dmMC.cxStyle5
      object ViewDoc4Num: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Num'
        Options.Editing = False
        Width = 54
      end
      object ViewDoc4Id: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1086#1079'.'
        DataBinding.FieldName = 'Id'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1056#1072#1089#1093#1086#1076
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1055#1088#1080#1093#1086#1076
            Value = 1
          end>
        Options.Editing = False
      end
      object ViewDoc4CodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
        Styles.Content = dmMC.cxStyle1
        Width = 55
      end
      object ViewDoc4Name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4FullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Options.Editing = False
        Width = 156
      end
      object ViewDoc4CodeEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CodeEdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            ImageIndex = 0
            Value = 2
          end>
        Options.Editing = False
        Width = 57
      end
      object ViewDoc4BarCode: TcxGridDBColumn
        Caption = #1064#1050
        DataBinding.FieldName = 'BarCode'
        Options.Editing = False
        Width = 82
      end
      object ViewDoc4QuantM: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1082' '#1089#1087#1080#1089#1072#1085#1080#1102
        DataBinding.FieldName = 'QuantM'
        Styles.Content = dmMC.cxStyle1
        Width = 79
      end
      object ViewDoc4Kol: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'Kol'
        Styles.Content = dmMC.cxStyle1
        Width = 73
      end
      object ViewDoc4Cena: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'Cena'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4Cena0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'Cena0'
        Visible = False
      end
      object ViewDoc4SumCena: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumCena'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4SumCena0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumCena0'
        Visible = False
      end
      object ViewDoc4NewCena: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'NewCena'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4SumNewCena: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumNewCena'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4SumRazn: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'SumRazn'
        Options.Editing = False
      end
      object ViewDoc4Remn: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Remn'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc4QuantN: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        DataBinding.FieldName = 'QuantN'
        Options.Editing = False
        Width = 94
      end
      object ViewDoc4ProcBrak: TcxGridDBColumn
        Caption = '% '#1073#1088#1072#1082#1072
        DataBinding.FieldName = 'ProcBrak'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle12
      end
    end
    object LevelDoc4: TcxGridLevel
      GridView = ViewDoc4
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 508
    Top = 200
  end
  object taSpecVnNoNeed: TClientDataSet
    Aggregates = <>
    FileName = 'SpecVn.cds'
    Params = <>
    Left = 196
    Top = 272
    object taSpecVnNoNeedNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecVnNoNeedCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpecVnNoNeedName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object taSpecVnNoNeedFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object taSpecVnNoNeedCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object taSpecVnNoNeedBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object taSpecVnNoNeedNDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedKolMest: TFloatField
      FieldName = 'KolMest'
      OnChange = taSpecVnNoNeedKolMestChange
    end
    object taSpecVnNoNeedKolWithMest: TFloatField
      FieldName = 'KolWithMest'
      OnChange = taSpecVnNoNeedKolWithMestChange
    end
    object taSpecVnNoNeedKol: TFloatField
      FieldName = 'Kol'
    end
    object taSpecVnNoNeedCena: TFloatField
      FieldName = 'Cena'
      OnChange = taSpecVnNoNeedCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedNewCena: TFloatField
      FieldName = 'NewCena'
      OnChange = taSpecVnNoNeedNewCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedProc: TFloatField
      FieldName = 'Proc'
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedSumCena: TFloatField
      FieldName = 'SumCena'
      OnChange = taSpecVnNoNeedSumCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedSumNewCena: TFloatField
      FieldName = 'SumNewCena'
      OnChange = taSpecVnNoNeedSumNewCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedSumRazn: TFloatField
      FieldName = 'SumRazn'
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedRemn: TFloatField
      FieldName = 'Remn'
      DisplayFormat = '0.000'
    end
    object taSpecVnNoNeedCenaSS: TFloatField
      FieldName = 'CenaSS'
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedSumCenaSS: TFloatField
      FieldName = 'SumCenaSS'
      DisplayFormat = '0.00'
    end
    object taSpecVnNoNeedId: TIntegerField
      FieldName = 'Id'
    end
  end
  object dstaSpecVn: TDataSource
    DataSet = taSpecVn
    Left = 276
    Top = 252
  end
  object amDocVn: TActionManager
    Left = 196
    Top = 200
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
    object acReadBar: TAction
      Caption = 'acReadBar'
      ShortCut = 16449
      OnExecute = acReadBarExecute
    end
    object acReadBar1: TAction
      Caption = 'acReadBar1'
      ShortCut = 16450
      OnExecute = acReadBar1Execute
    end
    object acSetPrice: TAction
      Caption = 'acSetPrice'
      ShortCut = 16498
      OnExecute = acSetPriceExecute
    end
    object acPrintDoc: TAction
      Caption = 'F7'
      ShortCut = 118
      OnExecute = acPrintDocExecute
    end
    object acPrintTermo: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1101#1090#1080#1082#1077#1090#1086#1082
      ShortCut = 113
      OnExecute = acPrintTermoExecute
    end
    object acPrintCenn: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074
      ShortCut = 116
      OnExecute = acPrintCennExecute
    end
    object acSetRemn: TAction
      Caption = 'acSetRemn'
      ShortCut = 114
      OnExecute = acSetRemnExecute
    end
    object acPostav: TAction
      Caption = 'acPostav'
      ShortCut = 32884
      OnExecute = acPostavExecute
    end
    object acAddPosIn: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1088#1080#1093#1086#1076
      OnExecute = acAddPosInExecute
    end
    object acAddPosOut: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1056#1072#1089#1093#1086#1076
      OnExecute = acAddPosOutExecute
    end
    object acViewPrihod: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1088#1080#1093#1086#1076#1072
      OnExecute = acViewPrihodExecute
    end
  end
  object frRepDVn: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit, pbPageSetup]
    Title = #1055#1077#1095#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    RebuildPrinter = False
    Left = 196
    Top = 348
    ReportForm = {19000000}
  end
  object frtaSpecVn: TfrDBDataSet
    DataSet = taSpecPr
    Left = 260
    Top = 348
  end
  object teSpecIn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 348
    Top = 204
    object teSpecInNum: TIntegerField
      FieldName = 'Num'
    end
    object teSpecInID: TIntegerField
      FieldName = 'ID'
    end
    object teSpecInName: TStringField
      FieldName = 'Name'
      Size = 50
    end
    object teSpecInFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object teSpecInCdeEdIzm: TIntegerField
      FieldName = 'CdeEdIzm'
    end
    object teSpecInBarCode: TStringField
      FieldName = 'BarCode'
    end
    object teSpecInNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object teSpecInKol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.000'
    end
    object teSpecInCena: TFloatField
      FieldName = 'Cena'
      DisplayFormat = '0.00'
    end
    object teSpecInNewCena: TFloatField
      FieldName = 'NewCena'
      DisplayFormat = '0.00'
    end
    object teSpecInCenaSS: TFloatField
      FieldName = 'CenaSS'
      DisplayFormat = '0.00'
    end
    object teSpecInProc: TFloatField
      FieldName = 'Proc'
    end
    object teSpecInSumCena: TFloatField
      FieldName = 'SumCena'
      DisplayFormat = '0.00'
    end
    object teSpecInSumNewCena: TFloatField
      FieldName = 'SumNewCena'
      DisplayFormat = '0.00'
    end
    object teSpecInSumCenaSS: TFloatField
      FieldName = 'SumCenaSS'
      DisplayFormat = '0.00'
    end
    object teSpecInRemn: TFloatField
      FieldName = 'Remn'
    end
    object teSpecInCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
  end
  object dsteSpecIn: TDataSource
    DataSet = teSpecIn
    Left = 348
    Top = 256
  end
  object tePartIn1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 468
    Top = 364
    object tePartIn1ID: TIntegerField
      FieldName = 'ID'
    end
    object tePartIn1IDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object tePartIn1SSTORE: TStringField
      FieldName = 'SSTORE'
      Size = 50
    end
    object tePartIn1IDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object tePartIn1IDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object tePartIn1ARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object tePartIn1DTYPE: TIntegerField
      FieldName = 'DTYPE'
    end
    object tePartIn1QPART: TFloatField
      FieldName = 'QPART'
      DisplayFormat = '0.000'
    end
    object tePartIn1QREMN: TFloatField
      FieldName = 'QREMN'
      DisplayFormat = '0.000'
    end
    object tePartIn1PRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object tePartIn1PRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object tePartIn1SINNCLI: TStringField
      FieldName = 'SINNCLI'
    end
    object tePartIn1SCLI: TStringField
      FieldName = 'SCLI'
      Size = 100
    end
    object tePartIn1PRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
  end
  object dstePartIn1: TDataSource
    DataSet = tePartIn1
    Left = 468
    Top = 420
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 596
    Top = 180
    object N1: TMenuItem
      Action = acAddPosIn
    end
    object N2: TMenuItem
      Action = acAddPosOut
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = acViewPrihod
    end
  end
  object taSpecVn: TdxMemData
    Indexes = <
      item
        FieldName = 'Num'
        SortOptions = []
      end>
    SortOptions = []
    Left = 276
    Top = 196
    object taSpecVnNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecVnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpecVnName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object taSpecVnFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object taSpecVnCodeEdIzm: TIntegerField
      FieldName = 'CodeEdIzm'
    end
    object taSpecVnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taSpecVnNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object taSpecVnKol: TFloatField
      FieldName = 'Kol'
      OnChange = taSpecVnKolChange
      DisplayFormat = '0.000'
    end
    object taSpecVnCena: TFloatField
      FieldName = 'Cena'
      OnChange = taSpecVnCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnNewCena: TFloatField
      FieldName = 'NewCena'
      OnChange = taSpecVnNewCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnProc: TFloatField
      FieldName = 'Proc'
      DisplayFormat = '0.0'
    end
    object taSpecVnSumCena: TFloatField
      FieldName = 'SumCena'
      OnChange = taSpecVnSumCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnSumNewCena: TFloatField
      FieldName = 'SumNewCena'
      OnChange = taSpecVnSumNewCenaChange
      DisplayFormat = '0.00'
    end
    object taSpecVnSumRazn: TFloatField
      FieldName = 'SumRazn'
      DisplayFormat = '0.00'
    end
    object taSpecVnRemn: TFloatField
      FieldName = 'Remn'
      DisplayFormat = '0.000'
    end
    object taSpecVnCenaSS: TFloatField
      FieldName = 'CenaSS'
      DisplayFormat = '0.00'
    end
    object taSpecVnSumCenaSS: TFloatField
      FieldName = 'SumCenaSS'
      DisplayFormat = '0.00'
    end
    object taSpecVnId: TIntegerField
      FieldName = 'Id'
    end
    object taSpecVnQuantN: TFloatField
      FieldName = 'QuantN'
      DisplayFormat = '0.000'
    end
    object taSpecVnProcBrak: TFloatField
      FieldName = 'ProcBrak'
      DisplayFormat = '0.00'
    end
    object taSpecVnQuantM: TFloatField
      FieldName = 'QuantM'
      OnChange = taSpecVnQuantMChange
      DisplayFormat = '0.000'
    end
    object taSpecVnCena0: TFloatField
      FieldName = 'Cena0'
      DisplayFormat = '0.00'
    end
    object taSpecVnSumCena0: TFloatField
      FieldName = 'SumCena0'
      DisplayFormat = '0.00'
    end
  end
  object taSpecPr: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecPrIndex1'
        Fields = 'Num;Id'
        Options = [ixPrimary]
      end>
    IndexFieldNames = 'Num;Id'
    Params = <>
    StoreDefs = True
    Left = 420
    Top = 204
    object taSpecPrNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecPrId: TIntegerField
      FieldName = 'Id'
    end
    object taSpecPrCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpecPrName: TStringField
      FieldName = 'Name'
      Size = 60
    end
    object taSpecPrFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object taSpecPrKol: TFloatField
      FieldName = 'Kol'
    end
    object taSpecPrCena: TFloatField
      FieldName = 'Cena'
    end
    object taSpecPrSumCena: TFloatField
      FieldName = 'SumCena'
    end
    object taSpecPrNewCena: TFloatField
      FieldName = 'NewCena'
    end
    object taSpecPrSumNewCena: TFloatField
      FieldName = 'SumNewCena'
    end
    object taSpecPrEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object taSpecPrNDS: TFloatField
      FieldName = 'NDS'
    end
    object taSpecPrNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
  end
end
