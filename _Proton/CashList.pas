unit CashList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, ComCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, Placemnt, cxContainer,
  cxLabel, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmCashList = class(TForm)
    FormPlacement1: TFormPlacement;
    GridCashList: TcxGrid;
    ViewCashList: TcxGridDBTableView;
    LevelCashList: TcxGridLevel;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    Label10: TcxLabel;
    ViewCashListNumber: TcxGridDBColumn;
    ViewCashListName: TcxGridDBColumn;
    ViewCashListLoad: TcxGridDBColumn;
    ViewCashListTake: TcxGridDBColumn;
    amCashList: TActionManager;
    acExit: TAction;
    ViewCashListRezerv: TcxGridDBColumn;
    acEditCash: TAction;
    ViewCashListPATH: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure acEditCashExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashList: TfmCashList;

implementation

uses Un1, MDB, AddCash, MT, u2fdk;

{$R *.dfm}

procedure TfmCashList.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridCashList.Align:=AlClient;
  ViewCashList.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmCashList.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCashList.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmCashList.acEditCashExecute(Sender: TObject);
Var iCash:INteger;
begin
  if pos('�����',Person.Name)=0 then exit;
  with dmMC do
  with dmMt do
  begin
    if quCashList.RecordCount>0 then
    begin
      //�������������
      iCash:=quCashListNumber.AsInteger;
      fmAddCash.Label3.Caption:=its(iCash);
      fmAddCash.cxTextEdit1.Text:=quCashListPATH.AsString;
      fmAddCash.ShowModal;
      if fmAddCash.ModalResult=mrOk then
      begin
        if ptCashPath.Active=False then ptCashPath.Active:=True else ptCashPath.Refresh;
        if ptCashPath.locate('ID',iCash,[]) then
        begin
          ptCashPath.Edit;
          ptCashPathPATH.AsString:=fmAddCash.cxTextEdit1.Text;
          ptCashPath.Post;
        end else
        begin
          ptCashPath.Append;
          ptCashPathID.AsInteger:=iCash;
          ptCashPathPATH.AsString:='';
          ptCashPath.Post;
          delay(10);
          
          ptCashPath.Edit;
          ptCashPathPATH.AsString:=fmAddCash.cxTextEdit1.Text;
          ptCashPath.Post;
          
          delay(10);
        end;

        ViewCashList.BeginUpdate;
        quCashList.Active:=False;
        quCashList.Active:=True;
        quCashList.Locate('Number',iCash,[]);
        ViewCashList.EndUpdate;

        ViewCashList.Controller.FocusRecord(ViewCashList.DataController.FocusedRowIndex,True);

      end;
    end;
  end;
end;

end.
