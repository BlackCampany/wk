unit Refer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, cxTextEdit, cxMemo, cxControls, cxContainer, cxEdit, cxGroupBox;

type
  TfmComm = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    Memo1: TcxMemo;
    Memo2: TcxMemo;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmComm: TfmComm;

implementation

{$R *.dfm}

procedure TfmComm.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmComm.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
  Memo2.Clear;
end;

end.
