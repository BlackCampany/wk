unit ShowPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfmShowPost = class(TForm)
    GrShowPost: TcxGrid;
    ViewShowPost: TcxGridDBTableView;
    LevShowPost: TcxGridLevel;
    ViewShowPostIDCLI: TcxGridDBColumn;
    ViewShowPostNameCli: TcxGridDBColumn;
    ViewShowPostQuant: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmShowPost: TfmShowPost;

implementation

uses dmPS;

{$R *.dfm}

procedure TfmShowPost.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmP.quQPost.Active:=false;
end;

end.
