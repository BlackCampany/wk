unit ObMoveTara;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmObVedTara = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridObVedTara: TcxGrid;
    LevelObVedTara: TcxGridLevel;
    dsteObVedTara: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepObTara: TfrReport;
    frtaObVedTara: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    teObVedTara: TdxMemData;
    teObVedTaraQBeg: TFloatField;
    teObVedTaraQIn: TFloatField;
    teObVedTaraQOut: TFloatField;
    teObVedTaraQRes: TFloatField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teObVedTaraQInv: TFloatField;
    ViewObVedTara: TcxGridDBTableView;
    amRepObTara: TActionManager;
    acMoveRepOb: TAction;
    teObVedTaraCodeT: TIntegerField;
    teObVedTaraNameT: TStringField;
    teObVedTaraPrIn: TFloatField;
    teObVedTaraSumIn: TFloatField;
    teObVedTaraPrOut: TFloatField;
    teObVedTaraSumOut: TFloatField;
    ViewObVedTaraCodeT: TcxGridDBColumn;
    ViewObVedTaraNameT: TcxGridDBColumn;
    ViewObVedTaraQBeg: TcxGridDBColumn;
    ViewObVedTaraQIn: TcxGridDBColumn;
    ViewObVedTaraSumIn: TcxGridDBColumn;
    ViewObVedTaraQOut: TcxGridDBColumn;
    ViewObVedTaraSumOut: TcxGridDBColumn;
    ViewObVedTaraQRes: TcxGridDBColumn;
    ViewObVedTaraQInv: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmObVedTara: TfmObVedTara;

implementation

uses Un1, MDB, MT, Move;

{$R *.dfm}

procedure TfmObVedTara.FormCreate(Sender: TObject);
begin
  GridObVedTara.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
//  ViewObVedTara.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmObVedTara.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewObVedTara.StoreToIniFile(CurDir+GridIni,False);
  teObVedTara.Active:=False;
end;

procedure TfmObVedTara.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmObVedTara.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmObVedTara.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewObVedTara);
end;

procedure TfmObVedTara.SpeedItem4Click(Sender: TObject);
//Var StrWk:String;
//    i:Integer;
begin
//������
  ViewObVedTara.BeginUpdate;
{  teObVed.Filter:=ViewObVed.DataController.Filter.FilterText;
  teObVed.Filtered:=True;

  frRepOb.LoadFromFile(CurDir + 'ObVed.frf');

  frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

//  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewObVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepOb.ReportName:='��������� ���������.';
  frRepOb.PrepareReport;
  frRepOb.ShowPreparedReport;

  teObVed.Filter:='';
  teObVed.Filtered:=False;}
  ViewObVedTara.EndUpdate;
end;

procedure TfmObVedTara.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepObVedTara') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmObVedTara.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmObVedTara.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmObVedTara.acMoveRepObExecute(Sender: TObject);
begin
//��������������
end;

end.
