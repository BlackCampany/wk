unit PartSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons;

type
  TfmPartSel = class(TForm)
    Panel1: TPanel;
    ViewPartIn: TcxGridDBTableView;
    LePartIn: TcxGridLevel;
    GridPart: TcxGrid;
    LePartOut: TcxGridLevel;
    ViewPartOut: TcxGridDBTableView;
    Panel2: TPanel;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPartSel: TfmPartSel;

implementation

{$R *.dfm}

procedure TfmPartSel.FormCreate(Sender: TObject);
begin
  GridPart.Align:=AlClient;
end;

procedure TfmPartSel.cxButton1Click(Sender: TObject);
begin
  Close;
end;

end.
