unit uCateg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ActnList, XPStyleActnCtrls, ActnMan,
  ExtCtrls, Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxTextEdit, cxContainer, cxLabel, ComCtrls;

type
  TfmCateg = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    GridCateg: TcxGrid;
    ViewCateg: TcxGridDBTableView;
    ViewCategID: TcxGridDBColumn;
    ViewCategName: TcxGridDBColumn;
    LevelCateg: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    amCateg: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    ViewCategSID: TcxGridDBColumn;
    procedure acAddExecute(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCateg: TfmCateg;

implementation

uses AddSingle, MDB, uAddDouble;

{$R *.dfm}

procedure TfmCateg.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prAddStatus') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  fmAddDouble.caption:='������: ����������.';
  fmAddDouble.cxTextEdit1.Text:='';
  fmAddDouble.cxTextEdit2.Text:='';
  fmAddDouble.ShowModal;
  if fmAddDouble.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewCateg.BeginUpdate;
      try
        if quCateg.Locate('SID',fmAddDouble.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          iMax:=1;
          if CountRec('A_CATEG')>0 then begin quCateg.Last; iMax:=quCategID.AsInteger+1; end;

          quE.SQL.Clear;
          quE.SQL.Add('INSERT into A_CATEG values ('+IntToStr(iMax)+','''+Copy(fmAddDouble.cxTextEdit1.Text,1,30)+''','''+Copy(fmAddDouble.cxTextEdit2.Text,1,50)+''')');
          quE.ExecSQL;

          quCateg.Active:=False;quCateg.Active:=True;
          quCateg.Locate('ID',iMax,[]);
        end;
      finally
        ViewCateg.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmCateg.Label1Click(Sender: TObject);
begin
  acAdd.Execute;
end;

procedure TfmCateg.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelStatus') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('A_CATEG')>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quCategComment.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewCateg.BeginUpdate;
        try
          try
            iR:=quCategID.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from A_CATEG');
            quD.SQL.Add('where ID='+IntToStr(iR));
            quD.ExecSQL;
            
            quCateg.Close;
            quCateg.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewCateg.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmCateg.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmCateg.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmCateg.acEditExecute(Sender: TObject);
Var iR:Integer;
begin
//�������������
  if not CanDo('prEditStatus') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('A_CATEG')>0 then
    begin
      fmAddDouble.caption:='������: ��������������.';
      fmAddDouble.cxTextEdit1.Text:=quCategSID.AsString;
      fmAddDouble.cxTextEdit2.Text:=quCategComment.AsString;
      fmAddDouble.ShowModal;
      if fmAddDouble.ModalResult=mrOk then
      begin
        ViewCateg.BeginUpdate;
        try
          try
            iR:=quCategID.AsInteger;

            quA.SQL.Clear;
            quA.SQL.Add('Update A_CATEG Set SID='''+Copy(fmAddDouble.cxTextEdit1.Text,1,30)+''',Comment='''+Copy(fmAddDouble.cxTextEdit2.Text,1,50)+'''');
            quA.SQL.Add('where ID='+IntToStr(iR));
            quA.ExecSQL;

            quCateg.Active:=False;quCateg.Active:=True;
            quCateg.Locate('ID',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewCateg.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmCateg.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmCateg.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
