unit PrintInSCHF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ExtCtrls, cxTextEdit, cxMaskEdit,
  cxSpinEdit, StdCtrls, cxControls, cxContainer, cxEdit, cxCheckBox,
  cxButtons, FR_DSet, FR_DBSet, FR_Class, DB, dxmdaset;

type
  TfmPrintInSCHF = class(TForm)
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Label7: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    Panel1: TPanel;
    cxButton1: TcxButton;
    frRepDIN: TfrReport;
    frtePrintD: TfrDBDataSet;
    tePrintD: TdxMemData;
    tePrintDNum: TIntegerField;
    tePrintDFullName: TStringField;
    tePrintDCode: TIntegerField;
    tePrintDEdIzm: TIntegerField;
    tePrintDKolMest: TFloatField;
    tePrintDKolWithMest: TFloatField;
    tePrintDKol: TFloatField;
    tePrintDPrice0: TFloatField;
    tePrintDPrice: TFloatField;
    tePrintDPrNDS: TFloatField;
    tePrintDSumNDS: TFloatField;
    tePrintDRSum0: TFloatField;
    tePrintDRSum: TFloatField;
    tePrintDMassa: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPrintInSCHF: TfmPrintInSCHF;

implementation

uses MDB, MT, DocsIn, Un1, u2fdk, sumprops;

{$R *.dfm}

procedure TfmPrintInSCHF.FormCreate(Sender: TObject);
begin
  cxTextEdit1.Text:='';
  cxTextEdit2.Text:='';
end;

procedure TfmPrintInSCHF.cxButton3Click(Sender: TObject);
Var
    rNds:Real;
    rSum0,rSumNDS:Real;
    StrWk1,StrWk2,StrWk3,sInn,sInnF:String;
begin
  with dmMC do
  with dmMT do
  with fmDocs1 do
  begin
    if quTTnIn.RecordCount>0 then
    begin
      if ptInLn.Active=False then ptINLn.Active:=True else ptInLn.Refresh;
      ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
      if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

      closete(tePrintD);

      ptInLn.CancelRange;
      ptInLn.SetRange([quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)],[quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)]);
      ptInLn.First;
      while not ptInLn.Eof do
      begin
        if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
        begin
          rNDS:=ptInLnNDSProc.AsFloat;
          rSumNDS:=ptInLnNDSSum.AsFloat;
          rSum0:=ptInLnOutNDSSum.AsFloat;

          if abs(rNDS)<0.01 then //��� �� ���������
          begin
            rNDS:=10;
            rSumNDS:=rv(ptInLnSumCenaTovarPost.AsFloat/(100+rNDS)*rNDS);
            rSum0:=ptInLnSumCenaTovarPost.AsFloat-rSumNDS;
          end;

          tePrintD.Append;
          tePrintDNum.AsInteger:=Trunc(ptInLnMediatorCost.AsFloat);
          tePrintDFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
          tePrintDCode.AsInteger:=ptInLnCodeTovar.AsInteger;
          tePrintDEdIzm.AsInteger:=ptInLnCodeEdIzm.AsInteger;
          tePrintDKolMest.AsFloat:=ptInLnKolMest.AsInteger;
          tePrintDKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
          tePrintDKol.AsFloat:=ptInLnKol.AsFloat;
          tePrintDPrice0.AsFloat:=0;
          if ptInLnKol.AsFloat<>0 then tePrintDPrice0.AsFloat:=rv(rSum0/ptInLnKol.AsFloat);
          tePrintDPrice.AsFloat:=ptInLnCenaTovar.AsFloat;
          tePrintDPrNDS.AsFloat:=rNDS;
          tePrintDSumNDS.AsFloat:=rSumNDS;
          tePrintDRSum0.AsFloat:=rSum0;
          tePrintDRSum.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
          tePrintDMassa.AsFloat:=0;
          tePrintD.Post;

        end;
        ptInLn.Next;
      end;

      frRepDIN.LoadFromFile(CommonSet.Reports + 'schfin.frf');

      frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnInDateInvoice.AsDateTime)+quTTnInNumber.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime);
      frVariables.Variable['RSum']:=quTTnInSummaTovarPost.AsFloat;
      frVariables.Variable['RSum0']:=quTTnInSummaTovarPost.AsFloat-quTTnInNDSTovar.AsFloat;
      frVariables.Variable['RSumNDS']:=quTTnInNDSTovar.AsFloat;

      quDepPars.Active:=False;
      quDepPars.ParamByName('ID').AsInteger:=quTTnInDepart.AsInteger; //
      quDepPars.Active:=True;

      if quDepPars.RecordCount>0 then
      begin
        frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
        frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
        frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
        frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
        frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
{                  frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;}
      end else
      begin
        frVariables.Variable['Cli2Name']:='';
        frVariables.Variable['Cli2Inn']:='';
        frVariables.Variable['Cli2Adr']:='';
        frVariables.Variable['Poluch']:='';
        frVariables.Variable['PoluchAdr']:='';
{                  frVariables.Variable['Cli2RSch']:='';
                  frVariables.Variable['Cli2Bank']:='';
                  frVariables.Variable['Cli2KSch']:='';
                  frVariables.Variable['Cli2Bik']:='';}
      end;
      quDepPars.Active:=False;

      prFCliINN(quTTnInCodePost.AsInteger,quTTnInIndexPost.AsInteger,StrWk1,StrWk2,StrWk3,SInnF);

      frVariables.Variable['Cli1Name']:=StrWk3;
      frVariables.Variable['Cli1Inn']:=StrWk1;
      frVariables.Variable['Cli1Adr']:=StrWk2;

      sInn:=StrWk1;
      if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';

      quCliPars.Active:=False;
      quCliPars.ParamByName('SINN').AsString:=SInnF;
      quCliPars.Active:=True;

      if quCliPars.RecordCount>0 then
      begin
        if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
        begin
          frVariables.Variable['Otpr']:=frVariables.Variable['Cli1Name'];
          frVariables.Variable['OtprAdr']:=frVariables.Variable['Cli1Adr'];
        end
        else
        begin
          frVariables.Variable['Otpr']:=quCliParsNAMEOTP.AsString;
          frVariables.Variable['OtprAdr']:=quCliParsADROTPR.AsString;
        end;
      end;

      //-------

      frVariables.Variable['Rukovod']:=cxTextEdit1.Text;
      frVariables.Variable['GlBuh']:=cxTextEdit2.Text;


      frRepDIN.ReportName:='����.';
      frRepDIN.PrepareReport;

      if cxCheckBox1.Checked then frRepDIN.ShowPreparedReport
      else frRepDIN.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

      tePrintD.Active:=False;
    end;
  end;
end;

procedure TfmPrintInSCHF.cxButton4Click(Sender: TObject);
Var
    rNds:Real;
    rSum0,rSumNDS:Real;
    StrWk1,StrWk2,StrWk3,sInn,sInnF:String;
    sStr:String;
begin
  with dmMC do
  with dmMT do
  with fmDocs1 do
  begin
    if quTTnIn.RecordCount>0 then
    begin
      if ptInLn.Active=False then ptINLn.Active:=True else ptInLn.Refresh;
      ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
      if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

      closete(tePrintD);

      ptInLn.CancelRange;
      ptInLn.SetRange([quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)],[quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)]);
      ptInLn.First;
      while not ptInLn.Eof do
      begin
        if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
        begin
          rNDS:=ptInLnNDSProc.AsFloat;
          rSumNDS:=ptInLnNDSSum.AsFloat;
          rSum0:=ptInLnOutNDSSum.AsFloat;

          if abs(rNDS)<0.01 then //��� �� ���������
          begin
            rNDS:=10;
            rSumNDS:=rv(ptInLnSumCenaTovarPost.AsFloat/(100+rNDS)*rNDS);
            rSum0:=ptInLnSumCenaTovarPost.AsFloat-rSumNDS;
          end;

          tePrintD.Append;
          tePrintDNum.AsInteger:=Trunc(ptInLnMediatorCost.AsFloat);
          tePrintDFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
          tePrintDCode.AsInteger:=ptInLnCodeTovar.AsInteger;
          tePrintDEdIzm.AsInteger:=ptInLnCodeEdIzm.AsInteger;
          tePrintDKolMest.AsFloat:=ptInLnKolMest.AsInteger;
          tePrintDKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
          tePrintDKol.AsFloat:=ptInLnKol.AsFloat;
          tePrintDPrice0.AsFloat:=0;
          if ptInLnKol.AsFloat<>0 then tePrintDPrice0.AsFloat:=rv(rSum0/ptInLnKol.AsFloat);
          tePrintDPrice.AsFloat:=ptInLnCenaTovar.AsFloat;
          tePrintDPrNDS.AsFloat:=rNDS;
          tePrintDSumNDS.AsFloat:=rSumNDS;
          tePrintDRSum0.AsFloat:=rSum0;
          tePrintDRSum.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
          tePrintDMassa.AsFloat:=0;
          tePrintD.Post;

        end;
        ptInLn.Next;
      end;

      frRepDIN.LoadFromFile(CommonSet.Reports + 'torg12in.frf');

      frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnInDateInvoice.AsDateTime)+quTTnInNumber.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime);
      frVariables.Variable['RSum']:=quTTnInSummaTovarPost.AsFloat;
      frVariables.Variable['RSum0']:=quTTnInSummaTovarPost.AsFloat-quTTnInNDSTovar.AsFloat;
      frVariables.Variable['RSumNDS']:=quTTnInNDSTovar.AsFloat;
      frVariables.Variable['SSum']:=MoneyToString(quTTnInSummaTovarPost.AsFloat,True,False);
      frVariables.Variable['TypeP']:=1;

      sStr:=MoneyToString(tePrintD.RecordCount,True,False);
      if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
      frVariables.Variable['SQStr']:=' ('+sStr+')';

      frVariables.Variable['Osnovanie']:='�������� �������';


      quDepPars.Active:=False;
      quDepPars.ParamByName('ID').AsInteger:=quTTnInDepart.AsInteger; //
      quDepPars.Active:=True;

      if quDepPars.RecordCount>0 then
      begin
        frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
        frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
        frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
        frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
        frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
        frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
        frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
        frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
        frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;
      end else
      begin
        frVariables.Variable['Cli2Name']:='';
        frVariables.Variable['Cli2Inn']:='';
        frVariables.Variable['Cli2Adr']:='';
        frVariables.Variable['Poluch']:='';
        frVariables.Variable['PoluchAdr']:='';
        frVariables.Variable['Cli2RSch']:='';
        frVariables.Variable['Cli2Bank']:='';
        frVariables.Variable['Cli2KSch']:='';
        frVariables.Variable['Cli2Bik']:='';
      end;
      quDepPars.Active:=False;

      prFCliINN(quTTnInCodePost.AsInteger,quTTnInIndexPost.AsInteger,StrWk1,StrWk2,StrWk3,SInnF);

      frVariables.Variable['Cli1Name']:=StrWk3;
      frVariables.Variable['Cli1Inn']:=StrWk1;
      frVariables.Variable['Cli1Adr']:=StrWk2;

      sInn:=StrWk1;
      if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';

      frVariables.Variable['Cli2RSch']:='';
      frVariables.Variable['Cli2Bank']:='';
      frVariables.Variable['Cli2KSch']:='';
      frVariables.Variable['Cli2Bik']:='';

      quCliPars.Active:=False;
      quCliPars.ParamByName('SINN').AsString:=SInnF;
      quCliPars.Active:=True;

      if quCliPars.RecordCount>0 then
      begin
        if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
        begin
          frVariables.Variable['Otpr']:=frVariables.Variable['Cli1Name'];
          frVariables.Variable['OtprAdr']:=frVariables.Variable['Cli1Adr'];
        end
        else
        begin
          frVariables.Variable['Otpr']:=quCliParsNAMEOTP.AsString;
          frVariables.Variable['OtprAdr']:=quCliParsADROTPR.AsString;
        end;

        frVariables.Variable['Cli2RSch']:=quCliParsRSch.AsString;
        frVariables.Variable['Cli2Bank']:=quCliParsKSch.AsString;
        frVariables.Variable['Cli2KSch']:=quCliParsBank.AsString;
        frVariables.Variable['Cli2Bik']:=quCliParsBik.AsString;
      end;

      //-------

//      frVariables.Variable['Rukovod']:=cxTextEdit1.Text;

      frVariables.Variable['GlBuh']:=cxTextEdit2.Text;


      frRepDIN.ReportName:='����.';
      frRepDIN.PrepareReport;

      if cxCheckBox1.Checked then frRepDIN.ShowPreparedReport
      else frRepDIN.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

      tePrintD.Active:=False;
    end;
  end;
end;

end.
