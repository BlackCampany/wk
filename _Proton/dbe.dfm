object dmE: TdmE
  OldCreateOrder = False
  Left = 474
  Top = 270
  Height = 242
  Width = 447
  object dsquUPL: TDataSource
    DataSet = quUPL
    Left = 36
    Top = 16
  end
  object quUPL: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select ul.ID,ul.IMH,ul.NAME,ul.IACTIVE,dep.Name as NAMEMH'
      'from "A_UPLICA" ul'
      'left join "Depart" dep on dep.ID=ul.IMH')
    Params = <>
    Left = 100
    Top = 16
    object quUPLID: TIntegerField
      FieldName = 'ID'
    end
    object quUPLIMH: TIntegerField
      FieldName = 'IMH'
    end
    object quUPLNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quUPLIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quUPLNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 30
    end
  end
  object dsHistOp: TDataSource
    DataSet = quDocHist
    Left = 184
    Top = 16
  end
  object quDocHist: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT '
      'dt."Name",'
      'dh."TYPED",'
      'dh."IDPERS",'
      'dh."DATEEDIT",'
      'dh."DOCDATE",'
      'dh."DOCNUM",'
      'dh."DOCID",'
      'dh."IOP",'
      'dh."NAMEPERS",'
      'dh."IDOP",'
      'isnull(c."NAMECASSIR",'#39' '#39') as "NAMECASSIR"'
      'from "A_DOCHIST" dh'
      'left join DocTYpeName dt on dt.Code=dh."TYPED"'
      'left join A_CASSIR c on c.ID=dh."IDPERS"')
    Params = <>
    Left = 248
    Top = 16
    object quDocHistName: TStringField
      FieldName = 'Name'
      Size = 29
    end
    object quDocHistTYPED: TSmallintField
      FieldName = 'TYPED'
      Required = True
    end
    object quDocHistIDPERS: TIntegerField
      FieldName = 'IDPERS'
      Required = True
    end
    object quDocHistDATEEDIT: TDateTimeField
      FieldName = 'DATEEDIT'
    end
    object quDocHistDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object quDocHistDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Size = 30
    end
    object quDocHistDOCID: TIntegerField
      FieldName = 'DOCID'
    end
    object quDocHistIOP: TStringField
      FieldName = 'IOP'
      Size = 10
    end
    object quDocHistNAMEPERS: TStringField
      FieldName = 'NAMEPERS'
      Size = 30
    end
    object quDocHistIDOP: TSmallintField
      FieldName = 'IDOP'
    end
    object quDocHistNAMECASSIR: TStringField
      FieldName = 'NAMECASSIR'
      Size = 100
    end
  end
  object dsquUPLMh: TDataSource
    DataSet = quUPLMh
    Left = 36
    Top = 76
  end
  object quUPLMh: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select ul.ID,ul.IMH,ul.NAME,ul.IACTIVE'
      'from "A_UPLICA" ul'
      'where ul.IMH=:IMH'
      'and ul.IACTIVE=1')
    Params = <
      item
        DataType = ftInteger
        Name = 'IMH'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 100
    Top = 76
    object quUPLMhID: TIntegerField
      FieldName = 'ID'
    end
    object quUPLMhIMH: TIntegerField
      FieldName = 'IMH'
    end
    object quUPLMhNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quUPLMhIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
  end
  object quCashList: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select cp.ID,cp.PATH,rc.Name from "A_CASHPATH" cp'
      'left join "RCashRegister" rc on cp.ID=rc.Number'
      'where cp.PATH > '#39#39
      'order by cp.ID')
    Params = <>
    Left = 324
    Top = 16
    object quCashListID: TIntegerField
      FieldName = 'ID'
    end
    object quCashListPATH: TStringField
      FieldName = 'PATH'
      Size = 300
    end
    object quCashListName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quLastFullInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 1 Inventry,Depart,DateInventry from "InventryHead"'
      
        'where Depart=:IDEP and Status='#39#1054#39' and Detail=0 and DateInventry<' +
        ':DDATE'
      'order by DateInventry desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 196
    Top = 84
    object quLastFullInvInventry: TIntegerField
      FieldName = 'Inventry'
    end
    object quLastFullInvDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quLastFullInvDateInventry: TDateField
      FieldName = 'DateInventry'
    end
  end
  object quSumCorr: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select ITEM,SUM(QUANTR-QUANTF) as RQUANTC,SUM((QUANTR-QUANTF)*PR' +
        'ICER)as RSUMC,SUM(SUMRSS-SUMFSS)as RSUMRSC from "A_INVLN"'
      'where IDH in'
      '(Select Inventry from "InventryHead"'
      'where'
      'Depart=120'
      'and Status='#39#1054#39
      'and Detail<>0'
      'and DateInventry<'#39'2013-10-02'#39
      'and DateInventry>'#39'2013-02-10'#39
      'and xDopStatus=1'
      'and Item=0)'
      'group by ITEM')
    Params = <>
    Left = 272
    Top = 84
    object quSumCorrITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quSumCorrRQUANTC: TFloatField
      FieldName = 'RQUANTC'
    end
    object quSumCorrRSUMC: TFloatField
      FieldName = 'RSUMC'
    end
    object quSumCorrRSUMRSC: TFloatField
      FieldName = 'RSUMRSC'
    end
  end
end
