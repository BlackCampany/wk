unit CliLic;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, ActnList, XPStyleActnCtrls,
  ActnMan, dxmdaset;

type
  TfmCliLic = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GrCliLic: TcxGrid;
    ViCliLic: TcxGridDBTableView;
    leCliLic: TcxGridLevel;
    amCliLic: TActionManager;
    acAddCliLic: TAction;
    acEditCliLic: TAction;
    acDelCliLic: TAction;
    ViCliLicDDATEB: TcxGridDBColumn;
    ViCliLicDDATEE: TcxGridDBColumn;
    ViCliLicSER: TcxGridDBColumn;
    ViCliLicSNUM: TcxGridDBColumn;
    ViCliLicORGAN: TcxGridDBColumn;
    ViCliLicID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acAddCliLicExecute(Sender: TObject);
    procedure acEditCliLicExecute(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acDelCliLicExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCliLic: TfmCliLic;

implementation

uses Un1, u2fdk, DMOReps, AddCliLic, dmOffice;

{$R *.dfm}

procedure TfmCliLic.FormCreate(Sender: TObject);
begin
  GrCliLic.Align:=AlClient;
end;

procedure TfmCliLic.acAddCliLicExecute(Sender: TObject);
Var iMax:INteger;
begin
  fmAddCliLic.cxTextEdit1.Text:='';
  fmAddCliLic.cxTextEdit2.Text:='';
  fmAddCliLic.cxTextEdit3.Text:='';
  fmAddCliLic.cxDateEdit1.Date:=Date;
  fmAddCliLic.cxDateEdit1.Properties.ReadOnly:=False;
  fmAddCliLic.cxDateEdit2.Date:=Date;
  fmAddCliLic.ShowModal;
  if fmAddCliLic.ModalResult=mrOk then
  begin
    with dmORep do
    with dmO do
    begin
      iMax:=GetId('123');

      quCliLic.Append;
      quCliLicID.AsInteger:=iMax;
      quCliLicICLI.AsInteger:=fmCliLic.Tag;
      quCliLicIDATEB.AsInteger:=Trunc(fmAddCliLic.cxDateEdit1.Date);
      quCliLicDDATEB.AsDateTime:=fmAddCliLic.cxDateEdit1.Date;
      quCliLicIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
      quCliLicDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
      quCliLicSER.AsString:=fmAddCliLic.cxTextEdit1.Text;
      quCliLicSNUM.AsString:=fmAddCliLic.cxTextEdit2.Text;
      quCliLicORGAN.AsString:=fmAddCliLic.cxTextEdit3.Text;
      quCliLic.Post;

    end;
  end;
end;

procedure TfmCliLic.acEditCliLicExecute(Sender: TObject);
begin
  if dmORep.quCliLic.RecordCount>0 then
  begin
    fmAddCliLic.cxTextEdit1.Text:=dmORep.quCliLicSER.AsString;
    fmAddCliLic.cxTextEdit2.Text:=dmORep.quCliLicSNUM.AsString;
    fmAddCliLic.cxTextEdit3.Text:=dmORep.quCliLicORGAN.AsString;
    fmAddCliLic.cxDateEdit1.Date:=dmORep.quCliLicIDATEB.AsInteger;
    fmAddCliLic.cxDateEdit1.Properties.ReadOnly:=True;
    fmAddCliLic.cxDateEdit2.Date:=dmORep.quCliLicIDATEE.AsInteger;

    fmAddCliLic.ShowModal;
    if fmAddCliLic.ModalResult=mrOk then
    begin
      with dmORep do
      begin
        quCliLic.Edit;
        quCliLicIDATEB.AsInteger:=Trunc(fmAddCliLic.cxDateEdit1.Date);
        quCliLicDDATEB.AsDateTime:=fmAddCliLic.cxDateEdit1.Date;
        quCliLicIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
        quCliLicDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
        quCliLicSER.AsString:=fmAddCliLic.cxTextEdit1.Text;
        quCliLicSNUM.AsString:=fmAddCliLic.cxTextEdit2.Text;
        quCliLicORGAN.AsString:=fmAddCliLic.cxTextEdit3.Text;
        quCliLic.Post;
      end;
    end;
  end;
end;

procedure TfmCliLic.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCliLic.acDelCliLicExecute(Sender: TObject);
begin
  if dmORep.quCliLic.RecordCount>0 then
  begin
    if MessageDlg('������� ������ �� �������� � '+dmORep.quCliLicSNUM.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmORep do
      begin
        quCliLic.Delete;
      end;
    end;
  end;
end;

end.
