object fmSelDep: TfmSelDep
  Left = 385
  Top = 385
  Width = 543
  Height = 140
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1086#1074#1099#1081' '#1086#1090#1076#1077#1083
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 40
    Top = 36
    Width = 31
    Height = 13
    Caption = #1054#1090#1076#1077#1083
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 96
    Top = 30
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Name'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = dsquDeps
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Width = 261
  end
  object Panel1: TPanel
    Left = 417
    Top = 0
    Width = 118
    Height = 106
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object quDeps: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "Depart"'
      'where Status1<>13'
      'Order by Name'
      '')
    Params = <>
    Left = 316
    Top = 60
    object quDepsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquDeps: TDataSource
    DataSet = quDeps
    Left = 364
    Top = 60
  end
end
