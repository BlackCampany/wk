object fmMainRnOffice: TfmMainRnOffice
  Left = 182
  Top = 109
  Width = 1063
  Height = 129
  Caption = #1054#1092#1080#1089
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  WindowState = wsMaximized
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 76
    Width = 1055
    Height = 19
    Color = 16579836
    Panels = <
      item
        Width = 400
      end
      item
        Width = 300
      end
      item
        Text = 'v 13.03'
        Width = 50
      end>
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 1055
    Height = 24
    UseSystemFont = False
    ActionManager = am1
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = 15660791
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = 15660791
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 24
    Width = 1055
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16762052
    PopupMenu = PopupMenu1
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 764
      Top = 4
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acGoods
      BtnCaption = #1058#1086#1074#1072#1088#1099
      Caption = #1058#1086#1074#1072#1088#1099
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7CC8051F7C1F7C1F7CC8051F7CC805D633D633D633D633
        D633D633D6331F7C1F7CC8051F7C1F7C1F7CC805C805C805D633D633D633D633
        D633D633D6331F7C1F7CC8051F7C1F7C1F7CC8051F7CC805D633D633D633D633
        D633D633D6331F7C1F7CC8051F7C1F7C1F7CC8051F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7CC8051F7C1F7CC805C805C805C805C805C805C805C805
        C8051F7C1F7C1F7C1F7CC8051F7C1F7CC805D633D633D633D633D633D633D633
        C8051F7C1F7C1F7C1F7CC805C805C805C805D633D633D633D633D633D633D633
        C8051F7C1F7C1F7C1F7CC8051F7C1F7CC805D633D633D633D633D633D633D633
        C8051F7C1F7C1F7C1F7CC8051F7C1F7CC805C805C805C805C805C805C805C805
        C8051F7C1F7C1F7C1F7CC8051F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CC805C805C805C805C805C805C805C805C8051F7C1F7C1F7C
        1F7C1F7C1F7C1F7CC805D633D633D633D633D633D633D633C8051F7C1F7C1F7C
        1F7C1F7C1F7C1F7CC805D633D633D633D633D633D633D633C8051F7C1F7C1F7C
        1F7C1F7C1F7C1F7CC805C805C805C805C805C805C805C805C8051F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1058#1086#1074#1072#1088#1099', '#1087#1088#1086#1076#1091#1082#1090#1099', '#1073#1083#1102#1076#1072
      Spacing = 1
      Left = 14
      Top = 4
      Visible = True
      OnClick = acGoodsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acClients
      BtnCaption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Glyph.Data = {
        66030000424D6603000000000000360000002800000010000000110000000100
        1800000000003003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF94969463616363616363
        6163636163636163636163636163636163636163949694FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF6361638C8E8C8C8E8C8C8E8C8C8E8C8C8E8C8C8E8C8C8E8C8C8E
        8C8C8E8C636163FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6361638C8E8C8C8E8C8C
        8A8C848A8C39AEDE8486848482847B827B7B7D7B525552FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF5A5D5A8486848482847B827B39AEDE39AEDE39AEDE73797B7375
        737371734A4D4AFFFFFFFFFFFF949694636163B5B2B55255527B797B7B797B52
        5152297DB54ACBF7297DB54A4D4A6B696B636563424542FFFFFFFFFFFF636163
        8C8E8CC6C7C64A4D4A6B6D6B4A4D4A6B696B636563297DB56361636361634241
        425A5D5A393C39FFFFFFFFFFFF6361638C8E8CC6C7C64245424245425A555A52
        5552525552525152525152525152525152393839393839FFFFFFFFFFFF5A5D5A
        848684C6C3C63938395251525251525251525251525251525251525251525251
        52525152393839FFFFFFFFFFFF5255527B797BBDBEBD5A5D5A39383939383939
        38393938393938393938393938393938393938396B7173FFFFFFFFFFFF4A4D4A
        6B6D6B848684B5B6B5B5B2B594BEDE3196CE84A2B5A5A2A5849EAD3196CEFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF4245424245425A555A525552525552ADAAAD8C
        8E8C3938393938393938398C8A8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF393839
        525152525152525152525152848684ADAAADADAAADADAAADADAAAD7B7D7BFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF6B717339383939383939383939383939383939
        38393938393938393938396B7173FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF3196CEB5D7EFDEEBEFB5D7EF3196CEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C9EA539383939383939
        38399C9EA5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Hint = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Spacing = 1
      Left = 74
      Top = 4
      Visible = True
      OnClick = acClientsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acDocIn
      BtnCaption = #1055#1088#1080#1093#1086#1076
      Caption = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Hint = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Spacing = 1
      Left = 154
      Top = 4
      Visible = True
      OnClick = acDocInExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acOut
      BtnCaption = #1042#1086#1079#1074#1088#1072#1090
      Caption = #1056#1072#1089#1093#1086#1076
      Hint = #1056#1072#1089#1093#1086#1076
      Spacing = 1
      Left = 214
      Top = 4
      Visible = True
      OnClick = acOutExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      Action = acOutB
      BtnCaption = #1056#1072#1089#1093#1086#1076
      Caption = #1056#1072#1089#1093#1086#1076
      Hint = #1056#1072#1089#1093#1086#1076' '#13#10#1073#1083#1102#1076
      Spacing = 1
      Left = 274
      Top = 4
      Visible = True
      OnClick = acOutBExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem7: TSpeedItem
      Action = acInv
      BtnCaption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      Hint = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      Spacing = 1
      Left = 674
      Top = 4
      Visible = True
      OnClick = acInvExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      Action = acInDocs
      BtnCaption = #1042#1085'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1042#1085'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Hint = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Spacing = 1
      Left = 454
      Top = 4
      Visible = True
      OnClick = acInDocsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      Action = acKomplekt
      BtnCaption = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
      Caption = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
      Hint = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
      Spacing = 1
      Left = 534
      Top = 4
      Visible = True
      OnClick = acKomplektExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      Action = acActs
      BtnCaption = #1040#1082#1090#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      Caption = #1040#1082#1090#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      Hint = #1040#1082#1090#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      Spacing = 1
      Left = 594
      Top = 4
      Visible = True
      OnClick = acActsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem11: TSpeedItem
      Action = acReal
      BtnCaption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      Spacing = 1
      Left = 334
      Top = 4
      Visible = True
      OnClick = acRealExecute
      SectionName = 'Untitled (0)'
    end
  end
  object am1: TActionManager
    ActionBars = <
      item
        Color = 16579836
        Items = <
          item
            Items = <
              item
                Action = acExit
                Caption = #1042#1099#1093#1086#1076
                ImageIndex = 10
              end>
            Caption = #1057#1080#1089#1090#1077#1084#1072
          end
          item
            Items = <
              item
                Action = acGoods
                Caption = #1058#1086#1074#1072#1088#1099', '#1087#1088#1086#1076#1091#1082#1090#1099', '#1073#1083#1102#1076#1072
                ImageIndex = 9
                ShortCut = 32852
              end
              item
                Action = acMenu
                ImageIndex = 34
              end
              item
                Action = acClients
                ImageIndex = 42
              end
              item
                Action = acMHP
                ImageIndex = 41
              end
              item
                Action = acCategR
              end
              item
                Action = acMessure
              end
              item
                Action = amOperT
              end
              item
                Action = acBGU
              end
              item
                Action = acTermoObr
              end
              item
                Action = acClassAlg
              end
              item
                Action = acMakers
              end
              item
                Items = <
                  item
                    Action = acPriceType
                  end>
                Caption = #1057#1083#1091#1078#1077#1073#1085#1099#1077
                ImageIndex = 5
              end>
            Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
          end
          item
            Items = <
              item
                Action = acDocIn
              end
              item
                Action = acMove
              end
              item
                Action = acInv
              end
              item
                Caption = '-'
              end
              item
                Action = acOut
              end
              item
                Action = acOutB
              end
              item
                Action = acReal
              end
              item
                Caption = '-'
              end
              item
                Action = acRecalcPer
              end
              item
                Action = acRecalcReal
              end
              item
                Action = acPartRemn
              end>
            Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099
          end
          item
            Items = <
              item
                Action = acRemn
              end
              item
                Action = acRemnSpeedReal
              end
              item
                Action = acMoveDate
              end
              item
                Action = acRepPrib
              end
              item
                Action = acObVed
              end
              item
                Action = acRepPost
              end
              item
                Action = acAvans
              end
              item
                Action = acAlcogol
              end
              item
                Action = acAlcoDecl
              end
              item
                Caption = '-'
              end
              item
                Action = acTovRep
              end>
            Caption = #1054#1090#1095#1077#1090#1099
          end
          item
            Items = <
              item
                Action = acExportBuh
                ImageIndex = 4
              end
              item
                Caption = '-'
              end
              item
                Action = acImp1CDocs
              end>
            Caption = #1069#1082#1089#1087#1086#1088#1090'/'#1048#1084#1087#1086#1088#1090
          end>
        ActionBar = ActionMainMenuBar1
      end
      item
      end>
    Images = dmO.imState
    Left = 408
    Top = 12
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ImageIndex = 10
      OnExecute = acExitExecute
    end
    object acGoods: TAction
      Caption = 'acGoods'
      ImageIndex = 33
      ShortCut = 32852
      OnExecute = acGoodsExecute
    end
    object acModify: TAction
      Caption = 'acModify'
      ImageIndex = 13
    end
    object acCateg: TAction
      Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1080
      ImageIndex = 11
    end
    object acMessure: TAction
      Caption = #1045#1076'.'#1080#1079#1084
      OnExecute = acMessureExecute
    end
    object acPriceType: TAction
      Caption = #1058#1080#1087#1099' '#1094#1077#1085
      OnExecute = acPriceTypeExecute
    end
    object acMHP: TAction
      Caption = #1052#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
      ImageIndex = 41
      OnExecute = acMHPExecute
    end
    object acClients: TAction
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      ImageIndex = 42
      OnExecute = acClientsExecute
    end
    object acDocIn: TAction
      Caption = #1055#1088#1080#1093#1086#1076
      OnExecute = acDocInExecute
    end
    object acMove: TAction
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      OnExecute = acMoveExecute
    end
    object acOut: TAction
      Caption = #1042#1086#1079#1074#1088#1072#1090
      OnExecute = acOutExecute
    end
    object acRemn: TAction
      Caption = #1054#1089#1090#1072#1090#1082#1080
      OnExecute = acRemnExecute
    end
    object acInv: TAction
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      OnExecute = acInvExecute
    end
    object acMenu: TAction
      Caption = #1052#1077#1085#1102' '#1088#1077#1089#1090#1086#1088#1072#1085#1072
      ImageIndex = 34
      OnExecute = acMenuExecute
    end
    object acMoveDate: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      OnExecute = acMoveDateExecute
    end
    object acOutB: TAction
      Caption = #1056#1072#1089#1093#1086#1076
      OnExecute = acOutBExecute
    end
    object acExportBuh: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1080#1102
      ImageIndex = 4
      OnExecute = acExportBuhExecute
    end
    object acTovRep: TAction
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090
      OnExecute = acTovRepExecute
    end
    object acRepPrib: TAction
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1087#1088#1080#1073#1099#1083#1080
      OnExecute = acRepPribExecute
    end
    object acObVed: TAction
      Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
      OnExecute = acObVedExecute
    end
    object acKomplekt: TAction
      Caption = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
      OnExecute = acKomplektExecute
    end
    object acInDocs: TAction
      Caption = #1042#1085'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      OnExecute = acInDocsExecute
    end
    object acActs: TAction
      Caption = #1040#1082#1090#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      OnExecute = acActsExecute
    end
    object amOperT: TAction
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      OnExecute = amOperTExecute
    end
    object acReal: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      OnExecute = acRealExecute
    end
    object acRecalcPer: TAction
      Caption = #1055#1077#1088#1077#1087#1088#1086#1074#1077#1089#1090#1080' '#1087#1077#1088#1080#1086#1076
      OnExecute = acRecalcPerExecute
    end
    object acRecalcReal: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1102' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      OnExecute = acRecalcRealExecute
    end
    object acRepPost: TAction
      Caption = #1054#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      OnExecute = acRepPostExecute
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 49219
      OnExecute = Action1Execute
    end
    object acPartRemn: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1087#1072#1088#1090#1080#1081
      OnExecute = acPartRemnExecute
    end
    object acAvans: TAction
      Caption = #1059#1089#1083#1091#1075#1080' '#1080' '#1040#1074#1072#1085#1089#1099
      OnExecute = acAvansExecute
    end
    object acBGU: TAction
      Caption = #1041#1077#1083#1082#1080', '#1078#1080#1088#1099', '#1091#1075#1083#1077#1074#1086#1076#1099' - '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082
      OnExecute = acBGUExecute
    end
    object acTermoObr: TAction
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077' '#1090#1080#1087#1099' '#1090#1077#1087#1083#1086#1074#1086#1081' '#1082#1091#1083#1080#1085#1072#1088#1085#1086#1081' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
      OnExecute = acTermoObrExecute
    end
    object acCategR: TAction
      Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1080' '#1073#1083#1102#1076' ('#1090#1086#1074#1072#1088#1086#1074')'
      OnExecute = acCategRExecute
    end
    object acRemnSpeedReal: TAction
      Caption = #1054#1089#1090#1072#1090#1082#1080' '#1090#1086#1074#1072#1088#1085#1099#1093' '#1079#1072#1087#1072#1089#1086#1074
      OnExecute = acRemnSpeedRealExecute
    end
    object acImp1CDocs: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
      OnExecute = acImp1CDocsExecute
    end
    object acClassAlg: TAction
      Caption = #1042#1080#1076#1099' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' ('#1040#1083#1082#1086#1075#1086#1083#1100')'
      OnExecute = acClassAlgExecute
    end
    object acMakers: TAction
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1080' ('#1040#1083#1082#1086#1075#1086#1083#1100')'
      OnExecute = acMakersExecute
    end
    object acAlcogol: TAction
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
      OnExecute = acAlcogolExecute
    end
    object acAlcoDecl: TAction
      Caption = #1044#1077#1082#1083#1072#1088#1072#1094#1080#1103' '#1087#1086' '#1040#1083#1082#1086#1075#1086#1083#1102
      OnExecute = acAlcoDeclExecute
    end
  end
  object TimerClose: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerCloseTimer
    Left = 860
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 920
    Top = 32
    object N1: TMenuItem
      Caption = #1062#1074#1077#1090
      OnClick = N1Click
    end
  end
end
