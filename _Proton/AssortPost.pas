unit AssortPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLabel, dxmdaset, cxMemo, cxImageComboBox, ActnList, XPStyleActnCtrls,
  ActnMan;

type
  TfmAssPost = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButton1: TcxButton;
    Panel2: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel6: TcxLabel;
    GridAssPost: TcxGrid;
    ViewAssPost: TcxGridDBTableView;
    LevelAssPost: TcxGridLevel;
    mePost: TdxMemData;
    mePostCode: TIntegerField;
    mePostName: TStringField;
    mePostNameFull: TStringField;
    mePostPriceDog: TFloatField;
    mePostPricePP: TFloatField;
    Memo1: TcxMemo;
    mePostEdIzm: TStringField;
    dsmePost: TDataSource;
    ViewAssPostRecId: TcxGridDBColumn;
    ViewAssPostCode: TcxGridDBColumn;
    ViewAssPostName: TcxGridDBColumn;
    ViewAssPostEdIzm: TcxGridDBColumn;
    ViewAssPostPriceDog: TcxGridDBColumn;
    ViewAssPostPricePP: TcxGridDBColumn;
    ViewAssPostNameFull: TcxGridDBColumn;
    cxButton2: TcxButton;
    mePostsMess: TStringField;
    ViewAssPostsMess: TcxGridDBColumn;
    cxLabel3: TcxLabel;
    ActionManager1: TActionManager;
    acDelPos: TAction;
    acDelAll: TAction;
    acAddList: TAction;
    acRecalcAssPost: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure ViewAssPostDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewAssPostDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure acRecalcAssPostExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAssPost: TfmAssPost;

implementation

uses Un1, MDB, Clients, MT, mCards, u2fdk;

{$R *.dfm}

procedure TfmAssPost.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
  cxButtonEdit1.Tag:=0;
  Label1.Tag:=0;
  cxButtonEdit1.Text:='';
  if CommonSet.Single<>1 then
  begin
    Panel2.Visible:=False;
    cxButton1.Visible:=False;
  end;
  GridAssPost.Align:=AlClient;
end;

procedure TfmAssPost.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S:String;
begin
  if fmclients.Showing then
  begin
    fmclients.Close;
    fmAssPost.Close;
    fmAssPost.Show;
  end;

  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  if Label1.Tag<=1 then //����������
  begin
    fmClients.LevelCli.Active:=True;
    fmClients.LevelIP.Active:=False;
  end;
  if Label1.Tag=2 then //���������������
  begin
    fmClients.LevelCli.Active:=False;
    fmClients.LevelIP.Active:=True;
  end;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end else
  begin


  end;

  bOpen:=False;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      Label1.Tag:=1;
      cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
      cxButtonEdit1.Text:=dmMC.quCliName.AsString;
    end else
    begin
      Label1.Tag:=2;
      cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
      cxButtonEdit1.Text:=dmMC.quIPName.AsString;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;

  iDirect:=0;

end;

procedure TfmAssPost.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAssPost.cxButtonEdit1PropertiesChange(Sender: TObject);
Var iC:INteger;
begin
  if (Label1.Tag>0)and(cxButtonEdit1.Tag>0) then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('����� ���� ������������ ������...');
    with dmMC do
    with dmMT do
    begin
      try
        ViewAssPost.BeginUpdate;
        CloseTe(mePost);

        quAssPost.Active:=False;
        quAssPost.ParamByName('ICLIT').AsInteger:=Label1.Tag;
        quAssPost.ParamByName('ICLI').AsInteger:=cxButtonEdit1.Tag;
        quAssPost.Active:=True;

        iC:=0;

        quAssPost.First;
        while not quAssPost.Eof do
        begin
          mePost.Append;
          mePostCode.AsInteger:=quAssPostGoodsId.AsInteger;
          mePostName.AsString:=quAssPostName.AsString;
          mePostEdIzm.AsInteger:=quAssPostEdIzm.AsInteger;
          mePostPriceDog.AsFloat:=quAssPostPriceIn.AsFloat;
          mePostPricePP.AsFloat:=0;
          mePostNameFull.AsString:=quAssPostFullName.AsString;
          mePostsMess.AsString:=quAssPostsMess.AsString;
          mePost.Post;

          quAssPost.Next;
          inc(iC);
          if iC mod 100 =0 then
          begin
            Memo1.Lines.Add('  ���������� - '+its(iC));
            delay(10);
          end;
        end;
        quAssPost.Active:=False;

      finally
        ViewAssPost.EndUpdate;
      end;
    end;
    Memo1.Lines.Add('������������ ��.');
  end;
end;

procedure TfmAssPost.cxLabel3Click(Sender: TObject);
Var rPriceM:Real;
    iC:INteger;
begin
  if MessageDlg('����������� ���� �� �� ������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������...'); delay(10);
      ViewAssPost.BeginUpdate;

      iC:=0;
      mePost.First;
      while not mePost.Eof do
      begin
        mePost.Edit;
        mePostPricePP.AsFloat:=prFLP(mePostCode.AsInteger,rPriceM);
        mePost.Post;

        mePost.Next;
        inc(iC);
        if iC mod 100 =0 then
        begin
          Memo1.Lines.Add('  ���������� - '+its(iC));
          delay(10);
        end;
      end;

      Memo1.Lines.Add('������ ��'); delay(10);
    finally
      ViewAssPost.EndUpdate;
    end;
  end;

end;

procedure TfmAssPost.cxLabel6Click(Sender: TObject);
begin
  if MessageDlg('�������� ����������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������...'); delay(10);
      ViewAssPost.BeginUpdate;

      CloseTe(mePost);

      Memo1.Lines.Add('������ ��'); delay(10);
    finally
      ViewAssPost.EndUpdate;
    end;
  end;
end;

procedure TfmAssPost.cxLabel2Click(Sender: TObject);
begin
  if mePost.RecordCount>0 then
  begin
    Memo1.Lines.Add('  ������  - '+mePostName.AsString+'('+its(mePostCode.AsInteger)+')');
    mePost.Delete;
    delay(10);
  end;
end;

procedure TfmAssPost.acDelPosExecute(Sender: TObject);
begin
  if (mePost.RecordCount>0)and(CommonSet.Single=1) then
  begin
    Memo1.Lines.Add('  ������  - '+mePostName.AsString+'('+its(mePostCode.AsInteger)+')');
    mePost.Delete;
    delay(10);
  end;
end;

procedure TfmAssPost.acDelAllExecute(Sender: TObject);
begin
  if CommonSet.Single=1 then
  begin
    if MessageDlg('�������� ����������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      try
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������...'); delay(10);
        ViewAssPost.BeginUpdate;

        CloseTe(mePost);

        Memo1.Lines.Add('������ ��'); delay(10);
      finally
        ViewAssPost.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmAssPost.acAddListExecute(Sender: TObject);
begin
//�������� ������
  if CommonSet.Single=1 then
  begin
    iDirect:=21;
    fmCards.Show;
  end;
end;

procedure TfmAssPost.ViewAssPostDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=21 then  Accept:=True;
end;

procedure TfmAssPost.ViewAssPostDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
//
  if (iDirect=21)and(CommonSet.Single=1) then
  begin
    iDirect:=0; //������ ���������

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ����������� ����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMT do
        begin
          ViewAssPost.BeginUpdate;
          if mePost.Active=False then CloseTe(mePost);
          
          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            if taCards.FindKey([iNum]) then
            begin
              if taCardsStatus.AsInteger<100 then
              begin
                if mePost.Locate('Code',iNum,[])= False then
                begin
                  mePost.Append;
                  mePostCode.AsInteger:=iNum;
                  mePostName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                  mePostEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                  mePostPriceDog.AsFloat:=0;
                  mePostPricePP.AsFloat:=0;
                  mePostNameFull.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                  mePostsMess.AsString:='';
                  mePost.Post;
                end;
              end;
            end;
          end;

          ViewAssPost.EndUpdate;
          ViewAssPost.Controller.FocusRecord(ViewAssPost.DataController.FocusedRowIndex,True);
          GridAssPost.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TfmAssPost.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAssPost.acRecalcAssPostExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    dCurD:TDateTime;
    iC:Integer;
begin
  if CommonSet.Single=1 then
  begin
    if (cxButtonEdit1.Tag>0)and(Label1.Tag>0) then
    begin
      if MessageDlg('�������� � ����������� ���������� ������ �� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMT do
        begin
          try
            ViewAssPost.BeginUpdate;

            if mePost.Active=False then closete(mePost);

            ptTTNIn.Active:=False; ptTTNIn.Active:=True;
            ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';
            ptInLn.Active:=False; ptINLn.Active:=True;

            DateB:=Trunc(date-200);
            DateE:=Trunc(date);
            Memo1.Clear;
            Memo1.Lines.Add(fmt+'����� ���� ������.');

            for iCurD:=DateB to DateE do
            begin
              Memo1.Lines.Add(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
              dCurD:=iCurD;
              ptTTNIn.CancelRange;
              ptTTNIn.SetRange([dCurD],[dCurD]);
              ptTTNIn.First;     iC:=0;
              while not ptTTNIn.Eof do
              begin
                if (ptTTNInIndexPost.AsInteger=Label1.Tag)and(ptTTNInCodePost.AsInteger=cxButtonEdit1.Tag) then
                begin
                  ptInLn.CancelRange;
                  ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
                  ptInLn.First;
                  while not ptInLn.Eof do
                  begin
                    if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
                    begin
                      if taCardsStatus.AsInteger<100 then
                      begin
                        if mePost.Locate('Code',ptInLnCodeTovar.AsInteger,[])= False then
                        begin
                          mePost.Append;
                          mePostCode.AsInteger:=ptInLnCodeTovar.AsInteger;
                          mePostName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                          mePostEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                          mePostPriceDog.AsFloat:=0;
                          mePostPricePP.AsFloat:=0;
                          mePostNameFull.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                          mePostsMess.AsString:='';
                          mePost.Post;
                        end;
                      end;
                    end;

                    ptInLn.Next;
                  end;
                end;
                ptTTNIn.Next; inc(iC);
                if iC mod 100 = 0 then
                begin
                  StatusBar1.Panels[0].Text:=its(iC);
                  delay(100);
                end;
              end;
            end;
            Memo1.Lines.Add(fmt+'������ ��.');
          finally
            ViewAssPost.EndUpdate;
          end;
        end;
      end;
    end else ShowMessage('�������� ���������� ...');
  end;
end;

procedure TfmAssPost.cxButton1Click(Sender: TObject);
begin
  //��������� �����������
  if CommonSet.Single=1 then
  begin
    if (cxButtonEdit1.Tag>0)and(Label1.Tag>0) then
    begin
      if MessageDlg('��������� ����������� ����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmMT do
        begin
          try
            ViewAssPost.BeginUpdate;
            Memo1.Clear;
            Memo1.Lines.Add(fmt+'����� ���� ����������.');

            if ptGdsCli.Active=False then ptGdsCli.Active:=True;
            ptGdsCli.Refresh;

            ptGdsCli.CancelRange;
            ptGdsCli.SetRange([Label1.Tag,cxButtonEdit1.Tag],[Label1.Tag,cxButtonEdit1.Tag]);
            ptGdsCli.First;
            while not ptGdsCli.Eof do
            begin
              if mePost.Locate('Code',ptGdsCliGoodsId.AsInteger,[]) then ptGdsCli.Next
              else
              begin
                Memo1.Lines.Add(fmt+'   ���. '+its(ptGdsCliGoodsId.AsInteger));
                ptGdsCli.Delete;
                delay(10);
              end;
            end;

            mePost.First;
            while not mePost.Eof do
            begin
              if ptGdsCli.FindKey([Label1.Tag,cxButtonEdit1.Tag,mePostCode.AsInteger])=False then
              begin
                Memo1.Lines.Add(fmt+'   ���. '+its(mePostCode.AsInteger));

                ptGdsCli.Append;
                ptGdsCliClientTypeId.AsInteger:=Label1.Tag;
                ptGdsCliClientId.AsInteger:=cxButtonEdit1.Tag;
                ptGdsCliGoodsId.AsInteger:=mePostCode.AsInteger;
                ptGdsCliPriceIn.AsFloat:=mePostPriceDog.AsFloat;
                ptGdsCliQuantIn.AsFloat:=0;
                ptGdsClixDate.AsDateTime:=date;
                ptGdsClisMess.AsString:=copy(mePostsMess.AsString,1,50);
                ptGdsCli.Post;

                delay(10);
              end;

              mePost.Next;
            end;

            Memo1.Lines.Add(fmt+'���������� ��.');

          finally
            ViewAssPost.EndUpdate;
          end;
        end;
      end;
    end else ShowMessage('�������� ���������� ...');
  end;
end;

end.
