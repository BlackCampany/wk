unit AddCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxCheckBox,
  ActnList, XPStyleActnCtrls, ActnMan, cxGraphics, cxMaskEdit,
  cxDropDownEdit;

type
  TfmAddCli = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label4: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label5: TLabel;
    cxTextEdit5: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    Label7: TLabel;
    cxTextEdit6: TcxTextEdit;
    cxTextEdit7: TcxTextEdit;
    Label6: TLabel;
    Label8: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label9: TLabel;
    cxTextEdit9: TcxTextEdit;
    Label10: TLabel;
    cxTextEdit10: TcxTextEdit;
    Label11: TLabel;
    cxTextEdit11: TcxTextEdit;
    Label12: TLabel;
    cxTextEdit12: TcxTextEdit;
    amAddCli: TActionManager;
    acESC: TAction;
    Label13: TLabel;
    cxTextEdit13: TcxTextEdit;
    Label14: TLabel;
    cxComboBox1: TcxComboBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    cxComboBox2: TcxComboBox;
    procedure FormShow(Sender: TObject);
    procedure acESCExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddCli: TfmAddCli;

implementation

{$R *.dfm}

procedure TfmAddCli.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
end;

procedure TfmAddCli.acESCExecute(Sender: TObject);
begin
  close;
end;

end.
