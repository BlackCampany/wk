unit Menu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, Placemnt,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridCardView, cxGridDBCardView, ActnList, XPStyleActnCtrls, ActnMan,
  Menus, cxDataStorage, cxTextEdit, cxContainer;

type
  TfmMenuClass = class(TForm)
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    Button3: TcxButton;
    LevelM: TcxGridLevel;
    GridM: TcxGrid;
    ViewM: TcxGridDBCardView;
    ViewMINFO: TcxGridDBCardViewRow;
    ViewMTREETYPE: TcxGridDBCardViewRow;
    ViewMSPRICE: TcxGridDBCardViewRow;
    am1: TActionManager;
    acUpUp: TAction;
    acUp: TAction;
    acExit: TAction;
    acOpenSelMenu: TAction;
    acSelOk: TAction;
    Label1: TLabel;
    acMod1: TAction;
    cxButton1: TcxButton;
    acOpenAll: TAction;
    ViewMPRNREST: TcxGridDBCardViewRow;
    ViewMBACKBGR: TcxGridDBCardViewRow;
    Panel2: TPanel;
    cxButton14: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acUpUpExecute(Sender: TObject);
    procedure acUpExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acOpenSelMenuExecute(Sender: TObject);
    procedure ViewMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acMod1Execute(Sender: TObject);
    procedure acOpenAllExecute(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  fmMenuClass: TfmMenuClass;
  Sifr:Integer;

implementation

uses Dm, Un1, Calc, Modif, Spec;

{$R *.dfm}

procedure TfmMenuClass.FormCreate(Sender: TObject);
begin
  GridM.Align:=AlClient;
  FormPlacement1.Active:=False;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewM.RestoreFromIniFile(CurDir+GridIni,False);
  if CommonSEt.ButtonAddAllVisible=0 then cxButton1.Visible:=False;
end;

procedure TfmMenuClass.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ViewM.StoreToIniFile(CurDir+GridIni);
end;

procedure TfmMenuClass.ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
  sType:String;
  sTestRemn,sRemn:String;
  iTestRemn:INteger;
  rRemn:Real;
begin
  if (AViewInfo is TcxGridCardRowDataViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;

  sType := VarAsType(AViewInfo.GridRecord.DisplayTexts[1], varString);
  if sType='T' then
  begin
    ACanvas.Canvas.Brush.Color:=$00CDAD98;
  end
  else
  begin
//  ACanvas.Canvas.Brush.Bitmap := ABitmap;
  end;

  sTestRemn := VarAsType(AViewInfo.GridRecord.DisplayTexts[3], varString);

  iTestRemn:=StrToINtDef(sTestRemn,0);
  if iTestRemn=1 then
  begin

    sRemn := VarAsType(AViewInfo.GridRecord.DisplayTexts[4], varString);
    rRemn:=StrToFloatDef(sRemn,0);
    if rRemn>0.01 then
    begin
      ACanvas.Canvas.Brush.Color:=$00DDBBFF;
    end else
    begin
      ACanvas.Canvas.Brush.Color:=$008080FF;
    end;
  end;

  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmMenuClass.acUpUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    prFormMenuList(0);
    quMenu.First;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMenuClass.acUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    taMenu.Active:=True;
    if taMenu.Locate('SIFR',quMenuPARENT.AsInteger,[]) and (quMenuPARENT.AsInteger<>0) then
    begin
      prFormMenuList(taMenuPARENT.AsInteger);

      quMenu.First;
    end;
    taMenu.Active:=False;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMenuClass.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMenuClass.FormShow(Sender: TObject);
begin
  if GridM.Tag=0 then //1024x768
  begin
    Top:=7;
    Left:=447;
  end;
  if GridM.Tag=1 then //800x600
  begin
    Top:=7;
    Left:=350;
  end;
  cxTextEdit1.Text:='';
  cxTextEdit1.Tag:=0;
  GridM.SetFocus;
end;

procedure TfmMenuClass.acOpenSelMenuExecute(Sender: TObject);
Var iParent:Integer;
    rQ,cSum,cSumD:Real;
    bAddPos:Boolean;
begin
  with dmC do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      prFormMenuList(iParent);

      quMenu.First;
//      delay(200);   //� ������� ��������� - ����� ���� First

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin
//    ModalResult:=mrOk;
//1      bBludoSel:=True;
//1      Close;
      if Sifr>0 then  quMenu.Locate('SIFR',sifr,[]);
      if quMenuSIFR.AsInteger<>Sifr then exit;

      bAddPos:=True;
      if quMenuPRNREST.AsInteger=1 then
      begin
        if quMenuBACKBGR.AsFloat>0.01 then
        begin
          bAddPos:=True;
          fmSpec.ViewSpec.Tag:=1; //����� ����������������� ������� ����� �����������
        end
        else bAddPos:=False;
      end;

      if bAddPos then
      begin

      fmSpec.ViewSpec.BeginUpdate;

      try
        if (quCurSpecSifr.AsInteger=quMenuSIFR.AsInteger)and(quCurSpecIStatus.AsInteger=0)and(quCurSpecQUEST.AsInteger=fmSpec.cxImageComboBox1.EditValue) then
        begin //������ +1
          rQ:=quCurSpecQuantity.AsFloat+1;

          CalcDiscontN(quMenuSIFR.AsInteger,Check.Max,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuPRICE.AsFloat*rQ-Check.DSum);
          cSumD:=RoundVal(quMenuPRICE.AsFloat*rQ)-cSum;

          quCurSpec.Edit;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuNAME.AsString;
          quCurSpecCode.AsString:=quMenuCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
          if quMenuLIMITPRICE.AsFloat>0 then
           quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
          quCurSpecQUEST.AsInteger:=fmSpec.cxImageComboBox1.EditValue;
          quCurSpec.Post;

        end
        else //�����
        begin
          rQ:=1;
          if quMenuDESIGNSIFR.AsInteger>0 then
          begin
            bAddPosFromMenu:= True;
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
            begin
              rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
            end else rQ:=0;
            bAddPosFromMenu:= False;
          end;

          if rQ>0 then
          begin
            CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

            cSum:=RoundVal(quMenuPRICE.AsFloat*rQ-Check.DSum);
            cSumD:=RoundVal(quMenuPRICE.AsFloat*rQ)-cSum;

            quCurSpec.Append;
            quCurSpecSTATION.AsInteger:=CommonSet.Station;
            quCurSpecID_TAB.AsInteger:=Tab.Id;
            quCurSpecId_Personal.AsInteger:=Person.Id;
            quCurSpecNumTable.AsString:=Tab.NumTable;
            quCurSpecId.AsInteger:=Check.Max+1;
            quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
            quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
            quCurSpecQuantity.AsFloat:=rQ;
            quCurSpecSumma.AsFloat:=cSum;
            quCurSpecDProc.AsFloat:=Check.DProc;
            quCurSpecDSum.AsFloat:=cSumD;
            quCurSpecIStatus.AsInteger:=0;
            quCurSpecName.AsString:=quMenuNAME.AsString;
            quCurSpecCode.AsString:=quMenuCODE.AsString;
            quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
            if quMenuLIMITPRICE.AsFloat>0 then
             quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
            else quCurSpecLimitM.AsInteger:=99;
            quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
            quCurSpecQUEST.AsInteger:=fmSpec.cxImageComboBox1.EditValue;
            quCurSpec.Post;
            quCurSpec.Refresh;

            inc(Check.Max);

            if (quMenuLINK.AsInteger>0)or(cxTextEdit1.Tag>0) then
            begin
              quCurMod.Active:=False;
              quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
              quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
              quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
              quCurMod.Active:=True;
            end;

            if quMenuLINK.AsInteger>0 then
            begin //������������
              acMod1.Execute;
            end;
            if cxTextEdit1.Tag>0 then //���������
            begin
              quCurMod.Append;
              quCurModID_TAB.AsInteger:=quCurSpecID_TAB.AsInteger;
              quCurModSTATION.AsInteger:=CommonSet.Station;
              quCurModId_Pos.AsInteger:=quCurSpecID.AsInteger;
              quCurModId.AsInteger:=Check.Max+1;
              quCurModSifr.AsInteger:=cxTextEdit1.Tag*(-1);
              quCurModName.AsString:=cxTextEdit1.Text;
              quCurModQuantity.AsFloat:=0;
              quCurMod.Post;

              inc(Check.Max);
            end;
          end;
        end;
//      GridSpec.SetFocus;
        if Check.DSum>0 then
        begin
          fmSpec.ViewSpecDPROC.Visible:=True;
          fmSpec.ViewSpecDSum.Visible:=True;
        end;
      finally
        fmSpec.ViewSpec.EndUpdate;
      end;

      end; // if bAddPos
    end;
  end;
end;

procedure TfmMenuClass.ViewMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
begin
  AHandled:=True;
  Sifr:=-1;
  StrWk:= ACellViewInfo.Value;
  if Pos('|',StrWk)>0 then
  begin
    Delete(StrWk,1,Pos('|',StrWk));
    Sifr:=StrToIntDef(StrWk,-1);
  end;
  acOpenSelMenu.Execute;
end;

procedure TfmMenuClass.acMod1Execute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModif:=TFmModif.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=quCurMod.RecordCount;

    fmModif.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModif.ShowModal;
    fmModif.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmMenuClass.acOpenAllExecute(Sender: TObject);
Var rQ,cSum,cSumD,rSumDAll:Real;
begin
  //�������� ���
  with dmC do
  begin
    try
      rSumDAll:=0;
      rQ:=1;

      ViewM.BeginUpdate;
      fmSpec.ViewSpec.BeginUpdate;
      quMenu.First;
      while not quMenu.Eof do
      begin
        if quMenuTREETYPE.AsString<>'T' then
        begin
          CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuPRICE.AsFloat*rQ-Check.DSum);
          cSumD:=RoundVal(quMenuPRICE.AsFloat*rQ)-cSum;
          rSumDAll:=rSumDAll+Check.DSum;

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=Tab.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
          quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuNAME.AsString;
          quCurSpecCode.AsString:=quMenuCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
          if quMenuLIMITPRICE.AsFloat>0 then
          quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
          quCurSpecQUEST.AsInteger:=fmSpec.cxImageComboBox1.EditValue;
          quCurSpec.Post;
          inc(Check.Max);
        end;

        quMenu.Next;
      end;
      if rSumDAll>0 then
      begin
        fmSpec.ViewSpecDPROC.Visible:=True;
        fmSpec.ViewSpecDSum.Visible:=True;
      end;

    finally
      fmSpec.ViewSpec.EndUpdate;
      ViewM.EndUpdate;
    end;
  end;
end;

procedure TfmMenuClass.cxButton14Click(Sender: TObject);
begin
  with dmC do
  begin
    if taMes.Active=False then taMes.Active:=True;
    taMes.FullRefresh;

    fmModif:=TFmModif.Create(Application);

    fmModif.Tag:=1;
    fmModif.Label1.Caption:='';
    fmModif.Caption:='���������.';
    fmModif.LevelMo.Visible:=False;
    fmModif.LevelMe.Visible:=True;

    fmModif.ShowModal;
    if fmModif.ModalResult=mrOk then
    begin
      if taMes.RecordCount>0 then
      begin
        cxTextEdit1.Tag:=taMesID.AsInteger;
        cxTextEdit1.Text:=taMesMESSAG.AsString;
      end;
    end;
    fmModif.Release;
  end;  
end;

procedure TfmMenuClass.cxButton2Click(Sender: TObject);
begin
  cxTextEdit1.Tag:=0;
  cxTextEdit1.Text:='';
end;

end.
