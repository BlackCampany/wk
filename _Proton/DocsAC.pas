unit DocsAC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocs3 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsAC: TcxGrid;
    ViewDocsAC: TcxGridDBTableView;
    LevelDocsAC: TcxGridLevel;
    amDocsAC: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewDocsACDepart: TcxGridDBColumn;
    ViewDocsACDateAct: TcxGridDBColumn;
    ViewDocsACNumber: TcxGridDBColumn;
    ViewDocsACAcStatus: TcxGridDBColumn;
    ViewDocsACID: TcxGridDBColumn;
    ViewDocsACNameDep: TcxGridDBColumn;
    ViewDocsACMatOldSum: TcxGridDBColumn;
    ViewDocsACMatNewSum: TcxGridDBColumn;
    ViewDocsACMatRaznica: TcxGridDBColumn;
    ViewDocsACChekBuh: TcxGridDBColumn;
    acAdd1: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsACDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acAdd1Execute(Sender: TObject);
    procedure ViewDocsACCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc3(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs3: TfmDocs3;
  bClearDocIn:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, AddDoc3, MT;

{$R *.dfm}

procedure TfmDocs3.prSetValsAddDoc3(iT:SmallINt); //0-����������, 1-��������������, 2-��������
Var iNum:INteger;
begin
  with dmMC do
  begin
    bOpen:=True;
    if iT=0 then
    begin
      iNum:=prFindNum(4)+1;

      fmAddDoc3.Caption:='���������: ����������.';

      if iNum>0 then
      begin
        fmAddDoc3.cxTextEdit1.Text:=its(iNum);
        fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc3.cxTextEdit1.Tag:=0;
        fmAddDoc3.Label1.Tag:=iNum;
      end else
      begin
        fmAddDoc3.cxTextEdit1.Text:='';
        fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc3.cxTextEdit1.Tag:=0;
      end;

      fmAddDoc3.cxDateEdit1.Date:=date;
      fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc3.cxTextEdit10.Text:='';
      fmAddDoc3.cxTextEdit10.Tag:=0;
      fmAddDoc3.cxDateEdit10.Date:=0;
      fmAddDoc3.cxDateEdit10.Tag:=0;


      quDeparts.Active:=False; quDeparts.Active:=True; quDeparts.First;

      fmAddDoc3.cxLookupComboBox1.EditValue:=quDepartsID.AsInteger;
      fmAddDoc3.cxLookupComboBox1.Text:=quDepartsName.AsString;
      fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc3.cxLabel1.Enabled:=True;
      fmAddDoc3.cxLabel2.Enabled:=True;
      fmAddDoc3.cxLabel3.Enabled:=True;
      fmAddDoc3.cxLabel4.Enabled:=True;
      fmAddDoc3.cxButton1.Enabled:=True;

      fmAddDoc3.ViewDoc3.OptionsData.Editing:=True;
      fmAddDoc3.ViewDoc3.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmAddDoc3.Caption:='���������: ��������������.';
      fmAddDoc3.cxTextEdit1.Text:=quTTnAcNumber.AsString;
      fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc3.cxTextEdit1.Tag:=quTTnAcID.AsInteger;

      fmAddDoc3.cxTextEdit10.Text:=quTTnAcNumber.AsString;
      fmAddDoc3.cxTextEdit10.Tag:=quTTnAcID.AsInteger;
      fmAddDoc3.cxDateEdit10.Date:=quTTnAcDateAct.AsDateTime;
      fmAddDoc3.cxDateEdit10.Tag:=quTTnAcDepart.AsInteger;

      fmAddDoc3.cxDateEdit1.Date:=quTTnAcDateAct.AsDateTime;
      fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc3.cxLookupComboBox1.EditValue:=quTTnAcDepart.AsInteger;
      fmAddDoc3.cxLookupComboBox1.Text:=quTTnAcNameDep.AsString;
      fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc3.cxLabel1.Enabled:=True;
      fmAddDoc3.cxLabel2.Enabled:=True;
      fmAddDoc3.cxLabel3.Enabled:=True;
      fmAddDoc3.cxLabel4.Enabled:=True;
      fmAddDoc3.cxButton1.Enabled:=True;

      fmAddDoc3.ViewDoc3.OptionsData.Editing:=True;
      fmAddDoc3.ViewDoc3.OptionsData.Deleting:=True;
    end;
    if iT=2 then
    begin
      fmAddDoc3.Caption:='���������: ��������.';
      fmAddDoc3.cxTextEdit1.Text:=quTTnAcNumber.AsString;
      fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc3.cxTextEdit1.Tag:=quTTnAcID.AsInteger;

      fmAddDoc3.cxTextEdit10.Text:=quTTnAcNumber.AsString;
      fmAddDoc3.cxTextEdit10.Tag:=quTTnAcID.AsInteger;
      fmAddDoc3.cxDateEdit10.Date:=quTTnAcDateAct.AsDateTime;
      fmAddDoc3.cxDateEdit10.Tag:=quTTnAcDepart.AsInteger;

      fmAddDoc3.cxDateEdit1.Date:=quTTnAcDateAct.AsDateTime;
      fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=True;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc3.cxLookupComboBox1.EditValue:=quTTnAcDepart.AsInteger;
      fmAddDoc3.cxLookupComboBox1.Text:=quTTnAcNameDep.AsString;
      fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc3.cxLabel1.Enabled:=False;
      fmAddDoc3.cxLabel2.Enabled:=False;
      fmAddDoc3.cxLabel3.Enabled:=False;
      fmAddDoc3.cxLabel4.Enabled:=False;
      fmAddDoc3.cxButton1.Enabled:=False;

      fmAddDoc3.ViewDoc3.OptionsData.Editing:=False;
      fmAddDoc3.ViewDoc3.OptionsData.Deleting:=False;
    end;
    bOpen:=False;
  end;
end;

procedure TfmDocs3.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs3.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs3.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsAC.Align:=AlClient;
  ViewDocsAC.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsAC.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs3.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelDocsAC.Visible then
      begin
        fmDocs3.Caption:='��������� ���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs3.ViewDocsAC.BeginUpdate;
        try
          quTTnAc.Active:=False;
          quTTnAc.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTnAc.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTnAc.Active:=True;
        finally
          fmDocs3.ViewDocsAC.EndUpdate;
        end;

      end else
      begin
        fmDocs3.Caption:='��������� ���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        ViewCards.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
        quDocsInCard.Active:=True;}
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocs3.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc3(0); //0-����������, 1-��������������, 2-��������

    CloseTa(fmAddDoc3.taSpecAc);
    fmAddDoc3.acSaveDoc.Enabled:=True;
    fmAddDoc3.Show;
  end;
end;

procedure TfmDocs3.acEditDoc1Execute(Sender: TObject);
Var //IDH:INteger;
//    rSum1,rSum2:Real;
    iC:INteger;
    Proc:Real;
begin
  //�������������
  if not CanDo('prEditDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnAc.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnAcAcStatus.AsInteger=0 then
      begin
        if quTTnAcDateAct.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnAcDepart.AsInteger,Trunc(quTTnAcDateAct.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        prSetValsAddDoc3(1); //0-����������, 1-��������������, 2-��������

        CloseTa(fmAddDoc3.taSpecAc);

        fmAddDoc3.acSaveDoc.Enabled:=True;

//        IDH:=quTTnAcID.AsInteger;

        quSpecAc.Active:=False;
        quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
        quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
        quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
        quSpecAc.Active:=True;

        quSpecAc.First;
        iC:=1;
        while not quSpecAc.Eof do
        begin
          with fmAddDoc3 do
          begin
            taSpecAc.Append;
            taSpecAcNum.AsInteger:=iC;
            taSpecAcCodeTovar.AsInteger:=quSpecAcTovar.AsInteger;
            taSpecAcCodeEdIzm.AsInteger:=quSpecAcEdIzm.AsInteger;
            taSpecAcBarCode.AsString:=quSpecAcBarCode.AsString;
            taSpecAcNDSProc.AsFloat:=0;
            taSpecAcKol.AsFloat:=quSpecAcKol.AsFloat;
            taSpecAcName.AsString:=quSpecAcName.AsString;
            taSpecAcFullName.AsString:=quSpecAcFullName.AsString;
            taSpecAcCena.AsFloat:=quSpecAcMatCenaOldSum.AsFloat;
            taSpecAcNewCena.AsFloat:=quSpecAcMatCenaNewSum.AsFloat;
            taSpecAcRazn.AsFloat:=quSpecAcMatCenaRaznica.AsFloat;

            Proc:=0;
            if quSpecAcMatCenaOldSum.AsFloat<>0 then
            begin
              Proc:=quSpecAcMatCenaRaznica.AsFloat/quSpecAcMatCenaOldSum.AsFloat*100;
            end;
            taSpecAcProc.AsFloat:=Proc;

            taSpecAcSumCena.AsFloat:=rv(quSpecAcMatSumOldSum.AsFloat);
            taSpecAcSumNewCena.AsFloat:=rv(quSpecAcMatSumNewSum.AsFloat);
            taSpecAcSumRazn.AsFloat:=rv(quSpecAcMatSumRaznica.AsFloat);
            taSpecAc.Post;
          end;
          quSpecAc.Next; inc(iC);
        end;
        fmAddDoc3.prSetNac;
        fmAddDoc3.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs3.acViewDoc1Execute(Sender: TObject);
Var iC:INteger;
    Proc:Real;
begin
  //��������
  if not CanDo('prViewDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnAc.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc3(2); //0-����������, 1-��������������, 2-��������

      CloseTa(fmAddDoc3.taSpecAc);

      fmAddDoc3.acSaveDoc.Enabled:=True;

//        IDH:=quTTnAcID.AsInteger;

      quSpecAc.Active:=False;
      quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
      quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
      quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
      quSpecAc.Active:=True;

      quSpecAc.First;
      iC:=1;
      while not quSpecAc.Eof do
      begin
        with fmAddDoc3 do
        begin
          taSpecAc.Append;
          taSpecAcNum.AsInteger:=iC;
          taSpecAcCodeTovar.AsInteger:=quSpecAcTovar.AsInteger;
          taSpecAcCodeEdIzm.AsInteger:=quSpecAcEdIzm.AsInteger;
          taSpecAcBarCode.AsString:=quSpecAcBarCode.AsString;
          taSpecAcNDSProc.AsFloat:=0;
          taSpecAcKol.AsFloat:=quSpecAcKol.AsFloat;
          taSpecAcName.AsString:=quSpecAcName.AsString;
          taSpecAcFullName.AsString:=quSpecAcFullName.AsString;
          taSpecAcCena.AsFloat:=quSpecAcMatCenaOldSum.AsFloat;
          taSpecAcNewCena.AsFloat:=quSpecAcMatCenaNewSum.AsFloat;
          taSpecAcRazn.AsFloat:=quSpecAcMatCenaRaznica.AsFloat;

          Proc:=0;
          if quSpecAcMatCenaOldSum.AsFloat<>0 then
          begin
            Proc:=quSpecAcMatCenaRaznica.AsFloat/quSpecAcMatCenaOldSum.AsFloat*100;
          end;
          taSpecAcProc.AsFloat:=Proc;

          taSpecAcSumCena.AsFloat:=rv(quSpecAcMatSumOldSum.AsFloat);
          taSpecAcSumNewCena.AsFloat:=rv(quSpecAcMatSumNewSum.AsFloat);
          taSpecAcSumRazn.AsFloat:=rv(quSpecAcMatSumRaznica.AsFloat);
          taSpecAc.Post;
        end;
        quSpecAc.Next; inc(iC);
      end;
      fmAddDoc3.prSetNac;
      fmAddDoc3.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs3.ViewDocsACDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quTTnAcAcStatus.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocs3.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnAc.RecordCount>0 then //���� ��� �������
    begin
      if quTTnAcAcStatus.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��� �'+quTTnAcNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(12,quTTnAcID.AsInteger,trunc(quTTnAcDateAct.AsDateTime),quTTnAcNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNOvrLn"');
          quD.SQL.Add('where Depart='+its(quTTnAcDepart.AsInteger));
          quD.SQL.Add('and DateAct='''+ds(quTTnAcDateAct.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnAcNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTnOvr"');
          quD.SQL.Add('where Depart='+its(quTTnAcDepart.AsInteger));
          quD.SQL.Add('and DateAct='''+ds(quTTnAcDateAct.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnAcNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          prRefrID(quTTnAc,ViewDocsAC,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs3.acOnDoc1Execute(Sender: TObject);
Var bOpr:Boolean;
begin
//������������
  with dmMC do
  begin
    if not CanDo('prOnDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnAc.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnAcAcStatus.AsInteger=0 then
      begin
        if quTTnAcDateAct.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnAcDepart.AsInteger,Trunc(quTTnAcDateAct.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quTTnAcNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {if prTOFind(Trunc(quTTnAcDateAct.AsDateTime),quTTnAcDepart.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnAcNameDep.AsString+' � '+FormatDateTime('dd.mm.yyyy',quTTnAcDateAct.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnAcDateAct.AsDateTime),quTTnAcDepart.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem7.Enabled:=True;
              exit;
            end;
          end;}
          // ��������� �� 2010-06-16----- ������
           {     quA.Active:=false;
                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>'''+formatDateTime('yyyy-mm-dd',quTTnAcDateAct.AsDateTime)+'''');
                quA.SQL.Add('and Detail=0');
                quA.SQL.Add('and Depart='+quTTnAcDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                  if (quA.RecordCount>0) then
                    begin
                      ShowMessage('������������� �� ���������, ��������� ������ ��������������');
                      quA.Active:=false;
                      exit;
                    end;
                quA.Active:=false;

                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>='''+formatDateTime('yyyy-mm-dd',quTTnAcDateAct.AsDateTime)+'''');
                quA.SQL.Add('and Detail>0');
                quA.SQL.Add('and Depart='+quTTnAcDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                CloseTe(dmMT.teInventryArt);
                dmMT.teInventryArt.Active:=true;
                  while not quA.Eof do
                    begin
                      quB.Active:=False;
                      quB.SQL.Clear;
                      quB.SQL.Add('select Item from "InventryLine"');
                      quB.SQL.Add('where Inventry ='+quA.FieldByName('Inventry').AsString);
                      quB.SQL.Add('and Depart='+quTTnAcDepart.AsString);

                      quB.Active:=True;
                      while not quB.Eof do
                        begin
                          dmMT.teInventryArt.Append;
                          dmMT.teInventryArtArticul.AsInteger:=quB.fieldbyName('Item').AsInteger;
                          dmMT.teInventryArt.Post;
                          quB.Next
                        end;
                      quB.Active:=False;
                      quA.Next;
                    end;

                quA.Active:=false;

                quSpecAc.Active:=False;
                quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
                quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
                quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
                quSpecAc.Active:=True;

                quSpecAc.First;
                while not quSpecAc.Eof do
                  begin
                    if dmMT.teInventryArt.Locate('Articul',quSpecAc.fieldbyname('Tovar').AsInteger,[]) then
                      begin
                        showMessage('������������� �� ��������, ��������� ��������� ��������������');
                        exit;
                      end;
                    quSpecAc.Next;
                  end;

                quSpecAc.Active:=False;
                dmMT.teInventryArt.Active:=false;
          //----------------------------- ������
             }
          prButtonSet(False);

          prAddHist(12,quTTnAcID.AsInteger,trunc(quTTnAcDateAct.AsDateTime),quTTnAcNumber.AsString,'�����.',1);

          quSpecAc.Active:=False;
          quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
          quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
          quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
          quSpecAc.Active:=True;

          bOpr:=True;
          if NeedPost(trunc(quTTnAcDateAct.AsDateTime)) then
          begin
            iNumPost:=PostNum(quTTNACID.AsInteger)*100+3;
            prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'TTNAC;ON;'+its(quTTnACDepart.AsInteger)+r+its(quTTnACID.AsInteger)+r+ds(quTTNACDateAct.AsDateTime)+r+quTTnACNumber.AsString+r+fs(quTTNACMatOldSum.AsFloat)+r+fs(quTTNACMatNewSum.AsFloat)+r+fs(quTTNACMatRaznica.AsFloat)+r);

              quSpecAc.First;
              while not quSpecAc.Eof do
              begin
                StrPost:='TTNACLN;ON;'+its(quTTnACDepart.AsInteger)+r+its(quTTnACID.AsInteger);
                StrPost:=StrPost+r+its(quSpecAcTovar.asINteger)+r+fs(quSpecAcKol.asfloat)+r+fs(quSpecAcMatCenaOldSum.asfloat)+r+fs(quSpecAcMatCenaNewSum.asfloat)+r+fs(quSpecAcMatCenaRaznica.asfloat)+r+fs(quSpecAcMatSumOldSum.asfloat)+r+fs(quSpecAcMatSumNewSum.asfloat)+r+fs(quSpecAcMatSumRaznica.asfloat)+r;
                writeln(fPost,StrPost);
                quSpecAc.Next;
              end;
            finally
              closefile(fPost);
            end;

            //������ ����� - ���� ��������
            bOpr:=prTr(sNumPost(iNumPost),Memo1);
          end;

          if bOpr then
          begin

//          prClearBuf(quTTnAcID.AsInteger,12,Trunc(quTTnAcDateAct.AsDateTime),1);

            quSpecAc.First;  //��������� ����� ���
            while not quSpecAc.Eof do
            begin
              if abs(quSpecAcMatCenaNewSum.AsFloat-quSpecAcMatCenaOldSum.AsFloat)>=0.01 then //
                prTPriceBuf(quSpecAcTovar.AsInteger,Trunc(quTTnAcDateAct.AsDateTime),quTTnAcID.AsInteger,12,quTTnAcNumber.AsString,quSpecAcMatCenaOldSum.AsFloat,quSpecAcMatCenaNewSum.AsFloat);
//              prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quTTnInNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

              quSpecAc.Next;
            end;
            quSpecAc.Active:=False;

            Memo1.Clear;
            prWH('����� ... ���� ������.',Memo1);

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "TTNOvr" Set AcStatus=3');
            quE.SQL.Add('where Depart='+its(quTTnAcDepart.AsInteger));
            quE.SQL.Add('and DateAct='''+ds(quTTnAcDateAct.AsDateTime)+'''');
            quE.SQL.Add('and Number='''+quTTnAcNumber.AsString+'''');
            quE.ExecSQL;

            prRefrID(quTTnAc,ViewDocsAC,0);

           //�������� ��
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnAcDateAct.AsDateTime))>=1 then prRecalcTO(trunc(quTTnAcDateAct.AsDateTime),trunc(Date)-1,nil,1,0);

            prWh('������ ��.',Memo1);
          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs3.acOffDoc1Execute(Sender: TObject);
Var bOpr:Boolean;
begin
//��������
  with dmMC do
  begin
    if not CanDo('prOffDocAc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTnAcDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    if quTTnAc.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnAcAcStatus.AsInteger=3 then
      begin
        if quTTnAcDateAct.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnAcDepart.AsInteger,Trunc(quTTnAcDateAct.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('�������� �������� �'+quTTnAcNumber.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {
          if prTOFind(Trunc(quTTnAcDATEDOC.AsDateTime),quTTnAcIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnAcNAMEMH.AsString+' � '+quTTnAcNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnAcDATEDOC.AsDateTime),quTTnAcIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem8.Enabled:=True;
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� �� ������� ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quTTnAcID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=1;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin //���� ��������, ��� �� ��������� ��������� � �������
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������.('+StrWk+') ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              //����� ������� - ���������� �������� � ������������ ������� ������


              bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quTTnAcDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            prWH('����� ... ���� ������.',Memo1); Delay(10);
           // 1 - �������� ��������� ������
           // 2 - ������� ��������� ������ �� ���������
           // 3 - �������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quTTnAcID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=1;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quTTnAcDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

           // 4 - �������� ������
            quTTnAc.Edit;
            quTTnAcAcStatus.AsInteger:=0;
            quTTnAc.Post;
            quTTnAc.Refresh;

            prWH('������ ��.',Memo1); Delay(10);
          end;}
          // ��������� �� 2010-06-16----- ������
           {     quA.Active:=false;
                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>'''+formatDateTime('yyyy-mm-dd',quTTnAcDateAct.AsDateTime)+'''');
                quA.SQL.Add('and Detail=0');
                quA.SQL.Add('and Depart='+quTTnAcDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                  if (quA.RecordCount>0) then
                    begin
                      ShowMessage('����� �� ��������, ��������� ������ ��������������');
                      quA.Active:=false;
                      exit;
                    end;
                quA.Active:=false;

                quA.SQL.Clear;
                quA.SQL.Add('select * from "InventryHead"');
                quA.SQL.Add('where DateInventry>='''+formatDateTime('yyyy-mm-dd',quTTnAcDateAct.AsDateTime)+'''');
                quA.SQL.Add('and Detail>0');
                quA.SQL.Add('and Depart='+quTTnAcDepart.AsString);
                quA.SQL.Add('and Status=''�''');

                quA.Active:=true;
                CloseTe(dmMT.teInventryArt);
                dmMT.teInventryArt.Active:=true;
                  while not quA.Eof do
                    begin
                      quB.Active:=False;
                      quB.SQL.Clear;
                      quB.SQL.Add('select Item from "InventryLine"');
                      quB.SQL.Add('where Inventry ='+quA.FieldByName('Inventry').AsString);
                      quB.SQL.Add('and Depart='+quTTnAcDepart.AsString);

                      quB.Active:=True;
                      while not quB.Eof do
                        begin
                          dmMT.teInventryArt.Append;
                          dmMT.teInventryArtArticul.AsInteger:=quB.fieldbyName('Item').AsInteger;
                          dmMT.teInventryArt.Post;
                          quB.Next
                        end;
                      quB.Active:=False;
                      quA.Next;
                    end;

                quA.Active:=false;

                quSpecAc.Active:=False;
                quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
                quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
                quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
                quSpecAc.Active:=True;

                quSpecAc.First;
                while not quSpecAc.Eof do
                  begin
                    if dmMT.teInventryArt.Locate('Articul',quSpecAc.fieldbyname('Tovar').AsInteger,[]) then
                      begin
                        showMessage('����� �� ��������, ��������� ��������� ��������������');
                        exit;
                      end;
                    quSpecAc.Next;
                  end;

                quSpecAc.Active:=False;
                dmMT.teInventryArt.Active:=false;
          //----------------------------- ������
          }
          prButtonSet(False);

          prAddHist(12,quTTnAcID.AsInteger,trunc(quTTnAcDateAct.AsDateTime),quTTnAcNumber.AsString,'�����.',0);

          quSpecAc.Active:=False;
          quSpecAc.ParamByName('IDEP').AsInteger:=quTTnAcDepart.AsInteger;
          quSpecAc.ParamByName('SDATE').AsDate:=Trunc(quTTnAcDateAct.AsDateTime);
          quSpecAc.ParamByName('SNUM').AsString:=quTTnAcNumber.AsString;
          quSpecAc.Active:=True;

          bOpr:=True;
          if NeedPost(trunc(quTTnAcDateAct.AsDateTime)) then
          begin
            iNumPost:=PostNum(quTTNACID.AsInteger)*100+3;
            prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'TTNAC;OF;'+its(quTTnACDepart.AsInteger)+r+its(quTTnACID.AsInteger)+r+ds(quTTNACDateAct.AsDateTime)+r+quTTnACNumber.AsString+r+fs(quTTNACMatOldSum.AsFloat)+r+fs(quTTNACMatNewSum.AsFloat)+r+fs(quTTNACMatRaznica.AsFloat)+r);

              quSpecAc.First;
              while not quSpecAc.Eof do
              begin
                StrPost:='TTNACLN;OF;'+its(quTTnACDepart.AsInteger)+r+its(quTTnACID.AsInteger);
                StrPost:=StrPost+r+its(quSpecAcTovar.asINteger)+r+fs(quSpecAcKol.asfloat)+r+fs(quSpecAcMatCenaOldSum.asfloat)+r+fs(quSpecAcMatCenaNewSum.asfloat)+r+fs(quSpecAcMatCenaRaznica.asfloat)+r+fs(quSpecAcMatSumOldSum.asfloat)+r+fs(quSpecAcMatSumNewSum.asfloat)+r+fs(quSpecAcMatSumRaznica.asfloat)+r;
                writeln(fPost,StrPost);
                quSpecAc.Next;
              end;
            finally
              closefile(fPost);
            end;

            //������ ����� - ���� ��������
            bOpr:=prTr(sNumPost(iNumPost),Memo1);
          end;

          if bOpr then
          begin
            Memo1.Clear;
            prWH('����� ... ���� ������.',Memo1);

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "TTNOvr" Set AcStatus=0');
            quE.SQL.Add('where Depart='+its(quTTnAcDepart.AsInteger));
            quE.SQL.Add('and DateAct='''+ds(quTTnAcDateAct.AsDateTime)+'''');
            quE.SQL.Add('and Number='''+quTTnAcNumber.AsString+'''');
            quE.ExecSQL;

            prRefrID(quTTnAc,ViewDocsAC,0);

            //�������� ��
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnAcDateAct.AsDateTime))>=1 then prRecalcTO(trunc(quTTnAcDateAct.AsDateTime),trunc(Date)-1,nil,1,0);

            prWh('������ ��.',Memo1);
          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs3.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocs3.acVidExecute(Sender: TObject);
begin
  //���
  with dmMC do
  begin
    {if LevelDocsAC.Visible then
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs3.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs3.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsAC.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs3.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs3.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsAC.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsAC.BeginUpdate;
      quTTnAc.Active:=False;
      quTTnAc.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quTTnAc.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quTTnAc.Active:=True;
      ViewDocsAC.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocs3.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs3.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsAC.Visible=False then exit;
  with dmMC do
  begin
    if quTTnAc.RecordCount>0 then //���� ��� �������������
    begin
      quSpecAc.Active:=False;
      quSpecAc.ParamByName('IDHD').AsInteger:=quTTnAcID.AsInteger;
      quSpecAc.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTnAcNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnAcNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnAcDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnAcNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecAc.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs3.acCopyExecute(Sender: TObject);
//var Par:Variant;
begin
  //����������
  with dmMC do
  begin
    {if quTTnAc.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnAcID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnAcID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnAcDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnAcNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quTTnAcIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnAcNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnAcIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnAcNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnAcSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnAcSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecAc.Active:=False;
      quSpecAc.ParamByName('IDHD').AsInteger:=quTTnAcID.AsInteger;
      quSpecAc.Active:=True;

      quSpecAc.First;
      while not quSpecAc.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnAcID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecAcNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecAcIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecAcQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecAcPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecAcSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecAcPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecAcSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecAcIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecAcSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecAcNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecAcSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecAcIDM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecAcIDM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecAcPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecAcSUMUCH.AsFloat;
        taSpecDoc.Post;

        quSpecAc.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocs3.acInsertDExecute(Sender: TObject);
{Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;}
begin
  // ��������
  with dmMC do
  begin
    {taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prAllViewOff;

          fmAddDoc3.Caption:='���������: ����� ��������.';
          fmAddDoc3.cxTextEdit1.Text:=prGetNum(1,0);
          fmAddDoc3.cxTextEdit1.Tag:=0;
          fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc3.cxTextEdit2.Text:='';
          fmAddDoc3.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc3.cxDateEdit1.Date:=Date;
          fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc3.cxDateEdit2.Date:=Date;
          fmAddDoc3.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc3.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc3.cxCurrencyEdit2.EditValue:=0;

          if taHeadDocIType.AsInteger=1 then
          begin
            fmAddDoc3.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc3.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc3.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc3.cxButtonEdit1.Tag:=0;
            fmAddDoc3.cxButtonEdit1.EditValue:=0;
            fmAddDoc3.cxButtonEdit1.Text:='';
          end;

          fmAddDoc3.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc3.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc3.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quDeparts.Active=False then quDeparts.Active:=True;
          quDeparts.FullRefresh;

          if quDeparts.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc3.Label15.Caption:='��. ����: '+quDepartsNAMEPRICE.AsString;
            fmAddDoc3.Label15.Tag:=quDepartsDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc3.Label15.Caption:='��. ����: ';
            fmAddDoc3.Label15.Tag:=0;
          end;

          fmAddDoc3.cxLabel1.Enabled:=True;
          fmAddDoc3.cxLabel2.Enabled:=True;
          fmAddDoc3.cxLabel3.Enabled:=True;
          fmAddDoc3.cxLabel4.Enabled:=True;
          fmAddDoc3.cxLabel5.Enabled:=True;
          fmAddDoc3.cxLabel6.Enabled:=True;
          fmAddDoc3.N1.Enabled:=True;

          fmAddDoc3.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc3.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc3.cxButton1.Enabled:=True;

          CloseTa(fmAddDoc3.taSpec);

          fmAddDoc3.ViewTara.OptionsData.Editing:=True;
          fmAddDoc3.ViewTara.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc3.taTaraS);

          fmAddDoc3.acSaveDoc.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc3 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecAcds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

     //     fmAddDoc3.ShowModal;
          fmAddDoc3.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocs3.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsAC.Visible then
  begin
    prNExportExel5(ViewDocsAC);
  end else
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocs3.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs3.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsAC);
end;

procedure TfmDocs3.acAdd1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc3(0); //0-����������, 1-��������������, 2-��������


    if FileExists(CurDir+'SpecAc.cds') then fmAddDoc3.taSpecAc.Active:=True
    else fmAddDoc3.taSpecAc.CreateDataSet;

    fmAddDoc3.acSaveDoc.Enabled:=True;
    fmAddDoc3.Show;
  end;
end;

procedure TfmDocs3.ViewDocsACCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsAC.ColumnCount-1 do
  begin
    if ViewDocsAC.Columns[i].Name='ViewDocsACAcStatus' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;
end;

end.
