object fmAddDoc5: TfmAddDoc5
  Left = 427
  Top = 221
  Width = 1088
  Height = 744
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit10: TcxDateEdit
    Left = 824
    Top = 176
    TabStop = False
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    TabOrder = 7
    Width = 177
  end
  object cxTextEdit10: TcxTextEdit
    Left = 824
    Top = 148
    TabStop = False
    Properties.MaxLength = 15
    Properties.ReadOnly = True
    TabOrder = 5
    Text = #1055#1077#1088#1074#1086#1085#1072#1095#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1080' '#1082#1086#1076
    Width = 177
  end
  object Edit1: TEdit
    Left = 656
    Top = 216
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Edit1'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1077
    Height = 101
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label5: TLabel
      Left = 20
      Top = 44
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label2: TLabel
      Left = 436
      Top = 12
      Width = 53
      Height = 13
      Caption = #1050#1072#1088#1090#1086#1090#1077#1082#1072
      Transparent = True
    end
    object Label3: TLabel
      Left = 436
      Top = 32
      Width = 48
      Height = 13
      Caption = #1042#1074#1086#1076' '#1086#1089#1090'.'
      Transparent = True
    end
    object Label4: TLabel
      Left = 436
      Top = 52
      Width = 28
      Height = 13
      Caption = #1062#1077#1085#1099
      Transparent = True
    end
    object Label6: TLabel
      Left = 436
      Top = 72
      Width = 67
      Height = 13
      Caption = #1044#1077#1090#1072#1083#1100#1085#1086#1089#1090#1100
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 10
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 40
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'Name'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmMC.dsquDeparts1
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 161
    end
    object cxButton3: TcxButton
      Left = 764
      Top = 8
      Width = 77
      Height = 37
      Caption = 'F7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 3
      OnClick = cxButton3Click
      Glyph.Data = {
        160B0000424D160B00000000000036040000280000002C000000280000000100
        080000000000E006000000000000000000000001000000000000E3E1DF00CBC8
        C300FDFDFD00ECEAE500E2DFDD00FAF9F600F5F5F50056B62700A39971008279
        5500736A4A00E5E4E200BBB9B500D3CDB900BAB4AB00BBB39500C0B89C00C8C2
        A9001961EF00C4BDA300F2F1EC00C6752200F8F8F800F0EFED00F7D4AB001649
        A900E8A76600B6AD8C0097928E00F4F4F300F1F1F000FCC88700A2E9AC0069BD
        9F00DCD9D600D0CCC800EBEAEA00F3B97900DEDCD800FEE2B600C1BBB300EEED
        EC00A49D8D00B1ADA800ADA9A300AAA49F008C878400B3A8990089845A00D3D0
        CC00D8D6D300D68F4A0075716F00FFF3D400CAC4BE00D5D3D000DAD5C300FDFD
        FA00E1B68900CA966D00DDD9CA00968A6200C4C1BD00E8E6E400E0DDCF00AFA5
        8400B4590400EEECE900A9A07A00948E89009A947C009C989600BD670C00CFCA
        B500ECEAE800FFD79D00FFFBF4009D936700FBFBFA00D4BDA900FFEAC400DAE9
        9800CCC6AF00F4DBD100EEB37300F3F3F100FFFFED00DAD4CF00E7E4DA00E8C7
        9D00E3E0D400D1823800A3857600918B6E00BA713600FEF4E600E29C5900D7D2
        C0008F531B00D6CFBD00F7F6F500FEEDE00082C8CE00FFD08E00837F7C006861
        4300F9F9F900FFFBE000EBE9DF0097B88B00E2A468006D67C500E7E5F600C5C3
        E300B394A00099BF2C00FFF4EA00DECFD800EDAE6E00EFBCA600FDFAFA00F7C0
        80004537EA00AECD4C00AC7F5100E9E9E900FFFFFF00FAFAFA00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242622242424242424242424242424242424242424242424242424242424
        2424242424242424242424242422372301363624242424242424242424242424
        24242424242424242424242424242424242424242424243231012C470E0E0E28
        2824242424242424242424242424242424242424242424242424242426573123
        242301360E2E682E682E2E2E2C0E282424242424242424242424242424242424
        242424242424225723363E2828282B2E6847524F1C1C1C45681C2B0E24242424
        24242424242424242424242424242424223723284534342A2C452E1C3E4C7E7E
        502F1C1C1C45682D282424242424242424242424242424242424223723284534
        2E2D47682E1C2A537E7E7E207E560D2A1C1C1C1C0E3624242424242424242424
        242424242657232845342C240B042D1C453B537E4C7E212021207E35101C1C2E
        2C36572424242424242424242424030437282E683E7E7E7E06042C3D6E7E653A
        4F19211D200707517E272F1C1C23223F242424242424242429290B012E2E2339
        7E7E7E7E1E042B4F56335E5C19192166660707077B7E56592E0C004324242424
        2424242455001C45227F7E7E7E7E7E7E17040C3B424219191212121207070773
        51397E4F2B004A2424242424242424241E472D7E7F4E7F7F7F7F7F7F430B3E2F
        4262191912121212757E7E7E7E5F0E3100432424242424242424242464687F06
        061D1D1D1D1D1D55433F23282F5E62191919127277657E7E2C2C3E0003242424
        2424242424242424641C1E1E1717171717171717294A32013E0C444862625E3B
        747E531C0E363224242424242424242424242424642B4A4A4A4A4A4A4A4A4A4A
        1717003231010C0E7C423A7E185C1C340E222424242424242424242424242424
        642B3F3F3F0B0B0B0B0B0B4A5555173F2637013E0C2C497C0A341C340E242424
        242424242424242424242424642B00000000000404040455647E7E385A003231
        360C0E2C2E341C3428242424242424242424242424242424162B040404040404
        04044A02027E59424233402637013E0C2B2C4734282424242424242424242424
        242424246A2E040404040404043F7E7E7E5F15484242423A003231360C0E2C34
        282424242424242424242424242424247F2E370404040404067E7E7E7E6E5B15
        4848424215612637013E0C3428242424242424242424242424242424163E0C29
        1717787E7E7E3049181A60335B15484242423B04322336453624242424242424
        2424242424242424241647327E7E7E706F3F6D3A1F257660335B154848424248
        4F26376801242424242424242424242424242424247F3E3E0C370B717A722579
        791F1F541A60335B15484842154A014731242424242424242424242424242424
        244E2B372331106E6E1A765425791F1F797660335B1515483A05470E22242424
        24242424242424242424242424022B0157274B1F76601A76762579791F1F251A
        60335B5B7E5568570024242424242424242424242424242424241E0E0D7E2767
        671F1A6E1A765425791F1F79541A605F7E473604242424242424242424242424
        242424242424241D0D7E2767676767791A1A1A765425791F1F795F7E232C0424
        2424242424242424242424242424242424242464637E504B4B4B676767251A1A
        76542579675F7E012B0024242424242424242424242424242424242424242416
        637E35505027274B4B4B67546E1A541843432B0E3F2424242424242424242424
        24242424242424242424247F637E6B566B3535505027271B344122040C472229
        242424242424242424242424242424242424242424242439287E7E397E395656
        6B35352F2E2C2D2B322924242424242424242424242424242424242424242424
        24242402320F567E4C7E7E7E7E394C0E45365700242424242424242424242424
        24242424242424242424242424242424024E01137E7E397E7E7E7E0C2D312424
        242424242424242424242424242424242424242424242424242424242424391E
        0E3C7E7E397E7E0C0E2224242424242424242424242424242424242424242424
        242424242424242424242424243F2F657E39390C3E0B24242424242424242424
        2424242424242424242424242424242424242424242424242424243113397E5C
        2217242424242424242424242424242424242424242424242424242424242424
        2424242424242424242828221D64242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424242424242424
        2424242424242424242424242424242424242424242424242424}
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox1: TcxCheckBox
      Left = 848
      Top = 72
      Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
      State = cbsChecked
      TabOrder = 4
      Width = 193
    end
    object cxComboBox1: TcxComboBox
      Left = 508
      Top = 8
      Properties.Items.Strings = (
        #1058#1086#1074#1072#1088
        #1058#1072#1088#1072)
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Text = #1058#1086#1074#1072#1088
      Width = 141
    end
    object cxComboBox2: TcxComboBox
      Left = 508
      Top = 28
      Properties.Items.Strings = (
        #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        #1057#1091#1084#1084#1072)
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      Text = #1050#1086#1083'-'#1074#1086
      Width = 141
    end
    object cxComboBox3: TcxComboBox
      Left = 508
      Top = 48
      Properties.Items.Strings = (
        #1050#1072#1088#1090#1086#1090#1077#1082#1072
        #1056#1072#1089#1095#1077#1090
        #1053#1077' '#1091#1095#1080#1090#1099#1074#1072#1090#1100)
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      Text = #1050#1072#1088#1090#1086#1090#1077#1082#1072
      Width = 141
    end
    object cxComboBox4: TcxComboBox
      Left = 508
      Top = 68
      Properties.Items.Strings = (
        #1042#1089#1103' '#1082#1072#1088#1090#1086#1090#1077#1082#1072
        #1043#1088#1091#1087#1087#1099
        #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1086)
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      Text = #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1086
      Width = 141
    end
    object cxCheckBox3: TcxCheckBox
      Left = 20
      Top = 72
      Caption = #1054#1090#1088#1072#1073#1072#1090#1099#1074#1072#1090#1100' '#1074' '#1089#1091#1084#1084#1086#1074#1086#1084' '#1091#1095#1077#1090#1077
      Properties.OnChange = cxCheckBox3PropertiesChange
      TabOrder = 9
      Width = 229
    end
    object cxCheckBox4: TcxCheckBox
      Left = 764
      Top = 72
      Caption = #1050#1088#1072#1090#1082#1086
      TabOrder = 10
      Width = 73
    end
    object cxCheckBox5: TcxCheckBox
      Left = 668
      Top = 8
      Caption = #1074' '#1094#1077#1085#1072#1093' '#1057#1057
      TabOrder = 11
      Width = 81
    end
    object cxCheckBox6: TcxCheckBox
      Left = 668
      Top = 28
      Caption = #1074' '#1094#1077#1085#1072#1093' '#1058#1054
      TabOrder = 12
      Width = 81
    end
    object cxCheckBox7: TcxCheckBox
      Left = 668
      Top = 72
      Caption = #1048#1053#1042'-3'
      TabOrder = 13
      Width = 69
    end
    object cxButton6: TcxButton
      Left = 948
      Top = 8
      Width = 93
      Height = 37
      Caption = #1051#1080#1089#1090' '#1087#1077#1088#1077#1089#1095#1077#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 14
      OnClick = cxButton6Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 677
    Width = 1077
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 634
    Width = 1077
    Height = 43
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 121
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 212
      Top = 12
      Width = 137
      Height = 25
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 400
      Top = 4
      Width = 45
      Height = 37
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton8Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 101
    Width = 153
    Height = 456
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object Label7: TLabel
      Left = 16
      Top = 236
      Width = 32
      Height = 13
      Caption = #1055#1086#1080#1089#1082
      Transparent = True
    end
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 7
      Top = 124
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1086#1089#1090#1072#1090#1082#1080' '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = acSetPriceExecute
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxLabel4: TcxLabel
      Left = 7
      Top = 176
      Cursor = crHandPoint
      Caption = #1043#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = acGenExecute
    end
    object cxTextEdit2: TcxTextEdit
      Left = 16
      Top = 256
      Properties.MaxLength = 15
      TabOrder = 6
      Text = 'cxTextEdit2'
      Width = 121
    end
    object cxButton5: TcxButton
      Left = 68
      Top = 292
      Width = 67
      Height = 25
      Caption = #1087#1086' '#1082#1086#1076#1091
      Default = True
      TabOrder = 7
      OnClick = cxButton5Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox2: TcxCheckBox
      Left = 8
      Top = 196
      Caption = #1087#1086' '#1074#1089#1077#1084' '#1086#1090#1076#1077#1083#1072#1084
      TabOrder = 8
      Width = 121
    end
    object cxLabel7: TcxLabel
      Left = 11
      Top = 360
      Cursor = crHandPoint
      Caption = #1059#1088#1072#1074#1085#1103#1090#1100' '#1092#1072#1082#1090'='#1095#1080#1089#1083
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = acEqualExecute
    end
  end
  object GridDoc5: TcxGrid
    Left = 168
    Top = 104
    Width = 909
    Height = 453
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewDoc5: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewDoc5DblClick
      OnDragDrop = ViewDoc5DragDrop
      OnDragOver = ViewDoc5DragOver
      NavigatorButtons.ConfirmDelete = False
      OnEditing = ViewDoc5Editing
      OnEditKeyDown = ViewDoc5EditKeyDown
      OnEditKeyPress = ViewDoc5EditKeyPress
      DataController.DataSource = dstaSpecInv
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QuantD'
          Column = ViewDoc5QuantD
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QuantF'
          Column = ViewDoc5QuantF
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QuantR'
          Column = ViewDoc5QuantR
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumD'
          Column = ViewDoc5SumD
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumF'
          Column = ViewDoc5SumF
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumR'
          Column = ViewDoc5SumR
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumRSS'
          Column = ViewDoc5SumRSS
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumFSS'
          Column = ViewDoc5SumFSS
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumDSS'
          Column = ViewDoc5SumDSS
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumC'
          Column = ViewDoc5SumC
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumRSC'
          Column = ViewDoc5SumRSC
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumRI'
          Column = ViewDoc5SumRI
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumRSSI'
          Column = ViewDoc5SumRSSI
        end
        item
          Format = '0.00'
          Position = spFooter
          FieldName = 'QuantC'
          Column = ViewDoc5QuantC
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'VolInv'
          Column = ViewDoc5VolInv
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumDTOSS'
          Column = ViewDoc5SumDTOSS
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumDTO'
          Column = ViewDoc5SumDTO
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantD'
          Column = ViewDoc5QuantD
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantF'
          Column = ViewDoc5QuantF
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantR'
          Column = ViewDoc5QuantR
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumD'
          Column = ViewDoc5SumD
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumF'
          Column = ViewDoc5SumF
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumR'
          Column = ViewDoc5SumR
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRSS'
          Column = ViewDoc5SumRSS
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumFSS'
          Column = ViewDoc5SumFSS
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumDSS'
          Column = ViewDoc5SumDSS
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumDTO'
          Column = ViewDoc5SumDTO
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRTO'
          Column = ViewDoc5SumRTO
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumC'
          Column = ViewDoc5SumC
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRSC'
          Column = ViewDoc5SumRSC
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRI'
          Column = ViewDoc5SumRI
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumRSSI'
          Column = ViewDoc5SumRSSI
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantC'
          Column = ViewDoc5QuantC
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'VolInv'
          Column = ViewDoc5VolInv
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumDTOSS'
          Column = ViewDoc5SumDTOSS
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      Styles.Footer = dmMC.cxStyle5
      object ViewDoc5Num: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Num'
      end
      object ViewDoc5CodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
        Width = 49
      end
      object ViewDoc5Name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 148
      end
      object ViewDoc5CodeEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CodeEdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
        Options.Editing = False
      end
      object ViewDoc5Price: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price'
      end
      object ViewDoc5QuantR: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1095#1077#1090#1085#1086#1077
        DataBinding.FieldName = 'QuantR'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumR: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1095#1077#1090#1085#1072#1103
        DataBinding.FieldName = 'SumR'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5QuantF: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077
        DataBinding.FieldName = 'QuantF'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumF: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103
        DataBinding.FieldName = 'SumF'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5QuantD: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'QuantD'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc5SumD: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'SumD'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc5IdGr: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099' 1'
        DataBinding.FieldName = 'IdGr'
        Options.Editing = False
        Width = 40
      end
      object ViewDoc5SGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072' 1'
        DataBinding.FieldName = 'SGr'
        Options.Editing = False
        Width = 148
      end
      object ViewDoc5IdSGr: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099' 2'
        DataBinding.FieldName = 'IdSGr'
        Options.Editing = False
        Width = 45
      end
      object ViewDoc5SSGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072' 2'
        DataBinding.FieldName = 'SSGr'
        Options.Editing = False
      end
      object ViewDoc5IdSSgr: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099' 3'
        DataBinding.FieldName = 'IdSSgr'
        Options.Editing = False
      end
      object ViewDoc5SSSGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072' 3'
        DataBinding.FieldName = 'SSSGr'
        Options.Editing = False
      end
      object ViewDoc5PriceSS: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1057#1057
        DataBinding.FieldName = 'PriceSS'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumRSS: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1088#1072#1089#1095#1077#1090#1085#1072#1103
        DataBinding.FieldName = 'SumRSS'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumFSS: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103
        DataBinding.FieldName = 'SumFSS'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumDSS: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'SumDSS'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewDoc5SumRTO: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1058#1054
        DataBinding.FieldName = 'SumRTO'
        Styles.Content = dmMC.cxStyle18
      end
      object ViewDoc5SumDTO: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072' '#1089' '#1058#1054
        DataBinding.FieldName = 'SumDTO'
        Styles.Content = dmMC.cxStyle26
      end
      object ViewDoc5QuantC: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
        DataBinding.FieldName = 'QuantC'
        Styles.Content = dmMC.cxStyle1
        Width = 70
      end
      object ViewDoc5SumC: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
        DataBinding.FieldName = 'SumC'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc5SumRSC: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
        DataBinding.FieldName = 'SumRSC'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewDoc5SumRI: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1095'. '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'SumRI'
        Styles.Content = dmMC.cxStyle18
      end
      object ViewDoc5SumRSSI: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1088#1072#1089#1095'. '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'SumRSSI'
        Styles.Content = dmMC.cxStyle18
      end
      object ViewDoc5iVid: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'iVid'
        Options.Editing = False
      end
      object ViewDoc5sMaker: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'sMaker'
        Width = 113
      end
      object ViewDoc5Vol: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084' '#1079#1072' '#1077#1076'.'
        DataBinding.FieldName = 'Vol'
      end
      object ViewDoc5VolInv: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'VolInv'
      end
      object ViewDoc5SumDTOSS: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072' '#1058#1054' c '#1095#1080#1089#1083'. '#1057#1057
        DataBinding.FieldName = 'SumDTOSS'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle26
      end
    end
    object LevelDoc5: TcxGridLevel
      GridView = ViewDoc5
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 557
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 8
    Height = 77
    Width = 1077
  end
  object cxButton4: TcxButton
    Left = 856
    Top = 8
    Width = 77
    Height = 37
    Action = acExportEx
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    TabOrder = 9
    Glyph.Data = {
      56080000424D560800000000000036000000280000001A0000001A0000000100
      18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
      80808080808080808080808080808080808080808000400040E02040E020FFFF
      FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
      C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
      C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
      C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
      C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
      20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
      2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
      C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
      A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
      FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
      2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
      FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
      C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
      C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
      FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
      2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
      FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
      C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
      004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
      00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
      FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
      FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
      FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
      C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
      FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
      DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
      FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
      FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
      FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
      FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
      D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
      FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
      FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
      E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
      40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
      E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
      E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
      F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
      C0C0C08080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808000
      2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
    LookAndFeel.Kind = lfOffice11
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 480
    Top = 192
  end
  object dstaSpecInv: TDataSource
    DataSet = taSpecInv
    Left = 472
    Top = 352
  end
  object prCalcPrice: TpFIBStoredProc
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_CALCPRICE (?INPRICE, ?IDGOOD, ?PRICE0, ?PRI' +
        'CE1)')
    StoredProcName = 'PR_CALCPRICE'
    Left = 196
    Top = 252
  end
  object amDocInv: TActionManager
    Left = 196
    Top = 200
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
    object acReadBar: TAction
      Caption = 'acReadBar'
      ShortCut = 16449
      OnExecute = acReadBarExecute
    end
    object acReadBar1: TAction
      Caption = 'acReadBar1'
      ShortCut = 16450
      OnExecute = acReadBar1Execute
    end
    object acSetPrice: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1086#1089#1090#1072#1090#1082#1080
      OnExecute = acSetPriceExecute
    end
    object acZPrice: TAction
      Caption = 'acZPrice'
      ShortCut = 120
    end
    object acPrintDoc: TAction
      Caption = 'F7'
      ShortCut = 118
    end
    object acCalcPrice: TAction
      Caption = 'acCalcPrice'
      ShortCut = 123
    end
    object acPrintCen: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074
      ShortCut = 116
      OnExecute = acPrintCenExecute
    end
    object acGen: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      OnExecute = acGenExecute
    end
    object acOutPutF: TAction
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1092#1072#1082#1090' '#1074' '#1092#1072#1081#1083
      OnExecute = acOutPutFExecute
    end
    object acInputF: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1092#1072#1082#1090#1072' '#1080#1079' '#1092#1072#1081#1083#1072
      OnExecute = acInputFExecute
    end
    object acExportEx: TAction
      OnExecute = acExportExExecute
    end
    object acSetGroups: TAction
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1075#1088#1091#1087#1087#1099
    end
    object acInputFile: TAction
      Caption = 'acInputFile'
      ShortCut = 49225
      OnExecute = acInputFileExecute
    end
    object acRecalcRemnPos: TAction
      Caption = 'acRecalcRemnPos'
      ShortCut = 49234
      OnExecute = acRecalcRemnPosExecute
    end
    object acSetSerch: TAction
      Caption = 'acSetSerch'
      ShortCut = 113
      OnExecute = acSetSerchExecute
    end
    object acTermFile: TAction
      Caption = #1055#1086#1076#1075#1086#1090#1086#1074#1082#1072' '#1092#1072#1081#1083#1072' '#1076#1083#1103' '#1090#1077#1088#1084#1080#1085#1072#1083#1072
      OnExecute = acTermFileExecute
    end
    object acEqual: TAction
      Caption = #1059#1088#1086#1074#1085#1103#1090#1100
      ShortCut = 49233
      OnExecute = acEqualExecute
    end
    object acTestRemn: TAction
      Caption = 'acTestRemn'
      ShortCut = 49242
      OnExecute = acTestRemnExecute
    end
    object acRecalcRemnInv: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1086#1090#1089#1090#1072#1090#1082#1080' '#1073#1077#1079' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
      ShortCut = 24692
      OnExecute = acRecalcRemnInvExecute
    end
    object acSaveDoc1: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1089#1087#1077#1094'. '#1074' '#1091#1095#1077#1090' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080
      ShortCut = 24659
      OnExecute = acSaveDoc1Execute
    end
    object acRecalcRemnSS: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1074' '#1057#1057
      ShortCut = 16466
      OnExecute = acRecalcRemnSSExecute
    end
    object acParts: TAction
      Caption = #1055#1072#1088#1090#1080#1086#1085#1085#1099#1081' '#1091#1095#1077#1090
      ShortCut = 16464
      OnExecute = acPartsExecute
    end
    object acCalcTOSum: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1087#1086' '#1058#1054
      ShortCut = 16468
      OnExecute = acCalcTOSumExecute
    end
    object acEqTOSS: TAction
      Caption = #1059#1088#1072#1074#1085#1103#1090#1100' '#1058#1054'=SS ('#1087#1077#1088#1074#1099#1081' '#1088#1072#1079')'
      OnExecute = acEqTOSSExecute
    end
    object acCreatePP0: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1055#1055' '#1089' '#1091#1076#1072#1083'. ('#1087#1077#1088#1074#1099#1081' '#1088#1072#1079')'
      OnExecute = acCreatePP0Execute
    end
    object acFactToZerro: TAction
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1092#1072#1082#1090
      OnExecute = acFactToZerroExecute
    end
    object acCalcTPos: TAction
      Caption = #1055#1088#1086#1089#1095#1077#1090' '#1089#1091#1084#1084' '#1087#1086' '#1058#1054' '#1086#1076#1085#1086#1081' '#1087#1086#1079#1080#1094#1080#1080
      ShortCut = 49236
      OnExecute = acCalcTPosExecute
    end
    object acTestMoveNoUse: TAction
      Caption = #1044#1086#1073'. '#1076#1074#1080#1078#1077#1085#1080#1103' '#1085#1077#1080#1089#1087'. '#1076#1083#1103' '#1089#1091#1084#1084#1099' '#1058#1054
      OnExecute = acTestMoveNoUseExecute
    end
    object acOutPutF1: TAction
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074#1089#1077' '#1074' '#1092#1072#1081#1083
      OnExecute = acOutPutF1Execute
    end
    object acInputF1: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1074#1089#1077#1075#1086' '#1080#1079' '#1092#1072#1081#1083#1072
      OnExecute = acInputF1Execute
    end
    object acCalcSCPos: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1089#1091#1084#1084#1099' '#1082#1086#1088#1088#1077#1082#1094#1080#1080' '#1076#1083#1103' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1087#1086#1079'.'
      OnExecute = acCalcSCPosExecute
    end
    object acFormNacl: TAction
      Caption = #1042' '#1092#1072#1081#1083' '#1076#1083#1103' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acFormNaclExecute
    end
    object acOutWithNDS: TAction
      Caption = 'acOutWithNDS'
      ShortCut = 24654
      OnExecute = acOutWithNDSExecute
    end
    object acRecalcSS1: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1057#1057' '#1087#1086' 1-'#1086#1081' '#1087#1086#1079#1080#1094#1080#1080
      ShortCut = 49234
      OnExecute = acRecalcSS1Execute
    end
    object acTmp1: TAction
      Caption = 'acTmp1'
      ShortCut = 114
      OnExecute = acTmp1Execute
    end
    object acCreateOut: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
      OnExecute = acCreateOutExecute
    end
    object acTestDoc1: TAction
      Caption = 'acTestDoc1'
      ShortCut = 24690
      OnExecute = acTestDoc1Execute
    end
    object acCreateRetDoc1: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1074' '#1086#1076#1085#1091' '#1074#1086#1079#1074#1088#1072#1090#1085#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102'.'
      OnExecute = acCreateRetDoc1Execute
    end
  end
  object frRepDINV: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit, pbPageSetup]
    Title = #1055#1077#1095#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    RebuildPrinter = False
    OnUserFunction = frRepDINVUserFunction
    Left = 196
    Top = 336
    ReportForm = {19000000}
  end
  object frtaSpecInv: TfrDBDataSet
    DataSet = taSpecInv
    Left = 372
    Top = 356
  end
  object PopupMenu1: TPopupMenu
    Left = 564
    Top = 192
    object N1: TMenuItem
      Action = acOutPutF
    end
    object N2: TMenuItem
      Action = acInputF
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Action = acOutPutF1
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074#1089#1077' '#1074' '#1092#1072#1081#1083' ('#1076#1083#1103' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103')'
    end
    object N16: TMenuItem
      Action = acInputF1
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1074#1089#1077#1075#1086' '#1080#1079' '#1092#1072#1081#1083#1072' ('#1076#1083#1103' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103')'
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object acSetPrice1: TMenuItem
      Action = acSetPrice
    end
    object N9: TMenuItem
      Action = acRecalcRemnSS
    end
    object N10: TMenuItem
      Action = acCalcTOSum
    end
    object SS1: TMenuItem
      Action = acEqTOSS
    end
    object N13: TMenuItem
      Action = acFactToZerro
    end
    object N14: TMenuItem
      Action = acCalcTPos
    end
    object N11: TMenuItem
      Action = acRecalcSS1
    end
    object N18: TMenuItem
      Action = acCalcSCPos
    end
    object N15: TMenuItem
      Action = acTestMoveNoUse
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1056#1072#1079#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N7: TMenuItem
      Action = acTermFile
    end
    object N19: TMenuItem
      Action = acFormNacl
    end
    object N20: TMenuItem
      Action = acCreateOut
    end
    object N21: TMenuItem
      Action = acCreateRetDoc1
    end
  end
  object teClass: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 368
    Top = 200
    object teClassId: TIntegerField
      FieldName = 'Id'
    end
    object teClassIdParent: TIntegerField
      FieldName = 'IdParent'
    end
    object teClassNameClass: TStringField
      FieldName = 'NameClass'
      Size = 30
    end
  end
  object taSpecInv: TClientDataSet
    Aggregates = <>
    FileName = 'SpecInv1.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecInvIndex1'
        Fields = 'CodeTovar'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'taSpecInvIndex2'
        Fields = 'SGr;SSGr;SSSGr;Name'
      end>
    IndexName = 'taSpecInvIndex1'
    Params = <>
    StoreDefs = True
    OnCalcFields = taSpecInvCalcFields
    Left = 284
    Top = 288
    object taSpecInvCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpecInvName: TStringField
      FieldName = 'Name'
      Size = 60
    end
    object taSpecInvCodeEdIzm: TIntegerField
      FieldName = 'CodeEdIzm'
    end
    object taSpecInvNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object taSpecInvPrice: TFloatField
      FieldName = 'Price'
      DisplayFormat = '0.00'
    end
    object taSpecInvQuantR: TFloatField
      FieldName = 'QuantR'
      DisplayFormat = '0.000'
    end
    object taSpecInvQuantF: TFloatField
      FieldName = 'QuantF'
      OnChange = taSpecInvQuantFChange
      DisplayFormat = '0.000'
    end
    object taSpecInvSumR: TFloatField
      FieldName = 'SumR'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumF: TFloatField
      FieldName = 'SumF'
      DisplayFormat = '0.00'
    end
    object taSpecInvQuantD: TFloatField
      FieldName = 'QuantD'
      DisplayFormat = '0.000'
    end
    object taSpecInvSumD: TFloatField
      FieldName = 'SumD'
      DisplayFormat = '0.00'
    end
    object taSpecInvIdGr: TIntegerField
      FieldName = 'IdGr'
    end
    object taSpecInvIdSGr: TIntegerField
      FieldName = 'IdSGr'
    end
    object taSpecInvIdSSGr: TIntegerField
      FieldName = 'IdSSGr'
    end
    object taSpecInvSGr: TStringField
      FieldName = 'SGr'
      Size = 30
    end
    object taSpecInvSSGr: TStringField
      FieldName = 'SSGr'
      Size = 30
    end
    object taSpecInvSSSGr: TStringField
      FieldName = 'SSSGr'
      Size = 30
    end
    object taSpecInvNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecInvPriceSS: TFloatField
      FieldName = 'PriceSS'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumRSS: TFloatField
      FieldName = 'SumRSS'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumFSS: TFloatField
      FieldName = 'SumFSS'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumDSS: TFloatField
      FieldName = 'SumDSS'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumRTO: TFloatField
      FieldName = 'SumRTO'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumDTO: TFloatField
      FieldName = 'SumDTO'
      DisplayFormat = '0.00'
    end
    object taSpecInvQuantC: TFloatField
      FieldName = 'QuantC'
      DisplayFormat = '0.000'
    end
    object taSpecInvSumC: TFloatField
      FieldName = 'SumC'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumRSC: TFloatField
      FieldName = 'SumRSC'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumRSSI: TFloatField
      FieldName = 'SumRSSI'
      DisplayFormat = '0.00'
    end
    object taSpecInvSumRI: TFloatField
      FieldName = 'SumRI'
      DisplayFormat = '0.00'
    end
    object taSpecInvBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taSpecInviVid: TIntegerField
      FieldName = 'iVid'
    end
    object taSpecInviMaker: TIntegerField
      FieldName = 'iMaker'
    end
    object taSpecInvsMaker: TStringField
      FieldName = 'sMaker'
      Size = 100
    end
    object taSpecInvVol: TFloatField
      FieldName = 'Vol'
      DisplayFormat = '0.000'
    end
    object taSpecInvVolInv: TFloatField
      FieldName = 'VolInv'
      DisplayFormat = '0.000'
    end
    object taSpecInvSumDTOSS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SumDTOSS'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object teSp: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 468
    Top = 276
    object teSpItem: TIntegerField
      FieldName = 'Item'
    end
    object teSpPrice: TFloatField
      FieldName = 'Price'
    end
    object teSpQuantR: TFloatField
      FieldName = 'QuantR'
    end
    object teSpQuantF: TFloatField
      FieldName = 'QuantF'
    end
    object teSpNum: TIntegerField
      FieldName = 'Num'
    end
    object teSpSumRSS: TFloatField
      FieldName = 'SumRSS'
    end
    object teSpSumFSS: TFloatField
      FieldName = 'SumFSS'
    end
    object teSpSumTSS: TFloatField
      FieldName = 'SumTSS'
    end
    object teSpQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object teSpSumC: TFloatField
      FieldName = 'SumC'
    end
    object teSpSumRSC: TFloatField
      FieldName = 'SumRSC'
    end
  end
  object teOutLn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 660
    Top = 196
    object teOutLniCode: TIntegerField
      FieldName = 'iCode'
    end
    object teOutLniM: TIntegerField
      FieldName = 'iM'
    end
    object teOutLnQuant: TFloatField
      FieldName = 'Quant'
    end
    object teOutLnPrice0: TFloatField
      FieldName = 'Price0'
    end
    object teOutLnPrice: TFloatField
      FieldName = 'Price'
    end
    object teOutLnPriceM: TFloatField
      FieldName = 'PriceM'
    end
    object teOutLniCli: TIntegerField
      FieldName = 'iCli'
    end
    object teOutLnIdDoc: TIntegerField
      FieldName = 'IdDoc'
    end
    object teOutLniTc: TIntegerField
      FieldName = 'iTc'
    end
  end
  object teCorr: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 732
    Top = 196
    object teCorrItem: TIntegerField
      FieldName = 'Item'
    end
    object teCorrRQuantC: TFloatField
      FieldName = 'RQuantC'
    end
    object teCorrRSumC: TFloatField
      FieldName = 'RSumC'
    end
    object teCorrRSumRSC: TFloatField
      FieldName = 'RSumRSC'
    end
  end
end
