unit Period3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox, cxSpinEdit,
  cxTimeEdit, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc;

type
  TfmSelPerSkl1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxTimeEdit1: TcxTimeEdit;
    Label2: TLabel;
    cxTimeEdit2: TcxTimeEdit;
    Label4: TLabel;
    CasherRnDb: TpFIBDatabase;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quSpecAll: TpFIBDataSet;
    quSpecAllOPERTYPE: TFIBStringField;
    quSpecAllSIFR: TFIBIntegerField;
    quSpecAllNAME: TFIBStringField;
    quSpecAllCODE: TFIBStringField;
    quSpecAllCONSUMMA: TFIBFloatField;
    quSpecAllQSUM: TFIBFloatField;
    quSpecAllDSUM: TFIBFloatField;
    quSpecAllRSUM: TFIBFloatField;
    quSelMH: TpFIBDataSet;
    quSelMHSTORE: TFIBIntegerField;
    quSpecAllDISCONT1: TFIBStringField;
    quSpecAllCLINAME: TFIBStringField;
    quSpecAllITYPE: TFIBSmallIntField;
    quSpecAllNAME1: TFIBStringField;
    quSpecAllCODEB: TFIBIntegerField;
    quSpecAllKB: TFIBFloatField;
    prSetStream: TpFIBStoredProc;
    quSpecAllSALET: TFIBSmallIntField;
    quSpecAllCNTPRICE: TFIBSmallIntField;
    quSpecAllTCAS: TFIBStringField;
    quSpecAllMCAS: TFIBStringField;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl1: TfmSelPerSkl1;

implementation

uses dmOffice, Un1, DMOReps, InputMess;

{$R *.dfm}

procedure TfmSelPerSkl1.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl1.cxButton1Click(Sender: TObject);
Var CurDate:TDateTime;
    DateB,DateE,iCurDate:Integer;
    sOper:String;
    IDH,IDS,NumDB:Integer;
    rSum,Km:Real;
    iCode,iM,iTag:INteger;
    bHead,bDel:Boolean;
    sPC,sPCDB:String;
begin
  CommonSet.DateFrom:=Trunc(cxDateEdit1.Date);
  CommonSet.DateTo:=Trunc(cxDateEdit2.Date)+1;

  NumDB:=cxLookupComboBox1.EditValue;

  Label4.Caption:='���������� ����� ������.';delay(10);
  with dmO do
  with dmORep do
  begin
    CasherRnDb.Connected:=False;
    CasherRnDb.DatabaseName:=DBNAME;
    try
      CasherRnDb.Connected:=True;
    except
      ShowMessage('������������ ����: '+DBNAME+'. ����� ������ ����������.');
    end;

    if CasherRnDb.Connected then
    begin
      //��������� ������
//      sOper:=''; //��������� ��������
      DateB:=Trunc(cxDateEdit1.Date);
      DateE:=Trunc(cxDateEdit2.Date);

      prSetStream.ParamByName('DATEB').AsDateTime:=DateB+cxTimeEdit1.Time;
      prSetStream.ParamByName('DATEE').AsDateTime:=DateE+cxTimeEdit2.Time;
      prSetStream.ExecProc;
      delay(100);

      for iCurDate:=DateB to DateE-1 do
      begin
        CurDate:=iCurDate+cxTimeEdit1.Time;


        //��� ���� �������� ������
        quTestInput.Active:=False;
        quTestInput.ParamByName('IDB').AsInteger:=NumDB;
        quTestInput.ParamByName('DDATE').AsDateTime:=iCurDate;
        quTestInput.Active:=True;

        bDel:=True;
        quTestInput.First;
        while not quTestInput.Eof do
        begin
          if quTestInputIACTIVE.AsInteger=1 then
          begin
            bDel:=False;
            break;
          end;
          quTestInput.Next;
        end;
        quTestInput.First;

        iTag:=0;
        if quTestInput.RecordCount>0 then
        begin //���� ���-��
          fmInputMess:=TfmInputMess.Create(Application);
          if bDel=False then fmInputMess.cxButton1.Enabled:=False;
          fmInputMess.GrInpMess.Tag:=0;
          fmInputMess.ShowModal;
          iTag:= fmInputMess.GrInpMess.Tag;
          fmInputMess.Release;
        end;

        if iTag=1 then
        begin //�������
          if MessageDlg('����� ��������� ���������, ��� � ����� ����������� ����� �� ��������. ���������� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            quTestInput.First;
            while not quTestInput.Eof do quTestInput.Delete;
            quDocsOutB.FullRefresh;
          end;
        end;
        if iTag=2 then
        begin //����������


        end;
        if iTag=3 then
        begin //��������
          Label4.Caption:='����� ������ ����������.';delay(10);
          exit;
        end;
                
        quSelMH.Active:=False;
        quSelMH.Active:=True;
        quSelMH.First;
        while not quSelMH.Eof do
        begin

          rSum:=0; IdH:=0; bHead:=False; sOper:=''; sPC:='PCCard';
          quSpecAll.Active:=False;
          quSpecAll.ParamByName('DATEB').AsDateTime:=CurDate;
          quSpecAll.ParamByName('DATEE').AsDateTime:=CurDate+1;
          quSpecAll.ParamByName('IDSKL').AsInteger:=quSelMHSTORE.AsInteger;
          quSpecAll.Active:=True;

          IDS:=1;
          quSpecAll.First;
          while not quSpecAll.Eof do
          begin
            if quSpecAllOPERTYPE.AsString='SalePC' then
            begin
              if CommonSet.SelPCard=1 then sPCDB:=quSpecAllDISCONT1.AsString
              else sPCDB:='PC';
              if sPCDB<>sPC then
              begin //������ SalePC �������� ���������� - ����� ����� �������� ��������
                if bHead then
                begin //��������� ��� ���� ����� ��������� �����
                  if abs(rSum)>0 then
                  begin
                    quDobHead.Edit;
                    quDOBHEADSUMUCH.AsFloat:=rSum;
                    quDobHead.Post;
                  end;
                end;

                sOper:=quSpecAllOPERTYPE.AsString;
                sPC:=sPCDB;

                IDH:=GetId('DocOutB');
                IDS:=1;
                quDOBHEAD.Active:=False;
                quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
                quDOBHEAD.Active:=True;

                quDOBHEAD.Append;
                quDOBHEADID.AsInteger:=IDH;
                quDOBHEADDATEDOC.AsDateTime:=iCurDate;
                quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
                quDOBHEADDATESF.AsDateTime:=iCurDate;
                quDOBHEADNUMSF.AsString:=IntToStr(IDH);
                quDOBHEADIDCLI.AsInteger:=NumDB;
                quDOBHEADIDSKL.AsInteger:=quSelMHSTORE.AsInteger;
                quDOBHEADSUMIN.AsFloat:=0;
                quDOBHEADSUMUCH.AsFloat:=0;
                quDOBHEADSUMTAR.AsFloat:=0;
                quDOBHEADSUMNDS0.AsFloat:=0;
                quDOBHEADSUMNDS1.AsFloat:=0;
                quDOBHEADSUMNDS2.AsFloat:=0;
                quDOBHEADPROCNAC.AsFloat:=0;
                quDOBHEADIACTIVE.AsInteger:=0;
                quDOBHEADOPER.AsString:=sOper;
                quDOBHEADCOMMENT.AsString:=quSpecAllCLINAME.AsString;
                quDOBHEAD.Post;

                quDOBSpec.Active:=False;
                quDobSpec.ParamByName('IDH').AsInteger:=IDH;
                quDobSpec.Active:=True;

                bHead:=True;
                rSum:=0; //��������� �����
              end;
            end
            else
            begin
              if quSpecAllOPERTYPE.AsString<>sOper then
              begin //��������� ��������� � ��������� sOper
                if bHead then
                begin //��������� ��� ���� ����� ��������� �����
                  if abs(rSum)>0 then
                  begin
                    quDobHead.Edit;
                    quDOBHEADSUMUCH.AsFloat:=rSum;
                    quDobHead.Post;
                  end;
                end;

                sOper:=quSpecAllOPERTYPE.AsString;

                IDH:=GetId('DocOutB');
                IDS:=1;
                quDOBHEAD.Active:=False;
                quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
                quDOBHEAD.Active:=True;

                quDOBHEAD.Append;
                quDOBHEADID.AsInteger:=IDH;
                quDOBHEADDATEDOC.AsDateTime:=iCurDate;
                quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
                quDOBHEADDATESF.AsDateTime:=iCurDate;
                quDOBHEADNUMSF.AsString:=IntToStr(IDH);
                quDOBHEADIDCLI.AsInteger:=NumDB;
                quDOBHEADIDSKL.AsInteger:=quSelMHSTORE.AsInteger;
                quDOBHEADSUMIN.AsFloat:=0;
                quDOBHEADSUMUCH.AsFloat:=0;
                quDOBHEADSUMTAR.AsFloat:=0;
                quDOBHEADSUMNDS0.AsFloat:=0;
                quDOBHEADSUMNDS1.AsFloat:=0;
                quDOBHEADSUMNDS2.AsFloat:=0;
                quDOBHEADPROCNAC.AsFloat:=0;
                quDOBHEADIACTIVE.AsInteger:=0;
                quDOBHEADOPER.AsString:=sOper;
                quDOBHEADCOMMENT.AsString:='';
                try  //�� ������ ������
                  if dmORep.quDB.Active then
                   quDOBHEADCOMMENT.AsString:=dmORep.quDBNAMEDB.AsString;
                except
                end;
                quDOBHEAD.Post;

                quDOBSpec.Active:=False;
                quDobSpec.ParamByName('IDH').AsInteger:=IDH;
                quDobSpec.Active:=True;

                bHead:=True;
                rSum:=0; //��������� �����
              end;
            end;

            //�������� ������������
//            IDS:=GetId('SpecOutB');

            iM:=0; Km:=0; iCode:=0;

            if quSpecAllITYPE.AsInteger=0 then iCode:=StrToIntDef(quSpecAllCODE.AsString,0); //�����
            if quSpecAllITYPE.AsInteger=1 then iCode:=StrToIntDef(quSpecAllCODEB.AsString,0); //������������
            if iCode>0 then
            begin
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=iCode;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin
                iM:=quFindCardIMESSURE.AsInteger;
                if iM>0 then
                begin
                  quM.Active:=False;
                  quM.ParamByName('IDM').AsInteger:=iM;
                  quM.Active:=True;
                  if quM.RecordCount>0 then KM:=quMKOEF.AsFloat;
                  quM.Active:=False;
                end;
              end else iCode:=0;
              quFindCard.Active:=False;
            end;

            quDobSpec.Append;
            quDOBSPECIDHEAD.AsInteger:=IDH;
            quDOBSPECID.AsInteger:=IDS;
            quDOBSPECSIFR.AsInteger:=quSpecAllSIFR.AsInteger;
            if quSpecAllITYPE.AsInteger=0 then
            begin
              quDOBSPECNAMEB.AsString:=quSpecAllNAME.AsString;
              quDOBSPECCODEB.AsString:=quSpecAllCODE.AsString;
              quDOBSPECKB.AsFloat:=quSpecAllCONSUMMA.AsFloat;
            end else
            begin
              quDOBSPECNAMEB.AsString:=quSpecAllNAME1.AsString;
              quDOBSPECCODEB.AsString:=quSpecAllCODEB.AsString;
              quDOBSPECKB.AsFloat:=quSpecAllKB.AsFloat;
            end;
            quDOBSPECDSUM.AsFloat:=quSpecAllDSUM.AsFloat;
            quDOBSPECRSUM.AsFloat:=quSpecAllRSUM.AsFloat;
            quDOBSPECQUANT.AsFloat:=quSpecAllQSUM.AsFloat;
            quDOBSPECIDCARD.AsInteger:=iCode;
            quDOBSPECIDM.AsInteger:=iM;
            quDOBSPECKM.AsFloat:=kM;
            quDOBSPECPRICER.AsFloat:=0;
            if abs(quSpecAllQSUM.AsFloat)>0 then quDOBSPECPRICER.AsFloat:=RoundEx(quSpecAllRSUM.AsFloat/quSpecAllQSUM.AsFloat*100)/100;

            quDOBSPECSALET.AsInteger:=1;
            quDOBSPECSSALET.AsString:='������� �������';

            if quSpecAllSALET.AsInteger>1 then
            begin
              quDOBSPECSALET.AsInteger:=quSpecAllSALET.AsInteger;
              quDOBSPECSSALET.AsString:=quSpecAllTCAS.AsString;
            end else
            begin
              if quSpecAllCNTPRICE.AsInteger>1 then
              begin
                quDOBSPECSALET.AsInteger:=quSpecAllCNTPRICE.AsInteger;
                quDOBSPECSSALET.AsString:=quSpecAllMCAS.AsString;
              end;
            end;

            quDobSpec.Post;

            inc(IDS);
            rSum:=rSum+quSpecAllRSUM.AsFloat;

            quSpecAll.Next;
          end;
          if bHead then
          begin //��������� ��� ���� ����� ��������� �����
            if abs(rSum)>0 then
            begin
              quDobHead.Edit;
              quDOBHEADSUMUCH.AsFloat:=rSum;
              quDobHead.Post;
            end;
          end;
          quDobHead.Active:=False;
          quDobSpec.Active:=False;

          quSelMH.Next;
        end;
        quSelMH.Active:=False;
      end;
    end;
  end;
  Label4.Caption:='����� ������ ��������.';Delay(500);
  ModalResult:=mrOk;
end;

procedure TfmSelPerSkl1.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  try
    if dmORep.quDB.Locate('ID',cxLookupComboBox1.EditValue,[]) then
      DBName:=dmORep.quDBPATHDB.AsString;
  except
  end;    
end;

end.
