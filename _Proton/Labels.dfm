object fmLabels: TfmLabels
  Left = 491
  Top = 259
  BorderStyle = bsDialog
  Caption = #1062#1077#1085#1085#1080#1082#1080
  ClientHeight = 271
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GrLabels: TcxGrid
    Left = 16
    Top = 12
    Width = 325
    Height = 237
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewLabels: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = False
      object ViewLabelsID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 48
      end
      object ViewLabelsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 139
      end
      object ViewLabelsFILEN: TcxGridDBColumn
        Caption = #1060#1072#1081#1083
        DataBinding.FieldName = 'FILEN'
        Width = 115
      end
    end
    object LevelLabels: TcxGridLevel
      GridView = ViewLabels
    end
  end
  object Panel1: TPanel
    Left = 351
    Top = 0
    Width = 145
    Height = 271
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 12
      Top = 20
      Width = 117
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 12
      Top = 52
      Width = 117
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object dstaL: TDataSource
    DataSet = taL
    Left = 164
    Top = 192
  end
  object taL: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 56
    Top = 188
    object taLID: TSmallintField
      FieldName = 'ID'
    end
    object taLNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object taLFILEN: TStringField
      FieldName = 'FILEN'
      Size = 30
    end
  end
end
