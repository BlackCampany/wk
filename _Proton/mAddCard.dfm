object fmAddCard: TfmAddCard
  Left = 407
  Top = 221
  BorderStyle = bsDialog
  Caption = #1050#1072#1088#1090#1086#1095#1082#1072' '#1090#1086#1074#1072#1088#1072
  ClientHeight = 653
  ClientWidth = 522
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 112
    Width = 57
    Height = 13
    Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
  end
  object Label2: TLabel
    Left = 8
    Top = 144
    Width = 52
    Height = 13
    Caption = #1064#1090#1088#1080#1093'-'#1082#1086#1076
  end
  object Label3: TLabel
    Left = 8
    Top = 184
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 216
    Width = 70
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 264
    Width = 36
    Height = 13
    Caption = #1057#1090#1088#1072#1085#1072
    Transparent = True
  end
  object Label6: TLabel
    Left = 8
    Top = 296
    Width = 35
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072
    Transparent = True
  end
  object Label8: TLabel
    Left = 396
    Top = 262
    Width = 24
    Height = 13
    Caption = #1053#1044#1057
  end
  object Label9: TLabel
    Left = 376
    Top = 310
    Width = 47
    Height = 13
    Caption = #1058#1086#1095#1085#1086#1089#1090#1100
  end
  object Label7: TLabel
    Left = 200
    Top = 110
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
  end
  object Label10: TLabel
    Left = 8
    Top = 360
    Width = 53
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072' '#1045#1059
    Transparent = True
  end
  object Label11: TLabel
    Left = 336
    Top = 334
    Width = 88
    Height = 13
    Caption = #1057#1088#1086#1082' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
  end
  object Label12: TLabel
    Left = 352
    Top = 358
    Width = 72
    Height = 13
    Caption = #1055#1088#1086#1094'. '#1086#1090#1093#1086#1076#1086#1074
  end
  object Label13: TLabel
    Left = 320
    Top = 382
    Width = 108
    Height = 13
    Caption = #1050#1088#1080#1090#1080#1095#1077#1089#1082#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
  end
  object Label15: TLabel
    Left = 8
    Top = 392
    Width = 91
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1090#1086#1074#1072#1088#1072
    Transparent = True
  end
  object Label16: TLabel
    Left = 8
    Top = 328
    Width = 31
    Height = 13
    Caption = #1041#1088#1101#1085#1076
    Transparent = True
  end
  object Label14: TLabel
    Left = 380
    Top = 436
    Width = 44
    Height = 13
    Caption = #1053#1072#1094#1077#1085#1082#1072
    Transparent = True
  end
  object Label17: TLabel
    Left = 316
    Top = 412
    Width = 110
    Height = 13
    Caption = #1060#1080#1082#1089#1080#1088#1086#1074#1072#1085#1085#1072#1103' '#1094#1077#1085#1072
  end
  object Label21: TLabel
    Left = 340
    Top = 460
    Width = 78
    Height = 13
    Caption = #1050#1085#1086#1087#1082#1072' '#1074' '#1074#1077#1089#1072#1093
    Transparent = True
  end
  object Label22: TLabel
    Left = 388
    Top = 286
    Width = 35
    Height = 13
    Caption = #1054#1073#1098#1077#1084
  end
  object Label23: TLabel
    Left = 8
    Top = 452
    Width = 79
    Height = 13
    Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
    Transparent = True
  end
  object Label24: TLabel
    Left = 8
    Top = 420
    Width = 75
    Height = 13
    Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 634
    Width = 522
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 400
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 581
    Width = 522
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 4
    object cxButton2: TcxButton
      Left = 260
      Top = 16
      Width = 91
      Height = 25
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      TabStop = False
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 372
      Top = 16
      Width = 89
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      TabStop = False
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object CheckBox1: TcxCheckBox
      Left = 8
      Top = 16
      Caption = #1086#1089#1090#1072#1090#1100#1089#1103' '#1074' '#1101#1090#1086#1081' '#1075#1088#1091#1087#1087#1077
      State = cbsChecked
      TabOrder = 2
      Width = 161
    end
  end
  object RadioGroup1: TcxRadioGroup
    Left = 8
    Top = 8
    Caption = #1058#1080#1087' '#1090#1086#1074#1072#1088#1072' '
    Properties.Items = <
      item
        Caption = #1064#1090#1091#1095#1085#1099#1081
        Value = 0
      end
      item
        Caption = #1042#1077#1089#1086#1074#1086#1081
        Value = 1
      end>
    Properties.OnChange = RadioGroup1PropertiesChange
    ItemIndex = 0
    Style.BorderStyle = ebsFlat
    TabOrder = 0
    Height = 81
    Width = 145
  end
  object RadioGroup2: TcxRadioGroup
    Left = 176
    Top = 8
    Caption = #1058#1080#1087' '#1096#1090#1088#1080#1093'-'#1082#1086#1076#1072' '
    Properties.Items = <
      item
        Caption = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081
        Value = 0
      end
      item
        Caption = #1060#1086#1088#1084#1080#1088#1091#1077#1084#1099#1081
        Value = 1
      end
      item
        Caption = #1050#1086#1088#1086#1090#1082#1080#1081
        Value = 2
      end>
    Properties.OnChange = RadioGroup2PropertiesChange
    ItemIndex = 0
    Style.BorderStyle = ebsFlat
    TabOrder = 1
    Height = 81
    Width = 169
  end
  object MaskEdit1: TcxMaskEdit
    Left = 88
    Top = 104
    Properties.Alignment.Horz = taRightJustify
    Properties.MaskKind = emkRegExpr
    Properties.EditMask = '\d+'
    Properties.MaxLength = 0
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 2
    Text = '1'
    Width = 73
  end
  object MaskEdit2: TcxMaskEdit
    Left = 88
    Top = 136
    BeepOnEnter = False
    Properties.MaskKind = emkRegExpr
    Properties.EditMask = '\d+'
    Properties.MaxLength = 0
    Properties.OnEditValueChanged = MaskEdit2PropertiesEditValueChanged
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 3
    Text = '2'
    Width = 129
  end
  object TextEdit1: TcxTextEdit
    Left = 96
    Top = 176
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 6
    Text = '123456789012345678901234567890'
    Width = 217
  end
  object TextEdit2: TcxTextEdit
    Left = 96
    Top = 208
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 7
    Text = '123456789012345678901234567890123456789012345678901234567890'
    Width = 417
  end
  object ButtonEdit1: TcxButtonEdit
    Left = 72
    Top = 256
    ParentFont = False
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = ButtonEdit1PropertiesButtonClick
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 28895
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 8
    Text = 'ButtonEdit1'
    Width = 214
  end
  object ButtonEdit2: TcxButtonEdit
    Left = 72
    Top = 288
    ParentFont = False
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = ButtonEdit2PropertiesButtonClick
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 4204565
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 9
    Text = 'ButtonEdit2'
    Width = 214
  end
  object CalcEdit2: TcxCalcEdit
    Left = 432
    Top = 256
    EditValue = 18
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 10
    Width = 70
  end
  object cxButton1: TcxButton
    Left = 344
    Top = 176
    Width = 75
    Height = 25
    Caption = '...'
    TabOrder = 12
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object CalcEdit3: TcxCalcEdit
    Left = 432
    Top = 304
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 13
    Width = 70
  end
  object CalcEdit1: TcxCalcEdit
    Left = 256
    Top = 104
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.00'
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 14
    Width = 89
  end
  object LookupComboBox1: TcxLookupComboBox
    Left = 72
    Top = 352
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'Code'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 200
        FieldName = 'Name'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dmMC.dsquEU1
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 19
    Width = 214
  end
  object CalcEdit4: TcxCalcEdit
    Left = 432
    Top = 328
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 15
    Width = 70
  end
  object CalcEdit5: TcxCalcEdit
    Left = 432
    Top = 352
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 16
    Width = 70
  end
  object cxButton4: TcxButton
    Left = 248
    Top = 136
    Width = 121
    Height = 25
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1076#1072
    TabOrder = 28
    TabStop = False
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object CalcEdit6: TcxCalcEdit
    Left = 432
    Top = 376
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 17
    Width = 70
  end
  object LookupComboBox3: TcxLookupComboBox
    Left = 165
    Top = 384
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1048#1076#1077#1085#1090'.'
        FieldName = 'sId'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Comment'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListOptions.GridLines = glNone
    Properties.ListOptions.ShowHeader = False
    Properties.ListSource = dmMC.dsquCateg
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 20
    Width = 121
  end
  object LookupComboBox4: TcxLookupComboBox
    Left = 72
    Top = 320
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEBRAND'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dmMC.dsquBrands1
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 18
    Width = 214
  end
  object CheckListBox1: TcxCheckListBox
    Left = 376
    Top = 8
    Width = 113
    Height = 153
    Items = <
      item
        State = cbsChecked
        Text = #1050#1086#1083#1100#1094#1086#1074#1086
      end
      item
        State = cbsChecked
        Text = #1041#1072#1088#1076#1080#1085#1072
      end
      item
        State = cbsChecked
        Text = #1041#1077#1073#1077#1083#1103
      end
      item
        State = cbsChecked
        Text = #1056#1077#1087#1080#1085#1072
      end
      item
        State = cbsChecked
        Text = '..'
      end
      item
        State = cbsChecked
        Text = '..'
      end
      item
        State = cbsChecked
        Text = '..'
      end
      item
        State = cbsChecked
        Text = '..'
      end
      item
        State = cbsChecked
        Text = '..'
      end>
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 27
    Visible = False
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 432
    Top = 432
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.0'
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 23
    Width = 69
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 432
    Top = 408
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.00'
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 24
    Width = 69
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 484
    Width = 325
    Height = 89
    Caption = #1041#1083#1086#1082' '#1079#1072#1103#1074#1086#1082
    TabOrder = 25
    object Label18: TLabel
      Left = 12
      Top = 16
      Width = 151
      Height = 13
      Caption = #1052#1080#1085#1080#1084#1072#1083#1100#1085#1099#1081' '#1086#1089#1090#1072#1090#1086#1082' '#1074' '#1076#1085#1103#1093
      Transparent = True
    end
    object Label19: TLabel
      Left = 12
      Top = 40
      Width = 157
      Height = 13
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1086#1089#1090#1072#1090#1086#1082' '#1074' '#1076#1085#1103#1093
      Transparent = True
    end
    object Label20: TLabel
      Left = 12
      Top = 64
      Width = 167
      Height = 13
      Caption = #1052#1080#1085#1080#1084#1072#1083#1100#1085#1086#1077' '#1082#1086#1083'-'#1074#1086' '#1076#1083#1103' '#1079#1072#1082#1072#1079#1072
      Transparent = True
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 208
      Top = 12
      Style.BorderStyle = ebsFlat
      Style.Shadow = True
      TabOrder = 0
      Width = 81
    end
    object cxSpinEdit2: TcxSpinEdit
      Left = 208
      Top = 36
      Style.BorderStyle = ebsFlat
      Style.Shadow = True
      TabOrder = 1
      Width = 81
    end
    object cxCalcEdit3: TcxCalcEdit
      Left = 208
      Top = 60
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '0.000'
      Style.BorderStyle = ebsFlat
      Style.Shadow = True
      TabOrder = 2
      Width = 81
    end
  end
  object cxSpinEdit3: TcxSpinEdit
    Left = 432
    Top = 456
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 26
    Width = 69
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 432
    Top = 280
    EditValue = 18
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 11
    Width = 70
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 96
    Top = 448
    Properties.ClearKey = 8
    Properties.DropDownWidth = 600
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = 'INN'
        Width = 150
        FieldName = 'INNM'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        SortOrder = soAscending
        Width = 300
        FieldName = 'NAMEM'
      end>
    Properties.ListSource = dmMC.dsquMakers1
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 22
    Width = 190
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 96
    Top = 416
    Properties.Alignment.Horz = taLeftJustify
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        FieldName = 'NAMECLA'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dmMC.dsquAlgClass1
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 21
    Width = 190
  end
  object amCards: TActionManager
    Left = 440
    Top = 152
    StyleName = 'XP Style'
    object acBarRead: TAction
      Caption = 'acBarRead'
      ShortCut = 123
      OnExecute = acBarReadExecute
    end
    object acTextEdit2: TAction
      Caption = 'acTextEdit2'
      ShortCut = 8237
      OnExecute = acTextEdit2Execute
    end
    object acClear: TAction
      Caption = 'acClear'
      ShortCut = 117
      OnExecute = acClearExecute
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      OnExecute = acExitExecute
    end
  end
end
