object fmCalcCal: TfmCalcCal
  Left = 266
  Top = 453
  Width = 879
  Height = 302
  Caption = #1056#1072#1089#1095#1077#1090' '#1101#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1086#1081' '#1094#1077#1085#1085#1086#1089#1090#1080
  Color = 16771022
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 720
    Top = 16
    Width = 132
    Height = 13
    Caption = #1069#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1072#1103' '#1094#1077#1085#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 724
    Top = 44
    Width = 41
    Height = 13
    Caption = #1085#1072' 100 '#1075
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 724
    Top = 72
    Width = 30
    Height = 13
    Caption = #1073#1077#1083#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 724
    Top = 96
    Width = 28
    Height = 13
    Caption = #1078#1080#1088#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 724
    Top = 120
    Width = 48
    Height = 13
    Caption = #1091#1075#1083#1077#1074#1086#1076#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 249
    Width = 871
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GrCal: TcxGrid
    Left = 0
    Top = 0
    Width = 705
    Height = 249
    Align = alLeft
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewCal: TcxGridDBBandedTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaCal
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          FieldName = 'Massa'
          Column = ViewCalMassa
        end
        item
          Kind = skSum
          FieldName = 'b1'
          Column = ViewCalb1
        end
        item
          Kind = skSum
          FieldName = 'b2'
          Column = ViewCalb2
        end
        item
          Kind = skSum
          FieldName = 'g1'
          Column = ViewCalg1
        end
        item
          Kind = skSum
          FieldName = 'g2'
          Column = ViewCalg2
        end
        item
          Kind = skSum
          FieldName = 'u1'
          Column = ViewCalu1
        end
        item
          Kind = skSum
          FieldName = 'u2'
          Column = ViewCalu2
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Bands = <
        item
        end
        item
          Caption = #1041#1077#1083#1082#1080
          Width = 110
        end
        item
          Caption = #1046#1080#1088#1099
          Width = 110
        end
        item
          Caption = #1059#1075#1083#1077#1074#1086#1076#1099
          Width = 110
        end>
      object ViewCalNum: TcxGridDBBandedColumn
        Caption = #8470
        DataBinding.FieldName = 'Num'
        Width = 28
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewCalName: TcxGridDBBandedColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 136
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewCalsm: TcxGridDBBandedColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'sm'
        Width = 30
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ViewCalObr: TcxGridDBBandedColumn
        Caption = #1054#1073#1088#1072#1073#1086#1090#1082#1072
        DataBinding.FieldName = 'Obr'
        Width = 100
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewCalMassa: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Massa'
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewCalb1: TcxGridDBBandedColumn
        Caption = #1044#1086' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'b1'
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewCalb2: TcxGridDBBandedColumn
        Caption = #1055#1086#1089#1083#1077' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'b2'
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewCalg1: TcxGridDBBandedColumn
        Caption = #1044#1086' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'g1'
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewCalg2: TcxGridDBBandedColumn
        Caption = #1055#1086#1089#1083#1077' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'g2'
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewCalu1: TcxGridDBBandedColumn
        Caption = #1044#1086' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'u1'
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewCalu2: TcxGridDBBandedColumn
        Caption = #1055#1086#1089#1083#1077' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        DataBinding.FieldName = 'u2'
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
    end
    object LevelCal: TcxGridLevel
      GridView = ViewCal
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 776
    Top = 40
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    TabOrder = 2
    Width = 77
  end
  object cxButton1: TcxButton
    Left = 736
    Top = 164
    Width = 107
    Height = 33
    Caption = #1055#1088#1080#1089#1074#1086#1080#1090#1100
    TabOrder = 3
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 736
    Top = 204
    Width = 105
    Height = 33
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 4
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 776
    Top = 68
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    TabOrder = 5
    Width = 77
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 776
    Top = 92
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    TabOrder = 6
    Width = 77
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 776
    Top = 116
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    TabOrder = 7
    Width = 77
  end
  object taCal: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 176
    Top = 64
    object taCalNum: TSmallintField
      FieldName = 'Num'
    end
    object taCalName: TStringField
      FieldName = 'Name'
      Size = 150
    end
    object taCalObr: TStringField
      FieldName = 'Obr'
      Size = 100
    end
    object taCalMassa: TFloatField
      FieldName = 'Massa'
    end
    object taCalMassa1: TFloatField
      FieldName = 'Massa1'
    end
    object taCalb1: TFloatField
      FieldName = 'b1'
    end
    object taCalb2: TFloatField
      FieldName = 'b2'
    end
    object taCalg1: TFloatField
      FieldName = 'g1'
    end
    object taCalg2: TFloatField
      FieldName = 'g2'
    end
    object taCalu1: TFloatField
      FieldName = 'u1'
    end
    object taCalu2: TFloatField
      FieldName = 'u2'
    end
    object taCalkb: TFloatField
      FieldName = 'kb'
    end
    object taCalim: TIntegerField
      FieldName = 'im'
    end
    object taCalsm: TStringField
      FieldName = 'sm'
    end
    object taCalIdCard: TIntegerField
      FieldName = 'IdCard'
    end
  end
  object dstaCal: TDataSource
    DataSet = taCal
    Left = 180
    Top = 120
  end
  object PopupMenu1: TPopupMenu
    Left = 296
    Top = 72
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
  end
end
