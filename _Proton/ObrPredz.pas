unit ObrPredz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, SpeedBar, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ActnList, XPStyleActnCtrls,
  ActnMan, Placemnt, dxmdaset, cxProgressBar, cxContainer, cxTextEdit,
  cxMemo, cxImageComboBox, Menus, StdCtrls;

type
  TfmObrPredz = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    teobrPredz: TdxMemData;
    teobrPredzIdCode1: TIntegerField;
    teobrPredzNameC: TStringField;
    teobrPredzIdGroup: TIntegerField;
    teobrPredzIdSGroup: TIntegerField;
    teobrPredzQOut: TFloatField;
    teobrPredzQObr: TFloatField;
    teobrPredzNameGr1: TStringField;
    teobrPredzNameGr2: TStringField;
    teobrPredzNameGr3: TStringField;
    teobrPredzNAMEDEP: TStringField;
    dsteobrPredz: TDataSource;
    FormPlacement1: TFormPlacement;
    amobrPredz: TActionManager;
    acPeriod: TAction;
    teobrPredziCli: TIntegerField;
    teobrPredzNameCli: TStringField;
    teobrPredzDepart: TIntegerField;
    Memo1: TcxMemo;
    teobrPredzProc: TIntegerField;
    teobrPredzEdIzm: TIntegerField;
    teobrPredzCodePoluch: TIntegerField;
    acProc: TAction;
    PBar1: TcxProgressBar;
    teobrPredzMOLVOZ: TStringField;
    teobrPredzPHONEVOZ: TStringField;
    teobrPredzEMAILVOZ: TStringField;
    acChangeCli: TAction;
    acChangeDep: TAction;
    teobrPredzQALLREM: TFloatField;
    acAllPost: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Panel1: TPanel;
    acReadBar: TAction;
    acReadBar1: TAction;
    Edit1: TEdit;
    GrObrPredz: TcxGrid;
    ViewObrPredz: TcxGridDBTableView;
    ViewObrPredzRecId: TcxGridDBColumn;
    ViewObrPredziCli: TcxGridDBColumn;
    ViewObrPredzNameCli: TcxGridDBColumn;
    ViewObrPredzDep: TcxGridDBColumn;
    ViewObrPredzNameDep: TcxGridDBColumn;
    ViewObrPredzMolVoz: TcxGridDBColumn;
    ViewObrPredzPhoneVoz: TcxGridDBColumn;
    ViewObrPredzEmailVoz: TcxGridDBColumn;
    ViewObrPredzNameGr1: TcxGridDBColumn;
    ViewObrPredzNameGr2: TcxGridDBColumn;
    ViewObrPredzNameGr3: TcxGridDBColumn;
    ViewObrPredzIdCode1: TcxGridDBColumn;
    ViewObrPredzNameC: TcxGridDBColumn;
    ViewObrPredzQOut: TcxGridDBColumn;
    ViewObrPredzQObr: TcxGridDBColumn;
    ViewObrPredzQAllRem: TcxGridDBColumn;
    ViewObrPredzProc: TcxGridDBColumn;
    LevObrPredz: TcxGridLevel;
    N2: TMenuItem;
    teobrPredzTypeVoz: TStringField;
    ViewObrPredzTypeVoz: TcxGridDBColumn;
    procedure acPeriodExecute(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewObrPredzCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure teobrPredzProcChange(Sender: TField);
    procedure ViewObrPredzEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ViewObrPredzProcPropertiesCloseUp(Sender: TObject);
    procedure acProcExecute(Sender: TObject);
    procedure acChangeCliExecute(Sender: TObject);
    procedure acChangeDepExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acAllPostExecute(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    procedure prnewpost(icli:integer;Q,Qobr:real);
    procedure prnewdep(depart:integer;Q,Qobr:real);
 //   procedure updoneform;
    procedure updallform;
    procedure clearpredz;

   { type
       TCliRec = record
       ID,ICLI:integer;
    end;    }

var
  fmObrPredz: TfmObrPredz;
  iCol:integer;
//  CliRec:TCliRec;
implementation

uses Un1,u2fdk,MT, dmPS, Period1, MDB, PostGood, PostGd, ReasonsV;

{$R *.dfm}
procedure prnewpost(icli:integer;Q,Qobr:real);  ////////////����� ����������
  var      iNum,fl,d:integer;
           CenaNotNDS,Cena,NewCena,SumCenaNotNDS,SumCena,SumNewCena:real;
           t,user:string;
begin
  with dmMC do
  with dmP do
  begin
    if (Qobr>0) then
    begin
      fl:=0;
      iNum:=prFindNum(25);
      while fl=0 do
      begin
        fl:=1;
       // iNum:=iNum+1;
        quC.Active:=False;
        quC.SQL.Clear;
        quC.SQL.Add('Select Count(*) as RecCount from "A_DocHRet"');
        quC.SQL.Add('where DocNum='+its(iNum));
        quC.Active:=True;
        if quCRecCount.AsInteger>0 then
        begin
          fl:=0;
          iNum:=iNum+1;
        end;
      end;
                        //������� ��������� ����
      quSelCena.Active:=False;
      quSelCena.ParamByName('CODE').AsInteger:=fmObrPredz.teobrPredzIdCode1.AsInteger;
      quSelCena.ParamByName('DEP').AsInteger:=fmObrPredz.teobrPredzDepart.AsInteger;
      quSelCena.ParamByName('ICLI').AsInteger:=fmObrPredz.teobrPredziCli.AsInteger;
      quSelCena.Active:=True;

      if quSelCena.RecordCount>0 then
      begin
        Cena:=RoundVal(quSelCenaPRICEIN.AsFloat);
        CenaNotNDS:=RoundVal(quSelCenaPRICEIN0.AsFloat);
        NewCena:=RoundVal(quSelCenaRPrice.AsFloat);
        SumCena:=RoundVal(Cena*Qobr);
        SumCenaNotNDS:=RoundVal(CenaNotNDS*Qobr);
        SumNewCena:=RoundVal(NewCena*Qobr);
      end
      else
      begin
        showmessage('��� ���������� �� ����� ������');
        exit;
      end;
      user:=Person.Name;
      t:=ts(now());


      quDateVoz.Active:=False;                                    //���� ���� ��������
      quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
      quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
      quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
      quDateVoz.ParamByName('IDCli').AsInteger:=icli;
      quDateVoz.Active:=True;
                                                   //��������� ���� �� ���� ��������
      if quDateVoz.RecordCount<1 then
      begin
        if MessageDlg('� ���������� ���������� ��� ������� ���������, ��������� �� ������� ����?',mtConfirmation,[mbYes, mbNo], 0)=mrYes then
          d:=trunc(date)               //���� ��� �� ������ ������� ����
        else exit;
      end
      else d:=quDateVozIDATEV.AsInteger;

      //�������� ����� ��������� ����������
      try
           //�������� �����
        quC.Active:=false;
        quC.SQL.Clear;
        quC.SQL.Add('Select Count(*) as RecCount from "IdPool"');
        quC.SQL.Add('where CodeObject=25');
        quC.Active:=true;
        if quCRecCount.AsInteger<1 then
        begin
          quA.Active:=false;
          quA.SQL.Clear;
          quA.SQL.Add('Insert into "IdPool" values (25,1)');
          quA.ExecSQL;
        end
        else
        begin
          quE.SQL.Clear;
          quE.SQL.Add('Update "IdPool" Set ID='+its(iNum)+'');
          quE.SQL.Add(' where CodeObject=25');
          quE.ExecSQL;
        end;
      finally
        try
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_DocHRet" values (');
          quA.SQL.Add(its(iNum)+',');
          quA.SQL.Add(''+its(iNum)+',');                    //DocNum
          quA.SQL.Add(its(Trunc(Date))+',');                //DocDate
          quA.SQL.Add(''''+t+''',');                        //DocTime
          quA.SQL.Add(''''+'��������� ���������� �� �����������'+''',');         //Comment
          quA.SQL.Add(''''+user+''','+'''0'''+')');                     //User   �������
          //quA.SQL.SaveToFile('C:\1.txt');
          quA.ExecSQL;

        finally
          //�������� ����� ������������ ����������
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_DocSRet" values (');
          quA.SQL.Add(its(iNum)+','''',');
          quA.SQL.Add(its(fmObrPredz.teobrPredzIdCode1.AsInteger)+',');
          quA.SQL.Add(its(fmObrPredz.teobrPredzDepart.AsInteger)+',');
          quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''','); //���-��
          quA.SQL.Add(''''+FloatToStrF(CenaNotNDS,ffFixed,10,2)+''','); //���� ���� ��� ���
          quA.SQL.Add(''''+FloatToStrF(Cena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(SumCenaNotNDS,ffFixed,10,2)+''','); //����� ���� ��� ���
          quA.SQL.Add(''''+FloatToStrF(SumCena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(SumNewCena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(NewCena,ffFixed,10,2)+''',');
          quA.SQL.Add(its(icli)+',');
          quA.SQL.Add('1,');                    //������� ��������
          quA.SQL.Add('0,');     //���-�� �������
          quA.SQL.Add('''0'''+',');
          quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''',');   //���-�� ��������
          quA.SQL.Add(its(d)+',');     //���� ��������
          quA.SQL.Add(its(Trunc(Date))+')');    //DocDate
       //   quA.SQL.SaveToFile('C:\1.txt');
          quA.ExecSQL;
        end;
      end;
    end;
    fmPD.Close;
    clearpredz;       //�������� ������ ����������
  end;
end;

procedure prnewdep(depart:integer;Q,Qobr:real);  ////////////����� ������
  var      iNum,fl,d:integer;
           CenaNotNDS,Cena,NewCena,SumCenaNotNDS,SumCena,SumNewCena:real;
           t,user:string;
begin
  with dmMC do
  with dmP do
  begin
    if (Qobr>0) then
    begin
      fl:=0;
      iNum:=prFindNum(25);
      while fl=0 do
      begin
        fl:=1;
        iNum:=iNum+1;
        quC.Active:=False;
        quC.SQL.Clear;
        quC.SQL.Add('Select Count(*) as RecCount from "A_DocHRet"');
        quC.SQL.Add('where DocNum='+its(iNum));
        quC.Active:=True;
        if quCRecCount.AsInteger>0 then fl:=0;
      end;
                        //������� ��������� ����
      quSelCena.Active:=False;
      quSelCena.ParamByName('CODE').AsInteger:=fmObrPredz.teobrPredzIdCode1.AsInteger;
      quSelCena.ParamByName('DEP').AsInteger:=fmObrPredz.teobrPredzDepart.AsInteger;
      quSelCena.ParamByName('ICLI').AsInteger:=fmObrPredz.teobrPredziCli.AsInteger;
      quSelCena.Active:=True;

      if quSelCena.RecordCount>0 then
      begin
        Cena:=RoundVal(quSelCenaPRICEIN.AsFloat);
        CenaNotNDS:=RoundVal(quSelCenaPRICEIN0.AsFloat);
        NewCena:=RoundVal(quSelCenaRPrice.AsFloat);
        SumCena:=RoundVal(Cena*Qobr);
        SumCenaNotNDS:=RoundVal(CenaNotNDS*Qobr);
        SumNewCena:=RoundVal(NewCena*Qobr);
      end
      else
      begin
        showmessage('��� ���������� �� ����� ������');
        exit;
      end;
      user:=Person.Name;
      t:=ts(now());

      quDateVoz.Active:=False;                                    //���� ���� ��������
      quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
      quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
      quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
      quDateVoz.ParamByName('IDCli').AsInteger:=fmObrPredz.teobrPredziCli.AsInteger;
      quDateVoz.Active:=True;
                                                   //��������� ���� �� ���� ��������
      if quDateVoz.RecordCount<1 then
      begin
        if MessageDlg('� ���������� ���������� ��� ������� ���������, ��������� �� ������� ����?',mtConfirmation,[mbYes, mbNo], 0)=mrYes then
          d:=trunc(date)               //���� ��� �� ������ ������� ����
        else exit;
      end
      else d:=quDateVozIDATEV.AsInteger;

      //�������� ����� ��������� ����������
      try
        //�������� �����
        quC.Active:=false;
        quC.SQL.Clear;
        quC.SQL.Add('Select Count(*) as RecCount from "IdPool"');
        quC.SQL.Add('where CodeObject=25');
        quC.Active:=true;
        if quCRecCount.AsInteger<1 then
        begin
          quA.Active:=false;
          quA.SQL.Clear;
          quA.SQL.Add('Insert into "IdPool" values (25,1)');
          quA.ExecSQL;
        end
        else
        begin
          quE.SQL.Clear;
          quE.SQL.Add('Update "IdPool" Set ID='+its(iNum)+'');
          quE.SQL.Add(' where CodeObject=25');
          quE.ExecSQL;
        end;
      finally
        try
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_DocHRet" values (');
          quA.SQL.Add(its(iNum)+',');
          quA.SQL.Add(''+its(iNum)+',');                    //DocNum
          quA.SQL.Add(its(Trunc(Date))+',');                //DocDate
          quA.SQL.Add(''''+t+''',');                        //DocTime
          quA.SQL.Add(''''+'��������� ������ �� �����������'+''',');         //Comment
          quA.SQL.Add(''''+user+''','+'''0'''+')');                     //User   �������
          //quA.SQL.SaveToFile('C:\1.txt');
          quA.ExecSQL;

        finally
          //�������� ����� ������������ ����������
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_DocSRet" values (');
          quA.SQL.Add(its(iNum)+','''',');
          quA.SQL.Add(its(fmObrPredz.teobrPredzIdCode1.AsInteger)+',');
          quA.SQL.Add(its(depart)+',');
          quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''','); //���-��
          quA.SQL.Add(''''+FloatToStrF(CenaNotNDS,ffFixed,10,2)+''','); //���� ���� ��� ���
          quA.SQL.Add(''''+FloatToStrF(Cena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(SumCenaNotNDS,ffFixed,10,2)+''','); //����� ���� ��� ���
          quA.SQL.Add(''''+FloatToStrF(SumCena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(SumNewCena,ffFixed,10,2)+''',');
          quA.SQL.Add(''''+FloatToStrF(NewCena,ffFixed,10,2)+''',');
          quA.SQL.Add(its(fmObrPredz.teobrPredziCli.AsInteger)+',');
          quA.SQL.Add('1,');                    //������� ��������
          quA.SQL.Add('0,');     //���-�� �������
          quA.SQL.Add('''0'''+',');
          quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''',');   //���-�� ��������
          quA.SQL.Add(its(d)+',');     //���� ��������
          quA.SQL.Add(its(Trunc(Date))+')');    //DocDate
       //   quA.SQL.SaveToFile('C:\1.txt');
          quA.ExecSQL;
        end;
      end;
    end;
    fmPD.Close;
    clearpredz;       //�������� ������ ����������
  end;
end;
      
                   
procedure updallform;
Var iStep,iC,iGr:INteger;
begin
  //��������� ������ �� �������
  with dmMt do
  with dmP do
  begin
    if fmObrPredz.Showing then fmObrPredz.Close;

    CommonSet.DateBeg:=Date-730;
    CommonSet.DateEnd:=Date;

    fmObrPredz.PBar1.Position:=0;
    fmObrPredz.Show;
    fmObrPredz.PBar1.Visible:=True;
    with fmObrPredz do
    begin
      Memo1.Clear;
      Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
      ViewObrPredz.BeginUpdate;
      CloseTe(teObrPredz);

      CloseTe(teClass);
      teClass.Active:=True;
      if ptSGr.Active=False then ptSGr.Active:=True;

      ptSGr.First;
      while not ptSGr.Eof do
      begin
        teClass.Append;
        teClassId.AsInteger:=ptSGrID.AsInteger;
        teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
        teClassNameClass.AsString:=ptSGrName.AsString;
        teClass.Post;

        ptSGr.next;
      end;
      ptSGr.Active:=False;

      iGr:=0;
      PBar1.Position:=10; delay(10);

      quObrPredz.Active:=False;
   //   quObrPredz.ParamByName('iDateB').AsInteger:=trunc(CommonSet.DateBeg);
   //   quObrPredz.ParamByName('iDateE').AsInteger:=trunc(CommonSet.DateEnd);
      quObrPredz.Active:=True;

      PBar1.Position:=40; delay(50);
      Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

      if quObrPredz.RecordCount>0 then
      begin
        quObrPredz.First;

        iStep:=quObrPredz.RecordCount div 60;
        if iStep=0 then iStep:=quObrPredz.RecordCount;
        iC:=0;
        teObrPredz.Active:=True;
        while not quObrPredz.Eof do
        begin
          teObrPredz.Append;
          teobrPredzCodePoluch.AsInteger:=quObrPredzVendor.AsInteger;
          teobrPredziCli.AsInteger:=quObrPredzIDCLI.AsInteger;
          teobrPredzNameCli.AsString:=quObrPredzNameCli.AsString;
          teobrPredzMOLVOZ.AsString:=quObrPredzMOLVOZ.AsString;
          teobrPredzPHONEVOZ.AsString:=quObrPredzPHONEVOZ.AsString;
          teobrPredzEMAILVOZ.AsString:=quObrPredzEMAILVOZ.AsString;
          teobrPredzDepart.AsInteger:=quObrPredzDEPART.AsInteger;
          teobrPredzNAMEDEP.AsString:=quObrPredzNameDep.AsString;
          teobrPredzIdCode1.AsInteger:=quObrPredzCODE.AsInteger;
          teobrPredzNameC.AsString:=quObrPredzFNameCode.AsString;
          teobrPredzEdIzm.AsInteger:=quObrPredzEdIzm.AsInteger;
          teobrPredzQOut.AsFloat:=quObrPredzQRem.AsFloat;
          teobrPredzQObr.AsFloat:=0;
          teobrPredzQALLREM.AsFloat:=quObrPredzQALLREM.AsFloat;
          teobrPredzIdGroup.AsInteger:=quObrPredzGoodsGroupID.AsInteger;
          teobrPredzIdSGroup.AsInteger:=quObrPredzSubGroupID.AsInteger;
          teobrPredzTypeVoz.AsString:=quObrPredzTypeVoz.AsString;


          teobrPredzProc.AsInteger:=3;       //�������� 0-������ 1-��������  2-������ ����������
                                             //3-������ 4-������� �����
          teobrPredzNameGr1.AsString:='';
          teobrPredzNameGr2.AsString:='';
          teobrPredzNameGr3.AsString:='';

          if teClass.Locate('Id',quObrPredzGoodsGroupID.AsInteger,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teobrPredzNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
          end;

          if (iGr>0) then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teobrPredzNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;

          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teobrPredzNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;
          teobrPredz.Post;

          quObrPredz.Next; inc(iC);

          if (iC mod iStep = 0) then
          begin
            PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;

        end;
        PBar1.Position:=100; delay(50);
        teObrPredz.First;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        PBar1.Visible:=False;
      end
      else
      begin
        PBar1.Position:=100; delay(50);
        Memo1.Lines.Add('��� ���������� �� �������');
        PBar1.Visible:=False;
      end;
      quObrPredz.Active:=False;
      teClass.Active:=False;

      ViewObrPredz.EndUpdate;
    end;
  end;
end;

procedure clearpredz;
  var      mindocdate,maxdocdate,pzak:integer;
           Qpredz,Qobr:real;
           iStep,iC:INteger;
begin

  with dmP do
  with fmObrPredz do
  begin
    if ptHPredz.Active=false then ptHPredz.Active:=true;
    if ptSPredz.Active=false then ptSPredz.Active:=true;

    PBar1.Position:=0;
    fmObrPredz.Show;
    PBar1.Visible:=True;

    Memo1.Clear;
    Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ��������� ������.'); delay(10);

    Qobr:=fmObrPredz.teobrPredzQObr.AsFloat;
    if (Qobr>0) then
    begin


      quSelSpV.First;
      mindocdate:=quSelSpVDOCDATE.AsInteger;
      maxdocdate:=quSelSpVDOCDATE.AsInteger;
      while not quSelSpV.Eof do
      begin
        if (mindocdate<quSelSpVDOCDATE.AsInteger) then maxdocdate:=quSelSpVDOCDATE.AsInteger //������� ������ ��� ���������� ���� �� ���������� ��� ���-��
        else maxdocdate:=mindocdate;

        ptSPredz.Locate('ID',quSelSpVID.AsInteger,[]);

        Qpredz:=ptSPredzQRem.AsFloat;

        if ((Qpredz-Qobr)<0.001) then //��� ���-�� ��������� ������ ��� ����� ���-�� � ����������
        begin
          Qobr:=(Qobr-Qpredz);

          ptSPredz.Edit;
          ptSPredzQRem.AsFloat:=0;
          ptSPredzStatus.AsString:='2';
          ptSPredz.Post;
        end
        else
        begin
          if ((Qpredz-Qobr)>0) then  //��� ���-�� � ���������� ������ ���-�� ���������
          begin
            ptSPredz.Edit;
            ptSPredzQRem.AsFloat:=Qpredz-Qobr;
            ptSPredzStatus.AsString:='1';
            ptSPredz.Post;
            Qobr:=0;
          end;
        end;
        if (Qobr=0) then break;
        quSelSpV.Next;
      end;
      PBar1.Position:=10; delay(10);
                                   //��������� ���� � ��������� ��� ���������� �������, ������ ������ ������ ��������� �������
                                  //���� ����� ������ ������� �� ������ ������ �������� �������
      quDocVoz2.Active:=false;
      quDocVoz2.ParamByName('DATEB').AsInteger:=mindocdate;
      quDocVoz2.ParamByName('DATEE').AsInteger:=maxdocdate;
      quDocVoz2.Active:=true;

      PBar1.Position:=40; delay(50);

      if quDocVoz2.RecordCount>0 then
      begin
        quDocVoz2.First;
        iStep:=quDocVoz2.RecordCount div 60;
        if iStep=0 then iStep:=quDocVoz2.RecordCount;
        iC:=0;
        while not quDocVoz2.Eof do
        begin
          ptHPredz.Locate('ID',quDocVoz2ID.AsInteger,[]);

          quSpecVoz2.Active:=false;
          quSpecVoz2.ParamByName('HEAD').AsInteger:=quDocVoz2ID.AsInteger;
          quSpecVoz2.Active:=true;

          //�������� ������ ��������� ���������� �� ������� ������������
          quSpecVoz2.First;
          pzak:=1;
          while not quSpecVoz2.Eof do
          begin
            if (quSpecVoz2Status.AsString<>'2') then
            begin
              if (quSpecVoz2Status.AsString='1') then
              begin
                pzak:=(-1);
                break;
              end else pzak:=0;
            end
            else
            begin
              if pzak=0 then
              begin
                pzak:=(-1);
                break;
              end;
            end;
            quSpecVoz2.Next;
          end;
          ptHPredz.edit;
          if pzak=0 then ptHPredzSTATUS.AsString:='0';
          if pzak=(-1) then ptHPredzSTATUS.AsString:='1';
          if pzak=1 then ptHPredzSTATUS.AsString:='2';
          ptHPredz.Post;

          //�������� ���������
          quDocVoz2.Next;
          inc(iC);

          if (iC mod iStep = 0) then
          begin
            PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'��������� ������ ��.'); delay(10);
        PBar1.Position:=100; delay(30);
      end;
    end;
  end;
end;
procedure TfmObrPredz.acChangeCliExecute(Sender: TObject);
begin
  with dmP do
  begin
    if teobrPredz.RecordCount>0 then
    begin
      fmPD.LPOST.Visible:=true;
      fmPD.LDep.Visible:=false;
      fmPD.Tag:=0;
     //

      if (quPostVozTypeVoz.AsInteger=3)then
      begin
        quCliNotVoz.Active:=false;
        quCliNotVoz.Active:=true;
        closete(fmPD.techcli);

        fmPD.VPOST.BeginUpdate;
        fmPD.techcli.Append;
        fmPD.techcliidcli.asinteger:=quCliNotVozidcli.AsInteger;
        fmPD.techcliNameCli.asstring:=quCliNotVozName.Asstring;
        fmPD.techcli.post;

        quCliNotVoz.Active:=false;
      end
      else
      begin
        quObPost.Active:=False;
        quObPost.ParamByName('IDCARD').AsInteger:=teobrPredzIdCode1.AsInteger;
        quObPost.ParamByName('DDATEB').AsDate:=trunc(Date-730);
        quObPost.ParamByName('DDATEE').AsDate:=trunc(Date);
        quObPost.Active:=True;

        quObPost.First;

        closete(fmPD.techcli);

        fmPD.VPOST.BeginUpdate;

        while not quObPost.Eof do
        begin
          fmPD.techcli.Append;
          fmPD.techcliidcli.asinteger:=quObPostidcli.AsInteger;
          fmPD.techcliNameCli.asstring:=quObPostNamePost.Asstring;
          fmPD.techcli.post;

          qufacli.active:=false;
          qufacli.ParamByName('ICLI').AsInteger:=quObPostidcli.AsInteger;
          qufacli.active:=true;

          if qufacli.RecordCount>0 then
          begin
            fmPD.techcli.Append;
            fmPD.techcliidcli.asinteger:=qufacliID.AsInteger;
            fmPD.techcliNameCli.asstring:=qufacliName.AsString;
            fmPD.techcli.post;
          end;
          qufacli.active:=false;

          quObPost.Next;
        end;
        quObPost.Active:=False;
      end;
      fmPD.VPOST.EndUpdate;

      fmPD.Caption:=teobrPredzNameC.AsString+': ���������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',Date-730)+' �� '+FormatDateTime('dd.mm.yyyy ',Date);
      fmPD.Show;
    end;
  end;
end;


procedure TfmObrPredz.acPeriodExecute(Sender: TObject);
begin
  updallform;
end;

procedure TfmObrPredz.SpeedItem1Click(Sender: TObject);
begin
 close;
end;

procedure TfmObrPredz.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  iCol:=0;
  if Person.Name='OPER CB' then N1.Visible:=true
  else N1.Visible:=false;
  if Person.Name='OPERZAK' then N1.Visible:=true
  else N1.Visible:=false;
end;

procedure TfmObrPredz.FormCreate(Sender: TObject);
begin
  GrObrPredz.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewObrPredz.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmObrPredz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewObrPredz.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  teobrPredz.Active:=False;
  dmP.ptSPredz.Active:=false;
  dmP.ptHPredz.Active:=false;
  dmP.quSelCena.Active:=False;
  dmP.quSpecVoz.Active:=false;
end;

procedure TfmObrPredz.ViewObrPredzCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
 ViewObrPredz.Controller.FocusRecord(ViewObrPredz.DataController.FocusedRowIndex,True);
end;

procedure TfmObrPredz.teobrPredzProcChange(Sender: TField);
begin
 if icol=1 then teobrPredz.Edit;
end;

procedure TfmObrPredz.ViewObrPredzEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewObrPredz.Controller.FocusedColumn.Name='ViewObrPredzProc' then iCol:=1;
end;

procedure TfmObrPredz.ViewObrPredzProcPropertiesCloseUp(Sender: TObject);
begin
   if teobrPredz.State in [dsEdit,dsInsert] then teobrPredz.Post;
   if (teobrPredzProc.AsInteger<>3) and (teobrPredzQObr.AsFloat>0) then
   begin
     if MessageDlg('��������� ��������� ��������?',mtConfirmation,[mbYes, mbNo], 0)=mrYES then
     begin
       acProc.Execute;
       teobrPredz.Edit;
       teobrPredzProc.AsInteger:=3;
       
       teobrPredz.Post;
     end
     else
     begin
       teobrPredz.Edit;
       teobrPredzProc.AsInteger:=3;
       teobrPredz.Post;
     end;
   end
   else
   begin
     teobrPredz.Edit;
     teobrPredzProc.AsInteger:=3;
     teobrPredz.Post;
   end;

end;

procedure TfmObrPredz.acProcExecute(Sender: TObject);
var Q,Qobr:real;
begin
  with dmMC do
  with dmP do
  begin
   if teobrPredz.State in [dsEdit,dsInsert] then teobrPredz.Post;

   Q:=teobrPredzQOut.AsFloat;
   Qobr:=teobrPredzQObr.AsFloat;
   if ptSPredz.Active=false then ptSPredz.Active:=true;
   if ptHPredz.Active=false then ptHPredz.Active:=true;

   if (Q>0) and (Qobr>0) then
   begin
     quSelSpV.Active:=false;
     quSelSpV.ParamByName('CODE').AsInteger:=teobrPredzIdCode1.AsInteger;
     quSelSpV.ParamByName('DEP').AsInteger:=teobrPredzDepart.AsInteger;
     quSelSpV.ParamByName('ICLI').AsInteger:=teobrPredziCli.AsInteger;
     quSelSpV.Active:=true;

     if quSelSpV.RecordCount>0 then
     begin

       case teobrPredzProc.AsInteger of
        0:begin
            clearpredz;
          //  updallform;      //��� �������� ��������� ������ ���-�� � �������
            if (Q-Qobr)<0.001 then teobrPredz.Delete
            else
            begin
              teobrPredz.Edit;
              teobrPredzQOut.AsFloat:=Q-Qobr;
              teobrPredzQObr.AsFloat:=0;
              teobrPredz.Post;
            end;
          end;

     {   1:begin   //������� ���� ������������
            clearpredz;
          //� ��� ���� �������� ����� � ������� ��������
            fl:=0;

            while fl=0 do
            begin
              fl:=1;
              iNum:=prFindNum(3)+1;
              quC.Active:=False;
              quC.SQL.Clear;
              quC.SQL.Add('Select Count(*) as RecCount from "TTNSelf"');
              quC.SQL.Add(' where Depart='+its(teobrPredzDepart.AsInteger));
              quC.SQL.Add(' and DateInvoice='''+FormatDateTime(sCrystDate,d)+'''');
              quC.SQL.Add(' and Number='+its(iNum));

              quC.Active:=true;
              if (quCRecCount.AsInteger>0) then
              begin
               iNum:=iNum+1;
               fl:=0;
              end;
            end;

            try
              //��������� �����
              quE.SQL.Clear;
              quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
              quE.SQL.Add('where CodeObject=12');
              quE.ExecSQL;
            finally
              IDH:=prMax('DocsVn')+1;
            end;

            if (Qobr>Q) then Qobr:=Q;

            try
              quSelCena.Active:=False;
              quSelCena.ParamByName('CODE').AsInteger:=teobrPredzIdCode1.AsInteger;
              quSelCena.ParamByName('DEP').AsInteger:=teobrPredzDepart.AsInteger;
              quSelCena.ParamByName('ICLI').AsInteger:=teobrPredziCli.AsInteger;
              quSelCena.Active:=True;
            finally
              Cena:=RoundVal(quSelCenaPRICEIN.AsFloat);
              NewCena:=RoundVal(quSelCenaRPrice.AsFloat);
              SumCena:=RoundVal(Cena*Qobr);
              SumNewCena:=RoundVal(NewCena*Qobr);
            end;

            try
              //��������� ���������
              quA.Active:=False;
              quA.SQL.Clear;
              quA.SQL.Add('INSERT into "TTNSelf" values (');
              quA.SQL.Add(its(teobrPredzDepart.AsInteger)+',');       //Depart
              quA.SQL.Add(''''+ds(d)+''',');                          //DateInvoice
              quA.SQL.Add(''''+its(iNum)+''',');                      //Number
              quA.SQL.Add('95,');                                     // FlowDepart
              quA.SQL.Add(fs(SumCena)+',');                         // SummaTovarSpis
              quA.SQL.Add(fs(SumNewCena)+',');                      // SummaTovarMove
              quA.SQL.Add(fs(SumNewCena)+',');                      // SummaTovarNew
              quA.SQL.Add('0,');                // SummaTara
              quA.SQL.Add('2,');                // Oprihod
              quA.SQL.Add('0,');                // AcStatusP
              quA.SQL.Add('0,');                // ChekBuhP
              quA.SQL.Add('0,');                // AcStatusR
              quA.SQL.Add('0,');                // ChekBuhR
              quA.SQL.Add('2,');                // ProvodTypeP
              quA.SQL.Add('0,');                // ProvodTypeR
              quA.SQL.Add('0,');                // GoodsSpisNDS10
              quA.SQL.Add('0,');                // GoodsSpisNDS20
              quA.SQL.Add('0,');               // GoodsMoveNDS10
              quA.SQL.Add('0,');               // GoodsMoveNDS20
              quA.SQL.Add('0,');                // GoodsNewNDS10
              quA.SQL.Add('0,');                // GoodsNewNDS20
              quA.SQL.Add('0,');                // GoodsLgtNDSSpis
              quA.SQL.Add('0,');               // GoodsLgtNDSMove
              quA.SQL.Add('0,');              // GoodsLgtNDSNew
              quA.SQL.Add('0,');              // GoodsNSPSpis10
              quA.SQL.Add('0,');              // GoodsNSPSpis20
              quA.SQL.Add('0,');              // GoodsNSPNew10
              quA.SQL.Add('0,');              // GoodsNSPNew20
              quA.SQL.Add('0,');              // GoodsNSP0Spis
              quA.SQL.Add('0,');              // GoodsNSP0New
              quA.SQL.Add(its(IDH)+',');      // ID
              quA.SQL.Add('0,');              // LinkInvoice
              quA.SQL.Add('0,');              // StartTransfer
              quA.SQL.Add('0,');              // EndTransfer
              quA.SQL.Add('''''');            // Rezerv
              quA.SQL.Add(')');
              quA.ExecSQL;
            finally
              try
                //��������� ������������
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "TTNSelfLn" values (');
                quA.SQL.Add(its(teobrPredzDepart.AsInteger));                       //  Depart
                quA.SQL.Add(','''+ds(d)+'''');                                    //DateInvoice
                quA.SQL.Add(','+its(iNum));                                       //  Number
                quA.SQL.Add(','+its(teobrPredzIdGroup.AsInteger));                // CodeGroup
                quA.SQL.Add(','+its(teobrPredzIdSGroup.AsInteger));                // CodePodgr
                quA.SQL.Add(','+its(teobrPredzIdCode1.AsInteger));                // CodeTovar
                quA.SQL.Add(','+its(teobrPredzEdIzm.AsInteger));                 // CodeEdIzm
                quA.SQL.Add(',2');                                        // TovarType
                quA.SQL.Add(','''+quSelCenaBarCode.AsString+'''');              // BarCode
                quA.SQL.Add(',0');                                              // KolMest taSpecVnKolMest.AsFloat
                quA.SQL.Add(',0');                                             // KolEdMest
                quA.SQL.Add(','+its(RoundEx(Qobr)));       //kolwithmest
                quA.SQL.Add(','+its(RoundEx(Qobr)));     // Kol
                quA.SQL.Add(','+fs(NewCena));                                               // CenaTovarMove
                quA.SQL.Add(','+fs(Cena));                                     // CenaTovarSpis
                quA.SQL.Add(',0');                                              // Procent
                quA.SQL.Add(','+fs(NewCena));                                    // NewCenaTovar
                quA.SQL.Add(','+fs(SumCena));                                    // SumCenaTovarSpis
                quA.SQL.Add(','+fs(SumNewCena));                                 // SumNewCenaTovar
                quA.SQL.Add(',0');                                              // CodeTara
                quA.SQL.Add(',0');                                              // VesTara
                quA.SQL.Add(',0');                                              // CenaTara
                quA.SQL.Add(',0');                                             // SumVesTara   - ���-�� �� ���������
                quA.SQL.Add(',0');                                              // SumCenaTara
                quA.SQL.Add(',2');                                              // ChekBuhPrihod
                quA.SQL.Add(',2');                                              // ChekBuhRashod
                quA.SQL.Add(')');
                quA.ExecSQL;

              finally
                Memo1.Lines.Add('�������� �� �������� ������');
                updallform;
               // updoneform;
              end;
            end;
          end;       }
        2:begin
            quPostVoz.Active:=False;
            quPostVoz.ParamByName('IDCARD').AsInteger:=teobrPredzIdCode1.AsInteger;
            quPostVoz.Active:=True;
            if (teobrPredziCli.AsInteger=2471) and (quPostVozTypeVoz.AsInteger=3) then
            begin
              showmessage('����� ������������. ������� ���������� ������!');
              ptSPredz.Active:=false;
              ptHPredz.Active:=false;
              quSelSpV.Active:=false;
              quPostVoz.Active:=False;
              teobrPredz.Edit;
              teobrPredzQObr.AsFloat:=0;
              teobrPredz.Post;
              exit;
            end;
            
            acChangeCli.Execute;
          end;

        4:begin
            acChangeDep.Execute;
          end;

       end;

     end;

   end
   else showmessage('������� ���-�� ��� ���������!');
  end;
end;

procedure TfmObrPredz.acChangeDepExecute(Sender: TObject);
begin
  fmPD.Tag:=1;
  fmPd.LPOST.Visible:=false;
  fmPd.LDep.Visible:=true;

  fmPD.VDEP.BeginUpdate;
  dmP.quDepVoz.Active:=False;
  dmP.quDepVoz.Active:=True;
  dmP.quDepVoz.First;
  fmPD.VDEP.EndUpdate;

  fmPD.Caption:='������';
  fmPD.Show;
end;

procedure TfmObrPredz.SpeedItem3Click(Sender: TObject);
begin
prNExportExel5(ViewObrPredz);
end;

procedure TfmObrPredz.acAllPostExecute(Sender: TObject);
  var Q,Qobr:real;
begin
  with dmMC do
  with dmP do
  begin
    if teobrPredz.RecordCount<1 then exit;

    if teobrPredz.State in [dsEdit,dsInsert] then teobrPredz.Post;

    Q:=teobrPredzQOut.AsFloat;
    Qobr:=teobrPredzQObr.AsFloat;
    if ptSPredz.Active=false then ptSPredz.Active:=true;
    if ptHPredz.Active=false then ptHPredz.Active:=true;

    if (Q>0) and (Qobr>0){ and (Q>=Qobr)} then
    begin
      quSelSpV.Active:=false;
      quSelSpV.ParamByName('CODE').AsInteger:=teobrPredzIdCode1.AsInteger;
      quSelSpV.ParamByName('DEP').AsInteger:=teobrPredzDepart.AsInteger;
      quSelSpV.ParamByName('ICLI').AsInteger:=teobrPredziCli.AsInteger;
      quSelSpV.Active:=true;

      if quSelSpV.RecordCount>0 then
      begin
        fmPD.LPOST.Visible:=true;
        fmPD.LDep.Visible:=false;
        fmPD.Tag:=0;
       //
        quAllCli.Active:=False;
        quAllCli.Active:=True;

        quAllCli.First;

        closete(fmPD.techcli);

        fmPD.VPOST.BeginUpdate;

        while not quAllCli.Eof do
        begin
          fmPD.techcli.Append;
          fmPD.techcliidcli.asinteger:=quAllCliID.AsInteger;
          fmPD.techcliNameCli.asstring:=quAllCliName.AsString;
          fmPD.techcli.post;

          quAllCli.Next;
        end;
        fmPD.techcli.First;
        quAllCli.Active:=False;

        fmPD.VPOST.EndUpdate;

        fmPD.Caption:=' ��� ���������� ';
        fmPD.Show;
      end;
    end
    else showmessage('������� ���-�� ��� ���������');

  end;
end;

procedure TfmObrPredz.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus; Edit1.SelectAll;
end;

procedure TfmObrPredz.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC:INteger;
begin

  Edit1.SetFocus;
  Edit1.SelectAll;

  sBar:=Edit1.Text;
  if sBar='' then exit;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then        //�������
  begin
    sBar:=copy(sBar,1,7);
  end;

  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if prFindBarC(sBar,0,iC) then teobrPredz.Locate('IdCode1',iC,[])
  else Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+' �������� � �� '+sBar+' ���.');

  ViewObrPredz.Controller.FocusRecord(ViewObrPredz.DataController.FocusedRowIndex,True);
end;

procedure TfmObrPredz.N2Click(Sender: TObject);
begin
  with dmP do
  with dmMC do
  begin

    quReasV.Active:=false;
    quReasV.Active:=true;
    fmReasonVoz.ShowModal;
    if fmReasonVoz.ModalResult=mrOK then
    begin
      if quReasVReason.AsString>'0' then
      begin
        quE.SQL.Clear;
        quE.SQL.Add('Update "A_DOCSRET" Set IDReason='+its(quReasVid.AsInteger)+'');
        quE.SQL.Add(' where IDCLI='+its(fmObrPredz.teobrPredziCli.AsInteger)+'');
        quE.SQL.Add(' and DEPART='+its(fmObrPredz.teobrPredzDepart.AsInteger)+'');
        quE.SQL.Add(' and CODE='+its(fmObrPredz.teobrPredzIdCode1.AsInteger)+'');
        quE.ExecSQL;
        updallform;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+' ������� ������� �� ����������, ������, ������  '+teobrPredzNameCli.AsString+'  '+teobrPredzNAMEDEP.AsString+'  '+teobrPredzNameC.AsString) ;
      end
      else  Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+' ������ �������� ������ �������');
    end;
    quReasV.Active:=false;
  end;

end;

end.
