object fmTBuff: TfmTBuff
  Left = 291
  Top = 216
  BorderStyle = bsDialog
  Caption = #1041#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
  ClientHeight = 349
  ClientWidth = 484
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GridTH: TcxGrid
    Left = 8
    Top = 8
    Width = 369
    Height = 337
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewD: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMT.dsHeadDoc
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewDIType: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'IType'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1055#1088'.'#1085#1072#1082#1083'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1056#1072#1089#1093'.'#1085#1072#1082#1083'.'
            Value = 2
          end
          item
            Description = #1048#1085#1074#1077#1085#1090'.'
            Value = 20
          end
          item
            Description = #1042#1085'.'#1085#1072#1082#1083' '
            Value = 4
          end
          item
            Description = #1042#1085'.'#1085#1072#1082#1083' '
            Value = 3
          end
          item
            Description = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
            Value = 12
          end
          item
            Description = #1056#1077#1072#1083#1080#1079'. '
            Value = 7
          end>
      end
      object ViewDId: TcxGridDBColumn
        Caption = #1042#1085'.'#1082#1086#1076
        DataBinding.FieldName = 'Id'
        Visible = False
      end
      object ViewDNameCli: TcxGridDBColumn
        Caption = #1054#1090' '#1082#1086#1075#1086
        DataBinding.FieldName = 'NameCli'
        Width = 120
      end
      object ViewDIdSkl: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'IdSkl'
        Visible = False
      end
      object ViewDDateDoc: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DateDoc'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewDNumDoc: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'NumDoc'
        Width = 65
      end
      object ViewDIdCli: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
        DataBinding.FieldName = 'IdCli'
        Visible = False
      end
      object ViewDNameSkl: TcxGridDBColumn
        Caption = #1052#1061
        DataBinding.FieldName = 'NameSkl'
        Width = 120
      end
      object ViewDSumIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SumIN'
      end
      object ViewDSumUch: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1091#1095'.'
        DataBinding.FieldName = 'SumUch'
      end
    end
    object ViewDSpec: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMT.dsSpecDoc
      DataController.DetailKeyFieldNames = 'IType;IdHead'
      DataController.KeyFieldNames = 'IType;IdHead'
      DataController.MasterKeyFieldNames = 'IType;Id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewDSpecIType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IType'
        Visible = False
      end
      object ViewDSpecIdHead: TcxGridDBColumn
        Caption = #1042#1085'.'#1082#1086#1076' '#1076#1086#1082'.'
        DataBinding.FieldName = 'IdHead'
        Visible = False
      end
      object ViewDSpecNum: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087'.'
        DataBinding.FieldName = 'Num'
        Width = 40
      end
      object ViewDSpecNameC: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameC'
        Width = 120
      end
      object ViewDSpecIdCard: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IdCard'
        Width = 40
      end
      object ViewDSpecColumn1: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1084#1077#1089#1090
        DataBinding.FieldName = 'QuantM'
      end
      object ViewDSpecQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'Quant'
      end
      object ViewDSpecPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewDSpecSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
        DataBinding.FieldName = 'SumIn'
      end
      object ViewDSpecPriceUch: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
        DataBinding.FieldName = 'PriceUch'
      end
      object ViewDSpecSumUch: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076'.'
        DataBinding.FieldName = 'SumUch'
      end
      object ViewDSpecIdNds: TcxGridDBColumn
        Caption = #1053#1044#1057
        DataBinding.FieldName = 'IdNds'
        Visible = False
      end
      object ViewDSpecSumNds: TcxGridDBColumn
        DataBinding.FieldName = 'SumNds'
        Visible = False
      end
      object ViewDSpecSm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'Sm'
        Visible = False
      end
      object ViewDSpecIdM: TcxGridDBColumn
        DataBinding.FieldName = 'IdM'
        Visible = False
      end
      object ViewDSpecProcPrice: TcxGridDBColumn
        Caption = #1053#1072#1094#1077#1085#1082#1072
        DataBinding.FieldName = 'ProcPrice'
      end
    end
    object LevelD: TcxGridLevel
      GridView = ViewD
      Visible = False
      object LevelDSpec: TcxGridLevel
        GridView = ViewDSpec
        Visible = False
      end
    end
  end
  object cxButton1: TcxButton
    Left = 392
    Top = 16
    Width = 81
    Height = 25
    Caption = #1042#1089#1090#1072#1074#1080#1090#1100
    ModalResult = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 392
    Top = 56
    Width = 81
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 392
    Top = 304
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 392
    Top = 272
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 4
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
end
