unit CatDisc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, ExtCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxCheckBox;

type
  TfmCatDisc = class(TForm)
    GrCatDisc: TcxGrid;
    ViewCatDisc: TcxGridDBTableView;
    ViewCatDiscGr: TcxGridDBColumn;
    ViewCatDiscSgr: TcxGridDBColumn;
    ViewCatDiscId: TcxGridDBColumn;
    LevelCatDisc: TcxGridLevel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewCatDiscD: TcxGridDBColumn;
    ViewCatDiscName: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewCatDiscDPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCatDisc: TfmCatDisc;

implementation
uses MDB,MT,Un1;

{$R *.dfm}

procedure TfmCatDisc.FormCreate(Sender: TObject);
begin
GrCatDisc.Align:=AlClient;
end;

procedure TfmCatDisc.cxButton1Click(Sender: TObject);
begin
prNExportExel5(ViewCatDisc);
end;

procedure TfmCatDisc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
dmMC.quCatDisc.Active:=false;
dmMT.teCatDisc.Active:=false;
end;

procedure TfmCatDisc.cxButton2Click(Sender: TObject);
begin
close;
//dmMC.quCatDisc.Active:=False;
//dmMT.teCatDisc.Active:=False;
end;

procedure TfmCatDisc.ViewCatDiscDPropertiesChange(Sender: TObject);
begin
  with dmMt do
  with dmMC do
  begin
    if teCatDisc.State in [dsEdit,dsInsert] then
    begin
      teCatDisc.Post;
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "SubGroup" Set ');
      quE.SQL.Add('V01='''+its(teCatDiscV01.AsInteger)+'''');
      quE.SQL.Add('where ID='+ its(teCatDiscID.AsInteger));
      quE.ExecSQL;
    end;
  end;

end;

end.
