object fmSortF: TfmSortF
  Left = 643
  Top = 545
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1080
  ClientHeight = 291
  ClientWidth = 323
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 71
    Height = 13
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100'  '
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 12
    Height = 13
    Caption = #1087#1086
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 226
    Width = 323
    Height = 65
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 56
      Top = 16
      Width = 89
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 192
      Top = 16
      Width = 91
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 112
    Top = 20
    Properties.KeyFieldNames = 'NameF'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Capt'
      end>
    Properties.ListSource = dstaRowF
    EditValue = ''
    TabOrder = 1
    Width = 185
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 112
    Top = 52
    Properties.KeyFieldNames = 'NameF'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Capt'
      end>
    Properties.ListSource = dstaColVal
    EditValue = ''
    TabOrder = 2
    Width = 185
  end
  object Panel2: TPanel
    Left = 24
    Top = 88
    Width = 265
    Height = 33
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxRadioButton1: TcxRadioButton
      Left = 16
      Top = 8
      Width = 113
      Height = 17
      Caption = #1087#1086' '#1091#1073#1099#1074#1072#1085#1080#1102
      Checked = True
      TabOrder = 0
      TabStop = True
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
    object cxRadioButton2: TcxRadioButton
      Left = 144
      Top = 8
      Width = 113
      Height = 17
      Caption = #1087#1086' '#1074#1086#1079#1088#1072#1089#1090#1072#1085#1080#1102
      TabOrder = 1
      TabStop = True
      LookAndFeel.Kind = lfOffice11
      Transparent = True
    end
  end
  object GroupBox1: TGroupBox
    Left = 24
    Top = 136
    Width = 265
    Height = 65
    Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100
    TabOrder = 4
    object Label3: TLabel
      Left = 88
      Top = 16
      Width = 40
      Height = 13
      Caption = #1087#1077#1088#1074#1099#1093' '
      Transparent = True
    end
    object cxCheckBox1: TcxCheckBox
      Left = 8
      Top = 24
      Caption = #1074#1089#1077
      Properties.OnChange = cxCheckBox1PropertiesChange
      State = cbsChecked
      TabOrder = 0
      Transparent = True
      Width = 57
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 136
      Top = 12
      Enabled = False
      TabOrder = 1
      Value = 20
      Width = 89
    end
    object CheckBox1: TCheckBox
      Left = 88
      Top = 40
      Width = 161
      Height = 17
      Caption = #1087#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1086#1089#1090#1072#1083#1100#1085#1099#1077
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 2
    end
  end
  object taRowF: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 184
    object taRowFId: TSmallintField
      FieldName = 'Id'
    end
    object taRowFCapt: TStringField
      FieldName = 'Capt'
      Size = 30
    end
    object taRowFNameF: TStringField
      FieldName = 'NameF'
      Size = 30
    end
  end
  object taColVal: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 136
    object taColValId: TSmallintField
      FieldName = 'Id'
    end
    object taColValCapt: TStringField
      FieldName = 'Capt'
      Size = 30
    end
    object taColValNameF: TStringField
      FieldName = 'NameF'
      Size = 30
    end
  end
  object dstaRowF: TDataSource
    DataSet = taRowF
    Left = 280
  end
  object dstaColVal: TDataSource
    DataSet = taColVal
    Left = 232
  end
end
