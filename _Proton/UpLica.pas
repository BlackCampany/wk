unit UpLica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, SpeedBar, ExtCtrls,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmUpLica = class(TForm)
    ViewUPL: TcxGridDBTableView;
    LevelUPL: TcxGridLevel;
    GrUPL: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    FormPlacement1: TFormPlacement;
    ViewUPLID: TcxGridDBColumn;
    ViewUPLIMH: TcxGridDBColumn;
    ViewUPLNAME: TcxGridDBColumn;
    ViewUPLIACTIVE: TcxGridDBColumn;
    ViewUPLNAMEMH: TcxGridDBColumn;
    acUPL: TActionManager;
    acAddUPL: TAction;
    acEditUPL: TAction;
    acDelUPL: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddUPLExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acEditUPLExecute(Sender: TObject);
    procedure acDelUPLExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUpLica: TfmUpLica;

implementation

uses Un1, dbe, AddUPL, MT, u2fdk;

{$R *.dfm}

procedure TfmUpLica.FormCreate(Sender: TObject);
begin
  GrUPL.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewUPL.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmUpLica.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewUPL.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmUpLica.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmUpLica.acAddUPLExecute(Sender: TObject);
Var iM:INteger;
begin
//  if not CanDo('prAddUPL') then begin ShowMessage('��� ����.'); exit; end;
  fmAddUPL:=tfmAddUPL.Create(Application);
  with fmAddUPL do
  begin
    quDeps.Active:=False;
    quDeps.Active:=True;

    quDeps.First;
    fmAddUPL.cxLookupComboBox2.EditValue:=quDepsID.AsInteger;
    fmAddUPL.cxTextEdit1.Text:='';
  end;
  fmAddUPL.ShowModal;
  if fmAddUPL.ModalResult=mrOk then
  begin
    with dmMT do
    with dmE do
    begin
      if ptUPL.Active=False then ptUPL.Active:=True else ptUPL.Refresh;

      if ptUPL.RecordCount>0 then
      begin
        ptUPL.Last;
        iM:=ptUPLID.AsInteger+1;
      end else iM:=1;

      ptUPL.Append;
      ptUPLID.AsInteger:=iM;
      ptUPLIMH.AsInteger:=fmAddUPL.cxLookupComboBox2.EditValue;
      ptUPLNAME.AsString:=AnsiToOemConvert(fmAddUPL.cxTextEdit1.Text);
      ptUPLIACTIVE.AsInteger:=fmAddUPL.cxComboBox1.ItemIndex;
      ptUPL.Post;

      fmUpLica.ViewUPL.BeginUpdate;
      quUPL.Active:=False;
      quUPL.Active:=True;
      quUPL.locate('ID',iM,[]);
      fmUpLica.ViewUPL.EndUpdate;

    end;
  end;
  fmAddUPL.Release;
end;

procedure TfmUpLica.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewUPL);
end;

procedure TfmUpLica.acEditUPLExecute(Sender: TObject);
Var iM:INteger;
begin
  with dmE do
  with dmMT do
  begin
    if quUPL.RecordCount>0 then
    begin
      fmAddUPL:=tfmAddUPL.Create(Application);
      with fmAddUPL do
      begin
        quDeps.Active:=False;
        quDeps.Active:=True;

        quDeps.Locate('ID',quUPLIMH.AsInteger,[]);
        fmAddUPL.cxLookupComboBox2.EditValue:=quDepsID.AsInteger;

        fmAddUPL.cxTextEdit1.Text:=quUPLNAME.AsString;
        fmAddUPL.cxComboBox1.ItemIndex:=quUPLIACTIVE.AsInteger;
      end;

      fmAddUPL.ShowModal;
      if fmAddUPL.ModalResult=mrOk then
      begin
        if ptUPL.Active=False then ptUPL.Active:=True else ptUPL.Refresh;
        iM:=quUPLID.AsInteger;

        if ptUPL.FindKey([iM]) then
        begin
          ptUPL.Edit;
          ptUPLIMH.AsInteger:=fmAddUPL.cxLookupComboBox2.EditValue;
          ptUPLNAME.AsString:=AnsiToOemConvert(fmAddUPL.cxTextEdit1.Text);
          ptUPLIACTIVE.AsInteger:=fmAddUPL.cxComboBox1.ItemIndex;
          ptUPL.Post;

          fmUpLica.ViewUPL.BeginUpdate;
          quUPL.Active:=False;
          quUPL.Active:=True;
          quUPL.locate('ID',iM,[]);
          fmUpLica.ViewUPL.EndUpdate;
        end;
      end;
      fmAddUPL.Release;
    end;
  end;
end;

procedure TfmUpLica.acDelUPLExecute(Sender: TObject);
Var iM:INteger;
begin
  with dmE do
  with dmMT do
  begin
    if quUPL.RecordCount>0 then
    begin
      if MessageDlg('�� �������, ��� ����� ������� �������������� ���� '+quUPLNAME.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if ptUPL.Active=False then ptUPL.Active:=True else ptUPL.Refresh;
        iM:=quUPLID.AsInteger;

        if ptUPL.FindKey([iM]) then
        begin
          ptUPL.Delete;

          fmUpLica.ViewUPL.BeginUpdate;
          quUPL.Active:=False;
          quUPL.Active:=True;
          fmUpLica.ViewUPL.EndUpdate;
        end;
      end;
    end;
  end;
end;

end.
