object fmAddCassir: TfmAddCassir
  Left = 368
  Top = 342
  Width = 442
  Height = 204
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1083#1080#1094#1077#1085#1079#1080#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 16
    Width = 22
    Height = 13
    Caption = #1048#1084#1103
  end
  object Label1: TLabel
    Left = 12
    Top = 48
    Width = 38
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100
    Transparent = True
    Visible = False
  end
  object Label3: TLabel
    Left = 12
    Top = 76
    Width = 86
    Height = 13
    Caption = #1060#1072#1081#1083' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Transparent = True
  end
  object Panel1: TPanel
    Left = 316
    Top = 0
    Width = 118
    Height = 170
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 56
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 249
  end
  object cxTextEdit2: TcxTextEdit
    Left = 56
    Top = 44
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit2'
    Visible = False
    Width = 133
  end
  object cxCheckBox1: TcxCheckBox
    Left = 16
    Top = 132
    Caption = #1059#1074#1086#1083#1077#1085
    Properties.ValueChecked = 0
    Properties.ValueGrayed = -1
    Properties.ValueUnchecked = 1
    TabOrder = 3
    Width = 121
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 24
    Top = 96
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 4
    Text = 'cxButtonEdit1'
    Width = 281
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 192
    Top = 128
  end
end
