unit RepHour;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ComCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView;

type
  TfmRealH = class(TForm)
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    GridRealH: TcxGrid;
    levelRealH: TcxGridLevel;
    StatusBar1: TStatusBar;
    frRep1: TfrReport;
    frRealH: TfrDBDataSet;
    ViewRealH: TcxGridDBBandedTableView;
    ViewRealHCl00: TcxGridDBBandedColumn;
    ViewRealHS00: TcxGridDBBandedColumn;
    ViewRealHCl01: TcxGridDBBandedColumn;
    ViewRealHS01: TcxGridDBBandedColumn;
    ViewRealHCl02: TcxGridDBBandedColumn;
    ViewRealHS02: TcxGridDBBandedColumn;
    ViewRealHCl03: TcxGridDBBandedColumn;
    ViewRealHS03: TcxGridDBBandedColumn;
    ViewRealHCl04: TcxGridDBBandedColumn;
    ViewRealHS04: TcxGridDBBandedColumn;
    ViewRealHCl05: TcxGridDBBandedColumn;
    ViewRealHS05: TcxGridDBBandedColumn;
    ViewRealHCl06: TcxGridDBBandedColumn;
    ViewRealHS06: TcxGridDBBandedColumn;
    ViewRealHCl07: TcxGridDBBandedColumn;
    ViewRealHS07: TcxGridDBBandedColumn;
    ViewRealHCl08: TcxGridDBBandedColumn;
    ViewRealHS08: TcxGridDBBandedColumn;
    ViewRealHCl09: TcxGridDBBandedColumn;
    ViewRealHS09: TcxGridDBBandedColumn;
    ViewRealHCl10: TcxGridDBBandedColumn;
    ViewRealHS10: TcxGridDBBandedColumn;
    ViewRealHCl11: TcxGridDBBandedColumn;
    ViewRealHS11: TcxGridDBBandedColumn;
    ViewRealHCl12: TcxGridDBBandedColumn;
    ViewRealHS12: TcxGridDBBandedColumn;
    ViewRealHCl13: TcxGridDBBandedColumn;
    ViewRealHS13: TcxGridDBBandedColumn;
    ViewRealHCl14: TcxGridDBBandedColumn;
    ViewRealHS14: TcxGridDBBandedColumn;
    ViewRealHCl15: TcxGridDBBandedColumn;
    ViewRealHS15: TcxGridDBBandedColumn;
    ViewRealHCl16: TcxGridDBBandedColumn;
    ViewRealHS16: TcxGridDBBandedColumn;
    ViewRealHCl17: TcxGridDBBandedColumn;
    ViewRealHS17: TcxGridDBBandedColumn;
    ViewRealHCl18: TcxGridDBBandedColumn;
    ViewRealHS18: TcxGridDBBandedColumn;
    ViewRealHCl19: TcxGridDBBandedColumn;
    ViewRealHS19: TcxGridDBBandedColumn;
    ViewRealHCl20: TcxGridDBBandedColumn;
    ViewRealHS20: TcxGridDBBandedColumn;
    ViewRealHCl21: TcxGridDBBandedColumn;
    ViewRealHS21: TcxGridDBBandedColumn;
    ViewRealHCl22: TcxGridDBBandedColumn;
    ViewRealHS22: TcxGridDBBandedColumn;
    ViewRealHCl23: TcxGridDBBandedColumn;
    ViewRealHS23: TcxGridDBBandedColumn;
    ViewRealHM00: TcxGridDBBandedColumn;
    ViewRealHM01: TcxGridDBBandedColumn;
    ViewRealHM02: TcxGridDBBandedColumn;
    ViewRealHM03: TcxGridDBBandedColumn;
    ViewRealHM04: TcxGridDBBandedColumn;
    ViewRealHM05: TcxGridDBBandedColumn;
    ViewRealHM06: TcxGridDBBandedColumn;
    ViewRealHM07: TcxGridDBBandedColumn;
    ViewRealHM08: TcxGridDBBandedColumn;
    ViewRealHM09: TcxGridDBBandedColumn;
    ViewRealHM10: TcxGridDBBandedColumn;
    ViewRealHM11: TcxGridDBBandedColumn;
    ViewRealHM12: TcxGridDBBandedColumn;
    ViewRealHM13: TcxGridDBBandedColumn;
    ViewRealHM14: TcxGridDBBandedColumn;
    ViewRealHM15: TcxGridDBBandedColumn;
    ViewRealHM16: TcxGridDBBandedColumn;
    ViewRealHM17: TcxGridDBBandedColumn;
    ViewRealHM18: TcxGridDBBandedColumn;
    ViewRealHM19: TcxGridDBBandedColumn;
    ViewRealHM20: TcxGridDBBandedColumn;
    ViewRealHM21: TcxGridDBBandedColumn;
    ViewRealHM22: TcxGridDBBandedColumn;
    ViewRealHM23: TcxGridDBBandedColumn;
    ViewRealHQS: TcxGridDBBandedColumn;
    ViewRealHQC: TcxGridDBBandedColumn;
    ViewRealHSS: TcxGridDBBandedColumn;
    ViewRealHSM: TcxGridDBBandedColumn;
    ViewRealHA00: TcxGridDBBandedColumn;
    ViewRealHA01: TcxGridDBBandedColumn;
    ViewRealHA02: TcxGridDBBandedColumn;
    ViewRealHA03: TcxGridDBBandedColumn;
    ViewRealHA04: TcxGridDBBandedColumn;
    ViewRealHA05: TcxGridDBBandedColumn;
    ViewRealHA06: TcxGridDBBandedColumn;
    ViewRealHA07: TcxGridDBBandedColumn;
    ViewRealHA08: TcxGridDBBandedColumn;
    ViewRealHA09: TcxGridDBBandedColumn;
    ViewRealHA10: TcxGridDBBandedColumn;
    ViewRealHA11: TcxGridDBBandedColumn;
    ViewRealHA12: TcxGridDBBandedColumn;
    ViewRealHA13: TcxGridDBBandedColumn;
    ViewRealHA14: TcxGridDBBandedColumn;
    ViewRealHA15: TcxGridDBBandedColumn;
    ViewRealHA16: TcxGridDBBandedColumn;
    ViewRealHA17: TcxGridDBBandedColumn;
    ViewRealHA18: TcxGridDBBandedColumn;
    ViewRealHA19: TcxGridDBBandedColumn;
    ViewRealHA20: TcxGridDBBandedColumn;
    ViewRealHA21: TcxGridDBBandedColumn;
    ViewRealHA22: TcxGridDBBandedColumn;
    ViewRealHA23: TcxGridDBBandedColumn;
    ViewRealHAITOG: TcxGridDBBandedColumn;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure ViewRealHDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRealH: TfmRealH;

implementation

uses Un1, Period1, MDB, uGraf, uGraf2;

{$R *.dfm}

procedure TfmRealH.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+CurIni;
  FormPlacement1.Active:=True;
  ViewRealH.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmRealH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewRealH.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  dmMC.quRepHour.Active:=False;  
end;

procedure TfmRealH.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmRealH.SpeedItem2Click(Sender: TObject);
begin
  //���������� �� �����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit2.Date);
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      StatusBar1.Panels[0].Text:='����� ���� ������������.'; Delay(10);

      ViewRealH.BeginUpdate;
      quRepHour.Active:=False;
      quRepHour.ParamByName('DATEB').Value:=Trunc(CommonSet.DateBeg);
      quRepHour.ParamByName('DATEE').Value:=Trunc(CommonSet.DateEnd);
      quRepHour.Active:=True;
      ViewRealH.EndUpdate;

      StatusBar1.Panels[0].Text:=''; Delay(10);

      fmRealH.Caption:='���������� �� ����� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+'     �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmRealH.SpeedItem1Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewRealH.BeginUpdate;
  with dmMC do
  begin
    quRepHour.Filter:=ViewRealH.DataController.Filter.FilterText;
    quRepHour.Filtered:=True;


    frRep1.LoadFromFile(CommonSet.Reports+ 'SaleHour.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);

    StrWk:=ViewRealH.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRep1.ReportName:='���������� �� �����.';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;

    quRepHour.Filter:='';
    quRepHour.Filtered:=False;
  end;
  ViewRealH.EndUpdate;
end;

procedure TfmRealH.SpeedItem5Click(Sender: TObject);
Var iH:ShortInt;
    StrWk:String;
begin
  with fmGraf1 do
  begin
    SpeedItem6.Visible:=True;
    Chart1.Title.Text.Clear;
    Chart1.Title.Text.Add('���������� �� ����������� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd));
    Chart1.LeftAxis.Title.Caption:='�����������.';
    Chart1.BottomAxis.Title.Caption:='�����.';
    Series1.Active:=False;
    Series2.Active:=False;
    Series3.Active:=False;
    Series4.Active:=False;
    Series5.Active:=False;
    Series6.Active:=False;
    Series7.Active:=False;
    Series8.Active:=False;
    Series9.Active:=False;
    Series10.Active:=False;
    Series11.Active:=False;
    Series12.Active:=False;
    Series13.Active:=False;
    Series14.Active:=False;
    Series15.Active:=False;
    with dmMC do
    begin
      ViewrealH.BeginUpdate;
      quRepHour.First;

      Series1.Active:=True;
      Series1.Title:='�� �����������';
      Series1.Clear;
      CloseTe(taRealH);
      if NOT FileExists('RealH.txt') then taRealH.SaveToTextFile('RealH.txt');
      taRealH.LoadFromTextFile('RealH.txt');

      if  (trunc(CommonSet.DateEnd)-trunc(CommonSet.DateBeg))<>0 then
      begin
      for iH:=0 to 23 do
      begin
        StrWk:=IntToStr(iH); If Length(StrWk)<2 then StrWk:='0'+StrWk;
        Series1.AddXY(iH+1,trunc(quRepHour.FieldByName('Cl'+StrWk).AsInteger/(trunc(CommonSet.DateEnd)-trunc(CommonSet.DateBeg))),'');

        if taRealH.Locate('iH',iH,[]) then
          begin
            taRealH.Edit;
              taRealHCl.AsInteger:=trunc(quRepHour.FieldByName('Cl'+StrWk).AsInteger/(trunc(CommonSet.DateEnd)-trunc(CommonSet.DateBeg)));
              //quRepHour.FieldByName('Cl'+StrWk).AsInteger;
            taRealH.Post;
          end
        else
          begin
            taRealH.Append;
              taRealHiH.AsInteger:=iH;
              taRealHCl.AsInteger:=trunc(quRepHour.FieldByName('Cl'+StrWk).AsInteger/(trunc(CommonSet.DateEnd)-trunc(CommonSet.DateBeg)));
              //quRepHour.FieldByName('Cl'+StrWk).AsInteger;
              taRealHPr.AsInteger:=0;
              taRealHAdm.AsInteger:=0;
            taRealH.Post;
          end;

      end;
      end;
      ViewRealH.EndUpdate;
    end;
  end;
  fmGraf1.Show;
end;

procedure TfmRealH.SpeedItem6Click(Sender: TObject);
Var iH:ShortInt;
    StrWk:String;
begin
  with fmGraf2 do
  begin
    Chart1.Title.Text.Clear;
    Chart1.Title.Text.Add('���������� �� ����� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+'     �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd));
    Chart1.LeftAxis.Title.Caption:='�����.';
    Chart1.BottomAxis.Title.Caption:='�����.';
    Series1.Active:=False;
    Series2.Active:=False;
    Series3.Active:=False;
    Series4.Active:=False;
    Series5.Active:=False;
    Series6.Active:=False;
    Series7.Active:=False;
    Series8.Active:=False;
    Series9.Active:=False;
    Series10.Active:=False;
    Series11.Active:=False;
    Series12.Active:=False;
    Series13.Active:=False;
    Series14.Active:=False;
    Series15.Active:=False;
    with dmMC do
    begin
      ViewrealH.BeginUpdate;
      quRepHour.First;

      Series1.Active:=True;
      Series1.Title:='�� �����';
      for iH:=0 to 23 do
      begin
        StrWk:=IntToStr(iH); If Length(StrWk)<2 then StrWk:='0'+StrWk;
        Series1.AddXY(iH+1,quRepHour.FieldByName('S'+StrWk).AsInteger,'');
      end;

      ViewRealH.EndUpdate;
    end;
  end;
  fmGraf2.Show;
end;

procedure TfmRealH.ViewRealHDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  CalcAvg1(2,0,4,fmRealH.ViewRealH);
end;

end.
