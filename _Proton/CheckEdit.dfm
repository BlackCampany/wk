object fmCheckEdit: TfmCheckEdit
  Left = 347
  Top = 284
  BorderStyle = bsDialog
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1086#1079#1080#1094#1080#1080
  ClientHeight = 393
  ClientWidth = 361
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 20
    Width = 45
    Height = 13
    Caption = #1050#1072#1089#1089#1072' '#8470
  end
  object Label2: TLabel
    Left = 20
    Top = 44
    Width = 47
    Height = 13
    Caption = #1057#1084#1077#1085#1072' '#8470
  end
  object Label3: TLabel
    Left = 20
    Top = 68
    Width = 34
    Height = 13
    Caption = #1063#1077#1082' '#8470
  end
  object Label4: TLabel
    Left = 20
    Top = 92
    Width = 37
    Height = 13
    Caption = #1050#1072#1089#1089#1080#1088
  end
  object Label5: TLabel
    Left = 20
    Top = 140
    Width = 34
    Height = 13
    Caption = #1050#1086#1083'-'#1074#1086
  end
  object Label6: TLabel
    Left = 20
    Top = 116
    Width = 57
    Height = 13
    Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
  end
  object Label7: TLabel
    Left = 20
    Top = 164
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
  end
  object Label8: TLabel
    Left = 20
    Top = 188
    Width = 37
    Height = 13
    Caption = #1057#1082#1080#1076#1082#1072
  end
  object Label9: TLabel
    Left = 20
    Top = 212
    Width = 34
    Height = 13
    Caption = #1057#1091#1084#1084#1072
  end
  object Label10: TLabel
    Left = 20
    Top = 236
    Width = 45
    Height = 13
    Caption = #1054#1090#1076#1077#1083' '#8470
  end
  object Label11: TLabel
    Left = 20
    Top = 260
    Width = 50
    Height = 13
    Caption = #1054#1087#1077#1088#1072#1094#1080#1103
  end
  object Label12: TLabel
    Left = 20
    Top = 284
    Width = 59
    Height = 13
    Caption = #1058#1080#1087' '#1086#1087#1083#1072#1090#1099
  end
  object Label13: TLabel
    Left = 196
    Top = 284
    Width = 78
    Height = 13
    Caption = '0-'#1085#1072#1083', 1-'#1073#1077#1079#1085#1072#1083
  end
  object Label14: TLabel
    Left = 196
    Top = 260
    Width = 112
    Height = 13
    Caption = 'P-'#1087#1088#1086#1076#1072#1078#1072', R-'#1074#1086#1079#1074#1088#1072#1090
  end
  object Panel1: TPanel
    Left = 0
    Top = 333
    Width = 361
    Height = 60
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 64
      Top = 16
      Width = 85
      Height = 29
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 212
      Top = 16
      Width = 85
      Height = 29
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 128
    Top = 16
    Properties.ReadOnly = True
    Properties.SpinButtons.Visible = False
    TabOrder = 1
    Width = 121
  end
  object cxSpinEdit2: TcxSpinEdit
    Left = 128
    Top = 40
    Properties.ReadOnly = True
    Properties.SpinButtons.Visible = False
    TabOrder = 2
    Width = 121
  end
  object cxSpinEdit3: TcxSpinEdit
    Left = 128
    Top = 64
    Properties.ReadOnly = True
    Properties.SpinButtons.Visible = False
    TabOrder = 3
    Width = 121
  end
  object cxSpinEdit4: TcxSpinEdit
    Left = 128
    Top = 88
    Properties.ReadOnly = True
    Properties.SpinButtons.Visible = False
    TabOrder = 4
    Width = 121
  end
  object cxSpinEdit5: TcxSpinEdit
    Left = 128
    Top = 112
    TabOrder = 5
    Width = 121
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 128
    Top = 136
    EditValue = 0.000000000000000000
    TabOrder = 6
    Width = 121
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 128
    Top = 160
    EditValue = 0.000000000000000000
    TabOrder = 7
    Width = 121
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 128
    Top = 184
    EditValue = 0.000000000000000000
    TabOrder = 8
    Width = 121
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 128
    Top = 208
    EditValue = 0.000000000000000000
    TabOrder = 9
    Width = 121
  end
  object cxSpinEdit6: TcxSpinEdit
    Left = 128
    Top = 232
    TabOrder = 10
    Width = 121
  end
  object cxTextEdit1: TcxTextEdit
    Left = 128
    Top = 256
    Properties.MaxLength = 1
    TabOrder = 11
    Text = 'cxTextEdit1'
    Width = 57
  end
  object cxSpinEdit7: TcxSpinEdit
    Left = 128
    Top = 280
    Properties.MaxValue = 1.000000000000000000
    TabOrder = 12
    Width = 57
  end
end
