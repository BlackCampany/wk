unit CorrDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, Menus, cxLookAndFeelPainters, cxButtons;

type
  TfmDocCorr = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxCurrencyEdit7: TcxCurrencyEdit;
    cxCurrencyEdit8: TcxCurrencyEdit;
    cxCurrencyEdit9: TcxCurrencyEdit;
    cxCurrencyEdit10: TcxCurrencyEdit;
    cxCurrencyEdit11: TcxCurrencyEdit;
    cxCurrencyEdit12: TcxCurrencyEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocCorr: TfmDocCorr;

implementation

{$R *.dfm}

procedure TfmDocCorr.FormShow(Sender: TObject);
begin
  cxButton1.SetFocus;
end;

procedure TfmDocCorr.cxButton1Click(Sender: TObject);
Var bErr:Boolean;
begin
  bErr:=False;

  cxButton1.SetFocus;

  cxCurrencyEdit7.PostEditValue;
  cxCurrencyEdit8.PostEditValue;
  cxCurrencyEdit9.PostEditValue;
  cxCurrencyEdit10.PostEditValue;
  cxCurrencyEdit11.PostEditValue;
  cxCurrencyEdit12.PostEditValue;

{  if abs(cxCurrencyEdit7.Value-cxCurrencyEdit8.Value-cxCurrencyEdit9.Value-cxCurrencyEdit10.Value-cxCurrencyEdit11.Value-cxCurrencyEdit12.Value)>0.001 then
  begin
    bErr:=True;
    ShowMessage('��������� ����� �� ���������. ���������� ����������.');
  end;}

  if abs(cxCurrencyEdit1.Value-cxCurrencyEdit7.Value)>0.001 then begin bErr:=True; showmessage('����� �� ��������� �� ���������. ���������� ����������.'); end;
  if abs(cxCurrencyEdit2.Value-cxCurrencyEdit8.Value)>0.001 then begin bErr:=True; showmessage('����� ��� 0% �� ���������. ���������� ����������.'); end;
  if abs(cxCurrencyEdit3.Value+cxCurrencyEdit4.Value-cxCurrencyEdit9.Value-cxCurrencyEdit10.Value)>0.001 then begin bErr:=True; showmessage('����� ��� 10% �� ���������. ���������� ����������.'); end;
  if abs(cxCurrencyEdit5.Value+cxCurrencyEdit6.Value-cxCurrencyEdit11.Value-cxCurrencyEdit12.Value)>0.001 then begin bErr:=True; showmessage('����� ��� 20% �� ���������. ���������� ����������.'); end;

  if bErr=False then  modalresult:=mrOk;

end;

end.
