unit HistoryTr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Placemnt;

type
  TfmHistoryTr = class(TForm)
    Memo1: TcxMemo;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmHistoryTr: TfmHistoryTr;

implementation

uses Un1, MainRnConv;

{$R *.dfm}

procedure TfmHistoryTr.FormCreate(Sender: TObject);
begin
  FormPlacement1.Active:=False;
  FormPlacement1.IniFileName := CurDir+CurIni;
  FormPlacement1.Active:=True;
  Memo1.Clear;
  With fmMainConvRn do
  begin
    Timer1.Enabled:=False;
    Timer1.Interval:=CommonSet.PeriodPing*1000;
    Timer1.Enabled:=True;
  end;
end;

procedure TfmHistoryTr.Memo1Click(Sender: TObject);
begin
  if AlphaBlend then AlphaBlend:=False
  else AlphaBlend:=True;
end;

end.
