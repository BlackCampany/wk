object dmMC: TdmMC
  OldCreateOrder = False
  Left = 907
  Top = 225
  Height = 992
  Width = 1280
  object quPassw: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT * FROM "A_RPERSONAL"'
      'where Uvolnen=0 and Modul1=1 and Id_Parent>0'
      'order by Name')
    Params = <>
    Left = 72
    Top = 16
    object quPasswID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object quPasswID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
      Required = True
    end
    object quPasswNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quPasswUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
      Required = True
    end
    object quPasswPASSW: TStringField
      FieldName = 'PASSW'
      Required = True
    end
    object quPasswMODUL1: TSmallintField
      FieldName = 'MODUL1'
      Required = True
    end
    object quPasswMODUL2: TSmallintField
      FieldName = 'MODUL2'
      Required = True
    end
    object quPasswMODUL3: TSmallintField
      FieldName = 'MODUL3'
      Required = True
    end
    object quPasswMODUL4: TSmallintField
      FieldName = 'MODUL4'
      Required = True
    end
    object quPasswMODUL5: TSmallintField
      FieldName = 'MODUL5'
      Required = True
    end
    object quPasswMODUL6: TSmallintField
      FieldName = 'MODUL6'
      Required = True
    end
    object quPasswBARCODE: TStringField
      FieldName = 'BARCODE'
      Required = True
    end
  end
  object PvSQL: TPvSqlDatabase
    AliasName = 'SCRYSTAL'
    Connected = True
    DatabaseName = 'PSQL'
    SessionName = 'PvSqlDefault'
    Left = 12
    Top = 16
  end
  object dsquPassw: TDataSource
    DataSet = quPassw
    Left = 68
    Top = 72
  end
  object quGr: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "GdsGroup"'
      'Order by Name')
    Params = <>
    Left = 136
    Top = 16
    object quGrID: TSmallintField
      FieldName = 'ID'
    end
    object quGrName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quSGr: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT GoodsGroupID,ID,Name FROM "SubGroup"'
      'where GoodsGroupID=:PARENTID'
      'Order by Name')
    Params = <
      item
        DataType = ftUnknown
        Name = 'PARENTID'
        ParamType = ptUnknown
      end>
    Left = 136
    Top = 72
    object SmallintField1: TSmallintField
      FieldName = 'ID'
    end
    object StringField1: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quCountry: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "Country"'
      'order by ID')
    Params = <>
    UpdateMode = upWhereChanged
    Left = 200
    Top = 16
    object quCountryID: TSmallintField
      FieldName = 'ID'
    end
    object quCountryName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquCountry: TDataSource
    DataSet = quCountry
    Left = 200
    Top = 64
  end
  object quCanDo: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT top 1 PREXEC FROM "A_RFUNCTION"'
      'where Id_Personal=:Person_id'
      'and Name=:Name_F')
    Params = <
      item
        DataType = ftInteger
        Name = 'Person_id'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'Name_F'
        ParamType = ptInput
        Value = ''
      end>
    Left = 268
    Top = 16
    object quCanDoPREXEC: TSmallintField
      FieldName = 'PREXEC'
    end
  end
  object quA: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 24
    Top = 128
  end
  object quC: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'Select Count(*) as RecCount from Country')
    Params = <>
    Left = 24
    Top = 184
    object quCRecCount: TAutoIncField
      FieldName = 'RecCount'
    end
  end
  object quE: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 72
    Top = 128
  end
  object quD: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    CursorType = ctDynamic
    Left = 120
    Top = 128
  end
  object quBrands: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,NAMEBRAND FROM "A_Brands"'
      'order by ID')
    Params = <>
    Left = 336
    Top = 16
    object quBrandsID: TIntegerField
      FieldName = 'ID'
    end
    object quBrandsNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
  end
  object dsquBrands: TDataSource
    DataSet = quBrands
    Left = 336
    Top = 60
  end
  object quMakers: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT * FROM "A_MAKER"')
    Params = <>
    Left = 400
    Top = 16
    object quMakersID: TAutoIncField
      FieldName = 'ID'
    end
    object quMakersNAMEM: TStringField
      FieldName = 'NAMEM'
      Size = 200
    end
    object quMakersINNM: TStringField
      FieldName = 'INNM'
      Size = 15
    end
    object quMakersKPPM: TStringField
      FieldName = 'KPPM'
      Size = 15
    end
  end
  object dsquMakers: TDataSource
    DataSet = quMakers
    Left = 400
    Top = 60
  end
  object quDeparts: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT ID,Name,Status1,Status2,ParentID,ObjectTypeID,FullName FR' +
        'OM "Depart"'
      'Where ObjectTypeID=6'
      'Order by ID')
    Params = <>
    Left = 464
    Top = 16
    object quDepartsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepartsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDepartsStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object quDepartsStatus2: TSmallintField
      FieldName = 'Status2'
    end
    object quDepartsParentID: TSmallintField
      FieldName = 'ParentID'
    end
    object quDepartsObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quDepartsFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
  end
  object dsquDeparts: TDataSource
    DataSet = quDeparts
    Left = 464
    Top = 60
  end
  object quDepParent: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID FROM "Depart"'
      'Where ObjectTypeID=5')
    Params = <>
    Left = 528
    Top = 16
    object quDepParentID: TSmallintField
      FieldName = 'ID'
    end
  end
  object quEU: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT * FROM "EUSPR"'
      'Order by Code')
    Params = <>
    Left = 592
    Top = 16
    object quEUCode: TSmallintField
      FieldName = 'Code'
    end
    object quEUName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quEUProcent1: TFloatField
      FieldName = 'Procent1'
      DisplayFormat = '0.000'
    end
    object quEUProcent2: TFloatField
      FieldName = 'Procent2'
      DisplayFormat = '0.000'
    end
    object quEUProcent3: TFloatField
      FieldName = 'Procent3'
      DisplayFormat = '0.000'
    end
    object quEUProcent4: TFloatField
      FieldName = 'Procent4'
      DisplayFormat = '0.000'
    end
  end
  object dsquEU: TDataSource
    DataSet = quEU
    Left = 592
    Top = 64
  end
  object quEUMax: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT max(Code) as Id FROM "EUSPR"'
      'where Code < 32000')
    Params = <>
    Left = 656
    Top = 16
    object quEUMaxId: TSmallintField
      FieldName = 'Id'
    end
  end
  object quCards: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quCardsCalcFields
    ParamCheck = False
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."BarCode", "Goods".' +
        '"ShRealize", "Goods"."TovarType", '
      
        '"Goods"."EdIzm", "Goods"."NDS", "Goods"."OKDP", "Goods"."Weght",' +
        ' "Goods"."SrokReal", '
      
        '"Goods"."TareWeight", "Goods"."KritOst", "Goods"."BHTStatus", "G' +
        'oods"."UKMAction", "Goods"."DataChange", '
      
        '"Goods"."NSP", "Goods"."WasteRate", "Goods"."Cena", "Goods"."Kol' +
        '", "Goods"."FullName", '
      
        '"Goods"."Country", "Goods"."Status", "Goods"."Prsision", "Goods"' +
        '."PriceState", "Goods"."SetOfGoods", '
      
        '"Goods"."PrePacking", "Goods"."MargGroup", "Goods"."ObjectTypeID' +
        '", "Goods"."FixPrice", '
      
        '"Goods"."PrintLabel", "Goods"."ImageID", "Country"."Name" as Nam' +
        'eCountry, "A_BRANDS"."NAMEBRAND" ,'
      '"Goods"."GoodsGroupId","Goods"."SubGroupId",'
      '"Goods"."V01"'
      ',"Goods"."V02"'
      ',"Goods"."V03"'
      ',"Goods"."V04"'
      ',"Goods"."V05"'
      ',"Goods"."V06"'
      ',"Goods"."V07"'
      ',"Goods"."V08"'
      ',"Goods"."V09"'
      ',"Goods"."V10"'
      ',"Goods"."V11"'
      ',"Goods"."V12"'
      ',"Goods"."V14"'
      
        ',"Goods"."Reserv1","Goods"."Reserv2","Goods"."Reserv3","Goods"."' +
        'Reserv4","Goods"."Reserv5"'
      ',"A_MAKER"."NAMEM"'
      ',"A_TYPEVOZ"."TYPEVOZ"'
      'FROM "Goods"'
      'left join "A_BRANDS" on "A_BRANDS"."ID" = "Goods"."V01"'
      'left join "Country" on "Country"."ID" = "Goods"."Country"'
      'left join "A_MAKER" on "A_MAKER"."ID" = "Goods"."V12"'
      'left join "A_TYPEVOZ" on "A_TYPEVOZ"."ID" = "Goods"."V14"'
      '')
    Params = <>
    LoadBlobOnOpen = False
    Left = 720
    Top = 16
    object quCardsID: TIntegerField
      FieldName = 'ID'
    end
    object quCardsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCardsBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quCardsShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object quCardsTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quCardsEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quCardsNDS: TFloatField
      FieldName = 'NDS'
    end
    object quCardsOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quCardsWeght: TFloatField
      FieldName = 'Weght'
    end
    object quCardsSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object quCardsTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quCardsKritOst: TFloatField
      FieldName = 'KritOst'
    end
    object quCardsBHTStatus: TSmallintField
      FieldName = 'BHTStatus'
    end
    object quCardsUKMAction: TSmallintField
      FieldName = 'UKMAction'
    end
    object quCardsDataChange: TDateField
      FieldName = 'DataChange'
    end
    object quCardsNSP: TFloatField
      FieldName = 'NSP'
    end
    object quCardsWasteRate: TFloatField
      FieldName = 'WasteRate'
    end
    object quCardsCena: TFloatField
      FieldName = 'Cena'
      DisplayFormat = '0.00'
    end
    object quCardsKol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.0'
    end
    object quCardsFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quCardsCountry: TSmallintField
      FieldName = 'Country'
    end
    object quCardsStatus: TSmallintField
      FieldName = 'Status'
    end
    object quCardsPrsision: TFloatField
      FieldName = 'Prsision'
    end
    object quCardsPriceState: TWordField
      FieldName = 'PriceState'
    end
    object quCardsSetOfGoods: TWordField
      FieldName = 'SetOfGoods'
    end
    object quCardsPrePacking: TStringField
      FieldName = 'PrePacking'
      Size = 5
    end
    object quCardsMargGroup: TStringField
      FieldName = 'MargGroup'
      Size = 2
    end
    object quCardsObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quCardsFixPrice: TFloatField
      FieldName = 'FixPrice'
      DisplayFormat = '0.00'
    end
    object quCardsPrintLabel: TWordField
      FieldName = 'PrintLabel'
    end
    object quCardsImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object quCardsNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
    object quCardsNameCountry: TStringField
      FieldName = 'NameCountry'
      Size = 30
    end
    object quCardsGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quCardsSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quCardsV01: TIntegerField
      FieldName = 'V01'
    end
    object quCardsV02: TIntegerField
      FieldName = 'V02'
    end
    object quCardsV03: TIntegerField
      FieldName = 'V03'
    end
    object quCardsV04: TIntegerField
      FieldName = 'V04'
    end
    object quCardsV05: TIntegerField
      FieldName = 'V05'
    end
    object quCardsReserv1: TFloatField
      FieldName = 'Reserv1'
      DisplayFormat = '0.000'
    end
    object quCardsReserv2: TFloatField
      FieldName = 'Reserv2'
      DisplayFormat = '0.0'
    end
    object quCardsReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object quCardsReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object quCardsReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object quCardsV06: TIntegerField
      FieldName = 'V06'
      DisplayFormat = '0'
    end
    object quCardsV07: TIntegerField
      FieldName = 'V07'
      DisplayFormat = '0'
    end
    object quCardsV08: TIntegerField
      FieldName = 'V08'
    end
    object quCardsV09: TIntegerField
      FieldName = 'V09'
    end
    object quCardsV10: TIntegerField
      FieldName = 'V10'
    end
    object quCardsVol: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Vol'
      Calculated = True
    end
    object quCardsV11: TIntegerField
      FieldName = 'V11'
    end
    object quCardsV12: TIntegerField
      FieldName = 'V12'
    end
    object quCardsNAMEM: TStringField
      FieldName = 'NAMEM'
      Size = 200
    end
    object quCardsV14: TIntegerField
      FieldName = 'V14'
    end
    object quCardsTYPEVOZ: TStringField
      FieldName = 'TYPEVOZ'
      Size = 100
    end
    object quCardsQremn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Qremn'
      LookupKeyFields = 'QRem'
      LookupResultField = 'QRem'
      EditFormat = '0.000'
      Calculated = True
    end
  end
  object dsquCards: TDataSource
    DataSet = quCards
    Left = 720
    Top = 72
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 968
    Top = 12
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 11457198
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15514252
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 11664566
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor]
      Color = 15195086
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor]
      Color = 13018773
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14996418
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clDefault
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 9788977
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 6832674
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15852504
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor]
      Color = 11562809
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clRed
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = 4887808
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14548957
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14548957
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 18432
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = 13828050
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 13882281
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15395541
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor]
      Color = 8388863
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor]
      Color = 16750591
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      TextColor = 4887808
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 13018773
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clBlue
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16765650
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 6052956
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = 6052956
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15458522
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15654399
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
  end
  object quGrFind: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT GoodsGroupID,Name FROM "SubGroup"'
      'where ID=:ID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 0
      end>
    UniDirectional = True
    LoadBlobOnOpen = False
    Left = 964
    Top = 388
    object quGrFindGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quGrFindName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quId: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SElect Id from Goods')
    Params = <>
    Left = 72
    Top = 184
    object quIdID: TIntegerField
      FieldName = 'ID'
    end
  end
  object quFindBar: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT * FROM "BarCode"'
      'where ID=:SBAR and GoodsId<>:IC ')
    Params = <
      item
        DataType = ftString
        Name = 'SBAR'
        ParamType = ptUnknown
        Value = ''
      end
      item
        DataType = ftInteger
        Name = 'IC'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 728
    Top = 276
    object quFindBarID: TStringField
      FieldName = 'ID'
      Size = 13
    end
    object quFindBarGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object quFindBarQuant: TFloatField
      FieldName = 'Quant'
    end
    object quFindBarBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object quFindBarDateChange: TDateField
      FieldName = 'DateChange'
    end
    object quFindBarBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object quFindBarCost: TFloatField
      FieldName = 'Cost'
    end
    object quFindBarStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object quFindEU: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "EUTRANS".Code,"EUTRANS".CodeTovar,"EUSPR".Name FROM "EUT' +
        'RANS"'
      'left join "EUSPR" on "EUSPR".Code="EUTRANS".Code'
      'where CodeTovar=:IDC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 728
    Top = 332
    object quFindEUCode: TSmallintField
      FieldName = 'Code'
    end
    object quFindEUCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quFindEUName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquEU1: TDataSource
    DataSet = quEU
    Left = 592
    Top = 112
  end
  object quFindC: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quFindCCalcFields
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName","Goods".' +
        '"BarCode","Goods"."TovarType",'
      
        '"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status",' +
        '"Goods"."GoodsGroupID","Goods"."SubGroupID",'
      '"SubGroup"."Name" as SubGroupName'
      'FROM "Goods"'
      'left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"'
      'where UPPER("Goods"."Name") like '#39'*%'#39)
    Params = <>
    LoadBlobOnOpen = False
    Left = 784
    Top = 276
    object quFindCID: TIntegerField
      FieldName = 'ID'
    end
    object quFindCName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindCBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindCTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindCEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindCCena: TFloatField
      FieldName = 'Cena'
      DisplayFormat = '0.00'
    end
    object quFindCStatus: TSmallintField
      FieldName = 'Status'
    end
    object quFindCSubGroupName: TStringField
      FieldName = 'SubGroupName'
      Size = 30
    end
    object quFindCGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quFindCSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quFindCNDS: TFloatField
      FieldName = 'NDS'
    end
    object quFindCFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindCGroupName: TStringField
      FieldKind = fkCalculated
      FieldName = 'GroupName'
      Size = 50
      Calculated = True
    end
  end
  object dsquFindC: TDataSource
    DataSet = quFindC
    Left = 784
    Top = 332
  end
  object quM: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select max(ID) as iMax from "TTnIn"')
    Params = <>
    Left = 24
    Top = 240
    object quMiMax: TIntegerField
      FieldName = 'iMax'
    end
  end
  object quBars: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "BarCode"'
      'where GoodsId=:GOODSID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'GOODSID'
        ParamType = ptUnknown
      end>
    LoadBlobOnOpen = False
    Left = 720
    Top = 136
    object quBarsGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object quBarsID: TStringField
      FieldName = 'ID'
      Size = 13
    end
    object quBarsQuant: TFloatField
      FieldName = 'Quant'
    end
    object quBarsBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object quBarsDateChange: TDateField
      FieldName = 'DateChange'
    end
    object quBarsBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object quBarsCost: TFloatField
      FieldName = 'Cost'
    end
    object quBarsStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object dsquBars: TDataSource
    DataSet = quBars
    Left = 720
    Top = 192
  end
  object dsquBrands1: TDataSource
    DataSet = quBrands
    Left = 336
    Top = 108
  end
  object quCateg: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CATEG"'
      'order by ID')
    Params = <>
    Left = 332
    Top = 172
    object quCategID: TIntegerField
      FieldName = 'ID'
    end
    object quCategSID: TStringField
      FieldName = 'SID'
      Size = 2
    end
    object quCategCOMMENT: TStringField
      FieldName = 'COMMENT'
    end
  end
  object dsquCateg: TDataSource
    DataSet = quCateg
    Left = 332
    Top = 220
  end
  object quTTnIn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select  '
      'ttn."Depart",'
      'ttn."DateInvoice",'
      'ttn."Number",'
      'ttn."SCFNumber",'
      'ttn."IndexPost",'
      'ttn."CodePost",'
      'ttn."SummaTovarPost",'
      'ttn."ReturnTovar",'
      'ttn."SummaTovar",'
      'ttn."Nacenka",'
      'ttn."ReturnTara",'
      'ttn."SummaTara",'
      'ttn."AcStatus",'
      'ttn."ProvodType",'
      'ttn."ChekBuh",'
      'ttn."SCFDate",'
      'ttn."OrderNumber",'
      'ttn."ID",'
      'ttn."NotNDSTovar",'
      'ttn."NDSTovar",'
      'ttn."NDS10",'
      'ttn."OutNDS10",'
      'ttn."NDS20",'
      'ttn."OutNDS20",'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'dep."Name" as NameDep,'
      'v."INN" as INNPost,'
      'i."INN" as INNInd,'
      'v.Raion,'
      'v.Status'
      ''
      ''
      'from "TTNIn" as ttn'
      'left outer join RVendor v on v.Vendor=ttn."CodePost"'
      'left outer join RIndividual i on i.Code=ttn."CodePost"'
      'left outer join Depart dep on dep.ID=ttn."Depart"'
      ''
      'where ttn.DateInvoice>=:DATEB'
      'and ttn.DateInvoice<=:DATEE'
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 24
    Top = 300
    object quTTnInDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnInDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnInNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnInSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quTTnInIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quTTnInCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quTTnInSummaTovarPost: TFloatField
      FieldName = 'SummaTovarPost'
      DisplayFormat = '0.00'
    end
    object quTTnInReturnTovar: TFloatField
      FieldName = 'ReturnTovar'
      DisplayFormat = '0.00'
    end
    object quTTnInSummaTovar: TFloatField
      FieldName = 'SummaTovar'
      DisplayFormat = '0.00'
    end
    object quTTnInNacenka: TFloatField
      FieldName = 'Nacenka'
      DisplayFormat = '0.00'
    end
    object quTTnInReturnTara: TFloatField
      FieldName = 'ReturnTara'
    end
    object quTTnInSummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quTTnInAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTnInChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quTTnInSCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quTTnInOrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object quTTnInID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnInNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quTTnInNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quTTnInNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quTTnInNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
    object quTTnInNotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
    object quTTnInNDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quTTnInOutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object quTTnInNDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quTTnInOutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object quTTnInNDSTovar: TFloatField
      FieldName = 'NDSTovar'
      DisplayFormat = '0.00'
    end
    object quTTnInINN: TStringField
      FieldKind = fkCalculated
      FieldName = 'INN'
      Calculated = True
    end
    object quTTnInINNPost: TStringField
      FieldName = 'INNPost'
    end
    object quTTnInINNInd: TStringField
      FieldName = 'INNInd'
    end
    object quTTnInProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object quTTnInRaion: TSmallintField
      FieldName = 'Raion'
    end
    object quTTnInStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object dsquTTnIn: TDataSource
    DataSet = quTTnIn
    Left = 24
    Top = 360
  end
  object dsquDeparts1: TDataSource
    DataSet = quDeparts
    Left = 464
    Top = 104
  end
  object quSpecIn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.DateInvoice,'
      'ln.Number,'
      'ln.IndexPost,'
      'ln.CodePost,'
      'ln.CodeGroup,'
      'ln.CodePodgr,'
      'ln.CodeTovar,'
      'ln.CodeEdIzm,'
      'ln.TovarType,'
      'ln.BarCode,'
      'ln.MediatorCost,'
      'ln.NDSProc,'
      'ln.NDSSum,'
      'ln.OutNDSSum,'
      'ln.BestBefore,'
      'ln.InvoiceQuant,'
      'ln.KolMest,'
      'ln.KolEdMest,'
      'ln.KolWithMest,'
      'ln.KolWithNecond,'
      'ln.Kol,'
      'ln.CenaTovar,'
      'ln.Procent,'
      'ln.NewCenaTovar,'
      'ln.SumCenaTovarPost,'
      'ln.SumCenaTovar,'
      'ln.ProcentN,'
      'ln.Necond,'
      'ln.CenaNecond,'
      'ln.SumNecond,'
      'ln.ProcentZ,'
      'ln.Zemlia,'
      'ln.ProcentO,'
      'ln.Othodi,'
      'ln.CodeTara,'
      'ln.VesTara,'
      'ln.CenaTara,'
      'ln.SumVesTara,'
      'ln.SumCenaTara,'
      'ln.ChekBuh,'
      'ln.SertBeginDate,'
      'ln.SertEndDate,'
      'ln.SertNumber,'
      'gd.Name,'
      'gd.FullName,'
      'gd.Cena,'
      'gd.V01,'
      'gd.V02,'
      'gd.V03,'
      'gd.V04,'
      'gd.V05,'
      'gd.V11,'
      'gd.V10,'
      'gd.Reserv1,'
      'gd.Reserv2,'
      'gd.Reserv3,'
      'gd.Reserv4,'
      'gd.Reserv5'
      'from "TTNInLn" ln'
      'left join Goods gd on gd.ID=Ln.CodeTovar'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 80
    Top = 300
    object quSpecInDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecInDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSpecInNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecInIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quSpecInCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quSpecInCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecInCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecInCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecInCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSpecInTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecInBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecInMediatorCost: TFloatField
      FieldName = 'MediatorCost'
    end
    object quSpecInNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSpecInNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSpecInOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quSpecInBestBefore: TDateField
      FieldName = 'BestBefore'
    end
    object quSpecInInvoiceQuant: TFloatField
      FieldName = 'InvoiceQuant'
    end
    object quSpecInKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSpecInKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSpecInKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSpecInKolWithNecond: TFloatField
      FieldName = 'KolWithNecond'
    end
    object quSpecInKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecInCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSpecInProcent: TFloatField
      FieldName = 'Procent'
    end
    object quSpecInNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quSpecInSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quSpecInSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecInProcentN: TFloatField
      FieldName = 'ProcentN'
    end
    object quSpecInNecond: TFloatField
      FieldName = 'Necond'
    end
    object quSpecInCenaNecond: TFloatField
      FieldName = 'CenaNecond'
    end
    object quSpecInSumNecond: TFloatField
      FieldName = 'SumNecond'
    end
    object quSpecInProcentZ: TFloatField
      FieldName = 'ProcentZ'
    end
    object quSpecInZemlia: TFloatField
      FieldName = 'Zemlia'
    end
    object quSpecInProcentO: TFloatField
      FieldName = 'ProcentO'
    end
    object quSpecInOthodi: TFloatField
      FieldName = 'Othodi'
    end
    object quSpecInCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecInVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSpecInCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecInSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecInSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecInChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecInSertBeginDate: TDateField
      FieldName = 'SertBeginDate'
    end
    object quSpecInSertEndDate: TDateField
      FieldName = 'SertEndDate'
    end
    object quSpecInSertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
    object quSpecInName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecInCena: TFloatField
      FieldName = 'Cena'
    end
    object quSpecInV02: TIntegerField
      FieldName = 'V02'
    end
    object quSpecInV01: TIntegerField
      FieldName = 'V01'
    end
    object quSpecInV03: TIntegerField
      FieldName = 'V03'
    end
    object quSpecInV04: TIntegerField
      FieldName = 'V04'
    end
    object quSpecInReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object quSpecInReserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object quSpecInReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object quSpecInReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object quSpecInReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object quSpecInV05: TIntegerField
      FieldName = 'V05'
    end
    object quSpecInFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quSpecInV11: TIntegerField
      FieldName = 'V11'
    end
    object quSpecInV10: TIntegerField
      FieldName = 'V10'
    end
  end
  object quCId: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."BarCode", "Goods".' +
        '"ShRealize", "Goods"."TovarType", '
      
        '"Goods"."EdIzm", "Goods"."NDS", "Goods"."OKDP", "Goods"."Weght",' +
        ' "Goods"."SrokReal", '
      
        '"Goods"."TareWeight", "Goods"."KritOst", "Goods"."BHTStatus", "G' +
        'oods"."UKMAction", "Goods"."DataChange", '
      
        '"Goods"."NSP", "Goods"."WasteRate", "Goods"."Cena", "Goods"."Kol' +
        '", "Goods"."FullName", '
      
        '"Goods"."Country", "Goods"."Status", "Goods"."Prsision", "Goods"' +
        '."PriceState", "Goods"."SetOfGoods", '
      
        '"Goods"."PrePacking", "Goods"."MargGroup", "Goods"."ObjectTypeID' +
        '", "Goods"."FixPrice", '
      '"Goods"."PrintLabel", "Goods"."ImageID", '
      '"Goods"."GoodsGroupId","Goods"."SubGroupId",'
      
        '"Goods"."V01","Goods"."V02","Goods"."V03","Goods"."V04","Goods".' +
        '"V14"'
      'FROM "Goods"'
      'where "Goods"."ID"=:IDC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 776
    Top = 16
    object quCIdID: TIntegerField
      FieldName = 'ID'
    end
    object quCIdName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCIdBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quCIdShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object quCIdTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quCIdEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quCIdNDS: TFloatField
      FieldName = 'NDS'
    end
    object quCIdOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quCIdWeght: TFloatField
      FieldName = 'Weght'
    end
    object quCIdSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object quCIdTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quCIdKritOst: TFloatField
      FieldName = 'KritOst'
    end
    object quCIdBHTStatus: TSmallintField
      FieldName = 'BHTStatus'
    end
    object quCIdUKMAction: TSmallintField
      FieldName = 'UKMAction'
    end
    object quCIdDataChange: TDateField
      FieldName = 'DataChange'
    end
    object quCIdNSP: TFloatField
      FieldName = 'NSP'
    end
    object quCIdWasteRate: TFloatField
      FieldName = 'WasteRate'
    end
    object quCIdCena: TFloatField
      FieldName = 'Cena'
    end
    object quCIdKol: TFloatField
      FieldName = 'Kol'
    end
    object quCIdFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quCIdCountry: TSmallintField
      FieldName = 'Country'
    end
    object quCIdStatus: TSmallintField
      FieldName = 'Status'
    end
    object quCIdPrsision: TFloatField
      FieldName = 'Prsision'
    end
    object quCIdPriceState: TWordField
      FieldName = 'PriceState'
    end
    object quCIdSetOfGoods: TWordField
      FieldName = 'SetOfGoods'
    end
    object quCIdPrePacking: TStringField
      FieldName = 'PrePacking'
      Size = 5
    end
    object quCIdMargGroup: TStringField
      FieldName = 'MargGroup'
      Size = 2
    end
    object quCIdObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quCIdFixPrice: TFloatField
      FieldName = 'FixPrice'
    end
    object quCIdPrintLabel: TWordField
      FieldName = 'PrintLabel'
    end
    object quCIdImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object quCIdGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quCIdSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quCIdV01: TIntegerField
      FieldName = 'V01'
    end
    object quCIdV02: TIntegerField
      FieldName = 'V02'
    end
    object quCIdV03: TIntegerField
      FieldName = 'V03'
    end
    object quCIdV04: TIntegerField
      FieldName = 'V04'
    end
    object quCIdV14: TIntegerField
      FieldName = 'V14'
    end
  end
  object quCli: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RVendor" v'
      '  left join A_CLIVOZ cv on v.Status=cv.IDCB '
      '  left join A_TypeVoz t on cv.IdTypeVoz = t.ID '
      'order by Name asc')
    Params = <>
    LoadBlobOnOpen = False
    Left = 848
    Top = 68
    object quCliVendor: TSmallintField
      FieldName = 'Vendor'
      Required = True
    end
    object quCliName: TStringField
      FieldName = 'Name'
      Required = True
      Size = 30
    end
    object quCliPayDays: TSmallintField
      FieldName = 'PayDays'
      Required = True
    end
    object quCliPayForm: TWordField
      FieldName = 'PayForm'
      Required = True
    end
    object quCliPayWaste: TWordField
      FieldName = 'PayWaste'
      Required = True
    end
    object quCliFirmName: TStringField
      FieldName = 'FirmName'
      Required = True
      Size = 6
    end
    object quCliPhone1: TStringField
      FieldName = 'Phone1'
      Required = True
      Size = 16
    end
    object quCliPhone2: TStringField
      FieldName = 'Phone2'
      Required = True
      Size = 16
    end
    object quCliPhone3: TStringField
      FieldName = 'Phone3'
      Required = True
      Size = 16
    end
    object quCliPhone4: TStringField
      FieldName = 'Phone4'
      Required = True
      Size = 16
    end
    object quCliFax: TStringField
      FieldName = 'Fax'
      Required = True
      Size = 16
    end
    object quCliTreatyDate: TDateField
      FieldName = 'TreatyDate'
      Required = True
    end
    object quCliTreatyNumber: TStringField
      FieldName = 'TreatyNumber'
      Required = True
      Size = 16
    end
    object quCliPostIndex: TStringField
      FieldName = 'PostIndex'
      Required = True
      Size = 6
    end
    object quCliGorod: TStringField
      FieldName = 'Gorod'
      Required = True
      Size = 30
    end
    object quCliRaion: TSmallintField
      FieldName = 'Raion'
      Required = True
    end
    object quCliStreet: TStringField
      FieldName = 'Street'
      Required = True
      Size = 30
    end
    object quCliHouse: TStringField
      FieldName = 'House'
      Required = True
      Size = 6
    end
    object quCliBuild: TStringField
      FieldName = 'Build'
      Required = True
      Size = 3
    end
    object quCliKvart: TStringField
      FieldName = 'Kvart'
      Required = True
      Size = 4
    end
    object quCliOKPO: TStringField
      FieldName = 'OKPO'
      Required = True
      Size = 10
    end
    object quCliOKONH: TStringField
      FieldName = 'OKONH'
      Required = True
      Size = 50
    end
    object quCliINN: TStringField
      FieldName = 'INN'
      Required = True
    end
    object quCliFullName: TStringField
      FieldName = 'FullName'
      Required = True
      Size = 100
    end
    object quCliCodeUchet: TStringField
      FieldName = 'CodeUchet'
      Required = True
    end
    object quCliUnTaxedNDS: TWordField
      FieldName = 'UnTaxedNDS'
      Required = True
    end
    object quCliStatusByte: TWordField
      FieldName = 'StatusByte'
      Required = True
    end
    object quCliStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object quCliMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object quCliPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object quCliEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
    object quCliSTBLOCK: TSmallintField
      FieldName = 'STBLOCK'
    end
    object quCliTYPEVOZ: TStringField
      FieldName = 'TYPEVOZ'
      Size = 100
    end
  end
  object dsquCli: TDataSource
    DataSet = quCli
    Left = 848
    Top = 116
  end
  object quIP: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RIndividual"'
      'order by Code')
    Params = <>
    LoadBlobOnOpen = False
    Left = 900
    Top = 84
    object quIPCode: TSmallintField
      FieldName = 'Code'
    end
    object quIPName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quIPFirmName: TStringField
      FieldName = 'FirmName'
      Size = 10
    end
    object quIPFamilia: TStringField
      FieldName = 'Familia'
    end
    object quIPIma: TStringField
      FieldName = 'Ima'
      Size = 15
    end
    object quIPOtchestvo: TStringField
      FieldName = 'Otchestvo'
      Size = 15
    end
    object quIPVidDok: TStringField
      FieldName = 'VidDok'
      Size = 30
    end
    object quIPSerPasp: TStringField
      FieldName = 'SerPasp'
      Size = 10
    end
    object quIPNumberPasp: TIntegerField
      FieldName = 'NumberPasp'
    end
    object quIPKemPasp: TStringField
      FieldName = 'KemPasp'
      Size = 60
    end
    object quIPDatePasp: TDateField
      FieldName = 'DatePasp'
    end
    object quIPCountry: TStringField
      FieldName = 'Country'
      Size = 30
    end
    object quIPPhone1: TStringField
      FieldName = 'Phone1'
      Size = 16
    end
    object quIPPhone2: TStringField
      FieldName = 'Phone2'
      Size = 16
    end
    object quIPPhone3: TStringField
      FieldName = 'Phone3'
      Size = 16
    end
    object quIPPhone4: TStringField
      FieldName = 'Phone4'
      Size = 16
    end
    object quIPFax: TStringField
      FieldName = 'Fax'
      Size = 16
    end
    object quIPEMail: TStringField
      FieldName = 'EMail'
    end
    object quIPPostIndex: TStringField
      FieldName = 'PostIndex'
      Size = 6
    end
    object quIPGorod: TStringField
      FieldName = 'Gorod'
      Size = 30
    end
    object quIPRaion: TSmallintField
      FieldName = 'Raion'
    end
    object quIPStreet: TStringField
      FieldName = 'Street'
      Size = 30
    end
    object quIPHouse: TStringField
      FieldName = 'House'
      Size = 6
    end
    object quIPBuild: TStringField
      FieldName = 'Build'
      Size = 3
    end
    object quIPKvart: TStringField
      FieldName = 'Kvart'
      Size = 4
    end
    object quIPINN: TStringField
      FieldName = 'INN'
    end
    object quIPNStrach: TStringField
      FieldName = 'NStrach'
      Size = 14
    end
    object quIPNumberSvid: TStringField
      FieldName = 'NumberSvid'
    end
    object quIPDateSvid: TDateField
      FieldName = 'DateSvid'
    end
    object quIPFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quIPDateBorn: TDateField
      FieldName = 'DateBorn'
    end
    object quIPCodeUchet: TStringField
      FieldName = 'CodeUchet'
    end
    object quIPStatus: TIntegerField
      FieldName = 'Status'
    end
  end
  object dsquIP: TDataSource
    DataSet = quIP
    Left = 900
    Top = 140
  end
  object quFindC1: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods".' +
        '"BarCode","Goods"."TovarType",'
      
        '"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status",' +
        ' "Goods"."V02"'
      'FROM "Goods"'
      'where "Goods"."Name" like '#39'%*%'#39)
    Params = <>
    LoadBlobOnOpen = False
    Left = 784
    Top = 388
    object quFindC1ID: TIntegerField
      FieldName = 'ID'
    end
    object quFindC1Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindC1BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindC1TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindC1EdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindC1NDS: TFloatField
      FieldName = 'NDS'
    end
    object quFindC1Cena: TFloatField
      FieldName = 'Cena'
    end
    object quFindC1Status: TSmallintField
      FieldName = 'Status'
    end
    object quFindC1FullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindC1V02: TIntegerField
      FieldName = 'V02'
    end
  end
  object quFindT: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RPack"'
      'where "RPack"."Code"=10')
    Params = <>
    LoadBlobOnOpen = False
    Left = 728
    Top = 388
    object quFindTCode: TSmallintField
      FieldName = 'Code'
    end
    object quFindTName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quTara: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RPack"'
      'order by "Code"')
    Params = <>
    Left = 400
    Top = 172
    object quTaraCode: TSmallintField
      FieldName = 'Code'
    end
    object quTaraName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquTara: TDataSource
    DataSet = quTara
    Left = 400
    Top = 220
  end
  object quTTnOut: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnOutCalcFields
    SQL.Strings = (
      'select  '
      'ttn."Depart",'
      'ttn."DateInvoice",'
      'ttn."Number",'
      'ttn."SCFNumber",'
      'ttn."IndexPoluch",'
      'ttn."CodePoluch",'
      'ttn."SummaTovar",'
      'ttn."SummaTovarSpis",'
      'ttn."NacenkaTovar",'
      'ttn."NDSTovar",'
      'ttn."SummaTara",'
      'ttn."SummaTaraSpis",'
      'ttn."NacenkaTara",'
      'ttn."AcStatus",'
      'ttn."ChekBuh",'
      'ttn."SCFDate",'
      'ttn."ID",'
      'ttn."Crock",'
      'ttn."ProvodType",'
      'v."Name" as NamePost,'
      'v."Status" as IDCLICB,'
      'i."Name" as NameInd,'
      'dep."Name" as NameDep,'
      'v."INN" as INNPost,'
      'i."INN" as INNInd'
      ''
      'from "TTNOut" as ttn'
      'left outer join RVendor v on v.Vendor=ttn."CodePoluch"'
      'left outer join RIndividual i on i.Code=ttn."CodePoluch"'
      'left outer join Depart dep on dep.ID=ttn."Depart"'
      ''
      'where ttn.DateInvoice>=:DATEB'
      'and ttn.DateInvoice<=:DATEE'
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 196
    Top = 292
    object quTTnOutDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnOutDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnOutNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnOutSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quTTnOutIndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quTTnOutCodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quTTnOutSummaTovar: TFloatField
      FieldName = 'SummaTovar'
      DisplayFormat = '0.00'
    end
    object quTTnOutSummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
      DisplayFormat = '0.00'
    end
    object quTTnOutNacenkaTovar: TFloatField
      FieldName = 'NacenkaTovar'
      DisplayFormat = '0.00'
    end
    object quTTnOutSummaTara: TFloatField
      FieldName = 'SummaTara'
      DisplayFormat = '0.00'
    end
    object quTTnOutSummaTaraSpis: TFloatField
      FieldName = 'SummaTaraSpis'
      DisplayFormat = '0.00'
    end
    object quTTnOutNacenkaTara: TFloatField
      FieldName = 'NacenkaTara'
      DisplayFormat = '0.00'
    end
    object quTTnOutAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTnOutChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quTTnOutSCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quTTnOutID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnOutNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quTTnOutNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quTTnOutNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quTTnOutNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 50
      Calculated = True
    end
    object quTTnOutProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object quTTnOutNDSTovar: TFloatField
      FieldName = 'NDSTovar'
      DisplayFormat = '0.00'
    end
    object quTTnOutINNPost: TStringField
      FieldName = 'INNPost'
    end
    object quTTnOutINNInd: TStringField
      FieldName = 'INNInd'
    end
    object quTTnOutINN: TStringField
      FieldKind = fkCalculated
      FieldName = 'INN'
      Calculated = True
    end
    object quTTnOutCrock: TFloatField
      FieldName = 'Crock'
      DisplayFormat = '0.00'
    end
    object quTTnOutIDCLICB: TSmallintField
      FieldName = 'IDCLICB'
    end
  end
  object dsquTTnOut: TDataSource
    DataSet = quTTnOut
    Left = 196
    Top = 348
  end
  object quFCli: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"'
      '')
    Params = <>
    LoadBlobOnOpen = False
    Left = 844
    Top = 276
    object quFCliVendor: TSmallintField
      FieldName = 'Vendor'
    end
    object quFCliName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFCliRaion: TSmallintField
      FieldName = 'Raion'
    end
    object quFCliUnTaxedNDS: TWordField
      FieldName = 'UnTaxedNDS'
    end
  end
  object quFIP: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 3 Code,Name from "RIndividual"'
      '')
    Params = <>
    LoadBlobOnOpen = False
    Left = 896
    Top = 276
    object quFIPCode: TSmallintField
      FieldName = 'Code'
    end
    object quFIPName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quSpecOut: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.DateInvoice,'
      'ln.Number,'
      'ln.CodeGroup,'
      'ln.CodePodgr,'
      'ln.CodeTovar,'
      'ln.CodeEdIzm,'
      'ln.TovarType,'
      'ln.BarCode,'
      'ln.OKDP,'
      'ln.NDSProc,'
      'ln.NDSSum,'
      'ln.OutNDSSum,'
      'ln.Akciz,'
      'ln.KolMest,'
      'ln.KolEdMest,'
      'ln.KolWithMest,'
      'ln.Kol,'
      'ln.CenaTovar,'
      'ln.CenaTovarSpis,'
      'ln.CenaPost,'
      'ln.SumCenaTovarSpis,'
      'ln.SumCenaTovar,'
      'ln.CodeTara,'
      'ln.VesTara,'
      'ln.CenaTara,'
      'ln.CenaTaraSpis,'
      'ln.SumVesTara,'
      'ln.SumCenaTara,'
      'ln.SumCenaTaraSpis,'
      'ln.ChekBuh,'
      'gd.Name,'
      'gd.FullName'
      'from "TTNOutLn" ln'
      'left join Goods gd on gd.ID=Ln.CodeTovar'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 256
    Top = 288
    object quSpecOutDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecOutDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSpecOutNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecOutCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecOutCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecOutCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecOutCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSpecOutBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecOutTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecOutOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quSpecOutNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSpecOutNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSpecOutOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quSpecOutAkciz: TFloatField
      FieldName = 'Akciz'
    end
    object quSpecOutKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSpecOutKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSpecOutKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSpecOutKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecOutCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSpecOutCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quSpecOutCenaPost: TFloatField
      FieldName = 'CenaPost'
    end
    object quSpecOutSumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSpecOutSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecOutCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecOutVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSpecOutCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecOutCenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
    object quSpecOutSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecOutSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecOutSumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object quSpecOutChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecOutName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecOutFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
  end
  object quFindC2: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT "Goods"."GoodsGroupID", "Goods"."SubGroupID"'
      'FROM "Goods"'
      'where "Goods"."ID"=:IDC')
    Params = <
      item
        DataType = ftUnknown
        Name = 'IDC'
        ParamType = ptUnknown
      end>
    LoadBlobOnOpen = False
    Left = 780
    Top = 440
    object quFindC2GoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quFindC2SubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
  end
  object quFC: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT TOP 1 "Goods"."Cena"'
      'FROM "Goods"'
      'where "Goods"."ID"=:IDC')
    Params = <
      item
        DataType = ftString
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 728
    Top = 440
    object quFCCena: TFloatField
      FieldName = 'Cena'
    end
  end
  object quFLastPrice: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select top 1 ln.CenaTovar,ln.NewCenaTovar,ln.OutNDSSum,ln.Kol,ln' +
        '.SertNumber from "TTNInLn" ln'
      
        'left join "TTNIn" hd on (hd."Depart"=ln."Depart" and hd."DateInv' +
        'oice"=ln."DateInvoice" and hd."Number"=ln."Number")'
      
        'where ln.CodeTovar=:IDC and hd.AcStatus>0 and Ln.DateInvoice >:I' +
        'DATE'
      'order by Ln.DateInvoice DESC')
    Params = <
      item
        DataType = ftString
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    LoadBlobOnOpen = False
    Left = 844
    Top = 332
    object quFLastPriceCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quFLastPriceNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quFLastPriceOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quFLastPriceKol: TFloatField
      FieldName = 'Kol'
    end
    object quFLastPriceSertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
  end
  object frTextExport1: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 1044
    Top = 8
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 1044
    Top = 64
  end
  object frCSVExport1: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = ';'
    Left = 1048
    Top = 116
  end
  object frHTMExport1: TfrHTMExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 1048
    Top = 168
  end
  object frHTML2Export1: TfrHTML2Export
    Scale = 1.000000000000000000
    Navigator.Position = []
    Navigator.Font.Charset = DEFAULT_CHARSET
    Navigator.Font.Color = clWindowText
    Navigator.Font.Height = -11
    Navigator.Font.Name = 'MS Sans Serif'
    Navigator.Font.Style = []
    Navigator.InFrame = False
    Navigator.WideInFrame = False
    Left = 1048
    Top = 220
  end
  object frHTMLTableExport1: TfrHTMLTableExport
    Left = 1052
    Top = 272
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    HighQuality = False
    CellsAlign = False
    CellsBorders = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    CellsMerged = False
    CellsWrapWords = False
    ExportPictures = False
    PageBreaks = False
    AsText = False
    Left = 1052
    Top = 320
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 1056
    Top = 372
  end
  object frTextAdvExport1: TfrTextAdvExport
    ScaleWidth = 1.000000000000000000
    ScaleHeight = 1.000000000000000000
    Borders = True
    Pseudogrpahic = False
    PageBreaks = True
    OEMCodepage = False
    EmptyLines = True
    LeadSpaces = True
    PrintAfter = False
    PrinterDialog = True
    UseSavedProps = True
    Left = 1060
    Top = 420
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    Left = 1112
    Top = 172
  end
  object frBMPExport1: TfrBMPExport
    Left = 1104
    Top = 12
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 1104
    Top = 64
  end
  object frTIFFExport1: TfrTIFFExport
    Left = 1108
    Top = 120
  end
  object quDeparts1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name,FullName,EnglishFullName FROM "Depart"'
      'Where ID=:IDC'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 468
    Top = 172
    object quDeparts1ID: TSmallintField
      FieldName = 'ID'
    end
    object quDeparts1Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDeparts1FullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quDeparts1EnglishFullName: TStringField
      FieldName = 'EnglishFullName'
      Size = 100
    end
  end
  object quFCli1: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RVendor" '
      'where Vendor=:IDC'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 844
    Top = 384
    object quFCli1Vendor: TSmallintField
      FieldName = 'Vendor'
    end
    object quFCli1Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFCli1PayDays: TSmallintField
      FieldName = 'PayDays'
    end
    object quFCli1PayForm: TWordField
      FieldName = 'PayForm'
    end
    object quFCli1PayWaste: TWordField
      FieldName = 'PayWaste'
    end
    object quFCli1FirmName: TStringField
      FieldName = 'FirmName'
      Size = 6
    end
    object quFCli1Phone1: TStringField
      FieldName = 'Phone1'
      Size = 16
    end
    object quFCli1Phone2: TStringField
      FieldName = 'Phone2'
      Size = 16
    end
    object quFCli1Phone3: TStringField
      FieldName = 'Phone3'
      Size = 16
    end
    object quFCli1Phone4: TStringField
      FieldName = 'Phone4'
      Size = 16
    end
    object quFCli1Fax: TStringField
      FieldName = 'Fax'
      Size = 16
    end
    object quFCli1TreatyDate: TDateField
      FieldName = 'TreatyDate'
    end
    object quFCli1TreatyNumber: TStringField
      FieldName = 'TreatyNumber'
      Size = 16
    end
    object quFCli1PostIndex: TStringField
      FieldName = 'PostIndex'
      Size = 6
    end
    object quFCli1Gorod: TStringField
      FieldName = 'Gorod'
      Size = 30
    end
    object quFCli1Raion: TSmallintField
      FieldName = 'Raion'
    end
    object quFCli1Street: TStringField
      FieldName = 'Street'
      Size = 30
    end
    object quFCli1House: TStringField
      FieldName = 'House'
      Size = 6
    end
    object quFCli1Build: TStringField
      FieldName = 'Build'
      Size = 3
    end
    object quFCli1Kvart: TStringField
      FieldName = 'Kvart'
      Size = 4
    end
    object quFCli1OKPO: TStringField
      FieldName = 'OKPO'
      Size = 10
    end
    object quFCli1OKONH: TStringField
      FieldName = 'OKONH'
      Size = 50
    end
    object quFCli1INN: TStringField
      FieldName = 'INN'
    end
    object quFCli1FullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quFCli1CodeUchet: TStringField
      FieldName = 'CodeUchet'
    end
    object quFCli1UnTaxedNDS: TWordField
      FieldName = 'UnTaxedNDS'
    end
    object quFCli1StatusByte: TWordField
      FieldName = 'StatusByte'
    end
    object quFCli1Status: TSmallintField
      FieldName = 'Status'
    end
  end
  object quFIP1: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RIndividual"'
      'where Code=:IDC'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 944
    Top = 276
    object quFIP1Code: TSmallintField
      FieldName = 'Code'
    end
    object quFIP1Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFIP1FirmName: TStringField
      FieldName = 'FirmName'
      Size = 10
    end
    object quFIP1Familia: TStringField
      FieldName = 'Familia'
    end
    object quFIP1Ima: TStringField
      FieldName = 'Ima'
      Size = 15
    end
    object quFIP1Otchestvo: TStringField
      FieldName = 'Otchestvo'
      Size = 15
    end
    object quFIP1VidDok: TStringField
      FieldName = 'VidDok'
      Size = 30
    end
    object quFIP1SerPasp: TStringField
      FieldName = 'SerPasp'
      Size = 10
    end
    object quFIP1NumberPasp: TIntegerField
      FieldName = 'NumberPasp'
    end
    object quFIP1KemPasp: TStringField
      FieldName = 'KemPasp'
      Size = 60
    end
    object quFIP1DatePasp: TDateField
      FieldName = 'DatePasp'
    end
    object quFIP1Country: TStringField
      FieldName = 'Country'
      Size = 30
    end
    object quFIP1Phone1: TStringField
      FieldName = 'Phone1'
      Size = 16
    end
    object quFIP1Phone2: TStringField
      FieldName = 'Phone2'
      Size = 16
    end
    object quFIP1Phone3: TStringField
      FieldName = 'Phone3'
      Size = 16
    end
    object quFIP1Phone4: TStringField
      FieldName = 'Phone4'
      Size = 16
    end
    object quFIP1Fax: TStringField
      FieldName = 'Fax'
      Size = 16
    end
    object quFIP1EMail: TStringField
      FieldName = 'EMail'
    end
    object quFIP1PostIndex: TStringField
      FieldName = 'PostIndex'
      Size = 6
    end
    object quFIP1Gorod: TStringField
      FieldName = 'Gorod'
      Size = 30
    end
    object quFIP1Raion: TSmallintField
      FieldName = 'Raion'
    end
    object quFIP1Street: TStringField
      FieldName = 'Street'
      Size = 30
    end
    object quFIP1House: TStringField
      FieldName = 'House'
      Size = 6
    end
    object quFIP1Build: TStringField
      FieldName = 'Build'
      Size = 3
    end
    object quFIP1Kvart: TStringField
      FieldName = 'Kvart'
      Size = 4
    end
    object quFIP1INN: TStringField
      FieldName = 'INN'
    end
    object quFIP1NStrach: TStringField
      FieldName = 'NStrach'
      Size = 14
    end
    object quFIP1NumberSvid: TStringField
      FieldName = 'NumberSvid'
    end
    object quFIP1DateSvid: TDateField
      FieldName = 'DateSvid'
    end
    object quFIP1FullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quFIP1DateBorn: TDateField
      FieldName = 'DateBorn'
    end
    object quFIP1CodeUchet: TStringField
      FieldName = 'CodeUchet'
    end
    object quFIP1Status: TIntegerField
      FieldName = 'Status'
    end
  end
  object quTTNAC: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnOutCalcFields
    SQL.Strings = (
      'select '
      'ttn.Depart,'
      'ttn.DateAct,'
      'ttn.Number,'
      'ttn.PostOldSum,'
      'ttn.PostNewSum,'
      'ttn.PostRaznica,'
      'ttn.MatOldSum,'
      'ttn.MatNewSum,'
      'ttn.MatRaznica,'
      'ttn.RaznOldSum,'
      'ttn.RaznNewSum,'
      'ttn.RaznRaznica,'
      'ttn.ChekBuh,'
      'ttn.NDS10OldSum,'
      'ttn.NDS10NewSum,'
      'ttn.NDS10Raznica,'
      'ttn.NDS20OldSum,'
      'ttn.NDS20NewSum,'
      'ttn.NDS20Raznica,'
      'ttn.AcStatus,'
      'ttn.NDS0OldSum,'
      'ttn.NDS0NewSum,'
      'ttn.NDS0Raznica,'
      'ttn.NSP10OldSum,'
      'ttn.NSP10NewSum,'
      'ttn.NSP10Raznica,'
      'ttn.NSP20OldSum,'
      'ttn.NSP20NewSum,'
      'ttn.NSP20Raznica,'
      'ttn.NSP0OldSum,'
      'ttn.NSP0NewSum,'
      'ttn.NSP0Raznica,'
      'ttn.ID,'
      'ttn.LinkInvoice,'
      'ttn.StartTransfer,'
      'ttn.EndTransfer,'
      'ttn.rezerv,'
      'dep."Name" as NameDep'
      'from "TTNOvr" ttn'
      'left outer join Depart dep on dep.ID=ttn."Depart"'
      ''
      'where ttn.DateAct>=:DATEB'
      'and ttn.DateAct<=:DATEE'
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 336
    Top = 276
    object quTTNACNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quTTNACDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTNACDateAct: TDateField
      FieldName = 'DateAct'
    end
    object quTTNACNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTNACMatOldSum: TFloatField
      FieldName = 'MatOldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACMatNewSum: TFloatField
      FieldName = 'MatNewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACMatRaznica: TFloatField
      FieldName = 'MatRaznica'
      DisplayFormat = '0.00'
    end
    object quTTNACRaznOldSum: TFloatField
      FieldName = 'RaznOldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACRaznNewSum: TFloatField
      FieldName = 'RaznNewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACRaznRaznica: TFloatField
      FieldName = 'RaznRaznica'
      DisplayFormat = '0.00'
    end
    object quTTNACChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quTTNACNDS10OldSum: TFloatField
      FieldName = 'NDS10OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS10NewSum: TFloatField
      FieldName = 'NDS10NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS10Raznica: TFloatField
      FieldName = 'NDS10Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS20OldSum: TFloatField
      FieldName = 'NDS20OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS20NewSum: TFloatField
      FieldName = 'NDS20NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS20Raznica: TFloatField
      FieldName = 'NDS20Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTNACNDS0OldSum: TFloatField
      FieldName = 'NDS0OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS0NewSum: TFloatField
      FieldName = 'NDS0NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNDS0Raznica: TFloatField
      FieldName = 'NDS0Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP10OldSum: TFloatField
      FieldName = 'NSP10OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP10NewSum: TFloatField
      FieldName = 'NSP10NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP10Raznica: TFloatField
      FieldName = 'NSP10Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP20OldSum: TFloatField
      FieldName = 'NSP20OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP20NewSum: TFloatField
      FieldName = 'NSP20NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP20Raznica: TFloatField
      FieldName = 'NSP20Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP0OldSum: TFloatField
      FieldName = 'NSP0OldSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP0NewSum: TFloatField
      FieldName = 'NSP0NewSum'
      DisplayFormat = '0.00'
    end
    object quTTNACNSP0Raznica: TFloatField
      FieldName = 'NSP0Raznica'
      DisplayFormat = '0.00'
    end
    object quTTNACID: TIntegerField
      FieldName = 'ID'
    end
    object quTTNACLinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quTTNACStartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quTTNACEndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quTTNACrezerv: TWordField
      FieldName = 'rezerv'
    end
  end
  object dsquTTNAC: TDataSource
    DataSet = quTTNAC
    Left = 340
    Top = 328
  end
  object quSpecAc: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.DateAct,'
      'ln.Number,'
      'ln.GGroup,'
      'ln.Podgr,'
      'ln.Tovar,'
      'ln.EdIzm,'
      'ln.TovarType,'
      'ln.BarCode,'
      'ln.Kol,'
      'ln.PostCenaOldSum,'
      'ln.PostCenaNewSum,'
      'ln.PostCenaRaznica,'
      'ln.MatCenaOldSum,'
      'ln.MatCenaNewSum,'
      'ln.MatCenaRaznica,'
      'ln.RaznCenaOldSum,'
      'ln.RaznCenaNewSum,'
      'ln.RaznCenaRaznica,'
      'ln.PostSumOldSum,'
      'ln.PostSumNewSum,'
      'ln.PostSumRaznica,'
      'ln.MatSumOldSum,'
      'ln.MatSumNewSum,'
      'ln.MatSumRaznica,'
      'ln.RaznSumOldSum,'
      'ln.RaznSumNewSum,'
      'ln.RaznSumRaznica,'
      'gd.Name,'
      'gd.FullName'
      'from "TTNOvrLn" Ln'
      'left join Goods gd on gd.ID=Ln.Tovar'
      'where ln.Depart=:IDEP'
      'and ln.DateAct=:SDATE'
      'and ln.Number=:SNUM'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 388
    Top = 276
    object quSpecAcDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecAcDateAct: TDateField
      FieldName = 'DateAct'
    end
    object quSpecAcNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecAcGGroup: TSmallintField
      FieldName = 'GGroup'
    end
    object quSpecAcPodgr: TSmallintField
      FieldName = 'Podgr'
    end
    object quSpecAcTovar: TIntegerField
      FieldName = 'Tovar'
    end
    object quSpecAcEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quSpecAcTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecAcBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecAcKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecAcPostCenaOldSum: TFloatField
      FieldName = 'PostCenaOldSum'
    end
    object quSpecAcPostCenaNewSum: TFloatField
      FieldName = 'PostCenaNewSum'
    end
    object quSpecAcPostCenaRaznica: TFloatField
      FieldName = 'PostCenaRaznica'
    end
    object quSpecAcMatCenaOldSum: TFloatField
      FieldName = 'MatCenaOldSum'
    end
    object quSpecAcMatCenaNewSum: TFloatField
      FieldName = 'MatCenaNewSum'
    end
    object quSpecAcMatCenaRaznica: TFloatField
      FieldName = 'MatCenaRaznica'
    end
    object quSpecAcRaznCenaOldSum: TFloatField
      FieldName = 'RaznCenaOldSum'
    end
    object quSpecAcRaznCenaNewSum: TFloatField
      FieldName = 'RaznCenaNewSum'
    end
    object quSpecAcRaznCenaRaznica: TFloatField
      FieldName = 'RaznCenaRaznica'
    end
    object quSpecAcPostSumOldSum: TFloatField
      FieldName = 'PostSumOldSum'
    end
    object quSpecAcPostSumNewSum: TFloatField
      FieldName = 'PostSumNewSum'
    end
    object quSpecAcPostSumRaznica: TFloatField
      FieldName = 'PostSumRaznica'
    end
    object quSpecAcMatSumOldSum: TFloatField
      FieldName = 'MatSumOldSum'
    end
    object quSpecAcMatSumNewSum: TFloatField
      FieldName = 'MatSumNewSum'
    end
    object quSpecAcMatSumRaznica: TFloatField
      FieldName = 'MatSumRaznica'
    end
    object quSpecAcRaznSumOldSum: TFloatField
      FieldName = 'RaznSumOldSum'
    end
    object quSpecAcRaznSumNewSum: TFloatField
      FieldName = 'RaznSumNewSum'
    end
    object quSpecAcRaznSumRaznica: TFloatField
      FieldName = 'RaznSumRaznica'
    end
    object quSpecAcName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecAcFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
  end
  object quTTnVn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      ' Vn."Depart",'
      ' Vn."DateInvoice",'
      ' Vn."Number",'
      ' Vn."FlowDepart",'
      ' Vn."SummaTovarSpis",'
      ' Vn."SummaTovarMove",'
      ' Vn."SummaTovarNew",'
      ' Vn."SummaTara",'
      ' Vn."Oprihod",'
      ' Vn."AcStatusP",'
      ' Vn."ChekBuhP",'
      ' Vn."AcStatusR",'
      ' Vn."ChekBuhR",'
      ' Vn."ProvodTypeP",'
      ' Vn."ProvodTypeR",'
      ' Vn."GoodsSpisNDS10",'
      ' Vn."GoodsSpisNDS20",'
      ' Vn."GoodsMoveNDS10",'
      ' Vn."GoodsMoveNDS20",'
      ' Vn."GoodsNewNDS10",'
      ' Vn."GoodsNewNDS20",'
      ' Vn."GoodsLgtNDSSpis",'
      ' Vn."GoodsLgtNDSMove",'
      ' Vn."GoodsLgtNDSNew",'
      ' Vn."GoodsNSPSpis10",'
      ' Vn."GoodsNSPSpis20",'
      ' Vn."GoodsNSPNew10",'
      ' Vn."GoodsNSPNew20",'
      ' Vn."GoodsNSP0Spis",'
      ' Vn."GoodsNSP0New",'
      ' Vn."ID",'
      ' Vn."LinkInvoice",'
      ' Vn."StartTransfer",'
      ' Vn."EndTransfer",'
      ' Vn."Rezerv",'
      ' dep1."Name" as NameD1, '
      ' dep2."Name" as NameD2 '
      ''
      'from "TTNSelf" Vn'
      ''
      'left outer join Depart dep1 on dep1.ID=Vn."Depart"'
      'left outer join Depart dep2 on dep2.ID=Vn."FlowDepart"'
      'where Vn.DateInvoice>=:DATEB'
      'and Vn.DateInvoice<=:DATEE'
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 416
    Top = 320
    object quTTnVnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnVnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnVnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnVnFlowDepart: TSmallintField
      FieldName = 'FlowDepart'
    end
    object quTTnVnSummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
      DisplayFormat = '0.00'
    end
    object quTTnVnSummaTovarMove: TFloatField
      FieldName = 'SummaTovarMove'
      DisplayFormat = '0.00'
    end
    object quTTnVnSummaTovarNew: TFloatField
      FieldName = 'SummaTovarNew'
      DisplayFormat = '0.00'
    end
    object quTTnVnSummaTara: TFloatField
      FieldName = 'SummaTara'
      DisplayFormat = '0.00'
    end
    object quTTnVnOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
    object quTTnVnAcStatusP: TWordField
      FieldName = 'AcStatusP'
    end
    object quTTnVnChekBuhP: TWordField
      FieldName = 'ChekBuhP'
    end
    object quTTnVnAcStatusR: TWordField
      FieldName = 'AcStatusR'
    end
    object quTTnVnChekBuhR: TWordField
      FieldName = 'ChekBuhR'
    end
    object quTTnVnProvodTypeP: TWordField
      FieldName = 'ProvodTypeP'
    end
    object quTTnVnProvodTypeR: TWordField
      FieldName = 'ProvodTypeR'
    end
    object quTTnVnGoodsSpisNDS10: TFloatField
      FieldName = 'GoodsSpisNDS10'
    end
    object quTTnVnGoodsSpisNDS20: TFloatField
      FieldName = 'GoodsSpisNDS20'
    end
    object quTTnVnGoodsMoveNDS10: TFloatField
      FieldName = 'GoodsMoveNDS10'
    end
    object quTTnVnGoodsMoveNDS20: TFloatField
      FieldName = 'GoodsMoveNDS20'
    end
    object quTTnVnGoodsNewNDS10: TFloatField
      FieldName = 'GoodsNewNDS10'
    end
    object quTTnVnGoodsNewNDS20: TFloatField
      FieldName = 'GoodsNewNDS20'
    end
    object quTTnVnGoodsLgtNDSSpis: TFloatField
      FieldName = 'GoodsLgtNDSSpis'
    end
    object quTTnVnGoodsLgtNDSMove: TFloatField
      FieldName = 'GoodsLgtNDSMove'
    end
    object quTTnVnGoodsLgtNDSNew: TFloatField
      FieldName = 'GoodsLgtNDSNew'
    end
    object quTTnVnGoodsNSPSpis10: TFloatField
      FieldName = 'GoodsNSPSpis10'
    end
    object quTTnVnGoodsNSPSpis20: TFloatField
      FieldName = 'GoodsNSPSpis20'
    end
    object quTTnVnGoodsNSPNew10: TFloatField
      FieldName = 'GoodsNSPNew10'
    end
    object quTTnVnGoodsNSPNew20: TFloatField
      FieldName = 'GoodsNSPNew20'
    end
    object quTTnVnGoodsNSP0Spis: TFloatField
      FieldName = 'GoodsNSP0Spis'
    end
    object quTTnVnGoodsNSP0New: TFloatField
      FieldName = 'GoodsNSP0New'
    end
    object quTTnVnID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnVnLinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quTTnVnStartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quTTnVnEndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quTTnVnRezerv: TStringField
      FieldName = 'Rezerv'
      Size = 48
    end
    object quTTnVnNameD1: TStringField
      FieldName = 'NameD1'
      Size = 30
    end
    object quTTnVnNameD2: TStringField
      FieldName = 'NameD2'
      Size = 30
    end
  end
  object dsquTTnVn: TDataSource
    DataSet = quTTnVn
    Left = 416
    Top = 372
  end
  object quSpecVn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'Ln."Depart",'
      'Ln."DateInvoice",'
      'Ln."Number",'
      'Ln."CodeGroup",'
      'Ln."CodePodgr",'
      'Ln."CodeTovar",'
      'Ln."CodeEdIzm",'
      'Ln."TovarType",'
      'Ln."BarCode",'
      'Ln."KolMest",'
      'Ln."KolEdMest",'
      'Ln."KolWithMest",'
      'Ln."Kol",'
      'Ln."CenaTovarMove",'
      'Ln."CenaTovarSpis",'
      'Ln."Procent",'
      'Ln."NewCenaTovar",'
      'Ln."SumCenaTovarSpis",'
      'Ln."SumNewCenaTovar",'
      'Ln."CodeTara",'
      'Ln."VesTara",'
      'Ln."CenaTara",'
      'Ln."SumVesTara",'
      'Ln."SumCenaTara",'
      'Ln."ChekBuhPrihod",'
      'Ln."ChekBuhRashod",'
      'gd.Name,'
      'gd.FullName,'
      'gd.NDS'
      'from "TTNSelfLn" Ln'
      'left join Goods gd on gd.ID=Ln."CodeTovar"'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 464
    Top = 308
    object quSpecVnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecVnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSpecVnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecVnCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecVnCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecVnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecVnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSpecVnTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecVnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecVnKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSpecVnKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSpecVnKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSpecVnKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecVnCenaTovarMove: TFloatField
      FieldName = 'CenaTovarMove'
    end
    object quSpecVnCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quSpecVnProcent: TFloatField
      FieldName = 'Procent'
    end
    object quSpecVnNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quSpecVnSumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSpecVnSumNewCenaTovar: TFloatField
      FieldName = 'SumNewCenaTovar'
    end
    object quSpecVnCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecVnVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSpecVnCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecVnSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecVnSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecVnChekBuhPrihod: TBooleanField
      FieldName = 'ChekBuhPrihod'
    end
    object quSpecVnChekBuhRashod: TBooleanField
      FieldName = 'ChekBuhRashod'
    end
    object quSpecVnName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecVnFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quSpecVnNDS: TFloatField
      FieldName = 'NDS'
    end
  end
  object quSpecIn1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.CodeTovar,'
      'gd.Cena,'
      'SUM(ln.Kol) as Kol,'
      'MAX(ln.CenaTovar) as CenaTovar,'
      'MAX(ln.NewCenaTovar) as NewCenaTovar,'
      'SUM(ln.SumCenaTovarPost) as SumCenaTovarPost,'
      'SUM(ln.SumCenaTovar) as SumCenaTovar,'
      'SUM(ln.NDSSum) as NDSSum '
      'from "TTNInLn" ln'
      'left join Goods gd on gd.ID=Ln.CodeTovar'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      'and ln.CodeTovar>0'
      ''
      'group by ln.Depart, gd.Cena, ln.CodeTovar'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 80
    Top = 360
    object quSpecIn1Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecIn1CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecIn1Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecIn1CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSpecIn1NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quSpecIn1SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quSpecIn1SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecIn1Cena: TFloatField
      FieldName = 'Cena'
    end
    object quSpecIn1NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
  end
  object quBufPr: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      '   buf.IDCARD,'
      '   buf.IDDOC,'
      '   buf.TYPEDOC,'
      '   buf.OLDP,'
      '   buf.NEWP,'
      '   buf.STATUS,'
      '   buf.NUMDOC,'
      '   buf.DATEDOC,'
      '   ca.Name,'
      '   ca.Cena,'
      '   ca.Reserv1'
      ''
      'from "A_BUFPRICE" buf'
      'left join Goods ca on ca.ID=buf.IDCARD'
      ''
      'where '
      ''
      'buf.DATEDOC>= :DATEB '
      'and buf.DATEDOC<= :DATEE '
      'and buf.STATUS<=:IST'
      ''
      'order by buf.IDDOC,ca.Name')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftSmallint
        Name = 'IST'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 180
    Top = 184
    object quBufPrIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object quBufPrIDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object quBufPrTYPEDOC: TSmallintField
      FieldName = 'TYPEDOC'
    end
    object quBufPrNEWP: TFloatField
      FieldName = 'NEWP'
      DisplayFormat = '0.00'
    end
    object quBufPrSTATUS: TSmallintField
      FieldName = 'STATUS'
    end
    object quBufPrNUMDOC: TStringField
      FieldName = 'NUMDOC'
    end
    object quBufPrDATEDOC: TDateField
      FieldName = 'DATEDOC'
    end
    object quBufPrName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quBufPrCena: TFloatField
      FieldName = 'Cena'
      DisplayFormat = '0.00'
    end
    object quBufPrOLDP: TFloatField
      FieldName = 'OLDP'
      DisplayFormat = '0.00'
    end
    object quBufPrReserv1: TFloatField
      FieldName = 'Reserv1'
      DisplayFormat = '0.000'
    end
  end
  object dsquBufPr: TDataSource
    DataSet = quBufPr
    Left = 192
    Top = 240
  end
  object quCashList: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quCashListCalcFields
    SQL.Strings = (
      'select'
      'CASH.Number,'
      'CASH.Name,'
      'CASH.ShRealiz,'
      'CASH.AddGoods,'
      'CASH.Technolog,'
      'CASH.Flags,'
      'CASH.Rezerv,'
      'CASH.ShopIndex,'
      'CP.PATH'
      'from "RCashRegister" CASH'
      'left join "A_CASHPATH" CP on CP.ID=CASH.Number'
      ''
      'Order by CASH.Number')
    Params = <>
    Left = 592
    Top = 196
    object quCashListNumber: TIntegerField
      FieldName = 'Number'
    end
    object quCashListName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCashListShRealiz: TIntegerField
      FieldName = 'ShRealiz'
    end
    object quCashListAddGoods: TIntegerField
      FieldName = 'AddGoods'
    end
    object quCashListTechnolog: TIntegerField
      FieldName = 'Technolog'
    end
    object quCashListFlags: TSmallintField
      FieldName = 'Flags'
    end
    object quCashListRezerv: TFloatField
      FieldName = 'Rezerv'
    end
    object quCashListShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object quCashListPATH: TStringField
      FieldName = 'PATH'
      Size = 300
    end
    object quCashListLoad: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Load'
      Calculated = True
    end
    object quCashListTake: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Take'
      Calculated = True
    end
  end
  object dsquCashList: TDataSource
    DataSet = quCashList
    Left = 592
    Top = 252
  end
  object quCash: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quCashCalcFields
    SQL.Strings = (
      'select'
      'Number,'
      'Name,'
      'ShRealiz,'
      'AddGoods,'
      'Technolog,'
      'Flags,'
      'Rezerv,'
      'ShopIndex'
      'from "RCashRegister"'
      'Order by Number')
    Params = <>
    Left = 660
    Top = 204
    object quCashNumber: TIntegerField
      FieldName = 'Number'
    end
    object quCashName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCashShRealiz: TIntegerField
      FieldName = 'ShRealiz'
    end
    object quCashAddGoods: TIntegerField
      FieldName = 'AddGoods'
    end
    object quCashTechnolog: TIntegerField
      FieldName = 'Technolog'
    end
    object quCashFlags: TSmallintField
      FieldName = 'Flags'
    end
    object quCashRezerv: TFloatField
      FieldName = 'Rezerv'
    end
    object quCashShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object quCashLoad: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'Load'
      Calculated = True
    end
    object quCashTake: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'Take'
      Calculated = True
    end
  end
  object quFindC3: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName","Goods".' +
        '"BarCode","Goods"."TovarType",'
      
        '"Goods"."EdIzm", "Goods"."Cena", "Goods"."Status","Goods"."Goods' +
        'GroupID","Goods"."SubGroupID",'
      ' "Goods"."Prsision","Goods"."V11"'
      'FROM "Goods"'
      'where "Goods"."ID"=:IDC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 844
    Top = 436
    object quFindC3ID: TIntegerField
      FieldName = 'ID'
    end
    object quFindC3Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindC3FullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindC3BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindC3TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindC3EdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindC3Cena: TFloatField
      FieldName = 'Cena'
    end
    object quFindC3Status: TSmallintField
      FieldName = 'Status'
    end
    object quFindC3GoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quFindC3SubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quFindC3Prsision: TFloatField
      FieldName = 'Prsision'
    end
    object quFindC3V11: TIntegerField
      FieldName = 'V11'
    end
  end
  object taCen: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 148
    Top = 424
    object taCenIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taCenFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object taCenCountry: TStringField
      FieldName = 'Country'
      Size = 50
    end
    object taCenPrice1: TFloatField
      FieldName = 'Price1'
      DisplayFormat = '0.00'
    end
    object taCenPrice2: TFloatField
      FieldName = 'Price2'
      DisplayFormat = '0.00'
    end
    object taCenDiscount: TFloatField
      FieldName = 'Discount'
    end
    object taCenBarCode: TStringField
      FieldName = 'BarCode'
    end
    object taCenEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object taCenQuant: TIntegerField
      FieldName = 'Quant'
    end
    object taCenPluScale: TStringField
      FieldName = 'PluScale'
      Size = 30
    end
    object taCenReceipt: TStringField
      FieldName = 'Receipt'
      Size = 150
    end
    object taCenQuanrR: TFloatField
      FieldName = 'QuanrR'
      DisplayFormat = '0.000'
    end
    object taCenDepName: TStringField
      FieldName = 'DepName'
      Size = 30
    end
    object taCensDisc: TStringField
      FieldName = 'sDisc'
      Size = 50
    end
    object taCenScaleKey: TIntegerField
      FieldName = 'ScaleKey'
    end
    object taCensDate: TStringField
      FieldName = 'sDate'
      Size = 15
    end
    object taCenV02: TIntegerField
      FieldName = 'V02'
    end
    object taCenV12: TIntegerField
      FieldName = 'V12'
    end
    object taCenMaker: TStringField
      FieldName = 'Maker'
      Size = 100
    end
  end
  object dstaCen: TDataSource
    DataSet = taCen
    Left = 188
    Top = 424
  end
  object quFindC4: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT '
      'Gd."ID", '
      'Gd."Name", '
      'Gd."FullName",'
      'Gd."BarCode",'
      'Gd."TovarType",'
      'Gd."EdIzm", '
      'Gd."Cena", '
      'Gd."Status",'
      'Gd."GoodsGroupID",'
      'Gd."SubGroupID",'
      'Gd."Country",'
      'Gd."SrokReal",'
      'Gd."V01",'
      'Gd."V02",'
      'Gd."V03",'
      'Gd."V04",'
      'Gd."V05",'
      'Gd."V06",'
      'Gd."V07",'
      'Gd."V08",'
      'Gd."V12",'
      'Cu."Name" as NameCu,'
      'Mk."NAMEM"'
      ''
      'FROM "Goods" Gd'
      'left join "Country" Cu on Gd."Country"=Cu."ID"'
      'left join "A_MAKER" Mk on Mk.ID=Gd."V12"'
      ''
      'where Gd."ID"=:IDC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 904
    Top = 448
    object quFindC4ID: TIntegerField
      FieldName = 'ID'
    end
    object quFindC4Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindC4FullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindC4BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindC4TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindC4EdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindC4Cena: TFloatField
      FieldName = 'Cena'
    end
    object quFindC4Status: TSmallintField
      FieldName = 'Status'
    end
    object quFindC4GoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quFindC4SubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quFindC4Country: TSmallintField
      FieldName = 'Country'
    end
    object quFindC4NameCu: TStringField
      FieldName = 'NameCu'
      Size = 30
    end
    object quFindC4SrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object quFindC4V01: TIntegerField
      FieldName = 'V01'
    end
    object quFindC4V02: TIntegerField
      FieldName = 'V02'
    end
    object quFindC4V03: TIntegerField
      FieldName = 'V03'
    end
    object quFindC4V04: TIntegerField
      FieldName = 'V04'
    end
    object quFindC4V05: TIntegerField
      FieldName = 'V05'
    end
    object quFindC4V06: TIntegerField
      FieldName = 'V06'
    end
    object quFindC4V07: TIntegerField
      FieldName = 'V07'
    end
    object quFindC4V08: TIntegerField
      FieldName = 'V08'
    end
    object quFindC4V12: TIntegerField
      FieldName = 'V12'
    end
    object quFindC4NAMEM: TStringField
      FieldName = 'NAMEM'
      Size = 200
    end
  end
  object frBarCodeObject1: TfrBarCodeObject
    Left = 348
    Top = 396
  end
  object RepCenn: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = True
    Left = 16
    Top = 432
    ReportForm = {19000000}
  end
  object frtaCen: TfrDBDataSet
    DataSet = taCen
    Left = 60
    Top = 468
  end
  object quCG: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'CARD,'
      'ITEM,'
      'DEPART'
      'from "CardGoods" '
      'where '
      'ITEM=:IDCARD ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 284
    Top = 476
    object quCGCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quCGITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quCGDEPART: TSmallintField
      FieldName = 'DEPART'
    end
  end
  object quCM: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'Select top 1 gm.QUANT_REST '
      'from "CardGoodsMove" gm'
      'where '
      'gm.CARD=:IDCARD'
      'order by gm.MOVEDATE DESC , DOC_TYPE DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 320
    Top = 472
    object quCMQUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quRemn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quRemnCalcFields
    SQL.Strings = (
      'select '
      'cg.CARD,'
      'cg.ITEM,'
      'cg.DEPART,'
      'de.Name,'
      'de.Status1,'
      'sum (dsr.QRem) as QRem   '
      ' '
      ''
      'from "CardGoods" cg'
      'left join Depart de on de.ID=cg.DEPART'
      'left join "A_DOCSRET" dsr on dsr.CODE=cg.ITEM '
      
        '                                                  and dsr.Depart' +
        '=cg.DEPART'
      'where '
      'cg.ITEM=:IDCARD and (de.Status1=9 or dsr.STATUS<>'#39'2'#39') '
      '  '
      'group by cg.CARD,cg.ITEM,cg.DEPART,de.Name,de.Status1')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 24
    Top = 524
    object quRemnCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quRemnITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quRemnDEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object quRemnName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quRemnRemn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Remn'
      DisplayFormat = '0.000'
      Calculated = True
    end
    object quRemnQRem: TFloatField
      FieldName = 'QRem'
      DisplayFormat = '0.000'
    end
    object quRemnStatus1: TSmallintField
      FieldName = 'Status1'
    end
  end
  object dsquRemn: TDataSource
    DataSet = quRemn
    Left = 24
    Top = 576
  end
  object quMove: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quMoveCalcFields
    SQL.Strings = (
      'select '
      'gm.CARD,'
      'gm.MOVEDATE,'
      'gm.DOC_TYPE,'
      'gm.DOCUMENT,'
      'gm.QUANT_MOVE,'
      'gm.QUANT_REST,'
      'de.Name,'
      'cg.DEPART'
      ''
      'from "CardGoodsMove" gm'
      'left join "CardGoods" cg on cg.CARD=gm.CARD'
      'left join "Depart" de on de.ID=cg.DEPART'
      ''
      'where cg.ITEM=:IDCARD'
      'and gm.MOVEDATE>=:DATEB'
      'and gm.MOVEDATE<=:DATEE'
      'order by gm.MOVEDATE,gm.DOC_TYPE,gm.DOCUMENT '
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 76
    Top = 524
    object quMoveCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quMoveMOVEDATE: TDateField
      FieldName = 'MOVEDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object quMoveDOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object quMoveDOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object quMoveQUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
      DisplayFormat = '0.000'
    end
    object quMoveQUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
      DisplayFormat = '0.000'
    end
    object quMoveName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quMoveDEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object quMoveQIN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QIN'
      Calculated = True
    end
    object quMoveQOUT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QOUT'
      Calculated = True
    end
  end
  object dsquMove: TDataSource
    DataSet = quMove
    Left = 76
    Top = 576
  end
  object quPool: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "IdPool"'
      'where CodeObject=:TD')
    Params = <
      item
        DataType = ftSmallint
        Name = 'TD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 128
    Top = 524
    object quPoolCodeObject: TSmallintField
      FieldName = 'CodeObject'
    end
    object quPoolId: TIntegerField
      FieldName = 'Id'
    end
  end
  object quM1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "CardGoodsMove"'
      'where CARD=:IDCARD'
      'and DOCUMENT=:IDDOC'
      'and DOC_TYPE=:DT')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDDOC'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DT'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 176
    Top = 524
    object quM1CARD: TIntegerField
      FieldName = 'CARD'
    end
    object quM1MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object quM1DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object quM1DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object quM1PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quM1QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object quM1SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object quM1QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quCM1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'Select top 1 gm.QUANT_REST '
      'from "CardGoodsMove" gm'
      'where '
      'gm.CARD=:IDCARD'
      'and gm.MOVEDATE<:DATEE'
      'order by gm.MOVEDATE DESC, DOC_TYPE DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 364
    Top = 468
    object quCM1QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quCM2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'Select * from "CardGoodsMove" gm'
      'where '
      'gm.CARD=:IDCARD'
      'and gm.MOVEDATE>:DATEE'
      'order by gm.MOVEDATE,gm.DOC_TYPE,gm.DOCUMENT ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftString
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 404
    Top = 468
    object quCM2CARD: TIntegerField
      FieldName = 'CARD'
    end
    object quCM2MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object quCM2DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object quCM2DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object quCM2PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quCM2QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object quCM2SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object quCM2QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quSpecOut1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select'
      'ln.Depart,'
      'ln.CodeTovar,'
      'ln.NDSProc,'
      'ln.CodeTara,'
      'ln.ChekBuh,'
      'ln.CenaTaraSpis,'
      'SUM(ln.Kol) as Kol,'
      'SUM(ln.SumCenaTovarSpis) as SumCenaTovarSpis,'
      'SUM(ln.SumCenaTovar) as SumCenaTovar,'
      'SUM(ln.KolMest) as KolMest,'
      'SUM(ln.SumCenaTara) as SumCenaTara,'
      'SUM(ln.SumCenaTaraSpis) as SumCenaTaraSpis,'
      'SUM(ln.NDSSum) as NDSSum,'
      'SUM(ln.SumVesTara) as SumVesTara'
      ''
      'from "TTNOutLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      
        'group by ln.Depart, ln.CodeTovar, ln.NDSProc, ln.CodeTara,ln.Che' +
        'kBuh,ln.CenaTaraSpis'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 256
    Top = 336
    object quSpecOut1Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecOut1CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecOut1Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecOut1NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSpecOut1CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecOut1SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSpecOut1SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecOut1KolMest: TFloatField
      FieldName = 'KolMest'
    end
    object quSpecOut1SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecOut1SumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object quSpecOut1NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSpecOut1SumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecOut1ChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecOut1CenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
  end
  object quCashRep: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quCashRepCalcFields
    SQL.Strings = (
      'select '
      'zr.CashNumber,'
      'zr.ZNumber,'
      'zr.ZSale,'
      'zr.ZReturn,'
      'zr.ZDiscount,'
      'zr.ZDate,'
      'zr.ZTime,'
      'zr.ZCrSale,'
      'zr.ZCrReturn,'
      'zr.ZCrDiscount,'
      '(zr.ZSale-zr.ZReturn) as SaleRet,'
      '(zr.ZSale-zr.ZCrSale-zr.ZReturn+zr.ZCrReturn) as SaleN,'
      '(zr.ZCrSale-zr.ZCrReturn) as SaleBn,'
      'zr.CurMoney'
      'from "Zreport" zr'
      'where zr."ZDate">=:DATEB'
      'and zr."ZDate"<=:DATEE'
      'order by zr.CashNumber,zr.ZNumber'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 232
    Top = 524
    object quCashRepCashNumber: TIntegerField
      FieldName = 'CashNumber'
    end
    object quCashRepZNumber: TIntegerField
      FieldName = 'ZNumber'
    end
    object quCashRepZSale: TFloatField
      FieldName = 'ZSale'
      DisplayFormat = '0.00'
    end
    object quCashRepZReturn: TFloatField
      FieldName = 'ZReturn'
      DisplayFormat = '0.00'
    end
    object quCashRepZDiscount: TFloatField
      FieldName = 'ZDiscount'
      DisplayFormat = '0.00'
    end
    object quCashRepZDate: TDateField
      FieldName = 'ZDate'
    end
    object quCashRepZTime: TTimeField
      FieldName = 'ZTime'
    end
    object quCashRepZCrSale: TFloatField
      FieldName = 'ZCrSale'
      DisplayFormat = '0.00'
    end
    object quCashRepZCrReturn: TFloatField
      FieldName = 'ZCrReturn'
      DisplayFormat = '0.00'
    end
    object quCashRepZCrDiscount: TFloatField
      FieldName = 'ZCrDiscount'
      DisplayFormat = '0.00'
    end
    object quCashRepSaleRet: TFloatField
      FieldName = 'SaleRet'
      DisplayFormat = '0.00'
    end
    object quCashRepSaleBn: TFloatField
      FieldName = 'SaleBn'
      DisplayFormat = '0.00'
    end
    object quCashRepCurMoney: TFloatField
      FieldName = 'CurMoney'
      DisplayFormat = '0'
    end
    object quCashRepCheckSum: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CheckSum'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object quCashRepSaleN: TFloatField
      FieldName = 'SaleN'
      DisplayFormat = '0.00'
    end
    object quCashRepRDiscAll: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RDiscAll'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object dsquCashRep: TDataSource
    DataSet = quCashRep
    Left = 256
    Top = 580
  end
  object quCashGood: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'cg.GrCode,'
      'cg.Code,'
      'de.Name,'
      'Sum(cg.Quant) as Quant,'
      'Sum(cg.Quant*cg.Price) as RSumD,  '
      'Sum(cg.Summa) as RSum  '
      'from "cq200912" cg'
      'left join Depart de on de.ID=cg.GrCode'
      'where cg.DateOperation='#39'2009-12-09'#39
      'group by cg.GrCode, cg.Code, de.Name')
    Params = <>
    Left = 296
    Top = 524
    object quCashGoodGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quCashGoodCode: TIntegerField
      FieldName = 'Code'
    end
    object quCashGoodName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCashGoodQuant: TFloatField
      FieldName = 'Quant'
    end
    object quCashGoodRSumD: TFloatField
      FieldName = 'RSumD'
      DisplayFormat = '0.00'
    end
    object quCashGoodRSum: TFloatField
      FieldName = 'RSum'
      DisplayFormat = '0.00'
    end
  end
  object quGens: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'tc.Depart,   '
      'tc.DateReport,'
      'tc.Summa,'
      'tc.Skidka,'
      'tc.ChekBuh,'
      'de.Name'
      'from "TovCassa" tc'
      'left join Depart de on de.ID=tc.Depart'
      'where tc.DateReport>=:DATEB'
      'and tc.DateReport<=:DATEE'
      'order by tc.DateReport,de.Name'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 356
    Top = 524
    object quGensDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quGensName: TStringField
      DisplayWidth = 100
      FieldName = 'Name'
      Size = 100
    end
    object quGensDateReport: TDateField
      FieldName = 'DateReport'
    end
    object quGensSumma: TFloatField
      FieldName = 'Summa'
      DisplayFormat = '0.00'
    end
    object quGensSkidka: TFloatField
      FieldName = 'Skidka'
      DisplayFormat = '0.00'
    end
    object quGensChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
  end
  object dsquGens: TDataSource
    DataSet = quGens
    Left = 356
    Top = 576
  end
  object quM2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "CardGoodsMove"'
      'where DOCUMENT=:IDDOC'
      'and DOC_TYPE=:DT')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDDOC'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DT'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 412
    Top = 524
    object quM2CARD: TIntegerField
      FieldName = 'CARD'
    end
    object quM2MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object quM2DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object quM2DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object quM2PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quM2QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object quM2SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object quM2QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quCG1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'CARD,'
      'ITEM,'
      'DEPART'
      'from "CardGoods" '
      'where '
      'CARD=:IDCARD ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 452
    Top = 456
    object quCG1CARD: TIntegerField
      FieldName = 'CARD'
    end
    object quCG1ITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quCG1DEPART: TSmallintField
      FieldName = 'DEPART'
    end
  end
  object quReports: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      're.Otdel,'
      'de.Name,'
      'de.V03,'
      're.xDate,'
      're.OldOstatokTovar,'
      're.PrihodTovar,'
      're.SelfPrihodTovar,'
      're.PrihodTovarSumma,'
      're.RashodTovar,'
      're.SelfRashodTovar,'
      're.RealTovar,'
      're.SkidkaTovar,'
      're.SenderSumma,'
      're.SenderSkidka,'
      're.OutTovar,'
      're.RashodTovarSumma,'
      're.PereoTovarSumma,'
      're.NewOstatokTovar,'
      're.OldOstatokTara,'
      're.PrihodTara,'
      're.SelfPrihodTara,'
      're.PrihodTaraSumma,'
      're.RashodTara,'
      're.SelfRashodTara,'
      're.RezervSumma,'
      're.RezervSkidka,'
      're.OutTara,'
      're.RashodTaraSumma,'
      're.xRezerv,'
      're.NewOstatokTara,'
      're.Oprihod'
      ''
      'from "REPORTS" re'
      'Left join Depart de on de.ID=re.Otdel'
      'where re."xDate">=:DATEB'
      'and  re."xDate"<=:DATEE'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 464
    Top = 520
    object quReportsOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quReportsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quReportsxDate: TDateField
      FieldName = 'xDate'
    end
    object quReportsOldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
      DisplayFormat = '0.00'
    end
    object quReportsPrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
      DisplayFormat = '0.00'
    end
    object quReportsSelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
      DisplayFormat = '0.00'
    end
    object quReportsPrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
      DisplayFormat = '0.00'
    end
    object quReportsRashodTovar: TFloatField
      FieldName = 'RashodTovar'
      DisplayFormat = '0.00'
    end
    object quReportsSelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
      DisplayFormat = '0.00'
    end
    object quReportsRealTovar: TFloatField
      FieldName = 'RealTovar'
      DisplayFormat = '0.00'
    end
    object quReportsSkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
      DisplayFormat = '0.00'
    end
    object quReportsSenderSumma: TFloatField
      FieldName = 'SenderSumma'
      DisplayFormat = '0.00'
    end
    object quReportsSenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
      DisplayFormat = '0.00'
    end
    object quReportsOutTovar: TFloatField
      FieldName = 'OutTovar'
      DisplayFormat = '0.00'
    end
    object quReportsRashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
      DisplayFormat = '0.00'
    end
    object quReportsPereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
      DisplayFormat = '0.00'
    end
    object quReportsNewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
      DisplayFormat = '0.00'
    end
    object quReportsOldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
      DisplayFormat = '0.00'
    end
    object quReportsPrihodTara: TFloatField
      FieldName = 'PrihodTara'
      DisplayFormat = '0.00'
    end
    object quReportsSelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
      DisplayFormat = '0.00'
    end
    object quReportsPrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
      DisplayFormat = '0.00'
    end
    object quReportsRashodTara: TFloatField
      FieldName = 'RashodTara'
      DisplayFormat = '0.00'
    end
    object quReportsSelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
      DisplayFormat = '0.00'
    end
    object quReportsRezervSumma: TFloatField
      FieldName = 'RezervSumma'
      DisplayFormat = '0.00'
    end
    object quReportsRezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
      DisplayFormat = '0.00'
    end
    object quReportsOutTara: TFloatField
      FieldName = 'OutTara'
      DisplayFormat = '0.00'
    end
    object quReportsRashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
      DisplayFormat = '0.00'
    end
    object quReportsxRezerv: TFloatField
      FieldName = 'xRezerv'
      DisplayFormat = '0.00'
    end
    object quReportsNewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
      DisplayFormat = '0.00'
    end
    object quReportsOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
    object quReportsV03: TIntegerField
      FieldName = 'V03'
    end
  end
  object dsquReports: TDataSource
    DataSet = quReports
    Left = 464
    Top = 576
  end
  object quCG2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'cg.CARD,'
      'cg.ITEM,'
      'cg.DEPART,'
      'de.Name'
      'from "CardGoods"  cg'
      'left join Depart de on de.ID=cg.DEPART'
      'where '
      'cg.ITEM=:IDCARD ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 504
    Top = 456
    object quCG2CARD: TIntegerField
      FieldName = 'CARD'
    end
    object quCG2ITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quCG2DEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object quCG2Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquCG2: TDataSource
    DataSet = quCG2
    Left = 544
    Top = 456
  end
  object quRepTO: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  top 1'
      're.Otdel,'
      're.xDate,'
      're.OldOstatokTovar,'
      're.PrihodTovar,'
      're.SelfPrihodTovar,'
      're.PrihodTovarSumma,'
      're.RashodTovar,'
      're.SelfRashodTovar,'
      're.RealTovar,'
      're.SkidkaTovar,'
      're.SenderSumma,'
      're.SenderSkidka,'
      're.OutTovar,'
      're.RashodTovarSumma,'
      're.PereoTovarSumma,'
      're.NewOstatokTovar,'
      're.OldOstatokTara,'
      're.PrihodTara,'
      're.SelfPrihodTara,'
      're.PrihodTaraSumma,'
      're.RashodTara,'
      're.SelfRashodTara,'
      're.RezervSumma,'
      're.RezervSkidka,'
      're.OutTara,'
      're.RashodTaraSumma,'
      're.xRezerv,'
      're.NewOstatokTara,'
      're.Oprihod'
      ''
      'from "REPORTS" re'
      'where re."xDate"<:DATEB and re.Otdel=:IDDEP'
      'order by re."xDate" DESC'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftInteger
        Name = 'IDDEP'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 528
    Top = 520
    object quRepTOOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quRepTOxDate: TDateField
      FieldName = 'xDate'
    end
    object quRepTOOldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
    end
    object quRepTOPrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
    end
    object quRepTOSelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
    end
    object quRepTOPrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
    end
    object quRepTORashodTovar: TFloatField
      FieldName = 'RashodTovar'
    end
    object quRepTOSelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
    end
    object quRepTORealTovar: TFloatField
      FieldName = 'RealTovar'
    end
    object quRepTOSkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
    end
    object quRepTOSenderSumma: TFloatField
      FieldName = 'SenderSumma'
    end
    object quRepTOSenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
    end
    object quRepTOOutTovar: TFloatField
      FieldName = 'OutTovar'
    end
    object quRepTORashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
    end
    object quRepTOPereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
    end
    object quRepTONewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
    end
    object quRepTOOldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
    end
    object quRepTOPrihodTara: TFloatField
      FieldName = 'PrihodTara'
    end
    object quRepTOSelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
    end
    object quRepTOPrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
    end
    object quRepTORashodTara: TFloatField
      FieldName = 'RashodTara'
    end
    object quRepTOSelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
    end
    object quRepTORezervSumma: TFloatField
      FieldName = 'RezervSumma'
    end
    object quRepTORezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
    end
    object quRepTOOutTara: TFloatField
      FieldName = 'OutTara'
    end
    object quRepTORashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
    end
    object quRepTOxRezerv: TFloatField
      FieldName = 'xRezerv'
    end
    object quRepTONewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
    end
    object quRepTOOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
  end
  object quS: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select Sum("SummaTovar") as RSum from "TTnIn"')
    Params = <>
    Left = 76
    Top = 240
    object quSRSum: TFloatField
      FieldName = 'RSum'
    end
  end
  object quPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPostCalcFields
    SQL.Strings = (
      'select '
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.IndexPost,'
      'tl.CodePost,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.NDSProc,'
      'tl.NDSSum,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.Procent,'
      'tl.NewCenaTovar,'
      'tl.SumCenaTovarPost,'
      'tl.SumCenaTovar,                    '
      'tl.OutNDSSum            '
      ''
      'from "TTNInLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=tl."CodePost"'
      'left outer join RIndividual i on i.Code=tl."CodePost"'
      ' '
      ' where tl.CodeTovar=:IDCARD'
      ' and tl.DateInvoice>=:DATEB'
      ' and tl.DateInvoice<=:DATEE'
      ' order by DateInvoice')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 740
    Top = 492
    object quPostDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quPostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPostDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPostNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPostIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quPostCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPostNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPostNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPostCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPostCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPostBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPostNDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.0'
    end
    object quPostNDSSum: TFloatField
      FieldName = 'NDSSum'
      DisplayFormat = '0.00'
    end
    object quPostKol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.000'
    end
    object quPostCenaTovar: TFloatField
      FieldName = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object quPostProcent: TFloatField
      FieldName = 'Procent'
      DisplayFormat = '0.0'
    end
    object quPostNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPostSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
      DisplayFormat = '0.00'
    end
    object quPostSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPostNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
    object quPostOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quPostCenaTovar0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CenaTovar0'
      Calculated = True
    end
  end
  object dsquPost: TDataSource
    DataSet = quPost
    Left = 596
    Top = 576
  end
  object quRepOB: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'cg.Card,'
      'cg.Item,'
      'gd.Name,'
      'gd.EdIzm,'
      'cg.Depart,'
      'de.Name as NameDep,'
      'gd.GoodsGroupID,'
      'gd.SubGroupID,'
      'gd.V01,'
      'gd.V02,'
      'gd.V03,'
      'gd.V04,'
      'gd.V05,'
      'gd.Cena'
      'from "CardGoods" cg'
      'left join "Goods" gd on gd.ID=cg.Item'
      'left join "Depart" de on de.ID=cg.Depart'
      'where gd.SubGroupID in (31)'
      'order by cg.Item,cg.Depart')
    Params = <>
    Left = 620
    Top = 520
    object quRepOBCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quRepOBITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quRepOBName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quRepOBEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quRepOBDEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object quRepOBNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quRepOBGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quRepOBSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quRepOBV01: TIntegerField
      FieldName = 'V01'
    end
    object quRepOBV02: TIntegerField
      FieldName = 'V02'
    end
    object quRepOBV03: TIntegerField
      FieldName = 'V03'
    end
    object quRepOBV04: TIntegerField
      FieldName = 'V04'
    end
    object quRepOBV05: TIntegerField
      FieldName = 'V05'
    end
    object quRepOBCena: TFloatField
      FieldName = 'Cena'
    end
  end
  object quMGrFind: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "GdsGroup"'
      'where ID=:ID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 0
      end>
    UniDirectional = True
    LoadBlobOnOpen = False
    Left = 964
    Top = 336
    object quMGrFindID: TSmallintField
      FieldName = 'ID'
    end
    object quMGrFindName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quPost1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost1CalcFields
    SQL.Strings = (
      'select top 3'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.IndexPost,'
      'tl.CodePost,'
      'v."Name" as NamePost,'
      'v."Inn" as InnP,'
      'i."Name" as NameInd,'
      'i."Inn" as InnI,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.Procent,'
      'tl.NewCenaTovar,'
      'tl.SumCenaTovarPost,'
      'tl.SumCenaTovar                    '
      ''
      'from "TTNInLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=tl."CodePost"'
      'left outer join RIndividual i on i.Code=tl."CodePost"'
      ' '
      'where tl.CodeTovar=:IDCARD'
      'order by DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 748
    Top = 560
    object quPost1Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost1Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPost1DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost1Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPost1IndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quPost1CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost1NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost1NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost1CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPost1CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost1BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPost1Kol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.000'
    end
    object quPost1CenaTovar: TFloatField
      FieldName = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost1Procent: TFloatField
      FieldName = 'Procent'
      DisplayFormat = '0.0'
    end
    object quPost1NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost1SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
      DisplayFormat = '0.00'
    end
    object quPost1SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost1NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 150
      Calculated = True
    end
    object quPost1InnP: TStringField
      FieldName = 'InnP'
    end
    object quPost1InnI: TStringField
      FieldName = 'InnI'
    end
    object quPost1sInn: TStringField
      FieldKind = fkCalculated
      FieldName = 'sInn'
      Calculated = True
    end
  end
  object dsquPost1: TDataSource
    DataSet = quPost1
    Left = 748
    Top = 616
  end
  object quCheckNumber: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select max(Ck_Number) as Nums  from "cq200912"'
      'where Cash_Code=:CASHNUM and NSmena=:ZNUM')
    Params = <
      item
        DataType = ftInteger
        Name = 'CASHNUM'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ZNUM'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 808
    Top = 504
    object quCheckNumberNums: TIntegerField
      FieldName = 'Nums'
    end
  end
  object quCq: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.' +
        'Ck_Card, '
      'SUM(cq.Quant*cq.Price-cq.Summa)as CorSum'
      ', sum(cq.Summa) as RSum '
      ', SUM(if(gd.NDS=10,(cq.Quant*cq.Price-cq.Summa),0))as CorSum10'
      ', sum(if(gd.NDS=10,cq.Summa,0))as RSum10'
      ', SUM(if(gd.NDS=18,(cq.Quant*cq.Price-cq.Summa),0))as CorSum18'
      ', sum(if(gd.NDS=18,cq.Summa,0))as RSum18'
      ''
      'from "cq200912" cq'
      'left join Goods gd on gd.ID=cq.Code'
      'where cq.DateOperation=:IDATE'
      
        'group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, c' +
        'q.Ck_Card'
      'order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card')
    Params = <
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 596
    Top = 456
    object quCqDateOperation: TDateField
      FieldName = 'DateOperation'
    end
    object quCqGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quCqCassir: TStringField
      FieldName = 'Cassir'
      Size = 10
    end
    object quCqCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quCqCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quCqCorSum: TFloatField
      FieldName = 'CorSum'
    end
    object quCqRSum: TFloatField
      FieldName = 'RSum'
    end
    object quCqCorSum10: TFloatField
      FieldName = 'CorSum10'
    end
    object quCqRSum10: TFloatField
      FieldName = 'RSum10'
    end
    object quCqCorSum18: TFloatField
      FieldName = 'CorSum18'
    end
    object quCqRSum18: TFloatField
      FieldName = 'RSum18'
    end
  end
  object quRepOb1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'cg.Item,'
      'gd.Name,'
      'gd.EdIzm,'
      'cg.Depart,'
      'de.Name as NameDep,'
      'gm.DOC_TYPE,'
      'gd.GoodsGroupID,'
      'gd.SubGroupID,'
      'gd.V01,'
      'gd.V02,'
      'SUM(gm.QUANT_MOVE) as Quant'
      'from "CardGoodsMove" gm'
      'left join "CardGoods" cg on cg.CARD=gm.CARD'
      'left join "Goods" gd on gd.ID=cg.Item'
      'left join "Depart" de on de.ID=cg.Depart'
      'where gm.MOVEDATE>='#39'2009-09-01'#39
      'and gm.MOVEDATE<='#39'2009-11-01'#39
      'and gd.SubGroupID in (31)'
      
        'group by cg.Item,gd.Name,gd.EdIzm,cg.Depart,de.Name,gm.DOC_TYPE,' +
        'gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02 '
      'order by cg.Item,cg.Depart')
    Params = <>
    Left = 656
    Top = 576
    object IntegerField1: TIntegerField
      FieldName = 'ITEM'
    end
    object StringField2: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object SmallintField2: TSmallintField
      FieldName = 'EdIzm'
    end
    object SmallintField3: TSmallintField
      FieldName = 'DEPART'
    end
    object StringField3: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object SmallintField4: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object SmallintField5: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object SmallintField6: TSmallintField
      FieldName = 'SubGroupID'
    end
    object FloatField1: TFloatField
      FieldName = 'Quant'
    end
    object IntegerField2: TIntegerField
      FieldName = 'V01'
    end
    object IntegerField3: TIntegerField
      FieldName = 'V02'
    end
  end
  object quTTnInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInvCalcFields
    SQL.Strings = (
      'select '
      'inv."Inventry",'
      'inv."Depart",'
      'inv."DateInventry",'
      'inv."Number",'
      'inv."CardIndex",'
      'inv."InputMode",'
      'inv."PriceMode",'
      'inv."Detail",'
      'inv."Item",'
      'inv."QuantActual",  '
      'inv."QuantRecord",                '
      'inv."SumRecord",              '
      'inv."SumActual",   '
      'inv."Status", '
      'inv."xDopStatus",'
      'dep."Name" as NameDep'
      ''
      'from "InventryHead" inv'
      'left outer join Depart dep on dep.ID=inv."Depart"'
      'where inv."DateInventry">=:DATEB'
      'and inv."DateInventry"<=:DATEE'
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 540
    Top = 320
    object quTTnInvInventry: TIntegerField
      FieldName = 'Inventry'
    end
    object quTTnInvDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnInvDateInventry: TDateField
      FieldName = 'DateInventry'
    end
    object quTTnInvNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnInvCardIndex: TWordField
      FieldName = 'CardIndex'
    end
    object quTTnInvInputMode: TWordField
      FieldName = 'InputMode'
    end
    object quTTnInvPriceMode: TWordField
      FieldName = 'PriceMode'
    end
    object quTTnInvDetail: TWordField
      FieldName = 'Detail'
    end
    object quTTnInvQuantActual: TFloatField
      FieldName = 'QuantActual'
      DisplayFormat = '0.000'
    end
    object quTTnInvQuantRecord: TFloatField
      FieldName = 'QuantRecord'
      DisplayFormat = '0.000'
    end
    object quTTnInvSumRecord: TFloatField
      FieldName = 'SumRecord'
      DisplayFormat = '0.00'
    end
    object quTTnInvSumActual: TFloatField
      FieldName = 'SumActual'
      DisplayFormat = '0.00'
    end
    object quTTnInvStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
    object quTTnInvNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quTTnInvItem: TIntegerField
      FieldName = 'Item'
    end
    object quTTnInvSumD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SumD'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object quTTnInvxDopStatus: TWordField
      FieldName = 'xDopStatus'
    end
  end
  object dsquTTnInv: TDataSource
    DataSet = quTTnInv
    Left = 540
    Top = 372
  end
  object quSpecInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'invln."Inventry",'
      'invln."Item",'
      'invln."Price",'
      'invln."QuantRecord",'
      'invln."QuantActual",'
      'invln."QuantLost",'
      'invln."Depart",'
      'invln."IndexPost",'
      'invln."CodePost",'
      'gd.Name,'
      'gd.Cena,'
      'gd.EdIzm,'
      'gd.NDS,'
      'gd.GoodsGroupID,'
      'gd.Barcode'
      'from "InventryLine" invln'
      'left join Goods gd on gd.ID=invln."Item"'
      ''
      'where invln.Depart=:IDEP'
      'and invln."Inventry"=:ID'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 600
    Top = 320
    object quSpecInvInventry: TIntegerField
      FieldName = 'Inventry'
    end
    object quSpecInvItem: TIntegerField
      FieldName = 'Item'
    end
    object quSpecInvPrice: TFloatField
      FieldName = 'Price'
    end
    object quSpecInvQuantRecord: TFloatField
      FieldName = 'QuantRecord'
    end
    object quSpecInvQuantActual: TFloatField
      FieldName = 'QuantActual'
    end
    object quSpecInvQuantLost: TFloatField
      FieldName = 'QuantLost'
    end
    object quSpecInvDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecInvIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quSpecInvCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quSpecInvName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecInvCena: TFloatField
      FieldName = 'Cena'
    end
    object quSpecInvEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quSpecInvNDS: TFloatField
      FieldName = 'NDS'
    end
    object quSpecInvGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quSpecInvBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
  end
  object quCGInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'cg.CARD,'
      'cg.ITEM,'
      'gd."Cena",'
      'gd."Name"'
      ''
      'from "CardGoods" cg'
      'left join "Goods" gd on gd."ID"=cg."ITEM"'
      ''
      'where cg.DEPART in (4)'
      'and gd."Status"<100')
    Params = <>
    Left = 660
    Top = 320
    object quCGInvCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quCGInvITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quCGInvCena: TFloatField
      FieldName = 'Cena'
    end
    object quCGInvName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quODate: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 1 "DateClose" from "A_CLOSEHIST"'
      'order by ID desc')
    Params = <>
    Left = 268
    Top = 68
    object quODateDateClose: TDateField
      FieldName = 'DateClose'
    end
  end
  object quLabs: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_LABELS"')
    Params = <>
    Left = 880
    Top = 500
    object quLabsID: TIntegerField
      FieldName = 'ID'
    end
    object quLabsNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object quLabsFILEN: TStringField
      FieldName = 'FILEN'
      Size = 100
    end
  end
  object dsquLabs: TDataSource
    DataSet = quLabs
    Left = 932
    Top = 500
  end
  object quLab1: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_LABELS"')
    Params = <>
    Left = 912
    Top = 548
    object quLab1ID: TIntegerField
      FieldName = 'ID'
    end
    object quLab1NAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object quLab1FILEN: TStringField
      FieldName = 'FILEN'
      Size = 100
    end
  end
  object quRepTO1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "REPORTS"'
      'where Otdel=:IDEP and xDate=:IDATE')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 528
    Top = 576
    object quRepTO1Otdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quRepTO1xDate: TDateField
      FieldName = 'xDate'
    end
    object quRepTO1OldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
    end
    object quRepTO1PrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
    end
    object quRepTO1SelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
    end
    object quRepTO1PrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
    end
    object quRepTO1RashodTovar: TFloatField
      FieldName = 'RashodTovar'
    end
    object quRepTO1SelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
    end
    object quRepTO1RealTovar: TFloatField
      FieldName = 'RealTovar'
    end
    object quRepTO1SkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
    end
    object quRepTO1SenderSumma: TFloatField
      FieldName = 'SenderSumma'
    end
    object quRepTO1SenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
    end
    object quRepTO1OutTovar: TFloatField
      FieldName = 'OutTovar'
    end
    object quRepTO1RashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
    end
    object quRepTO1PereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
    end
    object quRepTO1NewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
    end
    object quRepTO1OldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
    end
    object quRepTO1PrihodTara: TFloatField
      FieldName = 'PrihodTara'
    end
    object quRepTO1SelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
    end
    object quRepTO1PrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
    end
    object quRepTO1RashodTara: TFloatField
      FieldName = 'RashodTara'
    end
    object quRepTO1SelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
    end
    object quRepTO1RezervSumma: TFloatField
      FieldName = 'RezervSumma'
    end
    object quRepTO1RezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
    end
    object quRepTO1OutTara: TFloatField
      FieldName = 'OutTara'
    end
    object quRepTO1RashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
    end
    object quRepTO1xRezerv: TFloatField
      FieldName = 'xRezerv'
    end
    object quRepTO1NewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
    end
    object quRepTO1Oprihod: TBooleanField
      FieldName = 'Oprihod'
    end
  end
  object quScSpr: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "SprScale"')
    Params = <>
    Left = 992
    Top = 484
    object quScSprNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quScSprModel: TStringField
      FieldName = 'Model'
      Size = 10
    end
    object quScSprName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quScSprHubNumber: TWordField
      FieldName = 'HubNumber'
    end
    object quScSprPLUCount: TSmallintField
      FieldName = 'PLUCount'
    end
    object quScSprRezerv1: TIntegerField
      FieldName = 'Rezerv1'
    end
    object quScSprRezerv2: TIntegerField
      FieldName = 'Rezerv2'
    end
    object quScSprRezerv3: TIntegerField
      FieldName = 'Rezerv3'
    end
    object quScSprRezerv4: TIntegerField
      FieldName = 'Rezerv4'
    end
    object quScSprRezerv5: TIntegerField
      FieldName = 'Rezerv5'
    end
    object quScSprRezerv6: TIntegerField
      FieldName = 'Rezerv6'
    end
    object quScSprRezerv7: TIntegerField
      FieldName = 'Rezerv7'
    end
    object quScSprRezerv8: TIntegerField
      FieldName = 'Rezerv8'
    end
    object quScSprRezerv9: TIntegerField
      FieldName = 'Rezerv9'
    end
    object quScSprRezerv10: TIntegerField
      FieldName = 'Rezerv10'
    end
  end
  object quScale: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "SprScale"')
    Params = <>
    Left = 992
    Top = 532
    object quScaleNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quScaleModel: TStringField
      FieldName = 'Model'
      Size = 10
    end
    object quScaleName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quScaleHubNumber: TWordField
      FieldName = 'HubNumber'
    end
    object quScalePLUCount: TSmallintField
      FieldName = 'PLUCount'
    end
    object quScaleRezerv1: TIntegerField
      FieldName = 'Rezerv1'
    end
    object quScaleRezerv2: TIntegerField
      FieldName = 'Rezerv2'
    end
    object quScaleRezerv3: TIntegerField
      FieldName = 'Rezerv3'
    end
    object quScaleRezerv4: TIntegerField
      FieldName = 'Rezerv4'
    end
    object quScaleRezerv5: TIntegerField
      FieldName = 'Rezerv5'
    end
    object quScaleRezerv6: TIntegerField
      FieldName = 'Rezerv6'
    end
    object quScaleRezerv7: TIntegerField
      FieldName = 'Rezerv7'
    end
    object quScaleRezerv8: TIntegerField
      FieldName = 'Rezerv8'
    end
    object quScaleRezerv9: TIntegerField
      FieldName = 'Rezerv9'
    end
    object quScaleRezerv10: TIntegerField
      FieldName = 'Rezerv10'
    end
  end
  object dsquScale: TDataSource
    DataSet = quScale
    Left = 1044
    Top = 528
  end
  object quScaleItems: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quScaleItemsCalcFields
    SQL.Strings = (
      'select '
      'Number,'
      'Depart,'
      'PLU,'
      'GoodsItem,'
      'FirstName,'
      'LastName,'
      'Price,'
      'ShelfLife,'
      'TareWeight,'
      'GroupCode,'
      'MessageNo,'
      'Status'
      'from "ScalePLU"'
      'where Number=:SNUM'
      'order by PLU')
    Params = <
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = '4'
      end>
    Left = 992
    Top = 588
    object quScaleItemsNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quScaleItemsDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quScaleItemsPLU: TSmallintField
      FieldName = 'PLU'
    end
    object quScaleItemsGoodsItem: TIntegerField
      FieldName = 'GoodsItem'
    end
    object quScaleItemsFirstName: TStringField
      FieldName = 'FirstName'
      Size = 28
    end
    object quScaleItemsLastName: TStringField
      FieldName = 'LastName'
      Size = 28
    end
    object quScaleItemsPrice: TIntegerField
      FieldName = 'Price'
      DisplayFormat = '0.00'
    end
    object quScaleItemsShelfLife: TSmallintField
      FieldName = 'ShelfLife'
    end
    object quScaleItemsTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quScaleItemsGroupCode: TSmallintField
      FieldName = 'GroupCode'
    end
    object quScaleItemsMessageNo: TSmallintField
      FieldName = 'MessageNo'
    end
    object quScaleItemsStatus: TSmallintField
      FieldName = 'Status'
    end
    object quScaleItemsrPr: TFloatField
      FieldKind = fkCalculated
      FieldName = 'rPr'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object dsquScaleItems: TDataSource
    DataSet = quScaleItems
    Left = 1048
    Top = 576
  end
  object DigiClient: TIdTCPClient
    MaxLineAction = maException
    ReadTimeout = 200
    Port = 0
    Left = 844
    Top = 12
  end
  object quFindPlu: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select Number,Plu,Status from "ScalePLU"'
      'where GoodsItem=:IDCARD')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 904
    Top = 340
    object quFindPluNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quFindPluPLU: TSmallintField
      FieldName = 'PLU'
    end
    object quFindPluStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object quDepsTO: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "Depart"'
      'where Status1=9'
      'order by ID desc')
    Params = <>
    Left = 528
    Top = 64
    object quDepsTOID: TSmallintField
      FieldName = 'ID'
    end
    object quDepsTOName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDepsTOStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object quDepsTOStatus2: TSmallintField
      FieldName = 'Status2'
    end
    object quDepsTOStatus3: TSmallintField
      FieldName = 'Status3'
    end
    object quDepsTOStatus4: TIntegerField
      FieldName = 'Status4'
    end
    object quDepsTOStatus5: TIntegerField
      FieldName = 'Status5'
    end
    object quDepsTOParentID: TSmallintField
      FieldName = 'ParentID'
    end
    object quDepsTOObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quDepsTOClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object quDepsTOClientObjTypeID: TSmallintField
      FieldName = 'ClientObjTypeID'
    end
    object quDepsTOImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object quDepsTOFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quDepsTOEnglishFullName: TStringField
      FieldName = 'EnglishFullName'
      Size = 100
    end
    object quDepsTOPath: TStringField
      FieldName = 'Path'
      Size = 30
    end
    object quDepsTOV01: TIntegerField
      FieldName = 'V01'
    end
    object quDepsTOV02: TIntegerField
      FieldName = 'V02'
    end
    object quDepsTOV03: TIntegerField
      FieldName = 'V03'
    end
    object quDepsTOV04: TIntegerField
      FieldName = 'V04'
    end
    object quDepsTOV05: TIntegerField
      FieldName = 'V05'
    end
    object quDepsTOV06: TIntegerField
      FieldName = 'V06'
    end
    object quDepsTOV07: TIntegerField
      FieldName = 'V07'
    end
    object quDepsTOV08: TIntegerField
      FieldName = 'V08'
    end
    object quDepsTOV09: TIntegerField
      FieldName = 'V09'
    end
    object quDepsTOV10: TIntegerField
      FieldName = 'V10'
    end
    object quDepsTOV11: TIntegerField
      FieldName = 'V11'
    end
    object quDepsTOV12: TIntegerField
      FieldName = 'V12'
    end
    object quDepsTOV13: TIntegerField
      FieldName = 'V13'
    end
    object quDepsTOV14: TIntegerField
      FieldName = 'V14'
    end
  end
  object dsquScSpr: TDataSource
    DataSet = quScSpr
    Left = 1044
    Top = 484
  end
  object taToScale: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 1108
    Top = 484
    object taToScaleId: TIntegerField
      FieldName = 'Id'
    end
    object taToScaleBarcode: TStringField
      FieldName = 'Barcode'
      Size = 13
    end
    object taToScaleName: TStringField
      FieldName = 'Name'
      Size = 60
    end
    object taToScaleSrok: TIntegerField
      FieldName = 'Srok'
    end
    object taToScalePrice: TFloatField
      FieldName = 'Price'
    end
    object taToScaleNumF: TSmallintField
      FieldName = 'NumF'
    end
  end
  object quFPlu: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'PLU'
      'from "ScalePLU"'
      'where Number=:SNUM'
      'order by PLU')
    Params = <
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = '4'
      end>
    Left = 940
    Top = 588
    object quFPluPLU: TSmallintField
      FieldName = 'PLU'
    end
  end
  object quDepPars: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_DEPARTS"'
      'Where ID=:ID'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 528
    Top = 120
    object quDepParsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepParsNAMEDEP: TStringField
      FieldName = 'NAMEDEP'
      Size = 100
    end
    object quDepParsNAMEOTPR: TStringField
      FieldName = 'NAMEOTPR'
      Size = 100
    end
    object quDepParsADRDEP: TStringField
      FieldName = 'ADRDEP'
      Size = 300
    end
    object quDepParsADROTPR: TStringField
      FieldName = 'ADROTPR'
      Size = 300
    end
    object quDepParsLICO: TStringField
      FieldName = 'LICO'
      Size = 200
    end
    object quDepParsOGRN: TStringField
      FieldName = 'OGRN'
      Size = 300
    end
    object quDepParsINN: TStringField
      FieldName = 'INN'
    end
    object quDepParsKPP: TStringField
      FieldName = 'KPP'
    end
    object quDepParsFL: TStringField
      FieldName = 'FL'
      Size = 100
    end
    object quDepParsRSch: TStringField
      FieldName = 'RSch'
    end
    object quDepParsKSCh: TStringField
      FieldName = 'KSCh'
    end
    object quDepParsBank: TStringField
      FieldName = 'Bank'
      Size = 150
    end
    object quDepParsBik: TStringField
      FieldName = 'Bik'
      Size = 9
    end
    object quDepParsRukovod: TStringField
      FieldName = 'Rukovod'
      Size = 100
    end
    object quDepParsGlBuh: TStringField
      FieldName = 'GlBuh'
      Size = 100
    end
    object quDepParsStreet: TStringField
      FieldName = 'Street'
      Size = 100
    end
    object quDepParsDom: TStringField
      FieldName = 'Dom'
      Size = 10
    end
    object quDepParsKorp: TStringField
      FieldName = 'Korp'
      Size = 10
    end
    object quDepParsLitera: TStringField
      FieldName = 'Litera'
      Size = 10
    end
    object quDepParsIndex: TStringField
      FieldName = 'Index'
      Size = 10
    end
    object quDepParsTel: TStringField
      FieldName = 'Tel'
      Size = 50
    end
    object quDepParssEmail: TStringField
      FieldName = 'sEmail'
      Size = 50
    end
    object quDepParsDir1: TStringField
      FieldName = 'Dir1'
      Size = 50
    end
    object quDepParsDir2: TStringField
      FieldName = 'Dir2'
      Size = 50
    end
    object quDepParsDir3: TStringField
      FieldName = 'Dir3'
      Size = 50
    end
    object quDepParsGb1: TStringField
      FieldName = 'Gb1'
      Size = 50
    end
    object quDepParsGb2: TStringField
      FieldName = 'Gb2'
      Size = 50
    end
    object quDepParsGb3: TStringField
      FieldName = 'Gb3'
      Size = 50
    end
    object quDepParsLicVid: TStringField
      FieldName = 'LicVid'
      Size = 200
    end
    object quDepParsLicSer: TStringField
      FieldName = 'LicSer'
      Size = 50
    end
    object quDepParsLicNum: TStringField
      FieldName = 'LicNum'
      Size = 50
    end
    object quDepParsLicDB: TDateField
      FieldName = 'LicDB'
    end
    object quDepParsLicDE: TDateField
      FieldName = 'LicDE'
    end
    object quDepParsGLN: TStringField
      FieldName = 'GLN'
      Size = 15
    end
  end
  object quCds: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      'select ID, Reserv1 '
      'from Goods '
      'where Status<100')
    Params = <>
    LoadBlobOnOpen = False
    Left = 780
    Top = 68
    object quCdsID: TIntegerField
      FieldName = 'ID'
    end
    object quCdsReserv1: TFloatField
      FieldName = 'Reserv1'
    end
  end
  object quCDS1: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      'select cn.Code as ID, cd.Reserv1 '
      'from "cn201002" cn'
      'left join Goods cd on cd.ID=cn.Code'
      'where cn.RDate='#39'2010-02-04'#39)
    Params = <>
    LoadBlobOnOpen = False
    Left = 784
    Top = 124
    object IntegerField4: TIntegerField
      FieldName = 'ID'
    end
    object FloatField2: TFloatField
      FieldName = 'Reserv1'
    end
  end
  object quPriorD: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      'select ID,V01,V02 from "Depart"'
      'where V01>0 order by V01')
    Params = <>
    LoadBlobOnOpen = False
    Left = 784
    Top = 180
    object quPriorDID: TSmallintField
      FieldName = 'ID'
    end
    object quPriorDV01: TIntegerField
      FieldName = 'V01'
    end
    object quPriorDV02: TIntegerField
      FieldName = 'V02'
    end
  end
  object quPost2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost2CalcFields
    SQL.Strings = (
      'select top 3'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.IndexPost,'
      'tl.CodePost,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.Procent,'
      'tl.NewCenaTovar,'
      'tl.SumCenaTovarPost,'
      'tl.SumCenaTovar                    '
      ''
      'from "TTNInLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=tl."CodePost"'
      'left outer join RIndividual i on i.Code=tl."CodePost"'
      ' '
      'where tl.CodeTovar=:IDCARD'
      'order by DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 800
    Top = 560
    object quPost2Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost2Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPost2DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost2Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPost2IndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quPost2CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost2NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost2NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost2CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPost2CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost2BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPost2Kol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.000'
    end
    object quPost2CenaTovar: TFloatField
      FieldName = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost2Procent: TFloatField
      FieldName = 'Procent'
      DisplayFormat = '0.0'
    end
    object quPost2NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost2SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
      DisplayFormat = '0.00'
    end
    object quPost2SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost2NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
  end
  object dsquPost2: TDataSource
    DataSet = quPost2
    Left = 800
    Top = 612
  end
  object quSaleDep: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'Select SUM(Summa)as rSum ,SUM(SummaCor)as rSumD from "SaleByDepa' +
        'rt"'
      'where DateSale='#39'2010-02-01'#39' and Otdel=5')
    Params = <>
    Left = 532
    Top = 196
    object quSaleDeprSum: TFloatField
      FieldName = 'rSum'
    end
    object quSaleDeprSumD: TFloatField
      FieldName = 'rSumD'
    end
  end
  object quSpecVn1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.CodeTovar,'
      'Ln.CodeTara,'
      'SUM(ln.Kol) as Kol,'
      'MAX(Ln."CenaTovarSpis") as NewCena,'
      'SUM(Ln."SumCenaTovarSpis") as SumCenaTovarSpis,'
      'SUM(Ln."CenaTovarMove"*ln.Kol) as SumCenaTovarMove,'
      'SUM(Ln."SumCenaTara") as SumCenaTara'
      ''
      'from "TTNSelfLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      'group by ln.Depart, ln.CodeTovar,Ln.CodeTara'
      ''
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 464
    Top = 360
    object quSpecVn1Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecVn1CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecVn1Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecVn1NewCena: TFloatField
      FieldName = 'NewCena'
    end
    object quSpecVn1CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecVn1SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSpecVn1SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecVn1SumCenaTovarMove: TFloatField
      FieldName = 'SumCenaTovarMove'
    end
  end
  object quSc: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select Number from "ScalePLU"'
      'where GoodsItem in (3505,4073)'
      'group by Number'
      'Order by Number')
    Params = <>
    Left = 1108
    Top = 540
    object quScNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
  end
  object quScI: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "ScalePLU"'
      'where GoodsItem in (3505,4073)'
      'and NUmber ='#39'3'#39
      'Order by GoodsItem')
    Params = <>
    Left = 1108
    Top = 596
    object quScINumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quScIDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quScIPLU: TSmallintField
      FieldName = 'PLU'
    end
    object quScIGoodsItem: TIntegerField
      FieldName = 'GoodsItem'
    end
    object quScIFirstName: TStringField
      FieldName = 'FirstName'
      Size = 28
    end
    object quScILastName: TStringField
      FieldName = 'LastName'
      Size = 28
    end
    object quScIPrice: TIntegerField
      FieldName = 'Price'
    end
    object quScIShelfLife: TSmallintField
      FieldName = 'ShelfLife'
    end
    object quScITareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quScIGroupCode: TSmallintField
      FieldName = 'GroupCode'
    end
    object quScIMessageNo: TSmallintField
      FieldName = 'MessageNo'
    end
    object quScIStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object taCDep: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 228
    Top = 184
    object taCDepValT: TSmallintField
      FieldName = 'ValT'
    end
    object taCDepiDep: TIntegerField
      FieldName = 'iDep'
    end
    object taCDepsDep: TStringField
      FieldName = 'sDep'
      Size = 100
    end
    object taCDepDateSale: TDateField
      FieldName = 'DateSale'
    end
    object taCDepCashNum: TIntegerField
      FieldName = 'CashNum'
    end
    object taCDepCassir: TIntegerField
      FieldName = 'Cassir'
    end
    object taCDepRSum: TFloatField
      FieldName = 'RSum'
      DisplayFormat = '0.00'
    end
    object taCDepRSumD: TFloatField
      FieldName = 'RSumD'
      DisplayFormat = '0.00'
    end
    object taCDepRSumN10: TFloatField
      FieldName = 'RSumN10'
      DisplayFormat = '0.00'
    end
    object taCDepRSumN20: TFloatField
      FieldName = 'RSumN20'
      DisplayFormat = '0.00'
    end
    object taCDeprSumN0: TFloatField
      FieldName = 'rSumN0'
      DisplayFormat = '0.00'
    end
    object taCDepRSumD10: TFloatField
      FieldName = 'RSumD10'
      DisplayFormat = '0.00'
    end
    object taCDepRSumD20: TFloatField
      FieldName = 'RSumD20'
      DisplayFormat = '0.00'
    end
    object taCDepRSumD0: TFloatField
      FieldName = 'RSumD0'
      DisplayFormat = '0.00'
    end
  end
  object dstaCDep: TDataSource
    DataSet = taCDep
    Left = 288
    Top = 192
  end
  object quCDep: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'Select '
      'sd.DateSale,'
      'sd.CassaNumber,'
      'sd.Otdel,'
      'sd.Summa,'
      'sd.SummaCor,'
      'sd.Employee,'
      'sd.SummaNDSx10,'
      'sd.SummaNDSx20,'
      'sd.SummaNDSx0,'
      'sd.CorNDSx10,'
      'sd.CorNDSx20,'
      'sd.CorNDSx0,'
      'de.Name  '
      ''
      'from "SaleByDepart" sd'
      'left join Depart de on de.ID=sd.Otdel'
      'where DateSale>='#39'2010-02-01'#39)
    Params = <>
    Left = 240
    Top = 240
    object quCDepDateSale: TDateField
      FieldName = 'DateSale'
    end
    object quCDepCassaNumber: TIntegerField
      FieldName = 'CassaNumber'
    end
    object quCDepOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quCDepSumma: TFloatField
      FieldName = 'Summa'
    end
    object quCDepSummaCor: TFloatField
      FieldName = 'SummaCor'
    end
    object quCDepEmployee: TSmallintField
      FieldName = 'Employee'
    end
    object quCDepSummaNDSx10: TFloatField
      FieldName = 'SummaNDSx10'
    end
    object quCDepSummaNDSx20: TFloatField
      FieldName = 'SummaNDSx20'
    end
    object quCDepSummaNDSx0: TFloatField
      FieldName = 'SummaNDSx0'
    end
    object quCDepCorNDSx10: TFloatField
      FieldName = 'CorNDSx10'
    end
    object quCDepCorNDSx20: TFloatField
      FieldName = 'CorNDSx20'
    end
    object quCDepCorNDSx0: TFloatField
      FieldName = 'CorNDSx0'
    end
    object quCDepName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object quRepReal: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  '
      'cq.Code,'
      'cq.GrCode,'
      'cq.Operation,'
      'cq.Ck_Card,'
      'gd.Name,'
      'gd.EdIzm,'
      'gd.GoodsGroupID,'
      'gd.SubGroupID,'
      'gd.V01,'
      'gd.V02,'
      'de.Name as NameDep,'
      'sum(cq.Quant) as Quant,'
      'sum(cq.Ck_Disc) as Disc,'
      'sum(cq.Summa) as Summa'
      ''
      'from "cq201002" cq'
      ''
      'left join "Goods" gd on gd.ID=cq.Code'
      'left join "Depart" de on de.ID=cq.GrCode'
      ''
      'where cq.DateOperation = '#39'2010-02-15'#39' '
      'and GrCode=5'
      'and  gd.SubGroupID in (31)'
      ''
      'group by '
      'cq.Code,'
      'cq.GrCode,'
      'cq.Operation,'
      'cq.Ck_Card,'
      'gd.Name,'
      'gd.EdIzm,'
      'gd.GoodsGroupID,'
      'gd.SubGroupID,'
      'gd.V01,'
      'gd.V02,'
      'de.Name')
    Params = <>
    Left = 656
    Top = 460
    object quRepRealCode: TIntegerField
      FieldName = 'Code'
    end
    object quRepRealGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quRepRealOperation: TStringField
      FieldName = 'Operation'
      Size = 1
    end
    object quRepRealCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quRepRealName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quRepRealEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quRepRealGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quRepRealV01: TIntegerField
      FieldName = 'V01'
    end
    object quRepRealV02: TIntegerField
      FieldName = 'V02'
    end
    object quRepRealNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quRepRealQuant: TFloatField
      FieldName = 'Quant'
    end
    object quRepRealDisc: TFloatField
      FieldName = 'Disc'
    end
    object quRepRealSumma: TFloatField
      FieldName = 'Summa'
    end
  end
  object quSpecTest: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select * from "TTNInLn"'
      'where Depart in (5,13,14)'
      'and DateInvoice>='#39'2010-02-25'#39)
    Params = <>
    Left = 108
    Top = 468
    object quSpecTestDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecTestDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSpecTestNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecTestIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quSpecTestCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quSpecTestCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecTestCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecTestCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecTestCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSpecTestTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecTestBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecTestMediatorCost: TFloatField
      FieldName = 'MediatorCost'
    end
    object quSpecTestNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSpecTestNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSpecTestOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quSpecTestBestBefore: TDateField
      FieldName = 'BestBefore'
    end
    object quSpecTestInvoiceQuant: TFloatField
      FieldName = 'InvoiceQuant'
    end
    object quSpecTestKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSpecTestKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSpecTestKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSpecTestKolWithNecond: TFloatField
      FieldName = 'KolWithNecond'
    end
    object quSpecTestKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecTestCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSpecTestProcent: TFloatField
      FieldName = 'Procent'
    end
    object quSpecTestNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quSpecTestSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quSpecTestSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecTestProcentN: TFloatField
      FieldName = 'ProcentN'
    end
    object quSpecTestNecond: TFloatField
      FieldName = 'Necond'
    end
    object quSpecTestCenaNecond: TFloatField
      FieldName = 'CenaNecond'
    end
    object quSpecTestSumNecond: TFloatField
      FieldName = 'SumNecond'
    end
    object quSpecTestProcentZ: TFloatField
      FieldName = 'ProcentZ'
    end
    object quSpecTestZemlia: TFloatField
      FieldName = 'Zemlia'
    end
    object quSpecTestProcentO: TFloatField
      FieldName = 'ProcentO'
    end
    object quSpecTestOthodi: TFloatField
      FieldName = 'Othodi'
    end
    object quSpecTestCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecTestVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSpecTestCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecTestSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecTestSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecTestChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecTestSertBeginDate: TDateField
      FieldName = 'SertBeginDate'
    end
    object quSpecTestSertEndDate: TDateField
      FieldName = 'SertEndDate'
    end
    object quSpecTestSertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
  end
  object quRealTMP: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quMoveCalcFields
    SQL.Strings = (
      'Select * from "CardGoodsMove"'
      'where MOVEDATE='#39'2010-02-01'#39' and DOC_TYPE=7 ')
    Params = <>
    Left = 24
    Top = 644
    object quRealTMPCARD: TIntegerField
      FieldName = 'CARD'
    end
    object quRealTMPMOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object quRealTMPDOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object quRealTMPDOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object quRealTMPPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quRealTMPQUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object quRealTMPSUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object quRealTMPQUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object quFindC5: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName","Goods".' +
        '"BarCode","Goods"."TovarType",'
      
        '"Goods"."EdIzm", "Goods"."Cena", "Goods"."Status","Goods"."Goods' +
        'GroupID","Goods"."SubGroupID",'
      '"Goods"."Prsision","Goods"."V11"'
      'FROM "Goods"'
      'where "Goods"."Status"<=100')
    Params = <>
    LoadBlobOnOpen = False
    Left = 904
    Top = 396
    object quFindC5ID: TIntegerField
      FieldName = 'ID'
    end
    object quFindC5Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindC5FullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindC5BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindC5TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindC5EdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindC5Cena: TFloatField
      FieldName = 'Cena'
    end
    object quFindC5Status: TSmallintField
      FieldName = 'Status'
    end
    object quFindC5GoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quFindC5SubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quFindC5Prsision: TFloatField
      FieldName = 'Prsision'
    end
    object quFindC5V11: TIntegerField
      FieldName = 'V11'
    end
  end
  object quCashSum0: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select Ck_Card, Sum(Ck_Disc) as DSum,Sum(Summa) as RSum from "cq' +
        '201003"'
      
        'where DateOperation='#39'2010-03-17'#39' and Cash_Code=3 and Ck_Number<1' +
        '000'
      'group by Ck_Card')
    Params = <>
    Left = 260
    Top = 628
    object quCashSum0Ck_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quCashSum0DSum: TFloatField
      FieldName = 'DSum'
    end
    object quCashSum0RSum: TFloatField
      FieldName = 'RSum'
    end
  end
  object quSpecInTest: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln'
      
        'left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoic' +
        'e=hd.DateInvoice) and(Ln.Number=hd.Number)'
      'where ln.DateInvoice='#39'2010-02-01'#39
      'and ln.CodeTovar=65446'
      'and ln.Depart=13'
      'and hd.AcStatus=3'
      'group by hd.ID')
    Params = <>
    Left = 140
    Top = 308
    object quSpecInTestKol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecInTestID: TIntegerField
      FieldName = 'ID'
    end
  end
  object quSpecInT: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.CodeTara,'
      'SUM(ln.KolMest) as KolMest,'
      'MAX(ln.CenaTara) as CenaTara,'
      'SUM(ln.SumCenaTara) as SumCenaTara '
      'from "TTNInLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      'and ln.CodeTara>0'
      ''
      'group by ln.Depart, ln.CodeTara'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 140
    Top = 368
    object quSpecInTDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecInTCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecInTKolMest: TFloatField
      FieldName = 'KolMest'
    end
    object quSpecInTCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecInTSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
  end
  object quSpecOutT: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.CodeTara,'
      'SUM(ln.KolMest) as KolMest,'
      'MAX(ln.CenaTara) as CenaTara'
      'from "TTNOutLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      'and ln.CodeTara>0'
      ''
      'group by ln.Depart, ln.CodeTara')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 256
    Top = 384
    object quSpecOutTDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecOutTCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecOutTKolMest: TFloatField
      FieldName = 'KolMest'
    end
    object quSpecOutTCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
  end
  object quDepartsSt: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT ID,Name,Status1,Status2,Status3,Status4,Status5,ParentID,' +
        'ObjectTypeID FROM "Depart"'
      'Where ObjectTypeID=6 and Status1=9 and Status4<>0'
      'Order by ID')
    Params = <>
    Left = 468
    Top = 228
    object quDepartsStID: TSmallintField
      FieldName = 'ID'
    end
    object quDepartsStName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDepartsStStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object quDepartsStStatus2: TSmallintField
      FieldName = 'Status2'
    end
    object quDepartsStStatus3: TSmallintField
      FieldName = 'Status3'
    end
    object quDepartsStParentID: TSmallintField
      FieldName = 'ParentID'
    end
    object quDepartsStObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quDepartsStStatus4: TIntegerField
      FieldName = 'Status4'
    end
    object quDepartsStStatus5: TIntegerField
      FieldName = 'Status5'
    end
  end
  object dsquDepartsSt: TDataSource
    DataSet = quDepartsSt
    Left = 516
    Top = 252
  end
  object quCliPars: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CLIENTS"'
      'Where INN=:SINN'
      '')
    Params = <
      item
        DataType = ftString
        Name = 'SINN'
        ParamType = ptInput
        Value = ''
      end>
    Left = 960
    Top = 172
    object quCliParsINN: TStringField
      FieldName = 'INN'
    end
    object quCliParsNAMEOTP: TStringField
      FieldName = 'NAMEOTP'
      Size = 150
    end
    object quCliParsADROTPR: TStringField
      FieldName = 'ADROTPR'
      Size = 100
    end
    object quCliParsRSch: TStringField
      FieldName = 'RSch'
    end
    object quCliParsKSch: TStringField
      FieldName = 'KSch'
    end
    object quCliParsBank: TStringField
      FieldName = 'Bank'
      Size = 150
    end
    object quCliParsBik: TStringField
      FieldName = 'Bik'
      Size = 9
    end
    object quCliParsPRETCLI: TStringField
      FieldName = 'PRETCLI'
      Size = 200
    end
    object quCliParsDOGNUM: TStringField
      FieldName = 'DOGNUM'
      Size = 50
    end
    object quCliParsDOGDATE: TDateField
      FieldName = 'DOGDATE'
    end
  end
  object quPost3: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost1CalcFields
    SQL.Strings = (
      'select top 1'
      'tl.Depart'
      'from "TTNInLn" tl'
      ' '
      'where tl.CodeTovar=:IDCARD'
      'order by DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 852
    Top = 560
    object quPost3Depart: TSmallintField
      FieldName = 'Depart'
    end
  end
  object quSInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select "SumRecord" , "SumActual" , "Detail"  from "InventryHead"'
      'where "DateInventry"='#39'2010-05-06'#39
      'and "Item"=1'
      'and "Depart"=13')
    Params = <>
    Left = 128
    Top = 240
    object quSInvSumRecord: TFloatField
      FieldName = 'SumRecord'
    end
    object quSInvSumActual: TFloatField
      FieldName = 'SumActual'
    end
    object quSInvDetail: TWordField
      FieldName = 'Detail'
    end
  end
  object quSpecIn2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.DateInvoice,'
      'ln.Number,'
      'ln.IndexPost,'
      'ln.CodePost,'
      'ln.CodeGroup,'
      'ln.CodePodgr,'
      'ln.CodeTovar,'
      'ln.CodeEdIzm,'
      'ln.TovarType,'
      'ln.BarCode,'
      'ln.MediatorCost,'
      'ln.NDSProc,'
      'ln.NDSSum,'
      'ln.OutNDSSum,'
      'ln.BestBefore,'
      'ln.InvoiceQuant,'
      'ln.KolMest,'
      'ln.KolEdMest,'
      'ln.KolWithMest,'
      'ln.KolWithNecond,'
      'ln.Kol,'
      'ln.CenaTovar,'
      'ln.Procent,'
      'ln.NewCenaTovar,'
      'ln.SumCenaTovarPost,'
      'ln.SumCenaTovar,'
      'ln.CodeTara,'
      'ln.VesTara,'
      'ln.CenaTara,'
      'ln.SumVesTara,'
      'ln.SumCenaTara,'
      'ln.ChekBuh,'
      'gd.Name,'
      'gd.Cena,'
      'gd.V01,'
      'gd.V02,'
      'gd.V03,'
      'gd.V04,'
      'gd.V05,'
      'gd.Reserv1,'
      'gd.Reserv2,'
      'gd.Reserv3,'
      'gd.Reserv4,'
      'gd.Reserv5'
      'from "TTNInLn" ln'
      'left join Goods gd on gd.ID=Ln.CodeTovar'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 80
    Top = 416
    object quSpecIn2Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecIn2DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSpecIn2Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSpecIn2IndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quSpecIn2CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quSpecIn2CodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecIn2CodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecIn2CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecIn2CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSpecIn2TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSpecIn2BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSpecIn2MediatorCost: TFloatField
      FieldName = 'MediatorCost'
    end
    object quSpecIn2NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSpecIn2NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSpecIn2OutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quSpecIn2BestBefore: TDateField
      FieldName = 'BestBefore'
    end
    object quSpecIn2InvoiceQuant: TFloatField
      FieldName = 'InvoiceQuant'
    end
    object quSpecIn2KolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSpecIn2KolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSpecIn2KolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSpecIn2KolWithNecond: TFloatField
      FieldName = 'KolWithNecond'
    end
    object quSpecIn2Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecIn2CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSpecIn2Procent: TFloatField
      FieldName = 'Procent'
    end
    object quSpecIn2NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quSpecIn2SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quSpecIn2SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSpecIn2CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecIn2VesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSpecIn2CenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSpecIn2SumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSpecIn2SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecIn2ChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecIn2Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSpecIn2Cena: TFloatField
      FieldName = 'Cena'
    end
    object quSpecIn2V02: TIntegerField
      FieldName = 'V02'
    end
    object quSpecIn2Reserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object quSpecIn2V01: TIntegerField
      FieldName = 'V01'
    end
    object quSpecIn2V03: TIntegerField
      FieldName = 'V03'
    end
    object quSpecIn2V04: TIntegerField
      FieldName = 'V04'
    end
    object quSpecIn2Reserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object quSpecIn2Reserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object quSpecIn2Reserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object quSpecIn2Reserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object quSpecIn2V05: TIntegerField
      FieldName = 'V05'
    end
  end
  object quRepR: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      'select'
      'gd.GoodsGroupID,'
      'gd.ID,'
      'gd.Name,'
      'gd.BarCode,'
      'gd.TovarType,'
      'gd.EdIzm,'
      'gd.Cena,'
      'gd.FullName,'
      'gd.Status,'
      'gd.Reserv1,'
      'gd.V01,'
      'gd.V02,'
      'gd.V03,'
      'gd.V04,'
      'gd.V05'
      'from "Goods" gd'
      'where gd.SubGroupID in (31)')
    Params = <>
    UniDirectional = True
    LoadBlobOnOpen = False
    Left = 616
    Top = 640
    object quRepRGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quRepRID: TIntegerField
      FieldName = 'ID'
    end
    object quRepRName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quRepRBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quRepRTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quRepREdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quRepRCena: TFloatField
      FieldName = 'Cena'
    end
    object quRepRFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quRepRStatus: TSmallintField
      FieldName = 'Status'
    end
    object quRepRReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object quRepRV01: TIntegerField
      FieldName = 'V01'
    end
    object quRepRV02: TIntegerField
      FieldName = 'V02'
    end
    object quRepRV03: TIntegerField
      FieldName = 'V03'
    end
    object quRepRV04: TIntegerField
      FieldName = 'V04'
    end
    object quRepRV05: TIntegerField
      FieldName = 'V05'
    end
  end
  object quB: TPvQuery
    DatabaseName = 'SCRYSTAL'
    Params = <>
    Left = 124
    Top = 184
  end
  object quRepTovarPeriod: TPvQuery
    DatabaseName = 'SCRYSTAL'
    OnCalcFields = quRepTovarPeriodCalcFields
    SQL.Strings = (
      'select   Rdate'
      #9#9',Code'
      #9#9',Quant'
      #9#9',RSumCor'
      #9#9',gd.Name as GoodsName'
      #9#9',gr3.Name as gr3Name '
      #9#9',gr2.Name as gr2Name'
      #9#9',gr1.Name as gr1Name'
      #9#9',d.Name as DepartName'
      #9#9',br.NameBrand'
      #9#9',cat.SID'
      'from "cn201006" cn'
      'left join Goods gd on cn.Code=gd.ID'
      'left join SubGroup gr3 on gr3.ID=gd.SubGroupID'
      'left join SubGroup gr2 on gr2.ID=gr3.GoodsGroupID'
      'left join SubGroup gr1 on gr1.ID=gr2.GoodsGroupID'
      'left join Depart d on d.ID=cn.Depart'
      'left join A_Brands br on br.ID=gd.v03'
      'left join A_Categ cat on cat.ID=gd.v02'
      'where rdate between '#39'2010-06-01'#39' and '#39'2010-06-10'#39)
    Params = <>
    Left = 420
    Top = 700
    object quRepTovarPeriodRDate: TDateField
      FieldName = 'RDate'
    end
    object quRepTovarPeriodCode: TIntegerField
      FieldName = 'Code'
    end
    object quRepTovarPeriodQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '### ### ### ###.##'
    end
    object quRepTovarPeriodRSumCor: TFloatField
      FieldName = 'RSumCor'
      DisplayFormat = '### ### ### ###.##'
    end
    object quRepTovarPeriodGoodsName: TStringField
      FieldName = 'GoodsName'
      Size = 30
    end
    object quRepTovarPeriodgr3Name: TStringField
      FieldName = 'gr3Name'
      Size = 30
    end
    object quRepTovarPeriodgr2Name: TStringField
      FieldName = 'gr2Name'
      Size = 30
    end
    object quRepTovarPeriodgr1Name: TStringField
      FieldName = 'gr1Name'
      Size = 30
    end
    object quRepTovarPeriodDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quRepTovarPeriodNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
    object quRepTovarPeriodSID: TStringField
      FieldName = 'SID'
      Size = 2
    end
    object quRepTovarPeriodMonth: TStringField
      FieldKind = fkCalculated
      FieldName = 'Month'
      Size = 7
      Calculated = True
    end
    object quRepTovarPeriodyear: TStringField
      FieldKind = fkCalculated
      FieldName = 'year'
      Size = 4
      Calculated = True
    end
    object quRepTovarPeriodkvartal: TStringField
      FieldKind = fkCalculated
      FieldName = 'kvartal'
      Calculated = True
    end
  end
  object quSel: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '')
    Params = <>
    Left = 20
    Top = 704
  end
  object quRepTO2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'SUM(rep.OldOstatokTovar) as OldOstatokTovar,'
      'SUM(rep.PrihodTovar) as PrihodTovar,'
      'SUM(rep.SelfPrihodTovar) as SelfPrihodTovar,'
      'SUM(rep.PrihodTovarSumma) as PrihodTovarSumma,'
      'SUM(rep.RashodTovar) as RashodTovar,'
      'SUM(rep.SelfRashodTovar) as SelfRashodTovar,'
      'SUM(rep.RealTovar) as RealTovar,'
      'SUM(rep.SkidkaTovar) as SkidkaTovar,'
      'SUM(rep.SenderSumma) as SenderSumma,'
      'SUM(rep.SenderSkidka) as SenderSkidka,'
      'SUM(rep.OutTovar) as OutTovar,'
      'SUM(rep.RashodTovarSumma) as RashodTovarSumma,'
      'SUM(rep.PereoTovarSumma) as PereoTovarSumma,'
      'SUM(rep.NewOstatokTovar) as NewOstatokTovar,'
      'SUM(rep.OldOstatokTara) as OldOstatokTara,'
      'SUM(rep.PrihodTara) as PrihodTara,'
      'SUM(rep.SelfPrihodTara) as SelfPrihodTara,'
      'SUM(rep.PrihodTaraSumma) as PrihodTaraSumma,'
      'SUM(rep.RashodTara) as RashodTara,'
      'SUM(rep.SelfRashodTara) as SelfRashodTara,'
      'SUM(rep.RezervSumma) as RezervSumma,'
      'SUM(rep.RezervSkidka) as RezervSkidka,'
      'SUM(rep.OutTara) as OutTara,'
      'SUM(rep.RashodTaraSumma) as RashodTaraSumma,'
      'SUM(rep.xRezerv) as xRezerv,'
      'SUM(rep.NewOstatokTara) as NewOstatokTara'
      ''
      'from "REPORTS" rep'
      'left join Depart de on de.ID=rep.Otdel'
      'where rep.xDate=:IDATE'
      'and de.V01>0')
    Params = <
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptInput
        Value = 0d
      end>
    Left = 528
    Top = 632
    object quRepTO2OldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
    end
    object quRepTO2PrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
    end
    object quRepTO2SelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
    end
    object quRepTO2PrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
    end
    object quRepTO2RashodTovar: TFloatField
      FieldName = 'RashodTovar'
    end
    object quRepTO2SelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
    end
    object quRepTO2RealTovar: TFloatField
      FieldName = 'RealTovar'
    end
    object quRepTO2SkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
    end
    object quRepTO2SenderSumma: TFloatField
      FieldName = 'SenderSumma'
    end
    object quRepTO2SenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
    end
    object quRepTO2OutTovar: TFloatField
      FieldName = 'OutTovar'
    end
    object quRepTO2RashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
    end
    object quRepTO2PereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
    end
    object quRepTO2NewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
    end
    object quRepTO2OldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
    end
    object quRepTO2PrihodTara: TFloatField
      FieldName = 'PrihodTara'
    end
    object quRepTO2SelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
    end
    object quRepTO2PrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
    end
    object quRepTO2RashodTara: TFloatField
      FieldName = 'RashodTara'
    end
    object quRepTO2SelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
    end
    object quRepTO2RezervSumma: TFloatField
      FieldName = 'RezervSumma'
    end
    object quRepTO2RezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
    end
    object quRepTO2OutTara: TFloatField
      FieldName = 'OutTara'
    end
    object quRepTO2RashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
    end
    object quRepTO2xRezerv: TFloatField
      FieldName = 'xRezerv'
    end
    object quRepTO2NewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
    end
  end
  object quPost4: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost4CalcFields
    SQL.Strings = (
      'select top 5'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.IndexPost,'
      'tl.CodePost,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.Procent,'
      'tl.NewCenaTovar,'
      'tl.SumCenaTovarPost,'
      'tl.SumCenaTovar                    '
      ''
      'from "TTNInLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=tl."CodePost"'
      'left outer join RIndividual i on i.Code=tl."CodePost"'
      ' '
      'where tl.CodeTovar=:IDCARD'
      'order by DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 852
    Top = 624
    object quPost4Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost4Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPost4DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost4Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPost4IndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quPost4CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost4NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost4NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost4CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPost4CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost4BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPost4Kol: TFloatField
      FieldName = 'Kol'
    end
    object quPost4CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quPost4Procent: TFloatField
      FieldName = 'Procent'
    end
    object quPost4NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quPost4SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quPost4SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quPost4NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
  end
  object dsquPost4: TDataSource
    DataSet = quPost4
    Left = 852
    Top = 676
  end
  object IdFTP1: TIdFTP
    MaxLineAction = maException
    ReadTimeout = 0
    RecvBufferSize = 1024
    Host = '80.75.90.50'
    Password = 'tf875bvcxfg35HGFdfg'
    Username = 'dfsd54dsf'
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 242
    Top = 128
  end
  object quReportSvod: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      're.Otdel,'
      'de.Name,'
      'de.V03,'
      're.xDate,'
      're.OldOstatokTovar,'
      're.PrihodTovar,'
      're.SelfPrihodTovar,'
      're.PrihodTovarSumma,'
      're.RashodTovar,'
      're.SelfRashodTovar,'
      're.RealTovar,'
      're.SkidkaTovar,'
      're.SenderSumma,'
      're.SenderSkidka,'
      're.OutTovar,'
      're.RashodTovarSumma,'
      're.PereoTovarSumma,'
      're.NewOstatokTovar,'
      're.OldOstatokTara,'
      're.PrihodTara,'
      're.SelfPrihodTara,'
      're.PrihodTaraSumma,'
      're.RashodTara,'
      're.SelfRashodTara,'
      're.RezervSumma,'
      're.RezervSkidka,'
      're.OutTara,'
      're.RashodTaraSumma,'
      're.xRezerv,'
      're.NewOstatokTara,'
      're.Oprihod'
      ''
      'from "REPORTS" re'
      'Left join Depart de on de.ID=re.Otdel'
      'where re."xDate">=:DATEB'
      'and  re."xDate"<=:DATEE'
      'and de.V01>0'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 528
    Top = 684
    object quReportSvodOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quReportSvodName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quReportSvodV03: TIntegerField
      FieldName = 'V03'
    end
    object quReportSvodxDate: TDateField
      FieldName = 'xDate'
    end
    object quReportSvodOldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
    end
    object quReportSvodPrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
    end
    object quReportSvodSelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
    end
    object quReportSvodPrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
    end
    object quReportSvodRashodTovar: TFloatField
      FieldName = 'RashodTovar'
    end
    object quReportSvodSelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
    end
    object quReportSvodRealTovar: TFloatField
      FieldName = 'RealTovar'
    end
    object quReportSvodSkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
    end
    object quReportSvodSenderSumma: TFloatField
      FieldName = 'SenderSumma'
    end
    object quReportSvodSenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
    end
    object quReportSvodOutTovar: TFloatField
      FieldName = 'OutTovar'
    end
    object quReportSvodRashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
    end
    object quReportSvodPereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
    end
    object quReportSvodNewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
    end
    object quReportSvodOldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
    end
    object quReportSvodPrihodTara: TFloatField
      FieldName = 'PrihodTara'
    end
    object quReportSvodSelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
    end
    object quReportSvodPrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
    end
    object quReportSvodRashodTara: TFloatField
      FieldName = 'RashodTara'
    end
    object quReportSvodSelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
    end
    object quReportSvodRezervSumma: TFloatField
      FieldName = 'RezervSumma'
    end
    object quReportSvodRezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
    end
    object quReportSvodOutTara: TFloatField
      FieldName = 'OutTara'
    end
    object quReportSvodRashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
    end
    object quReportSvodxRezerv: TFloatField
      FieldName = 'xRezerv'
    end
    object quReportSvodNewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
    end
    object quReportSvodOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
  end
  object quRepHour: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quRepHourCalcFields
    SQL.Strings = (
      'Select '
      'Sum(QuantClient_00) as Cl00, Sum(SumSale_00) as S00, '
      'Sum(QuantClient_01) as Cl01, Sum(SumSale_01) as S01, '
      'Sum(QuantClient_02) as Cl02, Sum(SumSale_02) as S02, '
      'Sum(QuantClient_03) as Cl03, Sum(SumSale_03) as S03, '
      'Sum(QuantClient_04) as Cl04, Sum(SumSale_04) as S04, '
      'Sum(QuantClient_05) as Cl05, Sum(SumSale_05) as S05, '
      'Sum(QuantClient_06) as Cl06, Sum(SumSale_06) as S06, '
      'Sum(QuantClient_07) as Cl07, Sum(SumSale_07) as S07, '
      'Sum(QuantClient_08) as Cl08, Sum(SumSale_08) as S08, '
      'Sum(QuantClient_09) as Cl09, Sum(SumSale_09) as S09, '
      'Sum(QuantClient_10) as Cl10, Sum(SumSale_10) as S10, '
      'Sum(QuantClient_11) as Cl11, Sum(SumSale_11) as S11, '
      'Sum(QuantClient_12) as Cl12, Sum(SumSale_12) as S12, '
      'Sum(QuantClient_13) as Cl13, Sum(SumSale_13) as S13, '
      'Sum(QuantClient_14) as Cl14, Sum(SumSale_14) as S14, '
      'Sum(QuantClient_15) as Cl15, Sum(SumSale_15) as S15, '
      'Sum(QuantClient_16) as Cl16, Sum(SumSale_16) as S16, '
      'Sum(QuantClient_17) as Cl17, Sum(SumSale_17) as S17, '
      'Sum(QuantClient_18) as Cl18, Sum(SumSale_18) as S18, '
      'Sum(QuantClient_19) as Cl19, Sum(SumSale_19) as S19, '
      'Sum(QuantClient_20) as Cl20, Sum(SumSale_20) as S20, '
      'Sum(QuantClient_21) as Cl21, Sum(SumSale_21) as S21, '
      'Sum(QuantClient_22) as Cl22, Sum(SumSale_22) as S22, '
      'Sum(QuantClient_23) as Cl23, Sum(SumSale_23) as S23, '
      
        'Max(SumMax_00) as M00, Max(SumMax_01) as M01, Max(SumMax_02) as ' +
        'M02, Max(SumMax_03) as M03, '
      
        'Max(SumMax_04) as M04, Max(SumMax_05) as M05, Max(SumMax_06) as ' +
        'M06, Max(SumMax_07) as M07, '
      
        'Max(SumMax_08) as M08, Max(SumMax_09) as M09, Max(SumMax_10) as ' +
        'M10, Max(SumMax_11) as M11, '
      
        'Max(SumMax_12) as M12, Max(SumMax_13) as M13, Max(SumMax_14) as ' +
        'M14, Max(SumMax_15) as M15, '
      
        'Max(SumMax_16) as M16, Max(SumMax_17) as M17, Max(SumMax_18) as ' +
        'M18, Max(SumMax_19) as M19, '
      
        'Max(SumMax_20) as M20, Max(SumMax_21) as M21, Max(SumMax_22) as ' +
        'M22, Max(SumMax_23) as M23, '
      
        'Sum(QuantSale) as QS, Sum(QuantClient)as QC, Sum(SumSale) as SS,' +
        ' MAX(SumMax) as SM'
      'from CashDay'
      'where DateSale>=:DATEB and DateSale<=:DATEE'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 172
    Top = 696
    object quRepHourCl00: TFloatField
      FieldName = 'Cl00'
    end
    object quRepHourS00: TFloatField
      FieldName = 'S00'
      DisplayFormat = '0.##'
    end
    object quRepHourCl01: TFloatField
      FieldName = 'Cl01'
    end
    object quRepHourS01: TFloatField
      FieldName = 'S01'
      DisplayFormat = '0.##'
    end
    object quRepHourCl02: TFloatField
      FieldName = 'Cl02'
    end
    object quRepHourS02: TFloatField
      FieldName = 'S02'
      DisplayFormat = '0.##'
    end
    object quRepHourCl03: TFloatField
      FieldName = 'Cl03'
    end
    object quRepHourS03: TFloatField
      FieldName = 'S03'
      DisplayFormat = '0.##'
    end
    object quRepHourCl04: TFloatField
      FieldName = 'Cl04'
    end
    object quRepHourS04: TFloatField
      FieldName = 'S04'
      DisplayFormat = '0.##'
    end
    object quRepHourCl05: TFloatField
      FieldName = 'Cl05'
    end
    object quRepHourS05: TFloatField
      FieldName = 'S05'
      DisplayFormat = '0.##'
    end
    object quRepHourCl06: TFloatField
      FieldName = 'Cl06'
    end
    object quRepHourS06: TFloatField
      FieldName = 'S06'
      DisplayFormat = '0.##'
    end
    object quRepHourCl07: TFloatField
      FieldName = 'Cl07'
    end
    object quRepHourS07: TFloatField
      FieldName = 'S07'
      DisplayFormat = '0.##'
    end
    object quRepHourCl08: TFloatField
      FieldName = 'Cl08'
    end
    object quRepHourS08: TFloatField
      FieldName = 'S08'
      DisplayFormat = '0.##'
    end
    object quRepHourCl09: TFloatField
      FieldName = 'Cl09'
    end
    object quRepHourS09: TFloatField
      FieldName = 'S09'
      DisplayFormat = '0.##'
    end
    object quRepHourCl10: TFloatField
      FieldName = 'Cl10'
    end
    object quRepHourS10: TFloatField
      FieldName = 'S10'
      DisplayFormat = '0.##'
    end
    object quRepHourCl11: TFloatField
      FieldName = 'Cl11'
    end
    object quRepHourS11: TFloatField
      FieldName = 'S11'
      DisplayFormat = '0.##'
    end
    object quRepHourCl12: TFloatField
      FieldName = 'Cl12'
    end
    object quRepHourS12: TFloatField
      FieldName = 'S12'
      DisplayFormat = '0.##'
    end
    object quRepHourCl13: TFloatField
      FieldName = 'Cl13'
    end
    object quRepHourS13: TFloatField
      FieldName = 'S13'
      DisplayFormat = '0.##'
    end
    object quRepHourCl14: TFloatField
      FieldName = 'Cl14'
    end
    object quRepHourS14: TFloatField
      FieldName = 'S14'
      DisplayFormat = '0.##'
    end
    object quRepHourCl15: TFloatField
      FieldName = 'Cl15'
    end
    object quRepHourS15: TFloatField
      FieldName = 'S15'
      DisplayFormat = '0.##'
    end
    object quRepHourCl16: TFloatField
      FieldName = 'Cl16'
    end
    object quRepHourS16: TFloatField
      FieldName = 'S16'
      DisplayFormat = '0.##'
    end
    object quRepHourCl17: TFloatField
      FieldName = 'Cl17'
    end
    object quRepHourS17: TFloatField
      FieldName = 'S17'
      DisplayFormat = '0.##'
    end
    object quRepHourCl18: TFloatField
      FieldName = 'Cl18'
    end
    object quRepHourS18: TFloatField
      FieldName = 'S18'
      DisplayFormat = '0.##'
    end
    object quRepHourCl19: TFloatField
      FieldName = 'Cl19'
    end
    object quRepHourS19: TFloatField
      FieldName = 'S19'
      DisplayFormat = '0.##'
    end
    object quRepHourCl20: TFloatField
      FieldName = 'Cl20'
    end
    object quRepHourS20: TFloatField
      FieldName = 'S20'
      DisplayFormat = '0.##'
    end
    object quRepHourCl21: TFloatField
      FieldName = 'Cl21'
    end
    object quRepHourS21: TFloatField
      FieldName = 'S21'
      DisplayFormat = '0.##'
    end
    object quRepHourCl22: TFloatField
      FieldName = 'Cl22'
    end
    object quRepHourS22: TFloatField
      FieldName = 'S22'
      DisplayFormat = '0.##'
    end
    object quRepHourCl23: TFloatField
      FieldName = 'Cl23'
    end
    object quRepHourS23: TFloatField
      FieldName = 'S23'
      DisplayFormat = '0.##'
    end
    object quRepHourM00: TFloatField
      FieldName = 'M00'
      DisplayFormat = '0.##'
    end
    object quRepHourM01: TFloatField
      FieldName = 'M01'
      DisplayFormat = '0.##'
    end
    object quRepHourM02: TFloatField
      FieldName = 'M02'
      DisplayFormat = '0.##'
    end
    object quRepHourM03: TFloatField
      FieldName = 'M03'
      DisplayFormat = '0.##'
    end
    object quRepHourM04: TFloatField
      FieldName = 'M04'
      DisplayFormat = '0.##'
    end
    object quRepHourM05: TFloatField
      FieldName = 'M05'
      DisplayFormat = '0.##'
    end
    object quRepHourM06: TFloatField
      FieldName = 'M06'
      DisplayFormat = '0.##'
    end
    object quRepHourM07: TFloatField
      FieldName = 'M07'
      DisplayFormat = '0.##'
    end
    object quRepHourM08: TFloatField
      FieldName = 'M08'
      DisplayFormat = '0.##'
    end
    object quRepHourM09: TFloatField
      FieldName = 'M09'
      DisplayFormat = '0.##'
    end
    object quRepHourM10: TFloatField
      FieldName = 'M10'
      DisplayFormat = '0.##'
    end
    object quRepHourM11: TFloatField
      FieldName = 'M11'
      DisplayFormat = '0.##'
    end
    object quRepHourM12: TFloatField
      FieldName = 'M12'
      DisplayFormat = '0.##'
    end
    object quRepHourM13: TFloatField
      FieldName = 'M13'
      DisplayFormat = '0.##'
    end
    object quRepHourM14: TFloatField
      FieldName = 'M14'
      DisplayFormat = '0.##'
    end
    object quRepHourM15: TFloatField
      FieldName = 'M15'
      DisplayFormat = '0.##'
    end
    object quRepHourM16: TFloatField
      FieldName = 'M16'
      DisplayFormat = '0.##'
    end
    object quRepHourM17: TFloatField
      FieldName = 'M17'
      DisplayFormat = '0.##'
    end
    object quRepHourM18: TFloatField
      FieldName = 'M18'
      DisplayFormat = '0.##'
    end
    object quRepHourM19: TFloatField
      FieldName = 'M19'
      DisplayFormat = '0.##'
    end
    object quRepHourM20: TFloatField
      FieldName = 'M20'
      DisplayFormat = '0.##'
    end
    object quRepHourM21: TFloatField
      FieldName = 'M21'
      DisplayFormat = '0.##'
    end
    object quRepHourM22: TFloatField
      FieldName = 'M22'
      DisplayFormat = '0.##'
    end
    object quRepHourM23: TFloatField
      FieldName = 'M23'
      DisplayFormat = '0.##'
    end
    object quRepHourQS: TFloatField
      FieldName = 'QS'
      DisplayFormat = '0'
    end
    object quRepHourQC: TFloatField
      FieldName = 'QC'
    end
    object quRepHourSS: TFloatField
      FieldName = 'SS'
      DisplayFormat = '0.##'
    end
    object quRepHourSM: TFloatField
      FieldName = 'SM'
      DisplayFormat = '0.##'
    end
    object quRepHourA01: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A01'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA00: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A00'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA02: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A02'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA03: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A03'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA04: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A04'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA05: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A05'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA06: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A06'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA07: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A07'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA08: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A08'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA09: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A09'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA10: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A10'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA11: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A11'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA12: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A12'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA13: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A13'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA14: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A14'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA15: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A15'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA16: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A16'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA17: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A17'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA18: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A18'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA19: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A19'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA20: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A20'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA21: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A21'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA22: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A22'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourA23: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A23'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quRepHourAITOG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AITOG'
      DisplayFormat = '0.##'
      Calculated = True
    end
  end
  object dsquRepHour: TDataSource
    DataSet = quRepHour
    Left = 244
    Top = 708
  end
  object quRepSumCli: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'Select '
      'Sum(Client_20) as S20,'
      'Sum(Client_50) as S50,'
      'Sum(Client_100) as S100,'
      'Sum(Client_200) as S200,'
      'Sum(Client_500) as S500,'
      'Sum(Client_1000) as S1000,'
      'Sum(Client_Greater) as SMORE'
      ''
      'from CashDay'
      ''
      'where DateSale>=:DATEB and'
      'DateSale<=:DATEE'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 80
    Top = 696
    object quRepSumCliS20: TFloatField
      FieldName = 'S20'
    end
    object quRepSumCliS50: TFloatField
      FieldName = 'S50'
    end
    object quRepSumCliS100: TFloatField
      FieldName = 'S100'
    end
    object quRepSumCliS200: TFloatField
      FieldName = 'S200'
    end
    object quRepSumCliS500: TFloatField
      FieldName = 'S500'
    end
    object quRepSumCliS1000: TFloatField
      FieldName = 'S1000'
    end
    object quRepSumCliSMORE: TFloatField
      FieldName = 'SMORE'
    end
  end
  object quCQCheck: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select DateOperation,Cash_Code,NSmena,Ck_Number,Operation,SUM(Su' +
        'mma) as CheckSum, Count(*) as QuantPos, max(Times) as Times from' +
        ' "cq201010"'
      'where DateOperation='#39'2010-10-31'#39
      'group by DateOperation,Cash_Code,NSmena,Ck_Number,Operation')
    Params = <>
    Left = 352
    Top = 640
    object quCQCheckDateOperation: TDateField
      FieldName = 'DateOperation'
    end
    object quCQCheckCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quCQCheckNSmena: TIntegerField
      FieldName = 'NSmena'
    end
    object quCQCheckCk_Number: TIntegerField
      FieldName = 'Ck_Number'
    end
    object quCQCheckCheckSum: TFloatField
      FieldName = 'CheckSum'
    end
    object quCQCheckQuantPos: TIntegerField
      FieldName = 'QuantPos'
    end
    object quCQCheckOperation: TStringField
      FieldName = 'Operation'
      Size = 1
    end
    object quCQCheckTimes: TTimeField
      FieldName = 'Times'
    end
  end
  object quZad: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_ZADANIYA"'
      'where ZDate>=:DATEB and ZDATE<=:DATEE')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptInput
        Value = 0d
      end>
    Left = 656
    Top = 700
    object quZadId: TStringField
      FieldName = 'Id'
    end
    object quZadIType: TSmallintField
      FieldName = 'IType'
    end
    object quZadComment: TStringField
      FieldName = 'Comment'
      Size = 200
    end
    object quZadStatus: TSmallintField
      FieldName = 'Status'
    end
    object quZadZDate: TDateField
      FieldName = 'ZDate'
    end
    object quZadZTime: TTimeField
      FieldName = 'ZTime'
    end
    object quZadPersonId: TIntegerField
      FieldName = 'PersonId'
    end
    object quZadPersonName: TStringField
      FieldName = 'PersonName'
      Size = 50
    end
    object quZadFormDate: TDateField
      FieldName = 'FormDate'
    end
    object quZadFormTime: TTimeField
      FieldName = 'FormTime'
    end
    object quZadIdDoc: TStringField
      FieldName = 'IdDoc'
    end
  end
  object dsquZad: TDataSource
    DataSet = quZad
    Left = 704
    Top = 700
  end
  object quZadSel: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_ZADANIYA"'
      'where Status<2'
      'order by ZDate desc')
    Params = <>
    Left = 596
    Top = 704
    object quZadSelId: TStringField
      FieldName = 'Id'
    end
    object quZadSelIType: TSmallintField
      FieldName = 'IType'
    end
    object quZadSelComment: TStringField
      FieldName = 'Comment'
      Size = 200
    end
    object quZadSelStatus: TSmallintField
      FieldName = 'Status'
    end
    object quZadSelZDate: TDateField
      FieldName = 'ZDate'
    end
    object quZadSelZTime: TTimeField
      FieldName = 'ZTime'
    end
    object quZadSelPersonId: TIntegerField
      FieldName = 'PersonId'
    end
    object quZadSelPersonName: TStringField
      FieldName = 'PersonName'
      Size = 50
    end
    object quZadSelFormDate: TDateField
      FieldName = 'FormDate'
    end
    object quZadSelFormTime: TTimeField
      FieldName = 'FormTime'
    end
    object quZadSelIdDoc: TStringField
      FieldName = 'IdDoc'
    end
  end
  object quZ: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 176
    Top = 132
  end
  object quCQZ: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select cq.Ck_Card, SUM(cq.Ck_Disc)as CorSum, sum(cq.Summa) as RS' +
        'um'
      'from  "cq201012" cq'
      'left join Goods gd on gd.ID=cq.Code'
      'where cq.DateOperation='#39'2010-12-12'#39' and cq.Cash_Code=1'
      'group by cq.Ck_Card')
    Params = <>
    Left = 352
    Top = 696
    object quCQZCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quCQZCorSum: TFloatField
      FieldName = 'CorSum'
    end
    object quCQZRSum: TFloatField
      FieldName = 'RSum'
    end
  end
  object quAkciya: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quAkciyaCalcFields
    SQL.Strings = (
      'select  '
      'ak.Code,ak.AType,ak.DateB,ak.DateE,ak.Price,ak.DStop,'
      'ak.DProc,ak.StatusOn,ak.StatusOf,'
      'gd.Name,gd.GoodsGroupID,sg.Name as NameGr,ac.SID'
      ',ak.OldDstop, ak.OldPrice, ak.DocCode, ak.DocDate, ak.DocTime'
      ',br.NAMEBRAND'
      'from "A_AKCIYA" ak'
      'left join Goods gd  on gd.ID=ak.Code'
      'left join SubGroup sg  on sg.ID=gd.GoodsGroupID '
      'left join A_CATEG ac on ac.ID=ak.DSTOP and ak.AType=3'
      'left join "A_BRANDS" br on br."ID" = gd."V01"'
      'where '
      '   (DateB between :DATEB1 and :DATEE1)'
      'or (DateE between :DATEB2 and :DATEE2)'
      'or (DateB<:DATEB3 and DateE>:DATEE3);')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB1'
        ParamType = ptUnknown
        Value = 40179d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE1'
        ParamType = ptUnknown
        Value = 40179d
      end
      item
        DataType = ftDateTime
        Name = 'DATEB2'
        ParamType = ptUnknown
        Value = 40179d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE2'
        ParamType = ptUnknown
        Value = 40179d
      end
      item
        DataType = ftString
        Name = 'DATEB3'
        ParamType = ptUnknown
        Value = 40179d
      end
      item
        DataType = ftString
        Name = 'DATEE3'
        ParamType = ptUnknown
        Value = 40179d
      end>
    Left = 760
    Top = 696
    object quAkciyaAType: TSmallintField
      FieldName = 'AType'
    end
    object quAkciyaCode: TIntegerField
      FieldName = 'Code'
    end
    object quAkciyaDateB: TDateField
      FieldName = 'DateB'
    end
    object quAkciyaDateE: TDateField
      FieldName = 'DateE'
    end
    object quAkciyaPrice: TFloatField
      FieldName = 'Price'
    end
    object quAkciyaDStop: TSmallintField
      FieldName = 'DStop'
    end
    object quAkciyaDProc: TFloatField
      FieldName = 'DProc'
    end
    object quAkciyaStatusOn: TSmallintField
      FieldName = 'StatusOn'
    end
    object quAkciyaStatusOf: TSmallintField
      FieldName = 'StatusOf'
    end
    object quAkciyaName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quAkciyaGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quAkciyaNameGr: TStringField
      FieldName = 'NameGr'
      Size = 30
    end
    object quAkciyaVal1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Val1'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object quAkciyaSID: TStringField
      FieldName = 'SID'
      Size = 2
    end
    object quAkciyaVal2: TStringField
      FieldKind = fkCalculated
      FieldName = 'Val2'
      Calculated = True
    end
    object quAkciyaOldDStop: TSmallintField
      FieldName = 'OldDStop'
    end
    object quAkciyaOldPrice: TFloatField
      FieldName = 'OldPrice'
    end
    object quAkciyaDocCode: TIntegerField
      FieldName = 'DocCode'
    end
    object quAkciyaDocDate: TDateField
      FieldName = 'DocDate'
    end
    object quAkciyaDocTime: TIntegerField
      FieldName = 'DocTime'
    end
    object quAkciyaVal2After: TStringField
      FieldKind = fkCalculated
      FieldName = 'Val2After'
      Calculated = True
    end
    object quAkciyaDateTime: TStringField
      FieldKind = fkCalculated
      FieldName = 'DateTime'
      Calculated = True
    end
    object quAkciyaNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
  end
  object dsquAkciya: TDataSource
    DataSet = quAkciya
    Left = 760
    Top = 756
  end
  object quObmH: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quObmHCalcFields
    SQL.Strings = (
      'select '
      'dh.ID,'
      'dh.DOCDATE,'
      'dh.DOCNUM,'
      'dh.DEPART,'
      'dh.ITYPE,'
      'dh.INSUMIN,'
      'dh.INSUMOUT,'
      'dh.OUTSUMIN,'
      'dh.OUTSUMOUT,'
      'dh.CLITYPE,'
      'dh.CLICODE,'
      'dh.IACTIVE,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'dep."Name" as NameDep,'
      'v."INN" as INNPost,'
      'i."INN" as INNInd'
      ''
      'from "A_DOCEXCHHEAD" dh'
      'left outer join RVendor v on v.Vendor=dh.CLICODE'
      'left outer join RIndividual i on i.Code=dh.CLICODE'
      'left outer join Depart dep on dep.ID=dh.DEPART'
      ''
      'where dh.DOCDATE>=:DATEB'
      'and dh.DOCDATE<=:DATEE'
      ''
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 952
    Top = 668
    object quObmHID: TAutoIncField
      FieldName = 'ID'
    end
    object quObmHDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object quObmHDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quObmHDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quObmHITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quObmHINSUMIN: TFloatField
      FieldName = 'INSUMIN'
      DisplayFormat = '0.00'
    end
    object quObmHINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
      DisplayFormat = '0.00'
    end
    object quObmHOUTSUMIN: TFloatField
      FieldName = 'OUTSUMIN'
      DisplayFormat = '0.00'
    end
    object quObmHCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quObmHCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quObmHOUTSUMOUT: TFloatField
      FieldName = 'OUTSUMOUT'
      DisplayFormat = '0.00'
    end
    object quObmHNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quObmHNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quObmHNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quObmHINNPost: TStringField
      FieldName = 'INNPost'
    end
    object quObmHINNInd: TStringField
      FieldName = 'INNInd'
    end
    object quObmHNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
    object quObmHINN: TStringField
      FieldKind = fkCalculated
      FieldName = 'INN'
      Calculated = True
    end
    object quObmHIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
  end
  object dsquObmH: TDataSource
    DataSet = quObmH
    Left = 1008
    Top = 668
  end
  object quZakH: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quZakHCalcFields
    SQL.Strings = (
      'select '
      'dh.ID,'
      'dh.DOCDATE,'
      'dh.DOCNUM,'
      'dh.DEPART,'
      'dh.ITYPE,'
      'dh.INSUMIN,'
      'dh.INSUMOUT,'
      'dh.CLITYPE,'
      'dh.CLICODE,'
      'dh.IACTIVE,'
      'dh.DATEEDIT,'
      'dh.TIMEEDIT,'
      'dh.PERSON,'
      'dh."CLIQUANT",'
      'dh."CLISUMIN0",'
      'dh."CLISUMIN",'
      'dh."CLIQUANTN",'
      'dh."CLISUMIN0N",'
      'dh."CLISUMINN",'
      'dh."IDATENACL",'
      'dh."SNUMNACL",'
      'dh."IDATESCHF",'
      'dh."SNUMSCHF",'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'dep."Name" as NameDep,'
      'v."INN" as INNPost,'
      'i."INN" as INNInd'
      ''
      'from "A_DOCZHEAD" dh'
      'left outer join RVendor v on v.Vendor=dh.CLICODE'
      'left outer join RIndividual i on i.Code=dh.CLICODE'
      'left outer join Depart dep on dep.ID=dh.DEPART'
      ''
      'where dh.DOCDATE>=:DATEB'
      'and dh.DOCDATE<=:DATEE'
      ''
      ''
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 912
    Top = 732
    object quZakHID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object quZakHDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object quZakHDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quZakHDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quZakHITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quZakHINSUMIN: TFloatField
      FieldName = 'INSUMIN'
      DisplayFormat = '0.00'
    end
    object quZakHINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
      DisplayFormat = '0.00'
    end
    object quZakHCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quZakHCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quZakHIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quZakHNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quZakHNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quZakHNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quZakHINNPost: TStringField
      FieldName = 'INNPost'
    end
    object quZakHINNInd: TStringField
      FieldName = 'INNInd'
    end
    object quZakHPERSON: TStringField
      FieldName = 'PERSON'
      Size = 100
    end
    object quZakHDATEEDIT: TDateField
      FieldName = 'DATEEDIT'
    end
    object quZakHTIMEEDIT: TStringField
      FieldName = 'TIMEEDIT'
      Size = 10
    end
    object quZakHNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 150
      Calculated = True
    end
    object quZakHINN: TStringField
      FieldKind = fkCalculated
      FieldName = 'INN'
      Calculated = True
    end
    object quZakHSDATEEDIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'SDATEEDIT'
      Size = 15
      Calculated = True
    end
    object quZakHCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
      DisplayFormat = '0.000'
    end
    object quZakHCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
      DisplayFormat = '0.00'
    end
    object quZakHCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
      DisplayFormat = '0.00'
    end
    object quZakHCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
      DisplayFormat = '0.000'
    end
    object quZakHCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
      DisplayFormat = '0.00'
    end
    object quZakHCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
      DisplayFormat = '0.00'
    end
    object quZakHIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quZakHSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quZakHIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quZakHSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
    object quZakHsDatePost: TStringField
      FieldKind = fkCalculated
      FieldName = 'sDatePost'
      Calculated = True
    end
  end
  object dsquZakH: TDataSource
    DataSet = quZakH
    Left = 964
    Top = 732
  end
  object quSale: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select gd.NDS,SUM(po.SUMIN)as SUMIN,SUM(po.SUMIN0)as SUMIN0 from' +
        ' "A_PARTOUT" po'
      'left join GOODS gd on gd.ID=po.ARTICUL'
      'where po.IDATE=:IDATE and po.IDSTORE=:IDEP and po.ITYPE=7'
      'group by gd.NDS')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 848
    Top = 752
    object quSaleNDS: TFloatField
      FieldName = 'NDS'
    end
    object quSaleSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quSaleSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quCQCount: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select (max(Ck_Number)) as MaxCh from "cq201101"'
      'where '
      'DateOperation='#39'2011-01-05'#39' '
      'and Cash_Code=6 '
      'and (Ck_Number<1000 or Ck_Number>1020) ')
    Params = <>
    Left = 20
    Top = 760
    object quCQCountMaxCh: TIntegerField
      FieldName = 'MaxCh'
    end
  end
  object quPost5: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost5CalcFields
    SQL.Strings = (
      'select top 5'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.IndexPost,'
      'tl.CodePost,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.Procent,'
      'tl.NewCenaTovar,'
      'tl.SumCenaTovarPost,'
      'tl.SumCenaTovar,'
      'tl.OutNDSSum,'
      'tl.SertNumber            '
      ''
      'from "TTNInLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=tl."CodePost"'
      'left outer join RIndividual i on i.Code=tl."CodePost"'
      ' '
      'where tl.CodeTovar=:IDCARD'
      'and DateInvoice<=:DDATE'
      'order by DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 900
    Top = 620
    object quPost5Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost5Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPost5DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost5Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPost5IndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quPost5CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost5NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost5NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost5CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPost5CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost5BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPost5Kol: TFloatField
      FieldName = 'Kol'
    end
    object quPost5CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quPost5Procent: TFloatField
      FieldName = 'Procent'
    end
    object quPost5NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object quPost5SumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object quPost5SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quPost5NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
    object quPost5OutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quPost5CenaTovar0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CenaTovar0'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object quPost5SertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
  end
  object dsquPost5: TDataSource
    DataSet = quPost5
    Left = 900
    Top = 676
  end
  object quSpecVn2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'ln.Depart,'
      'ln.CodeTovar,'
      'ln.CodeGroup,'
      'ln.CodePodgr,'
      'Ln.CodeTara,'
      'ln.Kol,'
      'Ln."CenaTovarSpis",'
      'Ln."CenaTovarMove",'
      'Ln."SumCenaTovarSpis",'
      'Ln."SumNewCenaTovar",'
      'Ln."SumCenaTara",'
      'Ln."NewCenaTovar"'
      ''
      'from "TTNSelfLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      'order by CodeGroup,CodePodgr'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 476
    Top = 408
    object quSpecVn2Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSpecVn2CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecVn2CodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSpecVn2CodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSpecVn2CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSpecVn2Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecVn2CenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quSpecVn2SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSpecVn2SumNewCenaTovar: TFloatField
      FieldName = 'SumNewCenaTovar'
    end
    object quSpecVn2SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSpecVn2CenaTovarMove: TFloatField
      FieldName = 'CenaTovarMove'
    end
    object quSpecVn2NewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
  end
  object quPostVn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost1CalcFields
    SQL.Strings = (
      'select top 3'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.Kol,'
      'tl.CenaTovarSpis,'
      'tl.CenaTovarMove                 '
      ''
      'from "TTNSelfLn" tl'
      'left join Depart de on de.ID=tl.Depart'
      
        'left join "TTNSelf" hd on (hd.Depart=tl.Depart and hd.Number=tl.' +
        'Number and  hd.DateInvoice=tl.DateInvoice) '
      'where tl.CodeTovar=:IDCARD and hd.AcStatusP=3'
      'order by hd.DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 684
    Top = 516
    object quPostVnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quPostVnName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPostVnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPostVnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPostVnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPostVnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPostVnKol: TFloatField
      FieldName = 'Kol'
    end
    object quPostVnCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quPostVnCenaTovarMove: TFloatField
      FieldName = 'CenaTovarMove'
    end
  end
  object quFLastPriceVN: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select top 1 ln.CenaTovarSpis,ln.CenaTovarMove from "TTNSelfLn" ' +
        'ln'
      
        'left join "TTNSelf" hd on (hd.Depart=ln.Depart and hd.Number=ln.' +
        'Number and  hd.DateInvoice=ln.DateInvoice) '
      
        'where ln.CodeTovar=:IDC and hd.AcStatusP=3 and hd.DateInvoice>:I' +
        'DATE '
      'order by hd.DateInvoice DESC')
    Params = <
      item
        DataType = ftString
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0d
      end>
    LoadBlobOnOpen = False
    Left = 660
    Top = 256
    object quFLastPriceVNCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quFLastPriceVNCenaTovarMove: TFloatField
      FieldName = 'CenaTovarMove'
    end
  end
  object ImageList1: TImageList
    Left = 896
    Top = 12
    Bitmap = {
      494C010117001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001002000000000000060
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADA59C00737373005A5A
      5A00424242004A4A4A00ADADAD003939390021212100424242004A4A4A004242
      4200636363005252520052525200636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B5A42007B524200000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000ADA59C00736B
      6B004A4A4A004A525200738CAD005A6B8C003131310018181800393939004242
      4200393939004A4A4A00948C8400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B5A4200845A4200DEB5A500D6AD94008C5A42007B52
      3900000000000000000000000000000000008484840000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C60084848400848484000000000000000000000000000000000000000000EFD6
      C600B5ADAD006B7B94005A84CE00638CDE006384B50029314200212121005A5A
      5A006B6B6300BDADA50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000735A4A007B523900E7D6C600E7CEBD007B5A42007B524200D6B59C00D6AD
      94007B5A42007352390000000000000000008484840000000000000000000000
      0000000000008400000084000000840000008400000084000000840000000000
      0000848484008484840000000000000000000000000000000000000000000000
      00009CBDF70084B5F7006B9CEF005A84D600638CDE006B8CCE00424A5A00847B
      7300DECEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B5A4A0000000000FFFF
      F700735A4A007B523900E7D6C600E7CEBD007B5A42007B524200D6B59C00D6AD
      94007B5A42007352390000000000000000000000000000000000000000000000
      00008400000084000000FF000000FF0000008400000084000000840000008400
      000084000000840000000000000000000000000000000000000000000000B5C6
      E7008CBDFF008CBDFF008CBDFF0073A5F700638CE70073A5EF0094A5CE00E7CE
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C6B52008C5A
      4200F7EFE700F7E7DE007B5A4200845A4200DEB5A500D6AD94008C5A42007B52
      3900D6B59C00D6AD940073523900000000008484840084848400848484008484
      8400848484008400000084000000C6C6C6000000000084848400840000008400
      0000840000008400000084000000000000000000000000000000E7D6CE00A5BD
      F70094BDFF008CBDFF008CBDFF008CB5FF006B9CF700739CEF00A5BDE7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B5A4A0000000000FFFF
      F700735A4A007B523900E7D6C600E7CEBD007B5A42007B524200D6B59C00D6AD
      94007B5A42007352390000000000000000008484840000000000000000000000
      000000000000000000008484840000000000C6C6C60000000000840000000000
      0000848484008400000084000000000000000000000000000000DED6DE00A5C6
      FF0094BDFF0094BDFF0094BDFF0094BDFF007BADF7006B9CEF0094ADE700EFD6
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C6B52008C5A
      4200F7EFE700F7E7DE007B5A4200845A4200DEB5A500D6AD94008C5A42007B52
      3900D6B59C00D6AD94007352390000000000848484000000000084000000FF00
      0000000000000000000000000000C6C6C600000000000000000000000000C6C6
      C600000000008400000000000000000000000000000000000000CECEDE00ADCE
      FF009CC6FF009CC6FF009CC6FF009CC6FF008CB5FF006B9CEF005A7BB500B5AD
      A500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B5A4A0000000000FFFF
      F700735A4A007B523900E7D6C600E7CEBD007B5A42007B524200D6B59C00D6AD
      94007B5A4200735239000000000000000000848484000000000084000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6E700BDD6
      FF00A5CEFF00A5CEFF00ADCEFF00A5CEFF0094BDFF006B94E7005A84C6005252
      5A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C6B52008C5A
      4200F7EFE700F7E7DE007B5A42007B5A4200DEB5A500D6AD94008C5A42007B52
      3900D6B59C00D6AD94007352390000000000848484000000000084000000FF00
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000D6D6E700C6DE
      FF00B5D6FF00BDD6FF00BDDEFF00BDDEFF0094B5EF00394A6B0031426B002121
      2900B5ADA5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B5A4A0000000000FFFF
      F7007B5A42007B5A4200E7D6C600E7CEBD00DEC6B500DEBDAD00D6B59C00D6AD
      94007B5A4200735239000000000000000000848484000000000084000000FF00
      000000000000000000000000000000000000C6C6C60000000000840000000000
      0000000000000000000084848400000000000000000000000000DEDEEF00DEEF
      FF00CEE7FF00D6E7FF00DEEFFF00CEE7FF008CB5E70031313900000000001010
      1000AD9C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B5A42007B5A
      4200F7E7E700EFDED600EFDED600EFD6CE00E7D6C600E7CEBD00DEC6B500DEBD
      A500D6B59C00D6AD940073523900000000008484840000000000840000008400
      00000000000000000000000000000000000000000000C6C6C600000000000000
      000000000000840000000000000000000000000000000000000000000000BDC6
      D600C6D6EF00DEEFFF00EFF7FF00CEE7FF007B94BD0031313100080808001818
      1800ADA5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFDED600EFDE
      D600FFFFF700FFF7F700F7EFE700F7E7DE00EFDED600F7E7DE00DEBDA500DEB5
      A500000000000000000000000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      00000000000000000000000000000000000000000000EFE7DE007B737B003939
      420039424A005A6B7B007B94AD006B84A500525A630039393900212121003131
      2900D6C6BD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7E7DE00EFE7DE00FFF7F700F7EFEF00E7D6C600E7CEBD00000000000000
      0000000000000000000000000000000000000000000084848400C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000084848400000000000000000000000000EFDED600A59C9C007373
      73005A5A5A004242420031313100101010002121210052525200424242007B73
      7300F7E7DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F7EFE700F7E7DE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D6CE
      C600A59C9C00737373004A4A4A00292929002118180063635A00B5A5A500E7DE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7E7DE00DECEC600CEBDBD00CEC6BD00F7DEDE00F7EFDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B0073424200734242007342420073424200734242007342
      4200734242007342420073424200000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00FFFFE700F7EFDE00F7EFD600F7EFD600F7EFD600F7E7
      CE00F7EFD600EFDEC6007342420000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00F7EFDE00F7DEBD00F7D6BD00F7D6BD00EFD6B500EFD6
      B500EFDEBD00E7D6BD007342420000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00FFF7E700F7DEBD00F7D6B500F7D6B500F7D6B500F7D6
      AD00F7DEC600E7D6C60084524A0000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00FFF7EF00FFDEBD00FFDEBD00FFDEB500F7D6B500F7D6
      B500F7DEC600E7D6C60084524A0000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000000FF0084848400C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000084000084848400C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00FFFFF700FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400EFDECE008C5A5A0000000000000000000000000084848400FFFF
      FF00FFFFFF000000FF000000FF000000FF0084848400C6C6C600FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF0000840000008400000084000084848400C6C6C600FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B0000000000FFE7CE00FFE7C600FFDEC600FFDEC600FFE7
      C600FFF7DE00E7D6CE008C5A5A0000000000000000000000000084848400FFFF
      FF000000FF000000FF00FFFFFF000000FF000000FF0084848400FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF000084000000840000FFFFFF00008400000084000084848400FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000000000000000000000000000000000000000000000000000DE
      0000007300000039000000DE000000000000FFFFFF00FFFFF700FFFFF700E7D6
      D600C6B5AD00A59494009C635A0000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF008484840084848400FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00008400008484840084848400FFFF
      FF00FFFFFF0000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000039
      0000BDFFBD00BDFFBD000039000000000000000000000000000000000000A573
      6B00A5736B00A5736B00A5736B0000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF0084848400C6C6
      C600FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000084848400C6C6
      C600FFFFFF000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000DE0000007300000073
      0000BDFFBD00BDFFBD00007300000039000000DE00000000000000000000A573
      6B00E7A55200B5735A000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF008484
      8400FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00008400008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000073000018FF2100BDFF
      BD00BDFFBD00BDFFBD00BDFFBD0000DE000000390000A5736B00A5736B00A573
      6B00AD6B6B00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000018FF210000DE000000DE
      00006BFF730018FF2100007300000073000018FF210000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000DE
      00006BFF73006BFF730000730000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000006BFF
      730018FF210018FF21006BFF7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000018BDD6000084C6000084C6000084C6000084
      C6000084C6000084C600007BBD0018BDD6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A5A8C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000089CCE00089CCE007BBDCE0042DEF7007BBDCE0042DE
      F70084DEDE0042DEF70042DEF700007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5736B007342420073424200734242007342420073424200734242007342
      4200734242007342420073424200000000001873CE00214AAD005A5A8C008484
      840000000000000000000000000000000000000000006B6B6B007B736B006B6B
      6B00000000000000000000000000000000000000000000000000000000000000
      000000000000089CCE0018BDD6007BBDCE0084DEDE0052EFF70073CEDE0052EF
      F70073CEDE0052EFF7006BA5BD00007BBD000000000000000000000000000000
      0000A5736B007342420073424200734242007342420073424200734242007342
      4200734242007342420073424200000000000000000000000000000000000000
      0000A5736B00FFFFE700F7EFDE00F7EFD600F7EFD600F7EFD600F7EFD600F7E7
      CE00F7EFD600EFDEC600734242000000000000000000189CFF00006BD6002152
      B50063528C005A8CB500FFFFFF00947B7300BD947B00BDAD8C00C6B59400AD8C
      7300948473000000000000000000000000000000000000000000000000000000
      000018BDD60018BDD60052EFF70052EFF70052EFF70084DEDE0084DEDE0073CE
      DE0052EFF70084DEDE0052EFF700007BBD000000000000000000000000000000
      0000A5736B00FFFFE700F7EFDE00F7EFD600F7EFD600F7EFD600F7EFD600F7E7
      CE00F7EFD600EFDEC60073424200000000000000000000000000000000000000
      0000A5736B00F7EFDE00F7DEBD00F7D6BD00F7D6BD00F7D6BD00EFD6B500EFD6
      B500EFDEBD00E7D6BD0073424200000000000000000000000000000000001894
      F700106BD6006B84A500AD948C00E7C69400FFFFC600FFFFE700FFFFF700FFFF
      FF00C6A58C00846B5A00000000000000000000000000000000000000000018BD
      D60052CEF70052EFF70052EFF70052EFF70073CEDE0052EFF70073CEDE0084DE
      DE0073CEDE0084DEDE0052EFF700007BBD000000000000000000000000000000
      0000A5736B00FFEFDE00FFC69400FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400E7D6BD0073424200000000000000000010101000084A52001073
      840000000000FFF7E700F7DEBD00F7D6B500F7D6B500F7D6B500F7D6B500F7D6
      AD00F7DEC600E7D6C60084524A00000000000000000000000000000000008484
      8400FFFFFF0084B5BD00CE947300FFFFB500FFFFC600FFFFDE00FFFFFF00FFFF
      F700FFFFDE00A5846300526B6B00000000000000000000000000089CCE0031C6
      F70042DEF70052EFF70084DEDE0052EFF70084DEDE0052EFF70073CEDE0052EF
      F70052EFF70084DEDE0052EFF700007BBD000000000000000000000000000000
      0000A5736B00FFF7E700F7DEBD00F7D6B500F7D6B500F7D6B500F7D6B500F7D6
      AD00F7DEC600E7D6C60084524A0000000000000000008484840010DEEF0018C6
      DE0029B5D60052525200FFDEBD00FFDEBD00FFDEBD00FFDEB500F7D6B500F7D6
      B500F7DEC600E7D6C60084524A00000000000000000000000000000000008484
      8400FFFFFF009CC6C600EFCE9C00FFEFA500FFF7B500FFFFD600FFFFE700FFFF
      DE00FFFFD600BD9C73006B6B6300000000000000000018BDD6005AD6EF0039BD
      F7004AE7F70052F7F70052EFF70052EFF70073CEDE0084DEDE0052EFF70052EF
      F70084DEDE0052EFF700089CCE00007BBD000000000000000000000000000000
      0000A5736B00FFF7EF00FFDEBD00FFDEBD00FFDEBD00FFDEB500F7D6B500F7D6
      B500F7DEC600E7D6C60084524A000000000000000000B5B5B50008EFF70010D6
      EF0021BDDE0000000000FFC69400FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400EFDECE008C5A5A00000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00DED6AD00FFE79C00FFE79C00FFF7B500FFFFCE00FFFF
      C600FFFFBD00CEA57300736B630000000000089CCE00089CCE009CEFF70039BD
      F7005AE7F70073F7F70052EFF70052EFF70084DEDE0052EFF70084DEDE0052EF
      F70018BDD600089CCE0052EFF700007BBD000000000000000000000000000000
      0000A5736B00FFFFF700FFC69400FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400EFDECE008C5A5A000000000000000000000000008484840008E7
      F70018CEE70021B5D60052525200FFE7C600FFE7C600FFDEC600FFDEC600FFE7
      C600FFF7DE00E7D6CE008C5A5A00000000000000000000000000000000008484
      8400FFFFFF0084848400C6BD9C00FFF7B500FFE7AD00FFE7A500FFE7A500FFDE
      9400FFF7AD00AD8463004A6B730000000000089CCE00A5DEE700BDEFF70042BD
      F70084E7F700A5F7F700A5EFEF0052EFF70084DEDE0052EFF70052EFF70073CE
      DE00089CCE0052EFF70052EFF700007BBD000000000000000000000000000000
      0000A5736B0000000000FFE7CE00FFE7C600FFE7C600FFDEC600FFDEC600FFE7
      C600FFF7DE00E7D6CE008C5A5A00000000000000000000000000B5B5B50000F7
      FF0010DEEF0018C6DE0000000000FFFFFF00FFFFFF00FFFFF700FFFFF700E7D6
      D600C6B5AD00A59494009C635A00000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00C6B5AD00EFD6B500FFFFFF00FFEFD600FFF7B500FFFF
      B500DEB58400847B7B000000000000000000089CCE00E7EFEF00C6E7EF006BC6
      EF00ADE7EF00D6EFEF00CEEFEF007BBDCE0084DEDE0073CEDE0018BDD600089C
      CE0052EFF70052EFF7004AC6DE00007BBD000000000000000000000000000000
      0000A5736B000000000000000000FFFFFF00FFFFFF00FFFFF700FFFFF700E7D6
      D600C6B5AD00A59494009C635A00000000000000000000000000000000008484
      840008EFF70010D6EF0021BDDE0052525200525252000000000000000000A573
      6B00A5736B00A5736B00A5736B00000000000000000000000000000000008484
      8400FFFFFF00848484008484840094ADB500D6BDBD00DED6C600DECEA500BD9C
      7B009C8C8400000000000000000000000000089CCE00089CCE00089CCE00089C
      CE00089CCE00089CCE00089CCE00089CCE00089CCE00089CCE0018BDD60052EF
      F7006BD6D60052EFF70052EFF700007BBD000000000000000000000000000000
      0000A5736B00000000000000000000000000000000000000000000000000A573
      6B00A5736B00A5736B00A5736B0000000000000000000000000000000000B5B5
      B50000FFFF0008E7F70018CEE70000000000000000000000000000000000A573
      6B00E7A55200B5735A0000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007BB5AD007BADAD00FFFF
      FF00000000000000000000000000000000000000000000000000000000000084
      C60042DEE70063F7F70063F7F70063F7F7005AEFEF006BEFEF006BEFEF0052F7
      F70052F7F70052F7F7005AF7F700007BBD0000000000000094000000FF000000
      FF003131FF003131FF003131FF007373FF007373FF007373FF0000000000A573
      6B00E7A55200B5735A0000000000000000000000000000000000000000000000
      00008484840000F7FF0000000000000000000000000000000000A5736B00A573
      6B00AD6B6B000000000000000000000000000000000000000000000000008484
      8400FFFFFF00848484008484840084848400FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C60042C6DE006BD6E7007BD6E70073CEDE000084C6000084C60073CEDE008CF7
      F70073F7F70084F7F7009CF7F700007BBD00000000000000FF007373FF00ADAD
      FF00ADADFF00ADADFF007373FF003131FF003131FF0000009400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400CECECE00949494009494940084848400525252000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C6000084C6000084C60000000000000000000084C60073CE
      DE0073D6DE008CCEE7007BC6DE00007BBD00000000007373FF007373FF003131
      FF003131FF003131FF000000FF000000FF000000FF0000009400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B500EFEFEF00B5B5B500B5B5B50084848400000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C6000084C6000084C6000084C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A5A8C005A5A8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C600007BBD00007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000000000000000FF000000FF00000000000000
      00000000FF000000FF000000000000000000000000001873CE00214AAD005A5A
      8C00000000000000000000000000000000001884BD004294BD006B6B6B007B73
      6B006B6B6B0029A5DE0084C6E700007BBD00000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF00000000000000FF000000FF0000000000000000000000FF000000
      00000000FF000000000000000000000000000000000000000000189CFF00006B
      D6002152B50063528C005A8CB500398CB500947B7300BD947B00BDAD8C00C6B5
      9400AD8C7300948473006BA5BD00007BBD0000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000BDBDBD0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000039B5
      F7001894F700106BD6006B84A500AD948C00E7C69400FFFFC600FFFFE700FFFF
      F70000000000C6A58C00846B5A00007BBD00000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD00000000000000
      000000000000000000000000000000000000000000000000000018BDD60029B5
      F70052CEF7005ACEF70084B5BD00CE947300FFFFB500FFFFC600FFFFDE000000
      0000FFFFF700FFFFDE00A5846300526B6B0000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000089CCE0031C6
      F70042DEF70052EFF7009CC6C600EFCE9C00FFEFA500FFF7B500FFFFD600FFFF
      E700FFFFDE00FFFFD600BD9C73006B6B6300000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD00000000000000000000000000000000000000000018BDD6005AD6EF0039BD
      F7004AE7F70052F7F70084C6BD00DED6AD00FFE79C00FFE79C00FFF7B500FFFF
      CE00FFFFC600FFFFBD00CEA57300736B630000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      00000000000000000000000000000000000000000000089CCE009CEFF70039BD
      F7005AE7F70073F7F7007BDED600C6BD9C00FFF7B500FFE7AD00FFE7A500FFE7
      A500FFDE9400FFF7AD00AD8463004A6B73000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD000000000000000000000000000000000000000000A5DEE700BDEFF70042BD
      F70084E7F700A5F7F700A5EFEF00C6B5AD00EFD6B50000000000FFEFD600FFF7
      B500FFFFB500DEB58400847B7B00007BBD0000000000000000000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      0000BDBDBD0000000000000000000000000000000000E7EFEF00C6E7EF006BC6
      EF00ADE7EF00D6EFEF00CEEFEF007BBDCE0094ADB500D6BDBD00DED6C600DECE
      A500BD9C7B009C8C84004AC6DE00007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084000000840000000000000000000000000000000000000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD0000000000BDBD
      BD000000000000000000000000000000000000000000089CCE00089CCE00089C
      CE00089CCE00089CCE00089CCE0018BDD60063E7E70084DEDE007BB5AD007BAD
      AD006BD6D60052EFF70052EFF700007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C60042DEE70063F7F70063F7F70063F7F7005AEFEF006BEFEF006BEFEF0052F7
      F70052F7F70052F7F7005AF7F700007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000000000007B7B7B00BDBDBD0000000000BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C60042C6DE006BD6E7007BD6E70073CEDE000084C6000084C60073CEDE008CF7
      F70073F7F70084F7F7009CF7F700007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C6000084C6000084C60000000000000000000084C60073CE
      DE0073D6DE008CCEE7007BC6DE00007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C6000084C6000084C6000084C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD9C7B006B4A18006B4A
      18006B4A18006B4A18006B4A18006B4A1800AD9C7B0000000000000000000000
      00000000000000000000A57352001808000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B4A1800E7EFEF00D6DE
      E700C6CED600B5C6CE00B5C6CE00ADBDC6006B4A180000000000000000000000
      0000BD8C6B00A5735200DEEFEF00211000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B0000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B000000000084848400848484008484
      840084848400848484008484840084848400848484008484840000000000C6C6
      C600C6C6C60000000000C6C6C60000000000000000006B4A1800F7F7FF00E7EF
      EF0000000000C6CED600B5C6CE00ADBDC6006B4A1800000000008C6B52009463
      4A00D6E7E700DEEFEF00DEEFEF002910000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      00000000000000FFFF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B0000000000A57300007B5A00007B5A
      00004A4A4A004A4A4A00848484008484840084848400FFFFFF00FFFFFF00FFFF
      FF007B5A00007B5A0000A573000000000000000000006B4A1800000000000000
      0000E7EFEF0000000000C6CED600B5C6CE006B4A18005A392900ADB5B5008C94
      9C00DEE7EF00DEEFEF00DEEFEF0031180000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000000000CED6
      D6000000000000000000000000007B7B7B0000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      00000000000000FFFF00000000007B7B7B000000000000000000000000000000
      0000A5730000A57300004A4A4A004A4A4A0084848400FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000006B4A1800000000000000
      0000F7F7FF00E7EFEF0000000000C6CED6006B4A18008C949400B5BDBD008C9C
      9C00DEEFEF00DEEFEF00DEEFEF003918000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF00CED6D60000000000000000007B7B7B007B7B7B000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000000000CED6
      D6000000000000000000000000007B7B7B000000000000000000000000000000
      0000A5730000A5730000A57300004A4A4A00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000006B4A1800000000000000
      000000000000F7F7FF00E7EFEF00D6DEE7006B4A1800A5ADAD00C6CECE008C9C
      A500DEEFEF00DEEFEF00DEEFEF004A210000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000000000
      00000000000000000000000000007B7B7B0000007B00000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF00CED6D60000000000000000007B7B7B000000000000000000000000000000
      0000A5730000A5730000A57300004A4A4A00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000006B4A1800000000000000
      00000000000000000000F7F7FF00E7EFEF006B4A1800C6CECE00D6DEDE0094A5
      A500E7EFF700A5735200A57352004A2100000000000000000000000000007B7B
      7B0000FFFF000000000000FFFF000000000000FFFF0000FFFF000000000000FF
      FF000000000000FFFF00000000007B7B7B0000007B0000007B0000FFFF000000
      000000FFFF00000000007B7B7B0000007B0000FFFF0000000000000000000000
      00000000000000000000000000007B7B7B000000000000000000000000000000
      0000A5730000A5730000A57300004A4A4A00FFFFDE00FFFF8400F7EF7300FFFF
      00000000000000000000000000000000000000000000AD9C7B006B4A18006B4A
      18006B4A18006B4A18006B4A18006B4A1800AD9C7B00E7E7E700E7EFEF009C6B
      5200A5735200C6732100C66B1800522100007B7B7B0000FFFF0000FFFF007B7B
      7B000000000000FFFF007B7B7B0000FFFF00000000000000000000FFFF000000
      000000FFFF0000000000000000007B7B7B007B7B7B0000007B007B7B7B0000FF
      FF00000000007B7B7B0000007B007B7B7B000000000000FFFF000000000000FF
      FF000000000000FFFF00000000007B7B7B000000000000000000000000000000
      0000A5730000A5730000A57300004A4A4A00FFFF8400FFFF8400FFFF8400FFFF
      8400000000000000000000000000000000000000000000000000000000000000
      000052210000F7F7F700F7F7F70094A5A500F7F7F7009C6B5200A56B5200D684
      3100D67B290000000000C66B21005A290000000000007B7B7B00000000007B7B
      7B0000FFFF007B7B7B0000FFFF000000000000FFFF0000FFFF000000000000FF
      FF000000000000FFFF00000000007B7B7B000000000000007B0000007B007B7B
      7B0000FFFF0000007B0000007B000000000000FFFF000000000000FFFF000000
      000000FFFF0000000000000000007B7B7B000000000000000000000000000000
      0000A5730000A5730000A57300004A4A4A00FFFF0000FFFF8400FFFF8400FFFF
      DE00000000000000000000000000000000000000000000000000000000000000
      0000522100000000000000000000A5735200A5735200EF944200E78C39000000
      0000D6843100D6BD9400D6B58C00632900007B7B7B007B7B7B007B7B7B000000
      00007B7B7B000000000000FFFF000000000000FFFF000000000000FFFF000000
      000000FFFF0000000000000000007B7B7B00000000007B7B7B0000007B000000
      7B0000007B0000007B000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF00000000007B7B7B000000000000000000000000000000
      00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000A5735200A5735200EF944200EF944200EF944200EF944200E7CE
      AD00DEC6A500D6BD9C00BD735200D68C6B000000000000FFFF007B7B7B0000FF
      FF00000000007B7B7B007B7B7B007B7B7B007B7B7B0000000000000000000000
      000000000000000000000000000000000000000000007B7B7B0000007B000000
      7B0000007B000000000000FFFF000000000000FFFF000000000000FFFF000000
      000000FFFF0000000000000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EF944200EF944200EF944200EF944200EFDECE00EFDEBD00E7D6
      B500BD735200D68C6B000000000000000000000000007B7B7B0000FFFF007B7B
      7B0000FFFF007B7B7B0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0000007B0000007B000000
      7B0000007B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B6B6B004A4A4A004A4A4A004A4A4A004A4A4A006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EF944200EF944200EFE7CE00EFE7CE00EFE7CE00BD735200D68C
      6B00000000000000000000000000000000007B7B7B0000FFFF00000000007B7B
      7B0000000000000000007B7B7B0000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000007B0000007B007B7B7B000000
      000000007B0000007B007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B6B6B004A4A4A004A4A4A004A4A4A004A4A4A006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EFE7CE00EFE7CE00EFE7CE00BD735200D68C6B00000000000000
      00000000000000000000000000000000000000FFFF0000000000000000007B7B
      7B0000FFFF0000000000000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000007B0000007B007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EFE7CE00BD735200D68C6B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000007B0000007B007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBDA500BD73520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF080800FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF080800FF080800FF080800FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF080800FF080800FF080800FF080800FF080800FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000000000000084000000840000008400000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF080800FF080800FF080800FF848400FF080800FF080800FF080800FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000000000000000000000000000840000008400000084
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF080800FF080800FF848400FF848400FF848400FF080800FF080800FF08
      0800FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000000000000000000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF080800FF848400FF848400FF848400FF848400FF848400FF080800FF08
      0800FF080800FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF08
      0800FF080800FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF080800FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000FF848400FF84
      8400FF848400FF848400FF848400FF848400FF848400FF848400FF848400FF84
      8400FF848400FF84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF00FFFF800780000000FF3F0003C0010000
      FC0F4001E0030000F0037811F0070000A0038001E00F0000C0010081C01F0000
      A0037911C00F0000C00140C3C00F0000A00340EBC00F0000C00141EDC0070000
      A003404DC0070000C0014029E0070000C00F700580070000F03F800080070000
      FCFFC000E00F0000FFFFFF81F81F0000FFFFFFFFFFFF83FFF801C003C003C1FF
      F801C003C003E0FFF801C003C003F07FF801C003C003F83FF801C003C003FC1F
      F801C003C003800FFA01C003C0038007E101C003C003C003E1E1C003C003E007
      8063C003C003F07F8007C003C003F83F807FC003C003FC1FE1FFC003C003FE0F
      E1FFC003C003FF07FFFFFFFFFFFFFF83FE00FFFFFFFFBFFFFC00FFFFF0010007
      F800F001B0018007F000F0019001E003E000F0018001E001C000F0018001E001
      8000F0018001E0010000F001C001E0010000F401C001E0030000F601E061E007
      0000F7E1E063E007E0008023F007E007E000803FF81FE00FF0C0803FF81FE01F
      FFE1FFFFFC3FE03FFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FF0800FFFFFC0038F00
      954F800FC003C000AAAF954FE047E008954FAAAFF09FC010AAAF954FF90FC000
      954FAAAFF8AF8000AAAF954FF14F8000800FAAAFE0A78040CAF3800FE1538000
      E1F3CAFFC0A78000FFC0E1FFC14FE000FFC0FFC0C0BFE000FFF3FFC0E05FF0C0
      FFF3FFFFE00FFFE1FFFFFFFFF81FFFFFFFFF807CC000FFFFFFFF80708000C000
      80258040AA8880008001A0009544AA88F00FB000AA801544F00FB80095402A80
      F00FBC00A5281440F00F800008D408A8F00FF004A1288154F00FF610155482A8
      F00FF00088018554FFFFF00381FF0001F81FF00F2CFF11FFF81FF03F66FFF8FF
      FFFFF0FFEFFFFC7FFFFFF3FFFFFFFFFFFFFFFFFFFFFFFFFF8001800180018001
      BFFDBFFD80018001BFFDBFFD80018001BFFDBEFD80018001BFFDBC7D80018001
      BFFDB83D80018001BFFDB11D80018001BFFDB38D80018001BFFDB7C580018001
      BFFDBFE580018001BFFDBFF580018001BFFDBFFD80018001BFFDBFFD80018001
      8001800180018001FFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object quSOut3: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "TTNOutLn" ln'
      'left join "TTNOut" hd on '
      
        '(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Nu' +
        'mber=ln.Number)'
      ''
      'where ln.DateInvoice='#39'2010-12-18'#39
      'and ln."Depart"=188'
      'and hd."AcStatus"=3'
      'and hd."ProvodType"=3'
      ''
      'Order by ln.Depart, Ln.DateInvoice, ln.Number')
    Params = <>
    Left = 424
    Top = 760
    object quSOut3Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSOut3DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSOut3Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSOut3CodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quSOut3CodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quSOut3CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSOut3CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quSOut3TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSOut3BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSOut3OKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quSOut3NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quSOut3NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quSOut3OutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quSOut3Akciz: TFloatField
      FieldName = 'Akciz'
    end
    object quSOut3KolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quSOut3KolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quSOut3KolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quSOut3Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSOut3CenaPost: TFloatField
      FieldName = 'CenaPost'
    end
    object quSOut3CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quSOut3CenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quSOut3SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quSOut3SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quSOut3CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quSOut3VesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quSOut3CenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quSOut3CenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
    object quSOut3SumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quSOut3SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quSOut3SumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object quSOut3ChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSOut3Depart_1: TSmallintField
      FieldName = 'Depart_1'
    end
    object quSOut3DateInvoice_1: TDateField
      FieldName = 'DateInvoice_1'
    end
    object quSOut3Number_1: TStringField
      FieldName = 'Number_1'
      Size = 10
    end
    object quSOut3SCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quSOut3IndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quSOut3CodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quSOut3SummaTovar: TFloatField
      FieldName = 'SummaTovar'
    end
    object quSOut3NDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quSOut3OutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object quSOut3NDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quSOut3OutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object quSOut3LgtNDS: TFloatField
      FieldName = 'LgtNDS'
    end
    object quSOut3OutLgtNDS: TFloatField
      FieldName = 'OutLgtNDS'
    end
    object quSOut3NDSTovar: TFloatField
      FieldName = 'NDSTovar'
    end
    object quSOut3OutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object quSOut3NotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
    object quSOut3OutNDSSNTovar: TFloatField
      FieldName = 'OutNDSSNTovar'
    end
    object quSOut3SummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
    end
    object quSOut3NacenkaTovar: TFloatField
      FieldName = 'NacenkaTovar'
    end
    object quSOut3SummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quSOut3SummaTaraSpis: TFloatField
      FieldName = 'SummaTaraSpis'
    end
    object quSOut3NacenkaTara: TFloatField
      FieldName = 'NacenkaTara'
    end
    object quSOut3AcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quSOut3ChekBuh_1: TWordField
      FieldName = 'ChekBuh_1'
    end
    object quSOut3NAvto: TStringField
      FieldName = 'NAvto'
      Size = 10
    end
    object quSOut3NPList: TStringField
      FieldName = 'NPList'
      Size = 10
    end
    object quSOut3FIOVod: TStringField
      FieldName = 'FIOVod'
      Size = 24
    end
    object quSOut3PrnDate: TDateField
      FieldName = 'PrnDate'
    end
    object quSOut3PrnNumber: TStringField
      FieldName = 'PrnNumber'
      Size = 10
    end
    object quSOut3PrnKol: TFloatField
      FieldName = 'PrnKol'
    end
    object quSOut3PrnAkt: TStringField
      FieldName = 'PrnAkt'
      Size = 10
    end
    object quSOut3ProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object quSOut3NotNDS10: TFloatField
      FieldName = 'NotNDS10'
    end
    object quSOut3NotNDS20: TFloatField
      FieldName = 'NotNDS20'
    end
    object quSOut3WithNDS10: TFloatField
      FieldName = 'WithNDS10'
    end
    object quSOut3WithNDS20: TFloatField
      FieldName = 'WithNDS20'
    end
    object quSOut3Crock: TFloatField
      FieldName = 'Crock'
    end
    object quSOut3Discount: TFloatField
      FieldName = 'Discount'
    end
    object quSOut3NDS010: TFloatField
      FieldName = 'NDS010'
    end
    object quSOut3NDS020: TFloatField
      FieldName = 'NDS020'
    end
    object quSOut3NDS_10: TFloatField
      FieldName = 'NDS_10'
    end
    object quSOut3NDS_20: TFloatField
      FieldName = 'NDS_20'
    end
    object quSOut3NSP_10: TFloatField
      FieldName = 'NSP_10'
    end
    object quSOut3NSP_20: TFloatField
      FieldName = 'NSP_20'
    end
    object quSOut3SCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quSOut3GoodsNSP0: TFloatField
      FieldName = 'GoodsNSP0'
    end
    object quSOut3GoodsCostNSP: TFloatField
      FieldName = 'GoodsCostNSP'
    end
    object quSOut3OrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object quSOut3ID: TIntegerField
      FieldName = 'ID'
    end
    object quSOut3LinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quSOut3StartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quSOut3EndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quSOut3REZERV: TStringField
      FieldName = 'REZERV'
      Size = 4
    end
  end
  object quRepPrib: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      'po.ARTICUL,po.IDSTORE,po.ITYPE,po.SINN,'
      
        'gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.' +
        'V03,gd.V04,gd.V05,gd.Cena,gd.NDS,'
      'de.Name as NameDep, de.Status5,'
      'SUM(po.QUANT) as QUANT,'
      'SUM(po.SUMIN) as SUMIN,'
      'SUM(po.SUMOUT) as SUMOUT'
      'from "A_PARTOUT" po'
      'left join "Goods" gd on gd.ID=po.ARTICUL'
      'left join "Depart" de on de.ID=po.IDSTORE'
      'where po.ARTICUL>=0'
      'group by '
      'po.ARTICUL,po.IDSTORE,po.ITYPE, po.SINN,'
      
        'gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.' +
        'V03,gd.V04,gd.V05,gd.Cena,gd.NDS,de.Name,de.Status5'
      'order by po.ARTICUL,po.IDSTORE')
    Params = <>
    Left = 656
    Top = 756
    object quRepPribARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object quRepPribIDSTORE: TSmallintField
      FieldName = 'IDSTORE'
    end
    object quRepPribITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quRepPribName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quRepPribEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quRepPribGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quRepPribSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quRepPribV01: TIntegerField
      FieldName = 'V01'
    end
    object quRepPribV02: TIntegerField
      FieldName = 'V02'
    end
    object quRepPribV03: TIntegerField
      FieldName = 'V03'
    end
    object quRepPribV04: TIntegerField
      FieldName = 'V04'
    end
    object quRepPribV05: TIntegerField
      FieldName = 'V05'
    end
    object quRepPribCena: TFloatField
      FieldName = 'Cena'
    end
    object quRepPribNDS: TFloatField
      FieldName = 'NDS'
    end
    object quRepPribNameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quRepPribQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quRepPribSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quRepPribSUMOUT: TFloatField
      FieldName = 'SUMOUT'
    end
    object quRepPribSINN: TStringField
      FieldName = 'SINN'
    end
    object quRepPribStatus5: TIntegerField
      FieldName = 'Status5'
    end
  end
  object quInvCodeSum: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInvCalcFields
    SQL.Strings = (
      
        'select top 1 invh.DateInventry,invln.SUMFSS,invln.SUMDSS from "I' +
        'nventryHead" invh'
      'left join A_INVLN invln on invln.IDH=invh.Inventry'
      'where '
      'invh.Depart=4 '
      'and invh.DateInventry>='#39'2011-01-01'#39
      'and invh.DateInventry<='#39'2011-04-01'#39
      'and invln.ITEM=30433'
      'order by DateInventry desc')
    Params = <>
    Left = 592
    Top = 396
    object quInvCodeSumDateInventry: TDateField
      FieldName = 'DateInventry'
    end
    object quInvCodeSumSUMFSS: TFloatField
      FieldName = 'SUMFSS'
    end
    object quInvCodeSumSUMDSS: TFloatField
      FieldName = 'SUMDSS'
    end
  end
  object quZadRec: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_ZADANIYA"'
      'where ID=:IDZ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDZ'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 596
    Top = 756
    object quZadRecId: TStringField
      FieldName = 'Id'
    end
    object quZadRecIType: TSmallintField
      FieldName = 'IType'
    end
    object quZadRecComment: TStringField
      FieldName = 'Comment'
      Size = 200
    end
    object quZadRecStatus: TSmallintField
      FieldName = 'Status'
    end
    object quZadRecZDate: TDateField
      FieldName = 'ZDate'
    end
    object quZadRecZTime: TTimeField
      FieldName = 'ZTime'
    end
    object quZadRecPersonId: TIntegerField
      FieldName = 'PersonId'
    end
    object quZadRecPersonName: TStringField
      FieldName = 'PersonName'
      Size = 50
    end
    object quZadRecFormDate: TDateField
      FieldName = 'FormDate'
    end
    object quZadRecFormTime: TTimeField
      FieldName = 'FormTime'
    end
    object quZadRecIdDoc: TStringField
      FieldName = 'IdDoc'
    end
  end
  object quZCQ: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select Cash_Code, NSmena, Operation,Ck_Card, SUM(Summa) as RSUM,' +
        ' SUM(Ck_Disc) as DSUM, Count(*) as CountCh  from "cq201104" '
      'where DateOperation='#39'2011-04-04'#39
      'group by Cash_Code, NSmena, Operation,Ck_Card'
      'order by Cash_Code, NSmena, Operation')
    Params = <>
    Left = 220
    Top = 776
    object quZCQCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quZCQNSmena: TIntegerField
      FieldName = 'NSmena'
    end
    object quZCQOperation: TStringField
      FieldName = 'Operation'
      Size = 1
    end
    object quZCQRSUM: TFloatField
      FieldName = 'RSUM'
    end
    object quZCQDSUM: TFloatField
      FieldName = 'DSUM'
    end
    object quZCQCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quZCQCountCh: TIntegerField
      FieldName = 'CountCh'
    end
  end
  object quAssPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      
        'gc.ClientTypeId,gc.ClientId,gc.GoodsId,gc.PriceIn,gc.QuantIn,gc.' +
        'xDate,gc.sMess,'
      'ca.Name,ca.FullName,ca.EdIzm  '
      'from "A_GDSCLI" gc'
      'left join Goods ca on ca.ID=gc.GoodsId'
      'where '
      'gc.ClientTypeId=:ICLIT and gc.ClientId=:ICLI')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLIT'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 352
    Top = 760
    object quAssPostClientTypeId: TSmallintField
      FieldName = 'ClientTypeId'
    end
    object quAssPostClientId: TIntegerField
      FieldName = 'ClientId'
    end
    object quAssPostGoodsId: TIntegerField
      FieldName = 'GoodsId'
    end
    object quAssPostPriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object quAssPostQuantIn: TFloatField
      FieldName = 'QuantIn'
    end
    object quAssPostxDate: TDateField
      FieldName = 'xDate'
    end
    object quAssPostsMess: TStringField
      FieldName = 'sMess'
      Size = 50
    end
    object quAssPostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quAssPostFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quAssPostEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
  end
  object quPost6: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quPost6CalcFields
    SQL.Strings = (
      'select CaCli.GoodsId,'
      'CaCli.ClientTypeId,'
      'CaCli.ClientId, '
      'v."Name" as NamePost,'
      'i."Name" as NameInd'
      'from "A_GDSCLI" CaCli'
      'left outer join "RVendor" v on v.Vendor=CaCli.ClientId'
      'left outer join "RIndividual" i on i.Code=CaCli.ClientId'
      'where GoodsId=:IDC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDC'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 1080
    Top = 664
    object quPost6GoodsId: TIntegerField
      FieldName = 'GoodsId'
    end
    object quPost6ClientTypeId: TSmallintField
      FieldName = 'ClientTypeId'
    end
    object quPost6ClientId: TIntegerField
      FieldName = 'ClientId'
    end
    object quPost6NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost6NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost6NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
  end
  object dsquPost6: TDataSource
    DataSet = quPost6
    Left = 1080
    Top = 720
  end
  object quPost7: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quPost7CalcFields
    SQL.Strings = (
      'select top 10'
      'tl.Depart,'
      'de.Name,'
      'tl.DateInvoice,'
      'tl.Number,'
      'th.IndexPoluch,'
      'th.CodePoluch,'
      'v."Name" as NamePost,'
      'i."Name" as NameInd,'
      'tl.CodeTovar,'
      'tl.CodeEdIzm,'
      'tl.BarCode,'
      'tl.Kol,'
      'tl.CenaTovar,'
      'tl.CenaTovarSpis,'
      'tl.SumCenaTovarSpis,'
      'tl.SumCenaTovar,                    '
      'tl.NDSProc,'
      'tl.NDSSum,'
      'tl.CenaTaraSpis'
      ''
      'from "TTNOutLn" tl'
      ''
      
        'left join TTNOut th on (th.Depart=tl.Depart and th.Number=tl.Num' +
        'ber and th.DateInvoice=tl.DateInvoice)'
      'left join Depart de on de.ID=tl.Depart'
      'left outer join RVendor v on v.Vendor=th."CodePoluch"'
      'left outer join RIndividual i on i.Code=th."CodePoluch"'
      ' '
      'where th.ProvodType=3'
      'and th.AcStatus=3'
      'and tl.CodeTovar=:IDCARD'
      'and th.IndexPoluch=:INDP'
      'and th.CodePoluch=:CODP'
      'order by tl.DateInvoice DESC'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'INDP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'CODP'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 912
    Top = 788
    object quPost7Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost7Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quPost7DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost7Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quPost7IndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quPost7CodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quPost7NamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quPost7NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost7CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quPost7CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost7BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quPost7Kol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.000'
    end
    object quPost7CenaTovar: TFloatField
      FieldName = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost7CenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
      DisplayFormat = '0.00'
    end
    object quPost7SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
      DisplayFormat = '0.00'
    end
    object quPost7SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object quPost7NDSProc: TFloatField
      FieldName = 'NDSProc'
      DisplayFormat = '0.00'
    end
    object quPost7NDSSum: TFloatField
      FieldName = 'NDSSum'
      DisplayFormat = '0.00'
    end
    object quPost7NameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 150
      Calculated = True
    end
    object quPost7CenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
  end
  object dsquPost7: TDataSource
    DataSet = quPost7
    Left = 964
    Top = 796
  end
  object quFindCBar: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods".' +
        '"BarCode","Goods"."TovarType",'
      
        '"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status",' +
        ' "Goods"."V02"'
      'FROM "Goods"'
      'where "Goods"."BarCode" = :SBAR ')
    Params = <
      item
        DataType = ftString
        Name = 'SBAR'
        ParamType = ptUnknown
        Value = ' '
      end>
    LoadBlobOnOpen = False
    Left = 980
    Top = 224
    object quFindCBarID: TIntegerField
      FieldName = 'ID'
    end
    object quFindCBarName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quFindCBarFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quFindCBarBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFindCBarTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quFindCBarEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFindCBarNDS: TFloatField
      FieldName = 'NDS'
    end
    object quFindCBarCena: TFloatField
      FieldName = 'Cena'
    end
    object quFindCBarStatus: TSmallintField
      FieldName = 'Status'
    end
    object quFindCBarV02: TIntegerField
      FieldName = 'V02'
    end
  end
  object quPOut: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 1 * from "A_PARTOUT"'
      'where IDATE=40670'
      'and IDSTORE=4'
      'and ITYPE=7'
      'and SUMIN>1000')
    Params = <>
    Left = 268
    Top = 768
    object quPOutARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object quPOutIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quPOutIDSTORE: TSmallintField
      FieldName = 'IDSTORE'
    end
    object quPOutITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quPOutIDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object quPOutPARTIN: TIntegerField
      FieldName = 'PARTIN'
    end
    object quPOutID: TAutoIncField
      FieldName = 'ID'
    end
    object quPOutQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quPOutSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quPOutSUMOUT: TFloatField
      FieldName = 'SUMOUT'
    end
    object quPOutSINN: TStringField
      FieldName = 'SINN'
    end
  end
  object quCQZDay: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'Quant,Operation,DateOperation,Price,Store,Ck_Number,Ck_Curs,'
      'Ck_CurAbr,Ck_Disg,Ck_Disc,Quant_S,GrCode,Code,Cassir,Cash_Code,'
      'Ck_Card,Contr_Code,Contr_Cost,Seria,BestB,NDSx1,'
      'NDSx2,Times,Summa,SumNDS,SumNSP,PriceNSP,NSmena'
      'from "cq201108"'
      'where '
      'DateOperation='#39'2011-08-15'#39
      'and Cash_Code=1'
      'and Nsmena=875'
      '')
    Params = <>
    Left = 420
    Top = 640
    object quCQZDayQuant: TFloatField
      FieldName = 'Quant'
    end
    object quCQZDayOperation: TStringField
      FieldName = 'Operation'
      Size = 1
    end
    object quCQZDayDateOperation: TDateField
      FieldName = 'DateOperation'
    end
    object quCQZDayPrice: TFloatField
      FieldName = 'Price'
    end
    object quCQZDayStore: TStringField
      FieldName = 'Store'
      Size = 6
    end
    object quCQZDayCk_Number: TIntegerField
      FieldName = 'Ck_Number'
    end
    object quCQZDayCk_Curs: TFloatField
      FieldName = 'Ck_Curs'
    end
    object quCQZDayCk_CurAbr: TIntegerField
      FieldName = 'Ck_CurAbr'
    end
    object quCQZDayCk_Disg: TFloatField
      FieldName = 'Ck_Disg'
    end
    object quCQZDayCk_Disc: TFloatField
      FieldName = 'Ck_Disc'
    end
    object quCQZDayQuant_S: TFloatField
      FieldName = 'Quant_S'
    end
    object quCQZDayGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quCQZDayCode: TIntegerField
      FieldName = 'Code'
    end
    object quCQZDayCassir: TStringField
      FieldName = 'Cassir'
      Size = 10
    end
    object quCQZDayCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quCQZDayCk_Card: TIntegerField
      FieldName = 'Ck_Card'
    end
    object quCQZDayContr_Code: TStringField
      FieldName = 'Contr_Code'
      Size = 7
    end
    object quCQZDayContr_Cost: TFloatField
      FieldName = 'Contr_Cost'
    end
    object quCQZDaySeria: TStringField
      FieldName = 'Seria'
      Size = 10
    end
    object quCQZDayBestB: TStringField
      FieldName = 'BestB'
      Size = 10
    end
    object quCQZDayNDSx1: TFloatField
      FieldName = 'NDSx1'
    end
    object quCQZDayNDSx2: TFloatField
      FieldName = 'NDSx2'
    end
    object quCQZDayTimes: TTimeField
      FieldName = 'Times'
    end
    object quCQZDaySumma: TFloatField
      FieldName = 'Summa'
    end
    object quCQZDaySumNDS: TFloatField
      FieldName = 'SumNDS'
    end
    object quCQZDaySumNSP: TFloatField
      FieldName = 'SumNSP'
    end
    object quCQZDayPriceNSP: TFloatField
      FieldName = 'PriceNSP'
    end
    object quCQZDayNSmena: TIntegerField
      FieldName = 'NSmena'
    end
  end
  object quCloseHBuh: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 1 * from "A_CLOSEHISTMAIN"'
      'order by ID desc')
    Params = <>
    Left = 116
    Top = 756
    object quCloseHBuhID: TIntegerField
      FieldName = 'ID'
    end
    object quCloseHBuhDateClose: TDateField
      FieldName = 'DateClose'
    end
    object quCloseHBuhIdPers: TIntegerField
      FieldName = 'IdPers'
    end
    object quCloseHBuhDateEdit: TDateTimeField
      FieldName = 'DateEdit'
    end
  end
  object quRepCassir: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      
        'select GrCode,Cassir,Cash_Code,DateOperation,Count(*) as CountPo' +
        's,SUM(Summa) as Summa from "cq201112"'
      
        'where DateOperation>='#39'2011-12-01'#39' and DateOperation<='#39'2011-12-31' +
        #39
      'group by GrCode,Cassir,Cash_Code,DateOperation')
    Params = <>
    Left = 656
    Top = 816
    object quRepCassirGrCode: TSmallintField
      FieldName = 'GrCode'
    end
    object quRepCassirCassir: TStringField
      FieldName = 'Cassir'
      Size = 10
    end
    object quRepCassirCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quRepCassirCountPos: TIntegerField
      FieldName = 'CountPos'
    end
    object quRepCassirSumma: TFloatField
      FieldName = 'Summa'
    end
    object quRepCassirDateOperation: TDateField
      FieldName = 'DateOperation'
    end
  end
  object quZakHPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'dh.ID,'
      'dh.DOCDATE,'
      'dh.DOCNUM,'
      'dh.DEPART,'
      'dh.ITYPE,'
      'dh.INSUMIN,'
      'dh.INSUMOUT,'
      'dh.OUTSUMIN,'
      'dh.OUTSUMOUT,'
      'dh.CLITYPE,'
      'dh.CLICODE,'
      'dh.IACTIVE,'
      'dh.DATEEDIT,'
      'dh.TIMEEDIT,'
      'dh.PERSON,'
      'dh.CLIQUANT,'
      'dh.CLISUMIN0,'
      'dh.CLISUMIN,'
      'dh.CLIQUANTN,'
      'dh.CLISUMIN0N,'
      'dh.CLISUMINN,'
      'dh.IDATENACL,'
      'dh.SNUMNACL,'
      'dh.IDATESCHF,'
      'dh.SNUMSCHF,'
      'de.Name                                          '
      'from "A_DOCZHEAD" dh'
      'left join "Depart" de on de.Id=dh.DEPART'
      'where IDATENACL>=:IDATEB'
      'and  IDATENACL<=:IDATEE'
      'and CLITYPE=1 '
      'and CLICODE=:ICLI')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 800
    Top = 816
    object quZakHPostID: TAutoIncField
      FieldName = 'ID'
    end
    object quZakHPostDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object quZakHPostDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quZakHPostDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quZakHPostITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quZakHPostINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object quZakHPostINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
    end
    object quZakHPostOUTSUMIN: TFloatField
      FieldName = 'OUTSUMIN'
    end
    object quZakHPostOUTSUMOUT: TFloatField
      FieldName = 'OUTSUMOUT'
    end
    object quZakHPostCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quZakHPostCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quZakHPostIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quZakHPostDATEEDIT: TDateField
      FieldName = 'DATEEDIT'
    end
    object quZakHPostTIMEEDIT: TStringField
      FieldName = 'TIMEEDIT'
      Size = 10
    end
    object quZakHPostPERSON: TStringField
      FieldName = 'PERSON'
      Size = 100
    end
    object quZakHPostCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quZakHPostCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object quZakHPostCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object quZakHPostCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
      DisplayFormat = '0.000'
    end
    object quZakHPostCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
      DisplayFormat = '0.00'
    end
    object quZakHPostCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
      DisplayFormat = '0.00'
    end
    object quZakHPostIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quZakHPostSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quZakHPostIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quZakHPostSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
    object quZakHPostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquZakHPost: TDataSource
    DataSet = quZakHPost
    Left = 848
    Top = 828
  end
  object quZakHNum: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quZakHCalcFields
    SQL.Strings = (
      'select '
      'dh.ID,'
      'dh.DOCDATE,'
      'dh.DOCNUM,'
      'dh.DEPART,'
      'dh.ITYPE,'
      'dh.INSUMIN,'
      'dh.INSUMOUT,'
      'dh.OUTSUMIN,'
      'dh.OUTSUMOUT,'
      'dh.CLITYPE,'
      'dh.CLICODE,'
      'dh.IACTIVE,'
      'dh.DATEEDIT,'
      'dh.TIMEEDIT,'
      'dh.PERSON,'
      'dh.CLIQUANT,'
      'dh.CLISUMIN0,'
      'dh.CLISUMIN,'
      'dh.CLIQUANTN,'
      'dh.CLISUMIN0N,'
      'dh.CLISUMINN,'
      'dh.IDATENACL,'
      'dh.SNUMNACL,'
      'dh.IDATESCHF,'
      'dh.SNUMSCHF'
      'from "A_DOCZHEAD" dh'
      'where dh.DOCNUM=:SNUM'
      'and CLITYPE=1 '
      'and CLICODE=:ICLI'
      '')
    Params = <
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 748
    Top = 844
    object quZakHNumID: TAutoIncField
      FieldName = 'ID'
    end
    object quZakHNumDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object quZakHNumDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quZakHNumDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quZakHNumITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quZakHNumINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object quZakHNumINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
    end
    object quZakHNumOUTSUMIN: TFloatField
      FieldName = 'OUTSUMIN'
    end
    object quZakHNumOUTSUMOUT: TFloatField
      FieldName = 'OUTSUMOUT'
    end
    object quZakHNumCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quZakHNumCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quZakHNumIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quZakHNumDATEEDIT: TDateField
      FieldName = 'DATEEDIT'
    end
    object quZakHNumTIMEEDIT: TStringField
      FieldName = 'TIMEEDIT'
      Size = 10
    end
    object quZakHNumPERSON: TStringField
      FieldName = 'PERSON'
      Size = 100
    end
    object quZakHNumCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quZakHNumCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object quZakHNumCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object quZakHNumCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quZakHNumCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
    end
    object quZakHNumCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
    end
    object quZakHNumIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quZakHNumSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quZakHNumIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quZakHNumSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
  end
  object quGrafPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quGrafPostCalcFields
    SQL.Strings = (
      
        'select  gp.ICLI,gp.IDEP,gp.IDATEP,gp.IDATEZ,post.FullName,post.N' +
        'ame'
      'from "A_GRAFPOST" gp'
      'left join "RVendor" post on post.Status=gp.ICLI'
      'where gp.IDATEP>=:IDATEB'
      'and gp.IDATEP<=:IDATEE')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 24
    Top = 828
    object quGrafPostICLI: TIntegerField
      FieldName = 'ICLI'
    end
    object quGrafPostIDEP: TIntegerField
      FieldName = 'IDEP'
    end
    object quGrafPostIDATEP: TIntegerField
      FieldName = 'IDATEP'
    end
    object quGrafPostIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
    end
    object quGrafPostFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quGrafPostDDATEP: TDateField
      FieldKind = fkCalculated
      FieldName = 'DDATEP'
      Calculated = True
    end
    object quGrafPostDDATEZ: TDateField
      FieldKind = fkCalculated
      FieldName = 'DDATEZ'
      Calculated = True
    end
    object quGrafPostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquGrafPost: TDataSource
    DataSet = quGrafPost
    Left = 96
    Top = 828
  end
  object quSOutH3: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "TTNOut" '
      'where DateInvoice='#39'2010-12-18'#39
      'and "Depart"=182'
      'and "AcStatus"=3'
      'and "ProvodType"=3'
      ''
      'Order by Depart, DateInvoice, Number')
    Params = <>
    Left = 424
    Top = 816
    object quSOutH3Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quSOutH3DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quSOutH3Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quSOutH3SCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quSOutH3IndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quSOutH3CodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quSOutH3SummaTovar: TFloatField
      FieldName = 'SummaTovar'
    end
    object quSOutH3NDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quSOutH3OutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object quSOutH3NDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quSOutH3OutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object quSOutH3LgtNDS: TFloatField
      FieldName = 'LgtNDS'
    end
    object quSOutH3OutLgtNDS: TFloatField
      FieldName = 'OutLgtNDS'
    end
    object quSOutH3NDSTovar: TFloatField
      FieldName = 'NDSTovar'
    end
    object quSOutH3OutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object quSOutH3NotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
    object quSOutH3OutNDSSNTovar: TFloatField
      FieldName = 'OutNDSSNTovar'
    end
    object quSOutH3SummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
    end
    object quSOutH3NacenkaTovar: TFloatField
      FieldName = 'NacenkaTovar'
    end
    object quSOutH3SummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quSOutH3SummaTaraSpis: TFloatField
      FieldName = 'SummaTaraSpis'
    end
    object quSOutH3NacenkaTara: TFloatField
      FieldName = 'NacenkaTara'
    end
    object quSOutH3AcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quSOutH3ChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quSOutH3NAvto: TStringField
      FieldName = 'NAvto'
      Size = 10
    end
    object quSOutH3NPList: TStringField
      FieldName = 'NPList'
      Size = 10
    end
    object quSOutH3FIOVod: TStringField
      FieldName = 'FIOVod'
      Size = 24
    end
    object quSOutH3PrnDate: TDateField
      FieldName = 'PrnDate'
    end
    object quSOutH3PrnNumber: TStringField
      FieldName = 'PrnNumber'
      Size = 10
    end
    object quSOutH3PrnKol: TFloatField
      FieldName = 'PrnKol'
    end
    object quSOutH3PrnAkt: TStringField
      FieldName = 'PrnAkt'
      Size = 10
    end
    object quSOutH3ProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object quSOutH3NotNDS10: TFloatField
      FieldName = 'NotNDS10'
    end
    object quSOutH3NotNDS20: TFloatField
      FieldName = 'NotNDS20'
    end
    object quSOutH3WithNDS10: TFloatField
      FieldName = 'WithNDS10'
    end
    object quSOutH3WithNDS20: TFloatField
      FieldName = 'WithNDS20'
    end
    object quSOutH3Crock: TFloatField
      FieldName = 'Crock'
    end
    object quSOutH3Discount: TFloatField
      FieldName = 'Discount'
    end
    object quSOutH3NDS010: TFloatField
      FieldName = 'NDS010'
    end
    object quSOutH3NDS020: TFloatField
      FieldName = 'NDS020'
    end
    object quSOutH3NDS_10: TFloatField
      FieldName = 'NDS_10'
    end
    object quSOutH3NDS_20: TFloatField
      FieldName = 'NDS_20'
    end
    object quSOutH3NSP_10: TFloatField
      FieldName = 'NSP_10'
    end
    object quSOutH3NSP_20: TFloatField
      FieldName = 'NSP_20'
    end
    object quSOutH3SCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quSOutH3GoodsNSP0: TFloatField
      FieldName = 'GoodsNSP0'
    end
    object quSOutH3GoodsCostNSP: TFloatField
      FieldName = 'GoodsCostNSP'
    end
    object quSOutH3OrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object quSOutH3ID: TIntegerField
      FieldName = 'ID'
    end
    object quSOutH3LinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quSOutH3StartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quSOutH3EndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quSOutH3REZERV: TStringField
      FieldName = 'REZERV'
      Size = 4
    end
  end
  object quTestInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInvCalcFields
    SQL.Strings = (
      'select * from "InventryHead"'
      'where DateInventry>='#39'2012-06-20'#39
      'and DateInventry<='#39'2012-06-25'#39
      'and Number like '#39'Auto%'#39
      'and Status='#39#1063#39
      'Order by DateInventry')
    Params = <>
    Left = 116
    Top = 630
    object quTestInvInventry: TIntegerField
      FieldName = 'Inventry'
    end
    object quTestInvDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTestInvDateInventry: TDateField
      FieldName = 'DateInventry'
    end
    object quTestInvNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTestInvCardIndex: TWordField
      FieldName = 'CardIndex'
    end
    object quTestInvInputMode: TWordField
      FieldName = 'InputMode'
    end
    object quTestInvPriceMode: TWordField
      FieldName = 'PriceMode'
    end
    object quTestInvDetail: TWordField
      FieldName = 'Detail'
    end
    object quTestInvGGroup: TSmallintField
      FieldName = 'GGroup'
    end
    object quTestInvSubGroup: TSmallintField
      FieldName = 'SubGroup'
    end
    object quTestInvItem: TIntegerField
      FieldName = 'Item'
    end
    object quTestInvCountLost: TBooleanField
      FieldName = 'CountLost'
    end
    object quTestInvDateLost: TDateField
      FieldName = 'DateLost'
    end
    object quTestInvQuantRecord: TFloatField
      FieldName = 'QuantRecord'
    end
    object quTestInvSumRecord: TFloatField
      FieldName = 'SumRecord'
    end
    object quTestInvQuantActual: TFloatField
      FieldName = 'QuantActual'
    end
    object quTestInvSumActual: TFloatField
      FieldName = 'SumActual'
    end
    object quTestInvNumberOfLines: TIntegerField
      FieldName = 'NumberOfLines'
    end
    object quTestInvDateStart: TDateField
      FieldName = 'DateStart'
    end
    object quTestInvTimeStart: TTimeField
      FieldName = 'TimeStart'
    end
    object quTestInvPresident: TStringField
      FieldName = 'President'
    end
    object quTestInvPresidentPost: TStringField
      FieldName = 'PresidentPost'
    end
    object quTestInvMember1: TStringField
      FieldName = 'Member1'
    end
    object quTestInvMember1_Post: TStringField
      FieldName = 'Member1_Post'
    end
    object quTestInvMember2: TStringField
      FieldName = 'Member2'
    end
    object quTestInvMember2_Post: TStringField
      FieldName = 'Member2_Post'
    end
    object quTestInvMember3: TStringField
      FieldName = 'Member3'
    end
    object quTestInvMember3_Post: TStringField
      FieldName = 'Member3_Post'
      Size = 19
    end
    object quTestInvxDopStatus: TWordField
      FieldName = 'xDopStatus'
    end
    object quTestInvResponse1: TStringField
      FieldName = 'Response1'
    end
    object quTestInvResponse1_Post: TStringField
      FieldName = 'Response1_Post'
    end
    object quTestInvResponse2: TStringField
      FieldName = 'Response2'
    end
    object quTestInvResponse2_Post: TStringField
      FieldName = 'Response2_Post'
    end
    object quTestInvResponse3: TStringField
      FieldName = 'Response3'
    end
    object quTestInvResponse3_Post: TStringField
      FieldName = 'Response3_Post'
    end
    object quTestInvReason: TStringField
      FieldName = 'Reason'
      Size = 49
    end
    object quTestInvCASHER: TStringField
      FieldName = 'CASHER'
      Size = 10
    end
    object quTestInvStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
    object quTestInvLinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quTestInvStartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quTestInvEndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quTestInvRezerv: TStringField
      FieldName = 'Rezerv'
      Size = 27
    end
  end
  object quTestZadInv: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInvCalcFields
    SQL.Strings = (
      'select * from "A_ZADANIYA"'
      'where IType=2 and IdDoc=:IID')
    Params = <
      item
        DataType = ftInteger
        Name = 'IID'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 180
    Top = 630
    object quTestZadInvId: TStringField
      FieldName = 'Id'
      Size = 19
    end
    object quTestZadInvIType: TSmallintField
      FieldName = 'IType'
    end
    object quTestZadInvComment: TStringField
      FieldName = 'Comment'
      Size = 200
    end
    object quTestZadInvStatus: TSmallintField
      FieldName = 'Status'
    end
    object quTestZadInvZDate: TDateField
      FieldName = 'ZDate'
    end
    object quTestZadInvZTime: TTimeField
      FieldName = 'ZTime'
    end
    object quTestZadInvPersonId: TIntegerField
      FieldName = 'PersonId'
    end
    object quTestZadInvPersonName: TStringField
      FieldName = 'PersonName'
      Size = 50
    end
    object quTestZadInvFormDate: TDateField
      FieldName = 'FormDate'
    end
    object quTestZadInvFormTime: TTimeField
      FieldName = 'FormTime'
    end
    object quTestZadInvIdDoc: TStringField
      FieldName = 'IdDoc'
      Size = 19
    end
  end
  object quFindCli: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "RVendor"'
      'order by Name asc')
    Params = <>
    LoadBlobOnOpen = False
    Left = 848
    Top = 172
    object quFindCliVendor: TSmallintField
      FieldName = 'Vendor'
      Required = True
    end
    object quFindCliName: TStringField
      FieldName = 'Name'
      Required = True
      Size = 30
    end
    object quFindCliPayDays: TSmallintField
      FieldName = 'PayDays'
      Required = True
    end
    object quFindCliPayForm: TWordField
      FieldName = 'PayForm'
      Required = True
    end
    object quFindCliPayWaste: TWordField
      FieldName = 'PayWaste'
      Required = True
    end
    object quFindCliFirmName: TStringField
      FieldName = 'FirmName'
      Required = True
      Size = 6
    end
    object quFindCliPhone1: TStringField
      FieldName = 'Phone1'
      Required = True
      Size = 16
    end
    object quFindCliPhone2: TStringField
      FieldName = 'Phone2'
      Required = True
      Size = 16
    end
    object quFindCliPhone3: TStringField
      FieldName = 'Phone3'
      Required = True
      Size = 16
    end
    object quFindCliPhone4: TStringField
      FieldName = 'Phone4'
      Required = True
      Size = 16
    end
    object quFindCliFax: TStringField
      FieldName = 'Fax'
      Required = True
      Size = 16
    end
    object quFindCliTreatyDate: TDateField
      FieldName = 'TreatyDate'
      Required = True
    end
    object quFindCliTreatyNumber: TStringField
      FieldName = 'TreatyNumber'
      Required = True
      Size = 16
    end
    object quFindCliPostIndex: TStringField
      FieldName = 'PostIndex'
      Required = True
      Size = 6
    end
    object quFindCliGorod: TStringField
      FieldName = 'Gorod'
      Required = True
      Size = 30
    end
    object quFindCliRaion: TSmallintField
      FieldName = 'Raion'
      Required = True
    end
    object quFindCliStreet: TStringField
      FieldName = 'Street'
      Required = True
      Size = 30
    end
    object quFindCliHouse: TStringField
      FieldName = 'House'
      Required = True
      Size = 6
    end
    object quFindCliBuild: TStringField
      FieldName = 'Build'
      Required = True
      Size = 3
    end
    object quFindCliKvart: TStringField
      FieldName = 'Kvart'
      Required = True
      Size = 4
    end
    object quFindCliOKPO: TStringField
      FieldName = 'OKPO'
      Required = True
      Size = 10
    end
    object quFindCliOKONH: TStringField
      FieldName = 'OKONH'
      Required = True
      Size = 50
    end
    object quFindCliINN: TStringField
      FieldName = 'INN'
      Required = True
    end
    object quFindCliFullName: TStringField
      FieldName = 'FullName'
      Required = True
      Size = 100
    end
    object quFindCliCodeUchet: TStringField
      FieldName = 'CodeUchet'
      Required = True
    end
    object quFindCliUnTaxedNDS: TWordField
      FieldName = 'UnTaxedNDS'
      Required = True
    end
    object quFindCliStatusByte: TWordField
      FieldName = 'StatusByte'
      Required = True
    end
    object quFindCliStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
  end
  object dsquFindCli: TDataSource
    DataSet = quFindCli
    Left = 848
    Top = 228
  end
  object quCatDisc: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  '
      'ssg.GoodsGroupID,'
      'ssg.ID,'
      'ssg.Name,'
      'sg.ID as IDsg,'
      'sg.Name as NameSg,  '
      'sg.GoodsGroupID as GoodsGroupIDsg,'
      'gr.ID as IDgr,'
      'gr.Name as Namegr,'
      'ssg.V01'
      'from "SubGroup" ssg'
      'left join "SubGroup" sg on sg.ID=ssg.GoodsGroupID'
      'left join "SubGroup" gr on gr.ID=sg.GoodsGroupID'
      'where ssg.ID>=20000'
      'and sg.GoodsGroupID<10000'
      'order by gr.ID,sg.ID,ssg.ID asc')
    Params = <>
    Left = 656
    Top = 86
    object quCatDiscGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quCatDiscID: TSmallintField
      FieldName = 'ID'
    end
    object quCatDiscName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCatDiscIDsg: TSmallintField
      FieldName = 'IDsg'
    end
    object quCatDiscNameSg: TStringField
      FieldName = 'NameSg'
      Size = 30
    end
    object quCatDiscGoodsGroupIDsg: TSmallintField
      FieldName = 'GoodsGroupIDsg'
    end
    object quCatDiscIDgr: TSmallintField
      FieldName = 'IDgr'
    end
    object quCatDiscNamegr: TStringField
      FieldName = 'Namegr'
      Size = 30
    end
    object quCatDiscV01: TIntegerField
      FieldName = 'V01'
    end
  end
  object quAlgClass: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CLASSIFALG"')
    Params = <>
    Left = 24
    Top = 892
    object quAlgClassID: TIntegerField
      FieldName = 'ID'
    end
    object quAlgClassNAMECLA: TStringField
      FieldName = 'NAMECLA'
      Size = 200
    end
    object quAlgClassGetAM: TSmallintField
      FieldName = 'GetAM'
    end
  end
  object dsquAlgClass: TDataSource
    DataSet = quAlgClass
    Left = 96
    Top = 892
  end
  object dsquMakers1: TDataSource
    DataSet = quMakers
    Left = 400
    Top = 116
  end
  object dsquAlgClass1: TDataSource
    DataSet = quAlgClass
    Left = 168
    Top = 892
  end
  object quSpecInv1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select '
      'IDH,'
      'ITEM,'
      'QUANTR,'
      'QUANTF,'
      'QUANTD,'
      'PRICER,'
      'SUMR,'
      'SUMF,'
      'SUMD,'
      'SUMRSS,'
      'SUMFSS,'
      'SUMDSS,'
      'NUM,'
      'QUANTC,'
      'SUMC,'
      'SUMRSC'
      'from "A_INVLN"'
      'where IDH=:ID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 656
    Top = 384
    object quSpecInv1IDH: TStringField
      FieldName = 'IDH'
      Size = 19
    end
    object quSpecInv1ITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object quSpecInv1QUANTR: TFloatField
      FieldName = 'QUANTR'
    end
    object quSpecInv1QUANTF: TFloatField
      FieldName = 'QUANTF'
    end
    object quSpecInv1QUANTD: TFloatField
      FieldName = 'QUANTD'
    end
    object quSpecInv1PRICER: TFloatField
      FieldName = 'PRICER'
    end
    object quSpecInv1SUMR: TFloatField
      FieldName = 'SUMR'
    end
    object quSpecInv1SUMF: TFloatField
      FieldName = 'SUMF'
    end
    object quSpecInv1SUMD: TFloatField
      FieldName = 'SUMD'
    end
    object quSpecInv1SUMRSS: TFloatField
      FieldName = 'SUMRSS'
    end
    object quSpecInv1SUMFSS: TFloatField
      FieldName = 'SUMFSS'
    end
    object quSpecInv1SUMDSS: TFloatField
      FieldName = 'SUMDSS'
    end
    object quSpecInv1NUM: TIntegerField
      FieldName = 'NUM'
    end
    object quSpecInv1QUANTC: TFloatField
      FieldName = 'QUANTC'
    end
    object quSpecInv1SUMC: TFloatField
      FieldName = 'SUMC'
    end
    object quSpecInv1SUMRSC: TFloatField
      FieldName = 'SUMRSC'
    end
  end
  object quSpecOut2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInCalcFields
    SQL.Strings = (
      'select'
      'ln.CodeTovar,'
      'ln.ChekBuh,'
      'SUM(ln.Kol) as Kol,'
      'SUM(ln.SumVesTara) as SumVesTara'
      ''
      'from "TTNOutLn" ln'
      'where ln.Depart=:IDEP'
      'and ln.DateInvoice=:SDATE'
      'and ln.Number=:SNUM'
      ''
      'group by ln.CodeTovar, ln.ChekBuh'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'SNUM'
        ParamType = ptUnknown
        Value = ''
      end>
    Left = 256
    Top = 432
    object quSpecOut2CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quSpecOut2ChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quSpecOut2Kol: TFloatField
      FieldName = 'Kol'
    end
    object quSpecOut2SumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
  end
  object quCashPath: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CASHPATH"'
      'order by ID')
    Params = <>
    Left = 1048
    Top = 702
    object quCashPathID: TIntegerField
      FieldName = 'ID'
    end
    object quCashPathPATH: TStringField
      FieldName = 'PATH'
      Size = 300
    end
  end
  object quF: TPvQuery
    AutoRefresh = True
    DatabaseName = 'SCRYSTAL'
    Params = <>
    Left = 12
    Top = 80
  end
  object quRepRealAP: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quRepRealAPCalcFields
    SQL.Strings = (
      'select * from "A_REALALCO"'
      'where IDATE=:IDATE and IDREP=:IDREP'
      'and DATEOPER>=:DATEB and DATEOPER<:DATEE'
      'order by DATEOPER,TIMEOP')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDREP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 1056
    Top = 796
    object quRepRealAPIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quRepRealAPIDREP: TIntegerField
      FieldName = 'IDREP'
    end
    object quRepRealAPID: TAutoIncField
      FieldName = 'ID'
    end
    object quRepRealAPIDEP: TIntegerField
      FieldName = 'IDEP'
    end
    object quRepRealAPIDATER: TIntegerField
      FieldName = 'IDATER'
    end
    object quRepRealAPDATEOPER: TDateField
      FieldName = 'DATEOPER'
    end
    object quRepRealAPICODE: TIntegerField
      FieldName = 'ICODE'
    end
    object quRepRealAPBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object quRepRealAPFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 200
    end
    object quRepRealAPQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quRepRealAPAVID: TIntegerField
      FieldName = 'AVID'
    end
    object quRepRealAPVOL: TFloatField
      FieldName = 'VOL'
      DisplayFormat = '0.000'
    end
    object quRepRealAPTIMEOP: TTimeField
      FieldName = 'TIMEOP'
    end
    object quRepRealAPDATETIMEOP: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'DATETIMEOP'
      Calculated = True
    end
  end
  object dsquRepRealAP: TDataSource
    DataSet = quRepRealAP
    Left = 1144
    Top = 796
  end
  object quRepRealItog: TPvQuery
    DatabaseName = 'PSQL'
    OnCalcFields = quRepRealAPCalcFields
    SQL.Strings = (
      
        'select IDATE,IDREP,IDEP,ICODE,BARCODE,FULLNAME,AVID,VOL,SUM(QUAN' +
        'T) as QUANTITOG from "A_REALALCO"'
      
        'where IDATE=:IDATE and IDREP=:IDREP and DATEOPER>=:DATEB and DAT' +
        'EOPER<:DATEE'
      'group by IDATE,IDREP,IDEP,ICODE,BARCODE,FULLNAME,AVID,VOL'
      'order by AVID,FULLNAME')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDREP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 1056
    Top = 860
    object quRepRealItogIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quRepRealItogIDREP: TIntegerField
      FieldName = 'IDREP'
    end
    object quRepRealItogIDEP: TIntegerField
      FieldName = 'IDEP'
    end
    object quRepRealItogICODE: TIntegerField
      FieldName = 'ICODE'
    end
    object quRepRealItogBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object quRepRealItogFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 200
    end
    object quRepRealItogAVID: TIntegerField
      FieldName = 'AVID'
    end
    object quRepRealItogVOL: TFloatField
      FieldName = 'VOL'
    end
    object quRepRealItogQUANTITOG: TFloatField
      FieldName = 'QUANTITOG'
    end
  end
  object quGetAlcoList: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "Goods"'
      'where V11>0'
      'and Status<100')
    Params = <>
    Left = 280
    Top = 886
    object quGetAlcoListOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object quGetAlcoListGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quGetAlcoListSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quGetAlcoListID: TIntegerField
      FieldName = 'ID'
    end
    object quGetAlcoListName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quGetAlcoListBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quGetAlcoListShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object quGetAlcoListTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quGetAlcoListEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quGetAlcoListNDS: TFloatField
      FieldName = 'NDS'
    end
    object quGetAlcoListOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quGetAlcoListWeght: TFloatField
      FieldName = 'Weght'
    end
    object quGetAlcoListSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object quGetAlcoListTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quGetAlcoListKritOst: TFloatField
      FieldName = 'KritOst'
    end
    object quGetAlcoListBHTStatus: TSmallintField
      FieldName = 'BHTStatus'
    end
    object quGetAlcoListUKMAction: TSmallintField
      FieldName = 'UKMAction'
    end
    object quGetAlcoListDataChange: TDateField
      FieldName = 'DataChange'
    end
    object quGetAlcoListNSP: TFloatField
      FieldName = 'NSP'
    end
    object quGetAlcoListWasteRate: TFloatField
      FieldName = 'WasteRate'
    end
    object quGetAlcoListCena: TFloatField
      FieldName = 'Cena'
    end
    object quGetAlcoListKol: TFloatField
      FieldName = 'Kol'
    end
    object quGetAlcoListFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quGetAlcoListCountry: TSmallintField
      FieldName = 'Country'
    end
    object quGetAlcoListStatus: TSmallintField
      FieldName = 'Status'
    end
    object quGetAlcoListPrsision: TFloatField
      FieldName = 'Prsision'
    end
    object quGetAlcoListPriceState: TWordField
      FieldName = 'PriceState'
    end
    object quGetAlcoListSetOfGoods: TWordField
      FieldName = 'SetOfGoods'
    end
    object quGetAlcoListPrePacking: TStringField
      FieldName = 'PrePacking'
      Size = 5
    end
    object quGetAlcoListPrintLabel: TWordField
      FieldName = 'PrintLabel'
    end
    object quGetAlcoListMargGroup: TStringField
      FieldName = 'MargGroup'
      Size = 2
    end
    object quGetAlcoListFixPrice: TFloatField
      FieldName = 'FixPrice'
    end
    object quGetAlcoListReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object quGetAlcoListReserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object quGetAlcoListReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object quGetAlcoListReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object quGetAlcoListReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object quGetAlcoListObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quGetAlcoListImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object quGetAlcoListV01: TIntegerField
      FieldName = 'V01'
    end
    object quGetAlcoListV02: TIntegerField
      FieldName = 'V02'
    end
    object quGetAlcoListV03: TIntegerField
      FieldName = 'V03'
    end
    object quGetAlcoListV04: TIntegerField
      FieldName = 'V04'
    end
    object quGetAlcoListV05: TIntegerField
      FieldName = 'V05'
    end
    object quGetAlcoListV06: TIntegerField
      FieldName = 'V06'
    end
    object quGetAlcoListV07: TIntegerField
      FieldName = 'V07'
    end
    object quGetAlcoListV08: TIntegerField
      FieldName = 'V08'
    end
    object quGetAlcoListV09: TIntegerField
      FieldName = 'V09'
    end
    object quGetAlcoListV10: TIntegerField
      FieldName = 'V10'
    end
    object quGetAlcoListV11: TIntegerField
      FieldName = 'V11'
    end
    object quGetAlcoListV12: TIntegerField
      FieldName = 'V12'
    end
    object quGetAlcoListV13: TIntegerField
      FieldName = 'V13'
    end
    object quGetAlcoListV14: TIntegerField
      FieldName = 'V14'
    end
  end
  object quGetAVid: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CLASSIFALG"'
      'where GetAM=1')
    Params = <>
    Left = 352
    Top = 886
    object quGetAVidID: TIntegerField
      FieldName = 'ID'
    end
    object quGetAVidNAMECLA: TStringField
      FieldName = 'NAMECLA'
      Size = 200
    end
    object quGetAVidGetAM: TSmallintField
      FieldName = 'GetAM'
    end
  end
end
