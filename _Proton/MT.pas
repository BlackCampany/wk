unit MT;

interface

uses
  SysUtils, Classes, DB, pvtables, btvtables, dxmdaset, DBClient, cxMemo,Variants,ComCtrls;

type
  TdmMT = class(TDataModule)
    ptCG: TPvTable;
    ptCM: TPvTable;
    ptCGCARD: TIntegerField;
    ptCGITEM: TIntegerField;
    ptCGCOST: TFloatField;
    ptCGDEPART: TSmallintField;
    ptCGVENDOR: TIntegerField;
    ptCGATTRIBUTE: TSmallintField;
    dsptCG: TDataSource;
    ptCMCARD: TIntegerField;
    ptCMMOVEDATE: TDateField;
    ptCMDOC_TYPE: TSmallintField;
    ptCMDOCUMENT: TIntegerField;
    ptCMPRICE: TFloatField;
    ptCMQUANT_MOVE: TFloatField;
    ptCMSUM_MOVE: TFloatField;
    ptCMQUANT_REST: TFloatField;
    taCards: TPvTable;
    taCardsOtdel: TSmallintField;
    taCardsGoodsGroupID: TSmallintField;
    taCardsSubGroupID: TSmallintField;
    taCardsID: TIntegerField;
    taCardsName: TStringField;
    taCardsBarCode: TStringField;
    taCardsShRealize: TSmallintField;
    taCardsTovarType: TSmallintField;
    taCardsEdIzm: TSmallintField;
    taCardsNDS: TFloatField;
    taCardsOKDP: TFloatField;
    taCardsWeght: TFloatField;
    taCardsSrokReal: TSmallintField;
    taCardsTareWeight: TSmallintField;
    taCardsKritOst: TFloatField;
    taCardsBHTStatus: TSmallintField;
    taCardsUKMAction: TSmallintField;
    taCardsDataChange: TDateField;
    taCardsNSP: TFloatField;
    taCardsWasteRate: TFloatField;
    taCardsCena: TFloatField;
    taCardsKol: TFloatField;
    taCardsFullName: TStringField;
    taCardsCountry: TSmallintField;
    taCardsStatus: TSmallintField;
    taCardsPrsision: TFloatField;
    taCardsPriceState: TWordField;
    taCardsSetOfGoods: TWordField;
    taCardsPrePacking: TStringField;
    taCardsPrintLabel: TWordField;
    taCardsMargGroup: TStringField;
    taCardsFixPrice: TFloatField;
    taCardsReserv1: TFloatField;
    taCardsReserv2: TFloatField;
    taCardsReserv3: TFloatField;
    taCardsReserv4: TFloatField;
    taCardsReserv5: TFloatField;
    taCardsObjectTypeID: TSmallintField;
    taCardsImageID: TIntegerField;
    taCardsV01: TIntegerField;
    taCardsV02: TIntegerField;
    taCardsV03: TIntegerField;
    taCardsV04: TIntegerField;
    taCardsV05: TIntegerField;
    taCardsV06: TIntegerField;
    taCardsV07: TIntegerField;
    taCardsV08: TIntegerField;
    taCardsV09: TIntegerField;
    taCardsV10: TIntegerField;
    taCardsV11: TIntegerField;
    taCardsV12: TIntegerField;
    taCardsV13: TIntegerField;
    taCardsV14: TIntegerField;
    taRemove: TdxMemData;
    taRemoveArticul: TIntegerField;
    taRemoveDepart: TIntegerField;
    taRemoveQuant: TFloatField;
    taRemoveRes: TIntegerField;
    ptCQ: TPvTable;
    ptCQQuant: TFloatField;
    ptCQOperation: TStringField;
    ptCQDateOperation: TDateField;
    ptCQPrice: TFloatField;
    ptCQStore: TStringField;
    ptCQCk_Number: TIntegerField;
    ptCQCk_Curs: TFloatField;
    ptCQCk_CurAbr: TIntegerField;
    ptCQCk_Disg: TFloatField;
    ptCQCk_Disc: TFloatField;
    ptCQQuant_S: TFloatField;
    ptCQGrCode: TSmallintField;
    ptCQCode: TIntegerField;
    ptCQCassir: TStringField;
    ptCQCash_Code: TIntegerField;
    ptCQCk_Card: TIntegerField;
    ptCQContr_Code: TStringField;
    ptCQContr_Cost: TFloatField;
    ptCQSeria: TStringField;
    ptCQBestB: TStringField;
    ptCQNDSx1: TFloatField;
    ptCQNDSx2: TFloatField;
    ptCQTimes: TTimeField;
    ptCQSumma: TFloatField;
    ptCQSumNDS: TFloatField;
    ptCQSumNSP: TFloatField;
    ptCQPriceNSP: TFloatField;
    ptCQNSmena: TIntegerField;
    meMove: TdxMemData;
    meMoveCard: TIntegerField;
    meMoveMoveDate: TDateField;
    meMoveDoc_Type: TIntegerField;
    meMoveDocument: TIntegerField;
    meMoveQuantMove: TFloatField;
    meMoveQuantRest: TFloatField;
    meMoveName: TStringField;
    meMoveDepart: TIntegerField;
    meMoveQIn: TFloatField;
    meMoveQOut: TFloatField;
    dsmeMove: TDataSource;
    ptDep: TPvTable;
    ptDepID: TSmallintField;
    ptDepName: TStringField;
    ptDepStatus1: TSmallintField;
    ptDepStatus2: TSmallintField;
    ptDepStatus3: TSmallintField;
    ptDepStatus4: TIntegerField;
    ptDepStatus5: TIntegerField;
    ptDepParentID: TSmallintField;
    ptDepObjectTypeID: TSmallintField;
    ptDepClientID: TIntegerField;
    ptDepClientObjTypeID: TSmallintField;
    ptDepImageID: TIntegerField;
    ptDepFullName: TStringField;
    ptDepEnglishFullName: TStringField;
    ptDepPath: TStringField;
    ptDepV01: TIntegerField;
    ptDepV02: TIntegerField;
    ptDepV03: TIntegerField;
    ptDepV04: TIntegerField;
    ptDepV05: TIntegerField;
    ptDepV06: TIntegerField;
    ptDepV07: TIntegerField;
    ptDepV08: TIntegerField;
    ptDepV09: TIntegerField;
    ptDepV10: TIntegerField;
    ptDepV11: TIntegerField;
    ptDepV12: TIntegerField;
    ptDepV13: TIntegerField;
    ptDepV14: TIntegerField;
    ptCM1: TPvTable;
    ptCM1CARD: TIntegerField;
    ptCM1MOVEDATE: TDateField;
    ptCM1DOC_TYPE: TSmallintField;
    ptCM1DOCUMENT: TIntegerField;
    ptCM1PRICE: TFloatField;
    ptCM1QUANT_MOVE: TFloatField;
    ptCM1SUM_MOVE: TFloatField;
    ptCM1QUANT_REST: TFloatField;
    taHeadDoc: TClientDataSet;
    taHeadDocIType: TSmallintField;
    taHeadDocId: TIntegerField;
    taHeadDocDateDoc: TIntegerField;
    taHeadDocNumDoc: TStringField;
    taHeadDocIdCli: TIntegerField;
    taHeadDocNameCli: TStringField;
    taHeadDocIdSkl: TIntegerField;
    taHeadDocNameSkl: TStringField;
    taHeadDocSumIN: TFloatField;
    taHeadDocSumUch: TFloatField;
    taHeadDocComment: TStringField;
    dsHeadDoc: TDataSource;
    taSpecDoc: TClientDataSet;
    taSpecDocIType: TSmallintField;
    taSpecDocIdHead: TIntegerField;
    taSpecDocNum: TIntegerField;
    taSpecDocIdCard: TIntegerField;
    taSpecDocQuant: TFloatField;
    taSpecDocPriceIn: TFloatField;
    taSpecDocSumIn: TFloatField;
    taSpecDocPriceUch: TFloatField;
    taSpecDocSumUch: TFloatField;
    taSpecDocIdNds: TIntegerField;
    taSpecDocSumNds: TFloatField;
    taSpecDocNameC: TStringField;
    taSpecDocSm: TStringField;
    taSpecDocIdM: TIntegerField;
    taSpecDocKm: TFloatField;
    taSpecDocPriceUch1: TFloatField;
    taSpecDocSumUch1: TFloatField;
    taSpecDocProcPrice: TFloatField;
    dsSpecDoc: TDataSource;
    taHeadDociTypeCli: TIntegerField;
    taHeadDocSumTara: TFloatField;
    taSpecDocQuantM: TFloatField;
    taSpecDocIdTara: TIntegerField;
    taSpecDocQuantT: TFloatField;
    taSpecDocNameT: TStringField;
    taSpecDocPriceT: TFloatField;
    taSpecDocSumTara: TFloatField;
    taSpecDocBarCode: TStringField;
    ptMgr: TPvTable;
    ptMgrID: TAutoIncField;
    ptMgrName: TStringField;
    ptMgrFullName: TStringField;
    ptMgrGroupEU: TSmallintField;
    ptSGr: TPvTable;
    ptSGrGoodsGroupID: TSmallintField;
    ptSGrID: TAutoIncField;
    ptSGrName: TStringField;
    taACards: TPvTable;
    taACardsID: TIntegerField;
    taACardsRECEIPT: TStringField;
    ptDStop: TPvTable;
    ptDStopCode: TIntegerField;
    ptDStopPercent: TFloatField;
    ptDStopStatus: TSmallintField;
    ptDStopStatus_1: TSmallintField;
    ptCqRep: TPvTable;
    ptCqRepQuant: TFloatField;
    ptCqRepOperation: TStringField;
    ptCqRepDateOperation: TDateField;
    ptCqRepPrice: TFloatField;
    ptCqRepStore: TStringField;
    ptCqRepCk_Number: TIntegerField;
    ptCqRepCk_Curs: TFloatField;
    ptCqRepCk_CurAbr: TIntegerField;
    ptCqRepCk_Disg: TFloatField;
    ptCqRepCk_Disc: TFloatField;
    ptCqRepQuant_S: TFloatField;
    ptCqRepGrCode: TSmallintField;
    ptCqRepCode: TIntegerField;
    ptCqRepCassir: TStringField;
    ptCqRepCash_Code: TIntegerField;
    ptCqRepCk_Card: TIntegerField;
    ptCqRepContr_Code: TStringField;
    ptCqRepContr_Cost: TFloatField;
    ptCqRepSeria: TStringField;
    ptCqRepBestB: TStringField;
    ptCqRepNDSx1: TFloatField;
    ptCqRepNDSx2: TFloatField;
    ptCqRepTimes: TTimeField;
    ptCqRepSumma: TFloatField;
    ptCqRepSumNDS: TFloatField;
    ptCqRepSumNSP: TFloatField;
    ptCqRepPriceNSP: TFloatField;
    ptCqRepNSmena: TIntegerField;
    ptCqRepName: TStringField;
    dsptCqRep: TDataSource;
    taG: TPvTable;
    taGGoodsGroupID: TSmallintField;
    taGSubGroupID: TSmallintField;
    taGID: TIntegerField;
    taGName: TStringField;
    taGBarCode: TStringField;
    taGShRealize: TSmallintField;
    taGTovarType: TSmallintField;
    taGEdIzm: TSmallintField;
    taGNDS: TFloatField;
    taGSrokReal: TSmallintField;
    taGDataChange: TDateField;
    taGCena: TFloatField;
    taGKol: TFloatField;
    taGFullName: TStringField;
    taGCountry: TSmallintField;
    taGStatus: TSmallintField;
    taGV01: TIntegerField;
    taGV02: TIntegerField;
    taGV03: TIntegerField;
    ptCT: TPvTable;
    dsptCT: TDataSource;
    ptCTM: TPvTable;
    ptCTCARD: TIntegerField;
    ptCTITEM: TIntegerField;
    ptCTCOST: TFloatField;
    ptCTDEPART: TSmallintField;
    ptCTVENDOR: TIntegerField;
    ptCTATTRIBUTE: TSmallintField;
    ptCTMCARD: TIntegerField;
    ptCTMMOVEDATE: TDateField;
    ptCTMDOC_TYPE: TSmallintField;
    ptCTMDOCUMENT: TIntegerField;
    ptCTMPRICE: TFloatField;
    ptCTMQUANT_MOVE: TFloatField;
    ptCTMSUM_MOVE: TFloatField;
    ptCTMQUANT_REST: TFloatField;
    teClass: TdxMemData;
    teClassId: TIntegerField;
    teClassIdParent: TIntegerField;
    teClassNameClass: TStringField;
    ptPluLim: TPvTable;
    ptPluLimCode: TIntegerField;
    ptPluLimPercent: TFloatField;
    ptPluLimPercentDisc: TFloatField;
    dsptPluLim: TDataSource;
    ptMOL: TPvTable;
    ptMOLID: TSmallintField;
    ptMOLOTVL: TStringField;
    ptCTM1: TPvTable;
    ptCTM1CARD: TIntegerField;
    ptCTM1MOVEDATE: TDateField;
    ptCTM1DOC_TYPE: TSmallintField;
    ptCTM1DOCUMENT: TIntegerField;
    ptCTM1PRICE: TFloatField;
    ptCTM1QUANT_MOVE: TFloatField;
    ptCTM1SUM_MOVE: TFloatField;
    ptCTM1QUANT_REST: TFloatField;
    ptInvLine: TPvTable;
    ptInvLineInventry: TIntegerField;
    ptInvLineItem: TIntegerField;
    ptInvLinePrice: TFloatField;
    ptInvLineQuantRecord: TFloatField;
    ptInvLineQuantActual: TFloatField;
    ptInvLineQuantLost: TFloatField;
    ptInvLineDepart: TSmallintField;
    ptInvLineIndexPost: TSmallintField;
    ptInvLineCodePost: TSmallintField;
    ptSGrReserv1: TFloatField;
    ptSGrReserv2: TFloatField;
    ptSGrReserv3: TFloatField;
    ptCM2: TPvTable;
    ptCM2CARD: TIntegerField;
    ptCM2MOVEDATE: TDateField;
    ptCM2DOC_TYPE: TSmallintField;
    ptCM2DOCUMENT: TIntegerField;
    ptCM2PRICE: TFloatField;
    ptCM2QUANT_MOVE: TFloatField;
    ptCM2SUM_MOVE: TFloatField;
    ptCM2QUANT_REST: TFloatField;
    ptCn: TPvTable;
    ptCnXZ: TFloatField;
    ptCnRDate: TDateField;
    ptCnRPrice: TFloatField;
    ptCnDepart: TSmallintField;
    ptCnCode: TIntegerField;
    ptCnXZ1: TFloatField;
    ptCnRSum: TFloatField;
    ptCnXZ2: TFloatField;
    ptCnXZ3: TFloatField;
    ptCnQuant: TFloatField;
    ptCnRSumCor: TFloatField;
    meCg: TdxMemData;
    meCgCard: TIntegerField;
    ptInLn: TPvTable;
    ptInLnDepart: TSmallintField;
    ptInLnDateInvoice: TDateField;
    ptInLnNumber: TStringField;
    ptInLnIndexPost: TSmallintField;
    ptInLnCodePost: TSmallintField;
    ptInLnCodeGroup: TSmallintField;
    ptInLnCodePodgr: TSmallintField;
    ptInLnCodeTovar: TIntegerField;
    ptInLnCodeEdIzm: TSmallintField;
    ptInLnTovarType: TSmallintField;
    ptInLnBarCode: TStringField;
    ptInLnMediatorCost: TFloatField;
    ptInLnNDSProc: TFloatField;
    ptInLnNDSSum: TFloatField;
    ptInLnOutNDSSum: TFloatField;
    ptInLnBestBefore: TDateField;
    ptInLnInvoiceQuant: TFloatField;
    ptInLnKolMest: TIntegerField;
    ptInLnKolEdMest: TFloatField;
    ptInLnKolWithMest: TFloatField;
    ptInLnKolWithNecond: TFloatField;
    ptInLnKol: TFloatField;
    ptInLnCenaTovar: TFloatField;
    ptInLnProcent: TFloatField;
    ptInLnNewCenaTovar: TFloatField;
    ptInLnSumCenaTovarPost: TFloatField;
    ptInLnSumCenaTovar: TFloatField;
    ptInLnProcentN: TFloatField;
    ptInLnNecond: TFloatField;
    ptInLnCenaNecond: TFloatField;
    ptInLnSumNecond: TFloatField;
    ptInLnProcentZ: TFloatField;
    ptInLnZemlia: TFloatField;
    ptInLnProcentO: TFloatField;
    ptInLnOthodi: TFloatField;
    ptInLnCodeTara: TIntegerField;
    ptInLnVesTara: TFloatField;
    ptInLnCenaTara: TFloatField;
    ptInLnSumVesTara: TFloatField;
    ptInLnSumCenaTara: TFloatField;
    ptInLnChekBuh: TBooleanField;
    ptInLnSertBeginDate: TDateField;
    ptInLnSertEndDate: TDateField;
    ptInLnSertNumber: TStringField;
    ptInventryHead: TPvTable;
    ptInventryHeadInventry: TIntegerField;
    ptInventryHeadDepart: TSmallintField;
    ptInventryHeadDateInventry: TDateField;
    ptInventryHeadNumber: TStringField;
    ptInventryHeadCardIndex: TWordField;
    ptInventryHeadInputMode: TWordField;
    ptInventryHeadPriceMode: TWordField;
    ptInventryHeadDetail: TWordField;
    ptInventryHeadGGroup: TSmallintField;
    ptInventryHeadSubGroup: TSmallintField;
    ptInventryHeadItem: TIntegerField;
    ptInventryHeadCountLost: TBooleanField;
    ptInventryHeadDateLost: TDateField;
    ptInventryHeadQuantRecord: TFloatField;
    ptInventryHeadSumRecord: TFloatField;
    ptInventryHeadQuantActual: TFloatField;
    ptInventryHeadSumActual: TFloatField;
    ptInventryHeadNumberOfLines: TIntegerField;
    ptInventryHeadDateStart: TDateField;
    ptInventryHeadTimeStart: TTimeField;
    ptInventryHeadPresident: TStringField;
    ptInventryHeadPresidentPost: TStringField;
    ptInventryHeadMember1: TStringField;
    ptInventryHeadMember1_Post: TStringField;
    ptInventryHeadMember2: TStringField;
    ptInventryHeadMember2_Post: TStringField;
    ptInventryHeadMember3: TStringField;
    ptInventryHeadMember3_Post: TStringField;
    ptInventryHeadxDopStatus: TWordField;
    ptInventryHeadResponse1: TStringField;
    ptInventryHeadResponse1_Post: TStringField;
    ptInventryHeadResponse2: TStringField;
    ptInventryHeadResponse2_Post: TStringField;
    ptInventryHeadResponse3: TStringField;
    ptInventryHeadResponse3_Post: TStringField;
    ptInventryHeadReason: TStringField;
    ptInventryHeadCASHER: TStringField;
    ptInventryHeadStatus: TStringField;
    ptInventryHeadLinkInvoice: TWordField;
    ptInventryHeadStartTransfer: TWordField;
    ptInventryHeadEndTransfer: TWordField;
    ptInventryHeadRezerv: TStringField;
    teInventryArt: TdxMemData;
    teInventryArtArticul: TIntegerField;
    ptTTK: TPvTable;
    ptTTKID: TIntegerField;
    ptTTKNUM: TSmallintField;
    ptTTKIDC: TIntegerField;
    ptTTKPROC1: TFloatField;
    ptTTKPROC2: TFloatField;
    ptTTnInLn: TPvTable;
    ptTTnInLnDepart: TSmallintField;
    ptTTnInLnDateInvoice: TDateField;
    ptTTnInLnNumber: TStringField;
    ptTTnInLnIndexPost: TSmallintField;
    ptTTnInLnCodePost: TSmallintField;
    ptTTnInLnCodeGroup: TSmallintField;
    ptTTnInLnCodePodgr: TSmallintField;
    ptTTnInLnCodeTovar: TIntegerField;
    ptTTnInLnCodeEdIzm: TSmallintField;
    ptTTnInLnTovarType: TSmallintField;
    ptTTnInLnBarCode: TStringField;
    ptTTnInLnMediatorCost: TFloatField;
    ptTTnInLnNDSProc: TFloatField;
    ptTTnInLnNDSSum: TFloatField;
    ptTTnInLnOutNDSSum: TFloatField;
    ptTTnInLnBestBefore: TDateField;
    ptTTnInLnInvoiceQuant: TFloatField;
    ptTTnInLnKolMest: TIntegerField;
    ptTTnInLnKolEdMest: TFloatField;
    ptTTnInLnKolWithMest: TFloatField;
    ptTTnInLnKolWithNecond: TFloatField;
    ptTTnInLnKol: TFloatField;
    ptTTnInLnCenaTovar: TFloatField;
    ptTTnInLnProcent: TFloatField;
    ptTTnInLnNewCenaTovar: TFloatField;
    ptTTnInLnSumCenaTovarPost: TFloatField;
    ptTTnInLnSumCenaTovar: TFloatField;
    ptTTnInLnProcentN: TFloatField;
    ptTTnInLnNecond: TFloatField;
    ptTTnInLnCenaNecond: TFloatField;
    ptTTnInLnSumNecond: TFloatField;
    ptTTnInLnProcentZ: TFloatField;
    ptTTnInLnZemlia: TFloatField;
    ptTTnInLnProcentO: TFloatField;
    ptTTnInLnOthodi: TFloatField;
    ptTTnInLnCodeTara: TIntegerField;
    ptTTnInLnVesTara: TFloatField;
    ptTTnInLnCenaTara: TFloatField;
    ptTTnInLnSumVesTara: TFloatField;
    ptTTnInLnSumCenaTara: TFloatField;
    ptTTnInLnChekBuh: TBooleanField;
    ptTTnInLnSertBeginDate: TDateField;
    ptTTnInLnSertEndDate: TDateField;
    ptTTnInLnSertNumber: TStringField;
    ptBufPrice: TPvTable;
    ptBufPriceIDCARD: TIntegerField;
    ptBufPriceIDDOC: TIntegerField;
    ptBufPriceTYPEDOC: TSmallintField;
    ptBufPriceOLDP: TFloatField;
    ptBufPriceNEWP: TFloatField;
    ptBufPriceSTATUS: TSmallintField;
    ptBufPriceNUMDOC: TStringField;
    ptBufPriceDATEDOC: TDateField;
    ptCassir: TPvTable;
    ptCassirDateDay: TDateField;
    ptCassirCassaNumber: TIntegerField;
    ptCassirCodeKadr: TSmallintField;
    ptCassirDepart: TSmallintField;
    ptCassirPrihod: TFloatField;
    ptCassirTriger: TIntegerField;
    ptCassirSch1: TFloatField;
    ptCassirCor1: TFloatField;
    ptCassirSch2: TFloatField;
    ptCassirCor2: TFloatField;
    ptCassirSch3: TFloatField;
    ptCassirCor3: TFloatField;
    ptCassirSch4: TFloatField;
    ptCassirCor4: TFloatField;
    ptCassirSch5: TFloatField;
    ptCassirCor5: TFloatField;
    ptCassirSch6: TFloatField;
    ptCassirCor6: TFloatField;
    ptCassirSch7: TFloatField;
    ptCassirCor7: TFloatField;
    ptCassirSch8: TFloatField;
    ptCassirCor8: TFloatField;
    ptCassirSch9: TFloatField;
    ptCassirCor9: TFloatField;
    ptCassirSch10: TFloatField;
    ptCassirCor10: TFloatField;
    ptCassirSumma: TFloatField;
    ptCassirCorSumma: TFloatField;
    ptCassirRSch1: TFloatField;
    ptCassirRSch2: TFloatField;
    ptCassirRSch3: TFloatField;
    ptCassirRSch4: TFloatField;
    ptCassirRSch5: TFloatField;
    ptCassirRSch6: TFloatField;
    ptCassirRSch7: TFloatField;
    ptCassirRSch8: TFloatField;
    ptCassirRSch9: TFloatField;
    ptCassirRSch10: TFloatField;
    ptCassirRSumma: TFloatField;
    ptCassirOprihod: TBooleanField;
    ptCassirSummaNDSx10: TFloatField;
    ptCassirSummaNDSx20: TFloatField;
    ptCassirCorNDSx10: TFloatField;
    ptCassirCorNDSx20: TFloatField;
    ptCassirSummaNSPx10: TFloatField;
    ptCassirSummaNSPx20: TFloatField;
    ptCassirSummaNDSx0: TFloatField;
    ptCassirCorNDSx0: TFloatField;
    ptCassirSummaNSP0: TFloatField;
    ptCassirNDSx10: TFloatField;
    ptCassirNDSx20: TFloatField;
    ptCassirCorNSPx0: TFloatField;
    ptCassirCorNSPx10: TFloatField;
    ptCassirCorNSPx20: TFloatField;
    ptCassirRezerv: TFloatField;
    ptCassirShopIndex: TSmallintField;
    taGReserv1: TFloatField;
    taGReserv2: TFloatField;
    taGReserv3: TFloatField;
    taGReserv4: TFloatField;
    taGReserv5: TFloatField;
    taGV04: TIntegerField;
    taGV05: TIntegerField;
    taGV06: TIntegerField;
    ptCliGoods: TPvTable;
    ptCliGoodsGoodsID: TIntegerField;
    ptCliGoodsClientObjTypeID: TSmallintField;
    ptCliGoodsClientID: TSmallintField;
    ptCliGoodsCode: TIntegerField;
    ptCliGoodsBarCode: TStringField;
    ptCliGoodsQuant: TFloatField;
    ptCliGoodsBarFormat: TSmallintField;
    ptCliGoodsxDate: TDateField;
    ptCliGoodsBarStatus: TSmallintField;
    ptCliGoodsName: TStringField;
    ptCliGoodsStatus: TSmallintField;
    ptTTNIn: TPvTable;
    ptTTNInDepart: TSmallintField;
    ptTTNInDateInvoice: TDateField;
    ptTTNInNumber: TStringField;
    ptTTNInSCFNumber: TStringField;
    ptTTNInIndexPost: TSmallintField;
    ptTTNInCodePost: TSmallintField;
    ptTTNInID: TIntegerField;
    ptACValue: TPvTable;
    ptACValueID: TAutoIncField;
    ptACValueAddCharID: TIntegerField;
    ptACValueACValue: TStringField;
    ptZadan: TPvTable;
    ptZadanId: TBCDField;
    ptZadanIType: TSmallintField;
    ptZadanComment: TStringField;
    ptZadanStatus: TSmallintField;
    ptZadanZDate: TDateField;
    ptZadanZTime: TTimeField;
    ptZadanPersonId: TIntegerField;
    ptZadanPersonName: TStringField;
    ptZadanFormDate: TDateField;
    ptZadanFormTime: TTimeField;
    ptZadanIdDoc: TBCDField;
    ptCashs: TPvTable;
    ptCashsNumber: TIntegerField;
    ptCashsName: TStringField;
    ptCashsShRealiz: TIntegerField;
    ptCashsAddGoods: TIntegerField;
    ptCashsTechnolog: TIntegerField;
    ptCashsFlags: TSmallintField;
    ptCashsRezerv: TFloatField;
    ptCashsShopIndex: TSmallintField;
    ptCashsLoad: TIntegerField;
    ptCashsTake: TIntegerField;
    ptGdsLoad: TPvTable;
    ptGdsLoadCode: TIntegerField;
    ptGdsLoadIDate: TIntegerField;
    ptGdsLoadId: TIntegerField;
    ptGdsLoadDateLoad: TDateField;
    ptGdsLoadTimeLoad: TTimeField;
    ptGdsLoadCType: TStringField;
    ptGdsLoadPrice: TFloatField;
    ptGdsLoadDisc: TFloatField;
    ptGdsLoadPerson: TStringField;
    ptGdsLoadSel: TPvTable;
    ptGdsLoadSelCode: TIntegerField;
    ptGdsLoadSelIDate: TIntegerField;
    ptGdsLoadSelId: TIntegerField;
    ptGdsLoadSelDateLoad: TDateField;
    ptGdsLoadSelTimeLoad: TTimeField;
    ptGdsLoadSelCType: TStringField;
    ptGdsLoadSelPrice: TFloatField;
    ptGdsLoadSelDisc: TFloatField;
    ptGdsLoadSelPerson: TStringField;
    dsptGdsLoadSel: TDataSource;
    ptCateg: TPvTable;
    ptCategID: TIntegerField;
    ptCategSID: TStringField;
    ptCategCOMMENT: TStringField;
    ptCashDCrd: TPvTable;
    ptCashDCrdShopIndex: TSmallintField;
    ptCashDCrdCashNumber: TSmallintField;
    ptCashDCrdZNumber: TSmallintField;
    ptCashDCrdCheckNumber: TIntegerField;
    ptCashDCrdCardType: TSmallintField;
    ptCashDCrdCardNumber: TStringField;
    ptCashDCrdDiscountRub: TFloatField;
    ptCashDCrdDiscountCur: TFloatField;
    ptCashDCrdCasher: TStringField;
    ptCashDCrdDateSale: TDateField;
    ptCashDCrdTimeSale: TTimeField;
    ptZRep: TPvTable;
    ptZRepCashNumber: TIntegerField;
    ptZRepZNumber: TIntegerField;
    ptZRepZSale: TFloatField;
    ptZRepZReturn: TFloatField;
    ptZRepZDiscount: TFloatField;
    ptZRepZDate: TDateField;
    ptZRepZTime: TTimeField;
    ptZRepZCrSale: TFloatField;
    ptZRepZCrReturn: TFloatField;
    ptZRepZCrDiscount: TFloatField;
    ptZRepTSale: TFloatField;
    ptZRepTReturn: TFloatField;
    ptZRepCrSale: TFloatField;
    ptZRepCrReturn: TFloatField;
    ptZRepCurMoney: TFloatField;
    ptPartIN: TPvTable;
    ptTTNInAcStatus: TWordField;
    ptDExchH: TPvTable;
    ptDExchHID: TAutoIncField;
    ptDExchHDOCDATE: TDateField;
    ptDExchHDOCNUM: TStringField;
    ptDExchHDEPART: TIntegerField;
    ptDExchHITYPE: TSmallintField;
    ptDExchHINSUMIN: TFloatField;
    ptDExchHINSUMOUT: TFloatField;
    ptDExchHOUTSUMIN: TFloatField;
    ptDExchHOUTSUMOUT: TFloatField;
    ptDExchHCLITYPE: TSmallintField;
    ptDExchS: TPvTable;
    ptDExchSIDH: TIntegerField;
    ptDExchSIDS: TAutoIncField;
    ptDExchSSTYPE: TSmallintField;
    ptDExchSCODE: TIntegerField;
    ptDExchSQUANT: TFloatField;
    ptDExchSPRICEIN: TFloatField;
    ptDExchSPRICEOUT: TFloatField;
    ptDExchSSUMIN: TFloatField;
    ptDExchSSUMOUT: TFloatField;
    ptDExchSNAC: TFloatField;
    ptDExchHIACTIVE: TSmallintField;
    ptDExchHCLICODE: TIntegerField;
    ptCli: TPvTable;
    ptCliVendor: TSmallintField;
    ptCliName: TStringField;
    ptCliPayDays: TSmallintField;
    ptCliPayForm: TWordField;
    ptCliPayWaste: TWordField;
    ptCliFirmName: TStringField;
    ptCliPhone1: TStringField;
    ptCliPhone2: TStringField;
    ptCliPhone3: TStringField;
    ptCliPhone4: TStringField;
    ptCliFax: TStringField;
    ptCliTreatyDate: TDateField;
    ptCliTreatyNumber: TStringField;
    ptCliPostIndex: TStringField;
    ptCliGorod: TStringField;
    ptCliRaion: TSmallintField;
    ptCliStreet: TStringField;
    ptCliHouse: TStringField;
    ptCliBuild: TStringField;
    ptCliKvart: TStringField;
    ptCliOKPO: TStringField;
    ptCliOKONH: TStringField;
    ptCliINN: TStringField;
    ptCliFullName: TStringField;
    ptCliCodeUchet: TStringField;
    ptCliUnTaxedNDS: TWordField;
    ptCliStatusByte: TWordField;
    ptCliStatus: TSmallintField;
    ptIP: TPvTable;
    ptIPCode: TSmallintField;
    ptIPName: TStringField;
    ptIPFirmName: TStringField;
    ptIPFamilia: TStringField;
    ptIPIma: TStringField;
    ptIPOtchestvo: TStringField;
    ptIPVidDok: TStringField;
    ptIPSerPasp: TStringField;
    ptIPNumberPasp: TIntegerField;
    ptIPKemPasp: TStringField;
    ptIPDatePasp: TDateField;
    ptIPCountry: TStringField;
    ptIPPhone1: TStringField;
    ptIPPhone2: TStringField;
    ptIPPhone3: TStringField;
    ptIPPhone4: TStringField;
    ptIPFax: TStringField;
    ptIPEMail: TStringField;
    ptIPPostIndex: TStringField;
    ptIPGorod: TStringField;
    ptIPRaion: TSmallintField;
    ptIPStreet: TStringField;
    ptIPHouse: TStringField;
    ptIPBuild: TStringField;
    ptIPKvart: TStringField;
    ptIPINN: TStringField;
    ptIPNStrach: TStringField;
    ptIPNumberSvid: TStringField;
    ptIPDateSvid: TDateField;
    ptIPFullName: TStringField;
    ptIPDateBorn: TDateField;
    ptIPCodeUchet: TStringField;
    ptIPStatus: TIntegerField;
    ptPartO: TPvTable;
    ptPartINID: TAutoIncField;
    ptPartINIDSTORE: TSmallintField;
    ptPartINIDATE: TIntegerField;
    ptPartINIDDOC: TIntegerField;
    ptPartINARTICUL: TIntegerField;
    ptPartINSINNCLI: TStringField;
    ptPartINDTYPE: TSmallintField;
    ptPartINQPART: TFloatField;
    ptPartINQREMN: TFloatField;
    ptPartINPRICEIN: TFloatField;
    ptPartINPRICEOUT: TFloatField;
    ptPartOARTICUL: TIntegerField;
    ptPartOIDATE: TIntegerField;
    ptPartOIDSTORE: TSmallintField;
    ptPartOITYPE: TSmallintField;
    ptPartOIDDOC: TIntegerField;
    ptPartOPARTIN: TIntegerField;
    ptPartOID: TAutoIncField;
    ptPartOQUANT: TFloatField;
    ptPartOSUMIN: TFloatField;
    ptPartOSUMOUT: TFloatField;
    ptPartOSINN: TStringField;
    ptPool: TPvTable;
    ptPoolCodeObject: TSmallintField;
    ptPoolId: TIntegerField;
    ptInvLine1: TPvTable;
    ptInvLine1IDH: TBCDField;
    ptInvLine1ITEM: TIntegerField;
    ptInvLine1QUANTR: TFloatField;
    ptInvLine1QUANTF: TFloatField;
    ptInvLine1QUANTD: TFloatField;
    ptInvLine1PRICER: TFloatField;
    ptInvLine1SUMR: TFloatField;
    ptInvLine1SUMF: TFloatField;
    ptInvLine1SUMD: TFloatField;
    ptInvLine1SUMRSS: TFloatField;
    ptInvLine1SUMFSS: TFloatField;
    ptInvLine1SUMDSS: TFloatField;
    ptInvLine1NUM: TIntegerField;
    ptAkc: TPvTable;
    ptAkcCode: TIntegerField;
    ptAkcAType: TSmallintField;
    ptAkcDateB: TDateField;
    ptAkcDateE: TDateField;
    ptAkcPrice: TFloatField;
    ptAkcDStop: TSmallintField;
    ptAkcDProc: TFloatField;
    ptAkcStatusOn: TSmallintField;
    ptAkcStatusOf: TSmallintField;
    ptAkcOldPrice: TFloatField;
    ptAkcOldDStop: TSmallintField;
    ptAkcOldDProc: TFloatField;
    ptVnLn: TPvTable;
    ptVnLnDepart: TSmallintField;
    ptVnLnDateInvoice: TDateField;
    ptVnLnNumber: TStringField;
    ptVnLnCodeGroup: TSmallintField;
    ptVnLnCodePodgr: TSmallintField;
    ptVnLnCodeTovar: TIntegerField;
    ptVnLnCodeEdIzm: TSmallintField;
    ptVnLnTovarType: TSmallintField;
    ptVnLnBarCode: TStringField;
    ptVnLnKolMest: TIntegerField;
    ptVnLnKolEdMest: TFloatField;
    ptVnLnKolWithMest: TFloatField;
    ptVnLnKol: TFloatField;
    ptVnLnCenaTovarMove: TFloatField;
    ptVnLnCenaTovarSpis: TFloatField;
    ptVnLnProcent: TFloatField;
    ptVnLnNewCenaTovar: TFloatField;
    ptVnLnSumCenaTovarSpis: TFloatField;
    ptVnLnSumNewCenaTovar: TFloatField;
    ptVnLnCodeTara: TIntegerField;
    ptVnLnVesTara: TFloatField;
    ptVnLnCenaTara: TFloatField;
    ptVnLnSumVesTara: TFloatField;
    ptVnLnSumCenaTara: TFloatField;
    ptVnLnChekBuhPrihod: TBooleanField;
    ptVnLnChekBuhRashod: TBooleanField;
    ptBar: TPvTable;
    ptBarGoodsID: TIntegerField;
    ptBarID: TStringField;
    ptBarQuant: TFloatField;
    ptBarBarFormat: TSmallintField;
    ptBarDateChange: TDateField;
    ptBarBarStatus: TSmallintField;
    ptBarCost: TFloatField;
    ptBarStatus: TSmallintField;
    ptTO: TPvTable;
    ptTOOtdel: TSmallintField;
    ptTOxDate: TDateField;
    ptTOOldOstatokTovar: TFloatField;
    ptTOPrihodTovar: TFloatField;
    ptTOSelfPrihodTovar: TFloatField;
    ptTOPrihodTovarSumma: TFloatField;
    ptTORashodTovar: TFloatField;
    ptTOSelfRashodTovar: TFloatField;
    ptTORealTovar: TFloatField;
    ptTOSkidkaTovar: TFloatField;
    ptTOSenderSumma: TFloatField;
    ptTOSenderSkidka: TFloatField;
    ptTOOutTovar: TFloatField;
    ptTORashodTovarSumma: TFloatField;
    ptTOPereoTovarSumma: TFloatField;
    ptTONewOstatokTovar: TFloatField;
    ptTOOldOstatokTara: TFloatField;
    ptTOPrihodTara: TFloatField;
    ptTOSelfPrihodTara: TFloatField;
    ptTOPrihodTaraSumma: TFloatField;
    ptTORashodTara: TFloatField;
    ptTOSelfRashodTara: TFloatField;
    ptTORezervSumma: TFloatField;
    ptTORezervSkidka: TFloatField;
    ptTOOutTara: TFloatField;
    ptTORashodTaraSumma: TFloatField;
    ptTOxRezerv: TFloatField;
    ptTONewOstatokTara: TFloatField;
    ptTOOprihod: TBooleanField;
    ptGdsCli: TPvTable;
    ptGdsCliClientTypeId: TSmallintField;
    ptGdsCliClientId: TIntegerField;
    ptGdsCliGoodsId: TIntegerField;
    ptGdsCliPriceIn: TFloatField;
    ptGdsCliQuantIn: TFloatField;
    ptGdsClixDate: TDateField;
    ptGdsClisMess: TStringField;
    ptZHead: TPvTable;
    ptZSpec: TPvTable;
    ptZHeadID: TAutoIncField;
    ptZHeadDOCDATE: TDateField;
    ptZHeadDOCNUM: TStringField;
    ptZHeadDEPART: TIntegerField;
    ptZHeadITYPE: TSmallintField;
    ptZHeadINSUMIN: TFloatField;
    ptZHeadINSUMOUT: TFloatField;
    ptZHeadOUTSUMIN: TFloatField;
    ptZHeadOUTSUMOUT: TFloatField;
    ptZHeadCLITYPE: TSmallintField;
    ptZHeadCLICODE: TIntegerField;
    ptZHeadIACTIVE: TSmallintField;
    ptZSpecIDH: TIntegerField;
    ptZSpecIDS: TAutoIncField;
    ptZSpecCODE: TIntegerField;
    ptZSpecQUANT: TFloatField;
    ptZSpecPRICEIN: TFloatField;
    ptZSpecSUMIN: TFloatField;
    ptZSpecNDSPROC: TFloatField;
    ptZSpecREMN1: TFloatField;
    ptZSpecVREAL: TFloatField;
    ptZSpecREMNDAY: TFloatField;
    ptZSpecREMNMIN: TIntegerField;
    ptZSpecREMNMAX: TIntegerField;
    ptZSpecREMN2: TFloatField;
    ptZSpecQUANT1: TFloatField;
    ptZSpecPK: TFloatField;
    ptZSpecQUANTZ: TFloatField;
    ptZSpecQUANT2: TFloatField;
    ptZSpecQUANT3: TFloatField;
    ptZHeadDATEEDIT: TDateField;
    ptZHeadPERSON: TStringField;
    ptZHeadTIMEEDIT: TStringField;
    teAvSp: TdxMemData;
    teAvSpiDep: TIntegerField;
    teAvSpiPrih: TIntegerField;
    teAvSpiDateP: TIntegerField;
    teAvSpQIn: TFloatField;
    teAvSpQReal: TFloatField;
    teAvSpQRemn: TFloatField;
    teAvSpiDateReal: TIntegerField;
    teAvSprAvrR: TFloatField;
    teAvSpkDov: TFloatField;
    dsteAvSp: TDataSource;
    teAvSpdDateP: TStringField;
    ptTTNInSummaTovarPost: TFloatField;
    ptTTNInOblNDSTovar: TFloatField;
    ptTTNInLgtNDSTovar: TFloatField;
    ptTTNInNotNDSTovar: TFloatField;
    ptTTNInNDS10: TFloatField;
    ptTTNInOutNDS10: TFloatField;
    ptTTNInNDS20: TFloatField;
    ptTTNInOutNDS20: TFloatField;
    ptTTNInLgtNDS: TFloatField;
    ptTTNInOutLgtNDS: TFloatField;
    ptTTNInNDSTovar: TFloatField;
    ptTTNInOutNDSTovar: TFloatField;
    ptTTNInReturnTovar: TFloatField;
    ptTTNInOutNDSSNTovar: TFloatField;
    ptTTNInBoy: TFloatField;
    ptTTNInOthodiPost: TFloatField;
    ptTTNInDolgPost: TFloatField;
    ptTTNInSummaTovar: TFloatField;
    ptTTNInNacenka: TFloatField;
    ptTTNInTransport: TFloatField;
    ptTTNInNDSTransport: TFloatField;
    ptTTNInReturnTara: TFloatField;
    ptTTNInStrah: TFloatField;
    ptTTNInSummaTara: TFloatField;
    ptTTNInAmortTara: TFloatField;
    ptTTNInChekBuh: TWordField;
    ptTTNInProvodType: TWordField;
    ptTTNInNotNDS10: TFloatField;
    ptTTNInNotNDS20: TFloatField;
    ptTTNInWithNDS10: TFloatField;
    ptTTNInWithNDS20: TFloatField;
    ptTTNInCrock: TFloatField;
    ptTTNInOthodiPoluch: TFloatField;
    ptTTNInDiscount: TFloatField;
    ptTTNInNDS010: TFloatField;
    ptTTNInNDS020: TFloatField;
    ptTTNInNDS_10: TFloatField;
    ptTTNInNDS_20: TFloatField;
    ptTTNInNSP_10: TFloatField;
    ptTTNInNSP_20: TFloatField;
    ptTTNInSCFDate: TDateField;
    ptTTNInGoodsNSP0: TFloatField;
    ptTTNInGoodsCostNSP: TFloatField;
    ptTTNInOrderNumber: TIntegerField;
    ptTTNInLinkInvoice: TWordField;
    ptTTNInStartTransfer: TWordField;
    ptTTNInEndTransfer: TWordField;
    ptTTNInGoodsWasteNDS0: TFloatField;
    ptTTNInGoodsWasteNDS10: TFloatField;
    ptTTNInGoodsWasteNDS20: TFloatField;
    ptTTNInREZERV: TStringField;
    ptZHeadCLIQUANT: TFloatField;
    ptZHeadCLISUMIN0: TFloatField;
    ptZHeadCLISUMIN: TFloatField;
    ptZHeadCLIQUANTN: TFloatField;
    ptZHeadCLISUMIN0N: TFloatField;
    ptZHeadCLISUMINN: TFloatField;
    ptZHeadIDATENACL: TIntegerField;
    ptZHeadSNUMNACL: TStringField;
    ptZHeadIDATESCHF: TIntegerField;
    ptZHeadSNUMSCHF: TStringField;
    ptZSpecQUANTPODTV: TFloatField;
    ptZSpecCLIQUANT: TFloatField;
    ptZSpecCLIPRICE: TFloatField;
    ptZSpecCLIPRICE0: TFloatField;
    ptZSpecCLIQUANTN: TFloatField;
    ptZSpecCLIPRICEN: TFloatField;
    ptZSpecCLIPRICE0N: TFloatField;
    ptScale: TPvTable;
    ptScaleNumber: TStringField;
    ptScaleModel: TStringField;
    ptScaleName: TStringField;
    ptScaleHubNumber: TWordField;
    ptScalePLUCount: TSmallintField;
    ptScaleRezerv1: TIntegerField;
    ptScaleRezerv2: TIntegerField;
    ptScaleRezerv3: TIntegerField;
    ptScaleRezerv4: TIntegerField;
    ptScaleRezerv5: TIntegerField;
    ptScaleRezerv6: TIntegerField;
    ptScaleRezerv7: TIntegerField;
    ptScaleRezerv8: TIntegerField;
    ptScaleRezerv9: TIntegerField;
    ptScaleRezerv10: TIntegerField;
    ptKadri: TPvTable;
    ptKadriTabNumber: TStringField;
    ptKadriKartNumber: TSmallintField;
    ptKadriFamilia: TStringField;
    ptKadriIma: TStringField;
    ptKadriOtchestvo: TStringField;
    ptOutLn: TPvTable;
    ptOutLnDepart: TSmallintField;
    ptOutLnDateInvoice: TDateField;
    ptOutLnNumber: TStringField;
    ptOutLnCodeGroup: TSmallintField;
    ptOutLnCodePodgr: TSmallintField;
    ptOutLnCodeTovar: TIntegerField;
    ptOutLnCodeEdIzm: TSmallintField;
    ptOutLnTovarType: TSmallintField;
    ptOutLnBarCode: TStringField;
    ptOutLnOKDP: TFloatField;
    ptOutLnNDSProc: TFloatField;
    ptOutLnNDSSum: TFloatField;
    ptOutLnOutNDSSum: TFloatField;
    ptOutLnAkciz: TFloatField;
    ptOutLnKolMest: TIntegerField;
    ptOutLnKolEdMest: TFloatField;
    ptOutLnKolWithMest: TFloatField;
    ptOutLnKol: TFloatField;
    ptOutLnCenaPost: TFloatField;
    ptOutLnCenaTovar: TFloatField;
    ptOutLnCenaTovarSpis: TFloatField;
    ptOutLnSumCenaTovar: TFloatField;
    ptOutLnSumCenaTovarSpis: TFloatField;
    ptOutLnCodeTara: TIntegerField;
    ptOutLnVesTara: TFloatField;
    ptOutLnCenaTara: TFloatField;
    ptOutLnCenaTaraSpis: TFloatField;
    ptOutLnSumVesTara: TFloatField;
    ptOutLnSumCenaTara: TFloatField;
    ptOutLnSumCenaTaraSpis: TFloatField;
    ptOutLnChekBuh: TBooleanField;
    ptInvLine1QUANTC: TFloatField;
    ptInvLine1SUMC: TFloatField;
    ptInvLine1SUMRSC: TFloatField;
    ptGrafPost: TPvTable;
    ptGrafPostICLI: TIntegerField;
    ptGrafPostIDEP: TIntegerField;
    ptGrafPostIDATEP: TIntegerField;
    ptGrafPostIDATEZ: TIntegerField;
    ptPartINPRICEIN0: TFloatField;
    ptPartOSUMIN0: TFloatField;
    ptPartOIDC: TIntegerField;
    ptPartINIDC: TIntegerField;
    ptZSpecCLINNUM: TIntegerField;
    ptCountry: TPvTable;
    ptCountry1: TPvTable;
    ptCountry1ID: TIntegerField;
    ptCountry1NAME: TStringField;
    ptCountry1ABR: TStringField;
    ptCountry1CODE: TStringField;
    ptCountryID: TAutoIncField;
    ptCountryName: TStringField;
    ptGTD: TPvTable;
    ptGTDIDH: TIntegerField;
    ptGTDID: TIntegerField;
    ptGTDCODE: TIntegerField;
    ptGTDGTD: TStringField;
    dsquCatDisc: TDataSource;
    teCatDisc: TdxMemData;
    teCatDiscIDgr: TIntegerField;
    teCatDiscIDsg: TIntegerField;
    teCatDiscID: TIntegerField;
    teCatDiscName: TStringField;
    teCatDiscV01: TSmallintField;
    meMoveNum: TIntegerField;
    taAlgClass: TPvTable;
    taAlgClassID: TIntegerField;
    taAlgClassNAMECLA: TStringField;
    ptMakers: TPvTable;
    ptMakersID: TAutoIncField;
    ptMakersNAMEM: TStringField;
    ptMakersINNM: TStringField;
    ptMakersKPPM: TStringField;
    ptCliLic: TPvTable;
    ptCliLicITYPE: TSmallintField;
    ptCliLicICLI: TIntegerField;
    ptCliLicIDATEB: TIntegerField;
    ptCliLicDDATEB: TDateField;
    ptCliLicIDATEE: TIntegerField;
    ptCliLicDDATEE: TDateField;
    ptCliLicSER: TStringField;
    ptCliLicSNUM: TStringField;
    ptCliLicORGAN: TStringField;
    ptCliLicIDCLI: TIntegerField;
    teClassrNac: TFloatField;
    ptKadr: TPvTable;
    ptKadrID: TAutoIncField;
    ptKadrNAMECASSIR: TStringField;
    ptKadrPASSW: TStringField;
    ptKadrIACTIVE: TSmallintField;
    ptKadrISPEC: TSmallintField;
    ptCardsSaveAlco: TPvTable;
    ptCardsSaveAlcoID: TIntegerField;
    ptCardsSaveAlcoIGR1: TIntegerField;
    ptCardsSaveAlcoIGR2: TIntegerField;
    ptCardsSaveAlcoIGR3: TIntegerField;
    ptCardsSaveAlcoV10: TIntegerField;
    ptCardsSaveAlcoV11: TIntegerField;
    ptCardsSaveAlcoV12: TIntegerField;
    ptAlcoDHRec: TPvTable;
    ptAlcoDHRecIdOrg: TIntegerField;
    ptAlcoDHRecId: TIntegerField;
    ptAlcoDHRecIDateB: TIntegerField;
    ptAlcoDHRecIDateE: TIntegerField;
    ptAlcoDHRecDDateB: TDateField;
    ptAlcoDHRecDDateE: TDateField;
    ptAlcoDHRecIType: TIntegerField;
    ptAlcoDHRecsType: TStringField;
    ptAlcoDHRecSPers: TStringField;
    ptAlcoDS1: TPvTable;
    ptAlcoDS1IdH: TIntegerField;
    ptAlcoDS1Id: TAutoIncField;
    ptAlcoDS1iDep: TIntegerField;
    ptAlcoDS1iNum: TIntegerField;
    ptAlcoDS1iVid: TIntegerField;
    ptAlcoDS1iProd: TIntegerField;
    ptAlcoDS1rQIn1: TFloatField;
    ptAlcoDS1rQIn2: TFloatField;
    ptAlcoDS1rQIn3: TFloatField;
    ptAlcoDS1rQInIt1: TFloatField;
    ptAlcoDS1rQIn4: TFloatField;
    ptAlcoDS1rQIn5: TFloatField;
    ptAlcoDS1rQIn6: TFloatField;
    ptAlcoDS1rQInIt: TFloatField;
    ptAlcoDS1rQOut1: TFloatField;
    ptAlcoDS1rQOut2: TFloatField;
    ptAlcoDS1rQOut3: TFloatField;
    ptAlcoDS1rQOut4: TFloatField;
    ptAlcoDS1rQOutIt: TFloatField;
    ptAlcoDS1rQe: TFloatField;
    ptAlcoDS2: TPvTable;
    ptAlcoDS2IdH: TIntegerField;
    ptAlcoDS2Id: TAutoIncField;
    ptAlcoDS2iDep: TIntegerField;
    ptAlcoDS2iVid: TIntegerField;
    ptAlcoDS2iProd: TIntegerField;
    ptAlcoDS2iPost: TIntegerField;
    ptAlcoDS2iLic: TIntegerField;
    ptAlcoDS2DocDate: TDateField;
    ptAlcoDS2DocNum: TStringField;
    ptAlcoDS2GTD: TStringField;
    ptAlcoDS2rQIn: TFloatField;
    ptAlcoDS2LicNum: TStringField;
    ptAlcoDS2LicDateB: TDateField;
    ptAlcoDS2LicDateE: TDateField;
    ptAlcoDS2LicOrg: TStringField;
    ptAlcoDS2IdHdr: TIntegerField;
    ptAlcoDS1rQb: TFloatField;
    ptGroupCryst: TPvTable;
    ptGroupCrystOtdel: TSmallintField;
    ptGroupCrystID: TAutoIncField;
    ptGroupCrystName: TStringField;
    ptGroupCrystFullName: TStringField;
    ptGroupCrystReserv1: TFloatField;
    ptKadrITYPE: TSmallintField;
    ptKadrSDOC: TStringField;
    ptKadrSDOLG: TStringField;
    ptKadrIKASSIR: TSmallintField;
    ptInvLine2: TPvTable;
    ptInvLine2IDH: TBCDField;
    ptInvLine2ITEM: TIntegerField;
    ptInvLine2QUANTR: TFloatField;
    ptInvLine2QUANTF: TFloatField;
    ptInvLine2QUANTD: TFloatField;
    ptInvLine2PRICER: TFloatField;
    ptInvLine2SUMR: TFloatField;
    ptInvLine2SUMF: TFloatField;
    ptInvLine2SUMD: TFloatField;
    ptInvLine2SUMRSS: TFloatField;
    ptInvLine2SUMFSS: TFloatField;
    ptInvLine2SUMDSS: TFloatField;
    ptInvLine2NUM: TIntegerField;
    ptInvLine2QUANTC: TFloatField;
    ptInvLine2SUMC: TFloatField;
    ptInvLine2SUMRSC: TFloatField;
    ptCashPath: TPvTable;
    ptCashPathID: TIntegerField;
    ptCashPathPATH: TMemoField;
    ptUPL: TPvTable;
    ptUPLID: TIntegerField;
    ptUPLIMH: TIntegerField;
    ptUPLNAME: TStringField;
    ptUPLIACTIVE: TSmallintField;
    ptPersonal: TPvTable;
    ptPersonalID: TIntegerField;
    ptPersonalID_PARENT: TIntegerField;
    ptPersonalNAME: TStringField;
    ptPersonalUVOLNEN: TSmallintField;
    ptPersonalPASSW: TStringField;
    ptPersonalMODUL1: TSmallintField;
    ptPersonalMODUL2: TSmallintField;
    ptPersonalMODUL3: TSmallintField;
    ptPersonalMODUL4: TSmallintField;
    ptPersonalMODUL5: TSmallintField;
    ptPersonalMODUL6: TSmallintField;
    ptPersonalBARCODE: TStringField;
    ptNum: TPvTable;
    ptNumITYPE: TSmallintField;
    ptNumDNAME: TStringField;
    ptNumINUM: TIntegerField;
    ptNumSNUM: TStringField;
    taAlgClassGetAM: TSmallintField;
    ptKadriCODE: TIntegerField;
    procedure ptCqRepCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure ptPluLimCalcFields(DataSet: TDataSet);
    procedure ptCashsCalcFields(DataSet: TDataSet);
    procedure teAvSpCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function prFindTabRemn(iC:Integer):Real;
function prFindTCGRemn(iC:Integer):Real;

function prFindLastDateRemn(iC:Integer;Var LDate:TDateTime):Real;

procedure prSetR(iC:INteger;rQ:Real);
function prFindTRemnDep(iC,iD:Integer):Real;
function prFindTLastDep(iC:Integer):Integer;
procedure prAddTMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
procedure prFillMove(iC:INteger);
procedure prDelTMoveIdC(IdCard,iDate,iDoc,iTD:Integer);
procedure prDelTMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
function prFindTNameMGr(iGr:INteger):String;
Function prFindTNameGr(iGr:Integer):String;
procedure prCalcTMove(iCg:INteger;Var rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb:Real);
Function prFindPrice(iC:INteger):Real;
Function prFindReceipt(iC:INteger):String;
function prFindTRemnDepD(iC,iD,dD:Integer):Real;
procedure prCalcTaraMove(iT,iD:INteger;Var rQb,rQIn,rSIn,rQOut,rSOut:Real);

procedure prReCalcTMove(iCg,iDate:INteger);//������� �������� ��������������
function prReCalcTMoveLine(iC:INteger):Integer;//�������� �� ������������� ��������� ��������������


procedure prDelTMoveDate(Depart,IdCard,iDate,iTD:Integer);

procedure prFindNacGr(iC:Integer;Var n1,n2,n3,rPr:Real);
Function prFindTNameGroup(iGr:Integer):String;

function prFindTabSpeedRemn(iC,iTest:Integer;Var rSpeedDay,rQday:Real):Real;
function prFindTabSpeedRemn1(iC,InTime,nDay,DateB,DateE:Integer;Var rSpeedDay,rQday:Real):Real;
//��� ������� ������� �������
//function prFindTabSpeedRemn2(iC,DateB,DateE:Integer;Var rSpeedDay:Real):Real;
function prFindTabSpeedRemn2(iC,DateB,DateE:Integer;Var rSpeedDay,rQIn,rQReal:Real):Real;

function prFindTLastDate(iC:Integer):INteger;
function prFindPricePP(iC,iDep:INteger):Real; //�� ��������� �������


//����

procedure prDelTaraMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
function prFindTaraRemn(iC:Integer):Real;
procedure prSetRTara(iC:INteger;rQ:Real);
procedure prAddTaraMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ,rPr:Real);
procedure prRecalcTaraRemns(iDate:INteger);

Function prFLPT(Id,IndP,CodP:INteger;Var PostDate:TDateTime;Var PostNum:String; Var PostQ,rPriceIn0:Real;Var GTD:String):Real;
function InventryCheck(Date:tdatetime;depart:integer;Articul:integer):boolean;
procedure prTPriceBuf(IdCard,iDate,IdDoc,iTypeDoc:Integer;DocNum:String;Pr1,Pr2:Real);
function PostNum(iTDoc:INteger):INteger;

function fShop:INteger;
function fShopB:INteger;
function fShopName:String;
function fSingle:INteger;

procedure prAddZadan(iType,Id:INteger;S:String);
procedure prAddCashLoad(iC,iT:INteger;rPr,rD:Real);
function prFindTRemnDepDInv(iC,iD,dD:Integer):Real;
function prCalcPriceRemn(iC:INteger;rPr,rQ,rRemn:Real;iDate:INteger):Real;

Function prFindPriceDate(iC,iDate:INteger):Real;

Procedure prCalcPriceOut(iC,iDate:INteger;rPrIn,rQ:Real;Var rn1,rn2,rn3,rPr,rPrA:Real);
Procedure prCalcPriceOut1(iC,iDate:INteger;rPrIn,rQ:Real;Var rn1,rn2,rn3,rPr,rPrA:Real);

function prFindTFirstDate(iC:Integer):INteger;

procedure prDelPartInDoc(iDoc,iType:Integer);
function prDelPartO(iDoc,iType,iCode:Integer):Boolean;

function prFixPartRemn(iCode,iDate:Integer):Boolean; //�� ����

function prAddPartIn(iDoc,iType,iCode,iDate,iSkl:Integer;sInn:String;rQ,rPrIn,rPrOut,rPrIn0:Real;iCli:Integer):Boolean;
function prCalcPartO(iDoc,iType,iCode,iDate,iSkl:Integer;bWr:Boolean;rQo,rPrOut,rSumOut,rPrCli,rPrCli0:Real;sInnCli:String;var rSumIn,rSumIn0:Real;Var sInnIn:String; Var iCli:INteger):Boolean;
Function prFLPriceDateT(iCode,iDate:INteger;Var rPr0:Real):Real; //���������� ���� ���������� ������� �� ����
Function prFLPriceDateT1(iCode,iDate:INteger;var iDateIn:INteger; Var rPr0:Real):Real; //+���� ���������� �������
Function prFLPriceDateTVn(iCode,iDate:INteger;Var rPr0:Real):Real;

procedure prCalcPrib(iCode,iDateB,iDateE,iDep:INteger; Var rQReal,rSumIn,rSumOut:Real);

function prTestReCalcTMoveLine(iC:INteger):INteger;//�������� �� ������������� ��������� ��������������

function prDelPartIn(iDoc,iType,iDate,iSkl:Integer):Boolean;
function prReCalcPartO(iPartIn,iCode,iDate,iSkl:Integer):Boolean;

procedure prFillPart(iCode:INteger;sName:String);

Function prFindCliName(sInn:String):String;
Function prFindCliName1(iT,iC:Integer;Var Sinn:String; Var iCli:INteger):String;
Function prFindCliName2(iT,iC:Integer;Var Sinn,Skpp:String; Var iCli:INteger):String;

Function prFindPriceDateMC(iC,iDate:INteger):Real;  //��������� ������������� ���� �� ����
Function prFindCliCodecb(sInn:String):Integer;
Function prFindGLNCli(iCli:Integer):String;

Function prRetDName(iC:Integer):String;
procedure prSetTA(iDate:Integer);
procedure prReSetTA(iDate:Integer);
function prGetTA:Integer;

function prGetNum(iCode:SmallINt):INteger;
function prGetANum(iCode:SmallINt):INteger;

function prCalcSumInRemn(iCode,iDate,iSkl:Integer;rQr:Real; Var rSumIn:Real):Boolean;
procedure prCorrPartIn(iCode:Integer);
function prFindBAkciya(iC,iDB:INteger):INteger;
function prFindBAkciya1(iC,iDB:INteger; Var dDa,dDa1,bTop:Integer):INteger;
function prFindPriceAfterA(iC,iDB:INteger; Var rPrAfter:Real):Boolean;

procedure prRecalcSS(DateB,DateE:INteger; Memo1:tcxMemo);
function fSS(iDep:INteger):Integer;
function fTestInv(iDep,iDate:Integer):Boolean;
function fTestInvBack(iDep,iDate:Integer):Integer;
function sNumPost(iNum:INteger):String;
function prFindFullName(iDep:INteger):String;
Function prSDisc(iC:Integer):String;
function fSCat(iC:INteger):String;
function prFindQzAlready(iCode,iDateDoc:Integer):Real;

procedure prCalcTMoveAllDep(iC,iD:INteger;Var rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb:Real);
procedure prFillReal10(iC,iDe,iDCount:INteger);
procedure prNormPartInv(iCode,iDate,iDep:Integer;rQf:Real);

function prFindLastDateRemnDep(iC,iDep:Integer;Var LDate:TDateTime):Real;
procedure prFillRemn30(iC,iDe,iDCount:INteger);
procedure prFindCorrSum(Code,iD,iDate:INteger;Var rQuantC,rSumC,rSumRSC:Real; Memo1:tcxMemo);

function prCliNDS(iType,iCode,iCodeCB:Integer):Integer; // 1- ���������� ���   0 - �� ����������
procedure prFindTCountry(iCode:INteger; Var sCCode,sCountry:String);

function prFindPricePP0(iC,iDep:INteger;Var Price0:Real):Real;
Procedure ClassExpNewT( Node : TTreeNode; Tree:TTreeView);
procedure prReadClass;

function prFindMakerName(iM:Integer):String;


var
  dmMT: TdmMT;

//Const

//Btr1Path:String = '\\btr1\k$\SCrystal\data';

//Btr1Path:String = '\\server\d$\CRYSTAL_DOS\crystal\DATA';

implementation

uses MDB, Un1, u2fdk, Parts, dmPS;

{$R *.dfm}

function prFindMakerName(iM:Integer):String;
begin
  with dmMT do
  begin
    if iM>0 then
    begin
      if ptMakers.Active=False then ptMakers.Active:=True;
      Result:='';
      if ptMakers.FindKey([iM]) then Result:=OemToAnsiConvert(ptMakersNAMEM.AsString);
    end else Result:='';
  end;
end;

procedure prReadClass;
begin
  with dmMT do
  begin
    CloseTe(teClass);
    teClass.Active:=True;
    if ptSGr.Active=False then ptSGr.Active:=True;

    if Commonset.ClassOld=0 then // ����������� �������
    begin
      ptSGr.IndexFieldNames:='Name';

      ptSGr.First;
      while not ptSGr.Eof do
      begin
        teClass.Append;
        teClassId.AsInteger:=ptSGrID.AsInteger;
        teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
        teClassNameClass.AsString:=OemToAnsiConvert(ptSGrName.AsString);
        teClassrNac.AsFloat:=ptSGrReserv1.AsFloat;
        teClass.Post;

        ptSGr.next;
      end;

      ptSGr.IndexFieldNames:='ID';
    end;
    if Commonset.ClassOld=1 then // ��� ������ ������������� ��������
    begin
      if ptGroupCryst.Active=False then ptGroupCryst.Active:=True;

      ptGroupCryst.First;
      while not ptGroupCryst.Eof do
      begin
        teClass.Append;
        teClassId.AsInteger:=ptGroupCrystID.AsInteger;
        teClassIdParent.AsInteger:=0;
        teClassNameClass.AsString:=OemToAnsiConvert(ptGroupCrystName.AsString);
        teClassrNac.AsFloat:=ptGroupCrystReserv1.AsFloat;
        teClass.Post;

        ptGroupCryst.next;
      end;

      ptGroupCryst.Active:=False;

      ptSGr.IndexFieldNames:='Name';

      ptSGr.First;
      while not ptSGr.Eof do
      begin
        if ptSGrGoodsGroupID.AsInteger>0 then
        begin
          teClass.Append;
          teClassId.AsInteger:=ptSGrID.AsInteger;
          teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
          teClassNameClass.AsString:=OemToAnsiConvert(ptSGrName.AsString);
          teClassrNac.AsFloat:=ptSGrReserv1.AsFloat;
          teClass.Post;
        end;

        ptSGr.next;
      end;

      ptSGr.IndexFieldNames:='ID';

    end;
//    ptSGr.Active:=False;
  end;
end;


Procedure ClassExpNewT( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);

  with dmMT do
  begin
    if CommonSet.ClassType=1 then   //���������������
    begin
      while teClass.Locate('IdParent',ID,[]) do
      begin
        TreeNode:=Tree.Items.AddChildObject(Node,teClassNameClass.AsString+' ('+fs(teClassrNac.AsFloat)+'%)', Pointer(teClassId.AsInteger));
        TreeNode.ImageIndex:=12;
        TreeNode.SelectedIndex:=11;

        teClass.Delete;

        ClassExpNewT(TreeNode,Tree);
      end;
    end;

  if CommonSet.ClassType=0 then
  begin
   { quC:=TPvQuery.Create(Application);
    quC.Active:=False;
    quC.DatabaseName:='PSQL';
    quC.SQL.Clear;

    if ID=0 then
    begin
      quC.SQL.Add('select 0 as GoodsGroupID,(ID+10000) as ID,Name,Reserv1  from "GdsGroup"');
      quC.SQL.Add('order by Name');
    end else
    begin
      quC.SQL.Add('select  GoodsGroupID,ID,Name,Reserv1 from "SubGroup"');
      quC.SQL.Add('where GoodsGroupID='+IntToStr(Id-10000));
      quC.SQL.Add('order by Name');
    end;

    quC.Active:=True;
    quC.First;
    while not quC.Eof do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node, quC.FieldByName('Name').AsString+' ( ���. '+fs(quC.FieldByName('Reserv1').AsFloat)+'%)', Pointer(quC.FieldByName('Id').AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      ClassExpNew(TreeNode,Tree);

      quC.Next;
    end;
    quC.Active:=False;
    quC.Free;}
  end;
  end;
end;


procedure prFindTCountry(iCode:INteger; Var sCCode,sCountry:String);
begin
  with dmMT do
  begin
    sCCode:=''; sCountry:='';
    if taCards.findkey([iCode]) then
    begin
      if ptCountry1.Active=False then ptCountry1.Active:=True;
      if ptCountry1.FindKey([taCardsCountry.AsInteger]) then
      begin
        sCCode:=OemToAnsiConvert(ptCountry1CODE.AsString);
        sCountry:=OemToAnsiConvert(ptCountry1NAME.AsString);
      end;
    end;
  end;
end;

function prCliNDS(iType,iCode,iCodeCB:Integer):Integer; // 1- ���������� ���   0 - �� ����������
begin
  Result:=0;
  with dmMT do
  begin
    if ptIP.Active=false then ptIP.Active:=true;
    if ptCli.Active=false then ptCli.Active:=true;
    if iType=1 then
    begin
      if ptCli.Locate('Vendor',iCode,[]) then
      begin
        if ptCliUnTaxedNDS.AsInteger=0 then Result:=1 else Result:=0; 
      end;
    end;
    if iType=2 then
    begin
      Result:=0;
{      if ptIP.Locate('Code',iCode,[]) then
      begin
        StrO:=OemToAnsiConvert(ptIPName.AsString);
        Sinn:=ptIPINN.AsString;
        iCli:=0;
      end;}
    end;
  end;
end;

procedure prFillRemn30(iC,iDe,iDCount:INteger);
Var i,iCurD:Integer;
    dDateB,dDateE:TDateTime;
//    rRemn:Real;
begin
  with dmMT do
  begin
    for i:=1 to iDCount do arRD[i]:=0;

    dDateB:=iDe-iDCount+1;
    dDateE:=iDe;
    iCurD:=0;

    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      ptCM.Last;
      while (ptCMMOVEDATE.AsDateTime>=dDateB)and(ptCM.Bof=False) do
      begin
        if ptCMMOVEDATE.AsDateTime<=dDateE then
        begin
          i:=Trunc(ptCMMOVEDATE.AsDateTime)-Trunc(dDateB)+1;
          if i<>iCurD then
          begin //���� ���������
            arRD[i]:=arRD[i]+ptCMQUANT_REST.AsFloat;  //������ ������ ����� ����� ���� ���� � �������
            iCurD:=i;
          end;
        end;

        ptCM.Prior;
      end;

      ptCG.Next;
    end;
  end;
end;


procedure prNormPartInv(iCode,iDate,iDep:Integer;rQf:Real); //������������ ������ �� ��������������
Var rQp,rQs,rQ:Real;
begin
  with dmMT do
  begin
    if ptDep.Active=False then ptDep.Active:=True;
    if ptPartO.Active=False then ptPartO.Active:=True;

    if ptPartIN.Active=False then ptPartIn.Active:=True;
    ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';

    ptDep.First;
    while not ptDep.Eof do
    begin
      if (ptDepStatus1.AsInteger=9)and(ptDepId.AsInteger=iDep) then
      begin // ������ ������ �����

        if CommonSet.CalcSeb<2 then //CommonSet.CalcSeb=1 - �� ���������
        begin
          rQs:=rQf;
          if rQs<=0 then rQs:=0; //������ 0 ����� �����������

          if rQs>0 then
          begin
            ptPartIn.CancelRange;
            ptPartIn.SetRange([ptDepId.AsInteger,iCode,iDate],[ptDepId.AsInteger,iCode,iDate-365]);
            ptPartIn.First;
            while (ptPartIn.Eof=False)and(abs(rQs)>0.001) do
            begin
              rQp:=ptPartINQPART.AsFloat;

              if rQs<=rQp then
              begin
                rQ:=rQs;//��������� ������ ������ ���������
              end else
              begin
                rQ:=rQp;
              end;
              rQs:=rQs-rQ;

              ptPartIn.Edit;
              ptPartINQREMN.AsFloat:=rQ;
              ptPartIn.Post;

              ptPartIn.Next;
            end;
            if ptPartIn.Eof=False then //�������� ����������� ������
            begin
              while (ptPartIn.Eof=False)and(ptPartINQREMN.AsFloat>0.0001) do
              begin
                ptPartIn.Edit;
                ptPartINQREMN.AsFloat:=0;
                ptPartIn.Post;

                ptPartIn.Next;
              end;
            end;

          end else //0 �� ������� - ����� �������� ��� ������
          begin
            ptPartIn.CancelRange;
            ptPartIn.SetRange([ptDepId.AsInteger,iCode,iDate],[ptDepId.AsInteger,iCode,(iDate-365)]);
            ptPartIn.First;
            while (ptPartIn.Eof=False)and(ptPartINQREMN.AsFloat>0.0001) do
            begin
              ptPartIn.Edit;
              ptPartINQREMN.AsFloat:=0;
              ptPartIn.Post;

              ptPartIn.Next;
            end;
          end;
        end;
      end;
      ptDep.Next;
    end;
  end;
end;

procedure prFillReal10(iC,iDe,iDCount:INteger);
Var i:Integer;
    dDateB,dDateE:TDateTime;
begin
  with dmMT do
  begin
    for i:=1 to iDCount do arRD[i]:=0;

    dDateB:=iDe-iDCount+1;
    dDateE:=iDe;

    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      ptCM.Last;
      while (ptCMMOVEDATE.AsDateTime>=dDateB)and(ptCM.Bof=False) do
      begin
        if ptCMMOVEDATE.AsDateTime<=dDateE then
        begin
          if ptCMDOC_TYPE.AsInteger in [7] then  //����������
          begin
            i:=Trunc(ptCMMOVEDATE.AsDateTime)-Trunc(dDateB)+1;
            arRD[i]:=arRD[i]+(-1)*ptCMQUANT_MOVE.AsFloat;
          end;
        end;

        ptCM.Prior;
      end;

      ptCG.Next;
    end;
  end;
end;

function prFindQzAlready(iCode,iDateDoc:Integer):Real;
Var rQz:Real;
    iDate,iD:INteger;
begin
  with dmMT do
  begin
    rQz:=0;
    iDate:=Trunc(Date);

    ptZSpec.CancelRange;
    ptZSpec.IndexFieldNames:='CODE;IDH';
    ptZSpec.SetRange([iCode],[iCode]);
    ptZSpec.Last;
    while not ptZSpec.Bof do
    begin
      if ptZHead.FindKey([ptZSpecIDH.AsInteger]) then
      begin
        iD:=Trunc(ptZHeadDOCDATE.AsDateTime);

//        if iD<iDate then break; //�����������

        if (iD<=iDateDoc)and(iD>=iDate) then
        begin
          if (ptZHeadIACTIVE.AsInteger>0)and(ptZHeadIACTIVE.AsInteger<3) then
          begin
            rQz:=rQz+ptZSpecQUANT3.AsFloat;
          end;
        end;
      end;

      ptZSpec.Prior;
    end;

    Result:=rQz;
  end;
end;

function fSCat(iC:INteger):String;
begin
  with dmMT do
  begin
    if ptCateg.Active=False then ptCateg.Active:=True;
//    ptCateg.CancelRange;
//    ptCateg.IndexFieldNames:='ID';

    Result:='';
    if ptCateg.findkey([iC]) then Result:=ptCategSID.AsString;
  end;
end;


function prFindFullName(iDep:INteger):String;
begin
  with dmMT do
  begin
    if ptDep.Active=False then ptDep.Active:=True;
    ptDep.Refresh;
    Result:='';
    if ptDep.FindKey([iDep]) then Result:=OemToAnsiConvert(ptDepFullName.AsString);
  end;
end;

function sNumPost(iNum:INteger):String;
begin
  Result:=its(iNum);
  while Length(Result)<8 do Result:='0'+Result;
  Result:=its(fShop)+'_'+Result;
end;


function fTestInvBack(iDep,iDate:Integer):Integer;
Var DateB,DateE:TDateTime;
    sSt:String;
begin
  with dmMT do
  begin
    Result:=0;
    DateB:=0;
    DateE:=iDate-1;
    if ptInventryHead.Active=False then ptInventryHead.Active:=True;
    ptInventryHead.CancelRange;
    ptInventryHead.SetRange([iDep,DateB],[iDep,DateE]);
    ptInventryHead.Last;
    while not ptInventryHead.Bof do
    begin
      sSt:=OemToAnsiConvert(ptInventryHeadStatus.AsString);
      if (sSt<>'�') and (ptInventryHeadDetail.AsInteger=0) then
      begin
        Result:=Trunc(ptInventryHeadDateInventry.AsDateTime);
        Break;
      end;
      ptInventryHead.Prior;
    end;
  end;
end;

procedure prFindCorrSum(Code,iD,iDate:INteger;Var rQuantC,rSumC,rSumRSC:Real; Memo1:tcxMemo);
Var DateB,DateE:TDateTime;
    sSt:String;
    bMem:Boolean;
    par:Variant;
    par1:Variant;

  function rToVal:String;
  begin
    Result:=' � '+fs(rQuantC)+'; C '+fs(rSumC)+'; CC '+fs(rSumRSC);
  end;

begin
  with dmMT do
  begin
    rQuantC:=0; rSumC:=0; rSumRSC:=0;
    par := VarArrayCreate([0,1], varInteger);
    par1 := VarArrayCreate([0,2], varInteger);

    bMem:=False;
    if Memo1<>nil then bMem:=True;

    DateB:=0;
    DateE:=iDate-1;

    ptInventryHead.CancelRange;
    ptInventryHead.SetRange([iD,DateB],[iD,DateE]);
    ptInventryHead.Last;
    while not ptInventryHead.Bof do
    begin
      sSt:=OemToAnsiConvert(ptInventryHeadStatus.AsString);
      if (sSt<>'�') and (ptInventryHeadDetail.AsInteger=0) //������ ��������������
      then
      begin
        if bMem then Memo1.Lines.Add(' ����� (� ��� ���)'+ds1(ptInventryHeadDateInventry.AsDateTime)+' '+oemtoansiconvert(ptInventryHeadNumber.AsString)+rToVal);
        Break;
      end
      else //����� �� ������ � �����
      begin //������� �� ������
        if (ptInventryHeadItem.AsInteger=0) then //�� ������������ � ��
        begin //���� ������� � ����� ��������������
          if (ptInventryHeadxDopStatus.AsInteger=1) then  //��. � �������������
          begin
            par[0]:=ptInventryHeadInventry.AsInteger;
            par[1]:=Code;

            if ptInvLine1.Locate('IDH;ITEM',par,[]) then
            begin
              rQuantC:=rQuantC+(ptInvLine1QUANTR.AsFloat-ptInvLine1QUANTF.AsFloat);
              rSumC:=rSumC+(ptInvLine1QUANTR.AsFloat-ptInvLine1QUANTF.AsFloat)*ptInvLine1PRICER.AsFloat;
              rSumRSC:=rSumRSC+(ptInvLine1SUMRSS.AsFloat-ptInvLine1SUMFSS.AsFloat);

              if bMem then Memo1.Lines.Add(' ������������ '+its(ptInvLine1IDH.AsInteger)+' '+ds1(ptInventryHeadDateInventry.AsDateTime)+' '+oemtoansiconvert(ptInventryHeadNumber.AsString)+rToVal);
            end;
          end;
          if (ptInventryHeadxDopStatus.AsInteger=0) then  //��. � ���� �����
          begin
            par1[0]:=ptInventryHeadDepart.AsInteger;
            par1[1]:=ptInventryHeadInventry.AsInteger;
            par1[2]:=Code;

            if ptInvLine.Locate('Depart;Inventry;Item',par1,[]) then
            begin
              rQuantC:=rQuantC+(ptInvLineQuantRecord.AsFloat-ptInvLineQuantActual.AsFloat);
              rSumC:=rSumC+(ptInvLineQuantRecord.AsFloat-ptInvLineQuantActual.AsFloat)*ptInvLinePrice.AsFloat;
//            rSumRSC:=0; //���� ������ � ��������� �����

              if bMem then Memo1.Lines.Add(' ������������ '+its(ptInvLineInventry.AsInteger)+' '+ds1(ptInventryHeadDateInventry.AsDateTime)+' '+oemtoansiconvert(ptInventryHeadNumber.AsString)+rToVal);
            end;
          end;
        end else
        begin
          if ptInventryHeadItem.AsInteger=1 then  //������������ � ��
          begin
            if (ptInventryHeadxDopStatus.AsInteger=1) then  //��. � �������������
            begin
              par[0]:=ptInventryHeadInventry.AsInteger;
              par[1]:=Code;

              if ptInvLine1.Locate('IDH;ITEM',par,[]) then
              begin
                if bMem then Memo1.Lines.Add(' �������� ���. (�����) '+ds1(ptInventryHeadDateInventry.AsDateTime)+' '+oemtoansiconvert(ptInventryHeadNumber.AsString)+rToVal);
                Break; //���������� ��������
              end;
            end;
            if (ptInventryHeadxDopStatus.AsInteger=0) then  //��. � ���� �����
            begin
              par1[0]:=ptInventryHeadDepart.AsInteger;
              par1[1]:=ptInventryHeadInventry.AsInteger;
              par1[2]:=Code;

              if ptInvLine.Locate('Depart;Inventry;Item',par1,[]) then
              begin
                if bMem then Memo1.Lines.Add(' ������������ '+its(ptInvLineInventry.AsInteger)+' '+ds1(ptInventryHeadDateInventry.AsDateTime)+' '+oemtoansiconvert(ptInventryHeadNumber.AsString)+rToVal);
                Break;
              end;
            end;
          end;
        end;
      end;
      ptInventryHead.Prior;
    end;
  end;
end;


function fTestInv(iDep,iDate:Integer):Boolean;
Var DateB,DateE:TDateTime;
    sSt:String;
begin
  with dmMT do
  begin
    Result:=True;
    DateB:=iDate;
    DateE:=Date+5;
    if ptInventryHead.Active=False then ptInventryHead.Active:=True;
    ptInventryHead.CancelRange;
    ptInventryHead.SetRange([iDep,DateB],[iDep,DateE]);
    ptInventryHead.First;
    while not ptInventryHead.Eof do
    begin
      sSt:=OemToAnsiConvert(ptInventryHeadStatus.AsString);
      if (sSt<>'�') and (ptInventryHeadDetail.AsInteger=0) then
      begin
        Result:=False;
        Break;
      end;

      ptInventryHead.Next;
    end;
  end;
end;

function fSS(iDep:INteger):Integer;
begin
  Result:=0;
  with dmMT do
  begin
    if ptDep.Active=False then ptDep.Active:=true;
    ptDep.First;
    while not ptDep.Eof do
    begin
      if ptDepStatus1.AsInteger=9 then
      begin
        if ptDepID.AsInteger=iDep then
        begin
          Result:=ptDepStatus5.AsInteger;
          break;
        end;
      end;
      ptDep.Next;
    end;
  end;
end;


procedure prRecalcSS(DateB,DateE:INteger; Memo1:tcxMemo);
Var iCurD,DateE1:INteger;
    StrWk,sY:String;
    iC,iD:INteger;
    dCurD:TDateTime;
    rQ:Real;
    SInn,SAdr,sFName:String;
    CurD:TDateTime;
    rSumss,rSumSS0,rQIn,rPrIn:Real;
    tmpCena1:real; //�� ����� ����������� ���
    tmpCena2,tmpCena20:real;
    iCli:INteger;
    SInnF:String;

procedure wrmemo(StrPrint:String);
begin
  if Memo1<>nil then Memo1.Lines.Add(StrPrint);
  WriteHistory(StrPrint);
end;

begin
  with dmMT do
  with dmMC do
  begin
    DateE1:=Trunc(Date+1);
    wrmemo(fmt+'������ ('+StrWk+') ... �����.');
    try
      wrmemo(fmt+'  ������ ...');

      if ptPartO.Active=False then ptPartO.Active:=True;
      if ptDep.Active=False then ptDep.Active:=True;
      if ptPartIN.Active=False then ptPartIn.Active:=True;

      ptPartIn.Refresh;
      ptPartO.Refresh;
      ptDep.Refresh;

      ptPartO.IndexFieldNames:='IDSTORE;IDATE';

      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          wrmemo(fmt+'  ����� '+its(ptDepID.AsInteger));

          wrmemo(fmt+'    �������. ');
          iC:=0;
          ptPartIn.CancelRange;
          ptPartIn.IndexFieldNames:='IDSTORE;IDATE';
          ptPartIn.SetRange([ptDepID.AsInteger,DateB],[ptDepID.AsInteger,DateE1]);
          ptPartIn.First;
          while not ptPartIn.Eof do
          begin
            ptPartIn.Delete;
            inc(iC);
            if iC mod 100 = 0 then delay(20);
          end;
          ptPartIn.CancelRange;

          wrmemo(fmt+'    �������. ');

          ptPartIn.IndexFieldNames:='ID';

          ptPartO.CancelRange;
          ptPartO.SetRange([ptDepID.AsInteger,DateB],[ptDepID.AsInteger,DateE1]);

          ptPartO.First;
          while not ptPartO.Eof do
          begin
            if ptPartIn.FindKey([ptPartOPARTIN.AsInteger]) then  //����������� ������� ������
            begin
              rQ:=ptPartINQREMN.AsFloat+ptPartOQUANT.AsFloat; //����� �������
              if rQ>ptPartINQPART.AsFloat then rQ:=ptPartINQPART.AsFloat;

              ptPartIn.Edit;
              ptPartINQREMN.AsFloat:=rQ;
              ptPartIn.Post;
            end;

            ptPartO.Delete;

            inc(iC);
            if iC mod 100 = 0 then delay(20);
          end;
        end;
        ptDep.Next;
      end;

      wrmemo(fmt+'  ������� ������... ');

      ptPartIn.CancelRange;
      ptPartIn.IndexFieldNames:='IDSTORE;IDATE';

      ptTTNIn.Active:=False; ptTTNIn.Active:=True;
      ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';
      ptInLn.Active:=False; ptINLn.Active:=True;

      for iCurD:=DateB to DateE do
      begin
        wrmemo(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
        dCurD:=iCurD;
        ptTTNIn.CancelRange;
        ptTTNIn.SetRange([dCurD],[dCurD]);
        ptTTNIn.First;  iC:=0;
        while not ptTTNIn.Eof do
        begin
          if ptTTNInAcStatus.AsInteger=3 then
          begin
            prFCliINN(ptTTNInCodePost.AsInteger,ptTTNInIndexPost.AsInteger,SInn,SAdr,sFName,SInnF);
            if pos('/',sInn)>0 then sInn:=Copy(sInn,1,pos('/',sInn)-1);

            ptInLn.CancelRange;
            ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
            ptInLn.First;
            while not ptInLn.Eof do
            begin
              rQIn:=ptInLnKol.AsFloat;
              rPrIn:=0;
              if rQIn<>0 then rPrIn:=ptInLnOutNDSSum.AsFloat/rQIn; //��� �� ������ ����� ��� ���
              if prAddPartIn(ptTTNInID.AsInteger,1,ptInLnCodeTovar.AsInteger,iCurD,ptTTNInDepart.AsInteger,sInnF,ptInLnKol.AsFloat,ptInLnCenaTovar.AsFloat,ptInLnNewCenaTovar.AsFloat,rPrIn,ptTTNInCodePost.AsInteger)=False then
              begin
                wrmemo(fmt+'     ������ ���������� ������ (���.'+its(ptTTNInID.AsInteger)+' ���.'+its(ptInLnCodeTovar.AsInteger)+')');
              end;

              ptInLn.Next;
            end;
          end;
          ptTTNIn.Next; inc(iC);
          if iC mod 10 = 0 then
          begin
//                StatusBar1.Panels[0].Text:=its(iC);
            delay(100);
          end;
        end;
      end;

      wrmemo('     - ������ ..'); delay(10);
      try
        for iCurD:=DateB to DateE do
        begin
          wrmemo(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
          wrmemo('        - ����������� ..'); delay(10);

          try
            quTTNVn.Active:=False;
            quTTNVn.ParamByName('DATEB').AsDate:=iCurD;
            quTTNVn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
            quTTNVn.Active:=True;

            quTTNVn.First; iC:=0;
            while not quTTNVn.Eof do
            begin
              if quTTNVnAcStatusP.AsInteger=3 then
              begin //������� �� ������������

//                prFCliINN(ptTTNInCodePost.AsInteger,ptTTNInIndexPost.AsInteger,SInn,SAdr,sFName);
//                if pos('/',sInn)>0 then sInn:=Copy(sInn,1,pos('/',sInn)-1);
                sInn:='0';

                quSpecVn2.Active:=False;
                quSpecVn2.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
                quSpecVn2.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
                quSpecVn2.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
                quSpecVn2.Active:=True;
                quSpecVn2.First;      //������� ����������� ��� ������� ���� ������ �������������
                while not quSpecVn2.Eof do
                begin

                  if quSpecVn2Kol.AsFloat>0 then prCalcPartO(quTTnVnID.AsInteger,3,quSpecVn2CodeTovar.AsInteger,iCurD,quTTnVnDepart.AsInteger,True,quSpecVn2Kol.AsFloat,abs(quSpecVn2SumNewCenaTovar.AsFloat/quSpecVn2Kol.AsFloat),abs(quSpecVn2SumNewCenaTovar.AsFloat),quSpecVn2CenaTovarSpis.AsFloat,quSpecVn2CenaTovarSpis.AsFloat,'',rSumSS,rSumSS0,sInn,iCli);
                  if quSpecVn2Kol.AsFloat<0 then
                  begin
                    prAddPartIn(quTTnVnID.AsInteger,3,quSpecVn2CodeTovar.AsInteger,iCurD,quTTnVnFlowDepart.AsInteger,sInn,abs(quSpecVn2Kol.AsFloat),quSpecVn2CenaTovarSpis.AsFloat,abs(quSpecVn2NewCenaTovar.AsFloat),quSpecVn2CenaTovarSpis.AsFloat,-3);
                  end;

                  quSpecVn2.Next;
                end;
              end;
              quTTNVn.Next;
              inc(iC);
              if iC mod 10 = 0 then
              begin
//                    StatusBar1.Panels[0].Text:=its(iC);
                delay(100);
              end;
            end;
          finally
          end;

          wrmemo('        - ������� ..'); delay(10);

          quTTNOut.Active:=False;
          quTTNOut.ParamByName('DATEB').AsDate:=iCurD;
          quTTNOut.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
          quTTNOut.Active:=True;

          quTTNOut.First; iC:=0;
          while not quTTNOut.Eof do
          begin
            if (quTTNOutAcStatus.AsInteger=3)and(quTTnOutProvodType.AsInteger<>4) then
            begin
              sInn:=quTTnOutINN.AsString;
              if quTTnOutProvodType.AsInteger=3 then sInn:=''; //��� �� �������� - ��� �������

              quSpecOut1.Active:=False;
              quSpecOut1.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
              quSpecOut1.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
              quSpecOut1.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
              quSpecOut1.Active:=True;

              quSpecOut1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecOut1.Eof do
              begin
                if quSpecOut1CodeTovar.AsInteger>0 then
                begin
                  //���� ���� �� �������
                  if quSpecOut1Kol.AsFloat<>0 then tmpCena1:=(quSpecOut1SumCenaTovarSpis.AsFloat/quSpecOut1Kol.AsFloat) else tmpCena1:=0; //���� ���������
                  if quSpecOut1Kol.AsFloat<>0 then tmpCena2:=(quSpecOut1SumCenaTovar.AsFloat/quSpecOut1Kol.AsFloat) else tmpCena2:=0;     //���� �������� � ��� (����������)
                  tmpCena20:=quSpecOut1CenaTaraSpis.AsFloat;
                  if prCalcPartO(quTTnOutID.AsInteger,2,quSpecOut1CodeTovar.AsInteger
                                ,iCurD,quTTnOutDepart.AsInteger
                                ,True,quSpecOut1Kol.AsFloat
                                ,tmpCena1
                                ,quSpecOut1SumCenaTovarSpis.AsFloat
                                ,tmpCena2
                                ,tmpCena20
                                ,sInn,rSumSS,rSumSS0,sInn,iCli)=False then
                  begin
                    wrmemo(fmt+'     ������ ��������� ������ (���.'+its(quTTnOutID.AsInteger)+' ���.'+its(quSpecOut1CodeTovar.AsInteger)+')');
                  end;
                end;

                quSpecOut1.Next;
              end;
              quSpecOut1.Active:=False;
            end;
            quTTNOut.Next;
            inc(iC);
            if iC mod 10 = 0 then
            begin
//                  StatusBar1.Panels[0].Text:=its(iC);
              delay(100);
            end;
          end;

          wrmemo('        - ���������� ..'); delay(10);
          ptCn.Active:=False;
          ptCn.TableName:='cn'+FormatDateTime('yyyymm',iCurD);
          try
            ptCn.Active:=True;
            ptCn.CancelRange;
            CurD:=iCurD;
            ptCn.SetRange([CurD],[CurD]);
            ptCn.First;

            iC:=0;
            while not ptCn.Eof do
            begin
              StrWk:=FormatDateTime('mmdd',iCurD);
              sY:=FormatDateTime('yy',iCurD);
              iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

              try
                if (ptCnCode.AsInteger>0)and(ptCnQuant.AsFloat>0) then
                begin
                  if prCalcPartO(iD,7,ptCnCode.AsInteger,iCurD,ptCnDepart.AsInteger,True,ptCnQuant.AsFloat,(ptCnRSumCor.AsFloat/ptCnQuant.AsFloat),ptCnRSumCor.AsFloat,0,0,'',rSumss,rSumSS0,sInn,iCli)=False then
                  begin
                    wrmemo(fmt+'     ������ ��������� ������ (���.'+its(ID)+' ���.'+its(ptCnCode.AsInteger)+')');
                  end;
                end else
                begin
                  //������ �������
                  if (ptCnQuant.AsFloat<-0.001) then //�������
                  begin //����� ��������� ������ �� ���������
                    sInn:='';
                    //��� ��������� ������ ��� ������, ���� ���������� ���������� sInn
                    prCalcPartO(iD,7,ptCnCode.AsInteger,iCurD,ptCnDepart.AsInteger,False,0.01,(ptCnRSumCor.AsFloat/ptCnQuant.AsFloat),ptCnRSumCor.AsFloat,0,0,'',rSumss,rSumSS0,sInn,iCli);
                    prAddPartIn(iD,7,ptCnCode.AsInteger,iCurD,ptCnDepart.AsInteger,sInn,abs(ptCnQuant.AsFloat),abs(ptCnRSumCor.AsFloat/ptCnQuant.AsFloat),abs(ptCnRSumCor.AsFloat/ptCnQuant.AsFloat),abs(ptCnRSumCor.AsFloat/ptCnQuant.AsFloat),iCli);
                  end;
                end;
              except
                wrmemo(fmt+'     ������ ��������� ������ (���.'+its(ID)+' ���.'+its(ptCnCode.AsInteger)+')');
              end;

              ptCn.Next; inc(iC);
              if iC mod 100 = 0 then
              begin
//                    fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                delay(20);
              end;
            end;
          except
            ptCn.Active:=False;
          end;

          wrmemo('        - �������������� .. ('+its(iCurD)+')'); delay(10);

          try
            quTTnInv.Active:=False;
            quTTnInv.ParamByName('DATEB').AsDate:=iCurD;
            quTTnInv.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
            quTTnInv.Active:=True;

            quTTnInv.First; iC:=0;
            while not quTTnInv.Eof do
            begin
              if quTTnInvStatus.AsString='�' then
              begin //������� �� ������������
                if ptInvLine1.Active=False then ptInvLine1.Active:=True;

                ptInvLine1.CancelRange;
                ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
                ptInvLine1.First;
                while not ptInvLine1.Eof do
                begin
                  rQ:=ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat;
                  if abs(rQ)>0.0001 then
                  begin
                    if rQ>0 then //������
                    begin
                      prAddPartIn(quTTnInvInventry.AsInteger,20,ptInvLine1ITEM.AsInteger,iCurD,quTTnInvDepart.AsInteger,'',abs(rQ),(ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ,abs(ptInvLine1PRICER.AsFloat),(ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ,-20);
                    end else //������
                    begin
                      prCalcPartO(quTTnInvInventry.AsInteger,20,ptInvLine1ITEM.AsInteger,iCurD,quTTnInvDepart.AsInteger,True,abs(rQ),abs(quSpecVn2SumNewCenaTovar.AsFloat/quSpecVn2Kol.AsFloat),abs(ptInvLine1PRICER.AsFloat),abs((ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ),abs((ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ),'',rSumSS,rSumSS0,sInn,iCli);
                    end;
                  end;
                  prNormPartInv(ptInvLine1ITEM.AsInteger,iCurD,quTTnInvDepart.AsInteger,ptInvLine1QUANTF.AsFloat);
                  ptInvLine1.Next;
                end;
              end;
              quTTnInv.Next;
              inc(iC);
              if iC mod 10 = 0 then
              begin
//                    StatusBar1.Panels[0].Text:=its(iC);
                delay(100);
              end;
            end;
          finally
          end;

        end;
        wrmemo(''); delay(10);
        wrmemo('������������� �� '+FormatDateTime('dd.mm.yyyy',(DateE+1))); delay(10);
        prReSetTA(DateE+1);
      finally
      end;

    finally
      ptTTNIn.Active:=False;
      ptInLn.Active:=False;

      wrmemo(fmt+'������� ��������.');
//          fmSt.cxButton1.Enabled:=True;
    end;

  end;
end;

function prFindPriceAfterA(iC,iDB:INteger; Var rPrAfter:Real):Boolean;
Var DateB,DateE:TDateTime;
begin
  with dmMT do
  begin
    Result:=False;
    rPrAfter:=0;
    DateB:=iDb-100;
    DateE:=iDb+1;
    if ptAkc.Active=False then ptAkc.Active:=True;
    ptAkc.Refresh;
    ptAkc.SetRange([iC,1,DateB],[iC,1,DateE]);
    ptAkc.Last;
    while not ptAkc.Bof do
    begin
      Result:=True;
      rPrAfter:=ptAkcOldPrice.AsFloat;
      break;

      ptAkc.Prior;
    end;
  end;
end;


function prFindBAkciya(iC,iDB:INteger):INteger;
Var DateB,DateE:TDateTime;
    iBSt:Integer;
begin
  with dmMT do
  begin
    iBSt:=0;
    DateB:=iDb;
    DateE:=iDb+15;
    if ptAkc.Active=False then ptAkc.Active:=True;
    ptAkc.SetRange([iC,3,DateB],[iC,3,DateE]);
    ptAkc.First;
    while not ptAkc.Eof do
    begin
//      if (ptAkcDStop.AsInteger=2) or (ptAkcDStop.AsInteger=9) then
      if (ptAkcDStop.AsInteger<>6)and(ptAkcDStop.AsInteger<>8) then
      begin
        iBSt:=ptAkcDStop.AsInteger;
        break;
      end;
      ptAkc.Next;
    end;
    Result:=iBSt;
  end;
end;


function prFindBAkciya1(iC,iDB:INteger; Var dDa,dDa1,bTop:Integer):INteger;
Var DateB,DateE:TDateTime;
    iBSt:Integer;
begin
  with dmMT do
  begin
    iBSt:=0;
    DateB:=iDb;
    DateE:=iDb+15;
    dDa:=0;
    dDa1:=0;
    bTop:=0;
    if ptAkc.Active=False then ptAkc.Active:=True;
    ptAkc.SetRange([iC,3,DateB],[iC,3,DateE]); //3 - ��� ���������
    ptAkc.First;
    while not ptAkc.Eof do
    begin
//      if (ptAkcDStop.AsInteger=2) or (ptAkcDStop.AsInteger=9) then
      if (ptAkcDStop.AsInteger<>1)and(ptAkcDStop.AsInteger<>3)and(ptAkcDStop.AsInteger<>-3) then
      begin
        iBSt:=ptAkcDStop.AsInteger;
        dDa:=Trunc(ptAkcDateB.AsDateTime);
        break;
      end;
      ptAkc.Next;
    end;

    ptAkc.First;
    while not ptAkc.Eof do
    begin
      if (ptAkcDStop.AsInteger=3)or(ptAkcDStop.AsInteger=-3) then
      begin
        bTop:=ptAkcDStop.AsInteger;
        dDa1:=Trunc(ptAkcDateB.AsDateTime);
        break;
      end;
      ptAkc.Next;
    end;

    Result:=iBSt;
  end;
end;


procedure prCorrPartIn(iCode:Integer);
Var rQp,rQs,rQ,rPrIn,rPr1,rQr,rPrIn0:Real;
    DateB,DateE:TDateTime;
    Id:INteger;
    Sinn:String;
    DatePP:Integer;
    iDateB,iDateE:INteger;
    iCliCB:INteger;
begin
  with dmMT do
  begin
    if ptDep.Active=False then ptDep.Active:=True;
    if ptPartO.Active=False then ptPartO.Active:=True;

    if ptPartIN.Active=False then ptPartIn.Active:=True;
    ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';

    ptDep.First;
    while not ptDep.Eof do
    begin
      if ptDepStatus1.AsInteger=9 then
      begin
        rPrIn:=0;

        if CommonSet.CalcSeb<2 then //CommonSet.CalcSeb=1 - �� ���������
        begin
          rQs:=prFindTRemnDep(iCode,ptDepId.AsInteger);
          if rQs<=0 then rQs:=0; //������ 0 ����� �����������

          if rQs>0 then
          begin
            iDateB:=Trunc(Date-2000);
            iDateE:=Trunc(Date);
            ptPartIn.CancelRange;
            ptPartIn.SetRange([ptDepId.AsInteger,iCode,iDateE],[ptDepId.AsInteger,iCode,iDateB]);
            ptPartIn.First;
            while (ptPartIn.Eof=False)and(abs(rQs)>0.001) do
            begin
              rQp:=ptPartINQPART.AsFloat;
              rPrIn:=ptPartINPRICEIN.AsFloat;
              rPrIn0:=ptPartINPRICEIN0.AsFloat;

              if rQs<=rQp then
              begin
                rQ:=rQs; //��������� ������ ������ ���������
              end else
              begin
                rQ:=rQp;
              end;
              rQs:=rQs-rQ;

              ptPartIn.Edit;
              ptPartINQREMN.AsFloat:=rQ;
              ptPartIn.Post;

              ptPartIn.Next;
            end;
            if ptPartIn.Eof=False then //�������� ����������� ������
            begin
              while (ptPartIn.Eof=False)and(ptPartINQREMN.AsFloat>0.0001) do
              begin
                ptPartIn.Edit;
                ptPartINQREMN.AsFloat:=0;
                ptPartIn.Post;

                ptPartIn.Next;
              end;
            end;

            if abs(rQs)>0.001 then //������ ������ ��� �� �������������� ��������
            begin
              DatePP:=Trunc(Date);
              if rPrIn=0 then rPrIn:=prFLPriceDateT1(iCode,Trunc(Date),DatePP,rPrIn0);
//             prAddPartIn(quTTnInvInventry.AsInteger,20,ptInvLine1ITEM.AsInteger,iCurD,quTTnInvDepart.AsInteger,'',abs(rQ),(ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ,abs(ptInvLine1PRICER.AsFloat),(ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat)/rQ);

              ptPartIn.Append;
              ptPartINIDSTORE.AsInteger:=ptDepId.AsInteger;
              ptPartINIDATE.AsInteger:=Trunc(DatePP);
              ptPartINIDDOC.AsInteger:=0;
              ptPartINARTICUL.AsInteger:=iCode;
              ptPartINSINNCLI.AsString:='';
              ptPartINDTYPE.AsInteger:=21;
              ptPartINQPART.AsFloat:=rQs;
              ptPartINQREMN.AsFloat:=rQs;
              ptPartINPRICEIN.AsFloat:=rPrIn;
              ptPartINPRICEIN0.AsFloat:=rPrIn0;
              ptPartINPRICEOUT.AsFloat:=rPrIn;
              ptPartIn.Post;
            end;
          end;
        end else //������� ��������� ������ 1-�� ���  CommonSet.CalcSeb=���� �� ������� �������
        begin
          rQs:=prFindTRemnDepD(iCode,ptDepId.AsInteger,CommonSet.CalcSeb);
          if rQs<=0 then rQs:=0; //������ 0 ����� �����������

          if rQs>0 then
          begin
            rQr:=rQs; //��� ������� ������� ���� ���������
            rPr1:=0;
            DateB:=Date-100;
            DateE:=CommonSet.CalcSeb;

            if ptTTNIn.Active=False then ptTTNIn.Active:=True;
            ptTTNIn.Refresh;
            ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
            ptTTnIn.CancelRange;

            if ptTTNInLn.Active=False then ptTTNInLn.Active:=True;
            ptTTNInLn.Refresh;
            ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
            ptTTnInLn.CancelRange;
            ptTTnInLn.SetRange([iCode,DateB],[iCode,DateE]);
            ptTTnInLn.Last;
            while not ptTTnInLn.Bof do
            begin
              if ptTTnInLnDepart.AsInteger=ptDepId.AsInteger then
              begin
                Id:=0;
                if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                begin
                  Id:=ptTTNInID.AsInteger;
                  prFindCliName1(ptTTnInLnIndexPost.AsInteger,ptTTnInLnCodePost.AsInteger,Sinn,iCliCB);
                end;

                rPr1:=ptTTnInLnCenaTovar.AsFloat; //��� ����������� ������ ���� � �����
                try
                  rPrIn0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat; //���� �����, �� ��� ���
                except
                  rPrIn0:=rPr1; //���� �����, �� ��� ���
                end;

                if ptTTnInLnKol.AsFloat>=rQr then
                begin
                  ptPartIn.Append;
                  ptPartINIDSTORE.AsInteger:=ptDepId.AsInteger;
                  ptPartINIDATE.AsInteger:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
                  ptPartINIDDOC.AsInteger:=Id;
                  ptPartINARTICUL.AsInteger:=iCode;
                  ptPartINSINNCLI.AsString:=Sinn;
                  ptPartINDTYPE.AsInteger:=1;
                  ptPartINQPART.AsFloat:=ptTTnInLnKol.AsFloat;
                  ptPartINQREMN.AsFloat:=rQr;
                  ptPartINPRICEIN.AsFloat:=rPr1;
                  ptPartINPRICEIN0.AsFloat:=rPrIn0;
                  ptPartINPRICEOUT.AsFloat:=ptTTnInLnNewCenaTovar.AsFloat;
                  ptPartIn.Post;

                  rQr:=0;
                end else
                begin
                  rQr:=rQr-ptTTnInLnKol.AsFloat;

                  ptPartIn.Append;
                  ptPartINIDSTORE.AsInteger:=ptDepId.AsInteger;
                  ptPartINIDATE.AsInteger:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
                  ptPartINIDDOC.AsInteger:=Id;
                  ptPartINARTICUL.AsInteger:=iCode;
                  ptPartINSINNCLI.AsString:=Sinn;
                  ptPartINDTYPE.AsInteger:=1;
                  ptPartINQPART.AsFloat:=ptTTnInLnKol.AsFloat;
                  ptPartINQREMN.AsFloat:=ptTTnInLnKol.AsFloat;
                  ptPartINPRICEIN.AsFloat:=rPr1;
                  ptPartINPRICEIN0.AsFloat:=rPrIn0;
                  ptPartINPRICEOUT.AsFloat:=ptTTnInLnNewCenaTovar.AsFloat;
                  ptPartIn.Post;
                end;
                if rQr=0 then Break;
              end;
              ptTTnInLn.Prior;
            end;
            if rQr>0 then
            begin
              if rPr1<0.01 then rPr1:=prFLPriceDateT(iCode,Trunc(DateE),rPrIn0);
              if rPr1<0.01 then rPr1:=prFLPriceDateTVn(iCode,Trunc(DateE),rPrIn0);

              ptPartIn.Append;
              ptPartINIDSTORE.AsInteger:=ptDepId.AsInteger;
              ptPartINIDATE.AsInteger:=Trunc(CommonSet.CalcSeb);
              ptPartINIDDOC.AsInteger:=0;
              ptPartINARTICUL.AsInteger:=iCode;
              ptPartINSINNCLI.AsString:='';
              ptPartINDTYPE.AsInteger:=22;
              ptPartINQPART.AsFloat:=rQr;
              ptPartINQREMN.AsFloat:=rQr;
              ptPartINPRICEIN.AsFloat:=rPr1;
              ptPartINPRICEIN0.AsFloat:=rPrIn0;
              ptPartINPRICEOUT.AsFloat:=rPr1;
              ptPartIn.Post;
            end;
          end; //if rQs>0
        end; 
      end;
      ptDep.Next;
    end;
  end;
end;


function prCalcSumInRemn(iCode,iDate,iSkl:Integer;rQr:Real; Var rSumIn:Real):Boolean;
Var rQp,rQs,rQ,rPrIn,rPrIn0:Real;
    iSS:INteger;
begin
  Result:=True;
  with dmMT do
  begin
    rSumIn:=0;

    iSS:=fSS(iSkl);

    if rQr>0.001 then
    begin
      if ptDep.Active=False then ptDep.Active:=True;
      if ptPartO.Active=False then ptPartO.Active:=True;


      rQs:=rQr;
      rPrIn:=0;

      if ptPartIN.Active=False then ptPartIn.Active:=True;
      ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
      ptPartIn.CancelRange;
      ptPartIn.SetRange([iSkl,iCode,iDate],[iSkl,iCode,iDate-336]);
      ptPartIn.First;
      while (ptPartIn.Eof=False)and(abs(rQs)>0.001) do
      begin
        if (ptPartInDTYPE.AsInteger=20)and(ptPartInIDATE.AsInteger=iDate) then ptPartIn.Next
        else         //��� ����� ����
        begin
          if ptPartINPRICEIN.AsFloat>0.01 then
          begin
            rQp:=ptPartINQPART.AsFloat;
            rPrIn:=ptPartINPRICEIN.AsFloat;
            rPrIn0:=ptPartINPRICEIN0.AsFloat;

            if rQs<=rQp then
            begin
              rQ:=rQs;//��������� ������ ������ ���������
            end else
            begin
              rQ:=rQp;
            end;
            rQs:=rQs-rQ;

            if iSS<2 then rSumIn:=rSumIn+rPrIn*rQ;
            if iSS=2 then rSumIn:=rSumIn+rPrIn0*rQ;
          end;
          ptPartIn.Next;
        end;
      end;
      if abs(rQs)>0.001 then
      begin
        if rPrIn=0 then  rPrIn:=prFLPriceDateT(iCode,iDate,rPrIn0);
        if rPrIn<0.01 then rPrIn:=prFLPriceDateTVn(iCode,iDate,rPrIn0); //�� ���� ���������� ������� ��.���������
        if iSS<2 then rSumIn:=rSumIn+rQs*rPrIn;
        if iSS=2 then rSumIn:=rSumIn+rQs*rPrIn0;
      end;
    end else
    begin
      if rQr<-0.001 then // ������������� ���-��
      begin
        rPrIn:=prFLPriceDateT(iCode,iDate,rPrIn0); //�� ���� ���������� �������
        if rPrIn<0.01 then rPrIn:=prFLPriceDateTVn(iCode,iDate,rPrIn0); //�� ���� ���������� ������� ��.���������
        if iSS<2 then rSumIn:=rQr*rPrIn;
        if iSS=2 then rSumIn:=rQr*rPrIn0;
      end;
    end;

    rSumIn:=rv(rSumIn);

  end;
end;

function prGetTA:Integer;
Var iCode:SmallINt;
begin
  with dmMT do
  begin
    iCode:=100;
    Result:=0;
    if not ptPool.Active then ptPool.Open;
    if ptPool.locate('CodeObject',iCode,[]) then Result:=ptPoolId.AsInteger;
  end;
end;

procedure prReSetTA(iDate:Integer);
Var iCode:SmallINt;
begin
  with dmMT do
  begin
    iCode:=100;
    if not ptPool.Active then ptPool.Open;
    if ptPool.locate('CodeObject',iCode,[]) then
    begin
      ptPool.Edit;
      ptPoolId.AsInteger:=iDate;
      ptPool.Post;
    end else
    begin
      ptPool.Append;
      ptPoolCodeObject.AsInteger:=iCode; //����� ������������ ��� ������� ��
      ptPoolId.AsInteger:=iDate;
      ptPool.Post;
    end;
  end;
end;


procedure prSetTA(iDate:Integer);
Var iCode:SmallINt;
begin
  with dmMT do
  begin
    iCode:=100;
    if not ptPool.Active then ptPool.Open;
    if ptPool.locate('CodeObject',iCode,[]) then
    begin
      if iDate<ptPoolId.AsInteger then
      begin
        ptPool.Edit;
        ptPoolId.AsInteger:=iDate;
        ptPool.Post;
      end;
    end else
    begin
      ptPool.Append;
      ptPoolCodeObject.AsInteger:=iCode; //����� ������������ ��� ������� ��
      ptPoolId.AsInteger:=iDate;
      ptPool.Post;
    end;
    ptPool.Refresh;
  end;
end;

function prGetNum(iCode:SmallINt):INteger;
Var iNum:INteger;
begin
  iNum:=1;
  with dmMT do
  begin
    if not ptPool.Active then ptPool.Open;

    if ptPool.locate('CodeObject',iCode,[]) then
    begin
      iNum:=ptPoolId.AsInteger;

      ptPool.Edit;
      ptPoolId.AsInteger:=iNum+1;
      ptPool.Post;
    end else
    begin
      ptPool.Append;
      ptPoolCodeObject.AsInteger:=iCode; //���
      ptPoolId.AsInteger:=iNum;
      ptPool.Post;
    end;
    ptPool.Refresh;
  end;
  Result:=iNum;
end;


function prGetANum(iCode:SmallINt):INteger;
Var iNum:INteger;
begin
  iNum:=1;
  with dmMT do
  begin
    if not ptNum.Active then
    begin
      ptNum.DatabaseName:=Btr1Path;
      ptNum.Open;
    end;

    if ptNum.locate('ITYPE',iCode,[]) then
    begin
      iNum:=ptNumINUM.AsInteger;

      ptNum.Edit;
      ptNumINUM.AsInteger:=iNum+1;
      ptNum.Post;
    end else
    begin
      ptNum.Append;
      ptNumITYPE.AsInteger:=iCode; //���
      ptNumINUM.AsInteger:=iNum;
      ptNum.Post;
    end;
    ptNum.Refresh;
  end;
  Result:=iNum;
end;


Function prRetDName(iC:Integer):String;
begin
  with dmMT do
  begin
    Result:='';
    if not ptDStop.Active then ptDStop.Open;
    if ptDStop.FindKey([iC]) then
    begin
      if ptDStopPercent.AsFloat>99 then Result:='-';
    end;
  end;
end;


Function prSDisc(iC:Integer):String;
begin
  with dmMT do
  begin
    Result:='';
    if not ptDStop.Active then ptDStop.Open;
    if ptDStop.FindKey([iC]) then
    begin
      if ptDStopPercent.AsFloat>99 then Result:='������ �� ���������������'; 
    end;
  end;
end;

function prFindPricePP0(iC,iDep:INteger;Var Price0:Real):Real;
begin
  Result:=0;
  Price0:=0;

  if (CommonSet.CalcSeb=0) then exit;
  with dmMT do
  begin
    //������ ��� ��������� ������ �� ���������
    if ptPartIN.Active=False then ptPartIn.Active:=True;

    ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
    ptPartIn.CancelRange;
    ptPartIn.SetRange([iDep,iC],[iDep,iC]);
    ptPartIn.First;
    while (not ptPartIn.Eof) do
    begin
      Price0:=ptPartINPRICEIN0.AsFloat;
      Result:=ptPartINPRICEIN.AsFloat;
      Break;
    end;

  end;
end;

function prFindPricePP(iC,iDep:INteger):Real;
begin
  Result:=0;
  if (CommonSet.CalcSeb=0) then exit;
  with dmMT do
  begin
    //������ ��� ��������� ������ �� ���������
    if ptPartIN.Active=False then ptPartIn.Active:=True;

    ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
    ptPartIn.CancelRange;
    ptPartIn.SetRange([iDep,iC],[iDep,iC]);
    ptPartIn.First;
    while (not ptPartIn.Eof) do
    begin
      Result:=ptPartINPRICEIN.AsFloat;
      Break;
    end;

  end;
end;

Function prFindCliName1(iT,iC:Integer;Var Sinn:String; Var iCli:INteger):String;
Var StrO:String;
begin
  with dmMt do
  begin
    StrO:='';
    Sinn:='';
    iCli:=0;
    if ptIP.Active=false then ptIP.Active:=true;
    if ptCli.Active=false then ptCli.Active:=true;
    if iT=1 then
    begin
      if ptCli.Locate('Vendor',iC,[]) then
      begin
        StrO:=OemToAnsiConvert(ptCliName.AsString);
        Sinn:=ptCliINN.AsString;
        iCli:=ptCliStatus.AsInteger;
      end;
    end;
    if iT=2 then
    begin
      if ptIP.Locate('Code',iC,[]) then
      begin
        StrO:=OemToAnsiConvert(ptIPName.AsString);
        Sinn:=ptIPINN.AsString;
        iCli:=0;
      end;
    end;
    Result:=StrO;
  end;
end;

Function prFindCliName2(iT,iC:Integer;Var Sinn,Skpp:String; Var iCli:INteger):String;
Var StrO:String;
begin
  with dmMt do
  begin
    StrO:='';
    Sinn:='';
    Skpp:='';
    iCli:=0;
    if ptIP.Active=false then ptIP.Active:=true;
    if ptCli.Active=false then ptCli.Active:=true;
    if iT=1 then
    begin
      if ptCli.Locate('Vendor',iC,[]) then
      begin
        StrO:=OemToAnsiConvert(ptCliFullName.AsString);
        Sinn:=ptCliINN.AsString;
        Skpp:=ptCliOKPO.AsString;
        iCli:=ptCliStatus.AsInteger;
      end;
    end;
    if iT=2 then
    begin
      if ptIP.Locate('Code',iC,[]) then
      begin
        StrO:=OemToAnsiConvert(ptIPName.AsString);
        Sinn:=ptIPINN.AsString;
//        SKpp:=ptIPKPP.AsString; //��� � ���� ������
        iCli:=0;
      end;
    end;
    Result:=StrO;
  end;
end;


Function prFindGLNCli(iCli:Integer):String;
begin
  with dmMt do
  begin
    if ptCli.Active=false then ptCli.Active:=true;
    if ptCli.Locate('Vendor',iCli,[]) then Result:=ptCliCodeUchet.AsString else Result:='';
  end;
end;

Function prFindCliCodecb(sInn:String):Integer;
begin
  with dmMt do
  begin
    if ptCli.Active=false then ptCli.Active:=true;
    if ptCli.FindKey([sInn]) then Result:=ptCliStatus.AsInteger else Result:=0;
  end;
end;


Function prFindCliName(sInn:String):String;
Var StrO:String;
begin
  with dmMt do
  begin
    StrO:='';
    if ptIP.Active=false then ptIP.Active:=true;
    if ptCli.Active=false then ptCli.Active:=true;
    if ptCli.FindKey([sInn]) then StrO:=OemToAnsiConvert(ptCliName.AsString);
    if StrO='' then
      if ptIP.FindKey([sInn]) then StrO:=OemToAnsiConvert(ptIPName.AsString);
    Result:=StrO;
  end;
end;

procedure prFillPart(iCode:INteger;sName:String);
Var IDateB,iDateE:INteger;
begin
  with dmMT do
  with fmParts do
  begin
    iDateB:=Trunc(CommonSet.DateBeg);
    iDateE:=Trunc(CommonSet.DateEnd);
    fmParts.Label1.Tag:=iCode;
    fmParts.Label1.Caption:=sName;
    fmParts.Caption:='������ ������ �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
    fmParts.Show;
    try
      ViewP1.BeginUpdate;
      ViewP2.BeginUpdate;

      CloseTe(tePartIn);
      CloseTe(tePartO);
      if ptPartIN.Active=False then ptPartIN.Active:=True;
      if ptPartO.Active=False then ptPartO.Active:=True;
      if ptDep.Active=False then ptDep.Active:=True;

      ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
      ptPartO.IndexFieldNames:='ARTICUL;IDATE;IDSTORE;ITYPE;IDDOC;PARTIN;ID';

//      iNum:=1;

      ptDep.First;
      while not ptDep.Eof do
      begin
        if (ptDepStatus1.AsInteger=9) and (ptDepStatus5.AsInteger>0) then
        begin
          ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
          ptPartIN.CancelRange;
          ptPartIN.SetRange([ptDepID.AsInteger,iCode,iDateE],[ptDepID.AsInteger,iCode,iDateB]);
          ptPartIn.First;
          while not ptPartIn.Eof do
          begin
            tePartIn.Append;
            tePartInID.AsInteger:=ptPartINID.AsInteger;
            tePartInIDSTORE.AsInteger:=ptDepID.AsInteger;
            tePartInSSTORE.AsString:=OemToAnsiConvert(ptDepName.AsString);
            tePartInIDATE.AsInteger:=ptPartINIDATE.AsInteger;
            tePartInIDDOC.AsInteger:=ptPartINIDDOC.AsInteger;
            tePartInARTICUL.AsInteger:=ptPartINARTICUL.AsInteger;
            tePartInDTYPE.AsInteger:=ptPartINDTYPE.AsInteger;
            tePartInQPART.AsFloat:=ptPartINQPART.AsFloat;
            tePartInQREMN.AsFloat:=ptPartINQREMN.AsFloat;
            tePartInPRICEIN.AsFloat:=ptPartINPRICEIN.AsFloat;
            tePartInPRICEOUT.AsFloat:=ptPartINPRICEOUT.AsFloat;
            tePartInSINNCLI.AsString:=ptPartINSINNCLI.AsString;
            tePartInSCLI.AsString:=prFindCliName(ptPartINSINNCLI.AsString);
            tePartInPRICEIN0.AsFloat:=rv(ptPartINPRICEIN0.AsFloat);
            tePartInSumIn0.AsFloat:=rv(ptPartInQPART.AsFloat*ptPartInPRICEIN0.AsFloat);
            tePartIn.Post;

            ptPartIn.Next;
          end;
        end;
        ptDep.Next;
      end;

      ptPartIn.IndexFieldNames:='ID';
      ptPartIN.CancelRange;
      ptPartIn.IndexFieldNames:='ID';

      ptPartO.CancelRange;
      ptPartO.SetRange([iCode,iDateB],[iCode,iDateE]);
      ptPartO.First;
      while not ptPartO.Eof do
      begin
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' '+ds(ptPartOIDATE.AsInteger)+' '+fs(ptPartOQUANT.AsFloat)+' '+its(ptPartOPARTIN.AsInteger)+' '+fs(ptPartOSUMIN.AsFloat),nil);

        if tePartIn.Locate('ID',ptPartOPARTIN.AsInteger,[])=False then
        begin //��������� ������ �� ������ ��������� ��� ������ ���� ���������
          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' ��� ���� � ������ ��',nil);
          if ptPartOPARTIN.AsInteger>0 then
          begin //�������� ������  - ����� ������
            if ptPartIN.FindKey([ptPartOPARTIN.AsInteger]) then
            begin
              if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' �������� ������ - ��������� � ������ ',nil);

              tePartIn.Append;
              tePartInID.AsInteger:=ptPartINID.AsInteger;
              tePartInIDSTORE.AsInteger:=ptDepID.AsInteger;
              tePartInSSTORE.AsString:=OemToAnsiConvert(ptDepName.AsString);
              tePartInIDATE.AsInteger:=ptPartINIDATE.AsInteger;
              tePartInIDDOC.AsInteger:=ptPartINIDDOC.AsInteger;
              tePartInARTICUL.AsInteger:=ptPartINARTICUL.AsInteger;
              tePartInDTYPE.AsInteger:=ptPartINDTYPE.AsInteger;
              tePartInQPART.AsFloat:=ptPartINQPART.AsFloat;
              tePartInQREMN.AsFloat:=ptPartINQREMN.AsFloat;
              tePartInPRICEIN.AsFloat:=ptPartINPRICEIN.AsFloat;
              tePartInPRICEOUT.AsFloat:=ptPartINPRICEOUT.AsFloat;
              tePartInSINNCLI.AsString:=ptPartINSINNCLI.AsString;
              tePartInSCLI.AsString:=prFindCliName(ptPartINSINNCLI.AsString);
              tePartInPRICEIN0.AsFloat:=rv(ptPartINPRICEIN0.AsFloat);
              tePartInSumIn0.AsFloat:=rv(ptPartInQPART.AsFloat*ptPartInPRICEIN0.AsFloat);
              tePartIn.Post;
            end else  //�����������
            begin
              if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' �� ������ - ��������� � ������ (0)',nil);
              tePartIn.Append;
              tePartInID.AsInteger:=ptPartOPARTIN.AsInteger;
              tePartInIDSTORE.AsInteger:=0;
              tePartInSSTORE.AsString:='';
              tePartInIDATE.AsInteger:=ptPartOIDATE.AsInteger;
              tePartInIDDOC.AsInteger:=0;
              tePartInARTICUL.AsInteger:=ptPartOARTICUL.AsInteger;
              tePartInDTYPE.AsInteger:=0;
              tePartInQPART.AsFloat:=0;
              tePartInQREMN.AsFloat:=0;
              tePartInPRICEIN.AsFloat:=0;
              tePartInPRICEOUT.AsFloat:=0;
              tePartInSINNCLI.AsString:='';
              tePartInSCLI.AsString:='';
              tePartInPRICEIN0.AsFloat:=0;
              tePartInSumIn0.AsFloat:=0;
              tePartIn.Post;
            end;
          end else //�����������
          begin
            if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' �� �������� ������ - ��������� � ������ (1) ',nil);

            tePartIn.Append;
            tePartInID.AsInteger:=ptPartOPARTIN.AsInteger;
            tePartInIDSTORE.AsInteger:=0;
            tePartInSSTORE.AsString:='';
            tePartInIDATE.AsInteger:=ptPartOIDATE.AsInteger;
            tePartInIDDOC.AsInteger:=0;
            tePartInARTICUL.AsInteger:=ptPartOARTICUL.AsInteger;
            tePartInDTYPE.AsInteger:=0;
            tePartInQPART.AsFloat:=0;
            tePartInQREMN.AsFloat:=0;
            tePartInPRICEIN.AsFloat:=0;
            tePartInPRICEOUT.AsFloat:=0;
            tePartInSINNCLI.AsString:='';
            tePartInSCLI.AsString:='';
            tePartIn.Post;
          end;
        end;

        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' ����� � ������ �������. ',nil);

        tePartO.Append;
        tePartOID.AsInteger:=ptPartOID.AsInteger;
        tePartOARTICUL.AsInteger:=ptPartOARTICUL.AsInteger;
        tePartOIDATE.AsInteger:=ptPartOIDATE.AsInteger;
        tePartOIDSTORE.AsInteger:=ptPartOIDSTORE.AsInteger;
        tePartOITYPE.AsInteger:=ptPartOITYPE.AsInteger;
        tePartOIDDOC.AsInteger:=ptPartOIDDOC.AsInteger;
        tePartOPARTIN.AsInteger:=ptPartOPARTIN.AsInteger;
        tePartOQUANT.AsFloat:=ptPartOQUANT.AsFloat;
        tePartOSUMIN.AsFloat:=ptPartOSUMIN.AsFloat;

        tePartOPRIN.AsFloat:=0;
        if ptPartOQUANT.AsFloat<>0 then tePartOPRIN.AsFloat:=ptPartOSUMIN.AsFloat/ptPartOQUANT.AsFloat;

        tePartOSUMOUT.AsFloat:=ptPartOSUMOUT.AsFloat;

        tePartOPROUT.AsFloat:=0;
        if ptPartOQUANT.AsFloat<>0 then tePartOPROUT.AsFloat:=ptPartOSUMOUT.AsFloat/ptPartOQUANT.AsFloat;

        if ptDep.FindKey([ptPartOIDSTORE.AsInteger]) then  tePartOSSTORE.AsString:=OemToAnsiConvert(ptDepName.AsString)
        else tePartOSSTORE.AsString:='';
        if ptPartOPARTIN.AsInteger<=0 then
        begin
          tePartOSINN.AsString:='';
          tePartOSCLI.AsString:='';
        end else
        begin
          tePartOSINN.AsString:=ptPartOSINN.AsString;
          tePartOSCLI.AsString:=prFindCliName(ptPartOSINN.AsString);
        end;

        tePartOSUMIN0.AsFloat:=ptPartOSUMIN0.AsFloat;
        tePartOPRIN0.AsFloat:=0;
        if ptPartOQUANT.AsFloat<>0 then tePartOPRIN0.AsFloat:=ptPartOSUMIN0.AsFloat/ptPartOQUANT.AsFloat;

        tePartO.Post;

        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' ��. ',nil);

//        inc(iNum);

        ptPartO.Next;
      end;

    finally
      ViewP1.EndUpdate;
      ViewP2.EndUpdate;
//      ViewP1.ViewData.Expand(true);
    end;
  end;
end;

function prReCalcPartO(iPartIn,iCode,iDate,iSkl:Integer):Boolean;
begin
  result:=True;
  if (CommonSet.CalcSeb=0) then exit;
  with dmMT do
  begin
    if ptPartO.Active=False then ptPartO.Active:=True;
    ptPartO.IndexFieldNames:='ARTICUL;IDATE;IDSTORE;ITYPE;IDDOC;PARTIN';
    ptPartO.CancelRange;
    ptPartO.SetRange([iCode,iDate,iSkl],[iCode,Trunc(Date+1),iSkl]);
    ptPartO.First;
    while (not ptPartO.Eof) do
    begin
      if ptPartOPARTIN.AsInteger=iPartIn then
      begin                //������� ������ ������� �� ��������� ��������� ������
        ptPartO.Edit;
        ptPartOPARTIN.AsInteger:=-1;
        ptPartO.Post;
      end;

      ptPartO.Next;
    end;
  end;
end;

function prDelPartIn(iDoc,iType,iDate,iSkl:Integer):Boolean;
begin
  result:=True;
  if (CommonSet.CalcSeb=0) then exit;
  with dmMT do
  begin
    //������ ��� ��������� ������ �� ���������
    if ptPartIN.Active=False then ptPartIn.Active:=True;
    ptPartIn.IndexFieldNames:='IDDOC;DTYPE';
    ptPartIn.CancelRange;
    ptPartIn.SetRange([iDoc,iType],[iDoc,iType]);
    ptPartIn.First;
    while (not ptPartIn.Eof) do
    begin
      //������ ������ � ��������� ������� �� -1
      prReCalcPartO(ptPartINID.AsInteger,ptPartINARTICUL.AsInteger,ptPartINIDATE.AsInteger,iSkl);
      ptPartIn.Delete; //������� �� 1-�� ��������� ������
    end;
  end;
end;

procedure prCalcPrib(iCode,iDateB,iDateE,iDep:INteger; Var rQReal,rSumIn,rSumOut:Real);
begin
  if (CommonSet.CalcSeb=0) or (iCode=0) then exit;
  with dmMT do
  begin
    try
      rQReal:=0;
      rSumIn:=0;
      rSumOut:=0;

      if ptPartO.Active=False then ptPartO.Active:=True;
      ptPartO.IndexFieldNames:='ARTICUL;IDATE;IDSTORE;ITYPE;IDDOC;PARTIN';
      ptPartO.CancelRange;
      ptPartO.SetRange([iCode,iDateB],[iCode,iDateE]);
      ptPartO.First;
      while not ptPartO.Eof do
      begin
        if iDep>0 then
        begin
          if (ptPartOIDSTORE.AsInteger=iDep)and(ptPartOITYPE.AsInteger=7) then
          begin
            rQReal:=rQReal+ptPartOQUANT.AsFloat;
            rSumIn:=rSumIn+ptPartOSUMIN.AsFloat;
            rSumOut:=rSumOut+ptPartOSUMOUT.AsFloat;
          end;
        end else
        begin
{          rQReal:=rQReal+ptPartOQUANT.AsFloat;
          rSumIn:=rSumIn+ptPartOSUMIN.AsFloat;
          rSumOut:=rSumOut+ptPartOSUMOUT.AsFloat;}
        end;
        ptPartO.Next;
      end;

    except
    end;
  end;
end;

function prCalcPartO(iDoc,iType,iCode,iDate,iSkl:Integer;bWr:Boolean;rQo,rPrOut,rSumOut,rPrCli,rPrCli0:Real;sInnCli:String;var rSumIn,rSumIn0:Real;Var sInnIn:String; Var iCli:INteger):Boolean;
Var rPrIn,rQp,rQs,rQ,rSOut,rSOutP,rPrIn0:Real;
    rPrInPart,rPrIn0Part:Real;
    sInn:String;
    bCalc:Boolean;
    iC,iCp:INteger;
begin
  Result:=True;
  sInn:='0'; iCli:=0;
  if (CommonSet.CalcSeb=0) or (iCode=0) then exit;
  with dmMT do
  begin
    if iCode=CommonSet.TestCodeSS then
    begin
      prWH(its(CommonSet.TestCodeSS)+' - ??????. '+fs(rQo),nil);
    end;
    try
      rSumIn:=0;
      rSOut:=rSumOut;
      rSumIn0:=0;
      rPrInPart:=0;
      rPrIn0Part:=0;

      if ptDep.Active=False then ptDep.Active:=True;
      if ptPartO.Active=False then ptPartO.Active:=True;

      if ptPartIN.Active=False then ptPartIn.Active:=True;
      ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
      ptPartIn.CancelRange;
      ptPartIn.SetRange([iSkl,iCode,iDate],[iSkl,iCode,iDate-336]);
      ptPartIn.First; iCp:=0;
      while not ptPartIn.Eof do
      begin
        rPrInPart:=ptPartINPRICEIN.AsFloat;
        rPrIn0Part:=ptPartINPRICEIN0.AsFloat;

        inc(iCp);
        ptPartIn.Next;
        break;
      end;

      ptPartIn.First; iC:=0;
      while (not ptPartIn.Eof) and (ptPartINQREMN.AsFloat>0.00001) do
      begin
        ptPartIn.Next;

        inc(iC);

        if ptPartIn.Eof then
        begin
//          iC:=0;  //???????? ? ????? ?????? ? ???? ????????? ?????? - ?? ? ????????????
          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - iC:=0',nil);
        end else
        begin
          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - inc(iC)',nil);
        end;
      end;//??????? ?? 1-?? ????????? ??????? ?????? ????

      try
        if iC>=1 then
        begin
          ptPartIn.Prior; //??????? ?? 1-?? ?????????
          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ptPartIn.Prior',nil);
        end else
        begin //?????? ????? ???? , ?? ?? ???? ????????? - ????? ????????? ?? ?????????


        end;
      except
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ??1',nil);
      end;

      rQs:=rQo;
      rPrIn:=0; rPrIn0:=0;
      sInn:='';
      iCli:=0;

      if (iCP>0) then //?????-?? ?????? ????????? ????
      begin

      while (not ptPartIn.Bof) and (rQs>0) do //????????????? ??? ?
      begin
        if (sInnCli>'') or (rPrCli>0) then
        //??? ???????? ??? ???????? - ????? ????????? ?????????? ? ???? ?????????? ??? ????????
        begin
          bCalc:=False; //???-?? ?? ??????? - ?? ????????
          if sInnCli>'' then
          begin
            if (ptPartINSINNCLI.AsString=sInnCli) and (abs(ptPartINPRICEIN.AsFloat-rPrCli)<0.05) then  bCalc:=True //????? ? ????????? ? ???? - ???????? ? ???? ???????
          end else  //???? ???????????? ?????? ?? ???? (????????)
          begin
            if (abs(ptPartINPRICEIN.AsFloat-rPrCli)<0.05) then  bCalc:=True //?????? ???? - ???????? ? ???? ???????
          end;
        end else bCalc:=True; //????? ???????

        if bCalc then
        begin
          //???? ???? ??????? ???????? ?? ??? ??????
          if (ptPartINPRICEIN.AsFloat>0.05)or(ptPartINPRICEIN0.AsFloat>0.05) then
          begin
            //????????? ?????????
            rPrIn:=ptPartINPRICEIN.AsFloat;
            sInn:=ptPartINSINNCLI.asstring;
            rPrIn0:=ptPartINPRICEIN0.AsFloat;
            iCli:=ptPartINIDC.AsInteger;    //??? ?????????? ??

            if rPrIn<=0.05 then rPrIn:=rPrIn0;   //????????? ???? ????? ?????? ???? ???? ?????????? ?????? ? ??????? ?????
            if rPrIn0<=0.05 then rPrIn0:=rPrIn;

            rQp:=ptPartINQREMN.AsFloat; //???-?? ????????? ??????
            if rQp>0 then  //???? ????????? ??????
            begin
              if rQs<=rQp then
              begin
                rQ:=rQs;//????????? ?????? ?????? ?????????
                rSOutP:=rSOut;
                rSOut:=0;
              end else
              begin
                rQ:=rQp;
                rSOutP:=rv(rQ*rPrOut*1000)/1000;
                rSOut:=rSOut-rSOutP;
              end;
              rQs:=rQs-rQ;
              //????????? ????????? ??????
              //??????????? ????????? ??????
              if bWr then
              begin

                ptPartO.Append;
                ptPartOIDSTORE.AsInteger:=iSkl;
                ptPartOIDATE.AsInteger:=iDate;
                ptPartOARTICUL.AsInteger:=iCode;
                ptPartOPARTIN.AsInteger:=ptPartINID.AsInteger;
                ptPartOQUANT.AsFloat:=rQ;
                ptPartOSUMIN.AsFloat:=rv(rQ*rPrIn);
                ptPartOSUMIN0.AsFloat:=rv(rQ*rPrIn0);
                ptPartOSUMOUT.AsFloat:=rSOutP;
                ptPartOSINN.AsString:=ptPartINSINNCLI.asstring;
                ptPartOITYPE.AsInteger:=iType;
                ptPartOIDDOC.AsInteger:=iDoc;
                ptPartOIDC.AsInteger:=ptPartINIDC.AsInteger;
                ptPartO.Post;

                if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ptPartO.Append 1 '+fs(ptPartINPRICEIN.AsFloat)+' '+fs(rQ),nil);

                ptPartO.Refresh;
              end;

              rSumIn:=rSumIn+rQ*rPrIn;
              rSumIn0:=rSumIn0+rQ*rPrIn0;

              if bWr then
              begin
                ptPartIn.Edit;
                ptPartINQREMN.AsFloat:=rQp-rQ;
                ptPartIn.Post;
                ptPartIn.Refresh;

                if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ptPartIn.Edit',nil);
              end;
            end;
          end;
        end;
        ptPartIn.Prior;
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ptPartIn.Prior',nil);
      end;
      end;
      if rQs>0 then //???????? ????????????? ??????
      begin
        if (sInnCli>'') or (rPrCli>0) then //??? ????????  ??? ???????? - ???? ??????????? ?????????? ? ???? ??????
        begin
          rPrIn:=rPrCli;
          rPrIn0:=rPrCli0;

          sInn:=sInnCli;
          sInnIn:=sInnCli;
          iCli:=prFindCliCodecb(sInnCli);

          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - rPrIn:=rPrCli '+fs(rPrIn),nil);

        end else
        begin
          if (rPrIn=0) then //???? ?????????? ?????????? ?? ???? ????????? ??????
          begin
            rPrIn:= prFLPriceDateT(iCode,iDate,rPrIn0);
//            rPrIn:= 10;
            sInn:='';
            sInnCli:='';
            iCli:=0;
            if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - rPrIn:= prFLPriceDateT(iCode,iDate) '+fs(rPrIn),nil);
          end;
        end;

        if rPrIn=0 then  //??? ???????, ??? ????????? ?????? - ????? ?? ????????? ?????? ??? ????
        begin
          rPrIn:=rPrInPart;
          rPrIn0:=rPrIn0Part;
        end;

        if bWr then
        begin
// ???? ARTICUL;IDATE;IDSTORE;ITYPE;IDDOC;PARTIN

          ptPartO.Append;
          ptPartOIDSTORE.AsInteger:=iSkl;
          ptPartOIDATE.AsInteger:=iDate;
          ptPartOARTICUL.AsInteger:=iCode;
          ptPartOPARTIN.AsInteger:=0;
          ptPartOQUANT.AsFloat:=rQs;
          ptPartOSUMIN.AsFloat:=rv(rQs*rPrIn);    //
          ptPartOSUMIN0.AsFloat:=rv(rQs*rPrIn0);  //
          ptPartOSUMOUT.AsFloat:=rSOut;           //
          ptPartOSINN.AsString:=sInn;
          ptPartOITYPE.AsInteger:=iType;
          ptPartOIDDOC.AsInteger:=iDoc;
          ptPartOIDC.AsInteger:=iCli;
          ptPartO.Post;

          if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ptPartO.Append 2 '+fs(rPrIn),nil);

          ptPartO.Refresh;
        end;
        rSumIn:=rSumIn+rv(rQs*rPrIn*1000)/1000;
        rSumIn0:=rSumIn0+rv(rQs*rPrIn0*1000)/1000;
      end;
    except
      Result:=False;
      if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ????? ?? ??????. '+fs(rQo),nil);
    end;
    if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' - ?????. '+fs(rQo),nil);
    if iCode=CommonSet.TestCodeSS then prWH(' ',nil);
  end;
end;

function prFixPartRemn(iCode,iDate:Integer):Boolean; //�� ����
//Var rQR:Real;
//    iDateR:INteger;
begin
  Result:=True;
  if CommonSet.CalcSeb=0 then exit;
  with dmMT do
  begin
    try
      if ptDep.Active=False then ptDep.Active:=True;
      if ptPartO.Active=False then ptPartO.Active:=True;
      if ptPartIN.Active=False then ptPartIn.Active:=True;
      ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';

//      iDateR:=iDate-1; //�� ����
      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
{          rQr:=prFindTRemnDepD(iCode,ptDepID.AsInteger,iDateR);
          //����� ������� �� ����� ���� �� ������..      IDSTORE;ARTICUL;IDATE
          ptPartIn.CancelRange;
//          ptPartIn.SetRange([ptDepID.AsInteger,iCode,iDateR],[iSkl,iCode,iDate]);

          //�� ��������� ����
}

        end;
        ptDep.Next;
      end;
    except
      Result:=False;
    end;
  end;
end;


function prDelPartO(iDoc,iType,iCode:Integer):Boolean;
Var rQ:Real;
begin
  Result:=True;
  if CommonSet.CalcSeb=0 then exit;
  with dmMT do
  begin
    if ptPartO.Active=False then ptPartO.Active:=True;
    if ptPartIN.Active=False then ptPartIn.Active:=True;
    ptPartO.IndexFieldNames:='ITYPE;IDDOC;ARTICUL';
    ptPartO.CancelRange;
    ptPartO.SetRange([iType,iDoc,iCode],[iType,iDoc,iCode]);
    try
      ptPartO.First;
      while not ptPartO.Eof do
      begin
        if ptPartIn.FindKey([ptPartOPARTIN.AsInteger]) then  //����������� ������� ������
        begin
          rQ:=ptPartINQREMN.AsFloat+ptPartOQUANT.AsFloat; //����� �������
          if rQ>ptPartINQPART.AsFloat then rQ:=ptPartINQPART.AsFloat;

          ptPartIn.Edit;
          ptPartINQREMN.AsFloat:=rQ;
          ptPartIn.Post;
        end;

        ptPartO.Delete;
      end;
    except
      Result:=False;
    end;
  end;
end;


function prAddPartIn(iDoc,iType,iCode,iDate,iSkl:Integer;sInn:String;rQ,rPrIn,rPrOut,rPrIn0:Real;iCli:Integer):Boolean;
Var // iSS:INteger;
    rPr:Real;
begin
  Result:=True;
  if CommonSet.CalcSeb=0 then exit;
  with dmMT do
  begin
//    iSS:=0;
    if ptPartIN.Active=False then ptPartIn.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

//    if ptDep.Locate('ID',iSkl,[]) then iSS:=ptDepStatus5.AsInteger;

    rPr:=rPrIn;

//  if iSS=2 then rPr:=rPrIn0; //���� ��� ���

    try
      if iCode>0 then
      begin
        ptPartIn.Append;
        ptPartINIDSTORE.AsInteger:=iSkl;
        ptPartINIDATE.AsInteger:=iDate;
        ptPartINIDDOC.AsInteger:=iDoc;
        ptPartINARTICUL.AsInteger:=iCode;
        ptPartINSINNCLI.AsString:=sInn;
        ptPartINDTYPE.AsInteger:=iType;
        ptPartINQPART.AsFloat:=rQ;
        ptPartINQREMN.AsFloat:=rQ;
        ptPartINPRICEIN.AsFloat:=rPr;    //���� � ���
        ptPartINPRICEIN0.AsFloat:=rPrIn0; //���� ��� ���
        ptPartINPRICEOUT.AsFloat:=rPrOut; //���� �������
        ptPartINIDC.AsInteger:=iCli;     //��� ����������
        ptPartIn.Post;
      end;
    except
      ptPartIn.Cancel;
      Result:=False;
    end;
  end;
end;

procedure prDelPartInDoc(iDoc,iType:Integer);
begin
  if CommonSet.CalcSeb=0 then exit;
  with dmMT do
  begin
    if ptPartIN.Active=False then ptPartIn.Active:=True;
    ptPartIn.IndexFieldNames:='IDDOC;DTYPE';
    ptPartIn.CancelRange;
    ptPartIn.SetRange([iDoc,iType],[iDoc,iType]);
    ptPartIn.First;
    while not ptPartIn.Eof do ptPartIn.Delete;
  end;
end;

Procedure prCalcPriceOut(iC,iDate:INteger;rPrIn,rQ:Real;Var rn1,rn2,rn3,rPr,rPrA:Real);
Var rPrNac,rPriceLP,rPriceM,rPrAfter:Real;
    iBStatus:INteger;
begin
  with dmMT do
  begin
    rPr:=0;
    prFindNacGr(iC,rn1,rn2,rn3,rPrNac);
    iBStatus:=prFindBAkciya(iC,Trunc(Date+1));

    if taCards.findkey([iC]) then
    begin
      rPriceLP:=prFLP(iC,rPriceM);
      rPrA:=rPrIn;

      if (abs(rPrIn)<0.5)or(rn1<0.01)or(CommonSet.Single=1)or((iBStatus=2)and(rPrIn<=rPriceLP))or((iBStatus=9)and(rPrIn<=rPriceLP)) then
      begin
        rPr:=taCardsCena.AsFloat; //���� �� ��������
      end
      else
      begin
        //�������� ������ ���� �� �������
        rPr:=rv(rPrIn*(100+rn1)/100); //��������� �������� -  ������� �� ��������

        if taCardsFixPrice.AsFloat>0 then  //���� ������������� ����
        begin
          if (taCardsReserv5.AsFloat>0) then //���� ������� ���� ����������
          begin
            if (taCardsReserv5.AsFloat>=rPrIn) then rPr:= taCardsFixPrice.AsFloat
            else
            begin
              if taCardsFixPrice.AsFloat=taCardsCena.AsFloat then rPr:= taCardsFixPrice.AsFloat
              else
              begin  //���� ���� ����� �����
                if prFindPriceAfterA(iC,iDate,rPrAfter) then  //���� ����� ����
                begin
                  if rPrAfter>0 then rPr:=rPrAfter;  // else  �� ��������� �� ������� �� ���������
                end; // else  �� ��������� �� ������� �� ���������
              end;
            end;
          end else rPr:= taCardsFixPrice.AsFloat  //���� ������� ��� �� ������ ������������� ����
        end
        else //���� �� �������������
        begin
          if (taCardsReserv5.AsFloat>0) then //���� ������� ���� ����������
          begin
            if (taCardsReserv5.AsFloat>=rPrIn) then //���� �������� ������ = �������
            begin
              if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
              begin
                rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
                rPrA:=rPr;
                rPr:=rv(rPr*(100+rn1)/100); //� ��������
              end; //���� �� ���������  // else  �� ��������� �� ������� �� ���������
            end else //���� �������� ������ �������
            begin
              //���� ��������� ����� ��� ��� �� ������� ����
              if taCardsCena.AsFloat<=rv(taCardsReserv5.AsFloat*(100+taCardsReserv2.AsFloat)/100) then
              begin
                if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
                begin
                  rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
                  rPrA:=rPr;
                  rPr:=rv(rPr*(100+rn1)/100); //� ��������
                end; // else  �� ��������� �� ������� �� ���������
              end else
              begin
                if ptSGr.FindKey([taCardsGoodsGroupID.AsInteger]) then  //��������� �� ������� �� ���������
                begin
                  rn1:= ptSGrReserv1.AsFloat;
                  rn2:= ptSGrReserv2.AsFloat;
                  rn3:= ptSGrReserv3.AsFloat;
                end;

                if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
                begin
                  //��������� ����������� ���������� ����
                  rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
                  rPrA:=rPr;
                  rPr:=rv(rPr*(100+rn1)/100); //� �������� � ���������� �� 10 ���
                end else
                  rPr:=rv(rPrIn *(100+rn1)/100); //��������� �������� -  ������� �� ��������
              end;
            end;
          end else //������� ���
          begin
            if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
            begin
              //��������� ����������� ���������� ����
              rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
              rPrA:=rPr;
              rPr:=rv(rPr*(100+rn1)/100); //� �������� � ���������� �� 10 ���
            end; // else  �� ��������� �� ������� �� ���������
          end;
        end;

        if taCardsFixPrice.AsFloat=0 then rPr:=rv(rPr/10)*10;   //���������� �� 10 ���� �� ������������� ����

      end;
    end;
  end;
end;

Procedure prCalcPriceOut1(iC,iDate:INteger;rPrIn,rQ:Real;Var rn1,rn2,rn3,rPr,rPrA:Real);
Var rPrNac,rPriceLP,rPriceM:Real;
    iBStatus:INteger;
begin
  with dmMT do
  begin
    rPr:=0;
    prFindNacGr(iC,rn1,rn2,rn3,rPrNac);
    iBStatus:=prFindBAkciya(iC,Trunc(Date+1));

    if taCards.findkey([iC]) then
    begin
      rPriceLP:=prFLP(iC,rPriceM);
      rPrA:=rPrIn;
      if (abs(rPrIn)<0.1)or(CommonSet.Single=1)or(iBStatus=2)or(iBStatus=9) then rPr:=taCardsCena.AsFloat //���� �������� ���� = 0 �� ���� �� �������� - ��� �����
      else
      begin
        if rn1<0.5 then rPr:=taCardsCena.AsFloat  //���� ������� ��� - �� ������� �� ��������
        else rPr:=rv(rPrIn *(100+rn1)/100); //��������� �������� -  ������� �� ��������

//        rPrA:=rPrIn;

        if (taCardsV02.AsInteger=2)or(taCardsV02.AsInteger=9)or(taCardsV02.AsInteger=4) then
        begin
          if (taCardsV02.AsInteger=4) then  //S-��������
          begin
            if (taCardsReserv5.AsFloat>=rPrIn) then //�������� ���� ������������� �������� �����
            begin
              if taCardsFixPrice.AsFloat>0 then rPr:= taCardsFixPrice.AsFloat //������������� ����
              else
              begin
                if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
                begin
                  //��������� ����������� ���������� ����
                  rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
                  rPrA:=rPr;
                  rPr:=rv(rPr*(100+rn1)/100); //� �������� � ���������� �� 10 ���
                end else
                  rPr:=rv(rPrIn *(100+rn1)/100); //��������� �������� -  ������� �� ��������
              end;
            end else
            begin
              if ptSGr.FindKey([taCardsGoodsGroupID.AsInteger]) then  //��������� �� ������� �� ���������
              begin
                rn1:= ptSGrReserv1.AsFloat;
                rn2:= ptSGrReserv2.AsFloat;
                rn3:= ptSGrReserv3.AsFloat;
              end;

              if rn1<0.5 then rPr:=taCardsCena.AsFloat  //���� ������� ��� - �� ������� �� ��������
              else
              begin
                if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
                begin
                  //��������� ����������� ���������� ����
                  rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
                  rPrA:=rPr;
                  rPr:=rv(rPr*(100+rn1)/100); //� �������� � ���������� �� 10 ���
                end else
                  rPr:=rv(rPrIn *(100+rn1)/100); //��������� �������� -  ������� �� ��������
              end;
            end;
          end;
        end else //��� ��������
        begin
          if rPrIn<rPriceLP then //���� �� ��������� ��� ��������� ��� ��������� ����
          begin
            //��������� ����������� ���������� ����
            if taCardsReserv1.AsFloat>0 then    //���� ������ ��� ������������� ��������
            begin
              rPr:=prCalcPriceRemn(iC,rPrIn,rQ,taCardsReserv1.AsFloat,iDate);
              rPrA:=rPr;

              rPr:=rv(rPr*(100+rn1)/100); //� �������� � ���������� �� 10 ���
            end;
          end;
        end;

        rPr:=rv(rPr/10)*10;   //���������� �� 10

        if taCardsV02.AsInteger<>4 then
          if taCardsFixPrice.AsFloat>0 then
             rPr:= taCardsFixPrice.AsFloat;
      end;
    end;
  end;
end;


function prFindTLastDate(iC:Integer):INteger;
Var iDp:Integer;
begin
  with dmMT do
  begin
    IdP:=0;
    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([iC],[iC]);
    ptTTnInLn.Last;
    while not ptTTnInLn.Bof do
    begin
      iDP:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
      break;
      ptTTnInLn.Prior;
    end;
    Result:=iDP;
  end;
end;

function prFindTFirstDate(iC:Integer):INteger;
Var iDp:Integer;
begin
  with dmMT do
  begin
    IdP:=0;
    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([iC],[iC]);
    ptTTnInLn.First;
    while not ptTTnInLn.Eof do
    begin
      iDP:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
      break;
      ptTTnInLn.Next;
    end;
    Result:=iDP;
  end;
end;


function prCalcPriceRemn(iC:INteger;rPr,rQ,rRemn:Real;iDate:INteger):Real;
Var DateB,DateE:TDateTime;
    rQr,rSum,rPr1:Real;
begin
  with dmMT do
  begin
    Result:=rPr;
    exit;
//    
    Result:=0;
    if (rQ+rRemn)=0 then exit;
    DateB:=Date-100;
    DateE:=iDate-1;
    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    rSum:=rPr*rQ; //��� ����� �� ������� ��������
    rQr:=rRemn; //��� ������� ������� ���� ���������
    rPr1:=0;

    ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([iC,DateB],[iC,DateE]);
    ptTTnInLn.Last;
    while not ptTTnInLn.Bof do
    begin
      rPr1:=ptTTnInLnCenaTovar.AsFloat; //��� ����������� ������ ���� � �����
      if ptTTnInLnKol.AsFloat>=rQr then
      begin
        rSum:=rSum+rQr*ptTTnInLnCenaTovar.AsFloat;
        rQr:=0;
      end else
      begin
        rSum:=rSum+ptTTnInLnKol.AsFloat*ptTTnInLnCenaTovar.AsFloat;
        rQr:=rQr-ptTTnInLnKol.AsFloat;
      end;
      if rQr=0 then Break;

      ptTTnInLn.Prior;
    end;
    if rQr>0 then
    begin
      if rPr1=0 then rPr1:=rPr;
      rSum:=rSum+rQr*rPr1;   //���������� �������
    end;
    Result:=rv(rSum/(rQ+rRemn)); //����������� ���������� ����
  end;
end;

Function prFindPriceDate(iC,iDate:INteger):Real;
Var rPr:Real;
begin
  rPr:=0;
  with dmMt do
  begin
    if ptBufPrice.Active=False then ptBufPrice.Active:=True;
    ptBufPrice.IndexFieldNames:='IDCARD;DATEDOC;NUMDOC';
    ptBufPrice.CancelRange;
    ptBufPrice.SetRange([iC],[iC]);
    ptBufPrice.Last;
    while not ptBufPrice.Bof do
    begin
      if (ptBufPriceSTATUS.AsInteger=1) and (Trunc(ptBufPriceDATEDOC.AsDateTime)<iDate) then
      begin
        rPr:=ptBufPriceNEWP.AsFloat;  //��������� ������������� ���� �� ����
//        Break; //����� �� ���� � �� ������ ������ ���� �������
      end;
      ptBufPrice.Prior;
    end;
    Result:=rPr;
  end;
end;

Function prFindPriceDateMC(iC,iDate:INteger):Real;
Var rPr:Real;
begin
  rPr:=0;
  with dmMt do
  begin
    if Trunc(Date)-iDate<=2 then
    begin
      if taCards.FindKey([iC]) then rPr:=taCardsCena.AsFloat;
    end else
    begin
      if ptBufPrice.Active=False then ptBufPrice.Active:=True;
      ptBufPrice.IndexFieldNames:='IDCARD;DATEDOC;NUMDOC';
      ptBufPrice.CancelRange;
      ptBufPrice.SetRange([iC],[iC]);
      ptBufPrice.Last;
      while not ptBufPrice.Bof do
      begin
        if (ptBufPriceSTATUS.AsInteger=1) and (Trunc(ptBufPriceDATEDOC.AsDateTime)<iDate) then
        begin
          rPr:=ptBufPriceNEWP.AsFloat;  //��������� ������������� ���� �� ����
          Break;
        end;
        ptBufPrice.Prior;
      end;
    end;
    Result:=rPr;
  end;
end;


procedure prAddCashLoad(iC,iT:INteger;rPr,rD:Real);
Var iM:INteger;
    iDate:INteger;
begin
  with dmMt do
  begin
    if ptGdsLoad.Active=False then ptGdsLoad.Active:=True;
//    ptGdsLoad.CancelRange;
    iDate:=Trunc(Date);
    ptGdsLoad.SetRange([iC,iDate],[iC,iDate]);

    iM:=1;

    ptGdsLoad.First;
    while not ptGdsLoad.Eof do begin inc(iM); ptGdsLoad.Next; end;

    ptGdsLoad.Append;
    ptGdsLoadCode.AsInteger:=iC;
    ptGdsLoadIDate.AsInteger:=iDate;
    ptGdsLoadId.AsInteger:=iM;
    ptGdsLoadDateLoad.AsDateTime:=date;
    ptGdsLoadTimeLoad.AsDateTime:=Time;
    if iT=1 then ptGdsLoadCType.AsString:='P';
    if iT=2 then ptGdsLoadCType.AsString:='D';
    if iT=3 then ptGdsLoadCType.AsString:='C';
    if iT=4 then ptGdsLoadCType.AsString:='K';
    if iT=5 then ptGdsLoadCType.AsString:='�';
    ptGdsLoadPrice.AsFloat:=rPr;
    ptGdsLoadDisc.AsFloat:=rD;
    ptGdsLoadPerson.AsString:=CommonSet.Ip+'- '+Person.NameUser+' ('+Person.Name+')';
    ptGdsLoad.Post;

  end;
end;

function prReCalcTMoveLine(iC:INteger):INteger;//�������� �� ������������� ��������� ��������������
Var rQ:Real;
    DateInv:TDateTime;
    iNum,iCurD:INteger;
begin
  with dmMt do
  begin
    Result:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptDep.FindKey([ptCGDEPART.AsInteger]) then
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          ptCM.First;

          Result:=1;
          iNum:=1;
          iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
          rQ:=0;

          DateInv:=0;

          while not ptCM.eof do
          begin
            if iCurD<>Trunc(ptCMMOVEDATE.AsDateTime) then
            begin
              iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
              iNum:=1;
            end;

            if ptCMDOC_TYPE.AsInteger=20 then
            begin
              rQ:=ptCMQUANT_REST.AsFloat;        //��������� ������� �� ����
              DateInv:=ptCMMOVEDATE.AsDateTime;
            end
            else
            begin
              if ptCMMOVEDATE.AsDateTime>DateInv then
              begin
                rQ:=rQ+ptCMQUANT_MOVE.AsFloat;

                ptCM.Edit;
                ptCMPRICE.AsFloat:=iNum;
                ptCMQUANT_REST.AsFloat:=rQ;
                ptCM.Post;
              end;
            end;

            ptCM.Next;  inc(iNum);
          end;
        end;
      end;

      ptCG.Next;
    end;
  end;
end;

function prTestReCalcTMoveLine(iC:INteger):INteger;//�������� �� ������������� ��������� ��������������
Var rQ:Real;
begin
  with dmMt do
  begin
    Result:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptDep.FindKey([ptCGDEPART.AsInteger]) then
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          ptCM.First;

          Result:=0;

          rQ:=0;
          while not ptCM.eof do
          begin
            rQ:=rQ+ptCMQUANT_MOVE.AsFloat;

            ptCM.Next;
          end;
          if abs(ptCMQUANT_REST.AsFloat-rQ)>0.001 then Result:=1;
        end;
      end;
      ptCG.Next;
    end;
  end;
end;

procedure prAddZadan(iType,Id:INteger;S:String);
Var IdZ:INteger;
begin
  with dmMT do  //
  begin
    if ptZadan.Active=False then ptZadan.Active:=True;
    ptZadan.Last;
    IdZ:=ptZadanId.AsInteger+1;
    ptZadan.Append;
    ptZadanId.AsInteger:=idZ;
    ptZadanIType.AsInteger:=iType;
    ptZadanComment.AsString:=AnsiToOemConvert(S);
    ptZadanStatus.AsInteger:=0;
    ptZadanZDate.AsDateTime:=Trunc(Date);
    ptZadanFormDate.AsDateTime:=Trunc(Date);
    ptZadanZTime.AsDateTime:=Time;
    ptZadanFormTime.AsDateTime:=Time;
    ptZadanIdDoc.AsInteger:=Id;
    ptZadanPersonId.AsInteger:=0;
    ptZadanPersonName.AsString:='';
    ptZadan.Post;
  end;
end;


function fShop:INteger;
begin
  with dmMT do  //
  begin
    if ptDep.Active=False then ptDep.Active:=True;

    Result:=0;
    if ptDep.FindKey([1]) then Result:=ptDepStatus1.AsInteger;

    ptDep.Active:=False;
  end;
end;

function fSingle:INteger;
begin
  with dmMT do  //
  begin
    if ptDep.Active=False then ptDep.Active:=True;

    Result:=0;
    if ptDep.FindKey([1]) then Result:=ptDepStatus2.AsInteger;

    ptDep.Active:=False;
  end;
end;

function fShopName:String;
begin
  with dmMT do  //
  begin
    if ptDep.Active=False then ptDep.Active:=True;

    Result:='';
    if ptDep.FindKey([1]) then Result:=OemToAnsiConvert(ptDepName.AsString);

    ptDep.Active:=False;
  end;
end;

function fShopB:INteger;
begin
  with dmMT do  //
  begin
    if ptDep.Active=False then ptDep.Active:=True;

    ptDep.Refresh;
    Result:=0;
    if ptDep.FindKey([1]) then Result:=ptDepStatus2.AsInteger;

    ptDep.Active:=False;
  end;
end;


function PostNum(iTDoc:INteger):INteger;
begin
  with dmMT do
  begin
    if ptACValue.Active=False then ptACValue.Active:=True;

    ptACValue.Append;
    ptACValueAddCharID.AsInteger:=iTDoc;
    ptACValueACValue.AsString:=FormatDateTime('yyyy-mm-dd hh:nn',now);
    ptACValue.Post;

    Result:=ptACValueID.AsInteger;
  end;
end;

procedure prTPriceBuf(IdCard,iDate,IdDoc,iTypeDoc:Integer;DocNum:String;Pr1,Pr2:Real);
Var dDate:TDateTime;
begin
  with dmMT do
  begin
    if ptBufPrice.Active=False then ptBufPrice.Active:=True;

    ptBufPrice.IndexFieldNames:='IDCARD;IDDOC;TYPEDOC';

    dDate:=iDate;

    if ptBufPrice.FindKey([IdCard,iDate,iTypeDoc]) then
    begin
      ptBufPrice.Edit;
      ptBufPriceOLDP.AsFloat:=Pr1;
      ptBufPriceNEWP.AsFloat:=Pr2;
      ptBufPriceSTATUS.AsInteger:=0;
      ptBufPriceNUMDOC.AsString:=DocNum;
      ptBufPriceDATEDOC.AsDateTime:=iDate;

      ptBufPrice.Post;
    end else
    begin
      ptBufPrice.IndexFieldNames:='IDCARD;DATEDOC;NUMDOC';

      if ptBufPrice.FindKey([IdCard,dDate,AnsiToOemConvert(DocNum)]) then
      begin
        ptBufPrice.Edit;
        ptBufPriceOLDP.AsFloat:=Pr1;
        ptBufPriceNEWP.AsFloat:=Pr2;
        ptBufPriceSTATUS.AsInteger:=0;
        ptBufPriceNUMDOC.AsString:=DocNum;
        ptBufPriceDATEDOC.AsDateTime:=iDate;

        ptBufPrice.Post;
      end else
      begin
        try
          ptBufPrice.Append;
          ptBufPriceIDCARD.AsInteger:=IdCard;
          ptBufPriceIDDOC.AsInteger:=IdDoc;
          ptBufPriceTYPEDOC.AsInteger:=iTypeDoc;
          ptBufPriceOLDP.AsFloat:=Pr1;
          ptBufPriceNEWP.AsFloat:=Pr2;
          ptBufPriceSTATUS.AsInteger:=0;
          ptBufPriceNUMDOC.AsString:=DocNum;
          ptBufPriceDATEDOC.AsDateTime:=iDate;
          ptBufPrice.Post;
        except
          ptBufPrice.Cancel;
        end;
      end;
    end;

    ptBufPrice.IndexFieldNames:='IDCARD;IDDOC;TYPEDOC';


//IDCARD   IDDOC   TYPEDOC   OLDP   NEWP   STATUS   NUMDOC  DATEDOC
{    try
      quA.Active:=False;
      quA.SQL.Clear;
      quA.SQL.Add('INSERT into "A_BUFPRICE" values (');
      quA.SQL.Add(its(IdCard)+',');
      quA.SQL.Add(its(IdDoc)+',');
      quA.SQL.Add(its(iTypeDoc)+',');
      quA.SQL.Add(fs(Pr1)+',');
      quA.SQL.Add(fs(Pr2)+',');
      quA.SQL.Add('0,');
      quA.SQL.Add(''''+DocNum+''',');
      quA.SQL.Add(''''+ds(iDate)+'''');
      quA.SQL.Add(')');
      quA.ExecSQL;
    except
    end;}
  end;
end;



Function prFLPT(Id,IndP,CodP:INteger;Var PostDate:TDateTime;Var PostNum:String; Var PostQ,rPriceIn0:Real;Var GTD:String):Real;
Var DateB,DateE:TDateTime;
begin
  with dmMt do
  begin
    Result:=0;
    PostDate:=Date;
    PostNum:='';
    DateB:=Date-730;
    DateE:=Date+1;
    PostQ:=0;
    rPriceIn0:=0;
    GTD:='';

    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    ptTTnInLn.IndexFieldNames:='CodeTovar;IndexPost;CodePost;DateInvoice';

    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([Id,IndP,CodP,DateB],[Id,IndP,CodP,DateE]);
    ptTTnInLn.Last;
    while not ptTTnInLn.Bof do
    begin
      Result:=ptTTnInLnCenaTovar.AsFloat;
      PostDate:=ptTTnInLnDateInvoice.AsDateTime;
      PostNum:=OemToAnsiConvert(ptTTnInLnNumber.AsString);
      PostQ:=ptTTnInLnKol.AsFloat;

      if ptTTnInLnKol.AsFloat<>0 then rPriceIn0:=rv(ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat);

      GTD:=OemToAnsiConvert(ptTTnInLnSertNumber.AsString);

      break;

      ptTTnInLn.Prior;
    end;
  end;
end;

Function prFLPriceDateT1(iCode,iDate:INteger;var iDateIn:INteger; Var rPr0:Real):Real; //+���� ���������� �������
Var DateB,DateE:TDateTime;
begin
  with dmMt do
  begin
    Result:=0;
    DateB:=Date-2095; //3-� ����
    DateE:=iDate+1;
    iDateIn:=Trunc(Date);

    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([iCode,DateB],[iCode,DateE]);
    ptTTnInLn.Last;
    while not ptTTnInLn.Bof do
    begin
      iDateIn:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
      Result:=ptTTnInLnCenaTovar.AsFloat;
      rPr0:=ptTTnInLnCenaTovar.AsFloat;

      if ptTTnInLnKol.AsFloat<>0 then
      begin
        rPr0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat;
      end;

      //���� ��� ������� �.�. ��� ��� ������ �� ���� ������ �.�. ������ ����� ���� ������ � �.�. ����������, �������������� � �.�. �.�. ������ � ������������� ����.���. �� ������ ���������
      break;
      ptTTnInLn.Prior;
    end;
  end;
end;


Function prFLPriceDateT(iCode,iDate:INteger;Var rPr0:Real):Real;
Var DateB,DateE:TDateTime;
begin
  with dmMt do
  begin
    Result:=0;
    DateB:=Date-2095; //3-� ����
    DateE:=iDate+1;

    if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;

    ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

    ptTTnInLn.CancelRange;
    ptTTnInLn.SetRange([iCode,DateB],[iCode,DateE]);
    ptTTnInLn.Last;
    while not ptTTnInLn.Bof do
    begin
      if ptTTnInLnCenaTovar.AsFloat>0.05 then //������� ���� �� �����
      begin
        Result:=ptTTnInLnCenaTovar.AsFloat;
        rPr0:=ptTTnInLnCenaTovar.AsFloat;

        if ptTTnInLnKol.AsFloat<>0 then
        begin
          rPr0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat;
        end;

        //���� ��� ������� �.�. ��� ��� ������ �� ���� ������ �.�. ������ ����� ���� ������ � �.�. ����������, �������������� � �.�. �.�. ������ � ������������� ����.���. �� ������ ���������
        break;
      end;
      ptTTnInLn.Prior;
    end;
  end;
end;

Function prFLPriceDateTVn(iCode,iDate:INteger;Var rPr0:Real):Real;
Var DateB,DateE:TDateTime;
    rPr:Real;
begin
  with dmMt do
  begin
    DateB:=Date-1095;  //3 - ����
    DateE:=iDate+1;
    rPr:=0;

    if ptVnLn.Active=False then ptVnLn.Active:=True;

    ptVnLn.IndexFieldNames:='CodeTovar;DateInvoice';

    ptVnLn.CancelRange;
    ptVnLn.SetRange([iCode,DateB],[iCode,DateE]);
    ptVnLn.Last;
    while not ptVnLn.Bof do
    begin
      if ptVnLnKol.AsFloat<0 then   //������ ������ � �����
      begin
        rPr:=ptVnLnCenaTovarMove.AsFloat;
        if ptVnLnCenaTovarSpis.AsFloat<rPr then rPr:=ptVnLnCenaTovarSpis.AsFloat;
        rPr0:=rPr; //���� ��� �.�. ��. ��������� ������ ��� ��� ������ ����
        break;
      end;  
      ptVnLn.Prior;
    end;
    Result:=rPr;
  end;
end;


procedure prFindNacGr(iC:Integer;Var n1,n2,n3,rPr:Real);
begin
  with dmMt do
  begin
    if taG.Active=False then taG.Active:=True;
    if ptSGr.Active=False then ptSGr.Active:=True;

    n1:=0; n2:=0; n3:=0; rPr:=0;

    if taG.FindKey([iC]) then
    begin
      if taGReserv2.AsFloat>0.0001 then
      begin
        n1:= taGReserv2.AsFloat;
        n2:= taGReserv3.AsFloat;
        n3:= taGReserv4.AsFloat;
        rPr:= taGReserv5.AsFloat;
      end;

      if ptSGr.FindKey([taGGoodsGroupID.AsInteger]) then
      begin
        if n1=0 then n1:= ptSGrReserv1.AsFloat;
        if n2=0 then n2:= ptSGrReserv2.AsFloat;
        if n3=0 then n3:= ptSGrReserv3.AsFloat;
      end;
    end;
  end;
end;

procedure prCalcTaraMove(iT,iD:INteger;Var rQb,rQIn,rSIn,rQOut,rSOut:Real);
Var bR:Boolean;
    rQb1:Real;
begin
   rQIn:=0; rSIn:=0; rQOut:=0; rSOut:=0;
  with dmMt do
  begin
    if ptCT.Active=False then ptCT.Active:=True;
    if ptCTM.Active=False then ptCTM.Active:=True;
    if iD=0 then
    begin //�� ���� ������� �����
      rQb1:=0;
      ptCT.CancelRange;
      ptCT.SetRange([0,iT],[0,iT]);
      ptCT.First;
      while not ptCT.Eof do
      begin
        ptCTM.Last;
        bR:=False;
//        rQb:=ptCTMQUANT_REST.AsFloat;
        rQb:=0;
        while (not ptCTM.Bof)and(ptCTMMOVEDATE.AsDateTime>=CommonSet.DateBeg) do
        begin
          if ptCTMMOVEDATE.AsDateTime<=CommonSet.DateEnd then
          begin
            bR:=True;
            if ptCTMDOC_TYPE.AsInteger=1 then
            begin
              rQb:=ptCTMQUANT_REST.AsFloat-ptCTMQUANT_MOVE.AsFloat;
              rQIn:=rQIn+ptCTMQUANT_MOVE.AsFloat;
              rSIn:=rSIn+ptCTMSUM_MOVE.AsFloat;
            end;
            if (ptCTMDOC_TYPE.AsInteger>=2)and(ptCTMDOC_TYPE.AsInteger<=3) then
            begin
              rQb:=ptCTMQUANT_REST.AsFloat-ptCTMQUANT_MOVE.AsFloat;
              rQOut:=rQOut+ptCTMQUANT_MOVE.AsFloat;
              rSOut:=rSOut+ptCTMSUM_MOVE.AsFloat;
            end;
          end;
          ptCTM.Prior;
        end;
        if bR=False then //�������� � ������ ������� ������ - ������� ������ �����
          if not ptCTM.Bof then rQb:=ptCTMQUANT_REST.AsFloat; //���� ��������� ������ �� ���������

        rQb1:=rQb1+rQb; //������� �� ������

        ptCT.Next;
      end;
      rQb:=rQb1; //������� ��. �������� ��������
    end;
  end;
end;

procedure prRecalcTaraRemns(iDate:INteger);
Var rQR,rQM:Real;
begin
  with dmMT do //
  begin
    if ptCTM.Active=False then exit;
    ptCTM.Last;
    while (ptCTMMOVEDATE.AsDateTime>=iDate)and(ptCTM.Bof=False) do ptCTM.Prior;

    rQR:=0;
    if (ptCTM.Eof=False)and(ptCTMMOVEDATE.AsDateTime<iDate) then
    begin
      rQR:=ptCTMQUANT_REST.AsFloat;
      ptCTM.Next;
    end;

    while not ptCTM.Eof do
    begin
      rQm:=rQR+ptCTMQUANT_MOVE.AsFloat;
      if abs(rQm-ptCTMQUANT_REST.AsFloat)>0.001 then
      begin
        ptCTM.Edit;
        ptCTMQUANT_REST.AsFloat:=rQm;
        ptCTM.Post;

        rQR:=rQm;
      end else rQR:=ptCTMQUANT_REST.AsFloat;

      ptCTM.Next;
    end;
  end;
end;

procedure prAddTaraMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ,rPr:Real);
Var iC:Integer;
    bCG:Boolean;
begin
  with dmMT do //
  begin
    if ptCT.Active=False then ptCT.Active:=True;
    if ptCTM.Active=False then ptCTM.Active:=True;

    ptCT.CancelRange;
    ptCT.SetRange([0,IdCard],[0,IdCard]);
    ptCT.First;
    bCG:=False;
    while not ptCT.Eof do
    begin
      if ptCTDEPART.AsInteger=Depart then
      begin
        bCG:=True;
        iC:=ptCTCARD.AsInteger;
        ptCTM.Last;
        while (ptCTMMOVEDATE.AsDateTime>=iDate)and(ptCTM.Bof=False) do
        begin
          if (ptCTMDOCUMENT.AsInteger=iDoc) and (ptCTMDOC_TYPE.AsInteger=iTD) then
          begin
            ptCTM.Delete;
            Break;
          end;
          ptCTM.Prior;
        end;

        ptCTM.Append;
        ptCTMCARD.AsInteger:=iC;
        ptCTMMOVEDATE.AsDateTime:=iDate;
        ptCTMDOC_TYPE.AsInteger:=iTD;
        ptCTMDOCUMENT.AsInteger:=iDoc;
        ptCTMPRICE.AsFloat:=rPr;
        ptCTMQUANT_MOVE.AsFloat:=rQ;
        ptCTMSUM_MOVE.AsFloat:=RoundEx(rPr*rQ*1000)/1000;
        ptCTMQUANT_REST.AsFloat:=rQ;
        ptCTM.Post;

        prRecalcTaraRemns(iDate);
      end;
      ptCT.Next;
    end;

    if bCG=False then //�������� ����� - ����� ������� �������
    begin
      iC:=prMax('CT')+1;

      ptCT.Append;
      ptCTCARD.AsInteger:=iC;
      ptCTITEM.AsInteger:=IdCard;
      ptCTCOST.AsFloat:=0;
      ptCTDEPART.AsInteger:=Depart;
      ptCTVENDOR.AsInteger:=0;
      ptCTATTRIBUTE.AsInteger:=0;
      ptCT.Post;

      // � ��� ������

      ptCT.CancelRange;
      ptCT.SetRange([0,IdCard],[0,IdCard]);
      ptCT.First;
      while not ptCT.Eof do
      begin
        if ptCTDEPART.AsInteger=Depart then
        begin
          iC:=ptCTCARD.AsInteger;
          ptCTM.Last;
          while (ptCTMMOVEDATE.AsDateTime>=iDate)and(ptCTM.Bof=False) do
          begin
            if (ptCTMDOCUMENT.AsInteger=iDoc) and (ptCTMDOC_TYPE.AsInteger=iTD) then
            begin
              ptCTM.Delete;
              Break;
            end;
            ptCTM.Prior;
          end;

          ptCTM.Append;
          ptCTMCARD.AsInteger:=iC;
          ptCTMMOVEDATE.AsDateTime:=iDate;
          ptCTMDOC_TYPE.AsInteger:=iTD;
          ptCTMDOCUMENT.AsInteger:=iDoc;
          ptCTMPRICE.AsFloat:=rPr;
          ptCTMQUANT_MOVE.AsFloat:=rQ;
          ptCTMSUM_MOVE.AsFloat:=RoundEx(rPr*rQ*1000)/1000;
          ptCTMQUANT_REST.AsFloat:=rQ;
          ptCTM.Post;

          prRecalcTaraRemns(iDate);
          
        end;
        ptCT.Next;
      end;
    end;
//    rQr:=prFindTabRemn(IdCard);
//    prSetR(IdCard,rQR);
  end;
end;


procedure prSetRTara(iC:INteger;rQ:Real);
begin
  with dmMt do
  begin  //������ ���� �������������
{    if taCards.Active=False then taCards.Active:=True;
    if taCards.FindKey([iC]) then
    begin
      taCards.Edit;
      taCardsReserv1.AsFloat:=rQ;
      taCards.Post;
    end;}
  end;
end;


function prFindTaraRemn(iC:Integer):Real;
Var rQ:Real;
begin
  with dmMt do
  begin
    rQ:=0;
    if ptCT.Active=False then ptCT.Active:=True;
    if ptCTM.Active=False then ptCTM.Active:=True;
    ptCT.CancelRange;
    ptCT.SetRange([0,iC],[0,iC]);
    ptCT.First;
    while not ptCT.Eof do
    begin
      ptCTM.Last;
      rQ:=rQ+ptCTMQUANT_REST.AsFloat;
      ptCT.Next;
    end;
    Result:=rQ;
  end;
end;


procedure prDelTaraMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
begin
  with dmMT do  //
  begin
    if ptCT.Active=False then ptCT.Active:=True;
    if ptCTM.Active=False then ptCTM.Active:=True;
    ptCT.CancelRange;
    ptCT.SetRange([0,IdCard],[0,IdCard]);
    ptCT.First;
    while not ptCT.Eof do
    begin
      if ptCTDEPART.AsInteger=Depart then
      begin
        ptCTM.Last;
        while (ptCTMMOVEDATE.AsDateTime>=iDate)and(ptCTM.Bof=False) do
        begin
          if (ptCTMDOCUMENT.AsInteger=iDoc) and (ptCTMDOC_TYPE.AsInteger=iTD) then
          begin
            ptCTM.Delete;
            Break;
          end;

          ptCTM.Prior;
        end;

        prRecalcTaraRemns(iDate);

      end;
      ptCT.Next;
    end;
//    rQr:=prFindTabRemn(IdCard);
//    prSetR(IdCard,rQR);
  end;
end;

procedure prDelTMoveDate(Depart,IdCard,iDate,iTD:Integer);
Var rQR,rQm:Real;
    iNum:Integer;
    iCurD:INteger;
begin
  with dmMT do  //
  begin
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,IdCard],[0,IdCard]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptCGDEPART.AsInteger=Depart then
      begin
        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do
        begin
          //������ ��� �������� �� ���� ��������� �� ����
          if (ptCMMOVEDATE.AsDateTime=iDate)and(ptCMDOC_TYPE.AsInteger=iTD) then ptCM.Delete
          else ptCM.Prior;
        end;

        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do ptCM.Prior;  //��� � ������

        rQR:=0;
        iCurD:=0;
        iNum:=1;

        if (ptCM.Eof=False)and(ptCMMOVEDATE.AsDateTime<iDate) then
        begin
          rQR:=ptCMQUANT_REST.AsFloat;
          ptCM.Next;
        end;

        while not ptCM.Eof do
        begin
          if iCurD<>Trunc(ptCMMOVEDATE.AsDateTime) then
          begin
            iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
            iNum:=1;
          end;

          rQm:=rQR+ptCMQUANT_MOVE.AsFloat;
{          if abs(rQm-ptCMQUANT_REST.AsFloat)>0.001 then
          begin
            ptCM.Edit;
            ptCMQUANT_REST.AsFloat:=rQm;
            ptCM.Post;

            rQR:=rQm;
          end else rQR:=ptCMQUANT_REST.AsFloat;}

          ptCM.Edit;
          ptCMPRICE.AsFloat:=iNum;
          ptCMQUANT_REST.AsFloat:=rQm;
          ptCM.Post;

          rQR:=rQm;

          ptCM.Next; inc(iNum);
        end;
      end;
      ptCG.Next;
    end;
    rQr:=prFindTabRemn(IdCard);
    prSetR(IdCard,rQR);
  end;
end;


procedure prReCalcTMove(iCg,iDate:INteger);
Var rQR,rQm:Real;
begin
with dmMT do  //
  begin
    if ptCM1.Active=False then ptCM1.Active:=True;
    ptCM1.CancelRange;
    ptCM1.SetRange([iCg],[iCg]);

    ptCM1.Last;
    while (ptCM1MOVEDATE.AsDateTime>=iDate)and(ptCM1.Bof=False) do ptCM1.Prior;

    if not ptCM1.Eof then
    begin
      rQR:=ptCM1QUANT_REST.AsFloat;
      ptCM1.Next;
      while not ptCM1.Eof do
      begin
        rQm:=rQR+ptCM1QUANT_MOVE.AsFloat;
        if abs(rQm-ptCM1QUANT_REST.AsFloat)>0.001 then
        begin
          ptCM1.Edit;
          ptCM1QUANT_REST.AsFloat:=rQm;
          ptCM1.Post;

          rQR:=rQm;
        end else rQR:=ptCM1QUANT_REST.AsFloat;

        ptCM1.Next;
      end;
    end;
  end;
end;

function prFindTRemnDepD(iC,iD,dD:Integer):Real;
Var rQ:Real;
    MoveD:TDateTime;
begin
  with dmMt do
  begin
    rQ:=0;
    MoveD:=dD;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if iD>0 then //������� ����������� ������ �������
      begin
        if ptCGDEPART.AsInteger=iD then
        begin
          ptCM.Last;
          while not ptCM.Bof do
          begin
            if MoveD>=ptCMMOVEDATE.AsDateTime then
            begin
              rQ:=ptCMQUANT_REST.AsFloat;  //���� ������ ������ � ����� ������� ��������
              break;
            end;
            ptCM.Prior;
          end;
        end;
      end else
      begin //������� ������� �� ���� �������
        if iD=0 then //������� �� ���������� ������� - ����������
        begin
          if ptDep.FindKey([ptCGDEPART.AsInteger]) then
          begin
            if ptDepV01.AsInteger>0 then
            begin
              ptCM.Last;
              while not ptCM.Bof do
              begin
                if MoveD>=ptCMMOVEDATE.AsDateTime then
                begin
                  rQ:=rQ+ptCMQUANT_REST.AsFloat;
                  break;
                end;
                ptCM.Prior;
              end;
            end;
          end;
        end else // ������ �� ����
        begin
          ptCM.Last;
          while not ptCM.Bof do
          begin
            if MoveD>=ptCMMOVEDATE.AsDateTime then
            begin
              rQ:=rQ+ptCMQUANT_REST.AsFloat;
              break;
            end;
            ptCM.Prior;
          end;
        end;
      end;
      ptCG.Next;
    end;
    Result:=rQ;
  end;
end;


Function prFindReceipt(iC:INteger):String;
begin
  with dmMt do
  begin
    if taACards.Active=False then taACards.Active:=True;
    if taACards.FindKey([iC]) then
    begin
      Result:=taACardsRECEIPT.AsString;
    end else Result:='';
  end;
end;


Function prFindPrice(iC:INteger):Real;
begin
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;
    if taCards.FindKey([iC]) then
    begin
      Result:=taCardsCena.AsFloat;
    end else Result:=0;
  end;
end;

procedure prCalcTMove(iCg:INteger;Var rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb:Real);
begin
  with dmMT do  //
  begin
    if ptCM1.Active=False then ptCM1.Active:=True;

    rQIn:=0; rQOut:=0; rQReal:=0; rQvnIn:=0; rQVnOut:=0; rQInv:=0; rQb:=0;

    ptCM1.CancelRange;
    ptCM1.SetRange([iCg,CommonSet.DateBeg],[iCg,CommonSet.DateEnd]);

    ptCM1.Last;
    while ptCM1.Bof=False do
    begin
      if ptCM1DOC_TYPE.AsInteger=1 then begin rQIn:=rQIn+ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat;  end;
      if ptCM1DOC_TYPE.AsInteger=2 then begin rQOut:=rQOut+(-1)*ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
      if ptCM1DOC_TYPE.AsInteger=3 then begin rQVnIn:=rQVnIn+ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
      if ptCM1DOC_TYPE.AsInteger=4 then begin rQVnOut:=rQVnOut+(-1)*ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
      if ptCM1DOC_TYPE.AsInteger=7 then begin rQReal:=rQReal+(-1)*ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
      if ptCM1DOC_TYPE.AsInteger=20 then begin rQInv:=rQInv+ptCM1QUANT_MOVE.AsFloat; rQb:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;

      ptCM1.Prior;
    end;
    if (rQb=0)and(rQIn=0)and(rQvnIn=0)and(rQInv=0)and(rQVnOut=0)and(rQReal=0)and(rQOut=0) then
    begin
      ptCM1.CancelRange;
      ptCM1.SetRange([iCg,Date-365],[iCg,CommonSet.DateBeg]);
      ptCM1.Last;
      try
        rQb:=ptCM1QUANT_REST.AsFloat;
      except
      end;
    end;
  end;
end;

procedure prCalcTMoveAllDep(iC,iD:INteger;Var rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb:Real);
Var iCg,iDCur:INteger;
    rQbWk:Real;
begin
  with dmMT do  //
  begin
    if ptDep.Active=False then ptDep.Active:=True;
    if ptCM1.Active=False then ptCM1.Active:=True;

    rQIn:=0; rQOut:=0; rQReal:=0; rQvnIn:=0; rQVnOut:=0; rQInv:=0; rQb:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;
    if ptCM1.Active=False then ptCM1.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      iCg:=ptCGCard.AsInteger;
      rQbWk:=0;

      if iD=0 then iDCur:=ptCGDEPART.AsInteger else iDCur:=iD; //��� ������ - ���� �� ���� ������� , ���� �� ������

      if ptDep.FindKey([ptCGDEPART.AsInteger]) then
      begin
        if (ptDepStatus1.AsInteger=9)and(ptDepID.AsInteger=iDCur) then  //������ �������� ������ � ��� ��� �����
        begin

          ptCM1.CancelRange;
          ptCM1.SetRange([iCg,CommonSet.DateBeg],[iCg,CommonSet.DateEnd]);

          ptCM1.Last;
          while ptCM1.Bof=False do
          begin
            if ptCM1DOC_TYPE.AsInteger=1 then begin rQIn:=rQIn+ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat;  end;
            if ptCM1DOC_TYPE.AsInteger=2 then begin rQOut:=rQOut+(-1)*ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
            if ptCM1DOC_TYPE.AsInteger=3 then begin rQVnIn:=rQVnIn+ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
            if ptCM1DOC_TYPE.AsInteger=4 then begin rQVnOut:=rQVnOut+(-1)*ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
            if ptCM1DOC_TYPE.AsInteger=7 then begin rQReal:=rQReal+(-1)*ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;
            if ptCM1DOC_TYPE.AsInteger=20 then begin rQInv:=rQInv+ptCM1QUANT_MOVE.AsFloat; rQbWk:=ptCM1QUANT_REST.AsFloat-ptCM1QUANT_MOVE.AsFloat; end;

            ptCM1.Prior;
          end;
          if (rQbWk=0)and(rQIn=0)and(rQvnIn=0)and(rQInv=0)and(rQVnOut=0)and(rQReal=0)and(rQOut=0) then
          begin
            ptCM1.CancelRange;
            ptCM1.SetRange([iCg,Date-365],[iCg,CommonSet.DateBeg]);
            ptCM1.Last;
            try
              rQbWk:=ptCM1QUANT_REST.AsFloat;
            except
            end;
          end;
        end;
      end;

      rQb:=rQb+rQbWk;
      ptCG.Next;
    end;
  end;
end;



Function prFindTNameGroup(iGr:Integer):String;
begin
  with dmMT do  //
  begin
    Result:='';
    if ptSGr.Active=False then ptSGr.Active:=True;
    if ptSGr.FindKey([iGr]) then  Result:=OemToAnsiConvert(ptSGrName.AsString);
  end;
end;


Function prFindTNameGr(iGr:Integer):String;
Var n:INteger;
    iGr0:INteger;
begin
  with dmMT do  //
  begin
    Result:='';
    n:=0;
    if ptSGr.Active=False then ptSGr.Active:=True;
    iGr0:=iGr;
    while (iGr0<>0) and (n<10) do //����������� ����� �� ������ ������ �� 10 ���
    begin
      if ptSGr.FindKey([iGr0]) then iGr0:=ptSGrGoodsGroupID.AsInteger;
      inc(n);
    end;
    if iGr0=0 then Result:=OemToAnsiConvert(ptSGrName.AsString);
  end;
end;

function prFindTNameMGr(iGr:INteger):String;
begin
  with dmMT do  //
  begin
    Result:='';
    if ptMGr.Active=False then ptMGr.Active:=True;
    if ptMGr.FindKey([iGr]) then Result:=OemToAnsiConvert(ptMGrName.AsString);
  end;
end;

procedure prDelTMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
Var rQR,rQm:Real;
    iNum:Integer;
    iCurD:INteger;
begin
  with dmMT do  //
  begin
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,IdCard],[0,IdCard]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptCGDEPART.AsInteger=Depart then
      begin
        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do
        begin
          if (ptCMDOCUMENT.AsInteger=iDoc) and (ptCMDOC_TYPE.AsInteger=iTD) then
          begin
            ptCM.Delete;
            Break;
          end;

          ptCM.Prior;
        end;

        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do ptCM.Prior;


        rQR:=0;
        iCurD:=0;
        iNum:=1;
       
        if (ptCM.Eof=False)and(ptCMMOVEDATE.AsDateTime<iDate) then
        begin
          rQR:=ptCMQUANT_REST.AsFloat;
          ptCM.Next;
        end;

        while not ptCM.Eof do
        begin
          if iCurD<>Trunc(ptCMMOVEDATE.AsDateTime) then
          begin
            iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
            iNum:=1;
          end;

          rQm:=rQR+ptCMQUANT_MOVE.AsFloat;
{          if abs(rQm-ptCMQUANT_REST.AsFloat)>0.001 then
          begin
            ptCM.Edit;
            ptCMQUANT_REST.AsFloat:=rQm;
            ptCM.Post;

            rQR:=rQm;
          end else rQR:=ptCMQUANT_REST.AsFloat;}

          ptCM.Edit;
          ptCMPRICE.AsFloat:=iNum;
          ptCMQUANT_REST.AsFloat:=rQm;
          ptCM.Post;

          rQR:=rQm;

          ptCM.Next; inc(iNum);
        end;

      end;
      ptCG.Next;
    end;
    rQr:=prFindTabRemn(IdCard);
    prSetR(IdCard,rQR);
  end;
end;


procedure prDelTMoveIdC(IdCard,iDate,iDoc,iTD:Integer);
Var rQR,rQm:Real;

begin
  with dmMT do  //
  begin
    if ptCM1.Active=False then ptCM1.Active:=True;
    ptCM1.CancelRange;
    ptCM1.SetRange([IdCard],[IdCard]);

    ptCM1.Last;
    while (ptCM1MOVEDATE.AsDateTime>=iDate)and(ptCM1.Bof=False) do
    begin
      if (ptCM1DOCUMENT.AsInteger=iDoc) and (ptCM1DOC_TYPE.AsInteger=iTD) then
      begin
        ptCM1.Delete;
        Break;
      end;
      ptCM1.Prior;
    end;

    ptCM1.Last;
    while (ptCM1MOVEDATE.AsDateTime>=iDate)and(ptCM1.Bof=False) do ptCM1.Prior;

    rQR:=0;
    if (ptCM1.Eof=False)and(ptCM1MOVEDATE.AsDateTime<iDate) then
    begin
      rQR:=ptCM1QUANT_REST.AsFloat;
      ptCM1.Next;
    end;

    while not ptCM1.Eof do
    begin
      rQm:=rQR+ptCM1QUANT_MOVE.AsFloat;
      if abs(rQm-ptCM1QUANT_REST.AsFloat)>0.001 then
      begin
        ptCM1.Edit;
        ptCM1QUANT_REST.AsFloat:=rQm;
        ptCM1.Post;

        rQR:=rQm;
      end else rQR:=ptCM1QUANT_REST.AsFloat;

      ptCM1.Next;
    end;
    {   ���� ������� �.�. IdCard ���� ����������, �������� � ����� �� �������� ��� ����� ����� ��� ���������
    rQr:=prFindTabRemn(IdCard);
    prSetR(IdCard,rQR);
    }
  end;
end;


procedure prFillMove(iC:INteger);
begin
  with dmMT do  //
  begin
    CloseTe(meMove);

    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      ptCM.Last;
      while (ptCMMOVEDATE.AsDateTime>=CommonSet.DateBeg)and(ptCM.Bof=False) do
      begin
        if ptCMMOVEDATE.AsDateTime<=CommonSet.DateEnd then
        begin //����������
          ptDep.FindKey([ptCGDEPART.AsInteger]);

          meMove.Append;
          meMoveCard.AsInteger:=ptCGCARD.AsInteger;
          meMoveMoveDate.AsDateTime:=ptCMMOVEDATE.AsDateTime;
          meMoveDoc_Type.AsInteger:=ptCMDOC_TYPE.AsInteger;
          meMoveDocument.AsInteger:=ptCMDOCUMENT.AsInteger;
          meMoveQuantMove.AsFloat:=ptCMQUANT_MOVE.AsFloat;
          meMoveQuantRest.AsFloat:=ptCMQUANT_REST.AsFloat;
          meMoveName.AsString:=OemToAnsiConvert(ptDepName.AsString);
          meMoveDepart.AsInteger:=ptCGDEPART.AsInteger;

          if ptCMDOC_TYPE.AsInteger in [1] then
          begin
            meMoveQIn.AsFloat:=ptCMQUANT_MOVE.AsFloat;
            meMoveQOut.AsFloat:=0;
          end;
          if ptCMDOC_TYPE.AsInteger in [7] then
          begin
            meMoveQIn.AsFloat:=0;
            meMoveQOut.AsFloat:=ptCMQUANT_MOVE.AsFloat;
          end;
          if ptCMDOC_TYPE.AsInteger in [4] then
          begin
            if ptCMQUANT_MOVE.AsFloat>=0 then
            begin
              meMoveQIn.AsFloat:=ptCMQUANT_MOVE.AsFloat;
              meMoveQOut.AsFloat:=0;
            end else
            begin
              meMoveQIn.AsFloat:=0;
              meMoveQOut.AsFloat:=ptCMQUANT_MOVE.AsFloat;
            end;
          end;
          if ptCMDOC_TYPE.AsInteger in [3] then
          begin
            if ptCMQUANT_MOVE.AsFloat>=0 then
            begin
              meMoveQIn.AsFloat:=ptCMQUANT_MOVE.AsFloat;
              meMoveQOut.AsFloat:=0;
            end else
            begin
              meMoveQIn.AsFloat:=0;
              meMoveQOut.AsFloat:=ptCMQUANT_MOVE.AsFloat;
            end;
          end;
          if ptCMDOC_TYPE.AsInteger in [2,15,20] then
          begin
            if ptCMQUANT_MOVE.AsFloat>=0 then
            begin
              meMoveQIn.AsFloat:=ptCMQUANT_MOVE.AsFloat;
              meMoveQOut.AsFloat:=0;
            end else
            begin
              meMoveQIn.AsFloat:=0;
              meMoveQOut.AsFloat:=ptCMQUANT_MOVE.AsFloat;
            end;
          end;

          meMoveNum.AsInteger:=Trunc(ptCMPRICE.AsFloat);

          meMove.Post;
        end;

        ptCM.Prior;
      end;

      ptCG.Next;
    end;
  end;
end;

procedure prAddTMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
Var rQR,rQm:Real;
    iC:Integer;
    bCG:Boolean;
    iNum:Integer;
    iCurD:INteger;
begin
  with dmMT do  //
  begin
  
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,IdCard],[0,IdCard]);
    ptCG.First;
    bCG:=False;
    while not ptCG.Eof do
    begin
      if ptCGDEPART.AsInteger=Depart then
      begin
        bCG:=True;
        iC:=ptCGCARD.AsInteger;
        iNum:=1;
        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do
        begin
          if (ptCMDOCUMENT.AsInteger=iDoc) and (ptCMDOC_TYPE.AsInteger=iTD) then
          begin
            ptCM.Delete;
            Break;
          end;
          ptCM.Prior;
        end;

        ptCM.Append;
        ptCMCARD.AsInteger:=iC;
        ptCMMOVEDATE.AsDateTime:=iDate;
        ptCMDOC_TYPE.AsInteger:=iTD;
        ptCMDOCUMENT.AsInteger:=iDoc;
        ptCMPRICE.AsFloat:=iNum;
        ptCMQUANT_MOVE.AsFloat:=rQ;
        ptCMSUM_MOVE.AsFloat:=0;
        ptCMQUANT_REST.AsFloat:=rQ;
        ptCM.Post;

        ptCM.Last;
        while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do ptCM.Prior;

        if not ptCM.Eof then
        begin
          rQR:=ptCMQUANT_REST.AsFloat;
          iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
          ptCM.Next;
          while not ptCM.Eof do
          begin
            if iCurD<>Trunc(ptCMMOVEDATE.AsDateTime) then
            begin
              iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
              iNum:=1;
            end;
            rQm:=rQR+ptCMQUANT_MOVE.AsFloat;
          {  if abs(rQm-ptCMQUANT_REST.AsFloat)>0.001 then
            begin
              ptCM.Edit;
              ptCMPRICE.AsFloat:=iNum;
              ptCMQUANT_REST.AsFloat:=rQm;
              ptCM.Post;

              rQR:=rQm;
            end else rQR:=ptCMQUANT_REST.AsFloat;}
            //��������� ��� ��������� ����������

            ptCM.Edit;
            ptCMPRICE.AsFloat:=iNum;
            ptCMQUANT_REST.AsFloat:=rQm;
            ptCM.Post;

            rQR:=rQm;

            ptCM.Next; inc(iNum);
          end;
        end;
      end;
      ptCG.Next;
    end;

    if bCG=False then //�������� ����� - ����� ������� �������
    begin
      iC:=prMax('CG')+1;
      iNum:=1;

      ptCG.Append;
      ptCGCARD.AsInteger:=iC;
      ptCGITEM.AsInteger:=IdCard;
      ptCGCOST.AsFloat:=0;
      ptCGDEPART.AsInteger:=Depart;
      ptCGVENDOR.AsInteger:=0;
      ptCGATTRIBUTE.AsInteger:=0;
      ptCG.Post;

      // � ��� ������

      ptCG.CancelRange;
      ptCG.SetRange([0,IdCard],[0,IdCard]);
      ptCG.First;
      while not ptCG.Eof do
      begin
        if ptCGDEPART.AsInteger=Depart then
        begin
          iC:=ptCGCARD.AsInteger;
          ptCM.Last;
          while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do
          begin
            if (ptCMDOCUMENT.AsInteger=iDoc) and (ptCMDOC_TYPE.AsInteger=iTD) then
            begin
              ptCM.Delete;
              Break;
            end;
            ptCM.Prior;
          end;

          ptCM.Append;
          ptCMCARD.AsInteger:=iC;
          ptCMMOVEDATE.AsDateTime:=iDate;
          ptCMDOC_TYPE.AsInteger:=iTD;
          ptCMDOCUMENT.AsInteger:=iDoc;
          ptCMPRICE.AsFloat:=iNum;
          ptCMQUANT_MOVE.AsFloat:=rQ;
          ptCMSUM_MOVE.AsFloat:=0;
          ptCMQUANT_REST.AsFloat:=rQ;
          ptCM.Post;

          ptCM.Last;
          while (ptCMMOVEDATE.AsDateTime>=iDate)and(ptCM.Bof=False) do ptCM.Prior;

          if not ptCM.Eof then
          begin
            rQR:=ptCMQUANT_REST.AsFloat;
            iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
            ptCM.Next;
            while not ptCM.Eof do
            begin
              if iCurD<>Trunc(ptCMMOVEDATE.AsDateTime) then
              begin
                iCurD:=Trunc(ptCMMOVEDATE.AsDateTime);
                iNum:=1;
              end;
              rQm:=rQR+ptCMQUANT_MOVE.AsFloat;

          {    if abs(rQm-ptCMQUANT_REST.AsFloat)>0.001 then
              begin
                ptCM.Edit;
                ptCMQUANT_REST.AsFloat:=rQm;
                ptCM.Post;

                rQR:=rQm;
              end else rQR:=ptCMQUANT_REST.AsFloat; }

              ptCM.Edit;
              ptCMPRICE.AsFloat:=iNum;
              ptCMQUANT_REST.AsFloat:=rQm;
              ptCM.Post;

              rQR:=rQm;

              ptCM.Next; inc(iNum);
            end;
          end;
        end;
        ptCG.Next;
      end;
    end;
    rQr:=prFindTabRemn(IdCard);
    prSetR(IdCard,rQR);
  end;
end;


function prFindTLastDep(iC:Integer):Integer;
Var PostDate:TDateTime;
    iD:INteger;
begin
  iD:=0;
  with dmMt do
  begin
    PostDate:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      ptCM.Last;
      if PostDate<=ptCMMOVEDATE.AsDateTime then
      begin
        PostDate:=ptCMMOVEDATE.AsDateTime;
        iD:=ptCGDEPART.AsInteger;
      end;
      ptCG.Next;
    end;
    Result:=iD;
  end;
end;

function prFindTRemnDep(iC,iD:Integer):Real;  //������� �� ������
Var rQ:Real;
begin
  with dmMt do
  begin
    rQ:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptCGDEPART.AsInteger=iD then
      begin
        ptCM.Last;
        rQ:=rQ+ptCMQUANT_REST.AsFloat;
        break;
      end;
      ptCG.Next;
    end;
    Result:=rQ;
  end;
end;

procedure prSetR(iC:INteger;rQ:Real);
begin
  with dmMt do
  begin
    if taCards.Active=False then taCards.Active:=True;
    if taCards.FindKey([iC]) then
    begin
      taCards.Edit;
      taCardsReserv1.AsFloat:=rQ;
      taCards.Post;
    end;
  end;
end;

function prFindTCGRemn(iC:Integer):Real; //����� �� cardGoodsMove
Var rQ:Real;
begin
  with dmMt do
  begin
    rQ:=0;
    if ptCM1.Active=False then ptCM1.Active:=True;
    ptCM1.CancelRange;
    ptCM1.SetRange([iC],[iC]);
    ptCM1.First;
    while not ptCM1.Eof do
    begin
      ptCM1.Last;
      rQ:=ptCM1QUANT_REST.AsFloat;
      ptCM1.Next;
    end;
    Result:=rQ;
  end;
end;

function prFindTabRemn(iC:Integer):Real;
Var rQ,rQe:Real;
    dDateM:TDateTime;
begin
  with dmMt do
  begin
    rQ:=0;

    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptDep.FindKey([ptCGDEPART.AsInteger]) then
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          ptCM.Last;
          rQe:=ptCMQUANT_REST.AsFloat;
          dDateM:=ptCMMOVEDATE.AsDateTime;
          while (dDateM=ptCMMOVEDATE.AsDateTime)and(ptCM.Bof=False) do
          begin
            if ptCMDOC_TYPE.AsInteger=20 then  rQe:=ptCMQUANT_REST.AsFloat; //���� � ���� ���� ���� ��������������
            ptCM.Prior;
          end;
//          rQ:=rQ+ptCMQUANT_REST.AsFloat;
          if (Date-dDateM)>1000 then rQe:=0; //��������� �������� ���� 3-� ���� ����� -> 0

          rQ:=rQ+rQe;
        end;
      end;
      ptCG.Next;
    end;
    Result:=RoundEx(rQ*1000)/1000;
  end;
end;

function prFindLastDateRemn(iC:Integer;Var LDate:TDateTime):Real;
Var rQ,rQe:Real;
    dDateM:TDateTime;
begin
  with dmMt do
  begin
    LDate:=Date-1000;
    rQ:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptDep.FindKey([ptCGDEPART.AsInteger]) then
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          ptCM.Last;

          if ptCMMOVEDATE.AsDateTime>LDate then LDate:=ptCMMOVEDATE.AsDateTime;
          rQe:=ptCMQUANT_REST.AsFloat;

          dDateM:=ptCMMOVEDATE.AsDateTime;
          while (dDateM=ptCMMOVEDATE.AsDateTime)and(ptCM.Bof=False) do
          begin
            if ptCMDOC_TYPE.AsInteger=20 then  rQe:=ptCMQUANT_REST.AsFloat; //���� � ���� ���� ���� ��������������
            ptCM.Prior;
          end;
//          rQ:=rQ+ptCMQUANT_REST.AsFloat;
          rQ:=rQ+rQe;

        end;
      end;
      ptCG.Next;
    end;
    Result:=rQ;
  end;
end;

function prFindLastDateRemnDep(iC,iDep:Integer;Var LDate:TDateTime):Real;
Var rQ:Real;
begin
  with dmMt do
  begin
    LDate:=Date-1000;
    rQ:=0;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if ptCGDEPART.AsInteger=iDep then
      begin
        ptCM.Last;
        rQ:=rQ+ptCMQUANT_REST.AsFloat;
        if ptCMMOVEDATE.AsDateTime>LDate then LDate:=ptCMMOVEDATE.AsDateTime;
      end;
      ptCG.Next;
    end;
    Result:=rQ;
  end;
end;

function prFindTabSpeedRemn(iC,iTest:Integer;Var rSpeedDay,rQday:Real):Real;
Type TPrih = record
     QIn,QReal,QRemn,AvrR:Real;
     DDate:TDateTime;
     iDayReal,k:Integer;
     end;

Var rQ:Real;
    ArPrih:array[1..5] of TPrih;
    n,maxn,k,nDep,kDov:Integer;
    ArSpeed:Array[1..50] of Real; //������������ ����� ������� � �������
    maxDate:TDateTime;
    AvrReal:Real;
begin
  with dmMt do
  begin
    rQ:=0;
    rSpeedDay:=0;
    rQDay:=0;
    ndep:=1; //���������� ����� ������������

    for n:=1 to 50 do ArSpeed[n]:=0;

    //������ �������� ������ ��������� �������� �� ���� ������� � �����������

    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);

    //�������� ��������� - ��������� �� ����� ������� � ���� ��������� 5-� ��������� ��������
    //���� ��� ���� c ������������ � 183 ���� (�������) �.�. ������ �������� �����������

    ptCG.First;
    while not ptCG.Eof do
    begin
      for n:=1 to 5 do //�������� ��� �� ������
      begin
        ArPrih[n].QIn:=0; ArPrih[n].QReal :=0; ArPrih[n].QRemn:=0; ArPrih[n].DDate:=date; ArPrih[n].iDayReal:=0;
        ArPrih[n].k:=0; ArPrih[n].AvrR:=0;
      end;

      n:=0;
      ptCM.Last;

      rQ:=rQ+ptCMQUANT_REST.AsFloat; //����� ������� ������

      while (ptCM.Bof=False) and (n<CommonSet.SpeedReadPrihods) and (ptCMMOVEDATE.AsDateTime>(Date-CommonSet.SpeedRealDays)) do
      begin
        if (ptCMDOC_TYPE.AsInteger=1) and ((Date-ptCMMOVEDATE.AsDateTime)>CommonSet.SpeedRealNotLasDays) then // �� ������� ������� �� 5� ��������� ���� �.�. ������ ���� �� ��� ����������, ����� ���� ���������� ������ ���� 5� ����.
        begin //��� ������ ������
          inc(n);
          ArPrih[n].QIn:=ptCMQUANT_MOVE.AsFloat;
          ArPrih[n].DDate:=ptCMMOVEDATE.AsDateTime;
          ArPrih[n].QRemn:=ptCMQUANT_MOVE.AsFloat; //���������� ��������� ������� �������
        end;
        ptCM.Prior;
      end;

      //������� �������� ����� ������� � ������������
      if n>0 then
      begin
        maxn:=n; // ����� ���� �� 5�
      end  
      else    //���� �������� ������ - ������� ���������
      begin
        maxn:=1;             //������ ����� ��������� ������
        ArPrih[1].QIn:=10000;
        ArPrih[1].DDate:=Date-CommonSet.SpeedRealDays;
        ArPrih[1].QRemn:=10000; //���������� ��������� ������� �������
      end;

      MaxDate:=ArPrih[maxn].DDate; //������������ ���� � ������ �������� �������

     //��� ������������ �������� � 5� �������� ���������� ���� ���� ����� �� ������ ������  ���� ��������� ���������� ��� �������
      while (ptCMMOVEDATE.AsDateTime>=MaxDate)and(ptCM.Bof=False) do ptCM.Prior;

      // ��� ������� ������

      while not ptCM.Eof do //����� �������� ����� �� Break
      begin
        if (ArPrih[1].QRemn<=0)and(ArPrih[2].QRemn<=0)and(ArPrih[3].QRemn<=0)and(ArPrih[4].QRemn<=0)and(ArPrih[5].QRemn<=0) then Break; //�������� ���� ������ �� �������� ��� ��������� - ������

        // ����� ������� �������
        if  ptCMDOC_TYPE.AsInteger=2 then //����� ��������� ��������������� ������ - ������ ������ � ����� �� ������ ���� ������� � ������� ��� ������ �������
        begin
          for n:=maxn downto 1 do //������� � �����
          begin
            if (ArPrih[n].QRemn>0) and (ArPrih[n].DDate<=ptCMMOVEDATE.AsDateTime) then
            begin
              ArPrih[n].QRemn:=ArPrih[n].QRemn+ptCMQUANT_MOVE.AsFloat; //ptCMQUANT_MOVE ������ - �������������
              if ArPrih[n].QRemn<=0 then ArPrih[n].iDayReal:=Trunc(ptCMMOVEDATE.AsDateTime)-Trunc(ArPrih[n].DDate);
              break; //��������� �� �����
            end;
          end;
        end;

        if ptCMDOC_TYPE.AsInteger=7 then //���� ������� ����������
        begin
          for n:=maxn downto 1 do //������� � �����
          begin
            if (ArPrih[n].QRemn>0) and (ArPrih[n].DDate<=ptCMMOVEDATE.AsDateTime) then
            begin
              ArPrih[n].QRemn:=ArPrih[n].QRemn+ptCMQUANT_MOVE.AsFloat; //ptCMQUANT_MOVE ������ - �������������
              ArPrih[n].QReal:=ArPrih[n].QReal+(-1)*ptCMQUANT_MOVE.AsFloat;
//              if ArPrih[n].QRemn<=0 then ArPrih[n].iDayReal:=Trunc(ptCMMOVEDATE.AsDateTime)-Trunc(ArPrih[n].DDate)+1 //���-�� ���� � ������� �� ������� �������
//              else
              ArPrih[n].iDayReal:=Trunc(ptCMMOVEDATE.AsDateTime)-Trunc(ArPrih[n].DDate)+1;
            end;
          end;
        end;

        ptCM.Next;
      end;

      //��� ��������� - �������� ��������

      k:=0; AvrReal:=0; kDov:=5;
      for n:=5 downto 1 do //������� � �����
      begin
        if ArPrih[n].iDayReal>0 then //���-�� ���� ���������� >0
        begin
//          ArPrih[n].QReal:=ArPrih[n].QReal/ArPrih[n].iDayReal; //������� ���������� � ����

          ArPrih[n].AvrR:=ArPrih[n].QReal/ArPrih[n].iDayReal;

          AvrReal:=AvrReal+ArPrih[n].AvrR*(10+(kDov-n)*CommonSet.SpeedRealKDov);
          k:=k+10+(kDov-n)*CommonSet.SpeedRealKDov;

          ArPrih[n].k:=10+(kDov-n)*CommonSet.SpeedRealKDov;

{          if n=5 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*10;
            k:=k+10;      //10
          end;
          if n=4 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*12;
            k:=k+10+2;               //12
          end;
          if n=3 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*14;
            k:=k+10+4;                //14
          end;
          if n=2 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*16;
            k:=k+10+6;                //16
          end;
          if n=1 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*18;
            k:=k+10+8;                //18
          end;
          }
        end;
      end;

      if k>0 then
      begin
        ArSpeed[nDep]:=AvrReal/k;
        if iTest>0 then
        begin
          if teAvSp.Active=False then CloseTe(teAvSp);

          for n:=1 to 5 do //
          begin
            teAvSp.Append;
            teAvSpiDep.AsInteger:=ptCGDEPART.AsInteger;
            teAvSpiPrih.AsInteger:=n;
            teAvSpiDateP.AsInteger:=Trunc(ArPrih[n].DDate);
            teAvSpQIn.AsFloat:=ArPrih[n].QIn;
            teAvSpQReal.AsFloat:=ArPrih[n].QReal;
            teAvSpQRemn.AsFloat:=ArPrih[n].QRemn;
            teAvSpiDateReal.AsInteger:=ArPrih[n].iDayReal;
            teAvSprAvrR.AsFloat:=ArPrih[n].AvrR;
            teAvSpkDov.AsInteger:=ArPrih[n].k;
            teAvSp.Post;
          end;

        end;
      end;

      inc(nDep);

      ptCG.next;
    end;

    rSpeedDay:=0;
    for n:=1 to 50 do rSpeedDay:=rSpeedDay+ArSpeed[n]; //��������� ������� �������� �� ���� �������

    rQday:=365;
    if rSpeedDay>0 then rQDay:=rQ/rSpeedDay;
    if rQDay>365 then rQDay:=365;

    Result:=rQ;
  end;
end;


function prFindTabSpeedRemn1(iC,InTime,nDay,DateB,DateE:Integer;Var rSpeedDay,rQday:Real):Real;  //nDay - ���������� ��������� ���� - ������� �� ������� ��� ��������� ����������
Type TPrih = record
     QIn,QReal,QRemn:Real;
     DDate:TDateTime;
     iDayReal:Integer;
     end;

Var rQ:Real;
    ArPrih:array[1..5] of TPrih;
    n,maxn,k,nDep:Integer;
    ArSpeed:Array[1..50] of Real; //������������ ����� ������� � �������
    maxDate,DDateB,DDateE:TDateTime;
    AvrReal:Real;
begin
  with dmMt do
  begin
    rQ:=0;
    rSpeedDay:=0;
    rQDay:=0;
    ndep:=1; //���������� ����� ������������
    DDateB:=DateB;
    DDateE:=DateE;

    for n:=1 to 50 do ArSpeed[n]:=0;

    //������ �������� ������ ��������� �������� �� ���� ������� � �����������

    if ptCG.Active=False then ptCG.Active:=True;
    ptCM.Active:=False; //�������� ����� ptCM1 , �� ����� master-slave
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);

    //�������� ��������� - ��������� �� ����� ������� � �������� ��������� ��� � ���� ��������� InTime ��������� ��������
    ptCG.First;
    while not ptCG.Eof do
    begin
      for n:=1 to 5 do //�������� ��� ������� �� ������
      begin
        ArPrih[n].QIn:=0; ArPrih[n].QReal :=0; ArPrih[n].QRemn:=0; ArPrih[n].DDate:=date; ArPrih[n].iDayReal:=0;
      end;

      n:=0;

      if ptCM1.Active=False then ptCM1.Active:=True;
      ptCM1.CancelRange;
      ptCM1.SetRange([ptCGCARD.AsInteger,DDateB],[ptCGCARD.AsInteger,DDateE]);

      ptCM1.Last;

      rQ:=rQ+ptCM1QUANT_REST.AsFloat; //����� ������� ������

      while (ptCM1.Bof=False) and (n<InTime) do
      begin
        if (ptCM1DOC_TYPE.AsInteger=1) and ((Date-ptCM1MOVEDATE.AsDateTime)>=nDay) then // �� ������� ������� �� 5� ��������� ���� �.�. ������ ���� �� ��� ����������, ����� ���� ���������� ������ ���� 5� ����.
        begin //��� ������ ������
          inc(n);
          ArPrih[n].QIn:=ptCM1QUANT_MOVE.AsFloat;
          ArPrih[n].DDate:=ptCM1MOVEDATE.AsDateTime;
          ArPrih[n].QRemn:=ptCM1QUANT_MOVE.AsFloat; //���������� ��������� ������� �������
        end;
        ptCM1.Prior;
      end;

      //������� �������� ����� ������� � ������������
      maxn:=n; // ����� ���� �� 5�
      MaxDate:=ArPrih[maxn].DDate; //������������ ���� � ������ �������� �������
     //��� ������������ �������� � 5� �������� ���������� ���� ���� ����� �� ������ ������  ���� ��������� ���������� ��� �������
      while (ptCM1MOVEDATE.AsDateTime>=MaxDate)and(ptCM1.Bof=False) do ptCM1.Prior;

      // ��� ������� ������

      while not ptCM1.Eof do //����� �������� ����� �� Break
      begin
        if (ArPrih[1].QRemn<=0)and(ArPrih[2].QRemn<=0)and(ArPrih[3].QRemn<=0)and(ArPrih[4].QRemn<=0)and(ArPrih[5].QRemn<=0) then Break; //�������� ���� ������ �� �������� ��� ��������� - ������

        // ����� ������� �������
        if  ptCM1DOC_TYPE.AsInteger=2 then //����� ��������� ��������������� ������ - ������ ������ � ����� �� ������ ���� ������� � ������� ��� ������ �������
        begin
          for n:=maxn downto 1 do //������� � �����
          begin
            if (ArPrih[n].QRemn>0) and (ArPrih[n].DDate<=ptCM1MOVEDATE.AsDateTime) then
            begin
              ArPrih[n].QRemn:=ArPrih[n].QRemn+ptCM1QUANT_MOVE.AsFloat; //ptCM1QUANT_MOVE ������ - �������������
              if ArPrih[n].QRemn<=0 then ArPrih[n].iDayReal:=Trunc(ptCM1MOVEDATE.AsDateTime)-Trunc(ArPrih[n].DDate);
              break; //��������� �� �����
            end;
          end;
        end;

        if ptCM1DOC_TYPE.AsInteger=7 then //���� ������� ����������
        begin
          for n:=maxn downto 1 do //������� � �����
          begin
            if (ArPrih[n].QRemn>0) and (ArPrih[n].DDate<=ptCM1MOVEDATE.AsDateTime) then
            begin
              ArPrih[n].QRemn:=ArPrih[n].QRemn+ptCM1QUANT_MOVE.AsFloat; //ptCM1QUANT_MOVE ������ - �������������
              ArPrih[n].QReal:=ArPrih[n].QReal+(-1)*ptCM1QUANT_MOVE.AsFloat;
              if ArPrih[n].QRemn<=0 then ArPrih[n].iDayReal:=Trunc(ptCM1MOVEDATE.AsDateTime)-Trunc(ArPrih[n].DDate); //���-�� ���� � ������� �� ������� �������
            end;
          end;
        end;

        ptCM1.Next;
      end;

      //��� ��������� - �������� ��������

      k:=0; AvrReal:=0;
      for n:=5 downto 1 do //������� � �����
      begin
        if ArPrih[n].iDayReal>0 then //���-�� ���� ���������� >0
        begin
          ArPrih[n].QReal:=ArPrih[n].QReal/ArPrih[n].iDayReal; //������� ���������� � ����
          if n=5 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*10;
            k:=k+10;
          end;
          if n=4 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*12;
            k:=k+12;
          end;
          if n=3 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*14;
            k:=k+14;
          end;
          if n=2 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*16;
            k:=k+16;
          end;
          if n=1 then
          begin
            AvrReal:=AvrReal+ArPrih[n].QReal*18;
            k:=k+18;
          end;
        end;
      end;

      if k>0 then ArSpeed[nDep]:=AvrReal/k;
      inc(nDep);

      ptCG.next;
    end;

    rSpeedDay:=0;
    for n:=1 to 50 do rSpeedDay:=rSpeedDay+ArSpeed[n]; //��������� ������� �������� �� ���� �������

    rQday:=365;
    if rSpeedDay>0 then rQDay:=rQ/rSpeedDay;
    if rQDay>365 then rQDay:=365;

    Result:=rQ;
  end;
end;

//��� ������� ������� �������
function prFindTabSpeedRemn2(iC,DateB,DateE:Integer;Var rSpeedDay,rQIn,rQReal:Real):Real;
Var rQ,rQ1:Real;
    iDayReal:Integer;
    DDateB,DDateE:TDateTime;
begin
  with dmMt do
  begin
    rQ:=0;
    rQ1:=0;
    rSpeedDay:=0;
    DDateB:=DateB;
    DDateE:=DateE;
    iDayReal:=DateE-DateB+1;

    if ptCG.Active=False then ptCG.Active:=True;
    ptCM.Active:=False; //�������� ����� ptCM1 , �� ����� master-slave
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);

    //�������� ��������� - ��������� �� ����� ������� � �������� ��������� ��� � ���� ��������� InTime ��������� ��������
    ptCG.First;
    while not ptCG.Eof do
    begin

      if ptCM1.Active=False then ptCM1.Active:=True;
      ptCM1.CancelRange;
      ptCM1.SetRange([ptCGCARD.AsInteger,DDateB],[ptCGCARD.AsInteger,DDateE]);

      ptCM1.First;

      while not ptCM1.Eof do //����� �������� ����� �� Break
      begin
        if ptCM1DOC_TYPE.AsInteger=7 then //���� ������� ����������
        begin
          rQ:=rQ+(-1)*ptCM1QUANT_MOVE.AsFloat;
        end;
        if ptCM1DOC_TYPE.AsInteger=1 then //���� ������� ����������
        begin
          rQ1:=rQ1+ptCM1QUANT_MOVE.AsFloat;
        end;

        ptCM1.Next;
      end;

      ptCG.next;
    end;

    rSpeedDay:=0;
    try
      rSpeedDay:=rQ/iDayReal;
    except
    end;

    rQReal:=rQ;
    rQIn:=rQ1;

    Result:=rQ;
  end;
end;

procedure TdmMT.ptCqRepCalcFields(DataSet: TDataSet);
begin
  //
  if taCards.Active=False then taCards.Active:=True;
  if taCards.FindKey([ptCqRepCode.AsInteger]) then ptCqRepName.AsString:=OemToAnsiConvert(taCardsName.AsString) else ptCqRepName.AsString:='';
end;

procedure TdmMT.DataModuleCreate(Sender: TObject);
begin
  ptCG.DatabaseName:=Btr1Path;
  ptCM.DatabaseName:=Btr1Path;
  ptCM1.DatabaseName:=Btr1Path;
  ptCM2.DatabaseName:=Btr1Path;
  taCards.DatabaseName:=Btr1Path;
  taACards.DatabaseName:=Btr1Path;
  ptBar.DatabaseName:=Btr1Path;
  ptZHead.DatabaseName:=Btr1Path;
  ptZSpec.DatabaseName:=Btr1Path;
  taAlgClass.DatabaseName:=Btr1Path;
  ptMakers.DatabaseName:=Btr1Path;
  ptCQ.DatabaseName:=Btr1Path;
  ptCqRep.DatabaseName:=Btr1Path;
  ptCn.DatabaseName:=Btr1Path;
  ptPool.DatabaseName:=Btr1Path;
  ptTTK.DatabaseName:=Btr1Path;
  ptCliGoods.DatabaseName:=Btr1Path;
  ptAkc.DatabaseName:=Btr1Path;
  ptScale.DatabaseName:=Btr1Path;
  ptDep.DatabaseName:=Btr1Path;
  ptMgr.DatabaseName:=Btr1Path;
  taG.DatabaseName:=Btr1Path;
  ptSGr.DatabaseName:=Btr1Path;
  ptMOL.DatabaseName:=Btr1Path;
  ptInventryHead.DatabaseName:=Btr1Path;
  ptInvLine1.DatabaseName:=Btr1Path;
  ptBufPrice.DatabaseName:=Btr1Path;
  ptCassir.DatabaseName:=Btr1Path;
  ptTTNIn.DatabaseName:=Btr1Path;
  ptZadan.DatabaseName:=Btr1Path;
  ptGdsCli.DatabaseName:=Btr1Path;
  ptKadri.DatabaseName:=Btr1Path;
  ptDStop.DatabaseName:=Btr1Path;
  ptCT.DatabaseName:=Btr1Path;
  ptCTM.DatabaseName:=Btr1Path;
  ptPluLim.DatabaseName:=Btr1Path;
  ptCateg.DatabaseName:=Btr1Path;
  ptInvLine.DatabaseName:=Btr1Path;
  ptCTM1.DatabaseName:=Btr1Path;
  ptZRep.DatabaseName:=Btr1Path;
  ptACValue.DatabaseName:=Btr1Path;
  ptTTnInLn.DatabaseName:=Btr1Path;
  ptVnLn.DatabaseName:=Btr1Path;
  ptCashs.DatabaseName:=Btr1Path;
  ptCashDCrd.DatabaseName:=Btr1Path;
  ptOutLn.DatabaseName:=Btr1Path;
  ptGdsLoad.DatabaseName:=Btr1Path;
  ptInLn.DatabaseName:=Btr1Path;
  ptGdsLoadSel.DatabaseName:=Btr1Path;
  ptGrafPost.DatabaseName:=Btr1Path;
  ptDExchS.DatabaseName:=Btr1Path;
  ptDExchH.DatabaseName:=Btr1Path;
  ptTO.DatabaseName:=Btr1Path;
  ptPartIN.DatabaseName:=Btr1Path;
  ptPartO.DatabaseName:=Btr1Path;
  ptCli.DatabaseName:=Btr1Path;
  ptIP.DatabaseName:=Btr1Path;
  ptCountry.DatabaseName:=Btr1Path;
  ptCountry1.DatabaseName:=Btr1Path;
  ptGTD.DatabaseName:=Btr1Path;
  ptCliLic.DatabaseName:=Btr1Path;
  ptKadr.DatabaseName:=Btr1Path;
  ptAlcoDHRec.DatabaseName:=Btr1Path;
  ptAlcoDS1.DatabaseName:=Btr1Path;
  ptAlcoDS2.DatabaseName:=Btr1Path;
  ptGroupCryst.DatabaseName:=Btr1Path;
  ptCashPath.DatabaseName:=Btr1Path;
  ptUPL.DatabaseName:=Btr1Path;
  ptPersonal.DatabaseName:=Btr1Path;
end;

procedure TdmMT.ptPluLimCalcFields(DataSet: TDataSet);
begin
  ptPluLimPercentDisc.AsFloat:=RoundEx(100-ptPluLimPercent.AsFloat);
end;

function InventryCheck(Date:tdatetime;depart:integer;Articul:integer):boolean;
begin
          {quA.Active:=false;
          quA.SQL.Clear;
          quA.SQL.Add('select * from "InventryHead"');
          quA.SQL.Add('where DateInventry>'''+formatDateTime('yyyy-mm-dd',quTTnInDateInvoice.AsDateTime)+'''');
          quA.SQL.Add('and Detail=0');
          quA.SQL.Add('and Depart='+quTTnInDepart.AsString);

          quA.SQL.Add('and Status=''�''');

          quA.Active:=true;
          if (quA.RecordCount>0) then
          begin
            ShowMessage('����� �� ��������, ��������� ������ ��������������');
            quA.Active:=false;
            exit;
          end;
          quA.Active:=false;

          quA.SQL.Clear;
          quA.SQL.Add('select * from "InventryHead"');
          quA.SQL.Add('where DateInventry>='''+formatDateTime('yyyy-mm-dd',quTTnInDateInvoice.AsDateTime)+'''');
          quA.SQL.Add('and Detail>0');
          quA.SQL.Add('and Depart='+quTTnInDepart.AsString);
          quA.SQL.Add('and Status=''�''');

          quA.Active:=true;
          CloseTe(dmMT.teInventryArt);
          dmMT.teInventryArt.Active:=true;

          while not quA.Eof do
          begin
            quB.Active:=False;
            quB.SQL.Clear;
            quB.SQL.Add('select Item from "InventryLine"');
            quB.SQL.Add('where Inventry ='+quA.FieldByName('Inventry').AsString);
            quB.SQL.Add('and Depart='+quTTnInDepart.AsString);

            quB.Active:=True;
            while not quB.Eof do
            begin
              dmMT.teInventryArt.Append;
              dmMT.teInventryArtArticul.AsInteger:=quB.fieldbyName('Item').AsInteger;
              dmMT.teInventryArt.Post;
              quB.Next
            end;
            quB.Active:=False;
            quA.Next;
          end;

          quA.Active:=false;

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;
          while not quSpecIn1.Eof do
          begin
            if dmMT.teInventryArt.Locate('Articul',quSpecIn1.fieldbyname('CodeTovar').AsInteger,[]) then
            begin
              showMessage('����� �� ��������, ��������� ��������� ��������������');
              exit;
            end;
            quSpecIn1.Next;
          end;

          quSpecIn1.Active:=False;
          dmMT.teInventryArt.Active:=false;}


  result:=True;
end;

procedure TdmMT.ptCashsCalcFields(DataSet: TDataSet);
Var Fl,iLoad,iTake:SmallInt;
begin
  Fl:=ptCashsFlags.AsInteger;
  iLoad:=Fl and $80;
  iTake:=Fl and $10;
  if iLoad<>0 then  ptCashsLoad.AsInteger:=0 else ptCashsLoad.AsInteger:=1; //���� �� ���� �� ������� ����
  if iTake<>0 then  ptCashsTake.AsInteger:=0 else ptCashsTake.AsInteger:=1;
end;


function prFindTRemnDepDInv(iC,iD,dD:Integer):Real;
Var rQ:Real;
    MoveD:TDateTime;
begin
  with dmMt do
  begin
    rQ:=0;
    MoveD:=dD;
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;
    ptCG.CancelRange;
    ptCG.SetRange([0,iC],[0,iC]);
    ptCG.First;
    while not ptCG.Eof do
    begin
      if iD>0 then //������� ����������� ������ �������
      begin
        if ptCGDEPART.AsInteger=iD then
        begin
          ptCM.Last;
          while not ptCM.Bof do
          begin
            if MoveD>=ptCMMOVEDATE.AsDateTime then
            begin
//              rQ:=rQ+ptCMQUANT_REST.AsFloat;
//              break;
              if (ptCMDOC_TYPE.AsInteger=20)and(ptCMMOVEDATE.AsDateTime=MoveD) then
              begin
                //��� �������������� � ���� ���� - ��������� �� ���� - ����� ������ �������
              end else
              begin
                rQ:=rQ+ptCMQUANT_REST.AsFloat;
                break;
              end;
            end;
            ptCM.Prior;
          end;
        end;
      end else
      begin //������� ������� �� ���� �������
        if iD=0 then //������� �� ���� �������� ������� 
        begin
          if ptDep.FindKey([ptCGDEPART.AsInteger]) then
          begin
            if ptDepStatus1.AsInteger=9 then  //������ �������� ������
            begin
              ptCM.Last;
              while not ptCM.Bof do
              begin
                if (MoveD>=ptCMMOVEDATE.AsDateTime) then
                begin
                  if (ptCMDOC_TYPE.AsInteger=20)and(ptCMMOVEDATE.AsDateTime=MoveD) then
                  begin
                    //��� �������������� � ���� ���� - ��������� �� ���� - ����� ������ �������
                  end else
                  begin
                    rQ:=rQ+ptCMQUANT_REST.AsFloat;
                    break;
                  end;  
                end;
                ptCM.Prior;
              end;
            end;
          end;
        end else // ������ �� ����
        begin
          ptCM.Last;
          while not ptCM.Bof do
          begin
            if MoveD>=ptCMMOVEDATE.AsDateTime then
            begin
              rQ:=rQ+ptCMQUANT_REST.AsFloat;
              break;
            end;
            ptCM.Prior;
          end;
        end;
      end;
      ptCG.Next;
    end;
    Result:=rQ;
  end;
end;


procedure TdmMT.teAvSpCalcFields(DataSet: TDataSet);
begin
  teAvSpdDateP.AsString:=ds1(teAvSpiDateP.AsInteger);
end;

end.
