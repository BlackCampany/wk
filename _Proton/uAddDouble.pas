unit uAddDouble;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ActnList, XPStyleActnCtrls,
  ActnMan, cxCheckBox, cxTextEdit, cxControls, cxContainer, cxEdit,
  cxLabel, StdCtrls, cxButtons, ExtCtrls;

type
  TfmAddDouble = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxCheckBox1: TcxCheckBox;
    amAddSingle: TActionManager;
    acExit: TAction;
    cxLabel3: TcxLabel;
    cxTextEdit2: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDouble: TfmAddDouble;

implementation

{$R *.dfm}

end.
