unit AlgClass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmAlgClass = class(TForm)
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    amAlgClass: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    ViAlgClass: TcxGridDBTableView;
    leAlgClass: TcxGridLevel;
    GrAlgClass: TcxGrid;
    ViAlgClassID: TcxGridDBColumn;
    ViAlgClassNAMECLA: TcxGridDBColumn;
    ViAlgClassGetAM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAlgClass: TfmAlgClass;

implementation

uses MT, Un1, AddAlgClass, u2fdk, MDB, ImpExcel;

{$R *.dfm}

procedure TfmAlgClass.FormCreate(Sender: TObject);
begin
  GrAlgClass.Align:=AlClient;
end;

procedure TfmAlgClass.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViAlgClass);
end;

procedure TfmAlgClass.SpeedItem3Click(Sender: TObject);
var ExcelApp, Workbook, ISheet: Variant;
    i:integer;
    iCode:integer;
begin
  if MessageDlg('������� ������������� �� ����� Excel? (��� ��������� ������� ����� �������))',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
  //  ShowMessage('��������� �� Excel');
    fmImpExcel:=tfmImpExcel.Create(Application);
    try
      fmImpExcel.ShowModal;
      if fmImpExcel.ModalResult=mrOk then
      begin
        if fmImpExcel.cxButtonEdit1.Text>'' then
        begin
          if fmImpExcel.cxSpinEdit1.Value>0 then
          begin
            // ������� �� Excel

            with dmMc do
            with dmMt do
            begin
              if FileExists(fmImpExcel.cxButtonEdit1.Text) then
              begin
                quAlgClass.Active:=False;

                taAlgClass.First;
                while not taAlgClass.Eof do taAlgClass.Delete;

                ExcelApp := CreateOleObject('Excel.Application');
                ExcelApp.Application.EnableEvents := false;
                Workbook := ExcelApp.WorkBooks.Add(fmImpExcel.cxButtonEdit1.Text);
                ISheet := Workbook.Worksheets.Item[1];

                i:=fmImpExcel.cxSpinEdit1.Value;

                ViAlgClass.BeginUpdate;
                While String(ISheet.Cells.Item[i, 1].Value)<>'' do
                begin
                  iCode:=StrToINtDef(String(ISheet.Cells.Item[i, 1].Value),0);
                  if iCode>0 then
                  begin
                    if taAlgClass.FindKey([iCode])=False then taAlgClass.Append else taAlgClass.Edit;
                    taAlgClassID.AsInteger:=iCode;
                    taAlgClassNAMECLA.AsString:=AnsiToOemConvert(String(ISheet.Cells.Item[i, 2].Value));
                    taAlgClass.Post;
                  end;
                  inc(i);
                end;

                quAlgClass.Active:=True;
                quAlgClass.First;

                ViAlgClass.EndUpdate;
                ExcelApp.Quit;
                ExcelApp:=Unassigned;
              end else ShowMessage('���� - '+fmImpExcel.cxButtonEdit1.Text+' �� ������.');
            end;
          end;
        end;
      end;
    finally
      fmImpExcel.Release;
    end;
  end;
end;

procedure TfmAlgClass.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAlgClass.acAddExecute(Sender: TObject);
begin
  try
    fmAddAlgClass:=tfmAddAlgClass.Create(Application);

    fmAddAlgClass.cxSpinEdit1.Value:=0;
    fmAddAlgClass.cxTextEdit1.Text:='';
    fmAddAlgClass.cxCheckBox1.EditValue:=1;

    fmAddAlgClass.ShowModal;
    if fmAddAlgClass.ModalResult=mrOk then
    begin
      try
        fmAlgClass.ViAlgClass.BeginUpdate;
        if fmAddAlgClass.cxSpinEdit1.Value>0 then
        begin
          with dmMt do
          with dmMc do
          begin
            if taAlgClass.Active then
            begin
              if taAlgClass.FindKey([fmAddAlgClass.cxSpinEdit1.Value]) then
              begin
                showmessage('������� � ����� ����� ��� ����. ���������� ����������.');
              end else
              begin
                taAlgClass.Append;
                taAlgClassID.AsInteger:=fmAddAlgClass.cxSpinEdit1.Value;
                taAlgClassNAMECLA.AsString:=AnsiToOemConvert(fmAddAlgClass.cxTextEdit1.Text);
                taAlgClassGetAM.AsInteger:=fmAddAlgClass.cxCheckBox1.EditValue;
                taAlgClass.Post;

                quAlgClass.Active:=False;
                quAlgClass.Active:=True;
                quAlgClass.Locate('ID',fmAddAlgClass.cxSpinEdit1.Value,[]);
              end;
            end;
          end;
        end else showmessage('������� �������� ���� �����������. ���������� ����������.');
      finally
        fmAlgClass.ViAlgClass.EndUpdate;
      end;
    end;

  finally
    fmAddAlgClass.Release;
  end;
end;

procedure TfmAlgClass.acEditExecute(Sender: TObject);
begin
  //�������������
  with dmMt do
  with dmMc do
  begin
    if quAlgClass.RecordCount>0 then
    begin
      try
        fmAddAlgClass:=tfmAddAlgClass.Create(Application);

        fmAddAlgClass.cxSpinEdit1.Value:=quAlgClassID.AsInteger;
        fmAddAlgClass.cxSpinEdit1.Properties.ReadOnly:=True;
        fmAddAlgClass.cxTextEdit1.Text:=quAlgClassNAMECLA.AsString;
        fmAddAlgClass.cxCheckBox1.EditValue:=quAlgClassGetAM.AsInteger;

        fmAddAlgClass.ShowModal;
        if fmAddAlgClass.ModalResult=mrOk then
        begin
          if taAlgClass.FindKey([fmAddAlgClass.cxSpinEdit1.Value]) then
          begin
            try
              fmAlgClass.ViAlgClass.BeginUpdate;

              taAlgClass.Edit;
              taAlgClassNAMECLA.AsString:=AnsiToOemConvert(fmAddAlgClass.cxTextEdit1.Text);
              taAlgClassGetAM.AsInteger:=fmAddAlgClass.cxCheckBox1.EditValue;
              taAlgClass.Post;

              quAlgClass.Active:=False;
              quAlgClass.Active:=True;
              quAlgClass.Locate('ID',fmAddAlgClass.cxSpinEdit1.Value,[]);
            finally
              fmAlgClass.ViAlgClass.EndUpdate;
            end;
          end;
        end;
      finally
        fmAddAlgClass.Release;
      end;
    end;
  end;
end;

procedure TfmAlgClass.acDelExecute(Sender: TObject);
begin
  with dmMt do
  with dmMc do
  begin
    if quAlgClass.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quAlgClassNAMECLA.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if taAlgClass.FindKey([quAlgClassID.AsInteger]) then
        begin
          try
            fmAlgClass.ViAlgClass.BeginUpdate;

            taAlgClass.Delete;

            quAlgClass.Active:=False;
            quAlgClass.Active:=True;

            quAlgClass.First;
          finally
            fmAlgClass.ViAlgClass.EndUpdate;
          end;
        end;
      end;
    end;
  end;
end;

end.
