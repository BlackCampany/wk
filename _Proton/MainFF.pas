unit MainFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxCurrencyEdit,
  cxCalc, cxImageComboBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, dxfBackGround, RXClock,
  Menus, cxLookAndFeelPainters, cxButtons, cxTextEdit, cxGridCardView,
  cxGridDBCardView, ActnList, XPStyleActnCtrls, ActnMan, cxContainer,
  cxGridDBTableView, DBClient;

type
  TfmMainFF = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GridSpec: TcxGrid;
    ViewSpec: TcxGridDBBandedTableView;
    ViewSpecCODE: TcxGridDBBandedColumn;
    ViewSpecNAME: TcxGridDBBandedColumn;
    ViewSpecPRICE: TcxGridDBBandedColumn;
    ViewSpecQUANTITY: TcxGridDBBandedColumn;
    ViewSpecSUMMA: TcxGridDBBandedColumn;
    ViewSpecDPROC: TcxGridDBBandedColumn;
    ViewSpecDSUM: TcxGridDBBandedColumn;
    ViewSpecISTATUS: TcxGridDBBandedColumn;
    LevelSpec: TcxGridLevel;
    dxfBackGround1: TdxfBackGround;
    RxClock1: TRxClock;
    Panel2: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GridM: TcxGrid;
    ViewM: TcxGridDBCardView;
    ViewMINFO: TcxGridDBCardViewRow;
    ViewMTREETYPE: TcxGridDBCardViewRow;
    ViewMSPRICE: TcxGridDBCardViewRow;
    LevelM: TcxGridLevel;
    amMenu: TActionManager;
    acUpUp: TAction;
    acUp: TAction;
    acExit: TAction;
    acOpenSelMenu: TAction;
    acSelOk: TAction;
    LevelHK: TcxGridLevel;
    GridHK: TcxGrid;
    ViewHK: TcxGridTableView;
    Button5: TcxButton;
    cEdit1: TcxCurrencyEdit;
    LevelModif: TcxGridLevel;
    ViewModif: TcxGridDBTableView;
    ViewModifID_TAB: TcxGridDBColumn;
    ViewModifID_POS: TcxGridDBColumn;
    ViewModifID: TcxGridDBColumn;
    ViewModifSIFR: TcxGridDBColumn;
    ViewModifNAME: TcxGridDBColumn;
    ViewModifQUANTITY: TcxGridDBColumn;
    acModify: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    Panel3: TPanel;
    Label1: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    acDiscount: TAction;
    Label7: TLabel;
    Panel4: TPanel;
    Label10: TLabel;
    Panel5: TPanel;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    acCashPCard: TAction;
    taServP: TClientDataSet;
    taServPName: TStringField;
    taServPCode: TStringField;
    taServPQuant: TFloatField;
    taServPStream: TIntegerField;
    taServPiType: TSmallintField;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOpenSelMenuExecute(Sender: TObject);
    procedure acUpUpExecute(Sender: TObject);
    procedure acUpExecute(Sender: TObject);
    procedure ViewMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acModifyExecute(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure ViewMDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewHKDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ViewHKDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewHKCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acDiscountExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure acCashPCardExecute(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure RecalcSum;
    Procedure CreateHotList;
    Procedure DestrViewPers;
    Procedure CreateViewPers;
    Function prSaveSpec:Integer;
  end;

Procedure WriteStatus;

var
  fmMainFF: TfmMainFF;
  TableImage:TBitmap;
  FGridBrush:TBrush;
  bDr:Boolean;


implementation

uses Dm, Un1, UnCash, PreFF, ModifFF, CashEnd, Calc, Attention,
  UnitBN, CredCards, PreFF1, fmDiscountShape, Discont, UnDP2300, BnSber;

{$R *.dfm}

Function TFmMainFF.prSaveSpec:Integer;
Var IdH,iNum:INteger;
    rSum:Real;
begin
  with dmC do
  begin
    IdH:=GetId('TH'); //���������� ����� ��������� ������ (���� ������)
    Result:=IdH;

    ViewSpec.BeginUpdate;

    rSum:=0;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      rSum:=rSum+quCurSpecSUMMA.AsFloat;
      quCurSpec.Next;
    end;


    taTabAll.Active:=False;
    taTabAll.ParamByName('TID').AsInteger:=IdH;
    taTabAll.Active:=True;

    trUpdTab.StartTransaction;
    taTabAll.Append;
    taTabAllID.AsInteger:=IdH;
    taTabAllID_PERSONAL.AsInteger:=Person.Id;
    taTabAllNUMTABLE.AsString:='FF';
    taTabAllQUESTS.AsInteger:=1;
    taTabAllTABSUM.AsFloat:=rSum;
    taTabAllBEGTIME.AsDateTime:=now;
    taTabAllENDTIME.AsDateTime:=now;
    taTabAllDISCONT.AsString:=DiscountBar;
    taTabAllDISCONT1.AsString:=PDiscountBar;

    Case Operation of
    0: begin
         if iCashType=0 then taTabAllOPERTYPE.AsString:='Sale'
         else taTabAllOPERTYPE.AsString:='SaleBank';
       end;
    1: begin
         if iCashType=0 then taTabAllOPERTYPE.AsString:='Ret'
         else taTabAllOPERTYPE.AsString:='RetBank';
       end;
    2: begin
         taTabAllOPERTYPE.AsString:='SalePC';
       end;
    end;

    if CommonSet.CashNum<0 then taTabAllCHECKNUM.AsInteger:=CommonSet.CashChNum
    else taTabAllCHECKNUM.AsInteger:=Nums.iCheckNum;

    taTabAllSKLAD.AsInteger:=0;
    taTabAllSTATION.AsInteger:=CommonSet.Station;

    taTabAll.Post;
    trUpdTab.Commit;

    taTabAll.Active:=False;

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
    taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
    taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    taCurMod.Active:=True;

    taSpecAll.Active:=False;
    taSpecAll.ParamByName('TID').AsInteger:=IdH;
    taSpecAll.Active:=True;

    trUpdTab.StartTransaction;

    iNum:=1;

    taServP.Active:=False; //�������� ��� ������ �����
    taServP.CreateDataSet;

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      //�������� ��� ������ �����
      if quCurSpecIStatus.AsInteger=0 then
      begin
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecSifr.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;
      end;

      taSpecAll.Append;
      taSpecAllID_TAB.AsInteger:=IdH;
      taSpecAllID.AsInteger:=iNum;
      taSpecAllID_PERSONAL.AsInteger:=Person.Id;
      taSpecAllNUMTABLE.AsString:='FF';
      taSpecAllSIFR.AsInteger:=quCurSpecSifr.AsInteger;
      taSpecAllPRICE.AsFloat:=quCurSpecPrice.AsFloat;

      Case Operation of
    0,2: begin
           taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat;
           taSpecAllSUMMA.AsFloat:=quCurSpecSumma.AsFloat;
         end;
      1: begin
           taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat*(-1);
           taSpecAllSUMMA.AsFloat:=quCurSpecSumma.AsFloat*(-1);
         end;
      end;


      taSpecAllDISCOUNTPROC.AsFloat:=quCurSpecDProc.AsFloat;
      taSpecAllDISCOUNTSUM.AsFloat:=quCurSpecDSum.AsFloat;
      taSpecAllISTATUS.AsInteger:=1;
      taSpecAllITYPE.AsInteger:=0;
      taSpecAll.Post;

      inc(iNum);

      taCurMod.First;
      while not taCurMod.Eof do
      begin
        if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
        begin //��� �������
          if quCurSpecIStatus.AsInteger=0 then
          begin
            taServP.Append;
            taServPName.AsString:=quCurModAllNAME.AsString;
            taServPCode.AsString:='';
            taServPQuant.AsFloat:=0;
            taServPStream.AsInteger:=quCurSpecStream.AsInteger;
            taServPiType.AsInteger:=1; //�����������
            taServP.Post;
          end;  

          taSpecAll.Append;
          taSpecAllID_TAB.AsInteger:=IdH;
          taSpecAllID.AsInteger:=iNum;
          taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
          taSpecAllNUMTABLE.AsString:='FF';
          taSpecAllSIFR.AsInteger:=taCurModSifr.AsInteger;
          taSpecAllPRICE.AsFloat:=0;

          Case Operation of
        0,2: begin
               taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat;
             end;
          1: begin
               taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat*(-1);
             end;
          end;

          taSpecAllDISCOUNTPROC.AsFloat:=0;
          taSpecAllDISCOUNTSUM.AsFloat:=0;
          taSpecAllSUMMA.AsFloat:=0;
          taSpecAllISTATUS.AsInteger:=1;
          taSpecAllITYPE.AsInteger:=1;//�����������
          taSpecAll.Post;
          inc(iNum);
        end;
        taCurMod.Next;
      end;

      quCurSpec.Next;
    end;

    trUpdTab.Commit;

    taCurMod.Active:=False;

    ViewSpec.EndUpdate;

    if Operation in [0,2] then PrintServCh('�����');// ���� ������� (1) - �� ������� ������ �� ����
    taServP.Active:=False;

  end;
end;

Procedure TFmMainFF.CreateViewPers;
begin
  GridSpec.Visible:=True;
  GridHK.Visible:=True;
  GridM.Visible:=True;
  cEdit1.Visible:=True;
  Panel2.Visible:=True;
  Panel5.Visible:=False; //������ �������
  Button5.Visible:=True;
  cxButton1.Visible:=True;
  cxButton2.Visible:=True;
  cxButton3.Visible:=True;
  cxButton4.Visible:=True;
  cxButton10.Visible:=True; //������ �/�
//  cxButton5.Visible:=True;  //���� �������� - ������� ��� ������� ������������ �������
  cxButton6.Visible:=True;
  cxButton11.Visible:=True;
  Label1.Caption:=Person.Name;
  Label7.Visible:=True;
  Button5.Enabled:=True;
  Button5.SetFocus;
end;

Procedure TFmMainFF.DestrViewPers;
begin
  GridSpec.Visible:=False;
  GridHK.Visible:=False;
  GridM.Visible:=False;
  cEdit1.Visible:=False;
  Panel2.Visible:=False;
  Panel5.Visible:=False; //������ �������
  Button5.Visible:=False;
  cxButton1.Visible:=False;
  cxButton2.Visible:=False;
  cxButton3.Visible:=False;
  cxButton4.Visible:=False;
  cxButton5.Visible:=False;
  cxButton6.Visible:=False;
  cxButton11.Visible:=False;
  cxButton10.Visible:=False; //������ �/�
  Label7.Visible:=False;
end;

Procedure TFmMainFF.CreateHotList;
Var AColumn:TcxGridColumn;
    i,j:INteger;
begin

  for I := 0 to 4 do
  begin
    AColumn := ViewHK.CreateColumn;
    AColumn.Caption := IntToStr(i);
    AColumn.Name := 'k'+IntToStr(i);
    AColumn.DataBinding.ValueType := 'String';
    AColumn.Styles.Content:=dmC.cxStyle24;
    AColumn.Width:=100;
  end;

  ViewHK.DataController.RecordCount := 5;

  for I := 0 to 4 do    //������
    for J := 0 to 4 do  //�������
//      ViewHK.DataController.SetValue(I, J, '�������� '+IntToStr(i)+' '+IntToStr(J)+#13+'0,00'+#13+'111222');

      ViewHK.DataController.SetValue(I, J, '');
  with dmC do
  begin
    quHotKey.Active:=False;
    quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    quHotKey.Active:=True;
    quHotKey.First;
    while not quHotKey.Eof do
    begin
      Str(quHotKeyPRICE.AsFloat:6:2,StrWk);
      StrWk:=Copy(quHotKeyNAME.AsString,1,15)+#13+Copy(quHotKeyNAME.AsString,16,15)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);
      ViewHK.DataController.SetValue(quHotKeyIROW.AsInteger,quHotKeyICOL.AsInteger,StrWk);

      quHotKey.Next;
    end;
    quHotKey.Active:=False;
  end;


{
procedure TForm1.Edit1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft)and(ssAlt in Shift) then
  begin
    bDr:=True;
    sDr:=Edit1.Text;
    (Sender as TEdit).BeginDrag(True);
  end;
end;

procedure TForm1.Edit2DragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  if bDr then
  begin
    Edit2.Text:=sDr;
    bDr:=False;
    sDr:='';
  end;
end;

procedure TForm1.Edit2DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then
  begin
    Accept:=True;
  end;
end;

procedure TForm1.Edit1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then Accept:=True;
end;

procedure TForm1.cxButton1Click(Sender: TObject);
Var AColumn:TcxGridColumn;
    i,j:INteger;
begin
  for I := 0 to 4 do
  begin
    AColumn := View1.CreateColumn;
    AColumn.Caption := IntToStr(i);
    AColumn.DataBinding.ValueType := 'String';
    AColumn.Width:=100;
  end;

  View1.DataController.RecordCount := 5;

  for I := 0 to 4 do
    for J := 0 to 4 do
      View1.DataController.SetValue(I, J, '�������� '+IntToStr(i)+' '+IntToStr(J)+#13+#13+'0,00'+#13+'111222');

end;

procedure TForm1.View1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then
  begin
    Accept:=True;
  end;
end;

procedure TForm1.View1DragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  if bDr then
  begin
//    Edit2.Text:=sDr;
    bDr:=False;
    sDr:='';
  end;
end;

procedure TForm1.View1CellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
begin
  StrWk:=IntToStr(ACellViewInfo.Bounds.TopLeft.X)+' '+IntToStr(ACellViewInfo.Bounds.TopLeft.Y);
  Label1.Caption:=StrWk;
  StrWk:=ACellViewInfo.Text;
  Label2.Caption:=StrWk;
  delete(StrWk,1,POS(#13,StrWk));
  delete(StrWk,1,POS(#13,StrWk));
  delete(StrWk,1,POS(#13,StrWk));
  Label3.Caption:=StrWk;
end;}
end;


Procedure TfmMainFF.RecalcSum;
Var rSum:Real;
    Str1,Str2,StrWk:String;
begin
  with dmC do
  begin
    //��������� ����� �����
{    ViewSpec.BeginUpdate;
    rSum:=0;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSUMMA.AsFloat;
      quCurSpec.Next;
    end;
    try
      quCurSpec.Prior;
      quCurSpec.Next;
    except
    end;
    ViewSpec.EndUpdate;
    cEdit1.EditValue:=rSum;}

    if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

    quCurSum.Active:=False;
    quCurSum.ParamByName('IDT').AsInteger:=Tab.Id;
    quCurSum.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurSum.Active:=True;

    rSum:=quCurSumRSUM.AsCurrency;
    cEdit1.EditValue:=rSum;

    //��
    if rSum<>0 then
    begin
      Str(quCurSpecSUMMA.AsFloat:7:2,Str1);
      StrWk:=Copy(quCurSpecNAME.AsString,1,12);
      While Length(StrWk)<12 do StrWk:=StrWk+' ';
      Str1:=StrWk+' '+Str1;
      Str(rSum:9:2,Str2); While Length(Str2)<15 do Str2:=' '+Str2;
      Str2:='�����'+Str2;
      WriteDP(Str1,Str2);
    end;

  end;
end;

Procedure WriteStatus;
begin
  with fmMainFF do
  begin

    label8.caption:='����� � '+IntToStr(CommonSet.CashNum);
    Label2.Caption:='����: '+Nums.sDate;
    Label4.Caption:='����� �'+IntToStr(Nums.ZNum)+'  ��� � '+IntToStr(Nums.iCheckNum);
    Label3.Caption:='������ ���: ������ ('+IntToStr(Nums.iRet)+')';
    Label9.Caption:='���.�����  '+Nums.SerNum;
    Label6.Caption:='�������� (����) '+IntToStr(Nums.ZYet);

    Case Operation of
    0,2: begin
         Label5.Caption:='�������� - ������� ��������.';
       end;
    1: begin
         Label5.Caption:='�������� - �������.';
       end;
    end;


    if Nums.iRet=0 then Label3.Caption:='������ ���: '+Nums.sRet;
    if (Nums.iRet=2)or(Nums.iRet=22) then Label3.Caption:='������ ���: ���������� ������� �����';


    if CommonSet.CashNum=0 then label8.caption:='������� ���������.'
    else
    begin
      if CommonSet.CashNum>0 then label8.caption:='����� � '+intToStr(CommonSet.CashNum);
      if CommonSet.CashNum<0 then
      begin
        label8.caption:='������� �������.';
        Label2.Caption:='����: '+FormatDateTime('dd.mm.yyyy',date);
        Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
      end;
    end;
  end;
end;


procedure TfmMainFF.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
//  NewWidth:=1024;
  NewHeight:=768;
end;

procedure TfmMainFF.FormCreate(Sender: TObject);
Var StrWk:String;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  Label8.Caption:='';
  Label9.Caption:='';
  Label2.Caption:='';
  Label3.Caption:='';
  Label4.Caption:='';
  Label7.Caption:=''; //������ ������

  Operation:=0; //�������� �������
  Label5.Caption:='�������� - ������� ��������.';

  ReadIni;

//��������� �������� Nums

  Nums.ZNum:=CommonSet.CashZ;
  Nums.SerNum:='0';
  Nums.RegNum:='0';
  Nums.CheckNum:=INtToStr(CommonSet.CashChNum);
  Nums.iCheckNum:=CommonSet.CashChNum;
  Nums.iRet:=0;
  Nums.sRet:='���';
  Nums.sDate:=FormatDateTime(sFormatDate,Date);

  bFF:=True; //��� ���� ���

  DestrViewPers;


  // ���������� �������

//  SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_HIDEWINDOW );
//  Left:=0; Top:=0;
//  Width:=1024;
//  Height:=768;
//  Windowstate:=wsMaximized;


  //�������� ����� ����� ��� ����������� �������
  if CommonSet.CashNum>0 then
  begin
    try
      if CommonSet.CashPort>0 then
      begin
        StrWk:='COM'+IntToStr(CommonSet.CashPort);
        if CashOpen(PChar(StrWk)) then   //dll �������
        begin
          if GetSerial then
          begin
            if Nums.SerNum<>Commonset.CashSer then
            begin
              CommonSet.CashNum:=0;
              CashClose;
              showmessage('������ ��������� ������ �����');
            end;
          end
          else
          begin
            CommonSet.CashNum:=0;
            CashClose;
            showmessage('������ ��������� ��������� ������ �����.');
          end;
        end
        else
        begin
          CommonSet.CashNum:=0;
          showmessage('������ �������� �����.');
        end;
      end
      else
      begin
        CommonSet.CashNum:=0;
      end;
    except
      CommonSet.CashNum:=0;
    end;
  end;

  if CommonSet.CashNum>0 then //��������� ���� ������ - �������� �����
  begin
//    Label9.Caption:='���.�����  '+Nums.SerNum;

    Nums.iRet:=InspectSt(Nums.sRet);
    if (Nums.iRet=0) or (Nums.iRet=2)or(Nums.iRet=22)or(Nums.iRet=21) then Nums.bOpen:=True;
    if Nums.iRet=21 then
    begin
      CheckCancel;
      Nums.iRet:=InspectSt(Nums.sRet);
    end;
    GetSerial;
    CashDate(Nums.sDate);
    GetNums; //������
    GetRes; //�������

  end;

  WriteStatus;

  StrPre:='';
  Person.Id:=0;
  Person.Name:='';

  TableImage:=TBitMap.Create;
  TableImage.LoadFromFile(CurDir+'Pict1.bmp');
  FGridBrush := TBrush.Create;

end;

procedure TfmMainFF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_SHOWWINDOW);

  if CommonSet.CashNum>0 then CashClose;

  TableImage.Free;
  FGridBrush.Free;
//  Panel3.Visible:=False;
//  dmC.quTabs.Active:=False;
  dmC.quPers.Active:=False;
  dmC.CasherRnDb.Connected:=False;
//  delay(500);
end;

procedure TfmMainFF.FormShow(Sender: TObject);
begin
//  Left:=0; Top:=0;
//  Width:=1024;
//  Height:=768;
//  Windowstate:=wsMaximized;

  Timer1.Enabled:=True;
  with dmC do
  begin
    try
      CasherRnDb.Connected:=False;
      CasherRnDb.DatabaseName:=DBName;
      CasherRnDb.Connected:=True;

      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=0; //������
      quMenu.Active:=True;

      Person.Id:=1001;
      Person.Name:='�������������';

      Tab.Id_Personal:=Person.Id;     //���� �������������� ��������
      Tab.Name:='';
      Tab.OpenTime:=Now;
      Tab.NumTable:='0';
      Tab.Quests:=1;
      Tab.iStatus:=0; //������� �� �����
      Tab.DBar:='';
      if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);
      Tab.Summa:=0;
      Tab.DPercent:=0;
      Check.Max:=0;
      
      Label1.Caption:=Person.Name;

      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;

      quCurSpecMod.Active:=False;
      quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpecMod.Active:=True;

      quCurSpec.Active:=False;
      quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpec.Active:=True;
      quCurSpec.Last;

      while not quCurSpec.Bof do
      begin
        ViewSpec.Controller.FocusedRow.Expand(True);
        quCurSpec.Prior;
        delay(10);
      end;

      CreateHotList;
      //��
      WriteDP(' ������� �� �������.','                    ');
    except
      ShowMessage('������ �������� ���� - '+DBName);
      Close;
    end;
  end;
end;

procedure TfmMainFF.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  Person.Id:=0;
  Person.Name:='';
  if CommonSet.MReader>0 then fmPreFF.ShowModal
  else fmPre.ShowModal;
end;

procedure TfmMainFF.ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
  sType:String;
begin
  if (AViewInfo is TcxGridCardRowDataViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;

  sType := VarAsType(AViewInfo.GridRecord.DisplayTexts[1], varString);
  if sType='T'  then
  begin
    ACanvas.Canvas.Brush.Color:=$00CDAD98;
  end
  else
  begin
//    ACanvas.Canvas.Brush.Bitmap := ABitmap;
  end;
  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmMainFF.acOpenSelMenuExecute(Sender: TObject);
Var iParent:Integer;
    rQ:Real;
begin
  with dmC do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=iParent; //������
      quMenu.Active:=True;
      quMenu.First;
//      delay(200);   //� ������� ��������� - ����� ���� First

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin
      quCurSpec.Last;
      if quCurSpecSifr.AsInteger<>quMenuSIFR.AsInteger then //����� �������
      begin
        rQ:=1;
        if quMenuDESIGNSIFR.AsInteger>0 then
        begin
          fmCalc.CalcEdit1.EditValue:=1;
          fmCalc.ShowModal;
          if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
          begin
            rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
          end;
        end;

        CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,rQ*quMenuPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

        if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

        quCurSpec.Append;
        quCurSpecSTATION.AsInteger:=CommonSet.Station;
        quCurSpecID_TAB.AsInteger:=Tab.Id;
        quCurSpecId_Personal.AsInteger:=Person.Id;
        quCurSpecNumTable.AsString:=IntToStr(Tab.Id);
        quCurSpecId.AsInteger:=Check.Max+1;
        quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
        quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
        quCurSpecQuantity.AsFloat:=rQ;
        quCurSpecSumma.AsFloat:=quMenuPRICE.AsFloat*rQ-Check.DSum;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=Check.DSum;
        quCurSpecIStatus.AsInteger:=0;
        quCurSpecName.AsString:=quMenuNAME.AsString;
        quCurSpecCode.AsString:=quMenuCODE.AsString;
        quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
        if quMenuLIMITPRICE.AsFloat>0 then
        quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
        else quCurSpecLimitM.AsInteger:=99;
        quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
        quCurSpec.Post;
        inc(Check.Max);

        if quMenuLINK.AsInteger>0 then
        begin //������������
          acModify.Execute;
        end;
      end else
      begin
        rQ:=quCurSpecQuantity.AsFloat;

        CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ+1,(rQ+1)*quMenuPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

        quCurSpec.Edit;
        quCurSpecQuantity.AsFloat:=rQ+1;
        quCurSpecSumma.AsFloat:=quMenuPRICE.AsFloat*(rQ+1)-Check.DSum;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=Check.DSum;
        quCurSpec.Post;
      end;
      RecalcSum;
    end;
  end;
end;

procedure TfmMainFF.acUpUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    quMenu.Active:=False;
    quMenu.ParamByName('Id_Parent').Value:=0; //������
    quMenu.Active:=True;
    quMenu.First;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;  

procedure TfmMainFF.acUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    taMenu.Active:=True;
    if taMenu.Locate('SIFR',quMenuPARENT.AsInteger,[]) and (quMenuPARENT.AsInteger<>0) then
    begin
      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=taMenuPARENT.AsInteger; //������
      quMenu.Active:=True;
      quMenu.First;
    end;
    taMenu.Active:=False;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMainFF.ViewMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  AHandled:=True;
  if bPrintCheck then exit;
  bAddPosFromMenu:=True; //��� ���� ����� ������ � ��������� ����� ������������
  bAddPosFromSpec:=False;
  acOpenSelMenu.Execute;
end;

procedure TfmMainFF.acModifyExecute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModifFF:=TFmModifFF.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=0;  //���� ��� ������� ��� ���������

    fmModifFF.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModifFF.ShowModal;
    fmModifFF.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmMainFF.Button5Click(Sender: TObject);
Var
    strWk,StrWk1,StrWk2,Str1,Str2:String;
    iRet,IdH,IdC,i:Integer;
    bCheckOk:Boolean;
    rDiscont:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    BnStr:String;
    bNotErr:Boolean;
    rSumWithDisc:Real;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;
 
begin
  Button5.Enabled:=False;
  if not CanDo('prPrintCheck') then begin   Button5.Enabled:=True; exit; end;
  with dmC do
  begin
    quCurSpec.First;
    if quCurSpec.Eof then begin Button5.Enabled:=True; exit; end;

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      fmAttention.ShowModal;
      Button5.Enabled:=True;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    TabCh.Id_Personal:=Person.Id;
    TabCh.Name:=Person.Name;
    TabCh.DBar:=Tab.DBar;
    TabCh.Summa:=0;

    ViewSpec.BeginUpdate;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      TabCh.Summa:=TabCh.Summa+quCurSpecSUMMA.AsFloat;
      quCurSpec.Next;
    end;
    quCurSpec.First;
    ViewSpec.EndUpdate;

    if TabCh.Summa=0 then begin Button5.Enabled:=True; exit; end; //���� ������ ��������

    with fmCash do
    begin
      Caption:='������';
      Label5.Caption:='���. '+TabCh.Name;
      Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      cEdit1.EditValue:=TabCh.Summa;
      cEdit2.EditValue:=TabCh.Summa;
      cEdit3.EditValue:=0;
      cEdit2.SelectAll;

      if CommonSet.CashNum>0 then
      begin
        iRet:=InspectSt(StrWk);
        if iRet<>0 then
        begin
          Label7.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          fmMainFF.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          while TestStatus('InspectSt',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;
        //���� ����� �� ���� ��� � �����

        Label7.Caption:='������ ���: ��� ��.';
        fmMainFF.Label3.Caption:='������ ���: ��� ��.';
      end;
    end;
    iCashType:=0; //��� �� ���������
    prClearAnsw;
    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      //����� ������������� ������
      fmMainFF.cEdit1.EditValue:=fmCash.CEdit3.EditValue; //����� � �������� ����

      //��
      Str(fmCash.CEdit1.EditValue:9:2,Str1); While Length(Str1)<15 do Str1:=' '+Str1;
      Str1:='�����'+Str1;
      Str(fmCash.CEdit3.EditValue:9:2,Str2); While Length(Str2)<15 do Str2:=' '+Str2;
      Str2:='�����'+Str2;
      WriteDP(Str1,Str2);


      bCheckOk:=False;
      bPrintCheck:=False;
      bPrintCheck:=True; //������� ������ ����
      delay(10);

      //-----------------------------

      //������� ���
      if CommonSet.CashNum>0 then  //���������� �����
      begin
        bCheckOk:=True;
        //������ ��� �������� ����� ����
 {       //����� ��������
        Nums.iRet:=InspectSt(Nums.sRet);
        while TestStatus('InspectSt',sMessage)=False do
        begin
          fmAttention.Label1.Caption:=sMessage;
          fmAttention.ShowModal;
          Nums.iRet:=InspectSt(Nums.sRet);
        end;
 }
       //�������� ������� �������� �����
        CashDriver;

        Case Operation of
        0: begin
             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
             CheckStart;
             while TestStatus('CheckStart',sMessage)=False do
             begin
               fmAttention.Label1.Caption:=sMessage;
               fmAttention.ShowModal;
               Nums.iRet:=InspectSt(Nums.sRet);
               prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
             end;
           end;
        1: begin
             prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
             CheckRetStart;
             while TestStatus('CheckRetStart',sMessage)=False do
             begin
               fmAttention.Label1.Caption:=sMessage;
               fmAttention.ShowModal;
               Nums.iRet:=InspectSt(Nums.sRet);
               prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
             end;
           end;
        2: begin
           end;
        end;

        //�������
        ViewSpec.BeginUpdate;

        taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
        taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
        taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
        taCurMod.Active:=True;

        rDiscont:=0;
        rSumWithDisc:=0;

        quCurSpec.First;
        while not quCurSpec.Eof do
        begin      //��������� �������
          PosCh.Name:=quCurSpecNAME.AsString;
          PosCh.Code:=quCurSpecSIFR.AsString;
          PosCh.AddName:='';
          PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
          PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

          rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
          rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;
          //������������

          taCurMod.First;
          while not taCurMod.Eof do
          begin
            if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
            begin //��� �������
              PosCh.AddName:=PosCh.AddName+'|   '+taCurModNAME.AsString;
            end;
            taCurMod.Next;
          end;
          if PosCh.AddName>'' then  delete(PosCh.AddName,1,1);//������ ������ '|

          prWriteLog('~!CheckAddPos;'+IntToStr((Nums.iCheckNum+1))+';'+PosCh.Code+';'+FloatToStr(quCurSpecQUANTITY.AsFloat)+';'+FloatToStr(quCurSpecPRICE.AsFloat)+';'+FloatToStr(quCurSpecDSUM.AsFloat)+';');

          if (PosCh.Price*PosCh.Count/1000)>=1 then CheckAddPos(iSumPos); //��������� ������� ���� ������ >1 ���
          while TestStatus('CheckAddPos',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;

          quCurSpec.Next;
        end;

        taCurMod.Active:=False;
        ViewSpec.EndUpdate;

        //������������ ���������� - ������ ����, ������, ������;
        rSumWithDisc:=rSumWithDisc/100000; //��� ���� �.�. ��������� ��� � integer

        CheckTotal(iSumTotal);
        prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+Inttostr(iSumTotal)+';'+FloatToStr(rSumWithDisc));

        while TestStatus('CheckTotal',sMessage)=False do
        begin
          fmAttention.Label1.Caption:=sMessage;
          fmAttention.ShowModal;
          Nums.iRet:=InspectSt(Nums.sRet);
          prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
        end;

        //�������� iSumTotal
        if (abs(rSumWithDisc-(iSumTotal/100))>0.01)and(bCheckOk=True) then
        begin //����������� �������� ����� �� ������������ � � ����������� - ����
          //��������� ��� ��������� -> �������� ������
          prClearCheck;
        end;

        if (rDiscont>0)and(bCheckOk=True)  then
        begin
          CheckDiscount(rDiscont,iSumDisc,iSumIt);
          iSumTotal:=iSumIt;
          prWriteLog('!!AfterCheckDisct;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
          while TestStatus('CheckDiscount',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;

        //��������� ������� � ������������ �������.
        BnStr:='';
        if (iCashType=1)and(bCheckOk=True) then
        begin
          if CommonSet.BNManual=0 then
          begin
            BnStr:='|';
            //���� � �����
            BnStr:=BnStr+Copy(Answ.sDate,1,2)+'/'+Copy(Answ.sDate,3,2)+'/'+Copy(Answ.sDate,5,2)+'  ';
            BnStr:=BnStr+Copy(Answ.sTime,1,2)+':'+Copy(Answ.sTime,3,2)+'||';
          //��������
            if Operation=0 then BnStr:=BnStr+'��������: �������||'
            else BnStr:=BnStr+'��������: �������||';
            //�����
            StrWk1:=Copy(bnBar,pos('=',bnBar)+1,4); //YYMM
            if Answ.bnBar[1]='4' then BnStr:=BnStr+'Visa';
            if Answ.bnBar[1]='5' then BnStr:=BnStr+'Master Card';
            if Answ.bnBar[1]='6' then BnStr:=BnStr+'Union';

            StrWk2:=Copy(Answ.bnBar,1,16); //�������� �����
            if length(StrWk2)>=12 then for i:=5 to 12 do StrWk2[i]:='X';

             BnStr:=BnStr+' '+StrWk2+' '+Copy(StrWk1,3,2)+'/'+Copy(StrWk1,1,2)+'||';
            //��� �����������
            BnStr:=BnStr+'��� ����������� '+Answ.sAuthCode+'||';
            //�����
            Str((iSumTotal/100):9:2,StrWk1);
            BnStr:=BnStr+'�����: '+StrWk1+' ���.||';
            BnStr:=BnStr+'������� _______________________________||';
          end;
        end;

        if bCheckOk=True then
        begin
          CheckRasch(iCashType,RoundEx(fmCash.cEdit2.EditValue*100),BnStr,iSumIt);
          prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(fmCash.cEdit2.EditValue)+';'+INtToStr(iSumIt)+';');
          while TestStatus('CheckRasch',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;

        if bCheckOk=True then
        begin
          prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');
          CheckClose;
        end;

        if (BnStr>'')and(bCheckOk=True) then //���� ��� �������� �� �������
        begin
          bNotErr:=True;
          try
            OpenNFDoc;
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr('����� ��������. ��� �'+IntToStr(Nums.iCheckNum+1));
            StrWk:=CommonSet.DepartName;
            while length(strwk)<35 do StrWk:=' '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk);
            if bNotErr then bNotErr:=PrintNFStr('      ');
            if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(Nums.ZNum)+'            ������: '+IntToStr(Person.Id));
            if bNotErr then bNotErr:=PrintNFStr('');

            i:=0;
            Delete(BnStr,1,1); //������� |
            while (length(BnBar)>0)and(bNotErr=True)and(i<7) do
            begin
              StrWk:=Copy(BnStr,1,Pos('||',BnStr)-1);
              if pos('�������',StrWk)>0 then if bNotErr then bNotErr:=PrintNFStr('');
              if bNotErr then bNotErr:=PrintNFStr(StrWk);
              Delete(BnStr,1,Pos('||',BnStr)+1);
              inc(i); //�� ������ ������, �������� ����� �� �����
              if pos('�������',StrWk)>0 then Break; //��� ���� �����, ���-�� � ������� �� ��.
            end;
//            if bNotErr then PrintNFStr(BnStr); //������ �������� ������
          finally
            CloseNFDoc;
            CutDoc1;
          end;
        end;

        //�������� ������ ����� ������
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          Label3.Caption:='������ ���: ��� ��.';
          if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
          if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
          //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
          WriteCheckNum;
        end
        else
        begin
          Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        end;

{
        while TestStatus('InspectSt',sMessage)=False do
        begin
          fmAttention.Label1.Caption:=sMessage;
          fmAttention.ShowModal;
          Nums.iRet:=InspectSt(Nums.sRet);
        end;
}
      end;

     //��� ���������� - ���� ��������� � ����

      if CommonSet.CashNum<0 then bCheckOk:=True;
      if bCheckOk then
      begin
        inc(CommonSet.CashChNum); //����� �������� 1-��
        WriteCheckNum;

        IdH:=prSaveSpec;  // ������������ ��������, ������ cashsail

        //���� ���
        quCashSail.Active:=False;
        idC:=GetId('CS');
        quCashSail.ParamByName('IDC').AsInteger:=IdC;
        quCashSail.Active:=True;

        quCashSail.Append;
        quCashSailID.AsInteger:=IdC;
        quCashSailCASHNUM.AsInteger:=CommonSet.CashNum;
        if CommonSet.CashNum<0 then
        begin
          quCashSailZNUM.AsInteger:=CommonSet.CashZ;
          quCashSailCHECKNUM.AsInteger:=CommonSet.CashChNum;
        end
        else
        begin
          quCashSailZNUM.AsInteger:=Nums.ZNum;
          quCashSailCHECKNUM.AsInteger:=Nums.iCheckNum;
        end;

        quCashSailTAB_ID.AsInteger:=IdH;

        Case Operation of
        0: quCashSailTABSUM.AsFloat:=TabCh.Summa;
        1: quCashSailTABSUM.AsFloat:=(-1)*TabCh.Summa;
        2: quCashSailTABSUM.AsFloat:=0; //������� �� ��������� �����, ����� �������, ��� �� ������ ������
        end;

        quCashSailCLIENTSUM.AsFloat:=fmCash.cEdit2.EditValue;
        quCashSailCHDATE.AsDateTime:=now;
        quCashSailCASHERID.AsInteger:=Person.Id;
        quCashSailWAITERID.AsInteger:=Person.Id;
        quCashSailPAYTYPE.AsInteger:=iCashType;

        quCashSail.Post;

        quCashSail.Active:=False;

      end;


     //-----------------------------

      //������ ������� ��� � ������������
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;

      //��������� �� ������
      quCurSpecMod.Active:=False;
      quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpecMod.Active:=True;

      quCurSpec.Active:=False;
      quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpec.Active:=True;
      quCurSpec.Last;

      //�������������� �� ���������
      ViewSpecDPROC.Visible:=False;
      ViewSpecDSum.Visible:=False;
      DiscountBar:='';
      Label7.Caption:='';
      Tab.DBar:='';
      Tab.DPercent:=0;
      Tab.DName:='';
      Operation:=0;

      WriteStatus;
      bPrintCheck:=False; //������� ������ ����
    end;

    //��
    WriteDP(' ������� �� �������.','                    ');

    Button5.Enabled:=True;
  end;
end;

procedure TfmMainFF.cxButton1Click(Sender: TObject);
begin
  with dmC do
  begin
    if not quCurSpec.Bof then quCurSpec.Prior;
  end;
end;

procedure TfmMainFF.cxButton2Click(Sender: TObject);
begin
  with dmC do
  begin
    if not quCurSpec.Eof then
    begin
      quCurSpec.Next;
      if quCurSpec.Eof then
      begin
        quCurSpec.Prior;
        quCurSpec.Next;
      end;
    end;
  end;
end;

procedure TfmMainFF.Panel3Click(Sender: TObject);
begin
{
  if bPrintCheck then
  begin
    bExitPers:=True;
    exit;
  end;
  DestrViewPers;
  fmPre.ShowModal;
}
  DestrViewPers;
  if CommonSet.MReader=1 then fmPreFF.ShowModal
  else fmPre.ShowModal;
end;

procedure TfmMainFF.RxClock1Click(Sender: TObject);
begin
  if CanDo('prExit') then
    if MessageDlg('�� ������������� ������ �������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FormLog('Close',IntToStr(Tab.Id_Personal));
      Close;
    end;
end;

procedure TfmMainFF.cxButton3Click(Sender: TObject);
Var Id:INteger;
begin
  Button5.Enabled:=False;
  cxButton11.Enabled:=False;
  cxButton3.Enabled:=False;
  with dmC do
  begin
    if quCurSpec.Eof then quCurSpec.First;
    if not quCurSpec.Eof then
    begin
      if not CanDo('prDelPosFF') then begin showmessage('��� ����.'); exit; end;
      if MessageDlg('�� ������������� ������ ������� "'+quCurSpecNAME.AsString+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        FormLog('DelPos',IntToStr(Person.Id));

        if quCurSpecIStatus.AsInteger=1 then
        begin
          taServP.Active:=False; //�������� ��� ������ �����
          taServP.CreateDataSet;

          taServP.Append;
          taServPName.AsString:=quCurSpecName.AsString;
          taServPCode.AsString:=quCurSpecSifr.AsString;
          taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
          taServPStream.AsInteger:=quCurSpecStream.AsInteger;
          taServPiType.AsInteger:=0; //�����
          taServP.Post;

          PrintServCh('������');
        end;

        Id:=quCurSpecID.AsInteger;
        ViewModif.BeginUpdate;
        quCurSpecMod.First;
        while not quCurSpecMod.Eof do
        begin
          if quCurSpecModID_POS.AsInteger=Id then  quCurSpecMod.Delete
          else quCurSpecMod.Next;
        end;
        ViewModif.EndUpdate;

        quCurSpec.Locate('ID',Id,[]);
        quCurSpec.Delete;
      end;
    end;
    RecalcSum;
  end;
  Button5.Enabled:=True;
  cxButton11.Enabled:=True;
  cxButton3.Enabled:=True;
end;

procedure TfmMainFF.cxButton4Click(Sender: TObject);
Var rQ:Real;
begin
  with dmC do
  begin
    bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
    bAddPosFromSpec:=True;

    if quCurSpec.Eof then exit;
    fmCalc.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      delay(10);
      rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;

      CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,rQ*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

      quCurSpec.Edit;
      quCurSpecQuantity.AsFloat:=rQ;
      quCurSpecDProc.AsFloat:=Check.DProc;
      quCurSpecDSum.AsFloat:=Check.DSum;
      quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*rQ*100)/100-Check.DSum;
      quCurSpec.Post;

      RecalcSum;
    end
  end;
end;

procedure TfmMainFF.N1Click(Sender: TObject);
Var I,J:Integer;
begin
  //�������� ������
  J:=StrToINtDef(ViewHK.Controller.FocusedColumn.Caption,-1);
  I:=ViewHK.Controller.FocusedRowIndex;
// ShowMessage(IntToStr(I)+' '+IntToStr(J));
  with dmC do
  begin
    try
      quHotKey.Active:=False;
      quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      quHotKey.Active:=True;
      quHotKey.First;
      while not quHotKey.Eof do
      begin
        if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
           (quHotKeyIROW.AsInteger=I)and
           (quHotKeyICOL.AsInteger=J) then quHotKey.Delete
        else quHotKey.Next;
      end;
      quHotKey.Active:=False;
      ViewHK.DataController.SetValue(I,J,'');
    except
    end;
  end;

end;

procedure TfmMainFF.ViewMDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
//
end;

procedure TfmMainFF.ViewHKDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
 Accept:=False;
  if bDr then
  begin
    Accept:=True;
  end;
end;

procedure TfmMainFF.ViewMMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
//  if Button = mbRight then
  if Button = mbRight then
  begin
    bDr:=True;
    GridM.BeginDrag(True,3);
  end;
end;

procedure TfmMainFF.ViewHKDragDrop(Sender, Source: TObject; X, Y: Integer);
Var StrWk:String;
    I,J:INteger;
begin
//���������
  with dmC do
  begin
    if quMenuTREETYPE.AsString='F' then
    begin
      I:=Y div 45;
      J:=X div 100;
//      StrWk:=INtToStr(quMenuSIFR.AsInteger)+' '+INtToStr(X)+' '+INtToStr(Y)+' '+INtToStr(I)+' '+INtToStr(J);
//      ShowMessage(StrWk);
      try
        quHotKey.Active:=False;
        quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        quHotKey.Active:=True;

        quHotKey.First;
        while not quHotKey.Eof do
        begin
          if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
             (quHotKeyIROW.AsInteger=I)and
             (quHotKeyICOL.AsInteger=J) then
          begin
            quHotKey.Delete; break;
          end else quHotKey.Next;
        end;
        quHotKey.Append;
        quHotKeyCASHNUM.AsInteger:=CommonSet.CashNum;
        quHotKeyIROW.AsInteger:=I;
        quHotKeyICOL.AsInteger:=J;
        quHotKeySIFR.AsInteger:=quMenuSIFR.AsInteger;
        quHotKey.Post;

        quHotKey.Active:=False;

        Str(quMenuPRICE.AsFloat:6:2,StrWk);
        StrWk:=Copy(quMenuNAME.AsString,1,15)+#13+Copy(quMenuNAME.AsString,16,15)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quMenuSIFR.AsInteger);
        ViewHK.DataController.SetValue(I,J,StrWk);
      except
      end;
    end;
  end;
end;

procedure TfmMainFF.ViewHKCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
    iCode:Integer;
    rQ:Real;
begin
//  StrWk:=IntToStr(ACellViewInfo.Bounds.TopLeft.X)+' '+IntToStr(ACellViewInfo.Bounds.TopLeft.Y);
//  Label1.Caption:=StrWk; //���������
  if bPrintCheck then exit;
  if AButton=mbRight then exit;


  StrWk:=ACellViewInfo.Text;
//  Label2.Caption:=StrWk; // ������ ������� � ����������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //����
//  Label3.Caption:=StrWk;  //��� ������   4-�� ������
//  ShowMessage(StrWk);
  iCode:=StrToIntDef(StrWk,0);
  if iCode>0 then
  begin
    //��� ������, ������� 
    with dmC do
    begin
      quMenuHK.Active:=False;
      quMenuHK.ParamByName('ISIFR').AsInteger:=iCode;
      qumenuHK.Active:=True;
      if quMenuHK.RecordCount>0 then
      begin
        quMenuHK.First;

        quCurSpec.Last;
        if quCurSpecSifr.AsInteger<>quMenuHKSIFR.AsInteger then
        begin
          rQ:=1;

          if quMenuHKDESIGNSIFR.AsInteger>0 then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
            begin
              rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
            end;
          end;

          CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ,rQ*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=INtToStr(Tab.Id);
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=quMenuHKSIFR.AsInteger;
          quCurSpecPrice.AsFloat:=quMenuHKPRICE.AsFloat;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=quMenuHKPRICE.AsFloat*rQ-Check.DSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuHKNAME.AsString;
          quCurSpecCode.AsString:=quMenuHKCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuHKLINK.AsInteger;
          if quMenuHKLIMITPRICE.AsFloat>0 then
          quCurSpecLimitM.AsInteger:=RoundEx(quMenuHKLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuHKSTREAM.AsInteger;
          quCurSpec.Post;
          inc(Check.Max);

          if quMenuHKLINK.AsInteger>0 then
          begin //������������
            acModify.Execute;
          end;
        end else
        begin
          rQ:=quCurSpecQuantity.AsFloat;

          CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ+1,(rQ+1)*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          quCurSpec.Edit;
          quCurSpecQuantity.AsFloat:=rQ+1;
          quCurSpecSumma.AsFloat:=quMenuHKPRICE.AsFloat*(rQ+1)-Check.DSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpec.Post;
        end;
        RecalcSum;
      end;
      quMenuHK.Active:=False;
    end;
  end;
end;

procedure TfmMainFF.acDiscountExecute(Sender: TObject);
Var StrWk:String;
begin
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount_Shape.cxTextEdit1.Text:='';
      fmDiscount_Shape.Caption:='��������� ���������� �����';
      fmDiscount_Shape.Label1.Caption:='��������� ���������� �����';
      fmDiscount_Shape.ShowModal;
      if fmDiscount_Shape.ModalResult=mrOk then
      begin
        //������� ������� ������
//      showmessage('');

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          WriteHistory('��:"'+DiscountBar+'"');

          If FindDiscount(DiscountBar) then
          begin
            // ��������� ������
            Str(Tab.DPercent:5:2,StrWk);
            Label7.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
            //������ ������ �� ��� �������
            ViewSpec.BeginUpdate;
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);
              if Check.DSum<>0 then
              begin
                quCurSpec.Edit;
                quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100-Check.DSum;
                quCurSpecDProc.AsFloat:=Check.DProc;
                quCurSpecDSum.AsFloat:=Check.DSum;
                quCurSpec.Post;

                ViewSpecDPROC.Visible:=True;
                ViewSpecDSum.Visible:=True;
              end;
              quCurSpec.Next;
            end;
            quCurSpec.First;
            RecalcSum;
            ViewSpec.EndUpdate;
          end;
        end else //������ �������� - ��� ������ ������
        begin
            //������ ������ �� ��� �������
          PDiscountBar:='';
          DiscountBar:='';
          Check.DProc:=0;
          Check.DSum:=0;
          Tab.DBar:='';
          Tab.DPercent:=0;
          Tab.DName:='';

          ViewSpec.BeginUpdate;
          quCurSpec.First;
          While not quCurSpec.Eof do
          begin
            quCurSpec.Edit;
            quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100;
            quCurSpecDProc.AsFloat:=0;
            quCurSpecDSum.AsFloat:=0;
            quCurSpec.Post;

            quCurSpec.Next;
          end;
          //�������

          Label7.Caption:='';
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;

          quCurSpec.First;
          RecalcSum;
          ViewSpec.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton5Click(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpecLINKM.AsInteger>0 then  acModify.Execute;
  end;
end;

procedure TfmMainFF.Panel4Click(Sender: TObject);
begin
  if Panel5.Visible then Panel5.Visible:=False
  else begin
    Panel5.Visible:=True;
    if CanDo('prRetCheck') then cxButton9.Visible:=True else cxButton9.Visible:=False;
    if CanDo('prCashRep') then
    begin
      cxButton7.Visible:=True;
      cxButton8.Visible:=True;
    end else
    begin
      cxButton7.Visible:=False;
      cxButton8.Visible:=False;
    end;
  end;
end;

procedure TfmMainFF.cxButton7Click(Sender: TObject);
Var StrWk:String;
    iRet:Integer;
begin
  if OpenZ then
  begin
    if GetX=False then Showmessage('������ ��� ��������� X-������.');
  end
  else  //�������� �����
  begin
    iRet:=InspectSt(StrWk);
    if iRet=0 then
    begin
      if ZOpen=False then Showmessage('������ ��� �������� �����.')
      else
      begin
        if GetX=False then Showmessage('������ ��� ��������� X-������.');
      end;
    end
    else
    begin
      Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
      Showmessage('��������� ������ ��� - '+''''+StrWk+''''+'  ������ ('+IntToStr(iRet)+')');
    end;
  end;
end;


procedure TfmMainFF.cxButton8Click(Sender: TObject);
Var iRet:INteger;
begin
  //�������� �����
  if CommonSet.CashNum>0 then
  begin
    if OpenZ then //��� �������� ����� � �������� ������� �� �����
    begin
      //�������� ��� �������
      ZClose;
      iRet:=InspectSt(StrWk);
      if iRet=0 then
      begin
        Label3.Caption:='������ ���: ��� ��.';
        if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
        inc(CommonSet.CashZ);
        if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
        WriteCheckNum;
      end
      else
      begin
        Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
      end;




      //�������� � ���������

      {iRet:=InspectSt(StrWk);
      if (iRet=0) or (iRet=22) or (iRet=2) or (iRet=24)then
      begin
        ZClose;
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          Label3.Caption:='������ ���: ��� ��.';
          if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
          inc(CommonSet.CashZ);
          if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
          WriteIni;
        end
        else
        begin
          Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
          WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
        end;
      end
      else
      begin
        Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
        Showmessage('��������� ������ ��� - '+''''+StrWk+''''+'  ������ ('+IntToStr(iRet)+')');
      end; }
    end
    else
    begin
      Showmessage('����� ��� �������.');
      WriteHistoryFR('����� ��� �������.');
    end;
  end;
  If CommonSet.CashNum<0 then
  begin
    inc(CommonSet.CashZ);
    CommonSet.CashChNum:=0;
    WriteCheckNum;
    Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
  end;
end;

procedure TfmMainFF.cxButton9Click(Sender: TObject);
begin
  if not CanDo('prRetCheck') then exit;

  if Operation =0 then Operation:=1
  else Operation := 0;

  Case Operation of
  0,2: begin
       Label5.Caption:='�������� - ������� ��������.';
     end;
  1: begin
       Label5.Caption:='�������� - �������.';
     end;
  end;
end;

procedure TfmMainFF.acCashPCardExecute(Sender: TObject);
//Var StrWk:String;
Var rSum:Real;
begin
  if not CanDo('prPrintBNCheck') then exit;
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount.cxTextEdit1.Text:='';
      fmDiscount.Caption:='��������� ��������� �����';
      fmDiscount.ShowModal;
      if fmDiscount.ModalResult=mrOk then
      begin
        //������� ��������� ������ - ��������� ����� - �.�. �� ������
//      showmessage('');

        if fmDiscount.cxTextEdit1.Text>'' then
        begin
          PDiscountBar:=fmDiscount.cxTextEdit1.Text;
          If FindPCard(PDiscountBar) then
          begin
            bPrintCheck:=True; //������� ������ ����

            quCurSpec.First;
            if quCurSpec.Eof then exit;

            rSum:=0;

            ViewSpec.BeginUpdate;
            quCurSpec.First;
            while not quCurSpec.Eof do
            begin
              rSum:=rSum+quCurSpecSUMMA.AsFloat;
              quCurSpec.Next;
            end;
            quCurSpec.First;
            ViewSpec.EndUpdate;

            inc(CommonSet.CashChNum); //����� �������� 1-�� � ����
            WriteCheckNum;

            Operation:=2; //SalePC  ������� �� ��������� �����
            prSaveSpec;   // ��������� ������������ � ������� ������ �����

            //������ ������� ��� � ������������
            prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
            prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
            prClearCur.ExecProc;

            //��������� �� ������
            quCurSpecMod.Active:=False;
            quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
            quCurSpecMod.Active:=True;

            quCurSpec.Active:=False;
            quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
            quCurSpec.Active:=True;
            quCurSpec.Last;

            //�������������� �� ���������
            ViewSpecDPROC.Visible:=False;
            ViewSpecDSum.Visible:=False;
            DiscountBar:='';
            PDiscountBar:='';
            Label7.Caption:='';
            Tab.DBar:='';
            Tab.DPercent:=0;
            Tab.DName:='';
            Operation:=0;
            cEdit1.Value:=0;

            WriteStatus;
            bPrintCheck:=False; //������� ������ ����
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton11Click(Sender: TObject);
begin
  cxButton11.Enabled:=False;
  cxButton3.Enabled:=False;
  Button5.Enabled:=False;
  ViewSpec.BeginUpdate;
  with dmC do
  begin
    taServP.Active:=False; //�������� ��� ������ �����
    taServP.CreateDataSet;

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
    taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
    taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    taCurMod.Active:=True;

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      //�������� ��� ������ �����
      if quCurSpecIStatus.AsInteger=0 then
      begin
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecSifr.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;

        taCurMod.First;
        while not taCurMod.Eof do
        begin
          if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
          begin //��� �������
            taServP.Append;
            taServPName.AsString:=quCurModAllNAME.AsString;
            taServPCode.AsString:='';
            taServPQuant.AsFloat:=0;
            taServPStream.AsInteger:=quCurSpecStream.AsInteger;
            taServPiType.AsInteger:=1; //�����������
            taServP.Post;
          end;
          taCurMod.Next;
        end;
      end;

      quCurSpec.Edit;
      quCurSpecIStatus.AsInteger:=1; //���� �� ���� �������� ��� ��� ����������.
      quCurSpec.Post;

      quCurSpec.Next;
    end;

    if Operation in [0,2] then PrintServCh('�����');// ���� ������� (1) - �� ������� ������ �� ����

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
  end;
  ViewSpec.EndUpdate;
  Button5.Enabled:=True;
  cxButton11.Enabled:=True;
  cxButton3.Enabled:=True;
end;

procedure TfmMainFF.cxButton12Click(Sender: TObject);
begin
  if not CanDo('prSberRep') then begin Showmessage('��� ����.'); exit; end;
  if CommonSet.BNManual=2 then
  begin //��� ����
    try
      fmSber:=tFmSber.Create(Application);
      iMoneyBn:=0; //� ��������
      fmSber.Label1.Visible:=False;
      fmSber.cxButton9.Enabled:=True;
      fmSber.cxButton10.Enabled:=True;
      fmSber.cxButton11.Enabled:=True;
      fmSber.cxButton6.Enabled:=True;

      fmSber.cxButton2.Enabled:=False;
      fmSber.cxButton3.Enabled:=False;
      fmSber.cxButton4.Enabled:=False;
      fmSber.cxButton5.Enabled:=False;
      fmSber.cxButton7.Enabled:=False;
      fmSber.ShowModal;
    finally
      fmSber.Release;
    end;
  end;
end;

end.
