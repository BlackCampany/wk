unit SelPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit;

type
  TfmSelPost = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label4: TLabel;
    Label2: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButtonEdit2: TcxButtonEdit;
    OpenDialog1: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPost: TfmSelPost;

implementation

uses Clients, MDB, Un1;

{$R *.dfm}

procedure TfmSelPost.FormCreate(Sender: TObject);
begin
  cxButtonEdit1.Text:='';
  cxButtonEdit1.Tag:=0;
  cxButtonEdit2.Text:='';
  cxButtonEdit2.Tag:=0;
end;

procedure TfmSelPost.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit2.Text:=OpenDialog1.fileName;
end;

procedure TfmSelPost.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S:String;
begin
  if fmclients.Showing then  fmclients.Close;
  iDirect:=1; //0 - ������ , 1- �������

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      Label4.Tag:=1;
      cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
      cxButtonEdit1.Text:=dmMC.quCliName.AsString;

    end else
    begin
      Label4.Tag:=2;
      cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
      cxButtonEdit1.Text:=dmMC.quIPName.AsString;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;

  iDirect:=0;
end;

end.
