unit AddDoc2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxSpinEdit,
  cxCheckBox, FR_DSet, FR_DBSet, FR_Class, dxmdaset;

type
  TfmAddDoc2 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpecOut: TClientDataSet;
    dstaSpecOut: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    amDocOut: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    taSpecOutCodeTovar: TIntegerField;
    taSpecOutCodeEdIzm: TSmallintField;
    taSpecOutBarCode: TStringField;
    taSpecOutNDSProc: TFloatField;
    taSpecOutNDSSum: TFloatField;
    taSpecOutOutNDSSum: TFloatField;
    taSpecOutKolMest: TIntegerField;
    taSpecOutKolEdMest: TFloatField;
    taSpecOutKolWithMest: TFloatField;
    taSpecOutKol: TFloatField;
    taSpecOutCenaTovar: TFloatField;
    taSpecOutSumCenaTovar: TFloatField;
    taSpecOutName: TStringField;
    taSpecOutNum: TIntegerField;
    taSpecOutCodeTara: TIntegerField;
    taSpecOutVesTara: TFloatField;
    taSpecOutCenaTara: TFloatField;
    taSpecOutSumVesTara: TFloatField;
    taSpecOutSumCenaTara: TFloatField;
    taSpecOutNameTara: TStringField;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    Label9: TLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    Label10: TLabel;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxComboBox1: TcxComboBox;
    Label13: TLabel;
    taSpecOutTovarType: TIntegerField;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc2: TcxGrid;
    ViewDoc2: TcxGridDBTableView;
    ViewDoc2Num: TcxGridDBColumn;
    ViewDoc2CodeTovar: TcxGridDBColumn;
    ViewDoc2Name: TcxGridDBColumn;
    ViewDoc2CodeEdIzm: TcxGridDBColumn;
    ViewDoc2BarCode: TcxGridDBColumn;
    ViewDoc2NDSProc: TcxGridDBColumn;
    ViewDoc2NDSSum: TcxGridDBColumn;
    ViewDoc2OutNDSSum: TcxGridDBColumn;
    ViewDoc2KolMest: TcxGridDBColumn;
    ViewDoc2KolWithMest: TcxGridDBColumn;
    ViewDoc2CenaTovar: TcxGridDBColumn;
    ViewDoc2CenaTovarSpis: TcxGridDBColumn;
    ViewDoc2SumCenaTovar: TcxGridDBColumn;
    ViewDoc2CodeTara: TcxGridDBColumn;
    ViewDoc2NameTara: TcxGridDBColumn;
    ViewDoc2CenaTara: TcxGridDBColumn;
    ViewDoc2SumCenaTara: TcxGridDBColumn;
    LevelDoc2: TcxGridLevel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label6: TLabel;
    taSpecOutCenaTovarSpis: TFloatField;
    taSpecOutCenaPost: TFloatField;
    taSpecOutSumCenaTovarSpis: TFloatField;
    taSpecOutCenaTaraSpis: TFloatField;
    taSpecOutSumCenaTaraSpis: TFloatField;
    ViewDoc2SumCenaTovarSpis: TcxGridDBColumn;
    ViewDoc2Kol: TcxGridDBColumn;
    acSetPrice: TAction;
    acZPrice: TAction;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxSpinEdit1: TcxSpinEdit;
    Label7: TLabel;
    frRepDOUT: TfrReport;
    frtaSpecOut: TfrDBDataSet;
    taSpecOutFullName: TStringField;
    ViewDoc2FullName: TcxGridDBColumn;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    acPrintCen: TAction;
    taSpecOutRemn: TFloatField;
    ViewDoc2Remn: TcxGridDBColumn;
    taSpPrint: TdxMemData;
    taSpPrintNum: TIntegerField;
    taSpPrintCodeTovar: TIntegerField;
    taSpPrintCodeEdIzm: TSmallintField;
    taSpPrintNDSProc: TFloatField;
    taSpPrintOutNDSSum: TFloatField;
    taSpPrintKolMest: TFloatField;
    taSpPrintKolWithMest: TFloatField;
    taSpPrintKol: TFloatField;
    taSpPrintCenaTovar: TFloatField;
    taSpPrintSumCenaTovar: TFloatField;
    taSpPrintFullName: TStringField;
    taSpPrintIGr: TSmallintField;
    frtaSpPrint: TfrDBDataSet;
    taSpPrintNDSSum: TFloatField;
    taSpPrintCenaMag: TFloatField;
    taSpPrintSumCenaMag: TFloatField;
    acPostTov: TAction;
    cxButton8: TcxButton;
    acSetRemn: TAction;
    cxLabel7: TcxLabel;
    cxLabel10: TcxLabel;
    acTermoP: TAction;
    cxComboBox2: TcxComboBox;
    Label8: TLabel;
    cxButton9: TcxButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    taSpecOutPostDate: TDateField;
    taSpecOutPostNum: TStringField;
    ViewDoc2PostDate: TcxGridDBColumn;
    ViewDoc2PostNum: TcxGridDBColumn;
    cxButton10: TcxButton;
    taSpPrintPostQuant: TFloatField;
    taSpPrintPostDate: TDateField;
    taSpPrintPostNum: TStringField;
    ViewDoc2PostQuant: TcxGridDBColumn;
    taSpecOutPostQuant: TFloatField;
    cxLabel8: TcxLabel;
    acZPricePoslPrih: TAction;
    acSetNac: TAction;
    cxLabel9: TcxLabel;
    Vi1: TcxGridDBTableView;
    Lev1: TcxGridLevel;
    Gr1: TcxGrid;
    Vi1Name: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1CodeEdIzm: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Lev2: TcxGridLevel;
    Vi2: TcxGridDBTableView;
    Vi2Name: TcxGridDBColumn;
    Vi2DateInvoice: TcxGridDBColumn;
    Vi2Number: TcxGridDBColumn;
    Vi2Kol: TcxGridDBColumn;
    Vi2CenaTovar: TcxGridDBColumn;
    Vi2CenaTovarSpis: TcxGridDBColumn;
    Vi2NDSProc: TcxGridDBColumn;
    Vi2NDSSum: TcxGridDBColumn;
    Vi2NameCli: TcxGridDBColumn;
    ViewDoc2VesTara: TcxGridDBColumn;
    taSpPrintVesTara: TFloatField;
    taSpecOutrNac: TFloatField;
    ViewDoc2rNac: TcxGridDBColumn;
    taSpecOutBrak: TFloatField;
    taSpecOutKolF: TFloatField;
    taSpecOutSumF: TFloatField;
    taSpecOutCheckCM: TSmallintField;
    ViewDoc2Brak: TcxGridDBColumn;
    ViewDoc2CheckCM: TcxGridDBColumn;
    ViewDoc2KolF: TcxGridDBColumn;
    ViewDoc2SumF: TcxGridDBColumn;
    cxButton11: TcxButton;
    taSpPrintBrak: TFloatField;
    taSpPrintKolF: TFloatField;
    taSpPrintSumF: TFloatField;
    taSpPrintCenaTovar0: TFloatField;
    taSpPrintSumCenaTovar0: TFloatField;
    taSpecOutKolSpis: TFloatField;
    ViewDoc2KolSpis: TcxGridDBColumn;
    cxCurrencyEdit6: TcxCurrencyEdit;
    Label11: TLabel;
    cxComboBox3: TcxComboBox;
    ViewDoc2CenaTaraSpis: TcxGridDBColumn;
    Vi1CenaTovar0: TcxGridDBColumn;
    taSpecOutCCode: TStringField;
    taSpecOutCountry: TStringField;
    taSpecOutGTD: TStringField;
    ViewDoc2GTD: TcxGridDBColumn;
    acPrintSCHF: TAction;
    cxLabel11: TcxLabel;
    acPredz: TAction;
    cxLabel12: TcxLabel;
    acPredz2: TAction;
    cxButton12: TcxButton;
    taSpPrintNac: TFloatField;
    cxButton13: TcxButton;
    cxLabel15: TcxLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    taSpPrintOutNDSPrice: TFloatField;
    taSpPrintLastTTNNum: TStringField;
    taSpPrintLastTTNDate: TStringField;
    ViewDoc2CCode: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure ViewDoc2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure taSpecOutKolMestChange(Sender: TField);
    procedure taSpecOutKolWithMestChange(Sender: TField);
    procedure taSpecOutCenaTovarChange(Sender: TField);
    procedure taSpecOutNDSProcChange(Sender: TField);
    procedure taSpecOutNDSSumChange(Sender: TField);
    procedure taSpecOutOutNDSSumChange(Sender: TField);
    procedure taSpecOutSumCenaTovarChange(Sender: TField);
    procedure taSpecOutCenaTaraChange(Sender: TField);
    procedure taSpecOutSumCenaTaraChange(Sender: TField);
    procedure ViewDoc2DblClick(Sender: TObject);
    procedure cxCurrencyEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxCurrencyEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure taSpecOutKolChange(Sender: TField);
    procedure taSpecOutCenaTovarSpisChange(Sender: TField);
    procedure taSpecOutSumCenaTovarSpisChange(Sender: TField);
    procedure acSetPriceExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure acZPriceExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acPostTovExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acSetRemnExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure acTermoPExecute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure acZPricePoslPrihExecute(Sender: TObject);
    procedure cxLabel8Click(Sender: TObject);
    procedure acSetNacExecute(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
    procedure ViewDoc2SelectionChanged(Sender: TcxCustomGridTableView);
    procedure Vi1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure Vi2CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure taSpecOutCalcFields(DataSet: TDataSet);
    procedure cxButton11Click(Sender: TObject);
    procedure taSpecOutKolFChange(Sender: TField);
    procedure taSpecOutBrakChange(Sender: TField);
    procedure taSpecOutCheckCMChange(Sender: TField);
    procedure acPrintSCHFExecute(Sender: TObject);
    procedure acPredzExecute(Sender: TObject);
    procedure cxLabel11Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1Click(Sender: TObject);
    procedure acPredz2Execute(Sender: TObject);
    procedure cxLabel12Click(Sender: TObject);
    procedure taSpecOutCodeTovarChange(Sender: TField);
    procedure cxTextEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButton12Click(Sender: TObject);
    procedure ViewDoc2NameTaraPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton13Click(Sender: TObject);
    procedure cxLabel15Click(Sender: TObject);
    procedure taSpecOutBeforePost(DataSet: TDataSet);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtons(bSt:Boolean);
  end;
  procedure prTaraPropButClick;
  procedure prPostPropButClick;
var
  fmAddDoc2: TfmAddDoc2;
  bAdd:Boolean = False;
  sPretCli,sDogNum,sDogDate:String;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, sumprops, Move, MT, QuantMess,
  u2fdk, MainMCryst, TBuff, dmPS, DocsOut, SetMatrix, dbe;

{$R *.dfm}
procedure prTaraPropButClick;
begin
  with fmAddDoc2 do
  with dmMC do
  begin
    iDirect:=3;
    PosP.Id:=0; PosP.Name:='';
    if quTara.Active=False then quTara.Active:=True;
    delay(10);
    fmTara.ShowModal;
    iDirect:=0;
    if PosP.Id>0 then
    begin
      taSpecOut.Edit;
      taSpecOutCodeTara.AsInteger:=PosP.Id;
      taSpecOutNameTara.AsString:=PosP.Name;
      taSpecOut.Post;
      ViewDoc2.Controller.SelectAllColumns;
      iCol:=1;
      ViewDoc2KolMest.FocusWithSelection;
      ViewDoc2KolMest.Options.Editing:=true;
      ViewDoc2.Controller.FocusRecord(ViewDoc2.DataController.FocusedRowIndex,True);
      GridDoc2.SetFocus; 
    end;
  end;
end;

procedure prPostPropButClick;
Var S:String;
begin
  with fmAddDoc2 do
  begin
    if fmclients.Showing then  fmclients.Close;
    iDirect:=1; //0 - ������ , 1- �������

    bOpen:=True;

    if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
    if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

    if Label4.Tag<=1 then //����������
    begin
      fmClients.LevelCli.Active:=True;
      fmClients.LevelIP.Active:=False;
    end;
    if Label4.Tag=2 then //���������������
    begin
      fmClients.LevelCli.Active:=False;
      fmClients.LevelIP.Active:=True;
    end;

    S:=cxButtonEdit1.Text;
    if S>'' then
    begin
      fmClients.cxTextEdit1.Text:=S;

      with dmMC do
      begin
        quFCli.Active:=False;
        quFCli.SQL.Clear;
        quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
        quFCli.SQL.Add('where Name like ''%'+S+'%''');
        quFCli.SQL.Add('and Name not like ''%��%''');
        quFCli.Active:=True;

        if quFCli.RecordCount>0 then
        begin
          quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
          fmClients.LevelCli.Active:=True;
          fmClients.LevelIP.Active:=False;
        end else
        begin
          quFIP.Active:=False;
          quFIP.SQL.Clear;
          quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
          quFIP.SQL.Add('where Name like ''%'+S+'%''');
          quFIP.SQL.Add('and Name not like ''%��%''');
          quFIP.Active:=True;
          if quFIP.RecordCount>0 then
          begin
            quIP.Locate('Code',quFIPCode.AsInteger,[]);
            fmClients.LevelCli.Active:=False;
            fmClients.LevelIP.Active:=True;
          end;
        end;
        quFCli.Active:=False;
        quFIP.Active:=False;
      end;
    end;
    
    fmClients.ShowModal;
    if fmClients.ModalResult=mrOk then
    begin
      if fmClients.GridCli.ActiveView=fmClients.ViewCli then
      begin
        Label4.Tag:=1;
        cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quCliName.AsString;

        prFParCli(dmMC.quCliINN.AsString,sPretCli,sDogNum,sDogDate);
        if Trim(sPretCli)>'' then
        begin
          cxButton14.Visible:=True;
          cxButton15.Visible:=True;
          cxButton16.Visible:=True;
        end else
        begin
          cxButton14.Visible:=False;
          cxButton15.Visible:=False;
          cxButton16.Visible:=False;
        end;
      end else
      begin
        Label4.Tag:=2;
        cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
        cxButtonEdit1.Text:=dmMC.quIPName.AsString;
        prFParCli(dmMC.quIPINN.AsString,sPretCli,sDogNum,sDogDate);
        if Trim(sPretCli)>'' then
        begin
          cxButton14.Visible:=True;
          cxButton15.Visible:=True;
          cxButton16.Visible:=True;
        end else
        begin
          cxButton14.Visible:=False;
          cxButton15.Visible:=False;
          cxButton16.Visible:=False;
        end;
      end;
      
      if (iTypeDO=99) then  acAddPos.Execute;

    end else
    begin
      cxButtonEdit1.Tag:=0;
      cxButtonEdit1.Text:='';
    end;

    bOpen:=False;
    iDirect:=0;
  end;  
end;

procedure TfmAddDoc2.prButtons(bSt:Boolean);
begin
//  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  cxButton3.Enabled:=bSt;
  cxButton4.Enabled:=bSt;
  cxButton5.Enabled:=bSt;
  cxButton6.Enabled:=bSt;
  cxButton7.Enabled:=bSt;
  cxButton10.Enabled:=bSt;

  with dmP do
  begin
    quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);

    if (quAllDepartStatus3.AsInteger=1)or(cxComboBox3.ItemIndex=1) then
    begin
      cxButton3.Enabled:=bSt;
      cxButton4.Enabled:=bSt;
      cxButton5.Enabled:=bSt;
    end else
    begin

      cxButton3.Enabled:=False;
//      cxButton4.Enabled:=False;
      cxButton5.Enabled:=False;
    end;
  end;

end;


procedure TfmAddDoc2.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc2.Align:=alClient;
  ViewDoc2.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  if CommonSet.RC=1 then
  begin
    cxLabel9.Visible:=True;
    ViewDoc2CenaTovar.Caption:='���� �����.';
    ViewDoc2CenaTovarSpis.Caption:='���� ��������';
    ViewDoc2SumCenaTovar.Caption:='����� �����.';
    ViewDoc2SumCenaTovarSpis.Caption:='����� ��������';
    ViewDoc2CenaTovar.Editing:=False;
    ViewDoc2SumCenaTovar.Editing:=False;
  end else
  begin
    ViewDoc2CenaTovar.Caption:='���� ���������� � ���';
    ViewDoc2CenaTovarSpis.Caption:='���� ��������';
    ViewDoc2SumCenaTovar.Caption:='����� ����������';
    ViewDoc2SumCenaTovarSpis.Caption:='����� ��������';
  end;

  cxComboBox3.Properties.Items.Clear;
  cxComboBox3.Properties.Items.Add('�������');
  cxComboBox3.Properties.Items.Add(CommonSet.NamePost);

  if CommonSet.RC=1 then cxButton12.Visible:=True else cxButton12.Visible:=False; 
end;

procedure TfmAddDoc2.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  prPostPropButClick;

end;

procedure TfmAddDoc2.cxLabel1Click(Sender: TObject);             //p
begin
  acAddList.Execute;
end;

procedure TfmAddDoc2.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=2 then  Accept:=True;
end;

procedure TfmAddDoc2.ViewDoc2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if iDirect=3 then
  begin
    iDirect:=0; //������ ���������
    iCol:=0;

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        iMax:=1;
        fmAddDoc2.taSpecOut.First;
        if not fmAddDoc2.taSpecOut.Eof then
        begin
          fmAddDoc2.taSpecOut.Last;
          iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
        end;

        with dmMC do
        begin
          ViewDoc2.BeginUpdate;
          try

          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            quFindC1.Active:=False;
            quFindC1.SQL.Clear;
            quFindC1.SQL.Add('SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
            quFindC1.SQL.Add('FROM "Goods"');
            quFindC1.SQL.Add('where "Goods"."ID"='+INtToStr(iNum));
            quFindC1.Active:=True;

            if quFindC1.RecordCount=1 then
            begin
              taSpecOut.Append;
              taSpecOutNum.AsInteger:=iMax;
              taSpecOutCodeTovar.AsInteger:=quFindC1ID.AsInteger;
              taSpecOutCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
              taSpecOutBarCode.AsString:=quFindC1BarCode.AsString;
              taSpecOutNDSProc.AsFloat:=quFindC1NDS.AsFloat;
              taSpecOutNDSSum.AsFloat:=0;
              taSpecOutOutNDSSum.AsFloat:=0;
              taSpecOutKolMest.AsInteger:=1;
              taSpecOutKolEdMest.AsFloat:=0;
              taSpecOutKolWithMest.AsFloat:=1;
              taSpecOutKol.AsFloat:=1;
              taSpecOutCenaTovar.AsFloat:=0;
              taSpecOutSumCenaTovar.AsFloat:=0;
              taSpecOutCenaTovarSpis.AsFloat:=quFindC1Cena.AsFloat;
              taSpecOutSumCenaTovarSpis.AsFloat:=quFindC1Cena.AsFloat;
              taSpecOutName.AsString:=quFindC1Name.AsString;
              taSpecOutFullName.AsString:=quFindC1FullName.AsString;
              taSpecOutCodeTara.AsInteger:=0;
              taSpecOutVesTara.AsFloat:=0;
              taSpecOutCenaTara.AsFloat:=0;
              taSpecOutCenaTaraSpis.AsFloat:=0;
              taSpecOutSumVesTara.AsFloat:=0;
              taSpecOutSumCenaTara.AsFloat:=0;
              taSpecOutSumCenaTaraSpis.AsFloat:=0;
              taSpecOutBrak.AsFloat:=0;
              taSpecOutCheckCM.AsInteger:=0;
              taSpecOutKolF.AsFloat:=1;
              taSpecOutKolSpis.AsFloat:=0;
              taSpecOut.Post;

            end;
          end;

          finally
            ViewDoc2.EndUpdate;
          end;  
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc2.ViewDoc2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2KolMest' then iCol:=1;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2KolWithMest' then iCol:=2;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Kol' then iCol:=3;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CenaTovar' then iCol:=4;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CenaTovarSpis' then iCol:=5;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumCenaTovar' then iCol:=6;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumCenaTovarSpis' then iCol:=7;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NDSProc' then iCol:=8;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NDSSum' then iCol:=9;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2OutNDSSum' then iCol:=10;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CenaTara' then iCol:=11;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SumCenaTara' then iCol:=12;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Brak' then iCol:=14;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2KolF' then iCol:=13;
end;

procedure TfmAddDoc2.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  bDrOut:=False;
  cxLabel11.Tag:=0;
  cxLabel12.Tag:=0;
  if fmadddoc2.cxButton1.Tag=0 then cxButton13.Enabled:=false
  else cxButton13.Enabled:=true;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc2.SetFocus;

  if cxComboBox1.ItemIndex<>4 then
  begin
    Lev1.Visible:=True;
    Lev2.Visible:=False;
  end else
  begin
    Lev1.Visible:=False;
    Lev2.Visible:=True;
  end;

//  ViewDoc2NameG.Options.Editing:=False;
//  ViewTaraNameG.Options.Editing:=False;
end;

procedure TfmAddDoc2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmMC do
  begin
    if cxButton1.Enabled then
    begin
      if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
      begin
        bDrOut:=False;
        quPost5.Active:=false;
        quPost7.Active:=False;
        ViewDoc2.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
      end else
      begin
        Action:= caNone;
      end;
    end else
    begin
      bDrOut:=False;
      quPost5.Active:=false;
      quPost7.Active:=False;
      ViewDoc2.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end;
  end;  
end;

procedure TfmAddDoc2.cxLabel5Click(Sender: TObject);   //p
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc2.ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode,iCodef:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
//    rK:Real;
//    iM:Integer;
//    Sm:String;
    bAdd:Boolean;
    sCCode,sCountry:String;
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
      if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTara') and (iTypeDO=99) {and (Key=$0D) }then
      begin
        try
          if (length(AEdit.EditingValue)>10) then
          begin
            AEdit.EditValue:=copy(AEdit.EditingValue,1,10);
            exit;
          end;
          iCode:=VarAsType(AEdit.EditingValue, varInteger);
        except
          iCode:=0;
        end;

        if iCode>0 then
        if fTara(iCode,sName) then
        begin
          taSpecOut.Edit;
          taSpecOutCodeTara.AsInteger:=iCode;
          taSpecOutNameTara.AsString:=sName;
          taSpecOut.Post;
        end;
      end;
      if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NameTara') and (iTypeDO=99) then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;

        iDirect:=3;
        PosP.Id:=0; PosP.Name:='';
        if quTara.Active=False then quTara.Active:=True;
        delay(10);
        if sName>'' then quTara.Locate('Name',sName,[loCaseInsensitive]);
        fmTara.ShowModal;
        iDirect:=0;
        if PosP.Id>0 then
        begin
          taSpecOut.Edit;
          taSpecOutCodeTara.AsInteger:=PosP.Id;
          taSpecOutNameTara.AsString:=PosP.Name;
          taSpecOut.Post;
        end;
      end;
      if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTovar') and (iTypeDO<>99) then
      begin
        try
          if (length(AEdit.EditingValue)>10) then
          begin
            AEdit.EditValue:=copy(AEdit.EditingValue,1,10);
            exit;
          end;
          iCodef:=taSpecOutCodeTovar.AsInteger;    //p
          iCode:=VarAsType(AEdit.EditingValue, varInteger);

        except
          iCodef:=0;
          iCode:=0;
        end;
        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCode));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          bAdd:=True;
          if quFindCStatus.AsInteger>100 then
          begin
            bAdd:=False;
            if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True
            else taSpecOutCodeTovar.AsInteger:=iCodef;         //p
          end;
          if bAdd then
          begin
              sCCode:='';
              sCountry:='';

              if iCode>0 then prFindTCountry(iCode,sCCode,sCountry);

              taSpecOut.Edit;

              taSpecOutCCode.AsString:=sCCode;
              taSpecOutCountry.AsString:=sCountry;

              taSpecOutCodeTovar.AsInteger:=iCode;
              taSpecOutCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
              taSpecOutBarCode.AsString:=quFindCBarCode.AsString;
              taSpecOutNDSSum.AsFloat:=0;
              taSpecOutOutNDSSum.AsFloat:=0;
              taSpecOutKolMest.AsInteger:=1;
              taSpecOutKolEdMest.AsFloat:=0;
              taSpecOutKolWithMest.AsFloat:=0;
              taSpecOutKol.AsFloat:=0;
              taSpecOutCenaTovar.AsFloat:=0;
              taSpecOutSumCenaTovar.AsFloat:=0;
              taSpecOutCenaTovarSpis.AsFloat:=quFindCCena.AsFloat;
              taSpecOutSumCenaTovarSpis.AsFloat:=0;
              taSpecOutName.AsString:=quFindCName.AsString;
              taSpecOutFullName.AsString:=quFindCFullName.AsString;
              taSpecOutCodeTara.AsInteger:=0;
              taSpecOutVesTara.AsFloat:=0;
              taSpecOutCenaTara.AsFloat:=0;
              taSpecOutSumVesTara.AsFloat:=0;
              taSpecOutSumCenaTara.AsFloat:=0;
              taSpecOutCenaTaraSpis.AsFloat:=0;
             // dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
             { if dmP.quAllDepartStatus3.AsInteger=1 then
                taSpecOutNDSProc.AsFloat:=quFindCNDS.AsFloat
              else
                taSpecOutNDSProc.AsFloat:=0; }
              if (iTypeDO<>99)  then
              begin
                dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
                if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quFindCNDS.AsFloat
                else taSpecOutNDSProc.AsFloat:=0;

                if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and  (cxComboBox1.ItemIndex<>100))  then
                begin
                  if (taSpecOutNDSProc.AsFloat>0) then
                  begin
                    taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
                    taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
                  end
                  else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

                end else
                begin
                  if (taSpecOutNDSProc.AsFloat>0) then
                  begin
                    taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
                    taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
                  end
                  else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

                end;
                if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= (taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat); //���� ����� ������ ��� ���
              end;


              taSpecOut.Post;
          end;
        end
        else
        begin
          showmessage('�������� ������ � ����� '+ its(iCode) +' ��� � ����������� ������');
          taSpecOut.Edit;
          taSpecOutCodeTovar.AsInteger:=iCode;
          taSpecOutCodeEdIzm.AsInteger:=0;
          taSpecOutBarCode.AsString:='';
          taSpecOutNDSProc.AsFloat:=0;
          taSpecOutNDSSum.AsFloat:=0;
          taSpecOutOutNDSSum.AsFloat:=0;
          taSpecOutKolMest.AsInteger:=0;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=0;
          taSpecOutKol.AsFloat:=0;
          taSpecOutCenaTovar.AsFloat:=0;
          taSpecOutSumCenaTovar.AsFloat:=0;
          taSpecOutCenaTovarSpis.AsFloat:=0;
          taSpecOutSumCenaTovarSpis.AsFloat:=0;
          taSpecOutName.AsString:='';
          taSpecOutFullName.AsString:='';
          taSpecOutCodeTara.AsInteger:=0;
          taSpecOutVesTara.AsFloat:=0;
          taSpecOutCenaTara.AsFloat:=0;
          taSpecOutSumVesTara.AsFloat:=0;
          taSpecOutSumCenaTara.AsFloat:=0;
          taSpecOutCenaTaraSpis.AsFloat:=0;

          taSpecOutCCode.AsString:='';
          taSpecOutCountry.AsString:='';

          taSpecOut.Post;
        end;
        GridDoc2.SetFocus;
        ViewDoc2KolWithMest.Focused:=True;
      end;
      if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Name') and (iTypeDO<>99) {and (Key=$0D) }then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if sName>'' then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if CountRec1(quFindC) then
            begin
              bAdd:=True;
              if quFindCStatus.AsInteger>100 then
              begin
                bAdd:=False;
                if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
              end;
              if bAdd then
              begin
                  sCCode:='';
                  sCountry:='';

                  if quFindCID.AsInteger>0 then prFindTCountry(quFindCID.AsInteger,sCCode,sCountry);

                  taSpecOut.Edit;

                  taSpecOutCCode.AsString:=sCCode;
                  taSpecOutCountry.AsString:=sCountry;

                  taSpecOutCodeTovar.AsInteger:=quFindCID.AsInteger;

                  taSpecOutCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
                  taSpecOutBarCode.AsString:=quFindCBarCode.AsString;
                  taSpecOutNDSProc.AsFloat:=quFindCNDS.AsFloat;
                  taSpecOutNDSSum.AsFloat:=0;
                  taSpecOutOutNDSSum.AsFloat:=0;
                  taSpecOutKolMest.AsInteger:=1;
                  taSpecOutKolEdMest.AsFloat:=0;
                  taSpecOutKolWithMest.AsFloat:=0;
                  taSpecOutKol.AsFloat:=1;
                  taSpecOutCenaTovar.AsFloat:=0;
                  taSpecOutCenaTovarSpis.AsFloat:=quFindCCena.AsFloat;
                  taSpecOutSumCenaTovarSpis.AsFloat:=0;
                  taSpecOutSumCenaTovar.AsFloat:=0;
                  taSpecOutName.AsString:=quFindCName.AsString;
                  taSpecOutFullName.AsString:=quFindCFullName.AsString;
                  taSpecOutCodeTara.AsInteger:=0;
                  taSpecOutVesTara.AsFloat:=0;
                  taSpecOutCenaTara.AsFloat:=0;
                  taSpecOutSumVesTara.AsFloat:=0;
                  taSpecOutSumCenaTara.AsFloat:=0;
                  taSpecOutCenaTaraSpis.AsFloat:=0;
                  taSpecOut.Post;
              end;
              GridDoc2.SetFocus;
              ViewDoc2KolWithMest.Focused:=True;
            end;
          end;
        end;
      end;
    end else
      if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTovar') or (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTara') then
        if fTestKey(Key)=False then
          if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Cancel;
  end;
end;

procedure TfmAddDoc2.ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var // Km:Real;
    sName:String;
    sCCode,sCountry:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
    exit;  //p
    if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Name') and (iTypeDO<>99) then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if sName>'' then
      begin
        quFindC1.Active:=False;
        quFindC1.SQL.Clear;
        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
        quFindC1.SQL.Add('FROM "Goods"');
        quFindC1.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
        quFindC1.Active:=True;

        if quFindC1.RecordCount=1 then //����� ���� ������
        begin
          sCCode:='';
          sCountry:='';

          if quFindC1ID.AsInteger>0 then prFindTCountry(quFindC1ID.AsInteger,sCCode,sCountry);

          taSpecOut.Edit;

          taSpecOutCCode.AsString:=sCCode;
          taSpecOutCountry.AsString:=sCountry;

          taSpecOutCodeTovar.AsInteger:=quFindC1ID.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
          taSpecOutBarCode.AsString:=quFindC1BarCode.AsString;
          taSpecOutNDSProc.AsFloat:=quFindC1NDS.AsFloat;
          taSpecOutNDSSum.AsFloat:=0;
          taSpecOutOutNDSSum.AsFloat:=0;
          taSpecOutKolMest.AsInteger:=1;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=0;
          taSpecOutKol.AsFloat:=0;
          taSpecOutCenaTovar.AsFloat:=0;
          taSpecOutSumCenaTovar.AsFloat:=0;
          taSpecOutCenaTovarSpis.AsFloat:=quFindC1Cena.AsFloat;
          taSpecOutSumCenaTovarSpis.AsFloat:=0;
          taSpecOutName.AsString:=quFindC1Name.AsString;
          taSpecOutFullName.AsString:=quFindC1FullName.AsString;
          taSpecOutCodeTara.AsInteger:=0;
          taSpecOutVesTara.AsFloat:=0;
          taSpecOutCenaTara.AsFloat:=0;
          taSpecOutCenaTaraSpis.AsFloat:=0;
          taSpecOutSumVesTara.AsFloat:=0;
          taSpecOutSumCenaTara.AsFloat:=0;
          taSpecOutSumCenaTaraSpis.AsFloat:=0;
          taSpecOut.Post;

          AEdit.SelectAll;
          ViewDoc2Name.Options.Editing:=False;
          ViewDoc2Name.Focused:=True;
          Key:=#0;
        end;//}
      end;
    end;
  end;//}
end;

procedure TfmAddDoc2.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  if (cxButtonEdit1.Text='') or (cxLookupComboBox1.Text='') or (cxComboBox1.Text='') then
  begin
    showmessage('������� ��������� ����������, ����� �������� � ���!');
    exit;
  end;

//  if iTypeDO=1 then exit;

  iMax:=1;
  ViewDoc2.BeginUpdate;

  taSpecOut.First;
  if not taSpecOut.Eof then
  begin
    taSpecOut.Last;
    iMax:=taSpecOutNum.AsInteger+1;
  end;

  iCol:=0;

  taSpecOut.Append;
  taSpecOutNum.AsInteger:=iMax;
  taSpecOutCodeTovar.AsInteger:=0;
  taSpecOutCodeEdIzm.AsInteger:=1;
  taSpecOutBarCode.AsString:='';
  dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
  if dmP.quAllDepartStatus3.AsInteger=1 then
  begin
    taSpecOutNDSProc.AsFloat:=10;
  end else
  begin
    taSpecOutNDSProc.AsFloat:=0;
  end;
  //taSpecOutNDSProc.AsFloat:=10;
  taSpecOutNDSSum.AsFloat:=0;
  taSpecOutOutNDSSum.AsFloat:=0;
  taSpecOutKolMest.AsInteger:=1;
  taSpecOutKolEdMest.AsFloat:=0;
  taSpecOutKolWithMest.AsFloat:=1;
  taSpecOutKol.AsFloat:=1;
  taSpecOutCenaTovar.AsFloat:=0;
  taSpecOutCenaTovarSpis.AsFloat:=0;
  taSpecOutSumCenaTovar.AsFloat:=0;
  taSpecOutSumCenaTovarSpis.AsFloat:=0;
  taSpecOutName.AsString:='';
  taSpecOutCodeTara.AsInteger:=0;
  taSpecOutNameTara.AsString:='';
  taSpecOutVesTara.AsFloat:=0;
  taSpecOutCenaTara.AsFloat:=0;
  taSpecOutCenaTaraSpis.AsFloat:=0;
  taSpecOutSumVesTara.AsFloat:=0;
  taSpecOutSumCenaTara.AsFloat:=0;
  taSpecOutSumCenaTaraSpis.AsFloat:=0;
  taSpecOutFullName.AsString:='';
  taSpecOutBrak.AsFloat:=0;
  taSpecOutCheckCM.AsInteger:=0;
  taSpecOutKolF.AsFloat:=1;
  taSpecOutKolSpis.AsFloat:=0;
  taSpecOut.Post;

  cxLabel5.Tag:=cxLookupComboBox1.EditValue;

  ViewDoc2.EndUpdate;
  GridDoc2.SetFocus;
  if iTypeDO<>99 then
  begin
    ViewDoc2Name.Focused:=True;
  end
  else
  begin
    if taSpecOutCodeTara.AsInteger=0 then
    begin
      delay(500);
      showmessage('�������� ����!');
      prTaraPropButClick;
    end;
  end;
    //ViewDoc2NameTara.Focused:=True;
end;

procedure TfmAddDoc2.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecOut.RecordCount>0 then
  begin
    taSpecOut.Delete;
  end;
  if taSpecOut.RecordCount=0 then
  begin
    cxLabel11.Tag:=0;
    cxLabel12.Tag:=0;
  end;
end;

procedure TfmAddDoc2.acAddListExecute(Sender: TObject);
begin
  if (cxButtonEdit1.Text='') or (cxLookupComboBox1.Text='') or (cxComboBox1.Text='') then
  begin
    showmessage('������� ��������� ����������, ����� �������� � ���!');
    exit;
  end;
//  if iTypeDO=1 then exit;
  iDirect:=3;
  fmCards.Show;
end;

procedure TfmAddDoc2.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSum10M,rSum20M,rSum0:Real;
    iGr,iSGr:Integer;
    iNum,iNumP,n:INteger;
    bErr:Boolean;
    StrWk:String;
    ChSet: set of Char;
    q:Real; //��� �������� �������� ����
    rSumSpis,rPrSpis:Real;
    iSS,i:Integer;
    IndP,CodP:INteger;
    PostDate:TDateTime;
    PostNum,sGTD:String;
    PostQuant,rPriceIn0:Real;
    user:String;          //p
    edit99:boolean;       //p
    sCCode,sCountry:String;
begin
  //��������
  with dmMC do
  with dmMt do
  begin
    user:=Person.Name;
    edit99:=false;
    if (iTypeDO=99) and ((user='OPER CB') or (user='BUH') or (user='�����') or (user='OPERZAK')) then edit99:=True
    else edit99:=False;

    if (cxDateEdit1.Date<=prOpenDate) and (edit99=False) then  begin ShowMessage('������ ������.');  exit;  end;
    if (fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False) and (edit99=False) then begin ShowMessage('������ ������ (��������������).'); exit; end;

    ChSet:=['0','1','2','3','4','5','6','7','8','9'];

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton8.Enabled:=False;
    cxbutton1.Tag:=0;
    IdH:=cxTextEdit1.Tag;
    if taSpecOut.RecordCount<1 then
    begin
      showmessage('��� ������� ��� ����������!');
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      exit;
    end;
    if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;
                            //p
    if (CommonSet.RetOn=0)and(CommonSet.Single=0) then
    begin
      if (cxDateEdit1.Date>Date) or (cxDateEdit2.Date>Date) then
      begin                                                       //������ �� �������� ��������� ������� ������
        showmessage('������ ��������� �������� ������� ������.�������� ����������!');
        cxDateEdit1.Date:=Date;
        cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
        exit;
      end;
    end;

    if cxButtonEdit1.Text='' then
    begin
      showmessage('�� ������ ���������');
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      exit;
    end;
    bErr:=False;
    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin

      if abs(taSpecOutSumCenaTara.AsFloat)<0.01 then    //���� �� ���� ����� ���� �� ��������� ����� ������
      begin
        if (taSpecOutCodeTovar.AsInteger<>56573) and (taSpecOutCodeTovar.AsInteger<>71801) then
        begin
          if abs(RoundVal(taSpecOutCenaTovar.AsFloat))<0.01 then   //���� � ����� ������ ����,�� ��������
          begin
            Showmessage('����� ������ ����� ���� ������ >0, �������� ��������� ������ ��� ���������� � ���� ��� ���������� ���������');
            cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
            ViewDoc2.EndUpdate;
            exit;
          end;

          dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);    //����� ��������� ������ ��� � ����� ���
          if dmP.quAllDepartStatus3.AsInteger=1 then
          begin
            if taSpecOutNDSProc.AsFloat>0 then
            begin
              if taSpecOutNDSSum.AsFloat<0.01 then
              begin
                Showmessage('����� ����������� - ���������� ���. � ���� ������� ����� ��� �� ������ ���� = 0');
                cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
                ViewDoc2.EndUpdate;
                exit;
              end;
            end
            else
            begin
              Showmessage('����� ����������� - ���������� ���. � ���� ������� ������ ��� �� ������ ���� = 0');
              cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
              ViewDoc2.EndUpdate;
              exit;
            end;
          end
          else
          begin
            if taSpecOutNDSProc.AsFloat<0.01 then
            begin
              if taSpecOutNDSSum.AsFloat>0 then
              begin
                Showmessage('����� ����������� - �� ���������� ���. � ���� ������� ����� ��� �� ������ ���� > 0');
                cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
                ViewDoc2.EndUpdate;
                exit;
              end;
            end
            else
            begin
              Showmessage('����� ����������� - �� ���������� ���. � ���� ������� ������ ��� �� ������ ���� > 0');
              cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
              ViewDoc2.EndUpdate;
              exit;
            end;
          end;
        end;

        if (abs(RoundVal(taSpecOutSumCenaTovarSpis.AsFloat))<0.01) and ((taSpecOutCodeTovar.AsInteger<>56573) and (taSpecOutCodeTovar.AsInteger<>71801)) then bErr:=True;
        if (abs(RoundVal(taSpecOutSumCenaTovar.AsFloat))<0.01) and (taSpecOutCodeTovar.AsInteger<>56573) and (taSpecOutCodeTovar.AsInteger<>71801)then bErr:=True;
        if (abs(taSpecOutKol.AsFloat)<0.01) and (taSpecOutCodeTovar.AsInteger<>56573) and (taSpecOutCodeTovar.AsInteger<>71801)then bErr:=True;
      end
      else
      begin
        if abs(taSpecOutKolMest.AsFloat)<0.01 then bErr:=True;    //���� ����� �� ���� ���� �� ��������� ���-�� ����<>0
      end;
      taSpecOut.Next;
    end;
    ViewDoc2.EndUpdate;
    if bErr and (cxComboBox1.ItemIndex=4) then
    begin
      if MessageDlg('���������� ������� � ������� ��� ��������� ����������� (���� ����������� ������� ����, �����). �������� ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
        exit;
      end;
    end;

    if bErr and (cxComboBox1.ItemIndex<>4) then
    begin
      Showmessage('���������� ������� � ������� ��� ��������� ����������� (���� ����������� ������� ����, �����).');
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      exit;
    end;

    //��������� �� ������� ������� ����������
    {
    if (iTypeDO=1) then      // ���� <2
    begin
      IndP:=Label4.Tag;
      CodP:=cxButtonEdit1.Tag;
      if (IndP>0)and(CodP>0) then
      begin
        ViewDoc2.BeginUpdate;
        taSpecOut.First;
        while not taSpecOut.Eof do
        begin
          if (taSpecOutCodeTovar.AsInteger>0) and (taSpecOutCodeTovar.AsInteger<>56573) and (taSpecOutCodeTovar.AsInteger<>71801)then     //P
          begin
            if prFLPT(taSpecOutCodeTovar.AsInteger,IndP,CodP,PostDate,PostNum,PostQuant,rPriceIn0,sGTD)<0.01 then
            begin
              Showmessage('��������!!! ����� - '+taSpecOutName.AsString+'('+taSpecOutCodeTovar.AsString+') - �� ������ ������ �� ���������� ���������� !!!');
            end;
          end;
          taSpecOut.Next;
        end;
        ViewDoc2.EndUpdate;
      end;
    end;
    }                                //p
    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select count(*) as RecCount from "TTNOut"');
      quC.SQL.Add('where Depart='+INtToStr(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInvoice='''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='''+Trim(cxTextEdit1.Text)+'''');
      quC.Active:=True;

      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        cxButton1.Enabled:=True; cxButton2.Enabled:=True;
        exit;
      end;

      //p ��������� � ������� ������� ��� �� �������� ����������� �����
      if (iTypeDO=1) then
      begin
        if dmP.PtHist.Active=false then dmP.PtHist.Active:=true;
        dmP.PtHist.CancelRange;                    // ��������� ������
        dmP.PtHist.SetRangeStart;
        dmP.PtHistTYPED.AsInteger :=2;
        dmP.PtHistDOCDATE.AsDateTime := cxDateEdit1.Date;
        dmP.PtHistDOCNUM.AsString :=cxTextEdit1.Text;
        dmP.PtHist.SetRangeEnd;
        dmP.PtHistTYPED.AsInteger :=2;
        dmP.PtHistDOCDATE.AsDateTime := cxDateEdit1.Date;
        dmP.PtHistDOCNUM.AsString :=cxTextEdit1.Text;
        dmP.PtHist.ApplyRange;
        i:=0;
        dmP.PtHist.First;
        while not dmP.PtHist.Eof do
        begin
          if dmP.PtHistIDOP.AsInteger>0 then i:=1;
          dmP.PtHist.Next;
        end;
        dmP.PtHist.Active:=false;

        if i>0 then showmessage('��� ������������� ����� ��������� �������� � ���������� �� �����!');

        //p
      end;

    end;
    try
      ViewDoc2.BeginUpdate;
      //������� ������ ����

      //������������ ��� ������� � �����������
      iCol:=0;

      rSum1:=0; rSum2:=0; rSum3:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum10M:=0; rSum20M:=0;  rSum0:=0;
      rSumSpis:=0;
      iSS:=fSS(cxLookupComboBox1.EditValue);

      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        iNum:=taSpecOutNum.AsInteger;

        sCCode:=Trim(taSpecOutCCode.AsString);
        sCountry:=taSpecOutCountry.AsString;

        if sCCode='' then prFindTCountry(quSpecOutCodeTovar.AsInteger,sCCode,sCountry);

        taSpecOut.Edit;

        taSpecOutCCode.AsString:=sCCode;
        taSpecOutCountry.AsString:=sCountry;

        taSpecOutCenaTovar.AsFloat:=RoundVal(taSpecOutCenaTovar.AsFloat);
        taSpecOutCenaTovarSpis.AsFloat:=RoundVal(taSpecOutCenaTovarSpis.AsFloat);
        taSpecOutCenaPost.AsFloat:=RoundVal(taSpecOutCenaPost.AsFloat);
        taSpecOutSumCenaTovarSpis.AsFloat:=RoundVal(taSpecOutSumCenaTovarSpis.AsFloat);
        taSpecOutSumCenaTovar.AsFloat:=RV(taSpecOutSumCenaTovar.AsFloat);
        taSpecOutOutNDSSum.AsFloat:=RV(taSpecOutOutNDSSum.AsFloat);
        taSpecOutNDSSum.AsFloat:=rv(taSpecOutNDSSum.AsFloat);
        taSpecOutSumCenaTaraSpis.AsFloat:=iNum; //����� �������� � ���� taSpecOutSumCenaTaraSpis, ��� �������������� ������ ��� �� �������

        if cxComboBox1.ItemIndex=3 then
        begin
          taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
          taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
        end;

        taSpecOut.Post;


        rSum1:=rSum1+taSpecOutSumCenaTovar.AsFloat; //� ����� ����������
        rSum2:=rSum2+taSpecOutSumCenaTovarSpis.AsFloat; //� ����� ��������
        rSum3:=rSum3+taSpecOutSumCenaTara.AsFloat;  //���� ����

        if (taSpecOutCodeTovar.AsInteger>0)and(taSpecOutBrak.AsFloat>0.01)and(taSpecOutCheckCM.AsInteger=1) then
        begin
          if iSS<2 then rPrSpis:=taSpecOutCenaTovar.AsFloat
          else rPrSpis:=rv(taSpecOutSumCenaTovar.AsFloat*100/(100+taSpecOutNDSProc.AsFloat))/taSpecOutKol.AsFloat;

          rSumSpis:=rSumSpis+rv(rPrSpis*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat));
        end;

        if taSpecOutNDSProc.AsFloat>=15 then
        begin
          rSum20:=rSum20+RoundVal(taSpecOutOutNDSSum.AsFloat);
          rN20:=rN20+RoundVal(taSpecOutNDSSum.AsFloat);
          rSum20M:=rSum20M+RoundVal(taSpecOutSumCenaTovarSpis.AsFloat);
        end else
        begin
          if taSpecOutNDSProc.AsFloat>=8 then
          begin
            rSum10:=rSum10+RoundVal(taSpecOutOutNDSSum.AsFloat);
            rN10:=rN10+RoundVal(taSpecOutNDSSum.AsFloat);
            rSum10M:=rSum10M+RoundVal(taSpecOutSumCenaTovarSpis.AsFloat);
          end else // ��� ���
          begin
            rSum0:=rSum0+taSpecOutSumCenaTovar.AsFloat;
          end;
        end;
        taSpecOut.Next; delay(10);
      end;

      cxCurrencyEdit1.EditValue:=rSum1+cxCurrencyEdit4.EditValue+cxCurrencyEdit5.EditValue+cxCurrencyEdit6.EditValue;

      if cxTextEdit1.Tag=0 then
      begin
        IDH:=prMax('DocsOut')+1;
//        cxTextEdit1.Tag:=IDH;  //� �����
//      if cxTextEdit1.Text=prGetNum(1,0) then prGetNum(1,1); //��������
      end else //��� �� ���������� , ����� ������� ��� ��� ���� ����� ������� ��� �� 100 ��� �����
      begin
//    ������ ������������
//    ������  ������ ����� ���� ���� 100-������ ��������
      {
        quD.SQL.Clear;
        quD.SQL.Add('delete from "TTNOutLn"');
        quD.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quD.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date-36500)+'''');
        quD.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quD.ExecSQL;

        // ��������� ������ ������������ �� 100 ��� ����� �� ������ ������

        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNOutLn" set');
        quE.SQL.Add('DateInvoice='''+ds(cxDateEdit10.Date-36500)+'''');
        quE.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quE.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
        quE.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quE.ExecSQL;
       }
//    ������  ������� ����� ���� ����

        quD.SQL.Clear;
        quD.SQL.Add('delete from "TTNOutLn"');
        quD.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quD.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
        quD.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quD.ExecSQL;

      end;

      //����� �������� ���������
      if cxTextEdit1.Tag=0 then //����������
      begin
        cxbutton1.Tag:=IDH;
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "TTNOut" values (');
        quA.SQL.Add(its(cxLookupComboBox1.EditValue)+',');  //Depart
        quA.SQL.Add(''''+ds(cxDateEdit1.Date)+''',');     //DateInvoice
        quA.SQL.Add(''''+cxTextEdit1.Text+''',');         //Number
        quA.SQL.Add(''''+cxTextEdit2.Text+''',');         //SCFNumber
        quA.SQL.Add(its(Label4.Tag)+',');                 //IndexPoluch
        quA.SQL.Add(its(cxButtonEdit1.Tag)+',');          //CodePoluch
        quA.SQL.Add(fs(rSum1)+',');                       //SummaTovar
        quA.SQL.Add(fs(rN10)+',');                        //NDS10
        quA.SQL.Add(fs(rSum10)+',');                      //OutNDS10
        quA.SQL.Add(fs(rN20)+',');                        //NDS20
        quA.SQL.Add(fs(rSum20)+',');                      //OutNDS20
        quA.SQL.Add('0,0,');                              //LgtNDS   OutLgtNDS
        quA.SQL.Add(fs(rN10+rN20)+',');                   //NDSTovar
        quA.SQL.Add(fs(rSum10+rSum20)+','+fs(rSum0)+',');  //OutNDSTovar  //NotNDSTovar
        quA.SQL.Add(fs(rSum1-rSum10-rSum20)+',');         //OutNDSSNTovar
        quA.SQL.Add(fs(rSum2)+',');                       //SummaTovarSpis

//��� �� ������, �� � �� ��������  - ��� �����    quA.SQL.Add(fs(rSum2-rSum1)+',');         //NacenkaTovar
//������ ��� �������������� ������
//        quA.SQL.Add(fs(rSum2-rSum10-rSum20)+',');         //NacenkaTovar

        quA.SQL.Add(fs(rSum2-rSum1)+',');         //NacenkaTovar

        quA.SQL.Add(fs(rSum3)+',');                       //SummaTara
        quA.SQL.Add(fs(rSum3)+',0,');                     //SummaTaraSpis  NacenkaTara
        quA.SQL.Add('0,'+Its(cxComboBox3.ItemIndex)+','''','''','''',');               // AcStatus ChekBuh NAvto NPList  FIOVod
        quA.SQL.Add(''''+ds(Date)+''','''',0,'''',');     //PrnDate PrnNumber PrnKol PrnAkt
        quA.SQL.Add(its(iTypeDO)+',0,0,');    //ProvodType  NotNDS10  NotNDS20
        quA.SQL.Add(fs(rSum10+rN10)+',');                  //WithNDS10
        quA.SQL.Add(fs(rSum20+rN20)+','+fs(rSumSpis)+',');                //WithNDS20  Crock
        quA.SQL.Add('0,0,0,');                             //Discount NDS010 NDS020
        quA.SQL.Add(fs(rSum10M)+',');                      //NDS_10
        quA.SQL.Add(fs(rSum20M)+',0,0,');                   //NDS_20 NSP_10 NSP_20
        quA.SQL.Add(''''+ds(cxDateEdit2.Date)+''',0,0,0,'); //SCFDate GoodsNSP0 GoodsCostNSP OrderNumber
        quA.SQL.Add(its(IDH)+',0,0,0,'''')');               //ID  LinkInvoice  StartTransfer EndTransfer REZERV
//        quA.SQL.Add(')');

        quA.ExecSQL;
        fmAddDoc2.cxButton13.Enabled:=true;
      end else //����������
      begin
        cxbutton1.Tag:=IDH;
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('Update "TTNOut" set');
        quA.SQL.Add('Depart='+its(cxLookupComboBox1.EditValue));
        quA.SQL.Add(',DateInvoice='''+ds(cxDateEdit1.Date)+'''');
        quA.SQL.Add(',Number='''+cxTextEdit1.Text+'''');
        quA.SQL.Add(',SCFNumber='''+cxTextEdit2.Text+'''');
        quA.SQL.Add(',IndexPoluch='+its(Label4.Tag));
        quA.SQL.Add(',CodePoluch='+its(cxButtonEdit1.Tag));
        quA.SQL.Add(',SummaTovar='+fs(rSum1));
        quA.SQL.Add(',NDS10='+fs(rN10));
        quA.SQL.Add(',OutNDS10='+fs(rSum10));
        quA.SQL.Add(',NDS20='+fs(rN20));
        quA.SQL.Add(',OutNDS20='+fs(rSum20));
        quA.SQL.Add(',LgtNDS=0');
        quA.SQL.Add(',OutLgtNDS=0');
        quA.SQL.Add(',NDSTovar='+fs(rN10+rN20));
        quA.SQL.Add(',OutNDSTovar='+fs(rSum10+rSum20));
        quA.SQL.Add(',NotNDSTovar='+fs(rSum0));
        quA.SQL.Add(',OutNDSSNTovar='+fs(rSum1-rSum10-rSum20));
        quA.SQL.Add(',SummaTovarSpis='+fs(rSum2));
        quA.SQL.Add(',NacenkaTovar='+fs(rSum2-rSum1));
        quA.SQL.Add(',SummaTara='+fs(rSum3));
        quA.SQL.Add(',SummaTaraSpis='+fs(rSum3));
        quA.SQL.Add(',NacenkaTara=0');
        quA.SQL.Add(',AcStatus=0');
        quA.SQL.Add(',ChekBuh='+Its(cxComboBox3.ItemIndex));
        quA.SQL.Add(',NAvto=''''');
        quA.SQL.Add(',NPList=''''');
        quA.SQL.Add(',FIOVod=''''');
        quA.SQL.Add(',PrnDate='''+ds(Date)+'''');
        quA.SQL.Add(',PrnNumber=''''');
        quA.SQL.Add(',PrnKol=0');
        quA.SQL.Add(',PrnAkt=''''');
        quA.SQL.Add(',ProvodType='+its(iTypeDO));             //��� ��������� (���������� ��� ������� ������ �������� � ����� ���������)
        quA.SQL.Add(',NotNDS10=0');
        quA.SQL.Add(',NotNDS20=0');
        quA.SQL.Add(',WithNDS10='+fs(rSum10+rN10));
        quA.SQL.Add(',WithNDS20='+fs(rSum20+rN20));
        quA.SQL.Add(',Crock='+fs(rSumSpis));
        quA.SQL.Add(',Discount=0');
        quA.SQL.Add(',NDS010=0');
        quA.SQL.Add(',NDS020=0');
        quA.SQL.Add(',NDS_10='+fs(rSum10M));
        quA.SQL.Add(',NDS_20='+fs(rSum20M));
        quA.SQL.Add(',NSP_10=0');
        quA.SQL.Add(',NSP_20=0');
        quA.SQL.Add(',SCFDate='''+ds(cxDateEdit2.Date)+'''');
        quA.SQL.Add(',GoodsNSP0=0');
        quA.SQL.Add(',GoodsCostNSP=0');
        quA.SQL.Add(',OrderNumber=0');
        quA.SQL.Add(',ID='+IntToStr(IDH));
//        quA.SQL.Add(',LinkInvoice=0');
        quA.SQL.Add(',StartTransfer=0');
        quA.SQL.Add(',EndTransfer=0');
        quA.SQL.Add(',REZERV=''''');
        quA.SQL.Add('where Depart='+its(cxDateEdit10.Tag));
        quA.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
        quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quA.ExecSQL;
        fmAddDoc2.cxButton13.Enabled:=true;
      end;
      //������� �� ������������ - ��� ������ ����������
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
//        prFindGr(taSpecOutCodeTovar.AsInteger,iGr,iSGr); //������ �� ������ ���� �������������� �����
        iGr:=0;
//        iSGr:=0;

        try
          if Trunc(taSpecOutPostDate.AsDateTime)=0 then
            begin
              taSpecOut.Edit;
              taSpecOutPostDate.AsDateTime:=StrToDate('01.01.2000');
              taSpecOut.Post;
            end
        except
          taSpecOut.Edit;
          taSpecOutPostDate.AsDateTime:=StrToDate('01.01.2000');
          taSpecOut.Post;
        end;

        try
          q:=taSpecOutPostQuant.AsFloat;
        except
          taSpecOut.Edit;
          taSpecOutPostQuant.AsFloat:=0;
          taSpecOut.Post;
        end;

        iSGr:=Trunc(taSpecOutPostDate.AsDateTime)-40000;
        StrWk:=taSpecOutPostNum.AsString;
        if StrWk='' then StrWk:='0';
        for n:=1 to Length(StrWk) do
        begin
          if  StrWk[n] in ChSet = False then StrWk[n]:=' ';
        end;
        while pos(' ',StrWk)>0 do delete(StrWk,pos(' ',StrWk),1);
        iNumP:=StrToINtDef(StrWk,0);

        if taSpecOutGTD.AsString>'' then
        begin
          if (trim(taSpecOutCCode.AsString)='643')
          or (trim(taSpecOutCCode.AsString)='398')
          or (trim(taSpecOutCCode.AsString)='112') then
          begin //�� ������ ���� ��� ��� ������ �����
            taSpecOut.Edit;
            taSpecOutGTD.AsString:='';
            taSpecOut.Post;
          end;
        end;

        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "TTNOutLn" values (');
        quA.SQL.Add(its(cxLookupComboBox1.EditValue)+','''+ds(cxDateEdit1.Date)+''','''+cxTextEdit1.Text+''',');
//      Depart DateInvoice   Number

        quA.SQL.Add(its(iGr)+','+its(iSGr)+','+its(taSpecOutCodeTovar.AsInteger)+','+its(taSpecOutCodeEdIzm.AsInteger)+',');
//     CodeGroup CodePodgr CodeTovar CodeEdIzm

        quA.SQL.Add(its(taSpecOutTovarType.AsInteger)+','''+taSpecOutBarCode.AsString+''','+fs(taSpecOutBrak.AsFloat)+',');
//      TovarType BarCode OKDP

        quA.SQL.Add(fs(taSpecOutNDSProc.AsFloat)+','+fs(taSpecOutNDSSum.AsFloat)+','+fs(taSpecOutOutNDSSum.AsFloat)+','+fs(taSpecOutPostQuant.AsFloat)+',');
//      NDSProc NDSSum OutNDSSum Akciz


        quA.SQL.Add(its(taSpecOutKolMest.AsInteger)+','+fs(0)+','+fs(taSpecOutKolWithMest.AsFloat)+',');
//      KolMest KolEdMest KolWithMest

        quA.SQL.Add(fs(taSpecOutKol.AsFloat)+','+its(iNumP)+','+fs(taSpecOutCenaTovar.AsFloat)+','+fs(taSpecOutCenaTovarSpis.AsFloat)+','+fs(taSpecOutSumCenaTovar.AsFloat)+',');
//      Kol  CenaPost  CenaTovar CenaTovarSpis SumCenaTovar

        quA.SQL.Add(fs(taSpecOutSumCenaTovarSpis.AsFloat)+','+its(taSpecOutCodeTara.AsInteger)+','+fs(taSpecOutVesTara.AsFloat)+','+fs(taSpecOutCenaTara.AsFloat)+','+fs(taSpecOutCenaTaraSpis.AsFloat)+',');
//      SumCenaTovarSpis CodeTara   VesTara  CenaTara  CenaTaraSpis

        quA.SQL.Add(fs(taSpecOutKolF.AsFloat)+','+fs(taSpecOutSumCenaTara.AsFloat)+','+fs(taSpecOutSumCenaTaraSpis.AsFloat)+','+its(taSpecOutCheckCM.AsInteger));
//          SumVesTara SumCenaTara SumCenaTaraSpis ChekBuh

        quA.SQL.Add(')');
        quA.ExecSQL;
{
Depart DateInvoice Number CodeGroup CodePodgr CodeTovar CodeEdIzm TovarType BarCode OKDP NDSProc
NDSSum OutNDSSum Akciz
KolMest KolEdMest KolWithMest
Kol  CenaPost  CenaTovar CenaTovarSpis SumCenaTovar
SumCenaTovarSpis CodeTara VesTara CenaTara CenaTaraSpis SumVesTara  SumCenaTara SumCenaTaraSpis ChekBuh
}

        taSpecOut.Next;
      end;

      //�������� ����� ���

      if (cxComboBox1.ItemIndex>2) and (iTypeDO<>99) then  //����� ����� ��������� � ���
      begin
        if ptGTD.Active=False then ptGTD.Active:=True else ptGTD.Refresh;

        ptGTD.CancelRange;
        ptGTD.SetRange([IDH],[IDH]);

        ptGTD.First; //������ ������
        while not ptGTD.Eof do ptGTD.Delete;

        taSpecOut.First;
        while not taSpecOut.Eof do
        begin
          if taSpecOutGTD.AsString>'' then
          begin
            if (trim(taSpecOutCCode.AsString)<>'643')
            and (trim(taSpecOutCCode.AsString)<>'398')
            and (trim(taSpecOutCCode.AsString)<>'112') then
            begin
              ptGTD.Append;
              ptGTDIDH.AsInteger:=IDH;
              ptGTDID.AsInteger:=RoundEx(taSpecOutSumCenaTaraSpis.AsFloat);
              ptGTDCODE.AsInteger:=taSpecOutCodeTovar.AsInteger;
              ptGTDGTD.AsString:=AnsiToOemConvert(taSpecOutGTD.AsString);
              ptGTD.Post;
            end else
            begin //�� ������ ���� ��� ��� ������ �����
              taSpecOut.Edit;
              taSpecOutGTD.AsString:='';
              taSpecOut.Post;
            end;
          end;

          taSpecOut.Next;
        end;
      end;

      cxTextEdit1.Tag:=IDH;
      cxTextEdit10.Tag:=IDH;
      cxTextEdit10.Text:=cxTextEdit1.Text;
      cxDateEdit10.Date:=cxDateEdit1.Date;
      cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;

    finally
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True; cxButton13.Enabled:=True;
      ViewDoc2.EndUpdate;
      fmDocs2.ViewDocsOut.BeginUpdate;
      quTTnOut.Active:=False;
      CommonSet.DateBeg:=Trunc(cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(cxDateEdit1.Date);
      quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);
      quTTnOut.Active:=True;
      quTTnOut.Locate('ID',IDH,[]);
      fmDocs2.ViewDocsOut.EndUpdate;
      fmDocs2.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmAddDoc2.acDelAllExecute(Sender: TObject);
begin
  if taSpecOut.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      delay(50);
      taSpecOut.First; while not taSpecOut.Eof do taSpecOut.Delete;
      cxLabel11.Tag:=0;
      cxLabel12.Tag:=0;
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc2.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc2.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc2.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc2.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc2.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrOut then  Accept:=True;
end;

procedure TfmAddDoc2.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc2.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc2.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmMC do
    begin
     { quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;}
    end;
  end;
end;

procedure TfmAddDoc2.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc2.acReadBar1Execute(Sender: TObject); //p
Var sBar:String;
    iC,iMax:INteger;
begin
//  if (iTypeDO=1) or (iTypeDO=99) then exit; //P
  if (cxButtonEdit1.Text='') or (cxLookupComboBox1.Text='') or (cxComboBox1.Text='') then
  begin
    showmessage('������� ��������� ����������, ����� �������� � ���!');
    exit;
  end;

  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if taSpecOut.RecordCount>0 then
    if taSpecOut.Locate('BarCode',sBar,[]) then
    begin
      ViewDoc2.Controller.FocusRecord(ViewDoc2.DataController.FocusedRowIndex,True);
      GridDoc2.SetFocus;
      exit;
    end;

  if prFindBarC(sBar,0,iC) then
  begin
    iMax:=1;
    if taSpecOut.RecordCount>0 then begin taSpecOut.Last; iMax:=taSpecOutNum.AsInteger+1; end;


    with dmMC do
    begin
      taSpecOut.Append;
      taSpecOutNum.AsInteger:=iMax;
      taSpecOutCodeTovar.AsInteger:=iC;
      taSpecOutCodeEdIzm.AsInteger:=quCIdEdIzm.AsInteger;
      taSpecOutBarCode.AsString:=sBar;
      //taSpecOutNDSProc.AsFloat:=quCIdNDS.AsFloat;
      taSpecOutNDSSum.AsFloat:=0;
      taSpecOutOutNDSSum.AsFloat:=0;
      taSpecOutKolMest.AsInteger:=1;
      taSpecOutKolEdMest.AsFloat:=1;
      taSpecOutKolWithMest.AsFloat:=1;
      taSpecOutKol.AsFloat:=0;
      taSpecOutCenaTovar.AsFloat:=0;
      taSpecOutCenaTovarSpis.AsFloat:=quCIdCena.AsFloat;
      taSpecOutSumCenaTovar.AsFloat:=0;
      taSpecOutSumCenaTovarSpis.AsFloat:=quCIdCena.AsFloat;
      taSpecOutName.AsString:=quCIdName.AsString;
      taSpecOutCodeTara.AsInteger:=0;
      taSpecOutVesTara.AsFloat:=0;
      taSpecOutCenaTara.AsFloat:=0;
      taSpecOutSumVesTara.AsFloat:=0;
      taSpecOutSumCenaTara.AsFloat:=0;

      taSpecOutCenaTaraSpis.AsFloat:=0;

      taSpecOutFullName.AsString:=quCIdFullName.AsString;

      taSpecOutBrak.AsFloat:=0;
//    taSpecOutSumF.AsFloat:=;
      taSpecOutCheckCM.AsInteger:=0;
      taSpecOutKolF.AsFloat:=0;
      taSpecOutKolSpis.AsFloat:=0;

      if (iTypeDO<>99)  then
      begin
        dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
        if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quCIdNDS.AsFloat
        else taSpecOutNDSProc.AsFloat:=0;

        if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
        begin
          if (taSpecOutNDSProc.AsFloat>0) then
          begin
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
            taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
          end
          else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

        end else
        begin
          if (taSpecOutNDSProc.AsFloat>0) then
          begin
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
            taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
          end
          else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

        end;
        if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= (taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat); //���� ����� ������ ��� ���
      end;    

      taSpecOut.Post;

      ViewDoc2.Controller.FocusRecord(ViewDoc2.DataController.FocusedRowIndex,True);
      GridDoc2.SetFocus;

    end;
  end else
    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc2.taSpecOutKolMestChange(Sender: TField);
Var rQ:Real;
begin
  //���-�� ����            1
  if iCol=1 then   //���-��
  begin

    rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;
    taSpecOutKol.AsFloat:=rQ;
    taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
    taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);
//    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>4)) then
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;                         

    if taSpecOutCheckCM.AsInteger=1 then
      taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
    else
      taSpecOutKolSpis.AsFloat:=0;

    if (iTypeDO=99) or (iTypeDO=0) then
    begin
      taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
      taSpecOutKolF.AsFloat:=rQ+rQ*taSpecOutBrak.AsFloat/100;
      ViewDoc2CenaTara.FocusWithSelection;
    end;

  end;
end;

procedure TfmAddDoc2.taSpecOutKolWithMestChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then //���-��
  begin
    rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;
    taSpecOutKol.AsFloat:=rQ;
    taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
    taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);

    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;

    taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
    taSpecOutKolF.AsFloat:=rQ+rQ*taSpecOutBrak.AsFloat/100;

    if taSpecOutCheckCM.AsInteger=1 then
      taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
    else
      taSpecOutKolSpis.AsFloat:=0;

  end;
end;

procedure TfmAddDoc2.taSpecOutCenaTovarChange(Sender: TField);
begin
  if iCol=4 then //
  begin
    taSpecOutSumCenaTovar.AsFloat:=rv(taSpecOutKol.AsFloat*taSpecOutCenaTovar.AsFloat);
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutNDSProcChange(Sender: TField);
begin
  if iCol=8 then   //������ ���
  begin
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutNDSSumChange(Sender: TField);
begin
  // ����� ���
  if iCol=9 then   //����� ���
  begin
    //������ �� ������ ��� ��������� ����� ����� ���
  end;
end;

procedure TfmAddDoc2.taSpecOutOutNDSSumChange(Sender: TField);
begin
  // ����� ���
  if iCol=10 then   //����� ��� ���
  begin
    //������ �� ������ ��� ��������� ����� ����� ���
  end;
end;

procedure TfmAddDoc2.taSpecOutSumCenaTovarChange(Sender: TField);
begin
  if iCol=6 then   //����� ���������� �����
  begin
    if taSpecOutKol.AsFloat<>0 then
    begin
      taSpecOutCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat/taSpecOutKol.AsFloat;

      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
        taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
      end else
      begin
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
        taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
      end;
    end else
    begin
      taSpecOutNDSSum.AsFloat:=0;
      taSpecOutOutNDSSum.AsFloat:=0;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutCenaTaraChange(Sender: TField);
begin
  // ���� ����
  if iCol=11 then
  begin
    taSpecOutCenaTaraSpis.AsFloat:=taSpecOutCenaTara.AsFloat;
    taSpecOutSumCenaTara.AsFloat:=taSpecOutCenaTara.AsFloat*taSpecOutKolMest.AsFloat;

//    taSpecOutSumCenaTaraSpis.AsFloat:=taSpecOutCenaTara.AsFloat*taSpecOutKolMest.AsFloat;
  end;
end;

procedure TfmAddDoc2.taSpecOutSumCenaTaraChange(Sender: TField);
begin
  if iCol=12 then //����� �� ����
  begin
    if taSpecOutKolMest.AsFloat<>0 then
    begin
      taSpecOut.Edit;
      taSpecOutCenaTara.AsFloat:=taSpecOutSumCenaTara.AsFloat/taSpecOutKolMest.AsFloat;
      taSpecOutSumCenaTaraSpis.AsFloat:=taSpecOutSumCenaTara.AsFloat;
//      taSpecOutCenaTaraSpis.AsFloat:=taSpecOutSumCenaTara.AsFloat/taSpecOutKolMest.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc2.ViewDoc2DblClick(Sender: TObject);
begin

  if cxButton1.Enabled then
     if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CodeTovar') and (iTypeDO<>1) then
    begin
      ViewDoc2CodeTovar.Options.Editing:=True;
      ViewDoc2CodeTovar.Focused:=True;
    end;
   { if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Name') and (iTypeDO<>99) then
    begin
      exit; //p
      ViewDoc2Name.Options.Editing:=True;
      ViewDoc2Name.Focused:=True;
    end;   }
    if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2CenaTovar') and ((taSpecOutCodeTovar.AsInteger=56573) or (taSpecOutCodeTovar.AsInteger=71801)) and (iTypeDO<>99) then
    begin
      ViewDoc2CenaTovar.Options.Editing:=True;
      ViewDoc2CenaTovar.Focused:=True;
    end; 
    if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NameTara') and (iTypeDO=99) then
    begin
      iDirect:=3;
      PosP.Id:=0; PosP.Name:='';
      delay(10);
      fmTara.ShowModal;
      iDirect:=0;
      if PosP.Id>0 then
      begin
        taSpecOut.Edit;
        taSpecOutCodeTara.AsInteger:=PosP.Id;
        taSpecOutNameTara.AsString:=PosP.Name;
        taSpecOut.Post;
      end;
    end;
end;

procedure TfmAddDoc2.cxCurrencyEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxCurrencyEdit5.SetFocus;
    cxCurrencyEdit5.SelectAll;
  end;
end;

procedure TfmAddDoc2.cxCurrencyEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxCurrencyEdit6.SetFocus;
    cxCurrencyEdit6.SelectAll;
  end;
end;

procedure TfmAddDoc2.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxButton1.SetFocus;
  end;
end;

procedure TfmAddDoc2.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc2.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit2.SetFocus;
  end;
end;

procedure TfmAddDoc2.cxButtonEdit1PropertiesChange(Sender: TObject);
Var S:String;
    StrWk1,StrWk2,StrWk3,SInnF:String;
begin
  if bOpen then exit;
  S:=cxButtonEdit1.Text;
  if Length(S)>2 then
  begin
    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount=1 then
      begin
        Label4.Tag:=1;
        cxButtonEdit1.Tag:=dmMC.quFCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quFCliName.AsString;

        cxLookupComboBox1.SetFocus;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount=1 then
        begin
          Label4.Tag:=2;
          cxButtonEdit1.Tag:=dmMC.quFIPCode.AsInteger;
          cxButtonEdit1.Text:=dmMC.quFIPName.AsString;
          cxLookupComboBox1.SetFocus;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;

      prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,SInnF);

      prFParCli(SInnF,sPretCli,sDogNum,sDogDate);
      if Trim(sPretCli)>'' then
      begin
        cxButton14.Visible:=True;
        cxButton15.Visible:=True;
        cxButton16.Visible:=True;
      end else
      begin
        cxButton14.Visible:=False;
        cxButton15.Visible:=False;
        cxButton16.Visible:=False;
      end;

    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutKolChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=3 then //���-��
  begin
    if taSpecOutKolMest.AsFloat<=0 then
    begin
      taSpecOutKolMest.AsFloat:=1;
      taSpecOutKolWithMest.AsFloat:=taSpecOutKol.AsFloat;
    end else
    begin
      taSpecOutKolWithMest.AsFloat:=taSpecOutKol.AsFloat/taSpecOutKolMest.AsFloat;
    end;
    rQ:=taSpecOutKol.AsFloat;
    taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
    taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;
    taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
    
    if taSpecOutCheckCM.AsInteger=1 then
      taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
    else
      taSpecOutKolSpis.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutCenaTovarSpisChange(Sender: TField);
Var rSumOut,rNDS:Real;
begin
  if iCol=5 then //-
  begin
    rSumOut:=rv(taSpecOutCenaTovarSpis.AsFloat*taSpecOutKol.AsFloat);
    taSpecOutSumCenaTovarSpis.AsFloat:=rSumOut;

    if (CommonSet.RC=1) or (cxComboBox1.ItemIndex=3) then
    begin
      rNDS:=rv(rSumOut*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));
      taSpecOutNDSSum.AsFloat:=rNDS;
      taSpecOutOutNDSSum.AsFloat:=rSumOut-rNDS;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutSumCenaTovarSpisChange(Sender: TField);
Var rSumOut,rNDS:Real;
begin
  if iCol=7 then //-
  begin
    if taSpecOutKol.AsFloat<>0 then
    begin
      taSpecOutCenaTovarSpis.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat/taSpecOutKol.AsFloat;
      rSumOut:=taSpecOutSumCenaTovarSpis.AsFloat;
      if (CommonSet.RC=1) or (cxComboBox1.ItemIndex=3) then
      begin
        rNDS:=rv(rSumOut*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));
        taSpecOutNDSSum.AsFloat:=rNDS;
        taSpecOutOutNDSSum.AsFloat:=rSumOut-rNDS;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.acSetPriceExecute(Sender: TObject);
Var rPriceIn,rPriceM,rQ,rPriceIn0:Real;
    sGTD:String;
begin
  //����������� ����
  if cxButton1.Enabled then
  begin
    iCol:=4;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      rPriceIn:=prFLP0(taSpecOutCodeTovar.AsInteger,rPriceM,rPriceIn0,sGTD);

      rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;

      taSpecOut.Edit;
      taSpecOutCenaTovarSpis.AsFloat:=rPriceM;
      taSpecOutCenaTovar.AsFloat:=rPriceIn;
      taSpecOutKol.AsFloat:=rQ;
      taSpecOutSumCenaTovarSpis.AsFloat:=rQ*rPriceM;
      taSpecOutSumCenaTovar.AsFloat:=rv(rQ*rPriceIn);
      taSpecOutCenaTaraSpis.AsFloat:=rPriceIn0; //���� ���������� ��� ���


      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
        taSpecOutNDSSum.AsFloat:=rv(rPriceIn * rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
        taSpecOutOutNDSSum.AsFloat:=rv(rPriceIn * rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
      end else
      begin
        taSpecOutOutNDSSum.AsFloat:=rv(rPriceM * rQ/(100+taSpecOutNDSProc.AsFloat)*100);
        taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
      end;

      taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecOut.First;
  end;
end;

procedure TfmAddDoc2.cxLabel3Click(Sender: TObject);
begin
//  acZPrice.Execute;
  acSetRemn.Execute;
end;

procedure TfmAddDoc2.acZPriceExecute(Sender: TObject);
Var rPriceIn,rPriceM,rQ,rPriceIn0:Real;
    PostDate:TDateTime;
    PostNum,sGTD:String;
    IndP,CodP:INteger;
    PostQuant:Real;
begin
  //����������� ������ ����
  if cxButton1.Enabled then
  begin
    IndP:=Label4.Tag;
    CodP:=cxButtonEdit1.Tag;

    if cxComboBox1.ItemIndex=4 then exit;

    if (IndP>0)and(CodP>0) then
    begin

    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      iCol:=0;


    //      rPriceIn:=prFLP(taSpecOutCodeTovar.AsInteger,rPriceM);          //������ �� ���� �������
      rPriceIn:=prFLPT(taSpecOutCodeTovar.AsInteger,IndP,CodP,PostDate,PostNum,PostQuant,rPriceIn0,sGTD);       //���. �� ����������

      rPriceM:=prFindPrice(taSpecOutCodeTovar.AsInteger);

      rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;
//      if (cxLookupComboBox1.EditValue>0)and(taSpecOutCodeTovar.AsInteger>0)
//        then rRemn:=prFindTRemnDep(taSpecOutCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
//        else rRemn:=0;

      taSpecOut.Edit;
      taSpecOutCenaTovarSpis.AsFloat:=rPriceM;
      taSpecOutCenaTovar.AsFloat:=rPriceIn;
      taSpecOutKol.AsFloat:=rQ;
      taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
      taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);

      taSpecOutCenaTaraSpis.AsFloat:=rPriceIn0; //���� ���������� ��� ���

      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
        taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
      end else
      begin
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
        taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
      end;

      taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
      taSpecOutPostDate.AsDateTime:=PostDate;
      taSpecOutPostNum.AsString:=PostNum;
      taSpecOutPostQuant.AsFloat:=PostQuant;
      taSpecOutGTD.AsString:=sGTD;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecOut.First;
    dmMT.ptTTnInLn.Active:=False;
    ViewDoc2.EndUpdate;
    end else
    begin
      showmessage('�������� �����������.');
    end;
  end;
end;

procedure TfmAddDoc2.cxButton3Click(Sender: TObject);
Var StrWk1,StrWk2,StrWk3,sInn:String;
    iDep:INteger;
    sCCode,sCountry,SInnF:String;

begin
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;

  prButtons(False);

  ViewDoc2.BeginUpdate;

  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    if taSpecOutCodeTovar.AsInteger>0 then prFindTCountry(taSpecOutCodeTovar.AsInteger,sCCode,sCountry);

    taSpecOut.Edit;
    taSpecOutCenaTovar.AsFloat:=rv(taSpecOutCenaTovar.AsFloat);
    taSpecOutCenaTovarSpis.AsFloat:=rv(taSpecOutCenaTovarSpis.AsFloat);
    taSpecOutCenaPost.AsFloat:=rv(taSpecOutCenaPost.AsFloat);
    taSpecOutSumCenaTovarSpis.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat);
    taSpecOutSumCenaTovar.AsFloat:=rv(taSpecOutSumCenaTovar.AsFloat);
    taSpecOutNDSSum.AsFloat:=rv(taSpecOutNDSSum.AsFloat);
    taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutOutNDSSum.AsFloat);
    taSpecOutCCode.AsString:=sCCode;
    taSpecOutCountry.AsString:=sCountry;
    taSpecOut.Post;

    taSpecOut.Next;
  end;
  taSpecOut.First;

  ViewDoc2.EndUpdate;

  if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'schf.frf');
  end else
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'schfRc.frf');
//    frVariables.Variable['Rukovod']:='';
//    frVariables.Variable['GlBuh']:='';
  end;

  frVariables.Variable['Rukovod']:='';
  frVariables.Variable['GlBuh']:='';

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
//  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;

  prFDepINN(iDep,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      if (CommonSet.RC=0) then
      begin
        if Trim(quDepParsFL.AsString)>'' then
        begin
        
          frVariables.Variable['FL']:=quDepParsFL.AsString;
          frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
          frVariables.Variable['Rukovod']:='';
          frVariables.Variable['GlBuh']:='';
        end else
        begin
          frVariables.Variable['FL']:='';
          frVariables.Variable['OGRN']:='';
          frVariables.Variable['Rukovod']:=cxLookupComboBox2.Text;
          frVariables.Variable['GlBuh']:=cxLookupComboBox2.Text;
        end;
      end else
      begin
        frVariables.Variable['Rukovod']:=quDepParsRukovod.AsString;
        frVariables.Variable['GlBuh']:=quDepParsGlBuh.AsString;
      end;

{      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
      end else
      begin
        frVariables.Variable['Rukovod']:=quDepParsRukovod.AsString;
        frVariables.Variable['GlBuh']:=quDepParsGlBuh.AsString;
      end;}
    end else
    begin
      frVariables.Variable['Cli1Name']:='';
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
      frVariables.Variable['Rukovod']:=cxLookupComboBox2.Text;
      frVariables.Variable['GlBuh']:=cxLookupComboBox2.Text;
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,SInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=SInnF;
    quCliPars.Active:=True;

    //�������
    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;
    //-------

    //�������
    //if quCliPars.RecordCount>0 then
    //  frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
    //  frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
    //end;

    quCliPars.Active:=False;
  end;

//  frVariables.Variable['Lico']:=cxComboBox2.Text;
  frVariables.Variable['Lico']:='';

  frRepDOUT.ReportName:='����.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  prButtons(True);

end;

procedure TfmAddDoc2.cxButton4Click(Sender: TObject);
Var StrWk1,StrWk2,StrWk3,sInn,SInnF:String;
    rSum:Real;
    iC:INteger;
    SStr:String;
    iDep:INteger;
    iNDS:INteger;
begin
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;

  prButtons(False);
  //������� ������ - ����� ������ - �.�. ��� ���� - ����������� � ������
  ViewDoc2.BeginUpdate;

  rSum:=0; iC:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    taSpecOut.Edit;

    taSpecOutCenaTovar.AsFloat:=rv(taSpecOutCenaTovar.AsFloat);
    taSpecOutCenaTovarSpis.AsFloat:=rv(taSpecOutCenaTovarSpis.AsFloat);
    taSpecOutCenaPost.AsFloat:=rv(taSpecOutCenaPost.AsFloat);
    taSpecOutSumCenaTovarSpis.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat);
    taSpecOutSumCenaTovar.AsFloat:=rv(taSpecOutSumCenaTovar.AsFloat);
//    taSpecOutNDSSum.AsFloat:=rv(taSpecOutNDSSum.AsFloat);
//    taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutOutNDSSum.AsFloat);
    taSpecOut.Post;

    taSpecOut.Next; inc(iC);
  end;
  taSpecOut.First;

  CloseTe(taSpPrint);

  iNDS:=0;

    if dmMt.ptDep.Active=false then dmMt.ptDep.Active:=true;

  if dmMt.ptDep.FindKey([iDep]) then iNDS:=dmMt.ptDepStatus3.AsInteger;

  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;

      if iNDS=1 then
      begin
        //taSpecOutNDSProc.AsFloat:=10;
        taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
        taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
        taSpPrintOutNDSSum.AsFloat:=taSpecOutOutNDSSum.AsFloat;
      end else
      begin
      //taSpecOutNDSProc.AsFloat:=0;
        taSpPrintNDSProc.AsFloat:=0;
        taSpPrintNDSSum.AsFloat:=0;
        taSpPrintOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      end;

      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;

      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
        taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
        taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
        rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
      end else
      begin
        rSum:=rSum+taSpecOutSumCenaTovarSpis.AsFloat;
        taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
        taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      end;

      taSpPrintVesTara.AsFloat:=taSpecOutVesTara.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

      taSpPrint.Post;
    end else
    begin
      if taSpecOutCodeTara.AsInteger>0 then
      begin //��� ����
        taSpPrint.Append;
        taSpPrintIGr.AsInteger:=1;
        taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
        taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
        taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
        taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;

        taSpPrintNDSProc.AsFloat:=0;
        taSpPrintNDSSum.AsFloat:=0;
        taSpPrintOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

        taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
        taSpPrintKolWithMest.AsFloat:=1;
        taSpPrintKol.AsFloat:=taSpecOutKolMest.AsFloat;

        taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
        taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;

        rSum:=rSum+taSpecOutSumCenaTara.AsFloat;

        taSpPrintVesTara.AsFloat:=taSpecOutVesTara.AsFloat;
        taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
        taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

        taSpPrint.Post;
      end;
    end;
    taSpecOut.Next;
  end;

  ViewDoc2.EndUpdate;

  if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg12.frf');
  end
  else
  begin
    if (CommonSet.RC=1) then  frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg12rc.frf');
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
//  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;

  frVariables.Variable['RSum']:=rSum;
  frVariables.Variable['SSum']:=MoneyToString(rSum,True,False);

  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  sStr:=MoneyToString(iC,True,False);
  if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);

  frVariables.Variable['SQStr']:=' ('+sStr+')';

  prFDepINN(iDep,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;

  if ((cxComboBox1.Text='2') or (cxComboBox1.Text='3')) then  frVariables.Variable['Osnovanie']:='�������� �������';
  if ((cxComboBox1.Text='0') or (cxComboBox1.Text='1')) then  frVariables.Variable['Osnovanie']:='������� ������';

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
      frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
      frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
      frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:='';
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['Cli1RSch']:='';
      frVariables.Variable['Cli1Bank']:='';
      frVariables.Variable['Cli1KSch']:='';
      frVariables.Variable['Cli1Bik']:='';
    end;
    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,SInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=SInnF;
    quCliPars.Active:=True;
    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;
    quCliPars.Active:=False;
  end;

{
[Cli1Name] ��� [Cli1Inn],[OtprAdr], �.��. [Cli1RSch], � [Cli1Bank], ����.��. [Cli1KSch], ��� [Cli1Bik]
[Cli2Name], ��� [Cli2Inn], [PoluchAdr],  �.��. [Cli2RSch], � [Cli2Bank], ����.��. [Cli2KSch], ��� [Cli2Bik]
}

  frRepDOUT.ReportName:='���������.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;

  prButtons(True);
end;


procedure TfmAddDoc2.cxButton5Click(Sender: TObject);
Var StrWk1,StrWk2,StrWk3:String;
    sInn,sInnF:String;
    rSum:Real;
    iC:Integer;
    sStr:String;
    rQSpis:Real;
    rSum1,rSum2,rSumNDS:Real;
    iDep:INteger;
    iNDS:INteger;
begin
  iDep:=cxLookupComboBox1.EditValue;

  if cxComboBox3.ItemIndex=1 then iDep:=1;

  prButtons(False);

  ViewDoc2.BeginUpdate;

  iNDS:=0;
  if dmMt.ptDep.Active=false then dmMt.ptDep.Active:=true;

  if dmMt.ptDep.FindKey([iDep]) then iNDS:=dmMt.ptDepStatus3.AsInteger;

  rSum:=0; iC:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    taSpecOut.Edit;
    taSpecOutCenaTovar.AsFloat:=RoundVal(taSpecOutCenaTovar.AsFloat);
    taSpecOutCenaTovarSpis.AsFloat:=RoundVal(taSpecOutCenaTovarSpis.AsFloat);
    taSpecOutCenaPost.AsFloat:=RoundVal(taSpecOutCenaPost.AsFloat);
    taSpecOutSumCenaTovarSpis.AsFloat:=RoundVal(taSpecOutSumCenaTovarSpis.AsFloat);
    taSpecOutSumCenaTovar.AsFloat:=RoundVal(taSpecOutSumCenaTovar.AsFloat);
//    taSpecOutNDSSum.AsFloat:=RoundVal(taSpecOutNDSSum.AsFloat);
//    taSpecOutOutNDSSum.AsFloat:=RoundVal(taSpecOutOutNDSSum.AsFloat);
    taSpecOut.Post;

    taSpecOut.Next; inc(iC);
  end;
  taSpecOut.First;

  ViewDoc2.EndUpdate;

  if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'schf.frf');
  end else
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'schfRc.frf');
  end;

  frVariables.Variable['Rukovod']:='';
  frVariables.Variable['GlBuh']:='';

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
//  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;

  prFDepINN(iDep,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      if (CommonSet.RC=0) then
      begin
        if Trim(quDepParsFL.AsString)>'' then
        begin
          frVariables.Variable['FL']:=quDepParsFL.AsString;
          frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
          frVariables.Variable['Rukovod']:='';
          frVariables.Variable['GlBuh']:='';
        end else
        begin
          frVariables.Variable['FL']:='';
          frVariables.Variable['OGRN']:='';
          frVariables.Variable['Rukovod']:=cxLookupComboBox2.Text;
          frVariables.Variable['GlBuh']:=cxLookupComboBox2.Text;
        end;
      end else
      begin
        frVariables.Variable['Rukovod']:=quDepParsRukovod.AsString;
        frVariables.Variable['GlBuh']:=quDepParsGlBuh.AsString;
      end;
    end;
    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,SInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=SInnF; //��� ������ ��� ��������� ����� ������ ���
    quCliPars.Active:=True;
    //�������
    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;
    //-------

    //�������
    //if quCliPars.RecordCount>0 then
    //  frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
    //  frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
    //end;

    quCliPars.Active:=False;
  end;

//  frVariables.Variable['Lico']:=cxComboBox2.Text;
  frVariables.Variable['Lico']:='';

  frRepDOUT.ReportName:='����.';
  frRepDOUT.PrepareReport;
  frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  delay(100);

  ViewDoc2.BeginUpdate;

  rQSpis:=0;

  CloseTe(taSpPrint);
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    taSpPrint.Append;
    taSpPrintIGr.AsInteger:=1;
    taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
    taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
    taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
    taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
//    taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
//    taSpPrintOutNDSSum.AsFloat:=taSpecOutOutNDSSum.AsFloat;

    if iNDS=1 then
    begin
      //taSpecOutNDSProc.AsFloat:=10;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutOutNDSSum.AsFloat;
    end else
    begin
      //taSpecOutNDSProc.AsFloat:=0;
      taSpPrintNDSProc.AsFloat:=0;
      taSpPrintNDSSum.AsFloat:=0;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
    end;

    taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
    taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
    taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;

    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
    end else
    begin
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      rSum:=rSum+taSpecOutSumCenaTovarSpis.AsFloat;
    end;

    taSpPrintVesTara.AsFloat:=taSpecOutVesTara.AsFloat;
    taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
    taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

    if (taSpecOutCodeTovar.AsInteger>0)and(taSpecOutBrak.AsFloat>0.01)and(taSpecOutCheckCM.AsInteger=1) then
      rQSpis:=rQSpis+taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat;


    taSpPrint.Post;

    taSpecOut.Next;
  end;

  ViewDoc2.EndUpdate;

//  frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg12.frf');

  if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg12.frf');
  end
  else
  begin
    frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg12rc.frf');
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
//  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
  frVariables.Variable['Cli1Name']:='';

  frVariables.Variable['RSum']:=rSum;
  frVariables.Variable['SSum']:=MoneyToString(rSum,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  sStr:=MoneyToString(iC,True,False);
  if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);

  frVariables.Variable['SQStr']:=' ('+sStr+')';

  prFDepINN(iDep,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;

  if ((cxComboBox1.Text='2') or (cxComboBox1.Text='3')) then  frVariables.Variable['Osnovanie']:='�������� �������';
  if ((cxComboBox1.Text='0') or (cxComboBox1.Text='1')) then  frVariables.Variable['Osnovanie']:='������� ������';

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
      frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
      frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
      frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;
    end;
    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=SInnF;
    quCliPars.Active:=True;
    if quCliPars.RecordCount>0 then
    begin
      frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      frVariables.Variable['Cli2RSch']:=quCliParsRSch.AsString;
      frVariables.Variable['Cli2Bank']:=quCliParsBank.AsString;
      frVariables.Variable['Cli2KSch']:=quCliParsKSch.AsString;
      frVariables.Variable['Cli2Bik']:=quCliParsBik.AsString;
    end;
    quCliPars.Active:=False;
  end;

{
[Cli1Name] ��� [Cli1Inn],[OtprAdr], �.��. [Cli1RSch], � [Cli1Bank], ����.��. [Cli1KSch], ��� [Cli1Bik]
[Cli2Name], ��� [Cli2Inn], [PoluchAdr],  �.��. [Cli2RSch], � [Cli2Bank], ����.��. [Cli2KSch], ��� [Cli2Bik]
}

  frRepDOUT.ReportName:='���������.';
  frRepDOUT.PrepareReport;
  frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;

  //�������� ��� ��������
  if rQSpis<>0 then
  begin
    ViewDoc2.BeginUpdate;

    CloseTe(taSpPrint);
    rSum1:=0; rSum2:=0; rSumNDS:=0;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      if (taSpecOutCodeTovar.AsInteger>0)and(taSpecOutBrak.AsFloat>0.01)and(taSpecOutCheckCM.AsInteger=1) then
      begin
//      rSumP:=rSumP+taSpecOutSumCenaTovar.AsFloat;
//      rSum:=rSum+taSpecOutSumCenaTovarSpis.AsFloat;
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;
//      rSumNDS:=rSumNDS+RoundVal(taSpecOutNDSSum.AsFloat);

        taSpPrint.Append;
        taSpPrintIGr.AsInteger:=1;
        taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
        taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
        taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
        taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
        taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
        taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
        taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
        taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
        taSpPrintKol.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat;
        taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
        taSpPrintSumCenaTovar.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat));
        taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
        taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
        taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
        taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

        taSpPrintCenaTovar0.AsFloat:=rv(taSpecOutSumCenaTovar.AsFloat*100/(100+taSpecOutNDSProc.AsFloat))/taSpecOutKol.AsFloat;
        taSpPrintSumCenaTovar0.AsFloat:=rv(taSpPrintCenaTovar0.AsFloat*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat));
        taSpPrintNDSSum.AsFloat:=taSpPrintSumCenaTovar.AsFloat-taSpPrintSumCenaTovar0.AsFloat;

        rSum1:=rSum1+taSpPrintSumCenaTovar0.AsFloat;
        rSum2:=rSum2+taSpecOutSumCenaTovar.AsFloat;
        rSumNDS:=rSumNDS+taSpecOutSumCenaTovar.AsFloat-taSpPrintSumCenaTovar0.AsFloat;

        taSpPrint.Post;
      end;
      taSpecOut.Next;
    end;

    ViewDoc2.EndUpdate;

    frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActSpisNOut.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
    frVariables.Variable['Depart1']:='��������';
    frVariables.Variable['SumOld']:=rSum1;
    frVariables.Variable['SumNew']:=rSum2;
    frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum1),True,False);
    frVariables.Variable['SSumMag']:=MoneyToString(abs(rSum2),True,False);
    frVariables.Variable['SSumNDS']:=MoneyToString(abs(rSumNDS),True,False);

    frRepDOUT.ReportName:='��� ��������.';

    frRepDOUT.PrepareReport;

    frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

    taSpPrint.Active:=False;
  end;

  prButtons(True);
end;

procedure TfmAddDoc2.cxButton6Click(Sender: TObject);
Var StrWk1,StrWk2,StrWk3,sInn,sInnF:String;
    rSum,rSumT,rSumNDS,rSumP:Real;
    iDep:INteger;
begin  //��� �������� ����� ��������
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;

  CloseTe(taSpPrint);
  rSum:=0; rSumT:=0; rSumNDS:=0; rSumP:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      rSumP:=rSumP+taSpecOutSumCenaTovar.AsFloat;
      rSum:=rSum+taSpecOutSumCenaTovarSpis.AsFloat;
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;
      rSumNDS:=rSumNDS+RoundVal(taSpecOutNDSSum.AsFloat);

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    if taSpecOutCodeTara.AsInteger>0 then
    begin
//      rSumP:=rSumP+taSpecOutSumCenaTara.AsFloat;
//      rSum:=rSum+taSpecOutCenaTaraSpis.AsFloat;
      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;
//      rSumNDS:=rSumNDS+RoundVal(taSpecOutSumCenaTovar.AsFloat*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=2;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
//      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTaraSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    taSpecOut.Next;
  end;

  ViewDoc2.EndUpdate;

  with dmP do
  begin
//    if quDepartsSt.Active=False then quDepartsSt.Active:=True;

    quAllDepart.Locate('ID',iDep,[]);

    if quAllDepartStatus3.AsInteger=1 then
    begin  //���������� ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActOutM.frf');
    end else
    begin //������������ ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActOutM1.frf');
    end;
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
      frVariables.Variable['FL']:=quDepParsFL.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;

    quCliPars.Active:=False;
  end;

  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTovarP']:=rSumP;
  frVariables.Variable['SumTara']:=rSumT;
  frVariables.Variable['SumNDS']:=rSumNDS;

  frVariables.Variable['SSumTovarP']:=MoneyToString(rSumP,True,False);
  frVariables.Variable['SSumTovar']:=MoneyToString(rSum,True,False);
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['SSumNDS']:=MoneyToString(rSumNDS,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='��� �� �������.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);
  taSpPrint.Active:=False;
  prButtons(True);
end;


procedure TfmAddDoc2.cxButton7Click(Sender: TObject);
Var StrWk1,StrWk2,Strwk3,sInn,sInnF:String;
    rSum,rSumT,rSumNDS:Real;
    iDep:INteger;
begin
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;
  CloseTe(taSpPrint);
  rSum:=0; rSumT:=0; rSumNDS:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
      rSumNDS:=rSumNDS+rv(taSpecOutNDSSum.AsFloat);
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    if taSpecOutCodeTara.AsInteger>0 then
    begin
      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=2;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
//      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTaraSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  with dmMc do
  begin
    dmP.quAllDepart.Locate('ID',iDep,[]);

    if dmP.quAllDepartStatus3.AsInteger=1 then
    begin  //���������� ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActOut1.frf');
    end else
    begin //������������ ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActOut.frf');
    end;
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
      frVariables.Variable['FL']:=quDepParsFL.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;

    quCliPars.Active:=False;
  end;

{
  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
  prFDepINN(cxLookupComboBox1.EditValue,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;
  frVariables.Variable['Cli2Name']:=cxButtonEdit1.Text;
  prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3);
  frVariables.Variable['Cli2Inn']:=StrWk1;
  frVariables.Variable['Cli2Adr']:=StrWk2;
}

  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTara']:=rSumT;
  frVariables.Variable['SumNDS']:=rSumNDS;

  frVariables.Variable['SSumTovar']:=MoneyToString(rSum,True,False);
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['SSumNDS']:=MoneyToString(rSumNDS,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='��� �� �������.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc2.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc2.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc2.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc2.Columns[j].Name='ViewDoc2CodeTovar' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel4Click(Sender: TObject);
begin
  //����������� ����
  if cxButton1.Enabled then
  begin
    ViewDoc2.BeginUpdate;
    iCol:=4;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      taSpecOut.Edit;
      taSpecOutCenaTovar.AsFloat:=0;
      taSpecOutCenaTovarSpis.AsFloat:=0;
      taSpecOutCenaTaraSpis.AsFloat:=0;
      taSpecOut.Post;

      taSpecOut.Next;
//      delay(10);
    end;
    iCol:=0;
    taSpecOut.First;
    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.acPostTovExecute(Sender: TObject);
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if taSpecOut.RecordCount>0 then
    begin
      bDrOut:=True;
      
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=taSpecOutCodeTovar.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=taSpecOutCodeTovar.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=taSpecOutName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc2.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc2);
end;

procedure TfmAddDoc2.acSetRemnExecute(Sender: TObject);
Var rQ,rRemn:Real;
begin
  //����������� �������
  if cxButton1.Enabled then
  begin
    iCol:=2;
    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
//      rPriceIn:=prFLP(taSpecOutCodeTovar.AsInteger,rPriceM);

      rQ:=taSpecOutKolWithMest.AsFloat;

      if (cxLookupComboBox1.EditValue>0)and(taSpecOutCodeTovar.AsInteger>0)
        then rRemn:=prFindTRemnDep(taSpecOutCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
        else rRemn:=0;

      taSpecOut.Edit;
//      taSpecOutCenaTovarSpis.AsFloat:=rPriceM;
//      taSpecOutCenaTovar.AsFloat:=rPriceIn;
      taSpecOutKolWithMest.AsFloat:=rQ;   //������ ��� ��������� ���� ���� � ��������
      taSpecOutRemn.AsFloat:=rREmn;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecOut.First;
    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.cxLabel7Click(Sender: TObject);
begin
  acZPrice.Execute;
end;

procedure TfmAddDoc2.acTermoPExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;
begin
  //������ �� �����
  if ViewDoc2.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
//              prWritePrinter(StrWk+#$0D+#$0A);
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if prOpenPrinter(CommonSet.TPrintN) then
          begin
            for i:=0 to ViewDoc2.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewDoc2.Controller.SelectedRecords[i];

              iNum:=0;
              for j:=0 to Rec.ValueCount-1 do
                if ViewDoc2.Columns[j].Name='ViewDoc2CodeTovar' then begin  iNum:=Rec.Values[j]; break; end;

              if iNum>0 then
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  for k:=0 to Str.Count-1 do
                  begin
                    StrWk:=Str[k];
                    if pos('@38.3@',StrWk)>0 then
                    begin
                      insert('E30',StrWk,pos('@38.3@',StrWk));
                      delete(StrWk,pos('@38.3@',StrWk),6)
                    end;
                    if pos('@',StrWk)>0 then
                    begin //���� ��� �� ���� ����������??
                      Str1:=Strwk;

                      Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                      delete(Str1,1,pos('@',Str1));//3.30.36@"

                      Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                      delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                      Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                      if Str4='3' then   //��������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4FullName.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='5' then  //������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //5.30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4NameCu.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='7' then  //�������     7.6.36
                      begin
                        Str5:=INtToStr(iNum);
                      end;
                      if Str4='18' then   //����   18.10.36
                      begin
                        Str5:=ToStr(quFindC4Cena.AsFloat);
                      end;
                      if Str4='31' then    //��
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //31.13.8
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=13;

                        //�� �� ������� ����
                        sBar:=quFindC4BarCode.AsString;
                        if Length(sBar)<13 then
                        begin
                          if Length(sBar)=7 then //������� ����
                          begin
                            sBar:=ToStandart(sBar);
                            l:=13;
                          end;
                        end;
                        Str5:=Copy(sBar,1,l);
                      end;
                      if Str4='39' then   //���-��
                      begin
                        Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                      end;
                      StrWk:=Str2+Str5+Str1;
                    end;
                    prWritePrinter(StrWk+#$0D+#$0A);
                  end;
                end;
                quFindC4.Active:=False;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;

procedure TfmAddDoc2.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
var rNDS:Real;
begin
  with dmMc do
  with dmMt do
  with dmE do
  begin
      if cxLookupComboBox1.Properties.ReadOnly=true then exit;
      if (iTypeDO<>99) then
      begin
        if dmP.quAllDepart.Active=False then dmP.quAllDepart.Active:=True;
        ViewDoc2.BeginUpdate;

        dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);             //p

        if (dmP.quAllDepartStatus3.AsInteger=1) or (cxComboBox3.ItemIndex=1) then //���������� ��� ���� ����� ���� �����������
        begin
      {    cxButton3.Enabled:=True;
    //      cxButton4.Enabled:=True;
          cxButton5.Enabled:=True; }

          if taSpecOut.Active=False then CloseTa(taSpecOut);

          taSpecOut.First;
          while not taSpecOut.Eof do
          begin
            taSpecOut.Edit;

            rNDS:=0;
            if taCards.FindKey([taSpecOutCodeTovar.AsInteger]) then rNDS:=taCardsNDS.AsFloat;
            taSpecOutNDSProc.AsFloat:=rNDS;

            if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
            begin
              taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+rNDS)*rNDS;
              taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+rNDS)*100;
            end else
            begin
              taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+rNDS)*100);
              taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
            end;

            taSpecOut.Post;
            taSpecOut.Next;
          end;
        end else //�������� ����� �� ���������� ���
        begin
          if cxComboBox3.ItemIndex=0 then //����������� �������
          begin
    {        cxButton3.Enabled:=False;
    //      cxButton4.Enabled:=False;
            cxButton5.Enabled:=False;
                                           }
            if taSpecOut.Active=False then CloseTa(taSpecOut);

            taSpecOut.First;
            while not taSpecOut.Eof do
            begin
              taSpecOut.Edit;
              taSpecOutNDSProc.AsFloat:=0;
              taSpecOutNDSSum.AsFloat:=0;
              taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
              taSpecOut.Post;
              taSpecOut.Next;
            end;
          end;
        end;
        ViewDoc2.EndUpdate;
      end;

      quUPLMh.Active:=False;
      quUPLMh.ParamByName('IMH').AsInteger:=cxLookupComboBox1.EditValue;
      quUPLMh.Active:=True;

      quUPLMh.First;

      if quUPLMh.RecordCount>0 then
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=quUPLMhID.AsInteger;   //p
        fmAddDoc2.cxLookupComboBox2.Text:=quUPLMhNAME.AsString;       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end
      else
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=0;   //p
        fmAddDoc2.cxLookupComboBox2.Text:='';       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end;


   { if quTTNOutAcStatus.AsInteger<>3 then
    begin
      cxButton3.Enabled:=False;
      cxButton4.Enabled:=False;
      cxButton5.Enabled:=False;
    end
    else
    begin
      cxButton3.Enabled:=True;
      cxButton4.Enabled:=True;
      cxButton5.Enabled:=True;
    end; }
  end;

end;

procedure TfmAddDoc2.cxButton9Click(Sender: TObject);
Var StrWk:String;
//    bAdd:Boolean;
    iM:INteger;
    iR:INteger;
begin
  with dmMt do
  begin
    StrWk:=cxComboBox2.Text;
    if ptMOL.Locate('OTVL',StrWk,[loCaseInsensitive]) then
    begin
      ShowMessage('���. ���� ��� ����, ���������� ����������.');
    end else
    begin
      iR:= MessageDlg('�� ������������� ������ �������� ���. ���� - "'+StrWk+'"',mtConfirmation, [mbYes, mbNo], 0, mbYes);
      if iR=3  then
      begin
        iM:=ptMol.RecordCount+1;

        ptMol.Append;
        ptMOLID.AsInteger:=iM;
        ptMOLOTVL.AsString:=StrWk;
        ptMol.Post;

        cxComboBox2.Properties.Items.Clear;
        cxComboBox2.Properties.Items.Add(''); //������ ����
        ptMol.First;
        while not ptMol.Eof do
        begin
          cxComboBox2.Properties.Items.Add(ptMOLOTVL.AsString);
          ptMol.Next;
        end;
        cxComboBox2.ItemIndex:=ptMOL.RecordCount;

      end;
    end;
  end;
end;

procedure TfmAddDoc2.N1Click(Sender: TObject);
var Par:Variant;
    iC,i,j:INteger;
    Rec:TcxCustomGridRecord;
begin
  //����������
  with dmMT do
  with dmMC do
  begin
    if ViewDoc2.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=2;
    par[1]:=quTTnOutID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=2;
      taHeadDocId.AsInteger:=quTTnOutID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnOutDateInvoice.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnOutNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTnOutCodePoluch.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnOutNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnOutDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnOutNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnOutSummaTovar.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnOutSummaTovarSpis.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTnOutIndexPoluch.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTnOutSummaTara.AsFloat;
      taHeadDoc.Post;

      iC:=1;

      for i:=0 to ViewDoc2.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDoc2.Controller.SelectedRecords[i];
          taSpecDoc.Append;
          for j:=0 to Rec.ValueCount-1 do
          begin
                                                                     taSpecDocIType.AsInteger:=2;
                                                                     taSpecDocIdHead.AsInteger:=quTTnOutID.AsInteger;
                                                                     taSpecDocNum.AsInteger:=iC;
            if ViewDoc2.Columns[j].Name='ViewDoc2CodeTovar'     then taSpecDocIdCard.AsInteger:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2KolMest'       then taSpecDocQuantM.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2KolWithMest'   then taSpecDocQuant.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2CenaTovar'     then taSpecDocPriceIn.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2SumCenaTovar'  then taSpecDocSumIn.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2CenaTovarSpis' then taSpecDocPriceUch.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2CenaTovarSpis' then taSpecDocSumUch.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2NDSProc'       then taSpecDocIdNds.AsInteger:=RoundEx(Rec.Values[j]);
            if ViewDoc2.Columns[j].Name='ViewDoc2NDSSum'        then taSpecDocSumNds.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2Name'          then taSpecDocNameC.AsString:=Copy(Rec.Values[j],1,30);
                                                                     taSpecDocSm.AsString:='';
            if ViewDoc2.Columns[j].Name='ViewDoc2CodeEdIzm'     then taSpecDocIdM.AsInteger:=Rec.Values[j];
                                                                     taSpecDocKm.AsFloat:=0;
                                                                     taSpecDocPriceUch1.AsFloat:=0;
                                                                     taSpecDocSumUch1.AsFloat:=0;
                                                                     taSpecDocProcPrice.AsFloat:=0;
            if ViewDoc2.Columns[j].Name='ViewDoc2CodeTara'      then taSpecDocIdTara.AsInteger:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2KolMest'       then taSpecDocQuantT.AsFloat:=Rec.Values[j];
                                                                     taSpecDocNameT.AsString:='';
            if ViewDoc2.Columns[j].Name='ViewDoc2CenaTara'      then taSpecDocPriceT.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2SumCenaTara'   then taSpecDocSumTara.AsFloat:=Rec.Values[j];
            if ViewDoc2.Columns[j].Name='ViewDoc2BarCode'       then taSpecDocBarCode.AsString:=Rec.Values[j];
          end;
          inc(iC);
          taSpecDoc.Post;
        end;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmAddDoc2.N2Click(Sender: TObject);
Var StrWk:String;
begin
  // ��������
  with dmMC do
  with dmMT do
  begin
    if (cxComboBox1.ItemIndex=1) or (cxComboBox1.ItemIndex=10)  then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocOut') then
        begin
          fmAddDoc2.ViewDoc2.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc2 do
              begin
                taSpecOut.Append;
                taSpecOutNum.AsInteger:=taSpecDocNum.AsInteger;

                taSpecOutCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecOutCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecOutBarCode.AsString:=taSpecDocBarCode.AsString;
                taSpecOutNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                taSpecOutNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecOutOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                taSpecOutKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                taSpecOutKolEdMest.AsFloat:=0;
                taSpecOutKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecOutKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                taSpecOutCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecOutSumCenaTovar.AsFloat:=taSpecDocSumIn.AsFloat;

                taSpecOutCenaTaraSpis.AsFloat:=taSpecDocPriceIn.AsFloat; //���� ���������� ��� ��� - �������� ����������, �� ���� ���

                taSpecOutCenaTovarSpis.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecOutSumCenaTovarSpis.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecOutName.AsString:=taSpecDocNameC.AsString;
                taSpecOutCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                StrWk:='';
                if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                taSpecOutNameTara.AsString:=StrWk;

                taSpecOutVesTara.AsFloat:=0;
                taSpecOutCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                taSpecOutSumVesTara.AsFloat:=0;
                taSpecOutSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
                taSpecOutTovarType.AsInteger:=0;

                quCId.Active:=False;
                quCId.ParamByName('IDC').AsInteger:=taSpecDocIdCard.AsInteger;
                quCId.Active:=True;
                if quCId.RecordCount>0 then taSpecOutFullName.AsString:=quCIdFullName.AsString;
                quCId.Active:=False;

                taSpecOutBrak.AsFloat:=0;
                taSpecOutCheckCM.AsInteger:=0;
                taSpecOutKolF.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                taSpecOutKolSpis.AsFloat:=0;
                taSpecOut.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmAddDoc2.ViewDoc2.EndUpdate;

          fmAddDoc2.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmAddDoc2.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmAddDoc2.cxButton10Click(Sender: TObject);
Var StrWk1,StrWk2,Strwk3,sInn,sInnF:String;
    rSum,rSumT,rSumNDS:Real;
begin //��� �������� ����������
  prButtons(False);

  ViewDoc2.BeginUpdate;
  CloseTe(taSpPrint);
  rSum:=0; rSumT:=0; rSumNDS:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
      rSumNDS:=rSumNDS+rv(taSpecOutNDSSum.AsFloat);
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintPostDate.AsDateTime:=taSpecOutPostDate.AsDateTime;
      taSpPrintPostNum.AsString:=taSpecOutPostNum.AsString;
      taSpPrintPostQuant.AsFloat:=taSpecOutPostQuant.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    if taSpecOutCodeTara.AsInteger>0 then
    begin
      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=2;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
//      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTaraSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintPostDate.AsDateTime:=0;
      taSpPrintPostNum.AsString:='';
      taSpPrintPostQuant.AsFloat:=0;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      taSpPrint.Post;
    end;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActOutP.frf');

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=cxLookupComboBox1.EditValue;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
      frVariables.Variable['FL']:=quDepParsFL.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:='';
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;

    quCliPars.Active:=False;
  end;

{
  frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
  prFDepINN(cxLookupComboBox1.EditValue,StrWk1,StrWk2);
  frVariables.Variable['Cli1Inn']:=StrWk1;
  frVariables.Variable['Cli1Adr']:=StrWk2;
  frVariables.Variable['Cli2Name']:=cxButtonEdit1.Text;
  prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3);
  frVariables.Variable['Cli2Inn']:=StrWk1;
  frVariables.Variable['Cli2Adr']:=StrWk2;
}

  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

//  frVariables.Variable['SumTovar']:=rSum;
//  frVariables.Variable['SumTara']:=rSumT;
//  frVariables.Variable['SumNDS']:=rSumNDS;

//  frVariables.Variable['SSumTovar']:=MoneyToString(rSum,True,False);
//  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
//  frVariables.Variable['SSumNDS']:=MoneyToString(rSumNDS,True,False);

  frRepDOUT.ReportName:='��� �� �������.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.acZPricePoslPrihExecute(Sender: TObject);
Var rPriceIn,rPriceM,rQ,rPriceIn0:Real;
    PostDate:TDateTime;
    PostNum,sGTD:String;
    IndP,CodP:INteger;
    PostQuant:Real;
begin
  //����������� ������ ����
  if cxButton1.Enabled then
  begin
    iCol:=0;
    IndP:=Label4.Tag;
    CodP:=cxButtonEdit1.Tag;

    if cxComboBox1.ItemIndex=4 then exit;

    if (IndP>0)and(CodP>0) then
    begin

    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      rPriceIn:=prFLP0(taSpecOutCodeTovar.AsInteger,rPriceM,rPriceIn0,sGTD);          //������ �� ���� �������
      PostDate:=StrToDate('01.01.2000');
      PostNum:='0';
      PostQuant:=0;
      //rPriceIn:=prFLPT(taSpecOutCodeTovar.AsInteger,IndP,CodP,PostDate,PostNum,PostQuant);       //���. �� ����������

      rPriceM:=prFindPrice(taSpecOutCodeTovar.AsInteger);

      rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;
//      if (cxLookupComboBox1.EditValue>0)and(taSpecOutCodeTovar.AsInteger>0)
//        then rRemn:=prFindTRemnDep(taSpecOutCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
//        else rRemn:=0;

      taSpecOut.Edit;
      taSpecOutCenaTovarSpis.AsFloat:=rPriceM;
      taSpecOutCenaTovar.AsFloat:=rPriceIn;
      taSpecOutCenaTaraSpis.AsFloat:=rPriceIn0;

      taSpecOutKol.AsFloat:=rQ;
      taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
      taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);

      if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
      begin
        taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
      end else
      begin
        taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
        taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
      end;

      taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;
      taSpecOutPostDate.AsDateTime:=PostDate;
      taSpecOutPostNum.AsString:=PostNum;
      taSpecOutPostQuant.AsFloat:=PostQuant;
      taSpecOutGTD.AsString:=sGTD;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecOut.First;
    dmMT.ptTTnInLn.Active:=False;
    ViewDoc2.EndUpdate;
    end else
    begin
      showmessage('�������� �����������.');
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel8Click(Sender: TObject);
begin
  acZPricePoslPrih.Execute;
end;

procedure TfmAddDoc2.acSetNacExecute(Sender: TObject);
Var rPrIn,rQ:Real;
    rPrOut,rSumOut,rNDS:Real;
    rPrNac,rn1,rn2,rn3:Real;
    rSumBrak,rPrBrak:Real;
begin
//���������� �������
  if CommonSet.RC=1 then
  begin
    dmMt.taCards.Refresh;

    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      iCol:=0;
      rQ:=taSpecOutKol.AsFloat;
      if (taSpecOutCodeTovar.AsInteger>0)and(rQ<>0) then
      begin
        if (CommonSet.RC=1) or (cxComboBox1.ItemIndex=3) then
        begin
          with dmMT do
          begin
            rPrIn:=taSpecOutCenaTovar.AsFloat;
            rPrBrak:=0;

            if taSpecOutCheckCM.AsInteger=1 then
            begin
              rSumBrak:=taSpecOutCenaTovar.AsFloat*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat);
              rPrBrak:=rSumBrak/rQ;
            end;
            rPrOut:=rv((rPrIn*(100+CommonSet.DefNac)/100+rPrBrak)*10)/10;
            rSumOut:=rv((rPrIn*(100+CommonSet.DefNac)/100+rPrBrak)*10)/10*rQ;
            rNDS:=rv(rSumOut*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));

            if taCards.findkey([taSpecOutCodeTovar.AsInteger]) then
            begin
              if taCardsFixPrice.AsFloat>0 then  //���� ������������� ����
              begin
                rPrOut:=rv(taCardsFixPrice.AsFloat);
                rSumOut:=rPrOut*rQ;
                rNDS:=rv(rSumOut*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));
              end else
              begin
                prFindNacGr(taSpecOutCodeTovar.AsInteger,rn1,rn2,rn3,rPrNac);
                if rn1>0 then
                begin
                  if rn1<0.02 then rn1:=0;

                  rPrOut:=rv(rPrIn*(100+rn1)/100+rPrBrak);
                  rSumOut:=rPrOut*rQ;
                  rNDS:=rv(rSumOut*taSpecOutNDSProc.AsFloat/(100+taSpecOutNDSProc.AsFloat));
                end;
              end;

              taSpecOut.Edit;
              taSpecOutCenaTovarSpis.AsFloat:=rPrOut;
              taSpecOutSumCenaTovarSpis.AsFloat:=rSumOut;
              taSpecOutNDSSum.AsFloat:=rNDS;
              taSpecOutOutNDSSum.AsFloat:=rSumOut-rNDS;
              taSpecOut.Post;
            end;
          end;
        end;
      end;
      taSpecOut.Next;
    end;

    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.cxLabel9Click(Sender: TObject);
begin
  acSetNac.Execute;
end;

procedure TfmAddDoc2.ViewDoc2SelectionChanged(
  Sender: TcxCustomGridTableView);
Var IndP,CodP:INteger;
begin
// ����� ���
//  ShowMessage('���');
  //exit
  //��������� ������� �������� ������ - �������� ���� ���-�� ����������
  if taSpecOutCodeTovar.AsInteger<1 then exit;
  if ViewDoc2.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    if ((taSpecOutCodeTovar.AsInteger=56573) or (taSpecOutCodeTovar.AsInteger=71801)) then
    begin
      Vi1.BeginUpdate;
      quPost5.Active:=False;
      Vi1.EndUpdate;
      Vi2.BeginUpdate;
      quPost7.Active:=False;
      Vi2.EndUpdate;
    end
    else
    begin
      if Lev1.Visible then
      begin
        Vi1.BeginUpdate;
        quPost5.Active:=False;
        quPost5.ParamByName('IDCARD').Value:=taSpecOutCodeTovar.AsInteger;
        quPost5.ParamByName('DDATE').Value:=Date;
        quPost5.Active:=True;
        Vi1.EndUpdate;
      end;
      if Lev2.Visible then
      begin
        IndP:=Label4.Tag;
        CodP:=cxButtonEdit1.Tag;

        Vi2.BeginUpdate;
        quPost7.Active:=False;
        quPost7.ParamByName('IDCARD').AsInteger:=taSpecOutCodeTovar.AsInteger;
        quPost7.ParamByName('INDP').AsInteger:=IndP;
        quPost7.ParamByName('CODP').AsINteger:=CodP;
        quPost7.Active:=True;
        Vi2.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.Vi1CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //
//  if CommonSet.RC=1 then exit;
  if cxButton1.Enabled then
  begin
    with dmMc do
    begin
      if quPost5.RecordCount>0 then
      begin
        if taSpecOut.RecordCount>0 then
        begin
          iCol:=4;
          taSpecOut.Edit;
          taSpecOutCenaTovar.AsFloat:=quPost5CenaTovar.AsFloat;
//          taSpecOutSumCenaTovar.AsFloat:=rv(taSpecOutKol.AsFloat*quPost5CenaTovar.AsFloat);
//          taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutKol.AsFloat*quPost5CenaTovar.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
//          taSpecOutNDSSum.AsFloat:=rv(taSpecOutKol.AsFloat*quPost5CenaTovar.AsFloat)-rv(taSpecOutKol.AsFloat*quPost5CenaTovar.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);

          taSpecOutCenaTaraSpis.AsFloat:=quPost5CenaTovar0.AsFloat; //���� ������ ��� ���

          //��� ������ � � ��� �����������
          if quPost5SertNumber.AsString>'' then taSpecOutGTD.AsString:=quPost5SertNumber.AsString else taSpecOutGTD.AsString:='';
          taSpecOut.Post;

          ViewDoc2CenaTovar.Focused:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxComboBox1PropertiesChange(Sender: TObject);
Var IndP,CodP:INteger;
begin
  if ViewDoc2.Controller.SelectedRecordCount>1 then exit;

  if (cxComboBox1.ItemIndex<=4) then
  begin
    Lev1.Visible:=False;
    Lev2.Visible:=True;

    IndP:=Label4.Tag;
    CodP:=cxButtonEdit1.Tag;

    with dmMc do
    begin
      Vi2.BeginUpdate;
      quPost7.Active:=False;
      quPost7.ParamByName('IDCARD').AsInteger:=taSpecOutCodeTovar.AsInteger;
      quPost7.ParamByName('INDP').AsInteger:=IndP;
      quPost7.ParamByName('CODP').AsINteger:=CodP;
      quPost7.Active:=True;
      Vi2.EndUpdate;
    end;
  end else
  begin
    Lev1.Visible:=True;
    Lev2.Visible:=False;
  end;
end;

procedure TfmAddDoc2.Vi2CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var rQ:Real;
begin
  //
//  if CommonSet.RC=1 then exit;
  if cxButton1.Enabled then
  begin
    with dmMc do
    begin
      if quPost7.RecordCount>0 then
      begin
        if taSpecOut.RecordCount>0 then
        begin
          iCol:=4;

          rQ:=taSpecOutKol.AsFloat;
          taSpecOut.Edit;
          taSpecOutCenaTovar.AsFloat:=quPost7CenaTovar.AsFloat;
          taSpecOutCenaTovarSpis.AsFloat:=quPost7CenaTovarSpis.AsFloat;
          taSpecOutNDSProc.AsFloat:=quPost7NDSProc.AsFloat;
          taSpecOutCenaTaraSpis.AsFloat:=quPost7CenaTaraSpis.AsFloat; //���� ��� ���

          taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
          taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);
          if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then 
          begin
            taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
          end else
          begin
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
            taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
          end;
          taSpecOut.Post;

          ViewDoc2CenaTovar.Focused:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutCalcFields(DataSet: TDataSet);
begin
  if taSpecOutCenaTovar.AsFloat>0 then
    taSpecOutrNac.AsFloat:=rv((taSpecOutCenaTovarSpis.AsFloat-taSpecOutCenaTovar.AsFloat)/taSpecOutCenaTovar.AsFloat*100)
  else taSpecOutrNac.AsFloat:=0;
end;

procedure TfmAddDoc2.cxButton11Click(Sender: TObject);
Var rSum1,rSum2,rSumNDS:Real;
begin //��� �������� ����� ��������
  prButtons(False);

  ViewDoc2.BeginUpdate;

  iCol:=0;
  
  CloseTe(taSpPrint);
  rSum1:=0; rSum2:=0; rSumNDS:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    if (taSpecOutCodeTovar.AsInteger>0)and(taSpecOutBrak.AsFloat>0.01)and(taSpecOutCheckCM.AsInteger=1) then
    begin
//      rSumP:=rSumP+taSpecOutSumCenaTovar.AsFloat;
//      rSum:=rSum+taSpecOutSumCenaTovarSpis.AsFloat;
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;
//      rSumNDS:=rSumNDS+RoundVal(taSpecOutNDSSum.AsFloat);

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat));
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

      taSpPrintCenaTovar0.AsFloat:=rv(taSpecOutSumCenaTovar.AsFloat*100/(100+taSpecOutNDSProc.AsFloat))/taSpecOutKol.AsFloat;
      taSpPrintSumCenaTovar0.AsFloat:=rv(taSpPrintCenaTovar0.AsFloat*(taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat));
      taSpPrintNDSSum.AsFloat:=taSpPrintSumCenaTovar.AsFloat-taSpPrintSumCenaTovar0.AsFloat;

      rSum1:=rSum1+taSpPrintSumCenaTovar0.AsFloat;
      rSum2:=rSum2+taSpecOutSumCenaTovar.AsFloat;
      rSumNDS:=rSumNDS+taSpecOutSumCenaTovar.AsFloat-taSpPrintSumCenaTovar0.AsFloat;

      taSpPrint.Post;
    end;
    taSpecOut.Next;
  end;

  ViewDoc2.EndUpdate;

  frRepDOUT.LoadFromFile(CommonSet.Reports + 'ActSpisNOut.frf');

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
  frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
  frVariables.Variable['Depart1']:='��������';
  frVariables.Variable['SumOld']:=rSum1;
  frVariables.Variable['SumNew']:=rSum2;
  frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum1),True,False);
  frVariables.Variable['SSumMag']:=MoneyToString(abs(rSum2),True,False);
  frVariables.Variable['SSumNDS']:=MoneyToString(abs(rSumNDS),True,False);

  frRepDOUT.ReportName:='��� ��������.';

  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);
  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.taSpecOutKolFChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=13 then //���-�� ����
  begin
    taSpecOutKolMest.AsFloat:=1;
    taSpecOutKolWithMest.AsFloat:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);
    taSpecOutKol.AsFloat:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);

    rQ:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);
    taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
    taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;
    taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;

    if taSpecOutCheckCM.AsInteger=1 then
      taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
    else
      taSpecOutKolSpis.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutBrakChange(Sender: TField);
//
Var rQ:Real;
begin
  if iCol=14 then //����
  begin
    taSpecOutKolMest.AsFloat:=1;
    taSpecOutKolWithMest.AsFloat:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);
    taSpecOutKol.AsFloat:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);

    rQ:=taSpecOutKolF.AsFloat-(taSpecOutKolF.AsFloat*taSpecOutBrak.AsFloat/100);
    taSpecOutSumCenaTovarSpis.AsFloat:=rQ*taSpecOutCenaTovarSpis.AsFloat;
    taSpecOutSumCenaTovar.AsFloat:=rv(rQ*taSpecOutCenaTovar.AsFloat);
    if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100)) then
    begin
      taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
    end else
    begin
      taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
      taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
    end;
    taSpecOutSumCenaTara.AsFloat:=taSpecOutKolMest.AsFloat*taSpecOutCenaTara.AsFloat;

    if taSpecOutCheckCM.AsInteger=1 then
      taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
    else
      taSpecOutKolSpis.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutCheckCMChange(Sender: TField);
begin
  if taSpecOutCheckCM.AsInteger=1 then
    taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
  else
    taSpecOutKolSpis.AsFloat:=0;
end;

procedure TfmAddDoc2.acPrintSCHFExecute(Sender: TObject);
begin
//
  cxButton3.Click;
end;

procedure TfmAddDoc2.acPredzExecute(Sender: TObject);
var iMax:integer;
begin
  with dmP do
  begin
    if (cxButtonEdit1.Text>'') then
    begin
      if (cxLookupComboBox1.Text>'') then
      begin
        quFilVoz.Active:=false;
        quFilVoz.ParamByName('ICLI').Value:=cxButtonEdit1.Tag;
        quFilVoz.ParamByName('DEP').Value:=cxLookupComboBox1.EditValue;
        quFilVoz.Active:=true;

        if quFilVoz.RecordCount<1 then
        begin
          showmessage('��� ���������� �� �����������!');
          exit;
        end;
        iMax:=1;
        if (cxLabel11.Tag=1) or (cxLabel12.Tag=1) then
        begin
          if MessageDlg('��������� �� ����������� ��� ���������, ��������� �� ������?',mtConfirmation,[mbYes, mbNo], 0, mbNo)=3 then
          begin
            delay(30);
            quFilVoz.First;
            while not quFilVoz.Eof do
            begin
              if taSpecOut.Locate('CodeTovar;Kol;CenaTovar;CenaTovarSpis',VarArrayOf([quFilVozCODE.AsInteger,quFilVozQRem.AsFloat,quFilVozPRICEIN.AsFloat,quFilVozRPrice.AsFloat]),[]) then
              begin
                taSpecOut.Delete;
              end;
              quFilVoz.Next;
              delay(5);
            end;

            taSpecOut.First;
            while not taSpecOut.Eof do
            begin
              taSpecOut.Edit;
              taSpecOutNum.AsInteger:=iMax;
              iMax:=iMax+1;
              taSpecOut.Next;
              delay(5);
            end;
          end
          else exit;
        end;
        Vi1.BeginUpdate;
        ViewDoc2.BeginUpdate;
        quFilVoz.First;
        while not quFilVoz.Eof do
        begin
          taSpecOut.Last;
          iMax:=taSpecOutNum.AsInteger+1;

          taSpecOut.Append;
          taSpecOutNum.AsInteger:=iMax;
          taSpecOutCodeTovar.AsInteger:=quFilVozCODE.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quFilVozEdIzm.AsInteger;
          taSpecOutBarCode.AsString:=quFilVozBarCode.AsString;
          //taSpecOutNDSProc.AsFloat:=quFilVozNDS.AsFloat;
          taSpecOutNDSProc.AsFloat:=0;
          taSpecOutNDSSum.AsFloat:=0;
          taSpecOutOutNDSSum.AsFloat:=0;
          taSpecOutKolMest.AsInteger:=1;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=quFilVozQRem.AsFloat;
          taSpecOutKol.AsFloat:=taSpecOutKolWithMest.AsFloat*taSpecOutKolMest.AsInteger;
          taSpecOutCenaTovar.AsFloat:=quFilVozPRICEIN.AsFloat;
          taSpecOutCenaTovarSpis.AsFloat:=quFilVozRPrice.AsFloat;

         { if quFilVozRSUMIN.AsFloat>0.01 then taSpecOutSumCenaTovar.AsFloat:=quFilVozRSUMIN.AsFloat
          else }taSpecOutSumCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat;

          {if quFilVozRsumNDS.AsFloat>0.01 then taSpecOutSumCenaTovarSpis.AsFloat:=quFilVozRsumNDS.AsFloat
          else} taSpecOutSumCenaTovarSpis.AsFloat:=taSpecOutCenaTovarSpis.AsFloat*taSpecOutKol.AsFloat;

          taSpecOutName.AsString:=quFilVozNameCode.AsString;
          taSpecOutCodeTara.AsInteger:=0;
          taSpecOutNameTara.AsString:='';
          taSpecOutVesTara.AsFloat:=0;
          taSpecOutCenaTara.AsFloat:=0;
          taSpecOutCenaTaraSpis.AsFloat:=0; //���� ������ ��� ��� (���� ���� ��� ���)
          taSpecOutSumVesTara.AsFloat:=0;
          taSpecOutSumCenaTara.AsFloat:=0;
          taSpecOutSumCenaTaraSpis.AsFloat:=0;
          taSpecOutFullName.AsString:=quFilVozFNameCode.AsString;

          taSpecOutBrak.AsFloat:=0;
          taSpecOutCheckCM.AsInteger:=0;
          taSpecOutKolF.AsFloat:=taSpecOutKol.AsFloat;
          taSpecOutKolSpis.AsFloat:=0;

        {  taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);     //(���� ���/(100+%���)*100)
          taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;           //���� ��� - ����� ���
               }


          if (iTypeDO<>99)  then
          begin
            dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
            if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quFilVozNDS.AsFloat
            else taSpecOutNDSProc.AsFloat:=0;

            if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
                taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

            end else
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
             //   taSpecOutNDSProc.AsFloat:=quFilVozNDS.AsFloat;
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
                taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

            end;
            if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= (taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat); //���� ����� ������ ��� ���
          end;
          taSpecOut.Post;

          quFilVoz.Next;
          delay(10);
        end;
        Vi1.EndUpdate;
        cxLabel5.Tag:=cxLookupComboBox1.EditValue;
        cxLabel11.Tag:=1;
        ViewDoc2.EndUpdate;
      end
      else showmessage('�� ������� ����� ��������');
    end
    else showmessage('�� ������ ����������');
  end;
end;  

procedure TfmAddDoc2.cxLabel11Click(Sender: TObject);
begin
  if (cxButtonEdit1.Text='') or (cxLookupComboBox1.Text='') or (cxComboBox1.Text='') then
  begin
    showmessage('������� ��������� ����������, ����� �������� � ���!');
    exit;
  end;
  acPredz.Execute;
end;

procedure TfmAddDoc2.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
begin

  if (taSpecOut.RecordCount>0) then      //p
  begin
    if cxLookupComboBox1.Properties.ReadOnly=true then exit;
    if (cxLabel5.Tag<>cxLookupComboBox1.EditValue) then
    begin
      if MessageDlg('������� ������ ����� � �������� ������������?',mtConfirmation,[mbYes, mbNo], 0, mbNo)=3 then
      begin
        taSpecOut.First; while not taSpecOut.Eof do taSpecOut.Delete;
      {  dmMC.quPost5.Active:=False;
        dmMC.quPost7.Active:=False;  }
        cxLabel11.Tag:=0;
      end
      else cxLookupComboBox1.EditValue:=cxLabel5.Tag;
    end;
  end;                                                             //p
end;

procedure TfmAddDoc2.cxLookupComboBox1Click(Sender: TObject);
begin
  dmp.quAllDepart.Active:=false;
  dmp.quAllDepart.Active:=true;
  dmP.quAllDepart.First;
end;

procedure TfmAddDoc2.acPredz2Execute(Sender: TObject);
var iMax:integer;
begin
  with dmP do
  begin
    if (cxButtonEdit1.Text>'') then
    begin
      if (cxLookupComboBox1.Text>'') then
      begin
        quFilVoz2.Active:=false;
        quFilVoz2.ParamByName('ICLI').Value:=cxButtonEdit1.Tag;
        quFilVoz2.ParamByName('DEP').Value:=cxLookupComboBox1.EditValue; 
        quFilVoz2.ParamByName('Date').Value:=trunc(date);
        quFilVoz2.Active:=true;

        if quFilVoz2.RecordCount<1 then
        begin
          showmessage('��� ���������� �� �����������!');
          exit;
        end;
        iMax:=1;
        if (cxLabel11.Tag=1) or (cxLabel12.Tag=1) then
        begin
          if MessageDlg('��������� �� ����������� ��� ���������, ��������� �� ������?',mtConfirmation,[mbYes, mbNo], 0, mbNo)=3 then
          begin
            delay(30);
            quFilVoz2.First;
            while not quFilVoz2.Eof do
            begin
              if taSpecOut.Locate('CodeTovar;Kol;CenaTovar;CenaTovarSpis',VarArrayOf([quFilVoz2CODE.AsInteger,quFilVoz2QRem.AsFloat,quFilVoz2PRICEIN.AsFloat,quFilVoz2RPrice.AsFloat]),[]) then
              begin
                taSpecOut.Delete;
              end;
              quFilVoz2.Next;
              delay(5);
            end;

            taSpecOut.First;
            while not taSpecOut.Eof do
            begin
              taSpecOut.Edit;
              taSpecOutNum.AsInteger:=iMax;
              iMax:=iMax+1;
              taSpecOut.Next;
              delay(5);
            end;
          end
          else exit;
        end;
        vi1.BeginUpdate;
        ViewDoc2.BeginUpdate;
        quFilVoz2.First;
        while not quFilVoz2.Eof do
        begin
          taSpecOut.Last;
          iMax:=taSpecOutNum.AsInteger+1;

          taSpecOut.Append;
          taSpecOutNum.AsInteger:=iMax;
          taSpecOutCodeTovar.AsInteger:=quFilVoz2CODE.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quFilVoz2EdIzm.AsInteger;
          taSpecOutBarCode.AsString:=quFilVoz2BarCode.AsString;
      //    taSpecOutNDSProc.AsFloat:=quFilVoz2NDS.AsFloat;               //% ��� �� ��������
          taSpecOutNDSSum.AsFloat:=0;
          taSpecOutOutNDSSum.AsFloat:=0;
          taSpecOutKolMest.AsInteger:=1;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=quFilVoz2QRem.AsFloat;
          taSpecOutKol.AsFloat:=taSpecOutKolWithMest.AsFloat*taSpecOutKolMest.AsInteger;
          taSpecOutCenaTovar.AsFloat:=quFilVoz2PRICEIN.AsFloat;                //���� ���� � ���
          taSpecOutCenaTovarSpis.AsFloat:=quFilVoz2RPrice.AsFloat;             //���� ����

          {if quFilVoz2RSUMIN.AsFloat>0.01 then taSpecOutSumCenaTovar.AsFloat:=quFilVoz2RSUMIN.AsFloat       // ����� ����� � ���
          else} taSpecOutSumCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat;

         {if quFilVoz2RsumNDS.AsFloat>0.01 then taSpecOutSumCenaTovarSpis.AsFloat:=quFilVoz2RsumNDS.AsFloat     //����� ���� � ����� ���
          else} taSpecOutSumCenaTovarSpis.AsFloat:=taSpecOutCenaTovarSpis.AsFloat*taSpecOutKol.AsFloat;

          taSpecOutName.AsString:=quFilVoz2NameCode.AsString;
          taSpecOutCodeTara.AsInteger:=0;
          taSpecOutNameTara.AsString:='';
          taSpecOutVesTara.AsFloat:=0;
          taSpecOutCenaTara.AsFloat:=0;
          taSpecOutCenaTaraSpis.AsFloat:=0;                                    //���� ������ ��� ���
          taSpecOutSumVesTara.AsFloat:=0;
          taSpecOutSumCenaTara.AsFloat:=0;
          taSpecOutSumCenaTaraSpis.AsFloat:=0;
          taSpecOutFullName.AsString:=quFilVoz2FNameCode.AsString;

          taSpecOutBrak.AsFloat:=0;
          taSpecOutCheckCM.AsInteger:=0;
          taSpecOutKolF.AsFloat:=taSpecOutKol.AsFloat;
          taSpecOutKolSpis.AsFloat:=0;

          if (iTypeDO<>99)  then
          begin
            dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
            if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quFilVoz2NDS.AsFloat
            else taSpecOutNDSProc.AsFloat:=0;

            if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
                taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

            end else
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
               // taSpecOutNDSProc.AsFloat:=quFilVozNDS.AsFloat;
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
                taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

            end;
            if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= rv(taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat);
          end;

          taSpecOut.Post;
          delay(10);
          quFilVoz2.Next;
        end;
        cxLabel5.Tag:=cxLookupComboBox1.EditValue;
        cxLabel12.Tag:=1;
        vi1.EndUpdate;
        ViewDoc2.EndUpdate;
      end
      else showmessage('�� ������� ����� ��������');
    end
    else showmessage('�� ������ ����������');
  end;
end;

procedure TfmAddDoc2.cxLabel12Click(Sender: TObject);
begin
  if (cxButtonEdit1.Text='') or (cxLookupComboBox1.Text='') or (cxComboBox1.Text='') then
  begin
    showmessage('������� ��������� ����������, ����� �������� � ���!');
    exit;
  end;
  acPredz2.Execute;
end;

procedure TfmAddDoc2.taSpecOutCodeTovarChange(Sender: TField);
begin
fmAddDoc2.ViewDoc2SelectionChanged(ViewDoc2);
end;

procedure TfmAddDoc2.cxTextEdit1KeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['0'..'9',#8]) then Key := #0;
end;

procedure TfmAddDoc2.cxButton12Click(Sender: TObject);
Var StrWk1,StrWk2,StrWk3,sInnF:String;
    rSum1,rSum2,rSum3:Real;
//    iDep:INteger;
begin //������ ��� ����������� ���������

// iDep:=cxLookupComboBox1.EditValue;

  if (cxComboBox1.ItemIndex<3)or(cxComboBox1.ItemIndex>4) then exit;

  prButtons(False);

  ViewDoc2.BeginUpdate;

  CloseTe(taSpPrint);
  rSum1:=0; rSum2:=0; rSum3:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;

      taSpPrintCenaTovar0.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;

      try
        taSpPrintNac.AsFloat:=(taSpecOutCenaTovarSpis.AsFloat-taSpecOutCenaTovar.AsFloat)/taSpecOutCenaTovar.AsFloat*100;
      except
        taSpPrintNac.AsFloat:=0;
      end;

      taSpPrintSumCenaTovar0.AsFloat:=rv(taSpecOutCenaTaraSpis.AsFloat*taSpecOutKol.AsFloat);
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;

      taSpPrint.Post;

      rSum1:=rSum1+taSpPrintSumCenaTovar0.AsFloat;
      rSum2:=rSum2+taSpPrintSumCenaTovar.AsFloat;
      rSum3:=rSum3+taSpPrintSumCenaMag.AsFloat;

    end;

    taSpecOut.Next;
  end;

  ViewDoc2.EndUpdate;

  frRepDOUT.LoadFromFile(CommonSet.Reports + 'ReestrOut.frf');

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

  end;

  frVariables.Variable['rSum1']:=rSum1;
  frVariables.Variable['rSum2']:=rSum2;
  frVariables.Variable['rSum3']:=rSum3;

  frVariables.Variable['sSum1']:=MoneyToString(rSum1,True,False);
  frVariables.Variable['sSum2']:=MoneyToString(rSum2,True,False);
  frVariables.Variable['sSum3']:=MoneyToString(rSum3,True,False);

  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='������ ����������� ���������.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);
  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.ViewDoc2NameTaraPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  prTaraPropButClick;
end;

procedure TfmAddDoc2.cxButton13Click(Sender: TObject);
begin
    cxButton1.Click;
    fmDocs2.acOnDoc2.Execute;
    with dmMC do
    begin
      if quTTNOutAcStatus.AsInteger=0 then exit//fmDocs2.acEditDoc2.Execute //��������������
      else fmDocs2.acViewDoc2.Execute; //��������}
    end;

end;

procedure TfmAddDoc2.cxLabel15Click(Sender: TObject);
  var iMax:integer;
begin
 with dmP do
  begin
    quSetMatrix.Active:=false;
    quSetMatrix.Active:=true;
    fmSetMatrix.ShowModal;
    if fmSetMatrix.ModalResult=mrOK then
    begin
      iMax:=1;
      taSpecOut.First;
      if not taSpecOut.Eof then
      begin
        taSpecOut.Last;
        iMax:=taSpecOutNum.AsInteger+1;
      end;
      taSpecOut.Append;
      taSpecOutNum.AsInteger:=iMax;
      taSpecOutCodeTovar.AsInteger:=quSetMatrixID.AsInteger;
      taSpecOutCodeEdIzm.AsInteger:=quSetMatrixEdIzm.AsInteger;
      taSpecOutBarCode.AsString:=quSetMatrixBarCode.AsString;
      //taSpecOutNDSProc.AsFloat:=0;
      taSpecOutNDSSum.AsFloat:=0;
      taSpecOutOutNDSSum.AsFloat:=0;
      taSpecOutKolMest.AsInteger:=1;
      taSpecOutKolEdMest.AsFloat:=1;
      taSpecOutKolWithMest.AsFloat:=1;
      taSpecOutKol.AsFloat:=1;
      taSpecOutCenaTovar.AsFloat:=quSetMatrixCena.AsFloat;
      taSpecOutCenaTovarSpis.AsFloat:=quSetMatrixCena.AsFloat;
      taSpecOutSumCenaTovar.AsFloat:=quSetMatrixCena.AsFloat;
      taSpecOutSumCenaTovarSpis.AsFloat:=quSetMatrixCena.AsFloat;
      taSpecOutName.AsString:=quSetMatrixName.AsString;
      taSpecOutCodeTara.AsInteger:=0;
      taSpecOutNameTara.AsString:='';
      taSpecOutVesTara.AsFloat:=0;
      taSpecOutCenaTara.AsFloat:=0;
      taSpecOutCenaTaraSpis.AsFloat:=0; //���� ������ ��� ���   (���� ���� ��� ���)
      taSpecOutSumVesTara.AsFloat:=0;
      taSpecOutSumCenaTara.AsFloat:=0;
      taSpecOutSumCenaTaraSpis.AsFloat:=0;
      taSpecOutFullName.AsString:=quSetMatrixFullName.AsString;

      if (iTypeDO<>99)  then
      begin
        dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
        if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quSetMatrixNDS.AsFloat
        else taSpecOutNDSProc.AsFloat:=0;

        if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
        begin
          if (taSpecOutNDSProc.AsFloat>0) then
          begin
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
            taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
          end
          else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

        end else
        begin
          if (taSpecOutNDSProc.AsFloat>0) then
          begin
            taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
            taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
          end
          else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

        end;
        if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= (taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat); //���� ����� ������ ��� ���
      end;

      taSpecOutBrak.AsFloat:=0;
      taSpecOutCheckCM.AsInteger:=0;
      taSpecOutKolF.AsFloat:=1;
      taSpecOutKolSpis.AsFloat:=0;

      taSpecOut.Post;
      cxLabel5.Tag:=cxLookupComboBox1.EditValue;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutBeforePost(DataSet: TDataSet);
Var sGTD:String;
begin
  if CommonSet.RC=1 then  //���� ������� ������ �� ��
  begin
    with dmMT do
    begin
      if trim(taSpecOutGTD.AsString)='' then
      begin
        if (trim(taSpecOutCCode.AsString)<>'643')
        and (trim(taSpecOutCCode.AsString)<>'398')
        and (trim(taSpecOutCCode.AsString)<>'112') then
        begin
          if ptTTnInLn.Active then
          begin
            sGTD:='';
            ptTTnInLn.CancelRange;
            ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

            ptTTnInLn.SetRange([taSpecOutCodeTovar.AsInteger,cxDateEdit1.Date-90],[taSpecOutCodeTovar.AsInteger,cxDateEdit1.Date]);
            ptTTnInLn.Last;   //������� � �����
            while not ptTTnInLn.Bof do
            begin
              sGTD:=OemToAnsiConvert(Trim(ptTTnInLnSertNumber.AsString));
              if sGTD>='0' then Break;
              ptTTnInLn.Prior;
            end;
            taSpecOutGTD.AsString:=AnsiToOemConvert(sGTD);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxButton14Click(Sender: TObject);
Var StrWk1,StrWk2,Strwk3,sInn,sInnF:String;
    rSum,rSumT:Real;
    iDep:INteger;
begin
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;
  rSum:=0; rSumT:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if taSpecOutCodeTovar.AsInteger>0 then rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
    if taSpecOutCodeTara.AsInteger>0 then rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  frRepDOUT.LoadFromFile(CommonSet.Reports + 'PretCli.frf');

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
     end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;

      frVariables.Variable['PretCli']:=quCliParsPRETCLI.AsString;
      frVariables.Variable['DOGNUM']:=quCliParsDOGNUM.AsString;
      frVariables.Variable['DOGDATE']:=ds1(quCliParsDOGDATE.AsDateTime);
    end;

    quCliPars.Active:=False;
  end;


  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTara']:=rSumT;

  frVariables.Variable['SSumTovar']:=its(Trunc(rSum))+' ���. '+its(Trunc(rv(Frac(rSum)*100)))+' ���.';
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='���������.';
  frRepDOUT.PrepareReport;

  
  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.cxButton15Click(Sender: TObject);
Var StrWk1,StrWk2,Strwk3,sInn,sInnF:String;
    rSum,rSumT,rSumNDS:Real;
    iDep:INteger;
    PostDate:TDateTime;
    PostNum:String;
    PostQuant,rPriceIn0:Real;
    sGTD,sPostDate:String;
begin
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;
  CloseTe(taSpPrint);
  rSum:=0; rSumT:=0; rSumNDS:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if prFLPT(taSpecOutCodeTovar.AsInteger,Label4.Tag,cxButtonEdit1.Tag,PostDate,PostNum,PostQuant,rPriceIn0,sGTD)<0.01 then
    begin
      PostNum:='';
      sPostDate:='';
    end else
    begin
      PostNum:='�'+PostNum+' ��';
      sPostDate:=ds1(PostDate);
    end;

    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
      rSumNDS:=rSumNDS+rv(taSpecOutNDSSum.AsFloat);
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      if taSpPrintKol.AsFloat<>0 then taSpPrintOutNDSPrice.AsFloat:=rv(taSpecOutNDSSum.AsFloat/taSpPrintKol.AsFloat) else taSpPrintOutNDSPrice.AsFloat:=0;
      taSpPrintLastTTNNum.AsString:=PostNum;
      taSpPrintLastTTNDate.AsString:=sPostDate;
      taSpPrint.Post;
    end;

    if taSpecOutCodeTara.AsInteger>0 then
    begin
      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=2;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
//      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTaraSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      if taSpPrintKol.AsFloat<>0 then taSpPrintOutNDSPrice.AsFloat:=rv(taSpecOutNDSSum.AsFloat/taSpPrintKol.AsFloat) else taSpPrintOutNDSPrice.AsFloat:=0;
      taSpPrintLastTTNNum.AsString:=PostNum;
      taSpPrintLastTTNDate.AsString:=sPostDate;
      taSpPrint.Post;
    end;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  with dmMc do
  begin
    dmP.quAllDepart.Locate('ID',iDep,[]);

    if dmP.quAllDepartStatus3.AsInteger=1 then
    begin  //���������� ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg15.frf');
    end else
    begin //������������ ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg15_1.frf');
    end;
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
      frVariables.Variable['FL']:=quDepParsFL.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;

    quCliPars.Active:=False;
  end;

  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTara']:=rSumT;
  frVariables.Variable['SumNDS']:=rSumNDS;

  frVariables.Variable['SSumTovar']:=MoneyToString(rSum,True,False);
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['SSumNDS']:=MoneyToString(rSumNDS,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='���� 15.';
  frRepDOUT.PrepareReport;

  if cxCheckBox1.Checked then frRepDOUT.ShowPreparedReport
  else frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;
  prButtons(True);
end;

procedure TfmAddDoc2.cxButton16Click(Sender: TObject);
Var StrWk1,StrWk2,Strwk3,sInn,sInnF:String;
    rSum,rSumT,rSumNDS:Real;
    iDep:INteger;
    PostDate:TDateTime;
    PostNum:String;
    PostQuant,rPriceIn0:Real;
    sGTD,sPostDate:String;
    
begin  //��������� + ����15
  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;
  rSum:=0; rSumT:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if taSpecOutCodeTovar.AsInteger>0 then rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
    if taSpecOutCodeTara.AsInteger>0 then rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  frRepDOUT.LoadFromFile(CommonSet.Reports + 'PretCli.frf');

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
     end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;

      frVariables.Variable['PretCli']:=quCliParsPRETCLI.AsString;
      frVariables.Variable['DOGNUM']:=quCliParsDOGNUM.AsString;
      frVariables.Variable['DOGDATE']:=ds1(quCliParsDOGDATE.AsDateTime);
    end;

    quCliPars.Active:=False;
  end;


  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTara']:=rSumT;

  frVariables.Variable['SSumTovar']:=its(Trunc(rSum))+' ���. '+its(Trunc(rv(Frac(rSum)*100)))+' ���.';
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='���������.';
  frRepDOUT.PrepareReport;

  
  frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;

  iDep:=cxLookupComboBox1.EditValue;
  if cxComboBox3.ItemIndex=1 then iDep:=1;
  prButtons(False);

  ViewDoc2.BeginUpdate;
  CloseTe(taSpPrint);
  rSum:=0; rSumT:=0; rSumNDS:=0;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin

    if prFLPT(taSpecOutCodeTovar.AsInteger,Label4.Tag,cxButtonEdit1.Tag,PostDate,PostNum,PostQuant,rPriceIn0,sGTD)<0.01 then
    begin
      PostNum:='';
      sPostDate:='';
    end else
    begin
      PostNum:='�'+PostNum+' ��';
      sPostDate:=ds1(PostDate);
    end;

    if taSpecOutCodeTovar.AsInteger>0 then
    begin
      rSum:=rSum+taSpecOutSumCenaTovar.AsFloat;
      rSumNDS:=rSumNDS+rv(taSpecOutNDSSum.AsFloat);
//      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=1;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTovar.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutFullName.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTovar.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTovar.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTovarSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      if taSpPrintKol.AsFloat<>0 then taSpPrintOutNDSPrice.AsFloat:=rv(taSpecOutNDSSum.AsFloat/taSpPrintKol.AsFloat) else taSpPrintOutNDSPrice.AsFloat:=0;
      taSpPrintLastTTNNum.AsString:=PostNum;
      taSpPrintLastTTNDate.AsString:=sPostDate;
      taSpPrint.Post;
    end;

    if taSpecOutCodeTara.AsInteger>0 then
    begin
      rSumT:=rSumT+taSpecOutSumCenaTara.AsFloat;

      taSpPrint.Append;
      taSpPrintIGr.AsInteger:=2;
      taSpPrintNum.AsInteger:=taSpecOutNum.AsInteger;
      taSpPrintCodeTovar.AsInteger:=taSpecOutCodeTara.AsInteger;
      taSpPrintFullName.AsString:=taSpecOutNameTara.AsString;
      taSpPrintCodeEdIzm.AsInteger:=taSpecOutCodeEdIzm.AsInteger;
      taSpPrintNDSProc.AsFloat:=taSpecOutNDSProc.AsFloat;
      taSpPrintOutNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintKolMest.AsFloat:=taSpecOutKolMest.AsFloat;
      taSpPrintKolWithMest.AsFloat:=taSpecOutKolWithMest.AsFloat;
      taSpPrintKol.AsFloat:=taSpecOutKol.AsFloat;
      taSpPrintCenaTovar.AsFloat:=taSpecOutCenaTara.AsFloat;
      taSpPrintSumCenaTovar.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintNDSSum.AsFloat:=taSpecOutNDSSum.AsFloat;
      taSpPrintCenaMag.AsFloat:=taSpecOutCenaTaraSpis.AsFloat;
//      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTaraSpis.AsFloat;
      taSpPrintSumCenaMag.AsFloat:=taSpecOutSumCenaTara.AsFloat;
      taSpPrintBrak.AsFloat:=taSpecOutBrak.AsFloat;
      taSpPrintKolF.AsFloat:=taSpecOutKolF.AsFloat;
      if taSpPrintKol.AsFloat<>0 then taSpPrintOutNDSPrice.AsFloat:=rv(taSpecOutNDSSum.AsFloat/taSpPrintKol.AsFloat) else taSpPrintOutNDSPrice.AsFloat:=0;
      taSpPrintLastTTNNum.AsString:=PostNum;
      taSpPrintLastTTNDate.AsString:=sPostDate;
      taSpPrint.Post;
    end;

    taSpecOut.Next;
  end;
  ViewDoc2.EndUpdate;

  with dmMc do
  begin
    dmP.quAllDepart.Locate('ID',iDep,[]);

    if dmP.quAllDepartStatus3.AsInteger=1 then
    begin  //���������� ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg15.frf');
    end else
    begin //������������ ���
      frRepDOUT.LoadFromFile(CommonSet.Reports + 'torg15_1.frf');
    end;
  end;

  frVariables.Variable['DocNum']:=cxTextEdit1.Text;
  frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);

  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
      frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
      frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
      frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
      frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
      frVariables.Variable['FL']:=quDepParsFL.AsString;
    end else
    begin
      frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
      frVariables.Variable['Cli1Inn']:='';
      frVariables.Variable['Cli1Adr']:='';
      frVariables.Variable['Otpr']:='';
      frVariables.Variable['OtprAdr']:='';
      frVariables.Variable['OGRN']:='';
      frVariables.Variable['FL']:='';
    end;

    quDepPars.Active:=False;

    prFCliINN(cxButtonEdit1.Tag,Label4.Tag,StrWk1,StrWk2,StrWk3,sInnF);

    frVariables.Variable['Cli2Name']:=StrWk3;
    frVariables.Variable['Cli2Inn']:=StrWk1;
    frVariables.Variable['Cli2Adr']:=StrWk2;

    sInn:=StrWk1;
    if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
      begin
        frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
        frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
      end
      else
      begin
        frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
        frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
      end;
    end;

    quCliPars.Active:=False;
  end;

  frVariables.Variable['SumTovar']:=rSum;
  frVariables.Variable['SumTara']:=rSumT;
  frVariables.Variable['SumNDS']:=rSumNDS;

  frVariables.Variable['SSumTovar']:=MoneyToString(rSum,True,False);
  frVariables.Variable['SSumTara']:=MoneyToString(rSumT,True,False);
  frVariables.Variable['SSumNDS']:=MoneyToString(rSumNDS,True,False);
  frVariables.Variable['TypeP']:=cxComboBox1.ItemIndex;

  frRepDOUT.ReportName:='���� 15.';
  frRepDOUT.PrepareReport;

  frRepDOUT.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);

  taSpPrint.Active:=False;

  prButtons(True);
end;

end.
