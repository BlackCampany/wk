unit of_AvansRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, FIBDataSet,
  pFIBDataSet;

type
  TfmRepAvans = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridAvans: TcxGrid;
    LevelAvans: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    frRepOb: TfrReport;
    frtaObVed: TfrDBDataSet;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    ViewAvans: TcxGridDBTableView;
    quAv: TpFIBDataSet;
    quAvSIFR: TFIBIntegerField;
    quAvIDCARD: TFIBIntegerField;
    quAvNAMEB: TFIBStringField;
    quAvCATEGORY: TFIBIntegerField;
    quAvDATEDOC: TFIBDateField;
    quAvSUM: TFIBFloatField;
    dsquAv: TDataSource;
    ViewAvansSIFR: TcxGridDBColumn;
    ViewAvansIDCARD: TcxGridDBColumn;
    ViewAvansNAMEB: TcxGridDBColumn;
    ViewAvansCATEGORY: TcxGridDBColumn;
    ViewAvansDATEDOC: TcxGridDBColumn;
    ViewAvansSUM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepAvans: TfmRepAvans;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps, PeriodUni;

{$R *.dfm}

procedure TfmRepAvans.FormCreate(Sender: TObject);
begin
  GridAvans.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewAvans.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepAvans.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewAvans.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepAvans.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepAvans.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepAvans.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewAvans);
end;

procedure TfmRepAvans.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRepAvans.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmRepAvans.SpeedItem2Click(Sender: TObject);
begin
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmRepAvans.Caption:='������ � ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepAvans.Caption:='������ � ������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

    Memo1.Clear;
    try
      Memo1.Lines.Add('����� ���� ������������ ������ ...');
      ViewAvans.BeginUpdate;
      quAv.Active:=False;
      quAv.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
      quAv.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
      quAv.Active:=True;
    finally
      ViewAvans.EndUpdate;
      Memo1.Lines.Add('������������ ��.');
    end;  
  end;
end;

end.
