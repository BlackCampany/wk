unit PeriodR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfBackGround, cxControls, cxContainer, cxEdit, cxLabel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxGraphics, cxCheckBox,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, DB, pvtables,
  sqldataset, cxButtonEdit, cxGroupBox, cxRadioGroup;

type
  TfmPeriodR = class(TForm)
    dxfBackGround1: TdxfBackGround;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton10: TcxButton;
    cxLabel3: TcxLabel;
    cxCheckBox1: TcxCheckBox;
    cxLabel5: TcxLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    cxButton3: TcxButton;
    cxCheckBox3: TcxCheckBox;
    quDeps: TPvQuery;
    dsquDeps: TDataSource;
    quDepsID: TSmallintField;
    quDepsName: TStringField;
    cxButtonEdit1: TcxButtonEdit;
    quSG: TPvQuery;
    dsquSG: TDataSource;
    cxLabel6: TcxLabel;
    cxLookupComboBox4: TcxLookupComboBox;
    cxCheckBox4: TcxCheckBox;
    cxLabel7: TcxLabel;
    cxLookupComboBox5: TcxLookupComboBox;
    cxCheckBox5: TcxCheckBox;
    quBr: TPvQuery;
    dsquBr: TDataSource;
    quCat: TPvQuery;
    dsquCat: TDataSource;
    quBrID: TIntegerField;
    quBrNAMEBRAND: TStringField;
    quCatID: TIntegerField;
    quCatSID: TStringField;
    quCatCOMMENT: TStringField;
    cxLabel8: TcxLabel;
    cxLookupComboBox6: TcxLookupComboBox;
    cxCheckBox6: TcxCheckBox;
    quGr: TPvQuery;
    dsquGr: TDataSource;
    quGrGoodsGroupID: TSmallintField;
    quGrID: TSmallintField;
    quGrName: TStringField;
    quSGGoodsGroupID: TSmallintField;
    quSGID: TSmallintField;
    quSGNameSG: TStringField;
    cxRadioGroup1: TcxRadioGroup;
    cxRadioGroup2: TcxRadioGroup;
    procedure cxButton10Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxCheckBox3PropertiesChange(Sender: TObject);
    procedure cxCheckBox4PropertiesChange(Sender: TObject);
    procedure cxCheckBox5PropertiesChange(Sender: TObject);
    procedure cxCheckBox6PropertiesChange(Sender: TObject);
    procedure cxLookupComboBox6PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriodR: TfmPeriodR;

implementation

uses MDB, Un1, Clients;

{$R *.dfm}

procedure TfmPeriodR.cxButton10Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPeriodR.FormCreate(Sender: TObject);
Var iGr:Integer;
begin
  iGr:=0;
  quGr.Active:=False; quGr.Active:=True; quGr.First;
  try
    quGr.Next; //���� ���� �� ������� ������ - ��������������
  except
  end;
  if quGr.RecordCount>0 then iGr:=quGrID.AsInteger;

  cxLookupComboBox6.EditValue:=quGrId.AsInteger;

  quSG.Active:=False;
  quSG.ParamByName('IDGR').AsInteger:=iGr;
  quSG.Active:=True;
  quSG.First;
  cxLookupComboBox3.EditValue:=quSGId.AsInteger;

  quBr.Active:=False; quBr.Active:=True; quBr.First;
  cxLookupComboBox4.EditValue:=quBrId.AsInteger;

  quCat.Active:=False; quCat.Active:=True; quCat.First;
  cxLookupComboBox5.EditValue:=quCatId.AsInteger;

  cxLabel3.Tag:=1;
  cxButtonEdit1.Tag:=0;
  cxButtonEdit1.EditValue:=0;
  cxButtonEdit1.Text:='';
  cxButtonEdit1.Properties.ReadOnly:=False;

end;

procedure TfmPeriodR.cxButton1Click(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    ObPar.Post:='';
    ObPar.iPostT:=0;
    ObPar.iPost:=0;
  end
  else
  begin
    ObPar.Post:=cxButtonEdit1.Text;
    ObPar.iPostT:=cxLabel3.Tag;
    ObPar.iPost:=cxButtonEdit1.Tag;
  end;
  if cxCheckBox3.Checked
  then
  begin
    ObPar.SGroup:='';
    ObPar.iSGroup:=0;
  end else
  begin
    ObPar.SGroup:=cxLookupComboBox3.Text;
    ObPar.iSGroup:=cxLookupComboBox3.EditValue;
    ObPar.SSGr:=fGetSlave(ObPar.iSGroup)+INtToStr(ObPar.iSGroup);
  end;
  if cxCheckBox6.Checked
  then
  begin
    ObPar.Group:='';
    ObPar.iGroup:=0;
  end else
  begin
    ObPar.Group:=cxLookupComboBox6.Text;
    ObPar.iGroup:=cxLookupComboBox6.EditValue;
    ObPar.SSGr:=fGetSlave(ObPar.iGroup)+INtToStr(ObPar.iGroup);
  end;

  if cxCheckBox4.Checked
  then
  begin
    ObPar.Brand:='';
    ObPar.iBrand:=0;
  end else
  begin
    ObPar.Brand:=cxLookupComboBox4.Text;
    ObPar.iBrand:=cxLookupComboBox4.EditValue;
  end;

  if cxCheckBox5.Checked
  then
  begin
    ObPar.Cat:='';
    ObPar.iCat:=0;
  end else
  begin
    ObPar.Cat:=cxLookupComboBox5.Text;
    ObPar.iCat:=cxLookupComboBox5.EditValue;
  end;

  ObPar.iTop:=cxRadioGroup1.ItemIndex;
  ObPar.iPrice:=cxRadioGroup2.ItemIndex;
end;

procedure TfmPeriodR.cxButtonEdit1PropertiesChange(Sender: TObject);
Var S:String;
begin
  if bOpen then exit;
  S:=cxButtonEdit1.Text;
  if Length(S)>2 then
  begin
    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount=1 then
      begin
        cxLabel3.Tag:=1;
        cxButtonEdit1.Tag:=dmMC.quFCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quFCliName.AsString;
        cxLookupComboBox6.SetFocus;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount=1 then
        begin
          cxLabel3.Tag:=2;
          cxButtonEdit1.Tag:=dmMC.quFIPCode.AsInteger;
          cxButtonEdit1.Text:=dmMC.quFIPName.AsString;
          cxLookupComboBox6.SetFocus;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;
end;

procedure TfmPeriodR.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S:String;
begin
  if fmclients.Showing then
  begin
    fmclients.Close;
    fmPeriodR.Close;
    fmPeriodR.Show;
  end;

  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  if cxLabel3.Tag<=1 then //����������
  begin
    fmClients.LevelCli.Active:=True;
    fmClients.LevelIP.Active:=False;
  end;
  if cxLabel3.Tag=2 then //���������������
  begin
    fmClients.LevelCli.Active:=False;
    fmClients.LevelIP.Active:=True;
  end;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;

  bOpen:=False;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      cxLabel3.Tag:=1;
      cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
      cxButtonEdit1.Text:=dmMC.quCliName.AsString;
    end else
    begin
      cxLabel3.Tag:=2;
      cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
      cxButtonEdit1.Text:=dmMC.quIPName.AsString;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;

  iDirect:=0;
end;

procedure TfmPeriodR.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then cxButtonEdit1.Enabled:=False
  else cxButtonEdit1.Enabled:=True;
end;

procedure TfmPeriodR.cxCheckBox3PropertiesChange(Sender: TObject);
begin
  if cxCheckBox3.Checked then
  begin
    cxLookupComboBox3.Enabled:=False;
    cxButton3.Enabled:=False;
  end else
  begin
    cxLookupComboBox3.Enabled:=True;
    cxButton3.Enabled:=True;
  end;
end;

procedure TfmPeriodR.cxCheckBox4PropertiesChange(Sender: TObject);
begin
  if cxCheckBox4.Checked then
  begin
    cxLookupComboBox4.Enabled:=False;
  end else
  begin
    cxLookupComboBox4.Enabled:=True;
  end;
end;

procedure TfmPeriodR.cxCheckBox5PropertiesChange(Sender: TObject);
begin
  if cxCheckBox5.Checked then
  begin
    cxLookupComboBox5.Enabled:=False;
  end else
  begin
    cxLookupComboBox5.Enabled:=True;
  end;
end;

procedure TfmPeriodR.cxCheckBox6PropertiesChange(Sender: TObject);
begin
  if cxCheckBox6.Checked then
  begin
    cxLookupComboBox6.Enabled:=False;
  end else
  begin
    cxLookupComboBox6.Enabled:=True;
  end;
end;

procedure TfmPeriodR.cxLookupComboBox6PropertiesChange(Sender: TObject);
begin
  quSG.Active:=False;
  quSG.ParamByName('IDGR').AsInteger:=cxLookupComboBox6.EditValue;
  quSG.Active:=True;
  quSG.First;
  cxLookupComboBox3.EditValue:=quSGId.AsInteger;
end;

end.
