object fmZadMessage: TfmZadMessage
  Left = 400
  Top = 280
  Width = 436
  Height = 342
  Caption = #1040#1082#1090#1080#1074#1085#1086#1077' '#1079#1072#1076#1072#1085#1080#1077
  Color = 12124017
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 20
    Width = 304
    Height = 20
    Caption = #1042#1085#1080#1084#1072#1085#1080#1077' , '#1077#1089#1090#1100' '#1072#1082#1090#1080#1074#1085#1086#1077' '#1079#1072#1076#1072#1085#1080#1077'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 188
    Top = 48
    Width = 32
    Height = 20
    Caption = #8470' 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 289
    Width = 428
    Height = 19
    Color = 7397120
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 232
    Width = 428
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 20
      Top = 12
      Width = 110
      Height = 35
      Caption = #1047#1072#1076#1072#1085#1080#1077' '#1087#1086#1083#1091#1095#1077#1085#1086
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 144
      Top = 12
      Width = 110
      Height = 35
      Caption = #1047#1072#1076#1072#1085#1080#1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1086
      ModalResult = 1
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 296
      Top = 12
      Width = 110
      Height = 35
      Caption = #1042#1099#1081#1090#1080
      ModalResult = 2
      TabOrder = 2
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 16
    Top = 76
    Width = 401
    Height = 141
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 385
      Height = 125
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
    end
  end
end
