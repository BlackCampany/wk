object fmAddMH: TfmAddMH
  Left = 43
  Top = 314
  BorderStyle = bsDialog
  Caption = 'fmAddMH'
  ClientHeight = 617
  ClientWidth = 512
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 96
    Width = 53
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '
    Transparent = True
  end
  object Label2: TLabel
    Left = 436
    Top = 244
    Width = 25
    Height = 13
    Caption = #1058#1080#1087' '#1091#1095#1077#1090#1085#1086#1081' '#1094#1077#1085#1099
    Transparent = True
    Visible = False
  end
  object Label3: TLabel
    Left = 24
    Top = 124
    Width = 22
    Height = 13
    Caption = 'GLN'
    Transparent = True
  end
  object Label5: TLabel
    Left = 184
    Top = 124
    Width = 24
    Height = 13
    Caption = #1048#1053#1053
  end
  object Label6: TLabel
    Left = 352
    Top = 124
    Width = 23
    Height = 13
    Caption = #1050#1055#1055
  end
  object Label7: TLabel
    Left = 24
    Top = 152
    Width = 89
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label8: TLabel
    Left = 24
    Top = 180
    Width = 49
    Height = 13
    Caption = #1058#1080#1087' '#1091#1095#1077#1090#1072
  end
  object Label9: TLabel
    Left = 236
    Top = 180
    Width = 130
    Height = 13
    Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090' '#1087#1088#1080' '#1087#1077#1088#1077#1089#1095#1077#1090#1077
  end
  object Label10: TLabel
    Left = 24
    Top = 212
    Width = 30
    Height = 13
    Caption = #1043#1086#1088#1086#1076
  end
  object Label11: TLabel
    Left = 24
    Top = 236
    Width = 32
    Height = 13
    Caption = #1059#1083#1080#1094#1072
  end
  object Label12: TLabel
    Left = 24
    Top = 260
    Width = 23
    Height = 13
    Caption = #1044#1086#1084
  end
  object Label13: TLabel
    Left = 160
    Top = 260
    Width = 35
    Height = 13
    Caption = #1082#1086#1088#1087#1091#1089
  end
  object Label14: TLabel
    Left = 24
    Top = 288
    Width = 89
    Height = 13
    Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089
  end
  object Label15: TLabel
    Left = 236
    Top = 288
    Width = 45
    Height = 13
    Caption = #1058#1077#1083#1077#1092#1086#1085
  end
  object Label16: TLabel
    Left = 24
    Top = 316
    Width = 50
    Height = 13
    Caption = #1044#1080#1088#1077#1082#1090#1086#1088
  end
  object Label17: TLabel
    Left = 32
    Top = 340
    Width = 49
    Height = 13
    Caption = #1060#1072#1084#1080#1083#1080#1103
  end
  object Label18: TLabel
    Left = 32
    Top = 364
    Width = 22
    Height = 13
    Caption = #1048#1084#1103
  end
  object Label19: TLabel
    Left = 32
    Top = 384
    Width = 47
    Height = 13
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
  end
  object Label20: TLabel
    Left = 256
    Top = 320
    Width = 68
    Height = 13
    Caption = #1043#1083'. '#1073#1091#1093#1075#1072#1083#1090#1077#1088
  end
  object Label21: TLabel
    Left = 260
    Top = 340
    Width = 49
    Height = 13
    Caption = #1060#1072#1084#1080#1083#1080#1103
  end
  object Label22: TLabel
    Left = 260
    Top = 364
    Width = 22
    Height = 13
    Caption = #1048#1084#1103
  end
  object Label23: TLabel
    Left = 260
    Top = 384
    Width = 47
    Height = 13
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
  end
  object Label24: TLabel
    Left = 20
    Top = 420
    Width = 50
    Height = 13
    Caption = #1051#1080#1094#1077#1085#1079#1080#1103
  end
  object Label25: TLabel
    Left = 84
    Top = 420
    Width = 30
    Height = 13
    Caption = #1089#1077#1088#1080#1103
  end
  object Label26: TLabel
    Left = 256
    Top = 420
    Width = 11
    Height = 13
    Caption = #8470
  end
  object Label27: TLabel
    Left = 20
    Top = 444
    Width = 62
    Height = 13
    Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085#1072
  end
  object Label28: TLabel
    Left = 20
    Top = 468
    Width = 66
    Height = 13
    Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
  end
  object Label29: TLabel
    Left = 244
    Top = 468
    Width = 101
    Height = 13
    Caption = #1076#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1072' '#1087#1086
  end
  object Label30: TLabel
    Left = 20
    Top = 508
    Width = 87
    Height = 13
    Caption = #1058#1080#1087' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 598
    Width = 512
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 543
    Width = 512
    Height = 55
    Align = alBottom
    BevelInner = bvLowered
    Color = 16762052
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 144
      Top = 16
      Width = 91
      Height = 29
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 292
      Top = 16
      Width = 91
      Height = 29
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 24
    Top = 16
    Width = 457
    Height = 57
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 281
      Height = 41
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 84
    Top = 92
    Properties.MaxLength = 150
    TabOrder = 3
    Text = 'cxTextEdit1'
    Width = 417
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 468
    Top = 240
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        FieldName = 'NAMEPRICE'
      end>
    Properties.ListSource = dmO.dsPriceTSel
    TabOrder = 4
    Visible = False
    Width = 17
  end
  object cxTextEdit2: TcxTextEdit
    Left = 52
    Top = 120
    Properties.MaxLength = 150
    TabOrder = 5
    Text = 'cxTextEdit2'
    Width = 117
  end
  object cxTextEdit3: TcxTextEdit
    Left = 212
    Top = 120
    TabOrder = 6
    Text = 'cxTextEdit3'
    Width = 121
  end
  object cxTextEdit4: TcxTextEdit
    Left = 380
    Top = 120
    TabOrder = 7
    Text = 'cxTextEdit4'
    Width = 121
  end
  object cxTextEdit5: TcxTextEdit
    Left = 124
    Top = 148
    TabOrder = 8
    Text = 'cxTextEdit5'
    Width = 377
  end
  object cxTextEdit6: TcxTextEdit
    Left = 68
    Top = 208
    TabOrder = 9
    Text = 'cxTextEdit6'
    Width = 153
  end
  object cxTextEdit7: TcxTextEdit
    Left = 68
    Top = 232
    TabOrder = 10
    Text = 'cxTextEdit7'
    Width = 305
  end
  object cxTextEdit8: TcxTextEdit
    Left = 68
    Top = 256
    TabOrder = 11
    Text = 'cxTextEdit8'
    Width = 77
  end
  object cxTextEdit9: TcxTextEdit
    Left = 204
    Top = 256
    TabOrder = 12
    Text = 'cxTextEdit9'
    Width = 61
  end
  object cxTextEdit10: TcxTextEdit
    Left = 120
    Top = 284
    TabOrder = 13
    Text = 'cxTextEdit10'
    Width = 85
  end
  object cxTextEdit11: TcxTextEdit
    Left = 288
    Top = 284
    TabOrder = 14
    Text = 'cxTextEdit11'
    Width = 105
  end
  object cxTextEdit12: TcxTextEdit
    Left = 96
    Top = 336
    TabOrder = 15
    Text = 'cxTextEdit12'
    Width = 149
  end
  object cxTextEdit13: TcxTextEdit
    Left = 96
    Top = 360
    TabOrder = 16
    Text = 'cxTextEdit13'
    Width = 149
  end
  object cxTextEdit14: TcxTextEdit
    Left = 96
    Top = 384
    TabOrder = 17
    Text = 'cxTextEdit14'
    Width = 149
  end
  object cxTextEdit15: TcxTextEdit
    Left = 316
    Top = 336
    TabOrder = 18
    Text = 'cxTextEdit15'
    Width = 149
  end
  object cxTextEdit16: TcxTextEdit
    Left = 316
    Top = 360
    TabOrder = 19
    Text = 'cxTextEdit16'
    Width = 149
  end
  object cxComboBox1: TcxComboBox
    Left = 92
    Top = 176
    Properties.Items.Strings = (
      #1074' '#1079#1072#1082'.'#1094#1077#1085#1072#1093' '#1089' '#1053#1044#1057
      #1074' '#1079#1072#1082'. '#1094#1077#1085#1072#1093' '#1073#1077#1079' '#1053#1044#1057)
    TabOrder = 20
    Text = #1074' '#1079#1072#1082'.'#1094#1077#1085#1072#1093' '#1089' '#1053#1044#1057
    Width = 121
  end
  object cxComboBox2: TcxComboBox
    Left = 380
    Top = 176
    Properties.Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16'
      '17'
      '18'
      '19'
      '20')
    TabOrder = 21
    Text = '0'
    Width = 61
  end
  object cxTextEdit17: TcxTextEdit
    Left = 316
    Top = 384
    TabOrder = 22
    Text = 'cxTextEdit17'
    Width = 149
  end
  object cxTextEdit18: TcxTextEdit
    Left = 124
    Top = 416
    TabOrder = 23
    Text = 'cxTextEdit18'
    Width = 121
  end
  object cxTextEdit19: TcxTextEdit
    Left = 276
    Top = 416
    TabOrder = 24
    Text = 'cxTextEdit19'
    Width = 129
  end
  object cxTextEdit20: TcxTextEdit
    Left = 108
    Top = 440
    TabOrder = 25
    Text = 'cxTextEdit20'
    Width = 357
  end
  object cxDateEdit1: TcxDateEdit
    Left = 108
    Top = 464
    TabOrder = 26
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 344
    Top = 464
    TabOrder = 27
    Width = 121
  end
  object cxComboBox3: TcxComboBox
    Left = 116
    Top = 504
    Properties.Items.Strings = (
      #1054#1054#1054', '#1047#1040#1054' ...'
      #1048#1055)
    TabOrder = 28
    Text = #1054#1054#1054', '#1047#1040#1054' ...'
    Width = 161
  end
end
