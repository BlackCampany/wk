object fmPreRealAlco: TfmPreRealAlco
  Left = 1038
  Top = 672
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1095#1077#1090#1072' '#1078#1091#1088#1085#1072#1083#1072' '#1087#1086' '#1072#1083#1082#1086#1075#1086#1083#1102'.'
  ClientHeight = 123
  ClientWidth = 577
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 16
    Width = 50
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
  end
  object Label1: TLabel
    Left = 196
    Top = 16
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Label3: TLabel
    Left = 344
    Top = 16
    Width = 72
    Height = 13
    Caption = #1074#1082#1083#1102#1095#1080#1090#1077#1083#1100#1085#1086
  end
  object Label4: TLabel
    Left = 12
    Top = 56
    Width = 31
    Height = 13
    Caption = #1054#1090#1076#1077#1083
  end
  object Panel1: TPanel
    Left = 459
    Top = 0
    Width = 118
    Height = 123
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxDateEdit1: TcxDateEdit
    Left = 68
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Width = 113
  end
  object cxDateEdit2: TcxDateEdit
    Left = 220
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Width = 109
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 68
    Top = 50
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Name'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = dsquDeps
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 3
    Width = 261
  end
  object quDeps: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "Depart"'
      'where Status1<>13'
      'Order by Name'
      '')
    Params = <>
    Left = 344
    Top = 60
    object quDepsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquDeps: TDataSource
    DataSet = quDeps
    Left = 392
    Top = 60
  end
end
