unit SyncCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, DB,
  dxmdaset, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridBandedTableView, cxGridDBBandedTableView, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ActnList, XPStyleActnCtrls,
  ActnMan, cxCheckBox, cxProgressBar, ComCtrls;

type
  TfmSyncCash = class(TForm)
    Panel1: TPanel;
    meC: TdxMemData;
    meCCashNum: TIntegerField;
    meCCashName: TStringField;
    meCCashStart: TSmallintField;
    meCPosCards: TIntegerField;
    meCPosBar: TIntegerField;
    meCPosDisc: TIntegerField;
    meCPosClass: TIntegerField;
    meCPosPers: TIntegerField;
    Panel2: TPanel;
    Memo1: TcxMemo;
    LevelSyncCash: TcxGridLevel;
    GridSyncCash: TcxGrid;
    ViewSyncCash: TcxGridDBBandedTableView;
    dsMeC: TDataSource;
    ViewSyncCashCashNum: TcxGridDBBandedColumn;
    ViewSyncCashCashName: TcxGridDBBandedColumn;
    ViewSyncCashCashStart: TcxGridDBBandedColumn;
    ViewSyncCashPosCards: TcxGridDBBandedColumn;
    ViewSyncCashPosDisc: TcxGridDBBandedColumn;
    ViewSyncCashPosClass: TcxGridDBBandedColumn;
    ViewSyncCashPosPers: TcxGridDBBandedColumn;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    acSyncCash: TActionManager;
    acExit: TAction;
    meCCashPath: TStringField;
    ViewSyncCashCashPath: TcxGridDBBandedColumn;
    cxButton3: TcxButton;
    StatusBar1: TStatusBar;
    ViewSyncCashPosBar: TcxGridDBBandedColumn;
    teBar1: TdxMemData;
    teBar1Bar: TStringField;
    teBar1Articul: TIntegerField;
    teBar1CardSize: TStringField;
    teBar1Quant: TFloatField;
    teBar1Price: TFloatField;
    teBar: TdxMemData;
    teBarBar: TStringField;
    teBarArticul: TIntegerField;
    teBarCardSize: TStringField;
    teBarQuant: TFloatField;
    teBarPrice: TFloatField;
    teDS1: TdxMemData;
    teDS1Articul: TIntegerField;
    teDS1Comment: TStringField;
    teDS: TdxMemData;
    teDSArticul: TIntegerField;
    teDSComment: TStringField;
    teClass1: TdxMemData;
    teClass1Id: TIntegerField;
    teClass1IdParent: TIntegerField;
    teClass1Name: TStringField;
    teClass0: TdxMemData;
    teClass0Id: TIntegerField;
    teClass0IdParent: TIntegerField;
    teClass0Name: TStringField;
    acSetPrice: TAction;
    cxButton4: TcxButton;
    teCards1: TdxMemData;
    teCards1Articul: TIntegerField;
    teCards1Classif: TIntegerField;
    teCards1Depart: TIntegerField;
    teCards1Name: TStringField;
    teCards1Card_type: TSmallintField;
    teCards1Messuriment: TSmallintField;
    teCards1Price: TFloatField;
    teCards1Discquant: TFloatField;
    teCards1Discount: TFloatField;
    teCards1Scale: TStringField;
    teCards1AVid: TIntegerField;
    teCards1Krep: TFloatField;
    teCards: TdxMemData;
    teCardsArticul: TIntegerField;
    teCardsClassif: TIntegerField;
    teCardsDepart: TIntegerField;
    teCardsName: TStringField;
    teCardsCard_type: TSmallintField;
    teCardsMessuriment: TSmallintField;
    teCardsPrice: TFloatField;
    teCardsDiscquant: TFloatField;
    teCardsDiscount: TFloatField;
    teCardsScale: TStringField;
    teCardsAVid: TIntegerField;
    teCardsKrep: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSyncCash: TfmSyncCash;
  bStop:Boolean = False;
  bProc:Boolean = False;

implementation

uses MFB, MT, Un1, u2fdk, MDB;

{$R *.dfm}

procedure TfmSyncCash.FormCreate(Sender: TObject);
begin
  Memo1.Align:=AlClient;
end;

procedure TfmSyncCash.acExitExecute(Sender: TObject);
begin
  if bProc then
  begin

  end else Close;
end;

procedure TfmSyncCash.cxButton1Click(Sender: TObject);

var iStep,iC,iM,iCode,iEdit,iAdd,iDel,iFind:INteger;
    sPre,sCode,sBar:String;

  procedure wm(S:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(formatdatetime('hh:nn:ss  ',now)+S);
  end;

begin
  bProc:=True;
  bStop:=False;
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  cxButton4.Enabled:=False;
  cxButton3.Enabled:=True;

  meC.First;
  while not meC.Eof do
  begin
    meC.Edit;
    meCPosCards.AsInteger:=0;
    meCPosBar.AsInteger:=0;
    meCPosDisc.AsInteger:=0;
    meCPosClass.AsInteger:=0;
    meCPosPers.AsInteger:=0;
    meC.Post;

    meC.Next;
  end;


  Memo1.Clear;
  wm('������ ������������� ������.');

  CloseTe(teCards);
  CloseTe(teBar);
  CloseTe(teDS);
  CloseTe(teClass0);

  meC.First;
  while not meC.Eof do
  begin
    if meCCashStart.AsInteger=1 then
    begin
      wm('  '+meCCashName.AsString+'- ������.');
      with dmFB do
      with dmMT do
      begin
        try
          Casher.Close;
          Casher.DBName:=meCCashPath.AsString;
          Casher.Open;
        except
          wm('     ������ �������� �� - '+meCCashPath.AsString);
        end;
        if Casher.Connected then
        begin
          wm('     ���� ��');

          if taCards.Active=False then taCards.Active:=True
          else taCards.Refresh;

          if ptPluLim.Active=False then ptPluLim.Active:=True
          else ptPluLim.Refresh;

          if ptBar.Active=False then ptBar.Active:=True
          else ptBar.Refresh;
          ptBar.IndexFieldNames:='GoodsID';

          if ptSGr.Active=False then ptSGr.Active:=True
          else ptSGr.Refresh;


          if teCards.RecordCount=0 then  //���� ��� �������� ������ � ������ , ����� ��� � ���������
          begin
            wm('    ���������� ������.');

            StatusBar1.Panels[1].Text:='';
            StatusBar1.Panels[0].Text:=its(taCards.RecordCount);
            iC:=0;  iM:=0;
           
            taCards.First;
            while not taCards.Eof do
            begin
              if (taCardsStatus.AsInteger<100)and(taCardsID.AsInteger>0)  then
              begin
                sPre:='';
                if ptPluLim.FindKey([taCardsID.AsInteger]) then
                  if ptPluLimPercent.AsFloat>99 then
                  begin
                    sPre:='-';
                    teDS.Append;
                    teDSArticul.AsInteger:=taCardsID.AsInteger;
                    teDSComment.AsString:='no discount';
                    teDS.Post;
                  end;

                teCards.Append;
                teCardsArticul.AsInteger:=taCardsID.AsInteger;
                teCardsClassif.AsInteger:=taCardsSubGroupID.AsInteger;
                teCardsDepart.AsInteger:=0;
                teCardsName.AsString:=sPre+OemToAnsiConvert(taCardsFullName.AsString);
                teCardsCARD_TYPE.AsInteger:=taCardsTovarType.AsInteger;
                teCardsMessuriment.AsInteger:=taCardsEdIzm.AsInteger;
                teCardsPrice.AsFloat:=rv(taCardsCena.AsFloat);
                teCardsDiscquant.AsFloat:=0;
                teCardsDiscount.AsFloat:=0;
                teCardsSCALE.AsString:='NOSIZE';
                teCards.Post;

                ptBar.CancelRange;
                ptBar.SetRange([taCardsID.AsInteger],[taCardsID.AsInteger]);
                ptBar.First;
                while not ptBar.Eof do
                begin
                  try
                    teBar.Append;
                    teBarBar.AsString:=trim(ptBarID.AsString);
                    teBarArticul.AsInteger:=taCardsID.AsInteger;
                    if ptBarBarStatus.AsInteger=0 then  teBarCardSize.AsString:='NOSIZE' else teBarCardSize.AsString:='QUANTITY';
                    teBarQuant.AsFloat:=ptBarQuant.AsFloat;
                    teBarPrice.AsFloat:=0;
                    teBar.Post;
                  except
                    teBar.Cancel;
                  end;

                  ptBar.Next;
                end;


                inc(iM);
              end;

              taCards.Next; inc(iC);
              if iC mod 100 = 0 then
              begin
                StatusBar1.Panels[1].Text:=its(iC)+'    ���. - '+its(iM);
                delay(10);
//                if iC>10000 then break;
              end;

              if bStop then Break;
            end;

            ptSGr.First;
            while not ptSGr.Eof do
            begin
              teClass0.Append;
              teClass0Id.AsInteger:=ptSGrID.AsInteger;
              teClass0IdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
              teClass0Name.AsString:=OemToAnsiConvert(ptSGrName.AsString);
              teClass0.Post;

              ptSGr.Next;
            end;


            wm('    ���������� Ok ');
          end;

          wm('    ���������� ������ ��� �����.');

          if bStop then exit;

          closete(teCards1);
          closete(teBar1);
          closete(teDS1);
          closete(teClass1);

          teCards.First;
          while not teCards.Eof do
          begin
            teCards1.Append;
            teCards1Articul.AsInteger:=teCardsArticul.AsInteger;
            teCards1Classif.AsInteger:=teCardsClassif.AsInteger;
            teCards1Depart.AsInteger:=0;
            teCards1Name.AsString:=teCardsName.AsString;
            teCards1CARD_TYPE.AsInteger:=teCardsCARD_TYPE.AsInteger;
            teCards1Messuriment.AsInteger:=teCardsMessuriment.AsInteger;
            teCards1Price.AsFloat:=teCardsPrice.AsFloat;
            teCards1Discquant.AsFloat:=0;
            teCards1Discount.AsFloat:=0;
            teCards1SCALE.AsString:='NOSIZE';
            teCards1.Post;

            teCards.Next;
          end;

          teBar.First;
          while not teBar.Eof do
          begin
            teBar1.Append;
            teBar1Bar.AsString:=teBarBar.AsString;
            teBar1Articul.AsInteger:=teBarArticul.AsInteger;
            teBar1CardSize.AsString:=teBarCardSize.AsString;
            teBar1Quant.AsFloat:=teBarQuant.AsFloat;
            teBar1Price.AsFloat:=teBarPrice.AsFloat;
            teBar1.Post;

            teBar.Next;
          end;

          teDS.First;
          while not teDS.Eof do
          begin
            teDS1.Append;
            teDS1Articul.AsInteger:=teDSArticul.AsInteger;
            teDS1Comment.AsString:=teDSComment.AsString;
            teDS1.Post;

            teDS.Next;
          end;

          teClass0.First;
          while not teClass0.Eof do
          begin
            teClass1.Append;
            teClass1Id.AsInteger:=teClass0Id.AsInteger;
            teClass1IdParent.AsInteger:=teClass0IdParent.AsInteger;
            teClass1Name.AsString:=teClass0Name.AsString;
            teClass1.Post;

            teClass0.Next;
          end;

//---------------------------------------------------------------------------------------

          wm('     �������� ��������.('+its(teCards.RecordCount)+' ,1- '+its(teCards1.RecordCount)+')'); delay(10);

          teCards1.First;
          while not teCards1.Eof do
            if teCards1Articul.AsInteger=0 then teCards1.Delete else teCards1.Next;

          wm('       �������� �� 0  ���. '+its(teCards.RecordCount)+'  ���. '+its(teCards1.RecordCount)); delay(10);

          wm('       - ������ ��������.'); delay(10);
          taCardsFB.Open;

          iStep:=taCardsFB.RecordCount div 100;
          if iStep=0 then iStep:=1;
          iC:=0;   iEdit:=0; iAdd:=0; iDel:=0; iFind:=0;

          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(taCardsFB.RecordCount);

          taCardsFB.First;

          trUpd1.StartTransaction;

          while not taCardsFB.Eof do
          begin
            iCode:=StrToIntDef(taCardsFBARTICUL.AsString,0);

            if iCode>0 then
            begin
              if teCards1.Locate('Articul',iCode,[]) then
              begin
                inc(iFind);

                if (trim(teCards1Name.AsString)<>trim(taCardsFBNAME.AsString))
                    or (abs(teCards1Price.AsFloat-taCardsFBPRICE_RUB.AsFloat)>0.001)
                    or (taCardsFBCLASSIF.AsInteger<>teCards1Classif.AsInteger)
                then
                begin

                  taCardsFB.Edit;
                  taCardsFBCLASSIF.AsInteger:=teCards1Classif.AsInteger;
                  taCardsFBNAME.AsString:=teCards1Name.AsString;
                  taCardsFBCARD_TYPE.AsInteger:=teCards1Card_type.AsInteger+1;
                  if teCards1Messuriment.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
                  taCardsFBPRICE_RUB.AsFloat:=teCards1Price.AsFloat;
                  taCardsFBDISCQUANT.AsFloat:=0;
                  taCardsFBDISCOUNT.AsFloat:=0;
                  taCardsFBSCALE.AsString:='NOSIZE';
                  taCardsFB.Post;

                  inc(iEdit);
                end;

                teCards1.Delete; //���� ����� ��������

                taCardsFB.Next;
              end else
              begin
                taCardsFB.Delete;

                inc(iDel);
              end;
            end else
            begin
              taCardsFB.Delete;

              inc(iDel);
            end;

            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
            if iC mod iStep = 0 then
            begin
              meC.Edit;
              meCPosCards.AsInteger:=(iC div iStep);
              meC.Post;
              delay(10);

              trUpd1.Commit;
              delay(33);
              trUpd1.StartTransaction;
            end;

            if bStop then break;
          end;

          trUpd1.Commit;

          meC.Edit;
          meCPosCards.AsInteger:=100;
          meC.Post;
          delay(10);

          wm(' '); delay(10);
          wm('        ������� - '+its(iFind)); delay(10);
          wm('        �������� - '+its(iEdit)); delay(10);
          wm('        ������� - '+its(iDel)); delay(10);
          wm(' '); delay(10);

          iC:=0;

          wm('       - �������� ��������.'); delay(10);
          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(teCards1.RecordCount);

          trUpd1.StartTransaction;

          teCards1.First;
          while not teCards1.Eof do //�������� �������� ������� ��� � ����� - ���������
          begin
            sCode:=its(teCards1Articul.AsInteger);

            if taCardsFB.Locate('ARTICUL',sCode,[])=False then
            begin
              taCardsFB.Append;
              taCardsFBARTICUL.AsString:=its(teCards1Articul.AsInteger);
              taCardsFBCLASSIF.AsInteger:=teCards1Classif.AsInteger;
              taCardsFBNAME.AsString:=teCards1Name.AsString;
              taCardsFBCARD_TYPE.AsInteger:=teCards1Card_type.AsInteger+1;
              if teCards1Messuriment.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
              taCardsFBPRICE_RUB.AsFloat:=teCards1Price.AsFloat;
              taCardsFBDISCQUANT.AsFloat:=0;
              taCardsFBDISCOUNT.AsFloat:=0;
              taCardsFBSCALE.AsString:='NOSIZE';
              taCardsFB.Post;
            end else
            begin
              wm('         '+sCode+' ��� ���� � ���� ������ ��'); delay(10);
            end;

            inc(iC);
            inc(iAdd);

            teCards1.Delete;

            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
          end;

          trUpd1.Commit;

          taCardsFB.Close;
          wm(' '); delay(10);
//          wm('        ������� - '+its(iFind)); delay(10);
//          wm('        �������� - '+its(iEdit)); delay(10);
//          wm('        ������� - '+its(iDel)); delay(10);
          wm('        ��������� - '+its(iAdd)); delay(10);
          wm(' '); delay(10);
          wm('     �������� �������� ���������.'); delay(10);
          wm(' '); delay(10);

//---------------------------------------------------------------------------------------

          wm('     �������� ��.('+its(teBar.RecordCount)+'   ,1- '+its(teBar1.RecordCount)+')'); delay(10);

          wm('       - ������ ��������.'); delay(10);
          taBarFB.Open;

          iStep:=taBarFB.RecordCount div 100;
          if iStep=0 then iStep:=1;
          iC:=0;   iEdit:=0; iAdd:=0; iDel:=0; iFind:=0;

          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(taBarFB.RecordCount);

          taBarFB.First;

          trUpd2.StartTransaction;

          while not taBarFB.Eof do
          begin
            sBar:=Trim(taBarFBBARCODE.AsString);


            if teBar1.Locate('Bar',sBar,[]) then
            begin
              inc(iFind);

              if (its(teBar1Articul.AsInteger)<>trim(taBarFBCARDARTICUL.AsString))
                  or (teBar1CardSize.AsString<>taBarFBCARDSIZE.AsString)
                  or (abs(teBar1Quant.AsFloat-taBarFBQUANTITY.AsFloat)>0.0001)
              then
              begin
                taBarFB.Edit;
                taBarFBCARDARTICUL.AsString:=its(teBar1Articul.AsInteger);
                taBarFBCARDSIZE.AsString:=teBar1CardSize.AsString;
                taBarFBQUANTITY.AsFloat:=teBar1Quant.AsFloat;
                taBarFBPRICERUB.AsFloat:=teBar1Price.AsFloat;
                taBarFB.Post;

                inc(iEdit);
              end;

              teBar1.Delete; //���� ����� ��������

              taBarFB.Next;
            end else
            begin
              taBarFB.Delete;
              inc(iDel);
            end;

            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
            if iC mod iStep = 0 then
            begin
              meC.Edit;
              meCPosBar.AsInteger:=(iC div iStep);
              meC.Post;
              delay(10);

              trUpd2.Commit;
              delay(33);
              trUpd2.StartTransaction;
            end;

            if bStop then break;
          end;

          trUpd2.Commit;

          meC.Edit;
          meCPosBar.AsInteger:=100;
          meC.Post;
          delay(10);

          wm(' '); delay(10);
          wm('        ������� - '+its(iFind)); delay(10);
          wm('        �������� - '+its(iEdit)); delay(10);
          wm('        ������� - '+its(iDel)); delay(10);
          wm(' '); delay(10);

          iC:=0;

          wm('       - �������� ��������.'); delay(10);
          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(teBar1.RecordCount);

          trUpd2.StartTransaction;

          teBar1.First;
          while not teBar1.Eof do //�������� �������� ������� ��� � ����� - ���������
          begin
            sBar:=trim(teBar1Bar.AsString);
            if taBarFB.Locate('BARCODE',sBar,[])=False then
            begin
              taBarFB.Append;
              taBarFBBARCODE.AsString:=sBar;
              taBarFBCARDARTICUL.AsString:=its(teBar1Articul.AsInteger);
              taBarFBCARDSIZE.AsString:=teBar1CardSize.AsString;
              taBarFBQUANTITY.AsFloat:=teBar1Quant.AsFloat;
              taBarFBPRICERUB.AsFloat:=teBar1Price.AsFloat;
              taBarFB.Post;
            end else
            begin
              wm('         '+sBar+' ��� ���� � ���� ������ ��'); delay(10);
            end;

            inc(iC);
            inc(iAdd);

            teBar1.Delete;

            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
          end;

          trUpd2.Commit;

          taBarFB.Close;
          wm(' '); delay(10);
//          wm('        ������� - '+its(iFind)); delay(10);
//          wm('        �������� - '+its(iEdit)); delay(10);
//          wm('        ������� - '+its(iDel)); delay(10);
          wm('        ��������� - '+its(iAdd)); delay(10);
          wm(' '); delay(10);
          wm('     �������� �� ���������.'); delay(10);
          wm(' '); delay(10);

//---------------------------------------------------------------------------------------

          wm('     �������� ����-������.('+its(teDS.RecordCount)+'   ,1- '+its(teDS1.RecordCount)+')'); delay(10);

          wm('       - ������ ��������.'); delay(10);
          taDCSTOPFB.Open;

          iStep:=taDCSTOPFB.RecordCount div 100;
          if iStep=0 then iStep:=1;
          iC:=0;   iEdit:=0; iAdd:=0; iDel:=0; iFind:=0;

          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(taDCSTOPFB.RecordCount);

          taDCSTOPFB.First;

          trUpd3.StartTransaction;

          while not taDCSTOPFB.Eof do
          begin
            iCode:=StrToINtDef(taDCSTOPFBSART.AsString,0);

            if iCode>0 then
            begin
              if teDS1.Locate('Articul',iCode,[]) then
              begin
                inc(iFind);

                taDCSTOPFB.Edit;
                taDCSTOPFBPROC.AsFloat:=100;
                taDCSTOPFB.Post;

                teDS1.Delete; //���� ����� ��������

                taDCSTOPFB.Next;
              end else
              begin
                taDCSTOPFB.Delete;
                inc(iDel);
              end;
            end else
            begin
              wm('         - ������ �������������� '+taDCSTOPFBSART.AsString); delay(10);
              taDCSTOPFB.Next;
            end;

            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
            if iC mod iStep = 0 then
            begin
              meC.Edit;
              meCPosDisc.AsInteger:=(iC div iStep);
              meC.Post;
              delay(10);

              trUpd3.Commit;
              delay(33);
              trUpd3.StartTransaction;
            end;

            if bStop then break;
          end;

          trUpd3.Commit;

          meC.Edit;
          meCPosDisc.AsInteger:=100;
          meC.Post;
          delay(10);

          wm(' '); delay(10);
          wm('        ������� - '+its(iFind)); delay(10);
          wm('        �������� - '+its(iEdit)); delay(10);
          wm('        ������� - '+its(iDel)); delay(10);
          wm(' '); delay(10);

          iC:=0;

          wm('       - �������� ��������.'); delay(10);
          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(teDS1.RecordCount);

          trUpd3.StartTransaction;

          teDS1.First;
          while not teDS1.Eof do //�������� �������� ������� ��� � ����� - ���������
          begin
            iCode:=teDS1Articul.AsInteger;
            if taDCSTOPFB.Locate('SART',its(iCode),[])=False then
            begin
              taDCSTOPFB.Append;
              taDCSTOPFBSART.AsString:=its(iCode);
              taDCSTOPFBPROC.AsFloat:=100;
              taDCSTOPFB.Post;
            end else
            begin
              wm('         '+its(iCode)+' ��� ���� � ���� ������ ��'); delay(10);
            end;

            inc(iC);
            inc(iAdd);

            teDS1.Delete;

            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
          end;

          trUpd3.Commit;

          taDCSTOPFB.Close;
          wm(' '); delay(10);
//          wm('        ������� - '+its(iFind)); delay(10);
//          wm('        �������� - '+its(iEdit)); delay(10);
//          wm('        ������� - '+its(iDel)); delay(10);
          wm('        ��������� - '+its(iAdd)); delay(10);
          wm(' '); delay(10);
          wm('     �������� ����-������ ���������.'); delay(10);
          wm(' '); delay(10);

//---------------------------------------------------------------------------------------
          wm('     �������� ��������������.('+its(teClass0.RecordCount)+'   ,1- '+its(teClass1.RecordCount)+')'); delay(10);

          wm('       - ������ ��������.'); delay(10);
          taClassFB.Open;

          iStep:=taClassFB.RecordCount div 100;
          if iStep=0 then iStep:=1;
          iC:=0;   iEdit:=0; iAdd:=0; iDel:=0; iFind:=0;

          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(taClassFB.RecordCount);

          taClassFB.First;

          trUpd1.StartTransaction;

          while not taClassFB.Eof do
          begin
            iCode:=taClassFBID.AsInteger;


            if teClass1.Locate('Id',iCode,[]) then
            begin
              inc(iFind);

              if trim(teClass1Name.AsString)<>trim(taClassFBNAME.AsString)
              then
              begin
                taClassFB.Edit;
                taClassFBTYPE_CLASSIF.AsInteger:=1;
//                taClassFBID.AsInteger:=teClass1Id.AsInteger;
                taClassFBIACTIVE.AsInteger:=1;
                taClassFBID_PARENT.AsInteger:=teClass1IdParent.AsInteger;
                taClassFBNAME.AsString:=teClass1Name.AsString;
                taClassFBIEDIT.AsInteger:=1;
                taClassFBDISCSTOP.AsInteger:=0;
                taClassFB.Post;

                inc(iEdit);
              end;

              teClass1.Delete; //���� ����� ��������

              taClassFB.Next;
            end else
            begin
              taClassFB.Delete;
              inc(iDel);
            end;

            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
            if iC mod iStep = 0 then
            begin
              meC.Edit;
              meCPosClass.AsInteger:=(iC div iStep);
              meC.Post;
              delay(10);

              trUpd1.Commit;
              delay(33);
              trUpd1.StartTransaction;
            end;

            if bStop then break;
          end;

          trUpd1.Commit;

          meC.Edit;
          meCPosClass.AsInteger:=100;
          meC.Post;
          delay(10);

          wm(' '); delay(10);
          wm('        ������� - '+its(iFind)); delay(10);
          wm('        �������� - '+its(iEdit)); delay(10);
          wm('        ������� - '+its(iDel)); delay(10);
          wm(' '); delay(10);

          iC:=0;

          wm('       - �������� ��������.'); delay(10);
          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(teClass1.RecordCount);

          trUpd1.StartTransaction;

          teClass1.First;
          while not teClass1.Eof do //�������� �������� ������� ��� � ����� - ���������
          begin
            iCode:=teClass1Id.AsInteger;
            if taClassFB.Locate('ID',iCode,[])=False then
            begin
              taClassFB.Append;
              taClassFBTYPE_CLASSIF.AsInteger:=1;
              taClassFBID.AsInteger:=teClass1Id.AsInteger;
              taClassFBIACTIVE.AsInteger:=1;
              taClassFBID_PARENT.AsInteger:=teClass1IdParent.AsInteger;
              taClassFBNAME.AsString:=teClass1Name.AsString;
              taClassFBIEDIT.AsInteger:=1;
              taClassFBDISCSTOP.AsInteger:=0;
              taClassFB.Post;
            end else
            begin
              wm('         '+its(iCode)+' ��� ���� � ���� ������ ��'); delay(10);
            end;

            inc(iC);
            inc(iAdd);

            teClass1.Delete;

            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
          end;

          trUpd1.Commit;

          taClassFB.Close;
          wm(' '); delay(10);
          wm('        ��������� - '+its(iAdd)); delay(10);
          wm(' '); delay(10);
          wm('     �������� �������������� ���������.'); delay(10);
          wm(' '); delay(10);

        end else  wm('     ���� ����������');
        Casher.Close;
      end;
      wm('     '+meCCashName.AsString+'- �����.');
    end else wm('  '+meCCashName.AsString+'- �������.');

    meC.Next;
  end;

  wm('������������� ������ ���������.');

  teCards.Close;
  teCards1.Close;
  teBar.Close;
  teBar1.Close;
  teDS.Close;
  teDS1.Close;
  teClass0.Close;
  teClass1.Close;
  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
  cxButton4.Enabled:=True;

  cxButton3.Enabled:=False;
  bProc:=False;
end;

procedure TfmSyncCash.cxButton3Click(Sender: TObject);
begin
  if bStop=False then bStop:=True;
end;

procedure TfmSyncCash.acSetPriceExecute(Sender: TObject);
Var iCode:INteger;
    iC:Integer;

  procedure wm(S:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(formatdatetime('hh:nn:ss  ',now)+S);
  end;

begin
  if MessageDlg('������� ���� � ���������� � '+its(meCCashNum.AsInteger)+' �����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    wm('  '+meCCashName.AsString+'- ������.');
    with dmFB do
    with dmMT do
    begin
      try
        Casher.Close;
        Casher.DBName:=meCCashPath.AsString;
        Casher.Open;
      except
        wm('     ������ �������� �� - '+meCCashPath.AsString);
      end;
      if Casher.Connected then
      begin
        wm('     ���� ��');

        if taCards.Active=False then taCards.Active:=True
        else taCards.Refresh;

        StatusBar1.Panels[1].Text:='';
        delay(10);

        taCardsFB.Open;
        taCardsFB.First;
        iC:=0;

        wm('       ����� ...');

        while not taCardsFB.Eof do
        begin
          iCode:=StrToINtDef(taCardsFBARTICUL.AsString,0);
          if iCode>0 then
          begin
            if taCards.FindKey([iCode]) then
            begin
              taCards.Edit;
              taCardsCena.AsFloat:=taCardsFBPRICE_RUB.AsFloat;
              taCards.Post;

              inc(iC);
            end;
          end;

          if iC mod 100 = 0 then
          begin
            StatusBar1.Panels[1].Text:=its(iC);
            delay(10);
          end;

          taCardsFB.Next;
        end;

        taCardsFB.Close;

        wm('     �����');
      end;
    end;
  end;
end;

procedure TfmSyncCash.cxButton4Click(Sender: TObject);
var iStep,iC,iM,iCode,iEdit,iAdd,iDel,iFind:INteger;
    sPre,sCode,sBar:String;

  procedure wm(S:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(formatdatetime('hh:nn:ss  ',now)+S);
  end;
begin
  bProc:=True;
  bStop:=False;
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  cxButton4.Enabled:=False;
  cxButton3.Enabled:=True;

  meC.First;
  while not meC.Eof do
  begin
    meC.Edit;
    meCPosCards.AsInteger:=0;
    meCPosBar.AsInteger:=0;
    meCPosDisc.AsInteger:=0;
    meCPosClass.AsInteger:=0;
    meCPosPers.AsInteger:=0;
    meC.Post;

    meC.Next;
  end;

  Memo1.Clear;
  wm('������ ������������� ������.');

  CloseTe(teCards);

  meC.First;
  while not meC.Eof do
  begin
    if meCCashStart.AsInteger=1 then
    begin
      wm('  '+meCCashName.AsString+'- ������.');
      with dmFB do
      with dmMT do
      with dmMC do
      begin
        try
          Casher.Close;
          Casher.DBName:=meCCashPath.AsString;
          Casher.Open;
        except
          wm('     ������ �������� �� - '+meCCashPath.AsString);
        end;
        if Casher.Connected then
        begin
          wm('     ���� ��');

          if taCards.Active=False then taCards.Active:=True
          else taCards.Refresh;

          if ptPluLim.Active=False then ptPluLim.Active:=True
          else ptPluLim.Refresh;

          if teCards.RecordCount=0 then  //���� ��� �������� ������ � ������ , ����� ��� � ���������
          begin
            wm('    ���������� ������ ����������� �������.');

            quGetAlcoList.Active:=False;
            quGetAlcoList.Active:=True;

            StatusBar1.Panels[1].Text:='';
            StatusBar1.Panels[0].Text:=its(quGetAlcoList.RecordCount);
            iC:=0;  iM:=0;

            quGetAlcoList.First;
            while not quGetAlcoList.Eof do
            begin
              if taCards.FindKey([quGetAlcoListId.AsInteger]) then
              begin
                if (taCardsCena.AsFloat>0)and(taCardsStatus.AsInteger<100) then
                begin
                  sPre:='';
                  if ptPluLim.FindKey([taCardsID.AsInteger]) then
                    if ptPluLimPercent.AsFloat>99 then sPre:='-';

                  teCards.Append;
                  teCardsArticul.AsInteger:=taCardsID.AsInteger;
                  teCardsClassif.AsInteger:=taCardsSubGroupID.AsInteger;
                  teCardsDepart.AsInteger:=0;
                  teCardsName.AsString:=sPre+OemToAnsiConvert(taCardsFullName.AsString);
                  teCardsCARD_TYPE.AsInteger:=taCardsTovarType.AsInteger;
                  teCardsMessuriment.AsInteger:=taCardsEdIzm.AsInteger;
                  teCardsPrice.AsFloat:=rv(taCardsCena.AsFloat);
                  teCardsDiscquant.AsFloat:=0;
                  teCardsDiscount.AsFloat:=0;
                  teCardsSCALE.AsString:='NOSIZE';
                  teCardsAVid.AsInteger:=taCardsV11.AsInteger;
                  teCardsKrep.AsFloat:=0;
                  teCards.Post;

                  inc(iM);
                end;
              end;

              quGetAlcoList.Next;  inc(iC);
              if iC mod 100 = 0 then
              begin
                StatusBar1.Panels[1].Text:=its(iC)+'    ���. - '+its(iM);
                delay(10);
//                if iC>10000 then break;
              end;

              if bStop then Break;
            end;

            wm('    ���������� Ok ');
          end;

          wm('    ���������� ������ ��� �����.');

          if bStop then exit;

          closete(teCards1);

          teCards.First;
          while not teCards.Eof do
          begin
            teCards1.Append;
            teCards1Articul.AsInteger:=teCardsArticul.AsInteger;
            teCards1Classif.AsInteger:=teCardsClassif.AsInteger;
            teCards1Depart.AsInteger:=0;
            teCards1Name.AsString:=teCardsName.AsString;
            teCards1CARD_TYPE.AsInteger:=teCardsCARD_TYPE.AsInteger;
            teCards1Messuriment.AsInteger:=teCardsMessuriment.AsInteger;
            teCards1Price.AsFloat:=teCardsPrice.AsFloat;
            teCards1Discquant.AsFloat:=0;
            teCards1Discount.AsFloat:=0;
            teCards1SCALE.AsString:='NOSIZE';
            teCards1AVid.AsInteger:=teCardsAVid.AsInteger;
            teCards1Krep.AsFloat:=teCardsKrep.AsFloat;
            teCards1.Post;

            teCards.Next;
          end;

//---------------------------------------------------------------------------------------

          wm('     �������� ��������.('+its(teCards.RecordCount)+' ,1- '+its(teCards1.RecordCount)+')'); delay(10);

          teCards1.First;
          while not teCards1.Eof do
            if teCards1Articul.AsInteger=0 then teCards1.Delete else teCards1.Next;

          wm('       �������� �� 0  ���. '+its(teCards.RecordCount)+'  ���. '+its(teCards1.RecordCount)); delay(10);

          wm('       - ������ ��������.'); delay(10);
          taCardsFB.Open;

          iStep:=taCardsFB.RecordCount div 100;
          if iStep=0 then iStep:=1;
          iC:=0;   iEdit:=0; iAdd:=0; iDel:=0; iFind:=0;

          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(taCardsFB.RecordCount);

          taCardsFB.First;

          trUpd1.StartTransaction;

          while not taCardsFB.Eof do
          begin
            iCode:=StrToIntDef(taCardsFBARTICUL.AsString,0);

            if iCode>0 then
            begin
              if teCards1.Locate('Articul',iCode,[]) then
              begin
                inc(iFind);

                taCardsFB.Edit;
                taCardsFBCLASSIF.AsInteger:=teCards1Classif.AsInteger;
                taCardsFBNAME.AsString:=teCards1Name.AsString;
                taCardsFBCARD_TYPE.AsInteger:=teCards1Card_type.AsInteger+1;
                if teCards1Messuriment.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
                taCardsFBPRICE_RUB.AsFloat:=teCards1Price.AsFloat;
                taCardsFBDISCQUANT.AsFloat:=0;
                taCardsFBDISCOUNT.AsFloat:=0;
                taCardsFBSCALE.AsString:='NOSIZE';
                taCardsFBAVID.AsInteger:=teCards1AVid.AsInteger;
                taCardsFBALCVOL.AsFloat:=teCardsKrep.AsFloat;
                taCardsFB.Post;

                inc(iEdit);

                teCards1.Delete; //���� ����� ��������
              end;

              taCardsFB.Next;
            end else
            begin
              taCardsFB.Delete;  //������� ��������

              inc(iDel);
            end;

            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
            if iC mod iStep = 0 then
            begin
              meC.Edit;
              meCPosCards.AsInteger:=(iC div iStep);
              meC.Post;
              delay(10);

              trUpd1.Commit;
              delay(33);
              trUpd1.StartTransaction;
            end;

            if bStop then break;
          end;

          trUpd1.Commit;

          meC.Edit;
          meCPosCards.AsInteger:=100;
          meC.Post;
          delay(10);

          wm(' '); delay(10);
          wm('        ������� - '+its(iFind)); delay(10);
          wm('        �������� - '+its(iEdit)); delay(10);
          wm('        ������� - '+its(iDel)); delay(10);
          wm(' '); delay(10);

          iC:=0;

          wm('       - �������� ��������.'); delay(10);
          StatusBar1.Panels[1].Text:='';
          StatusBar1.Panels[0].Text:=its(teCards1.RecordCount);

          trUpd1.StartTransaction;

          teCards1.First;
          while not teCards1.Eof do //�������� �������� ������� ��� � ����� - ���������
          begin
            sCode:=its(teCards1Articul.AsInteger);

            if taCardsFB.Locate('ARTICUL',sCode,[])=False then
            begin
              taCardsFB.Append;
              taCardsFBARTICUL.AsString:=its(teCards1Articul.AsInteger);
              taCardsFBCLASSIF.AsInteger:=teCards1Classif.AsInteger;
              taCardsFBNAME.AsString:=teCards1Name.AsString;
              taCardsFBCARD_TYPE.AsInteger:=teCards1Card_type.AsInteger+1;
              if teCards1Messuriment.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
              taCardsFBPRICE_RUB.AsFloat:=teCards1Price.AsFloat;
              taCardsFBDISCQUANT.AsFloat:=0;
              taCardsFBDISCOUNT.AsFloat:=0;
              taCardsFBSCALE.AsString:='NOSIZE';
              taCardsFBAVID.AsInteger:=teCards1AVid.AsInteger;
              taCardsFBALCVOL.AsFloat:=teCardsKrep.AsFloat;
              taCardsFB.Post;
            end else
            begin
              wm('         '+sCode+' ��� ���� � ���� ������ ��'); delay(10);
            end;

            inc(iC);
            inc(iAdd);

            teCards1.Delete;

            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[1].Text:=its(iC);
              delay(10);
            end;
          end;

          trUpd1.Commit;

          taCardsFB.Close;
          wm(' '); delay(10);
//          wm('        ������� - '+its(iFind)); delay(10);
//          wm('        �������� - '+its(iEdit)); delay(10);
//          wm('        ������� - '+its(iDel)); delay(10);
          wm('        ��������� - '+its(iAdd)); delay(10);
          wm(' '); delay(10);
          wm('     �������� �������� ���������.'); delay(10);
          wm(' '); delay(10);

        end else  wm('     ���� ����������');
        Casher.Close;
      end;
      wm('     '+meCCashName.AsString+'- �����.');
    end else wm('  '+meCCashName.AsString+'- �������.');

    meC.Next;
  end;

  wm('������������� ������ ���������.');

  teCards.Close;
  teCards1.Close;
  teBar.Close;
  teBar1.Close;
  teDS.Close;
  teDS1.Close;
  teClass0.Close;
  teClass1.Close;
  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
  cxButton3.Enabled:=False;
  cxButton4.Enabled:=True;
  bProc:=False;
end;

end.
