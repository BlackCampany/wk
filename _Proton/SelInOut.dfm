object fmInOutSel: TfmInOutSel
  Left = 427
  Top = 281
  BorderStyle = bsDialog
  Caption = #1059#1082#1072#1078#1080#1090#1077' '#1080#1089#1090#1086#1095#1085#1080#1082' '#1080' '#1087#1088#1080#1077#1084#1085#1080#1082
  ClientHeight = 154
  ClientWidth = 454
  Color = 16766378
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel5: TcxLabel
    Left = 12
    Top = 16
    Caption = #1041#1088#1072#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
    Transparent = True
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 120
    Top = 48
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Text = 'cxButtonEdit2'
    Width = 293
  end
  object cxLabel1: TcxLabel
    Left = 12
    Top = 52
    Caption = #1042#1099#1075#1088#1091#1078#1072#1090#1100' '#1074' '#1092#1072#1081#1083
    Transparent = True
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 120
    Top = 16
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Text = 'cxButtonEdit1'
    Width = 293
  end
  object Panel1: TPanel
    Left = 0
    Top = 96
    Width = 454
    Height = 58
    Align = alBottom
    BevelInner = bvLowered
    Color = 16759671
    TabOrder = 4
    object cxButton1: TcxButton
      Left = 88
      Top = 12
      Width = 97
      Height = 33
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 264
      Top = 13
      Width = 97
      Height = 32
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 212
    Top = 72
  end
  object OpenDialog2: TOpenDialog
    Left = 280
    Top = 72
  end
end
