unit ClasSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  cxDataStorage;

type
  TfmClassSel = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ClassTree: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClassSel: TfmClassSel;

implementation

uses mDB, Un1, MT;

{$R *.dfm}

procedure TfmClassSel.FormCreate(Sender: TObject);
begin

  ClassTree.Items.BeginUpdate;
//  dmMC.ClassExpNew(nil,ClassTree);
  prReadClass;
  ClassExpNewT(nil,ClassTree);
  ClassTree.Items.EndUpdate;

//  dmMC.CardsExpandLevel(nil,ClassTree);
  try
    if ClassTree.Items.Count>0 then ClassTree.Items[0].Selected:=True;
    cxButton2.Enabled:=False;
  except
  end;
end;

procedure TfmClassSel.ClassTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if Node.getFirstChild=nil then cxButton2.Enabled:=True
  else cxButton2.Enabled:=False;
end;

procedure TfmClassSel.cxButton2Click(Sender: TObject);
begin
  with dmMC do
  begin
    if ClassTree.Selected<>Nil then
    begin
      PosP.Id:=Integer(ClassTree.Selected.Data);
      PosP.Name:=ClassTree.Selected.Text;
      ModalResult:=mrOk;
    end else
    begin
      showmessage('�������� ���������...');
    end;
  end;
end;

end.
