unit CashInOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxfBackGround, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ExtCtrls, cxTextEdit, cxCurrencyEdit, cxControls, cxContainer,
  cxEdit, cxLabel;

type
  TfmCashInOut = class(TForm)
    dxfBackGround1: TdxfBackGround;
    Panel1: TPanel;
    Button1: TcxButton;
    cxButton1: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    procedure cxCurrencyEdit2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashInOut: TfmCashInOut;

implementation

uses Calc, Un1;

{$R *.dfm}

procedure TfmCashInOut.cxCurrencyEdit2Click(Sender: TObject);
Var rSum:Real;
begin
  //��������� �����
  fmCalc.CalcEdit1.EditValue:=0;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then
  begin
    rSum:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
    if rSum>cxCurrencyEdit1.Value then rSum:=cxCurrencyEdit1.Value;
    cxCurrencyEdit2.Value:=rSum;
  end;
end;

end.
