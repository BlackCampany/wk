unit ReasonsV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons;

type
  TfmReasonVoz = class(TForm)
    GridReasV: TcxGrid;
    ViewReasV: TcxGridDBTableView;
    ViewReasVID: TcxGridDBColumn;
    ViewReasVReason: TcxGridDBColumn;
    LevelReasV: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
    procedure ViewReasVCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmReasonVoz: TfmReasonVoz;

implementation
uses Un1;
{$R *.dfm}

procedure TfmReasonVoz.FormCreate(Sender: TObject);
begin
  GridReasV.Align:=AlClient;
  ViewReasV.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmReasonVoz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewReasV.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmReasonVoz.cxButton1Click(Sender: TObject);
begin
 close;
end;

procedure TfmReasonVoz.ViewReasVCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
   fmReasonVoz.ModalResult:=mrOK;
end;

end.
