unit AddDoc5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, Menus, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxImageComboBox, DBClient, dxmdaset, FR_DSet, FR_DBSet,
  FR_Class, ActnList, XPStyleActnCtrls, ActnMan, FIBQuery, pFIBQuery,
  pFIBStoredProc, Placemnt, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxLabel, cxCheckBox, StdCtrls, cxButtons, ExtCtrls, cxTextEdit,cxCalc,
  cxRadioGroup, cxContainer;

{
  Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxCheckBox,
  FR_Class, FR_DSet, FR_DBSet, cxCalc, cxRadioGroup, cxMemo, dxmdaset,
  cxContainer;
}


type

    TfmAddDoc5 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label5: TLabel;
    Label12: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    FormPlacement1: TFormPlacement;
    dstaSpecInv: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    amDocInv: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc5: TcxGrid;
    ViewDoc5: TcxGridDBTableView;
    LevelDoc5: TcxGridLevel;
    acSetPrice: TAction;
    acZPrice: TAction;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    frRepDINV: TfrReport;
    frtaSpecInv: TfrDBDataSet;
    acPrintDoc: TAction;
    acCalcPrice: TAction;
    acPrintCen: TAction;
    ViewDoc5CodeTovar: TcxGridDBColumn;
    ViewDoc5Name: TcxGridDBColumn;
    ViewDoc5CodeEdIzm: TcxGridDBColumn;
    ViewDoc5Price: TcxGridDBColumn;
    ViewDoc5QuantR: TcxGridDBColumn;
    ViewDoc5QuantF: TcxGridDBColumn;
    ViewDoc5SumR: TcxGridDBColumn;
    ViewDoc5SumF: TcxGridDBColumn;
    ViewDoc5QuantD: TcxGridDBColumn;
    ViewDoc5SumD: TcxGridDBColumn;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    cxComboBox1: TcxComboBox;
    cxComboBox2: TcxComboBox;
    cxComboBox3: TcxComboBox;
    cxComboBox4: TcxComboBox;
    Memo1: TcxMemo;
    cxLabel4: TcxLabel;
    acGen: TAction;
    PopupMenu1: TPopupMenu;
    acOutPutF: TAction;
    acInputF: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    cxButton4: TcxButton;
    acExportEx: TAction;
    ViewDoc5IdGr: TcxGridDBColumn;
    ViewDoc5SGr: TcxGridDBColumn;
    ViewDoc5IdSGr: TcxGridDBColumn;
    ViewDoc5SSGr: TcxGridDBColumn;
    acSetGroups: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    acInputFile: TAction;
    acRecalcRemnPos: TAction;
    Label7: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxButton5: TcxButton;
    acSetSerch: TAction;
    cxButton8: TcxButton;
    ViewDoc5IdSSgr: TcxGridDBColumn;
    ViewDoc5SSSGr: TcxGridDBColumn;
    teClass: TdxMemData;
    teClassId: TIntegerField;
    teClassIdParent: TIntegerField;
    teClassNameClass: TStringField;
    taSpecInv: TClientDataSet;
    taSpecInvCodeTovar: TIntegerField;
    taSpecInvName: TStringField;
    taSpecInvCodeEdIzm: TIntegerField;
    taSpecInvNDSProc: TFloatField;
    taSpecInvPrice: TFloatField;
    taSpecInvQuantR: TFloatField;
    taSpecInvQuantF: TFloatField;
    taSpecInvSumR: TFloatField;
    taSpecInvSumF: TFloatField;
    taSpecInvQuantD: TFloatField;
    taSpecInvSumD: TFloatField;
    taSpecInvIdGr: TIntegerField;
    taSpecInvIdSGr: TIntegerField;
    taSpecInvIdSSGr: TIntegerField;
    taSpecInvSGr: TStringField;
    taSpecInvSSGr: TStringField;
    taSpecInvSSSGr: TStringField;
    cxCheckBox2: TcxCheckBox;
    acTermFile: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acEqual: TAction;
    cxCheckBox3: TcxCheckBox;
    teSp: TdxMemData;
    teSpItem: TIntegerField;
    teSpPrice: TFloatField;
    teSpQuantR: TFloatField;
    teSpQuantF: TFloatField;
    acTestRemn: TAction;
    ViewDoc5Num: TcxGridDBColumn;
    taSpecInvNum: TIntegerField;
    teSpNum: TIntegerField;
    acRecalcRemnInv: TAction;
    cxCheckBox4: TcxCheckBox;
    cxLabel7: TcxLabel;
    acSaveDoc1: TAction;
    taSpecInvPriceSS: TFloatField;
    taSpecInvSumRSS: TFloatField;
    taSpecInvSumFSS: TFloatField;
    taSpecInvSumDSS: TFloatField;
    ViewDoc5PriceSS: TcxGridDBColumn;
    ViewDoc5SumRSS: TcxGridDBColumn;
    ViewDoc5SumFSS: TcxGridDBColumn;
    ViewDoc5SumDSS: TcxGridDBColumn;
    teSpSumRSS: TFloatField;
    teSpSumFSS: TFloatField;
    acRecalcRemnSS: TAction;
    cxCheckBox5: TcxCheckBox;
    acParts: TAction;
    taSpecInvSumRTO: TFloatField;
    taSpecInvSumDTO: TFloatField;
    ViewDoc5SumRTO: TcxGridDBColumn;
    ViewDoc5SumDTO: TcxGridDBColumn;
    acCalcTOSum: TAction;
    teSpSumTSS: TFloatField;
    cxCheckBox6: TcxCheckBox;
    cxCheckBox7: TcxCheckBox;
    acEqTOSS: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    acCreatePP0: TAction;
    SS1: TMenuItem;
    acFactToZerro: TAction;
    N13: TMenuItem;
    acCalcTPos: TAction;
    N14: TMenuItem;
    acTestMoveNoUse: TAction;
    N15: TMenuItem;
    acOutPutF1: TAction;
    acInputF1: TAction;
    N12: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    taSpecInvQuantC: TFloatField;
    taSpecInvSumC: TFloatField;
    taSpecInvSumRSC: TFloatField;
    ViewDoc5QuantC: TcxGridDBColumn;
    ViewDoc5SumC: TcxGridDBColumn;
    ViewDoc5SumRSC: TcxGridDBColumn;
    taSpecInvSumRSSI: TFloatField;
    taSpecInvSumRI: TFloatField;
    ViewDoc5SumRI: TcxGridDBColumn;
    ViewDoc5SumRSSI: TcxGridDBColumn;
    teSpQuantC: TFloatField;
    teSpSumC: TFloatField;
    teSpSumRSC: TFloatField;
    cxButton6: TcxButton;
    taSpecInvBarCode: TStringField;
    acCalcSCPos: TAction;
    N18: TMenuItem;
    acFormNacl: TAction;
    acOutWithNDS: TAction;
    acRecalcSS1: TAction;
    N11: TMenuItem;
    taSpecInviVid: TIntegerField;
    taSpecInviMaker: TIntegerField;
    taSpecInvsMaker: TStringField;
    taSpecInvVol: TFloatField;
    taSpecInvVolInv: TFloatField;
    ViewDoc5iVid: TcxGridDBColumn;
    ViewDoc5sMaker: TcxGridDBColumn;
    ViewDoc5Vol: TcxGridDBColumn;
    ViewDoc5VolInv: TcxGridDBColumn;
    N19: TMenuItem;
    acSetPrice1: TMenuItem;
    taSpecInvSumDTOSS: TFloatField;
    ViewDoc5SumDTOSS: TcxGridDBColumn;
    acTmp1: TAction;
    acCreateOut: TAction;
    teOutLn: TdxMemData;
    teOutLniCode: TIntegerField;
    teOutLnQuant: TFloatField;
    teOutLnPrice0: TFloatField;
    teOutLnPrice: TFloatField;
    teOutLnPriceM: TFloatField;
    teOutLniM: TIntegerField;
    teOutLniCli: TIntegerField;
    teOutLnIdDoc: TIntegerField;
    N20: TMenuItem;
    teOutLniTc: TIntegerField;
    acTestDoc1: TAction;
    teCorr: TdxMemData;
    teCorrItem: TIntegerField;
    teCorrRQuantC: TFloatField;
    teCorrRSumC: TFloatField;
    teCorrRSumRSC: TFloatField;
    acCreateRetDoc1: TAction;
    N21: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc5DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc5DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc5Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc5EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc5EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure ViewDoc5DblClick(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure taSpecInv1QuantFChange(Sender: TField);
    procedure taSpecInv1PriceChange(Sender: TField);
    procedure acGenExecute(Sender: TObject);
    procedure acInputFExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure acOutPutFExecute(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure acInputFileExecute(Sender: TObject);
    procedure acRecalcRemnPosExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acSetSerchExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure taSpecInvQuantFChange(Sender: TField);
    procedure acTermFileExecute(Sender: TObject);
    procedure acEqualExecute(Sender: TObject);
    procedure acTestRemnExecute(Sender: TObject);
    procedure acRecalcRemnInvExecute(Sender: TObject);
    procedure acSaveDoc1Execute(Sender: TObject);
    procedure acRecalcRemnSSExecute(Sender: TObject);
    procedure acPartsExecute(Sender: TObject);
    procedure acCalcTOSumExecute(Sender: TObject);
    procedure frRepDINVUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure acEqTOSSExecute(Sender: TObject);
    procedure acCreatePP0Execute(Sender: TObject);
    procedure acFactToZerroExecute(Sender: TObject);
    procedure acCalcTPosExecute(Sender: TObject);
    procedure acTestMoveNoUseExecute(Sender: TObject);
    procedure acOutPutF1Execute(Sender: TObject);
    procedure acInputF1Execute(Sender: TObject);
    procedure cxCheckBox3PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure acCalcSCPosExecute(Sender: TObject);
    procedure acFormNaclExecute(Sender: TObject);
    procedure acOutWithNDSExecute(Sender: TObject);
    procedure acRecalcSS1Execute(Sender: TObject);
    procedure taSpecInvCalcFields(DataSet: TDataSet);
    procedure acTmp1Execute(Sender: TObject);
    procedure acCreateOutExecute(Sender: TObject);
    procedure acTestDoc1Execute(Sender: TObject);
    procedure acCreateRetDoc1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButton(bSt:Boolean);

  end;

var
  fmAddDoc5: TfmAddDoc5;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, DocsIn, sumprops, DocsInv,
  InvExport, MT, u2fdk, MainMCryst, Status, dbe;

{$R *.dfm}

procedure TfmAddDoc5.prButton(bSt:Boolean);
begin
  cxTextEdit1.Properties.ReadOnly:=not bSt;
  cxDateEdit1.Properties.ReadOnly:=not bSt;
  cxLookupComboBox1.Properties.ReadOnly:=not bSt;

  cxLabel1.Enabled:=bSt;
  cxLabel2.Enabled:=bSt;
  cxLabel3.Enabled:=bSt;
  cxLabel4.Enabled:=bSt;
  cxLabel5.Enabled:=bSt;
  cxLabel6.Enabled:=bSt;

  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  cxButton8.Enabled:=bSt;

  cxComboBox1.Properties.ReadOnly:=not bSt;
  cxComboBox2.Properties.ReadOnly:=not bSt;
  cxComboBox3.Properties.ReadOnly:=not bSt;
  cxComboBox4.Properties.ReadOnly:=not bSt;

  ViewDoc5.OptionsData.Editing:=bSt;
  ViewDoc5.OptionsData.Deleting:=bSt;
end;

procedure TfmAddDoc5.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc5.Align:=alClient;
  ViewDoc5.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
{  if CommonSet.Single=1 then
  begin
    acOutPutF1.Enabled:=True;
    acOutPutF1.Visible:=True;
    acInputF1.Enabled:=True;
    acInputF1.Visible:=True;
  end else
  begin
    acOutPutF1.Enabled:=False;
    acOutPutF1.Visible:=False;
    acInputF1.Enabled:=False;
    acInputF1.Visible:=False;
  end;}

  acOutPutF1.Enabled:=True;
  acOutPutF1.Visible:=True;
  acInputF1.Enabled:=True;
  acInputF1.Visible:=True;
end;

procedure TfmAddDoc5.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc5.ViewDoc5DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=6 then  Accept:=True;
end;

procedure TfmAddDoc5.ViewDoc5DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
//
  if iDirect=6 then
  begin
    iDirect:=0; //������ ���������
    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin

        with dmMC do
        begin
          ViewDoc5.BeginUpdate;
          try
          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            quFindC1.Active:=False;
            quFindC1.SQL.Clear;
            quFindC1.SQL.Add('SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status", "Goods"."V02"');
            quFindC1.SQL.Add('FROM "Goods"');
            quFindC1.SQL.Add('where "Goods"."ID"='+INtToStr(iNum));
            quFindC1.Active:=True;

            if quFindC1.RecordCount=1 then
            begin
              if taSpecInv.Locate('CodeTovar',quFindC1ID.AsInteger,[])= False then
              begin
                taSpecInv.Append;
                taSpecInvCodeTovar.AsInteger:=quFindC1ID.AsInteger;
                taSpecInvName.AsString:=quFindC1Name.AsString;
                taSpecInvCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
                taSpecInvNDSProc.AsFloat:=quFindC1NDS.AsFloat;
                taSpecInvPrice.AsFloat:=quFindC1Cena.AsFloat;
                taSpecInvQuantR.AsFloat:=0;
                taSpecInvQuantF.AsFloat:=0;
                taSpecInvSumR.AsFloat:=0;
                taSpecInvSumF.AsFloat:=0;
                taSpecInvQuantD.AsFloat:=0;
                taSpecInvSumD.AsFloat:=0;
                taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;

                taSpecInv.Post;

              end;
            end;
          end;

          finally
            ViewDoc5.EndUpdate;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc5.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc5.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  ViewDoc5.BeginUpdate;
  taSpecInv.First;
  while not taSpecInv.Eof do
  begin
{      taSpecInv.Edit;
      taSpecInvPrice2.AsFloat:=taSpecInvPrice1.AsFloat;
      taSpecInvSum2.AsFloat:=taSpecInvSum1.AsFloat;
      taSpecInvSumNac.AsFloat:=0;
      taSpecInvProcNac.AsFloat:=0;
      taSpecInv.Post;
      }
    taSpecInv.Next;
    delay(10);
  end;
  taSpecInv.First;
  ViewDoc5.EndUpdate;
end;

procedure TfmAddDoc5.ViewDoc5Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5QuantF' then iCol:=1;  //���-�� ����
  if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Price' then iCol:=2;  //���-�� ����
end;

procedure TfmAddDoc5.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc5.SetFocus;

  cxTextEdit2.Text:='';

  Memo1.Clear;

  if CommonSet.Single=1 then
  begin
    cxLabel7.Visible:=True;
    ViewDoc5iVid.Visible:=True;
    ViewDoc5sMaker.Visible:=True;
    ViewDoc5Vol.Visible:=True;
    ViewDoc5VolInv.Visible:=True;
  end else
  begin
    cxLabel7.Visible:=False;
    ViewDoc5iVid.Visible:=False;
    ViewDoc5sMaker.Visible:=False;
    ViewDoc5Vol.Visible:=False;
    ViewDoc5VolInv.Visible:=False;
  end;
end;

procedure TfmAddDoc5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      ViewDoc5.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    ViewDoc5.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  end;
end;

procedure TfmAddDoc5.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc5.ViewDoc5EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
//    rK:Real;
//    iM:Integer;
//    Sm:String;

begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCode));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          taSpecInv.Edit;
          taSpecInvCodeTovar.AsInteger:=quFindCID.AsInteger;
          taSpecInvName.AsString:=quFindCName.AsString;
          taSpecInvCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
          taSpecInvNDSProc.AsFloat:=quFindCNDS.AsFloat;
          taSpecInvPrice.AsFloat:=quFindCCena.AsFloat;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvQuantD.AsFloat:=0;
          taSpecInvSumD.AsFloat:=0;
          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;

          ViewDoc5QuantF.Focused:=True;
        end;
      end;
      if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Name' then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if (sName>'')and(Length(sName)>=3) then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if CountRec1(quFindC) then
            begin
              taSpecInv.Edit;
              taSpecInvCodeTovar.AsInteger:=quFindCID.AsInteger;
              taSpecInvName.AsString:=quFindCName.AsString;
              taSpecInvCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
              taSpecInvNDSProc.AsFloat:=quFindCNDS.AsFloat;
              taSpecInvPrice.AsFloat:=quFindCCena.AsFloat;
              taSpecInvQuantR.AsFloat:=0;
              taSpecInvQuantF.AsFloat:=0;
              taSpecInvSumR.AsFloat:=0;
              taSpecInvSumF.AsFloat:=0;
              taSpecInvQuantD.AsFloat:=0;
              taSpecInvSumD.AsFloat:=0;
              taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
              taSpecInv.Post;

              ViewDoc5QuantF.Focused:=True;
            end;
          end;
        end;
      end;
    end else
      if (ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5CodeTovar') then
        if fTestKey(Key)=False then
          if taSpecInv.State in [dsEdit,dsInsert] then taSpecInv.Cancel;
  end;
end;

procedure TfmAddDoc5.ViewDoc5EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var // Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
    if (ViewDoc5.Controller.FocusedColumn.Name='ViewDoc1Name') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if (sName>'')and(Length(sName)>=3) then
      begin
        quFindC1.Active:=False;
        quFindC1.SQL.Clear;
        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status", "Goods"."V02"');
        quFindC1.SQL.Add('FROM "Goods"');
        quFindC1.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
        quFindC1.Active:=True;

        if quFindC1.RecordCount=1 then //����� ���� ������
        begin
          taSpecInv.Edit;
          taSpecInvCodeTovar.AsInteger:=quFindC1ID.AsInteger;
          taSpecInvName.AsString:=quFindC1Name.AsString;
          taSpecInvCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
          taSpecInvNDSProc.AsFloat:=quFindC1NDS.AsFloat;
          taSpecInvPrice.AsFloat:=quFindC1Cena.AsFloat;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvQuantD.AsFloat:=0;
          taSpecInvSumD.AsFloat:=0;
          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;

          AEdit.SelectAll;
          ViewDoc5Name.Options.Editing:=False;
          ViewDoc5Name.Focused:=True;
          Key:=#0;
        end;//}
      end;
    end;
  end;//}
end;

procedure TfmAddDoc5.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  ViewDoc5.BeginUpdate;

  if taSpecInv.Locate('CodeTovar',0,[])=False then
  begin
    taSpecInv.Append;
    taSpecInvCodeTovar.AsInteger:=0;
    taSpecInvName.AsString:='';
    taSpecInvCodeEdIzm.AsInteger:=1;
    taSpecInvNDSProc.AsFloat:=18;
    taSpecInvPrice.AsFloat:=0;
    taSpecInvQuantR.AsFloat:=0;
    taSpecInvQuantF.AsFloat:=0;
    taSpecInvSumR.AsFloat:=0;
    taSpecInvSumF.AsFloat:=0;
    taSpecInvQuantD.AsFloat:=0;
    taSpecInvSumD.AsFloat:=0;
    taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
    taSpecInv.Post;
  end;

  ViewDoc5.EndUpdate;
  GridDoc5.SetFocus;

  ViewDoc5CodeTovar.Options.Editing:=True;
  ViewDoc5Name.Options.Editing:=True;
  ViewDoc5Name.Focused:=True;
end;

procedure TfmAddDoc5.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if cxButton1.Enabled then
  if taSpecInv.RecordCount>0 then
  begin
    if pos('Auto',cxTextEdit1.Text)=0 then taSpecInv.Delete else showmessage('�������� ������� ��� ������� ���� ��������� ��������� !!!');
  end;
end;

procedure TfmAddDoc5.acAddListExecute(Sender: TObject);
begin
  iDirect:=6;
  fmCards.Show;
end;

procedure TfmAddDoc5.cxButton1Click(Sender: TObject);
Var
    rSum1,rSum2,rSumQ1,rSumQ2,rSum1SS,rSum2SS,rSum3SS:Real;
    IdH:INteger;
    StrWk:String;
    iC,iT,iSS:INteger;
begin
  //��������  - �������
  with dmMC do
  with dmMT do
  begin
    StatusBar1.Panels[0].Text:='';
    StatusBar1.Panels[1].Text:='';

    if cxDateEdit1.Date<=prOpenDate then
    begin
      ShowMessage('������ ������.');
      exit;
    end;

    prButton(False);
    iT:=ViewDoc5.Tag;

    ViewDoc5.BeginUpdate;
    Memo1.Clear;
    Memo1.Lines.Add('����� ���� ���������� ���������.');
    delay(10);

    idh:=0;

    if taSpecInv.State in [dsEdit,dsInsert] then taSpecInv.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    iSS:=fSS(cxLookupComboBox1.EditValue);

//    if (taSpecInv.RecordCount<=200)and(iSS>0) then //���-�� ������� �� ����� - ��� ������������ �����
    if (pos('Auto',cxTextEdit1.Text)=1)  //������������
    or((taSpecInv.RecordCount<=200)and(iSS>0)) //���-�� ������� �� ����� - ��� ������������ �����
    then 
    begin
      Memo1.Lines.Add('   ���-�� ������� - '+its(taSpecInv.RecordCount));
      Memo1.Lines.Add('   ');
      acSetPrice.Execute; delay(100);
      Memo1.Lines.Add('   ');
      acRecalcRemnSS.Execute;  delay(100);
      Memo1.Lines.Add('   ');
      acCalcTOSum.Execute;  delay(100);
      Memo1.Lines.Add('   ');
    end;


    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "InventryHead"');
      quC.SQL.Add('where Depart='+its(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInventry='''+ds(cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='''+cxTextEdit1.Text+'''');
      quC.Active:=True;

      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        prButton(True);
        exit;
      end;
    end;
    try
      //������� ������ ����

      Memo1.Lines.Add('  - ����������.'); delay(10);

      CloseTe(teSp);

      rSum1:=0; rSum2:=0; rSumQ1:=0; rSumQ2:=0; rSum1SS:=0; rSum2SS:=0; rSum3SS:=0;
      iCol:=0; iC:=0;
      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        teSp.Append;
        teSpItem.AsInteger:=taSpecInvCodeTovar.AsInteger;
        teSpPrice.AsFloat:=taSpecInvPrice.AsFloat;
        teSpQuantR.AsFloat:=taSpecInvQuantR.AsFloat;
        teSpQuantF.AsFloat:=taSpecInvQuantF.AsFloat;
        teSpNum.AsInteger:=taSpecInvNum.AsInteger;
        teSpSumRSS.AsFloat:=taSpecInvSumRSS.AsFloat;
        teSpSumFSS.AsFloat:=taSpecInvSumFSS.AsFloat;
        teSpSumTSS.AsFloat:=taSpecInvSumRTO.AsFloat;
        if (cxCheckBox3.Checked)and(ViewDoc5.Tag=1) then
        begin
          teSpQuantC.AsFloat:=taSpecInvQuantC.AsFloat;
          teSpSumC.AsFloat:=taSpecInvSumC.AsFloat;
          teSpSumRSC.AsFloat:=taSpecInvSumRSC.AsFloat;
        end
        else
        begin
          teSpQuantC.AsFloat:=0;
          teSpSumC.AsFloat:=0;
          teSpSumRSC.AsFloat:=0;
        end;
        teSp.Post;

      //������������ ��� ������� � �����������
{        taSpecInv.Edit;
        taSpecInvSumR.AsFloat:=RoundVal(taSpecInvSumR.AsFloat);
        taSpecInvSumF.AsFloat:=RoundVal(taSpecInvSumF.AsFloat);
        taSpecInv.Post;
}
        if (cxCheckBox3.Checked)and(ViewDoc5.Tag=1) then
        begin
          rSum1:=rSum1+rv(taSpecInvSumR.AsFloat+taSpecInvSumC.AsFloat);
          rSum2:=rSum2+rv(taSpecInvSumF.AsFloat);
          rSum1SS:=rSum1SS+rv(taSpecInvSumRSS.AsFloat+taSpecInvSumRSC.AsFloat);
          rSum2SS:=rSum2SS+rv(taSpecInvSumFSS.AsFloat);
          rSum3SS:=rSum3SS+rv(taSpecInvSumRTO.AsFloat);
          rSumQ1:=rSumQ1+taSpecInvQuantR.AsFloat+taSpecInvQuantC.AsFloat;
          rSumQ2:=rSumQ2+taSpecInvQuantF.AsFloat;
        end else
        begin
          rSum1:=rSum1+rv(taSpecInvSumR.AsFloat);
          rSum2:=rSum2+rv(taSpecInvSumF.AsFloat);
          rSum1SS:=rSum1SS+rv(taSpecInvSumRSS.AsFloat);
          rSum2SS:=rSum2SS+rv(taSpecInvSumFSS.AsFloat);
          rSum3SS:=rSum3SS+rv(taSpecInvSumRTO.AsFloat);
          rSumQ1:=rSumQ1+taSpecInvQuantR.AsFloat;
          rSumQ2:=rSumQ2+taSpecInvQuantF.AsFloat;
        end;

        taSpecInv.Next;  inc(iC);

        if iC mod 100=0 then
        begin
          StatusBar1.Panels[0].Text:=its(iC);
          delay(10);
        end;
      end;

      if cxTextEdit1.Tag=0 then
      begin
        IDH:=prMax('DocsInv')+1;
      end else //��� �� ����������
      begin
        IDH:=cxTextEdit1.Tag;
      end;

      Memo1.Lines.Add('  - ���������.'); delay(10);

      //����� �������� ���������
      if cxTextEdit1.Tag=0 then //����������
      begin
        quA.Active:=False;
        StrWk:='';
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "InventryHead" values (');

        StrWk:=StrWk+(its(idh)+',');//Inventry
        StrWk:=StrWk+(its(cxLookupComboBox1.EditValue)+',');//Depart
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateInventry
        StrWk:=StrWk+(''''+cxTextEdit1.Text+''',');//Number
        StrWk:=StrWk+(its(cxComboBox1.ItemIndex)+',');//CardIndex
        StrWk:=StrWk+(its(cxComboBox2.ItemIndex)+',');//InputMode
        StrWk:=StrWk+(its(cxComboBox3.ItemIndex)+',');//PriceMode
        StrWk:=StrWk+(its(cxComboBox4.ItemIndex)+',');//Detail
        StrWk:=StrWk+('0,');//GGroup
        StrWk:=StrWk+('0,');//SubGroup
        if cxCheckBox3.Checked then StrWk:=StrWk+('1,') //Item
        else StrWk:=StrWk+('0,');
        StrWk:=StrWk+('0,');//CountLost
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateLost
        if (iT=0) or (iSS=0) then
        begin
          StrWk:=StrWk+(fs(rSumQ1)+',');//QuantRecord
          StrWk:=StrWk+(fs(rSum1)+',');//SumRecord
          StrWk:=StrWk+(fs(rSumQ2)+',');//QuantActual
          StrWk:=StrWk+(fs(rSum2)+',');//SumActual
        end;
        if (iT=1)and(iSS<>0) then
        begin
          StrWk:=StrWk+(fs(rSumQ1)+',');//QuantRecord
          StrWk:=StrWk+(fs(rSum3SS)+',');//SumRecord
          StrWk:=StrWk+(fs(rSumQ2)+',');//QuantActual
          StrWk:=StrWk+(fs(rSum2SS)+',');//SumActual
        end;

        StrWk:=StrWk+('0,');//NumberOfLines
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateStart
        StrWk:=StrWk+('''23:59:59'',');//TimeStart
        StrWk:=StrWk+(''''',');//President
        StrWk:=StrWk+(''''',');//PresidentPost
        StrWk:=StrWk+(''''',');//Member1
        StrWk:=StrWk+(''''',');//Member1_Post
        StrWk:=StrWk+(''''',');//Member2
        StrWk:=StrWk+(''''',');//Member2_Post
        StrWk:=StrWk+(''''',');//Member3
        StrWk:=StrWk+(''''',');//Member3_Post
        StrWk:=StrWk+(its(iT)+',');//xDopStatus
        StrWk:=StrWk+(''''',');//Response1
        StrWk:=StrWk+(''''',');//Response1_Post
        StrWk:=StrWk+(''''',');//Response2
        StrWk:=StrWk+(''''',');//Response2_Post
        StrWk:=StrWk+(''''',');//Response3
        StrWk:=StrWk+(''''',');//Response3_Post
        StrWk:=StrWk+(''''',');//Reason
        StrWk:=StrWk+(''''',');//CASHER
        StrWk:=StrWk+('''�'',');//Status
        StrWk:=StrWk+('0,');//LinkInvoice
        StrWk:=StrWk+('0,');//StartTransfer
        StrWk:=StrWk+('0,');//EndTransfer
        StrWk:=StrWk+('''''');//Rezerv

        quA.SQL.Add(StrWk);

        quA.SQL.Add(')');
        quA.ExecSQL;

      end else //����������
      begin
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('Update "InventryHead" set');

        quA.SQL.Add('Inventry='+its(cxTextEdit1.Tag)+',');//Inventry
        quA.SQL.Add('Depart='+its(cxLookupComboBox1.EditValue)+',');//Depart
        quA.SQL.Add('DateInventry='+''''+ds(cxDateEdit1.Date)+''',');//DateInventry
        quA.SQL.Add('Number='+''''+cxTextEdit1.Text+''',');//Number
        quA.SQL.Add('CardIndex='+its(cxComboBox1.ItemIndex)+',');//CardIndex
        quA.SQL.Add('InputMode='+its(cxComboBox2.ItemIndex)+',');//InputMode
        quA.SQL.Add('PriceMode='+its(cxComboBox3.ItemIndex)+',');//PriceMode
        quA.SQL.Add('Detail='+its(cxComboBox4.ItemIndex)+',');//Detail
        quA.SQL.Add('GGroup='+'0,');//GGroup
        quA.SQL.Add('SubGroup='+'0,');//SubGroup

        if cxCheckBox3.Checked then quA.SQL.Add('Item='+'1,') //Item
        else quA.SQL.Add('Item='+'0,');//Item

        quA.SQL.Add('CountLost=0,');//CountLost
        quA.SQL.Add('DateLost='+''''+ds(cxDateEdit1.Date)+''',');//DateLost
        if (iT=0)or(iSS=0) then
        begin
          quA.SQL.Add('QuantRecord='+fs(rSumQ1)+',');//QuantRecord
          quA.SQL.Add('SumRecord='+fs(rSum1)+',');//SumRecord
          quA.SQL.Add('QuantActual='+fs(rSumQ2)+',');//QuantActual
          quA.SQL.Add('SumActual='+fs(rSum2)+',');//SumActual
        end;
        if (iT=1)and(iSS<>0) then
        begin
          quA.SQL.Add('QuantRecord='+fs(rSumQ1)+',');//QuantRecord
          quA.SQL.Add('SumRecord='+fs(rSum3SS)+',');//SumRecord
          quA.SQL.Add('QuantActual='+fs(rSumQ2)+',');//QuantActual
          quA.SQL.Add('SumActual='+fs(rSum2SS)+',');//SumActual
        end;
        quA.SQL.Add('NumberOfLines='+'0,');//NumberOfLines
        quA.SQL.Add('DateStart='+''''+ds(cxDateEdit1.Date)+''',');//DateStart
        quA.SQL.Add('TimeStart='+'''23:59:59'',');//TimeStart
        quA.SQL.Add('President='+''''',');//President
        quA.SQL.Add('PresidentPost='+''''',');//PresidentPost
        quA.SQL.Add('Member1='+''''',');//Member1
        quA.SQL.Add('Member1_Post='+''''',');//Member1_Post
        quA.SQL.Add('Member2='+''''',');//Member2
        quA.SQL.Add('Member2_Post='+''''',');//Member2_Post
        quA.SQL.Add('Member3='+''''',');//Member3
        quA.SQL.Add('Member3_Post='+''''',');//Member3_Post
        quA.SQL.Add('xDopStatus='+its(iT)+',');//xDopStatus
        quA.SQL.Add('Response1='+''''',');//Response1
        quA.SQL.Add('Response1_Post='+''''',');//Response1_Post
        quA.SQL.Add('Response2='+''''',');//Response2
        quA.SQL.Add('Response2_Post='+''''',');//Response2_Post
        quA.SQL.Add('Response3='+''''',');//Response3
        quA.SQL.Add('Response3_Post='+''''',');//Response3_Post
        quA.SQL.Add('Reason='+''''',');//Reason
        quA.SQL.Add('CASHER='+''''',');//CASHER
        quA.SQL.Add('Status='+'''�'',');//Status
        quA.SQL.Add('LinkInvoice='+'0,');//LinkInvoice
        quA.SQL.Add('StartTransfer='+'0,');//StartTransfer
        quA.SQL.Add('EndTransfer='+'0,');//EndTransfer
        quA.SQL.Add('Rezerv='+'''''');//Rezerv
        quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quA.SQL.Add('and DateInventry='''+ds(cxDateEdit10.Date)+'''');
        quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quA.ExecSQL;
      end;
      //������� �� ������������ - ��� ������ ����������

      Memo1.Lines.Add('  - ������������.'); delay(10);
      if iT=0 then
      begin
        ptInvLine.Active:=False;
        ptInvLine.Active:=True;
        ptInvLine.CancelRange;
        ptInvLine.SetRange([cxLookupComboBox1.EditValue,idh],[cxLookupComboBox1.EditValue,idh]);

        Memo1.Lines.Add('  - ���������.'); delay(10);

        iC:=0;
        ptInvLine.First;
        while not ptInvLine.Eof do
        begin
          if teSp.Locate('Item',ptInvLineItem.AsInteger,[]) then
          begin
            ptInvLine.Edit;
            ptInvLinePrice.AsFloat:=teSpPrice.AsFloat;
            ptInvLineQuantRecord.AsFloat:=teSpQuantR.AsFloat;
            ptInvLineQuantActual.AsFloat:=teSpQuantF.AsFloat;
            ptInvLineIndexPost.AsInteger:=teSpNum.AsInteger;
            ptInvLine.Post;

            teSp.Delete;

            ptInvLine.Next;
          end else ptInvLine.Delete;

          inc(iC);
          if iC mod 50 =0 then
          begin
            StatusBar1.Panels[0].Text:=its(iC);
            delay(10);
          end;
        end;

        Memo1.Lines.Add('  - ����������.'); delay(10);
        iC:=0;
        teSp.First;
        while not teSp.Eof do
        begin
          ptInvLine.Append;
          ptInvLineInventry.AsInteger:=idh;
          ptInvLineItem.AsInteger:=teSpItem.AsInteger;
          ptInvLinePrice.AsFloat:=teSpPrice.AsFloat;
          ptInvLineQuantRecord.AsFloat:=teSpQuantR.AsFloat;
          ptInvLineQuantActual.AsFloat:=teSpQuantF.AsFloat;
          ptInvLineQuantLost.AsFloat:=0;
          ptInvLineDepart.AsInteger:=cxLookupComboBox1.EditValue;
          ptInvLineIndexPost.AsInteger:=teSpNum.AsInteger;
          ptInvLineCodePost.AsInteger:=0;
          ptInvLine.Post;

          teSp.Next; inc(iC);
          if iC mod 50 =0 then
          begin
            StatusBar1.Panels[1].Text:=its(iC);
            delay(10);
          end;
        end;
        ptInvLine.Active:=False;
      end;
      if iT=1 then
      begin
        ptInvLine1.Active:=False;
        ptInvLine1.Active:=True;
        ptInvLine1.CancelRange;
        ptInvLine1.SetRange([idh],[idh]);

        Memo1.Lines.Add('  - ���������.'); delay(10);

        iC:=0;
        ptInvLine1.First;
        while not ptInvLine1.Eof do
        begin
          if teSp.Locate('Item',ptInvLine1Item.AsInteger,[]) then
          begin
            ptInvLine1.Edit;
            ptInvLine1PriceR.AsFloat:=teSpPrice.AsFloat;
            ptInvLine1QuantR.AsFloat:=teSpQuantR.AsFloat;
            ptInvLine1QuantF.AsFloat:=teSpQuantF.AsFloat;
            ptInvLine1QUANTD.AsFloat:=teSpQuantF.AsFloat-teSpQuantR.AsFloat;
            ptInvLine1NUM.AsInteger:=teSpNum.AsInteger;
            ptInvLine1SUMRSS.AsFloat:=teSpSUMRSS.AsFloat;
            ptInvLine1SUMFSS.AsFloat:=teSpSUMFSS.AsFloat;
            ptInvLine1SUMDSS.AsFloat:=teSpSumTSS.AsFloat;
            ptInvLine1QUANTC.AsFloat:=teSpQUANTC.AsFloat;
            ptInvLine1SUMC.AsFloat:=teSpSUMC.AsFloat;
            ptInvLine1SUMRSC.AsFloat:=teSpSUMRSC.AsFloat;
            ptInvLine1.Post;

            teSp.Delete;

            ptInvLine1.Next;
          end else ptInvLine1.Delete;

          inc(iC);
          if iC mod 50 =0 then
          begin
            StatusBar1.Panels[0].Text:=its(iC);
            delay(10);
          end;
        end;

        Memo1.Lines.Add('  - ����������.'); delay(10);
        iC:=0;
        teSp.First;
        while not teSp.Eof do
        begin
          ptInvLine1.Append;
          ptInvLine1IDH.AsInteger:=idh;
          ptInvLine1ITEM.AsInteger:=teSpItem.AsInteger;
          ptInvLine1PRICER.AsFloat:=teSpPrice.AsFloat;
          ptInvLine1QuantR.AsFloat:=teSpQuantR.AsFloat;
          ptInvLine1QuantF.AsFloat:=teSpQuantF.AsFloat;
          ptInvLine1QuantD.AsFloat:=teSpQuantF.AsFloat-teSpQuantR.AsFloat;
          ptInvLine1NUM.AsInteger:=teSpNum.AsInteger;
          ptInvLine1SUMRSS.AsFloat:=teSpSUMRSS.AsFloat;
          ptInvLine1SUMFSS.AsFloat:=teSpSUMFSS.AsFloat;
          ptInvLine1SUMDSS.AsFloat:=teSpSumTSS.AsFloat;
          ptInvLine1QUANTC.AsFloat:=teSpQUANTC.AsFloat;
          ptInvLine1SUMC.AsFloat:=teSpSUMC.AsFloat;
          ptInvLine1SUMRSC.AsFloat:=teSpSUMRSC.AsFloat;
          ptInvLine1.Post;

          teSp.Next; inc(iC);
          if iC mod 50 =0 then
          begin
            StatusBar1.Panels[1].Text:=its(iC);
            delay(10);
          end;
        end;
        ptInvLine1.Active:=False;
      end;

      cxTextEdit1.Tag:=IDH;

      cxTextEdit10.Tag:=IDH;
      cxTextEdit10.Text:=cxTextEdit1.Text;
      cxDateEdit10.Date:=cxDateEdit1.Date;
      cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;
    finally
      teSp.Active:=False;
      prButton(True);

      fmDocs5.ViewDocsInv.BeginUpdate;
      quTTnInv.Active:=False;
      quTTnInv.Active:=True;
      quTTnInv.Locate('Inventry',idh,[]);
      fmDocs5.ViewDocsInv.EndUpdate;

      ViewDoc5.EndUpdate;
      Memo1.Lines.Add('���������� ��.');
    end;
  end;
end;

procedure TfmAddDoc5.acDelAllExecute(Sender: TObject);
begin
  if cxButton1.Enabled then
  if taSpecInv.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      if pos('Auto',cxTextEdit1.Text)=0 then
      begin
        ViewDoc5.BeginUpdate;
        taSpecInv.First; while not taSpecInv.Eof do taSpecInv.Delete;
        ViewDoc5.EndUpdate;
      end else showmessage('�������� ������� ��� ������� ���� ��������� ��������� !!!');
    end;
  end;
end;

procedure TfmAddDoc5.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc5.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc5.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc5.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc5.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc5.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc5.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc5.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc5.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC:INteger;
begin
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if taSpecInv.RecordCount>0 then
    if taSpecInv.Locate('BarCode',sBar,[]) then
    begin
      ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
      GridDoc5.SetFocus;
      exit;
    end;

  if prFindBarC(sBar,0,iC) then
  begin
    with dmMC do
    begin
      if taSpecInv.Locate('CodeTovar',0,[])=False then
      begin
        taSpecInv.Append;
        taSpecInvCodeTovar.AsInteger:=iC;
        taSpecInvName.AsString:=quCIdName.AsString;
        taSpecInvCodeEdIzm.AsInteger:=quCIdEdIzm.AsInteger;
        taSpecInvNDSProc.AsFloat:=quCIdNDS.AsFloat;
        taSpecInvPrice.AsFloat:=quCIdCena.AsFloat;
        taSpecInvQuantR.AsFloat:=0;
        taSpecInvQuantF.AsFloat:=0;
        taSpecInvSumR.AsFloat:=0;
        taSpecInvSumF.AsFloat:=0;
        taSpecInvQuantD.AsFloat:=0;
        taSpecInvSumD.AsFloat:=0;
        taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
        taSpecInv.Post;

        ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
        GridDoc5.SetFocus;
      end;
    end;
  end else
    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc5.ViewDoc5DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
    if (ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Name') then
    begin
      ViewDoc5Name.Options.Editing:=True;
      ViewDoc5Name.Focused:=True;
    end;
end;

procedure TfmAddDoc5.acSetPriceExecute(Sender: TObject);
Var rPriceM,rRemn:Real;
    iD:INteger;
    iC:INteger;
    rQuantC,rSumC,rSumRSC,rSumRss,rSumFss:Real;
    rQuantC1,rSumC1,rSumRSC1:Real;
    ArrDeps:array[1..100] of integer;
    n:INteger;
    LastInvD:TDateTime; //���� ��������� ������ ��������������
begin
//����������� �������
//  if cxButton1.Enabled then
//  begin
    iCol:=0;
//    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);
    Memo1.Lines.Add('  - ����� '+its(taSpecInv.RecordCount)); delay(10);
//    prButton(False);
    ViewDoc5.BeginUpdate;
    dstaSpecInv.DataSet:=nil;

    with dmMt do
    begin
      ptInventryHead.Active:=False; ptInventryHead.Active:=True;
      ptInvLine1.Active:=False; ptInvLine1.Active:=True;
      ptInvLine1.IndexFieldNames:='IDH;ITEM';
      ptInvLine.Active:=False; ptInvLine.Active:=True;
      ptInvLine.IndexFieldNames:='Depart;Inventry;Item;Price';
    end;


    iD:=0;
    if cxLookupComboBox1.EditValue>0 then iD:=cxLookupComboBox1.EditValue;
    if cxCheckBox2.Checked then iD:=0;

    if iD=0 then
    begin
      for n:=1 to 100 do ArrDeps[n]:=0;
      with dmMT do
      begin
        n:=1;
        if ptDep.Active=False then ptDep.Active:=True;
        ptDep.First;
        while not ptDep.Eof do
        begin
          if ptDepV01.AsInteger>0 then
          begin
            ArrDeps[n]:=ptDepID.AsInteger;
            inc(n);
          end;
          ptDep.Next;
        end;
      end;
    end;

    if (iD>0)and(cxCheckBox3.Checked) then  //���������� ����� � �������� ����  -  ������� �������������� ������
    begin
      //1. ���� ���� ������ ��������������
      with dmE do
      begin
        quLastFullInv.Active:=False;
        quLastFullInv.ParamByName('IDEP').AsInteger:=iD;
        quLastFullInv.ParamByName('DDATE').AsDateTime:=cxDateEdit1.Date;
        quLastFullInv.Active:=True;

        LastInvD:=39448; //01.01.2008
        if quLastFullInv.RecordCount>0 then
        begin
          quLastFullInv.First;
          LastInvD:=quLastFullInvDateInventry.AsDateTime;
        end;
        quLastFullInv.Active:=False;

        //�������� ����� �� ���� �������������� ��������������� ������

        quSumCorr.Active:=False;
        quSumCorr.SQL.Clear;
        quSumCorr.SQL.Add('select ITEM,SUM(QUANTR-QUANTF) as RQUANTC,SUM((QUANTR-QUANTF)*PRICER)as RSUMC,SUM(SUMRSS-SUMFSS)as RSUMRSC from "A_INVLN"');
        quSumCorr.SQL.Add('where IDH in');
        quSumCorr.SQL.Add('(Select Inventry from "InventryHead"');
        quSumCorr.SQL.Add('where');
        quSumCorr.SQL.Add('Depart='+its(iD));
        quSumCorr.SQL.Add('and Status=''�''');
        quSumCorr.SQL.Add('and Detail<>0');
        quSumCorr.SQL.Add('and DateInventry<'''+ds(cxDateEdit1.Date)+'''');
        quSumCorr.SQL.Add('and DateInventry>'''+ds(LastInvD)+'''');
        quSumCorr.SQL.Add('and xDopStatus=1');
        quSumCorr.SQL.Add('and Item=0)');
        quSumCorr.SQL.Add('group by ITEM');

        Memo1.Lines.Add('   - ������������ �������������� ������ � '+ds(LastInvD+1)+' �� '+ds(cxDateEdit1.Date-1)+' ���.'); delay(10);
        try
          closete(teCorr);
          quSumCorr.Active:=True;
          quSumCorr.First;
          while not quSumCorr.EOF do
          begin
            if (quSumCorrRQUANTC.AsFloat<>0)or(quSumCorrRSUMC.AsFloat<>0)or(quSumCorrRSUMRSC.AsFloat<>0) then
            begin
              teCorr.Append;
              teCorrItem.AsInteger:=quSumCorrITEM.AsInteger;
              teCorrRQuantC.AsFloat:=quSumCorrRQUANTC.AsFloat;
              teCorrRSumC.AsFloat:=quSumCorrRSUMC.AsFloat;
              teCorrRSumRSC.AsFloat:=quSumCorrRSUMRSC.AsFloat;
              teCorr.Post;
            end;

            quSumCorr.Next;
          end;
        finally
          Memo1.Lines.Add('   - ������������ ��������.'); delay(10);
          Memo1.Lines.Add('   - ������ ��������.'); delay(10);
          quSumCorr.Active:=False;
        end;
      end;
    end;

    taSpecInv.First;   iC:=0;
    while not taSpecInv.Eof do
    begin
//      rPriceM:=prFindPrice(taSpecInvCodeTovar.AsInteger);

      rPriceM:=taSpecInvPrice.AsFloat;
      if rPriceM=0 then rPriceM:=prFindPrice(taSpecInvCodeTovar.AsInteger);

      if (taSpecInvCodeTovar.AsInteger>0) then
      begin
        rRemn:=prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date))
      end else rRemn:=0;

      rQuantC:=0;
      rSumC:=0;
      rSumRSC:=0;

//      if (ViewDoc5.tag=1)and(cxCheckBox3.Checked) then //��� ������������ � ������������� � ���� � ��
      if (cxCheckBox3.Checked) then //��� ������������ � ������������� � ���� � ��
      begin // ����� ������� ���������
//        rQuantC:=0; rSumC:=0; rSumRSC:=0;
        //����� ����� ����� �������� � �������� ������� �� ����� � ��
        if iD>0 then
        begin
          rQuantC:=0; rSumC:=0; rSumRSC:=0;
          if teCorr.Locate('Item',taSpecInvCodeTovar.AsInteger,[]) then
          begin
            rQuantC:=teCorrRQuantC.AsFloat;
            rSumC:=teCorrRSumC.AsFloat;
            rSumRSC:=teCorrRSumRSC.AsFloat;
          end;

//          prFindCorrSum(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date),rQuantC,rSumC,rSumRSC,nil)
        end
        else   //�� ���������� �������
        begin
          for n:=1 to 100 do
          begin
            if ArrDeps[n]>0 then
            begin
              prFindCorrSum(taSpecInvCodeTovar.AsInteger,ArrDeps[n],Trunc(cxDateEdit1.Date),rQuantC1,rSumC1,rSumRSC1,nil);

              rQuantC:=rQuantC+rQuantC1;
              rSumC:=rSumC+rSumC1;
              rSumRSC:=rSumRSC+rSumRSC1;
            end;
          end;
        end;
      end;

      rSumRss:=taSpecInvSumRSS.AsFloat;
      rSumFss:=taSpecInvSumFSS.AsFloat;


      taSpecInv.Edit;
      taSpecInvPrice.AsFloat:=rPriceM;
      taSpecInvQuantR.AsFloat:=RE(rREmn);
      taSpecInvSumR.AsFloat:=rv(rPriceM*rREmn);
      taSpecInvSumF.AsFloat:=rv(rPriceM*taSpecInvQuantF.AsFloat);

      taSpecInvQuantC.AsFloat:=rQuantC;
      taSpecInvSumC.AsFloat:=rSumC;
      taSpecInvSumRI.AsFloat:=rv(rPriceM*rREmn)+rSumC;

      taSpecInvQuantD.AsFloat:=RE(taSpecInvQuantF.AsFloat)-RE(rREmn)-rQuantC;
      taSpecInvSumD.AsFloat:=rv((RE(taSpecInvQuantF.AsFloat)-RE(rREmn))*rPriceM-rSumC);

     //�������������� �������� �� �������������
      taSpecInvSumRSC.AsFloat:=rSumRSC;
      taSpecInvSumRSSI.AsFloat:=rSumRss+rSumRSC;
      taSpecInvSumDSS.AsFloat:=rSumFss-rSumRss-rSumRSC;

      taSpecInv.Post;

      taSpecInv.Next; inc(iC);

      if iC mod 10 = 0 then
      begin
        StatusBar1.Panels[0].Text:=its(iC);
        delay(10);
      end;
    end;
    taSpecInv.First;

    teCorr.Close;

    dstaSpecInv.DataSet:=taSpecInv;
    ViewDoc5.EndUpdate;
//    prButton(True);
    Memo1.Lines.Add('������ �������� ��������.'); delay(10);
//  end;

end;

procedure TfmAddDoc5.cxLabel7Click(Sender: TObject);
begin
  GridDoc5.SetFocus;
  acCalcPrice.Execute;
end;

procedure TfmAddDoc5.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc5.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc5.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc5.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc5.Columns[j].Name='ViewDoc5CodeTovar' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc5.taSpecInv1QuantFChange(Sender: TField);
begin
  //����� ����
  if iCol=1 then
  begin
    taSpecInvSumF.AsFloat:=taSpecInvPrice.AsFloat*taSpecInvQuantF.AsFloat;
    taSpecInvQuantD.AsFloat:=taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat;
    taSpecInvSumD.AsFloat:=(taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat)*taSpecInvPrice.AsFloat;
  end;
end;

procedure TfmAddDoc5.taSpecInv1PriceChange(Sender: TField);
begin
  if iCol=2 then
  begin
    taSpecInvSumR.AsFloat:=taSpecInvPrice.AsFloat*taSpecInvQuantR.AsFloat;
    taSpecInvSumF.AsFloat:=taSpecInvPrice.AsFloat*taSpecInvQuantF.AsFloat;
    taSpecInvQuantD.AsFloat:=taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat;
    taSpecInvSumD.AsFloat:=(taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat)*taSpecInvPrice.AsFloat;
  end;
end;

procedure TfmAddDoc5.acGenExecute(Sender: TObject);
Var rRemn:Real;
    iC,iD:INteger;
    sDeps:String;
    dLastD:tDateTime;
    iDifDay:INteger;
begin
  //������������
  if cxButton1.Enabled then
  begin
    iCol:=0;
    StatusBar1.Panels[0].Text:='';
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ��������� ��������.'); delay(10);
    prButton(False);
    ViewDoc5.BeginUpdate;
    dstaSpecInv.DataSet:=nil;
    with dmMC do
    with dmMT do
    begin
      iD:=cxLookupComboBox1.EditValue;
      sDeps:=its(Id);

      if cxCheckBox2.Checked then
      begin
//        sDeps:='5,13,14';
        sDeps:='';
        ptDep.Active:=False;
        ptDep.Active:=True;
        ptDep.First;
        while not ptDep.Eof do
        begin
          if ptDepV01.AsInteger>0 then sDeps:=sDeps+','+its(ptDepID.AsInteger);
          ptDep.Next;
        end;
        if sDeps>'' then delete(sDeps,1,1);
        if sDeps='' then sDeps:=its(iD) else Id:=0;
      end;

      quCGInv.Active:=False;
      quCGInv.SQL.Clear;
      quCGInv.SQL.Add('select');
      quCGInv.SQL.Add('cg.CARD,');
      quCGInv.SQL.Add('cg.ITEM,');
      quCGInv.SQL.Add('gd."Cena",');
      quCGInv.SQL.Add('gd."Name"');
      quCGInv.SQL.Add('from "CardGoods" cg');
      quCGInv.SQL.Add('left join "Goods" gd on gd."ID"=cg."ITEM"');
      quCGInv.SQL.Add('where cg.DEPART in ('+sDeps+')');
      quCGInv.SQL.Add('and gd."Status"<100');

      Memo1.Lines.Add('  ������������ ������...'); delay(10);
      quCGInv.Active:=True;
      Memo1.Lines.Add('  Ok. ('+its(quCGInv.RecordCount)+')'); delay(10);

      Memo1.Lines.Add('  ����� ... ���� ������ ��������.'); delay(10);
      iC:=0;
      quCGInv.First;
      while not quCGInv.Eof do
      begin
//        rRemn:=prFindRemn4(quCGInvCARD.AsInteger,Trunc(cxDateEdit1.Date));
        rRemn:=prFindTRemnDepD(quCGInvITEM.AsInteger,iD,Trunc(cxDateEdit1.Date));
        iDifDay:=1000;
        if abs(rRemn)<0.001 then      //������� ������ � ����, �� ���� ��������� ��� ���� �� �������� �� ���������� ��������� �������
        begin

          if cxCheckBox2.Checked then prFindLastDateRemn(quCGInvITEM.AsInteger,dLastD)
          else prFindLastDateRemnDep(quCGInvITEM.AsInteger,iD,dLastD);

          iDifDay:=Trunc(cxDateEdit1.Date-dLastD);
        end;

        if (abs(rRemn)>0.001)or(iDifDay<220) then  //���� ���� �������� ������ ��� 220 ���� ����� �� ������� �������� � ��������������.
        begin
          if taSpecInv.Locate('CodeTovar',quCGInvITEM.AsInteger,[]) then
          begin
            taSpecInv.Edit;
            taSpecInvPrice.AsFloat:=quCGInvCena.AsFloat;
            taSpecInvQuantR.AsFloat:=rREmn;
            taSpecInvSumR.AsFloat:=quCGInvCena.AsFloat*rREmn;
            taSpecInvSumF.AsFloat:=quCGInvCena.AsFloat*taSpecInvQuantF.AsFloat;
            taSpecInvQuantD.AsFloat:=taSpecInvQuantF.AsFloat-rREmn;
            taSpecInvSumD.AsFloat:=(taSpecInvQuantF.AsFloat-rREmn)*quCGInvCena.AsFloat;
            taSpecInv.Post;
          end else
          begin
            taSpecInv.Append;
            taSpecInvCodeTovar.AsInteger:=quCGInvITEM.AsInteger;
            taSpecInvName.AsString:=quCGInvName.AsString;
            taSpecInvNDSProc.AsFloat:=18;
            taSpecInvPrice.AsFloat:=quCGInvCena.AsFloat;
            taSpecInvQuantR.AsFloat:=rREmn;
            taSpecInvSumR.AsFloat:=quCGInvCena.AsFloat*rREmn;
            taSpecInvQuantF.AsFloat:=0;
            taSpecInvSumF.AsFloat:=0;
            taSpecInvQuantD.AsFloat:=(-1)*rREmn;
            taSpecInvSumD.AsFloat:=(-1)*quCGInvCena.AsFloat*rREmn;
            taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
            taSpecInv.Post;
          end;
        end;

        quCGInv.Next; inc(iC); StatusBar1.Panels[0].Text:=its(iC);
        delay(10);
      end;
      Memo1.Lines.Add('  Ok.'); delay(10);
      quCGInv.Active:=False;
    end;
    taSpecInv.First;
    dstaSpecInv.DataSet:=taSpecInv;
    ViewDoc5.EndUpdate;
    prButton(True);
    Memo1.Lines.Add('��������� ��.'); delay(10);
  end;
end;

procedure TfmAddDoc5.acInputFExecute(Sender: TObject);
begin
  //��������� ���� �� �����
  fmInvExport.Caption:='��������� ���� �� �����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=1;
  fmInvExport.Tag:=0;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.acExportExExecute(Sender: TObject);
begin
  prNExportExel5(ViewDoc5);
end;

procedure TfmAddDoc5.acOutPutFExecute(Sender: TObject);
begin
  //��������� ���� � ����
  fmInvExport.Caption:='��������� ���� � ����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=0;
  fmInvExport.Tag:=0;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.N4Click(Sender: TObject);
begin
  ViewDoc5.DataController.Groups.FullExpand;
//  ViewReal.DataController.Groups.FullCollapse;
end;

procedure TfmAddDoc5.N5Click(Sender: TObject);
begin
  ViewDoc5.DataController.Groups.FullCollapse;
end;

procedure TfmAddDoc5.acInputFileExecute(Sender: TObject);
begin
  //��������� ���� �� �����
  fmInvExport.Caption:='��������� ��� �� �����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=2;
  fmInvExport.Tag:=0;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.acRecalcRemnPosExecute(Sender: TObject);
Var rRemn:Real;
begin
// ����������� ������� �� �������
  if cxButton1.Enabled then
  begin
    iCol:=0;
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ �������� �� �������.'); delay(10);
    prButton(False);
    if (cxLookupComboBox1.EditValue>0)and(taSpecInvCodeTovar.AsInteger>0)
    then rRemn:=prFindRemn3(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue)
    else rRemn:=0;

    taSpecInv.Edit;
    taSpecInvQuantR.AsFloat:=rREmn;
    taSpecInvSumR.AsFloat:=taSpecInvPrice.AsFloat*rREmn;
    taSpecInvSumF.AsFloat:=taSpecInvPrice.AsFloat*taSpecInvQuantF.AsFloat;
    taSpecInvQuantD.AsFloat:=taSpecInvQuantF.AsFloat-rREmn;
    taSpecInvSumD.AsFloat:=(taSpecInvQuantF.AsFloat-rREmn)*taSpecInvPrice.AsFloat;
    taSpecInv.Post;

    prButton(True);
    Memo1.Lines.Add('������ ��������.'); delay(10);
  end;
end;

procedure TfmAddDoc5.cxButton5Click(Sender: TObject);
begin
  taSpecInv.Locate('CodeTovar',StrToIntDef(cxTextEdit2.Text,0),[]);
//  ViewDoc5QuantF.Focused:=True;
  GridDoc5.SetFocus;
  ViewDoc5QuantF.Focused:=True;
// ViewDoc5.OptionsSelection.
// ViewDoc5QuantF.FocusWithSelection;
end;

procedure TfmAddDoc5.acSetSerchExecute(Sender: TObject);
begin
  cxTextEdit2.SetFocus;
  cxTextEdit2.SelectAll;
end;

procedure TfmAddDoc5.cxButton3Click(Sender: TObject);
//������
Var vPP:TfrPrintPages;
    rSum1,rSum2:Real;  //,rSum3
begin
  try
    ViewDoc5.BeginUpdate;
    taSpecInv.IndexName:='taSpecInvIndex2';
    rSum1:=0; rSum2:=0; //rSum3:=0;
    taSpecInv.First;
    if (cxCheckBox5.Checked=False)and(cxCheckBox6.Checked=False) then
    begin
      while not taSpecINv.Eof do
      begin
        rSum1:=rSum1+rv(taSpecInvSumRI.AsFloat);
        rSum2:=rSum2+rv(taSpecInvSumF.AsFloat);

        taSpecInv.Next;
      end;
    end else
    begin
      while not taSpecINv.Eof do
      begin
        if cxCheckBox5.Checked then
        begin
          rSum1:=rSum1+rv(taSpecInvSumRSS.AsFloat);
          rSum2:=rSum2+rv(taSpecInvSumFSS.AsFloat);
        end;
        if cxCheckBox6.Checked then
        begin
          rSum1:=rSum1+rv(taSpecInvSumRTO.AsFloat);
          rSum2:=rSum2+rv(taSpecInvSumFSS.AsFloat);
        end;

        taSpecInv.Next;
      end;
    end;

    if (cxCheckBox5.Checked=False)and(cxCheckBox6.Checked=False)and(cxCheckBox7.Checked=False) then
      frRepDINV.LoadFromFile(CommonSet.Reports + 'Inventry1.frf')
    else
    begin
      if cxCheckBox5.Checked then frRepDINV.LoadFromFile(CommonSet.Reports + 'Inventryss.frf');
      if cxCheckBox6.Checked then frRepDINV.LoadFromFile(CommonSet.Reports + 'Inventryto.frf');
      if cxCheckBox7.Checked then frRepDINV.LoadFromFile(CommonSet.Reports + 'InventryUni.frf');
    end;

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
    frVariables.Variable['SumR']:=rSum1;
    frVariables.Variable['SumF']:=rSum2;
    frVariables.Variable['SumD']:=rSum2-rSum1;
    frVariables.Variable['SSumR']:=MoneyToString(abs(rSum1),True,False);
    frVariables.Variable['SSumF']:=MoneyToString(abs(rSum2),True,False);
    frVariables.Variable['SSumD']:=MoneyToString(abs(rSum2-rSum1),True,False);
    frVariables.Variable['Filial']:=fShopName;
  
    if cxCheckBox4.Checked then frVariables.Variable['PrintM']:=False else frVariables.Variable['PrintM']:=True;

    frRepDINV.ReportName:='������������������ ���������.';
    frRepDINV.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDINV.ShowPreparedReport
    else frRepDINV.PrintPreparedReport('',1,False,vPP);
  finally
    taSpecInv.IndexName:='taSpecInvIndex1';
    ViewDoc5.EndUpdate;
  end;
end;

procedure TfmAddDoc5.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc5);
end;

procedure TfmAddDoc5.taSpecInvQuantFChange(Sender: TField);
begin
  if taSpecInvQuantF.AsFloat<0 then taSpecInvQuantF.AsFloat:=0;
  taSpecInvSumF.AsFloat:=rv(taSpecInvPrice.AsFloat*taSpecInvQuantF.AsFloat);
  taSpecInvQuantD.AsFloat:=re(taSpecInvQuantF.AsFloat)-re(taSpecInvQuantR.AsFloat);
  taSpecInvSumD.AsFloat:=rv((re(taSpecInvQuantF.AsFloat)-re(taSpecInvQuantR.AsFloat))*taSpecInvPrice.AsFloat);
end;

procedure TfmAddDoc5.acTermFileExecute(Sender: TObject);
Var f:TextFile;
    Str1,Str2:String;
    iC:Integer;
begin
 //���������� ����� ��� ���������
  iC:=0;
  with dmMc do
  begin
    try
      ViewDoc5.BeginUpdate;

      assignfile(f,CurDir+'Referenc.dat');
      rewrite(f);

      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        quBars.Active:=False;
        quBars.ParamByName('GOODSID').AsInteger:=taSpecInvCodeTovar.AsInteger;
        quBars.Active:=True;
        quBars.First;
        while not quBars.Eof do
        begin

          Str1:=quBarsID.AsString;
          while Length(Str1)<13 do Str1:=Str1+' ';
          Str1:=Str1+its(taSpecInvCodeTovar.AsInteger);
          while Length(Str1)<18 do Str1:=Str1+' ';

          Str2:=FloatToStr(RV(taSpecInvPrice.AsFloat));
          while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
          Str1:=Str1+Str2;
          while Length(Str1)<27 do Str1:=Str1+' ';

          Str1:=Str1+Copy(AnsiToOemConvert(taSpecInvName.AsString),1,22);
          while Length(Str1)<49 do Str1:=Str1+' ';

          writeln(f,Str1);

          inc(iC);

          quBars.Next;
        end;

        quBars.Active:=False;
        taSpecInv.Next;
      end;
    finally
      ViewDoc5.EndUpdate;
      CloseFile(f);
      ShowMessage('�������� � ���� - '+IntToStr(iC)+' ����������.');
    end;
  end;
end;

procedure TfmAddDoc5.acEqualExecute(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    ViewDoc5.BeginUpdate;

    taSpecInv.First;
    while not taSpecInv.Eof do
    begin
      if abs(taSpecInvQuantF.AsFloat)<0.001 then
      begin
        taSpecInv.Edit;
        if taSpecInvQuantR.AsFloat>0 then taSpecInvQuantF.AsFloat:=taSpecInvQuantR.AsFloat
        else taSpecInvQuantF.AsFloat:=0;
        taSpecInv.Post;
      end;

      taSpecInv.Next;
    end;
    taSpecInv.First;
    ViewDoc5.EndUpdate;
  end;
end;

procedure TfmAddDoc5.acTestRemnExecute(Sender: TObject);
Var rRemn:Real;
    iD:INteger;
    iC:INteger;
begin
//����������� �������

  iCol:=0;
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);
  prButton(False);
  ViewDoc5.BeginUpdate;
  taSpecInv.First;   iC:=0;
  iD:=0;
  if cxLookupComboBox1.EditValue>0 then iD:=cxLookupComboBox1.EditValue;

  while not taSpecInv.Eof do
  begin

    if (taSpecInvCodeTovar.AsInteger>0) then
    begin
      if cxCheckBox2.Checked then iD:=0;
      rRemn:=prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date))
    end else rRemn:=0;

    if abs(rRemn-taSpecInvQuantF.AsFloat)>0.0001 then
    begin
      Memo1.Lines.Add(its(taSpecInvCodeTovar.AsInteger)+';'+fs(rRemn)+';'+fs(taSpecInvQuantF.AsFloat)+';'+fs(prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,iD,Trunc(Date))));
//    ����� ��������� �������������� �� ��������
      prDelTMoveId(iD,taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxTextEdit1.Tag,20,0);
      rRemn:=prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date));
      if abs(taSpecInvQuantF.AsFloat-rRemn)>0.0001 then
        prAddTMoveId(iD,taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxTextEdit1.Tag,20,(taSpecInvQuantF.AsFloat-rRemn));
    end else //������� ����, �� ����� ��������� ������ ������ � ��������
    begin
      prDelTMoveId(iD,taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxTextEdit1.Tag,20,0);
      rRemn:=prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date));
      prAddTMoveId(iD,taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxTextEdit1.Tag,20,RoundEx((taSpecInvQuantF.AsFloat-rRemn)*1000)/1000);
    end;

    taSpecInv.Next; inc(iC);

    if iC mod 10 = 0 then
    begin
      StatusBar1.Panels[0].Text:=its(iC);
      delay(10);
    end;
  end;
  taSpecInv.First;
  ViewDoc5.EndUpdate;
  prButton(True);
  Memo1.Lines.Add('������ �������� ��������.'); delay(10);
end;

procedure TfmAddDoc5.acRecalcRemnInvExecute(Sender: TObject);
Var rPriceM,rRemn:Real;
    iD:INteger;
    iC:INteger;
begin
//����������� �������  ��� ����� ��������������
  if cxButton1.Enabled then
  begin
    iCol:=0;
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);
    Memo1.Lines.Add('  - ����� '+its(taSpecInv.RecordCount)); delay(10);
    ViewDoc5.BeginUpdate;
    dstaSpecInv.DataSet:=nil;

    iD:=0;
    if cxLookupComboBox1.EditValue>0 then iD:=cxLookupComboBox1.EditValue;
    if cxCheckBox2.Checked then iD:=0;

    taSpecInv.First;   iC:=0;
    while not taSpecInv.Eof do
    begin
//      rPriceM:=prFindPrice(taSpecInvCodeTovar.AsInteger);

      rPriceM:=taSpecInvPrice.AsFloat;

      if (taSpecInvCodeTovar.AsInteger>0) then
      begin
        rRemn:=prFindTRemnDepDInv(taSpecInvCodeTovar.AsInteger,iD,Trunc(cxDateEdit1.Date))
      end else rRemn:=0;


      taSpecInv.Edit;
//      taSpecInvPrice.AsFloat:=rPriceM;
      taSpecInvQuantR.AsFloat:=RE(rREmn);
      taSpecInvSumR.AsFloat:=rv(rPriceM*rREmn);
      taSpecInvSumF.AsFloat:=rv(rPriceM*taSpecInvQuantF.AsFloat);
      taSpecInvQuantD.AsFloat:=RE(taSpecInvQuantF.AsFloat)-RE(rREmn);
      taSpecInvSumD.AsFloat:=rv((RE(taSpecInvQuantF.AsFloat)-RE(rREmn))*rPriceM);
      taSpecInv.Post;

      taSpecInv.Next; inc(iC);

      if iC mod 10 = 0 then
      begin
        StatusBar1.Panels[0].Text:=its(iC);
        delay(10);
      end;
    end;
    taSpecInv.First;
    dstaSpecInv.DataSet:=taSpecInv;
    ViewDoc5.EndUpdate;
    Memo1.Lines.Add('������ �������� ��������.'); delay(10);
  end;
end;

procedure TfmAddDoc5.acSaveDoc1Execute(Sender: TObject);
Var
    rSum1,rSum2,rSumQ1,rSumQ2:Real;
    IdH:INteger;
    StrWk:String;
    iC:INteger;
begin
  //���������� �� ������ ����
  //��������  - �������
  with dmMC do
  with dmMT do
  begin
    StatusBar1.Panels[0].Text:='';
    StatusBar1.Panels[1].Text:='';

    if cxDateEdit1.Date<=prOpenDate then
    begin
      ShowMessage('������ ������.');
      exit;
    end;

    prButton(False);
    ViewDoc5.BeginUpdate;
    Memo1.Clear;
    Memo1.Lines.Add('����� ���� ���������� ���������.');
    delay(10);

    idh:=0;

    if taSpecInv.State in [dsEdit,dsInsert] then taSpecInv.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "InventryHead"');
      quC.SQL.Add('where Depart='+its(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInventry='''+ds(cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='''+cxTextEdit1.Text+'''');
      quC.Active:=True;
      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        prButton(True);
        exit;
      end;
    end;
    try
      //������� ������ ����

      Memo1.Lines.Add('  - ����������.'); delay(10);

      CloseTe(teSp);

      rSum1:=0; rSum2:=0; rSumQ1:=0; rSumQ2:=0;
      iCol:=0; iC:=0;
      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        teSp.Append;
        teSpItem.AsInteger:=taSpecInvCodeTovar.AsInteger;
        teSpPrice.AsFloat:=taSpecInvPrice.AsFloat;
        teSpQuantR.AsFloat:=taSpecInvQuantR.AsFloat;
        teSpQuantF.AsFloat:=taSpecInvQuantF.AsFloat;
        teSpNum.AsInteger:=taSpecInvNum.AsInteger;
        teSp.Post;


      //������������ ��� ������� � �����������

        rSum1:=rSum1+rv(taSpecInvSumR.AsFloat);
        rSum2:=rSum2+rv(taSpecInvSumF.AsFloat);
        rSumQ1:=rSumQ1+taSpecInvQuantR.AsFloat;
        rSumQ2:=rSumQ2+taSpecInvQuantF.AsFloat;

        taSpecInv.Next;  inc(iC);

        if iC mod 100 =0 then
        begin
          StatusBar1.Panels[0].Text:=its(iC);
          delay(10);
        end;
      end;

      if cxTextEdit1.Tag=0 then
      begin
        IDH:=prMax('DocsInv')+1;
      end else //��� �� ����������
      begin
        IDH:=cxTextEdit1.Tag;
{
        quD.SQL.Clear;
        quD.SQL.Add('delete from "InventryLine"');
        quD.SQL.Add('where Depart='+its(cxDateEdit10.Tag));
        quD.SQL.Add('and Inventry='+its(cxTextEdit1.Tag*(-1)));
        quD.ExecSQL;

        // ��������� ������ ������������ �� 100 ��� ����� �� ������ ������

        quE.SQL.Clear;
        quE.SQL.Add('Update "InventryLine" set');
        quE.SQL.Add('Inventry='+its(cxTextEdit1.Tag*(-1)));
        quE.SQL.Add('where Depart='+its(cxDateEdit10.Tag));
        quE.SQL.Add('and Inventry='+its(cxTextEdit1.Tag));
        quE.ExecSQL;

//    ������  ������� ����� ���� ����

        quD.SQL.Clear;
        quD.SQL.Add('delete from "InventryLine"');
        quD.SQL.Add('where Depart='+its(cxDateEdit10.Tag));
        quD.SQL.Add('and Inventry='+its(cxTextEdit1.Tag));
        quD.ExecSQL;
}
        delay(200);
      end;

      Memo1.Lines.Add('  - ���������.'); delay(10);

      //����� �������� ���������
      if cxTextEdit1.Tag=0 then //����������
      begin
        quA.Active:=False;
        StrWk:='';
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "InventryHead" values (');

        StrWk:=StrWk+(its(idh)+',');//Inventry
        StrWk:=StrWk+(its(cxLookupComboBox1.EditValue)+',');//Depart
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateInventry
        StrWk:=StrWk+(''''+cxTextEdit1.Text+''',');//Number
        StrWk:=StrWk+(its(cxComboBox1.ItemIndex)+',');//CardIndex
        StrWk:=StrWk+(its(cxComboBox2.ItemIndex)+',');//InputMode
        StrWk:=StrWk+(its(cxComboBox3.ItemIndex)+',');//PriceMode
        StrWk:=StrWk+(its(cxComboBox4.ItemIndex)+',');//Detail
        StrWk:=StrWk+('0,');//GGroup
        StrWk:=StrWk+('0,');//SubGroup
        if cxCheckBox3.Checked then StrWk:=StrWk+('1,') //Item
        else StrWk:=StrWk+('0,');
        StrWk:=StrWk+('0,');//CountLost
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateLost
        StrWk:=StrWk+(fs(rSumQ1)+',');//QuantRecord
        StrWk:=StrWk+(fs(rSum1)+',');//SumRecord
        StrWk:=StrWk+(fs(rSumQ2)+',');//QuantActual
        StrWk:=StrWk+(fs(rSum2)+',');//SumActual
        StrWk:=StrWk+('0,');//NumberOfLines
        StrWk:=StrWk+(''''+ds(cxDateEdit1.Date)+''',');//DateStart
        StrWk:=StrWk+('''23:59:59'',');//TimeStart
        StrWk:=StrWk+(''''',');//President
        StrWk:=StrWk+(''''',');//PresidentPost
        StrWk:=StrWk+(''''',');//Member1
        StrWk:=StrWk+(''''',');//Member1_Post
        StrWk:=StrWk+(''''',');//Member2
        StrWk:=StrWk+(''''',');//Member2_Post
        StrWk:=StrWk+(''''',');//Member3
        StrWk:=StrWk+(''''',');//Member3_Post
        StrWk:=StrWk+('1,');//xDopStatus
        StrWk:=StrWk+(''''',');//Response1
        StrWk:=StrWk+(''''',');//Response1_Post
        StrWk:=StrWk+(''''',');//Response2
        StrWk:=StrWk+(''''',');//Response2_Post
        StrWk:=StrWk+(''''',');//Response3
        StrWk:=StrWk+(''''',');//Response3_Post
        StrWk:=StrWk+(''''',');//Reason
        StrWk:=StrWk+(''''',');//CASHER
        StrWk:=StrWk+('''�'',');//Status
        StrWk:=StrWk+('0,');//LinkInvoice
        StrWk:=StrWk+('0,');//StartTransfer
        StrWk:=StrWk+('0,');//EndTransfer
        StrWk:=StrWk+('''''');//Rezerv

        quA.SQL.Add(StrWk);

        quA.SQL.Add(')');
        quA.ExecSQL;

      end else //����������
      begin
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('Update "InventryHead" set');

        quA.SQL.Add('Inventry='+its(cxTextEdit1.Tag)+',');//Inventry
        quA.SQL.Add('Depart='+its(cxLookupComboBox1.EditValue)+',');//Depart
        quA.SQL.Add('DateInventry='+''''+ds(cxDateEdit1.Date)+''',');//DateInventry
        quA.SQL.Add('Number='+''''+cxTextEdit1.Text+''',');//Number
        quA.SQL.Add('CardIndex='+its(cxComboBox1.ItemIndex)+',');//CardIndex
        quA.SQL.Add('InputMode='+its(cxComboBox2.ItemIndex)+',');//InputMode
        quA.SQL.Add('PriceMode='+its(cxComboBox3.ItemIndex)+',');//PriceMode
        quA.SQL.Add('Detail='+its(cxComboBox4.ItemIndex)+',');//Detail
        quA.SQL.Add('GGroup='+'0,');//GGroup
        quA.SQL.Add('SubGroup='+'0,');//SubGroup

        if cxCheckBox3.Checked then quA.SQL.Add('Item='+'1,') //Item
        else quA.SQL.Add('Item='+'0,');//Item

        quA.SQL.Add('CountLost=0,');//CountLost
        quA.SQL.Add('DateLost='+''''+ds(cxDateEdit1.Date)+''',');//DateLost
        quA.SQL.Add('QuantRecord='+fs(rSumQ1)+',');//QuantRecord
        quA.SQL.Add('SumRecord='+fs(rSum1)+',');//SumRecord
        quA.SQL.Add('QuantActual='+fs(rSumQ2)+',');//QuantActual
        quA.SQL.Add('SumActual='+fs(rSum2)+',');//SumActual
        quA.SQL.Add('NumberOfLines='+'0,');//NumberOfLines
        quA.SQL.Add('DateStart='+''''+ds(cxDateEdit1.Date)+''',');//DateStart
        quA.SQL.Add('TimeStart='+'''23:59:59'',');//TimeStart
        quA.SQL.Add('President='+''''',');//President
        quA.SQL.Add('PresidentPost='+''''',');//PresidentPost
        quA.SQL.Add('Member1='+''''',');//Member1
        quA.SQL.Add('Member1_Post='+''''',');//Member1_Post
        quA.SQL.Add('Member2='+''''',');//Member2
        quA.SQL.Add('Member2_Post='+''''',');//Member2_Post
        quA.SQL.Add('Member3='+''''',');//Member3
        quA.SQL.Add('Member3_Post='+''''',');//Member3_Post
        quA.SQL.Add('xDopStatus='+'1,');//xDopStatus
        quA.SQL.Add('Response1='+''''',');//Response1
        quA.SQL.Add('Response1_Post='+''''',');//Response1_Post
        quA.SQL.Add('Response2='+''''',');//Response2
        quA.SQL.Add('Response2_Post='+''''',');//Response2_Post
        quA.SQL.Add('Response3='+''''',');//Response3
        quA.SQL.Add('Response3_Post='+''''',');//Response3_Post
        quA.SQL.Add('Reason='+''''',');//Reason
        quA.SQL.Add('CASHER='+''''',');//CASHER
        quA.SQL.Add('Status='+'''�'',');//Status
        quA.SQL.Add('LinkInvoice='+'0,');//LinkInvoice
        quA.SQL.Add('StartTransfer='+'0,');//StartTransfer
        quA.SQL.Add('EndTransfer='+'0,');//EndTransfer
        quA.SQL.Add('Rezerv='+'''''');//Rezerv
        quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quA.SQL.Add('and DateInventry='''+ds(cxDateEdit10.Date)+'''');
        quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quA.ExecSQL;
      end;
      //������� �� ������������ - ��� ������ ����������

      Memo1.Lines.Add('  - ������������.'); delay(10);

      ptInvLine1.Active:=False;
      ptInvLine1.Active:=True;
      ptInvLine1.CancelRange;
      ptInvLine1.SetRange([idh],[idh]);

      Memo1.Lines.Add('  - ���������.'); delay(10);

      iC:=0;
      ptInvLine1.First;
      while not ptInvLine1.Eof do
      begin
        if teSp.Locate('Item',ptInvLine1Item.AsInteger,[]) then
        begin
          ptInvLine1.Edit;
          ptInvLine1PriceR.AsFloat:=teSpPrice.AsFloat;
          ptInvLine1QuantR.AsFloat:=teSpQuantR.AsFloat;
          ptInvLine1QuantF.AsFloat:=teSpQuantF.AsFloat;
          ptInvLine1QUANTD.AsFloat:=teSpQuantF.AsFloat-teSpQuantR.AsFloat;
          ptInvLine1NUM.AsInteger:=teSpNum.AsInteger;
          ptInvLine1.Post;

          teSp.Delete;

          ptInvLine1.Next;
        end else ptInvLine1.Delete;

        inc(iC);
        if iC mod 50 =0 then
        begin
          StatusBar1.Panels[0].Text:=its(iC);
          delay(10);
        end;
      end;

      Memo1.Lines.Add('  - ����������.'); delay(10);
      iC:=0;
      teSp.First;
      while not teSp.Eof do
      begin
        ptInvLine1.Append;
        ptInvLine1IDH.AsInteger:=idh;
        ptInvLine1ITEM.AsInteger:=teSpItem.AsInteger;
        ptInvLine1PRICER.AsFloat:=teSpPrice.AsFloat;
        ptInvLine1QuantR.AsFloat:=teSpQuantR.AsFloat;
        ptInvLine1QuantF.AsFloat:=teSpQuantF.AsFloat;
        ptInvLine1QuantD.AsFloat:=teSpQuantF.AsFloat-teSpQuantR.AsFloat;
        ptInvLine1NUM.AsInteger:=teSpNum.AsInteger;
        ptInvLine1.Post;

        teSp.Next; inc(iC);
        if iC mod 50 =0 then
        begin
          StatusBar1.Panels[1].Text:=its(iC);
          delay(10);
        end;
      end;


      ptInvLine1.Active:=False;

      cxTextEdit1.Tag:=IDH;

      cxTextEdit10.Tag:=IDH;
      cxTextEdit10.Text:=cxTextEdit1.Text;
      cxDateEdit10.Date:=cxDateEdit1.Date;
      cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;
    finally
      teSp.Active:=False;
      prButton(True);

      fmDocs5.ViewDocsInv.BeginUpdate;
      quTTnInv.Active:=False;
      quTTnInv.Active:=True;
      quTTnInv.Locate('Inventry',idh,[]);
      fmDocs5.ViewDocsInv.EndUpdate;

      ViewDoc5.EndUpdate;
      Memo1.Lines.Add('���������� ��.');
    end;
  end;
end;

procedure TfmAddDoc5.acRecalcRemnSSExecute(Sender: TObject);
Var iC:Integer;
    rSumR,rSumF,rSumRSC,rQuantC:Real;
    iCode:Integer;
begin
//�������� �������� �� ��
//  Memo1.Clear;
  prWH('������ ������� � ����� ��.',Memo1);
  iC:=0; iCode:=0;
  ViewDoc5.BeginUpdate;
  try
    taSpecInv.First;
    while not taSpecInv.Eof do
    begin
      iCode:=taSpecInvCodeTovar.AsInteger;
      try
        prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantR.AsFloat,rSumR);

        if abs(taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat)<=0.001 then rSumF:=rSumR
        else prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantF.AsFloat,rSumF);

        rSumRSC:=taSpecInvSumRSC.AsFloat;
        rQuantC:=taSpecInvQuantC.AsFloat;

        taSpecInv.Edit;
        taSpecInvSumRSS.AsFloat:=rSumR;
        taSpecInvSumRSSI.AsFloat:=rSumR+rSumRSC;
        taSpecInvSumFSS.AsFloat:=rSumF;
        taSpecInvSumDSS.AsFloat:=rSumF-rSumR-rSumRSC;
        if abs(taSpecInvQuantR.AsFloat+rQuantC)>0.0001 then taSpecInvPriceSS.AsFloat:=(rSumR+rSumRSC)/(taSpecInvQuantR.AsFloat+rQuantC) else taSpecInvPriceSS.AsFloat:=0;
        taSpecInv.post;
      except
        prWH('   ������ ���� '+its(iC)+' '+its(iCode),Memo1);
      end;
      taSpecInv.Next; inc(iC);
      if iC mod 100=0 then
      begin
        prWH('   '+its(iC)+' '+its(iCode),Memo1);
        delay(100);
      end;
    end;
  finally
    prWH('  Last '+its(iC)+' '+its(iCode),Memo1);
    ViewDoc5.EndUpdate;
    prWH('������ �� ��.',Memo1);
  end;
end;

procedure TfmAddDoc5.acPartsExecute(Sender: TObject);
begin
  if not CanDo('prViewPartCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  if taSpecInv.RecordCount>0 then prFillPart(taSpecInvCodeTovar.AsInteger,taSpecInvName.AsString);
end;

procedure TfmAddDoc5.acCalcTOSumExecute(Sender: TObject);
Var iC:Integer;
    rSumR,rSumD:Real;
    DateB:INteger;
begin
//  Memo1.Clear;
  Memo1.Lines.Add('������ ������� � ����� �� �� ��.');
  ViewDoc5.BeginUpdate;
  try
    taSpecInv.First;
    iC:=0;

    while not taSpecInv.Eof do
    begin
{      prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantR.AsFloat,rSumR);
      if abs(taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat)<=0.001 then rSumF:=rSumR
      else prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantF.AsFloat,rSumF);
}
      DateB:=fTestInvBack(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))+1;
//      DateB:=0;

      prRecalcTOPos(taSpecInvCodeTovar.AsInteger,cxLookupComboBox1.EditValue,DateB,Trunc(cxDateEdit1.Date),Memo1,rSumR); //�������� ���� �� �� �� ��������� ���

      rSumD:=taSpecInvSumFSS.AsFloat-rSumR;

      taSpecInv.Edit;
      taSpecInvSumRTO.AsFloat:=rSumR;
      taSpecInvSumDTO.AsFloat:=rSumD;
      taSpecInv.post;

      taSpecInv.Next; inc(iC);
      if iC mod 100=0 then
      begin
        Memo1.Lines.Add('   '+its(iC));
        delay(100);
      end;
    end;
  finally
    ViewDoc5.EndUpdate;
    Memo1.Lines.Add('������ �� �� ��.');
  end;
end;

procedure TfmAddDoc5.frRepDINVUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
Var Str1:String;
begin
//  if AnsiCompareText('SUMSTR', Name) = 0 then val.My_Convertion_Routine(frParser.Calc(p1));
  Str1:=MoneyToString(frParser.Calc(p1),True,False);
  if AnsiCompareText('SUMSTR', Name) = 0 then val:=Str1;
  if AnsiCompareText('DIGITSTR', Name) = 0 then
  begin
    if POS('���',Str1)>0 then Str1:=Copy(Str1,1,POS('���',Str1)-1);
    val:=Str1;
  end;
end;


procedure TfmAddDoc5.acEqTOSSExecute(Sender: TObject);
Var iC:Integer;
begin
  Memo1.Clear;
  Memo1.Lines.Add('�������� ���� �� � ��. (������ ��� ��� ��������)');
  ViewDoc5.BeginUpdate;
  try
    dstaSpecInv.DataSet:=nil;
    taSpecInv.First;
    iC:=0;

    while not taSpecInv.Eof do
    begin
      taSpecInv.Edit;
      taSpecInvSumRTO.AsFloat:=taSpecInvSumFSS.AsFloat;
      taSpecInvSumDTO.AsFloat:=0;
      taSpecInv.post;

      taSpecInv.Next; inc(iC);
      if iC mod 100=0 then
      begin
        Memo1.Lines.Add('   '+its(iC));
        delay(10);
      end;
    end;
  finally
    dstaSpecInv.DataSet:=taSpecInv;
    ViewDoc5.EndUpdate;
    Memo1.Lines.Add('��.')
  end;
end;

procedure TfmAddDoc5.acCreatePP0Execute(Sender: TObject);
Var iDep,iC,iCliCB:Integer;
    rQs,rPr1,rQr,rPr0:Real;
    DateB,DateE:TDateTime;
    Id:INteger;
    Sinn:String;
    rPrIn0:Real;
begin
  //�������� ��������� ������
  if pos('�����',Person.Name)>0 then
  begin
    if MessageDlg('������� ��� �� ?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      with dmMT do
      begin
        // ���� �� �� ������
        if ptPartIn.Active=False then ptPartIn.Active:=True;
        ptPartIn.Refresh;

        Memo1.Clear; delay(10);
        Memo1.Lines.Add('������ ������������ �� (� ���������).'); delay(10);


        iDep:=cxLookupComboBox1.EditValue;

        Memo1.Lines.Add('  ��c��� ��� �� �� ������ - '+its(iDep)); delay(10);

        ptPartIn.CancelRange; //IDSTORE;ARTICUL;IDATE
        ptPartIn.IndexFieldNames:='IDSTORE;IDATE';
        ptPartIn.SetRange([iDep],[iDep]);
        ptPartIn.First;
        iC:=0;
        while not ptPartIn.Eof do
        begin
          ptPartIn.Delete; inc(iC);
          if iC mod 100 =0 then
          begin
            StatusBar1.Panels[0].Text:=its(iC);
            delay(10);
          end;
        end;
        Memo1.Lines.Add('  ���������.'); delay(10);
        Memo1.Lines.Add('  ��������� ��.'); delay(10);
        iC:=0;
        try
          ViewDoc5.BeginUpdate;
          taSpecInv.First;
          while not taSpecInv.Eof do
          begin
            rQs:=taSpecInvQuantF.AsFloat;

            if rQs<=0 then rQs:=0; //������ 0 ����� �����������

            if rQs>0 then
            begin
              rQr:=rQs; //��� ������� ������� ���� ���������
              rPr1:=0;
              rPr0:=0;
              DateB:=Date-200;
              DateE:=cxDateEdit1.Date;

              if ptTTNIn.Active=False then ptTTNIn.Active:=True;
              ptTTNIn.Refresh;
              ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
              ptTTnIn.CancelRange;

              if ptTTNInLn.Active=False then ptTTNInLn.Active:=True;
              ptTTNInLn.Refresh;
              ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
              ptTTnInLn.CancelRange;
              ptTTnInLn.SetRange([taSpecInvCodeTovar.AsInteger,DateB],[taSpecInvCodeTovar.AsInteger,DateE]);
              ptTTnInLn.Last;
              while not ptTTnInLn.Bof do
              begin
                if ptTTnInLnDepart.AsInteger=iDep then
                begin
                  Id:=0;
                  if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                  begin
                    Id:=ptTTNInID.AsInteger;
                    prFindCliName1(ptTTnInLnIndexPost.AsInteger,ptTTnInLnCodePost.AsInteger,Sinn,iCliCB);
                  end;

                  rPr1:=ptTTnInLnCenaTovar.AsFloat; //��� ����������� ������ ���� � �����
                  try
                    rPr0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat; //���� �����, �� ��� ���
                  except
                    rPr0:=rPr1;
                  end;
                  if ptTTnInLnKol.AsFloat>=rQr then
                  begin
                    ptPartIn.Append;
                    ptPartINIDSTORE.AsInteger:=iDep;
                    ptPartINIDATE.AsInteger:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
                    ptPartINIDDOC.AsInteger:=Id;
                    ptPartINARTICUL.AsInteger:=taSpecInvCodeTovar.AsInteger;
                    ptPartINSINNCLI.AsString:=Sinn;
                    ptPartINDTYPE.AsInteger:=1;
                    ptPartINQPART.AsFloat:=ptTTnInLnKol.AsFloat;
                    ptPartINQREMN.AsFloat:=rQr;
                    ptPartINPRICEIN.AsFloat:=rPr1;
                    ptPartINPRICEIN0.AsFloat:=rPr0;
                    ptPartINPRICEOUT.AsFloat:=ptTTnInLnNewCenaTovar.AsFloat;
                    ptPartIn.Post;

                    rQr:=0;
                  end else
                  begin
                    rQr:=rQr-ptTTnInLnKol.AsFloat;

                    ptPartIn.Append;
                    ptPartINIDSTORE.AsInteger:=iDep;
                    ptPartINIDATE.AsInteger:=Trunc(ptTTnInLnDateInvoice.AsDateTime);
                    ptPartINIDDOC.AsInteger:=Id;
                    ptPartINARTICUL.AsInteger:=taSpecInvCodeTovar.AsInteger;
                    ptPartINSINNCLI.AsString:=Sinn;
                    ptPartINDTYPE.AsInteger:=1;
                    ptPartINQPART.AsFloat:=ptTTnInLnKol.AsFloat;
                    ptPartINQREMN.AsFloat:=ptTTnInLnKol.AsFloat;
                    ptPartINPRICEIN.AsFloat:=rPr1;
                    ptPartINPRICEIN0.AsFloat:=rPr0;
                    ptPartINPRICEOUT.AsFloat:=ptTTnInLnNewCenaTovar.AsFloat;
                    ptPartIn.Post;
                  end;
                  if rQr=0 then Break;
                end;
                ptTTnInLn.Prior;
              end;
              if rQr>0 then
              begin
                if rPr1<0.01 then rPr1:=prFLPriceDateT(taSpecInvCodeTovar.AsInteger,Trunc(DateE),rPrIn0);
                if rPr1<0.01 then rPr1:=prFLPriceDateTVn(taSpecInvCodeTovar.AsInteger,Trunc(DateE),rPrIn0);

                ptPartIn.Append;
                ptPartINIDSTORE.AsInteger:=iDep;
                ptPartINIDATE.AsInteger:=Trunc(CommonSet.CalcSeb);
                ptPartINIDDOC.AsInteger:=0;
                ptPartINARTICUL.AsInteger:=taSpecInvCodeTovar.AsInteger;
                ptPartINSINNCLI.AsString:='';
                ptPartINDTYPE.AsInteger:=22;
                ptPartINQPART.AsFloat:=rQr;
                ptPartINQREMN.AsFloat:=rQr;
                ptPartINPRICEIN.AsFloat:=rPr1;
                ptPartINPRICEIN0.AsFloat:=rPr0;
                ptPartINPRICEOUT.AsFloat:=rPr1;
                ptPartIn.Post;
              end;
            end; //if rQs>0


            taSpecInv.Next; inc(iC);
            if iC mod 100 =0 then
            begin
              StatusBar1.Panels[0].Text:=its(iC);
              delay(10);
            end;
          end;

          Memo1.Lines.Add('  �� ��.'); delay(10);
        finally
          ViewDoc5.EndUpdate;
        end;
        Memo1.Lines.Add('������ ��.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmAddDoc5.acFactToZerroExecute(Sender: TObject);
//�������� ����
Var iC:Integer;
begin
  if cxButton1.Enabled then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('�������� ���� ...');
    ViewDoc5.BeginUpdate;
    try
      dstaSpecInv.DataSet:=nil;
      taSpecInv.First;
      iC:=0;

      while not taSpecInv.Eof do
      begin
        taSpecInv.Edit;
        taSpecInvQuantF.AsFloat:=0;
        taSpecInvSumF.AsFloat:=0;
        taSpecInvQuantD.AsFloat:=0-re(taSpecInvQuantR.AsFloat);
        taSpecInvSumD.AsFloat:=rv((0-re(taSpecInvQuantR.AsFloat))*taSpecInvPrice.AsFloat);
        taSpecInv.post;

        taSpecInv.Next; inc(iC);
        if iC mod 100=0 then
        begin
          Memo1.Lines.Add('   '+its(iC));
          delay(10);
        end;
      end;
    finally
      dstaSpecInv.DataSet:=taSpecInv;
      ViewDoc5.EndUpdate;
      Memo1.Lines.Add('��.')
    end;
  end else
    Memo1.Lines.Add('� ������ ��������� �������������� ���������.');

end;


procedure TfmAddDoc5.acCalcTPosExecute(Sender: TObject);
Var rSumR:Real;
    DateB:INteger;
begin
  try
    Memo1.Clear;
    if taSpecInv.RecordCount>0 then
    begin

      Memo1.Lines.Add('������ ������� � ����� �� �� ��.');
      CommonSet.TestCodeSS:=taSpecInvCodeTovar.AsInteger;
      DateB:=fTestInvBack(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))+1;
      prRecalcTOPos(taSpecInvCodeTovar.AsInteger,cxLookupComboBox1.EditValue,DateB,Trunc(cxDateEdit1.Date),Memo1,rSumR); //�������� ���� �� �� �� ��������� ���

    end;
  finally
    Memo1.Lines.Add('������ �� �� ��.');
  end;
end;

procedure TfmAddDoc5.acTestMoveNoUseExecute(Sender: TObject);
begin
//� ���� �� �������� �������������� � ������ �� ��� �� ����� ��
  Memo1.Clear;
  if pos('�����',Person.Name)>0 then
  begin
    Memo1.Lines.Add('������.');
    try



    finally
      Memo1.Lines.Add('������� ��������.');
    end;
  end else Memo1.Lines.Add('��� ����.');
end;

procedure TfmAddDoc5.acOutPutF1Execute(Sender: TObject);
begin
  //��������� ���� � ����
  fmInvExport.Caption:='��������� ���� � ����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=0;
  fmInvExport.Tag:=1;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.acInputF1Execute(Sender: TObject);
begin
  //��������� ���� �� �����
  fmInvExport.Caption:='��������� ���� �� �����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=1;
  fmInvExport.Tag:=1;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.cxCheckBox3PropertiesChange(Sender: TObject);
begin
  if (cxCheckBox3.Checked) and (ViewDoc5.Tag=1) then
  begin
    ViewDoc5SumRSC.Visible:=True;
    ViewDoc5SumRSSI.Visible:=True;
    ViewDoc5QuantC.Visible:=True;
    ViewDoc5SumC.Visible:=True;
    ViewDoc5SumRI.Visible:=True;
  end else
  begin
    ViewDoc5SumRSC.Visible:=False;
    ViewDoc5SumRSSI.Visible:=False;
    ViewDoc5QuantC.Visible:=False;
    ViewDoc5SumC.Visible:=False;
    ViewDoc5SumRI.Visible:=False;
  end;
end;

procedure TfmAddDoc5.cxButton6Click(Sender: TObject);
//������
Var vPP:TfrPrintPages;
begin
  try
    ViewDoc5.BeginUpdate;
    taSpecInv.IndexName:='taSpecInvIndex2';

    frRepDINV.LoadFromFile(CommonSet.Reports + 'InventryListPer.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
    frVariables.Variable['Filial']:=fShopName;

    frRepDINV.ReportName:='������������������ ���������.';
    frRepDINV.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDINV.ShowPreparedReport
    else frRepDINV.PrintPreparedReport('',1,False,vPP);
  finally
    taSpecInv.IndexName:='taSpecInvIndex1';
    ViewDoc5.EndUpdate;
  end;
end;

procedure TfmAddDoc5.acCalcSCPosExecute(Sender: TObject);
var rQuantC,rSumC,rSumRSC:Real;
begin
 //
  with dmMC do
  with dmMt do
  begin
    if taSpecInv.RecordCount>0 then
    begin
      Memo1.Lines.Add('������. ('+its(taSpecInvCodeTovar.AsInteger)+')');

      ptInventryHead.Active:=False; ptInventryHead.Active:=True;
      ptInvLine1.Active:=False; ptInvLine1.Active:=True;
      ptInvLine1.IndexFieldNames:='IDH;ITEM';

      if (ViewDoc5.tag=1)and(cxCheckBox3.Checked) then //��� ������������ � ������������� � ���� � ��
      begin // ����� ������� ���������
        rQuantC:=0; rSumC:=0; rSumRSC:=0;
        //����� ����� ����� �������� � �������� ������� �� ����� � ��
        prFindCorrSum(taSpecInvCodeTovar.AsInteger,cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date)-1,rQuantC,rSumC,rSumRSC,Memo1);
      end else
       Memo1.Lines.Add('��������� ���. ���������� ��� �������.');

      Memo1.Lines.Add('�����.('+its(taSpecInvCodeTovar.AsInteger)+')');
    end;
  end;
end;

procedure TfmAddDoc5.acFormNaclExecute(Sender: TObject);
  //� ���� ��� ���������
Var f:TextFile;
    Str1:String;
    iC:Integer;
    rSumNDS,rSumWithNDS:Real;
begin
  iC:=0;
  with dmMt do
  begin
    try
      ViewDoc5.BeginUpdate;
      memo1.Clear;
      memo1.Lines.Add('������.');

      assignfile(f,CurDir+'Nacl.csv');
      rewrite(f);

      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        if taCards.FindKey([taSpecInvCodeTovar.AsInteger]) then
        begin
          rSumWithNDS:=rv((RoundEx(taCardsNDS.AsFloat)+100)*taSpecInvSumFSS.AsFloat/100);
          rSumNDS:=rSumWithNDS-taSpecInvSumFSS.AsFloat;

          Str1:=its(taSpecInvNum.AsInteger)+';';
          Str1:=Str1+its(taSpecInvCodeTovar.AsInteger)+';';
          Str1:=Str1+taSpecInvName.AsString+';';
          if taSpecInvCodeEdIzm.AsInteger=1 then Str1:=Str1+'��.'+';'
          else Str1:=Str1+'��.'+';';
          Str1:=Str1+taSpecInvSGr.AsString+';';
          Str1:=Str1+taSpecInvSSGr.AsString+';';
          Str1:=Str1+taSpecInvSSSGr.AsString+';';
          Str1:=Str1+fs1(taSpecInvPriceSS.AsFloat)+';';
          Str1:=Str1+fs1(taSpecInvQuantF.AsFloat)+';';
          Str1:=Str1+its(RoundEx(taCardsNDS.AsFloat))+';';
          Str1:=Str1+fs1(taSpecInvSumFSS.AsFloat)+';';
          Str1:=Str1+fs1(rSumNDS)+';';
          Str1:=Str1+fs1(rSumWithNDS)+';';

          writeln(f,Str1);
          inc(iC);

          if iC mod 100 = 0 then
          begin
            memo1.Lines.Add('���������� - '+its(iC));
            delay(10);
          end;

        end else
        begin
          memo1.Lines.Add('����� - '+its(taSpecInvCodeTovar.AsInteger)+' �� ������.');
        end;

        taSpecInv.Next;
      end;
    finally
      ViewDoc5.EndUpdate;
      CloseFile(f);
      memo1.Lines.Add('�� - '+its(iC));
    end;
  end;
end;

procedure TfmAddDoc5.acOutWithNDSExecute(Sender: TObject);
begin
  //��������� ���� � ����
  fmInvExport.Caption:='��������� ���� � ����';
  fmInvExport.Label1.Caption:='���� ��� ��������';
  fmInvExport.cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  fmInvExport.cxButtonEdit2.Tag:=0;
  fmInvExport.Tag:=2;
  fmInvExport.ShowModal;
end;

procedure TfmAddDoc5.acRecalcSS1Execute(Sender: TObject);
Var iC:Integer;
    rSumR,rSumF,rSumRSC,rQuantC:Real;
    iCode:Integer;
begin
  prWH('������ ������� � ����� ��. (1 �������)',Memo1);
  iC:=0; iCode:=0;
  ViewDoc5.BeginUpdate;
  try
//    taSpecInv.First;
    if not taSpecInv.Eof then
    begin
      iCode:=taSpecInvCodeTovar.AsInteger;
      try
        prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantR.AsFloat,rSumR);

        if abs(taSpecInvQuantF.AsFloat-taSpecInvQuantR.AsFloat)<=0.001 then rSumF:=rSumR
        else prCalcSumInRemn(taSpecInvCodeTovar.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue, taSpecInvQuantF.AsFloat,rSumF);

        rSumRSC:=taSpecInvSumRSC.AsFloat;
        rQuantC:=taSpecInvQuantC.AsFloat;

        taSpecInv.Edit;
        taSpecInvSumRSS.AsFloat:=rSumR;
        taSpecInvSumRSSI.AsFloat:=rSumR+rSumRSC;
        taSpecInvSumFSS.AsFloat:=rSumF;
        taSpecInvSumDSS.AsFloat:=rSumF-rSumR-rSumRSC;
        if abs(taSpecInvQuantR.AsFloat+rQuantC)>0.0001 then taSpecInvPriceSS.AsFloat:=(rSumR+rSumRSC)/(taSpecInvQuantR.AsFloat+rQuantC) else taSpecInvPriceSS.AsFloat:=0;
        taSpecInv.post;

        prWH('   ����� '+its(taSpecInvCodeTovar.AsInteger),Memo1);
        prWH('   ���� '+its(Trunc(cxDateEdit1.Date))+' ����� '+its(cxLookupComboBox1.EditValue)+' ���-�� ����. '+fs(taSpecInvQuantR.AsFloat),Memo1);
        prWH('   ����� �� ���� '+fs(rSumR),Memo1);
        prWH('   ����� �� ��������� ���� '+fs(rSumRSC),Memo1);
        prWH('   ����� �� ����  '+fs(rSumF),Memo1);
        prWH('   ���-�� ���������  '+fs(rQuantC),Memo1);
        prWH('   ���-�� ���� '+fs(taSpecInvQuantR.AsFloat),Memo1);

      except
        prWH('   ������ ���� '+its(iC)+' '+its(iCode),Memo1);
      end;
    end;
  finally
    prWH('  Last '+its(iC)+' '+its(iCode),Memo1);
    ViewDoc5.EndUpdate;
    prWH('������ �� ��.',Memo1);
  end;
end;

procedure TfmAddDoc5.taSpecInvCalcFields(DataSet: TDataSet);
begin
  taSpecInvSumDTOSS.AsFloat:=taSpecInvSumRTO.AsFloat-taSpecInvSumRSSI.AsFloat;
end;

procedure TfmAddDoc5.acTmp1Execute(Sender: TObject);
Var f:TextFile;
    Str1,Str2:String;
    iC:Integer;
    iDate:INteger;
    rQ:Real;
    bWr:Boolean;
begin
  // �������� 0 ��������
  if MessageDlg('�������� �������� 0 �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
  begin
    iC:=0;
    with dmMC do
    with dmMt do
    begin
      try
        ViewDoc5.BeginUpdate;
        memo1.Clear;
        memo1.Lines.Add('������.');

        assignfile(f,CurDir+'Remn0');
        rewrite(f);
        iDate:=Trunc(cxDateEdit1.Date);

        taSpecInv.First;
        while not taSpecInv.Eof do
        begin
          if (abs(taSpecInvQuantR.AsFloat)<0.001) and (abs(taSpecInvQuantF.AsFloat)<0.001) then
          begin //��� ������ - ����� ������� ������� �� �������
            bWr:=False;
            if ptDep.Active=False then ptDep.Active:=True else ptDep.Refresh;
            ptDep.First;
            while not ptDep.Eof do
            begin
              if ptDepV01.AsInteger>0 then //��������
              begin
                rQ:=prFindTRemnDepD(taSpecInvCodeTovar.AsInteger,ptDepID.AsInteger,iDate);
                if abs(rQ)>0.01 then bWr:=True;
              end;

              ptDep.Next;
            end;
            if bWr then //� ������� �� �����-�� ���������
            begin
              Str1:='01'; //�� �� ���� �����

              Str1:=Str1+taSpecInvCodeTovar.AsString; //��� ������
              while length(Str1)<7 do Str1:=Str1+' ';

              Str2:=taSpecInvPrice.AsString; //����
              while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
              Str1:=Str1+Str2;
              while length(Str1)<16 do Str1:=Str1+' ';

              Str2:=taSpecInvQuantF.AsString; //���-��
              while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
              Str1:=Str1+Str2;
              while length(Str1)<26 do Str1:=Str1+' ';

              Str1:=Str1+'1'; //������ - ����� �� ����� ����

              writeln(f,Str1);
            end;
          end;
          inc(iC);

          if iC mod 100 = 0 then
          begin
            memo1.Lines.Add('���������� - '+its(iC));
            delay(10);
          end;

          taSpecInv.Next;
        end;
      finally
        ViewDoc5.EndUpdate;
        CloseFile(f);
        memo1.Lines.Add('�� - '+its(iC));
      end;
    end;
  end;
end;

procedure TfmAddDoc5.acCreateOutExecute(Sender: TObject);
Var bDo:Boolean;
    iC:Integer;
    rQr:Real;
    rPr1,rPr0:Real;
    DateB,DateE:TDateTime;
    iDep,iSS,iCli,iCode,IdDoc,iNumD,iTc:Integer;
    sNumD:String;
    IDH:INteger;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSum10M,rSum20M,rSum0:Real;
    dDateDoc:TDateTime;
    iCPost:Integer;
    par:Variant;

begin
//������������ ���������� ���������
  bDo:=False;
  if pos('BUH',Person.Name)>0 then bDo:=True;
  if pos('�����',Person.Name)>0 then bDo:=True;
  if pos('OPER CB',Person.Name)>0 then bDo:=True;
  if pos('OPERZAK',Person.Name)>0 then bDo:=True;

  if bDo then
  begin
    with dmMT do
    with dmMC do
    begin
      if MessageDlg('������ ������ � ������������ ���������� ��������� �� ������������ ���������� ��������������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;
        try
          //1. �� ��������� �������� ������������ ��� ������� �� ��������� ����������
          fmSt.Memo1.Lines.Add(fmt+'������.'); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'    ����� ���� ������ �� �����������'); delay(10);

          dDateDoc:=cxDateEdit1.Date+1;
          iC:=0;
          iDep:=cxLookupComboBox1.EditValue;
          iSS:=fSS(iDep);

          par := VarArrayCreate([0,2],varVariant);

          fmSt.StatusBar1.Panels[0].Text:=its(iC);
          fmSt.StatusBar1.Panels[1].Text:=its(taSpecInv.RecordCount);


          CloseTe(teOutLn);
          ViewDoc5.BeginUpdate;
          taSpecInv.First;
          while not taSpecInv.Eof do
          begin
            if taSpecInvQuantF.AsFloat>0.001 then
            begin //������� ������� ���� - ����� ���������
              rQr:=taSpecInvQuantF.AsFloat;
              rPr1:=0;
              rPr0:=0;
              iCode:=taSpecInvCodeTovar.AsInteger;

              DateB:=Date-500;
              DateE:=dDateDoc;

              if ptTTNIn.Active=False then ptTTNIn.Active:=True;
              ptTTNIn.Refresh;
              ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
              ptTTnIn.CancelRange;

              if ptTTNInLn.Active=False then ptTTNInLn.Active:=True;
              ptTTNInLn.Refresh;
              ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
              ptTTnInLn.CancelRange;
              ptTTnInLn.SetRange([iCode,DateB],[iCode,DateE]);

              ptTTnInLn.Last;
              while not ptTTnInLn.Bof do
              begin
                if ptTTnInLnDepart.AsInteger=iDep then
                begin
                  IdDoc:=0;
                  iCli:=0;
                  iTc:=0;

                  if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                  begin
                    IdDoc:=ptTTNInID.AsInteger;
                    iTc:=ptTTnInLnIndexPost.AsInteger;
                    iCli:=ptTTnInLnCodePost.AsInteger;
                  end;

                  rPr1:=ptTTnInLnCenaTovar.AsFloat; //��� ����������� ������ ���� � �����
                  try
                    rPr0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat; //���� �����, �� ��� ���
                  except
                    rPr0:=rPr1; //���� �����, �� ��� ���
                  end;

                  if iSS<2 then rPr0:=rPr1;

                  if ptTTnInLnKol.AsFloat>=rQr then
                  begin
                    par[0]:=iCode;
                    par[1]:=iCli;
                    par[2]:=rPr1;

                    if teOutLn.Locate('iCode;iCli;Price',par,[]) then
                    begin
                      teOutLn.Edit;
                      teOutLnQuant.AsFloat:=teOutLnQuant.AsFloat+rQr;
                      teOutLn.Post;
                    end else
                    begin
                      teOutLn.Append;
                      teOutLniCode.AsInteger:=iCode;
                      teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                      teOutLnQuant.AsFloat:=rQr;
                      teOutLnPrice0.AsFloat:=rPr0;
                      teOutLnPrice.AsFloat:=rPr1;
                      teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                      teOutLniCli.AsInteger:=iCli;
                      teOutLnIdDoc.AsInteger:=IdDoc;
                      teOutLniTc.AsInteger:=iTc;
                      teOutLn.Post;
                    end;
                    rQr:=0;
                  end else
                  begin
                    rQr:=rQr-ptTTnInLnKol.AsFloat;

                    par[0]:=iCode;
                    par[1]:=iCli;
                    par[2]:=rPr1;

                    if teOutLn.Locate('iCode;iCli;Price',par,[]) then
                    begin
                      teOutLn.Edit;
                      teOutLnQuant.AsFloat:=teOutLnQuant.AsFloat+ptTTnInLnKol.AsFloat;
                      teOutLn.Post;
                    end else
                    begin
                      teOutLn.Append;
                      teOutLniCode.AsInteger:=iCode;
                      teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                      teOutLnQuant.AsFloat:=ptTTnInLnKol.AsFloat;;
                      teOutLnPrice0.AsFloat:=rPr0;
                      teOutLnPrice.AsFloat:=rPr1;
                      teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                      teOutLniCli.AsInteger:=iCli;
                      teOutLnIdDoc.AsInteger:=IdDoc;
                      teOutLniTc.AsInteger:=iTc;
                      teOutLn.Post;
                    end;
                  end;
                  if rQr=0 then Break;
                end;
                ptTTnInLn.Prior;
              end;
              if rQr>0 then //�� ����� ������
              begin
                if rPr1<0.01 then rPr1:=prFLPriceDateT(iCode,Trunc(DateE),rPr0);
                if rPr1<0.01 then rPr1:=prFLPriceDateTVn(iCode,Trunc(DateE),rPr0);

                par[0]:=iCode;
                par[1]:=0;
                par[2]:=rPr1;

                if teOutLn.Locate('iCode;iCli;Price',par,[]) then
                begin
                  teOutLn.Edit;
                  teOutLnQuant.AsFloat:=teOutLnQuant.AsFloat+rQr;
                  teOutLn.Post;
                end else
                begin
                  teOutLn.Append;
                  teOutLniCode.AsInteger:=iCode;
                  teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                  teOutLnQuant.AsFloat:=ptTTnInLnKol.AsFloat;;
                  teOutLnPrice0.AsFloat:=rPr0;
                  teOutLnPrice.AsFloat:=rPr1;
                  teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                  teOutLniCli.AsInteger:=0;
                  teOutLnIdDoc.AsInteger:=0;
                  teOutLniTc.AsInteger:=1;
                  teOutLn.Post;
                end;
              end;
            end;

            taSpecInv.Next; inc(iC);
            fmSt.StatusBar1.Panels[0].Text:=its(iC);
            delay(10);
          end;
          ViewDoc5.EndUpdate;

          fmSt.Memo1.Lines.Add(fmt+'       ��.'); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'    ����� ���� ������������ ����������.'); delay(10);
          iC:=0;
          iCPost:=300;

          if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

          fmSt.StatusBar1.Panels[0].Text:=its(iC);
          fmSt.StatusBar1.Panels[1].Text:=its(teOutLn.RecordCount);

          iC:=teOutLn.RecordCount;

          while (iC>0) and (iCPost>0) do
          begin
            iCli:=-1;

            teOutLn.First;
            while not teOutLn.Eof do
            begin
              iCli:=teOutLniCli.AsInteger;
              if iCli>=0 then Break;
              teOutLn.Next;
            end;

            if iCli>=0 then   //������ �������� ����������
            begin
              iNumD:=prFindNum(2)+1;
              sNumD:=its(iNumD);
              while length(sNumD)<6 do sNumD:='0'+sNumD;
              sNumD:='I'+sNumD;
              IDH:=prMax('DocsOut')+1;

              fmSt.Memo1.Lines.Add(fmt+'         �������� - '+sNumD+' ('+its(IDH)+')'); delay(10);

              // ������� ������ ��� � ���� ��������� ����� ������������ - �������� �� �����

              rSum1:=0; rSum2:=0; rSum3:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum10M:=0; rSum20M:=0;  rSum0:=0;

              teOutLn.First;
              while not teOutLn.Eof do
              begin
                if teOutLniCli.AsInteger=iCli then
                begin
                  if taCards.FindKey([teOutLniCode.AsInteger]) then
                  begin
                    rSum1:=rSum1+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat); //� ����� ����������
                    rSum2:=rSum2+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat); //� ����� ��������
                    rSum3:=rSum3+0;  //���� ����

                    if taCardsNDS.AsFloat>=15 then
                    begin
                      rSum20:=rSum20+rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                      rN20:=rN20+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat-teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                      rSum20M:=rSum20M+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
                    end else
                    begin
                      if taCardsNDS.AsFloat>=8 then
                      begin
                        rSum10:=rSum10+rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                        rN10:=rN10+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat-teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                        rSum10M:=rSum10M+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
                      end else // ��� ���
                      begin
                        rSum0:=rSum0+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat);
                      end;
                    end;
                  end;
                end;
                teOutLn.Next;
              end;

              quA.Active:=False;
              quA.SQL.Clear;
              quA.SQL.Add('INSERT into "TTNOut" values (');
              quA.SQL.Add(its(iDep)+',');  //Depart
              quA.SQL.Add(''''+ds(dDateDoc)+''',');     //DateInvoice
              quA.SQL.Add(''''+sNumD+''',');         //Number
              quA.SQL.Add(''''+sNumD+''',');         //SCFNumber
              quA.SQL.Add(its(1)+',');                 //IndexPoluch
              quA.SQL.Add(its(iCli)+',');          //CodePoluch
              quA.SQL.Add(fs(rSum1)+',');                       //SummaTovar
              quA.SQL.Add(fs(rN10)+',');                        //NDS10
              quA.SQL.Add(fs(rSum10)+',');                      //OutNDS10
              quA.SQL.Add(fs(rN20)+',');                        //NDS20
              quA.SQL.Add(fs(rSum20)+',');                      //OutNDS20
              quA.SQL.Add('0,0,');                              //LgtNDS   OutLgtNDS
              quA.SQL.Add(fs(rN10+rN20)+',');                   //NDSTovar
              quA.SQL.Add(fs(rSum10+rSum20)+','+fs(rSum0)+',');  //OutNDSTovar  //NotNDSTovar
              quA.SQL.Add(fs(rSum1-rSum10-rSum20)+',');         //OutNDSSNTovar
              quA.SQL.Add(fs(rSum2)+',');                       //SummaTovarSpis
              quA.SQL.Add(fs(rSum2-rSum1)+',');         //NacenkaTovar
              quA.SQL.Add(fs(rSum3)+',');                       //SummaTara
              quA.SQL.Add(fs(rSum3)+',0,');                     //SummaTaraSpis  NacenkaTara
              quA.SQL.Add('0,1,'''','''','''',');               // AcStatus ChekBuh NAvto NPList  FIOVod
              quA.SQL.Add(''''+ds(dDateDoc)+''','''',0,'''',');     //PrnDate PrnNumber PrnKol PrnAkt
              quA.SQL.Add('1,0,0,');    //ProvodType  NotNDS10  NotNDS20
              quA.SQL.Add(fs(rSum10+rN10)+',');                  //WithNDS10
              quA.SQL.Add(fs(rSum20+rN20)+','+fs(0)+',');                //WithNDS20  Crock
              quA.SQL.Add('0,0,0,');                             //Discount NDS010 NDS020
              quA.SQL.Add(fs(rSum10M)+',');                      //NDS_10
              quA.SQL.Add(fs(rSum20M)+',0,0,');                   //NDS_20 NSP_10 NSP_20
              quA.SQL.Add(''''+ds(dDateDoc)+''',0,0,0,'); //SCFDate GoodsNSP0 GoodsCostNSP OrderNumber
              quA.SQL.Add(its(IDH)+',0,0,0,'''')');               //ID  LinkInvoice  StartTransfer EndTransfer REZERV

              quA.ExecSQL;

              //������������

              if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
              ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';
              ptOutLn.CancelRange;
              ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNumD)],[iDep,dDateDoc,AnsiToOemConvert(sNumD)]);

              teOutLn.First;
              while not teOutLn.Eof do
              begin
                if teOutLniCli.AsInteger=iCli then
                begin
                  if taCards.FindKey([teOutLniCode.AsInteger]) then
                  begin
                    ptOutLn.Append;

                    ptOutLnDepart.AsInteger:=iDep;
                    ptOutLnDateInvoice.AsDateTime:=dDateDoc;
                    ptOutLnNumber.AsString:=AnsiToOemConvert(sNumD);
                    ptOutLnCodeGroup.AsInteger:=0;
                    ptOutLnCodePodgr.AsInteger:=0;
                    ptOutLnCodeTovar.AsInteger:=teOutLniCode.AsInteger;
                    ptOutLnCodeEdIzm.AsInteger:=teOutLniM.AsInteger;
                    ptOutLnTovarType.AsInteger:=taCardsTovarType.AsInteger;
                    ptOutLnBarCode.AsString:=taCardsBarCode.AsString;
                    ptOutLnOKDP.AsFloat:=0;
                    ptOutLnNDSProc.AsFloat:=taCardsNDS.AsFloat;
                    ptOutLnNDSSum.AsFloat:=rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat)-rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                    ptOutLnOutNDSSum.AsFloat:=rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                    ptOutLnAkciz.AsFloat:=0;
                    ptOutLnKolMest.AsInteger:=1;
                    ptOutLnKolEdMest.AsFloat:=0;
                    ptOutLnKolWithMest.AsFloat:=teOutLnQuant.AsFloat;
                    ptOutLnKol.AsFloat:=teOutLnQuant.AsFloat;
                    ptOutLnCenaPost.AsFloat:=teOutLnPrice0.AsFloat;
                    ptOutLnCenaTovar.AsFloat:=teOutLnPrice.AsFloat;
                    ptOutLnCenaTovarSpis.AsFloat:=teOutLnPriceM.AsFloat;
                    ptOutLnSumCenaTovar.AsFloat:=rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat);
                    ptOutLnSumCenaTovarSpis.AsFloat:=rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
                    ptOutLnCodeTara.AsInteger:=0;
                    ptOutLnVesTara.AsFloat:=0;
                    ptOutLnCenaTara.AsFloat:=0;
                    ptOutLnCenaTaraSpis.AsFloat:=teOutLnPrice0.AsFloat;
                    ptOutLnSumVesTara.AsFloat:=0;
                    ptOutLnSumCenaTara.AsFloat:=0;
                    ptOutLnSumCenaTaraSpis.AsFloat:=0;
                    ptOutLnChekBuh.AsBoolean:=False;

                    ptOutLn.Post;

                  end;


                  teOutLn.Delete;
                end else teOutLn.Next;
              end;
            end;

            iC:=teOutLn.RecordCount;
            dec(iCPost);
          end;

          fmSt.Memo1.Lines.Add(fmt+'       ��.'); delay(10);

        finally
          teOutLn.Active:=False;
          fmSt.Memo1.Lines.Add(fmt+'�������� �������.'); delay(10);
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc5.acTestDoc1Execute(Sender: TObject);
Var iDep:Integer;
    rQr,rPr1,rPr0:Real;
    DateB,DateE,dDateDoc:TDateTime;
    IdDoc,iCli,iTc,iCode:Integer;

begin
  taSpecInv.Locate('CodeTovar',StrToIntDef(cxTextEdit2.Text,0),[]);
  iDep:=cxLookupComboBox1.EditValue;
  CloseTe(teOutLn);

  dDateDoc:=cxDateEdit1.Date+1;

  with dmMT do
  with dmMC do
  begin
           if taSpecInvQuantF.AsFloat>0.001 then
           begin //������� ������� ���� - ����� ���������
              rPr1:=0;
              rPr0:=0;
              iCode:=taSpecInvCodeTovar.AsInteger;

              DateB:=Date-500;
              DateE:=dDateDoc-1;

              if ptTTNIn.Active=False then ptTTNIn.Active:=True;
              ptTTNIn.Refresh;
              ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
              ptTTnIn.CancelRange;

              if ptTTNInLn.Active=False then ptTTNInLn.Active:=True;
              ptTTNInLn.Refresh;
              ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
              ptTTnInLn.CancelRange;
              ptTTnInLn.SetRange([iCode,DateB],[iCode,DateE]);

              rQr:=taSpecInvQuantF.AsFloat;

              ptTTnInLn.Last;
              while not ptTTnInLn.Bof do
              begin
                if ptTTnInLnDepart.AsInteger=iDep then
                begin
                  IdDoc:=0;
                  iCli:=0;
                  iTc:=0;

                  if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                  begin
                    IdDoc:=ptTTNInID.AsInteger;
                    iTc:=ptTTnInLnIndexPost.AsInteger;
                    iCli:=ptTTnInLnCodePost.AsInteger;
                  end;

                  rPr1:=ptTTnInLnCenaTovar.AsFloat; //��� ����������� ������ ���� � �����
                  rPr0:=rPr1;

                  if ptTTnInLnKol.AsFloat>=rQr then
                  begin
                    teOutLn.Append;
                    teOutLniCode.AsInteger:=iCode;
                    teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                    teOutLnQuant.AsFloat:=rQr;
                    teOutLnPrice0.AsFloat:=rPr0;
                    teOutLnPrice.AsFloat:=rPr1;
                    teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                    teOutLniCli.AsInteger:=iCli;
                    teOutLnIdDoc.AsInteger:=IdDoc;
                    teOutLniTc.AsInteger:=iTc;
                    teOutLn.Post;

                    rQr:=0;
                  end else
                  begin
                    rQr:=rQr-ptTTnInLnKol.AsFloat;

                    teOutLn.Append;
                    teOutLniCode.AsInteger:=iCode;
                    teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                    teOutLnQuant.AsFloat:=ptTTnInLnKol.AsFloat;
                    teOutLnPrice0.AsFloat:=rPr0;
                    teOutLnPrice.AsFloat:=rPr1;
                    teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                    teOutLniCli.AsInteger:=iCli;
                    teOutLniTc.AsInteger:=iTc;
                    teOutLnIdDoc.AsInteger:=IdDoc;
                    teOutLn.Post;
                  end;
                  if rQr=0 then Break;
                end;
                ptTTnInLn.Prior;
              end;
              if rQr>0 then //�� ����� ������
              begin
                if rPr1<0.01 then rPr1:=prFLPriceDateT(iCode,Trunc(DateE),rPr0);
                if rPr1<0.01 then rPr1:=prFLPriceDateTVn(iCode,Trunc(DateE),rPr0);

                teOutLn.Append;
                teOutLniCode.AsInteger:=iCode;
                teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                teOutLnQuant.AsFloat:=rQr;
                teOutLnPrice0.AsFloat:=rPr0;
                teOutLnPrice.AsFloat:=rPr1;
                teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                teOutLniCli.AsInteger:=0;
                teOutLniTc.AsInteger:=1;
                teOutLnIdDoc.AsInteger:=0;
                teOutLn.Post;
              end;
            end;



  end;
end;

procedure TfmAddDoc5.acCreateRetDoc1Execute(Sender: TObject);
Var bDo:Boolean;
    iC:Integer;
    rQr:Real;
    rPr1,rPr0:Real;
    iDep,iCode,iNumD:Integer;
    sNumD:String;
    IDH:INteger;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSum10M,rSum20M,rSum0:Real;
    dDateDoc:TDateTime;
begin
  //������������  1-� ���������� ���������
  bDo:=False;
  if pos('BUH',Person.Name)>0 then bDo:=True;
  if pos('�����',Person.Name)>0 then bDo:=True;
  if pos('OPER CB',Person.Name)>0 then bDo:=True;
  if pos('OPERZAK',Person.Name)>0 then bDo:=True;

  if bDo then
  begin
    with dmMT do
    with dmMC do
    begin
      if MessageDlg('������ ������ � ������������ ���������� ��������� �� ������������ ���������� ��������������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;
        try
          //1. �� ��������� �������� ������������ ��� ������� �� ��������� ����������
          fmSt.Memo1.Lines.Add(fmt+'������.'); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'    ����� ���� ������ ..'); delay(10);

          iC:=0;
          iDep:=cxLookupComboBox1.EditValue;
          dDateDoc:=Date;

          fmSt.StatusBar1.Panels[0].Text:=its(iC);
          fmSt.StatusBar1.Panels[1].Text:=its(taSpecInv.RecordCount);

          CloseTe(teOutLn);
          ViewDoc5.BeginUpdate;
          taSpecInv.First;
          while not taSpecInv.Eof do
          begin
            if taSpecInvQuantF.AsFloat>0.001 then
            begin //������� ������� ���� - ����� ���������
              rQr:=taSpecInvQuantF.AsFloat;
              rPr0:=0;
              iCode:=taSpecInvCodeTovar.AsInteger;
              rPr1:=prFLPriceDateT(iCode,Trunc(cxDateEdit1.Date),rPr0);

              if teOutLn.Locate('iCode',iCode,[]) then
              begin
                teOutLn.Edit;
                teOutLnQuant.AsFloat:=teOutLnQuant.AsFloat+rQr;
                teOutLn.Post;
              end else
              begin
                teOutLn.Append;
                teOutLniCode.AsInteger:=iCode;
                teOutLniM.AsInteger:=taSpecInvCodeEdIzm.AsInteger;
                teOutLnQuant.AsFloat:=rQr;
                teOutLnPrice0.AsFloat:=rPr0;
                teOutLnPrice.AsFloat:=rPr1;
                teOutLnPriceM.AsFloat:=taSpecInvPrice.AsFloat;
                teOutLniCli.AsInteger:=0;
                teOutLnIdDoc.AsInteger:=0;
                teOutLniTc.AsInteger:=0;
                teOutLn.Post;
              end;
            end;

            taSpecInv.Next; inc(iC);
            fmSt.StatusBar1.Panels[0].Text:=its(iC);
            delay(10);
          end;
          ViewDoc5.EndUpdate;

          fmSt.Memo1.Lines.Add(fmt+'       ��.'); delay(10);
          fmSt.Memo1.Lines.Add(fmt+'    ����� ���� ������������ ���������.'); delay(10);

          if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

          fmSt.StatusBar1.Panels[0].Text:=its(iC);
          fmSt.StatusBar1.Panels[1].Text:=its(teOutLn.RecordCount);

          iNumD:=prFindNum(2)+1;
          sNumD:=its(iNumD);
          while length(sNumD)<6 do sNumD:='0'+sNumD;
          sNumD:='I'+sNumD;
          IDH:=prMax('DocsOut')+1;

          fmSt.Memo1.Lines.Add(fmt+'         �������� - '+sNumD+' ('+its(IDH)+')'); delay(10);

          rSum1:=0; rSum2:=0; rSum3:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSum10M:=0; rSum20M:=0;  rSum0:=0;

          teOutLn.First;
          while not teOutLn.Eof do
          begin
            if taCards.FindKey([teOutLniCode.AsInteger]) then
            begin
              rSum1:=rSum1+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat); //� ����� ����������
              rSum2:=rSum2+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat); //� ����� ��������
              rSum3:=rSum3+0;  //���� ����

              if taCardsNDS.AsFloat>=15 then
              begin
                rSum20:=rSum20+rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                rN20:=rN20+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat-teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                rSum20M:=rSum20M+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
              end else
              begin
                if taCardsNDS.AsFloat>=8 then
                begin
                  rSum10:=rSum10+rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                  rN10:=rN10+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat-teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
                  rSum10M:=rSum10M+rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
                end else // ��� ���
                begin
                  rSum0:=rSum0+rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat);
                end;
              end;
            end;
            teOutLn.Next;
          end;

          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "TTNOut" values (');
          quA.SQL.Add(its(iDep)+',');  //Depart
          quA.SQL.Add(''''+ds(dDateDoc)+''',');     //DateInvoice
          quA.SQL.Add(''''+sNumD+''',');         //Number
          quA.SQL.Add(''''+sNumD+''',');         //SCFNumber
          quA.SQL.Add(its(1)+',');                 //IndexPoluch
          quA.SQL.Add(its(0)+',');          //CodePoluch
          quA.SQL.Add(fs(rSum1)+',');                       //SummaTovar
          quA.SQL.Add(fs(rN10)+',');                        //NDS10
          quA.SQL.Add(fs(rSum10)+',');                      //OutNDS10
          quA.SQL.Add(fs(rN20)+',');                        //NDS20
          quA.SQL.Add(fs(rSum20)+',');                      //OutNDS20
          quA.SQL.Add('0,0,');                              //LgtNDS   OutLgtNDS
          quA.SQL.Add(fs(rN10+rN20)+',');                   //NDSTovar
          quA.SQL.Add(fs(rSum10+rSum20)+','+fs(rSum0)+',');  //OutNDSTovar  //NotNDSTovar
          quA.SQL.Add(fs(rSum1-rSum10-rSum20)+',');         //OutNDSSNTovar
          quA.SQL.Add(fs(rSum2)+',');                       //SummaTovarSpis
          quA.SQL.Add(fs(rSum2-rSum1)+',');         //NacenkaTovar
          quA.SQL.Add(fs(rSum3)+',');                       //SummaTara
          quA.SQL.Add(fs(rSum3)+',0,');                     //SummaTaraSpis  NacenkaTara
          quA.SQL.Add('0,1,'''','''','''',');               // AcStatus ChekBuh NAvto NPList  FIOVod
          quA.SQL.Add(''''+ds(dDateDoc)+''','''',0,'''',');     //PrnDate PrnNumber PrnKol PrnAkt
          quA.SQL.Add('1,0,0,');    //ProvodType  NotNDS10  NotNDS20
          quA.SQL.Add(fs(rSum10+rN10)+',');                  //WithNDS10
          quA.SQL.Add(fs(rSum20+rN20)+','+fs(0)+',');                //WithNDS20  Crock
          quA.SQL.Add('0,0,0,');                             //Discount NDS010 NDS020
          quA.SQL.Add(fs(rSum10M)+',');                      //NDS_10
          quA.SQL.Add(fs(rSum20M)+',0,0,');                   //NDS_20 NSP_10 NSP_20
          quA.SQL.Add(''''+ds(dDateDoc)+''',0,0,0,'); //SCFDate GoodsNSP0 GoodsCostNSP OrderNumber
          quA.SQL.Add(its(IDH)+',0,0,0,'''')');               //ID  LinkInvoice  StartTransfer EndTransfer REZERV

          quA.ExecSQL;

          //������������

          if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
          ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';
          ptOutLn.CancelRange;
          ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNumD)],[iDep,dDateDoc,AnsiToOemConvert(sNumD)]);

          teOutLn.First;
          while not teOutLn.Eof do
          begin

            if taCards.FindKey([teOutLniCode.AsInteger]) then
            begin
              ptOutLn.Append;

              ptOutLnDepart.AsInteger:=iDep;
              ptOutLnDateInvoice.AsDateTime:=dDateDoc;
              ptOutLnNumber.AsString:=AnsiToOemConvert(sNumD);
              ptOutLnCodeGroup.AsInteger:=0;
              ptOutLnCodePodgr.AsInteger:=0;
              ptOutLnCodeTovar.AsInteger:=teOutLniCode.AsInteger;
              ptOutLnCodeEdIzm.AsInteger:=teOutLniM.AsInteger;
              ptOutLnTovarType.AsInteger:=taCardsTovarType.AsInteger;
              ptOutLnBarCode.AsString:=taCardsBarCode.AsString;
              ptOutLnOKDP.AsFloat:=0;
              ptOutLnNDSProc.AsFloat:=taCardsNDS.AsFloat;
              ptOutLnNDSSum.AsFloat:=rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat)-rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
              ptOutLnOutNDSSum.AsFloat:=rv(teOutLnPrice0.AsFloat*teOutLnQuant.AsFloat);
              ptOutLnAkciz.AsFloat:=0;
              ptOutLnKolMest.AsInteger:=1;
              ptOutLnKolEdMest.AsFloat:=0;
              ptOutLnKolWithMest.AsFloat:=teOutLnQuant.AsFloat;
              ptOutLnKol.AsFloat:=teOutLnQuant.AsFloat;
              ptOutLnCenaPost.AsFloat:=teOutLnPrice0.AsFloat;
              ptOutLnCenaTovar.AsFloat:=teOutLnPrice.AsFloat;
              ptOutLnCenaTovarSpis.AsFloat:=teOutLnPriceM.AsFloat;
              ptOutLnSumCenaTovar.AsFloat:=rv(teOutLnPrice.AsFloat*teOutLnQuant.AsFloat);
              ptOutLnSumCenaTovarSpis.AsFloat:=rv(teOutLnPriceM.AsFloat*teOutLnQuant.AsFloat);
              ptOutLnCodeTara.AsInteger:=0;
              ptOutLnVesTara.AsFloat:=0;
              ptOutLnCenaTara.AsFloat:=0;
              ptOutLnCenaTaraSpis.AsFloat:=teOutLnPrice0.AsFloat;
              ptOutLnSumVesTara.AsFloat:=0;
              ptOutLnSumCenaTara.AsFloat:=0;
              ptOutLnSumCenaTaraSpis.AsFloat:=0;
              ptOutLnChekBuh.AsBoolean:=False;

              ptOutLn.Post;
            end;

            teOutLn.Next;
          end;

          fmSt.Memo1.Lines.Add(fmt+'       ��.'); delay(10);

        finally
          teOutLn.Active:=False;
          fmSt.Memo1.Lines.Add(fmt+'������������ ��������.'); delay(10);
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

end.


