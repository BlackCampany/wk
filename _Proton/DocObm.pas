unit DocObm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocs6 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    amDocsObm: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc6: TAction;
    acViewDoc6: TAction;
    acDelDoc1: TAction;
    acOnDoc6: TAction;
    acOffDoc6: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    acEdit1: TAction;
    acTestMoveDoc: TAction;
    acOprih: TAction;
    acRazrub: TAction;
    ViewObm: TcxGridDBTableView;
    LevelObm: TcxGridLevel;
    GridObm: TcxGrid;
    ViewObmID: TcxGridDBColumn;
    ViewObmDOCDATE: TcxGridDBColumn;
    ViewObmDOCNUM: TcxGridDBColumn;
    ViewObmINSUMIN: TcxGridDBColumn;
    ViewObmINSUMOUT: TcxGridDBColumn;
    ViewObmOUTSUMIN: TcxGridDBColumn;
    ViewObmOUTSUMOUT: TcxGridDBColumn;
    ViewObmNameDep: TcxGridDBColumn;
    ViewObmNameCli: TcxGridDBColumn;
    ViewObmINN: TcxGridDBColumn;
    ViewObmIACTIVE: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc6Execute(Sender: TObject);
    procedure acViewDoc6Execute(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc6Execute(Sender: TObject);
    procedure acOffDoc6Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure ViewObmDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc6(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs6: TfmDocs6;
  bClearDocObm:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, MT, TBuff, u2fdk, AddDoc6;

{$R *.dfm}

procedure TfmDocs6.prSetValsAddDoc6(iT:SmallINt); //0-����������, 1-��������������, 2-��������
begin
  with dmMC do
  begin
    bOpen:=True;
    if iT=0 then
    begin
      fmAddDoc6.Caption:='���������: ����������.';
      fmAddDoc6.cxTextEdit1.Text:='';
      fmAddDoc6.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc6.cxTextEdit1.Tag:=0;

      fmAddDoc6.cxDateEdit1.Date:=date;
      fmAddDoc6.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc6.cxButtonEdit1.Tag:=1;
      fmAddDoc6.cxButtonEdit1.EditValue:=0;
      fmAddDoc6.cxButtonEdit1.Text:='';
      fmAddDoc6.cxButtonEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc6.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmAddDoc6.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmAddDoc6.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc6.cxLabel1.Enabled:=True;
      fmAddDoc6.cxLabel2.Enabled:=True;
      fmAddDoc6.cxLabel5.Enabled:=True;
      fmAddDoc6.cxLabel6.Enabled:=True;
      fmAddDoc6.cxButton1.Enabled:=True;

      fmAddDoc6.ViewDoc6.OptionsData.Editing:=True;
      fmAddDoc6.ViewDoc6.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmAddDoc6.Caption:='���������: ��������������.';
      fmAddDoc6.cxTextEdit1.Text:=quObmHDOCNUM.AsString;
      fmAddDoc6.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc6.cxTextEdit1.Tag:=quObmHID.AsInteger;

      fmAddDoc6.cxDateEdit1.Date:=quObmHDOCDATE.AsDateTime;
      fmAddDoc6.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc6.Label4.Tag:=quObmHCLITYPE.AsInteger;
      fmAddDoc6.cxButtonEdit1.Tag:=quObmHCLICODE.AsInteger;
      fmAddDoc6.cxButtonEdit1.Text:=quObmHNameCli.AsString;
      fmAddDoc6.cxButtonEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True;

      fmAddDoc6.cxLookupComboBox1.EditValue:=quObmHDepart.AsInteger;
      fmAddDoc6.cxLookupComboBox1.Text:=quObmHNameDep.AsString;
      fmAddDoc6.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc6.cxLabel1.Enabled:=True;
      fmAddDoc6.cxLabel2.Enabled:=True;
      fmAddDoc6.cxLabel5.Enabled:=True;
      fmAddDoc6.cxLabel6.Enabled:=True;
      fmAddDoc6.cxButton1.Enabled:=True;

      fmAddDoc6.ViewDoc6.OptionsData.Editing:=True;
      fmAddDoc6.ViewDoc6.OptionsData.Deleting:=True;
    end;
    if iT=2 then
    begin
      fmAddDoc6.Caption:='���������: ��������.';
      fmAddDoc6.cxTextEdit1.Text:=quObmHDOCNUM.AsString;
      fmAddDoc6.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc6.cxTextEdit1.Tag:=quObmHID.AsInteger;

      fmAddDoc6.cxDateEdit1.Date:=quObmHDOCDATE.AsDateTime;
      fmAddDoc6.cxDateEdit1.Properties.ReadOnly:=True;

      fmAddDoc6.Label4.Tag:=quObmHCLITYPE.AsInteger;
      fmAddDoc6.cxButtonEdit1.Tag:=quObmHCLICODE.AsInteger;
      fmAddDoc6.cxButtonEdit1.Text:=quObmHNameCli.AsString;
      fmAddDoc6.cxButtonEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc6.cxLookupComboBox1.EditValue:=quObmHDepart.AsInteger;
      fmAddDoc6.cxLookupComboBox1.Text:=quObmHNameDep.AsString;
      fmAddDoc6.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc6.cxLabel1.Enabled:=False;
      fmAddDoc6.cxLabel2.Enabled:=False;
      fmAddDoc6.cxLabel5.Enabled:=False;
      fmAddDoc6.cxLabel6.Enabled:=False;
      fmAddDoc6.cxButton1.Enabled:=False;

      fmAddDoc6.ViewDoc6.OptionsData.Editing:=False;
      fmAddDoc6.ViewDoc6.OptionsData.Deleting:=False;
    end;
    bOpen:=False;
  end;
end;

procedure TfmDocs6.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs6.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs6.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.IniSection:='DocObm';
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridObm.Align:=AlClient;
  ViewObm.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewObm.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs6.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelObm.Visible then
      begin
        fmDocs6.Caption:='��������� �� ����� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs6.ViewObm.BeginUpdate;
        try
          quObmH.Active:=False;
          quObmH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quObmH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quObmH.Active:=True;
        finally
          fmDocs6.ViewObm.EndUpdate;
        end;

      end;
    end;
  end;
end;

procedure TfmDocs6.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc6(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc6.teSpecObm);
    fmAddDoc6.acSaveDoc.Enabled:=True;
    fmAddDoc6.Show;
  end;
end;

procedure TfmDocs6.acEditDoc6Execute(Sender: TObject);
Var //IDH:INteger;
//    rSum1,rSum2:Real;
    iC:INteger;
begin
  //�������������
  if not CanDo('prEditDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quObmH.RecordCount>0 then //���� ��� �������������
    begin
      if quObmHIACTIVE.AsInteger=0 then
      begin
        if quObmHDOCDATE.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quObmHDepart.AsInteger,Trunc(quObmHDOCDATE.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        prSetValsAddDoc6(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmAddDoc6.teSpecObm);

        fmAddDoc6.acSaveDoc.Enabled:=True;

        if ptDExchS.Active=False then ptDExchS.Active:=True;
        ptDExchS.CancelRange;
        ptDExchS.SetRange([quObmHID.AsInteger],[quObmHID.AsInteger]);
        with fmAddDoc6 do
        begin
          fmAddDoc6.ViewDoc6.BeginUpdate;
          try
          iC:=1;
          ptDExchS.First;
          while not ptDExchS.Eof do
          begin
            if taCards.FindKey([ptDExchSCODE.AsInteger]) then
            begin
              teSpecObm.Append;
              teSpecObmNum.AsInteger:=iC;
              teSpecObmCode.AsInteger:=ptDExchSCODE.AsInteger;
              teSpecObmsBar.AsString:=taCardsBarCode.AsString;
              teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
              teSpecObmsType.AsInteger:=ptDExchSSTYPE.asinteger;
              teSpecObmQuant.AsFloat:=ptDExchSQUANT.AsFloat;
              teSpecObmPriceIn.AsFloat:=ptDExchSPRICEIN.AsFloat;
              teSpecObmNac.AsFloat:=ptDExchSNAC.AsFloat;
              teSpecObmPriceOut.AsFloat:=ptDExchSPRICEOUT.AsFloat;
              teSpecObmSumIn.AsFloat:=ptDExchSSUMIN.AsFloat;
              teSpecObmSumOut.AsFloat:=ptDExchSSUMOUT.AsFloat;
              teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
              teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
              teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
              teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
              teSpecObm.Post;
              inc(iC);
            end;
            ptDExchS.Next;
          end;
          finally
            fmAddDoc6.ViewDoc6.EndUpdate;
          end;  
        end;

        fmAddDoc6.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs6.acViewDoc6Execute(Sender: TObject);
Var iC:INteger;
begin
  //��������
  if not CanDo('prViewDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quObmH.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc6(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmAddDoc6.teSpecObm);

      fmAddDoc6.acSaveDoc.Enabled:=False;

      if ptDExchS.Active=False then ptDExchS.Active:=True;
      ptDExchS.CancelRange;
      ptDExchS.SetRange([quObmHID.AsInteger],[quObmHID.AsInteger]);
      with fmAddDoc6 do
      begin
        fmAddDoc6.ViewDoc6.BeginUpdate;
        try
        iC:=1;
        ptDExchS.First;

        while not ptDExchS.Eof do
        begin
          if taCards.FindKey([ptDExchSCODE.AsInteger]) then
          begin
            teSpecObm.Append;
            teSpecObmNum.AsInteger:=iC;
            teSpecObmCode.AsInteger:=ptDExchSCODE.AsInteger;
            teSpecObmsBar.AsString:=taCardsBarCode.AsString;
            teSpecObmName.AsString:=OemToAnsiConvert(taCardsName.AsString);
            teSpecObmiM.AsInteger:=taCardsEdIzm.AsInteger;
            teSpecObmsType.AsInteger:=ptDExchSSTYPE.asinteger;
            teSpecObmQuant.AsFloat:=ptDExchSQUANT.AsFloat;
            teSpecObmPriceIn.AsFloat:=ptDExchSPRICEIN.AsFloat;
            teSpecObmNac.AsFloat:=ptDExchSNAC.AsFloat;
            teSpecObmPriceOut.AsFloat:=ptDExchSPRICEOUT.AsFloat;
            teSpecObmSumIn.AsFloat:=ptDExchSSUMIN.AsFloat;
            teSpecObmSumOut.AsFloat:=ptDExchSSUMOUT.AsFloat;
            teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
            teSpecObmPriceM.AsFloat:=taCardsCena.AsFloat;
            teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
            teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
            teSpecObm.Post;
            inc(iC);
          end;
          ptDExchS.Next;
        end;
        finally
          fmAddDoc6.ViewDoc6.EndUpdate;
        end;
      end;
      fmAddDoc6.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs6.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quObmH.RecordCount>0 then //���� ��� �������
    begin
      if quObmHIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quObmHDOCNUM.AsString+' �� '+quObmHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(6,quObmHID.AsInteger,trunc(quObmHDOCDATE.AsDateTime),quObmHDOCNUM.AsString,'Off',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "A_DOCEXCHSPEC"');
          quD.SQL.Add('where IDH='+its(quObmHID.AsInteger));
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "A_DOCEXCHHEAD"');
          quD.SQL.Add('where ID='+its(quObmHID.AsInteger));
          quD.ExecSQL;
          delay(100);

          prRefrID(quObmH,ViewObm,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs6.acOnDoc6Execute(Sender: TObject);
Var bOpr:Boolean;
    iNumPost:INteger;
    rPr:Real;
begin
//������������
  with dmMC do
  with dmMT do
  begin
    if not CanDo('prOnDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quObmH.RecordCount>0 then //���� ��� ������������
    begin
      if quObmHIACTIVE.AsInteger=0 then
      begin
        if quObmHDOCDATE.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quObmHDepart.AsInteger,Trunc(quObmHDOCDATE.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quObmHDOCNUM.AsString+' �� '+quObmHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          prAddHist(15,quObmHID.AsInteger,trunc(quObmHDOCDATE.AsDateTime),quObmHDOCNUM.AsString,'On',0);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);

          bOpr:=True;

          try
            fmAddDoc6.ViewDoc6.BeginUpdate;

            if ptDExchS.Active=False then ptDExchS.Active:=True;
            ptDExchS.CancelRange;
            ptDExchS.SetRange([quObmHID.AsInteger],[quObmHID.AsInteger]);

            if NeedPost(trunc(quObmHDOCDATE.AsDateTime)) then
            begin
              iNumPost:=PostNum(quObmHID.AsInteger)*100+6;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNOBM;ON;'+its(quObmHDEPART.AsInteger)+r+its(quObmHID.AsInteger)+r+ds(quObmHDOCDATE.AsDateTime)+r+quObmHDOCNUM.AsString+r+fs(quObmHINSUMIN.AsFloat)+r+fs(quObmHINSUMOUT.AsFloat)+r+fs(quObmHOUTSUMIN.AsFloat)+r+fs(quObmHOUTSUMOUT.AsFloat)+r+fs(quObmHITYPE.AsINteger)+r);

                ptDExchS.First;
                while not ptDExchS.Eof do
                begin
                  StrPost:='TTNOBMLN;ON;'+its(quObmHDEPART.AsInteger)+r+its(quObmHID.AsInteger);
                  StrPost:=StrPost+r+its(ptDExchSCODE.asINteger)+r+fs(ptDExchSQUANT.asfloat)+r+fs(ptDExchSSUMIN.asfloat)+r+fs(ptDExchSSUMOUT.asfloat)+r;
                  writeln(fPost,StrPost);
                  ptDExchS.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin

              ptDExchS.First;  //��������� ����� ���  � ��������� ��������������
              while not ptDExchS.Eof do
              begin
                 //���� ���-�� + �� ���� ������ - �������������� ����� ���� ���� � ���� ��� � �����
                if ptDExchSQUANT.AsFloat>=0 then
                begin //���� ������
                  rPr:=prFC(ptDExchSCODE.AsInteger);
                  if abs(ptDExchSPRICEOUT.AsFloat-rPr)>=0.01 then //
                    prTPriceBuf(ptDExchSCODE.AsInteger,Trunc(quObmHDOCDATE.AsDateTime),quObmHID.AsInteger,15,quObmHDOCNUM.AsString,rPr,ptDExchSPRICEOUT.AsFloat);
                end;

                prDelTMoveId(quObmHDEPART.AsInteger,ptDExchSCODE.AsInteger,Trunc(quObmHDOCDATE.AsDateTime),quObmHID.AsInteger,15,ptDExchSQUANT.AsFloat);
                prAddTMoveId(quObmHDEPART.AsInteger,ptDExchSCODE.AsInteger,Trunc(quObmHDOCDATE.AsDateTime),quObmHID.AsInteger,15,ptDExchSQUANT.AsFloat);

                ptDExchS.Next;
              end;

              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "A_DOCEXCHHEAD" Set IACTIVE=1');
              quE.SQL.Add('where ID='+its(quObmHID.AsInteger));
              quE.ExecSQL;

              prRefrID(quObmH,ViewObm,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quObmHDOCDATE.AsDateTime))>=1 then prRecalcTO(trunc(quObmHDOCDATE.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);

            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          finally
            fmAddDoc6.ViewDoc6.EndUpdate;
          end;

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs6.acOffDoc6Execute(Sender: TObject);
Var   bOpr:Boolean;

begin
//��������
  with dmMC do
  with dmMT do
  begin
    if not CanDo('prOffDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    //��� ���������
    if quObmH.RecordCount>0 then //���� ��� ������������
    begin
      if quObmHIACTIVE.AsInteger=1 then
      begin
        if quObmHDOCDATE.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quObmHDepart.AsInteger,Trunc(quObmHDOCDATE.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('�������� �������� �'+quObmHDOCNUM.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);
          prAddHist(1,quTTnInID.AsInteger,trunc(quObmHDOCDATE.AsDateTime),quObmHDOCNUM.AsString,'�����.',0);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);
          bOpr:=True;

          try
            fmAddDoc6.ViewDoc6.BeginUpdate;

            if ptDExchS.Active=False then ptDExchS.Active:=True;
            ptDExchS.CancelRange;
            ptDExchS.SetRange([quObmHID.AsInteger],[quObmHID.AsInteger]);

            if NeedPost(trunc(quObmHDOCDATE.AsDateTime)) then
            begin
              iNumPost:=PostNum(quObmHID.AsInteger)*100+6;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNOBM;OF;'+its(quObmHDEPART.AsInteger)+r+its(quObmHID.AsInteger)+r+ds(quObmHDOCDATE.AsDateTime)+r+quObmHDOCNUM.AsString+r+fs(quObmHINSUMIN.AsFloat)+r+fs(quObmHINSUMOUT.AsFloat)+r+fs(quObmHOUTSUMIN.AsFloat)+r+fs(quObmHOUTSUMOUT.AsFloat)+r+fs(quObmHITYPE.AsINteger)+r);

                ptDExchS.First;
                while not ptDExchS.Eof do
                begin
                  StrPost:='TTNOBMLN;OF;'+its(quObmHDEPART.AsInteger)+r+its(quObmHID.AsInteger);
                  StrPost:=StrPost+r+its(ptDExchSCODE.asINteger)+r+fs(ptDExchSQUANT.asfloat)+r+fs(ptDExchSSUMIN.asfloat)+r+fs(ptDExchSSUMOUT.asfloat)+r;
                  writeln(fPost,StrPost);
                  ptDExchS.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin

              ptDExchS.First;  //��������� ����� ���  � ��������� ��������������
              while not ptDExchS.Eof do
              begin
                prDelTMoveId(quObmHDEPART.AsInteger,ptDExchSCODE.AsInteger,Trunc(quObmHDOCDATE.AsDateTime),quObmHID.AsInteger,15,ptDExchSQUANT.AsFloat);

                ptDExchS.Next;
              end;

              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "A_DOCEXCHHEAD" Set IACTIVE=0');
              quE.SQL.Add('where ID='+its(quObmHID.AsInteger));
              quE.ExecSQL;

              prRefrID(quObmH,ViewObm,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quObmHDOCDATE.AsDateTime))>=1 then prRecalcTO(trunc(quObmHDOCDATE.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);

            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          finally
            fmAddDoc6.ViewDoc6.EndUpdate;
          end;


          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs6.Timer1Timer(Sender: TObject);
begin
  if bClearDocObm=True then begin StatusBar1.Panels[0].Text:=''; bClearDocObm:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocObm:=True;
end;

procedure TfmDocs6.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs6.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  prNExportExel5(ViewObm);
end;

procedure TfmDocs6.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs6.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewObm);
end;

procedure TfmDocs6.acEdit1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc6(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc6.teSpecObm);

    fmAddDoc6.acSaveDoc.Enabled:=True;
    fmAddDoc6.Show;
  end;
end;

procedure TfmDocs6.ViewObmDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quObmHIACTIVE.AsInteger=0 then acEditDoc6.Execute //��������������
    else acViewDoc6.Execute; //��������}
  end;
end;

end.
