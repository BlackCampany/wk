unit AddDoc7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxSpinEdit,
  cxCheckBox, FR_DSet, FR_DBSet, FR_Class, dxmdaset, cxCalc, cxMemo;

type
  TfmAddDoc7 = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    cxLabel6: TcxLabel;
    amDocZ: TActionManager;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    acReadBar: TAction;
    acReadBar1: TAction;
    acPrintCen: TAction;
    acPostTov: TAction;
    cxButton8: TcxButton;
    acSetRemn: TAction;
    acTermoP: TAction;
    acSetNac: TAction;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    teSpecZ: TdxMemData;
    teSpecZCode: TIntegerField;
    dsteSpecZ: TDataSource;
    teSpecZName: TStringField;
    teSpecZFullName: TStringField;
    teSpecZEdIzm: TSmallintField;
    teSpecZRemn1: TFloatField;
    teSpecZvReal: TFloatField;
    teSpecZRemnDay: TFloatField;
    teSpecZPriceIn: TFloatField;
    teSpecZSumIn: TFloatField;
    teSpecZNDSProc: TStringField;
    teSpecZRemnMin: TFloatField;
    teSpecZRemnMax: TFloatField;
    teSpecZRemn2: TFloatField;
    teSpecZQuant1: TFloatField;
    teSpecZPK: TFloatField;
    teSpecZQuantZ: TFloatField;
    teSpecZQuant2: TFloatField;
    teSpecZQuant3: TFloatField;
    cxCalcEdit1: TcxCalcEdit;
    Label2: TLabel;
    teSpecZiCat: TIntegerField;
    teSpecZiTop: TIntegerField;
    teSpecZiN: TIntegerField;
    cxLabel3: TcxLabel;
    acAddAssPost: TAction;
    cxLabel4: TcxLabel;
    acCalcZ: TAction;
    Panel4: TPanel;
    Memo1: TcxMemo;
    acMoveSel: TAction;
    ViewRealDay: TcxGridDBTableView;
    LevRealDay: TcxGridLevel;
    GrRealDay: TcxGrid;
    teRealDay: TdxMemData;
    teRealDayD1: TFloatField;
    teRealDayD2: TFloatField;
    teRealDayD3: TFloatField;
    teRealDayD4: TFloatField;
    teRealDayD5: TFloatField;
    teRealDayD6: TFloatField;
    teRealDayD7: TFloatField;
    teRealDayD8: TFloatField;
    teRealDayD9: TFloatField;
    teRealDayD0: TFloatField;
    dsteRealDay: TDataSource;
    ViewRealDayD0: TcxGridDBColumn;
    ViewRealDayD1: TcxGridDBColumn;
    ViewRealDayD2: TcxGridDBColumn;
    ViewRealDayD3: TcxGridDBColumn;
    ViewRealDayD4: TcxGridDBColumn;
    ViewRealDayD5: TcxGridDBColumn;
    ViewRealDayD6: TcxGridDBColumn;
    ViewRealDayD7: TcxGridDBColumn;
    ViewRealDayD8: TcxGridDBColumn;
    ViewRealDayD9: TcxGridDBColumn;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    frRepZak: TfrReport;
    frtaSpecZak: TfrDBDataSet;
    tePr: TdxMemData;
    tePrCode: TIntegerField;
    tePrName: TStringField;
    tePrFullName: TStringField;
    tePrEdIzm: TSmallintField;
    tePrRemn1: TFloatField;
    tePrvReal: TFloatField;
    tePrRemnDay: TFloatField;
    tePrRemnMin: TFloatField;
    tePrRemnMax: TFloatField;
    tePrRemn2: TFloatField;
    tePrQuant1: TFloatField;
    tePrPK: TFloatField;
    tePrQuantZ: TFloatField;
    tePrQuant2: TFloatField;
    tePrQuant3: TFloatField;
    tePrPriceIn: TFloatField;
    tePrSumIn: TFloatField;
    tePrNDSProc: TStringField;
    tePriCat: TIntegerField;
    tePriTop: TIntegerField;
    tePriNov: TIntegerField;
    teSpecZBarCode: TStringField;
    tePrBarCode: TStringField;
    tePrNum: TIntegerField;
    ViewDoc7: TcxGridDBTableView;
    LevelDoc7: TcxGridLevel;
    GridDoc7: TcxGrid;
    ViewDoc7RecId: TcxGridDBColumn;
    ViewDoc7Code: TcxGridDBColumn;
    ViewDoc7Name: TcxGridDBColumn;
    ViewDoc7FullName: TcxGridDBColumn;
    ViewDoc7EdIzm: TcxGridDBColumn;
    ViewDoc7Remn1: TcxGridDBColumn;
    ViewDoc7vReal: TcxGridDBColumn;
    ViewDoc7RemnDay: TcxGridDBColumn;
    ViewDoc7RemnMin: TcxGridDBColumn;
    ViewDoc7RemnMax: TcxGridDBColumn;
    ViewDoc7Remn2: TcxGridDBColumn;
    ViewDoc7Quant1: TcxGridDBColumn;
    ViewDoc7PK: TcxGridDBColumn;
    ViewDoc7QuantZ: TcxGridDBColumn;
    ViewDoc7Quant2: TcxGridDBColumn;
    ViewDoc7Quant3: TcxGridDBColumn;
    ViewDoc7PriceIn: TcxGridDBColumn;
    ViewDoc7SumIn: TcxGridDBColumn;
    ViewDoc7NDSProc: TcxGridDBColumn;
    ViewDoc7iCat: TcxGridDBColumn;
    ViewDoc7iTop: TcxGridDBColumn;
    ViewDoc7iN: TcxGridDBColumn;
    ViewDoc7BarCode: TcxGridDBColumn;
    teSpecZCliQuant: TFloatField;
    teSpecZCliPrice: TFloatField;
    teSpecZCliPrice0: TFloatField;
    teSpecZCliSumIn: TFloatField;
    teSpecZCliSumIn0: TFloatField;
    teSpecZCliQuantN: TFloatField;
    teSpecZCliPriceN: TFloatField;
    teSpecZCliPrice0N: TFloatField;
    teSpecZCliSumInN: TFloatField;
    teSpecZCliSumIn0N: TFloatField;
    ViewDoc7CliPrice: TcxGridDBColumn;
    ViewDoc7CliPrice0N: TcxGridDBColumn;
    ViewDoc7CliPriceN: TcxGridDBColumn;
    ViewDoc7CliQuant: TcxGridDBColumn;
    ViewDoc7CliQuantN: TcxGridDBColumn;
    ViewDoc7CliSumIn: TcxGridDBColumn;
    ViewDoc7CliSumIn0: TcxGridDBColumn;
    ViewDoc7CliSumIn0N: TcxGridDBColumn;
    ViewDoc7CliSumInN: TcxGridDBColumn;
    ViewDoc7CliPrice0: TcxGridDBColumn;
    teSpecZPriceIn0: TFloatField;
    teSpecZSumIn0: TFloatField;
    ViewDoc7PriceIn0: TcxGridDBColumn;
    ViewDoc7SumIn0: TcxGridDBColumn;
    teSpecZCliNNum: TIntegerField;
    ViewDoc7CliNNum: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure cxLabel2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure ViewDoc2DblClick(Sender: TObject);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acPostTovExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure teSpecZPriceInChange(Sender: TField);
    procedure teSpecZSumInChange(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure acAddAssPostExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acCalcZExecute(Sender: TObject);
    procedure acMoveSelExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewDoc71SelectionChanged(Sender: TcxCustomGridTableView);
    procedure ViewDoc7CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViewDoc7DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewDoc7DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc7Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure teSpecZQuant3Change(Sender: TField);
    procedure ViewDoc7SelectionChanged(Sender: TcxCustomGridTableView);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtons(bSt:Boolean);
  end;

var
  fmAddDoc7: TfmAddDoc7;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, sumprops, Move, MT, QuantMess,
  u2fdk, MainMCryst, TBuff;

{$R *.dfm}

procedure TfmAddDoc7.prButtons(bSt:Boolean);
begin
//  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;

  with dmMc do
  begin
//    if quDepartsSt.Active=False then quDepartsSt.Active:=True;

    quDepartsSt.Locate('ID',cxLookupComboBox1.EditValue,[]);

    if quDepartsStStatus3.AsInteger=1 then
    begin
    end else
    begin
    end;
  end;

end;


procedure TfmAddDoc7.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc7.Align:=alClient;
  ViewDoc7.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);

end;

procedure TfmAddDoc7.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var S:String;
begin
   if fmclients.Showing then fmclients.Close;
  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  if Label4.Tag<=1 then //����������
  begin
    fmClients.LevelCli.Active:=True;
    fmClients.LevelIP.Active:=False;
  end;
  if Label4.Tag=2 then //���������������
  begin
    fmClients.LevelCli.Active:=False;
    fmClients.LevelIP.Active:=True;
  end;

  S:=cxButtonEdit1.Text;
  if S>'' then
  begin
    fmClients.cxTextEdit1.Text:=S;

    with dmMC do
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('select Top 3 Vendor,Name,Raion,UnTaxedNDS from "RVendor"');
      quFCli.SQL.Add('where Name like ''%'+S+'%''');
      quFCli.SQL.Add('and Name not like ''%��%''');
      quFCli.Active:=True;

      if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        fmClients.LevelCli.Active:=True;
        fmClients.LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');
        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          fmClients.LevelCli.Active:=False;
          fmClients.LevelIP.Active:=True;
        end;
      end;
      quFCli.Active:=False;
      quFIP.Active:=False;
    end;
  end;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      Label4.Tag:=1;
      cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
      cxButtonEdit1.Text:=dmMC.quCliName.AsString;
    end else
    begin
      Label4.Tag:=2;
      cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
      cxButtonEdit1.Text:=dmMC.quIPName.AsString;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;

  bOpen:=False;
  iDirect:=0;
end;

procedure TfmAddDoc7.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc7.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=2 then  Accept:=True;
end;

procedure TfmAddDoc7.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc7.FormShow(Sender: TObject);
Var dDate:TDateTime;
begin
  iCol:=0;
  iColT:=0;
  bDrOut:=False;
  dDate:=Date;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc7.SetFocus;

  Memo1.Clear;

  ViewRealDayD9.Caption:=ds1(dDate-1);
  if dw(dDate-1)>5
    then ViewRealDayD9.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD9.Styles.Content:=nil;
  ViewRealDayD8.Caption:=ds1(dDate-2);
  if dw(dDate-2)>5
    then ViewRealDayD8.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD8.Styles.Content:=nil;
  ViewRealDayD7.Caption:=ds1(dDate-3);
  if dw(dDate-3)>5
    then ViewRealDayD7.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD7.Styles.Content:=nil;
  ViewRealDayD6.Caption:=ds1(dDate-4);
  if dw(dDate-4)>5
    then ViewRealDayD6.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD6.Styles.Content:=nil;
  ViewRealDayD5.Caption:=ds1(dDate-5);
  if dw(dDate-5)>5
    then ViewRealDayD5.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD5.Styles.Content:=nil;
  ViewRealDayD4.Caption:=ds1(dDate-6);
  if dw(dDate-6)>5
    then ViewRealDayD4.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD4.Styles.Content:=nil;
  ViewRealDayD3.Caption:=ds1(dDate-7);
  if dw(dDate-7)>5
    then ViewRealDayD3.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD3.Styles.Content:=nil;
  ViewRealDayD2.Caption:=ds1(dDate-8);
  if dw(dDate-8)>5
    then ViewRealDayD2.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD2.Styles.Content:=nil;
  ViewRealDayD1.Caption:=ds1(dDate-9);
  if dw(dDate-9)>5
    then ViewRealDayD1.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD1.Styles.Content:=nil;
  ViewRealDayD0.Caption:=ds1(dDate-10);
  if dw(dDate-10)>5
    then ViewRealDayD0.Styles.Content:=dmMC.cxStyle7
    else ViewRealDayD0.Styles.Content:=nil;

//  ViewDoc7NameG.Options.Editing:=False;
//  ViewTaraNameG.Options.Editing:=False;
end;

procedure TfmAddDoc7.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      bDrOut:=False;
      ViewDoc7.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    bDrOut:=False;
    ViewDoc7.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  end;
end;

procedure TfmAddDoc7.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if teSpecZ.RecordCount>0 then
  begin
    teSpecZ.Delete;
  end;
end;

procedure TfmAddDoc7.acAddListExecute(Sender: TObject);
begin
  iDirect:=22;
  fmCards.Show;
end;

procedure TfmAddDoc7.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rInSumIn:Real;
    iNum:Integer;
begin
  //��������
  with dmMC do
  with dmMT do
  begin

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton8.Enabled:=False;

    IdH:=cxTextEdit1.Tag;

    if teSpecZ.State in [dsEdit,dsInsert] then teSpecZ.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    //�� ������������ ������ ��������� �� ���� - ������

    try
      ViewDoc7.BeginUpdate;

      rInSumIn:=0;
      iCol:=0;

      //��������� �����
      teSpecZ.First;
      while not teSpecZ.Eof do
      begin
        rInSumIn:=rInSumIn+rv(teSpecZSumIn.AsFloat);
        teSpecZ.Next;
      end;

      if ptZHead.Active=False then ptZHead.Active:=True;
      ptZHead.Refresh;

      if IDH=0 then //����������
      begin
        ptZHead.Append;
        ptZHeadDOCDATE.AsDateTime:=cxDateEdit1.Date;
        ptZHeadDOCNUM.AsString:=AnsiToOemConvert(cxTextEdit1.Text);
        ptZHeadDEPART.AsInteger:=cxLookupComboBox1.EditValue;
        ptZHeadITYPE.AsInteger:=0;
        ptZHeadINSUMIN.AsFloat:=rInSumIn;
        ptZHeadINSUMOUT.AsFloat:=0;
        ptZHeadOUTSUMIN.AsFloat:=0;
        ptZHeadOUTSUMOUT.AsFloat:=0;
        ptZHeadCLITYPE.AsInteger:=Label4.Tag;
        ptZHeadCLICODE.AsInteger:=cxButtonEdit1.Tag;
        ptZHeadIACTIVE.AsInteger:=0;
        ptZHeadDATEEDIT.AsDateTime:=Trunc(Date);
        ptZHeadTIMEEDIT.AsString:=TimeToStr(now);
        ptZHeadPERSON.AsString:=CommonSet.Ip+'('+AnsiToOemConvert(Person.Name)+')';
        ptZHeadCLIQUANT.AsFloat:=0;
        ptZHeadCLISUMIN0.AsFloat:=0;
        ptZHeadCLISUMIN.AsFloat:=0;
        ptZHeadCLIQUANTN.AsFloat:=0;
        ptZHeadCLISUMIN0N.AsFloat:=0;
        ptZHeadCLISUMINN.AsFloat:=0;
        ptZHeadIDATENACL.AsInteger:=0;
        ptZHeadSNUMNACL.AsString:='';
        ptZHeadIDATESCHF.AsInteger:=0;
        ptZHeadSNUMSCHF.AsString:='';

        ptZHead.Post;
        delay(20);

        ptZHead.Refresh;
        IDH:=ptZHeadID.AsInteger;
        cxTextEdit1.Tag:=IdH;

        iNum:=StrToINtDef(cxTextEdit1.Text,0);
        if (iNum>0) and (iNum=Label1.Tag) then
        begin //��������� �����
          quE.SQL.Clear;
          quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
          quE.SQL.Add('where CodeObject=22');
          quE.ExecSQL;
        end;

      end else
      begin
        if ptZHead.FindKey([IDH]) then
        begin
          ptZHead.Edit;
          ptZHeadDOCDATE.AsDateTime:=cxDateEdit1.Date;
          ptZHeadDOCNUM.AsString:=AnsiToOemConvert(cxTextEdit1.Text);
          ptZHeadDEPART.AsInteger:=cxLookupComboBox1.EditValue;
          ptZHeadITYPE.AsInteger:=0;
          ptZHeadINSUMIN.AsFloat:=rInSumIn;
          ptZHeadINSUMOUT.AsFloat:=0;
          ptZHeadOUTSUMIN.AsFloat:=0;
          ptZHeadOUTSUMOUT.AsFloat:=0;
          ptZHeadCLITYPE.AsInteger:=Label4.Tag;
          ptZHeadCLICODE.AsInteger:=cxButtonEdit1.Tag;
          ptZHeadIACTIVE.AsInteger:=0;
          ptZHeadDATEEDIT.AsDateTime:=Date;
          ptZHeadTIMEEDIT.AsString:=TimeToStr(now);
          ptZHeadPERSON.AsString:=CommonSet.Ip+'('+AnsiToOemConvert(Person.Name)+')';
          ptZHead.Post;
          delay(20);
        end;
      end;

      if ptZSpec.Active=False then ptZSpec.Active:=True;
      ptZSpec.CancelRange;
      ptZSpec.IndexFieldNames:='IDH;IDS';
      ptZSpec.SetRange([IDH],[IDH]);
      ptZSpec.First;
      while not ptZSpec.Eof do
      begin
        if teSpecZ.Locate('Code',ptZSpecCODE.AsInteger,[]) then
        begin
          ptZSpec.Edit;
          ptZSpecQUANT.AsFloat:=0;
          ptZSpecPRICEIN.AsFloat:=teSpecZPriceIn.AsFloat;
          ptZSpecSUMIN.AsFloat:=teSpecZSumIn.AsFloat;
          ptZSpecNDSPROC.AsFloat:=teSpecZNDSProc.AsFloat;
          ptZSpecREMN1.AsFloat:=teSpecZRemn1.AsFloat;
          ptZSpecVREAL.AsFloat:=teSpecZvReal.AsFloat;
          ptZSpecREMNDAY.AsFloat:=teSpecZRemnDay.AsFloat;
          ptZSpecREMNMIN.AsInteger:=teSpecZRemnMin.AsInteger;
          ptZSpecREMNMAX.AsInteger:=teSpecZRemnMax.AsInteger;
          ptZSpecREMN2.AsFloat:=teSpecZRemn2.AsFloat;
          ptZSpecQUANT1.AsFloat:=teSpecZQuant1.AsFloat;
          ptZSpecPK.AsFloat:=teSpecZPK.AsFloat;
          ptZSpecQUANTZ.AsFloat:=teSpecZQuantZ.AsFloat;
          ptZSpecQUANT2.AsFloat:=teSpecZQuant2.AsFloat;
          ptZSpecQUANT3.AsFloat:=teSpecZQuant3.AsFloat;
          {
          ptZSpecCLIQUANT.AsFloat:=
          ptZSpecCLIPRICE: TFloatField
          ptZSpecCLIPRICE0: TFloatField
          ptZSpecCLIQUANTN: TFloatField
          ptZSpecCLIPRICEN: TFloatField
          ptZSpecCLIPRICE0N: TFloatField
          }

          ptZSpec.Post;

          ptZSpec.Next;
        end else ptZSpec.Delete ;
      end;

      delay(10);

      teSpecZ.First;
      while not teSpecZ.Eof do
      begin
        if ptZSpec.Locate('Code',teSpecZCode.AsInteger,[])=False then
        begin
          ptZSpec.Append;
          ptZSpecIDH.AsInteger:=IDH;
          ptZSpecCODE.AsInteger:=teSpecZCode.AsInteger;
          ptZSpecQUANT.AsFloat:=0;
          ptZSpecPRICEIN.AsFloat:=teSpecZPriceIn.AsFloat;
          ptZSpecSUMIN.AsFloat:=teSpecZSumIn.AsFloat;
          ptZSpecNDSPROC.AsFloat:=teSpecZNDSProc.AsFloat;
          ptZSpecREMN1.AsFloat:=teSpecZRemn1.AsFloat;
          ptZSpecVREAL.AsFloat:=teSpecZvReal.AsFloat;
          ptZSpecREMNDAY.AsFloat:=teSpecZRemnDay.AsFloat;
          ptZSpecREMNMIN.AsInteger:=teSpecZRemnMin.AsInteger;
          ptZSpecREMNMAX.AsInteger:=teSpecZRemnMax.AsInteger;
          ptZSpecREMN2.AsFloat:=teSpecZRemn2.AsFloat;
          ptZSpecQUANT1.AsFloat:=teSpecZQuant1.AsFloat;
          ptZSpecPK.AsFloat:=teSpecZPK.AsFloat;
          ptZSpecQUANTZ.AsFloat:=teSpecZQuantZ.AsFloat;
          ptZSpecQUANT2.AsFloat:=teSpecZQuant2.AsFloat;
          ptZSpecQUANT3.AsFloat:=teSpecZQuant3.AsFloat;
          ptZSpecCLIQUANT.AsFloat:=0;
          ptZSpecCLIPRICE.AsFloat:=0;
          ptZSpecCLIPRICE0.AsFloat:=0;
          ptZSpecCLIQUANTN.AsFloat:=0;
          ptZSpecCLIPRICEN.AsFloat:=0;
          ptZSpecCLIPRICE0N.AsFloat:=0;

          ptZSpec.Post;
        end;
        teSpecZ.Next;
      end;
    finally
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      ViewDoc7.EndUpdate;

      quZakH.Active:=False;
      quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quZakH.Active:=True;

      quZakH.Locate('ID',IDH,[]);
    end;
  end;
end;

procedure TfmAddDoc7.acDelAllExecute(Sender: TObject);
begin
  if teSpecZ.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
    begin
      try
        ViewDoc7.BeginUpdate;
        CloseTe(teSpecZ);
      finally
        ViewDoc7.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmAddDoc7.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc7.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc7.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc7.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc7.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrOut then  Accept:=True;
end;

procedure TfmAddDoc7.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc7.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc7.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmMC do
    begin
     { quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;}
    end;
  end;
end;

procedure TfmAddDoc7.ViewDoc2DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
    if (ViewDoc7.Controller.FocusedColumn.Name='ViewDoc7Name') then
    begin
      ViewDoc7Name.Options.Editing:=True;
      ViewDoc7Name.Focused:=True;
    end;
end;

procedure TfmAddDoc7.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxButton1.SetFocus;
  end;
end;

procedure TfmAddDoc7.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc7.cxButton6Click(Sender: TObject);
begin //��� �������� ����� ��������
end;


procedure TfmAddDoc7.cxButton7Click(Sender: TObject);
begin //��� �������� ����������
end;

procedure TfmAddDoc7.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc7.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc7.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc7.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc7.Columns[j].Name='ViewDoc7Code' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc7.acPostTovExecute(Sender: TObject);
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if teSpecZ.RecordCount>0 then
    begin
      bDrObm:=True;
      
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=teSpecZCode.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=teSpecZCode.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=teSpecZName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc7.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc7);
end;

procedure TfmAddDoc7.teSpecZPriceInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then
  begin
    rQ:=teSpecZQuant3.AsFloat;
    teSpecZSumIn.AsFloat:=rQ*teSpecZPriceIn.AsFloat;
  end;
end;

procedure TfmAddDoc7.teSpecZSumInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=3 then
  begin
    rQ:=teSpecZQuant3.AsFloat;
    if rQ<>0 then
    begin
      teSpecZPriceIn.AsFloat:=rv(teSpecZSumIn.AsFloat)/rQ;
    end;
  end;
end;

procedure TfmAddDoc7.cxLabel3Click(Sender: TObject);
begin
  //�������� ����������� ����������
  if cxButton1.Enabled then acAddAssPost.Execute;
end;

procedure TfmAddDoc7.acAddAssPostExecute(Sender: TObject);
Var iC:INteger;
    sName:String;
begin
  //�������� ����������� ����������
  if (Label4.Tag>0)and(cxButtonEdit1.Tag>0) then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('����� ���� ������������ ������...');
    with dmMC do
    with dmMT do
    begin
      try
        ViewDoc7.BeginUpdate;
        CloseTe(teSpecZ);

        quAssPost.Active:=False;
        quAssPost.ParamByName('ICLIT').AsInteger:=Label4.Tag;
        quAssPost.ParamByName('ICLI').AsInteger:=cxButtonEdit1.Tag;
        quAssPost.Active:=True;

        iC:=0;

        quAssPost.First;
        while not quAssPost.Eof do
        begin
          if taCards.FindKey([quAssPostGoodsId.AsInteger]) then
          begin
            sName:=OemToAnsiConvert(taCardsName.AsString);
            if sName[1]<>'*' then
            begin
              if teSpecZ.Locate('Code',taCardsID.AsInteger,[])= False then
              begin
                teSpecZ.Append;
                teSpecZCode.AsInteger:=taCardsID.AsInteger;
                teSpecZName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                teSpecZEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                teSpecZFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                teSpecZiCat.AsInteger:=taCardsV02.AsInteger;
                teSpecZiTop.AsInteger:=taCardsV04.AsInteger;
                teSpecZiN.AsInteger:=taCardsV05.AsInteger;
                teSpecZRemn1.AsFloat:=0;
                teSpecZvReal.AsFloat:=0;
                teSpecZRemnDay.AsFloat:=0;
                teSpecZRemnMin.AsInteger:=taCardsV06.AsInteger;
                teSpecZRemnMax.AsInteger:=taCardsV07.AsInteger;
                teSpecZRemn2.AsFloat:=0;
                teSpecZQuant1.AsFloat:=0;
                teSpecZPK.AsFloat:=fmAddDoc7.cxCalcEdit1.Value;
                teSpecZQuantZ.AsFloat:=0;
                teSpecZQuant2.AsFloat:=0;
                teSpecZQuant3.AsFloat:=0;
                teSpecZPriceIn.AsFloat:=quAssPostPriceIn.AsFloat;
                teSpecZSumIn.AsFloat:=0;
                teSpecZNDSProc.AsFloat:=taCardsNDS.AsFloat;
                teSpecZBarCode.AsString:=taCardsBarCode.AsString;
                teSpecZ.Post;
              end else
              begin
                teSpecZ.Edit;
                teSpecZPriceIn.AsFloat:=quAssPostPriceIn.AsFloat;
                teSpecZSumIn.AsFloat:=rv(teSpecZQuant3.AsFloat*quAssPostPriceIn.AsFloat);
                teSpecZNDSProc.AsFloat:=taCardsNDS.AsFloat;
                teSpecZ.Post;
              end;
            end;
          end;

          quAssPost.Next;
          inc(iC);
          if iC mod 100 =0 then
          begin
            Memo1.Lines.Add('  ���������� - '+its(iC));
            delay(10);
          end;
        end;
        quAssPost.Active:=False;

      finally
        ViewDoc7.EndUpdate;
      end;
    end;
    Memo1.Lines.Add('������������ ��.');
  end;
end;

procedure TfmAddDoc7.cxLabel4Click(Sender: TObject);
begin
  //������
  if cxButton1.Enabled then acCalcZ.Execute;
end;

procedure TfmAddDoc7.acCalcZExecute(Sender: TObject);
Var iC:INteger;
    rRemn,rQz:Real;
    rSpeedDay,rQDay:Real;
    iCountDay:INteger;
    iDate:INteger;
begin
  with dmMt do
  with dmMC do
  begin
    try
      ViewDoc7.BeginUpdate;
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������ ...');  delay(10);

      if ptZHead.Active=False then ptZHead.Active:=True
      else ptZHead.Refresh;

      if ptZSpec.Active=False then ptZSpec.Active:=True
      else ptZSpec.Refresh;

      iC:=0;
      iCol:=0;
      teSpecZ.First;
      while not teSpecZ.Eof do
      begin
        if taCards.FindKey([teSpecZCode.AsInteger]) then
        begin
          iDate:=Trunc(cxDateEdit1.Date);
          rRemn:=prFindTabRemn(teSpecZCode.AsInteger);
          prFindTabSpeedRemn(teSpecZCode.AsInteger,0,rSpeedDay,rQDay);
          rQz:=prFindQzAlready(teSpecZCode.AsInteger,iDate);
          iCountDay:=Trunc(cxDateEdit1.Date)-Trunc(Date);

          teSpecZ.Edit;
          teSpecZRemn1.AsFloat:=rRemn;
          teSpecZvReal.AsFloat:=rSpeedDay;
          teSpecZRemnDay.AsFloat:=rQDay;
          teSpecZRemnMin.AsInteger:=taCardsV06.AsInteger;
          teSpecZRemnMax.AsInteger:=taCardsV07.AsInteger;
          teSpecZPK.AsFloat:=cxCalcEdit1.Value;
          teSpecZQuantZ.AsFloat:=rQz;
          teSpecZRemn2.AsFloat:=rRemn-rSpeedDay*iCountDay;

          if rQDay<teSpecZRemnMax.AsInteger then
          begin
            if (rRemn-rSpeedDay*iCountDay)>0 then
              teSpecZQuant1.AsFloat:=(teSpecZRemnMax.AsInteger*rSpeedDay)-(rRemn-rSpeedDay*iCountDay)
            else
              teSpecZQuant1.AsFloat:=(teSpecZRemnMax.AsInteger*rSpeedDay);

            teSpecZQuant2.AsFloat:=teSpecZQuant1.AsFloat*cxCalcEdit1.Value-rQz;
            teSpecZQuant3.AsFloat:=teSpecZQuant1.AsFloat*cxCalcEdit1.Value-rQz;

            if teSpecZQuant1.AsFloat<0 then teSpecZQuant1.AsFloat:=0;
            if teSpecZQuant2.AsFloat<0 then teSpecZQuant2.AsFloat:=0;
            if teSpecZQuant3.AsFloat<0 then teSpecZQuant3.AsFloat:=0;

            if teSpecZQuant2.AsFloat<taCardsKol.AsFloat then teSpecZQuant2.AsFloat:=0;
            if teSpecZQuant3.AsFloat<taCardsKol.AsFloat then teSpecZQuant3.AsFloat:=0;
          end else
          begin
            teSpecZQuant1.AsFloat:=0;
            teSpecZQuant2.AsFloat:=0;
            teSpecZQuant3.AsFloat:=0;
          end;

          teSpecZSumIn.AsFloat:=rv(teSpecZPriceIn.AsFloat*teSpecZQuant3.AsFloat);
          teSpecZ.Post;
        end;

        teSpecZ.Next;
        inc(iC);
        if iC mod 20 =0 then
        begin
          Memo1.Lines.Add('  ���������� - '+its(iC));
          delay(10);
        end;
      end;

      Memo1.Lines.Add('������ ��.');
    finally
      ViewDoc7.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc7.acMoveSelExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewDoc7.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teSpecZCode.AsInteger;
      prFillMove(teSpecZCode.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teSpecZName.AsString+'('+teSpecZCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc7.cxButton3Click(Sender: TObject);
Var vPP:TfrPrintPages;
    iNum:INteger;
    sName:String;
begin
  try
    ViewDoc7.BeginUpdate;
    CloseTe(tePr);

    iNUm:=0;

    teSpecZ.First;
    while not teSpecZ.Eof do
    begin
      sName:=teSpecZName.AsString;

      if sName[1]<>'*'  then
      begin
        if teSpecZQuant3.AsFloat>0.001 then
        begin
          inc(iNum);

          tePr.Append;
          tePrNum.AsInteger:=iNum;
          tePrCode.AsInteger:=teSpecZCode.AsInteger;
          tePrName.AsString:=teSpecZName.AsString;
          tePrFullName.AsString:=teSpecZFullName.AsString;
          tePrEdIzm.AsInteger:=teSpecZEdIzm.AsInteger;
          tePrBarCode.AsString:=teSpecZBarCode.AsString;
          tePrQuant3.AsFloat:=teSpecZQuant3.AsFloat;
          tePrPriceIn.AsFloat:=teSpecZPriceIn.AsFloat;
          tePr.Post;

        end;
      end;

      teSpecZ.Next;
    end;

    frRepZak.ReportName:='������ �� �������� ������.';

    frRepZak.LoadFromFile(CommonSet.Reports+'zakaz.frf');


    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['CliTo']:=cxButtonEdit1.Text;

    with dmMC do
    begin
      quDepPars.Active:=False;
      quDepPars.ParamByName('ID').AsInteger:=cxLookupComboBox1.EditValue;
      quDepPars.Active:=True;
      if quDepPars.RecordCount>0 then
      begin
        frVariables.Variable['CliFrom']:=quDepParsNAMEDEP.AsString;
        frVariables.Variable['AdrFrom']:=quDepParsADROTPR.AsString;
      end else
      begin
        frVariables.Variable['Cli1Name']:=cxLookupComboBox1.Text;
        frVariables.Variable['AdrFrom']:='';
      end;

      quDepPars.Active:=False;
    end;

    frRepZak.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepZak.ShowPreparedReport
    else frRepZak.PrintPreparedReport('',1,False,vPP);
  finally
    ViewDoc7.EndUpdate;
  end;
end;

procedure TfmAddDoc7.ViewDoc71SelectionChanged(
  Sender: TcxCustomGridTableView);
Var iCode:Integer;
    i:Integer;
begin
// ����� ���
//  ShowMessage('���');

  if ViewDoc7.Controller.SelectedRecordCount=1 then
  begin
    iCode:=teSpecZCode.AsInteger;
    prFillReal10(iCode,Trunc(Date-1),10);

    teRealDay.Active:=False;
    teRealDay.Active:=True;

    teRealDay.Append;
    for i:=1 to 10 do teRealDay.FieldByName('D'+its(i-1)).AsFloat:=arRD[i];
    teRealDay.Post;

  end;
end;

procedure TfmAddDoc7.ViewDoc7CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    StrWk:String;
    rQ:Real;
begin
  StrWk:='';
  for i:=0 to ViewDoc7.ColumnCount-1 do
  begin
    if ViewDoc7.Columns[i].Name='ViewDoc7Quant3' then StrWk:=AViewInfo.GridRecord.DisplayTexts[i];
  end;
  rQ:=StrToFloatDef(StrWk,0);
  if rQ>0 then ACanvas.Canvas.Brush.Color :=  $00BFFFBF;  //$0000FD00; //

//  if rQ>0 then ACanvas.Canvas.Brush.Color := $00B3B3FF; //��� ��������� �������
//  if rQ>0 then ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� ������� �������
end;

procedure TfmAddDoc7.ViewDoc7DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if iDirect=22 then
  begin
    iDirect:=0; //������ ���������
    iCol:=0;

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        with dmMT do
        begin
          ViewDoc7.BeginUpdate;
          try

          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            if taCards.FindKey([iNum]) then
            begin
              if teSpecZ.Locate('Code',taCardsID.AsInteger,[])= False then
              begin
                teSpecZ.Append;
                teSpecZCode.AsInteger:=taCardsID.AsInteger;
                teSpecZName.AsString:=OemToAnsiConvert(taCardsName.AsString);
                teSpecZEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
                teSpecZFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                teSpecZiCat.AsInteger:=taCardsV02.AsInteger;
                teSpecZiTop.AsInteger:=taCardsV04.AsInteger;
                teSpecZiN.AsInteger:=taCardsV05.AsInteger;
                teSpecZRemn1.AsFloat:=0;
                teSpecZvReal.AsFloat:=0;
                teSpecZRemnDay.AsFloat:=0;
                teSpecZRemnMin.AsInteger:=taCardsV06.AsInteger;
                teSpecZRemnMax.AsInteger:=taCardsV07.AsInteger;
                teSpecZRemn2.AsFloat:=0;
                teSpecZQuant1.AsFloat:=0;
                teSpecZPK.AsFloat:=fmAddDoc7.cxCalcEdit1.Value;
                teSpecZQuantZ.AsFloat:=0;
                teSpecZQuant2.AsFloat:=0;
                teSpecZQuant3.AsFloat:=0;
                teSpecZPriceIn.AsFloat:=0;
                teSpecZSumIn.AsFloat:=0;
                teSpecZNDSProc.AsFloat:=taCardsNDS.AsFloat;
                teSpecZBarCode.AsString:=taCardsBarCode.AsString;
                teSpecZ.Post;
              end;
            end;
          end;

          finally
            ViewDoc7.EndUpdate;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfmAddDoc7.ViewDoc7DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=22 then  Accept:=True;
end;

procedure TfmAddDoc7.ViewDoc7Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc7.Controller.FocusedColumn.Name='ViewDoc7Quant3' then iCol:=1;
  if ViewDoc7.Controller.FocusedColumn.Name='ViewDoc7PriceIn' then iCol:=2;
  if ViewDoc7.Controller.FocusedColumn.Name='ViewDoc7SumIn' then iCol:=3;
end;

procedure TfmAddDoc7.teSpecZQuant3Change(Sender: TField);
begin
  if iCol=1 then //���-�� � ������
  begin
    teSpecZSumIn.AsFloat:=rv(teSpecZQuant3.AsFloat*teSpecZPriceIn.AsFloat);
  end;
end;


procedure TfmAddDoc7.ViewDoc7SelectionChanged(
  Sender: TcxCustomGridTableView);
Var iCode:Integer;
    i:Integer;
begin
 if ViewDoc7.Controller.SelectedRecordCount=1 then
  begin
    iCode:=teSpecZCode.AsInteger;
    prFillReal10(iCode,Trunc(Date-1),10);

    teRealDay.Active:=False;
    teRealDay.Active:=True;

    teRealDay.Append;
    for i:=1 to 10 do teRealDay.FieldByName('D'+its(i-1)).AsFloat:=arRD[i];
    teRealDay.Post;
  end;
end;

end.
