unit MainFromCashToPosFB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, Menus,
  cxLookAndFeelPainters, cxButtons, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxMemo, FIBDatabase, pFIBDatabase, cxSpinEdit, DB, FIBDataSet,
  pFIBDataSet, FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TfmMainCashOut = class(TForm)
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    Memo1: TcxMemo;
    CasherDb: TpFIBDatabase;
    Cash: TpFIBDatabase;
    trSelectCash: TpFIBTransaction;
    trUpdateCash: TpFIBTransaction;
    Label4: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label5: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quSumTr: TpFIBDataSet;
    quSumTrOPERATION: TFIBSmallIntField;
    quSumTrSUMOP: TFIBFloatField;
    prDelCashZ: TpFIBStoredProc;
    quCashPay: TpFIBDataSet;
    quCashPaySHOPINDEX: TFIBSmallIntField;
    quCashPayCASHNUMBER: TFIBIntegerField;
    quCashPayZNUMBER: TFIBIntegerField;
    quCashPayCHECKNUMBER: TFIBIntegerField;
    quCashPayCASHER: TFIBIntegerField;
    quCashPayCHECKSUM: TFIBFloatField;
    quCashPayPAYSUM: TFIBFloatField;
    quCashPayDSUM: TFIBFloatField;
    quCashPayDBAR: TFIBStringField;
    quCashPayCOUNTPOS: TFIBSmallIntField;
    quCashPayCHDATE: TFIBDateTimeField;
    quCashPayPAYMENT: TFIBSmallIntField;
    prAddCashP: TpFIBStoredProc;
    prAddCashS: TpFIBStoredProc;
    fbCashSail: TpFIBDataSet;
    fbCashSailSHOPINDEX: TFIBSmallIntField;
    fbCashSailCASHNUMBER: TFIBIntegerField;
    fbCashSailZNUMBER: TFIBIntegerField;
    fbCashSailCHECKNUMBER: TFIBIntegerField;
    fbCashSailID: TFIBIntegerField;
    fbCashSailCHDATE: TFIBDateTimeField;
    fbCashSailARTICUL: TFIBStringField;
    fbCashSailBAR: TFIBStringField;
    fbCashSailCARDSIZE: TFIBStringField;
    fbCashSailQUANTITY: TFIBFloatField;
    fbCashSailPRICERUB: TFIBFloatField;
    fbCashSailDPROC: TFIBFloatField;
    fbCashSailDSUM: TFIBFloatField;
    fbCashSailTOTALRUB: TFIBFloatField;
    fbCashSailCASHER: TFIBIntegerField;
    fbCashSailDEPART: TFIBIntegerField;
    fbCashSailOPERATION: TFIBSmallIntField;
    fbCashSailSECPOS: TFIBSmallIntField;
    fbCashSailECHECK: TFIBSmallIntField;
    quZListDet2: TpFIBDataSet;
    quZListDet2CASHNUM: TFIBSmallIntField;
    quZListDet2ZNUM: TFIBIntegerField;
    quZListDet2INOUT: TFIBSmallIntField;
    quZListDet2ITYPE: TFIBSmallIntField;
    quZListDet2RSUM: TFIBFloatField;
    quZListDet2IDATE: TFIBIntegerField;
    quZListDet2DDATE: TFIBDateTimeField;
    quZListDet: TpFIBDataSet;
    quZListDetCASHNUM: TFIBSmallIntField;
    quZListDetZNUM: TFIBIntegerField;
    quZListDetINOUT: TFIBSmallIntField;
    quZListDetITYPE: TFIBSmallIntField;
    quZListDetRSUM: TFIBFloatField;
    quZListDetIDATE: TFIBIntegerField;
    quZListDetDDATE: TFIBDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure WrMess(Strwk_: string);
  end;

procedure Delay(MSecs: Longint);
function RV( X: Double ): Double;

var
  fmMainCashOut: TfmMainCashOut;
  bStop:Boolean=False;
  CurDir:String;

implementation

{$R *.dfm}


function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
  try
    ScaledFractPart := Trunc(X*100);
    if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
    if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
    if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
    RV:= ScaledFractPart/100;
  except
    RV:=0;
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

procedure TfmMainCashOut.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  cxTextEdit1.Text:='192.168.11.101:E:\_Casher\DB\CASHERDB.GDB';
  cxTextEdit2.Text:='192.168.11.150:D:\PosFb\cash.gdb';
  cxDateEdit1.Date:=Date-8;
  cxDateEdit2.Date:=Date-1;
  Memo1.Clear;
end;

procedure TfmMainCashOut.WrMess(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
//    strwk1:=FormatDateTime('yyyy_mm',Date);
    StrWk_:='     '+StrWk_;
    Memo1.Lines.Add(StrWk_);
    Strwk1:='Receiver.txt';
    Application.ProcessMessages;
    FileN:=CurDir+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


procedure TfmMainCashOut.cxButton2Click(Sender: TObject);
Var TimeShift,dBeg,dEnd:TDateTime;
    iDateB,iDateE,iCurrD:INteger;
    StrWk:String;
    rDisc,rDiscPr:Real;
    SumRealN,SumRetN,SumRealB,SumRetB:Real;
    CashNum:INteger;
    iZ:INteger;
begin
  if (CasherDb.Connected=True)and(Cash.Connected=True) then
  begin
    WrMess('');
    WrMess('���� ��.');
    try
      cxButton1.Enabled:=False;
      cxButton2.Enabled:=False;
      cxButton4.Enabled:=False;


      WrMess('  �������� �������� ������.');

      iDateB:=Trunc(cxDateEdit1.Date);
      iDateE:=Trunc(cxDateEdit2.Date);
      TimeShift:=1/24*2;  //2-���� ����
      CashNum:=cxSpinEdit1.Value;

      for iCurrD:=iDateB to iDateE do
      begin
        dBeg:=iCurrD+TimeShift;
        dEnd:=iCurrD+1+TimeShift;

        SumRetN:=0;
        SumRealN:=0;
        SumRetB:=0;
        SumRealB:=0;


        quSumTr.Active:=False;
        quSumTr.SelectSQL.Clear;
//          quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB-DSUM) as SumOp  FROM CASHSAIL');
        quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB) as SumOp  FROM CASHSAIL');
        quSumTr.SelectSQL.Add('WHERE');
        quSumTr.SelectSQL.Add('SHOPINDEX=1');
        quSumTr.SelectSQL.Add('and CASHNUMBER='+IntToStr(CashNum));

        quSumTr.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
        quSumTr.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');

        quSumTr.SelectSQL.Add('Group by Operation');
        quSumTr.SelectSQL.Add('Order by Operation');
        quSumTr.Active:=True;

        quSumTr.First;
        while not quSumTr.Eof do
        begin
          Case quSumTrOPERATION.AsInteger of
          0:SumRetN:=quSumTrSUMOP.AsFloat;
          1:SumRealN:=quSumTrSUMOP.AsFloat;
          4:SumRetB:=quSumTrSUMOP.AsFloat;
          5:SumRealB:=quSumTrSUMOP.AsFloat;
          end;

          quSumTr.Next;
        end;
        quSumTr.Active:=False;

        WrMess(' ������������ ���� - '+formatDateTime('dd.mm.yyyy',iCurrD));
        WrMess('    ����� � �����:');
        WrMess('     �������  ���. - '+FloatToStr(rv(SumRealN)));
        WrMess('     �������� ���. - '+FloatToStr(rv(SumRetN)));
        WrMess('     �������  �/�. - '+FloatToStr(rv(SumRealB)));
        WrMess('     �������� �/�. - '+FloatToStr(rv(SumRetB)));

        SumRetN:=0;
        SumRealN:=0;
        SumRetB:=0;
        SumRealB:=0;

        //������

//        EXECUTE PROCEDURE DELCASHSZDAY (?CASHNUMBER, ?ZNUM, ?ZDATEB, ?ZDATEE)

        prDelCashZ.ParamByName('CASHNUMBER').AsInteger:=CashNum;
        prDelCashZ.ParamByName('ZNUM').AsInteger:=0;
        prDelCashZ.ParamByName('ZDATEB').AsDateTime:=dBeg;
        prDelCashZ.ParamByName('ZDATEE').AsDateTime:=dEnd;
        prDelCashZ.ExecProc;

        delay(33);

        quCashPay.Active:=False;
        quCashPay.SelectSQL.Clear;
        quCashPay.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,CHECKSUM,PAYSUM,DSUM,DBAR,COUNTPOS,CHDATE,PAYMENT');
        quCashPay.SelectSQL.Add('FROM CASHPAY');
        quCashPay.SelectSQL.Add('where DSUM<>0');
        quCashPay.SelectSQL.Add('and CASHNUMBER='+IntToStr(CashNum));
        quCashPay.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
        quCashPay.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
        quCashPay.Active:=True;

        quCashPay.First;
        while not quCashPay.Eof do
        begin
          prAddCashP.ParamByName('SHOPINDEX').AsInteger:=quCashPaySHOPINDEX.AsInteger;
          prAddCashP.ParamByName('CASHNUMBER').AsInteger:=quCashPayCASHNUMBER.AsInteger;
          prAddCashP.ParamByName('ZNUMBER').AsInteger:=quCashPayZNUMBER.AsInteger;
          prAddCashP.ParamByName('CHECKNUMBER').AsInteger:=quCashPayCHECKNUMBER.AsInteger;
          prAddCashP.ParamByName('CASHER').AsInteger:=quCashPayCASHER.AsInteger;
          prAddCashP.ParamByName('CHECKSUM').AsFloat:=quCashPayCHECKSUM.AsFloat;
          prAddCashP.ParamByName('PAYSUM').AsFloat:=quCashPayPAYSUM.AsFloat;
          prAddCashP.ParamByName('DSUM').AsFloat:=quCashPayDSUM.AsFloat;
          prAddCashP.ParamByName('DBAR').AsString:=quCashPayDBAR.AsString;
          prAddCashP.ParamByName('COUNTPOS').AsInteger:=quCashPayCOUNTPOS.AsInteger;
          prAddCashP.ParamByName('CHDATE').AsDateTime:=quCashPayCHDATE.AsDateTime;
          prAddCashP.ParamByName('PAYMENT').AsInteger:=quCashPayPAYMENT.AsInteger;
          prAddCashP.ExecProc;

          quCashPay.Next;
        end;

        //�������� �����
        fbCashSail.Active:=False;
        fbCashSail.SelectSQL.Clear;
        fbCashSail.SelectSQL.Add('SELECT cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID,cs.CHDATE,cs.ARTICUL,cs.BAR,cs.CARDSIZE,cs.QUANTITY,cs.PRICERUB,cs.DPROC,cs.DSUM,cs.TOTALRUB,cs.CASHER,cs.DEPART,cs.OPERATION,cs.SECPOS,cs.ECHECK');
        fbCashSail.SelectSQL.Add('FROM CASHSAIL cs');
        fbCashSail.SelectSQL.Add('where');
        fbCashSail.SelectSQL.Add('SHOPINDEX=1');
        fbCashSail.SelectSQL.Add('and cs.CASHNUMBER='+IntToStr(CashNum));
        fbCashSail.SelectSQL.Add('and cs.CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
        fbCashSail.SelectSQL.Add('and cs.CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
        fbCashSail.SelectSQL.Add('Order by cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID');
        fbCashSail.Active:=True;

        fbCashSail.First;
        while not fbCashSail.Eof do
        begin
          Case fbCashSailOPERATION.AsInteger of
          0:SumRetN:=SumRetN+fbCashSailTotalRub.AsFloat;
          1:SumRealN:=SumRealN+fbCashSailTotalRub.AsFloat;
          4:SumRetB:=SumRetB+fbCashSailTotalRub.AsFloat;
          5:SumRealB:=SumRealB+fbCashSailTotalRub.AsFloat;
          end;

//          EXECUTE PROCEDURE ADDCASHS (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?CHECKNUMBER, ?ID, ?CHDATE, ?ARTICUL, ?BAR, ?CARDSIZE, ?QUANTITY, ?PRICERUB, ?DPROC, ?DSUM, ?TOTALRUB, ?CASHER, ?DEPART, ?OPERATION)
          prAddCashS.ParamByName('SHOPINDEX').AsInteger:=fbCashSailSHOPINDEX.AsInteger;
          prAddCashS.ParamByName('CASHNUMBER').AsInteger:=fbCashSailCASHNUMBER.AsInteger;
          prAddCashS.ParamByName('ZNUMBER').AsInteger:=fbCashSailZNUMBER.AsInteger;
          prAddCashS.ParamByName('CHECKNUMBER').AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
          prAddCashS.ParamByName('ID').AsInteger:=fbCashSailID.AsInteger;
          prAddCashS.ParamByName('CHDATE').AsDateTime :=fbCashSailCHDATE.AsDateTime;
          prAddCashS.ParamByName('ARTICUL').AsString:=fbCashSailARTICUL.AsString;
          prAddCashS.ParamByName('BAR').AsString:=fbCashSailBAR.AsString;
          prAddCashS.ParamByName('CARDSIZE').AsString:=fbCashSailCARDSIZE.AsString;
          prAddCashS.ParamByName('QUANTITY').AsFloat:=fbCashSailQUANTITY.AsFloat;
          prAddCashS.ParamByName('PRICERUB').AsFloat:=fbCashSailPRICERUB.AsFloat;
          prAddCashS.ParamByName('DPROC').AsFloat:=fbCashSailDPROC.AsFloat;
          prAddCashS.ParamByName('DSUM').AsFloat:=fbCashSailDSUM.AsFloat;
          prAddCashS.ParamByName('TOTALRUB').AsFloat:=fbCashSailTOTALRUB.AsFloat;
          prAddCashS.ParamByName('CASHER').AsInteger:=fbCashSailCASHER.AsInteger;
          prAddCashS.ParamByName('DEPART').AsInteger:=fbCashSailDEPART.AsInteger;
          prAddCashS.ParamByName('OPERATION').AsInteger:=fbCashSailOPERATION.AsInteger;
          prAddCashS.ParamByName('SECPOS').AsInteger:=fbCashSailSECPOS.AsInteger;
          prAddCashS.ParamByName('ECHECK').AsInteger:=fbCashSailECHECK.AsInteger;
          prAddCashS.ExecProc;

          fbCashSail.Next;

          iZ:=fbCashSailZNUMBER.AsInteger;
        end;

        WrMess('');
//        WrMess('    ����� � �����:');
        WrMess('     �������  ���. - '+FloatToStr(rv(SumRealN)));
        WrMess('     �������� ���. - '+FloatToStr(rv(SumRetN)));
        WrMess('     �������  �/�. - '+FloatToStr(rv(SumRealB)));
        WrMess('     �������� �/�. - '+FloatToStr(rv(SumRetB)));
//      WrMess('');
//        WrMess('     �������� ����� ..');

        quZListDet2.Active:=False;
        quZListDet2.ParamByName('CNUM').AsInteger:=CashNum;
        quZListDet2.ParamByName('ZNUM').AsInteger:=iZ;
        quZListDet2.Active:=True;
        quZListDet2.First;
        while not quZListDet2.Eof do quZListDet2.Delete;

        quZListDet.Active:=False;
        quZListDet.ParamByName('CNUM').AsInteger:=CashNum;
        quZListDet.ParamByName('ZNUM').AsInteger:=iZ;
        quZListDet.Active:=True;
        quZListDet.First;
        while not quZListDet.Eof do
        begin
          quZListDet2.Append;
          quZListDet2CASHNUM.AsInteger:=CashNum;
          quZListDet2ZNUM.AsInteger:=iZ;
          quZListDet2INOUT.AsInteger:=quZListDetINOUT.AsInteger; //�������
          quZListDet2ITYPE.AsInteger:=quZListDetITYPE.AsInteger; //��� ���� ��������
          quZListDet2RSUM.AsFloat:=quZListDetRSUM.AsFloat;
          quZListDet2IDATE.AsInteger:=quZListDetIDATE.AsInteger;
          quZListDet2DDATE.AsDateTime:=quZListDetDDATE.AsDateTime;
          quZListDet2.Post;

          quZListDet.Next;
        end;
//        WrMess('     ����� ��.');

        quZListDet.Active:=False;
        quZListDet2.Active:=False;

        fbCashSail.Active:=False;
        quCashPay.Active:=False;
      end;
    finally
      cxButton1.Enabled:=True;
      cxButton2.Enabled:=True;
      cxButton4.Enabled:=True;
    end;
  end else
  begin
    WrMess('���� �������');
  end;
end;

procedure TfmMainCashOut.cxButton1Click(Sender: TObject);
begin
  try
    if CasherDb.Connected=True then
    begin
      WrMess('�� ����� ��� ������.');
    end else
    begin
      WrMess('��������� �� ����� ...');
      CasherDb.DBName:=cxTextEdit1.Text;
      CasherDb.Open;
      if CasherDb.Connected=True then
      begin
        WrMess('�� ����� ��.');
      end;
    end;
  except
    WrMess('������� �������� �� ����� ...');
  end;

  try
    if Cash.Connected=True then
    begin
      WrMess('�� ������ ��� ������.');
    end else
    begin
      WrMess('��������� �� ������ ...');
      Cash.DBName:=cxTextEdit2.Text;
      Cash.Open;
      if Cash.Connected=True then
      begin
        WrMess('�� ������ ��.');
      end;
    end;
  except
    WrMess('������� �������� �� ������ ...');
  end;

//  cxButton4.Enabled:=True;
end;

procedure TfmMainCashOut.cxButton4Click(Sender: TObject);
begin
  try
    WrMess('��������� ����.');
    CasherDb.Close;
    Cash.Close;
    WrMess('���� �������.');
  except
    WrMess('������ �������� ����.');
  end;
end;

procedure TfmMainCashOut.cxButton3Click(Sender: TObject);
begin
  bStop:=True;
end;

end.
