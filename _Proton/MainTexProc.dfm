object fmMainTexProc: TfmMainTexProc
  Left = 424
  Top = 113
  Width = 468
  Height = 631
  Caption = #1058#1077#1093#1085#1080#1095#1077#1089#1082#1080#1077' '#1087#1088#1086#1094#1077#1076#1091#1088#1099
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit1: TcxDateEdit
    Left = 20
    Top = 16
    TabOrder = 0
    Width = 145
  end
  object cxButton1: TcxButton
    Left = 304
    Top = 16
    Width = 97
    Height = 25
    Action = acStartTexProc
    Caption = #1055#1091#1089#1082
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 296
    Top = 48
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    State = cbsChecked
    TabOrder = 2
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 4
    Top = 76
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 497
    Width = 453
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 578
    Width = 460
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 28
    Top = 108
  end
  object amCalcSS: TActionManager
    Left = 64
    Top = 200
    StyleName = 'XP Style'
    object acStartTexProc: TAction
      Caption = 'acStartTexProc'
      OnExecute = acStartTexProcExecute
    end
  end
end
