unit uGraf3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ExtCtrls, SpeedBar, ComCtrls, TeeProcs, TeEngine,
  Chart, Series, ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmGraf3 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Panel1: TPanel;
    Chart1: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    amGraf1: TActionManager;
    acPrintGraf: TAction;
    SpeedItem2: TSpeedItem;
    acGr1: TAction;
    acGr2: TAction;
    acGr3: TAction;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    FormPlacement1: TFormPlacement;
    Series3: TLineSeries;
    Series4: TLineSeries;
    Series5: TLineSeries;
    Series6: TLineSeries;
    Series7: TLineSeries;
    Series8: TLineSeries;
    Series9: TLineSeries;
    Series10: TLineSeries;
    Series11: TLineSeries;
    Series12: TLineSeries;
    Series13: TLineSeries;
    Series14: TLineSeries;
    Series15: TLineSeries;
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPrintGrafExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prCalcGraf(Chart:TChart);
  end;

var
  fmGraf3: TfmGraf3;

implementation

uses Un1, MDB;

{$R *.dfm}

Procedure TfmGraf3.prCalcGraf(Chart:TChart);
{Var k,kMax,kMin:INteger;
    Arr:Array of Real;
    iMb,iMe,i,iQ,iM:Integer;
    bStar:Boolean;
    StrWk:String;
    iVal:Double;}
begin
// ������� ������ ��� ��������

{  bStar:=CanDo('prStarBp');
  with dmMFK do
  begin
    if iCurGraf=1 then
    begin //������ ������

      Series2.Clear;

      k:=0; i:=iMb; iQ:=0;
      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(k);
          iQ:=MonthToQuart(i); //��������� �� �������;

          iVal:=Round(Arr[k]) div 1000 ;
          Series2.AddXY(k,iVal,QuartToStr(iQ));
        end;
        i:=IncrM(i);
      end;

      //������ ������

      Arr:=nil;

      SetLength(Arr,kMax+1); //������ ��������, ������������ ������ k - ���������;

      //������ ������

      Series1.Clear;

      k:=0; i:=iMb; iQ:=0;
      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(k);
          iQ:=MonthToQuart(i); //��������� �� �������;

          iVal:=Round(Arr[k]) div 1000 ;
          Series1.AddXY(k,iVal,QuartToStr(iQ));
        end;
        i:=IncrM(i);
      end;

      Chart.Repaint;
      Arr:=nil;
    end;

    if iCurGraf=2 then
    begin //������ ������ ���� 2 //�������� �������� - ��� ����

      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select MinDate=Min(OperDate),MaxDate=Max(OperDate) from BPOper');
      quF.SQL.Add('where Id_Project='+IntToStr(PosPr.Id));
      quF.Active:=True;
      iMb:=DateToMonth(quF.FieldByName('MinDate').AsDateTime);
      iMe:=DateToMonth(quF.FieldByName('MaxDate').AsDateTime);

      kMax:=0; i:=iMb; iQ:=0; kMin:=0;
      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(kMax);
          iQ:=MonthToQuart(i);
          if kMin=0 then kMin:=iQ; //��������� ����������� ������� ��� ������������� ���������
        end;
        i:=IncrM(i);
      end;

      SetLength(Arr,kMax+1); //������ ��������, ������������ ������ k - ���������;

      //������ ������

      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select sMonth=Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2),');
      if bStar=True then quF.SQL.Add('fSum=Sum(Su1+Su2)')
      else quF.SQL.Add('fSum=Sum(Su1)');
      quF.SQL.Add('from BPOper');
      quF.SQL.Add('left Join PlanSch ps on ps.Id=AccKt');
      quF.SQL.Add('where Id_project='+IntToStr(PosPr.Id));
      quF.SQL.Add('and ps.StrNum like ''4321%''');
      quF.SQL.Add('Group by Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2)');
      quF.Active:=True;

      quF.First;  //����� �������� � ������
      while not quF.Eof do
      begin
        StrWk:=quF.FieldByName('sMonth').AsString;
        While pos(' ',strwk)>0 do delete(StrWk,pos(' ',strwk),1);
        iM:=StrToINtDef(StrWk,0);

        k:=0; i:=iMb; iQ:=0;
        while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
        begin
          if iQ<MonthToQuart(i) then
          begin
            inc(k);
            iQ:=MonthToQuart(i); //��������� �� �������;
          end;
          if iQ=MonthToQuart(iM) then
          begin
             Arr[k]:=Arr[k]+quF.FieldByName('fSum').AsFloat;
             Break;
          end;
          i:=IncrM(i);
        end;
        quF.Next;
      end;

      delay(10);
     //������ ����� (��� ��-��)

      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select sMonth=Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2),');
      if bStar=True then quF.SQL.Add('fSum=Sum(Su1+Su2)')
      else quF.SQL.Add('fSum=Sum(Su1)');
      quF.SQL.Add('from BPOper');
      quF.SQL.Add('left Join PlanSch ps on ps.Id=AccDt');
      quF.SQL.Add('where Id_project='+IntToStr(PosPr.Id));
      quF.SQL.Add('and ps.StrNum like ''4321%''');
      quF.SQL.Add('Group by Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2)');
      quF.Active:=True;

      quF.First;  //����� �������� � ������
      while not quF.Eof do
      begin
        StrWk:=quF.FieldByName('sMonth').AsString;
        While pos(' ',strwk)>0 do delete(StrWk,pos(' ',strwk),1);
        iM:=StrToINtDef(StrWk,0);

        k:=0; i:=iMb; iQ:=0;
        while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
        begin
          if iQ<MonthToQuart(i) then
          begin
            inc(k);
            iQ:=MonthToQuart(i); //��������� �� �������;
          end;
          if iQ=MonthToQuart(iM) then
          begin
            Arr[k]:=Arr[k]-quF.FieldByName('fSum').AsFloat;
            Break;
          end;
          i:=IncrM(i);
        end;
        quF.Next;
      end;



      Series3.Clear;

      k:=0; i:=iMb; iQ:=0; iVal:=0;

      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(k);
          iQ:=MonthToQuart(i); //��������� �� �������;

          iVal:=iVal+(Round(Arr[k]) div 1000)+k/10000;     //      ����������� ����
          Series3.AddXY(k,iVal,QuartToStr(iQ));
        end;
        i:=IncrM(i);
      end;
      Arr:=nil;

      // ������ ������ ���� 2  //������ �������
      Series4.Clear;
      k:=1; iVal:=0;
      with fmBipl do
      begin
        if taGr.Locate('Id',31,[]) then //320000
        begin
          for i:=0 to taGr.FieldDefs.Count-1 do
          begin //�������� ������ �������� �.�. ������������� �� ������ ���������
            if Pos('k2',taGr.FieldDefs.Items[i].Name)>0 then //��������� �������
            begin
              IQ:=StrToIntDef(Copy(taGr.FieldDefs.Items[i].Name,2,5),0);
              if iQ >= kMin then
              begin
                iVal:=iVal+Round(taGr.FieldByName(taGr.FieldDefs.Items[i].Name).AsFloat/1000);
                if iQ>0 then Series4.AddXY(k,iVal,QuartToStr(iQ));
              end;
              inc(k);
            end;
          end;
        end;
      end;//}

  {    Chart.Repaint;
    end;
    if iCurGraf=3 then
    begin //������ ������
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select MinDate=Min(OperDate),MaxDate=Max(OperDate) from BPOper');
      quF.SQL.Add('where Id_Project='+IntToStr(PosPr.Id));
      quF.Active:=True;
      iMb:=DateToMonth(quF.FieldByName('MinDate').AsDateTime);
      iMe:=DateToMonth(quF.FieldByName('MaxDate').AsDateTime);

      kMax:=0; i:=iMb; iQ:=0; kMin:=0;
      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(kMax);
          iQ:=MonthToQuart(i);
          if kMin=0 then kMin:=iQ; //��������� ����������� ������� ��� ������������� ���������
        end;
        i:=IncrM(i);
      end;

      SetLength(Arr,kMax+1); //������ ��������, ������������ ������ k - ���������;


      // ������ ������ ���� 3  //������ �������
      Series5.Clear;
      k:=1; iVal:=0;
      with fmBipl do
      begin
        if taGr.Locate('Id',51,[]) then //331100
        begin
          for i:=0 to taGr.FieldDefs.Count-1 do
          begin //�������� ������ �������� �.�. ������������� �� ������ ���������
            if Pos('k2',taGr.FieldDefs.Items[i].Name)>0 then //��������� �������
            begin
              IQ:=StrToIntDef(Copy(taGr.FieldDefs.Items[i].Name,2,5),0);
              if iQ >= kMin then
              begin
                iVal:=iVal+Round(taGr.FieldByName(taGr.FieldDefs.Items[i].Name).AsFloat/1000);
                if iQ>0 then Series5.AddXY(k,iVal,QuartToStr(iQ));
              end;
              inc(k);
            end;
          end;
        end;
      end;//}

      //������ ������ ���� 3


   {   quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select sMonth=Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2),');
      if bStar=True then quF.SQL.Add('fSum=Sum(Su1+Su2)')
      else quF.SQL.Add('fSum=Sum(Su1)');
      quF.SQL.Add('from BPOper');
      quF.SQL.Add('where Id_project='+IntToStr(PosPr.Id));
      quF.SQL.Add('and (AccDt=84 or AccDt=88)');
      quF.SQL.Add('Group by Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2)');
      quF.Active:=True;

      quF.First;  //����� �������� � ������
      while not quF.Eof do
      begin
        StrWk:=quF.FieldByName('sMonth').AsString;
        While pos(' ',strwk)>0 do delete(StrWk,pos(' ',strwk),1);
        iM:=StrToINtDef(StrWk,0);

        k:=0; i:=iMb; iQ:=0;
        while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
        begin
          if iQ<MonthToQuart(i) then
          begin
            inc(k);
            iQ:=MonthToQuart(i); //��������� �� �������;
          end;
          if iQ=MonthToQuart(iM) then
          begin
             Arr[k]:=Arr[k]+quF.FieldByName('fSum').AsFloat;
             Break;
          end;
          i:=IncrM(i);
        end;
        quF.Next;
      end;

      delay(10);


      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('Select sMonth=Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2),');
      if bStar=True then quF.SQL.Add('fSum=Sum(Su1+Su2)')
      else quF.SQL.Add('fSum=Sum(Su1)');
      quF.SQL.Add('from BPOper');
      quF.SQL.Add('where Id_project='+IntToStr(PosPr.Id));
      quF.SQL.Add('and (AccKt=84 or AccKt=88)');
      quF.SQL.Add('Group by Substring(Convert(Char(40),OperDate,102),1,4)+Substring(Convert(Char(40),OperDate,102),6,2)');
      quF.Active:=True;

      quF.First;  //����� �������� � ������
      while not quF.Eof do
      begin
        StrWk:=quF.FieldByName('sMonth').AsString;
        While pos(' ',strwk)>0 do delete(StrWk,pos(' ',strwk),1);
        iM:=StrToINtDef(StrWk,0);

        k:=0; i:=iMb; iQ:=0;
        while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
        begin
          if iQ<MonthToQuart(i) then
          begin
            inc(k);
            iQ:=MonthToQuart(i); //��������� �� �������;
          end;
          if iQ=MonthToQuart(iM) then
          begin
            Arr[k]:=Arr[k]-quF.FieldByName('fSum').AsFloat;
            Break;
          end;
          i:=IncrM(i);
        end;
        quF.Next;
      end;

      Series6.Clear;

      k:=0; i:=iMb; iQ:=0; iVal:=0;

      while i<=iMe do  //���� �� �������, �������� � ��������, �������� ������ � ���������
      begin
        if iQ<MonthToQuart(i) then
        begin
          inc(k);
          iQ:=MonthToQuart(i); //��������� �� �������;

          iVal:=iVal+(Round(Arr[k]) div 1000)+k/10000;     //      ����������� ����
          Series6.AddXY(k,iVal,QuartToStr(iQ));
        end;
        i:=IncrM(i);
      end;
      Arr:=nil;

      Chart.Repaint;
    end;
  end;}
end;



procedure TfmGraf3.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGraf3.acPrintGrafExecute(Sender: TObject);
//Var rRect:TRect;
begin
//������
 { rRect.Left:=5;
  rRect.Right:=
  0,Printer.PageWidth-1,Printer.PageHeight-1}

  Chart1.PrintLandscape;
end;

procedure TfmGraf3.FormCreate(Sender: TObject);
begin
  Chart1.Visible:=True;
  Chart1.Align:=alClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmGraf3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmMC.quRepSumCli.Active:=False;
end;

end.
