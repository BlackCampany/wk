unit CorrTO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalc, StdCtrls, Menus,
  cxLookAndFeelPainters, cxButtons;

type
  TfmCorrTO = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    cxCalcEdit5: TcxCalcEdit;
    cxCalcEdit6: TcxCalcEdit;
    cxCalcEdit7: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxCalcEdit6PropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCorrTO: TfmCorrTO;

implementation

uses Un1;

{$R *.dfm}

procedure TfmCorrTO.cxCalcEdit6PropertiesEditValueChanged(Sender: TObject);
begin
  cxCalcEdit5.Value:=rv(cxCalcEdit1.Value+cxCalcEdit2.Value-cxCalcEdit3.Value-cxCalcEdit4.Value);
  cxCalcEdit7.Value:=rv(cxCalcEdit6.Value-cxCalcEdit5.Value);
end;

end.
