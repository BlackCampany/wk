object fmMainLoader: TfmMainLoader
  Left = 303
  Top = 116
  Width = 582
  Height = 422
  Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1080' '#1074#1099#1075#1088#1091#1079#1082#1072' '#1090#1077#1088#1084#1080#1085#1072#1083#1086#1074'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 0
    Top = 193
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
    Height = 196
    Width = 574
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 185
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object cxPageControl1: TcxPageControl
      Left = 0
      Top = 5
      Width = 433
      Height = 176
      ActivePage = Page1
      Color = 16766378
      LookAndFeel.Kind = lfOffice11
      ParentColor = False
      TabOrder = 0
      ClientRectBottom = 176
      ClientRectRight = 433
      ClientRectTop = 24
      object Page1: TcxTabSheet
        Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1090#1086#1074#1072#1088#1086#1074
        ImageIndex = 1
        object Label1: TLabel
          Left = 52
          Top = 116
          Width = 58
          Height = 13
          Caption = #1047#1072#1075#1088#1091#1078#1077#1085#1086' '
          Transparent = True
        end
        object Label2: TLabel
          Left = 128
          Top = 116
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object Label3: TLabel
          Left = 16
          Top = 116
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object Label7: TLabel
          Left = 176
          Top = 92
          Width = 19
          Height = 13
          Caption = #1058#1080#1087
          Transparent = True
        end
        object cxLabel1: TcxLabel
          Left = 12
          Top = 16
          Caption = #1060#1072#1081#1083' '#1090#1086#1074#1072#1088#1086#1074
          Transparent = True
        end
        object cxButtonEdit1: TcxButtonEdit
          Left = 96
          Top = 16
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Text = 'cxButtonEdit1'
          Width = 309
        end
        object cxButton2: TcxButton
          Left = 296
          Top = 116
          Width = 125
          Height = 25
          Caption = #1043#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
          TabOrder = 2
          OnClick = cxButton2Click
          LookAndFeel.Kind = lfOffice11
        end
        object cxLabel2: TcxLabel
          Left = 12
          Top = 48
          Caption = #1055#1086#1088#1090
          Transparent = True
        end
        object cxComboBox1: TcxComboBox
          Left = 96
          Top = 44
          Properties.Items.Strings = (
            'COM1'
            'COM2'
            'COM3'
            'COM4'
            'COM5'
            'COM6'
            'COM7'
            'COM8'
            'COM9'
            'COM10'
            'COM11'
            'COM12'
            'COM13'
            'COM14'
            'COM15'
            'COM16')
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          Text = 'COM1'
          Width = 65
        end
        object cxLabel3: TcxLabel
          Left = 172
          Top = 48
          Caption = '(19200,8,1,n)'
          Transparent = True
        end
        object cxLabel4: TcxLabel
          Left = 12
          Top = 76
          Caption = #1047#1072#1076#1077#1088#1078#1082#1072' '#1084#1089
          Transparent = True
        end
        object cxSpinEdit1: TcxSpinEdit
          Left = 96
          Top = 72
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 7
          Value = 20
          Width = 65
        end
        object cxRButton1: TcxRadioButton
          Left = 280
          Top = 48
          Width = 113
          Height = 17
          Caption = #1058#1086#1074#1072#1088#1086#1074
          Checked = True
          TabOrder = 8
          TabStop = True
          LookAndFeel.Kind = lfOffice11
          Transparent = True
        end
        object cxRButton2: TcxRadioButton
          Left = 280
          Top = 68
          Width = 113
          Height = 17
          Caption = #1055#1088#1077#1092#1080#1082#1089#1086#1074
          TabOrder = 9
          LookAndFeel.Kind = lfOffice11
          Transparent = True
        end
        object cxButton5: TcxButton
          Left = 296
          Top = 88
          Width = 125
          Height = 25
          Caption = #1055#1086#1076#1075#1086#1090#1086#1074#1080#1090#1100' '#1092#1072#1081#1083
          TabOrder = 10
          OnClick = cxButton5Click
          LookAndFeel.Kind = lfOffice11
        end
        object cxComboBox3: TcxComboBox
          Left = 176
          Top = 112
          Properties.Items.Strings = (
            'Cipher'
            'M3')
          Properties.OnChange = cxComboBox3PropertiesChange
          Style.BorderStyle = ebsOffice11
          TabOrder = 11
          Text = 'Cipher'
          Width = 93
        end
      end
      object Page2: TcxTabSheet
        Caption = #1055#1088#1080#1077#1084#1082#1072' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080' '#1074' '#1092#1072#1081#1083
        ImageIndex = 1
        object Label6: TLabel
          Left = 16
          Top = 116
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object Label4: TLabel
          Left = 52
          Top = 116
          Width = 43
          Height = 13
          Caption = #1055#1088#1080#1085#1103#1090#1086
          Transparent = True
        end
        object Label5: TLabel
          Left = 128
          Top = 116
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object cxLabel5: TcxLabel
          Left = 12
          Top = 16
          Caption = #1042#1099#1075#1088#1091#1078#1072#1090#1100' '#1074' '#1092#1072#1081#1083
          Transparent = True
        end
        object cxLabel6: TcxLabel
          Left = 12
          Top = 48
          Caption = #1055#1086#1088#1090
          Transparent = True
        end
        object cxButtonEdit2: TcxButtonEdit
          Left = 120
          Top = 16
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Text = 'cxButtonEdit2'
          Width = 293
        end
        object cxComboBox2: TcxComboBox
          Left = 96
          Top = 44
          Properties.Items.Strings = (
            'COM1'
            'COM2'
            'COM3'
            'COM4'
            'COM5'
            'COM6'
            'COM7'
            'COM8'
            'COM9'
            'COM10'
            'COM11'
            'COM12'
            'COM13'
            'COM14'
            'COM15'
            'COM16')
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          Text = 'COM1'
          Width = 65
        end
        object cxLabel8: TcxLabel
          Left = 12
          Top = 76
          Caption = #1047#1072#1076#1077#1088#1078#1082#1072' '#1084#1089
          Transparent = True
        end
        object cxSpinEdit2: TcxSpinEdit
          Left = 96
          Top = 72
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          Value = 150
          Width = 65
        end
        object cxLabel7: TcxLabel
          Left = 172
          Top = 48
          Caption = '(19200,8,1,n)'
          Transparent = True
        end
        object cxButton3: TcxButton
          Left = 272
          Top = 84
          Width = 137
          Height = 25
          Caption = #1055#1088#1080#1085#1103#1090#1100' Cipher'
          TabOrder = 7
          OnClick = cxButton3Click
          LookAndFeel.Kind = lfOffice11
        end
        object cxButton4: TcxButton
          Left = 272
          Top = 52
          Width = 137
          Height = 25
          Caption = #1059#1076#1072#1083#1080#1090#1100' '#1092#1072#1081#1083
          TabOrder = 8
          OnClick = cxButton4Click
          LookAndFeel.Kind = lfOffice11
        end
        object cxButton6: TcxButton
          Left = 272
          Top = 112
          Width = 137
          Height = 25
          Caption = #1055#1088#1080#1085#1103#1090#1100' M3'
          TabOrder = 9
          OnClick = cxButton6Click
          LookAndFeel.Kind = lfOffice11
        end
      end
    end
    object Panel1: TPanel
      Left = 437
      Top = 0
      Width = 137
      Height = 185
      Align = alRight
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 1
      object cxButton1: TcxButton
        Left = 20
        Top = 16
        Width = 97
        Height = 25
        Caption = #1042#1099#1093#1086#1076
        TabOrder = 0
        OnClick = cxButton1Click
        LookAndFeel.Kind = lfOffice11
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 480
    Top = 88
  end
  object DevPrint: TVaComm
    Baudrate = br19200
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = dtrEnabled
    FlowControl.ControlRts = rtsEnabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    DeviceName = 'COM1'
    MonitorEvents = [ceCts, ceDsr, ceError, ceRing, ceRxChar, ceTxEmpty]
    Left = 406
    Top = 204
  end
  object taArt: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 480
    Top = 204
    object taArtCode: TIntegerField
      FieldName = 'Code'
    end
  end
  object quCTerm: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      'select'
      'bar."GoodsId",'
      'bar."ID",'
      'gd."Name",'
      'gd."Cena",'
      'gd."EdIzm",'
      'gd."Barcode"'
      'from "BarCode" bar'
      'left join "Goods" gd on gd."ID"=bar."GoodsId"'
      'where gd."Status"<100'
      'Order by bar."GoodsId"')
    Params = <>
    LoadBlobOnOpen = False
    Left = 24
    Top = 216
    object quCTermGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object quCTermID: TStringField
      FieldName = 'ID'
      Size = 13
    end
    object quCTermName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCTermCena: TFloatField
      FieldName = 'Cena'
    end
    object quCTermEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quCTermBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
  end
end
