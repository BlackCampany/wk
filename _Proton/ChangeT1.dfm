object fmChangeT1: TfmChangeT1
  Left = 434
  Top = 508
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100
  ClientHeight = 178
  ClientWidth = 342
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 301
    Height = 37
    Alignment = taCenter
    AutoSize = False
    Caption = 'Label1'
    Transparent = True
    Layout = tlCenter
    WordWrap = True
  end
  object Label2: TLabel
    Left = 16
    Top = 76
    Width = 140
    Height = 13
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100' ( '#1055#1086#1083#1091#1095#1072#1090#1077#1083#1100' )'
  end
  object Panel1: TPanel
    Left = 0
    Top = 116
    Width = 342
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = 16760962
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 48
      Top = 16
      Width = 101
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 204
      Top = 16
      Width = 99
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxComboBox1: TcxComboBox
    Left = 177
    Top = 72
    Properties.Items.Strings = (
      #1052#1072#1075#1072#1079#1080#1085
      #1048#1055' '#1054#1043#1051#1054#1041#1051#1048#1053' '#1040'.'#1040'.')
    Style.BorderStyle = ebsOffice11
    TabOrder = 1
    Text = #1052#1072#1075#1072#1079#1080#1085
    Width = 141
  end
end
