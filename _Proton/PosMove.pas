unit PosMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, ExtCtrls, cxButtons,
  cxRadioGroup, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridCardView, cxGridDBCardView, Menus, cxDataStorage, cxGroupBox,
  cxImageComboBox;

type
  TfmPosMove = class(TForm)
    cxButton3: TcxButton;
    Bevel2: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Bevel1: TBevel;
    Label3: TLabel;
    CalcEdit1: TcxCalcEdit;
    cxRadioGroup1: TcxRadioGroup;
    cxButton1: TcxButton;
    Panel1: TPanel;
    TextEdit1: TcxTextEdit;
    Label4: TLabel;
    Panel2: TPanel;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    ViewMove: TcxGridDBCardView;
    ViewMoveNUMTABLE: TcxGridDBCardViewRow;
    ViewMoveTABSUM: TcxGridDBCardViewRow;
    cxImageComboBox1: TcxImageComboBox;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    procedure cxRadioGroup1PropertiesChange(Sender: TObject);
    procedure TextEdit1Enter(Sender: TObject);
    procedure CalcEdit1Enter(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxRadioButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPosMove: TfmPosMove;

implementation

uses Calc, Dm, Un1, Spec;

{$R *.dfm}

procedure TfmPosMove.cxRadioGroup1PropertiesChange(Sender: TObject);
begin
  if cxRadioGroup1.ItemIndex=0 then
  begin
    Panel1.Visible:=True;
    Panel2.Visible:=False;
  end;
  if cxRadioGroup1.ItemIndex=1 then
  begin
    Panel1.Visible:=False;
    Panel2.Visible:=True;
    with dmC do
    begin
      ViewMove.BeginUpdate;
      quTabspers.Active:=False;
      quTabsPers.SelectSQL.Clear;
      quTabsPers.SelectSQL.Add('SELECT ID, ID_PERSONAL, NUMTABLE, QUESTS, TABSUM, DISCONT, BEGTIME, ISTATUS, NUMZ');
      quTabsPers.SelectSQL.Add('FROM TABLES');
      quTabsPers.SelectSQL.Add('Where Id_personal='+IntToStr(Tab.Id_Personal));
      quTabsPers.SelectSQL.Add('and ISTATUS=0'); //������ ������c��� � ������ �� ��� ��������� ���� - �������
      quTabsPers.SelectSQL.Add('and ID<>'+IntToStr(Tab.Id)); //������ ������c��� � ����
      quTabsPers.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMZ');
      quTabspers.Active:=True;
      ViewMove.EndUpdate;
    end;
  end;
end;

procedure TfmPosMove.TextEdit1Enter(Sender: TObject);
Var bTab:Boolean;
    iCount:Integer;
    sNum:String;
begin
  fmCalc.Caption:='����� ������ �����.';
  fmCalc.CalcEdit1.Text:=TextEdit1.Text;
  bTab:=False;
  while not bTab do
  begin
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      sNum:=Copy(fmCalc.CalcEdit1.Text,1,8);
      with dmC do
      begin
        quTabExists.Active:=False;
        quTabExists.ParamByName('Id_Personal').AsInteger:=Tab.Id_Personal;
        quTabExists.ParamByName('NumTab').AsString:=sNum;
        quTabExists.Active:=True;
        iCount:=quTabExistsCOUNT.AsInteger;
        if iCount>0 then showmessage('������ ����� ��� ���������. �������� ������.')
        else bTab:=true;
      end;
      TextEdit1.Text:=sNum;
    end;
  end;
  fmCalc.Caption:='';
end;

procedure TfmPosMove.CalcEdit1Enter(Sender: TObject);
Var rQ:real;
begin
  fmCalc.Caption:='����������';
  rQ:=dmC.quCurSpecQUANTITY.AsFloat;
  fmCalc.CalcEdit1.EditValue:=rQ;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then
  begin
    if (fmCalc.CalcEdit1.EditValue<=rQ) and (fmCalc.CalcEdit1.EditValue>0)
    then CalcEdit1.EditValue:=fmCalc.CalcEdit1.EditValue;
  end;
  fmCalc.Caption:='';
end;

procedure TfmPosMove.cxRadioButton1Click(Sender: TObject);
begin
  if cxRadioButton1.Checked then
  begin
    Label3.Visible:=True;
    CalcEdit1.Visible:=True;
    cxImageComboBox1.Visible:=False;
  end else
  begin
    Label3.Visible:=False;
    CalcEdit1.Visible:=False;
    cxImageComboBox1.Visible:=True;
  end;
end;

procedure TfmPosMove.FormShow(Sender: TObject);
begin
  cxRadioButton1.Checked:=True;
  Label3.Visible:=True;
  CalcEdit1.Visible:=True;
  cxImageComboBox1.Visible:=False;
end;

procedure TfmPosMove.cxRadioButton2Click(Sender: TObject);
begin
  if cxRadioButton1.Checked then
  begin
    Label3.Visible:=True;
    CalcEdit1.Visible:=True;
    cxImageComboBox1.Visible:=False;
  end else
  begin
    Label3.Visible:=False;
    CalcEdit1.Visible:=False;
    cxImageComboBox1.Visible:=True;
  end;
end;

end.
