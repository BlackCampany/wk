object fmZakHPost: TfmZakHPost
  Left = 286
  Top = 625
  BorderStyle = bsDialog
  Caption = #1047#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  ClientHeight = 254
  ClientWidth = 804
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 200
    Width = 804
    Height = 54
    Align = alBottom
    BevelInner = bvLowered
    Color = 16744576
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 64
      Top = 12
      Width = 105
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      Colors.Default = 16753828
      Colors.Normal = clWhite
      Colors.Hot = 16758783
      Colors.Pressed = clBlue
      Colors.Disabled = clSilver
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C10420000000000000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7F10421042FF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7F000210421863FF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7F00020002000210421863FF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7F00020002FF7F000200021042FF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000210421042FF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7F000210421863FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7F00021042FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0002FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C10421042104210421042104210421042104210421042
        10421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton10: TcxButton
      Left = 212
      Top = 12
      Width = 105
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Colors.Default = 16753828
      Colors.Normal = clWhite
      Colors.Hot = 16758783
      Colors.Pressed = clBlue
      Colors.Disabled = clSilver
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridZHPost: TcxGrid
    Left = 20
    Top = 16
    Width = 761
    Height = 173
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewZHPost: TcxGridDBTableView
      OnDblClick = ViewZHPostDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquZakHPost
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewZHPostName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'Name'
        Width = 119
      end
      object ViewZHPostDOCNUM: TcxGridDBColumn
        Caption = #8470' '#1079#1072#1103#1074#1082#1080
        DataBinding.FieldName = 'DOCNUM'
        Width = 68
      end
      object ViewZHPostSNUMNACL: TcxGridDBColumn
        Caption = #8470' '#1053#1040#1050#1051
        DataBinding.FieldName = 'SNUMNACL'
        Width = 77
      end
      object ViewZHPostSNUMSCHF: TcxGridDBColumn
        Caption = #8470' '#1057#1063#1060#1050
        DataBinding.FieldName = 'SNUMSCHF'
        Width = 77
      end
      object ViewZHPostDEPART: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'DEPART'
        Visible = False
      end
      object ViewZHPostIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmMC.ImageList1
        Properties.Items = <
          item
            Description = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1086#1076#1086#1073#1088#1077#1085#1072
            ImageIndex = 5
            Value = 9
          end
          item
            Description = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1072#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1072
            ImageIndex = 7
            Value = 10
          end
          item
            Description = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1086#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085#1072
            ImageIndex = 20
            Value = 11
          end>
      end
      object ViewZHPostCLIQUANTN: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1053#1040#1050#1051
        DataBinding.FieldName = 'CLIQUANTN'
      end
      object ViewZHPostCLISUMIN0N: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1073#1077#1079' '#1053#1044#1057' '#1087#1086' '#1053#1040#1050#1051
        DataBinding.FieldName = 'CLISUMIN0N'
      end
      object ViewZHPostCLISUMINN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089' '#1053#1044#1057' '#1087#1086' '#1053#1040#1050#1051
        DataBinding.FieldName = 'CLISUMINN'
      end
      object ViewZHPostIDATENACL: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1053#1040#1050#1051
        DataBinding.FieldName = 'IDATENACL'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewZHPostIDATESCHF: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1057#1063#1060#1050
        DataBinding.FieldName = 'IDATESCHF'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewZHPostID: TcxGridDBColumn
        Caption = #1042#1085'.'#1082#1086#1076
        DataBinding.FieldName = 'ID'
        Visible = False
      end
    end
    object LevelZHPost: TcxGridLevel
      GridView = ViewZHPost
    end
  end
end
