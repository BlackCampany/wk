unit FindKassir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxImageComboBox;

type
  TfmFindKassir = class(TForm)
    Panel1: TPanel;
    ViewFKassir: TcxGridDBTableView;
    LevelFKassir: TcxGridLevel;
    GridFKassir: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFKassirID: TcxGridDBColumn;
    ViewFKassirNAMECASSIR: TcxGridDBColumn;
    ViewFKassirSDOLG: TcxGridDBColumn;
    ViewFKassirIKASSIR: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ViewFKassirDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindKassir: TfmFindKassir;

implementation

uses Kadr;

{$R *.dfm}

procedure TfmFindKassir.FormCreate(Sender: TObject);
begin
  GridFKassir.Align:=AlClient;
end;

procedure TfmFindKassir.FormShow(Sender: TObject);
begin
  fmFindKassir.GridFKassir.SetFocus;
  fmFindKassir.ViewFKassir.Controller.FocusRecord(fmFindKassir.ViewFKassir.DataController.FocusedRowIndex,True);
end;

procedure TfmFindKassir.ViewFKassirDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
