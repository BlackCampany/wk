unit DocInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCalendar, cxContainer, cxTextEdit, cxMemo, Menus;

type
  TfmDocsInv = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsInv: TcxGrid;
    ViewDocsInv: TcxGridDBTableView;
    LevelDocsInv: TcxGridLevel;
    amDocsInv: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsInv: TcxGridLevel;
    ViewCardsInv: TcxGridDBTableView;
    ViewCardsInvNAME: TcxGridDBColumn;
    ViewCardsInvNAMESHORT: TcxGridDBColumn;
    ViewCardsInvIDCARD: TcxGridDBColumn;
    ViewCardsInvQUANT: TcxGridDBColumn;
    ViewCardsInvPRICEIN: TcxGridDBColumn;
    ViewCardsInvSUMIN: TcxGridDBColumn;
    ViewCardsInvPRICEUCH: TcxGridDBColumn;
    ViewCardsInvSUMUCH: TcxGridDBColumn;
    ViewCardsInvIDNDS: TcxGridDBColumn;
    ViewCardsInvSUMNDS: TcxGridDBColumn;
    ViewCardsInvDATEDOC: TcxGridDBColumn;
    ViewCardsInvNUMDOC: TcxGridDBColumn;
    ViewCardsInvNAMECL: TcxGridDBColumn;
    ViewCardsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvID: TcxGridDBColumn;
    ViewDocsInvDATEDOC: TcxGridDBColumn;
    ViewDocsInvNUMDOC: TcxGridDBColumn;
    ViewDocsInvIDSKL: TcxGridDBColumn;
    ViewDocsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvIACTIVE: TcxGridDBColumn;
    ViewDocsInvITYPE: TcxGridDBColumn;
    ViewDocsInvSUM3: TcxGridDBColumn;
    ViewDocsInvSUM31: TcxGridDBColumn;
    ViewDocsInvSUM2: TcxGridDBColumn;
    ViewDocsInvSUM21: TcxGridDBColumn;
    ViewDocsInvSUMDIFIN: TcxGridDBColumn;
    ViewDocsInvSUMDIFUCH: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    PopupMenu1: TPopupMenu;
    acCopy: TAction;
    acPast: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewDocsInvSUM1: TcxGridDBColumn;
    ViewDocsInvSUM11: TcxGridDBColumn;
    N3: TMenuItem;
    Excel1: TMenuItem;
    ViewDocsInvCOMMENT: TcxGridDBColumn;
    ViewDocsInvSUMTARAR: TcxGridDBColumn;
    ViewDocsInvSUMTARAF: TcxGridDBColumn;
    ViewDocsInvSUMTARAD: TcxGridDBColumn;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    acExit: TAction;
    ViewDocsInvIDETAL: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInvDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acPastExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure ViewDocsInvCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N4Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prCalcRemnInv(IDH,iDate,IdSkl:Integer);
    procedure prOn(IDH,iDate,IdSkl:Integer;Var rSum1,rSum2,rSum11,rSum22:Real);
  end;

var
  fmDocsInv: TfmDocsInv;
  bClearDocIn:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, AddInv, DMOReps, TBuff,
  MainRnOffice;

{$R *.dfm}

procedure TfmDocsInv.prOn(IDH,iDate,IdSkl:Integer;Var rSum1,rSum2,rSum11,rSum22:Real);
Var rQ,rQs,rQInv,rQp:Real;
    rPrice,rPriceUch,rPrice0:Real;
    rQCalc:Real;
    rSum0,rSum00:Real;
    iSS:INteger;
begin
  with dmO do
  with dmORep do
  begin
    quSpecInvC.Active:=False;
    quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
    quSpecInvC.Active:=True;

    quSpecInv.Active:=False;  // ��� ��� ������ ����������� ���-�� �� ������� �.�. � �������� ������ ���� �������������� �������� ��� ��������� ����
    quSpecInv.ParamByName('IDH').AsInteger:=IDH;
    quSpecInv.Active:=True;

    rSum1:=0; rSum11:=0; rSum2:=0; rSum22:=0;  rSum0:=0; rSum00:=0;

    iSS:=prISS(IdSkl);

    quSpecInvC.First;
    while not quSpecInvC.Eof do
    begin
//            rQc:=quSpecInvCQUANT.AsFloat;
      rSum1:=rSum1+quSpecInvCSumIn.AsFloat; rSum11:=rSum11+quSpecInvCSumUch.AsFloat;
      rSum2:=rSum2+quSpecInvCSUMINFACT.AsFloat; rSum22:=rSum22+quSpecInvCSUMUCHFACT.AsFloat;
      rSum0:=rSum0+quSpecInvCSUMIN0.AsFloat; rSum00:=rSum00+quSpecInvCSUMINFACT0.AsFloat;

      rPrice:=0; rPriceUch:=0;  rPrice0:=0;

      rQCalc:=0; //��������� �������� - �������� ��� �������
{
      if quSpecInvCQUANTFACT.AsFloat>0 then rPrice:=RoundVal(quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat)
      else if abs(quSpecInvCQUANT.AsFloat)>0 then rPrice:=RoundVal(quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat);

      if quSpecInvCQUANTFACT.AsFloat>0 then rPriceUch:=RoundVal(quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat)
      else if abs(quSpecInvCQUANT.AsFloat)>0 then rPriceUch:=RoundVal(quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat);
}
      if quSpecInvCQUANTFACT.AsFloat>0 then
      begin
        rPrice:=RoundVal(quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat);
        rPriceUch:=RoundVal(quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat);
        rPrice0:=RoundVal(quSpecInvCSUMINFACT0.AsFloat/quSpecInvCQUANTFACT.AsFloat);
      end else
      begin
        if abs(quSpecInvCQUANT.AsFloat)>0 then
        begin
          rPrice:=RoundVal(quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat);
          rPriceUch:=RoundVal(quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat);
          rPrice0:=RoundVal(quSpecInvCSUMIN0.AsFloat/quSpecInvCQUANT.AsFloat);
        end;
      end;

      if quSpecInv.Locate('IDCARD',quSpecInvCIDCARD.AsInteger,[]) then rQCalc:=quSpecInvQUANT.AsFloat*quSpecInvKM.AsFloat;

      rQInv:=quSpecInvCQUANTFACT.AsFloat-rQCalc;

//      rQInv:=quSpecInvCQUANTFACT.AsFloat-quSpecInvCQUANT.AsFloat;

      if rQInv=0 then rQInv:=0.000001;  //�.�. �� ������ � ������� ���-���, �� � � ������� ��������.
      if rQInv>0 then  //��� = 0 ���� �� ����� ����������� � �� ����� ����������
      begin //�������-������ �� ������ , �� ������������ 0 ���-�� ����.
              {�� ��� ��� ������ - �� ������ ����� ������� ������ ���� quSpecInvCQUANTFACT.AsF�������2loat>0
              � ���� quSpecInvCQUANTFACT.AsFloat<=0 �����
              1. �������������� ������
              2. ��������� ��� �������� ������
              3. ������� ��������� ������ �� ���������

              ������ ������ �������� � ����� ���������  prAddPartIn2
              }

        prAddPartIn2.ParamByName('IDSKL').AsInteger:=IdSkl;
        prAddPartIn2.ParamByName('IDDOC').AsInteger:=IDH;
        prAddPartIn2.ParamByName('DTYPE').AsInteger:=3;
        prAddPartIn2.ParamByName('IDATE').AsInteger:=iDate;
        prAddPartIn2.ParamByName('IDCARD').AsInteger:=quSpecInvCIDCARD.AsInteger;
        prAddPartIn2.ParamByName('IDCLI').AsInteger:=0;
        prAddPartIn2.ParamByName('QUANTF').AsFloat:=quSpecInvCQUANTFACT.AsFloat;
        prAddPartIn2.ParamByName('QUANTC').AsFloat:=rQCalc;
        prAddPartIn2.ParamByName('PRICEIN').AsFloat:=rPrice;
        prAddPartIn2.ParamByName('PRICEUCH').AsFloat:=rPriceUch;
        prAddPartIn2.ParamByName('PRICEIN0').AsFloat:=rPrice0;
        prAddPartIn2.ExecProc;

      end else
      begin //���������-������ �� ������
        //������� �������� ������ �� �������
        //����� �������� ��� �������� � �������

       // ����������� ��� �� ����� ��������������

        rQs:=rQInv*(-1);
        prSelPartIn(quSpecInvCIDCARD.AsInteger,IdSkl,0,0);

        quSelPartIn.First;

        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
                //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;

//          prWH('���. ������: ���  '+quSpecInvCIDCARD.AsString+' '+quSpecInvCName.AsString+', ����� '+quDocsInvSelIDSKL.AsString+', ��� ��. ������ '+quSelPartInID.AsString+', ���-�� '+FloatToStr(RoundEx(rQ*1000)/1000));

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecInvCIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=IDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=3;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
          if iSS=2 then prAddPartOut.ParamByName('PRICEIN').AsFloat:=rPrice0
          else prAddPartOut.ParamByName('PRICEIN').AsFloat:=rPrice;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=rPriceUch*rQ;
          prAddPartout.ExecProc;

          quSelPartIn.Next;
        end;

        quSelPartIn.Active:=False;

        //��������� �������� � ������������� ������
        if rQs>0 then //�������� ������������� ������, �������� � ������
        begin
                  //��������� ��� ����� ���������
//          prWH('�����. ������: ���  '+quSpecInvCIDCARD.AsString+' '+quSpecInvCName.AsString+', ����� '+quDocsInvSelIDSKL.AsString+', ��� ��. ������ '+quSelPartInID.AsString+', ���-�� '+FloatToStr(RoundEx(rQs*1000)/1000));

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecInvCIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=3;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
//          prAddPartOut.ParamByName('PRICEIN').AsFloat:=rPrice;
          if iSS=2 then prAddPartOut.ParamByName('PRICEIN').AsFloat:=rPrice0
          else prAddPartOut.ParamByName('PRICEIN').AsFloat:=rPrice;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=rPriceUch*rQs;
          prAddPartout.ExecProc;
        end;
      end;

      quSpecInvC.Next;
    end;
    quSpecInv.Active:=False;  // ��� ��� ������ ����������� ���-�� �� ������� �.�. � �������� ������ ���� �������������� �������� ��� ��������� ����
    quSpecInvC.Active:=False;

    if iSS=2 then
    begin
      rSum1:=rSum0;
      rSum2:=rSum00;
    end;
  end;
end;

procedure TfmDocsInv.prCalcRemnInv(IDH,iDate,IdSkl:Integer);
Var rQ:Real;
    rSum,rSumTO,rSum0:Real;
    iSS:Integer;
begin
  with dmO do
  with dmORep do
  begin
    iSS:=prISS(IdSkl);

    quSpecInvC.Active:=False;
    quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
    quSpecInvC.Active:=True;

    quSpecInvC.First;
    while not quSpecInvC.Eof do
    begin
      rQ:=prCalcRemn(quSpecInvCIdCard.AsInteger,iDate,IdSkl);

      prCalcSumRemn.ParamByName('IDCARD').AsInteger:=quSpecInvCIdCard.AsInteger;
      prCalcSumRemn.ParamByName('IDSKL').AsInteger:=IdSkl;
      prCalcSumRemn.ParamByName('DDATE').AsDateTime:=iDate;
      prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
      prCalcSumRemn.ExecProc;

      rSumTO:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;
      //rSumTO - ��� �������� ���� , ���� ��� ��������������

      rSum:=prCalcRemnSumF(quSpecInvCIdCard.AsInteger,iDate,IdSkl,rQ,rSum0);
        //rSum - ��� �������������� ���� , �.�. �������������� �� ��������� �������
//        rSum:=prCalcRemnSum(quSpecInvCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if quSpecInvCKm.AsFloat<>0 then rQ:=rQ/quSpecInvCKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*quSpecInvCKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.

      quSpecInvC.Edit;
      quSpecInvCQuant.AsFloat:=rQ;
      quSpecInvCSUMINR.AsFloat:=RoundVal(rSumTO);
      quSpecInvCSumIn.AsFloat:=RoundVal(rSum);
      quSpecInvCSumUch.AsFloat:=RoundVal(rSum);

      quSpecInvC.Post;

      quSpecInvC.Next;
    end;

    quSpecInvC.Active:=False;

    quSpecInv.Active:=False;
    quSpecInv.ParamByName('IDH').AsInteger:=IDH;
    quSpecInv.Active:=True;

    quSpecInv.First;
    while not quSpecInv.Eof do
    begin
      rQ:=prCalcRemn(quSpecInvIdCard.AsInteger,iDate,IdSkl);

      prCalcSumRemn.ParamByName('IDCARD').AsInteger:=quSpecInvIdCard.AsInteger;
      prCalcSumRemn.ParamByName('IDSKL').AsInteger:=IdSkl;
      prCalcSumRemn.ParamByName('DDATE').AsDate:=iDate;
      prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
      prCalcSumRemn.ExecProc;
      rSumTO:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;

      rSum:=prCalcRemnSumF(quSpecInvIdCard.AsInteger,iDate,IdSkl,rQ,rSum0);
        //rSum - ��� �������������� ���� , �.�. �������������� �� ��������� �������

//        rSum:=prCalcRemnSum(quSpecInvIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if quSpecInvKm.AsFloat<>0 then rQ:=rQ/quSpecInvKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*quSpecInvKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
//      end;

      quSpecInv.Edit;
      quSpecInvQuant.AsFloat:=rQ;
      quSpecInvSumIn.AsFloat:=RoundVal(rSum);
      quSpecInvSumUch.AsFloat:=RoundVal(rSum);
      quSpecInvSUMINR.AsFloat:=RoundVal(rSumTO);
      quSpecInv.Post;

      quSpecInv.Next;
    end;
    quSpecInv.Active:=False;
    quSpecInvC.Active:=False;

  end;
end;



procedure TfmDocsInv.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInv.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsInv.Align:=AlClient;
  ViewDocsInv.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsInv.RestoreFromIniFile(CurDir+GridIni);

  StatusBar1.Color:= UserColor.TTnInv;
  SpeedBar1.Color := UserColor.TTnInv;

  Memo1.Clear;
end;

procedure TfmDocsInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsInv.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsInv.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsInv.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsInv.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='�������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsInv.BeginUpdate;
        quDocsInvSel.Active:=False;
        quDocsInvSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInvSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInvSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quDocsInvSel.Active:=True;
        ViewDocsInv.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsInv.BeginUpdate;
{       quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsInv.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsInv.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if fmAddInv.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddInv.Caption:='��������������: ����� ��������.';
    fmAddInv.cxTextEdit1.Text:=prGetNum(3,0);
    fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddInv.cxTextEdit1.Tag:=0; //������� ���������� ���������

    fmAddInv.cxComboBox1.ItemIndex:=1; //��� ���������

    fmAddInv.cxTextEdit2.Text:='';
    fmAddInv.cxTextEdit2.Properties.ReadOnly:=False;

    fmAddInv.cxDateEdit1.Date:=Date;
    fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;

    fmAddInv.cxLookupComboBox1.EditValue:=0;
    fmAddInv.cxLookupComboBox1.Text:='';
    fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddInv.Label15.Caption:='��. ����: ';
      fmAddInv.Label15.Tag:=0;
    end;

    fmAddInv.cxLabel1.Enabled:=True;
    fmAddInv.cxLabel2.Enabled:=True;
    fmAddInv.cxLabel7.Enabled:=True;
    fmAddInv.cxLabel8.Enabled:=True;
    fmAddInv.cxLabel9.Enabled:=True;
    fmAddInv.cxButton1.Enabled:=True;
    fmAddInv.cxButton3.Enabled:=False;
    fmAddInv.Label3.Enabled:=True;
    fmAddInv.Label4.Enabled:=True;
    fmAddInv.Label6.Enabled:=True;
    fmAddInv.cxCheckBox1.Enabled:=True;

    fmAddInv.ViewInv.OptionsData.Editing:=True;
    fmAddInv.ViewInvC.OptionsData.Editing:=True;

    fmAddInv.taSpec.Active:=False;
    fmAddInv.taSpecC.Active:=False;
    taCalcB.Active:=False;

    fmAddInv.dsSpec.DataSet:=fmAddInv.taSpec1;
    fmAddInv.dsSpecC.DataSet:=fmAddInv.taSpecC1;
    dsCalcB.DataSet:=taCalcB1;

    CloseTe(fmAddInv.taSpec1);
    CloseTe(fmAddInv.taSpecC1);
    CloseTe(taCalcB1);

    fmAddInv.Show;
  end;
end;

procedure TfmDocsInv.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddInv.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        Memo1.Clear;
        prWH('����� ���� �������� ���������.',Memo1); Delay(10);

        prAllViewOff;

        fmAddInv.Caption:='��������������: ��������������.';
        fmAddInv.cxTextEdit1.Text:=quDocsInvSelNUMDOC.AsString;
        fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddInv.cxTextEdit1.Tag:=quDocsInvSelId.AsInteger;
        fmAddInv.cxDateEdit1.Date:=quDocsInvSelDATEDOC.AsDateTime;
        fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

        fmAddInv.cxComboBox1.ItemIndex:=quDocsInvSelIDETAL.AsInteger-1; 

        fmAddInv.cxTextEdit2.Text:=quDocsInvSelCOMMENT.AsString;
        fmAddInv.cxTextEdit2.Properties.ReadOnly:=False;

        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;

        fmAddInv.cxLookupComboBox1.EditValue:=quDocsInvSelIDSKL.AsInteger;
        fmAddInv.cxLookupComboBox1.Text:=quDocsInvSelNAMEMH.AsString;
        fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsInvSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInvSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddInv.Label15.Caption:='��. ����: ';
          fmAddInv.Label15.Tag:=0;
        end;

        fmAddInv.cxLabel1.Enabled:=True;
        fmAddInv.cxLabel2.Enabled:=True;
        fmAddInv.cxLabel7.Enabled:=True;
        fmAddInv.cxLabel8.Enabled:=True;
        fmAddInv.cxLabel9.Enabled:=True;
        fmAddInv.cxButton1.Enabled:=True;
        fmAddInv.cxButton3.Enabled:=False;
        fmAddInv.Label3.Enabled:=True;
        fmAddInv.Label4.Enabled:=True;
        fmAddInv.Label6.Enabled:=True;
        fmAddInv.cxCheckBox1.Enabled:=True;


        fmAddInv.ViewInv.OptionsData.Editing:=True;
        fmAddInv.ViewInvC.OptionsData.Editing:=True;

        CloseTe(fmAddInv.taSpec1);
        CloseTe(fmAddInv.taSpecC1);

        IDH:=quDocsInvSelID.AsInteger;

        prWH('   ������ � �����.',Memo1);

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDH').AsInteger:=IDH;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddInv do
          begin

            taSpec1.Append;
            taSpec1Num.AsInteger:=quSpecInvNUM.AsInteger;
            taSpec1IdGoods.AsInteger:=quSpecInvIDCARD.AsInteger;
            taSpec1NameG.AsString:=quSpecInvNAME.AsString;
            taSpec1IM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
            taSpec1SM.AsString:=quSpecInvNAMESHORT.AsString;
            taSpec1Quant.AsFloat:=quSpecInvQUANT.AsFloat;
            taSpec1SumIn.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpec1SumInTO.AsFloat:=quSpecInvSUMINR.AsFloat;
            taSpec1SumUch.AsFloat:=quSpecInvSUMUCH.AsFloat;
            taSpec1PriceIn.AsFloat:=0; taSpec1PriceUch.AsFloat:=0; taSpec1PriceIn0.AsFloat:=0;
            if quSpecInvQUANT.AsFloat<>0 then
            begin
              taSpec1PriceIn0.AsFloat:=quSpecInvSUMIN0.AsFloat/quSpecInvQUANT.AsFloat;
              taSpec1PriceIn.AsFloat:=quSpecInvSUMIN.AsFloat/quSpecInvQUANT.AsFloat;
              taSpec1PriceUch.AsFloat:=quSpecInvSUMUCH.AsFloat/quSpecInvQUANT.AsFloat;
            end;
            taSpec1QuantFact.AsFloat:=quSpecInvQUANTFACT.AsFloat;
            taSpec1SumInF.AsFloat:=quSpecInvSUMINFACT.AsFloat;
            taSpec1SumUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
            taSpec1PriceInF.AsFloat:=0; taSpec1PriceUchF.AsFloat:=0; taSpec1PriceInF0.AsFloat:=0;
            if quSpecInvQUANTFACT.AsFloat<>0 then
            begin
              taSpec1PriceInF.AsFloat:=quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpec1PriceInF0.AsFloat:=quSpecInvSUMINFACT0.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpec1PriceUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
            end;

            taSpec1Km.AsFloat:=quSpecInvKM.AsFloat;
            taSpec1TCard.AsInteger:=quSpecInvTCARD.AsInteger;
            taSpec1NoCalc.AsInteger:=quSpecInvNOCALC.AsInteger;
            taSpec1IdGroup.AsInteger:=quSpecInvIDGROUP.AsInteger;
//            taSpec1NameGr.AsString:=quSpecInvNAMECL.AsString;
            taSpec1CType.AsInteger:=quSpecInvCATEGORY.AsInteger;

            taSpec1SumIn0.AsFloat:=quSpecInvSUMIN0.AsFloat;
            taSpec1SumInF0.AsFloat:=quSpecInvSUMINFACT0.AsFloat;
            taSpec1NDSProc.AsFloat:=quSpecInvNDSPROC.AsFloat;
            taSpec1SumNDS.AsFloat:=quSpecInvNDSSUM.AsFloat;

            taSpec1.Post;
          end;
          quSpecInv.Next;
        end;

        prWH('   ������.',Memo1);

        quSpecInvC.Active:=False;
        quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvC.Active:=True;

        quSpecInvC.First;
        while not quSpecInvC.Eof do
        begin
          with fmAddInv do
          begin
            taSpecC1.Append;
            taSpecC1Num.AsInteger:=quSpecInvCNUM.AsInteger;
            taSpecC1IdGoods.AsInteger:=quSpecInvCIDCARD.AsInteger;
            taSpecC1NameG.AsString:=quSpecInvCNAME.AsString;
            taSpecC1IM.AsInteger:=quSpecInvCIDMESSURE.AsInteger;
            taSpecC1SM.AsString:=quSpecInvCNAMESHORT.AsString;
            taSpecC1Quant.AsFloat:=quSpecInvCQUANT.AsFloat;
            taSpecC1SumIn.AsFloat:=quSpecInvCSUMIN.AsFloat;
            taSpecC1SumUch.AsFloat:=quSpecInvCSUMUCH.AsFloat;
            taSpecC1PriceIn.AsFloat:=0; taSpecC1PriceUch.AsFloat:=0; taSpecC1PriceIn0.AsFloat:=0;
            if quSpecInvCQUANT.AsFloat<>0 then
            begin
              taSpecC1PriceIn.AsFloat:=quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecC1PriceIn0.AsFloat:=quSpecInvCSUMIN0.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecC1PriceUch.AsFloat:=quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat;
            end;
            taSpecC1QuantFact.AsFloat:=quSpecInvCQUANTFACT.AsFloat;
            taSpecC1SumInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat;
            taSpecC1SumUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat;
            taSpecC1PriceInF.AsFloat:=0; taSpecC1PriceUchF.AsFloat:=0; taSpecC1PriceInF0.AsFloat:=0;
            if quSpecInvCQUANTFACT.AsFloat<>0 then
            begin
              taSpecC1PriceInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecC1PriceInF0.AsFloat:=quSpecInvCSUMINFACT0.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecC1PriceUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
            end;

            taSpecC1Km.AsFloat:=quSpecInvCKM.AsFloat;
            taSpecC1TCard.AsInteger:=quSpecInvCTCARD.AsInteger;
            taSpecC1IdGroup.AsInteger:=quSpecInvCIDGROUP.AsInteger;
//            taSpecC1NameGr.AsString:=quSpecInvCNAMECL.AsString;
            taSpecC1SumInTO.AsFloat:=quSpecInvCSUMINR.AsFloat;
            taSpecC1CType.AsInteger:=quSpecInvCCATEGORY.AsInteger;

            taSpecC1SumIn0.AsFloat:=quSpecInvCSUMIN0.AsFloat;
            taSpecC1SumInF0.AsFloat:=quSpecInvCSUMINFACT0.AsFloat;
            taSpecC1NDSProc.AsFloat:=quSpecInvCNDSPROC.AsFloat;
            taSpecC1SumNDS.AsFloat:=quSpecInvCNDSSUM.AsFloat;

            taSpecC1.Post;
          end;
          quSpecInvC.Next;
        end;
        prWH('   �����������.',Memo1);

        CloseTe(taCalcB1);

        quSpecInvBC.Active:=False;
        quSpecInvBC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvBC.Active:=True;

        quSpecInvBC.First;
        while not quSpecInvBC.Eof do
        begin
          taCalcB1.Append;
          taCalcB1ID.AsInteger:=quSpecInvBCIDB.AsInteger;
          taCalcB1CODEB.AsInteger:=quSpecInvBCCODEB.AsInteger;
          taCalcB1NAMEB.AsString:=Copy(quSpecInvBCNAMEB.AsString,1,30);
          taCalcB1QUANT.AsFloat:=quSpecInvBCQUANT.AsFloat;
          taCalcB1PRICEOUT.AsFloat:=quSpecInvBCPRICEOUT.AsFloat;
          taCalcB1SUMOUT.AsFloat:=quSpecInvBCSUMOUT.AsFloat;
          taCalcB1IDCARD.AsInteger:=quSpecInvBCIDCARD.AsInteger;
          taCalcB1NAMEC.AsString:=Copy(quSpecInvBCNAMEC.AsString,1,30);
          taCalcB1QUANTC.AsFloat:=quSpecInvBCQUANTC.AsFloat;
          taCalcB1PRICEIN.AsFloat:=quSpecInvBCPRICEIN.AsFloat;
          taCalcB1SUMIN.AsFloat:=quSpecInvBCSUMIN.AsFloat;
          taCalcB1IM.AsInteger:=quSpecInvBCIM.AsInteger;
          taCalcB1SM.AsString:=quSpecInvBCSM.AsString;
          taCalcB1SB.AsString:=quSpecInvBCSB.AsString;

          taCalcB1PRICEIN0.AsFloat:=quSpecInvBCPRICEIN0.AsFloat;
          taCalcB1SUMIN0.AsFloat:=quSpecInvBCSUMIN0.AsFloat;

          taCalcB1.Post;

          quSpecInvBC.Next;
        end;

        prAllViewOn;

        fmAddInv.taSpec.Active:=False;
        fmAddInv.taSpecC.Active:=False;
        taCalcB.Active:=False;

        fmAddInv.dsSpec.DataSet:=fmAddInv.taSpec1;
        fmAddInv.dsSpecC.DataSet:=fmAddInv.taSpecC1;
        dsCalcB.DataSet:=taCalcB1;

        prWH('   �������� ��.',Memo1);
        fmAddInv.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;//}
  end;
end;

procedure TfmDocsInv.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddInv.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
        Memo1.Clear;
        prWH('����� ���� �������� ���������.',Memo1);

        prAllViewOff;

        fmAddInv.Caption:='��������������: ��������.';
        fmAddInv.cxTextEdit1.Text:=quDocsInvSelNUMDOC.AsString;
        fmAddInv.cxTextEdit1.Properties.ReadOnly:=True;
        fmAddInv.cxTextEdit1.Tag:=quDocsInvSelId.AsInteger;
        fmAddInv.cxDateEdit1.Date:=quDocsInvSelDATEDOC.AsDateTime;
        fmAddInv.cxDateEdit1.Properties.ReadOnly:=True;
        fmAddInv.cxTextEdit2.Text:=quDocsInvSelCOMMENT.AsString;
        fmAddInv.cxTextEdit2.Properties.ReadOnly:=True;

        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;

        fmAddInv.cxComboBox1.ItemIndex:=quDocsInvSelIDETAL.AsInteger-1;

        fmAddInv.cxLookupComboBox1.EditValue:=quDocsInvSelIDSKL.AsInteger;
        fmAddInv.cxLookupComboBox1.Text:=quDocsInvSelNAMEMH.AsString;
        fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=True;

        CurVal.IdMH:=quDocsInvSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInvSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddInv.Label15.Caption:='��. ����: ';
          fmAddInv.Label15.Tag:=0;
        end;

        fmAddInv.cxLabel1.Enabled:=False;
        fmAddInv.cxLabel2.Enabled:=False;
        fmAddInv.cxLabel7.Enabled:=False;
        fmAddInv.cxLabel8.Enabled:=False;
        fmAddInv.cxLabel9.Enabled:=False;
        fmAddInv.cxButton1.Enabled:=False;
        fmAddInv.cxButton3.Enabled:=True;

        fmAddInv.cxCheckBox1.Enabled:=False;

        fmAddInv.Label3.Enabled:=False;
        fmAddInv.Label4.Enabled:=False;
        fmAddInv.Label6.Enabled:=False;


        fmAddInv.ViewInv.OptionsData.Editing:=False;
        fmAddInv.ViewInvC.OptionsData.Editing:=False;

        CloseTa(fmAddInv.taSpec);
        CloseTa(fmAddInv.taSpecC);

        IDH:=quDocsInvSelID.AsInteger;

        prWH('   ������ � �����.',Memo1);

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDH').AsInteger:=IDH;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddInv do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecInvNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecInvIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecInvNAME.AsString;
            taSpecIM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
            taSpecSM.AsString:=quSpecInvNAMESHORT.AsString;
            taSpecQuant.AsFloat:=quSpecInvQUANT.AsFloat;
            taSpecSumIn.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpecSumInTO.AsFloat:=quSpecInvSUMINR.AsFloat;
            taSpecSumUch.AsFloat:=quSpecInvSUMUCH.AsFloat;
            taSpecPriceIn.AsFloat:=0; taSpecPriceUch.AsFloat:=0;
            if quSpecInvQUANT.AsFloat<>0 then
            begin
              taSpecPriceIn.AsFloat:=quSpecInvSUMIN.AsFloat/quSpecInvQUANT.AsFloat;
              taSpecPriceUch.AsFloat:=quSpecInvSUMUCH.AsFloat/quSpecInvQUANT.AsFloat;
            end;
            taSpecQuantFact.AsFloat:=quSpecInvQUANTFACT.AsFloat;
            taSpecSumInF.AsFloat:=quSpecInvSUMINFACT.AsFloat;
            taSpecSumUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
            taSpecPriceInF.AsFloat:=0; taSpecPriceUchF.AsFloat:=0;
            if quSpecInvQUANTFACT.AsFloat<>0 then
            begin
              taSpecPriceInF.AsFloat:=quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpecPriceUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
            end;

            taSpecKm.AsFloat:=quSpecInvKM.AsFloat;
            taSpecTCard.AsInteger:=quSpecInvTCARD.AsInteger;
            taSpecNoCalc.AsInteger:=quSpecInvNOCALC.AsInteger;
            taSpecId_Group.AsInteger:=quSpecInvIDGROUP.AsInteger;
            taSpecNameGr.AsString:=quSpecInvNAMECL.AsString;
            taSpecCType.AsInteger:=quSpecInvCATEGORY.AsInteger;
            taSpecSumIn0.AsFloat:=quSpecInvSUMIN0.AsFloat;
            taSpecSumInF0.AsFloat:=quSpecInvSUMINFACT0.AsFloat;
            taSpecNDSProc.AsFloat:=quSpecInvNDSPROC.AsFloat;
            taSpecSumNDS.AsFloat:=quSpecInvNDSSUM.AsFloat;

            taSpec.Post;
          end;
          quSpecInv.Next;
        end;

        prWH('   ������.',Memo1);

        quSpecInvC.Active:=False;
        quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvC.Active:=True;

        quSpecInvC.First;
        while not quSpecInvC.Eof do
        begin
          with fmAddInv do
          begin
            taSpecC.Append;
            taSpecCNum.AsInteger:=quSpecInvCNUM.AsInteger;
            taSpecCIdGoods.AsInteger:=quSpecInvCIDCARD.AsInteger;
            taSpecCNameG.AsString:=quSpecInvCNAME.AsString;
            taSpecCIM.AsInteger:=quSpecInvCIDMESSURE.AsInteger;
            taSpecCSM.AsString:=quSpecInvCNAMESHORT.AsString;
            taSpecCQuant.AsFloat:=quSpecInvCQUANT.AsFloat;
            taSpecCSumIn.AsFloat:=quSpecInvCSUMIN.AsFloat;
            taSpecCSumUch.AsFloat:=quSpecInvCSUMUCH.AsFloat;
            taSpecCPriceIn.AsFloat:=0; taSpecCPriceUch.AsFloat:=0; taSpecCPriceIn0.AsFloat:=0;
            if quSpecInvCQUANT.AsFloat<>0 then
            begin
              taSpecCPriceIn.AsFloat:=quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecCPriceIn0.AsFloat:=quSpecInvCSUMIN0.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecCPriceUch.AsFloat:=quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat;
            end;
            taSpecCQuantFact.AsFloat:=quSpecInvCQUANTFACT.AsFloat;
            taSpecCSumInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat;
            taSpecCSumUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat;
            taSpecCPriceInF.AsFloat:=0; taSpecCPriceUchF.AsFloat:=0; taSpecCPriceInF0.AsFloat:=0;
            if quSpecInvCQUANTFACT.AsFloat<>0 then
            begin
              taSpecCPriceInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecCPriceInF0.AsFloat:=quSpecInvCSUMINFACT0.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecCPriceUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
            end;

            taSpecCKm.AsFloat:=quSpecInvCKM.AsFloat;
            taSpecCTCard.AsInteger:=quSpecInvCTCARD.AsInteger;
            taSpecCId_Group.AsInteger:=quSpecInvCIDGROUP.AsInteger;
            taSpecCNameGr.AsString:=quSpecInvCNAMECL.AsString;
            taSpecCSumInTO.AsFloat:=quSpecInvCSUMINR.AsFloat;
            taSpecCCType.AsInteger:=quSpecInvCCATEGORY.AsInteger;

            taSpecCSumIn0.AsFloat:=quSpecInvCSUMIN0.AsFloat;
            taSpecCSumInF0.AsFloat:=quSpecInvCSUMINFACT0.AsFloat;
            taSpecCSumInDif0.AsFloat:=quSpecInvCSUMINFACT0.AsFloat-quSpecInvCSUMIN0.AsFloat;
            taSpecCNDSProc.AsFloat:=quSpecInvCNDSPROC.AsFloat;
            taSpecCSumNDS.AsFloat:=quSpecInvCNDSSUM.AsFloat;

            taSpecC.Post;
          end;
          quSpecInvC.Next;
        end;
        prWH('   �����������.',Memo1);

        CloseTe(taCalcB1);

        quSpecInvBC.Active:=False;
        quSpecInvBC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvBC.Active:=True;

        quSpecInvBC.First;
        while not quSpecInvBC.Eof do
        begin
          taCalcB1.Append;
          taCalcB1ID.AsInteger:=quSpecInvBCIDB.AsInteger;
          taCalcB1CODEB.AsInteger:=quSpecInvBCCODEB.AsInteger;
          taCalcB1NAMEB.AsString:=Copy(quSpecInvBCNAMEB.AsString,1,30);
          taCalcB1QUANT.AsFloat:=quSpecInvBCQUANT.AsFloat;
          taCalcB1PRICEOUT.AsFloat:=quSpecInvBCPRICEOUT.AsFloat;
          taCalcB1SUMOUT.AsFloat:=quSpecInvBCSUMOUT.AsFloat;
          taCalcB1IDCARD.AsInteger:=quSpecInvBCIDCARD.AsInteger;
          taCalcB1NAMEC.AsString:=Copy(quSpecInvBCNAMEC.AsString,1,30);
          taCalcB1QUANTC.AsFloat:=quSpecInvBCQUANTC.AsFloat;
          taCalcB1PRICEIN.AsFloat:=quSpecInvBCPRICEIN.AsFloat;
          taCalcB1SUMIN.AsFloat:=quSpecInvBCSUMIN.AsFloat;
          taCalcB1IM.AsInteger:=quSpecInvBCIM.AsInteger;
          taCalcB1SM.AsString:=quSpecInvBCSM.AsString;
          taCalcB1SB.AsString:=quSpecInvBCSB.AsString;
          taCalcB1PRICEIN0.AsFloat:=quSpecInvBCPRICEIN0.AsFloat;
          taCalcB1SUMIN0.AsFloat:=quSpecInvBCSUMIN0.AsFloat;
          taCalcB1.Post;

          quSpecInvBC.Next;
        end;

        fmAddInv.taSpec1.Active:=False;
        fmAddInv.taSpecC1.Active:=False;

        fmAddInv.dsSpec.DataSet:=fmAddInv.taSpec;
        fmAddInv.dsSpecC.DataSet:=fmAddInv.taSpecC;

        prAllViewOn;

        prWH('   �������� ��.',Memo1);
        fmAddInv.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;//}
  end;
end;

procedure TfmDocsInv.ViewDocsInvDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsInvSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsInv.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������������� �'+quDocsInvSelNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.asDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          IDH:=quDocsInvSelID.AsInteger;
          quDocsInvSel.Delete;

{          quSpecInv.Active:=False;
          quSpecInv.ParamByName('IDH').AsInteger:=IDH;
          quSpecInv.Active:=True;

          quSpecInv.First; //������
          while not quSpecInv.Eof do quSpecInv.Delete;
          quSpecInv.Active:=False;

          quSpecInvC.Active:=False;
          quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
          quSpecInvC.Active:=True;

          quSpecInvC.First; //������
          while not quSpecInvC.Eof do quSpecInvC.Delete;
          quSpecInvC.Active:=False;
}
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsInv.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
//    rQc:Real;
    rSum1,rSum2,rSum11,rSum22,rSumTO:Real;
begin
//������������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount=0 then  exit;

    if not CanDo('prOnDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsInvSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsInvSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          rSumTO:=0;
          if prTOFind(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInvSelNAMEMH.AsString+' � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              rSumTO:=prFindTOEnd(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger);
              prTODel(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������)
         // 2 - �������� ����� ������
         // 3 - �������� ��������������
         // 4 - �������� ������

          Memo1.Clear;
          prWH('����� ... ���� ��������� ���������.',Memo1);
          IDH:=quDocsInvSelID.AsInteger;
         // 1 - ������� ��������� ��������� ������ �� ��������� (�� ������ ������) ��� �������� ������ ��������� ��������� ������
         // � ��� ��������� ��������������

          prDelPartInv.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPartInv.ParamByName('DTYPE').AsInteger:=3;
          prDelPartInv.ExecProc;

          prOn(IDH,trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger,rSum1,rSum2,rSum11,rSum22);

         // �������� ������

          quDocsInvSel.Edit;
          quDocsInvSelIACTIVE.AsInteger:=1;
          if quDocsInvSelIDETAL.AsInteger=2 then quDocsInvSelSUM3.AsFloat:=rSumTO;
          quDocsInvSel.Post;
          quDocsInvSel.Refresh;

          prWH('��������� ���������.',Memo1); 

        end;
      end;
    end;

  end;
end;

procedure TfmDocsInv.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount=0 then  exit;
  
    if not CanDo('prOffDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsInvSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsInvSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInvSelNAMEMH.AsString+' � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� �� ������� ���������� �����
         // ���� ������ ��
          Memo1.Clear;
          prWH('����� ... ���� ��������� ���������.',Memo1);

          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=3;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+' �����.');
//             bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ������ �� ���������
            // 3 - �������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=3;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsInvSelDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            delay(10);

            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=3;
            prDelPartOut.ExecProc;

            delay(10);
            // 4 - �������� ������
            quDocsInvSel.Edit;
            quDocsInvSelIACTIVE.AsInteger:=0;
            quDocsInvSel.Post;

            delay(10);

            quDocsInvSel.Refresh;
          end;
          prWH('��������� ���������.',Memo1);
        end;
      end;
    end;
  end;
end;

procedure TfmDocsInv.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsInv.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsInv.Visible then
    begin
    end;
  end;
end;

procedure TfmDocsInv.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInv.acCopyExecute(Sender: TObject);
var Par:Variant;
    IDH:INteger;
    iNum:INteger;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=3; //������������
    par[1]:=quDocsInvSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=3;
      taHeadDocId.AsInteger:=quDocsInvSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsInvSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsInvSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=0;
      taHeadDocNameCli.AsString:='';
      taHeadDocIdSkl.AsInteger:=quDocsInvSelIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsInvSelNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsInvSelSUM2.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsInvSelSUM21.AsFloat;
      taHeadDoc.Post;

      IDH:=quDocsInvSelID.AsInteger;


      quSpecInv.Active:=False;
      quSpecInv.ParamByName('IDH').AsInteger:=IDH;
      quSpecInv.Active:=True;

      quSpecInv.First; iNum:=1;     //����� ������ ����� �.�. ��������� �������������
      while not quSpecInv.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=3;
        taSpecDocIdHead.AsInteger:=IDH;
        taSpecDocNum.AsInteger:=iNum;
        taSpecDocIdCard.AsInteger:=quSpecInvIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=RoundEx(quSpecInvQUANTFACT.AsFloat*1000)/1000;
        taSpecDocPriceIn.AsFloat:=0;
        if abs(quSpecInvQUANTFACT.AsFloat)>0 then
          taSpecDocPriceIn.AsFloat:=RoundEx(quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat*100)/100;
        taSpecDocSumIn.AsFloat:=quSpecInvSUMINFACT.AsFloat;
        taSpecDocPriceUch.AsFloat:=0;
        if abs(quSpecInvQUANTFACT.AsFloat)>0 then
          taSpecDocPriceUch.AsFloat:=RoundEx(quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat*100)/100;
        taSpecDocSumUch.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
        taSpecDocIdNds.AsInteger:=0;
        taSpecDocSumNds.AsFloat:=0;
        taSpecDocNameC.AsString:=Copy(quSpecInvNAME.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInvNAMESHORT.AsString;
        taSpecDocIdM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
        taSpecDocKm.AsFloat:=quSpecInvKM.AsFloat;
        taSpecDocProcPrice.AsFloat:=quSpecInvNOCALC.AsInteger;
        taSpecDoc.Post;

        inc(iNum);

        quSpecInv.Next;
      end;

    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsInv.acPastExecute(Sender: TObject);
Var iNum:Integer;
    NameGr:String;
    IdGr:Integer;
begin
//��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocInv') then
        begin
          prAllViewOff;

          fmAddInv.Caption:='��������������: ����� ��������.';
          fmAddInv.cxTextEdit1.Text:=prGetNum(3,0);
          fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddInv.cxTextEdit1.Tag:=0; //������� ���������� ���������
          fmAddInv.cxDateEdit1.Date:=Date;
          fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then
          begin
            quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
            quMHAll.Active:=True;
          end;
          quMHAll.FullRefresh;

          fmAddInv.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddInv.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;


          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
            fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddInv.Label15.Caption:='��. ����: ';
            fmAddInv.Label15.Tag:=0;
          end;

          fmAddInv.cxLabel1.Enabled:=True;
          fmAddInv.cxLabel2.Enabled:=True;
          fmAddInv.cxLabel7.Enabled:=True;
          fmAddInv.cxLabel8.Enabled:=True;
          fmAddInv.cxLabel9.Enabled:=True;
          fmAddInv.cxButton1.Enabled:=True;
          fmAddInv.Label3.Enabled:=True;
          fmAddInv.Label4.Enabled:=True;
          fmAddInv.Label6.Enabled:=True;

          CloseTa(fmAddInv.taSpec);
          CloseTa(fmAddInv.taSpecC);
          CloseTa(taCalcB);

          taSpecDoc.First; iNum:=1;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin

              with fmAddInv do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iNum;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=0;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceUch.AsFloat:=0;
                taSpecSumUch.AsFloat:=0;
                taSpecQuantFact.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPriceInF.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSumInF.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPriceUchF.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSumUchF.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecKm.AsFloat:=taSpecDocKm.AsFloat;
                taSpecTCard.AsInteger:=prTypeTC(taSpecDocIdCard.AsInteger);
                prFindGroup(taSpecDocIdCard.AsInteger,NameGr,IdGr);
                taSpecNameGr.asString:=NameGr;
                taSpecId_Group.asInteger:=IdGr;
                taSpecNoCalc.AsInteger:=RoundEx(taSpecDocProcPrice.AsFloat);
                taSpec.Post;
                inc(iNum);
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddInv.Show;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsInv.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsInv);
end;

procedure TfmDocsInv.ViewDocsInvCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsInv.ColumnCount-1 do
  begin
    if ViewDocsInv.Columns[i].Name='ViewDocsInvIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF; 
end;

procedure TfmDocsInv.N4Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnInv := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsInv.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
