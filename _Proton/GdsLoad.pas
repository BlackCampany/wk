unit GdsLoad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls;

type
  TfmGdsLoad = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    GrLoad: TcxGrid;
    ViewLoad: TcxGridDBTableView;
    LevelLoad: TcxGridLevel;
    ViewLoadCode: TcxGridDBColumn;
    ViewLoadIDate: TcxGridDBColumn;
    ViewLoadId: TcxGridDBColumn;
    ViewLoadDateLoad: TcxGridDBColumn;
    ViewLoadTimeLoad: TcxGridDBColumn;
    ViewLoadCType: TcxGridDBColumn;
    ViewLoadPrice: TcxGridDBColumn;
    ViewLoadDisc: TcxGridDBColumn;
    ViewLoadPerson: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGdsLoad: TfmGdsLoad;

implementation

uses MT, Period1, Un1;

{$R *.dfm}

procedure TfmGdsLoad.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGdsLoad.FormCreate(Sender: TObject);
begin
  GrLoad.Align:=AlClient;
end;

procedure TfmGdsLoad.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewLoad);
end;

procedure TfmGdsLoad.SpeedItem2Click(Sender: TObject);
Var iDateB,iDateE:Integer;
begin
//������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
    iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMT do
    begin
      try
        ViewLoad.BeginUpdate;

        if ptGdsLoadSel.Active=False then ptGdsLoadSel.Active:=True;
//        ptGdsLoadSel.CancelRange;
        ptGdsLoadSel.SetRange([ViewLoad.Tag,iDateB],[ViewLoad.Tag,iDateE]);

      finally
        ViewLoad.EndUpdate;
      end;
    end;
  end;
end;

end.
