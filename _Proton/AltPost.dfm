object fmAltPost: TfmAltPost
  Left = 490
  Top = 303
  Width = 539
  Height = 417
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' "'#1057#1074#1103#1079#1072#1085#1085#1099#1077' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1080'"'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridAltPost: TcxGrid
    Left = 0
    Top = 0
    Width = 531
    Height = 340
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewAltPost: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmP.dsquAltPost
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object ViewAltPostIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090'.'
        DataBinding.FieldName = 'IDCLI'
        Visible = False
        Width = 62
      end
      object ViewAltPostName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'Name'
        Width = 246
      end
      object ViewAltPostFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1089#1090'.'
        DataBinding.FieldName = 'FullName'
        Visible = False
        Width = 204
      end
      object ViewAltPostIDALTCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1089#1074#1103#1079'. '#1087#1086#1089#1090'.'
        DataBinding.FieldName = 'IDALTCLI'
        Visible = False
      end
      object ViewAltPostAName: TcxGridDBColumn
        Caption = #1057#1074#1103#1079#1072#1085#1085#1099#1081' '#1087#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'AName'
        Width = 259
      end
      object ViewAltPostAFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1089#1074#1103#1079'. '#1087#1086#1089#1090'.'
        DataBinding.FieldName = 'AFullName'
        Visible = False
        Width = 205
      end
    end
    object LevelAltPost: TcxGridLevel
      GridView = ViewAltPost
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 343
    Width = 531
    Height = 41
    Align = alBottom
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 201
      Top = 7
      Width = 145
      Height = 25
      Caption = #1042#1067#1061#1054#1044
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 148
    Top = 56
  end
end
