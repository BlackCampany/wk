unit RepRDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmRDayVed = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    dsRDayVed: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepRDay: TfrReport;
    frtaRDayVed: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    teRDayVed: TdxMemData;
    teRDayVedIdCode1: TIntegerField;
    teRDayVedNameC: TStringField;
    teRDayVediM: TSmallintField;
    teRDayVedIdGroup: TIntegerField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teRDayVedNameGr1: TStringField;
    teRDayVedNameGr2: TStringField;
    teRDayVedIdSGroup: TIntegerField;
    teRDayVediBrand: TIntegerField;
    teRDayVediCat: TSmallintField;
    amRepDayOb: TActionManager;
    acMoveRepOb: TAction;
    teRDayVedNameGr3: TStringField;
    teRDayVedQR: TFloatField;
    teRDayVedPrice: TFloatField;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teRDayVedAvrSpeed: TFloatField;
    teRDayVedQRDay: TFloatField;
    teRDayVedsTop: TStringField;
    teRDayVedsNov: TStringField;
    ViewRDayVed: TcxGridDBTableView;
    LevelRDayVed: TcxGridLevel;
    GridRDayVed: TcxGrid;
    ViewRDayVedIdCode1: TcxGridDBColumn;
    ViewRDayVedNameC: TcxGridDBColumn;
    ViewRDayVediM: TcxGridDBColumn;
    ViewRDayVedQR: TcxGridDBColumn;
    ViewRDayVedNameGr1: TcxGridDBColumn;
    ViewRDayVedNameGr2: TcxGridDBColumn;
    ViewRDayVedNameGr3: TcxGridDBColumn;
    ViewRDayVediCat: TcxGridDBColumn;
    ViewRDayVedPrice: TcxGridDBColumn;
    ViewRDayVedAvrSpeed: TcxGridDBColumn;
    ViewRDayVedQRDay: TcxGridDBColumn;
    ViewRDayVedsTop: TcxGridDBColumn;
    ViewRDayVedsNov: TcxGridDBColumn;
    teRDayVedDayN: TIntegerField;
    ViewRDayVedDayN: TcxGridDBColumn;
    teRDayVedD0: TFloatField;
    teRDayVedD1: TFloatField;
    teRDayVedD2: TFloatField;
    teRDayVedD3: TFloatField;
    teRDayVedD4: TFloatField;
    teRDayVedD5: TFloatField;
    teRDayVedD6: TFloatField;
    teRDayVedD7: TFloatField;
    teRDayVedD8: TFloatField;
    teRDayVedD9: TFloatField;
    ViewRDayVedD0: TcxGridDBColumn;
    ViewRDayVedD1: TcxGridDBColumn;
    ViewRDayVedD2: TcxGridDBColumn;
    ViewRDayVedD3: TcxGridDBColumn;
    ViewRDayVedD4: TcxGridDBColumn;
    ViewRDayVedD5: TcxGridDBColumn;
    ViewRDayVedD6: TcxGridDBColumn;
    ViewRDayVedD7: TcxGridDBColumn;
    ViewRDayVedD8: TcxGridDBColumn;
    ViewRDayVedD9: TcxGridDBColumn;
    teRDayVedQremVoz: TFloatField;
    ViewRDayVedQRemVoz: TcxGridDBColumn;
    teRDayVedQNVoz: TFloatField;
    ViewRDayVedQNVoz: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewRDayVedSelectionChanged(Sender: TcxCustomGridTableView);
    procedure teRDayVedCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ptDaysCols(bSt:Boolean);

  end;

var
  fmRDayVed: TfmRDayVed;

implementation

uses Un1, MDB, MT, Move, dmPS;

{$R *.dfm}

procedure TfmRDayVed.ptDaysCols(bSt:Boolean);
Var dDate:TDateTime;
begin
  ViewRDayVedD0.Visible:=bSt;
  ViewRDayVedD1.Visible:=bSt;
  ViewRDayVedD2.Visible:=bSt;
  ViewRDayVedD3.Visible:=bSt;
  ViewRDayVedD4.Visible:=bSt;
  ViewRDayVedD5.Visible:=bSt;
  ViewRDayVedD6.Visible:=bSt;
  ViewRDayVedD7.Visible:=bSt;
  ViewRDayVedD8.Visible:=bSt;
  ViewRDayVedD9.Visible:=bSt;

  if bSt then
  begin
    dDate:=Date;

    ViewRDayVedD9.Caption:=ds1(dDate-1);
    if dw(dDate-1)>5
      then ViewRDayVedD9.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD9.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD8.Caption:=ds1(dDate-2);
    if dw(dDate-2)>5
      then ViewRDayVedD8.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD8.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD7.Caption:=ds1(dDate-3);
    if dw(dDate-3)>5
      then ViewRDayVedD7.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD7.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD6.Caption:=ds1(dDate-4);
    if dw(dDate-4)>5
      then ViewRDayVedD6.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD6.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD5.Caption:=ds1(dDate-5);
    if dw(dDate-5)>5
      then ViewRDayVedD5.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD5.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD4.Caption:=ds1(dDate-6);
    if dw(dDate-6)>5
      then ViewRDayVedD4.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD4.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD3.Caption:=ds1(dDate-7);
    if dw(dDate-7)>5
      then ViewRDayVedD3.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD3.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD2.Caption:=ds1(dDate-8);
    if dw(dDate-8)>5
      then ViewRDayVedD2.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD2.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD1.Caption:=ds1(dDate-9);
    if dw(dDate-9)>5
      then ViewRDayVedD1.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD1.Styles.Content:=dmMC.cxStyle1;
    ViewRDayVedD0.Caption:=ds1(dDate-10);
    if dw(dDate-10)>5
      then ViewRDayVedD0.Styles.Content:=dmMC.cxStyle25
      else ViewRDayVedD0.Styles.Content:=dmMC.cxStyle1;
  end;
end;


procedure TfmRDayVed.FormCreate(Sender: TObject);
begin
  GridRDayVed.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;

  ViewRDayVed.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+Person.NameUser+'\'+GridIni);
end;

procedure TfmRDayVed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  teRDayVed.Active:=False;
  ViewRDayVed.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+Person.NameUser+'\'+GridIni,False);
end;

procedure TfmRDayVed.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRDayVed.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRDayVed.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewRDayVed);
end;

procedure TfmRDayVed.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewRDayVed.BeginUpdate;
  teRDayVed.Filter:=ViewRDayVed.DataController.Filter.FilterText;
  teRDayVed.Filtered:=True;

  frRepRDay.LoadFromFile(CurDir + 'RVed.frf');

  frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

//  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewRDayVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepRDay.ReportName:='��������� ���������.';
  frRepRDay.PrepareReport;
  frRepRDay.ShowPreparedReport;

  teRDayVed.Filter:='';
  teRDayVed.Filtered:=False;
  ViewRDayVed.EndUpdate;
end;

procedure TfmRDayVed.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepRVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmRDayVed.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRDayVed.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmRDayVed.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewRDayVed.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teRDayVedIdCode1.AsInteger;
      prFillMove(teRDayVedIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teRDayVedNameC.AsString+'('+teRDayVedIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmRDayVed.ViewRDayVedSelectionChanged(Sender: TcxCustomGridTableView);
begin
  if ViewRDayVed.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teRDayVedIdCode1.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmRDayVed.teRDayVedCalcFields(DataSet: TDataSet);
begin
  with dmP do
  begin
    quAllRem.Active:=false;
    quAllRem.ParamByName('CODE').AsInteger:= teRDayVedIdCode1.AsInteger;
    quAllRem.Active:=true;
    teRDayVedQremVoz.AsFloat:=quAllRemQRem.AsFloat;
    quAllRem.Active:=false;
    teRDayVedQNVoz.AsFloat:=teRDayVedQR.AsFloat-teRDayVedQremVoz.AsFloat;
  end;
end;

end.
