unit Passw;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGraphics;

type
  TfmPerA = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TcxLookupComboBox;
    Edit1: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Enter(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPerA: TfmPerA;
  CountEnter:Integer;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmPerA.FormCreate(Sender: TObject);
begin
  Left:=1;
  Top:=1;
  CountEnter:=0;
end;

procedure TfmPerA.Edit1Enter(Sender: TObject);
begin
  Text:='';
end;

procedure TfmPerA.Button1Click(Sender: TObject);
Var StrWk:String;
begin
  strwk:=Edit1.Text;
  with dmC do
  begin
    quPer.Locate('ID',ComboBox1.EditValue,[]);
    if quPerCheck.AsString=Edit1.Text then
    begin //������� ������
      Person.Id:=quPerId.AsInteger;
      Person.Name:=quPerName.AsString;
      quPer.Active:=False;
      ModalResult:=1;
    end
    else
    begin
      inc(CountEnter);
      if CountEnter>2 then close;
      showmessage('������ ������������. �������� '+IntToStr(3-CountEnter)+' �������.');
      Edit1.Text:='';
      Edit1.SetFocus;
    end;  
  end;
end;

procedure TfmPerA.Button2Click(Sender: TObject);
begin
  dmC.quPer.Active:=False;
  Close;
end;

procedure TfmPerA.FormShow(Sender: TObject);
begin
  with dmC do
  begin
    if CasherRnDb.Connected then
    begin
      quPer.Active:=True;
      ComboBox1.EditValue:=Person.Id;
      StatusBar1.Panels[0].Text:=Btr1Path;
      Edit1.SetFocus;
      Edit1.SelectAll;
    end;
  end;
end;

end.
