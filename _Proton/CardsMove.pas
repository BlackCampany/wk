unit CardsMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxCalendar, ActnList, XPStyleActnCtrls, ActnMan, StdCtrls, Placemnt,
  cxImageComboBox, DBClient, FR_DSet, FR_DBSet, FR_Class, dxmdaset;

type
  TfmCardsMove = class(TForm)
    PageControl1: TPageControl;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    ViewPartIn: TcxGridDBTableView;
    LevelPartIn: TcxGridLevel;
    GridPartIn: TcxGrid;
    GridPartOut: TcxGrid;
    ViewPartOut: TcxGridDBTableView;
    LevelPartOut: TcxGridLevel;
    GridMove: TcxGrid;
    ViewMove: TcxGridDBTableView;
    LevelMove: TcxGridLevel;
    ViewPartInID: TcxGridDBColumn;
    ViewPartInNAMEMH: TcxGridDBColumn;
    ViewPartInIDDOC: TcxGridDBColumn;
    ViewPartInNUMDOC: TcxGridDBColumn;
    ViewPartInARTICUL: TcxGridDBColumn;
    ViewPartInIDCLI: TcxGridDBColumn;
    ViewPartInNAMECL: TcxGridDBColumn;
    ViewPartInDTYPE: TcxGridDBColumn;
    ViewPartInQPART: TcxGridDBColumn;
    ViewPartInQREMN: TcxGridDBColumn;
    ViewPartInPRICEIN: TcxGridDBColumn;
    ViewPartInPRICEOUT: TcxGridDBColumn;
    ViewPartInIDATE: TcxGridDBColumn;
    ViewPartInSUMIN: TcxGridDBColumn;
    ViewPartInSUMREMN: TcxGridDBColumn;
    ViewPartOutARTICUL: TcxGridDBColumn;
    ViewPartOutIDDATE: TcxGridDBColumn;
    ViewPartOutIDSTORE: TcxGridDBColumn;
    ViewPartOutNAMEMH: TcxGridDBColumn;
    ViewPartOutIDPARTIN: TcxGridDBColumn;
    ViewPartOutIDDOC: TcxGridDBColumn;
    ViewPartOutNUMDOC: TcxGridDBColumn;
    ViewPartOutIDCLI: TcxGridDBColumn;
    ViewPartOutNAMECL: TcxGridDBColumn;
    ViewPartOutDTYPE: TcxGridDBColumn;
    ViewPartOutQUANT: TcxGridDBColumn;
    ViewPartOutPRICEIN: TcxGridDBColumn;
    ViewPartOutSUMIN: TcxGridDBColumn;
    amCM: TActionManager;
    acExit: TAction;
    acPeriod: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Edit1: TEdit;
    ViewMoveARTICUL: TcxGridDBColumn;
    ViewMoveIDATE: TcxGridDBColumn;
    ViewMoveIDSTORE: TcxGridDBColumn;
    ViewMoveNAMEMH: TcxGridDBColumn;
    ViewMoveRB: TcxGridDBColumn;
    ViewMovePOSTIN: TcxGridDBColumn;
    ViewMovePOSTOUT: TcxGridDBColumn;
    ViewMoveVNIN: TcxGridDBColumn;
    ViewMoveVNOUT: TcxGridDBColumn;
    ViewMoveINV: TcxGridDBColumn;
    ViewMoveQREAL: TcxGridDBColumn;
    ViewMoveRE: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    TabSheet4: TTabSheet;
    GridRealC: TcxGrid;
    ViewRealC: TcxGridDBTableView;
    LevelRealC: TcxGridLevel;
    ViewRealCDATEDOC: TcxGridDBColumn;
    ViewRealCCODEB: TcxGridDBColumn;
    ViewRealCNAMEB: TcxGridDBColumn;
    ViewRealCQUANT: TcxGridDBColumn;
    ViewRealCPRICEOUT: TcxGridDBColumn;
    ViewRealCSUMOUT: TcxGridDBColumn;
    ViewRealCQUANTC: TcxGridDBColumn;
    ViewRealCPRICEIN: TcxGridDBColumn;
    ViewRealCSUMIN: TcxGridDBColumn;
    ViewRealCSM: TcxGridDBColumn;
    ViewRealCQB: TcxGridDBColumn;
    acSelDoc: TAction;
    SpeedItem3: TSpeedItem;
    taCardMoveRep: TClientDataSet;
    taCardMoveRepNum: TIntegerField;
    taCardMoveRepsDate: TStringField;
    taCardMoveRepDocNum: TStringField;
    taCardMoveRepClient: TStringField;
    taCardMoveRepQuantIn: TFloatField;
    taCardMoveRepSumIn: TFloatField;
    taCardMoveRepQuantOut: TFloatField;
    taCardMoveRepSumOut: TFloatField;
    acPrint: TAction;
    SpeedItem4: TSpeedItem;
    RepCMove: TfrReport;
    frtaCardMoveRep: TfrDBDataSet;
    taCardMoveRepDocType: TStringField;
    acExportXl: TAction;
    SpeedItem5: TSpeedItem;
    TabSheet5: TTabSheet;
    GrCardM: TcxGrid;
    ViCardM: TcxGridDBTableView;
    LevelCardM: TcxGridLevel;
    ViCardMRecId: TcxGridDBColumn;
    ViCardMTypeD: TcxGridDBColumn;
    ViCardMDateD: TcxGridDBColumn;
    ViCardMsFrom: TcxGridDBColumn;
    ViCardMsTo: TcxGridDBColumn;
    ViCardMsM: TcxGridDBColumn;
    ViCardMQuant: TcxGridDBColumn;
    ViCardMQRemn: TcxGridDBColumn;
    ViCardMComment: TcxGridDBColumn;
    ViewPartInPRICEIN0: TcxGridDBColumn;
    ViewPartInSUMIN0: TcxGridDBColumn;
    LevelRealB: TcxGridLevel;
    ViewRealB: TcxGridDBTableView;
    ViewRealBDATEDOC: TcxGridDBColumn;
    ViewRealBNUMDOC: TcxGridDBColumn;
    ViewRealBCODEB: TcxGridDBColumn;
    ViewRealBNAMEB: TcxGridDBColumn;
    ViewRealBQUANT: TcxGridDBColumn;
    ViewRealBPRICEOUT: TcxGridDBColumn;
    ViewRealBSUMOUT: TcxGridDBColumn;
    ViewRealBSUMIN: TcxGridDBColumn;
    ViewRealBRNAC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSelDocExecute(Sender: TObject);
    procedure ViewPartInDblClick(Sender: TObject);
    procedure ViewPartOutDblClick(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acExportXlExecute(Sender: TObject);
    procedure ViCardMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViCardMDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure prGoto(iTypeD,IdDoc,iDate:Integer);

var
  fmCardsMove: TfmCardsMove;

implementation

uses dmOffice, SelPerSkl, Un1, DMOReps, DocsIn, DocInv, DocsVn, ActPer,
  DocCompl, DocsOut, DocOutR, DocOutB;

{$R *.dfm}

procedure prGoto(iTypeD,IdDoc,iDate:Integer);
Var DateB,DateE:TDateTime;
begin
//������� �� ��������
  with dmO do
  with dmORep do
  begin
    DateB:=iDate;
    DateE:=iDate+1;
    if iTypeD=1 then
    begin
      fmDocsIn.Caption:='��������� ��������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsIn.ViewDocsIn.BeginUpdate;

      quDocsInSel.Active:=False;
      quDocsInSel.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsInSel.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsInSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsInSel.Active:=True;

      quDocsInSel.Locate('ID',IdDoc,[]);

      fmDocsIn.LevelDocsIn.Visible:=True;
      fmDocsIn.LevelCards.Visible:=False;
      fmDocsIn.SpeedItem3.Visible:=True;
      fmDocsIn.SpeedItem4.Visible:=True;
      fmDocsIn.SpeedItem5.Visible:=True;
      fmDocsIn.SpeedItem6.Visible:=True;
      fmDocsIn.SpeedItem7.Visible:=True;
      fmDocsIn.SpeedItem8.Visible:=True;

      fmDocsIn.ViewDocsIn.EndUpdate;

      fmDocsIn.Show;
    end;
    if iTypeD=3 then //��������������
    begin
      fmDocsInv.Caption:='�������������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);

      fmDocsInv.ViewDocsInv.BeginUpdate;

      quDocsInvSel.Active:=False;
      quDocsInvSel.ParamByName('DATEB').AsDate:=DateB;
      quDocsInvSel.ParamByName('DATEE').AsDate:=DateE;
      quDocsInvSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsInvSel.Active:=True;
      quDocsInvSel.Locate('ID',IdDoc,[]);

      fmDocsInv.LevelDocsInv.Visible:=True;
      fmDocsInv.LevelCardsInv.Visible:=False;
      fmDocsInv.SpeedItem3.Visible:=True;
      fmDocsInv.SpeedItem4.Visible:=True;
      fmDocsInv.SpeedItem5.Visible:=True;
      fmDocsInv.SpeedItem6.Visible:=True;
      fmDocsInv.SpeedItem7.Visible:=True;
      fmDocsInv.SpeedItem8.Visible:=True;

      fmDocsInv.ViewDocsInv.EndUpdate;

      fmDocsInv.Show;
    end;
    if iTypeD=4 then
    begin
      fmDocsVn.Caption:='���������� ��������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsVn.ViewDocsVn.BeginUpdate;

      quDocsVnSel.Active:=False;
      quDocsVnSel.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsVnSel.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsVnSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsVnSel.ParamByName('IDPERSON1').AsInteger:=Person.Id;
      quDocsVnSel.Active:=True;

      quDocsVnSel.Locate('ID',IdDoc,[]);

      fmDocsVn.LevelDocsVn.Visible:=True;
      fmDocsVn.LevelCardsVn.Visible:=False;
      fmDocsVn.SpeedItem3.Visible:=True;
      fmDocsVn.SpeedItem4.Visible:=True;
      fmDocsVn.SpeedItem5.Visible:=True;
      fmDocsVn.SpeedItem6.Visible:=True;
      fmDocsVn.SpeedItem7.Visible:=True;
      fmDocsVn.SpeedItem8.Visible:=True;

      fmDocsVn.ViewDocsVn.EndUpdate;

      fmDocsVn.Show;
    end;
    if iTypeD=5 then
    begin
      fmDocsActs.Caption:='���� ����������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsActs.ViewActs.BeginUpdate;

      quDocsActs.Active:=False;
      quDocsActs.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsActs.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsActs.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsActs.Active:=True;

      quDocsActs.Locate('ID',IdDoc,[]);

      fmDocsActs.LevelActs.Visible:=True;
      fmDocsActs.LevelCardsActs.Visible:=False;
      fmDocsActs.SpeedItem3.Visible:=True;
      fmDocsActs.SpeedItem4.Visible:=True;
      fmDocsActs.SpeedItem5.Visible:=True;
      fmDocsActs.SpeedItem6.Visible:=True;
      fmDocsActs.SpeedItem7.Visible:=True;
      fmDocsActs.SpeedItem8.Visible:=True;

      fmDocsActs.ViewActs.EndUpdate;

      fmDocsActs.Show;
    end;
    if iTypeD=6 then
    begin
      fmDocsCompl.Caption:='������������ ���� (���� ����) '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsCompl.ViewComplB.BeginUpdate;

      quDocsCompl.Active:=False;
      quDocsCompl.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsCompl.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsCompl.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsCompl.ParamByName('IDPERSON1').AsInteger:=Person.Id;
      quDocsCompl.Active:=True;

      quDocsCompl.Locate('ID',IdDoc,[]);

      fmDocsCompl.ViewComplB.EndUpdate;

      fmDocsCompl.Show;
    end;

    if iTypeD=7 then
    begin
      fmDocsOut.Caption:='�������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsOut.ViewDocsOut.BeginUpdate;

      quDocsOutSel.Active:=False;
      quDocsOutSel.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsOutSel.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsOutSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsOutSel.Active:=True;

      quDocsOutSel.Locate('ID',IdDoc,[]);

      fmDocsOut.ViewDocsOut.EndUpdate;

      fmDocsOut.Show;
    end;
    if iTypeD=2 then
    begin
      fmDocsOutB.Caption:='���������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsOutB.ViewDocsOutB.BeginUpdate;

      quDocsOutB.Active:=False;
      quDocsOutB.ParamByName('DATEB').AsDate:=Trunc(DateB);
      quDocsOutB.ParamByName('DATEE').AsDate:=Trunc(DateE);
      quDocsOutB.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsOutB.Active:=True;

      quDocsOutB.Locate('ID',IdDoc,[]);

      fmDocsOutB.ViewDocsOutB.EndUpdate;

      fmDocsOutB.Show;
    end;
    if iTypeD=8 then
    begin
      fmDocsReal.Caption:='���������� �� ������� �� '+FormatDateTiMe('dd.mm.yyyy',DateB);
      fmDocsReal.ViewDocsR.BeginUpdate;
      prFormQuDocsR;
      quDocsRSel.Locate('ID',IdDoc,[]);
      fmDocsReal.ViewDocsR.EndUpdate;

      fmDocsReal.Show;
    end;
  end;
end;

procedure TfmCardsMove.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=AlClient;
  PageControl1.ActivePageIndex:=2;
  ViewPartIn.RestoreFromIniFile(CurDir+GridIni);
  ViewPartOut.RestoreFromIniFile(CurDir+GridIni);
  ViewMove.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmCardsMove.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmO do
  with dmORep do
  begin
    quCPartIn.Active:=False;
    quCPartOut.Active:=False;
    quCMove.Active:=False;
    taCMove.Close;
    taCardMoveRep.Close;
  end;
  ViewPartIn.StoreToIniFile(CurDir+GridIni,False);
  ViewPartOut.StoreToIniFile(CurDir+GridIni,False);
  ViewMove.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmCardsMove.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmCardsMove.acPeriodExecute(Sender: TObject);
Var iDateB,iDateE:INteger;
begin
  //�� ������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
    else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
//    Label3.Visible:=False;
//    cxLookupComboBox1.Visible:=False;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
//    if fmSelPerSkl.cxRadioButton1.Checked then iType:=0 else iType:=1;

    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
    CommonSet.DateFrom:=iDateB;
    CommonSet.DateTo:=iDateE+1;

    prPreShowMove(fmCardsMove.Tag,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,fmCardsMove.Edit1.Text);
  end;
  fmSelPerSkl.Release;
end;

procedure TfmCardsMove.acSelDocExecute(Sender: TObject);
begin
  //������� �� ��������
  with dmO do
  with dmORep do
  begin
    if PageControl1.ActivePageIndex=0 then
    begin //������
      if quCPartIn.RecordCount>0 then
        prGoto(quCPartInDTYPE.AsInteger,quCPartInIDDOC.AsInteger,quCPartInIDATE.AsInteger);
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      if quCPartOut.RecordCount>0 then
        prGoto(quCPartOutDTYPE.AsInteger,quCPartOutIDDOC.AsInteger,quCPartOutIDDATE.AsInteger);
    end;
    if PageControl1.ActivePageIndex=4 then
    begin
      if taCardM.RecordCount>0 then
        prGoto(taCardMTypeD.AsInteger,taCardMIdDoc.AsInteger,Trunc(taCardMDateD.AsDateTime));
    end;
  end;
end;

procedure TfmCardsMove.ViewPartInDblClick(Sender: TObject);
begin
//  acSelDoc.Execute;
  with dmO do
  begin
    if quCPartIn.RecordCount>0 then
    begin
      prGoto(quCPartInDTYPE.AsInteger,quCPartInIDDOC.AsInteger,quCPartInIDATE.AsInteger);
    end;
  end;
end;

procedure TfmCardsMove.ViewPartOutDblClick(Sender: TObject);
begin
//  acSelDoc.Execute;
  with dmO do
  begin
    if quCPartOut.RecordCount>0 then
    begin
      prGoto(quCPartOutDTYPE.AsInteger,quCPartOutIDDOC.AsInteger,quCPartOutIDDATE.AsInteger);
    end;
  end;
end;

procedure TfmCardsMove.acPrintExecute(Sender: TObject);
Var iDateB,iDateE,iCurDate,iMax:Integer;
    sDoc:String;
begin
//������ ��������
  with dmO do
  with dmORep do
  begin
    iDateB:=0;
    iDateE:=0;
    CloseTa(taCardMoveRep);
    iMax:=1;

    ViewPartIn.BeginUpdate;
    ViewPartOut.BeginUpdate;
    ViewMove.BeginUpdate;

    if quCPartIn.RecordCount>0 then
    begin
      quCPartIn.First;
      iDateB:=quCPartInIDATE.AsInteger;
      quCPartIn.Last;
      iDateE:=quCPartInIDATE.AsInteger;
      quCPartIn.First;
    end;
    if quCPartOut.RecordCount>0 then
    begin
      quCPartOut.First;
      if iDateB>quCPartOutIDDATE.AsInteger then
      begin
        iDateB:=quCPartOutIDDATE.AsInteger;
      end;
      quCPartOut.Last;
      if iDateE<quCPartOutIDDATE.AsInteger then
      begin
        iDateE:=quCPartOutIDDATE.AsInteger;
      end;
      quCPartOut.First;
    end;


    for iCurDate:=iDateB to iDateE do
    begin
      while (quCPartInIDATE.AsInteger<=iCurDate)and(quCPartIn.Eof=False) do
      begin  //������
        sDoc:='';
        if quCPartInDTYPE.AsInteger=1 then sDoc:='����.����.';
        if quCPartInDTYPE.AsInteger=2 then sDoc:='����������';
        if quCPartInDTYPE.AsInteger=3 then sDoc:='������.';
        if quCPartInDTYPE.AsInteger=4 then sDoc:='��.����.';
        if quCPartInDTYPE.AsInteger=5 then sDoc:='���.�������.';
        if quCPartInDTYPE.AsInteger=6 then sDoc:='��������.';
        if quCPartInDTYPE.AsInteger=7 then sDoc:='�������.';
        if quCPartInDTYPE.AsInteger=8 then sDoc:='����. �������';
        if quCPartInDTYPE.AsInteger=9 then sDoc:='��������';

        taCardMoveRep.Append;
        taCardMoveRepNum.AsInteger:=iMax;
        taCardMoveRepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quCPartInIDATE.AsInteger);
        taCardMoveRepDocNum.AsString:=quCPartInNUMDOC.AsString;
        taCardMoveRepClient.AsString:=quCPartInNAMECL.AsString;
        taCardMoveRepQuantIn.AsFloat:=quCPartInQPART.AsFloat;
        taCardMoveRepSumIn.AsFloat:=quCPartInSUMIN.AsFloat;
        taCardMoveRepDocType.AsString:=sDoc;
        taCardMoveRepQuantOut.AsFloat:=0;
        taCardMoveRepSumOut.AsFloat:=0;
        taCardMoveRep.Post; inc(iMax);

        quCPartIn.Next;
      end;
      while (quCPartOutIDDATE.AsInteger<=iCurDate)and(quCPartOut.Eof=False) do
      begin  //������
        sDoc:='';
        if quCPartOutDTYPE.AsInteger=1 then sDoc:='����.����.';
        if quCPartOutDTYPE.AsInteger=2 then sDoc:='����������';
        if quCPartOutDTYPE.AsInteger=3 then sDoc:='������.';
        if quCPartOutDTYPE.AsInteger=4 then sDoc:='��.����.';
        if quCPartOutDTYPE.AsInteger=5 then sDoc:='���.�������.';
        if quCPartOutDTYPE.AsInteger=6 then sDoc:='��������.';
        if quCPartOutDTYPE.AsInteger=7 then sDoc:='�������.';
        if quCPartOutDTYPE.AsInteger=8 then sDoc:='����. �������';
        if quCPartOutDTYPE.AsInteger=9 then sDoc:='��������';

        taCardMoveRep.Append;
        taCardMoveRepNum.AsInteger:=iMax;
        taCardMoveRepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quCPartOutIDDATE.AsInteger);
        taCardMoveRepDocNum.AsString:=quCPartOutNUMDOC.AsString;
        taCardMoveRepClient.AsString:=quCPartOutNAMECL.AsString;
        taCardMoveRepQuantIn.AsFloat:=0;
        taCardMoveRepSumIn.AsFloat:=0;
        taCardMoveRepDocType.AsString:=sDoc;
        taCardMoveRepQuantOut.AsFloat:=quCPartOutQUANT.AsFloat;
        taCardMoveRepSumOut.AsFloat:=quCPartOutSUMIN.AsFloat;
        taCardMoveRep.Post; inc(iMax);

        quCPartOut.Next;
      end;
    end;

    iDateE:=Trunc(CommonSet.DateTo-1);
    if (iDateE-Date)>1000 then iDateE:=Trunc(Date);

    RepCMove.LoadFromFile(CurDir + 'CardMove.frf');

    frVariables.Variable['sPeriod']:='������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE);
    frVariables.Variable['Skl']:=CommonSet.NameStore;
    frVariables.Variable['NameC']:=CardSel.Name;
    frVariables.Variable['SM']:=CardSel.mesuriment;
    taCMove.First;
    frVariables.Variable['QBeg']:=taCMoveRB.AsFloat;
    taCMove.Last;
    frVariables.Variable['QEnd']:=taCMoveRE.AsFloat;

    ViewPartIn.EndUpdate;
    ViewPartOut.EndUpdate;
    ViewMove.EndUpdate;

    RepCMove.ReportName:='��������� �� ���������.';
    RepCMove.PrepareReport;
    RepCMove.ShowPreparedReport;

  end;
end;

procedure TfmCardsMove.acExportXlExecute(Sender: TObject);
begin
//
  if PageControl1.ActivePageIndex=0 then  prNExportExel5(ViewPartIn);
  if PageControl1.ActivePageIndex=1 then  prNExportExel5(ViewPartOut);
  if PageControl1.ActivePageIndex=2 then  prNExportExel5(ViewMove);
  if PageControl1.ActivePageIndex=3 then
  begin
    if LevelRealC.Visible then prNExportExel5(ViewRealC);
    if LevelRealB.Visible then prNExportExel5(ViewRealB);
  end;
  if PageControl1.ActivePageIndex=4 then  prNExportExel5(ViCardM);
end;

procedure TfmCardsMove.ViCardMCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    rDif:Real;

begin
//  if AViewInfo.GridRecord.Selected then exit;

  rDif:=0;
  for i:=0 to ViCardM.ColumnCount-1 do
  begin
    if ViCardM.Columns[i].Name='ViCardMQuant' then
    begin
      rDif:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varDouble);
      break;
    end;
  end;

  if rDif>0  then
  begin
    ACanvas.Canvas.Brush.Color := $00C0DCC1;
//    ACanvas.Canvas.Font.Color := clGray;
  end;

{  if rDif<0  then
  begin
    ACanvas.Canvas.Brush.Color := $00ECD9FF;
  end;
}
end;

procedure TfmCardsMove.ViCardMDblClick(Sender: TObject);
begin
//  acSelDoc.Execute;
  with dmORep do
  begin
    if taCardM.RecordCount>0 then
      prGoto(taCardMTypeD.AsInteger,taCardMIdDoc.AsInteger,Trunc(taCardMDateD.AsDateTime));
  end;
end;

end.
