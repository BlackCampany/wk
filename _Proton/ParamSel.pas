unit ParamSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxCheckBox;

type
  TfmParamSel = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxCheckBox1: TcxCheckBox;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmParamSel: TfmParamSel;

implementation

uses ClassSel, Un1;

{$R *.dfm}

procedure TfmParamSel.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  fmClassSel:=TfmClassSel.Create(Application);
  fmClassSel.ShowModal;
  if fmClassSel.ModalResult=mrOk then
  begin
    cxButtonEdit1.Text:=fmClassSel.ClassTree.Selected.Text;
    cxButtonEdit1.Tag:=Integer(fmClassSel.ClassTree.Selected.Data);

    fmClassSel.Release;

  end else
    fmClassSel.Release;
end;

end.
