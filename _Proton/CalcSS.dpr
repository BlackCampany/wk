program CalcSS;

uses
  Forms,
  MainSS in 'MainSS.pas' {fmManager},
  MT in 'MT.pas' {dmMT: TDataModule},
  Un1 in 'Un1.pas',
  MDB in 'MDB.pas' {dmMC: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmManager, fmManager);
  Application.CreateForm(TdmMT, dmMT);
  Application.CreateForm(TdmMC, dmMC);
  Application.Run;
end.
