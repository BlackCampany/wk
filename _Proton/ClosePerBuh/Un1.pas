unit Un1;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ADODB, Variants, IdGlobal, ComCtrls, EasyCompression,
  cxGridDBTableView,DB,DBClient,ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  cxCustomData, dxmdaset, pvtables, pvsqltables, btvtables, sqldataset, cxMemo,WinSpool,cxGridCustomTableView;


procedure Delay(MSecs: Longint);
Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
Procedure RefreshTree(Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
procedure WriteHistory(Strwk_: string);
procedure WriteLog(Strwk_: string);
procedure prWH(Strwk_: string;Memo1:TcxMemo);


Procedure ReadIni;
Procedure WriteIni;

function RoundEx( X: Double ): Integer;
function RoundVal( X: Double ): Double;
function RoundHi( X: Double ): Integer; //���������� � ������� ������� ������
function RV( X: Double ): Double;

procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
procedure prExportExel2(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tADOQuery);

procedure prNExportExel3(Vi:tcxGridDBTableView);
procedure prNExportExel4(Vi:tcxGridDBTableView);
procedure prNExportExel5(Vi:tcxGridDBTableView);

Procedure prCreateVi(ta:TClientDataSet;V:TcxGridDBTableView);

function R1000( X: Double ): Double;

function IsOLEObjectInstalled(Name: String): boolean;

//Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TPvQuery);
procedure CloseTa(taT:tClientDataSet);
procedure CloseTe(taT:tdxMemData);

procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);
procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
function fRToStr(r:Real):String;
Function fTestKey(Key:Word):Boolean;
Function fs(rSum:Real):String;
function ds(d:TDateTime):String;
Function its(i:Integer):String;
function dts(d:TDateTime):String;
function ts(d:TDateTime):String;

Function prOpenPrinter(PName:String):Boolean;
Procedure prWritePrinter(S:String);
Procedure prClosePrinter(PName:String);
function ToStr(rSum:Real):String;
function IntToIp(Ip:INteger):String;
function StrToIp(sIp:String):INteger;
function fmt:String;
Function ToStandart(S:String):String;


type TPerson = record
     Id:Integer;
     Name:String;
     Modul:String;
     end;

     TCommonSet = record
     PathExport:String;
     CashPath:String;
     TrfPath:String;
     TmpPath:String;
     NetPath:String;
     PathImport:String;
     FtpExport:String;
     FtpImport:String;
     PathHistory:String;
     PathArh:String;
     PeriodPing:INteger;
     PathCryst:String;
     iMin,iMax,iMinVes,iMaxVes:Integer;
     Prefix,PrefixVes:String;
     DirectToCryst:INteger;
     CountStart:INteger;
     HostIp:String;
     DateBeg,DateEnd:TDateTime;
     TrDocAutoStart:String;
     TrDocDay:Integer;
     TrDayCount:Integer;
     NumPost:Integer;
     TrStr:String;
     HostUser:String;
     HostPassw:string;
     Filial:INteger;
     CurNac:Real;
     TPrintN:String;
     TPrintCode:INteger;
     CloseDay:INteger;
     end;

     TPeriodPar = record
     Post,Shop,Group,SGroup,Brand,Categ:String;
     end;

     TObPar = record
     Post,Depart,Group,SGroup,Brand,Cat:String;
     PostIDs,DepartIDs,GroupIDs:String;
     iPostT,iPost,iDepart,iGroup,iSGroup,iBrand,iCat:INteger;
     end;

     TPostSet = record
     SMPTServ,Login,Passw:String;
     SendToAdr1,SendToAdr2:String;
     end;

     TPos = Record
     Id:INteger;
     IdGr:INteger;
     Name:String;
     end;

     TGm = Record
     DateM:TDateTime;
     QIn,QOut,QVnIn,QVnOut,QReal,QInv,QRemn:Real;
     rPr,rSum:Currency;
     end;

Var CurDir:String;
    Person:TPerson;
    sFormatDate:String = 'mm.dd.yyyy';
    sCrystDate:String = 'yyyy-mm-dd';
    sTimeDate:String = 'yyyy-mm-dd hh:nn:ss';
    sTime:String = 'hh:nn:ss';
    CommonSet:TCommonSet;
    Posp:TPos;
    iEdit:Integer = 0;
    bAdd:Boolean = False;
    bAddB:Boolean = False;
    FindVal:Integer = 1; //�� ��������
    bTransp:Boolean = False;
    Ftr:TextFile;
    bFtp:Boolean = False;
    bNotPr:Boolean = False;
    bFtpOk:Boolean = True;
    iCountSec:INteger;
    bClearStatusBar:Boolean = False;
    iCurGraf:ShortInt = 1;
    sCurPost:String;
    PostSet:TPostSet;
    PeriodPar:TPeriodPar;
    bRefreshCard:Boolean = False;
    tA:ShortInt = 1;
    bDrIn:Boolean = False;
    bDrOut:Boolean = False;
    iDirect:INteger = 0; //������ ���������, ��� ������ ���������� ����
                         // 2 - ���������� � ������������ �������
    bOpen:Boolean = False;
    iCol:Integer = 0;
    iColT:Integer = 0;
    ObPar:TObPar;

//    bPrintOpen: Boolean = False; //��� LPT
    PrintHandle:THandle; //��� LPT


Const CurIni:String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      CashNon:String = 'Cash.non';
      R:String = ';';
      P:String = ',';
      TrExt:String = '.cr~';
      levelValue: array [0..9] of TECLCompressionLevel =
              (eclNone, zlibFastest, zlibNormal, zlibMax, bzipFastest,
               bzipNormal, bzipMax, ppmFastest, ppmNormal, ppmMax);
      odInac:String = 'odNoFocusRect_';
      WarnMess:String = '����� �������� ��������� �������.';
      VerMess:String = 'Ver for Elisey (pr. Ivanchenco)';
      sMask:String = '192.168.0.';

implementation


Function ToStandart(S:String):String;
var StrWk,Str1:String;
    iBar: array[1..13] of integer;
    n,c,n1:Integer;
begin
  StrWk:=S;
  while Length(StrWk)<12 do StrWk:=StrWk+'0';
  for n:=1 to 12 do
  begin
    str1:=Copy(StrWk,n,1);
    iBar[n]:=StrToIntDef(Str1,0);
  end;
  //220123401000C
  c:=0;
  n:=(iBar[2]+iBar[4]+iBar[6]+iBar[8]+iBar[10]+iBar[12])*3+(iBar[1]+iBar[3]+iBar[5]+iBar[7]+iBar[9]+iBar[11]);
  for n1:=0 to 9 do
  begin
    if ((n+n1) mod 10)=0 then
    begin
      c:=n1;
      break;
    end;
  end;
  iBar[13]:=c;
  Str1:='';
  for n:=1 to 13 do str1:=Str1+IntToStr(iBar[n]);

  strwk:=Str1;
  Result:=StrWk;
end;


function fmt:String;
begin
  Result:=FormatDateTime('hh:nn:ss ',time);
end;

function StrToIp(sIp:String):INteger;
Var i:INteger;
    iP:INteger;
begin
  while length(sIp)<3 do sIp:='0'+sIp;
  Ip:=0;
  for i:=3 downto 1 do
  begin
    Ip:=Ip+ord(sIp[i]);
    Ip:=Ip shl 8;
  end;
  ip:=ip+3;
  Result:=Ip;
end;

function IntToIp(Ip:INteger):String;
Var B:Byte;
    Str:String;
begin
  Result:='';
  Str:='';
  try
    if Ip>0 then
    begin
      Ip:=Ip shr 8;
      b:=lo(Ip);
      Str:=Str+Char(b);
      Ip:=Ip shr 8;
      b:=lo(Ip);
      Str:=Str+Char(b);
      Ip:=Ip shr 8;
      b:=lo(Ip);
      Str:=Str+Char(b);
      Result:=Str;
    end;
  except
    Result:='';
  end;
end;

function ToStr(rSum:Real):String;
Var StrWk:String;
begin
  StrWk:='';
  Str(rSum:12:2,StrWk);
  trim(StrWk);
  result:=StrWk;
end;


Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

function ds(d:TDateTime):String;
begin
  result:=FormatDateTime(sCrystDate,d);
end;

function dts(d:TDateTime):String;
begin
  result:=FormatDateTime(sTimeDate,d);
end;

function ts(d:TDateTime):String;
begin
  result:=FormatDateTime(sTime,d);
end;


Function fs(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

Function fTestKey(Key:Word):Boolean;
begin
  Result:=False;
  if (Key=$30)
  or (Key=$31)
  or (Key=$32)
  or (Key=$33)
  or (Key=$34)
  or (Key=$35)
  or (Key=$36)
  or (Key=$37)
  or (Key=$38)
  or (Key=$39)
  then Result:=True;
end;


function fRToStr(r:Real):String;
Var S:String;
begin
  S:=FloatToStr(RoundEx(r*1000)/1000);
  while pos(',',S)>0 do S[pos(',',S)]:='.';
  Result:=S;
end;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;


procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  AVarSum, AVarSum1, AVarNac: Variant;
  RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then
      begin
        prCalcDefGroup(i1,i2,i3,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          AVarSum := GroupSummaryValues[AChildDataGroupIndex, i1];
          AVarSum1 := GroupSummaryValues[AChildDataGroupIndex, i2];
          if not (VarIsNull(AVarSum) or VarIsNull(AVarSum1)) then
          begin
            AVarNac := (AVarSum1 - AVarSum) / AVarSum*100;
            GroupSummaryValues[AChildDataGroupIndex, i3] := Format('%4.1f', [Double(AVarNac)])+'%';
//          GroupSummaryValues[AChildDataGroupIndex, 2] := Double(AVarNac);
//          Memo1.Lines.Add('In '+FloatToStr(Double(AVarSum))+' Out '+FloatToStr(Double(AVarSum1))+' Nac '+FloatToStr(Double(AVarNac))+' AChildDataGroupIndex '+INtToStr(AChildDataGroupIndex));
            delay(10);
          end;
        end;
      end;
    end;
  end;
end;


procedure prNExportExel4(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            ArrayData[iRow,k] :=StrWk;
            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
          SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spFooter then
          begin
            sField:=SumGr.FieldName;
            sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                ArrayData[iRow,ff]:=sSum;
              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;


procedure prNExportExel5(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    Rec:TcxCustomGridRecord;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  if Vi.Controller.SelectedRowCount>1 then //������������������ ���������
  begin
    iGCount:=0;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.Controller.SelectedRowCount+1;

    iRowV:=Vi.Controller.SelectedRowCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to Vi.Controller.SelectedRecordCount-1 do
    begin
      Rec:=Vi.Controller.SelectedRecords[i];
      k:=1;
      for j:=0 to Vi.ColumnCount-1 do
      begin
        if Vi.Columns[j].Visible then
        begin
          try
            StrWk:=Rec.Values[j]
          except
            StrWk:='';
          end;  
          if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
          ArrayData[iRow+i,k] :=StrWk;
          inc(k);
        end;
      end;
    end;
  end else
  begin       //��� ������

    iGCount:=Vi.DataController.Groups.GroupingItemCount;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.DataController.RowCount+1;

    iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to iRowV-1 do
    begin
      with Vi.DataController do
      begin
        RowInf:=GetRowInfo(i);
        if IsGroupingRow(RowInf,Vi.DataController)=False then
        begin
          k:=1+iGCount;
          for j:=0 to Vi.ColumnCount-1 do
          begin
            if Vi.Columns[j].Visible then
            begin
              StrWk:=GetRowDisplayText(RowInf,j);
              if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
              ArrayData[iRow,k] :=StrWk;
              inc(k);
            end;
          end
        end else
        begin
          iLev:=RowInf.Level;
          iItem:=Groups.GroupingItemIndex[iLev];

          ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
          iCountDG:=Summary.DefaultGroupSummaryItems.Count;
          for ll:=0 to iCountDG-1 do
          begin
            SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
            if SumGr.Position=spFooter then
            begin
              sField:=SumGr.FieldName;
              sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

              for cc:=iGCount+1 to iCol+iGCount do
              begin
                sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
                delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
                if sColumn=sField then //����� �������
                begin
                  ff:=cc;
                  ArrayData[iRow,ff]:=sSum;
                end;
              end;
            end;
          end;


        end;
        inc(iRow);
      end;
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;



procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);
Var rSumIn,rSumOut,rNac:Real;
    StrWk:String;
    vSum:Variant;
begin
  rSumIn:=0; rSumOut:=0;
  vSum:=V.DataController.Summary.FooterSummaryValues[i1];
  if vSum<>Null then rSumIn:=vSum;

  vSum:=V.DataController.Summary.FooterSummaryValues[i2];
  if vSum<>Null then rSumOut:=vSum;

  rNac:=0;
  if rSumIn<>0 then
    rNac:=(rSumOut-rSumIn)/rSumIn*100;
  str(rNac:2:1,StrWk);
  StrWk:=StrWk+'%';
  V.DataController.Summary.FooterSummaryValues[i3]:=StrWk;
end;


procedure CloseTe(taT:tdxMemData);
begin
  taT.Close;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
    taT.Active:=False;
  end;
//  taT.Free;
//  taT.Create(Application);
  taT.Open;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
  end;  
end;

Procedure prCreateVi(ta:TClientDataSet;V:TcxGridDBTableView);
Var i:Integer;
    CurCol: TcxGridDBColumn;
    vS1:TcxDataSummaryItem;
begin
  v.ClearItems;
  v.DataController.Summary.FooterSummaryItems.Clear;
  v.DataController.Summary.DefaultGroupSummaryItems.Clear;

  for i:=0 to ta.FieldDefs.Count-1 do
  begin
    CurCol:=v.CreateColumn;
    CurCol.Name:=V.Name+ta.FieldDefs.Items[i].DisplayName;
    CurCol.DataBinding.FieldName:=ta.FieldDefs.Items[i].Name;
    CurCol.HeaderAlignmentHorz:=taCenter;
    CurCol.Caption:=ta.FieldDefs.Items[i].DisplayName;
    CurCol.Width:=100;
    CurCol.Options.Editing:=False;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;
  end;
  //  v.DataController.KeyFieldNames:='Id';
end;


procedure prNExportExel3(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    arSum:Array[1..20] of Real;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn:String;
    SumGr:tcxGridDBTableSummaryItem;

procedure prStrToArr(S:String);
Var n:Integer;
    s2:String;
begin
  for n:=1 to 20 do arSum[n]:=1000000000; //������� ��� �������
  while pos('(',S)>0 do delete(S,pos('(',S),1);
  while pos(')',S)>0 do delete(S,pos(')',S),1);
  while pos('�',S)>0 do delete(S,pos('�',S),1);
  while pos('.',S)>0 do delete(S,pos('.',S),1);
  while pos(',',S)>0 do
  begin
    if pos(',',S)>0 then S[pos(',',S)]:='&';
    if pos(',',S)>0 then S[pos(',',S)]:='^';
  end;
  while pos('&',S)>0 do S[pos('&',S)]:=',';
  n:=0;
  while S>'' do
  begin
    if Pos('^',S)>0 then
    begin
      S2:=Copy(S,1,Pos('^',S)-1);
      delete(S,1,pos('^',S));
    end else
    begin
      S2:=S;
      S:='';
    end;
    inc(n);
    arSum[n]:=StrToFloatDef(S2,0);
  end;

end;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            ArrayData[iRow,k] :=StrWk;
            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);
        StrWk:=Summary.GroupSummaryText[i];

        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
           SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spGroup then
          begin
            sField:=SumGr.FieldName;
//            ArrayData[iRow,iLev+5+ll]:=sField;

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                if arSum[ll+1]<1000000000 then
                begin
                  ArrayData[iRow,ff]:=arSum[ll+1];
                end;

              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;

function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RV:= ScaledFractPart/100;
end;


function RoundVal( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundVal := ScaledFractPart/100;
end;


function R1000( X: Double ): Double;
begin
  Result:=RoundEx(X*1000)/1000;
end;

procedure CloseTa(taT:tClientDataSet);
begin
 if taT.Active then
 begin
   taT.Close;
//   taT.Free;
   Delay(50);
   taT.CreateDataSet;
 end else taT.CreateDataSet;
end;


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// L�� CLSID OLE-������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // +���� ������
    Result := true
  else
    Result := false;
end;

procedure prExportExel2(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tADOQuery);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i]:=taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;



function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;

function RoundHi( X: Double ): Integer;
//var  ScaledFractPart:Integer;
begin
 RoundHi:=Trunc(X);
 if Frac(X)>0 then RoundHi :=Trunc(X)+1;
 if Frac(X)<0 then RoundHi :=Trunc(X)-1;
end;



procedure WriteLog(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    Strwk1:='Log.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    WriteLn(F,Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



//uses MainSync;

{
Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         ClRec.TYPE_CLASSIF:=StrToIntDef(StrWk,-1);
         if ClRec.TYPE_CLASSIF<0 then result:=False;
       end;
    2: begin
         ClRec.ID:=StrToIntDef(StrWk,-1);
         if ClRec.Id<0 then result:=False;
       end;
    3: begin
         ClRec.Id_Parent:=StrToIntDef(StrWk,-1);
         if ClRec.Id_Parent<0 then result:=False;
       end;
    4: begin
         ClRec.Name:=StrIn;
       end;
    end;
  end;
end;

}



Procedure ReadIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  Person.Id:=f.ReadInteger('Config','PersonId',0);
  CommonSet.PathCryst:=f.ReadString('Config','PathCryst','C:\Crystal\Data');
  CommonSet.CashPath:=f.ReadString('Config','CashPath','C:\_MCrystal\Bin\Pos');
  CommonSet.TrfPath:=f.ReadString('Config','TrfPath','C:\_MCrystal\Bin\Trf');
  CommonSet.TmpPath:=f.ReadString('Config','TmpPath','C:\_MCrystal\Bin\Tmp');
  CommonSet.NetPath:=f.ReadString('Config','NetPath','C:');
  CommonSet.TPrintN:=f.ReadString('Config','TPrintN','');
  CommonSet.TPrintCode:=f.ReadInteger('Config','TPrintCode',1); //1-Win  2-Dos
  CommonSet.CloseDay:=f.ReadInteger('Config','CloseDay',7); //date-7


  if CommonSet.CashPath[Length(CommonSet.CashPath)]<>'\' then CommonSet.CashPath:=CommonSet.CashPath+'\';
  if CommonSet.TrfPath[Length(CommonSet.TrfPath)]<>'\' then CommonSet.TrfPath:=CommonSet.TrfPath+'\';
  if CommonSet.TmpPath[Length(CommonSet.TmpPath)]<>'\' then CommonSet.TmpPath:=CommonSet.TmpPath+'\';
  if CommonSet.NetPath[Length(CommonSet.NetPath)]<>'\' then CommonSet.NetPath:=CommonSet.NetPath+'\';

  CommonSet.iMin:=f.ReadInteger('Config','iMin',0);
  CommonSet.iMax:=f.ReadInteger('Config','iMax',50000);
  CommonSet.iMinVes:=f.ReadInteger('Config','iMinVes',50000);
  CommonSet.iMaxVes:=f.ReadInteger('Config','iMaxVes',60000);
  CommonSet.Prefix:=f.ReadString('Config','Prefix','26');
  CommonSet.PrefixVes:=f.ReadString('Config','PrefixVes','22');
  sFormatDate:=f.ReadString('Config','FormatDate','mm.dd.yyyy');

  f.WriteInteger('Config','PersonId',Person.Id);
  f.WriteString('Config','PathCryst',CommonSet.PathCryst);
  f.WriteString('Config','CashPath',CommonSet.CashPath);
  f.WriteString('Config','TrfPath',CommonSet.TrfPath);
  f.WriteString('Config','TmpPath',CommonSet.TmpPath);
  f.WriteString('Config','NetPath',CommonSet.NetPath);

  f.WriteString('Config','TPrintN',CommonSet.TPrintN);
  f.WriteInteger('Config','TPrintCode',CommonSet.TPrintCode); //1-Win  2-Dos
  f.WriteInteger('Config','CloseDay',CommonSet.CloseDay); //������������ ��� ������ ����

  f.WriteInteger('Config','iMin',CommonSet.iMin);
  f.WriteInteger('Config','iMax',CommonSet.iMax);
  f.WriteInteger('Config','iMinVes',CommonSet.iMinVes);
  f.WriteInteger('Config','iMaxVes',CommonSet.iMaxVes);
  f.WriteString('Config','Prefix',CommonSet.Prefix);
  f.WriteString('Config','PrefixVes',CommonSet.PrefixVes);

  f.Free;
end;

Procedure WriteIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  f.WriteInteger('Config','PersonId',Person.Id);
  f.WriteString('Config','PathCryst',CommonSet.PathCryst);
  f.WriteInteger('Config','iMin',CommonSet.iMin);
  f.WriteInteger('Config','iMax',CommonSet.iMax);
  f.WriteInteger('Config','iMinVes',CommonSet.iMinVes);
  f.WriteInteger('Config','iMaxVes',CommonSet.iMaxVes);
  f.WriteString('Config','Prefix',CommonSet.Prefix);
  f.WriteString('Config','PrefixVes',CommonSet.PrefixVes);
  f.WriteString('Config','TPrintN',CommonSet.TPrintN);
  f.WriteInteger('Config','TPrintCode',CommonSet.TPrintCode); //1-Win  2-Dos

  f.Free;
end;


Procedure RefreshTree(Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
begin
  while tree.Items.Count>0 do tree.Items[0].Delete;
  RExpandLevel( Nil,Tree,quTree,PersonalId);
//  tree.Items[0].Expand(True);
  delay(10);
end;

Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.Parameters.ParamByName('ParentID').Value:=ID;
  quTree.Parameters.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID_Classif').AsInteger));
    if quTree.FieldByName('Rights').AsInteger=1 then //��� �������
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('Rights').AsInteger=0 then  //���� ������
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.Parameters.ParamByName('ParentID').Value:=ID;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    if quTree.FieldByName('ID_PARENT').AsInteger=0 then
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('ID_PARENT').AsInteger>0 then
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Var ChildNode,CurNode:TTreeNode;
begin
  ChildNode:=Node.getFirstChild;
  while ChildNode<>Nil do
  begin
    if bOn then ChildNode.ImageIndex:=2
    else ChildNode.ImageIndex:=0;
    CurNode:=ChildNode;
    CheckNodeOn(CurNode,bOn);
    ChildNode:=CurNode.getNextChild(CurNode);
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    StrWk1:=FormatDateTime('hh:nn sss',now)+' ';
    WriteLn(F,StrWk1+Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

procedure prWH(Strwk_: string;Memo1:TcxMemo);
begin
  WriteHistory(Strwk_);
  Memo1.Lines.Add(Strwk_);
  Delay(10);
end;


Function prOpenPrinter(PName:String):Boolean;
Var  DocInfo1: TDocInfo1;
begin
//  prWriteLog('   �������� ������� - '+PName);
  Result:=False;
  if OpenPrinter(PChar(PName), PrintHandle, nil) then
  begin
    Result:=True;
    with DocInfo1 do begin
      pDocName := PChar('Tmp_doc');
      pOutputFile := nil;
      pDataType := 'RAW';
    end;
    StartDocPrinter(PrintHandle, 1, @DocInfo1);
    StartPagePrinter(PrintHandle);
  end;
//  else prWriteLog('   ������� �� ���� - '+PName);
end;

Procedure prClosePrinter(PName:String);
begin
  EndPagePrinter(PrintHandle);
  EndDocPrinter(PrintHandle);
  ClosePrinter(PrintHandle);
end;

Procedure prWritePrinter(S:String);
Var  N: DWORD;
begin
  WritePrinter(PrintHandle, PChar(S), Length(S), N);
end;


end.
