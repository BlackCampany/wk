object fmClosePer: TfmClosePer
  Left = 320
  Top = 111
  Width = 596
  Height = 493
  Caption = #1047#1072#1082#1088#1099#1090#1080#1077' '#1087#1077#1088#1080#1086#1076#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 588
    Height = 65
    Align = alTop
    BevelInner = bvLowered
    Color = 50176
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 16
      Width = 187
      Height = 16
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1087#1077#1088#1080#1086#1076' '#1087#1086' ('#1074#1082#1083'.)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 20
      Top = 40
      Width = 69
      Height = 16
      Caption = 'v 12.05.24'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxDateEdit1: TcxDateEdit
      Left = 212
      Top = 14
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 133
    end
    object cxButton1: TcxButton
      Left = 360
      Top = 12
      Width = 85
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 464
      Top = 12
      Width = 85
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 2
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrClose: TcxGrid
    Left = 12
    Top = 72
    Width = 565
    Height = 373
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViClose: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquCloseH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViCloseID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'ID'
      end
      object ViCloseDateClose: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1090#1086' '#1087#1086
        DataBinding.FieldName = 'DateClose'
        Width = 90
      end
      object ViCloseIdPers: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1077#1088#1089#1086#1085#1072#1083#1072
        DataBinding.FieldName = 'IdPers'
      end
      object ViCloseDateEdit: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1077#1081#1089#1090#1074#1080#1103
        DataBinding.FieldName = 'DateEdit'
      end
      object ViCloseNAME: TcxGridDBColumn
        Caption = #1050#1077#1084
        DataBinding.FieldName = 'NAME'
        Width = 153
      end
    end
    object LevelClose: TcxGridLevel
      GridView = ViClose
    end
  end
  object PvSQL: TPvSqlDatabase
    AliasName = 'SCRYSTAL'
    Connected = True
    DatabaseName = 'PSQL'
    SessionName = 'PvSqlDefault'
    Left = 20
    Top = 100
  end
  object quPassw: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT * FROM "A_RPERSONAL"'
      'where Uvolnen=0 and Modul1=1 and Id_Parent>0'
      'order by Name')
    Params = <>
    Left = 72
    Top = 100
    object quPasswID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object quPasswID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
      Required = True
    end
    object quPasswNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quPasswUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
      Required = True
    end
    object quPasswPASSW: TStringField
      FieldName = 'PASSW'
      Required = True
    end
    object quPasswMODUL1: TSmallintField
      FieldName = 'MODUL1'
      Required = True
    end
    object quPasswMODUL2: TSmallintField
      FieldName = 'MODUL2'
      Required = True
    end
    object quPasswMODUL3: TSmallintField
      FieldName = 'MODUL3'
      Required = True
    end
    object quPasswMODUL4: TSmallintField
      FieldName = 'MODUL4'
      Required = True
    end
    object quPasswMODUL5: TSmallintField
      FieldName = 'MODUL5'
      Required = True
    end
    object quPasswMODUL6: TSmallintField
      FieldName = 'MODUL6'
      Required = True
    end
    object quPasswBARCODE: TStringField
      FieldName = 'BARCODE'
      Required = True
    end
  end
  object dsquPassw: TDataSource
    DataSet = quPassw
    Left = 72
    Top = 152
  end
  object quCloseH: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      'ch.ID,'
      'ch.DateClose,'
      'ch.IdPers,'
      'ch.DateEdit,'
      'per.NAME'
      'from "A_CLOSEHISTMAIN" ch'
      'left join "A_RPERSONAL" per on per.ID=ch.IdPers '
      'order by ch.ID DESC')
    Params = <>
    Left = 140
    Top = 100
    object quCloseHID: TIntegerField
      FieldName = 'ID'
    end
    object quCloseHDateClose: TDateField
      FieldName = 'DateClose'
    end
    object quCloseHIdPers: TIntegerField
      FieldName = 'IdPers'
    end
    object quCloseHDateEdit: TDateTimeField
      FieldName = 'DateEdit'
    end
    object quCloseHNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
  end
  object dsquCloseH: TDataSource
    DataSet = quCloseH
    Left = 144
    Top = 152
  end
  object quA: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 208
    Top = 116
  end
  object quM: TPvQuery
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select max(ID) as iMax from "A_CLOSEHISTMAIN"')
    Params = <>
    Left = 268
    Top = 116
    object quMiMax: TIntegerField
      FieldName = 'iMax'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 352
    Top = 116
  end
  object Timer2: TTimer
    Interval = 10000
    OnTimer = Timer2Timer
    Left = 424
    Top = 116
  end
  object quCloseCur: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select top 1 * from "A_CLOSEHIST"'
      'order by ID desc')
    Params = <>
    Left = 52
    Top = 220
    object quCloseCurID: TIntegerField
      FieldName = 'ID'
    end
    object quCloseCurDateClose: TDateField
      FieldName = 'DateClose'
    end
    object quCloseCurIdPers: TIntegerField
      FieldName = 'IdPers'
    end
    object quCloseCurDateEdit: TDateTimeField
      FieldName = 'DateEdit'
    end
  end
end
