unit ChangePost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, StdCtrls, cxButtons, ExtCtrls, cxMaskEdit,
  cxButtonEdit;

type
  TfmChangePost = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Memo1: TcxMemo;
    cxButtonEdit1: TcxButtonEdit;
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmChangePost: TfmChangePost;

implementation

uses MDB, Clients, Un1;

{$R *.dfm}

procedure TfmChangePost.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmChangePost.cxButton2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmChangePost.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  iDirect:=1; //0 - ������ , 1- �������

  bOpen:=True;

  if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;

  fmClients.LevelCli.Active:=True;
  fmClients.LevelIP.Active:=False;

  bOpen:=False;

  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    if fmClients.GridCli.ActiveView=fmClients.ViewCli then
    begin
      if pos('��',dmMC.quCliName.AsString)<>1 then
      begin
        Label4.Tag:=1;
        cxButtonEdit1.Tag:=dmMC.quCliVendor.AsInteger;
        cxButtonEdit1.Text:=dmMC.quCliName.AsString;
      end else showmessage('����� ������� ���������� �������� !!!');
    end else
    begin
      if pos('��',dmMC.quIPName.AsString)<>1 then
      begin
        Label4.Tag:=2;
        cxButtonEdit1.Tag:=dmMC.quIPCode.AsInteger;
        cxButtonEdit1.Text:=dmMC.quIPName.AsString;
      end  else showmessage('����� ������� ���������� �������� !!!');
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
    Label4.Tag:=0;
  end;

  iDirect:=0;
end;

procedure TfmChangePost.cxButton1Click(Sender: TObject);
Var sInn,sFName,sAdr,SInnF:String;
begin
  if (Label4.Tag=0) or (cxButtonEdit1.Tag=0) then
  begin
    Showmessage('�������� ����������.');
    exit;
  end;
  with dmMC do
  begin
    if MessageDlg('���������� ������ ����������? ��� ������ ��������� �����.', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������');
      try
        Memo1.Lines.Add('  ���������.');

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ');
        quE.SQL.Add('IndexPost='+its(Label4.Tag)+',');
        quE.SQL.Add('CodePost='+its(cxButtonEdit1.Tag));
        quE.SQL.Add('where ');
        quE.SQL.Add('DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.SQL.Add('and IndexPost='+its(quTTnInIndexPost.AsInteger));
        quE.SQL.Add('and CodePost='+its(quTTnInCodePost.AsInteger));
        quE.ExecSQL;

        Memo1.Lines.Add('  Ok.');

        Memo1.Lines.Add('  ������������.');

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNInLn" Set ');
        quE.SQL.Add('IndexPost='+its(Label4.Tag)+',');
        quE.SQL.Add('CodePost='+its(cxButtonEdit1.Tag));
        quE.SQL.Add('where ');
        quE.SQL.Add('DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.SQL.Add('and IndexPost='+its(quTTnInIndexPost.AsInteger));
        quE.SQL.Add('and CodePost='+its(quTTnInCodePost.AsInteger));
        quE.ExecSQL;

        Memo1.Lines.Add('  Ok.');


        Memo1.Lines.Add('  ��������� ������.');

        sInn:='';
        prFCliINN(cxButtonEdit1.Tag,Label4.Tag,sInn,sFName,sAdr,SInnF);

        if pos('/',sInn)>0 then sInn:=Copy(sInn,1,pos('/',sInn)-1);

        if sInn<>'' then
        begin
          Memo1.Lines.Add('    ����� ��� - '+sInn);

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_PARTIN" Set ');
          quE.SQL.Add('SINNCLI='''+SInnF+'''');
          quE.SQL.Add('where ');
          quE.SQL.Add('IDATE='+its(Trunc(quTTnInDateInvoice.AsDateTime)));
          quE.SQL.Add('and IDSTORE='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and IDDOC='+its(quTTnInID.AsInteger));
          quE.ExecSQL;
        end;

        Memo1.Lines.Add('  Ok.');


        Memo1.Lines.Add('  ��������� ������.');

        if sInn<>'' then
        begin
          Memo1.Lines.Add('    ����� ��� - '+sInn);

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_PARTOUT" Set ');
          quE.SQL.Add('SINN='''+SInnF+'''');
          quE.SQL.Add('where PARTIN in (');
          quE.SQL.Add('select ID from "A_PARTIN"');
          quE.SQL.Add('where ');
          quE.SQL.Add('IDATE='+its(Trunc(quTTnInDateInvoice.AsDateTime)));
          quE.SQL.Add('and IDSTORE='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and IDDOC='+its(quTTnInID.AsInteger));
          quE.SQL.Add(')');
          quE.ExecSQL;
        end;

{select * from "A_PARTOUT"
where PARTIN in (
select ID from "A_PARTIN"
where IDATE=40756
and IDSTORE=5
and IDDOC=95073)
}
        Memo1.Lines.Add('  Ok.');

      except

      end;
      Memo1.Lines.Add('������ ���������. ');
    end;
  end;
end;

end.
