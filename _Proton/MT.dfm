object dmMT: TdmMT
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 869
  Top = 313
  Height = 786
  Width = 998
  object ptCG: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    SessionName = 'PvDefault'
    IndexFieldNames = 'VENDOR;ITEM'
    TableName = 'CardGoods'
    Left = 28
    Top = 12
    object ptCGCARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCGITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object ptCGCOST: TFloatField
      FieldName = 'COST'
    end
    object ptCGDEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object ptCGVENDOR: TIntegerField
      FieldName = 'VENDOR'
    end
    object ptCGATTRIBUTE: TSmallintField
      FieldName = 'ATTRIBUTE'
    end
  end
  object ptCM: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexName = 'key0'
    MasterFields = 'CARD'
    MasterSource = dsptCG
    TableName = 'CardGoodsMove'
    Left = 84
    Top = 12
    object ptCMCARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCMMOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object ptCMDOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object ptCMDOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object ptCMPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ptCMQUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object ptCMSUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object ptCMQUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object dsptCG: TDataSource
    DataSet = ptCG
    Left = 28
    Top = 64
  end
  object taCards: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'Goods'
    Left = 148
    Top = 12
    object taCardsOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object taCardsGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object taCardsSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object taCardsID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object taCardsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object taCardsBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object taCardsShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object taCardsTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object taCardsEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object taCardsNDS: TFloatField
      FieldName = 'NDS'
    end
    object taCardsOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object taCardsWeght: TFloatField
      FieldName = 'Weght'
    end
    object taCardsSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object taCardsTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object taCardsKritOst: TFloatField
      FieldName = 'KritOst'
    end
    object taCardsBHTStatus: TSmallintField
      FieldName = 'BHTStatus'
    end
    object taCardsUKMAction: TSmallintField
      FieldName = 'UKMAction'
    end
    object taCardsDataChange: TDateField
      FieldName = 'DataChange'
    end
    object taCardsNSP: TFloatField
      FieldName = 'NSP'
    end
    object taCardsWasteRate: TFloatField
      FieldName = 'WasteRate'
    end
    object taCardsCena: TFloatField
      FieldName = 'Cena'
    end
    object taCardsKol: TFloatField
      FieldName = 'Kol'
    end
    object taCardsFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object taCardsCountry: TSmallintField
      FieldName = 'Country'
    end
    object taCardsStatus: TSmallintField
      FieldName = 'Status'
    end
    object taCardsPrsision: TFloatField
      FieldName = 'Prsision'
    end
    object taCardsPriceState: TWordField
      FieldName = 'PriceState'
    end
    object taCardsSetOfGoods: TWordField
      FieldName = 'SetOfGoods'
    end
    object taCardsPrePacking: TStringField
      FieldName = 'PrePacking'
      Size = 5
    end
    object taCardsPrintLabel: TWordField
      FieldName = 'PrintLabel'
    end
    object taCardsMargGroup: TStringField
      FieldName = 'MargGroup'
      Size = 2
    end
    object taCardsFixPrice: TFloatField
      FieldName = 'FixPrice'
    end
    object taCardsReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object taCardsReserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object taCardsReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object taCardsReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object taCardsReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object taCardsObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object taCardsImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object taCardsV01: TIntegerField
      FieldName = 'V01'
    end
    object taCardsV02: TIntegerField
      FieldName = 'V02'
    end
    object taCardsV03: TIntegerField
      FieldName = 'V03'
    end
    object taCardsV04: TIntegerField
      FieldName = 'V04'
    end
    object taCardsV05: TIntegerField
      FieldName = 'V05'
    end
    object taCardsV06: TIntegerField
      FieldName = 'V06'
    end
    object taCardsV07: TIntegerField
      FieldName = 'V07'
    end
    object taCardsV08: TIntegerField
      FieldName = 'V08'
    end
    object taCardsV09: TIntegerField
      FieldName = 'V09'
    end
    object taCardsV10: TIntegerField
      FieldName = 'V10'
    end
    object taCardsV11: TIntegerField
      FieldName = 'V11'
    end
    object taCardsV12: TIntegerField
      FieldName = 'V12'
    end
    object taCardsV13: TIntegerField
      FieldName = 'V13'
    end
    object taCardsV14: TIntegerField
      FieldName = 'V14'
    end
  end
  object taRemove: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 216
    Top = 12
    object taRemoveArticul: TIntegerField
      FieldName = 'Articul'
    end
    object taRemoveDepart: TIntegerField
      FieldName = 'Depart'
    end
    object taRemoveQuant: TFloatField
      FieldName = 'Quant'
    end
    object taRemoveRes: TIntegerField
      FieldName = 'Res'
    end
  end
  object ptCQ: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'DateOperation;Cash_Code;Ck_Number'
    TableName = 'cq201002'
    Left = 280
    Top = 12
    object ptCQQuant: TFloatField
      FieldName = 'Quant'
      Required = True
    end
    object ptCQOperation: TStringField
      FieldName = 'Operation'
      Required = True
      Size = 1
    end
    object ptCQDateOperation: TDateField
      FieldName = 'DateOperation'
      Required = True
    end
    object ptCQPrice: TFloatField
      FieldName = 'Price'
      Required = True
    end
    object ptCQStore: TStringField
      FieldName = 'Store'
      Required = True
      Size = 6
    end
    object ptCQCk_Number: TIntegerField
      FieldName = 'Ck_Number'
      Required = True
    end
    object ptCQCk_Curs: TFloatField
      FieldName = 'Ck_Curs'
      Required = True
    end
    object ptCQCk_CurAbr: TIntegerField
      FieldName = 'Ck_CurAbr'
      Required = True
    end
    object ptCQCk_Disg: TFloatField
      FieldName = 'Ck_Disg'
      Required = True
    end
    object ptCQCk_Disc: TFloatField
      FieldName = 'Ck_Disc'
      Required = True
    end
    object ptCQQuant_S: TFloatField
      FieldName = 'Quant_S'
      Required = True
    end
    object ptCQGrCode: TSmallintField
      FieldName = 'GrCode'
      Required = True
    end
    object ptCQCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object ptCQCassir: TStringField
      FieldName = 'Cassir'
      Required = True
      Size = 10
    end
    object ptCQCash_Code: TIntegerField
      FieldName = 'Cash_Code'
      Required = True
    end
    object ptCQCk_Card: TIntegerField
      FieldName = 'Ck_Card'
      Required = True
    end
    object ptCQContr_Code: TStringField
      FieldName = 'Contr_Code'
      Required = True
      Size = 7
    end
    object ptCQContr_Cost: TFloatField
      FieldName = 'Contr_Cost'
      Required = True
    end
    object ptCQSeria: TStringField
      FieldName = 'Seria'
      Required = True
      Size = 10
    end
    object ptCQBestB: TStringField
      FieldName = 'BestB'
      Required = True
      Size = 10
    end
    object ptCQNDSx1: TFloatField
      FieldName = 'NDSx1'
      Required = True
    end
    object ptCQNDSx2: TFloatField
      FieldName = 'NDSx2'
      Required = True
    end
    object ptCQTimes: TTimeField
      FieldName = 'Times'
      Required = True
    end
    object ptCQSumma: TFloatField
      FieldName = 'Summa'
      Required = True
    end
    object ptCQSumNDS: TFloatField
      FieldName = 'SumNDS'
      Required = True
    end
    object ptCQSumNSP: TFloatField
      FieldName = 'SumNSP'
      Required = True
    end
    object ptCQPriceNSP: TFloatField
      FieldName = 'PriceNSP'
      Required = True
    end
    object ptCQNSmena: TIntegerField
      FieldName = 'NSmena'
      Required = True
    end
  end
  object meMove: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 28
    Top = 180
    object meMoveCard: TIntegerField
      FieldName = 'Card'
    end
    object meMoveMoveDate: TDateField
      FieldName = 'MoveDate'
    end
    object meMoveNum: TIntegerField
      FieldName = 'Num'
    end
    object meMoveDoc_Type: TIntegerField
      FieldName = 'Doc_Type'
    end
    object meMoveDocument: TIntegerField
      FieldName = 'Document'
    end
    object meMoveQuantMove: TFloatField
      FieldName = 'QuantMove'
      DisplayFormat = '0.000'
    end
    object meMoveQuantRest: TFloatField
      FieldName = 'QuantRest'
      DisplayFormat = '0.000'
    end
    object meMoveName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object meMoveDepart: TIntegerField
      FieldName = 'Depart'
    end
    object meMoveQIn: TFloatField
      FieldName = 'QIn'
      DisplayFormat = '0.000'
    end
    object meMoveQOut: TFloatField
      FieldName = 'QOut'
      DisplayFormat = '0.000'
    end
  end
  object dsmeMove: TDataSource
    DataSet = meMove
    Left = 108
    Top = 184
  end
  object ptDep: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'Depart'
    Left = 344
    Top = 12
    object ptDepID: TSmallintField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptDepName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptDepStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object ptDepStatus2: TSmallintField
      FieldName = 'Status2'
    end
    object ptDepStatus3: TSmallintField
      FieldName = 'Status3'
    end
    object ptDepStatus4: TIntegerField
      FieldName = 'Status4'
    end
    object ptDepStatus5: TIntegerField
      FieldName = 'Status5'
    end
    object ptDepParentID: TSmallintField
      FieldName = 'ParentID'
    end
    object ptDepObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object ptDepClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object ptDepClientObjTypeID: TSmallintField
      FieldName = 'ClientObjTypeID'
    end
    object ptDepImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object ptDepFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object ptDepEnglishFullName: TStringField
      FieldName = 'EnglishFullName'
      Size = 100
    end
    object ptDepPath: TStringField
      FieldName = 'Path'
      Size = 30
    end
    object ptDepV01: TIntegerField
      FieldName = 'V01'
    end
    object ptDepV02: TIntegerField
      FieldName = 'V02'
    end
    object ptDepV03: TIntegerField
      FieldName = 'V03'
    end
    object ptDepV04: TIntegerField
      FieldName = 'V04'
    end
    object ptDepV05: TIntegerField
      FieldName = 'V05'
    end
    object ptDepV06: TIntegerField
      FieldName = 'V06'
    end
    object ptDepV07: TIntegerField
      FieldName = 'V07'
    end
    object ptDepV08: TIntegerField
      FieldName = 'V08'
    end
    object ptDepV09: TIntegerField
      FieldName = 'V09'
    end
    object ptDepV10: TIntegerField
      FieldName = 'V10'
    end
    object ptDepV11: TIntegerField
      FieldName = 'V11'
    end
    object ptDepV12: TIntegerField
      FieldName = 'V12'
    end
    object ptDepV13: TIntegerField
      FieldName = 'V13'
    end
    object ptDepV14: TIntegerField
      FieldName = 'V14'
    end
  end
  object ptCM1: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'CARD;MOVEDATE'
    TableName = 'CardGoodsMove'
    Left = 84
    Top = 64
    object ptCM1CARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCM1MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object ptCM1DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object ptCM1DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object ptCM1PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ptCM1QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object ptCM1SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object ptCM1QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object taHeadDoc: TClientDataSet
    Aggregates = <>
    FileName = 'HeadDoc.cds'
    Params = <>
    Left = 32
    Top = 264
    object taHeadDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taHeadDocId: TIntegerField
      FieldName = 'Id'
    end
    object taHeadDocDateDoc: TIntegerField
      FieldName = 'DateDoc'
    end
    object taHeadDocNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 15
    end
    object taHeadDocIdCli: TIntegerField
      FieldName = 'IdCli'
    end
    object taHeadDocNameCli: TStringField
      FieldName = 'NameCli'
      Size = 70
    end
    object taHeadDocIdSkl: TIntegerField
      FieldName = 'IdSkl'
    end
    object taHeadDocNameSkl: TStringField
      FieldName = 'NameSkl'
      Size = 70
    end
    object taHeadDocSumIN: TFloatField
      FieldName = 'SumIN'
      DisplayFormat = '0.00'
    end
    object taHeadDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taHeadDocComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
    object taHeadDociTypeCli: TIntegerField
      FieldName = 'iTypeCli'
    end
    object taHeadDocSumTara: TFloatField
      FieldName = 'SumTara'
    end
  end
  object dsHeadDoc: TDataSource
    DataSet = taHeadDoc
    Left = 32
    Top = 320
  end
  object taSpecDoc: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 104
    Top = 260
    object taSpecDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDocIdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDocNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDocIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDocQuantM: TFloatField
      FieldName = 'QuantM'
    end
    object taSpecDocQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDocPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocIdNds: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDocSumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDocNameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDocSm: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDocIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDocKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecDocPriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDocSumUch1: TFloatField
      FieldName = 'SumUch1'
    end
    object taSpecDocProcPrice: TFloatField
      FieldName = 'ProcPrice'
      DisplayFormat = '0.0'
    end
    object taSpecDocIdTara: TIntegerField
      FieldName = 'IdTara'
    end
    object taSpecDocQuantT: TFloatField
      FieldName = 'QuantT'
    end
    object taSpecDocNameT: TStringField
      FieldName = 'NameT'
      Size = 30
    end
    object taSpecDocPriceT: TFloatField
      FieldName = 'PriceT'
    end
    object taSpecDocSumTara: TFloatField
      FieldName = 'SumTara'
    end
    object taSpecDocBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
  end
  object dsSpecDoc: TDataSource
    DataSet = taSpecDoc
    Left = 104
    Top = 320
  end
  object ptMgr: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'GdsGroup'
    Left = 396
    Top = 12
    object ptMgrID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptMgrName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptMgrFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object ptMgrGroupEU: TSmallintField
      FieldName = 'GroupEU'
    end
  end
  object ptSGr: TPvTable
    DatabaseName = '\\SYS4\d$\SCRYSTAL\DATA'
    IndexFieldNames = 'ID'
    TableName = 'SubGroup'
    Left = 452
    Top = 12
    object ptSGrGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object ptSGrID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptSGrName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptSGrReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object ptSGrReserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object ptSGrReserv3: TFloatField
      FieldName = 'Reserv3'
    end
  end
  object taACards: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_GOODS'
    Left = 148
    Top = 60
    object taACardsID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object taACardsRECEIPT: TStringField
      FieldName = 'RECEIPT'
      Size = 150
    end
  end
  object ptDStop: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Code'
    TableName = 'PluLim'
    Left = 520
    Top = 12
    object ptDStopCode: TIntegerField
      FieldName = 'Code'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptDStopPercent: TFloatField
      FieldName = 'Percent'
    end
    object ptDStopStatus: TSmallintField
      FieldName = 'Status'
    end
    object ptDStopStatus_1: TSmallintField
      FieldName = 'Status_1'
    end
  end
  object ptCqRep: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    OnCalcFields = ptCqRepCalcFields
    IndexFieldNames = 'DateOperation;Cash_Code;Ck_Number'
    TableName = 'cq201002'
    Left = 280
    Top = 72
    object ptCqRepQuant: TFloatField
      FieldName = 'Quant'
      Required = True
      DisplayFormat = '0.000'
    end
    object ptCqRepOperation: TStringField
      FieldName = 'Operation'
      Required = True
      Size = 1
    end
    object ptCqRepDateOperation: TDateField
      FieldName = 'DateOperation'
      Required = True
    end
    object ptCqRepPrice: TFloatField
      FieldName = 'Price'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepStore: TStringField
      FieldName = 'Store'
      Required = True
      Size = 6
    end
    object ptCqRepCk_Number: TIntegerField
      FieldName = 'Ck_Number'
      Required = True
    end
    object ptCqRepCk_Curs: TFloatField
      FieldName = 'Ck_Curs'
      Required = True
    end
    object ptCqRepCk_CurAbr: TIntegerField
      FieldName = 'Ck_CurAbr'
      Required = True
    end
    object ptCqRepCk_Disg: TFloatField
      FieldName = 'Ck_Disg'
      Required = True
    end
    object ptCqRepCk_Disc: TFloatField
      FieldName = 'Ck_Disc'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepQuant_S: TFloatField
      FieldName = 'Quant_S'
      Required = True
      DisplayFormat = '0.00000'
    end
    object ptCqRepGrCode: TSmallintField
      FieldName = 'GrCode'
      Required = True
    end
    object ptCqRepCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object ptCqRepCassir: TStringField
      FieldName = 'Cassir'
      Required = True
      Size = 10
    end
    object ptCqRepCash_Code: TIntegerField
      FieldName = 'Cash_Code'
      Required = True
    end
    object ptCqRepCk_Card: TIntegerField
      FieldName = 'Ck_Card'
      Required = True
    end
    object ptCqRepContr_Code: TStringField
      FieldName = 'Contr_Code'
      Required = True
      Size = 7
    end
    object ptCqRepContr_Cost: TFloatField
      FieldName = 'Contr_Cost'
      Required = True
    end
    object ptCqRepSeria: TStringField
      FieldName = 'Seria'
      Required = True
      Size = 10
    end
    object ptCqRepBestB: TStringField
      FieldName = 'BestB'
      Required = True
      Size = 10
    end
    object ptCqRepNDSx1: TFloatField
      FieldName = 'NDSx1'
      Required = True
    end
    object ptCqRepNDSx2: TFloatField
      FieldName = 'NDSx2'
      Required = True
    end
    object ptCqRepTimes: TTimeField
      FieldName = 'Times'
      Required = True
    end
    object ptCqRepSumma: TFloatField
      FieldName = 'Summa'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepSumNDS: TFloatField
      FieldName = 'SumNDS'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepSumNSP: TFloatField
      FieldName = 'SumNSP'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepPriceNSP: TFloatField
      FieldName = 'PriceNSP'
      Required = True
      DisplayFormat = '0.00'
    end
    object ptCqRepNSmena: TIntegerField
      FieldName = 'NSmena'
      Required = True
    end
    object ptCqRepName: TStringField
      FieldKind = fkCalculated
      FieldName = 'Name'
      Size = 30
      Calculated = True
    end
  end
  object dsptCqRep: TDataSource
    DataSet = ptCqRep
    Left = 344
    Top = 76
  end
  object taG: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    OnCalcFields = ptCqRepCalcFields
    IndexFieldNames = 'ID'
    TableName = 'Goods'
    Left = 400
    Top = 76
    object taGGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object taGSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object taGID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object taGName: TStringField
      FieldName = 'Name'
      Size = 30
      Transliterate = False
    end
    object taGBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object taGShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object taGTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object taGEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object taGNDS: TFloatField
      FieldName = 'NDS'
    end
    object taGSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object taGDataChange: TDateField
      FieldName = 'DataChange'
    end
    object taGCena: TFloatField
      FieldName = 'Cena'
    end
    object taGKol: TFloatField
      FieldName = 'Kol'
    end
    object taGFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object taGCountry: TSmallintField
      FieldName = 'Country'
    end
    object taGStatus: TSmallintField
      FieldName = 'Status'
    end
    object taGV01: TIntegerField
      FieldName = 'V01'
    end
    object taGV02: TIntegerField
      FieldName = 'V02'
    end
    object taGV03: TIntegerField
      FieldName = 'V03'
    end
    object taGReserv1: TFloatField
      FieldName = 'Reserv1'
    end
    object taGReserv2: TFloatField
      FieldName = 'Reserv2'
    end
    object taGReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object taGReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object taGReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object taGV04: TIntegerField
      FieldName = 'V04'
    end
    object taGV05: TIntegerField
      FieldName = 'V05'
    end
    object taGV06: TIntegerField
      FieldName = 'V06'
    end
  end
  object ptCT: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    SessionName = 'PvDefault'
    IndexFieldNames = 'VENDOR;ITEM'
    TableName = 'CardPak'
    Left = 576
    Top = 12
    object ptCTCARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCTITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object ptCTCOST: TFloatField
      FieldName = 'COST'
    end
    object ptCTDEPART: TSmallintField
      FieldName = 'DEPART'
    end
    object ptCTVENDOR: TIntegerField
      FieldName = 'VENDOR'
    end
    object ptCTATTRIBUTE: TSmallintField
      FieldName = 'ATTRIBUTE'
    end
  end
  object dsptCT: TDataSource
    DataSet = ptCT
    Left = 576
    Top = 64
  end
  object ptCTM: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexName = 'key0'
    MasterFields = 'CARD'
    MasterSource = dsptCT
    TableName = 'CardPackMove'
    Left = 628
    Top = 12
    object ptCTMCARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCTMMOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object ptCTMDOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object ptCTMDOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object ptCTMPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ptCTMQUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object ptCTMSUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object ptCTMQUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object teClass: TdxMemData
    Indexes = <>
    SortOptions = [soCaseInsensitive]
    SortedField = 'NameClass'
    Left = 164
    Top = 184
    object teClassId: TIntegerField
      FieldName = 'Id'
    end
    object teClassIdParent: TIntegerField
      FieldName = 'IdParent'
    end
    object teClassNameClass: TStringField
      FieldName = 'NameClass'
      Size = 30
    end
    object teClassrNac: TFloatField
      FieldName = 'rNac'
    end
  end
  object ptPluLim: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    OnCalcFields = ptPluLimCalcFields
    IndexFieldNames = 'Code'
    TableName = 'PluLim'
    Left = 688
    Top = 12
    object ptPluLimCode: TIntegerField
      FieldName = 'Code'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptPluLimPercent: TFloatField
      FieldName = 'Percent'
    end
    object ptPluLimPercentDisc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercentDisc'
      Calculated = True
    end
  end
  object dsptPluLim: TDataSource
    DataSet = ptPluLim
    Left = 688
    Top = 60
  end
  object ptMOL: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexName = 'PK_ID'
    TableName = 'A_MOL'
    Left = 452
    Top = 76
    object ptMOLID: TSmallintField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptMOLOTVL: TStringField
      FieldName = 'OTVL'
      Size = 150
    end
  end
  object ptCTM1: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'MOVEDATE'
    TableName = 'CardPackMove'
    Left = 628
    Top = 64
    object ptCTM1CARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCTM1MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object ptCTM1DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object ptCTM1DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object ptCTM1PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ptCTM1QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object ptCTM1SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object ptCTM1QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object ptInvLine: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;Inventry;Item;Price'
    TableName = 'InventryLine'
    Left = 532
    Top = 136
    object ptInvLineInventry: TIntegerField
      FieldName = 'Inventry'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInvLineItem: TIntegerField
      FieldName = 'Item'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInvLinePrice: TFloatField
      FieldName = 'Price'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInvLineQuantRecord: TFloatField
      FieldName = 'QuantRecord'
    end
    object ptInvLineQuantActual: TFloatField
      FieldName = 'QuantActual'
    end
    object ptInvLineQuantLost: TFloatField
      FieldName = 'QuantLost'
    end
    object ptInvLineDepart: TSmallintField
      FieldName = 'Depart'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInvLineIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object ptInvLineCodePost: TSmallintField
      FieldName = 'CodePost'
    end
  end
  object ptCM2: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'MOVEDATE'
    TableName = 'CardGoodsMove'
    Left = 84
    Top = 120
    object ptCM2CARD: TIntegerField
      FieldName = 'CARD'
    end
    object ptCM2MOVEDATE: TDateField
      FieldName = 'MOVEDATE'
    end
    object ptCM2DOC_TYPE: TSmallintField
      FieldName = 'DOC_TYPE'
    end
    object ptCM2DOCUMENT: TIntegerField
      FieldName = 'DOCUMENT'
    end
    object ptCM2PRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ptCM2QUANT_MOVE: TFloatField
      FieldName = 'QUANT_MOVE'
    end
    object ptCM2SUM_MOVE: TFloatField
      FieldName = 'SUM_MOVE'
    end
    object ptCM2QUANT_REST: TFloatField
      FieldName = 'QUANT_REST'
    end
  end
  object ptCn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'RDate;Code'
    TableName = 'cn201005'
    Left = 256
    Top = 136
    object ptCnXZ: TFloatField
      FieldName = 'XZ'
      Required = True
    end
    object ptCnRDate: TDateField
      FieldName = 'RDate'
      Required = True
    end
    object ptCnRPrice: TFloatField
      FieldName = 'RPrice'
      Required = True
    end
    object ptCnDepart: TSmallintField
      FieldName = 'Depart'
      Required = True
    end
    object ptCnCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object ptCnXZ1: TFloatField
      FieldName = 'XZ1'
      Required = True
    end
    object ptCnRSum: TFloatField
      FieldName = 'RSum'
      Required = True
    end
    object ptCnXZ2: TFloatField
      FieldName = 'XZ2'
      Required = True
    end
    object ptCnXZ3: TFloatField
      FieldName = 'XZ3'
      Required = True
    end
    object ptCnQuant: TFloatField
      FieldName = 'Quant'
      Required = True
    end
    object ptCnRSumCor: TFloatField
      FieldName = 'RSumCor'
      Required = True
    end
  end
  object meCg: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 164
    Top = 264
    object meCgCard: TIntegerField
      FieldName = 'Card'
    end
  end
  object ptInLn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;DateInvoice;Number'
    TableName = 'TTNInLn'
    Left = 612
    Top = 436
    object ptInLnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object ptInLnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object ptInLnNumber: TStringField
      FieldName = 'Number'
      Size = 10
      Transliterate = False
    end
    object ptInLnIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object ptInLnCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object ptInLnCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object ptInLnCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object ptInLnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object ptInLnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object ptInLnTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object ptInLnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object ptInLnMediatorCost: TFloatField
      FieldName = 'MediatorCost'
    end
    object ptInLnNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object ptInLnNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object ptInLnOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object ptInLnBestBefore: TDateField
      FieldName = 'BestBefore'
    end
    object ptInLnInvoiceQuant: TFloatField
      FieldName = 'InvoiceQuant'
    end
    object ptInLnKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object ptInLnKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object ptInLnKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object ptInLnKolWithNecond: TFloatField
      FieldName = 'KolWithNecond'
    end
    object ptInLnKol: TFloatField
      FieldName = 'Kol'
    end
    object ptInLnCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object ptInLnProcent: TFloatField
      FieldName = 'Procent'
    end
    object ptInLnNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object ptInLnSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object ptInLnSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object ptInLnProcentN: TFloatField
      FieldName = 'ProcentN'
    end
    object ptInLnNecond: TFloatField
      FieldName = 'Necond'
    end
    object ptInLnCenaNecond: TFloatField
      FieldName = 'CenaNecond'
    end
    object ptInLnSumNecond: TFloatField
      FieldName = 'SumNecond'
    end
    object ptInLnProcentZ: TFloatField
      FieldName = 'ProcentZ'
    end
    object ptInLnZemlia: TFloatField
      FieldName = 'Zemlia'
    end
    object ptInLnProcentO: TFloatField
      FieldName = 'ProcentO'
    end
    object ptInLnOthodi: TFloatField
      FieldName = 'Othodi'
    end
    object ptInLnCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object ptInLnVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object ptInLnCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object ptInLnSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object ptInLnSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object ptInLnChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object ptInLnSertBeginDate: TDateField
      FieldName = 'SertBeginDate'
    end
    object ptInLnSertEndDate: TDateField
      FieldName = 'SertEndDate'
    end
    object ptInLnSertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
  end
  object ptInventryHead: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;DateInventry'
    TableName = 'InventryHead'
    Left = 452
    Top = 136
    object ptInventryHeadInventry: TIntegerField
      FieldName = 'Inventry'
    end
    object ptInventryHeadDepart: TSmallintField
      FieldName = 'Depart'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInventryHeadDateInventry: TDateField
      FieldName = 'DateInventry'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptInventryHeadNumber: TStringField
      FieldName = 'Number'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 10
    end
    object ptInventryHeadCardIndex: TWordField
      FieldName = 'CardIndex'
    end
    object ptInventryHeadInputMode: TWordField
      FieldName = 'InputMode'
    end
    object ptInventryHeadPriceMode: TWordField
      FieldName = 'PriceMode'
    end
    object ptInventryHeadDetail: TWordField
      FieldName = 'Detail'
    end
    object ptInventryHeadGGroup: TSmallintField
      FieldName = 'GGroup'
    end
    object ptInventryHeadSubGroup: TSmallintField
      FieldName = 'SubGroup'
    end
    object ptInventryHeadItem: TIntegerField
      FieldName = 'Item'
    end
    object ptInventryHeadCountLost: TBooleanField
      FieldName = 'CountLost'
    end
    object ptInventryHeadDateLost: TDateField
      FieldName = 'DateLost'
    end
    object ptInventryHeadQuantRecord: TFloatField
      FieldName = 'QuantRecord'
    end
    object ptInventryHeadSumRecord: TFloatField
      FieldName = 'SumRecord'
    end
    object ptInventryHeadQuantActual: TFloatField
      FieldName = 'QuantActual'
    end
    object ptInventryHeadSumActual: TFloatField
      FieldName = 'SumActual'
    end
    object ptInventryHeadNumberOfLines: TIntegerField
      FieldName = 'NumberOfLines'
    end
    object ptInventryHeadDateStart: TDateField
      FieldName = 'DateStart'
    end
    object ptInventryHeadTimeStart: TTimeField
      FieldName = 'TimeStart'
    end
    object ptInventryHeadPresident: TStringField
      FieldName = 'President'
    end
    object ptInventryHeadPresidentPost: TStringField
      FieldName = 'PresidentPost'
    end
    object ptInventryHeadMember1: TStringField
      FieldName = 'Member1'
    end
    object ptInventryHeadMember1_Post: TStringField
      FieldName = 'Member1_Post'
    end
    object ptInventryHeadMember2: TStringField
      FieldName = 'Member2'
    end
    object ptInventryHeadMember2_Post: TStringField
      FieldName = 'Member2_Post'
    end
    object ptInventryHeadMember3: TStringField
      FieldName = 'Member3'
    end
    object ptInventryHeadMember3_Post: TStringField
      FieldName = 'Member3_Post'
      Size = 19
    end
    object ptInventryHeadxDopStatus: TWordField
      FieldName = 'xDopStatus'
    end
    object ptInventryHeadResponse1: TStringField
      FieldName = 'Response1'
    end
    object ptInventryHeadResponse1_Post: TStringField
      FieldName = 'Response1_Post'
    end
    object ptInventryHeadResponse2: TStringField
      FieldName = 'Response2'
    end
    object ptInventryHeadResponse2_Post: TStringField
      FieldName = 'Response2_Post'
    end
    object ptInventryHeadResponse3: TStringField
      FieldName = 'Response3'
    end
    object ptInventryHeadResponse3_Post: TStringField
      FieldName = 'Response3_Post'
    end
    object ptInventryHeadReason: TStringField
      FieldName = 'Reason'
      Size = 49
    end
    object ptInventryHeadCASHER: TStringField
      FieldName = 'CASHER'
      Size = 10
    end
    object ptInventryHeadStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
    object ptInventryHeadLinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object ptInventryHeadStartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object ptInventryHeadEndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object ptInventryHeadRezerv: TStringField
      FieldName = 'Rezerv'
      Size = 27
    end
  end
  object teInventryArt: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 340
    Top = 136
    object teInventryArtArticul: TIntegerField
      FieldName = 'Articul'
    end
  end
  object ptTTK: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID;NUM'
    TableName = 'A_TTK'
    Left = 264
    Top = 264
    object ptTTKID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptTTKNUM: TSmallintField
      FieldName = 'NUM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptTTKIDC: TIntegerField
      FieldName = 'IDC'
    end
    object ptTTKPROC1: TFloatField
      FieldName = 'PROC1'
    end
    object ptTTKPROC2: TFloatField
      FieldName = 'PROC2'
    end
  end
  object ptTTnInLn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'CodeTovar;DateInvoice'
    TableName = 'TTNInLn'
    Left = 476
    Top = 336
    object ptTTnInLnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object ptTTnInLnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object ptTTnInLnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object ptTTnInLnIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object ptTTnInLnCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object ptTTnInLnCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object ptTTnInLnCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object ptTTnInLnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object ptTTnInLnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object ptTTnInLnTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object ptTTnInLnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object ptTTnInLnMediatorCost: TFloatField
      FieldName = 'MediatorCost'
    end
    object ptTTnInLnNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object ptTTnInLnNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object ptTTnInLnOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object ptTTnInLnBestBefore: TDateField
      FieldName = 'BestBefore'
    end
    object ptTTnInLnInvoiceQuant: TFloatField
      FieldName = 'InvoiceQuant'
    end
    object ptTTnInLnKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object ptTTnInLnKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object ptTTnInLnKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object ptTTnInLnKolWithNecond: TFloatField
      FieldName = 'KolWithNecond'
    end
    object ptTTnInLnKol: TFloatField
      FieldName = 'Kol'
    end
    object ptTTnInLnCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object ptTTnInLnProcent: TFloatField
      FieldName = 'Procent'
    end
    object ptTTnInLnNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object ptTTnInLnSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
    end
    object ptTTnInLnSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object ptTTnInLnProcentN: TFloatField
      FieldName = 'ProcentN'
    end
    object ptTTnInLnNecond: TFloatField
      FieldName = 'Necond'
    end
    object ptTTnInLnCenaNecond: TFloatField
      FieldName = 'CenaNecond'
    end
    object ptTTnInLnSumNecond: TFloatField
      FieldName = 'SumNecond'
    end
    object ptTTnInLnProcentZ: TFloatField
      FieldName = 'ProcentZ'
    end
    object ptTTnInLnZemlia: TFloatField
      FieldName = 'Zemlia'
    end
    object ptTTnInLnProcentO: TFloatField
      FieldName = 'ProcentO'
    end
    object ptTTnInLnOthodi: TFloatField
      FieldName = 'Othodi'
    end
    object ptTTnInLnCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object ptTTnInLnVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object ptTTnInLnCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object ptTTnInLnSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object ptTTnInLnSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object ptTTnInLnChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object ptTTnInLnSertBeginDate: TDateField
      FieldName = 'SertBeginDate'
    end
    object ptTTnInLnSertEndDate: TDateField
      FieldName = 'SertEndDate'
    end
    object ptTTnInLnSertNumber: TStringField
      FieldName = 'SertNumber'
      Size = 30
    end
  end
  object ptBufPrice: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDCARD;DATEDOC;NUMDOC'
    TableName = 'A_BUFPRICE'
    Left = 360
    Top = 264
    object ptBufPriceIDCARD: TIntegerField
      FieldName = 'IDCARD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptBufPriceIDDOC: TIntegerField
      FieldName = 'IDDOC'
      Required = True
    end
    object ptBufPriceTYPEDOC: TSmallintField
      FieldName = 'TYPEDOC'
      Required = True
    end
    object ptBufPriceOLDP: TFloatField
      FieldName = 'OLDP'
    end
    object ptBufPriceNEWP: TFloatField
      FieldName = 'NEWP'
    end
    object ptBufPriceSTATUS: TSmallintField
      FieldName = 'STATUS'
    end
    object ptBufPriceNUMDOC: TStringField
      FieldName = 'NUMDOC'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptBufPriceDATEDOC: TDateField
      FieldName = 'DATEDOC'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object ptCassir: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'DateDay;Depart;CassaNumber;CodeKadr'
    TableName = 'CashRegByDay'
    Left = 420
    Top = 264
    object ptCassirDateDay: TDateField
      FieldName = 'DateDay'
    end
    object ptCassirCassaNumber: TIntegerField
      FieldName = 'CassaNumber'
    end
    object ptCassirCodeKadr: TSmallintField
      FieldName = 'CodeKadr'
    end
    object ptCassirDepart: TSmallintField
      FieldName = 'Depart'
    end
    object ptCassirPrihod: TFloatField
      FieldName = 'Prihod'
    end
    object ptCassirTriger: TIntegerField
      FieldName = 'Triger'
    end
    object ptCassirSch1: TFloatField
      FieldName = 'Sch1'
    end
    object ptCassirCor1: TFloatField
      FieldName = 'Cor1'
    end
    object ptCassirSch2: TFloatField
      FieldName = 'Sch2'
    end
    object ptCassirCor2: TFloatField
      FieldName = 'Cor2'
    end
    object ptCassirSch3: TFloatField
      FieldName = 'Sch3'
    end
    object ptCassirCor3: TFloatField
      FieldName = 'Cor3'
    end
    object ptCassirSch4: TFloatField
      FieldName = 'Sch4'
    end
    object ptCassirCor4: TFloatField
      FieldName = 'Cor4'
    end
    object ptCassirSch5: TFloatField
      FieldName = 'Sch5'
    end
    object ptCassirCor5: TFloatField
      FieldName = 'Cor5'
    end
    object ptCassirSch6: TFloatField
      FieldName = 'Sch6'
    end
    object ptCassirCor6: TFloatField
      FieldName = 'Cor6'
    end
    object ptCassirSch7: TFloatField
      FieldName = 'Sch7'
    end
    object ptCassirCor7: TFloatField
      FieldName = 'Cor7'
    end
    object ptCassirSch8: TFloatField
      FieldName = 'Sch8'
    end
    object ptCassirCor8: TFloatField
      FieldName = 'Cor8'
    end
    object ptCassirSch9: TFloatField
      FieldName = 'Sch9'
    end
    object ptCassirCor9: TFloatField
      FieldName = 'Cor9'
    end
    object ptCassirSch10: TFloatField
      FieldName = 'Sch10'
    end
    object ptCassirCor10: TFloatField
      FieldName = 'Cor10'
    end
    object ptCassirSumma: TFloatField
      FieldName = 'Summa'
    end
    object ptCassirCorSumma: TFloatField
      FieldName = 'CorSumma'
    end
    object ptCassirRSch1: TFloatField
      FieldName = 'RSch1'
    end
    object ptCassirRSch2: TFloatField
      FieldName = 'RSch2'
    end
    object ptCassirRSch3: TFloatField
      FieldName = 'RSch3'
    end
    object ptCassirRSch4: TFloatField
      FieldName = 'RSch4'
    end
    object ptCassirRSch5: TFloatField
      FieldName = 'RSch5'
    end
    object ptCassirRSch6: TFloatField
      FieldName = 'RSch6'
    end
    object ptCassirRSch7: TFloatField
      FieldName = 'RSch7'
    end
    object ptCassirRSch8: TFloatField
      FieldName = 'RSch8'
    end
    object ptCassirRSch9: TFloatField
      FieldName = 'RSch9'
    end
    object ptCassirRSch10: TFloatField
      FieldName = 'RSch10'
    end
    object ptCassirRSumma: TFloatField
      FieldName = 'RSumma'
    end
    object ptCassirOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
    object ptCassirSummaNDSx10: TFloatField
      FieldName = 'SummaNDSx10'
    end
    object ptCassirSummaNDSx20: TFloatField
      FieldName = 'SummaNDSx20'
    end
    object ptCassirCorNDSx10: TFloatField
      FieldName = 'CorNDSx10'
    end
    object ptCassirCorNDSx20: TFloatField
      FieldName = 'CorNDSx20'
    end
    object ptCassirSummaNSPx10: TFloatField
      FieldName = 'SummaNSPx10'
    end
    object ptCassirSummaNSPx20: TFloatField
      FieldName = 'SummaNSPx20'
    end
    object ptCassirSummaNDSx0: TFloatField
      FieldName = 'SummaNDSx0'
    end
    object ptCassirCorNDSx0: TFloatField
      FieldName = 'CorNDSx0'
    end
    object ptCassirSummaNSP0: TFloatField
      FieldName = 'SummaNSP0'
    end
    object ptCassirNDSx10: TFloatField
      FieldName = 'NDSx10'
    end
    object ptCassirNDSx20: TFloatField
      FieldName = 'NDSx20'
    end
    object ptCassirCorNSPx0: TFloatField
      FieldName = 'CorNSPx0'
    end
    object ptCassirCorNSPx10: TFloatField
      FieldName = 'CorNSPx10'
    end
    object ptCassirCorNSPx20: TFloatField
      FieldName = 'CorNSPx20'
    end
    object ptCassirRezerv: TFloatField
      FieldName = 'Rezerv'
    end
    object ptCassirShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
  end
  object ptCliGoods: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ClientObjTypeID;ClientID;GoodsID'
    TableName = 'GdsClient'
    Left = 264
    Top = 328
    object ptCliGoodsGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object ptCliGoodsClientObjTypeID: TSmallintField
      FieldName = 'ClientObjTypeID'
    end
    object ptCliGoodsClientID: TSmallintField
      FieldName = 'ClientID'
    end
    object ptCliGoodsCode: TIntegerField
      FieldName = 'Code'
    end
    object ptCliGoodsBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object ptCliGoodsQuant: TFloatField
      FieldName = 'Quant'
    end
    object ptCliGoodsBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object ptCliGoodsxDate: TDateField
      FieldName = 'xDate'
    end
    object ptCliGoodsBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object ptCliGoodsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptCliGoodsStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object ptTTNIn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;DateInvoice;Number'
    TableName = 'TTNIn'
    Left = 360
    Top = 336
    object ptTTNInDepart: TSmallintField
      FieldName = 'Depart'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptTTNInDateInvoice: TDateField
      FieldName = 'DateInvoice'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptTTNInNumber: TStringField
      FieldName = 'Number'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 10
    end
    object ptTTNInSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object ptTTNInIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object ptTTNInCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object ptTTNInID: TIntegerField
      FieldName = 'ID'
    end
    object ptTTNInAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object ptTTNInSummaTovarPost: TFloatField
      FieldName = 'SummaTovarPost'
    end
    object ptTTNInOblNDSTovar: TFloatField
      FieldName = 'OblNDSTovar'
    end
    object ptTTNInLgtNDSTovar: TFloatField
      FieldName = 'LgtNDSTovar'
    end
    object ptTTNInNotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
    object ptTTNInNDS10: TFloatField
      FieldName = 'NDS10'
    end
    object ptTTNInOutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object ptTTNInNDS20: TFloatField
      FieldName = 'NDS20'
    end
    object ptTTNInOutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object ptTTNInLgtNDS: TFloatField
      FieldName = 'LgtNDS'
    end
    object ptTTNInOutLgtNDS: TFloatField
      FieldName = 'OutLgtNDS'
    end
    object ptTTNInNDSTovar: TFloatField
      FieldName = 'NDSTovar'
    end
    object ptTTNInOutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object ptTTNInReturnTovar: TFloatField
      FieldName = 'ReturnTovar'
    end
    object ptTTNInOutNDSSNTovar: TFloatField
      FieldName = 'OutNDSSNTovar'
    end
    object ptTTNInBoy: TFloatField
      FieldName = 'Boy'
    end
    object ptTTNInOthodiPost: TFloatField
      FieldName = 'OthodiPost'
    end
    object ptTTNInDolgPost: TFloatField
      FieldName = 'DolgPost'
    end
    object ptTTNInSummaTovar: TFloatField
      FieldName = 'SummaTovar'
    end
    object ptTTNInNacenka: TFloatField
      FieldName = 'Nacenka'
    end
    object ptTTNInTransport: TFloatField
      FieldName = 'Transport'
    end
    object ptTTNInNDSTransport: TFloatField
      FieldName = 'NDSTransport'
    end
    object ptTTNInReturnTara: TFloatField
      FieldName = 'ReturnTara'
    end
    object ptTTNInStrah: TFloatField
      FieldName = 'Strah'
    end
    object ptTTNInSummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object ptTTNInAmortTara: TFloatField
      FieldName = 'AmortTara'
    end
    object ptTTNInChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object ptTTNInProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object ptTTNInNotNDS10: TFloatField
      FieldName = 'NotNDS10'
    end
    object ptTTNInNotNDS20: TFloatField
      FieldName = 'NotNDS20'
    end
    object ptTTNInWithNDS10: TFloatField
      FieldName = 'WithNDS10'
    end
    object ptTTNInWithNDS20: TFloatField
      FieldName = 'WithNDS20'
    end
    object ptTTNInCrock: TFloatField
      FieldName = 'Crock'
    end
    object ptTTNInOthodiPoluch: TFloatField
      FieldName = 'OthodiPoluch'
    end
    object ptTTNInDiscount: TFloatField
      FieldName = 'Discount'
    end
    object ptTTNInNDS010: TFloatField
      FieldName = 'NDS010'
    end
    object ptTTNInNDS020: TFloatField
      FieldName = 'NDS020'
    end
    object ptTTNInNDS_10: TFloatField
      FieldName = 'NDS_10'
    end
    object ptTTNInNDS_20: TFloatField
      FieldName = 'NDS_20'
    end
    object ptTTNInNSP_10: TFloatField
      FieldName = 'NSP_10'
    end
    object ptTTNInNSP_20: TFloatField
      FieldName = 'NSP_20'
    end
    object ptTTNInSCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object ptTTNInGoodsNSP0: TFloatField
      FieldName = 'GoodsNSP0'
    end
    object ptTTNInGoodsCostNSP: TFloatField
      FieldName = 'GoodsCostNSP'
    end
    object ptTTNInOrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object ptTTNInLinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object ptTTNInStartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object ptTTNInEndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object ptTTNInGoodsWasteNDS0: TFloatField
      FieldName = 'GoodsWasteNDS0'
    end
    object ptTTNInGoodsWasteNDS10: TFloatField
      FieldName = 'GoodsWasteNDS10'
    end
    object ptTTNInGoodsWasteNDS20: TFloatField
      FieldName = 'GoodsWasteNDS20'
    end
    object ptTTNInREZERV: TStringField
      FieldName = 'REZERV'
      Size = 4
    end
  end
  object ptACValue: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    TableName = 'ACValue'
    Left = 688
    Top = 220
    object ptACValueID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptACValueAddCharID: TIntegerField
      FieldName = 'AddCharID'
    end
    object ptACValueACValue: TStringField
      FieldName = 'ACValue'
      Size = 30
    end
  end
  object ptZadan: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexName = 'PK_Id'
    TableName = 'A_ZADANIYA'
    Left = 416
    Top = 336
    object ptZadanId: TBCDField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 19
      Size = 0
    end
    object ptZadanIType: TSmallintField
      FieldName = 'IType'
    end
    object ptZadanComment: TStringField
      FieldName = 'Comment'
      Size = 200
    end
    object ptZadanStatus: TSmallintField
      FieldName = 'Status'
    end
    object ptZadanZDate: TDateField
      FieldName = 'ZDate'
    end
    object ptZadanZTime: TTimeField
      FieldName = 'ZTime'
    end
    object ptZadanPersonId: TIntegerField
      FieldName = 'PersonId'
    end
    object ptZadanPersonName: TStringField
      FieldName = 'PersonName'
      Size = 50
    end
    object ptZadanFormDate: TDateField
      FieldName = 'FormDate'
    end
    object ptZadanFormTime: TTimeField
      FieldName = 'FormTime'
    end
    object ptZadanIdDoc: TBCDField
      FieldName = 'IdDoc'
      Precision = 19
      Size = 0
    end
  end
  object ptCashs: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    OnCalcFields = ptCashsCalcFields
    IndexFieldNames = 'Number'
    TableName = 'RCashRegister'
    Left = 540
    Top = 324
    object ptCashsNumber: TIntegerField
      FieldName = 'Number'
    end
    object ptCashsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptCashsShRealiz: TIntegerField
      FieldName = 'ShRealiz'
    end
    object ptCashsAddGoods: TIntegerField
      FieldName = 'AddGoods'
    end
    object ptCashsTechnolog: TIntegerField
      FieldName = 'Technolog'
    end
    object ptCashsFlags: TSmallintField
      FieldName = 'Flags'
    end
    object ptCashsRezerv: TFloatField
      FieldName = 'Rezerv'
    end
    object ptCashsShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object ptCashsLoad: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Load'
      Calculated = True
    end
    object ptCashsTake: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Take'
      Calculated = True
    end
  end
  object ptGdsLoad: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Code;IDate;Id'
    TableName = 'A_GDSLOAD'
    Left = 600
    Top = 320
    object ptGdsLoadCode: TIntegerField
      FieldName = 'Code'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadIDate: TIntegerField
      FieldName = 'IDate'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadId: TIntegerField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadDateLoad: TDateField
      FieldName = 'DateLoad'
    end
    object ptGdsLoadTimeLoad: TTimeField
      FieldName = 'TimeLoad'
    end
    object ptGdsLoadCType: TStringField
      FieldName = 'CType'
      Size = 1
    end
    object ptGdsLoadPrice: TFloatField
      FieldName = 'Price'
    end
    object ptGdsLoadDisc: TFloatField
      FieldName = 'Disc'
    end
    object ptGdsLoadPerson: TStringField
      FieldName = 'Person'
      Size = 50
    end
  end
  object ptGdsLoadSel: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Code;IDate;Id'
    TableName = 'A_GDSLOAD'
    Left = 684
    Top = 320
    object ptGdsLoadSelCode: TIntegerField
      FieldName = 'Code'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadSelIDate: TIntegerField
      FieldName = 'IDate'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadSelId: TIntegerField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsLoadSelDateLoad: TDateField
      FieldName = 'DateLoad'
    end
    object ptGdsLoadSelTimeLoad: TTimeField
      FieldName = 'TimeLoad'
    end
    object ptGdsLoadSelCType: TStringField
      FieldName = 'CType'
      Size = 1
    end
    object ptGdsLoadSelPrice: TFloatField
      FieldName = 'Price'
    end
    object ptGdsLoadSelDisc: TFloatField
      FieldName = 'Disc'
    end
    object ptGdsLoadSelPerson: TStringField
      FieldName = 'Person'
      Size = 50
    end
  end
  object dsptGdsLoadSel: TDataSource
    DataSet = ptGdsLoadSel
    Left = 684
    Top = 376
  end
  object ptCateg: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_CATEG'
    Left = 520
    Top = 76
    object ptCategID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCategSID: TStringField
      FieldName = 'SID'
      Size = 2
    end
    object ptCategCOMMENT: TStringField
      FieldName = 'COMMENT'
    end
  end
  object ptCashDCrd: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ShopIndex;CashNumber;ZNumber;CheckNumber;CardType;CardNumber'
    TableName = 'CashDCrd'
    Left = 540
    Top = 380
    object ptCashDCrdShopIndex: TSmallintField
      FieldName = 'ShopIndex'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCashDCrdCashNumber: TSmallintField
      FieldName = 'CashNumber'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCashDCrdZNumber: TSmallintField
      FieldName = 'ZNumber'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCashDCrdCheckNumber: TIntegerField
      FieldName = 'CheckNumber'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCashDCrdCardType: TSmallintField
      FieldName = 'CardType'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCashDCrdCardNumber: TStringField
      FieldName = 'CardNumber'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 22
    end
    object ptCashDCrdDiscountRub: TFloatField
      FieldName = 'DiscountRub'
    end
    object ptCashDCrdDiscountCur: TFloatField
      FieldName = 'DiscountCur'
    end
    object ptCashDCrdCasher: TStringField
      FieldName = 'Casher'
      Size = 10
    end
    object ptCashDCrdDateSale: TDateField
      FieldName = 'DateSale'
    end
    object ptCashDCrdTimeSale: TTimeField
      FieldName = 'TimeSale'
    end
  end
  object ptZRep: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ZDate;CashNumber'
    TableName = 'Zreport'
    Left = 692
    Top = 128
    object ptZRepCashNumber: TIntegerField
      FieldName = 'CashNumber'
    end
    object ptZRepZNumber: TIntegerField
      FieldName = 'ZNumber'
    end
    object ptZRepZSale: TFloatField
      FieldName = 'ZSale'
    end
    object ptZRepZReturn: TFloatField
      FieldName = 'ZReturn'
    end
    object ptZRepZDiscount: TFloatField
      FieldName = 'ZDiscount'
    end
    object ptZRepZDate: TDateField
      FieldName = 'ZDate'
    end
    object ptZRepZTime: TTimeField
      FieldName = 'ZTime'
    end
    object ptZRepZCrSale: TFloatField
      FieldName = 'ZCrSale'
    end
    object ptZRepZCrReturn: TFloatField
      FieldName = 'ZCrReturn'
    end
    object ptZRepZCrDiscount: TFloatField
      FieldName = 'ZCrDiscount'
    end
    object ptZRepTSale: TFloatField
      FieldName = 'TSale'
    end
    object ptZRepTReturn: TFloatField
      FieldName = 'TReturn'
    end
    object ptZRepCrSale: TFloatField
      FieldName = 'CrSale'
    end
    object ptZRepCrReturn: TFloatField
      FieldName = 'CrReturn'
    end
    object ptZRepCurMoney: TFloatField
      FieldName = 'CurMoney'
    end
  end
  object ptPartIN: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDSTORE;ARTICUL;IDATE'
    TableName = 'A_PARTIN'
    Left = 820
    Top = 16
    object ptPartINID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object ptPartINIDSTORE: TSmallintField
      FieldName = 'IDSTORE'
    end
    object ptPartINIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object ptPartINIDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object ptPartINARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object ptPartINSINNCLI: TStringField
      FieldName = 'SINNCLI'
    end
    object ptPartINDTYPE: TSmallintField
      FieldName = 'DTYPE'
    end
    object ptPartINQPART: TFloatField
      FieldName = 'QPART'
    end
    object ptPartINQREMN: TFloatField
      FieldName = 'QREMN'
    end
    object ptPartINPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object ptPartINPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
    end
    object ptPartINPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object ptPartINIDC: TIntegerField
      FieldName = 'IDC'
    end
  end
  object ptDExchH: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_DOCEXCHHEAD'
    Left = 768
    Top = 372
    object ptDExchHID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptDExchHDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object ptDExchHDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object ptDExchHDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object ptDExchHITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object ptDExchHINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object ptDExchHINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
    end
    object ptDExchHOUTSUMIN: TFloatField
      FieldName = 'OUTSUMIN'
    end
    object ptDExchHOUTSUMOUT: TFloatField
      FieldName = 'OUTSUMOUT'
    end
    object ptDExchHCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object ptDExchHIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object ptDExchHCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
  end
  object ptDExchS: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDH;IDS'
    TableName = 'A_DOCEXCHSPEC'
    Left = 764
    Top = 316
    object ptDExchSIDH: TIntegerField
      FieldName = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptDExchSIDS: TAutoIncField
      FieldName = 'IDS'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptDExchSSTYPE: TSmallintField
      FieldName = 'STYPE'
    end
    object ptDExchSCODE: TIntegerField
      FieldName = 'CODE'
    end
    object ptDExchSQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object ptDExchSPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object ptDExchSPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
    end
    object ptDExchSSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object ptDExchSSUMOUT: TFloatField
      FieldName = 'SUMOUT'
    end
    object ptDExchSNAC: TFloatField
      FieldName = 'NAC'
    end
  end
  object ptCli: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'INN'
    TableName = 'RVendor'
    Left = 820
    Top = 132
    object ptCliVendor: TSmallintField
      FieldName = 'Vendor'
      Required = True
    end
    object ptCliName: TStringField
      FieldName = 'Name'
      Required = True
      Size = 30
    end
    object ptCliPayDays: TSmallintField
      FieldName = 'PayDays'
      Required = True
    end
    object ptCliPayForm: TWordField
      FieldName = 'PayForm'
      Required = True
    end
    object ptCliPayWaste: TWordField
      FieldName = 'PayWaste'
      Required = True
    end
    object ptCliFirmName: TStringField
      FieldName = 'FirmName'
      Required = True
      Size = 6
    end
    object ptCliPhone1: TStringField
      FieldName = 'Phone1'
      Required = True
      Size = 16
    end
    object ptCliPhone2: TStringField
      FieldName = 'Phone2'
      Required = True
      Size = 16
    end
    object ptCliPhone3: TStringField
      FieldName = 'Phone3'
      Required = True
      Size = 16
    end
    object ptCliPhone4: TStringField
      FieldName = 'Phone4'
      Required = True
      Size = 16
    end
    object ptCliFax: TStringField
      FieldName = 'Fax'
      Required = True
      Size = 16
    end
    object ptCliTreatyDate: TDateField
      FieldName = 'TreatyDate'
      Required = True
    end
    object ptCliTreatyNumber: TStringField
      FieldName = 'TreatyNumber'
      Required = True
      Size = 16
    end
    object ptCliPostIndex: TStringField
      FieldName = 'PostIndex'
      Required = True
      Size = 6
    end
    object ptCliGorod: TStringField
      FieldName = 'Gorod'
      Required = True
      Size = 30
    end
    object ptCliRaion: TSmallintField
      FieldName = 'Raion'
      Required = True
    end
    object ptCliStreet: TStringField
      FieldName = 'Street'
      Required = True
      Size = 30
    end
    object ptCliHouse: TStringField
      FieldName = 'House'
      Required = True
      Size = 6
    end
    object ptCliBuild: TStringField
      FieldName = 'Build'
      Required = True
      Size = 3
    end
    object ptCliKvart: TStringField
      FieldName = 'Kvart'
      Required = True
      Size = 4
    end
    object ptCliOKPO: TStringField
      FieldName = 'OKPO'
      Required = True
      Size = 10
    end
    object ptCliOKONH: TStringField
      FieldName = 'OKONH'
      Required = True
      Size = 50
    end
    object ptCliINN: TStringField
      FieldName = 'INN'
      Required = True
    end
    object ptCliFullName: TStringField
      FieldName = 'FullName'
      Required = True
      Size = 100
    end
    object ptCliCodeUchet: TStringField
      FieldName = 'CodeUchet'
      Required = True
    end
    object ptCliUnTaxedNDS: TWordField
      FieldName = 'UnTaxedNDS'
      Required = True
    end
    object ptCliStatusByte: TWordField
      FieldName = 'StatusByte'
      Required = True
    end
    object ptCliStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
  end
  object ptIP: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'INN'
    TableName = 'RIndividual'
    Left = 820
    Top = 184
    object ptIPCode: TSmallintField
      FieldName = 'Code'
    end
    object ptIPName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptIPFirmName: TStringField
      FieldName = 'FirmName'
      Size = 10
    end
    object ptIPFamilia: TStringField
      FieldName = 'Familia'
    end
    object ptIPIma: TStringField
      FieldName = 'Ima'
      Size = 15
    end
    object ptIPOtchestvo: TStringField
      FieldName = 'Otchestvo'
      Size = 15
    end
    object ptIPVidDok: TStringField
      FieldName = 'VidDok'
      Size = 30
    end
    object ptIPSerPasp: TStringField
      FieldName = 'SerPasp'
      Size = 10
    end
    object ptIPNumberPasp: TIntegerField
      FieldName = 'NumberPasp'
    end
    object ptIPKemPasp: TStringField
      FieldName = 'KemPasp'
      Size = 60
    end
    object ptIPDatePasp: TDateField
      FieldName = 'DatePasp'
    end
    object ptIPCountry: TStringField
      FieldName = 'Country'
      Size = 30
    end
    object ptIPPhone1: TStringField
      FieldName = 'Phone1'
      Size = 16
    end
    object ptIPPhone2: TStringField
      FieldName = 'Phone2'
      Size = 16
    end
    object ptIPPhone3: TStringField
      FieldName = 'Phone3'
      Size = 16
    end
    object ptIPPhone4: TStringField
      FieldName = 'Phone4'
      Size = 16
    end
    object ptIPFax: TStringField
      FieldName = 'Fax'
      Size = 16
    end
    object ptIPEMail: TStringField
      FieldName = 'EMail'
    end
    object ptIPPostIndex: TStringField
      FieldName = 'PostIndex'
      Size = 6
    end
    object ptIPGorod: TStringField
      FieldName = 'Gorod'
      Size = 30
    end
    object ptIPRaion: TSmallintField
      FieldName = 'Raion'
    end
    object ptIPStreet: TStringField
      FieldName = 'Street'
      Size = 30
    end
    object ptIPHouse: TStringField
      FieldName = 'House'
      Size = 6
    end
    object ptIPBuild: TStringField
      FieldName = 'Build'
      Size = 3
    end
    object ptIPKvart: TStringField
      FieldName = 'Kvart'
      Size = 4
    end
    object ptIPINN: TStringField
      FieldName = 'INN'
    end
    object ptIPNStrach: TStringField
      FieldName = 'NStrach'
      Size = 14
    end
    object ptIPNumberSvid: TStringField
      FieldName = 'NumberSvid'
    end
    object ptIPDateSvid: TDateField
      FieldName = 'DateSvid'
    end
    object ptIPFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object ptIPDateBorn: TDateField
      FieldName = 'DateBorn'
    end
    object ptIPCodeUchet: TStringField
      FieldName = 'CodeUchet'
    end
    object ptIPStatus: TIntegerField
      FieldName = 'Status'
    end
  end
  object ptPartO: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ITYPE;IDDOC;ARTICUL'
    TableName = 'A_PARTOUT'
    Left = 820
    Top = 68
    object ptPartOARTICUL: TIntegerField
      FieldName = 'ARTICUL'
      Required = True
    end
    object ptPartOIDATE: TIntegerField
      FieldName = 'IDATE'
      Required = True
    end
    object ptPartOIDSTORE: TSmallintField
      FieldName = 'IDSTORE'
      Required = True
    end
    object ptPartOITYPE: TSmallintField
      FieldName = 'ITYPE'
      Required = True
    end
    object ptPartOIDDOC: TIntegerField
      FieldName = 'IDDOC'
      Required = True
    end
    object ptPartOPARTIN: TIntegerField
      FieldName = 'PARTIN'
      Required = True
    end
    object ptPartOID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object ptPartOQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object ptPartOSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object ptPartOSUMOUT: TFloatField
      FieldName = 'SUMOUT'
    end
    object ptPartOSINN: TStringField
      FieldName = 'SINN'
    end
    object ptPartOSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object ptPartOIDC: TIntegerField
      FieldName = 'IDC'
    end
  end
  object ptPool: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'CodeObject;Id'
    TableName = 'IdPool'
    Left = 256
    Top = 192
    object ptPoolCodeObject: TSmallintField
      FieldName = 'CodeObject'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptPoolId: TIntegerField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object ptInvLine1: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDH;ITEM'
    TableName = 'A_INVLN'
    Left = 452
    Top = 192
    object ptInvLine1IDH: TBCDField
      FieldName = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 19
      Size = 0
    end
    object ptInvLine1ITEM: TIntegerField
      FieldName = 'ITEM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptInvLine1QUANTR: TFloatField
      FieldName = 'QUANTR'
    end
    object ptInvLine1QUANTF: TFloatField
      FieldName = 'QUANTF'
    end
    object ptInvLine1QUANTD: TFloatField
      FieldName = 'QUANTD'
    end
    object ptInvLine1PRICER: TFloatField
      FieldName = 'PRICER'
    end
    object ptInvLine1SUMR: TFloatField
      FieldName = 'SUMR'
    end
    object ptInvLine1SUMF: TFloatField
      FieldName = 'SUMF'
    end
    object ptInvLine1SUMD: TFloatField
      FieldName = 'SUMD'
    end
    object ptInvLine1SUMRSS: TFloatField
      FieldName = 'SUMRSS'
    end
    object ptInvLine1SUMFSS: TFloatField
      FieldName = 'SUMFSS'
    end
    object ptInvLine1SUMDSS: TFloatField
      FieldName = 'SUMDSS'
    end
    object ptInvLine1NUM: TIntegerField
      FieldName = 'NUM'
    end
    object ptInvLine1QUANTC: TFloatField
      FieldName = 'QUANTC'
    end
    object ptInvLine1SUMC: TFloatField
      FieldName = 'SUMC'
    end
    object ptInvLine1SUMRSC: TFloatField
      FieldName = 'SUMRSC'
    end
  end
  object ptAkc: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Code;AType;DateB;DateE'
    TableName = 'A_AKCIYA'
    Left = 264
    Top = 396
    object ptAkcCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object ptAkcAType: TSmallintField
      FieldName = 'AType'
      Required = True
    end
    object ptAkcDateB: TDateField
      FieldName = 'DateB'
      Required = True
    end
    object ptAkcDateE: TDateField
      FieldName = 'DateE'
      Required = True
    end
    object ptAkcPrice: TFloatField
      FieldName = 'Price'
    end
    object ptAkcDStop: TSmallintField
      FieldName = 'DStop'
    end
    object ptAkcDProc: TFloatField
      FieldName = 'DProc'
    end
    object ptAkcStatusOn: TSmallintField
      FieldName = 'StatusOn'
    end
    object ptAkcStatusOf: TSmallintField
      FieldName = 'StatusOf'
    end
    object ptAkcOldPrice: TFloatField
      FieldName = 'OldPrice'
    end
    object ptAkcOldDStop: TSmallintField
      FieldName = 'OldDStop'
    end
    object ptAkcOldDProc: TFloatField
      FieldName = 'OldDProc'
    end
  end
  object ptVnLn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'CodeTovar;DateInvoice'
    TableName = 'TTNSelfLn'
    Left = 692
    Top = 268
    object ptVnLnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object ptVnLnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object ptVnLnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object ptVnLnCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object ptVnLnCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object ptVnLnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object ptVnLnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object ptVnLnTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object ptVnLnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object ptVnLnKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object ptVnLnKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object ptVnLnKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object ptVnLnKol: TFloatField
      FieldName = 'Kol'
    end
    object ptVnLnCenaTovarMove: TFloatField
      FieldName = 'CenaTovarMove'
    end
    object ptVnLnCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object ptVnLnProcent: TFloatField
      FieldName = 'Procent'
    end
    object ptVnLnNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
    end
    object ptVnLnSumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object ptVnLnSumNewCenaTovar: TFloatField
      FieldName = 'SumNewCenaTovar'
    end
    object ptVnLnCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object ptVnLnVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object ptVnLnCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object ptVnLnSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object ptVnLnSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object ptVnLnChekBuhPrihod: TBooleanField
      FieldName = 'ChekBuhPrihod'
    end
    object ptVnLnChekBuhRashod: TBooleanField
      FieldName = 'ChekBuhRashod'
    end
  end
  object ptBar: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'GoodsID'
    TableName = 'BarCode'
    Left = 148
    Top = 120
    object ptBarGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object ptBarID: TStringField
      FieldName = 'ID'
      Size = 13
    end
    object ptBarQuant: TFloatField
      FieldName = 'Quant'
    end
    object ptBarBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object ptBarDateChange: TDateField
      FieldName = 'DateChange'
    end
    object ptBarBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object ptBarCost: TFloatField
      FieldName = 'Cost'
    end
    object ptBarStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object ptTO: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Otdel;xDate'
    TableName = 'REPORTS'
    Left = 828
    Top = 316
    object ptTOOtdel: TSmallintField
      FieldName = 'Otdel'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptTOxDate: TDateField
      FieldName = 'xDate'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptTOOldOstatokTovar: TFloatField
      FieldName = 'OldOstatokTovar'
      Required = True
    end
    object ptTOPrihodTovar: TFloatField
      FieldName = 'PrihodTovar'
      Required = True
    end
    object ptTOSelfPrihodTovar: TFloatField
      FieldName = 'SelfPrihodTovar'
      Required = True
    end
    object ptTOPrihodTovarSumma: TFloatField
      FieldName = 'PrihodTovarSumma'
      Required = True
    end
    object ptTORashodTovar: TFloatField
      FieldName = 'RashodTovar'
      Required = True
    end
    object ptTOSelfRashodTovar: TFloatField
      FieldName = 'SelfRashodTovar'
      Required = True
    end
    object ptTORealTovar: TFloatField
      FieldName = 'RealTovar'
      Required = True
    end
    object ptTOSkidkaTovar: TFloatField
      FieldName = 'SkidkaTovar'
      Required = True
    end
    object ptTOSenderSumma: TFloatField
      FieldName = 'SenderSumma'
      Required = True
    end
    object ptTOSenderSkidka: TFloatField
      FieldName = 'SenderSkidka'
      Required = True
    end
    object ptTOOutTovar: TFloatField
      FieldName = 'OutTovar'
      Required = True
    end
    object ptTORashodTovarSumma: TFloatField
      FieldName = 'RashodTovarSumma'
      Required = True
    end
    object ptTOPereoTovarSumma: TFloatField
      FieldName = 'PereoTovarSumma'
      Required = True
    end
    object ptTONewOstatokTovar: TFloatField
      FieldName = 'NewOstatokTovar'
      Required = True
    end
    object ptTOOldOstatokTara: TFloatField
      FieldName = 'OldOstatokTara'
      Required = True
    end
    object ptTOPrihodTara: TFloatField
      FieldName = 'PrihodTara'
      Required = True
    end
    object ptTOSelfPrihodTara: TFloatField
      FieldName = 'SelfPrihodTara'
      Required = True
    end
    object ptTOPrihodTaraSumma: TFloatField
      FieldName = 'PrihodTaraSumma'
      Required = True
    end
    object ptTORashodTara: TFloatField
      FieldName = 'RashodTara'
      Required = True
    end
    object ptTOSelfRashodTara: TFloatField
      FieldName = 'SelfRashodTara'
      Required = True
    end
    object ptTORezervSumma: TFloatField
      FieldName = 'RezervSumma'
      Required = True
    end
    object ptTORezervSkidka: TFloatField
      FieldName = 'RezervSkidka'
      Required = True
    end
    object ptTOOutTara: TFloatField
      FieldName = 'OutTara'
      Required = True
    end
    object ptTORashodTaraSumma: TFloatField
      FieldName = 'RashodTaraSumma'
      Required = True
    end
    object ptTOxRezerv: TFloatField
      FieldName = 'xRezerv'
      Required = True
    end
    object ptTONewOstatokTara: TFloatField
      FieldName = 'NewOstatokTara'
      Required = True
    end
    object ptTOOprihod: TBooleanField
      FieldName = 'Oprihod'
      Required = True
    end
  end
  object ptGdsCli: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ClientTypeId;ClientId;GoodsId'
    TableName = 'A_GDSCLI'
    Left = 372
    Top = 412
    object ptGdsCliClientTypeId: TSmallintField
      FieldName = 'ClientTypeId'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsCliClientId: TIntegerField
      FieldName = 'ClientId'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsCliGoodsId: TIntegerField
      FieldName = 'GoodsId'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGdsCliPriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object ptGdsCliQuantIn: TFloatField
      FieldName = 'QuantIn'
    end
    object ptGdsClixDate: TDateField
      FieldName = 'xDate'
    end
    object ptGdsClisMess: TStringField
      FieldName = 'sMess'
      Size = 50
    end
  end
  object ptZHead: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_DOCZHEAD'
    Left = 56
    Top = 400
    object ptZHeadID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptZHeadDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object ptZHeadDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object ptZHeadDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object ptZHeadITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object ptZHeadINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object ptZHeadINSUMOUT: TFloatField
      FieldName = 'INSUMOUT'
    end
    object ptZHeadOUTSUMIN: TFloatField
      FieldName = 'OUTSUMIN'
    end
    object ptZHeadOUTSUMOUT: TFloatField
      FieldName = 'OUTSUMOUT'
    end
    object ptZHeadCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object ptZHeadCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object ptZHeadIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object ptZHeadDATEEDIT: TDateField
      FieldName = 'DATEEDIT'
    end
    object ptZHeadPERSON: TStringField
      FieldName = 'PERSON'
      Size = 100
    end
    object ptZHeadTIMEEDIT: TStringField
      FieldName = 'TIMEEDIT'
      Size = 10
    end
    object ptZHeadCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object ptZHeadCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object ptZHeadCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object ptZHeadCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object ptZHeadCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
    end
    object ptZHeadCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
    end
    object ptZHeadIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object ptZHeadSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object ptZHeadIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object ptZHeadSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
  end
  object ptZSpec: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'CODE;IDH'
    TableName = 'A_DOCZSPEC'
    Left = 56
    Top = 460
    object ptZSpecIDH: TIntegerField
      FieldName = 'IDH'
      Required = True
    end
    object ptZSpecIDS: TAutoIncField
      FieldName = 'IDS'
      Required = True
    end
    object ptZSpecCODE: TIntegerField
      FieldName = 'CODE'
    end
    object ptZSpecQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object ptZSpecPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object ptZSpecSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object ptZSpecNDSPROC: TFloatField
      FieldName = 'NDSPROC'
    end
    object ptZSpecREMN1: TFloatField
      FieldName = 'REMN1'
    end
    object ptZSpecVREAL: TFloatField
      FieldName = 'VREAL'
    end
    object ptZSpecREMNDAY: TFloatField
      FieldName = 'REMNDAY'
    end
    object ptZSpecREMNMIN: TIntegerField
      FieldName = 'REMNMIN'
    end
    object ptZSpecREMNMAX: TIntegerField
      FieldName = 'REMNMAX'
    end
    object ptZSpecREMN2: TFloatField
      FieldName = 'REMN2'
    end
    object ptZSpecQUANT1: TFloatField
      FieldName = 'QUANT1'
    end
    object ptZSpecPK: TFloatField
      FieldName = 'PK'
    end
    object ptZSpecQUANTZ: TFloatField
      FieldName = 'QUANTZ'
      DisplayFormat = '0.000'
    end
    object ptZSpecQUANT2: TFloatField
      FieldName = 'QUANT2'
    end
    object ptZSpecQUANT3: TFloatField
      FieldName = 'QUANT3'
    end
    object ptZSpecQUANTPODTV: TFloatField
      FieldName = 'QUANTPODTV'
    end
    object ptZSpecCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object ptZSpecCLIPRICE: TFloatField
      FieldName = 'CLIPRICE'
    end
    object ptZSpecCLIPRICE0: TFloatField
      FieldName = 'CLIPRICE0'
    end
    object ptZSpecCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object ptZSpecCLIPRICEN: TFloatField
      FieldName = 'CLIPRICEN'
    end
    object ptZSpecCLIPRICE0N: TFloatField
      FieldName = 'CLIPRICE0N'
    end
    object ptZSpecCLINNUM: TIntegerField
      FieldName = 'CLINNUM'
    end
  end
  object teAvSp: TdxMemData
    Indexes = <>
    SortOptions = []
    OnCalcFields = teAvSpCalcFields
    Left = 156
    Top = 400
    object teAvSpiDep: TIntegerField
      FieldName = 'iDep'
    end
    object teAvSpiPrih: TIntegerField
      FieldName = 'iPrih'
    end
    object teAvSpiDateP: TIntegerField
      FieldName = 'iDateP'
    end
    object teAvSpQIn: TFloatField
      FieldName = 'QIn'
      DisplayFormat = '0.000'
    end
    object teAvSpQReal: TFloatField
      FieldName = 'QReal'
      DisplayFormat = '0.000'
    end
    object teAvSpQRemn: TFloatField
      FieldName = 'QRemn'
      DisplayFormat = '0.000'
    end
    object teAvSpiDateReal: TIntegerField
      FieldName = 'iDateReal'
    end
    object teAvSprAvrR: TFloatField
      FieldName = 'rAvrR'
      DisplayFormat = '0.000'
    end
    object teAvSpkDov: TFloatField
      FieldName = 'kDov'
    end
    object teAvSpdDateP: TStringField
      FieldKind = fkCalculated
      FieldName = 'dDateP'
      Calculated = True
    end
  end
  object dsteAvSp: TDataSource
    DataSet = teAvSp
    Left = 156
    Top = 456
  end
  object ptScale: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    TableName = 'SprScale'
    Left = 264
    Top = 456
    object ptScaleNumber: TStringField
      FieldName = 'Number'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 10
    end
    object ptScaleModel: TStringField
      FieldName = 'Model'
      Size = 10
    end
    object ptScaleName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ptScaleHubNumber: TWordField
      FieldName = 'HubNumber'
    end
    object ptScalePLUCount: TSmallintField
      FieldName = 'PLUCount'
    end
    object ptScaleRezerv1: TIntegerField
      FieldName = 'Rezerv1'
    end
    object ptScaleRezerv2: TIntegerField
      FieldName = 'Rezerv2'
    end
    object ptScaleRezerv3: TIntegerField
      FieldName = 'Rezerv3'
    end
    object ptScaleRezerv4: TIntegerField
      FieldName = 'Rezerv4'
    end
    object ptScaleRezerv5: TIntegerField
      FieldName = 'Rezerv5'
    end
    object ptScaleRezerv6: TIntegerField
      FieldName = 'Rezerv6'
    end
    object ptScaleRezerv7: TIntegerField
      FieldName = 'Rezerv7'
    end
    object ptScaleRezerv8: TIntegerField
      FieldName = 'Rezerv8'
    end
    object ptScaleRezerv9: TIntegerField
      FieldName = 'Rezerv9'
    end
    object ptScaleRezerv10: TIntegerField
      FieldName = 'Rezerv10'
    end
  end
  object ptKadri: TPvTable
    DatabaseName = '\\SYS4\e$\_MCrystal\Data'
    IndexFieldNames = 'CODE'
    TableName = 'Kadri'
    Left = 428
    Top = 432
    object ptKadriTabNumber: TStringField
      FieldName = 'TabNumber'
      Required = True
      Size = 10
    end
    object ptKadriKartNumber: TSmallintField
      FieldName = 'KartNumber'
      Required = True
    end
    object ptKadriFamilia: TStringField
      FieldName = 'Familia'
      Required = True
    end
    object ptKadriIma: TStringField
      FieldName = 'Ima'
      Required = True
      Size = 15
    end
    object ptKadriOtchestvo: TStringField
      FieldName = 'Otchestvo'
      Required = True
      Size = 15
    end
    object ptKadriCODE: TIntegerField
      FieldName = 'CODE'
      Required = True
    end
  end
  object ptOutLn: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;DateInvoice;Number'
    TableName = 'TTNOutLn'
    Left = 532
    Top = 508
    object ptOutLnDepart: TSmallintField
      FieldName = 'Depart'
    end
    object ptOutLnDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object ptOutLnNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object ptOutLnCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object ptOutLnCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object ptOutLnCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object ptOutLnCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object ptOutLnTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object ptOutLnBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object ptOutLnOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object ptOutLnNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object ptOutLnNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object ptOutLnOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object ptOutLnAkciz: TFloatField
      FieldName = 'Akciz'
    end
    object ptOutLnKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object ptOutLnKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object ptOutLnKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object ptOutLnKol: TFloatField
      FieldName = 'Kol'
    end
    object ptOutLnCenaPost: TFloatField
      FieldName = 'CenaPost'
    end
    object ptOutLnCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object ptOutLnCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object ptOutLnSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object ptOutLnSumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object ptOutLnCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object ptOutLnVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object ptOutLnCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object ptOutLnCenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
    object ptOutLnSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object ptOutLnSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object ptOutLnSumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object ptOutLnChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
  end
  object ptGrafPost: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ICLI;IDEP;IDATEP'
    TableName = 'A_GRAFPOST'
    Left = 688
    Top = 436
    object ptGrafPostICLI: TIntegerField
      FieldName = 'ICLI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGrafPostIDEP: TIntegerField
      FieldName = 'IDEP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGrafPostIDATEP: TIntegerField
      FieldName = 'IDATEP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGrafPostIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
    end
  end
  object ptCountry: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'Country'
    Left = 892
    Top = 20
    object ptCountryID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ptCountryName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object ptCountry1: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_COUNTRY'
    Left = 892
    Top = 76
    object ptCountry1ID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCountry1NAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object ptCountry1ABR: TStringField
      FieldName = 'ABR'
      Size = 10
    end
    object ptCountry1CODE: TStringField
      FieldName = 'CODE'
      Size = 10
    end
  end
  object ptGTD: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDH;ID'
    TableName = 'A_GTDOUT'
    Left = 892
    Top = 132
    object ptGTDIDH: TIntegerField
      FieldName = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGTDID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptGTDCODE: TIntegerField
      FieldName = 'CODE'
    end
    object ptGTDGTD: TStringField
      FieldName = 'GTD'
      Size = 30
    end
  end
  object dsquCatDisc: TDataSource
    DataSet = teCatDisc
    Left = 624
    Top = 164
  end
  object teCatDisc: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F050000000400000003000500494467720004000000
      030005004944736700040000000300030049440014000000010005004E616D65
      00020000000200040056303100}
    SortOptions = []
    Left = 624
    Top = 120
    object teCatDiscIDgr: TIntegerField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'IDgr'
    end
    object teCatDiscIDsg: TIntegerField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1087#1072
      FieldName = 'IDsg'
    end
    object teCatDiscID: TIntegerField
      DisplayLabel = #1050#1086#1076' '#1082#1072#1090#1077#1075#1086#1088#1080#1080
      FieldName = 'ID'
    end
    object teCatDiscName: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'Name'
    end
    object teCatDiscV01: TSmallintField
      DisplayLabel = #1057#1082#1080#1076#1082#1072
      FieldName = 'V01'
    end
  end
  object taAlgClass: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_CLASSIFALG'
    Left = 56
    Top = 520
    object taAlgClassID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object taAlgClassNAMECLA: TStringField
      FieldName = 'NAMECLA'
      Size = 200
    end
    object taAlgClassGetAM: TSmallintField
      FieldName = 'GetAM'
    end
  end
  object ptMakers: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_MAKER'
    Left = 132
    Top = 520
    object ptMakersID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptMakersNAMEM: TStringField
      FieldName = 'NAMEM'
      Size = 200
    end
    object ptMakersINNM: TStringField
      FieldName = 'INNM'
      Size = 15
    end
    object ptMakersKPPM: TStringField
      FieldName = 'KPPM'
      Size = 15
    end
  end
  object ptCliLic: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ITYPE;ICLI;IDATEB'
    TableName = 'A_CLIENTSALG'
    Left = 208
    Top = 520
    object ptCliLicITYPE: TSmallintField
      FieldName = 'ITYPE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCliLicICLI: TIntegerField
      FieldName = 'ICLI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCliLicIDATEB: TIntegerField
      FieldName = 'IDATEB'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCliLicDDATEB: TDateField
      FieldName = 'DDATEB'
    end
    object ptCliLicIDATEE: TIntegerField
      FieldName = 'IDATEE'
    end
    object ptCliLicDDATEE: TDateField
      FieldName = 'DDATEE'
    end
    object ptCliLicSER: TStringField
      FieldName = 'SER'
      Size = 50
    end
    object ptCliLicSNUM: TStringField
      FieldName = 'SNUM'
      Size = 50
    end
    object ptCliLicORGAN: TStringField
      FieldName = 'ORGAN'
      Size = 200
    end
    object ptCliLicIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
  end
  object ptKadr: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.3.150\k$\SCrystal\DATA'
    IndexFieldNames = 'ID'
    TableName = 'A_CASSIR'
    Left = 424
    Top = 496
    object ptKadrID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptKadrNAMECASSIR: TStringField
      FieldName = 'NAMECASSIR'
      Size = 100
    end
    object ptKadrPASSW: TStringField
      FieldName = 'PASSW'
      Size = 25
    end
    object ptKadrIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object ptKadrISPEC: TSmallintField
      FieldName = 'ISPEC'
    end
    object ptKadrITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object ptKadrSDOC: TStringField
      FieldName = 'SDOC'
      Size = 200
    end
    object ptKadrSDOLG: TStringField
      FieldName = 'SDOLG'
      Size = 200
    end
    object ptKadrIKASSIR: TSmallintField
      FieldName = 'IKASSIR'
    end
  end
  object ptCardsSaveAlco: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_C'
    Left = 824
    Top = 456
    object ptCardsSaveAlcoID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptCardsSaveAlcoIGR1: TIntegerField
      FieldName = 'IGR1'
    end
    object ptCardsSaveAlcoIGR2: TIntegerField
      FieldName = 'IGR2'
    end
    object ptCardsSaveAlcoIGR3: TIntegerField
      FieldName = 'IGR3'
    end
    object ptCardsSaveAlcoV10: TIntegerField
      FieldName = 'V10'
    end
    object ptCardsSaveAlcoV11: TIntegerField
      FieldName = 'V11'
    end
    object ptCardsSaveAlcoV12: TIntegerField
      FieldName = 'V12'
    end
  end
  object ptAlcoDHRec: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Id'
    TableName = 'A_ALCGDH'
    Left = 56
    Top = 584
    object ptAlcoDHRecId: TIntegerField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptAlcoDHRecIdOrg: TIntegerField
      FieldName = 'IdOrg'
      Required = True
    end
    object ptAlcoDHRecIDateB: TIntegerField
      FieldName = 'IDateB'
    end
    object ptAlcoDHRecIDateE: TIntegerField
      FieldName = 'IDateE'
    end
    object ptAlcoDHRecDDateB: TDateField
      FieldName = 'DDateB'
    end
    object ptAlcoDHRecDDateE: TDateField
      FieldName = 'DDateE'
    end
    object ptAlcoDHRecIType: TIntegerField
      FieldName = 'IType'
    end
    object ptAlcoDHRecsType: TStringField
      FieldName = 'sType'
      Size = 120
    end
    object ptAlcoDHRecSPers: TStringField
      FieldName = 'SPers'
      Size = 100
    end
  end
  object ptAlcoDS1: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IdH;Id'
    TableName = 'A_ALCGDS1'
    Left = 132
    Top = 588
    object ptAlcoDS1IdH: TIntegerField
      FieldName = 'IdH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptAlcoDS1Id: TAutoIncField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptAlcoDS1iDep: TIntegerField
      FieldName = 'iDep'
    end
    object ptAlcoDS1iNum: TIntegerField
      FieldName = 'iNum'
    end
    object ptAlcoDS1iVid: TIntegerField
      FieldName = 'iVid'
    end
    object ptAlcoDS1iProd: TIntegerField
      FieldName = 'iProd'
    end
    object ptAlcoDS1rQIn1: TFloatField
      FieldName = 'rQIn1'
    end
    object ptAlcoDS1rQIn2: TFloatField
      FieldName = 'rQIn2'
    end
    object ptAlcoDS1rQIn3: TFloatField
      FieldName = 'rQIn3'
    end
    object ptAlcoDS1rQInIt1: TFloatField
      FieldName = 'rQInIt1'
    end
    object ptAlcoDS1rQIn4: TFloatField
      FieldName = 'rQIn4'
    end
    object ptAlcoDS1rQIn5: TFloatField
      FieldName = 'rQIn5'
    end
    object ptAlcoDS1rQIn6: TFloatField
      FieldName = 'rQIn6'
    end
    object ptAlcoDS1rQInIt: TFloatField
      FieldName = 'rQInIt'
    end
    object ptAlcoDS1rQOut1: TFloatField
      FieldName = 'rQOut1'
    end
    object ptAlcoDS1rQOut2: TFloatField
      FieldName = 'rQOut2'
    end
    object ptAlcoDS1rQOut3: TFloatField
      FieldName = 'rQOut3'
    end
    object ptAlcoDS1rQOut4: TFloatField
      FieldName = 'rQOut4'
    end
    object ptAlcoDS1rQOutIt: TFloatField
      FieldName = 'rQOutIt'
    end
    object ptAlcoDS1rQe: TFloatField
      FieldName = 'rQe'
    end
    object ptAlcoDS1rQb: TFloatField
      FieldName = 'rQb'
    end
  end
  object ptAlcoDS2: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IdH;Id'
    TableName = 'A_ALCGDS2'
    Left = 208
    Top = 588
    object ptAlcoDS2IdH: TIntegerField
      FieldName = 'IdH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptAlcoDS2Id: TAutoIncField
      FieldName = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptAlcoDS2iDep: TIntegerField
      FieldName = 'iDep'
    end
    object ptAlcoDS2iVid: TIntegerField
      FieldName = 'iVid'
    end
    object ptAlcoDS2iProd: TIntegerField
      FieldName = 'iProd'
    end
    object ptAlcoDS2iPost: TIntegerField
      FieldName = 'iPost'
    end
    object ptAlcoDS2iLic: TIntegerField
      FieldName = 'iLic'
    end
    object ptAlcoDS2DocDate: TDateField
      FieldName = 'DocDate'
    end
    object ptAlcoDS2DocNum: TStringField
      FieldName = 'DocNum'
    end
    object ptAlcoDS2GTD: TStringField
      FieldName = 'GTD'
      Size = 50
    end
    object ptAlcoDS2rQIn: TFloatField
      FieldName = 'rQIn'
    end
    object ptAlcoDS2LicNum: TStringField
      FieldName = 'LicNum'
      Size = 100
    end
    object ptAlcoDS2LicDateB: TDateField
      FieldName = 'LicDateB'
    end
    object ptAlcoDS2LicDateE: TDateField
      FieldName = 'LicDateE'
    end
    object ptAlcoDS2LicOrg: TStringField
      FieldName = 'LicOrg'
      Size = 200
    end
    object ptAlcoDS2IdHdr: TIntegerField
      FieldName = 'IdHdr'
    end
  end
  object ptGroupCryst: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Name'
    TableName = 'GdsGroup'
    Left = 304
    Top = 588
    object ptGroupCrystOtdel: TSmallintField
      FieldName = 'Otdel'
    end
    object ptGroupCrystID: TAutoIncField
      FieldName = 'ID'
    end
    object ptGroupCrystName: TStringField
      FieldName = 'Name'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 30
    end
    object ptGroupCrystFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object ptGroupCrystReserv1: TFloatField
      FieldName = 'Reserv1'
    end
  end
  object ptInvLine2: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDH;ITEM'
    TableName = 'A_INVLN'
    Left = 528
    Top = 196
    object ptInvLine2IDH: TBCDField
      FieldName = 'IDH'
      Required = True
      Precision = 19
      Size = 0
    end
    object ptInvLine2ITEM: TIntegerField
      FieldName = 'ITEM'
      Required = True
    end
    object ptInvLine2QUANTR: TFloatField
      FieldName = 'QUANTR'
    end
    object ptInvLine2QUANTF: TFloatField
      FieldName = 'QUANTF'
    end
    object ptInvLine2QUANTD: TFloatField
      FieldName = 'QUANTD'
    end
    object ptInvLine2PRICER: TFloatField
      FieldName = 'PRICER'
    end
    object ptInvLine2SUMR: TFloatField
      FieldName = 'SUMR'
    end
    object ptInvLine2SUMF: TFloatField
      FieldName = 'SUMF'
    end
    object ptInvLine2SUMD: TFloatField
      FieldName = 'SUMD'
    end
    object ptInvLine2SUMRSS: TFloatField
      FieldName = 'SUMRSS'
    end
    object ptInvLine2SUMFSS: TFloatField
      FieldName = 'SUMFSS'
    end
    object ptInvLine2SUMDSS: TFloatField
      FieldName = 'SUMDSS'
    end
    object ptInvLine2NUM: TIntegerField
      FieldName = 'NUM'
    end
    object ptInvLine2QUANTC: TFloatField
      FieldName = 'QUANTC'
    end
    object ptInvLine2SUMC: TFloatField
      FieldName = 'SUMC'
    end
    object ptInvLine2SUMRSC: TFloatField
      FieldName = 'SUMRSC'
    end
  end
  object ptCashPath: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_CASHPATH'
    Left = 448
    Top = 584
    object ptCashPathID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ptCashPathPATH: TMemoField
      FieldName = 'PATH'
      BlobType = ftMemo
      Size = 300
    end
  end
  object ptUPL: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_UPLICA'
    Left = 536
    Top = 584
    object ptUPLID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptUPLIMH: TIntegerField
      FieldName = 'IMH'
    end
    object ptUPLNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object ptUPLIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
  end
  object ptPersonal: TPvTable
    AutoRefresh = True
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_RPERSONAL'
    Left = 896
    Top = 184
    object ptPersonalID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ptPersonalID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
      Required = True
    end
    object ptPersonalNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object ptPersonalUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
      Required = True
    end
    object ptPersonalPASSW: TStringField
      FieldName = 'PASSW'
      Required = True
    end
    object ptPersonalMODUL1: TSmallintField
      FieldName = 'MODUL1'
      Required = True
    end
    object ptPersonalMODUL2: TSmallintField
      FieldName = 'MODUL2'
      Required = True
    end
    object ptPersonalMODUL3: TSmallintField
      FieldName = 'MODUL3'
      Required = True
    end
    object ptPersonalMODUL4: TSmallintField
      FieldName = 'MODUL4'
      Required = True
    end
    object ptPersonalMODUL5: TSmallintField
      FieldName = 'MODUL5'
      Required = True
    end
    object ptPersonalMODUL6: TSmallintField
      FieldName = 'MODUL6'
      Required = True
    end
    object ptPersonalBARCODE: TStringField
      FieldName = 'BARCODE'
      Required = True
    end
  end
  object ptNum: TPvTable
    AutoRefresh = True
    DatabaseName = '\\SYS4\e$\_MCrystal\Data'
    TableName = 'A_NUM'
    Left = 620
    Top = 508
    object ptNumITYPE: TSmallintField
      FieldName = 'ITYPE'
      Required = True
    end
    object ptNumDNAME: TStringField
      FieldName = 'DNAME'
      Size = 50
    end
    object ptNumINUM: TIntegerField
      FieldName = 'INUM'
    end
    object ptNumSNUM: TStringField
      FieldName = 'SNUM'
      Size = 50
    end
  end
end
