unit DocOutR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, DBClient,
  dxmdaset;

type
  TfmDocsReal = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsR: TcxGrid;
    ViewDocsR: TcxGridDBTableView;
    LevelDocsR: TcxGridLevel;
    ViewDocsRID: TcxGridDBColumn;
    ViewDocsRDATEDOC: TcxGridDBColumn;
    ViewDocsRNUMDOC: TcxGridDBColumn;
    ViewDocsRNAMECL: TcxGridDBColumn;
    ViewDocsRIDSKL_FROM: TcxGridDBColumn;
    ViewDocsRIDSKL_TO: TcxGridDBColumn;
    ViewDocsRSUMIN: TcxGridDBColumn;
    ViewDocsRSUMUCH: TcxGridDBColumn;
    ViewDocsRSUMTAR: TcxGridDBColumn;
    ViewDocsRPROCNAC: TcxGridDBColumn;
    ViewDocsRNAMEMH: TcxGridDBColumn;
    ViewDocsRIACTIVE: TcxGridDBColumn;
    amDocsReal: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsR: TcxGridLevel;
    ViewCardsR: TcxGridDBTableView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsReal: TfrReport;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    frtaSpecRSel: TfrDBDataSet;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    acPrintTTN13: TAction;
    N5: TMenuItem;
    taSpecRPrint: TClientDataSet;
    taSpecRPrintNum: TIntegerField;
    taSpecRPrintIdGoods: TIntegerField;
    taSpecRPrintNameG: TStringField;
    taSpecRPrintIM: TIntegerField;
    taSpecRPrintSM: TStringField;
    taSpecRPrintQuant: TFloatField;
    taSpecRPrintPriceIn: TCurrencyField;
    taSpecRPrintSumIn: TCurrencyField;
    taSpecRPrintPriceR: TCurrencyField;
    taSpecRPrintSumR: TCurrencyField;
    taSpecRPrintSumNac: TCurrencyField;
    taSpecRPrintProcNac: TFloatField;
    taSpecRPrintKm: TFloatField;
    taSpecRPrintTCard: TIntegerField;
    taSpecRPrintMassa: TStringField;
    N6: TMenuItem;
    acAddDocs: TAction;
    N7: TMenuItem;
    frquSpecRSel: TfrDBDataSet;
    PopupMenu2: TPopupMenu;
    N8: TMenuItem;
    acExit: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    acPrint2: TAction;
    ViewCardsRIDCARD: TcxGridDBColumn;
    ViewCardsRNAME: TcxGridDBColumn;
    ViewCardsRNAMESHORT: TcxGridDBColumn;
    ViewCardsRCATEGORY: TcxGridDBColumn;
    ViewCardsRDATEDOC: TcxGridDBColumn;
    ViewCardsRMHTO: TcxGridDBColumn;
    ViewCardsRMHFROM: TcxGridDBColumn;
    ViewCardsRNAMECL: TcxGridDBColumn;
    ViewCardsRKVART: TcxGridDBColumn;
    ViewCardsRWEEKOFM: TcxGridDBColumn;
    ViewCardsRQUANT: TcxGridDBColumn;
    ViewCardsRSUMIN: TcxGridDBColumn;
    ViewCardsRSUMR: TcxGridDBColumn;
    ViewCardsRRNac: TcxGridDBColumn;
    ViewCardsRDAYWEEK: TcxGridDBColumn;
    SpeedItem10: TSpeedItem;
    acCalcR: TAction;
    ViewCardsROPER: TcxGridDBColumn;
    acPrintTY: TAction;
    N11: TMenuItem;
    taSpTY: TClientDataSet;
    taSpTYNum: TIntegerField;
    taSpTYIdGoods: TIntegerField;
    taSpTYNameG: TStringField;
    taSpTYIM: TIntegerField;
    taSpTYSM: TStringField;
    taSpTYQuant: TFloatField;
    taSpTYPriceIn: TCurrencyField;
    taSpTYSumIn: TCurrencyField;
    taSpTYPriceR: TCurrencyField;
    taSpTYSumR: TCurrencyField;
    taSpTYINds: TIntegerField;
    taSpTYSNds: TStringField;
    taSpTYRNds: TCurrencyField;
    taSpTYSumNac: TCurrencyField;
    taSpTYProcNac: TFloatField;
    taSpTYKm: TFloatField;
    taSpTYTCard: TIntegerField;
    taSpTYOper: TSmallintField;
    taSpTYCTO: TStringField;
    taSpTYMassa: TStringField;
    frtaSpTY: TfrDBDataSet;
    taSpTYComm1: TStringField;
    taSpTYComm2: TStringField;
    taSpTYComm3: TStringField;
    taSpTYComm4: TStringField;
    taSpTYComm5: TStringField;
    taSpTYCTOCOMM: TStringField;
    frquSpecRSelSF: TfrDBDataSet;
    ViewCardsRNAMECAT: TcxGridDBColumn;
    PopupMenu3: TPopupMenu;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    Excel2: TMenuItem;
    ViewDocsRBZTYPE: TcxGridDBColumn;
    ViewDocsRBZSTATUS: TcxGridDBColumn;
    ViewDocsRBZSUMR: TcxGridDBColumn;
    ViewDocsRBZSUMF: TcxGridDBColumn;
    ViewDocsRBZSUMS: TcxGridDBColumn;
    ViewDocsRIEDIT: TcxGridDBColumn;
    ViewDocsRPERSEDIT: TcxGridDBColumn;
    acSetStatusBZ: TAction;
    SpeedItem11: TSpeedItem;
    acToProizv: TAction;
    SpeedItem12: TSpeedItem;
    frquDocsRCard1: TfrDBDataSet;
    acPrintActRazn: TAction;
    N15: TMenuItem;
    tePrintA: TdxMemData;
    tePrintAiNUm: TIntegerField;
    tePrintAiCode: TIntegerField;
    tePrintANameCard: TStringField;
    tePrintAiM: TIntegerField;
    tePrintAsM: TStringField;
    tePrintAsMest: TStringField;
    tePrintAQuant: TFloatField;
    tePrintAPrice: TFloatField;
    tePrintANDS: TFloatField;
    tePrintARSum: TFloatField;
    tePrintAQuantF: TFloatField;
    tePrintArSumF: TFloatField;
    tePrintAQuantD: TFloatField;
    tePrintArSumD: TFloatField;
    frtePrintA: TfrDBDataSet;
    frquDocsRCard2: TfrDBDataSet;
    frquDocsRCli: TfrDBDataSet;
    acSendCB: TAction;
    N16: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsRDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acPrintTTN13Execute(Sender: TObject);
    procedure acAddDocsExecute(Sender: TObject);
    procedure ViewDocsRCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N8Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPrint2Execute(Sender: TObject);
    procedure acCalcRExecute(Sender: TObject);
    procedure acPrintTYExecute(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure Excel2Click(Sender: TObject);
    procedure acSetStatusBZExecute(Sender: TObject);
    procedure acToProizvExecute(Sender: TObject);
    procedure acPrintActRaznExecute(Sender: TObject);
    procedure frRepDocsRealGetValue(const ParName: String;
      var ParValue: Variant);
    procedure acSendCBExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prOpen(IDH:Integer);
    procedure prOn(IdH,IdSkl,iDate,iCli:Integer;var rSum1,rSum2:Real);
  end;

procedure prFormVid(iT:INteger);

var
  fmDocsReal: TfmDocsReal;
  bClearDocR:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc3, DMOReps, TBuff, AddDoc4,
  MainRnOffice, sumprops, SummaryRDoc, SetStatusBZ, CB, SelPerSkl,
  SElPerSkl3;

{$R *.dfm}

procedure prFormVid(iT:INteger);
Var iTz,iStz:INteger;
begin
  with dmORep do
  with dmO do
  begin
    iTz:=0;
    iStz:=0;
    if (iT=1)or(iT=2) then
    begin
      if quDocsRSelBZTYPE.AsInteger=1 then //��� ����������
      begin
        iTz:=1;
        iStz:=quDocsRSelBZSTATUS.AsInteger;

{ 0 - ������������ (���������)
  1 - ������������ (��������� ��� � ������)
  2 - ��������� (��������� ���������)
  3 - ������� (��������� �������)}

      end;
    end;

{
    if iT=0 then
    if iT=1 then
    if iT=2 then
}
    if iT=0 then fmAddDoc4.Caption:='��������� �� ���������� : ����������.';
    if iT=1 then fmAddDoc4.Caption:='��������� �� ���������� : ��������������.';
    if iT=2 then fmAddDoc4.Caption:='��������� �� ���������� : ��������.';

    if (iTz=1) then fmAddDoc4.Caption:=fmAddDoc4.Caption+' (�����)';

    if iT=0 then fmAddDoc4.cxTextEdit1.Text:=prGetNum(8,0);
    if (iT=1)or(iT=2) then fmAddDoc4.cxTextEdit1.Text:=quDocsRSelNUMDOC.AsString;
    if iT=0 then fmAddDoc4.cxTextEdit1.Tag:=0;
    if (iT=1)or(iT=2) then fmAddDoc4.cxTextEdit1.Tag:=quDocsRSelID.AsInteger;
    if (iT=0)or(iT=1) then fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=True;

    if iT=0 then fmAddDoc4.cxTextEdit2.Text:='';
    if (iT=1)or(iT=2) then fmAddDoc4.cxTextEdit2.Text:=quDocsRSelNUMSF.AsString;
    if (iT=0)or(iT=1) then fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=True;

    if iT=0 then fmAddDoc4.cxDateEdit1.Date:=date;
    if (iT=1)or(iT=2) then fmAddDoc4.cxDateEdit1.Date:=quDocsRSelDATEDOC.AsDateTime;
    if (iT=0)or(iT=1) then fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=True;

    if iT=0 then fmAddDoc4.cxDateEdit2.Date:=date;
    if (iT=1)or(iT=2) then fmAddDoc4.cxDateEdit2.Date:=quDocsRSelDATESF.AsDateTime;
    if (iT=0)or(iT=1) then fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=True;

    fmAddDoc4.cxButtonEdit1.Enabled:=True;
    if iT=0 then
    begin
      fmAddDoc4.cxButtonEdit1.Tag:=0;
      fmAddDoc4.cxButtonEdit1.EditValue:=0;
      fmAddDoc4.cxButtonEdit1.Text:='';
    end;
    if (iT=1)or(iT=2) then
    begin
      fmAddDoc4.cxButtonEdit1.Tag:=quDocsRSelIDCLI.AsInteger;
      fmAddDoc4.cxButtonEdit1.EditValue:=quDocsRSelIDCLI.AsInteger;
      fmAddDoc4.cxButtonEdit1.Text:=quDocsRSelNAMECL.AsString;
    end;
    if (iT=0)or(iT=1) then fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=True;

    if iT=0 then fmAddDoc4.cxCalcEdit1.EditValue:=0;
    if (iT=1)or(iT=2) then fmAddDoc4.cxCalcEdit1.EditValue:=quDocsRSelPROCNAC.AsFloat;
    if (iT=0)or(iT=1) then fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=True;

    if taNDS.Active=False then taNDS.Active:=True;
    taNds.FullRefresh;

    fmAddDoc4.cxButtonEdit2.Enabled:=True;
    if iT=0 then
    begin
      fmAddDoc4.cxButtonEdit2.Tag:=0;
      fmAddDoc4.cxButtonEdit2.EditValue:=0;
      fmAddDoc4.cxButtonEdit2.Text:='';
    end;
    if (iT=1)or(iT=2) then
    begin
      fmAddDoc4.cxButtonEdit2.Tag:=quDocsRSelIDFROM.AsInteger;
      fmAddDoc4.cxButtonEdit2.EditValue:=quDocsRSelIDFROM.AsInteger;
      fmAddDoc4.cxButtonEdit2.Text:=quDocsRSelNAMECLFROM.AsString;
    end;
    if (iT=0)or(iT=1) then fmAddDoc4.cxButtonEdit2.Properties.ReadOnly:=False;
    if iT=2 then fmAddDoc4.cxButtonEdit2.Properties.ReadOnly:=True;

    if quMHAll.Active=False then
    begin
      quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;

    if iT=0 then
    begin
      fmAddDoc4.cxLookupComboBox1.EditValue:=0;
      fmAddDoc4.cxLookupComboBox1.Text:='';
      fmAddDoc4.cxLookupComboBox1.Tag:=0; //ISS
      if quMHAll.RecordCount>0 then
      begin
        quMHAll.First;
        fmAddDoc4.cxLookupComboBox1.EditValue:=quMHAllID.AsInteger;
        fmAddDoc4.cxLookupComboBox1.Text:=quMHAllNAMEMH.AsString;
        fmAddDoc4.cxLookupComboBox1.Tag:=quMHAllISS.AsInteger; //ISS
      end;
    end;
    if (iT=1)or(iT=2) then
    begin
      fmAddDoc4.cxLookupComboBox1.EditValue:=quDocsRSelIDSKL.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quDocsRSelNAMEMH.AsString;
      fmAddDoc4.cxLookupComboBox1.Tag:=0; //ISS

      CurVal.IdMH:=quDocsRSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsRSelNAMEMH.AsString;
      if quMHAll.Locate('ID',CurVal.IdMH,[]) then fmAddDoc4.cxLookupComboBox1.Tag:=quMHAllISS.AsInteger; //ISS
    end;
    if (iT=0)or(iT=1) then fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;
    if (iT=2)or(iTz=1) then fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=True;

    if (iT=0)or(iT=1) then
    begin
      fmAddDoc4.cxLabel1.Enabled:=True;
      fmAddDoc4.cxLabel2.Enabled:=True;
      fmAddDoc4.cxLabel3.Enabled:=True;
      fmAddDoc4.cxLabel4.Enabled:=True;
      fmAddDoc4.cxLabel7.Enabled:=True;
      fmAddDoc4.cxLabel5.Enabled:=True;
      fmAddDoc4.cxLabel6.Enabled:=True;

      fmAddDoc4.N1.Enabled:=True;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;

      fmAddDoc4.ViewDoc4Quant.Caption:='���-��';

      fmAddDoc4.ViewDoc4Num.Options.Editing:=True;
      fmAddDoc4.ViewDoc4IdGoods.Options.Editing:=True;
      fmAddDoc4.ViewDoc4NameG.Options.Editing:=True;
      fmAddDoc4.ViewDoc4IM.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SM.Options.Editing:=True;
      fmAddDoc4.ViewDoc4Quant.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumIn.Options.Editing:=True;
      fmAddDoc4.ViewDoc4PriceR.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumR.Options.Editing:=True;
      fmAddDoc4.ViewDoc4INds.Options.Editing:=True;
      fmAddDoc4.ViewDoc4RNds.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumNac.Options.Editing:=True;
      fmAddDoc4.ViewDoc4ProcNac.Options.Editing:=True;
      fmAddDoc4.ViewDoc4Km.Options.Editing:=True;
      fmAddDoc4.ViewDoc4TCard.Options.Editing:=True;
      fmAddDoc4.ViewDoc4Oper.Options.Editing:=True;
      fmAddDoc4.ViewDoc4CTO.Options.Editing:=True;
      fmAddDoc4.ViewDoc4CType.Options.Editing:=True;
      fmAddDoc4.ViewDoc4PriceIn0.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumIn0.Options.Editing:=True;
      fmAddDoc4.ViewDoc4SumOut0.Options.Editing:=True;
      fmAddDoc4.ViewDoc4RNdsOut.Options.Editing:=True;
      fmAddDoc4.ViewDoc4BZQUANTR.Options.Editing:=True;
      fmAddDoc4.ViewDoc4BZQUANTF.Options.Editing:=True;
      fmAddDoc4.ViewDoc4BZQUANTS.Options.Editing:=True;

      fmAddDoc4.acAddTara.Enabled:=True;
      fmAddDoc4.acDelTara.Enabled:=True;

      if (iTz=1) then
      begin
        fmAddDoc4.cxLabel1.Enabled:=False;
        fmAddDoc4.cxLabel2.Enabled:=False;
        fmAddDoc4.cxLabel3.Enabled:=False;
        fmAddDoc4.cxLabel4.Enabled:=True;
        fmAddDoc4.cxLabel7.Enabled:=True;
        fmAddDoc4.cxLabel5.Enabled:=False;
        fmAddDoc4.cxLabel6.Enabled:=False;

        fmAddDoc4.N1.Enabled:=False;
        fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;

        fmAddDoc4.ViewDoc4Quant.Caption:='���-�� ����������';

        fmAddDoc4.ViewDoc4Num.Options.Editing:=False;
        fmAddDoc4.ViewDoc4IdGoods.Options.Editing:=False;
        fmAddDoc4.ViewDoc4NameG.Options.Editing:=False;
        fmAddDoc4.ViewDoc4IM.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SM.Options.Editing:=False;
        fmAddDoc4.ViewDoc4Quant.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SumIn.Options.Editing:=False;
        fmAddDoc4.ViewDoc4PriceR.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SumR.Options.Editing:=False;
        fmAddDoc4.ViewDoc4INds.Options.Editing:=False;
        fmAddDoc4.ViewDoc4RNds.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SumNac.Options.Editing:=False;
        fmAddDoc4.ViewDoc4ProcNac.Options.Editing:=False;
        fmAddDoc4.ViewDoc4Km.Options.Editing:=False;
        fmAddDoc4.ViewDoc4TCard.Options.Editing:=False;
        fmAddDoc4.ViewDoc4Oper.Options.Editing:=False;
        fmAddDoc4.ViewDoc4CTO.Options.Editing:=False;
        fmAddDoc4.ViewDoc4CType.Options.Editing:=False;
        fmAddDoc4.ViewDoc4PriceIn0.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SumIn0.Options.Editing:=False;
        fmAddDoc4.ViewDoc4SumOut0.Options.Editing:=False;
        fmAddDoc4.ViewDoc4RNdsOut.Options.Editing:=False;
        fmAddDoc4.ViewDoc4BZQUANTR.Options.Editing:=False;
        fmAddDoc4.ViewDoc4BZQUANTF.Options.Editing:=False;
        fmAddDoc4.ViewDoc4BZQUANTS.Options.Editing:=False;

{ 0 - ������������ (���������)
  1 - ������������ (��������� ��� � ������)
  2 - ��������� (��������� ���������)
  3 - ������� (��������� �������)}

        Case iStz of
        0:begin
            if cando('DocRQuantF') then fmAddDoc4.ViewDoc4BZQUANTF.Options.Editing:=True;
          end;
        1:begin
            if cando('DocRQuantS') then fmAddDoc4.ViewDoc4BZQUANTS.Options.Editing:=True;
          end;
        2:begin
            if cando('DocRQuantP') then fmAddDoc4.ViewDoc4Quant.Options.Editing:=True;
          end;
        3:begin //� ������ ������� ����� ������������� ������ ������
            if cando('DocRQuantE') then fmAddDoc4.ViewDoc4Quant.Options.Editing:=True;
          end;
        end;
      end;

      fmAddDoc4.cxButton1.Enabled:=True;
      fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
      fmAddDoc4.acSaveDoc.Enabled:=True;

      if cando('DocRSetPrice') then
        fmAddDoc4.ViewDoc4PriceR.Options.Editing:=True
      else
        fmAddDoc4.ViewDoc4PriceR.Options.Editing:=False;

      if cando('DocRSetPrice') then
        fmAddDoc4.ViewDoc4SumR.Options.Editing:=True
      else
        fmAddDoc4.ViewDoc4SumR.Options.Editing:=False;

    end;

    if (iT=2) then
    begin
      fmAddDoc4.cxLabel1.Enabled:=False;
      fmAddDoc4.cxLabel2.Enabled:=False;
      fmAddDoc4.cxLabel3.Enabled:=False;
      fmAddDoc4.cxLabel4.Enabled:=False;
      fmAddDoc4.cxLabel7.Enabled:=False;
      fmAddDoc4.cxLabel5.Enabled:=False;
      fmAddDoc4.cxLabel6.Enabled:=False;
      fmAddDoc4.cxButton1.Enabled:=False;
      fmAddDoc4.N1.Enabled:=False;
      fmAddDoc4.ViewDoc4.OptionsData.Editing:=False;
      fmAddDoc4.ViewDoc4.OptionsData.Deleting:=False;

      fmAddDoc4.acSaveDoc.Enabled:=False;
      fmAddDoc4.acAddTara.Enabled:=False;
      fmAddDoc4.acDelTara.Enabled:=False;

    end;

    if (iTz=1) then
    begin
      fmAddDoc4.ViewDoc4BZQUANTR.Visible:=True;
      fmAddDoc4.ViewDoc4BZQUANTF.Visible:=True;
      fmAddDoc4.ViewDoc4BZQUANTS.Visible:=True;

      { 0 - ������������ (���������)
  1 - ������������ (��������� ��� � ������)
  2 - ��������� (��������� ���������)
  3 - ������� (��������� �������)}

      fmAddDoc4.cxRadioGroup1.ItemIndex:=iStz;
      fmAddDoc4.cxRadioGroup1.Visible:=True;
    end else
    begin
      fmAddDoc4.ViewDoc4BZQUANTR.Visible:=False;
      fmAddDoc4.ViewDoc4BZQUANTF.Visible:=False;
      fmAddDoc4.ViewDoc4BZQUANTS.Visible:=False;
      fmAddDoc4.cxRadioGroup1.Visible:=False;
    end;

    CloseTe(fmAddDoc4.taSpec);
  end;
end;

procedure TfmDocsReal.prOn(IdH,IdSkl,iDate,iCli:Integer;var rSum1,rSum2:Real);
Var PriceSp,PriceUch,rSumIn,rQs,rQ,rQp,rMessure,rQRemn:Real;
    iSS:INteger;
    rSum0,rSumIn0,PriceSp0:Real;
    rProcNDS:Real;
begin
  with dmO do
  with dmORep do
  begin
    iSS:=prIss(IdSkl);  // if iSS=2 ��������� ���

    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    rSum1:=0; rSum2:=0; rSum0:=0;

    quSpecRSel.First;
    while not quSpecRSel.Eof do
    begin

      PriceSp:=0;
      PriceSp0:=0;
      rSumIn:=0;
      rSumIn0:=0;
//            rSumUch:=0;
      rQs:=quSpecRSelQUANT.AsFloat*quSpecRSelKM.AsFloat; //�������� � ��������

      if rQs>0 then
      begin //����������� ������
        prSelPartIn(quSpecRSelIDCARD.AsInteger,IdSkl,0,0);

        quSelPartIn.First;
        if rQs>0 then
        begin
          while (not quSelPartIn.Eof) and (rQs>0) do
          begin
           //���� �� ���� ������� ���� �����, ��������� �������� ���
            rQp:=quSelPartInQREMN.AsFloat;
            if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                        else  rQ:=rQp;
            rQs:=rQs-rQ;

            PriceSp:=quSelPartInPRICEIN.AsFloat;
            rSumIn:=rSumIn+RoundVal(PriceSp*rQ);

            PriceSp0:=quSelPartInPRICEIN0.AsFloat;
            rSumIn0:=rSumIn0+RoundVal(PriceSp0*rQ);

            if quSpecRSelCATEGORY.AsInteger=1 then
            begin
              rSum1:=rSum1+RoundVal(PriceSp*rQ); //����� ��������� �����
              rSum0:=rSum0+RoundVal(PriceSp0*rQ); //����� ��������� �����
            end;
            if quSpecRSelCATEGORY.AsInteger=4 then
               rSum2:=rSum2+RoundVal(PriceSp*rQ); //����� ��������� ����


            prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecRSelIDCARD.AsInteger;
            prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
            prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
            prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
            prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
            prAddPartOut.ParamByName('DTYPE').AsInteger:=8;
            prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;

            if iSS<>2 then prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp
            else prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;

            prAddPartOut.ParamByName('SUMOUT').AsFloat:=RoundVal(quSpecRSelPRICER.AsFloat*rQ);
            prAddPartout.ExecProc;

            quSelPartIn.Next;
          end;

          if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
          begin
            if PriceSp=0 then
            begin //��� ���� ���������� ������� � ���������� ����������
              prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecRSelIDCARD.AsInteger;
              prCalcLastPrice1.ParamByName('ISKL').AsInteger:=IdSkl;
              prCalcLastPrice1.ExecProc;

              PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
              PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

              rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
              if (rMessure<>0)and(rMessure<>1) then
              begin
                PriceSp:=PriceSp/rMessure;
                PriceSp0:=PriceSp0/rMessure;
              end;
            end;

            prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecRSelIDCARD.AsInteger;
            prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
            prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
            prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
            prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
            prAddPartOut.ParamByName('DTYPE').AsInteger:=8;
            prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;

            if iSS<>2 then prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp
            else prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;
            
            prAddPartOut.ParamByName('SUMOUT').AsFloat:=quSpecRSelPRICER.AsFloat*rQs;
            prAddPartout.ExecProc;

            rSumIn:=rSumIn+RoundVal(PriceSp*rQs);
            rSumIn0:=rSumIn0+RoundVal(PriceSp0*rQs);
//                rSumUch:=rSumUch+RoundVal(PriceUch*rQs);
            if quSpecRSelCATEGORY.AsInteger=1 then
            begin
              rSum1:=rSum1+RoundVal(PriceSp*rQs); //����� ���������  �����
              rSum0:=rSum0+RoundVal(PriceSp0*rQs); //����� ���������  ����� ��� ���
            end;
            if quSpecRSelCATEGORY.AsInteger=4 then
              rSum2:=rSum2+RoundVal(PriceSp*rQs); //����� ���������  ����
          end;
        end;
        quSelPartIn.Active:=False;

      //�������� ���������
        quSpecRSel.Edit;

        if iSS=2 then
        begin
          prGetNDS(quSpecRSelINDS.AsInteger,rProcNDS);
          quSpecRSelIDNDS.AsInteger:=quSpecRSelINDS.AsInteger;
          quSpecRSelSUMNDSOUT.AsFloat:=rv(quSpecRSelSUMR.AsFloat*rProcNDS/(100+rProcNDS));
          quSpecRSelSUMOUT0.AsFloat:=quSpecRSelSUMR.AsFloat-rv(quSpecRSelSUMR.AsFloat*rProcNDS/(100+rProcNDS));

          if quSpecRSelQUANT.AsFloat<>0 then
          begin
            quSpecRSelSUMIN.AsFloat:=rSumIn;
            quSpecRSelPRICEIN.AsFloat:=rSumIn/quSpecRSelQUANT.AsFloat;
            quSpecRSelSUMIN0.AsFloat:=rSumIn0;
            quSpecRSelPRICEIN0.AsFloat:=rSumIn0/quSpecRSelQUANT.AsFloat;
            quSpecRSelSUMNDS.AsFloat:=rSumIn-rSumIn0;
          end else
          begin
            quSpecRSelSUMIN.AsFloat:=0;
            quSpecRSelPRICEIN.AsFloat:=0;
            quSpecRSelSUMIN0.AsFloat:=0;
            quSpecRSelPRICEIN0.AsFloat:=0;
            quSpecRSelSUMNDS.AsFloat:=0;
          end;
        end else  //�� ��������� ���
        begin
          quSpecRSelIDNDS.AsInteger:=1;
          quSpecRSelSUMNDSOUT.AsFloat:=0;
          quSpecRSelSUMOUT0.AsFloat:=quSpecRSelSUMR.AsFloat;

          if quSpecRSelQUANT.AsFloat<>0 then
          begin
            quSpecRSelSUMIN.AsFloat:=rSumIn;
            quSpecRSelPRICEIN.AsFloat:=rSumIn/quSpecRSelQUANT.AsFloat;
            quSpecRSelSUMIN0.AsFloat:=rSumIn;
            quSpecRSelPRICEIN0.AsFloat:=rSumIn/quSpecRSelQUANT.AsFloat;
            quSpecRSelSUMNDS.AsFloat:=0;
          end else
          begin
            quSpecRSelSUMIN.AsFloat:=0;
            quSpecRSelPRICEIN.AsFloat:=0;
            quSpecRSelSUMIN0.AsFloat:=0;
            quSpecRSelPRICEIN0.AsFloat:=0;
            quSpecRSelSUMNDS.AsFloat:=0;
          end;
        end;

        quSpecRSel.Post;

      end else  //������������� ����� - ��� ������� ��������� ������  (�������)
      begin
        //������ - �������� �������� ����� �������� �� ���� �.�. �� ����� �����
        //������������ ��� ��� �����

        quSpecRSel.Edit;
        if iSS=2 then
        begin
          prGetNDS(quSpecRSelINDS.AsInteger,rProcNDS);
          quSpecRSelIDNDS.AsInteger:=quSpecRSelINDS.AsInteger;
          quSpecRSelSUMNDSOUT.AsFloat:=rv(quSpecRSelSUMR.AsFloat*rProcNDS/(100+rProcNDS));
          quSpecRSelSUMOUT0.AsFloat:=quSpecRSelSUMR.AsFloat-rv(quSpecRSelSUMR.AsFloat*rProcNDS/(100+rProcNDS));

          rSumIn:=quSpecRSelSUMIN.AsFloat;
          rSumIn0:=quSpecRSelSUMIN.AsFloat-rv(quSpecRSelSUMIN.AsFloat*rProcNDS/(100+rProcNDS));

          quSpecRSelSUMIN0.AsFloat:=rSumIn0;

          if quSpecRSelQUANT.AsFloat<>0 then quSpecRSelPRICEIN0.AsFloat:=rv(rSumIn0/quSpecRSelQUANT.AsFloat)
          else quSpecRSelPRICEIN0.AsFloat:=0;

          quSpecRSelSUMNDS.AsFloat:=rSumIn-rSumIn0;
        end else
        begin
          quSpecRSelIDNDS.AsInteger:=1;
          quSpecRSelSUMNDSOUT.AsFloat:=0;
          quSpecRSelSUMOUT0.AsFloat:=quSpecRSelSUMR.AsFloat;
          quSpecRSelSUMIN0.AsFloat:=quSpecRSelSUMIN.AsFloat;
          quSpecRSelPRICEIN0.AsFloat:=quSpecRSelPRICEIN.AsFloat;
          quSpecRSelSUMNDS.AsFloat:=0;
        end;
        quSpecRSel.Post;


        //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
        rQRemn:=prCalcRemn(quSpecRSelIDCARD.AsInteger,iDate-1,IdSkl);
        if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
        begin
          quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecRSelIDCARD.AsInteger;
          quClosePartIn.ParamByName('IDSKL').AsInteger:=IdSkl;
          quClosePartIn.ParamByName('IDATE').AsInteger:=iDate-1;
          quClosePartIn.ExecQuery;
        end;

        prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl;
        prAddPartIn1.ParamByName('IDDOC').AsInteger:=IdH;
        prAddPartIn1.ParamByName('DTYPE').AsInteger:=8;
        prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
        prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecRSelIDCARD.AsInteger;
        prAddPartIn1.ParamByName('IDCLI').AsInteger:=iCli;
        prAddPartIn1.ParamByName('QUANT').AsFloat:=rQs*(-1); //������ ����

        PriceSp:=0;
        PriceSp0:=0;
        PriceUch:=0;
        if quSpecRSelKM.AsFloat<>0 then
        begin
          PriceSp:=quSpecRSelPRICEIN.AsFloat/quSpecRSelKM.AsFloat;   //���� � ������� ��
          PriceUch:=quSpecRSelPRICER.AsFloat/quSpecRSelKM.AsFloat; //���� � ������� ��
          PriceSp0:=quSpecRSelPRICEIN0.AsFloat/quSpecRSelKM.AsFloat;   //���� � ������� ��
        end;

        prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
        prAddPartIn1.ParamByName('PRICEIN0').AsFloat:=PriceSp0;
        prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
        prAddPartIn1.ParamByName('ISS').AsInteger:=iSS;

        prAddPartIn1.ExecProc;

        if quSpecRSelCATEGORY.AsInteger=1 then
        begin
          rSum1:=rSum1+quSpecRSelSUMIN.AsFloat; //����� ��������� �����
          rSum0:=rSum0+quSpecRSelSUMIN0.AsFloat; //����� ��������� �����
        end;
        if quSpecRSelCATEGORY.AsInteger=4 then
          rSum2:=rSum2+quSpecRSelSUMIN.AsFloat; //����� ��������� ����
      end;

      quSpecRSel.Next;
      delay(10);
    end;

    if iSS=2 then rSum1:=rSum0;

    quSpecRSel.Active:=False;
  end;
end;

procedure TfmDocsReal.prOpen(IDH:Integer);
begin
  with dmO do
  with dmORep do                    
  begin
    prAllViewOff;

    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    quSpecRSel.First;
    while not quSpecRSel.Eof do
    begin
      with fmAddDoc4 do
      begin
        taSpec.Append;
        taSpecNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpecIdGoods.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpecNameG.AsString:=quSpecRSelNAMEC.AsString;
        taSpecIM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpecSM.AsString:=quSpecRSelSM.AsString;
        taSpecQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpecPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpecSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpecPriceR.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecSumR.AsFloat:=rv(quSpecRSelSUMR.AsFloat);
        taSpecINds.AsInteger:=quSpecRSelIDNDS.AsInteger;
        taSpecSNds.AsString:=quSpecRSelNAMENDS.AsString;
        taSpecRNds.AsFloat:=rv(quSpecRSelSUMNDS.AsFloat);
        taSpecSumNac.AsFloat:=quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat;
        taSpecProcNac.AsFloat:=0;
        if quSpecRSelSUMIN.AsFloat<>0 then
          taSpecProcNac.AsFloat:=RoundEx((quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat)/quSpecRSelSUMIN.AsFloat*10000)/100;
        if quSpecRSelKM.AsFloat>0 then taSpecKM.AsFloat:=quSpecRSelKM.AsFloat
        else
        begin
          taSpecKM.AsFloat:=prFindKM(quSpecRSelIDM.AsInteger);
        end;
        taSpecTCard.AsInteger:=quSpecRSelTCard.AsInteger;
        taSpecOper.AsInteger:=quSpecRSelOPER.AsInteger;
        taSpecCTO.AsString:=quSpecRSelNAMECTO.AsString;
        taSpecCType.AsInteger:=quSpecRSelCATEGORY.AsInteger;
        taSpecPriceIn0.AsFloat:=quSpecRSelPRICEIN0.AsFloat;
        taSpecSumIn0.AsFloat:=quSpecRSelSUMIN0.AsFloat;
        taSpecSumOut0.AsFloat:=rv(quSpecRSelSUMR.AsFloat)-rv(quSpecRSelSUMNDSOUT.AsFloat);
        taSpecRNdsOut.AsFloat:=quSpecRSelSUMNDSOUT.AsFloat;

        taSpecBZQUANTR.AsFloat:=quSpecRSelBZQUANTR.AsFloat;
        taSpecBZQUANTF.AsFloat:=quSpecRSelBZQUANTF.AsFloat;
        taSpecBZQUANTS.AsFloat:=quSpecRSelBZQUANTS.AsFloat;

        taSpec.Post;
      end;
      quSpecRSel.Next;
    end;

    prAllViewOn;
  end;
end;

procedure TfmDocsReal.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  delay(100);
end;


procedure TfmDocsReal.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsReal.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Interval:=1000*CommonSet.Of_DocRefresh;
  Timer1.Enabled:=True;
  GridDocsR.Align:=AlClient;
  
  ViewDocsR.RestoreFromIniFile(CurDir+Person.Name+'\'+GridIni);
  ViewCardsR.RestoreFromIniFile(CurDir+Person.Name+'\'+GridIni);

  StatusBar1.Color:= UserColor.TTnOutR;
  SpeedBar1.Color := UserColor.TTnOutR;

  if CommonSet.AutoZak=1 then
  begin
    ViewDocsRBZTYPE.Visible:=True;
    ViewDocsRBZSTATUS.Visible:=True;
    ViewDocsRBZSUMR.Visible:=True;
    ViewDocsRBZSUMF.Visible:=True;
    ViewDocsRBZSUMS.Visible:=True;
  end else
  begin
    ViewDocsRBZTYPE.Visible:=False;
    ViewDocsRBZSTATUS.Visible:=False;
    ViewDocsRBZSUMR.Visible:=False;
    ViewDocsRBZSUMF.Visible:=False;
    ViewDocsRBZSUMS.Visible:=False;
  end;
end;

procedure TfmDocsReal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsR.StoreToIniFile(CurDir+Person.Name+'\'+GridIni,False);
  ViewCardsR.StoreToIniFile(CurDir+Person.Name+'\'+GridIni,False);
end;

procedure TfmDocsReal.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date);

    with dmO do
    with dmORep do
    begin
      if LevelDocsR.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsReal.Caption:='���������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsR.BeginUpdate;
        prFormQuDocsR;
        ViewDocsR.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsReal.Caption:='���������� �� ������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ������ ...');
        delay(10);

        ViewCardsR.BeginUpdate;
        dsquDocsRCard.DataSet:=nil;

        quDocsRCard.Active:=False;
        quDocsRCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCard.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quDocsRCard.ParamByName('IDPERSON1').AsInteger:=Person.Id;
        quDocsRCard.Active:=True;

        dsquDocsRCard.DataSet:=quDocsRCard;
        ViewCardsR.EndUpdate;

        Memo1.Lines.Add('��.');
        delay(10);
      end;
    end;
  end;
end;

procedure TfmDocsReal.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc4.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  prFormVid(0); //����������
  fmAddDoc4.Show;
end;

procedure TfmDocsReal.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc4.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        prFormVid(1); //������������ ���� 0-���������� 1-�������������� 2-��������

        IDH:=quDocsRSelID.AsInteger;

        quDocsRSel.Refresh;
        if (quDocsRSelIEDIT.AsInteger=0) or ((quDocsRSelIEDIT.AsInteger=1)and(quDocsRSelIPERSEDIT.AsInteger=Person.Id)) then
        begin
          quDocsRSel.Edit;
          quDocsRSelIEDIT.AsInteger:=1;
          quDocsRSelPERSEDIT.AsString:=Person.Name;
          quDocsRSelIPERSEDIT.AsInteger:=Person.Id;
          quDocsRSel.Post;

          prSetSync('DocR','OPN',IDH,quDocsRSelIDSKL.AsInteger);

          prOpen(IDH);
          fmAddDoc4.Show;
        end;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsReal.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc4.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      prFormVid(2);

      IDH:=quDocsRSelID.AsInteger;
      prOpen(IDH);

      fmAddDoc4.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsReal.ViewDocsRDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsRSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsReal.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsRSel.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsReal.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumInDoc,rSumTDoc:Real;
begin
  //������������
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount=0 then exit;
    if quDocsRSelBZTYPE.AsInteger=1 then
    begin
      if quDocsRSelBZSTATUS.AsInteger<>3 then
      begin
        Memo1.Lines.Add('������������� ������� ��������� �������� ������ � ������� "�������".');
        exit;
      end;
    end;

    if not CanDo('prOnDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsRSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsRSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsRSelNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

          //������� ��� ������ ���� ���� �� ������ ������ �� ������� ��������� � �� ������� � �� �������, ������� ��� ���� �� �����

          IDH:=quDocsRSelID.AsInteger;

          //����� �������������� ����� ��������� �� - � ������� ����� - �.�. ��� ������.

          quSpecRSel.Active:=False;
          quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecRSel.Active:=True;

          quSpecRSel.First;
          while not quSpecRSel.Eof do
          begin
            if quSpecRSelQUANT.AsFloat<0 then
            begin //������
              if quSpecRSelPRICEIN.AsFloat<0.01 then
              begin
                showmessage('������������ �������� ���������� - ������������ ����. (��� '+IntToStr(quSpecRSelIDCARD.AsInteger)+')');
                quSpecRSel.Active:=False;
                prButtonSet(True);
                exit;
              end;
            end;
            quSpecRSel.Next;
          end;
          quSpecRSel.Active:=False;

          prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPart.ParamByName('DTYPE').AsInteger:=8;
          prDelPart.ExecProc;

          //����������� ��� ������
          prOn(IDH,quDocsRSelIDSKL.AsInteger,TRUNC(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDCLI.AsInteger,rSumInDoc,rSumTDoc);

          //�������� ������
          quDocsRSel.Edit;
          quDocsRSelSUMIN.AsFloat:=rSumInDoc;
          quDocsRSelSUMTAR.AsFloat:=rSumTDoc;
          quDocsRSelIACTIVE.AsInteger:=1;
          quDocsRSel.Post;
          quDocsRSel.Refresh;

          prSetSync('DocR','ON',quDocsRSelID.AsInteger,quDocsRSelIDSKL.AsInteger);
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsReal.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;

    if not CanDo('prOffDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsRSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsRSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsRSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin

          if prTOFind(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsRSelNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=8;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('DTYPE').AsInteger:=8;
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
            prDelPartOut.ExecProc;

            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsRSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=8;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            // 4 - �������� ������
            if quDocsRSelBZTYPE.AsInteger=1 then
            begin
              quDocsRSel.Edit;
              quDocsRSelIACTIVE.AsInteger:=0;
              quDocsRSelBZSTATUS.AsInteger:=2;
              quDocsRSel.Post;
            end else
            begin
              quDocsRSel.Edit;
              quDocsRSelIACTIVE.AsInteger:=0;
              quDocsRSel.Post;
            end;

            quDocsRSel.Refresh;

            prSetSync('DocR','OFF',quDocsRSelID.AsInteger,quDocsRSelIDSKL.AsInteger);
          end;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsReal.Timer1Timer(Sender: TObject);
Var CurId:INteger;
begin
  if bClearDocR=True then begin StatusBar1.Panels[0].Text:=''; bClearDocR:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocR:=True;
  with dmORep do
  begin
    if fmDocsReal.Visible then
    begin
//      fmMainRnOffice.StatusBar1.Panels[1].Text:=FormatDateTime('dd.mm.yyyy hh:nn:ss',Now-(CommonSet.Of_DocRefresh/86400*2));

      quGetSync.Active:=False;
      quGetSync.ParamByName('PERS1').AsInteger:=Person.Id;
      quGetSync.ParamByName('PERS2').AsInteger:=Person.Id;
      quGetSync.ParamByName('DACT').AsString:=FormatDateTime('dd.mm.yyyy hh:nn:ss',Now-(60/86400));
      quGetSync.Active:=True;
      if quGetSync.RecordCount>0 then
      begin
        CurId:=quDocsRSelID.AsInteger;
        try
          ViewDocsR.BeginUpdate;

          quGetSync.First;
          while not quGetSync.Eof do
          begin
            if quDocsRSel.Locate('ID',quGetSyncIDDOC.AsInteger,[]) then quDocsRSel.Refresh;
            quGetSync.Next;
          end;
        finally
          quDocsRSel.Locate('ID',CurId,[]);
          ViewDocsR.EndUpdate;
        end;
      end;

      quGetSync.Active:=False;
    end else
    begin
//      fmMainRnOffice.StatusBar1.Panels[1].Text:='NoVisible';
    end;
  end;
end;

procedure TfmDocsReal.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsR.Visible then
    begin
      LevelDocsR.Visible:=False;
      LevelCardsR.Visible:=True;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;
      SpeedItem10.Visible:=True;

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ������ ...');
      delay(10);

      ViewCardsR.BeginUpdate;

      dsquDocsRCard.DataSet:=nil;

      quDocsRCard.Active:=False;
      quDocsRCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsRCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsRCard.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsRCard.ParamByName('IDPERSON1').AsInteger:=Person.Id;
      quDocsRCard.Active:=True;

      dsquDocsRCard.DataSet:=quDocsRCard;

      ViewCardsR.EndUpdate;

      Memo1.Lines.Add('��.');
      delay(10);

    end else
    begin
      LevelDocsR.Visible:=True;
      LevelCardsR.Visible:=False;

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ������ ...');
      delay(10);

      ViewDocsR.BeginUpdate;
      prFormQuDocsR;
      ViewDocsR.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;
      SpeedItem10.Visible:=False;

      Memo1.Lines.Add('��.');
      delay(10);

    end;
  end;
end;

procedure TfmDocsReal.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsReal.acPrint1Execute(Sender: TObject);
var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
    rSumR,rSumR0,rSumRT:Real;
    ISS:Integer;
begin
//������ �������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      iSS:=prISS(quDocsRSelIDSKL.AsInteger);

      rSumRT:=0;

      quSpecRSel2.Active:=False;
      quSpecRSel2.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel2.Active:=True;

      quSpecRSel2.First;
      while not quSpecRSel2.Eof do
      begin
        rSumR:=rv(quSpecRSel2SUMR.AsFloat);
        if quSpecRSel2CATEGORY.AsInteger=4 then rSumRT:=rSumRT+rv(quSpecRSel2SUMR.AsFloat);
        if iSS=2 then rSumR0:=rv(quSpecRSel2SUMOUT0.AsFloat)
        else rSumR0:=rSumR;

        quSpecRSel2.Edit;
        quSpecRSel2SUMR.AsFloat:=rSumR;
        quSpecRSel2SUMOUT0.AsFloat:=rSumR0;
        quSpecRSel2SUMNDSOUT.AsFloat:=rSumR-rSumR0;
        if abs(rSumR0-rSumR)<0.01 then quSpecRSel2IDNDS.AsInteger:=1;
        quSpecRSel2.Post;

        quSpecRSel2.Next;
      end;
      quSpecRSel2.First;


      frRepDocsReal.LoadFromFile(CurDir + 'ttn12.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      prFindCl(quDocsRSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli2Otpr']:=S5;
      frVariables.Variable['Cli2OtprAdr']:=S6;
      frVariables.Variable['Cli2RSch']:=S7;
      frVariables.Variable['Cli2KSch']:=S8;
      frVariables.Variable['Cli2Bank']:=S9;
      frVariables.Variable['Cli2Bik']:=S10;


      prFindCl(quDocsRSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli1Otpr']:=S5;
      frVariables.Variable['Cli1OtprAdr']:=S6;
      frVariables.Variable['Cli1RSch']:=S7;
      frVariables.Variable['Cli1KSch']:=S8;
      frVariables.Variable['Cli1Bank']:=S9;
      frVariables.Variable['Cli1Bik']:=S10;

      frVariables.Variable['sSum']:=MoneyToString(abs(quDocsRSelSUMUCH.AsFloat),True,False);
      frVariables.Variable['rSum']:=quDocsRSelSUMUCH.AsFloat;

      frVariables.Variable['sSumT']:=MoneyToString(abs(rSumRT),True,False);
      frVariables.Variable['rSumT']:=rSumRT;

      frRepDocsReal.ReportName:='��������� - ���������� �� �������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel2.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.acCopyExecute(Sender: TObject);
var Par:Variant;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=4;
    par[1]:=quDocsRSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=8;
      taHeadDocId.AsInteger:=quDocsRSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsRSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsRSelIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsRSelNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsRSelIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsRSelNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsRSelSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsRSelSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=8;
        taSpecDocIdHead.AsInteger:=quSpecRSelIDHEAD.AsInteger;
        taSpecDocNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecRSelIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecRSelSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecRSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecRSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpecDocKm.AsFloat:=quSpecRSelKM.AsFloat;
        taSpecDocPriceUch1.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpecDoc.Post;
      
        quSpecRSel.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsReal.acInsertDExecute(Sender: TObject);
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocR') then
        begin
          prAllViewOff;

          fmAddDoc4.Caption:='��������� �� ���������� : ��������������.';
          fmAddDoc4.cxTextEdit1.Text:=prGetNum(8,0);
          fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxTextEdit1.Tag:=0;

          fmAddDoc4.cxTextEdit2.Text:='';
          fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit1.Date:=Date;
          fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit2.Date:=Date;
          fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxCalcEdit1.EditValue:=0;
          fmAddDoc4.cxCalcEdit1.Properties.ReadOnly:=False;

          if taNDS.Active=False then taNDS.Active:=True;
            taNds.FullRefresh;;

          fmAddDoc4.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc4.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
          fmAddDoc4.cxButtonEdit1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxButtonEdit1.Enabled:=True;

    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
            quMHAll.FullRefresh;

          fmAddDoc4.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc4.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmAddDoc4.cxLabel1.Enabled:=True;
          fmAddDoc4.cxLabel2.Enabled:=True;
          fmAddDoc4.cxLabel3.Enabled:=True;
          fmAddDoc4.cxLabel4.Enabled:=True;
          fmAddDoc4.cxLabel7.Enabled:=True;
          fmAddDoc4.cxLabel5.Enabled:=True;
          fmAddDoc4.cxLabel6.Enabled:=True;
          fmAddDoc4.N1.Enabled:=True;

          fmAddDoc4.ViewDoc4.OptionsData.Editing:=True;
          fmAddDoc4.ViewDoc4.OptionsData.Deleting:=True;

          fmAddDoc4.cxButton1.Enabled:=True;
          CloseTe(fmAddDoc4.taSpec);

          fmAddDoc4.acSaveDoc.Enabled:=True;

          taSpecDoc.First;

          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecKM.AsFloat:=taSpecDocKm.AsFloat;
                taSpecQuant.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecPriceIn.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSumIn.AsFloat:=RoundVal(taSpecDocSumIn.AsFloat);
                taSpecPriceR.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSumR.AsFloat:=RoundVal(taSpecDocSumUch.AsFloat);
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=RoundVal(taSpecDocSumUch.AsFloat)-RoundVal(taSpecDocSumIn.AsFloat);
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                  taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpecTCard.AsInteger:=prTypeTC(taSpecDocIdCard.AsInteger);
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddDoc4.Show;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsReal.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsReal.Excel1Click(Sender: TObject);
begin
//������� � ������
  if LevelDocsR.Visible then prNExportExel5(ViewDocsR)
  else prNExportExel5(ViewCardsR);
end;

procedure TfmDocsReal.acPrintTTN13Execute(Sender: TObject);
begin
//������ ��� ��.��������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecRSel2.Active:=False;
      quSpecRSel2.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel2.Active:=True;

      CloseTa(taSpecRPrint);

      quSpecRSel2.First;
      while not quSpecRSel2.Eof do
      begin
        taSpecRPrint.Append;
        taSpecRPrintNum.AsInteger:=quSpecRSel2ID.AsInteger;
        taSpecRPrintIdGoods.AsInteger:=quSpecRSel2IDCARD.AsInteger;
        taSpecRPrintNameG.AsString:=quSpecRSel2NAMEC.AsString;
        taSpecRPrintIM.AsInteger:=quSpecRSel2IDM.AsInteger;
        taSpecRPrintSM.AsString:=quSpecRSel2SM.AsString;
        taSpecRPrintQuant.AsFloat:=quSpecRSel2QUANT.AsFloat;
        taSpecRPrintPriceIn.AsFloat:=quSpecRSel2PRICEIN.AsFloat;
        taSpecRPrintSumIn.AsFloat:=quSpecRSel2SUMIN.AsFloat;
        taSpecRPrintPriceR.AsFloat:=quSpecRSel2PRICER.AsFloat;
        taSpecRPrintSumR.AsFloat:=rv(quSpecRSel2SUMR.AsFloat);
        taSpecRPrintSumNac.AsFloat:=quSpecRSel2SUMR.AsFloat-quSpecRSel2SUMIN.AsFloat;
        taSpecRPrintProcNac.AsFloat:=0;
        if quSpecRSel2SUMIN.AsFloat<>0 then
          taSpecRPrintProcNac.AsFloat:=RoundEx((quSpecRSel2SUMR.AsFloat-quSpecRSel2SUMIN.AsFloat)/quSpecRSel2SUMIN.AsFloat*10000)/100;
        if quSpecRSel2KM.AsFloat>0 then taSpecRPrintKM.AsFloat:=quSpecRSel2KM.AsFloat
        else
        begin
          taSpecRPrintKM.AsFloat:=prFindKM(quSpecRSel2IDM.AsInteger);
        end;
        taSpecRPrintTCard.AsInteger:=quSpecRSel2TCard.AsInteger;

        if quSpecRSel2TCard.AsInteger=1 then
        taSpecRPrintMassa.AsString:=prFindMassaTC(quSpecRSel2IDCARD.AsInteger);

        taSpecRPrint.Post;

        quSpecRSel2.Next;
      end;


      frRepDocsReal.LoadFromFile(CurDir + 'ttn13_1.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsReal.ReportName:='��������� - ���������� �� �������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel2.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.acAddDocsExecute(Sender: TObject);
Var IDH,IDS:INteger;
    rSum:Real;
begin
// ������������ ��������� ��������� - �������� � �����������
  with dmO do
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;
    if MessageDlg('������������ ��������� �� �������� � �����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quSpecRSel1.Active:=False;
      quSpecRSel1.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel1.ParamByName('OPER').AsInteger:=1; //��������
      quSpecRSel1.Active:=True;
      if quSpecRSel1.RecordCount>0 then
      begin
        //��������� �������� ��������
        IDH:=GetId('DocOutB');
        quDOBHEAD.Active:=False;
        quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
        quDOBHEAD.Active:=True;

        quDOBHEAD.Append;
        quDOBHEADID.AsInteger:=IDH;
        quDOBHEADDATEDOC.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDOBHEADNUMDOC.AsString:=quDocsRSelNUMDOC.AsString;
        quDOBHEADDATESF.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDOBHEADNUMSF.AsString:='';
        quDOBHEADIDCLI.AsInteger:=0;
        quDOBHEADIDSKL.AsInteger:=quDocsRSelIDSKL.AsInteger;
        quDOBHEADSUMIN.AsFloat:=0;
        quDOBHEADSUMUCH.AsFloat:=0;
        quDOBHEADSUMTAR.AsFloat:=0;
        quDOBHEADSUMNDS0.AsFloat:=0;
        quDOBHEADSUMNDS1.AsFloat:=0;
        quDOBHEADSUMNDS2.AsFloat:=0;
        quDOBHEADPROCNAC.AsFloat:=0;
        quDOBHEADIACTIVE.AsInteger:=0;
        quDOBHEADOPER.AsString:='��';
        quDOBHEADCOMMENT.AsString:='�������� �������� � ����������.';
        quDOBHEAD.Post;

        quDOBSpec.Active:=False;
        quDobSpec.ParamByName('IDH').AsInteger:=IDH;
        quDobSpec.Active:=True;

        rSum:=0;
        IDS:=1;

        quSpecRSel1.First;
        while not quSpecRSel1.Eof do
        begin
          if quSpecRSel1QUANT.AsFloat<0 then
          begin

            quDobSpec.Append;
            quDOBSPECIDHEAD.AsInteger:=IDH;
            quDOBSPECID.AsInteger:=IDS;
            quDOBSPECSIFR.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quDOBSPECNAMEB.AsString:=quSpecRSel1NAMEC.AsString;
            quDOBSPECCODEB.AsString:='';
            quDOBSPECKB.AsFloat:=1;
            quDOBSPECDSUM.AsFloat:=0;
            quDOBSPECRSUM.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quDOBSPECQUANT.AsFloat:=quSpecRSel1QUANT.AsFloat*(-1);
            quDOBSPECIDCARD.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quDOBSPECIDM.AsInteger:=quSpecRSel1IDM.AsInteger;
            quDOBSPECKM.AsFloat:=quSpecRSel1KM.AsFloat;
            quDOBSPECPRICER.AsFloat:=0;
            quDobSpec.Post;

            inc(IDS);

            rSum:=rSum+quSpecRSel1SUMIN.AsFloat*(-1);
          end;

          quSpecRSel1.Next;
        end;

        quDobHead.Edit;
        quDOBHEADSUMIN.AsFloat:=rSum;
        quDOBHEADSUMUCH.AsFloat:=rSum;
        quDobHead.Post;

        quDobHead.Active:=False;
        quDOBSpec.Active:=False;
      end;
      quSpecRSel1.Active:=False;

      quSpecRSel1.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel1.ParamByName('OPER').AsInteger:=2; //�����������
      quSpecRSel1.Active:=True;
      if quSpecRSel1.RecordCount>0 then
      begin
        IDH:=GetId('DocAct');

        quDocsActsId.Active:=False;
        quDocsActsId.ParamByName('IDH').AsInteger:=IDH;
        quDocsActsId.Active:=True;

        quDocsActsId.Append;
        quDocsActsIdID.AsInteger:=IDH;
        quDocsActsIdDATEDOC.AsDateTime:=quDocsRSelDATEDOC.AsDateTime;
        quDocsActsIdNUMDOC.AsString:=quDocsRSelNUMDOC.AsString;
        quDocsActsIdIDSKL.AsInteger:=quDocsRSelIDSKL.AsInteger;
        quDocsActsIdSUMIN.AsFloat:=0;
        quDocsActsIdSUMUCH.AsFloat:=0;
        quDocsActsIdOPER.AsString:='��';
        quDocsActsIdIACTIVE.AsInteger:=0;
        quDocsActsIdCOMMENT.AsString:='����������� �������� � ����������.';
        quDocsActsId.Post;

        quSpecAO.Active:=False;
        quSpecAO.ParamByName('IDH').AsInteger:=IDH;
        quSpecAO.Active:=True;

        quSpecAO.First;
        while not quSpecAO.Eof do quSpecAO.Delete;

        rSum:=0;
        IDS:=1;

        quSpecRSel1.First;
        while not quSpecRSel1.Eof do
        begin
          if quSpecRSel1QUANT.AsFloat<0 then
          begin
            quSpecAO.Append;
            quSpecAOIDHEAD.AsInteger:=IDH;
            quSpecAOID.AsInteger:=IDS;
            quSpecAOIDCARD.AsInteger:=quSpecRSel1IDCARD.AsInteger;
            quSpecAOQUANT.AsFloat:=quSpecRSel1QUANT.AsFloat*(-1);
            quSpecAOIDM.AsInteger:=quSpecRSel1IDM.AsInteger;
            quSpecAOKM.AsFloat:=quSpecRSel1KM.AsFloat;
            quSpecAOPRICEIN.AsFloat:=quSpecRSel1PRICEIN.AsFloat;
            quSpecAOSUMIN.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quSpecAOPRICEINUCH.AsFloat:=quSpecRSel1PRICEIN.AsFloat;
            quSpecAOSUMINUCH.AsFloat:=quSpecRSel1SUMIN.AsFloat*(-1);
            quSpecAOTCARD.AsInteger:=quSpecRSel1TCARD.AsInteger;
            quSpecAO.Post;

            inc(IDS);

            rSum:=rSum+quSpecRSel1SUMIN.AsFloat*(-1);
          end;
          quSpecRSel1.Next;
        end;

        quDocsActsId.Edit;
        quDocsActsIdSUMIN.AsFloat:=rSum;
        quDocsActsIdSUMUCH.AsFloat:=rSum;
        quDocsActsId.Post;


        quSpecAO.Active:=False;
        quDocsActsId.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsReal.ViewDocsRCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsR.ColumnCount-1 do
  begin
    if ViewDocsR.Columns[i].Name='ViewDocsRIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF;
end;

procedure TfmDocsReal.N8Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnOutR := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsReal.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsReal.acPrint2Execute(Sender: TObject);
var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
    rSumR,rSumR0:Real;
    iSS:INteger;
begin
  //������ ����� �������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      iSS:=prISS(quDocsRSelIDSKL.AsInteger);

      quSpecRSel2.Active:=False;
      quSpecRSel2.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel2.Active:=True;

      quSpecRSel2.First;
      while not quSpecRSel2.Eof do
      begin
        rSumR:=rv(quSpecRSel2SUMR.AsFloat);
        if iSS=2 then rSumR0:=rv(quSpecRSel2SUMOUT0.AsFloat)
        else rSumR0:=rSumR;

        quSpecRSel2.Edit;
        quSpecRSel2SUMR.AsFloat:=rSumR;
        quSpecRSel2SUMOUT0.AsFloat:=rSumR0;
        quSpecRSel2SUMNDSOUT.AsFloat:=rSumR-rSumR0;

        if abs(rSumR0-rSumR)<0.01 then quSpecRSel2IDNDS.AsInteger:=1;

        quSpecRSel2.Post;
        quSpecRSel2.Next;
      end;
      quSpecRSel2.First;

      frRepDocsReal.LoadFromFile(CurDir + 'schfr.frf');

      frVariables.Variable['DocNum']:=quDocsRSelNUMSF.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      prFindCl(quDocsRSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli2Otpr']:=S5;
      frVariables.Variable['Cli2OtprAdr']:=S6;
      frVariables.Variable['Cli2RSch']:=S7;
      frVariables.Variable['Cli2KSch']:=S8;
      frVariables.Variable['Cli2Bank']:=S9;
      frVariables.Variable['Cli2Bik']:=S10;


      prFindCl(quDocsRSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli1Otpr']:=S5;
      frVariables.Variable['Cli1OtprAdr']:=S6;
      frVariables.Variable['Cli1RSch']:=S7;
      frVariables.Variable['Cli1KSch']:=S8;
      frVariables.Variable['Cli1Bank']:=S9;
      frVariables.Variable['Cli1Bik']:=S10;

      frVariables.Variable['sSum']:=MoneyToString(abs(quDocsRSelSUMUCH.AsFloat),True,False);

      frRepDocsReal.ReportName:='���� �������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel2.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.acCalcRExecute(Sender: TObject);
begin
//�����
// ����������� ���������
  fmSummary1:=tfmSummary1.Create(Application);
  fmSummary1.PivotGrid1.Visible:=True;
  fmSummary1.PivotGrid2.Visible:=False;
  fmSummary1.Caption:=fmDocsReal.Caption;
  fmSummary1.ShowModal;
  fmSummary1.Release;
end;

procedure TfmDocsReal.acPrintTYExecute(Sender: TObject);
Var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
    rQ1,rQ2:Real;
begin
  //������ ������������� �������������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      CloseTa(taSpTY);

      quCTO1.Active:=False;
      quCTO1.Active:=True;

      rQ1:=0; rQ2:=0;

      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        taSpTY.Append;
        taSpTYNum.AsInteger:=quSpecRSelID.AsInteger;
        taSpTYIdGoods.AsInteger:=quSpecRSelIDCARD.AsInteger;
        taSpTYNameG.AsString:=quSpecRSelNAMEC.AsString;
        taSpTYIM.AsInteger:=quSpecRSelIDM.AsInteger;
        taSpTYSM.AsString:=quSpecRSelSM.AsString;
        taSpTYQuant.AsFloat:=quSpecRSelQUANT.AsFloat;
        taSpTYPriceIn.AsFloat:=quSpecRSelPRICEIN.AsFloat;
        taSpTYSumIn.AsFloat:=quSpecRSelSUMIN.AsFloat;
        taSpTYPriceR.AsFloat:=quSpecRSelPRICER.AsFloat;
        taSpTYSumR.AsFloat:=quSpecRSelSUMR.AsFloat;
        taSpTYSumNac.AsFloat:=quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat;
        taSpTYProcNac.AsFloat:=0;
        if quSpecRSelSUMIN.AsFloat<>0 then
          taSpTYProcNac.AsFloat:=RoundEx((quSpecRSelSUMR.AsFloat-quSpecRSelSUMIN.AsFloat)/quSpecRSelSUMIN.AsFloat*10000)/100;
        if quSpecRSelKM.AsFloat>0 then taSpTYKM.AsFloat:=quSpecRSelKM.AsFloat
        else
        begin
          taSpTYKM.AsFloat:=prFindKM(quSpecRSelIDM.AsInteger);
        end;
        taSpTYTCard.AsInteger:=quSpecRSelTCard.AsInteger;

        if (pos('��',quSpecRSelSM.AsString)>0)or(pos('��',quSpecRSelSM.AsString)>0)or(pos('��',quSpecRSelSM.AsString)>0) then
        begin
          rQ2:=rQ2+quSpecRSelQUANT.AsFloat;
        end else
        begin
          rQ1:=rQ1+quSpecRSelQUANT.AsFloat;
        end;

        if quSpecRSelTCard.AsInteger=1 then taSpTYMassa.AsString:=prFindMassaTC(quSpecRSelIDCARD.AsInteger);

        taSpTYCTO.AsString:=quSpecRSelNAMECTO.AsString;

        if quCTO1.Locate('ID',quSpecRSelID1.AsInteger,[]) then
        begin
          taSpTYCTOCOMM.AsString:=quCTO1NAME1.AsString;
          taSpTYComm1.
          AsString:=quCTO1COMM1.AsString;
          taSpTYComm2.AsString:=quCTO1COMM2.AsString;
          taSpTYComm3.AsString:=quCTO1COMM3.AsString;
          taSpTYComm4.AsString:=quCTO1COMM4.AsString;
          taSpTYComm5.AsString:=quCTO1COMM5.AsString;
        end else
        begin
          taSpTYCTOCOMM.AsString:='';
          taSpTYComm1.AsString:='';
          taSpTYComm2.AsString:='';
          taSpTYComm3.AsString:='';
          taSpTYComm4.AsString:='';
          taSpTYComm5.AsString:='';
        end;

        taSpTY.Post;

        quSpecRSel.Next;
      end;
      quCTO1.Active:=False;

      frRepDocsReal.LoadFromFile(CurDir + 'ty.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);

      prFindCl(quDocsRSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      prFindCl(quDocsRSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;

      frVariables.Variable['Q1']:=rQ1;
      frVariables.Variable['Q2']:=rQ2;

      {
      frVariables.Variable['FromMH']:=quDocsRSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsRSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      prFindCl(quDocsRSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli2Otpr']:=S5;
      frVariables.Variable['Cli2OtprAdr']:=S6;
      frVariables.Variable['Cli2RSch']:=S7;
      frVariables.Variable['Cli2KSch']:=S8;
      frVariables.Variable['Cli2Bank']:=S9;
      frVariables.Variable['Cli2Bik']:=S10;

      prFindCl(quDocsRSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli1Otpr']:=S5;
      frVariables.Variable['Cli1OtprAdr']:=S6;
      frVariables.Variable['Cli1RSch']:=S7;
      frVariables.Variable['Cli1KSch']:=S8;
      frVariables.Variable['Cli1Bank']:=S9;
      frVariables.Variable['Cli1Bik']:=S10;

      frVariables.Variable['sSum']:=MoneyToString(abs(quDocsRSelSUMUCH.AsFloat),True,False);
 }
      frRepDocsReal.ReportName:='������������ �������������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.N12Click(Sender: TObject);
begin
//��������
  ViewCardsR.BeginUpdate;
  ViewCardsR.DataController.Groups.FullCollapse;
  ViewCardsR.EndUpdate;
end;

procedure TfmDocsReal.N13Click(Sender: TObject);
begin
//��������
  ViewCardsR.BeginUpdate;
  ViewCardsR.DataController.Groups.FullExpand;
  ViewCardsR.EndUpdate;
end;

procedure TfmDocsReal.Excel2Click(Sender: TObject);
begin
//������� � ������
  if LevelDocsR.Visible then prNExportExel5(ViewDocsR)
  else prNExportExel5(ViewCardsR);
end;

procedure TfmDocsReal.acSetStatusBZExecute(Sender: TObject);
Var iStz:INteger;
    IDH:INteger;
    rSumInDoc,rSumTDoc:Real;
    StrWk:String;
    IDHCB:Integer;
    rSum0,rSum,rSumQ:Real;
    bCB:Boolean;
begin
  // �������� ������
  with dmORep do
  begin
    if quDocsRSel.RecordCount=0 then exit;
    if (quDocsRSelBZTYPE.AsInteger=0) then
    begin
      Memo1.Lines.Add('�������� ��� ��������� (������). ��������� ������� ����������.');
      exit;
    end;
    if (quDocsRSelIACTIVE.AsInteger=1) then
    begin
      Memo1.Lines.Add('�������� ������ ���������. ��������� ������� ����������.');
      exit;
    end;
    iStz:=quDocsRSelBZSTATUS.AsInteger;
    fmSetStatusBZ.cxRadioGroup1.ItemIndex:=iStz;
    case iStz of
    0:begin
        fmSetStatusBZ.cxButton1.Enabled:=True;
        fmSetStatusBZ.cxButton2.Enabled:=False;
        fmSetStatusBZ.cxButton3.Enabled:=False;
      end;
    1:begin
        fmSetStatusBZ.cxButton1.Enabled:=False;
        fmSetStatusBZ.cxButton2.Enabled:=True;
        fmSetStatusBZ.cxButton3.Enabled:=False;
      end;
    2:begin
        fmSetStatusBZ.cxButton1.Enabled:=False;
        fmSetStatusBZ.cxButton2.Enabled:=False;
        fmSetStatusBZ.cxButton3.Enabled:=True;
      end;
    3:begin
        fmSetStatusBZ.cxButton1.Enabled:=False;
        fmSetStatusBZ.cxButton2.Enabled:=False;
        fmSetStatusBZ.cxButton3.Enabled:=False;
      end;
    end;

    fmSetStatusBZ.ShowModal;
    if fmSetStatusBZ.ModalResult=mrOk then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('��������� ������� ��������� ');
      iStz:=fmSetStatusBZ.cxButton4.Tag;
      case iStz of
      0:begin
          Memo1.Lines.Add('C���������� ');
        end;
      1:begin
          if cando('DocRQuantF') then
          begin
            //�������� ������ - �������
            quDocsRSel.Edit;
            quDocsRSelBZSTATUS.AsInteger:=1;
            quDocsRSel.Post;
            quDocsRSel.Refresh;

            prSetSync('DocR','ST1',quDocsRSelID.AsInteger,quDocsRSelIDSKL.AsInteger);

            Memo1.Lines.Add('����������� ')
          end else Memo1.Lines.Add('��� ����.');
        end;
      2:begin
          if cando('DocRQuantS') then
          begin
            //�������� ������ - ���������
            quDocsRSel.Edit;
            quDocsRSelBZSTATUS.AsInteger:=2;
            quDocsRSel.Post;
            quDocsRSel.Refresh;

            prSetSync('DocR','ST2',quDocsRSelID.AsInteger,quDocsRSelIDSKL.AsInteger);

            Memo1.Lines.Add('��������');
          end else Memo1.Lines.Add('��� ����.');
        end;
      3:begin
          if cando('DocRQuantP') then
          begin
            //�������� ������ - ������� - ����� ������������ �������� �����
            IDHCB:=0;
            IDH:=0;
            //������������
            with dmORep do
            with dmO do
            begin
              if not CanDo('prOnDocR') then begin Memo1.Lines.Add('��� ����.'); exit; end;
              if not CanEdit(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

              bCB:=False;

              if (CommonSet.AutoZak=1) then
              begin
                Memo1.Lines.Add('���� ��.'); delay(10);
                with dmCB do
                begin
                  try
                    msConnection.Connected:=False;
                    Strwk:='FILE NAME='+CurDir+'Ecr.udl';
                    msConnection.ConnectionString:=Strwk;
                    delay(10);
                    msConnection.Connected:=True;
                    delay(10);
                    if msConnection.Connected then
                    begin
                      bCB:=True;
                      Memo1.Lines.Add('����� � �� ��.');
                    end;
                  except
                  end;
                end;
              end;

              if bCB=False then
              begin
                Memo1.Lines.Add('������ ���������� � ��. ��������� ������� ����������. ���������� � ���������������.'); delay(10);
                exit;
              end;

              prButtonSet(False);

              Memo1.Lines.Add('�����.. ���� ������������� ���������.');

              if quDocsRSel.RecordCount>0 then //���� ��� ������������
              begin
                if quDocsRSelIACTIVE.AsInteger=0 then
                begin
                  if prTOFind(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger)=1 then
                  begin //�� ����
                    prTODel(Trunc(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDSKL.AsInteger);
                  end;

                  IDH:=quDocsRSelID.AsInteger;

                  try
                    IDHCB:=quDocsRSelIDHCB.AsInteger;
                  except
                    IDHCB:=0;
                  end;
                  //����� �������������� ����� ��������� �� - � ������� ����� - �.�. ��� ������.
                  // ��� �� ����

                  //������� ��� ������ ���� ���� �� ������ ������ �� ������� ��������� � �� ������� � �� �������, ������� ��� ���� �� �����
                  prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
                  prDelPart.ParamByName('DTYPE').AsInteger:=8;
                  prDelPart.ExecProc;


                  //����������� ��� ������
                  prOn(IDH,quDocsRSelIDSKL.AsInteger,TRUNC(quDocsRSelDATEDOC.AsDateTime),quDocsRSelIDCLI.AsInteger,rSumInDoc,rSumTDoc);

                  //�������� ������
                  quDocsRSel.Edit;
                  quDocsRSelSUMIN.AsFloat:=rSumInDoc;
                  quDocsRSelSUMTAR.AsFloat:=rSumTDoc;
                  quDocsRSelIACTIVE.AsInteger:=1;   //�������
                  quDocsRSelBZSTATUS.AsInteger:=3;  //�������
                  quDocsRSel.Post;
                  quDocsRSel.Refresh;
                end;
              end;
            end;

            Memo1.Lines.Add('������������� ��.');
            if (CommonSet.AutoZak=1)and(IDHCB>0)and(IDH>0) then
            begin
              Memo1.Lines.Add('��������� ������� ��.'); delay(10);
              with dmCB do
              begin
                try
              {    msConnection.Connected:=False;   //������ ��������� ��� ��������
                  Strwk:='FILE NAME='+CurDir+'Ecr.udl';
                  msConnection.ConnectionString:=Strwk;
                  delay(10);
                  msConnection.Connected:=True;
                  delay(10);}
                  if msConnection.Connected then
                  begin
                    //������ ������������ ����� ������

                    quDocZHRec.Active:=False;
                    quDocZHRec.Parameters.ParamByName('IDH').Value:=IDHCB;
                    quDocZHRec.Active:=True;

                    if quDocZHRec.RecordCount>0 then
                    if quDocZHRecIACTIVE.AsInteger<=20 then //�������� -  ���� <9
                    begin
                      quSpecRToCB.Active:=False;
                      quSpecRToCB.ParamByName('IDH').AsInteger:=IDH;
                      quSpecRToCB.Active:=True;

                      quDocZSSel.Active:=False;
                      quDocZSSel.Parameters.ParamByName('IIDH').Value:=IDHCB;
                      quDocZSSel.Active:=True;
                      quDocZSSel.First;
                      while not quDocZSSel.Eof do
                      begin
                        if quSpecRToCB.Locate('CODEZAK',quDocZSSelCODE.AsInteger,[]) then
                        begin
                          if (quSpecRToCBQUANT.AsFloat>0)or(quSpecRToCBBZQUANTF.AsFloat>0) then
                          begin
                            quDocZSSel.Edit;

                            quDocZSSelCLIQUANTN.AsFloat:=quSpecRToCBQUANT.AsFloat;
                            quDocZSSelCLIPRICEN.AsFloat:=quSpecRToCBPRICER.AsFloat;
                            if (quSpecRToCBQUANT.AsFloat>0) then quDocZSSelCLIPRICE0N.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat)
                            else quDocZSSelCLIPRICE0N.AsFloat:=rv(quSpecRToCBPRICER.AsFloat/(100+quSpecRToCBPROC.AsFloat)*100);

                            quDocZSSelCLINNUM.AsInteger:=quSpecRToCBID.AsInteger;

                            quDocZSSelCLIQUANT.AsFloat:=quSpecRToCBQUANT.AsFloat;
                            quDocZSSelCLIPRICE.AsFloat:=quSpecRToCBPRICER.AsFloat;
                            if (quSpecRToCBQUANT.AsFloat>0) then quDocZSSelCLIPRICE0.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat)
                            else quDocZSSelCLIPRICE0.AsFloat:=rv(quSpecRToCBPRICER.AsFloat/(100+quSpecRToCBPROC.AsFloat)*100);

                            quDocZSSelQUANTZFACT.AsFloat:=quSpecRToCBBZQUANTF.AsFloat;

                            quDocZSSel.Post;
                          end;
                        end;

                        quDocZSSel.Next;
                      end;
                      quDocZSSel.Active:=False;

                      // �� ������������ ����������� - ������ ��������� ������������ � ��������� ��������
                      
                      quDocZSNacl.Active:=False;
                      quDocZSNacl.Parameters.ParamByName('IDH').Value:=IDHCB;
                      quDocZSNacl.Active:=True;

                      rSum0:=0;
                      rSum:=0;
                      rSumQ:=0;

                      quDocZSNacl.First;
                      while not quDocZSNacl.Eof do quDocZSNacl.Delete;

                      quSpecRToCB.First;
                      while not quSpecRToCB.Eof do
                      begin
                        if quSpecRToCBQUANT.AsFloat>0 then
                        begin
                          quDocZSNacl.Append;
                          quDocZSNaclIDH.AsINteger:=IDHCB;
                          quDocZSNaclIDS.AsINteger:=quSpecRToCBID.AsInteger;

                          if quSpecRToCBCATEGORY.AsInteger=1 then quDocZSNaclICODE.AsINteger:=quSpecRToCBCODEZAK.AsInteger
                          else quDocZSNaclICODE.AsINteger:=quSpecRToCBIDCARD.AsInteger;

                          quDocZSNaclICARDTYPE.AsINteger:=quSpecRToCBCATEGORY.AsInteger;
                          quDocZSNaclQUANT.AsFloat:=quSpecRToCBQUANT.AsFloat;
                          quDocZSNaclPRICE0.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat);
                          quDocZSNaclPRICE.AsFloat:=quSpecRToCBPRICER.AsFloat;
                          quDocZSNaclRNDS.AsFloat:=quSpecRToCBPROC.AsFloat;
                          quDocZSNaclRSUM0.AsFloat:=quSpecRToCBSUMOUT0.AsFloat;
                          quDocZSNaclRSUM.AsFloat:=quSpecRToCBSUMR.AsFloat;
                          quDocZSNaclRSUMNDS.AsFloat:=quSpecRToCBSUMNDSOUT.AsFloat;
                          quDocZSNaclNAME.AsString:=quSpecRToCBNAME.AsString;
                          quDocZSNacl.Post;

                          if quSpecRToCBCATEGORY.AsInteger=1 then
                          begin
                            rSum0:=rSum0+quSpecRToCBSUMOUT0.AsFloat;
                            rSum:=rSum+quSpecRToCBSUMR.AsFloat;
                            rSumQ:=rSumQ+quSpecRToCBQUANT.AsFloat;
                          end;

                        end;
                        quSpecRToCB.Next;
                      end;

                      quDocZSNacl.Active:=False;
                      quSpecRToCB.Active:=False;

                      //��� ������������ ������������ - ������ ���������

                      quDocZHRec.Edit;
                      quDocZHRecIACTIVE.AsInteger:=9; //��������� ��������
                      quDocZHRecSENDTO.AsInteger:=0;  //������ � �������� � �������
                      quDocZHRecIDHPRO.AsInteger:=IDH;
                      quDocZHRecSENDNACL.AsInteger:=1; //��� ���������� ���������� ��� ��������� �� �������

                      quDocZHRecIDATENACL.AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
                      quDocZHRecSNUMNACL.AsString:=quDocsRSelNUMDOC.AsString;
                      quDocZHRecIDATESCHF.AsInteger:=Trunc(quDocsRSelDATESF.AsDateTime);
                      quDocZHRecSNUMSCHF.AsString:=quDocsRSelNUMSF.AsString;

                      quDocZHRecCLISUMINN.AsFloat:=quDocsRSelSUMUCH.AsFloat;
                      quDocZHRecCLISUMIN0N.AsFloat:=rSum0;
                      quDocZHRecCLIQUANTN.AsFloat:=rSumQ;

                      quDocZHRec.Post;

                      if quDocsRSelSUMUCH.AsFloat<>rSum then
                        Memo1.Lines.Add('  - ������������ ����� ���������. ��������� - '+fts(quDocsRSelSUMUCH.AsFloat)+'. ������������ - '+fts(rSum)+'.');
                    end else
                    begin
                      Memo1.Lines.Add(' ������ ��������� �� ��� �������. �������������� ����������.');
                    end;
                    quDocZHRec.Active:=False;
                  end;
                  Memo1.Lines.Add('��������� ������� � �� ���������.');
                except
                  Memo1.Lines.Add('������ ����� � ��.');
                  msConnection.Connected:=False;
                end;
              end;
            end;

            prButtonSet(True);

            prSetSync('DocR','ST3',quDocsRSelID.AsInteger,quDocsRSelIDSKL.AsInteger);

            Memo1.Lines.Add('�������');
          end else Memo1.Lines.Add('��� ����.');
        end;
      end;
    end;
  end;
end;

procedure TfmDocsReal.acToProizvExecute(Sender: TObject);
Var iDateB,iDateE:INteger;
begin
  //����� � ������������ �� ��  //�� ������
  fmSelPerSkl3:=tfmSelPerSkl3.Create(Application);
  with fmSelPerSkl3 do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
    else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl3.ShowModal;
  if fmSelPerSkl3.ModalResult=mrOk then
  begin
    iDateB:=Trunc(fmSelPerSkl3.cxDateEdit1.Date);
    iDateE:=Trunc(fmSelPerSkl3.cxDateEdit2.Date)+1; //������ ���� <

    CommonSet.IdStore:=fmSelPerSkl3.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl3.cxLookupComboBox1.Text;
    CommonSet.DateFrom:=iDateB;
    CommonSet.DateTo:=iDateE; //������ ���� <

    with dmO do
    with dmORep do
    begin
      Memo1.Clear;

      Memo1.Lines.Add('�����.. ���� ������������ ������.'); delay(10);

      if fmSelPerSkl3.cxRadioButton1.Checked then
      begin
        quDocsRCard1.Active:=False;
        quDocsRCard1.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCard1.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCard1.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
        quDocsRCard1.Active:=True;
        Memo1.Lines.Add('������������ ��.'); delay(10);

        frRepDocsReal.LoadFromFile(CurDir + 'realtoprod.frf');
      end;

      if fmSelPerSkl3.cxRadioButton2.Checked then
      begin

        quDocsRCli.Active:=False;
        quDocsRCli.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCli.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCli.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
        quDocsRCli.Active:=True;

        quDocsRCard2.Active:=False;
        quDocsRCard2.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCard2.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCard2.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
        quDocsRCard2.Active:=True;

        quDocsRCard1.Active:=False;
        quDocsRCard1.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCard1.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCard1.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
        quDocsRCard1.Active:=True;

        quDocsRCliGr.Active:=False;
        quDocsRCliGr.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsRCliGr.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsRCliGr.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
        quDocsRCliGr.Active:=True;

        Memo1.Lines.Add('������������ ��.'); delay(10);

        frRepDocsReal.LoadFromFile(CurDir + 'realtoprod1.frf');
      end;

      frRepDocsReal.ReportName:='������ ���� � ������������.';

      frVariables.Variable['MH']:=CommonSet.NameStore;
      frVariables.Variable['SPER']:='   c '+ds1(iDateB)+' �� '+ds1(iDateE-1)+' ������������.';

      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quDocsRCard1.Active:=False;
      quDocsRCard2.Active:=False;
      quDocsRCli.Active:=False;
      quDocsRCliGr.Active:=False;
    end;

    fmSelPerSkl3.Release;
  end else fmSelPerSkl3.Release;
end;

procedure TfmDocsReal.acPrintActRaznExecute(Sender: TObject);
Var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
    rQ1,rQ2:Real;
begin
  //������ ������������� �������������
  if LevelDocsR.Visible=False then exit;
  with dmORep do
  with dmO do
  begin
    if quDocsRSel.RecordCount>0 then //���� ��� �������
    begin
      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=quDocsRSelID.AsInteger;
      quSpecRSel.Active:=True;

      CloseTe(tePrintA);

      rQ1:=0; rQ2:=0;

      quSpecRSel.First;
      while not quSpecRSel.Eof do
      begin
        if (abs(quSpecRSelBZQUANTS.AsFloat)>0) or (abs(quSpecRSelQUANT.AsFloat)>0) then
        begin
          tePrintA.Append;
          tePrintAiNUm.AsInteger:=quSpecRSelID.AsInteger;
          tePrintAiCode.AsInteger:=quSpecRSelIDCARD.AsInteger;
          tePrintANameCard.AsString:=quSpecRSelNAMEC.AsString;
          tePrintAiM.AsInteger:=quSpecRSelIDM.AsInteger;
          tePrintAsM.AsString:=quSpecRSelSM.AsString;
          tePrintAsMest.AsString:='';
          tePrintAQuant.AsFloat:=quSpecRSelBZQUANTS.AsFloat;
          tePrintAPrice.AsFloat:=quSpecRSelPRICER.AsFloat;
          tePrintANDS.AsFloat:=quSpecRSelPROC.AsFloat;
          tePrintARSum.AsFloat:=rv(quSpecRSelPRICER.AsFloat*quSpecRSelBZQUANTS.AsFloat);
          tePrintAQuantF.AsFloat:=quSpecRSelQUANT.AsFloat;
          tePrintArSumF.AsFloat:=rv(quSpecRSelPRICER.AsFloat*quSpecRSelQUANT.AsFloat);
          tePrintAQuantD.AsFloat:=quSpecRSelBZQUANTS.AsFloat-quSpecRSelQUANT.AsFloat;
          tePrintArSumD.AsFloat:=rv(quSpecRSelPRICER.AsFloat*quSpecRSelBZQUANTS.AsFloat)-rv(quSpecRSelPRICER.AsFloat*quSpecRSelQUANT.AsFloat);
          tePrintA.Post;
        end;
        quSpecRSel.Next;
      end;

      frRepDocsReal.LoadFromFile(CurDir + 'rActDif.frf');

      frVariables.Variable['Num']:=quDocsRSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsRSelDATEDOC.AsDateTime);

      prFindCl(quDocsRSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      prFindCl(quDocsRSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;

      frVariables.Variable['Q1']:=rQ1;
      frVariables.Variable['Q2']:=rQ2;

      frRepDocsReal.ReportName:='��� �����������.';
      frRepDocsReal.PrepareReport;
      frRepDocsReal.ShowPreparedReport;

      quSpecRSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsReal.frRepDocsRealGetValue(const ParName: String;
  var ParValue: Variant);
Var iCli,iCode:INteger;
    par:Variant;
begin
  if ParName='ColVal' then
  begin
    with dmORep do
    begin
      iCli:=quDocsRCliIDCLI.AsInteger;
      iCode:=quDocsRCard2IDCARD.AsInteger;
      ParValue:=0;

      par := VarArrayCreate([0,1], varInteger);
      par[0]:=iCode;
      par[1]:=iCli;
      if quDocsRCard1.Locate('IDCARD;IDCLI',par,[]) then
      begin
        ParValue:=quDocsRCard1BZQUANTF.AsFloat;
      end;
    end;
  end;
  if ParName='ColSum' then
  begin
    with dmORep do
    begin
      ParValue:=0;

      iCli:=quDocsRCliIDCLI.AsInteger;
      iCode:=quDocsRCard2PARENT.AsInteger;
      ParValue:=0;

      par := VarArrayCreate([0,1], varInteger);
      par[0]:=iCode;
      par[1]:=iCli;
      if quDocsRCliGr.Locate('PARENT;IDCLI',par,[]) then
      begin
        ParValue:=quDocsRCliGrBZQUANTF.AsFloat;
      end;
    end;
  end;
end;

procedure TfmDocsReal.acSendCBExecute(Sender: TObject);
Var IDH:INteger;
    StrWk:String;
    IDHCB:Integer;
    rSum0,rSum,rSumQ:Real;
begin
  // ��������� �������
  with dmORep do
  with dmO do
  begin
    if not CanDo('prOnDocR') then begin Memo1.Lines.Add('��� ����.'); exit; end;

    if quDocsRSel.RecordCount=0 then exit;
    if (quDocsRSelBZTYPE.AsInteger=0) then
    begin
      Memo1.Lines.Add('�������� ��� ��������� (������).');
      exit;
    end;
    if (quDocsRSelIACTIVE.AsInteger=1) then  //������ �������� ��� ���������
    begin
      try
        IDHCB:=quDocsRSelIDHCB.AsInteger;
        IDH:=quDocsRSelID.AsInteger;
      except
        IDHCB:=0;
        IDH:=0;
      end;

      if (CommonSet.AutoZak=1)and(IDHCB>0)and(IDH>0) then
      begin
        prButtonSet(False);

        Memo1.Lines.Add('��������� ������� ��.'); delay(10);
        with dmCB do
        begin
          try
            msConnection.Connected:=False;
            Strwk:='FILE NAME='+CurDir+'Ecr.udl';
            msConnection.ConnectionString:=Strwk;
            delay(10);
            msConnection.Connected:=True;
            delay(10);
            if msConnection.Connected then
            begin
              Memo1.Lines.Add('����� � �� ��.');
              //������ ������������ ����� ������

              quDocZHRec.Active:=False;
              quDocZHRec.Parameters.ParamByName('IDH').Value:=IDHCB;
              quDocZHRec.Active:=True;

              if quDocZHRec.RecordCount>0 then
              if quDocZHRecIACTIVE.AsInteger<=20 then //�������� -  ���� <9
              begin
                quSpecRToCB.Active:=False;
                quSpecRToCB.ParamByName('IDH').AsInteger:=IDH;
                quSpecRToCB.Active:=True;

                quDocZSSel.Active:=False;
                quDocZSSel.Parameters.ParamByName('IIDH').Value:=IDHCB;
                quDocZSSel.Active:=True;
                quDocZSSel.First;
                while not quDocZSSel.Eof do
                begin
                  if quSpecRToCB.Locate('CODEZAK',quDocZSSelCODE.AsInteger,[]) then
                  begin
                    if (quSpecRToCBQUANT.AsFloat>0)or(quSpecRToCBBZQUANTF.AsFloat>0) then
                    begin
                      quDocZSSel.Edit;

                      quDocZSSelCLIQUANTN.AsFloat:=quSpecRToCBQUANT.AsFloat;
                      quDocZSSelCLIPRICEN.AsFloat:=quSpecRToCBPRICER.AsFloat;
                      if (quSpecRToCBQUANT.AsFloat>0) then quDocZSSelCLIPRICE0N.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat)
                      else quDocZSSelCLIPRICE0N.AsFloat:=rv(quSpecRToCBPRICER.AsFloat/(100+quSpecRToCBPROC.AsFloat)*100);

                      quDocZSSelCLINNUM.AsInteger:=quSpecRToCBID.AsInteger;

                      quDocZSSelCLIQUANT.AsFloat:=quSpecRToCBQUANT.AsFloat;
                      quDocZSSelCLIPRICE.AsFloat:=quSpecRToCBPRICER.AsFloat;
                      if (quSpecRToCBQUANT.AsFloat>0) then quDocZSSelCLIPRICE0.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat)
                      else quDocZSSelCLIPRICE0.AsFloat:=rv(quSpecRToCBPRICER.AsFloat/(100+quSpecRToCBPROC.AsFloat)*100);

                      quDocZSSelQUANTZFACT.AsFloat:=quSpecRToCBBZQUANTF.AsFloat;

                      quDocZSSel.Post;
                    end;
                  end;

                  quDocZSSel.Next;
                end;
                quDocZSSel.Active:=False;

                // �� ������������ ����������� - ������ ��������� ������������ � ��������� ��������

                quDocZSNacl.Active:=False;
                quDocZSNacl.Parameters.ParamByName('IDH').Value:=IDHCB;
                quDocZSNacl.Active:=True;

                rSum0:=0;
                rSum:=0;
                rSumQ:=0;

                quDocZSNacl.First;
                while not quDocZSNacl.Eof do quDocZSNacl.Delete;

                quSpecRToCB.First;
                while not quSpecRToCB.Eof do
                begin
                  if quSpecRToCBQUANT.AsFloat>0 then
                  begin
                    quDocZSNacl.Append;
                    quDocZSNaclIDH.AsINteger:=IDHCB;
                    quDocZSNaclIDS.AsINteger:=quSpecRToCBID.AsInteger;

                    if quSpecRToCBCATEGORY.AsInteger=1 then quDocZSNaclICODE.AsINteger:=quSpecRToCBCODEZAK.AsInteger
                    else quDocZSNaclICODE.AsINteger:=quSpecRToCBIDCARD.AsInteger;

                    quDocZSNaclICARDTYPE.AsINteger:=quSpecRToCBCATEGORY.AsInteger;
                    quDocZSNaclQUANT.AsFloat:=quSpecRToCBQUANT.AsFloat;
                    quDocZSNaclPRICE0.AsFloat:=rv(quSpecRToCBSUMOUT0.AsFloat/quSpecRToCBQUANT.AsFloat);
                    quDocZSNaclPRICE.AsFloat:=quSpecRToCBPRICER.AsFloat;
                    quDocZSNaclRNDS.AsFloat:=quSpecRToCBPROC.AsFloat;
                    quDocZSNaclRSUM0.AsFloat:=quSpecRToCBSUMOUT0.AsFloat;
                    quDocZSNaclRSUM.AsFloat:=quSpecRToCBSUMR.AsFloat;
                    quDocZSNaclRSUMNDS.AsFloat:=quSpecRToCBSUMNDSOUT.AsFloat;
                    quDocZSNaclNAME.AsString:=quSpecRToCBNAME.AsString;
                    quDocZSNacl.Post;

                    if quSpecRToCBCATEGORY.AsInteger=1 then
                    begin
                      rSum0:=rSum0+quSpecRToCBSUMOUT0.AsFloat;
                      rSum:=rSum+quSpecRToCBSUMR.AsFloat;
                      rSumQ:=rSumQ+quSpecRToCBQUANT.AsFloat;
                    end;

                  end;
                  quSpecRToCB.Next;
                end;

                quDocZSNacl.Active:=False;
                quSpecRToCB.Active:=False;

                //��� ������������ ������������ - ������ ���������

                quDocZHRec.Edit;
                quDocZHRecIACTIVE.AsInteger:=9; //��������� ��������
                quDocZHRecSENDTO.AsInteger:=0;  //������ � �������� � �������
                quDocZHRecIDHPRO.AsInteger:=IDH;
                quDocZHRecSENDNACL.AsInteger:=1; //��� ���������� ���������� ��� ��������� �� �������

                quDocZHRecIDATENACL.AsInteger:=Trunc(quDocsRSelDATEDOC.AsDateTime);
                quDocZHRecSNUMNACL.AsString:=quDocsRSelNUMDOC.AsString;
                quDocZHRecIDATESCHF.AsInteger:=Trunc(quDocsRSelDATESF.AsDateTime);
                quDocZHRecSNUMSCHF.AsString:=quDocsRSelNUMSF.AsString;

                quDocZHRecCLISUMINN.AsFloat:=quDocsRSelSUMUCH.AsFloat;
                quDocZHRecCLISUMIN0N.AsFloat:=rSum0;
                quDocZHRecCLIQUANTN.AsFloat:=rSumQ;

                quDocZHRec.Post;

              end;
              quDocZHRec.Active:=False;
            end;
            Memo1.Lines.Add('��������� ������� � �� ���������.');
          except
            Memo1.Lines.Add('������ ����� � ��.');
            msConnection.Connected:=False;
          end;
        end;
        prButtonSet(True);
      end;
    end;
  end;
end;

end.
