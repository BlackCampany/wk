object fmAssPost: TfmAssPost
  Left = 211
  Top = 161
  Width = 846
  Height = 577
  Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 531
    Width = 838
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 838
    Height = 53
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Transparent = True
    end
    object cxButtonEdit1: TcxButtonEdit
      Tag = 1
      Left = 92
      Top = 16
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Properties.OnChange = cxButtonEdit1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Text = 'cxButtonEdit1'
      Width = 281
    end
    object cxButton1: TcxButton
      Left = 400
      Top = 12
      Width = 221
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 652
      Top = 12
      Width = 75
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 2
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 53
    Width = 157
    Height = 406
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxLabel1: TcxLabel
      Left = 8
      Top = 12
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 12
      Top = 40
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel6: TcxLabel
      Left = 12
      Top = 60
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxLabel3: TcxLabel
      Left = 12
      Top = 96
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1091' '#1055#1055
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
  end
  object GridAssPost: TcxGrid
    Left = 176
    Top = 61
    Width = 567
    Height = 376
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
    object ViewAssPost: TcxGridDBTableView
      OnDragDrop = ViewAssPostDragDrop
      OnDragOver = ViewAssPostDragOver
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsmePost
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.Selection = dmMC.cxStyle24
      Styles.Footer = dmMC.cxStyle5
      object ViewAssPostRecId: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'RecId'
        Visible = False
        Options.Editing = False
      end
      object ViewAssPostCode: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'Code'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
        Width = 55
      end
      object ViewAssPostName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
      end
      object ViewAssPostEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
        Options.Editing = False
      end
      object ViewAssPostPriceDog: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1076#1086#1075#1086#1074#1086#1088#1091
        DataBinding.FieldName = 'PriceDog'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewAssPostPricePP: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1055#1055
        DataBinding.FieldName = 'PricePP'
        Options.Editing = False
        Styles.Content = dmMC.cxStyle5
      end
      object ViewAssPostsMess: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'sMess'
        Width = 168
      end
      object ViewAssPostNameFull: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072' ('#1087#1086#1083#1085#1086#1077')'
        DataBinding.FieldName = 'NameFull'
        Options.Editing = False
        Width = 300
      end
    end
    object LevelAssPost: TcxGridLevel
      GridView = ViewAssPost
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 459
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
    Height = 72
    Width = 838
  end
  object mePost: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 280
    Top = 92
    object mePostCode: TIntegerField
      FieldName = 'Code'
    end
    object mePostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object mePostEdIzm: TStringField
      FieldName = 'EdIzm'
      Size = 10
    end
    object mePostPriceDog: TFloatField
      FieldName = 'PriceDog'
      DisplayFormat = '0.00'
    end
    object mePostPricePP: TFloatField
      FieldName = 'PricePP'
      DisplayFormat = '0.00'
    end
    object mePostNameFull: TStringField
      FieldName = 'NameFull'
      Size = 100
    end
    object mePostsMess: TStringField
      FieldName = 'sMess'
      Size = 100
    end
  end
  object dsmePost: TDataSource
    DataSet = mePost
    Left = 280
    Top = 140
  end
  object ActionManager1: TActionManager
    Left = 388
    Top = 104
    StyleName = 'XP Style'
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acRecalcAssPost: TAction
      Caption = 'acRecalcAssPost'
      ShortCut = 49234
      OnExecute = acRecalcAssPostExecute
    end
  end
end
