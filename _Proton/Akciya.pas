unit Akciya;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan, cxCheckBox;

type
  TfmAkciya = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridAkciya: TcxGrid;
    LevelAkciya: TcxGridLevel;
    FPAkciya: TFormPlacement;
    SpeedItem4: TSpeedItem;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    ViewAkciya: TcxGridDBTableView;
    amAkciya: TActionManager;
    acMoveRepOb: TAction;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    ViewAkciyaCode: TcxGridDBColumn;
    ViewAkciyaAType: TcxGridDBColumn;
    ViewAkciyaDateB: TcxGridDBColumn;
    ViewAkciyaDateE: TcxGridDBColumn;
    ViewAkciyaStatusOn: TcxGridDBColumn;
    ViewAkciyaStatusOf: TcxGridDBColumn;
    ViewAkciyaName: TcxGridDBColumn;
    ViewAkciyaNameGr: TcxGridDBColumn;
    ViewAkciyaVal1: TcxGridDBColumn;
    ViewAkciyaVal2: TcxGridDBColumn;
    LevelAkciya1: TcxGridLevel;
    ViewAkciya1: TcxGridDBTableView;
    teA: TdxMemData;
    dsteA: TDataSource;
    teACode: TIntegerField;
    teADateB: TDateField;
    teADateE: TDateField;
    teAStOn: TSmallintField;
    teAStOf: TSmallintField;
    teAVReal1: TFloatField;
    teAVReal2: TFloatField;
    teAVReal3: TFloatField;
    teAName: TStringField;
    teAEdIzm: TStringField;
    ViewAkciya1Code: TcxGridDBColumn;
    ViewAkciya1DateB: TcxGridDBColumn;
    ViewAkciya1DateE: TcxGridDBColumn;
    ViewAkciya1AType: TcxGridDBColumn;
    ViewAkciya1StOn: TcxGridDBColumn;
    ViewAkciya1StOf: TcxGridDBColumn;
    ViewAkciya1VReal1: TcxGridDBColumn;
    ViewAkciya1VReal2: TcxGridDBColumn;
    ViewAkciya1VReal3: TcxGridDBColumn;
    ViewAkciya1Name: TcxGridDBColumn;
    ViewAkciya1RecId: TcxGridDBColumn;
    teARemn1: TFloatField;
    teARemn2: TFloatField;
    ViewAkciya1Remn1: TcxGridDBColumn;
    ViewAkciya1Remn2: TcxGridDBColumn;
    teAQIn: TFloatField;
    teAQReal: TFloatField;
    teAAType: TStringField;
    ViewAkciya1QIn: TcxGridDBColumn;
    ViewAkciya1QReal: TcxGridDBColumn;
    ViewAkciyaVal2After: TcxGridDBColumn;
    ViewAkciyaDocCode: TcxGridDBColumn;
    ViewAkciyaDateTime: TcxGridDBColumn;
    ViewAkciyaNAMEBRAND: TcxGridDBColumn;
    acTestA: TAction;
    SpeedItem6: TSpeedItem;
    ViewAkciyaDStop: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewAkciyaSelectionChanged(Sender: TcxCustomGridTableView);
    procedure ViewAkciya1SelectionChanged(Sender: TcxCustomGridTableView);
    procedure ViewAkciya1DblClick(Sender: TObject);
    procedure ViewAkciyaDblClick(Sender: TObject);
    procedure acTestAExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAkciya: TfmAkciya;

implementation

uses Un1, MDB, MT, Move, ResTest;

{$R *.dfm}

procedure TfmAkciya.FormCreate(Sender: TObject);
begin
  GridAkciya.Align:=AlClient;
  FPAkciya.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FPAkciya.IniSection:='Akciya';
  FPAkciya.Active:=True;
//  ViewObVed.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAkciya.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewObVed.StoreToIniFile(CurDir+GridIni,False);
  dmMC.quAkciya.Active:=False;
end;

procedure TfmAkciya.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmAkciya.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAkciya.SpeedItem3Click(Sender: TObject);
begin
  if LevelAkciya.Visible then prNExportExel5(ViewAkciya)
  else prNExportExel5(ViewAkciya1);
end;

procedure TfmAkciya.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepAkciya') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmAkciya.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmAkciya.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmAkciya.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if LevelAkciya.Visible then
    begin
      if ViewAkciya.Controller.SelectedRecordCount=1 then
      begin
        fmMove.ViewM.BeginUpdate;
        fmMove.GridM.Tag:=quAkciyaCode.AsInteger;
        prFillMove(quAkciyaCode.AsInteger);

        fmMove.ViewM.EndUpdate;

        fmMove.Caption:=quAkciyaName.AsString+'('+quAkciyaCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

        fmMove.LevelP.Visible:=False;
        fmMove.LevelM.Visible:=True;

        fmMove.Show;
      end;
    end;
    if LevelAkciya1.Visible then
    begin
      if ViewAkciya1.Controller.SelectedRecordCount=1 then
      begin
        fmMove.ViewM.BeginUpdate;
        fmMove.GridM.Tag:=teACode.AsInteger;
        prFillMove(teACode.AsInteger);

        fmMove.ViewM.EndUpdate;

        fmMove.Caption:=teAName.AsString+'('+teACode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

        fmMove.LevelP.Visible:=False;
        fmMove.LevelM.Visible:=True;

        fmMove.Show;
      end;
    end;
  end;
end;

procedure TfmAkciya.ViewAkciyaSelectionChanged(Sender: TcxCustomGridTableView);
begin
  if ViewAkciya.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=quAkciyaCode.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmAkciya.ViewAkciya1SelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewAkciya1.Controller.SelectedRecordCount=1 then
  begin
    with dmMc do
    begin
      Vi1.BeginUpdate;
      quPost4.Active:=False;
      quPost4.ParamByName('IDCARD').Value:=teACode.AsInteger;
      quPost4.Active:=True;
      Vi1.EndUpdate;
    end;
  end;
end;

procedure TfmAkciya.ViewAkciya1DblClick(Sender: TObject);
begin
  acMoveRepOb.Execute;
end;

procedure TfmAkciya.ViewAkciyaDblClick(Sender: TObject);
begin
  acMoveRepOb.Execute;
end;

procedure TfmAkciya.acTestAExecute(Sender: TObject);
begin
  fmRes.Memo1.Clear;
  fmRes.Show;
end;

end.
