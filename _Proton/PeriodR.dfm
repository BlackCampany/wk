object fmPeriodR: TfmPeriodR
  Left = 404
  Top = 130
  BorderStyle = bsDialog
  Caption = #1055#1077#1088#1080#1086#1076
  ClientHeight = 395
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 347
    Width = 404
    Height = 48
    Align = alBottom
    BevelInner = bvLowered
    Color = 16744576
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 68
      Top = 12
      Width = 89
      Height = 25
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      Colors.Default = 16753828
      Colors.Normal = clWhite
      Colors.Hot = 16758783
      Colors.Pressed = clBlue
      Colors.Disabled = clSilver
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C10420000000000000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7F10421042FF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7F000210421863FF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7F00020002000210421863FF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7F00020002FF7F000200021042FF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000210421042FF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7F000210421863FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7F00021042FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0002FF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C10421042104210421042104210421042104210421042
        10421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton10: TcxButton
      Left = 232
      Top = 12
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton10Click
      Colors.Default = 16753828
      Colors.Normal = clWhite
      Colors.Hot = 16758783
      Colors.Pressed = clBlue
      Colors.Disabled = clSilver
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLabel3: TcxLabel
    Left = 20
    Top = 20
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Transparent = True
  end
  object cxCheckBox1: TcxCheckBox
    Left = 299
    Top = 18
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox1PropertiesChange
    Style.BorderStyle = ebsOffice11
    TabOrder = 2
    Transparent = True
    Width = 65
  end
  object cxLabel5: TcxLabel
    Left = 20
    Top = 92
    Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
    Transparent = True
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 92
    Top = 90
    Enabled = False
    Properties.DropDownRows = 12
    Properties.DropDownWidth = 300
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        FieldName = 'NameSG'
      end>
    Properties.ListSource = dsquSG
    Style.BorderStyle = ebsOffice11
    TabOrder = 4
    Width = 177
  end
  object cxButton3: TcxButton
    Left = 269
    Top = 91
    Width = 19
    Height = 19
    Caption = '...'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    Visible = False
    Colors.Default = clWhite
  end
  object cxCheckBox3: TcxCheckBox
    Left = 299
    Top = 90
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox3PropertiesChange
    State = cbsChecked
    Style.BorderStyle = ebsOffice11
    TabOrder = 6
    Transparent = True
    Width = 65
  end
  object cxButtonEdit1: TcxButtonEdit
    Tag = 1
    Left = 92
    Top = 20
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Properties.OnChange = cxButtonEdit1PropertiesChange
    Style.BorderStyle = ebsOffice11
    TabOrder = 7
    Text = 'cxButtonEdit1'
    Width = 201
  end
  object cxLabel6: TcxLabel
    Left = 20
    Top = 132
    Caption = #1041#1088#1101#1085#1076
    Transparent = True
  end
  object cxLookupComboBox4: TcxLookupComboBox
    Left = 92
    Top = 130
    Enabled = False
    Properties.DropDownRows = 12
    Properties.DropDownWidth = 300
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEBRAND'
      end>
    Properties.ListSource = dsquBr
    Style.BorderStyle = ebsOffice11
    TabOrder = 9
    Width = 177
  end
  object cxCheckBox4: TcxCheckBox
    Left = 299
    Top = 130
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox4PropertiesChange
    State = cbsChecked
    Style.BorderStyle = ebsOffice11
    TabOrder = 10
    Transparent = True
    Width = 65
  end
  object cxLabel7: TcxLabel
    Left = 20
    Top = 172
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
    Transparent = True
  end
  object cxLookupComboBox5: TcxLookupComboBox
    Left = 92
    Top = 170
    Enabled = False
    Properties.DropDownRows = 12
    Properties.DropDownWidth = 300
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        FieldName = 'SID'
      end
      item
        FieldName = 'COMMENT'
      end>
    Properties.ListSource = dsquCat
    Style.BorderStyle = ebsOffice11
    TabOrder = 12
    Width = 177
  end
  object cxCheckBox5: TcxCheckBox
    Left = 299
    Top = 170
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox5PropertiesChange
    State = cbsChecked
    Style.BorderStyle = ebsOffice11
    TabOrder = 13
    Transparent = True
    Width = 65
  end
  object cxLabel8: TcxLabel
    Left = 20
    Top = 52
    Caption = #1043#1088#1091#1087#1087#1072
    Transparent = True
  end
  object cxLookupComboBox6: TcxLookupComboBox
    Left = 92
    Top = 50
    Properties.DropDownRows = 12
    Properties.DropDownWidth = 300
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Name'
      end>
    Properties.ListSource = dsquGr
    Properties.OnChange = cxLookupComboBox6PropertiesChange
    Style.BorderStyle = ebsOffice11
    TabOrder = 15
    Width = 177
  end
  object cxCheckBox6: TcxCheckBox
    Left = 299
    Top = 50
    Caption = #1087#1086' '#1074#1089#1077#1084
    Properties.OnChange = cxCheckBox6PropertiesChange
    Style.BorderStyle = ebsOffice11
    TabOrder = 16
    Transparent = True
    Width = 65
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 24
    Top = 220
    Caption = #1044#1086#1087'. '#1087#1072#1088#1072#1084#1077#1090#1088#1099
    ParentColor = False
    Properties.Items = <
      item
        Caption = #1042#1089#1077
        Value = 0
      end
      item
        Caption = #1058#1086#1087#1099
        Value = 1
      end
      item
        Caption = #1053#1086#1074#1080#1085#1082#1080
        Value = 2
      end
      item
        Caption = #1058#1086#1087#1099' '#1080' '#1085#1086#1074#1080#1085#1082#1080
        Value = 3
      end>
    ItemIndex = 0
    Style.BorderStyle = ebsOffice11
    Style.Color = clWhite
    TabOrder = 17
    Height = 109
    Width = 181
  end
  object cxRadioGroup2: TcxRadioGroup
    Left = 216
    Top = 220
    Caption = #1062#1077#1085#1099' '#1086#1090#1095#1077#1090#1072
    ParentColor = False
    Properties.Items = <
      item
        Caption = #1055#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1087#1088#1080#1093#1086#1076#1072
        Value = 0
      end
      item
        Caption = #1055#1088#1086#1076#1072#1078#1085#1099#1077' ('#1080#1079' '#1082#1072#1088#1090#1086#1095#1082#1080')'
        Value = 1
      end>
    ItemIndex = 0
    Style.Color = clWhite
    TabOrder = 18
    Visible = False
    Height = 77
    Width = 169
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16763594
    BkColor.EndColor = 16318464
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 168
    Top = 20
  end
  object quDeps: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "Depart"'
      'Order by Name'
      '')
    Params = <>
    Left = 136
    Top = 112
    object quDepsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquDeps: TDataSource
    DataSet = quDeps
    Left = 184
    Top = 112
  end
  object quSG: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select sg.GoodsGroupID,sg.ID,sg.Name as NameSG'
      'from "SubGroup" sg'
      ''
      'where sg.GoodsGroupID=:IDGR'
      'and sg.ID>=10000 and sg.ID<20000'
      'order by sg.Name')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDGR'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 316
    Top = 184
    object quSGGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quSGID: TSmallintField
      FieldName = 'ID'
    end
    object quSGNameSG: TStringField
      FieldName = 'NameSG'
      Size = 30
    end
  end
  object dsquSG: TDataSource
    DataSet = quSG
    Left = 360
    Top = 184
  end
  object quBr: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_BRANDS"'
      'order by NAMEBRAND')
    Params = <>
    Left = 56
    Top = 144
    object quBrID: TIntegerField
      FieldName = 'ID'
    end
    object quBrNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
  end
  object dsquBr: TDataSource
    DataSet = quBr
    Left = 96
    Top = 144
  end
  object quCat: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_CATEG"')
    Params = <>
    Left = 152
    Top = 168
    object quCatID: TIntegerField
      FieldName = 'ID'
    end
    object quCatSID: TStringField
      FieldName = 'SID'
      Size = 2
    end
    object quCatCOMMENT: TStringField
      FieldName = 'COMMENT'
    end
  end
  object dsquCat: TDataSource
    DataSet = quCat
    Left = 196
    Top = 168
  end
  object quGr: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select sg.GoodsGroupID,sg.ID,sg.Name'
      'from "SubGroup" sg'
      'where sg.GoodsGroupID=0'
      'and sg.ID<10000'
      'order by sg.Name')
    Params = <>
    Left = 316
    Top = 136
    object quGrGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quGrID: TSmallintField
      FieldName = 'ID'
    end
    object quGrName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquGr: TDataSource
    DataSet = quGr
    Left = 360
    Top = 136
  end
end
