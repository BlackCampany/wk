unit PostGood;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmP = class(TForm)
    FormPlacement1: TFormPlacement;
    GP: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    amP: TActionManager;
    acExit: TAction;
    LP: TcxGridLevel;
    VP: TcxGridDBTableView;
    VPDepart: TcxGridDBColumn;
    VPName: TcxGridDBColumn;
    VPDateInvoice: TcxGridDBColumn;
    VPCodeTovar: TcxGridDBColumn;
    VPCenaTovar: TcxGridDBColumn;
    VPNewCenaTovar: TcxGridDBColumn;
    VPNameCli: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure VPDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmP: TfmP;

implementation

uses Un1,dmPS, AddDoc8, Period1, MDB;

{$R *.dfm}

procedure TfmP.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GP.Align:=AlClient;
  VP.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmP.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmP.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmP.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=fmPeriod1.cxDateEdit1.Date;
    CommonSet.DateEnd:=fmPeriod1.cxDateEdit2.Date;
    with dmP do
    begin
        fmP.VP.BeginUpdate;
        quPost2.Active:=False;
        quPost2.ParamByName('IDCARD').Value:=fmAddDoc8.taSpecVozCode.AsInteger;
        quPost2.ParamByName('DDATEB').Value:=CommonSet.DateBeg;
        quPost2.ParamByName('DDATEE').Value:=CommonSet.DateEnd;
        quPost2.Active:=True;
        quPost2.First;
        fmP.VP.EndUpdate;
        fmP.Caption:=quPost2NameCode.AsString+'��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmP.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(VP)

end;

procedure TfmP.acExitExecute(Sender: TObject);
begin
  Close;
  bDrVoz:=false;
end;

procedure TfmP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VP.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmP.VPDblClick(Sender: TObject);
  var cenaformat:real;
      icli, fl:integer;
begin

  with dmP do
  with fmAddDoc8 do
  begin
      icli:=0;
      fl:=0;
      if bDrVoz=false then //���� ����� ������� �� ��������
      begin
        if quPost2.RecordCount>0 then
        begin
          if (quPostVozIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
          begin
            quTypeCli.Active:=false;
            quTypeCli.ParamByName('ICLI').AsInteger:=quPost2IDALTCLI.AsInteger;
            quTypeCli.Active:=True;
            if quTypeCli.RecordCount>0 then         //���� �� ���������� ���� ������ (����������, ������������)
            begin
              if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPost2IDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
              else icli:=0;                          //���� ��� �� ��������
            end
            else icli:=0;                            //��� ������, ����� ���� ��������
          end
          else icli:=quPost2idcli.AsInteger;        //����������� ����. ����������

          quDateVoz.Active:=False;
          quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
          quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
          quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
          quDateVoz.ParamByName('IDCli').AsInteger:=icli;//quPostVozidcli.AsInteger;
          quDateVoz.Active:=True;

                                                                            //��������� ���� �� ���� ��������
          if quDateVoz.RecordCount<1 then
          begin
            if MessageDlg('� ���������� ���������� ��� ������� ���������, ��������� �� ������� ����?',mtConfirmation,[mbYes, mbNo], 0)=mrYes then
            predztoday(0,0,icli)  else exit;
          end else predzgrafvoz(0,0,icli);
        end;
      end
      else
      begin                 //���� ����� ������� ��  alt+f5
        if quPost2.RecordCount>0 then       //���� ���� ���������� �� �������� ��������
        begin
          if (quPostVozIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
          begin                                   //���� ���� ��������� �����������
            fl:=1;
            quTypeCli.Active:=false;
            quTypeCli.ParamByName('ICLI').AsInteger:=quPost2IDALTCLI.AsInteger;
            quTypeCli.Active:=True;
            if quTypeCli.RecordCount>0 then         //���� �� ���������� ���� ������ (����������, ������������)
            begin
              if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPostVozIDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
              else icli:=0;                          //���� ��� �� ��������
            end
            else icli:=0;                            //��� ������, ����� ���� ��������
          end
          else
          begin
            fl:=0;                               //���� ��� ��������� ������������
            icli:=quPost2idcli.AsInteger;        //����������� ����. ����������
          end;

          ViewDoc8.BeginUpdate;
          taSpecVoz.Edit;

          if fl=0 then
          begin
            taSpecVozidcli.AsInteger:=icli;
            taSpecVozNamecli.AsString:=quPost2NamePost.AsString;
          end
          else
          begin
            quNamePost.Active:=false;
            quNamePost.ParamByName('icli').AsInteger:=icli;
            quNamePost.Active:=True;

            if quNamePost.RecordCount>0 then
            begin
              taSpecVozidcli.AsInteger:=icli;
              taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
            end;
          end;

          taSpecVozDepart.AsInteger:=quPost2Depart.AsInteger;
          taSpecVozDepartName.AsString:=quPost2NameDep.AsString;

          if quPost2kol.AsFloat>0 then
          cenaformat:=((quPost2SumPost.AsFloat-quPost2NDSSum.AsFloat)/quPost2kol.AsFloat)
          else cenaformat:=0;
          taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));

          cenaformat:=quPost2PriceIn.AsFloat;
          taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
          cenaformat:=quPost2Rprice.AsFloat;
          taSpecVozRPrice.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
          taSpecVoz.Post;
          ViewDoc8.EndUpdate;

        end
        else exit;

        ViewDoc8.Controller.FocusRecord(ViewDoc8.DataController.FocusedRowIndex,True);
        griddoc8.SetFocus;
        ViewDoc8Quant.Focused:=true;
        viewdoc8quant.Selected:=true;
        ViewDoc8.Controller.SelectAllColumns;
        ViewDoc8Quant.Options.Editing:=True;
        ViewDoc8Reason.Options.Editing:=true;
        fmAddDoc8.Show;
      end;
      fmP.close;
      bDrVoz:=false
  end;
end;

end.
