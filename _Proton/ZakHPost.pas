unit ZakHPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCalendar, cxImageComboBox;

type
  TfmZakHPost = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton10: TcxButton;
    ViewZHPost: TcxGridDBTableView;
    LevelZHPost: TcxGridLevel;
    GridZHPost: TcxGrid;
    ViewZHPostDOCNUM: TcxGridDBColumn;
    ViewZHPostDEPART: TcxGridDBColumn;
    ViewZHPostIACTIVE: TcxGridDBColumn;
    ViewZHPostCLIQUANTN: TcxGridDBColumn;
    ViewZHPostCLISUMIN0N: TcxGridDBColumn;
    ViewZHPostCLISUMINN: TcxGridDBColumn;
    ViewZHPostIDATENACL: TcxGridDBColumn;
    ViewZHPostSNUMNACL: TcxGridDBColumn;
    ViewZHPostIDATESCHF: TcxGridDBColumn;
    ViewZHPostSNUMSCHF: TcxGridDBColumn;
    ViewZHPostName: TcxGridDBColumn;
    ViewZHPostID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ViewZHPostDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmZakHPost: TfmZakHPost;

implementation

uses MDB, Un1;

{$R *.dfm}

procedure TfmZakHPost.FormCreate(Sender: TObject);
begin
  GridZHPost.Align:=AlClient;
end;

procedure TfmZakHPost.ViewZHPostDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
