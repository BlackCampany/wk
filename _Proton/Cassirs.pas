unit Cassirs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxmdaset;

type
  TfmCassirs = class(TForm)
    Panel1: TPanel;
    Memo1: TcxDBMemo;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    ViewCassir: TcxGridDBTableView;
    LeCassir: TcxGridLevel;
    GrCassir: TcxGrid;
    teCassirs: TdxMemData;
    teCassirsId: TIntegerField;
    teCassirsName: TStringField;
    teCassirsRSum: TFloatField;
    teCassirsCountPos: TIntegerField;
    dsteCassirs: TDataSource;
    ViewCassirRecId: TcxGridDBColumn;
    ViewCassirId: TcxGridDBColumn;
    ViewCassirName: TcxGridDBColumn;
    ViewCassirRSum: TcxGridDBColumn;
    ViewCassirCountPos: TcxGridDBColumn;
    ViewCassirDepart: TcxGridDBColumn;
    teCassirsDepart: TStringField;
    teCassirsiDep: TIntegerField;
    teCassirsCashNum: TIntegerField;
    ViewCassirCashNum: TcxGridDBColumn;
    teCassirsdDate: TDateTimeField;
    teCassirsiDate: TIntegerField;
    ViewCassirdDate: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCassirs: TfmCassirs;
  bCalc:Boolean = False;

implementation

uses MDB, Un1;

{$R *.dfm}

procedure TfmCassirs.FormCreate(Sender: TObject);
begin
  GrCassir.Align:=AlClient;
  Memo1.Clear;
end;

procedure TfmCassirs.cxButton3Click(Sender: TObject);
begin
  if bCalc=False then close
  else showmessage('����� ��������� �������.');
end;

procedure TfmCassirs.cxButton1Click(Sender: TObject);
begin
  prNExportExel5(ViewCassir);
end;

end.
