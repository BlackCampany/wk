unit DStop;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmDStop = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewDStop: TcxGridDBTableView;
    LevelDStop: TcxGridLevel;
    GrDStop: TcxGrid;
    ViewDStopCode: TcxGridDBColumn;
    ViewDStopPercentDisc: TcxGridDBColumn;
    ViewDStopPercent: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    amDStop: TActionManager;
    acLoadSel: TAction;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acLoadSelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDStop: TfmDStop;

implementation

uses MT, Un1, MDB, Status, MFB, u2fdk;

{$R *.dfm}

procedure TfmDStop.FormCreate(Sender: TObject);
begin
  GrDStop.Align:=AlClient;
end;

procedure TfmDStop.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDStop.cxButton1Click(Sender: TObject);
begin
  prNExportExel5(ViewDStop);
end;

procedure TfmDStop.acLoadSelExecute(Sender: TObject);
Var i,j,iNum:INteger;
    rProc,rPr:Real;
    Rec:TcxCustomGridRecord;
    sName,sMessur:String;
begin
  //��������� ����������
  with dmMc do
  with dmMt do
  begin
    try
      if ViewDStop.Controller.SelectedRecordCount=0 then exit;
      if fmSt.cxButton1.Enabled=False then exit;

      if taCards.Active=False then taCards.Active:=True;

      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('���� �������� ����..');

      if dmFB.Cash.Connected then
      begin
        fmSt.Memo1.Lines.Add('  FB ����� ����.');

        fmSt.Memo1.Lines.Add('  � �������� '+its(ViewDStop.Controller.SelectedRecordCount)+' �����.');

        for i:=0 to ViewDStop.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDStop.Controller.SelectedRecords[i];

          iNum:=0; rProc:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDStop.Columns[j].Name='ViewDStopCode' then iNum:=Rec.Values[j];
            if ViewDStop.Columns[j].Name='ViewDStopPercent' then rProc:=Rec.Values[j];
          end;

          if iNum>0 then
          begin
            if taCards.FindKey([iNum]) then
            begin
              sName:=OemToAnsiConvert(taCardsFullName.AsString);
              if rProc>99 then sName:='-'+sName;

              rPr:=rv(taCardsCena.AsFloat);

              prAddCashLoad(iNum,1,rPr,0);

              if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

              if  taCardsStatus.AsInteger>=100 then prAddPosFB(1,taCardsID.AsInteger,taCardsV11.AsInteger,0,taCardsGoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),0,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(rPr))
              else
              begin
                prAddPosFB(1,iNum,taCardsV11.AsInteger,0,taCardsGoodsGroupID.AsInteger,0,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(rPr));
                prAddPosFB(18,iNum,0,0,0,0,1,'','','','','',0,rProc,0,0);
              end;
            end;
          end;
        end;

        fmSt.Memo1.Lines.Add('�������� ���������.');
      end else fmSt.Memo1.Lines.Add('  FB ����� ���. ���������� � ���������������.');
    finally
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

end.
