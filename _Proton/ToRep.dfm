object fmTORep: TfmTORep
  Left = 518
  Top = 310
  Width = 753
  Height = 494
  Caption = #1058#1086#1074#1072#1088#1085#1099#1077' '#1086#1090#1095#1077#1090#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridTO: TcxGrid
    Left = 4
    Top = 52
    Width = 729
    Height = 389
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewTO: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquReports
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'OutTovar'
          Column = ViewTOOutTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'PereoTovarSumma'
          Column = ViewTOPereoTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'PrihodTovar'
          Column = ViewTOPrihodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'PrihodTovarSumma'
          Column = ViewTOPrihodTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RashodTovar'
          Column = ViewTORashodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RashodTovarSumma'
          Column = ViewTORashodTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RealTovar'
          Column = ViewTORealTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SelfPrihodTovar'
          Column = ViewTOSelfPrihodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SelfRashodTovar'
          Column = ViewTOSelfRashodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SkidkaTovar'
          Column = ViewTOSkidkaTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'OldOstatokTovar'
          Column = ViewTOOldOstatokTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'NewOstatokTovar'
          Column = ViewTONewOstatokTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SenderSumma'
          Column = ViewTOSenderSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SenderSkidka'
          Column = ViewTOSenderSkidka
        end
        item
          Format = '0.00'
          Position = spFooter
          FieldName = 'xRezerv'
          Column = ViewTOxRezerv
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'OutTovar'
          Column = ViewTOOutTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'PereoTovarSumma'
          Column = ViewTOPereoTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'PrihodTovar'
          Column = ViewTOPrihodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'PrihodTovarSumma'
          Column = ViewTOPrihodTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RashodTovar'
          Column = ViewTORashodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RashodTovarSumma'
          Column = ViewTORashodTovarSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RealTovar'
          Column = ViewTORealTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SelfPrihodTovar'
          Column = ViewTOSelfPrihodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SelfRashodTovar'
          Column = ViewTOSelfRashodTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SkidkaTovar'
          Column = ViewTOSkidkaTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'OldOstatokTovar'
          Column = ViewTOOldOstatokTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'NewOstatokTovar'
          Column = ViewTONewOstatokTovar
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SenderSumma'
          Column = ViewTOSenderSumma
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SenderSkidka'
          Column = ViewTOSenderSkidka
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'xRezerv'
          Column = ViewTOxRezerv
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewTOOtdel: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'Otdel'
      end
      object ViewTOName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'Name'
        Width = 152
      end
      object ViewTOxDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'xDate'
      end
      object ViewTOOldOstatokTovar: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095#1072#1083#1086
        DataBinding.FieldName = 'OldOstatokTovar'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewTOPrihodTovar: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'PrihodTovar'
      end
      object ViewTOSelfPrihodTovar: TcxGridDBColumn
        Caption = #1042#1085'. '#1087#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'SelfPrihodTovar'
      end
      object ViewTOPrihodTovarSumma: TcxGridDBColumn
        Caption = #1042#1089#1077#1075#1086' '#1087#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'PrihodTovarSumma'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewTORashodTovar: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'RashodTovar'
      end
      object ViewTOSelfRashodTovar: TcxGridDBColumn
        Caption = #1042#1085'.'#1088#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'SelfRashodTovar'
      end
      object ViewTORealTovar: TcxGridDBColumn
        Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
        DataBinding.FieldName = 'RealTovar'
        Width = 94
      end
      object ViewTOxRezerv: TcxGridDBColumn
        Caption = #1050#1072#1089#1089#1072' '#1074' '#1094#1077#1085#1072#1093' '#1079#1072#1082#1091#1087'.'
        DataBinding.FieldName = 'xRezerv'
        Width = 101
      end
      object ViewTOSkidkaTovar: TcxGridDBColumn
        Caption = #1057#1082#1080#1076#1082#1072
        DataBinding.FieldName = 'SkidkaTovar'
      end
      object ViewTOSenderSumma: TcxGridDBColumn
        Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1073'/'#1085
        DataBinding.FieldName = 'SenderSumma'
      end
      object ViewTOSenderSkidka: TcxGridDBColumn
        Caption = #1057#1082#1080#1076#1082#1072' '#1073'/'#1085
        DataBinding.FieldName = 'SenderSkidka'
      end
      object ViewTOOutTovar: TcxGridDBColumn
        Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
        DataBinding.FieldName = 'OutTovar'
      end
      object ViewTORashodTovarSumma: TcxGridDBColumn
        Caption = #1042#1089#1077#1075#1086' '#1088#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'RashodTovarSumma'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewTOPereoTovarSumma: TcxGridDBColumn
        Caption = #1055#1077#1088#1077#1086#1094#1077#1085#1082#1072
        DataBinding.FieldName = 'PereoTovarSumma'
      end
      object ViewTONewOstatokTovar: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094
        DataBinding.FieldName = 'NewOstatokTovar'
        Styles.Content = dmMC.cxStyle1
        Width = 105
      end
      object ViewTOOldOstatokTara: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1086' '#1090#1072#1088#1077' '#1085#1072' '#1085#1072#1095#1072#1083#1086
        DataBinding.FieldName = 'OldOstatokTara'
        Width = 93
      end
      object ViewTOPrihodTara: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1090#1072#1088#1099
        DataBinding.FieldName = 'PrihodTara'
      end
      object ViewTOSelfPrihodTara: TcxGridDBColumn
        Caption = #1042#1085'. '#1087#1088#1080#1093#1086#1076' '#1090#1072#1088#1099
        DataBinding.FieldName = 'SelfPrihodTara'
      end
      object ViewTOPrihodTaraSumma: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1090#1072#1088#1099' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'PrihodTaraSumma'
      end
      object ViewTORashodTara: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1090#1072#1088#1099
        DataBinding.FieldName = 'RashodTara'
      end
      object ViewTOSelfRashodTara: TcxGridDBColumn
        Caption = #1042#1085'. '#1088#1072#1089#1093#1086#1076' '#1090#1072#1088#1099
        DataBinding.FieldName = 'SelfRashodTara'
      end
      object ViewTOOutTara: TcxGridDBColumn
        Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1090#1072#1088#1077
        DataBinding.FieldName = 'OutTara'
      end
      object ViewTORashodTaraSumma: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1087#1086' '#1090#1072#1088#1077' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'RashodTaraSumma'
        Width = 91
      end
      object ViewTONewOstatokTara: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1086' '#1090#1072#1088#1077' '#1085#1072' '#1082#1086#1085#1077#1094
        DataBinding.FieldName = 'NewOstatokTara'
      end
      object ViewTOOprihod: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'Oprihod'
      end
      object ViewTOV03: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'V03'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1050'-'#1088
            Value = 1
          end>
        Width = 49
      end
    end
    object ViewSSR: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteSSR
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUM'
          Column = ViewSSRRSUM
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUMDIF'
          Column = ViewSSRRSUMDIF
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUMIN'
          Column = ViewSSRRSUMIN
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUMINPP'
          Column = ViewSSRRSUMINPP
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      object ViewSSRiDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'iDate'
      end
      object ViewSSRdDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1058#1054
        DataBinding.FieldName = 'dDate'
      end
      object ViewSSRiDep: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'iDep'
      end
      object ViewSSRNameDep: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'NameDep'
      end
      object ViewSSRRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1080' '#1087#1086' '#1058#1054
        DataBinding.FieldName = 'RSUM'
      end
      object ViewSSRRSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1057#1057' '#1087#1088#1086#1076#1072#1078#1080' '#1087#1086' '#1058#1054
        DataBinding.FieldName = 'RSUMIN'
      end
      object ViewSSRRSUMINPP: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1074' '#1087#1072#1088#1090#1080#1086#1085#1085#1086#1084' '#1091#1095#1077#1090#1077
        DataBinding.FieldName = 'RSUMINPP'
      end
      object ViewSSRRSUMDIF: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'RSUMDIF'
      end
    end
    object LevelTO: TcxGridLevel
      GridView = ViewTO
    end
    object LevelSSR: TcxGridLevel
      GridView = ViewSSR
      Visible = False
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 745
    Height = 46
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 15135710
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 454
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1080#1086#1076'|'
      Spacing = 1
      Left = 14
      Top = 4
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel|'
      Spacing = 1
      Left = 74
      Top = 4
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acGenTO
      BtnCaption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1058#1054
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1058#1054
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF29200829200829
        2008FFFFFFFFFFFF0000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF292008292008EFE3CEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF292008
        2920089C694AEFE3CEC68E4AC68E4A292008FFFFFFFFFFFF0000FFFFFF101410
        084D52107184000000FFFFFFFFFFFF292008292008CEC3B5DED7C69C694AC68E
        4AEFE3CEEFE3CE292008FFFFFFFFFFFF0000FFFFFF84828410DFEF18C7DE29B2
        D6525552211808735139BDB6ADA57539B586429C694AEFE7D6C68E4AC68E4A29
        2008FFFFFFFFFFFF0000FFFFFFB5B2B508EFF710D7EF21BEDE000000BDBEBD73
        5139946D39CEC7BDE7DBCE9C694AC68E4AF7E7D6EFE3D6292008FFFFFFFFFFFF
        0000FFFFFFFFFFFF84828408E7F718CFE721B6D6525552735139BDBAB5A57539
        B586429C694AF7E7DE9C694A9C694A292008FFFFFFFFFFFF0000FFFFFFFFFFFF
        B5B2B500F7FF10DFEF18C7DE0000007351399C6D39CECBC6E7DFD69C694A9C69
        4AC67121C67121292008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF84828408EF
        F710D7EF21BEDE525552C6C3BD8C5D4294654ACE7529CE7521CE7121C6712129
        2008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFB5B2B500FBFF08E7F718CFE700
        00008C5D42C67529CE7929CE7929CE7929EFDFCEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF29200884828400F7FF000000000000000000D68231
        D67D31EFDFC6EFDFCEBD7152BD7152FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF292008B5B2B5EFEFEFB5B2B5848284000000E7D7BDEFDBC6BD7152BD71
        52FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF92
        42848284CECFCE949294848284525552BD7152FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF9242B5B2B5EFEFEFB5
        B2B5848284000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF846129D6BE94BD7152848284848284848284FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFBD7152BD7152FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Hint = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1058#1054
      Spacing = 1
      Left = 304
      Top = 4
      Visible = True
      OnClick = acGenTOExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acPrint
      Caption = #1055#1077#1095#1072#1090#1100' '#1058#1054
      Glyph.Data = {
        86070000424D86070000000000003600000028000000180000001A0000000100
        1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
        B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
        A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
        85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
        C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
        A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
        F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
        C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
        FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
        ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
        C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
        FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
        D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
        87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
        E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
        B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
        F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
        C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
        C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
        E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
        FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
        E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
        B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
        D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
        B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
        D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
        B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
        FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
        F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
        C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
        FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
        F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
        D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1095#1072#1090#1100' '#1058#1054
      Spacing = 1
      Left = 154
      Top = 4
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 120
    Top = 108
  end
  object amTO: TActionManager
    Left = 416
    Top = 136
    StyleName = 'XP Style'
    object acGenTO: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1058#1054
      OnExecute = acGenTOExecute
    end
    object acPrint: TAction
      OnExecute = acPrintExecute
    end
    object acCorrPartOut: TAction
      Caption = #1050#1086#1088#1088#1077#1082#1094#1080#1103' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080' '#1074' '#1079#1072#1082'.'#1094#1077#1085#1072#1093
      OnExecute = acCorrPartOutExecute
    end
    object acSSR: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1057#1057' '#1087#1072#1088#1090#1080#1086#1085#1085#1086#1075#1086' '#1091#1095#1077#1090#1072
      ShortCut = 16467
      OnExecute = acSSRExecute
    end
    object acGenSv: TAction
      Caption = #1055#1077#1088#1077#1089#1086#1089#1090#1072#1074#1083#1077#1085#1080#1077' '#1089#1074#1086#1076#1085#1086#1075#1086' '#1086#1090#1095#1077#1090#1072
      ShortCut = 49237
      OnExecute = acGenSvExecute
    end
  end
  object RepTO: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 40
    Top = 192
    ReportForm = {19000000}
  end
  object frtaTOIn: TfrDBDataSet
    DataSet = taIn
    Left = 92
    Top = 192
  end
  object frtaTOOut: TfrDBDataSet
    DataSet = taOut
    Left = 148
    Top = 192
  end
  object quTTnInR: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInRCalcFields
    SQL.Strings = (
      'select  '
      'ttn."Depart",'
      'ttn."DateInvoice",'
      'ttn."Number",'
      'ttn."SCFNumber",'
      'ttn."IndexPost",'
      'ttn."CodePost",'
      'ttn."SummaTovarPost",'
      'ttn."ReturnTovar",'
      'ttn."SummaTovar",'
      'ttn."Nacenka",'
      'ttn."ReturnTara",'
      'ttn."SummaTara",'
      'ttn."AcStatus",'
      'ttn."ChekBuh",'
      'ttn."SCFDate",'
      'ttn."OrderNumber",'
      'ttn."ID",'
      '(ttn."OutNDSTovar"+ttn."NotNDSTovar") as "OutNDSTovar",'
      'ttn."NDS10",'
      'ttn."NDS20",'
      'ttn."OutNDS10",'
      'ttn."OutNDS20",'
      'v."Name" as NamePost,'
      'i."Name" as NameInd'
      ''
      'from "TTNIn" as ttn'
      'left outer join RVendor v on v.Vendor=ttn."CodePost"'
      'left outer join RIndividual i on i.Code=ttn."CodePost"'
      ''
      'where ttn.DateInvoice=:DATEB'
      'and ttn."Depart"=:DEPART'
      'and ttn."AcStatus"=3'
      ''
      ''
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftInteger
        Name = 'DEPART'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 240
    Top = 200
    object quTTnInRDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnInRDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnInRNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnInRSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quTTnInRIndexPost: TSmallintField
      FieldName = 'IndexPost'
    end
    object quTTnInRCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quTTnInRSummaTovarPost: TFloatField
      FieldName = 'SummaTovarPost'
      DisplayFormat = '0.00'
    end
    object quTTnInRReturnTovar: TFloatField
      FieldName = 'ReturnTovar'
      DisplayFormat = '0.00'
    end
    object quTTnInRSummaTovar: TFloatField
      FieldName = 'SummaTovar'
      DisplayFormat = '0.00'
    end
    object quTTnInRNacenka: TFloatField
      FieldName = 'Nacenka'
      DisplayFormat = '0.00'
    end
    object quTTnInRReturnTara: TFloatField
      FieldName = 'ReturnTara'
    end
    object quTTnInRSummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quTTnInRAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTnInRChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quTTnInRSCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quTTnInROrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object quTTnInRID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnInRNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quTTnInRNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quTTnInRNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 100
      Calculated = True
    end
    object quTTnInROutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object quTTnInRNDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quTTnInRNDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quTTnInROutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object quTTnInROutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
  end
  object quTTnOutR: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnOutRCalcFields
    SQL.Strings = (
      'select  '
      'ttn."Depart",'
      'ttn."DateInvoice",'
      'ttn."Number",'
      'ttn."SCFNumber",'
      'ttn."IndexPoluch",'
      'ttn."CodePoluch",'
      'ttn."SummaTovar",'
      'ttn."SummaTovarSpis",'
      'ttn."NacenkaTovar",'
      'ttn."SummaTara",'
      'ttn."SummaTaraSpis",'
      'ttn."NacenkaTara",'
      'ttn."AcStatus",'
      'ttn."ChekBuh",'
      'ttn."SCFDate",'
      'ttn."ID",'
      'ttn."NDS10",'
      'ttn."NDS20",'
      'ttn."OutNDSTovar",'
      'ttn."NotNDSTovar",'
      'v."Name" as NamePost,'
      'i."Name" as NameInd'
      ''
      'from "TTNOut" as ttn'
      'left outer join RVendor v on v.Vendor=ttn."CodePoluch"'
      'left outer join RIndividual i on i.Code=ttn."CodePoluch"'
      ''
      'where ttn.DateInvoice=:DATEB'
      'and ttn."Depart"=:DEPART'
      'and ttn."AcStatus"=3'
      'and ttn."ProvodType"<>3'
      'and ttn."ProvodType"<>4')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftInteger
        Name = 'DEPART'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 312
    Top = 200
    object quTTnOutRDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnOutRDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnOutRNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnOutRSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quTTnOutRIndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quTTnOutRCodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quTTnOutRSummaTovar: TFloatField
      FieldName = 'SummaTovar'
      DisplayFormat = '0.00'
    end
    object quTTnOutRSummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
      DisplayFormat = '0.00'
    end
    object quTTnOutRNacenkaTovar: TFloatField
      FieldName = 'NacenkaTovar'
      DisplayFormat = '0.0'
    end
    object quTTnOutRSummaTara: TFloatField
      FieldName = 'SummaTara'
      DisplayFormat = '0.00'
    end
    object quTTnOutRSummaTaraSpis: TFloatField
      FieldName = 'SummaTaraSpis'
      DisplayFormat = '0.00'
    end
    object quTTnOutRNacenkaTara: TFloatField
      FieldName = 'NacenkaTara'
      DisplayFormat = '0.0'
    end
    object quTTnOutRAcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTnOutRChekBuh: TWordField
      FieldName = 'ChekBuh'
    end
    object quTTnOutRSCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quTTnOutRID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnOutRNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
    object quTTnOutRNameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quTTnOutRNameCli: TStringField
      FieldKind = fkCalculated
      FieldName = 'NameCli'
      Size = 50
      Calculated = True
    end
    object quTTnOutRNDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quTTnOutRNDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quTTnOutROutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object quTTnOutRNotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
  end
  object taIn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 192
    Top = 128
    object taIniType: TSmallintField
      FieldName = 'iType'
    end
    object taInNameGr: TStringField
      FieldName = 'NameGr'
      Size = 30
    end
    object taInNameGr1: TStringField
      FieldName = 'NameGr1'
    end
    object taInNameCli: TStringField
      FieldName = 'NameCli'
      Size = 100
    end
    object taInNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 10
    end
    object taInSumIn: TFloatField
      FieldName = 'SumIn'
    end
    object taInSumOut: TFloatField
      FieldName = 'SumOut'
    end
    object taInSumInT: TFloatField
      FieldName = 'SumInT'
    end
    object taInOutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object taInOutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object taInOutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object taInNds10: TFloatField
      FieldName = 'Nds10'
    end
    object taInNds20: TFloatField
      FieldName = 'Nds20'
    end
  end
  object taOut: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 244
    Top = 128
    object taOutiType: TSmallintField
      FieldName = 'iType'
    end
    object taOutNameGr: TStringField
      FieldName = 'NameGr'
      Size = 30
    end
    object taOutNameGr1: TStringField
      FieldName = 'NameGr1'
    end
    object taOutNameCli: TStringField
      FieldName = 'NameCli'
      Size = 100
    end
    object taOutNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 10
    end
    object taOutSumIn: TFloatField
      FieldName = 'SumIn'
    end
    object taOutSumOut: TFloatField
      FieldName = 'SumOut'
    end
    object taOutSumOutT: TFloatField
      FieldName = 'SumOutT'
    end
    object taOutSumOutOutNDS: TFloatField
      FieldName = 'SumOutOutNDS'
    end
    object taOutSumNDS10: TFloatField
      FieldName = 'SumNDS10'
    end
    object taOutSumNDS20: TFloatField
      FieldName = 'SumNDS20'
    end
  end
  object quTTnSelfOut: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInRCalcFields
    SQL.Strings = (
      'select '
      'ttns.Depart,'
      'ttns.DateInvoice,'
      'ttns.Number,'
      'ttns.FlowDepart,'
      'ttns.SummaTovarSpis,'
      'ttns.SummaTovarMove,'
      'ttns.SummaTovarNew,'
      'ttns.SummaTara,'
      'ttns.Oprihod,'
      'ttns.AcStatusP,'
      'ttns.ChekBuhP,'
      'ttns.AcStatusR,'
      'ttns.ChekBuhR,'
      'ttns.ProvodTypeP,'
      'ttns.ProvodTypeR,'
      'defr.Name as DepFrom,'
      'deto.Name as DepTo'
      ''
      'from "TTNSelf" ttns'
      ''
      'left join Depart defr on defr.ID=ttns.Depart '
      'left join Depart deto on deto.ID=ttns.FlowDepart '
      ''
      'where ttns.Depart=:DEPART and ttns.DateInvoice=:DATEB'
      'and ttns.AcStatusR=3')
    Params = <
      item
        DataType = ftInteger
        Name = 'DEPART'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 396
    Top = 200
    object quTTnSelfOutDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnSelfOutDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnSelfOutNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnSelfOutFlowDepart: TSmallintField
      FieldName = 'FlowDepart'
    end
    object quTTnSelfOutSummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
    end
    object quTTnSelfOutSummaTovarMove: TFloatField
      FieldName = 'SummaTovarMove'
    end
    object quTTnSelfOutSummaTovarNew: TFloatField
      FieldName = 'SummaTovarNew'
    end
    object quTTnSelfOutSummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quTTnSelfOutOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
    object quTTnSelfOutAcStatusP: TWordField
      FieldName = 'AcStatusP'
    end
    object quTTnSelfOutChekBuhP: TWordField
      FieldName = 'ChekBuhP'
    end
    object quTTnSelfOutAcStatusR: TWordField
      FieldName = 'AcStatusR'
    end
    object quTTnSelfOutChekBuhR: TWordField
      FieldName = 'ChekBuhR'
    end
    object quTTnSelfOutProvodTypeP: TWordField
      FieldName = 'ProvodTypeP'
    end
    object quTTnSelfOutProvodTypeR: TWordField
      FieldName = 'ProvodTypeR'
    end
    object quTTnSelfOutDepFrom: TStringField
      FieldName = 'DepFrom'
      Size = 30
    end
    object quTTnSelfOutDepTo: TStringField
      FieldName = 'DepTo'
      Size = 30
    end
  end
  object quTTnSelfIn: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnInRCalcFields
    SQL.Strings = (
      'select '
      'ttns.Depart,'
      'ttns.DateInvoice,'
      'ttns.Number,'
      'ttns.FlowDepart,'
      'ttns.SummaTovarSpis,'
      'ttns.SummaTovarMove,'
      'ttns.SummaTovarNew,'
      'ttns.SummaTara,'
      'ttns.Oprihod,'
      'ttns.AcStatusP,'
      'ttns.ChekBuhP,'
      'ttns.AcStatusR,'
      'ttns.ChekBuhR,'
      'ttns.ProvodTypeP,'
      'ttns.ProvodTypeR,'
      'defr.Name as DepFrom,'
      'deto.Name as DepTo'
      ''
      'from "TTNSelf" ttns'
      ''
      'left join Depart defr on defr.ID=ttns.Depart '
      'left join Depart deto on deto.ID=ttns.FlowDepart '
      ''
      'where ttns.Depart<>ttns.FlowDepart '
      'and ttns.FlowDepart=:DEPART '
      'and ttns.DateInvoice=:DATEB'
      'and ttns.AcStatusR=3')
    Params = <
      item
        DataType = ftInteger
        Name = 'DEPART'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 484
    Top = 196
    object quTTnSelfInDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnSelfInDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnSelfInNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnSelfInFlowDepart: TSmallintField
      FieldName = 'FlowDepart'
    end
    object quTTnSelfInSummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
    end
    object quTTnSelfInSummaTovarMove: TFloatField
      FieldName = 'SummaTovarMove'
    end
    object quTTnSelfInSummaTovarNew: TFloatField
      FieldName = 'SummaTovarNew'
    end
    object quTTnSelfInSummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quTTnSelfInOprihod: TBooleanField
      FieldName = 'Oprihod'
    end
    object quTTnSelfInAcStatusP: TWordField
      FieldName = 'AcStatusP'
    end
    object quTTnSelfInChekBuhP: TWordField
      FieldName = 'ChekBuhP'
    end
    object quTTnSelfInAcStatusR: TWordField
      FieldName = 'AcStatusR'
    end
    object quTTnSelfInChekBuhR: TWordField
      FieldName = 'ChekBuhR'
    end
    object quTTnSelfInProvodTypeP: TWordField
      FieldName = 'ProvodTypeP'
    end
    object quTTnSelfInProvodTypeR: TWordField
      FieldName = 'ProvodTypeR'
    end
    object quTTnSelfInDepFrom: TStringField
      FieldName = 'DepFrom'
      Size = 30
    end
    object quTTnSelfInDepTo: TStringField
      FieldName = 'DepTo'
      Size = 30
    end
  end
  object taSvod: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 184
    Top = 268
    object taSvodiDep: TIntegerField
      FieldName = 'iDep'
    end
    object taSvodNameDep: TStringField
      FieldName = 'NameDep'
      Size = 50
    end
    object taSvodrIn: TFloatField
      FieldName = 'rIn'
    end
    object taSvodrOut: TFloatField
      FieldName = 'rOut'
    end
    object taSvodrPrih: TFloatField
      FieldName = 'rPrih'
    end
    object taSvodrRet: TFloatField
      FieldName = 'rRet'
    end
    object taSvodrReal: TFloatField
      FieldName = 'rReal'
    end
    object taSvodrInv: TFloatField
      FieldName = 'rInv'
    end
    object taSvodrPereoc: TFloatField
      FieldName = 'rPereoc'
    end
    object taSvodrRealBn: TFloatField
      FieldName = 'rRealBn'
    end
    object taSvodrOutVn: TFloatField
      FieldName = 'rOutVn'
    end
  end
  object frtaSvod: TfrDBDataSet
    DataSet = taSvod
    Left = 124
    Top = 268
  end
  object quTTnOutR3: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnOutRCalcFields
    SQL.Strings = (
      'select * from "TTNOutLn" ln'
      'left join "TTNOut" hd on '
      
        '(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Nu' +
        'mber=ln.Number)'
      ''
      'where ln.DateInvoice='#39'2010-12-18'#39
      'and ln."Depart"=188'
      'and hd."AcStatus"=3'
      'and hd."ProvodType"=3'
      ''
      'Order by ln.Depart, Ln.DateInvoice, ln.Number')
    Params = <>
    Left = 312
    Top = 268
    object quTTnOutR3Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnOutR3DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnOutR3Number: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnOutR3CodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object quTTnOutR3CodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object quTTnOutR3CodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object quTTnOutR3CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quTTnOutR3TovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quTTnOutR3BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quTTnOutR3OKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quTTnOutR3NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quTTnOutR3NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quTTnOutR3OutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object quTTnOutR3Akciz: TFloatField
      FieldName = 'Akciz'
    end
    object quTTnOutR3KolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object quTTnOutR3KolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object quTTnOutR3KolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object quTTnOutR3Kol: TFloatField
      FieldName = 'Kol'
    end
    object quTTnOutR3CenaPost: TFloatField
      FieldName = 'CenaPost'
    end
    object quTTnOutR3CenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object quTTnOutR3CenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object quTTnOutR3SumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object quTTnOutR3SumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object quTTnOutR3CodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object quTTnOutR3VesTara: TFloatField
      FieldName = 'VesTara'
    end
    object quTTnOutR3CenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object quTTnOutR3CenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
    object quTTnOutR3SumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object quTTnOutR3SumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object quTTnOutR3SumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object quTTnOutR3ChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
    object quTTnOutR3Depart_1: TSmallintField
      FieldName = 'Depart_1'
    end
    object quTTnOutR3DateInvoice_1: TDateField
      FieldName = 'DateInvoice_1'
    end
    object quTTnOutR3Number_1: TStringField
      FieldName = 'Number_1'
      Size = 10
    end
    object quTTnOutR3SCFNumber: TStringField
      FieldName = 'SCFNumber'
      Size = 10
    end
    object quTTnOutR3IndexPoluch: TSmallintField
      FieldName = 'IndexPoluch'
    end
    object quTTnOutR3CodePoluch: TSmallintField
      FieldName = 'CodePoluch'
    end
    object quTTnOutR3SummaTovar: TFloatField
      FieldName = 'SummaTovar'
    end
    object quTTnOutR3NDS10: TFloatField
      FieldName = 'NDS10'
    end
    object quTTnOutR3OutNDS10: TFloatField
      FieldName = 'OutNDS10'
    end
    object quTTnOutR3NDS20: TFloatField
      FieldName = 'NDS20'
    end
    object quTTnOutR3OutNDS20: TFloatField
      FieldName = 'OutNDS20'
    end
    object quTTnOutR3LgtNDS: TFloatField
      FieldName = 'LgtNDS'
    end
    object quTTnOutR3OutLgtNDS: TFloatField
      FieldName = 'OutLgtNDS'
    end
    object quTTnOutR3NDSTovar: TFloatField
      FieldName = 'NDSTovar'
    end
    object quTTnOutR3OutNDSTovar: TFloatField
      FieldName = 'OutNDSTovar'
    end
    object quTTnOutR3NotNDSTovar: TFloatField
      FieldName = 'NotNDSTovar'
    end
    object quTTnOutR3OutNDSSNTovar: TFloatField
      FieldName = 'OutNDSSNTovar'
    end
    object quTTnOutR3SummaTovarSpis: TFloatField
      FieldName = 'SummaTovarSpis'
    end
    object quTTnOutR3NacenkaTovar: TFloatField
      FieldName = 'NacenkaTovar'
    end
    object quTTnOutR3SummaTara: TFloatField
      FieldName = 'SummaTara'
    end
    object quTTnOutR3SummaTaraSpis: TFloatField
      FieldName = 'SummaTaraSpis'
    end
    object quTTnOutR3NacenkaTara: TFloatField
      FieldName = 'NacenkaTara'
    end
    object quTTnOutR3AcStatus: TWordField
      FieldName = 'AcStatus'
    end
    object quTTnOutR3ChekBuh_1: TWordField
      FieldName = 'ChekBuh_1'
    end
    object quTTnOutR3NAvto: TStringField
      FieldName = 'NAvto'
      Size = 10
    end
    object quTTnOutR3NPList: TStringField
      FieldName = 'NPList'
      Size = 10
    end
    object quTTnOutR3FIOVod: TStringField
      FieldName = 'FIOVod'
      Size = 24
    end
    object quTTnOutR3PrnDate: TDateField
      FieldName = 'PrnDate'
    end
    object quTTnOutR3PrnNumber: TStringField
      FieldName = 'PrnNumber'
      Size = 10
    end
    object quTTnOutR3PrnKol: TFloatField
      FieldName = 'PrnKol'
    end
    object quTTnOutR3PrnAkt: TStringField
      FieldName = 'PrnAkt'
      Size = 10
    end
    object quTTnOutR3ProvodType: TWordField
      FieldName = 'ProvodType'
    end
    object quTTnOutR3NotNDS10: TFloatField
      FieldName = 'NotNDS10'
    end
    object quTTnOutR3NotNDS20: TFloatField
      FieldName = 'NotNDS20'
    end
    object quTTnOutR3WithNDS10: TFloatField
      FieldName = 'WithNDS10'
    end
    object quTTnOutR3WithNDS20: TFloatField
      FieldName = 'WithNDS20'
    end
    object quTTnOutR3Crock: TFloatField
      FieldName = 'Crock'
    end
    object quTTnOutR3Discount: TFloatField
      FieldName = 'Discount'
    end
    object quTTnOutR3NDS010: TFloatField
      FieldName = 'NDS010'
    end
    object quTTnOutR3NDS020: TFloatField
      FieldName = 'NDS020'
    end
    object quTTnOutR3NDS_10: TFloatField
      FieldName = 'NDS_10'
    end
    object quTTnOutR3NDS_20: TFloatField
      FieldName = 'NDS_20'
    end
    object quTTnOutR3NSP_10: TFloatField
      FieldName = 'NSP_10'
    end
    object quTTnOutR3NSP_20: TFloatField
      FieldName = 'NSP_20'
    end
    object quTTnOutR3SCFDate: TDateField
      FieldName = 'SCFDate'
    end
    object quTTnOutR3GoodsNSP0: TFloatField
      FieldName = 'GoodsNSP0'
    end
    object quTTnOutR3GoodsCostNSP: TFloatField
      FieldName = 'GoodsCostNSP'
    end
    object quTTnOutR3OrderNumber: TIntegerField
      FieldName = 'OrderNumber'
    end
    object quTTnOutR3ID: TIntegerField
      FieldName = 'ID'
    end
    object quTTnOutR3LinkInvoice: TWordField
      FieldName = 'LinkInvoice'
    end
    object quTTnOutR3StartTransfer: TWordField
      FieldName = 'StartTransfer'
    end
    object quTTnOutR3EndTransfer: TWordField
      FieldName = 'EndTransfer'
    end
    object quTTnOutR3REZERV: TStringField
      FieldName = 'REZERV'
      Size = 4
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 588
    Top = 136
    object N1: TMenuItem
      Action = acCorrPartOut
    end
    object N2: TMenuItem
      Action = acGenSv
    end
  end
  object quTTnOutRSpis: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quTTnOutRCalcFields
    SQL.Strings = (
      'select  '
      'ttn."Depart",'
      'ttn."DateInvoice",'
      'ttn."Number",'
      'ttn."Crock"'
      ''
      'from "TTNOut" as ttn'
      ''
      'where ttn.DateInvoice=:DATEB'
      'and ttn."Depart"=:DEPART'
      'and ttn."AcStatus"=3'
      'and ttn."ProvodType"=3'
      'and ttn."Crock">0'
      '')
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftInteger
        Name = 'DEPART'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 308
    Top = 332
    object quTTnOutRSpisDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quTTnOutRSpisDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quTTnOutRSpisNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object quTTnOutRSpisCrock: TFloatField
      FieldName = 'Crock'
    end
  end
  object teSSR: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 464
    Top = 278
    object teSSRiDate: TIntegerField
      FieldName = 'iDate'
    end
    object teSSRdDate: TDateField
      FieldName = 'dDate'
    end
    object teSSRiDep: TIntegerField
      FieldName = 'iDep'
    end
    object teSSRNameDep: TStringField
      FieldName = 'NameDep'
      Size = 50
    end
    object teSSRRSUM: TFloatField
      FieldName = 'RSUM'
      DisplayFormat = '0.00'
    end
    object teSSRRSUMIN: TFloatField
      FieldName = 'RSUMIN'
      DisplayFormat = '0.00'
    end
    object teSSRRSUMINPP: TFloatField
      FieldName = 'RSUMINPP'
      DisplayFormat = '0.00'
    end
    object teSSRRSUMDIF: TFloatField
      FieldName = 'RSUMDIF'
      DisplayFormat = '0.00'
    end
  end
  object dsteSSR: TDataSource
    DataSet = teSSR
    Left = 466
    Top = 332
  end
end
