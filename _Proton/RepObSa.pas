unit RepObSa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk;

type
  TfmRepOb = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridObVed: TcxGrid;
    taObVed: TClientDataSet;
    dsObVed: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepOb: TfrReport;
    frtaObVed: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    taObVedIdCode: TIntegerField;
    taObVedNameC: TStringField;
    taObVediM: TIntegerField;
    taObVedSm: TStringField;
    taObVedIdGroup: TIntegerField;
    taObVedNameClass: TStringField;
    taObVedQBeg: TFloatField;
    taObVedSBeg: TCurrencyField;
    taObVedQIn: TFloatField;
    taObVedSIn: TCurrencyField;
    taObVedQOut: TFloatField;
    taObVedSOut: TCurrencyField;
    taObVedQRes: TFloatField;
    taObVedSRes: TCurrencyField;
    taObVedPrice: TFloatField;
    taObVedKm: TFloatField;
    teObVed: TdxMemData;
    teObVedIdCode: TIntegerField;
    teObVedNameC: TStringField;
    teObVediM: TSmallintField;
    teObVedSm: TStringField;
    teObVedIdGroup: TIntegerField;
    teObVedQBeg: TFloatField;
    teObVedSBeg: TCurrencyField;
    teObVedQIn: TFloatField;
    teObVedSIn: TCurrencyField;
    teObVedQOut: TFloatField;
    teObVedSOut: TCurrencyField;
    teObVedQRes: TFloatField;
    teObVedSRes: TCurrencyField;
    teObVedPrice: TFloatField;
    teObVedKm: TFloatField;
    dsquAllCards: TDataSource;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teObVedNameGr1: TStringField;
    teObVedNameGr2: TStringField;
    LevelObVed: TcxGridLevel;
    ViewObVed: TcxGridDBTableView;
    ViewObVedRecId: TcxGridDBColumn;
    ViewObVedIdCode: TcxGridDBColumn;
    ViewObVedNameC: TcxGridDBColumn;
    ViewObVedSm: TcxGridDBColumn;
    ViewObVedQBeg: TcxGridDBColumn;
    ViewObVedSBeg: TcxGridDBColumn;
    ViewObVedQIn: TcxGridDBColumn;
    ViewObVedSIn: TcxGridDBColumn;
    ViewObVedQOut: TcxGridDBColumn;
    ViewObVedSOut: TcxGridDBColumn;
    ViewObVedQRes: TcxGridDBColumn;
    ViewObVedSRes: TcxGridDBColumn;
    ViewObVedPrice: TcxGridDBColumn;
    ViewObVedNameGr1: TcxGridDBColumn;
    ViewObVedNameGr2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure taObVedCalcFields(DataSet: TDataSet);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepOb: TfmRepOb;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps;

{$R *.dfm}

procedure TfmRepOb.FormCreate(Sender: TObject);
begin
  GridObVed.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewObVed.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepOb.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewObVed.StoreToIniFile(CurDir+GridIni,False);
  teObVed.Active:=False;
end;

procedure TfmRepOb.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepOb.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepOb.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewObVed);
end;

procedure TfmRepOb.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewObVed.BeginUpdate;
  teObVed.Filter:=ViewObVed.DataController.Filter.FilterText;
  teObVed.Filtered:=True;

  frRepOb.LoadFromFile(CurDir + 'ObVed.frf');

  if CommonSet.DateTo>=iMaxDate then frVariables.Variable['sPeriod']:=' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewObVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepOb.ReportName:='��������� ���������.';
  frRepOb.PrepareReport;
  frRepOb.ShowPreparedReport;

  teObVed.Filter:='';
  teObVed.Filtered:=False;
  ViewObVed.EndUpdate;
end;

procedure TfmRepOb.taObVedCalcFields(DataSet: TDataSet);
begin
  taObVedQRes.AsFloat:=taObVedQBeg.AsFloat+taObVedQIn.AsFloat-taObVedQOut.AsFloat;
  taObVedSRes.AsFloat:=taObVedSBeg.AsFloat+taObVedSIn.AsFloat-taObVedSOut.AsFloat;
end;

procedure TfmRepOb.SpeedItem2Click(Sender: TObject);
type TRSum = record
     qRemn,qIn,qRet,qVnIn,qVnOut,qOut,qInv,qInvIn,qInvOut,qRes:Real;
     sRemn,sIn,sRet,sVnIn,sVnOut,sOut,sInv,sInvIn,sInvOut,sRes:Real;
     end;

Var  bAdd:Boolean;
     iCount:Integer;
     arSum:TRSum;
     rQ,rQRec,rPr,rPrU,rPr0,rSum0:Real;
     S1,S2:String;

begin
//��������� ���������
  if not CanDo('prRepObVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    with dmO do
    with dmORep do
    begin
//      fmRepOb.Show;  delay(10);//}
      with fmRepOb do
      begin
        Memo1.Clear;
        Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
        ViewObVed.BeginUpdate;

        CloseTe(fmRepOb.teObVed);

        quAllCards.Active:=False;
        quAllCards.Active:=True;

        dsquAllCards.DataSet:=dmO.quAllCards;
        PBar1.Properties.Min:=0;
        PBar1.Properties.Max:=quAllCards.RecordCount;
        iCount:=0;
        PBar1.Position:=iCount;
        PBar1.Visible:=True;

        quAllCards.First;
        while not quAllCards.Eof do
        begin
          bAdd:=False;
          //� �������
          arSum.qRemn:=prCalcRemn(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore);

          quMovePer.Active:=False;
          quMovePer.ParamByName('IDGOOD').AsInteger:=quAllCardsID.AsInteger;
          quMovePer.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
          quMovePer.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
          quMovePer.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
          quMovePer.Active:=True;

          arSum.qIn:=round(quMovePerPOSTIN.AsFloat*1000)/1000;
          arSum.qRet :=round(quMovePerPOSTOUT.AsFloat*1000)/1000;
          arSum.qVnIn :=round(quMovePerVNIN.AsFloat*1000)/1000;
          arSum.qVnOut :=round(quMovePerVNOUT.AsFloat*1000)/1000;
          arSum.qOut:=round(quMovePerQREAL.AsFloat*1000)/1000;
          arSum.qInv:=round(quMovePerINV.AsFloat*1000)/1000;

          if abs(arSum.qRemn)>=0.001 then bAdd:=True;
          if abs(arSum.qIn)>=0.001 then bAdd:=True;
          if abs(arSum.qRet)>=0.001 then bAdd:=True;
          if abs(arSum.qVnIn)>=0.001 then bAdd:=True;
          if abs(arSum.qVnOut)>=0.001 then bAdd:=True;
          if abs(arSum.qOut)>=0.001 then bAdd:=True;
          if abs(arSum.qInv)>=0.001 then bAdd:=True;

          quMovePer.Active:=False;

          if bAdd then
          begin
            //������������ �� ��������� �������
            //��� � ��������
            arSum.sRemn:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore,arSum.qRemn,rSum0);
            //��� � �������
            if quAllCardsKOEF.AsFloat<>0 then arSum.qRemn:=arSum.qRemn/quAllCardsKOEF.AsFloat; //�� �������� ����� � �������

            if quAllCardsKOEF.AsFloat<>0 then
            begin
             arSum.qIn:=arSum.qIn/quAllCardsKOEF.AsFloat;
             arSum.qRet:=arSum.qRet/quAllCardsKOEF.AsFloat;
             arSum.qVnIn:=arSum.qVnIn/quAllCardsKOEF.AsFloat;
             arSum.qVnOut:=arSum.qVnOut/quAllCardsKOEF.AsFloat;
             arSum.qOut:=arSum.qOut/quAllCardsKOEF.AsFloat;
             arSum.qInv:=arSum.qInv/quAllCardsKOEF.AsFloat;
            end;

            if arSum.qInv>=0 then begin arSum.qInvIn:=arSum.qInv; arSum.qInvOut:=0; end;
            if arSum.qInv<0 then begin arSum.qInvIn:=0; arSum.qInvOut:=(-1)*arSum.qInv; end;

            arSum.sIn:=0;arSum.sVnIn:=0;arSum.sInvIn:=0;
            arSum.sRet:=0;arSum.sVnOut:=0;arSum.sInvOut:=0;arSum.sOut:=0;

            //������

            if arSum.qVnIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE<>1 and DTYPE<>3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qVnIn;
              arSum.sVnIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnIn:=arSum.sVnIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sVnIn:=arSum.sVnIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qInvIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qInvIn;
              arSum.sInvIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvIn:=arSum.sInvIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sInvIn:=arSum.sInvIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=1');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');
              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qIn;
              arSum.sIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sIn:=arSum.sIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sIn:=arSum.sIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;
              quSelInSum.Active:=False;
            end;




{
SELECT IDDATE,QUANT,PRICEIN,SUMOUT
FROM OF_PARTOUT
where IDDATE>=:DATEB and IDDATE<=:DATEE and IDSTORE=:IDSKL and ARTICUL=:IDGOOD and DTYPE=1
order by IDDATE desc
}

// ������

            if arSum.qRet>0 then  //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=7');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qRet; arSum.sRet:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sRet:=arSum.sRet+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sRet:=arSum.sRet+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qOut>0 then //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=2 or DTYPE=8)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qOut; arSum.sOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sOut:=arSum.sOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sOut:=arSum.sOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qInvOut>0 then //��������������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qInvOut; arSum.sInvOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvOut:=arSum.sInvOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sInvOut:=arSum.sInvOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qVnOut>0 then //���������� ������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=4 or DTYPE=5 or DTYPE=6)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qVnOut; arSum.sVnOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnOut:=arSum.sVnOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sVnOut:=arSum.sVnOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            arSum.QRes:=arSum.qRemn+(arSum.qIn+arSum.qVnIn+arSum.qInvIn)-(arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut);
            //����� ��� � ������ ��������

            arSum.SRes:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateTo),CommonSet.IdStore,(arSum.QRes*quAllCardsKOEF.AsFloat),rSum0);

            prFind2group(quAllCardsPARENT.AsInteger,S1,S2);

            teObVed.Append;
            teObVedIdCode.AsInteger:=quAllCardsID.AsInteger;
            teObVedNameC.AsString:=quAllCardsNAME.AsString;
            teObVediM.AsInteger:=quAllCardsIMESSURE.AsInteger;
            teObVedSm.AsString:=quAllCardsNAMESHORT.AsString;
            teObVedIdGroup.AsInteger:=quAllCardsPARENT.AsInteger;
            teObVedNameGr1.AsString:=S1;
            teObVedNameGr2.AsString:=S2;
            teObVedQBeg.AsFloat:=arSum.qRemn;
            teObVedSBeg.AsFloat:=arSum.sRemn;
            teObVedQIn.AsFloat:=arSum.qIn+arSum.qVnIn+arSum.qInvIn;
            teObVedSIn.AsFloat:=arSum.sIn+arSum.sVnIn+arSum.sInvIn;
            teObVedQOut.AsFloat:=arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut;
            teObVedSOut.AsFloat:=arSum.sRet+arSum.sVnOut+arSum.sOut+arSum.sInvOut;
            teObVedQRes.AsFloat:=arSum.QRes;
            teObVedSRes.AsFloat:=arSum.SRes;
            teObVedPrice.AsFloat:=rPr;
            teObVedKm.AsFloat:=quAllCardsKOEF.AsFloat;
            teObVed.Post;

          end;

          quAllCards.Next;
          inc(iCount);
          PBar1.Position:=iCount; delay(10);
        end;

        dsquAllCards.DataSet:=Nil;
        PBar1.Visible:=False;

        ViewObVed.EndUpdate;
      end;
      fmRepOb.Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmRepOb.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRepOb.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

end.
