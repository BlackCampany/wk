unit MainMCryst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ActnMan, ActnColorMaps, ActnList,
  XPStyleActnCtrls, SpeedBar, ExtCtrls, ToolWin, ActnCtrls, ActnMenus,
  ComCtrls, cxControls, cxContainer, cxEdit, cxTrackBar, cxGraphics,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, DB, pvtables, sqldataset, cxCheckBox, ShellApi, IniFiles,
  cxCalendar;

type
  TfmMainMCryst = class(TForm)


    ActionMainMenuBar1: TActionMainMenuBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    AM1: TActionManager;
    System: TAction;
    Exit: TAction;
    acCard: TAction;
    acCountry: TAction;
    acTrans: TAction;
    acDocs: TAction;
    acRemns: TAction;
    acBrands: TAction;
    acEU: TAction;
    XPColorMap1: TXPColorMap;
    StatusBar1: TStatusBar;
    acMakers: TAction;
    acDeparts: TAction;
    acDocsIn: TAction;
    acCli: TAction;
    acTara: TAction;
    acOut: TAction;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem12: TSpeedItem;
    acPereoc: TAction;
    acVn: TAction;
    acInv: TAction;
    SpeedItem13: TSpeedItem;
    acBufPrice: TAction;
    acLoadCashAll: TAction;
    acCashList: TAction;
    acTakeCash: TAction;
    acCreateCashMove: TAction;
    acRepCash: TAction;
    acTovRep: TAction;
    acOb: TAction;
    acRemnDay: TAction;
    acTerm: TAction;
    cxLookupComboBox1: TcxLookupComboBox;
    Label1: TLabel;
    acLabes: TAction;
    acTermo: TAction;
    acScaleSpr: TAction;
    acScaleLoad: TAction;
    acRecalcRemn: TAction;
    acCalcCashByDep: TAction;
    acRepReal: TAction;
    acDStop: TAction;
    acCheck: TAction;
    asBufRebild: TAction;
    acRecalcAllRemnReal: TAction;
    acRepRealDeps: TAction;
    acZerro: TAction;
    acMoveTara: TAction;
    acDStopList: TAction;
    acTestMoveTara: TAction;
    acTestMove: TAction;
    acTestMoveWk: TAction;
    cxCheckBox1: TcxCheckBox;
    Label2: TLabel;
    acTestMoveVin: TAction;
    acTovPeriod: TAction;
    acStatus: TAction;
    acRecalcCassir: TAction;
    acCalcPostAssortiment: TAction;
    acRepTovZapas: TAction;
    acNoUser: TAction;
    acRepHour: TAction;
    acRepCliSum: TAction;
    acDelCards: TAction;
    acDelMove0: TAction;
    acDelStop: TAction;
    acZadList: TAction;
    TimerZad: TTimer;
    acTestRecalcMove1: TAction;
    Action2: TAction;
    acRecalcSt: TAction;
    acRecalcSSIn: TAction;
    acRepPrib: TAction;
    acPostZ1: TAction;
    acAkciya: TAction;
    acExch: TAction;
    SpeedItem14: TSpeedItem;
    acOnlyTestRecalcMove: TAction;
    acMoveBZ: TAction;
    acRemnDayBZ: TAction;
    acTestNoUse1: TAction;
    acPartCorr: TAction;
    Label3: TLabel;
    acObor: TAction;
    acActionsR: TAction;
    acLoadClassif: TAction;
    acAssortimentPost: TAction;
    acRemnDays: TAction;
    acRecalcAssPost: TAction;
    acCashFBLoad: TAction;
    acRepCassir: TAction;
    acGrafPostavok: TAction;
    acPartFil0: TAction;
    Action3: TAction;
    TimerTestInv: TTimer;
    acCatStopList: TAction;
    cxDEdit1: TcxDateEdit;
    acAlgClass: TAction;
    acAlg: TAction;
    acKassir: TAction;
    acReasonVoz: TAction;
    acGrafVoz: TAction;
    acDocHVoz: TAction;
    acAlcoNew: TAction;
    acCalcRecalc: TAction;
    acTypeVoz: TAction;
    acMV31: TAction;
    acObrPredz: TAction;
    acDelayVoz: TAction;
    acAltPost: TAction;
    acInvVoz: TAction;
    acTestRemn: TAction;
    acDocInvVoz: TAction;
    acHistOp: TAction;
    acUpLica: TAction;
    acSyncCash: TAction;
    acRealAlco: TAction;
    acCashFBLoadAlco: TAction;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure ExitExecute(Sender: TObject);
    procedure acCardExecute(Sender: TObject);
    procedure acCountryExecute(Sender: TObject);
    procedure acBrandsExecute(Sender: TObject);
    procedure acMakersExecute(Sender: TObject);
    procedure acDepartsExecute(Sender: TObject);
    procedure acEUExecute(Sender: TObject);
    procedure acDocsInExecute(Sender: TObject);
    procedure acCliExecute(Sender: TObject);
    procedure acTaraExecute(Sender: TObject);
    procedure acOutExecute(Sender: TObject);
    procedure acPereocExecute(Sender: TObject);
    procedure acVnExecute(Sender: TObject);
    procedure acInvExecute(Sender: TObject);
    procedure acBufPriceExecute(Sender: TObject);
    procedure acCashListExecute(Sender: TObject);
    procedure acTakeCashExecute(Sender: TObject);
    procedure acCreateCashMoveExecute(Sender: TObject);
    procedure acRepCashExecute(Sender: TObject);
    procedure acTovRepExecute(Sender: TObject);
    procedure acObExecute(Sender: TObject);
    procedure acRemnDayExecute(Sender: TObject);
    procedure acTermExecute(Sender: TObject);
    procedure acLabesExecute(Sender: TObject);
    procedure acTermoExecute(Sender: TObject);
    procedure acScaleSprExecute(Sender: TObject);
    procedure acScaleLoadExecute(Sender: TObject);
    procedure acRecalcRemnExecute(Sender: TObject);
    procedure acCalcCashByDepExecute(Sender: TObject);
    procedure acRepRealExecute(Sender: TObject);
    procedure acDStopExecute(Sender: TObject);
    procedure acCheckExecute(Sender: TObject);
    procedure asBufRebildExecute(Sender: TObject);
    procedure acRecalcAllRemnRealExecute(Sender: TObject);
    procedure acLoadCashAllExecute(Sender: TObject);
    procedure acRepRealDepsExecute(Sender: TObject);
    procedure acZerroExecute(Sender: TObject);
    procedure acMoveTaraExecute(Sender: TObject);
    procedure acDStopListExecute(Sender: TObject);
    procedure acTestMoveTaraExecute(Sender: TObject);
    procedure acTestMoveExecute(Sender: TObject);
    procedure acTestMoveWkExecute(Sender: TObject);
    procedure acTestMoveVinExecute(Sender: TObject);
    procedure acTovPeriodExecute(Sender: TObject);
    procedure acStatusExecute(Sender: TObject);
    procedure acRecalcCassirExecute(Sender: TObject);
    procedure acCalcPostAssortimentExecute(Sender: TObject);
    procedure acRepTovZapasExecute(Sender: TObject);
    procedure acNoUserExecute(Sender: TObject);
    procedure acRepHourExecute(Sender: TObject);
    procedure acRepCliSumExecute(Sender: TObject);
    procedure acDelCardsExecute(Sender: TObject);
    procedure acDelMove0Execute(Sender: TObject);
    procedure acDelStopExecute(Sender: TObject);
    procedure acZadListExecute(Sender: TObject);
    procedure TimerZadTimer(Sender: TObject);
    procedure acTestRecalcMove1Execute(Sender: TObject);
    procedure acRecalcStExecute(Sender: TObject);
    procedure acRecalcSSInExecute(Sender: TObject);
    procedure acRepPribExecute(Sender: TObject);
    procedure acPostZ1Execute(Sender: TObject);
    procedure acAkciyaExecute(Sender: TObject);
    procedure acExchExecute(Sender: TObject);
    procedure acOnlyTestRecalcMoveExecute(Sender: TObject);
    procedure acMoveBZExecute(Sender: TObject);
    procedure acRemnDayBZExecute(Sender: TObject);
    procedure acTestNoUse1Execute(Sender: TObject);
    procedure acPartCorrExecute(Sender: TObject);
    procedure Label3DblClick(Sender: TObject);
    procedure acOborExecute(Sender: TObject);
    procedure acActionsRExecute(Sender: TObject);
    procedure acLoadClassifExecute(Sender: TObject);
    procedure acAssortimentPostExecute(Sender: TObject);
    procedure acRemnDaysExecute(Sender: TObject);
    procedure acRecalcAssPostExecute(Sender: TObject);
    procedure acCashFBLoadExecute(Sender: TObject);
    procedure acRepCassirExecute(Sender: TObject);
    procedure acGrafPostavokExecute(Sender: TObject);
    procedure acPartFil0Execute(Sender: TObject);
    procedure TimerTestInvTimer(Sender: TObject);
    procedure acCatStopListExecute(Sender: TObject);
    procedure acAlgClassExecute(Sender: TObject);
    procedure acAlgExecute(Sender: TObject);
    procedure acKassirExecute(Sender: TObject);
    procedure acReasonVozExecute(Sender: TObject);
    procedure acGrafVozExecute(Sender: TObject);
    procedure acDocHVozExecute(Sender: TObject);
    procedure acAlcoNewExecute(Sender: TObject);
    procedure acCalcRecalcExecute(Sender: TObject);
    procedure acTypeVozExecute(Sender: TObject);
    procedure acMV31Execute(Sender: TObject);
    procedure acObrPredzExecute(Sender: TObject);
    procedure acDelayVozExecute(Sender: TObject);
    procedure acAltPostExecute(Sender: TObject);
    procedure acInvVozExecute(Sender: TObject);
    procedure acTestRemnExecute(Sender: TObject);
    procedure acDocInvVozExecute(Sender: TObject);
    procedure acHistOpExecute(Sender: TObject);
    procedure acUpLicaExecute(Sender: TObject);
    procedure acSyncCashExecute(Sender: TObject);
    procedure acRealAlcoExecute(Sender: TObject);
    procedure acCashFBLoadAlcoExecute(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function prOA(S:String):String;
Function prAO(S:String):String;
procedure SetDecimalSeparator( Ch: String );

var
  fmMainMCryst: TfmMainMCryst;

implementation

uses Un1, MDB, PasswMainCard, mCards, mCountry, mBrands, mMakers, mDeparts,
  mEu, u2fdk, DocsIn, Clients, mTara, DocsOut, DocsAC, DocsVn, BufPrice,
  CashList, Status, Period1, PXDB, CashRep, Gens, ToRep, Period, ObMove,
  DocsInv, TermLoad, Labels, TermoPr, ScaleSpr, Scales, MT, RepReal,
  Checks, CashDep, ObMoveTara, DStop, RVed, PeriodR, RepTovarPeriod,
  uCateg, RepRDay, RepHour, uGraf3, Zadan, ZadMessge, MFB, Prib1, Akciya,
  DocObm, DocZ, uTermElisey, DepSel, OborRep, AssortPost, RDayVedWide,
  CashFBLoad, Cassirs, GrafPostavok, AddDoc2,RepVoz, CatDisc, AlgClass,
  PreAlg, RepAlg, Kadr, ReasonsV, dmPS, GrafVozvrat, DocHVoz, TypeVoz,
  ObrPredz, DelayV,DBAlg,AlcoHd, AltPost, InvVoz, SInvVoz, HistoryOper,
  dbe, UpLica, SyncCash, PreRealAlco, RepRealAP, LoadCashAVid;

{$R *.dfm}


Function prOA(S:String):String;
begin
  Result:=OemToAnsiConvert(S);
end;

Function prAO(S:String):String;
begin
  Result:=AnsiToOemConvert(S);
end;


procedure TfmMainMCryst.FormCreate(Sender: TObject);
var FversionL:textfile;
    iversionL:integer;
    FversionN:textfile;
    iversionN:integer;
begin
//  WriteHistory(' FormCreate');
//  WriteHistory(' CurDir...');
  CurDir := ExtractFilePath(ParamStr(0));
//  WriteHistory(' ReadIni...');
  ReadIni;
//  WriteHistory(' DateB,DateE ...');
  CommonSet.Ip:=prGetIP;

  if pos(' ',CommonSet.Ip)>0 then CommonSet.Ip:=Copy(CommonSet.Ip,1,pos(' ',CommonSet.Ip));

  Person.NameUser:=GetCurrentUserName;

  CommonSet.DateBeg:=Date;   //������ �������
  CommonSet.DateEnd:=Date;
  CommonSet.CurNac:=20;

  CommonSet.ADateBeg:=Date-180;
  CommonSet.ADateEnd:=Date+30;

  try
    if DirectoryExists(CurDir+'Arh')=False then createdir(CurDir+'Arh');
    if DirectoryExists(CurDir+'Tmp')=False then createdir(CurDir+'Tmp');
    if DirectoryExists(CurDir+'Trf')=False then createdir(CurDir+'Trf');
    if DirectoryExists(CurDir+DelP(CommonSet.Ip))=False then createdir(CurDir+DelP(CommonSet.Ip));
    if DirectoryExists(CurDir+DelP(CommonSet.Ip)+'\'+Person.NameUser)=False then createdir(CurDir+DelP(CommonSet.Ip)+'\'+Person.NameUser);
  except
  end;

  if FileExists(CurDir+'SpecOut.cds') then DeleteFile(CurDir+'SpecOut.cds');
  try
    iversionL:=0;
    iversionN:=0;
    if FileExists('version.txt') and FileExists(CommonSet.UpdatePath+'version.txt') then
    begin
      try
        AssignFile(FversionL, 'version.txt');
        Reset(FversionL);
        Readln(FversionL, iversionL);

        AssignFile(FversionN, CommonSet.UpdatePath+'version.txt');
        Reset(FversionN);
        Readln(FversionN, iversionN);
      finally
        CloseFile(FversionL);
        CloseFile(FversionN);
      end;
    end;
    if iversionL<iversionN then ShowMessage('������ ��������� ��������, �������� ���������');
  except
    ShowMessage('������ ��������� �� ����������, �������� ���������');
  end;

  if CommonSet.CalcSeb=0 then acRecalcSSIn.Visible:=False else acRecalcSSIn.Visible:=True;
  if CommonSet.CalcSeb=0 then acRepPrib.Visible:=False else acRepPrib.Visible:=True;
  if CommonSet.CalcSeb=0 then acPartCorr.Visible:=False else acPartCorr.Visible:=True;
//  if CommonSet.Single=1 then acPostZ1.Visible:=True else acPostZ1.Visible:=False;
  if CommonSet.Single=1 then acRemnDays.Visible:=True else acRemnDays.Visible:=False;
  if CommonSet.Single=1 then acRecalcAssPost.Visible:=True else acRecalcAssPost.Visible:=False;
  if CommonSet.Single=1 then acRepCassir.Visible:=True else acRepCassir.Visible:=False;
  if CommonSet.Single=1 then acAlg.Visible:=True else acAlg.Visible:=False;
  if CommonSet.Single=1 then acCalcCashByDep.Visible:=True else acCalcCashByDep.Visible:=False;
  if CommonSet.Single=1 then acAlcoNew.Visible:=True else acAlcoNew.Visible:=False;

//  SetDecimalSeparator('.');
end;

procedure TfmMainMCryst.FormShow(Sender: TObject);
Var StrErr,fileName:String;
    sDep:String;
    iDep:Integer;
begin


//  WriteHistory(' Show');
  Person.Modul:='Crd';
  StrErr:='';
  with dmMC do
  begin
    try
//      WriteHistory(' - ���������� � �����..');
      PvSQL.Connected:=False;
//      PvSQL.AliasName:=CommonSet.PathCryst;
      PvSQL.Connected:=True;
      if PvSQL.Connected=False then StrErr:='�� ������� ������������ � ���� ������.';
//      WriteHistory(' Ok.');
    except
      StrErr:='������ ��� ����������� � ���� ������.';
    end;
//    WriteHistory(' ... '+StrErr);
  end;
  if StrErr='' then
  begin
//    WriteHistory(' �������� �������������.');
    fmPerA:=tfmPerA.Create(Application);
//    WriteHistory(' ����������� �������������.');
    fmPerA.ShowModal;
    if fmPerA.ModalResult=mrOk then Caption:=Caption+'   : '+Person.Name
      else StrErr:='������ ������������� ������������';
    fmPerA.Release;
  end;
  if StrErr>'' then
  begin
    ShowMessage(StrErr);
    Close;
  end else writeini;

//  dmMC.taClass.Active:=True;
  bRefreshCard:=False;
  fmCards:=TFmCards.Create(Application);
  try
    if fmCards.CardsTree.Items.Count>0 then fmCards.CardsTree.Items[0].Selected:=True;
  except
  end;
  bRefreshCard:=True;

{ SetDecimalSeparator('.');
  fmScale:=TFmScale.Create(Application);
  SetDecimalSeparator(',');}

//  fmScale:=TFmScale.Create(Application);

  with dmMC do
  with dmMT do
  begin
    quScale.Active:=False; quScale.Active:=True; quScale.First;
//    if quScale.RecordCount>0 then cxLookupComboBox1.EditValue:=quScaleNumber.AsString;

{
    if quScale.RecordCount>0 then
    begin
      quScaleItems.Active:=False;
      quScaleItems.ParamByName('SNUM').AsString:=quScaleNumber.AsString;
      quScaleItems.Active:=True;
    end;
}

    quCountry.Active:=False; quCountry.Active:=True;
    quCateg.Active:=False; quCateg.Active:=True;
    quBrands.Active:=False; quBrands.Active:=True;
    quMakers.Active:=False; quMakers.Active:=True;
    quDeparts.Active:=False; quDeparts.Active:=True;
    quEU.Active:=False; quEU.Active:=True;

    quLabs.Active:=True;
    quLabs.First;
    cxLookupComboBox1.EditValue:=quLabsID.AsInteger;
    cxLookupComboBox1.Text:=quLabsNAME.AsString;

    iShop:=fShop;
    if CommonSet.Single=1 then CommonSet.Single:=fSingle;

    if taCards.Active=False then taCards.Active:=True;
    if ptDep.Active=False then ptDep.Active:=True;

    ptDep.First;
    sDep:=''; iDep:=0;
    while not ptDep.Eof do
    begin
      if ptDepStatus1.AsInteger=9 then
      begin
        if iDep=0 then begin iDep:=ptDepID.AsInteger; sDep:=OemToAnsiConvert(ptDepFullName.AsString); end;
        if ptDepV01.AsInteger=1 then begin iDep:=ptDepID.AsInteger; sDep:=OemToAnsiConvert(ptDepFullName.AsString); end;
      end;
      ptDep.Next;
    end;



    Label3.Caption:=Copy(sDEp,1,50);
    Label3.Tag:=iDep;

    cxDEdit1.Date:=Date;


    //����� ����� ������ ��������� ��� �������������
{
    if Person.Id=1001 then
    begin
      if ptPersonal.Active=False then ptPersonal.Active:=True else ptPersonal.Refresh;
      if ptPersonal.FindKey([1001]) then
      begin
        ptPersonal.Edit;
        ptPersonalPASSW.AsString:='100663';
        ptPersonal.Post;
      end;
    end;
    if Person.Id=1010 then
    begin
      if ptPersonal.Active=False then ptPersonal.Active:=True else ptPersonal.Refresh;
      if ptPersonal.FindKey([1010]) then
      begin
        ptPersonal.Edit;
        ptPersonalPASSW.AsString:='31540';
        ptPersonal.Post;
      end;
    end;

    ptPersonal.Active:=False;
    }
  end;

  if CommonSet.ZadanieDelaySek>0 then
  begin
    TimerZad.Interval:=CommonSet.ZadanieDelaySek*1000;
    TimerZad.Enabled:=True;
  end;

  TimerTestInv.Enabled:=True;


  StatusBar1.Panels[0].Text:=CommonSet.Ip+'('+GetCurrentUserName+')';
  if CommonSet.Single=1 then
  begin
    StatusBar1.Panels[2].Text:=its(CommonSet.Single);
    acAlgClass.Visible:=True;
  end else
  begin
    acAlgClass.Visible:=False;
  end;

  try
//    if DirectoryExists(CurDir+Person.Name)=False then createdir(CurDir+Person.Name);
    if DirectoryExists(CurDir+DelP(CommonSet.Ip))=False then createdir(CurDir+DelP(CommonSet.Ip));
  except
    showmessage('������ ����������� - '+CurDir+DelP(CommonSet.Ip));
  end;

  if CommonSet.Single=1 then
  begin
    AM1.ActionBars[0].Items[7].Visible:=False;
    CommonSet.RetOn:=1;
    acMV31.Enabled:=false
//    TAtionCients[7].Visible:=False;
  end else
  begin
    fileName:='Vozvrattoday.txt';            //p ��� ���������� �������� ���������
                                                            //�������� 0=��������� 1-���������
    if FileSearch(fileName,CurDir)=''                        //p  ������� ������
    then CommonSet.RetOn:=1 else CommonSet.RetOn:=0;
    if CommonSet.RetOn=1 then acMV31.Enabled:=false
    else acMV31.Enabled:=true;
//  SetDecimalSeparator(',');

  end;

  fmCards.Show;
end;

procedure TfmMainMCryst.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=132;
end;

procedure TfmMainMCryst.ExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMainMCryst.acCardExecute(Sender: TObject);
begin
//�������� ������
  fmCards.Show;
end;

procedure TfmMainMCryst.acCountryExecute(Sender: TObject);
begin
  fmCountry.Show;
end;

procedure TfmMainMCryst.acBrandsExecute(Sender: TObject);
begin
  fmBrands.Show;
end;

procedure TfmMainMCryst.acMakersExecute(Sender: TObject);
begin
  //������������� ����� ���������
  with dmMC do
  begin
    fmMakers.ViewMakers.BeginUpdate;
    quMakers.Active:=False;
    quMakers.Active:=True;
    fmMakers.ViewMakers.EndUpdate;

    fmMakers.Show;

  end;

{  with dmMt do
  with dmMc do
  begin
    try
      fmMakers:=tfmMakers.Create(Application);
      if taAlgClass.Active=False then taAlgClass.Active:=True;
      quAlgClass.Active:=False; quAlgClass.Active:=True;

      fmMakers.ShowModal;
    finally
      taAlgClass.Active:=False;
      quAlgClass.Active:=False;

      fmMakers.Release;
    end;
  end;
}
end;

procedure TfmMainMCryst.acDepartsExecute(Sender: TObject);
begin
  fmDeparts.Show;
end;

procedure TfmMainMCryst.acEUExecute(Sender: TObject);
begin
  fmEU.Show;
end;

procedure TfmMainMCryst.acDocsInExecute(Sender: TObject);
begin
// ������
  with dmMC do
  begin
    fmDocs1.LevelDocsIn.Visible:=True;
    fmDocs1.LevelCards.Visible:=False;

    fmDocs1.ViewDocsIn.BeginUpdate;
    try
      quTTNIn.Active:=False;
      quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quTTNIn.Active:=True;
    finally
      fmDocs1.ViewDocsIn.EndUpdate;
    end;
  end;
  fmDocs1.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs1.Show;
end;

procedure TfmMainMCryst.acCliExecute(Sender: TObject);
begin
  if fmClients.Showing then
  begin
    fmClients.Close;
    fmClients.Show;
  end
  else
  begin
    fmClients.Show;
    fmclients.cxTextEdit1.Text:='';
  //  fmclients.cxCheckBox1.Checked:=false;
  end;
  if (dmMC.quCli.Active=false) then
  begin
    with dmMC do
    begin
      fmClients.ViewCli.BeginUpdate;
      dsquCli.DataSet:=nil;
      try
        //quCli.Active:=False;
        quCli.SQL.Clear;
        quCli.SQL.Add('select * from "RVendor" v left join A_CLIVOZ cv on v.Status=cv.IDCB ');
        quCli.SQL.Add('left join A_TypeVoz t on cv.IdTypeVoz = t.ID ');
      {  quCli.SQL.Add('where Name not like ''��'+'%'' and Name not like ''� ��'+'%''');
        quCli.SQL.Add('and Status not like ''-'+'%''');  }
        quCli.SQL.Add('order by Name asc');
        quCli.Active:=True;
      finally
        dsquCli.DataSet:=quCli;
        fmClients.ViewCli.EndUpdate;
      end;
    end;
  end;
 // if dmMC.quCli.Active=False then dmMC.quCli.Active:=True;
  if dmMC.quIP.Active=False then dmMC.quIP.Active:=True;                                                   //����� P

end;

procedure TfmMainMCryst.acTaraExecute(Sender: TObject);
begin
  //����
  if dmMC.quTara.Active=False then dmMC.quTara.Active:=True; 
  fmTara.Show;
end;

procedure TfmMainMCryst.acOutExecute(Sender: TObject);
begin
// �������
  with dmMC do
  begin
    fmDocs2.ViewDocsOut.BeginUpdate;
    try
      quTTNOut.Active:=False;
      quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quTTNOut.Active:=True;
    finally
      fmDocs2.ViewDocsOut.EndUpdate;
    end;
  end;
  fmDocs2.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs2.Show;
end;

procedure TfmMainMCryst.acPereocExecute(Sender: TObject);
begin
//����������
  with dmMC do
  begin
    fmDocs3.ViewDocsAC.BeginUpdate;
    try
      quTTNAC.Active:=False;
      quTTNAC.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNAC.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quTTNAC.Active:=True;
    finally
      fmDocs3.ViewDocsAC.EndUpdate;
    end;
  end;
  fmDocs3.Caption:='��������� ���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs3.Show;
end;

procedure TfmMainMCryst.acVnExecute(Sender: TObject);
begin
  //�����������
  with dmMC do
  begin
    fmDocs4.ViewDocsVn.BeginUpdate;
    try
      quTTNVn.Active:=False;
      quTTNVn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNVn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quTTNVn.Active:=True;
    finally
      fmDocs4.ViewDocsVn.EndUpdate;
    end;
  end;
  fmDocs4.Caption:='��������� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs4.Show;
end;

procedure TfmMainMCryst.acInvExecute(Sender: TObject);
begin
//��������������
  with dmMC do
  begin
    fmDocs5.prButton(True);
    fmDocs5.ViewDocsInv.BeginUpdate;
    try
      quTTNInv.Active:=False;
      quTTNInv.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quTTNInv.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quTTNInv.Active:=True;
    finally
      fmDocs5.ViewDocsInv.EndUpdate;
    end;
  end;
  fmDocs5.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs5.Show;
end;

procedure TfmMainMCryst.acBufPriceExecute(Sender: TObject);
begin
  //����� ��������� ���
  with dmMC do
  begin
    fmBufPrice.ViewBufPr.BeginUpdate;
    quBufPr.Active:=False;
    quBufPr.ParamByName('DATEB').AsDate:=Trunc(fmBufPrice.cxDateEdit1.Date);
    quBufPr.ParamByName('DATEE').AsDate:=Trunc(fmBufPrice.cxDateEdit2.Date);
    quBufPr.ParamByName('IST').AsInteger:=0;
    quBufPr.Active:=True;

    fmBufPrice.ViewBufPr.EndUpdate;
    fmBufPrice.Show;
  end;
end;

procedure TfmMainMCryst.acCashListExecute(Sender: TObject);
begin
  //������ ����
  with dmMC do
  begin
    quCashList.Active:=False;
    quCashList.Active:=True;
    fmCashList.Show;
  end;
end;

procedure TfmMainMCryst.acTakeCashExecute(Sender: TObject);
begin
  //������ ������

  //ShellExecute(handle,'open',PChar(ExtractFilePath(Application.ExeName)+'temp.rtf'),nil,nil,SW_SHOWNORMAL);

  if FileExists(CommonSet.CloseCashPath) then
  begin
    ShellExecute(handle, 'open', PChar(CommonSet.CloseCashPath),'','', SW_SHOWNORMAL);
  end else
    showmessage('�������� ���� �� ���������. ���������� � ��������������.');
end;

procedure TfmMainMCryst.acCreateCashMoveExecute(Sender: TObject);
begin
  //��������������� ������
  with dmMc do
  begin
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      quGens.Active:=False;
      quGens.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quGens.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quGens.Active:=True;
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      fmGens.ShowModal;
    end;
  end;
end;

procedure TfmMainMCryst.acRepCashExecute(Sender: TObject);
begin
  // ����� �� ������
  with dmMc do
  begin
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
      fmCashRep.ViewCashRep.BeginUpdate;

      quCashRep.Active:=False;
      quCashRep.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quCashRep.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quCashRep.Active:=True;

      fmCashRep.ViewCashRep.EndUpdate;

      fmCashRep.Caption:='����� �� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      fmCashRep.Show;
    end;
  end;
end;

procedure TfmMainMCryst.acTovRepExecute(Sender: TObject);
begin
  //�������� �����
  with dmMc do
  begin
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

//      fmTORep:=TfmTORep.Create(Application);

      fmTORep.ViewTO.BeginUpdate;

      quReports.Active:=False;
      quReports.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quReports.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quReports.Active:=True;

      fmTORep.ViewTO.EndUpdate;

      fmTORep.Caption:='�������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      fmTORep.Show;
    end;
  end;
end;

procedure TfmMainMCryst.acObExecute(Sender: TObject);
Var rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb,rQe:Real;
    rQIn1,rQOut1,rQReal1,rQvnIn1,rQVnOut1,rQInv1,rQb1:Real;
    iItem,iStep,iC,iEdizm,iGr,iSGr,iCat,DayN:INteger;
    sName,sTop,sNov:String;
    bCat,bCatD,bTop,bTopD:INteger;
begin
  if fmclients.Showing then fmclients.Close;
  //�������� ������� - ��������� ���������
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;

//  fmPeriod.cxLookupComboBox4.Visible:=False; fmPeriod.cxCheckBox4.Visible:=False; fmPeriod.cxLabel6.Visible:=False;
//  fmPeriod.cxLookupComboBox5.Visible:=False; fmPeriod.cxCheckBox5.Visible:=False; fmPeriod.cxLabel7.Visible:=False;

  fmPeriod.cxCheckBox7.Checked:=True;
  fmPeriod.cxLabel9.Visible:=True;
  fmPeriod.cxCheckBox7.Visible:=True;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);
    //�������
    with dmMc do
    with dmMt do
    begin
      fmObVed.PBar1.Position:=0;
      fmObVed.Caption:='��������� ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd)+'  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmObVed.Tag:=0;
      fmObVed.Show;
      fmObVed.PBar1.Visible:=True;
      with fmObVed do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewObVed.BeginUpdate;
        CloseTe(fmObVed.teObVed);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;


        fmObVed.PBar1.Position:=10; delay(10);

        quRepOB.Active:=False;
        quRepOB.SQL.Clear;
        quRepOB.SQL.Add('select');
        quRepOB.SQL.Add('cg.Card,');
        quRepOB.SQL.Add('cg.Item,');
        quRepOB.SQL.Add('gd.Name,');
        quRepOB.SQL.Add('gd.EdIzm,');
        quRepOB.SQL.Add('cg.Depart,');
        quRepOB.SQL.Add('de.Name as NameDep,');
        quRepOB.SQL.Add('gd.GoodsGroupID,');
        quRepOB.SQL.Add('gd.SubGroupID,');
        quRepOB.SQL.Add('gd.V01,');
        quRepOB.SQL.Add('gd.V02,');
        quRepOB.SQL.Add('gd.V03,');
        quRepOB.SQL.Add('gd.V04,');
        quRepOB.SQL.Add('gd.V05,');
        quRepOB.SQL.Add('gd.Cena');
        quRepOB.SQL.Add('from "CardGoods" cg');
        quRepOB.SQL.Add('left join "Goods" gd on gd.ID=cg.Item');
        quRepOB.SQL.Add('left join "Depart" de on de.ID=cg.Depart');
        quRepOB.SQL.Add('where cg.Item>0 ');

        if ObPar.iStatus=1 then quRepOB.SQL.Add('and gd.Status<100 '); //������ ������������

//        quRepOB.SQL.Add('where cg.Item=14452 and gd.Status<100 ');

        if ObPar.iPost>0 then
        begin
{          quRepOB.SQL.Add('and cg.Item in (select CodeTovar from "TTNInLn"');
          quRepOB.SQL.Add('where DateInvoice>='''+ds(Date-150)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepOB.SQL.Add('group by CodeTovar');
          quRepOB.SQL.Add(')');
}
          quRepOB.SQL.Add('and cg.Item in (select GoodsID from "GdsClient"');
          quRepOB.SQL.Add('where ClientObjTypeID='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepOB.SQL.Add(')');

        end;

        if ObPar.iDepart>0 then
          quRepOB.SQL.Add('and cg.Depart in ('+its(ObPar.iDepart)+')');

        if ObPar.iSGroup>0 then quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepOB.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepOB.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepOB.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepOB.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepOB.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;

        quRepOB.SQL.Add('order by cg.Item,cg.Depart');

        quRepOB.Active:=True;

        fmObVed.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

        rQIn1:=0; rQOut1:=0; rQReal1:=0; rQvnIn1:=0; rQVnOut1:=0; rQInv1:=0; rQb1:=0;

        quRepOb.First;
        iItem:=0;
        if quRepOB.RecordCount>0 then
        begin
          iItem:=quRepOBITEM.AsInteger;
        end;

        iEdizm:=1; iGr:=0; iSGr:=0; sName:=''; iCat:=0; 

        iStep:=quRepOB.RecordCount div 60;
        if iStep=0 then iStep:=quRepOB.RecordCount;
        iC:=0;

        while not quRepOB.Eof do
        begin

          if iItem<>quRepOBITEM.AsInteger then
          begin
      //      rQe:=prFindRemn3(iItem,Trunc(CommonSet.DateEnd),ObPar.iDepart);
            rQe:=rQb1+rQIn1+rQvnIn1+rQInv1-rQVnOut1-rQReal1-rQOut1;
//            if (rQb1<>0)or(rQe<>0)or(rQIn1<>0)or(rQvnIn1<>0)or(rQInv1<>0)or(rQVnOut1<>0)or(rQReal1<>0)or(rQOut1<>0) then //���-�� ����
//            begin

            //���� � ����� ������

            teObVed.Append;
            teObVedIdCode1.AsInteger:=iItem;
            teObVedNameC.AsString:=sName;
            teObVediM.AsInteger:=iEdizm;
            teObVedIdGroup.AsInteger:=iGr;
            teObVedIdSGroup.AsInteger:=iSGr;
            teObVedQBeg.AsFloat:=rQb1;
            teObVedQIn.AsFloat:=rQIn1;
            teObVedQVnIn.AsFloat:=rQvnIn1;
            teObVedQVnOut.AsFloat:=rQVnOut1;
            teObVedQReal.AsFloat:=rQReal1;
            teObVedQOut.AsFloat:=rQOut1;
            teObVedQRes.AsFloat:=rQe;
            teObVedQInv.AsFloat:=rQInv1;

            teObVedNameGr1.AsString:='';
            teObVedNameGr2.AsString:='';
            teObVedNameGr3.AsString:='';

            if teClass.Locate('Id',iGr,[]) then
            begin
              iGr:=teClassIdParent.AsInteger;
              teObVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                iGr:=teClassIdParent.AsInteger;
              end;
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
            end;

            teObVediCat.AsInteger:=iCat;
            teObVedsTop.AsString:=sTop;
            teObVedsNov.AsString:=sNov;
            if sNov>' ' then
            begin
              DayN:=RoundEx(Date-prFindTFirstDate(iItem));
              if DayN>91 then DayN:=0;
              teObVedDayN.AsInteger:=DayN;
            end;


            bCat:=prFindBAkciya1(quRepOBItem.AsInteger,Trunc(Date+1),bCatD,bTopD,bTop);
            teObVedBStatus.AsInteger:=bCat;
            if bCatD>1000 then teObVedBStatusD.AsDateTime:=bCatD
            else teObVedBStatusD.AsString:='';

            teObVedBTop.AsInteger:=bTop;

            if bTopD>1000 then teObVedBTopD.AsDateTime:=bTopD
            else teObVedBTopD.AsString:='';

            teObVedsCat.AsString:=fSCat(iCat);

            if bCat<=1 then teObVedsBStatus.AsString:='' else teObVedsBStatus.AsString:=fSCat(bCat);
            if bTop>0 then teObVedsBTop.AsString:='BT' else teObVedsBTop.AsString:='';


            teObVed.Post;

        //    end; //���� � ����� ������ - ������� ���������

            iItem:=quRepOBITEM.AsInteger;

            rQIn1:=0; rQOut1:=0; rQReal1:=0; rQvnIn1:=0; rQVnOut1:=0; rQInv1:=0; rQb1:=0;

          end;
{         if quRepOBDOC_TYPE.AsInteger=1 then rQIn:=rQIn+quRepOBQuant.AsFloat;
          if quRepOBDOC_TYPE.AsInteger=2 then rQOut:=rQOut+(-1)*quRepOBQuant.AsFloat;
          if quRepOBDOC_TYPE.AsInteger=3 then rQVnIn:=rQVnIn+quRepOBQuant.AsFloat;
          if quRepOBDOC_TYPE.AsInteger=4 then rQVnOut:=rQVnOut+(-1)*quRepOBQuant.AsFloat;
          if quRepOBDOC_TYPE.AsInteger=7 then rQReal:=rQReal+(-1)*quRepOBQuant.AsFloat;
          if quRepOBDOC_TYPE.AsInteger=20 then rQInv:=rQInv+quRepOBQuant.AsFloat;
}
          prCalcTMove(quRepOBCARD.AsInteger,rQIn,rQOut,rQReal,rQvnIn,rQVnOut,rQInv,rQb);
          rQIn1:=rQIn1+rQIn;
          rQOut1:=rQOut1+rQOut;
          rQVnIn1:=rQVnIn1+rQVnIn;
          rQVnOut1:=rQVnOut1+rQVnOut;
          rQReal1:=rQReal1+rQReal;
          rQInv1:=rQInv1+rQInv;
          rQb1:=rQb1+rQb;

          iEdizm:=quRepOBEdIzm.AsInteger;
          iGr:=quRepOBGoodsGroupID.AsInteger;
          iSGr:=quRepOBSubGroupID.AsInteger;
          sName:=quRepOBName.AsString;
          iCat:=quRepOBV02.AsInteger;

          if quRepOBV04.AsInteger=0 then sTop:=' ' else sTop:='T';
          if quRepOBV05.AsInteger=0 then sNov:=' ' else
          begin
            sNov:='N';
//            DayN:=RoundEx(Date-prFindTFirstDate(quRepRID.AsInteger));
//            if DayN>91 then DayN:=0;
          end;

          quRepOb.Next; inc(iC);

          if (iC mod iStep = 0) then
          begin
            fmObVed.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;
        if iItem>0 then
        begin
          rQe:=rQb1+rQIn1+rQvnIn1+rQInv1-rQVnOut1-rQReal1-rQOut1;
//          if (rQb1<>0)or(rQe<>0)or(rQIn1<>0)or(rQvnIn1<>0)or(rQInv1<>0)or(rQVnOut1<>0)or(rQReal1<>0)or(rQOut1<>0) then //���-�� ����
//          begin
          //���� � ����� ������ - ������� ���������
            teObVed.Append;
            teObVedIdCode1.AsInteger:=iItem;
            teObVedNameC.AsString:=sName;
            teObVediM.AsInteger:=iEdizm;
            teObVedIdGroup.AsInteger:=iGr;
            teObVedIdSGroup.AsInteger:=iSGr;
            teObVedQBeg.AsFloat:=rQb1;
            teObVedQIn.AsFloat:=rQIn1;
            teObVedQVnIn.AsFloat:=rQvnIn1;
            teObVedQVnOut.AsFloat:=rQVnOut1;
            teObVedQReal.AsFloat:=rQReal1;
            teObVedQOut.AsFloat:=rQOut1;
            teObVedQRes.AsFloat:=rQe;
            teObVedQInv.AsFloat:=rQInv1;
//            teObVedNameGr1.AsString:=prFindTNameMGr(iGr);
//            teObVedNameGr2.AsString:=prFindTNameGr(iSGr,iGr1);

            teObVedNameGr1.AsString:='';
            teObVedNameGr2.AsString:='';
            teObVedNameGr3.AsString:='';

            if teClass.Locate('Id',iGr,[]) then
            begin
              iGr:=teClassIdParent.AsInteger;
              teObVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;

            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                iGr:=teClassIdParent.AsInteger;
              end;
            end;
            
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
            end;
            teObVediCat.AsInteger:=iCat;

            teObVedsTop.AsString:=sTop;
            teObVedsNov.AsString:=sNov;
            if sNov>' ' then
            begin
              DayN:=RoundEx(Date-prFindTFirstDate(iItem));
              if DayN>91 then DayN:=0;
              teObVedDayN.AsInteger:=DayN;
            end;

            bCat:=prFindBAkciya1(iItem,Trunc(Date+1),bCatD,bTopD,bTop);
            teObVedBStatus.AsInteger:=bCat;
            if bCatD>1000 then teObVedBStatusD.AsDateTime:=bCatD
            else teObVedBStatusD.AsString:='';

            teObVedBTop.AsInteger:=bTop;

            if bTopD>1000 then teObVedBTopD.AsDateTime:=bTopD
            else teObVedBTopD.AsString:='';

            teObVedsCat.AsString:=fSCat(iCat);

            if bCat<=1 then teObVedsBStatus.AsString:='' else teObVedsBStatus.AsString:=fSCat(bCat);
            if bTop>0 then teObVedsBTop.AsString:='BT' else teObVedsBTop.AsString:='';

            teObVed.Post;
//          end;
          //���� � ����� ������ - ������� ���������

        end;

        fmObVed.PBar1.Position:=100; delay(100);

        quRepOB.Active:=False;
        teClass.Active:=False;
        ViewObVed.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmObVed.PBar1.Visible:=False;
      end;
    end;
    fmPeriod.Release;
  end else
    fmPeriod.Release;
end;

procedure TfmMainMCryst.acRemnDayExecute(Sender: TObject);
//������� �������� �������
Var rQr:Real;
    iStep,iC,iGr:INteger;
    StrPr:String;
    rPr0:Real;
begin
  if fmclients.Showing then fmclients.Close;
  fmPeriodR:=TfmPeriodR.Create(Application);
  fmPeriodR.cxRadioGroup2.Visible:=True;
  fmPeriodR.ShowModal;
  if fmPeriodR.ModalResult=mrOk then
  begin
    //�������
    with dmMc do
    with dmMt do
    begin
      fmRVed.PBar1.Position:=0;
      if ObPar.iPrice=1 then StrPr:=' � ��������� �����.' else StrPr:=' � ���������� ����� (c ���).';

      fmRVed.Caption:='�������  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group + StrPr;
      
      fmRVed.Show;
      fmRVed.PBar1.Visible:=True;
      with fmRVed do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewRVed.BeginUpdate;
        CloseTe(fmRVed.teRVed);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;

        fmRVed.PBar1.Position:=10; delay(10);

        quRepR.Active:=False;
        quRepR.SQL.Clear;
        quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');
        quRepR.SQL.Add('from "Goods" gd');
        quRepR.SQL.Add('where  Status<100');

        if ObPar.iPost>0 then
        begin
{
          quRepR.SQL.Add('and gd.ID in (select CodeTovar from "TTNInLn"');
          quRepR.SQL.Add('where DateInvoice>='''+ds(Date-150)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepR.SQL.Add('group by CodeTovar');
          quRepR.SQL.Add(')');
}
          quRepR.SQL.Add('and gd.ID in (select GoodsID from "GdsClient"');
          quRepR.SQL.Add('where ClientObjTypeID='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepR.SQL.Add(')');

        end;

        if ObPar.iSGroup>0 then quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepR.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepR.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepR.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepR.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepR.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;

        quRepR.Active:=True;

        fmRVed.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

        quRepR.First;

        iStep:=quRepR.RecordCount div 60;
        if iStep=0 then iStep:=quRepR.RecordCount;
        iC:=0;

        while not quRepR.Eof do
        begin
          rQr:=prFindTabRemn(quRepRID.AsInteger);

          teRVed.Append;
          teRVedIdCode1.AsInteger:=quRepRID.AsInteger;
          teRVedNameC.AsString:=quRepRName.AsString;
          teRVediM.AsInteger:=quRepREdIzm.AsInteger;
          teRVedQR.AsFloat:=rQr;
          teRVedIdGroup.AsInteger:=quRepRGoodsGroupID.AsInteger;
          teRVedIdSGroup.AsInteger:=0;
          teRVedNameGr1.AsString:='';
          teRVedNameGr2.AsString:='';
          teRVedNameGr3.AsString:='';
          teRVediBrand.AsInteger:=quRepRV01.AsInteger;
          teRVediCat.AsInteger:=quRepRV02.AsInteger;

          //���� ���� ���������� �������

          if ObPar.iPrice=1 then teRVedPrice.AsFloat:=quRepRCena.AsFloat
          else teRVedPrice.AsFloat:=prFLPriceDateT(quRepRID.AsInteger,Trunc(Date),rPr0);

          iGr:=quRepRGoodsGroupID.AsInteger;

          if teClass.Locate('Id',iGr,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teRVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;

          if quRepRV04.AsInteger=0 then teRVedsTop.AsString:=' ' else teRVedsTop.AsString:='T';
          if quRepRV05.AsInteger=0 then teRVedsNov.AsString:=' ' else
          begin
            teRVedsNov.AsString:='N';
            teRVedDayN.AsInteger:=RoundEx(Date-prFindTFirstDate(quRepRID.AsInteger));
            if teRVedDayN.AsInteger>91 then teRVedDayN.AsInteger:=0;
          end;

          teRVed.Post;

          quRepR.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmRVed.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        quRepR.Active:=True;

        fmRVed.PBar1.Position:=100; delay(100);

        quRepR.Active:=False;
        ViewRVed.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmRVed.PBar1.Visible:=False;
      end;
    end;
    fmPeriodR.Release;
  end else
    fmPeriodR.Release;
end;

procedure TfmMainMCryst.acTermExecute(Sender: TObject);
begin
//������ � ���������� ����� ������
  fmMainLoader.Show;
end;

procedure TfmMainMCryst.acLabesExecute(Sender: TObject);
Var iID:INteger;
begin
//�������
  if not CanDo('prEditLabels') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  begin
    fmLabels:=tfmLabels.Create(Application);
    with dmMC do
    with fmLabels do
    begin
      ViewLabels.BeginUpdate;
      CloseTe(taL);

      quLab1.Active:=False;
      quLab1.Active:=True;
      quLab1.First;
      while not quLab1.Eof do
      begin
        taL.Append;
        taLID.AsInteger:=quLab1ID.AsInteger;
        taLNAME.AsString:=quLab1NAME.AsString;
        taLFILEN.AsString:=quLab1FILEN.AsString;
        taL.Post;

        quLab1.Next;
      end;
      ViewLabels.EndUpdate;
      fmLabels.ShowModal;
      if fmLabels.ModalResult=mrOk then
      begin
        //��������
        ViewLabels.BeginUpdate;
        taL.First;
        while not taL.Eof do
        begin
          if quLab1.Locate('ID',taLID.AsInteger,[]) then
          begin //���������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "A_LABELS" Set ');
            quE.SQL.Add('NAME='''+taLNAME.AsString+''',');
            quE.SQL.Add('FILEN='''+taLFILEN.AsString+'''');
            quE.SQL.Add('where ID='+its(taLID.AsInteger));
            quE.ExecSQL;
          end else
          begin  //����������
            quA.Active:=False;
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "A_LABELS" values (');
            quA.SQL.Add(its(taLID.AsInteger)+',');
            quA.SQL.Add(''''+taLNAME.AsString+''',');
            quA.SQL.Add(''''+taLFILEN.AsString+'''');
            quA.SQL.Add(')');
            quA.ExecSQL;
          end;

          taL.Next;
        end;
        ViewLabels.EndUpdate;
      end;
      taL.Active:=False;
      quLab1.Active:=False;
      iId:=quLabsID.AsInteger;
      quLabs.Active:=False;
      quLabs.Active:=True;
      if quLabs.Locate('ID',iId,[])=False then
      begin
        quLabs.First;
        cxLookupComboBox1.EditValue:=quLabsID.AsInteger;
        cxLookupComboBox1.Text:=quLabsNAME.AsString;
      end;
    end;
    fmLabels.Release;
  end;
end;

procedure TfmMainMCryst.acTermoExecute(Sender: TObject);
begin
// ��������� �������������
  fmTermoPr:=TfmTermoPr.Create(Application);
  fmTermoPr.cxTextEdit1.Text:=CommonSet.TPrintN;
  fmTermoPr.cxRadioGroup1.ItemIndex:=CommonSet.TPrintCode-1;
  fmTermoPr.ShowModal;
  fmTermoPr.Release;
end;

procedure TfmMainMCryst.acScaleSprExecute(Sender: TObject);
Var bMT:Boolean;
    f:TIniFile;
    ft:TextFile;
    StrIp:String;

begin
  //���������� �����
  if not CanDo('prEditScale') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  begin
    fmScaleSpr:=tfmScaleSpr.Create(Application);
    with dmMC do
    with fmScaleSpr do
    begin
      ViewSc.BeginUpdate;
      CloseTe(taSc);

      quScSpr.Active:=False;
      quScSpr.Active:=True;
      quScSpr.First;
      while not quScSpr.Eof do
      begin
        taSc.Append;
        taScNumber.AsString:=quScSprNumber.AsString;
        taScModel.AsString:=quScSprModel.AsString;
        taScName.AsString:=quScSprName.AsString;
        taScPluCount.AsInteger:=quScSprPLUCount.AsInteger;
        taScIp1.AsString:= IntToIp(quScSprRezerv1.AsInteger);
        taScIp2.AsString:= IntToIp(quScSprRezerv2.AsInteger);
        taScIp3.AsString:= IntToIp(quScSprRezerv3.AsInteger);
        taScIp4.AsString:= IntToIp(quScSprRezerv4.AsInteger);
        taScIp5.AsString:= IntToIp(quScSprRezerv5.AsInteger);
        taSc.Post;

        quScSpr.Next;
      end;
      ViewSc.EndUpdate;
      fmScaleSpr.ShowModal;
      if fmScaleSpr.ModalResult=mrOk then
      begin
        //��������
        ViewSc.BeginUpdate;
        taSc.First;
        while not taSc.Eof do
        begin
          if quScSpr.Locate('Number',taScNumber.AsString,[]) then
          begin //���������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "SprScale" Set ');
            quE.SQL.Add('Model='''+taScModel.AsString+''',');
            quE.SQL.Add('Name='''+taScName.AsString+''',');
            quE.SQL.Add('PLUCount='+its(taScPluCount.AsInteger)+',');
            quE.SQL.Add('Rezerv1='+its(StrToIp(taScIp1.AsString))+',');
            quE.SQL.Add('Rezerv2='+its(StrToIp(taScIp2.AsString))+',');
            quE.SQL.Add('Rezerv3='+its(StrToIp(taScIp3.AsString))+',');
            quE.SQL.Add('Rezerv4='+its(StrToIp(taScIp4.AsString))+',');
            quE.SQL.Add('Rezerv5='+its(StrToIp(taScIp5.AsString)));
            quE.SQL.Add('where Number='''+taScNumber.AsString+'''');
            quE.ExecSQL;
          end else
          begin  //����������
            quA.Active:=False;
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "SprScale" values (');
            quA.SQL.Add(''''+taScNumber.AsString+''',');
            quA.SQL.Add(''''+taScModel.AsString+''',');
            quA.SQL.Add(''''+taScName.AsString+''',');
            quA.SQL.Add('0,');
            quA.SQL.Add(its(taScPluCount.AsInteger)+',');
            quA.SQL.Add(its(StrToIp(taScIp1.AsString))+',');
            quA.SQL.Add(its(StrToIp(taScIp2.AsString))+',');
            quA.SQL.Add(its(StrToIp(taScIp3.AsString))+',');
            quA.SQL.Add(its(StrToIp(taScIp4.AsString))+',');
            quA.SQL.Add(its(StrToIp(taScIp5.AsString))+',');
            quA.SQL.Add('0,');
            quA.SQL.Add('0,');
            quA.SQL.Add('0,');
            quA.SQL.Add('0,');
            quA.SQL.Add('0');
            quA.SQL.Add(')');
            quA.ExecSQL;
          end;
          taSc.Next;
        end;
        ViewSc.EndUpdate;

        with dmMT do
        begin
          bMT:=False;
          ptScale.Active:=False;
          ptScale.Active:=True;
          ptScale.First;
          while not ptScale.Eof do
          begin
            if pos('MT',ptScaleModel.AsString)>0 then begin bMT:=True; break; end;
            ptScale.Next;
          end;

          if bMT then  //���� �������� - ����� �������� �������� ��������
          begin
            if FileExists(CurDir+'SCALEADDRESS.INI') then //���� ���
            begin
              assignfile(ft,CurDir+'SCALEADDRESS.INI');
              reset(ft);
              closefile(ft);
              erase(ft);
            end;

            delay(30);

            f:=TIniFile.create(CurDir+'SCALEADDRESS.INI');

            f.WriteInteger('CONFIG','MEDIA',1);
            f.WriteInteger('CONFIG','COMPORT',2);
            f.WriteInteger('CONFIG','THREADNUM',4);

            ptScale.First;
            while not ptScale.Eof do
            begin
              if pos('MT',ptScaleModel.AsString)>0 then
              begin
                StrIp := IntToIp(ptScaleRezerv1.AsInteger);
                if (StrIp>'')and(StrIp<>'000') then
                begin
                  StrIp:=CommonSet.sMask+StrIp;
                  while pos('0',StrIp)=1 do delete(StrIp,1,1);

//                  f.WriteString(its(ptScaleNumber.AsInteger),'NAME',ptScaleName.AsString);
                  f.WriteString(its(ptScaleNumber.AsInteger),'NAME','');
                  f.WriteString(its(ptScaleNumber.AsInteger),'IP',StrIp);
                  f.WriteInteger(its(ptScaleNumber.AsInteger),'PORT',3001);
                end;

                StrIp := IntToIp(ptScaleRezerv2.AsInteger);
                  while pos('0',StrIp)=1 do delete(StrIp,1,1);
                if (StrIp>'')and(StrIp<>'000') then
                begin
                  StrIp:=CommonSet.sMask+StrIp;
                  f.WriteString(its(ptScaleNumber.AsInteger*10+2),'NAME',ptScaleName.AsString);
                  f.WriteString(its(ptScaleNumber.AsInteger*10+2),'IP',StrIp);
                  f.WriteInteger(its(ptScaleNumber.AsInteger*10+2),'PORT',3001);
                end;

                StrIp := IntToIp(ptScaleRezerv3.AsInteger);
                while pos('0',StrIp)=1 do delete(StrIp,1,1);
                if (StrIp>'')and(StrIp<>'000') then
                begin
                  StrIp:=CommonSet.sMask+StrIp;
                  f.WriteString(its(ptScaleNumber.AsInteger*10+3),'NAME',ptScaleName.AsString);
                  f.WriteString(its(ptScaleNumber.AsInteger*10+3),'IP',StrIp);
                  f.WriteInteger(its(ptScaleNumber.AsInteger*10+3),'PORT',3001);
                end;

                StrIp := IntToIp(ptScaleRezerv4.AsInteger);
                while pos('0',StrIp)=1 do delete(StrIp,1,1);
                if (StrIp>'')and(StrIp<>'000') then
                begin
                  StrIp:=CommonSet.sMask+StrIp;
                  f.WriteString(its(ptScaleNumber.AsInteger*10+4),'NAME',ptScaleName.AsString);
                  f.WriteString(its(ptScaleNumber.AsInteger*10+4),'IP',StrIp);
                  f.WriteInteger(its(ptScaleNumber.AsInteger*10+4),'PORT',3001);
                end;

                StrIp := IntToIp(ptScaleRezerv5.AsInteger);
                while pos('0',StrIp)=1 do delete(StrIp,1,1);
                if (StrIp>'')and(StrIp<>'000') then
                begin
                  StrIp:=CommonSet.sMask+StrIp;
                  f.WriteString(its(ptScaleNumber.AsInteger*10+5),'NAME',ptScaleName.AsString);
                  f.WriteString(its(ptScaleNumber.AsInteger*10+5),'IP',StrIp);
                  f.WriteInteger(its(ptScaleNumber.AsInteger*10+5),'PORT',3001);
                end;

              end;
              ptScale.Next;
            end;
            f.Free;
          end;
          ptScale.Active:=False;
        end;

      end;
      taSc.Active:=False;
      quScSpr.Active:=False;

    end;
    fmScaleSpr.Release;
  end;
end;

procedure TfmMainMCryst.acScaleLoadExecute(Sender: TObject);
begin
  if fmScale=nil then
  begin
    SetDecimalSeparator('.');
    delay(100);
    fmScale:=TFmScale.Create(Application);
    delay(100);
    SetDecimalSeparator(',');
  end;
  fmScale.Show;
end;

procedure TfmMainMCryst.acRecalcRemnExecute(Sender: TObject);
Var iC:Integer;
    rQ:Real;
begin
  with dmMc do
  with dmMT do
  begin
    if MessageDlg('�������� �������� �������� �� ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin

      StatusBar1.Panels[0].Text:='������������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      quCds.Active:=False;
      quCds.Active:=True;

      StatusBar1.Panels[0].Text:='�������'; StatusBar1.Panels[1].Text:='0'; delay(10);

      StatusBar1.Panels[0].Text:='0'; StatusBar1.Panels[1].Text:=INtToStr(quCds.RecordCount); delay(10);
      iC:=0;
      quCds.Last;
      while not quCDS.Bof do
      begin
        rQ:=prFindTabRemn(quCdsID.AsInteger);

        if abs(rQ-quCdsReserv1.AsFloat)>0.1 then
        begin //���� ��������
          prSetR(quCdsID.AsInteger,rQ);
{          quE.Active:=False;
          quE.SQL.Clear;

          quE.SQL.Add('Update Goods Set ');
          quE.SQL.Add('Reserv1='+fs(rQ));
          quE.SQL.Add('where ID='+IntToStr(quCdsID.AsInteger));
          quE.ExecSQL;}
        end;

        quCds.Prior; inc(iC); StatusBar1.Panels[0].Text:=IntToStr(iC); delay(10);
      end;
      quCds.Active:=False;
    end;
  end;
end;

procedure TfmMainMCryst.acCalcCashByDepExecute(Sender: TObject);
Var StrWk,StrTa:String;
    DateB,DateE,iCurD:Integer;
    iC:INteger;
    CurD:TDateTime;
    vRest,vRestOut:Real;
    par:Variant;
    iPriv,iDep,iPostDep,i,iMainDep:INteger;
    iDeps:array[1..20] of integer;
    bInv:Boolean;
    iCassir:Integer;

function fTestDep(iDep:INteger):Integer;
Var k:INteger;
    bDep:Boolean;
begin
  Result:=5;
  for k:=1 to 20 do if iDeps[k]>0 then Result:=iDeps[k]; //�� ��������� ���������
  bDep:=False;
  for k:=1 to 20 do
  begin
    if iDeps[k]=iDep then bDep:=True;
  end;
  if bDep then Result:=iDep;
end;

begin
  //�������� ���������� �� �������
  with dmMc do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

      bInv:=False;
      ptDep.First;
      while not ptDep.Eof do
      begin
        if fTestInv(ptDepID.AsInteger,DateB)=False then
        begin
          bInv:=True;
          Break;
        end;
        ptDep.Next;
      end;

      if (DateB<=prOpenDate)or(bInv=True) then
      begin
        if bInv then ShowMessage('������ ������.(��������������)')
        else ShowMessage('������ ������.');
      end else
      begin

      if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
      else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      try
        try
          quPriorD.Active:=False;
          quPriorD.Active:=True;

          if ptDep.Active=False then ptDep.Active:=True;

          iMainDep:=0;
          ptDep.First;
          while not ptDep.Eof do
          begin
            if ptDepV02.AsInteger>iMainDep then iMainDep:=ptDepID.AsInteger;
            ptDep.Next;
          end;

          for i:=1 to 20 do iDeps[i]:=0;
          i:=1;
          quPriorD.First;
          while (quPriorD.Eof=False)and (i<=20) do
          begin
            iDeps[i]:=quPriorDID.AsInteger;
            quPriorD.Next; inc(i);
          end;
          quPriorD.First;


          par := VarArrayCreate([0,1], varInteger);

          fmSt.Memo1.Lines.Add(fmt+'������ ��������� ('+INtToStr(iMainDep)+')'+StrWk);
          for iCurD:=DateB to DateE do
          begin
            CloseTe(taRemove);

            iC:=0;

            fmSt.Memo1.Lines.Add(fmt+'');
            fmSt.Memo1.Lines.Add(fmt+'  ��������� ����  '+ FormatDateTime('dd.mm.yyyy',iCurD)); delay(10);

            ptCQ.Active:=False;
            ptCQ.DatabaseName:=Btr1Path;
            ptCQ.TableName:='cq'+FormatDateTime('yyyymm',iCurD);
            ptCQ.Active:=True;
            ptCQ.CancelRange;
            CurD:=iCurD;
            ptCQ.SetRange([CurD],[CurD]);
            ptCQ.First;
            while not ptCQ.Eof do
            begin
              iDep:=ptCQGrCode.AsInteger;
              if ptCQCk_CurAbr.AsInteger>0 then //����� ������ ��� �� �����
              begin
                iDep:=ptCQCk_CurAbr.AsInteger;
                ptCQ.Edit;
                ptCQGrCode.AsInteger:=iDep;
                ptCQCk_CurAbr.AsInteger:=0;
                ptCQ.post;
              end;

{             if ptCQGrCode.AsInteger=4 then //������� ������
              begin
                ptCQ.Edit;
                ptCQGrCode.AsInteger:=5;
                ptCQCk_CurAbr.AsInteger:=0;
                ptCQ.post;
                iDep:=5;
              end;
}
//              if (ptCQOperation.AsString='P')and(iDep=iMainDep) then //������������


              if (ptCQOperation.AsString='P')and(iDep=iMainDep) then //������������ ������ (130 - ��� �������)
              begin
                  //����������� ��� ������
                iPriv:=0;
                quPriorD.First;
                while (quPriorD.Eof=False)and(iPriv=0) do
                begin
                  //������� �� ������ ��� � �� ����.
                  vRest:=prFindTRemnDepD(ptCQCode.AsInteger,quPriorDID.AsInteger,iCurD);
                  if vRest>0 then
                  begin //��������� ������� � ������ ������
                    vRestOut:=0;
                    par[0]:=ptCQCode.AsInteger;  //��� ������
                    par[1]:=ptCQGrCode.AsInteger; //�����
                    if taRemove.Locate('Articul;Depart',par,[]) then  vRestOut:=taRemoveQuant.AsFloat;
                    vRest:=vRest-vRestOut-ptCQQuant.AsFloat;
                    if vRest>=0 then
                    begin
                      iPriv:=quPriorDID.AsInteger;
                      break;
                    end;
                  end;
                  quPriorD.Next;
                end;
                if iPriv>0 then //������ ����� � ������������ ���������
                begin
                  iDep:=ptCQGrCode.AsInteger;
                  ptCQ.Edit;
                  ptCQGrCode.AsInteger:=iPriv;
                  ptCQCk_CurAbr.AsInteger:=iDep;
                  ptCQ.post;

                  par[0]:=ptCQCode.AsInteger;  //��� ������
                  par[1]:=iPriv; //�����
                  if taRemove.Locate('Articul;Depart',par,[]) then
                  begin
                    taRemove.Edit;
                    taRemoveQuant.AsFloat:=taRemoveQuant.AsFloat+ptCQQuant.AsFloat;
                    taRemoveRes.AsInteger:=iPriv;
                    taRemove.Post;
                  end else
                  begin
                    taRemove.Append;
                    taRemoveArticul.AsInteger:=ptCQCode.AsInteger;
                    taRemoveDepart.AsInteger:=iPriv;
                    taRemoveQuant.AsFloat:=ptCQQuant.AsFloat;
                    taRemoveRes.AsInteger:=iPriv;
                    taRemove.Post;
                  end;
                end else //�� ������ ����� � ������������ ���������
                begin
                  iPostDep:=prFindTLastDep(ptCQCode.AsInteger);
                  if iPostDep>0 then
                  begin
                    iPostDep:=fTestDep(iPostDep); //��������� ��������� ����� - ����� ������������ ��� ���
                    if iPostDep<>ptCQGrCode.AsInteger then
                    begin
                      iDep:=ptCQGrCode.AsInteger;
                      ptCQ.Edit;
                      ptCQGrCode.AsInteger:=iPostDep;
                      ptCQCk_CurAbr.AsInteger:=iDep;
                      ptCQ.post;
                    end;
                  end;
                end;
              end;
              if (ptCQOperation.AsString='R')and(ptCQGrCode.AsInteger=iMainDep) then
              begin
                iPostDep:=prFindTLastDep(ptCQCode.AsInteger);
                if iPostDep>0 then
                begin
                  iPostDep:=fTestDep(iPostDep); //��������� ��������� ����� - ����� ������������ ��� ���
                  if iPostDep<>ptCQGrCode.AsInteger then
                  begin
                    iDep:=ptCQGrCode.AsInteger;
                    ptCQ.Edit;
                    ptCQGrCode.AsInteger:=iPostDep;
                    ptCQCk_CurAbr.AsInteger:=iDep;
                    ptCQ.post;
                  end;
                end;
              end;

              ptCQ.Next;  inc(iC); if iC mod 100=0 then
              begin
                fmSt.Memo1.Lines.Add(fmt+'  ..'+INtToStr(iC));
                delay(10);
              end;
            end;

            ptCQ.Active:=False;
            ptCQ.DatabaseName:=Btr1Path;

            fmSt.Memo1.Lines.Add(fmt+'  ��'); delay(10);
            taRemove.Active:=False;

            fmSt.Memo1.Lines.Add(fmt+'  �������� ���������� �� �������.'); delay(10);

            StrTa:='cq'+formatDateTime('yyyymm',iCurD);

            quD.SQL.Clear;
            quD.SQL.Add('delete from "SaleByDepart" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
            quD.ExecSQL;
            quD.SQL.Clear;
            quD.SQL.Add('delete from "SaleByDepartBN" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
            quD.ExecSQL;

            quCq.Active:=False;
            quCq.SQL.Clear;
            quCq.SQL.Add('select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card,');
            quCq.SQL.Add('SUM(cq.Ck_Disc) as CorSum');
            quCq.SQL.Add(', sum(cq.Summa) as RSum');

            quCq.SQL.Add(', SUM(if(gd.NDS<=13,cq.Ck_Disc,0))as CorSum10');
            quCq.SQL.Add(', sum(if(gd.NDS<=13,cq.Summa,0))as RSum10');
            quCq.SQL.Add(', SUM(if(gd.NDS>13,cq.Ck_Disc,0))as CorSum18');
            quCq.SQL.Add(', sum(if(gd.NDS>13,cq.Summa,0))as RSum18');
            quCq.SQL.Add('from "'+StrTa+'" cq');
            quCq.SQL.Add('left join Goods gd on gd.ID=cq.Code');
            quCq.SQL.Add('where cq.DateOperation='''+ds(iCurD)+'''');
            quCq.SQL.Add('group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card');
            quCq.SQL.Add('order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card');

            quCq.Active:=True;

            quCq.First;
            while not quCq.Eof do
            begin
              iCassir:=quCqCassir.AsInteger;
              if iCassir>1000000 then iCassir:=(iCassir-1000000)*-1;
              quA.SQL.Clear;
              if quCqCk_Card.AsInteger=0 then //���
              begin
                quA.SQL.Add('INSERT into "SaleByDepart" values (');
              end else //������
              begin
                quA.SQL.Add('INSERT into "SaleByDepartBN" values (');
              end;
              quA.SQL.Add(''''+ds(iCurD)+''','); //DateSale
              quA.SQL.Add(its(quCqCash_Code.AsInteger)+',');  //CassaNumber
              quA.SQL.Add(its(quCqGrCode.AsInteger)+',');//Otdel
              quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat))+',');//Summa
              quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat))+',');//SummaCor
              quA.SQL.Add(its(iCassir)+',');//Employee
//              quA.SQL.Add(fs(roundVal(quCqRSum10.AsFloat))+',');//SummaNDSx10
              quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx10
              quA.SQL.Add(fs(roundVal(quCqRSum18.AsFloat))+',');//SummaNDSx20
              quA.SQL.Add(fs(roundVal(quCqCorSum10.AsFloat))+',');//CorNDSx10
              quA.SQL.Add(fs(roundVal(quCqCorSum18.AsFloat))+',');//CorNDSx20
              quA.SQL.Add('0,');//SummaNSPx10
              quA.SQL.Add('0,');//SummaNSPx20
              quA.SQL.Add('0,');//SummaOblNSP
              quA.SQL.Add('0,');//SummaNDSx0
              quA.SQL.Add('0,');//CorNDSx0
              quA.SQL.Add('0,');//SummaNSP0
              quA.SQL.Add('0,');//NDSx10
              quA.SQL.Add('0,');//NDSx20
              quA.SQL.Add('0,');//CorNSPx0
              quA.SQL.Add('0,');//CorNSPx10
              quA.SQL.Add('0,');//CorNSPx20
              quA.SQL.Add('0,');//Rezerv
              quA.SQL.Add('3,');//ShopIndex
              quA.SQL.Add('0,');//RezervSumma
              quA.SQL.Add('0,');//RezervSumNSP
              quA.SQL.Add('0,');//RezervSkid
              quA.SQL.Add('0,');//RezervSkidNSP
              quA.SQL.Add('0,');//SenderSumma
              quA.SQL.Add('0,');//SenderSumNSP
              quA.SQL.Add('0,');//SenderSkid
              quA.SQL.Add('0');//SenderSkidNSP
              quA.SQL.Add(')');
              quA.ExecSQL;

              quCq.Next;
            end;

            fmSt.Memo1.Lines.Add(fmt+'  C��������� ���������.'); delay(10);
          end;
        except;
        end;
      finally
        quPriorD.Active:=False;
        fmSt.Memo1.Lines.Add(fmt+'�������� �������.'); delay(10);
        fmSt.cxButton1.Enabled:=True;
      end;
      end;
    end;
  end;

end;

procedure TfmMainMCryst.acRepRealExecute(Sender: TObject);
var
    iStep,iC,iGr:INteger;
    mm,mm1:String;
begin
//����� �� ����������
  if fmclients.Showing then fmclients.Close;
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;

//  fmPeriod.cxLookupComboBox4.Visible:=False; fmPeriod.cxCheckBox4.Visible:=False; fmPeriod.cxLabel6.Visible:=False;
//  fmPeriod.cxLookupComboBox5.Visible:=False; fmPeriod.cxCheckBox5.Visible:=False; fmPeriod.cxLabel7.Visible:=False;
  fmPeriod.cxCheckBox7.Checked:=True;
  fmPeriod.cxLabel9.Visible:=False;
  fmPeriod.cxCheckBox7.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);

    mm:=FormatDateTime('yyyymm',CommonSet.DateBeg);
    mm1:=FormatDateTime('yyyymm',CommonSet.DateEnd);

    if mm<>mm1 then
    begin
      showmessage('������ ������� ������ ���� � �������� ������ ������.');
    end else
    begin

    //�������
    with dmMc do
    with dmMt do
    begin
      fmRepReal.PBar1.Position:=0;
      fmRepReal.Caption:='���������� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd)+'  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmRepReal.Show;
      fmRepReal.PBar1.Visible:=True;
      with fmRepReal do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewGrReal.BeginUpdate;
        CloseTe(fmRepReal.teR);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;


        fmRepReal.PBar1.Position:=10; delay(10);

        quRepReal.Active:=False;
        quRepReal.SQL.Clear;
//        quRepReal.SQL.Add('');
        quRepReal.SQL.Add('select');
        quRepReal.SQL.Add('cq.Code,');
        quRepReal.SQL.Add('cq.GrCode,');
        quRepReal.SQL.Add('cq.Operation,');
        quRepReal.SQL.Add('cq.Ck_Card,');
        quRepReal.SQL.Add('gd.Name,');
        quRepReal.SQL.Add('gd.EdIzm,');
        quRepReal.SQL.Add('gd.GoodsGroupID,');
        quRepReal.SQL.Add('gd.V01,');
        quRepReal.SQL.Add('gd.V02,');
        quRepReal.SQL.Add('de.Name as NameDep,');
        quRepReal.SQL.Add('sum(cq.Quant) as Quant,');
        quRepReal.SQL.Add('sum(cq.Ck_Disc) as Disc,');
        quRepReal.SQL.Add('sum(cq.Summa) as Summa');
        quRepReal.SQL.Add('from "cq'+mm+'" cq');
        quRepReal.SQL.Add('left join "Goods" gd on gd.ID=cq.Code');
        quRepReal.SQL.Add('left join "Depart" de on de.ID=cq.GrCode');

        quRepReal.SQL.Add('where cq.DateOperation >= '''+ds(CommonSet.DateBeg)+''' and cq.DateOperation <='''+ds(CommonSet.DateEnd)+'''');

        if ObPar.iPost>0 then
        begin
        {  quRepReal.SQL.Add('and cq.Code in (select CodeTovar from "TTNInLn"');
          quRepReal.SQL.Add('where DateInvoice>='''+ds(Date-365)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepReal.SQL.Add('group by CodeTovar');
          quRepReal.SQL.Add(')');   }

          quRepReal.SQL.Add('and cq.Code in (select GoodsID from "GdsClient"');
          quRepReal.SQL.Add('where ClientObjTypeID='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepReal.SQL.Add(')');

        end;

        if ObPar.iDepart>0 then
          quRepReal.SQL.Add('and cq.GrCode in ('+its(ObPar.iDepart)+')');

        if ObPar.iSGroup>0 then quRepReal.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepReal.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepReal.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepReal.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        quRepReal.SQL.Add('group by cq.Code,cq.GrCode,cq.Operation,cq.Ck_Card,gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.V01,gd.V02,de.Name');

        quRepReal.Active:=True;

        fmRepReal.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

        quRepReal.First;

        iStep:=quRepReal.RecordCount div 60;
        if iStep=0 then iStep:=quRepReal.RecordCount;
        iC:=0;

        while not quRepReal.Eof do
        begin
          teR.Append;
          teRIdCode1.AsInteger:=quRepRealCode.AsInteger;
          teRNameC.AsString:=quRepRealName.AsString;
          teROper.AsString:=quRepRealOperation.AsString;
          teRPlType.AsInteger:=quRepRealCk_Card.AsInteger;
          teRiM.AsInteger:=quRepRealEdIzm.AsInteger;
          teRIdGroup.AsInteger:=quRepRealGoodsGroupID.AsInteger;

//          teRIdSGroup.AsInteger:=quRepRealSubGroupID.AsInteger;
//          teRNameGr1.AsString:=prFindTNameMGr(quRepRealGoodsGroupID.AsInteger);
//          teRNameGr2.AsString:=prFindTNameGr(quRepRealSubGroupID.AsInteger,iGr1);

          teRNameGr1.AsString:='';
          teRNameGr2.AsString:='';
          teRNameGr3.AsString:='';

          iGr:=quRepRealGoodsGroupID.AsInteger;
          if teClass.Locate('Id',iGr,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teRNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;

          teRiBrand.AsInteger:=quRepRealV01.AsInteger;
          teRiCat.AsInteger:=quRepRealV02.AsInteger;
          teRDepName.AsString:=quRepRealNameDep.AsString;
          teRQuant.AsFloat:=quRepRealQuant.AsFloat;
          teRRSum.AsFloat:=quRepRealSumma.AsFloat;
          teRRDisc.AsFloat:=quRepRealDisc.AsFloat;
          teRiDep.AsInteger:=quRepRealGrCode.AsInteger;
          teR.Post;


          quRepReal.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmRepReal.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        fmRepReal.PBar1.Position:=100; delay(100);

        quRepReal.Active:=False;
        teClass.Active:=False;
        ViewGrReal.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmRepReal.PBar1.Visible:=False;
      end;
    end;
    end;

    fmPeriod.Release;
  end else
    fmPeriod.Release;
end;

procedure TfmMainMCryst.acDStopExecute(Sender: TObject);
Var bOk:Boolean;
    sPath,StrWk,sCards,sBar,sNum,sCashLdd,sName,sMessur:String;
    NumPost:INteger;
begin
  //�������� ����
  //�������� ������ �� �����
  with dmMt do
  with dmMC do
  begin
    try
//     if fmSt.cxButton1.Enabled=False then exit;

      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('���� �������� ����..');

      bOk:=True;

      if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX.');

      dmPx.Session1.Active:=False;
      dmPx.Session1.NetFileDir:=CommonSet.NetPath;
      dmPx.Session1.Active:=True;

      //������ �������� �� �������� �������
      StrWk:=FloatToStr(now);
      Delete(StrWk,1,2);
      Delete(StrWk,pos(',',StrWk),1);
      StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

      NumPost:=StrToINtDef(StrWk,0);
      sCards:='_'+IntToHex(NumPost,7);
      sBar:='_'+IntToHex(NumPost+1,7);

      //����� �������� ������
      CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
      CopyFile(PChar(CommonSet.TrfPath+'DStop.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'DStop.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

      try
        with dmPx do
        begin
          taDStop.Active:=False;
          taDStop.DatabaseName:=CommonSet.TmpPath;
          taDStop.TableName:=sBar+'.db';
          taDStop.Active:=True;

          taCard.Active:=False;
          taCard.DatabaseName:=CommonSet.TmpPath;
          taCard.TableName:=sCards+'.db';
          taCard.Active:=True;

          if ptDStop.Active=False then ptDStop.Active:=True;
          ptDStop.First;
          while not ptDStop.Eof do
          begin
            prAddCashLoad(ptDStopCode.AsInteger,2,0,ptDStopPercent.AsFloat);

            prAddPosFB(18,ptDStopCode.AsInteger,0,0,0,0,1,'','','','','',0,ptDStopPercent.AsFloat,0,0);

            taDStop.Append;
            taDStopCardArticul.AsString:=ptDStopCode.AsString;
            taDStopPercent.AsFloat:=ptDStopPercent.AsFloat;
            taDStop.Post;

            //����� ��� �������� ������� ���� � �������� ���� ���
            if taCards.FindKey([ptDStopCode.AsInteger]) then
            begin
              sName:=OemToAnsiConvert(prRetDName(taCardsID.AsInteger)+taCardsFullName.AsString);

              if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

              taCard.Append;
              taCardArticul.AsString:=taCardsID.AsString;
              taCardName.AsString:=AnsiToOemConvert(sName);
              taCardMesuriment.AsString:=AnsiToOemConvert(sMessur);
              taCardAdd1.AsString:='';
              taCardAdd2.AsString:='';
              taCardAdd3.AsString:='';
              taCardAddNum1.AsFloat:=0;
              taCardAddNum2.AsFloat:=0;
              taCardAddNum3.AsFloat:=0;
              taCardScale.AsString:='NOSIZE';
              taCardGroop1.AsInteger:=taCardsGoodsGroupID.AsInteger;
              taCardGroop2.AsInteger:=taCardsSubGroupID.AsInteger;
              taCardGroop3.AsInteger:=0;
              taCardGroop4.AsInteger:=0;
              taCardGroop5.AsInteger:=0;
              taCardMesPresision.AsFloat:=RoundEx(taCardsPrsision.AsFloat*1000)/1000;
              taCardPriceRub.AsFloat:=rv(taCardsCena.AsFloat);
              taCardPriceCur.AsFloat:=0;
              taCardClientIndex.AsInteger:=0;
              taCardCommentary.AsString:='';
              taCardDeleted.AsInteger:=1;
              taCardModDate.AsDateTime:=Trunc(Date);
              taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
              taCardModPersonIndex.AsInteger:=0;
              taCard.Post;

              prAddPosFB(1,taCardsID.AsInteger,taCardsV11.AsInteger,0,taCardsGoodsGroupID.AsInteger,0,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));
            end;

            ptDStop.Next;
          end;
          taDStop.Active:=False;
          ptDStop.Active:=False;
          taCard.Active:=False;
        end;
      except
        fmSt.Memo1.Lines.Add('������ ������������ ������.');
        fmSt.Memo1.Lines.Add('  �������� ����������!!!');
        bOk:=False;
      end;
      quCash.Active:=False;
      quCash.Active:=True;
      while not quCash.Eof do
      begin
        if quCashLoad.AsInteger=1 then
        begin
          fmSt.Memo1.Lines.Add(' '+quCashName.AsString);
          sPath:=CommonSet.CashPath+'\'+quCashNumber.AsString+'\';

          StrWk:=quCashNumber.AsString;
          while length(StrWk)<2 do strwk:='0'+StrWk;
          StrWk:='cash'+StrWk+'.ldd';
          sCashLdd:=StrWk;

          if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
          begin
            fmSt.Memo1.Lines.Add('');
            fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
            fmSt.Memo1.Lines.Add('  �������� ����������!!!');
            fmSt.Memo1.Lines.Add('  ���������� �����...');
            fmSt.Memo1.Lines.Add('');
            bOk:=False;
          end else
          begin //��� ����� ���������� �������� �����
            //���� �����������
            try
              with dmPx do
              begin
                sNum:=quCashNumber.AsString+'\';

                //���������
                CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                Delay(50);

                CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                StrWk:=quCashNumber.AsString;
                while length(StrWk)<3 do strwk:='0'+StrWk;
                StrWk:='cash'+StrWk+'.db';

                if Fileexists(CommonSet.CashPath+StrWk)=False then
                begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                  CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                  MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                end;

                taList.Active:=False;
                taList.DatabaseName:=CommonSet.TmpPath;
                taList.TableName:=CommonSet.CashPath+sNum+StrWk;
                taList.Active:=True;

                taList.Append;
                taListTName.AsString:=sCards;
                taListDataType.AsInteger:=1;   //�������� �� ������� ������ �� ���������������
                taListOperation.AsInteger:=1;
                taList.Post;

                taList.Append;
                taListTName.AsString:=sBar;
                taListDataType.AsInteger:=18;   //������ ������ ����������� ������ �� ������
                taListOperation.AsInteger:=0;
                taList.Post;

                taList.Active:=False;

                if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                delay(50);
                if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
              end; //dmPx
            except
              fmSt.Memo1.Lines.Add('  ������ ������ � �������..');
              bOk:=False;
            end;
          end;

          fmSt.Memo1.Lines.Add(' ��');
        end else
        begin
        //  fmSt.Memo1.Lines.Add(' '+quCashName.AsString+' �� �������.');
        end;

        quCash.Next;
      end;

        //������ �����

      if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
      if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
      if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
      if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

      if bOk then
      begin
        fmSt.Memo1.Lines.Add('�������� ��.');
      end
      else //����� ������� ������ �� ����
        fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
    finally
      quCash.Active:=False;
      dmPx.Session1.Active:=False;
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

procedure TfmMainMCryst.acCheckExecute(Sender: TObject);
Var mm,mm1:String;
begin
//�������� ����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;

  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    delay(10);
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    mm:=FormatDateTime('yyyymm',CommonSet.DateBeg);
    mm1:=FormatDateTime('yyyymm',CommonSet.DateEnd);

    if mm<>mm1 then
    begin
      showmessage('������ ������ ���� � �������� ������ ������.');
    end else
    begin
      //�������
      with dmMc do
      with dmMt do
      begin
        fmChecks.PBar1.Position:=0;
        fmChecks.Caption:='���� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
        fmChecks.Show;
        fmChecks.Memo1.Clear;
        fmChecks.Memo1.Lines.Add('����� ... ���� ������������.');
        delay(50);

        fmChecks.ViewChecks.BeginUpdate;
        ptCqRep.Active:=False;
        ptCqRep.DatabaseName:=Btr1Path;

        ptCqRep.TableName:='cq'+mm;
        ptCqRep.Active:=True;
        ptCqRep.CancelRange;
        ptCqRep.SetRange([CommonSet.DateBeg],[CommonSet.DateEnd]);
        fmChecks.ViewChecks.EndUpdate;

        fmChecks.Memo1.Lines.Add('������������ ��.');
        delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMCryst.asBufRebildExecute(Sender: TObject);
Var prDoc,prC:Real;
    iC,iCh:INteger;
begin
  //�������� ���������� �� �������
  with dmMc do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      if taG.Active=False then taG.Active:=True;
      iC:=0; iCh:=0;
      StatusBar1.Panels[0].Text:='������';

      quSpecTest.Active:=False;
      quSpecTest.Active:=True;
      quSpecTest.First;
      while not quSpecTest.Eof do
      begin
        prDoc:=quSpecTestNewCenaTovar.AsFloat;
        if taG.FindKey([quSpecTestCodeTovar.AsInteger]) then
        begin
          prC:=taGCena.AsFloat;
          if abs(prDoc-prC)>0.02 then
          begin //���� ����������� � ����������
            prTPriceBuf(quSpecTestCodeTovar.AsInteger,Trunc(quSpecTestDateInvoice.AsDateTime),(iCh+1),1,quSpecTestNumber.AsString,prC,prDoc);
            inc(iCh);
            StatusBar1.Panels[1].Text:=IntToStr(iCh);
          end;
        end;
        quSpecTest.Next;  inc(iC);
        StatusBar1.Panels[0].Text:=intToStr(iC);
        delay(10);
      end;

      taG.Active:=False;
      quSpecTest.Active:=False;
    end;
  end;
end;

procedure TfmMainMCryst.acRecalcAllRemnRealExecute(Sender: TObject);
Var iC,iDate:Integer;
begin
  with dmMc do
  with dmMT do
  begin
    if MessageDlg('�������� �������� �������� �� ���� ���������? (������������ ����������)',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='������������'; StatusBar1.Panels[1].Text:='0'; delay(10);

      quRealTMP.Active:=False;
      quRealTMP.Active:=True;

      StatusBar1.Panels[0].Text:='0'; StatusBar1.Panels[1].Text:=INtToStr(quRealTMP.RecordCount); delay(10);
      iC:=0;
      iDate:=Trunc(StrToDate('31.01.2010'));
      quRealTMP.First;
      while not quRealTMP.Eof do
      begin
        prReCalcTMove(quRealTMPCARD.AsInteger,iDate);

        quRealTMP.Next; inc(iC); StatusBar1.Panels[0].Text:=IntToStr(iC); delay(10);
      end;
      quRealTMP.Active:=False;
    end;
    StatusBar1.Panels[0].Text:='��'; StatusBar1.Panels[1].Text:=''; delay(10);
  end;
end;

procedure TfmMainMCryst.acLoadCashAllExecute(Sender: TObject);
Var bOk:Boolean;
    sPath,StrWk,sCards,sBar,sNum,sCashLdd,sMessur,sCSz,sName:String;
    iNum:INteger;
    rPr:Real;
    iC,NumPost:INteger;
begin
  //������ ��������� ����
  if not CanDo('prFullCashLoad') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  begin
    if MessageDlg('�� ������������� ������ �������� ������ ��������� ����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� ������ ��������� ����...',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        //�������
        with dmMc do
        with dmMT do
        begin


    try
      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('���� �������� ����..');

      fmSt.Memo1.Lines.Add('  ����� ���� ������������ ������ ..');
      if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX.');

      bOk:=True;

      dmPx.Session1.Active:=False;
      dmPx.Session1.NetFileDir:=CommonSet.NetPath;
      dmPx.Session1.Active:=True;

      //������ �������� �� �������� �������
      StrWk:=FloatToStr(now);
      Delete(StrWk,1,2);
      Delete(StrWk,pos(',',StrWk),1);
      StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

      NumPost:=StrToINtDef(StrWk,0);
      sCards:='_'+IntToHex(NumPost,7);
      sBar:='_'+IntToHex(NumPost+1,7);

//      sCards:=StrWk;
//      sBar:=IntToStr(StrToInt(StrWk)+1);

      //����� �������� ������
      CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Bars.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Bars.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

      try
        with dmPx do
        begin
          taCard.Active:=False;
          taCard.DatabaseName:=CommonSet.TmpPath;
          taCard.TableName:=sCards+'.db';
          taCard.Active:=True;

          taBar.Active:=False;
          taBar.DatabaseName:=CommonSet.TmpPath;
          taBar.TableName:=sBar+'.db';
          taBar.Active:=True;

          quFindC5.Active:=False;
          quFindC5.Active:=True;

          iC:=0;
          quFindC5.First;
          while not quFindC5.Eof do
          begin
            iNum:=quFindC5ID.AsInteger;
            rPr:=quFindC5Cena.AsFloat;

            quBars.Active:=False;
            quBars.ParamByName('GOODSID').AsInteger:=iNum;
            quBars.Active:=True;

            prAddCashLoad(iNum,1,rPr,0);

            if quFindC5EdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

            sName:=prRetDName(quFindC5ID.AsInteger)+quFindC5FullName.AsString;
            prAddPosFB(1,quFindC5ID.AsInteger,quFindC5V11.AsInteger,0,quFindC5GoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),1,'','',sName,sMessur,'',0,0,RoundEx(quFindC5Prsision.AsFloat*1000)/1000,rv(rPr));

            taCard.Append;
            taCardArticul.AsString:=quFindC5ID.AsString;
            taCardName.AsString:=AnsiToOemConvert(sName);
            taCardMesuriment.AsString:=AnsiToOemConvert(sMessur);
            taCardMesPresision.AsFloat:=RoundEx(quFindC5Prsision.AsFloat*1000)/1000;
            taCardAdd1.AsString:='';
            taCardAdd2.AsString:='';
            taCardAdd3.AsString:='';
            taCardAddNum1.AsFloat:=0;
            taCardAddNum2.AsFloat:=0;
            taCardAddNum3.AsFloat:=0;
            taCardScale.AsString:='NOSIZE';
            taCardGroop1.AsInteger:=quFindC5GoodsGroupID.AsInteger;
            taCardGroop2.AsInteger:=quFindC5SubGroupID.AsInteger;
            taCardGroop3.AsInteger:=0;
            taCardGroop4.AsInteger:=0;
            taCardGroop5.AsInteger:=0;

            taCardPriceRub.AsFloat:=rv(rPr);

            taCardPriceCur.AsFloat:=0;
            taCardClientIndex.AsInteger:=Trunc(quCashRezerv.AsFloat);
            taCardCommentary.AsString:='';
            taCardDeleted.AsInteger:=1;
            taCardModDate.AsDateTime:=Trunc(Date);
            taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
            taCardModPersonIndex.AsInteger:=0;
            taCard.Post;

            quBars.First;
            while not quBars.Eof do
            begin
              try

                if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                else sCSz:='QUANTITY';

                prAddPosFB(2,quFindC5ID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(rPr));

                if taBar.Locate('BarCode',quBarsID.AsString,[])=False then
                begin
                  taBar.Append;
                  taBarBarCode.AsString:=quBarsID.AsString;
                  taBarCardArticul.AsString:=quBarsGoodsID.AsString;
                  taBarCardSize.AsString:=sCSz;
                  taBarQuantity.AsFloat:=quBarsQuant.AsFloat;
                  taBar.Post;
                end;  
              except
                fmSt.Memo1.Lines.Add('    ������ �� - '+quBarsID.AsString);
                taBar.Cancel;
              end;

              quBars.Next;
            end;

            quBars.Active:=False;
            quFindC5.Next; inc(iC);
            if iC mod 1000 = 0 then
            begin
              fmSt.Memo1.Lines.Add('    ���������� - '+INtToStr(iC));
              delay(100);
            end;
          end;
          taCard.Active:=False;
          taBar.Active:=False;
        end;
      except
        fmSt.Memo1.Lines.Add('������ ������������ ������.');
        fmSt.Memo1.Lines.Add('  �������� ����������!!!');
        bOk:=False;
      end;
      quCash.Active:=False;
      quCash.Active:=True;
      while not quCash.Eof do
      begin
        if quCashLoad.AsInteger=1 then
        begin
          fmSt.Memo1.Lines.Add(' '+quCashName.AsString);
          sPath:=CommonSet.CashPath+'\'+quCashNumber.AsString+'\';

          StrWk:=quCashNumber.AsString;
          while length(StrWk)<2 do strwk:='0'+StrWk;
          StrWk:='cash'+StrWk+'.ldd';
          sCashLdd:=StrWk;

          if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
          begin
            fmSt.Memo1.Lines.Add('');
            fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
            fmSt.Memo1.Lines.Add('  �������� ����������!!!');
            fmSt.Memo1.Lines.Add('  ���������� �����...');
            fmSt.Memo1.Lines.Add('');
            bOk:=False;
          end else
          begin //��� ����� ���������� �������� �����
            //���� �����������
            try
              with dmPx do
              begin
                sNum:=quCashNumber.AsString+'\';

                //���������
                CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                Delay(50);

                CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                StrWk:=quCashNumber.AsString;
                while length(StrWk)<3 do strwk:='0'+StrWk;
                StrWk:='cash'+StrWk+'.db';

                if Fileexists(CommonSet.CashPath+StrWk)=False then
                begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                  CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                  MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                end;

                taList.Active:=False;
                taList.DatabaseName:=CommonSet.TmpPath;
                taList.TableName:=CommonSet.CashPath+sNum+StrWk;
                taList.Active:=True;

                taList.Append;
                taListTName.AsString:=sCards;
                taListDataType.AsInteger:=1;
                taListOperation.AsInteger:=1;
                taList.Post;

                taList.Append;
                taListTName.AsString:=sBar;
                taListDataType.AsInteger:=2;
                taListOperation.AsInteger:=1;
                taList.Post;

                taList.Active:=False;

                if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                delay(50);
                if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
              end; //dmPx
            except
              fmSt.Memo1.Lines.Add('  ������ ������ � �������..');
              bOk:=False;
            end;
          end;

          fmSt.Memo1.Lines.Add(' ��');
        end else
        begin
        //  fmSt.Memo1.Lines.Add(' '+quCashName.AsString+' �� �������.');
        end;

        quCash.Next;
      end;

        //������ �����

      if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
      if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
      if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
      if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

      if bOk then
      begin
        //��������
        // ��� �� ���� ������, ��� ������
        {
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "Goods" Set Status=1');
        quE.ExecSQL;
         }
        fmSt.Memo1.Lines.Add('�������� ��������� ��.');
      end
      else //����� ������� ������ �� ����
        fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
    finally
      quCash.Active:=False;
      dmPx.Session1.Active:=False;
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;


        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acRepRealDepsExecute(Sender: TObject);
begin
  //���������� �� �������
  with dmMc do
  begin
    //��������
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      fmCashDep.ViewCDep.BeginUpdate ;

      CloseTe(taCDep);

      quCDep.Active:=False;
      quCDep.SQL.Clear;
      quCDep.SQL.Add('Select');
      quCDep.SQL.Add('sd.DateSale,');
      quCDep.SQL.Add('sd.CassaNumber,');
      quCDep.SQL.Add('sd.Otdel,');
      quCDep.SQL.Add('sd.Summa,');
      quCDep.SQL.Add('sd.SummaCor,');
      quCDep.SQL.Add('sd.Employee,');
      quCDep.SQL.Add('sd.SummaNDSx10,');
      quCDep.SQL.Add('sd.SummaNDSx20,');
      quCDep.SQL.Add('sd.SummaNDSx0,');
      quCDep.SQL.Add('sd.CorNDSx10,');
      quCDep.SQL.Add('sd.CorNDSx20,');
      quCDep.SQL.Add('sd.CorNDSx0,');
      quCDep.SQL.Add('de.Name');
      quCDep.SQL.Add('from "SaleByDepart" sd');
      quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
      quCDep.SQL.Add('where DateSale>='''+ds(CommonSet.DateBeg)+'''');
      quCDep.SQL.Add('and DateSale<='''+ds(CommonSet.DateEnd)+'''');
      quCDep.Active:=True;

      quCDep.First;
      while not quCDep.Eof do
      begin
        taCDep.Append;
        taCDepValT.AsInteger:=1;
        taCDepiDep.AsInteger:=quCDepOtdel.AsInteger;
        taCDepsDep.AsString:=quCDepName.AsString;
        taCDepDateSale.AsDateTime:=quCDepDateSale.AsDateTime;
        taCDepCashNum.AsInteger:=quCDepCassaNumber.AsInteger;
        taCDepCassir.AsInteger:=quCDepEmployee.AsInteger;
        taCDepRSum.AsFloat:=quCDepSumma.AsFloat;
        taCDepRSumD.AsFloat:=quCDepSummaCor.AsFloat;
        taCDepRSumN10.AsFloat:=quCDepSummaNDSx10.AsFloat;
        taCDepRSumN20.AsFloat:=quCDepSummaNDSx20.AsFloat;
        taCDeprSumN0.AsFloat:=quCDepSummaNDSx0.AsFloat;
        taCDepRSumD10.AsFloat:=quCDepCorNDSx10.AsFloat;
        taCDepRSumD20.AsFloat:=quCDepCorNDSx20.AsFloat;
        taCDepRSumD0.AsFloat:=quCDepCorNDSx0.AsFloat;
        taCDep.Post;

        quCDep.Next;
      end;


      quCDep.Active:=False;
      quCDep.SQL.Clear;
      quCDep.SQL.Add('Select');
      quCDep.SQL.Add('sd.DateSale,');
      quCDep.SQL.Add('sd.CassaNumber,');
      quCDep.SQL.Add('sd.Otdel,');
      quCDep.SQL.Add('sd.Summa,');
      quCDep.SQL.Add('sd.SummaCor,');
      quCDep.SQL.Add('sd.Employee,');
      quCDep.SQL.Add('sd.SummaNDSx10,');
      quCDep.SQL.Add('sd.SummaNDSx20,');
      quCDep.SQL.Add('sd.SummaNDSx0,');
      quCDep.SQL.Add('sd.CorNDSx10,');
      quCDep.SQL.Add('sd.CorNDSx20,');
      quCDep.SQL.Add('sd.CorNDSx0,');
      quCDep.SQL.Add('de.Name');
      quCDep.SQL.Add('from "SaleByDepartBn" sd');
      quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
      quCDep.SQL.Add('where DateSale>='''+ds(CommonSet.DateBeg)+'''');
      quCDep.SQL.Add('and DateSale<='''+ds(CommonSet.DateEnd)+'''');
      quCDep.Active:=True;

      quCDep.First;
      while not quCDep.Eof do
      begin
        taCDep.Append;
        taCDepValT.AsInteger:=2;
        taCDepiDep.AsInteger:=quCDepOtdel.AsInteger;
        taCDepsDep.AsString:=quCDepName.AsString;
        taCDepDateSale.AsDateTime:=quCDepDateSale.AsDateTime;
        taCDepCashNum.AsInteger:=quCDepCassaNumber.AsInteger;
        taCDepCassir.AsInteger:=quCDepEmployee.AsInteger;
        taCDepRSum.AsFloat:=quCDepSumma.AsFloat;
        taCDepRSumD.AsFloat:=quCDepSummaCor.AsFloat;
        taCDepRSumN10.AsFloat:=quCDepSummaNDSx10.AsFloat;
        taCDepRSumN20.AsFloat:=quCDepSummaNDSx20.AsFloat;
        taCDeprSumN0.AsFloat:=quCDepSummaNDSx0.AsFloat;
        taCDepRSumD10.AsFloat:=quCDepCorNDSx10.AsFloat;
        taCDepRSumD20.AsFloat:=quCDepCorNDSx20.AsFloat;
        taCDepRSumD0.AsFloat:=quCDepCorNDSx0.AsFloat;
        taCDep.Post;

        quCDep.Next;
      end;
      fmCashDep.ViewCDep.EndUpdate;
//      fmCashDep:=TfmCashDep.Create(Application);
      fmCashDep.Show;
//      fmCashDep.Release;
    end;
  end;
end;

procedure TfmMainMCryst.acZerroExecute(Sender: TObject);
begin
  showmessage('� ��� ��� ���� �� ������ ��������.');
end;

procedure TfmMainMCryst.acMoveTaraExecute(Sender: TObject);
var iStep,iC:INteger;
    rQb,rQIn,rSIn,rQOut,rSOut:Real;
begin
 //�������� ����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
   //�������
    with dmMc do
    with dmMT do
    begin

      fmObVedTara.PBar1.Position:=0;
      fmObVedTara.Caption:='�������� �� ���� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
      fmObVedTara.Show;
      fmObVedTara.PBar1.Visible:=True;

      with fmObVedTara do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewObVedTara.BeginUpdate;
        CloseTe(fmObVedTara.teObVedTara);

        fmObVedTara.PBar1.Position:=10; delay(10);

        quTara.Active:=False;
        dsquTara.DataSet:=nil;

        quTara.Active:=True;

        rQb:=0; rQIn:=0; rSIn:=0; rQOut:=0; rSOut:=0; //rQRes:=0; rQInv:=0;

        iStep:=quTara.RecordCount div 90;
        if iStep=0 then iStep:=quTara.RecordCount;
        iC:=0;

        quTara.First;
        while not quTara.Eof do
        begin
          prCalcTaraMove(quTaraCode.AsInteger,0,rQb,rQIn,rSIn,rQOut,rSOut);

          teObVedTara.Append;
          teObVedTaraCodeT.AsInteger:=quTaraCode.AsInteger;
          teObVedTaraNameT.AsString:=quTaraName.AsString;
          teObVedTaraQBeg.AsFloat:=rQb;
          teObVedTaraQIn.AsFloat:=rQIn;
          teObVedTaraPrIn.AsFloat:=0;
          teObVedTaraSumIn.AsFloat:=rSIn;
          teObVedTaraPrOut.AsFloat:=0;
          teObVedTaraQOut.AsFloat:=rQOut;
          teObVedTaraSumOut.AsFloat:=rSOut;
          teObVedTaraQRes.AsFloat:=rQb+rQIn+rQOut;
          teObVedTaraQInv.AsFloat:=0;
          teObVedTara.Post;

          quTara.Next;
          inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmObVedTara.PBar1.Position:=10+(iC div iStep);
            delay(10);
          end;
        end;
        quTara.Active:=False;
        dsquTara.DataSet:=quTara;

        teObVedTara.First;
        while not teObVedTara.Eof do
        begin
          if (teObVedTaraQBeg.AsFloat=0) and
             (teObVedTaraQIn.AsFloat=0) and
             (teObVedTaraQOut.AsFloat=0) then teObVedTara.Delete
          else teObVedTara.Next;
        end;

        fmObVedTara.PBar1.Position:=100; delay(100);

        ViewObVedTara.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmObVedTara.PBar1.Visible:=False;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acDStopListExecute(Sender: TObject);
begin
//����������� ������ �� �����
  fmDStop:=TfmDStop.Create(Application);
  try
    dmMt.ptPluLim.Active:=True;
    fmDStop.ShowModal;
  finally
    dmMt.ptPluLim.Active:=False;
    fmDStop.Release;
  end;
end;

procedure TfmMainMCryst.acTestMoveTaraExecute(Sender: TObject);
var DateB,DateE,iCurD:Integer;
    StrWk:String;
    dCurD:TDateTime;
begin
//�������� �������������� �� ����
  //�������� ���������� �� �������
  with dmMc do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

//      if DateB<=prOpenDate then
      if DateB>0 then //�������� �.�. ������������� �������������� ����� ������
      begin
        ShowMessage('������ ������.');
      end else
      begin

        if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
        else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;
        try
          try
            fmSt.Memo1.Lines.Add(fmt+'������ ��������� '+StrWk);
            for iCurD:=DateB to DateE do
            begin
              fmSt.Memo1.Lines.Add(fmt+'');
              fmSt.Memo1.Lines.Add(fmt+'  ��������� ����  '+ FormatDateTime('dd.mm.yyyy',iCurD)); delay(10);
              fmSt.Memo1.Lines.Add(fmt+'    - ���� ��������������.'); delay(10);

              dCurD:=iCurD;
              if ptCTM1.Active=False then ptCTM1.Active:=True;
              ptCTM1.CancelRange;
              ptCTM1.SetRange([dCurD],[dCurD]);
              ptCTM1.First;
              while not ptCTM1.Eof do
              begin
                ptCTM1.Delete; //���� ��� �� ����
                delay(10);
              end;

              //������� ���������

              //�������
              fmSt.Memo1.Lines.Add(fmt+'    - ����������� �������.'); delay(10);

              fmDocs1.ViewDocsIn.BeginUpdate;
              try
                quTTNIn.Active:=False;
                quTTNIn.ParamByName('DATEB').AsDate:=iCurD;
                quTTNIn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                quTTNIn.Active:=True;

                quTTNIn.First;
                while not quTTNIn.Eof do
                begin
                  if quTTnInSummaTara.AsFloat<>0 then
                  begin  //�����������
                    fmSt.Memo1.Lines.Add(fmt+'       ���.� '+quTTnInNumber.AsString+' '+quTTnInSummaTara.AsString); delay(10);

                    quSpecInT.Active:=False;
                    quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                    quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                    quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                    quSpecInT.Active:=True;

                    quSpecInT.First;  // ��������������
                    while not quSpecInT.Eof do
                    begin
                      prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);
                      quSpecInT.Next;
                    end;
                    quSpecInT.Active:=False;
                  end;
                  quTTNIn.next; delay(10);
                end;
              finally
                fmDocs1.ViewDocsIn.EndUpdate;
              end;

              //�������
              fmSt.Memo1.Lines.Add(fmt+'    - ����������� �������.'); delay(10);
              fmDocs2.ViewDocsOut.BeginUpdate;
              try
                quTTnOut.Active:=False;
                quTTnOut.ParamByName('DATEB').AsDate:=iCurD;
                quTTnOut.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                quTTnOut.Active:=True;

                quTTnOut.First;
                while not quTTnOut.Eof do
                begin
                  if quTTnOutSummaTara.AsFloat<>0 then
                  begin  //�����������
                    fmSt.Memo1.Lines.Add(fmt+'       ���.� '+quTTnOutNumber.AsString+' '+quTTnOutSummaTara.AsString); delay(10);

                    quSpecOutT.Active:=False;
                    quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                    quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                    quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                    quSpecOutT.Active:=True;

                    quSpecOutT.First;  // ��������������
                    while not quSpecOutT.Eof do
                    begin
                      prAddTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1),quSpecOutTCenaTara.AsFloat);
                      quSpecOutT.Next;
                    end;
                    quSpecOutT.Active:=False;
                  end;
                  quTTnOut.next; delay(10);
                end;
              finally
                fmDocs2.ViewDocsOut.EndUpdate;
              end;

            end;
          except;
          end;
        finally
          fmSt.Memo1.Lines.Add(fmt+'�������� �������.'); delay(10);
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acTestMoveExecute(Sender: TObject);
begin
  ShowMessage('� ��� ��� ���� �� ������ ��������.');
end;

procedure TfmMainMCryst.acTestMoveWkExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk,sY:String;
    iC,Id,iDep,iDel:INteger;
    CurD:TDateTime;
    rRemn,rQSpis:Real;
    iT:INteger;
begin
//�������� ��������������
  if MessageDlg('�������� �������������� ��������� ������, ����� ���� ��� ���������� ������, �������� ��������?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
  begin
    if MessageDlg('�������, ��� ����� �������� ��������? (����� ���� ��� ���������� ������)', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmMC do
      with dmMt do
      begin
        fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; //���� ����� �� ���������
        fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
        fmPeriod1.Label3.Caption:='';
        fmPeriod1.ShowModal;
        if fmPeriod1.ModalResult=mrOk then
        begin
          DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
          DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

//          if DateB<=prOpenDate then
          if DateB<0 then //�������� - �������� �������������� �� ����� �� �� ������
          begin
            ShowMessage('������ ������.');
          end else
          begin

            if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
            else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

            fmSt.Memo1.Clear;
            fmSt.Show;
            with fmSt do
            begin
              cxButton1.Enabled:=False;
              Memo1.Lines.Add('������ �������� �������������� ('+StrWk+') ... �����.');
              try
                Memo1.Lines.Add('');
                Memo1.Lines.Add('   ������ ������ ('+StrWk+') ... �����.');
                if CommonSet.Test=0 then
                begin
                  ptCM.Active:=False;
                  ptCG.Active:=False;
                  ptCM1.Active:=False;
                  ptCM2.Active:=False;

                  ptCM2.Active:=True;
                  ptCM2.CancelRange;
                  ptCM2.SetRange([fmPeriod1.cxDateEdit1.Date],[fmPeriod1.cxDateEdit2.Date]);
                  ptCM2.First;
                  iC:=0;
                  while not ptCM2.Eof do
                  begin
                    ptCM2.Delete;
                    inc(iC);
                    if iC mod 100 = 0 then
                    begin
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                      delay(20);
                    end;
                  end;
                  ptCM2.Active:=False;
                end;
                Memo1.Lines.Add('   ������ ��.');
                Memo1.Lines.Add('');

                for iCurD:=DateB to DateE do
                begin //������������ �� �����
                  Memo1.Lines.Add('   ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD)); delay(100);

                  Memo1.Lines.Add('     - ������� ..'); delay(10);
                  fmDocs1.ViewDocsIn.BeginUpdate;
                  try
                    quTTNIn.Active:=False;
                    quTTNIn.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNIn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNIn.Active:=True;

                    quTTNIn.First; iC:=0;
                    while not quTTNIn.Eof do
                    begin
                      if quTTnInAcStatus.AsInteger=3 then
                      begin
                        if CommonSet.Test=0 then
                        begin
                          quSpecIn1.Active:=False;
                          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                          quSpecIn1.Active:=True;

                          quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
                          while not quSpecIn1.Eof do
                          begin
                            prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                            quSpecIn1.Next;
                          end;
                          quSpecIn1.Active:=False;


                          if quTTnInSummaTara.AsFloat<>0 then
                          begin //�������� �� ����
                            quSpecInT.Active:=False;
                            quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                            quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                            quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                            quSpecInT.Active:=True;

                            quSpecInT.First; // ��������������
                            while not quSpecInT.Eof do
                            begin
                              prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);
                              quSpecInT.Next;
                            end;
                            quSpecInT.Active:=False;
                          end;
                        end;
                      end;
                      quTTNIn.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;

                    //�������� ���������� ������������
                    if CommonSet.TestLostSpec=1 then
                    begin
                      Memo1.Lines.Add('       - �������� ���������� ������������ ..'); delay(10);
                      ptInLn.Active:=False ; ptINLn.Active:=True;
                      if ptDep.Active=False then ptDep.Active:=True;

                      ptTTNIn.Active:=False; ptTTNIn.Active:=True;
                      ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';

                      iC:=0; iDel:=0;

                      CurD:=iCurD;

                      ptDep.First;
                      while not ptDep.Eof do
                      begin
                        if ptDepStatus1.AsInteger=9 then
                        begin
                          ptInLn.CancelRange;
                          ptInLn.SetRange([ptDepId.AsInteger,CurD],[ptDepId.AsInteger,CurD]);
                          ptInLn.First;
                          while not ptInLn.Eof do
                          begin
                            if ptTTnIn.FindKey([ptInLnDepart.AsInteger,ptInLnDateInvoice.AsDateTime,ptInLnNumber.AsString]) then
                            begin
                             ptInLn.Next;
                            end
                            else
                            begin
                              inc(iDel);
                              ptInLn.Delete;
                            end;

                            inc(iC);
                            if iC mod 100 =0 then
                            begin
                              Memo1.Lines.Add('       - ���������� '+its(iC)+'  ���. '+its(iDel)); delay(10);
                            end;
                          end;
                        end;

                        ptDep.Next;
                      end;
                      Memo1.Lines.Add('       - ��. ����� '+its(iC)+'  ���. '+its(iDel)); delay(10);
                    end;

                  finally
                    fmDocs1.ViewDocsIn.EndUpdate;
                  end;
                  Memo1.Lines.Add('     - �������� ..'); delay(10);
                  fmDocs2.ViewDocsOut.BeginUpdate;
                  try
                    quTTNOut.Active:=False;
                    quTTNOut.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNOut.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNOut.Active:=True;

                    quTTNOut.First; iC:=0;
                    while not quTTNOut.Eof do
                    begin
                      if quTTNOutAcStatus.AsInteger=3 then
                      begin
                        if CommonSet.Test=0 then
                        begin
                          quSpecOut2.Active:=False;
                          quSpecOut2.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                          quSpecOut2.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                          quSpecOut2.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                          quSpecOut2.Active:=True;

                          quSpecOut2.First;  //��������� ����� ���  � ��������� ��������������
                          while not quSpecOut2.Eof do
                          begin
                            prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut2Kol.AsFloat*-1));

                            {
                            if abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat)>0.001 then
                            begin
                              rQSpis:=abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat);

                              prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
                            end;
                            }

                            if quSpecOut2ChekBuh.AsBoolean=True then
                            begin
                              if abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat)>0.001 then
                              begin
                                rQSpis:=abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat);
                                prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
                              end;
                            end;

                            quSpecOut2.Next;
                          end;
                          quSpecOut2.Active:=False;

                          if quTTnOutSummaTara.AsFloat<>0 then
                          begin //�������� �� ����
                            quSpecOutT.Active:=False;
                            quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                            quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                            quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                            quSpecOutT.Active:=True;

                            quSpecOutT.First;  // ��������������
                            while not quSpecOutT.Eof do
                            begin
                              prAddTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1),quSpecOutTCenaTara.AsFloat);

                              quSpecOutT.Next;
                            end;
                            quSpecOutT.Active:=False;
                          end;
                        end;
                      end;
                      quTTNOut.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs2.ViewDocsOut.EndUpdate;
                  end;

                  Memo1.Lines.Add('     - ����������� ..'); delay(10);

                  fmDocs4.ViewDocsVN.BeginUpdate;
                  try
                    quTTNVn.Active:=False;
                    quTTNVn.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNVn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNVn.Active:=True;

                    quTTNVn.First; iC:=0;
                    while not quTTNVn.Eof do
                    begin
                      if quTTnVnAcStatusP.AsInteger=3 then
                      begin
                        if CommonSet.Test=0 then
                        begin

                          quSpecVn1.Active:=False;
                          quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
                          quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
                          quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
                          quSpecVn1.Active:=True;

                          quSpecVn1.First;  //��������� ����� ���  � ��������� ��������������
                          while not quSpecVn1.Eof do
                          begin
                            prAddTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(-1)*quSpecVn1Kol.AsFloat);

                            if (quTTnVnDepart.AsInteger<>quTTnVnFlowDepart.AsInteger) and (pos('��������',quTTnVnNameD2.AsString)=0) then //������ ������ - ����� ������
                            begin
                              prAddTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);
                            end;

                            quSpecVn1.Next;
                          end;
                          quSpecVn1.Active:=False;
                        end;
                      end;
                      quTTNVn.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs4.ViewDocsVN.EndUpdate;
                  end;

                  Memo1.Lines.Add('     - ���������� ..'); delay(10);

                  if CommonSet.Test=0 then
                  begin
                    ptCn.Active:=False;
                    ptCn.TableName:='cn'+FormatDateTime('yyyymm',iCurD);
                    try
                      ptCn.Active:=True;
                      ptCn.CancelRange;
                      CurD:=iCurD;
                      ptCn.SetRange([CurD],[CurD]);
                      ptCn.First;

                      iC:=0;
                      while not ptCn.Eof do
                      begin
                        StrWk:=FormatDateTime('mmdd',iCurD);
                        sY:=FormatDateTime('yy',iCurD);
                        iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

                        prAddTMoveId(ptCnDepart.AsInteger,ptCnCode.AsInteger,iCurD,iD,7,(-1)*ptCnQuant.AsFloat);

                        ptCn.Next; inc(iC);
                        if iC mod 100 = 0 then
                        begin
                          fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                          delay(20);
                        end;
                      end;
                    except
                      ptCn.Active:=False;
                    end;
                  end;
                  
                  Memo1.Lines.Add('     - �������������� ..'); delay(10);

                  fmDocs5.ViewDocsInv.BeginUpdate;
                  try
                    quTTNInv.Active:=False;
                    quTTNInv.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNInv.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNInv.Active:=True;

                    quTTnInv.First; iC:=0;
                    while not quTTnInv.Eof do
                    begin
                      if quTTnInvStatus.AsString='�' then
                      begin
                        iDep:=quTTnInvDepart.AsInteger;

                        iT:=quTTnInvxDopStatus.AsInteger;
                        Memo1.Lines.Add('     - '+its(quTTnInvInventry.AsInteger)+' ('+its(iT)+')'); delay(10);

                        if CommonSet.Test=0 then
                        begin
                          if iT=0 then
                          begin
                            quSpecInv.Active:=False;
                            quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
                            quSpecInv.ParamByName('ID').AsInteger:=quTTnInvInventry.AsInteger;
                            quSpecInv.Active:=True;

                            iC:=0;
                            quSpecInv.First;  //��������� ����� ���  � ��������� ��������������
                            while not quSpecInv.Eof do
                            begin
                              if (quSpecInvItem.AsInteger>0) then
                                rRemn:=prFindTRemnDepD(quSpecInvItem.AsInteger,iDep,Trunc(quTTnInvDateInventry.AsDateTime))
                              else rRemn:=0;

{                              if abs(quSpecInvQuantActual.AsFloat-rRemn)>0.001 then
                              begin
// ������� �� ���� � ��� ���� ������ �������  prDelTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
                                prAddTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-rRemn));
                              end;
}
                             //���������� �� ������� ���� � ��������
                              prAddTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,re(quSpecInvQuantActual.AsFloat-rRemn));

                              quSpecInv.Next;
                              inc(iC);
                              if iC mod 100 = 0 then
                              begin
                                fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                                delay(20);
                              end;
                            end;
                            quSpecInv.Active:=False;
                          end;
                          if iT=1 then
                          begin
                            if ptInvLine1.Active=False then ptInvLine1.Active:=True;
                            ptInvLine1.Refresh;
                            ptInvLine1.CancelRange;
                            ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
                            iC:=0;
                            ptInvLine1.First;
                            while not ptInvLine1.Eof do
                            begin
                              if (ptInvLine1ITEM.AsInteger>0) then
                                rRemn:=prFindTRemnDepD(ptInvLine1ITEM.AsInteger,iDep,Trunc(quTTnInvDateInventry.AsDateTime))
                              else rRemn:=0;

                       //       if abs(ptInvLine1QUANTF.AsFloat-rRemn)>0.001 then
                       //       begin

                              //���������� �� ������� ���� � ��������

                              prAddTMoveId(quTTnInvDepart.AsInteger,ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,Re(ptInvLine1QUANTF.AsFloat-rRemn));
                       //     end;

                              ptInvLine1.Next;
                              if iC mod 100 = 0 then
                              inc(iC);
                              begin
                                fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                                delay(20);
                              end;
                            end;
                          end;
                        end;
                      end;
                      quTTnInv.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs5.ViewDocsInv.EndUpdate;
                  end;

                  Memo1.Lines.Add('   ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD)+'  ��.'); delay(10);

                  Memo1.Lines.Add('');delay(10);
                end;
              finally
                cxButton1.Enabled:=True;
              end;
              Memo1.Lines.Add('��.');
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acTestMoveVinExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk,sY:String;
    iC,Id:INteger;
    CurD:TDateTime;
begin
//�������� �������������� �� �������
  if MessageDlg('�������� �������������� ��������� ������, ����� ���� ��� ���������� ������, �������� ��������?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
  begin
    if MessageDlg('�������, ��� ����� �������� ��������? (����� ���� ��� ���������� ������)', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmMC do
      with dmMt do
      begin
        fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; //���� ����� �� ���������
        fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
        fmPeriod1.Label3.Caption:='';
        fmPeriod1.ShowModal;
        if fmPeriod1.ModalResult=mrOk then
        begin
          DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
          DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

          if DateB=0 then
          begin
            ShowMessage('������ ������.');
          end else
          begin

            if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
            else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

            fmSt.Memo1.Clear;
            fmSt.Show;
            with fmSt do
            begin
              cxButton1.Enabled:=False;
              Memo1.Lines.Add('������ �������� �������������� ('+StrWk+') ... �����.');
              try
                Memo1.Lines.Add('');
                Memo1.Lines.Add('   ������ ������ ('+StrWk+') ... �����.');

                ptCM.Active:=False;
                ptCG.Active:=False;
                ptCM1.Active:=False;
                ptCM2.Active:=False;

              {  ptCM2.Active:=True;
                ptCM2.CancelRange;
                ptCM2.SetRange([fmPeriod1.cxDateEdit1.Date],[fmPeriod1.cxDateEdit2.Date]);
                ptCM2.First;
                iC:=0;
                while not ptCM2.Eof do
                begin
                  ptCM2.Delete;
                  inc(iC);
                  if iC mod 100 = 0 then
                  begin
                    fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                    delay(20);
                  end;
                end;
                ptCM2.Active:=False;   }

                Memo1.Lines.Add('   ��� ������.');
                Memo1.Lines.Add('');

                for iCurD:=DateB to DateE do
                begin //������������ �� �����
                  Memo1.Lines.Add('   ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD)); delay(100);

                  Memo1.Lines.Add('     - ������� ..'); delay(10);

                  fmDocs1.ViewDocsIn.BeginUpdate;
                  try
                    quTTNIn.Active:=False;
                    quTTNIn.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNIn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNIn.Active:=True;

                    quTTNIn.First; iC:=0;
                    while not quTTNIn.Eof do
                    begin
                      if (quTTnInAcStatus.AsInteger=3) and (quTTnInDepart.AsInteger=4) then
                      begin
                        quSpecIn1.Active:=False;
                        quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                        quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                        quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                        quSpecIn1.Active:=True;

                        quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
                        while not quSpecIn1.Eof do
                        begin
                          prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                          quSpecIn1.Next;
                        end;
                        quSpecIn1.Active:=False;


                        if quTTnInSummaTara.AsFloat<>0 then
                        begin //�������� �� ����
                          quSpecInT.Active:=False;
                          quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                          quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                          quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                          quSpecInT.Active:=True;

                          quSpecInT.First;  // ��������������
                          while not quSpecInT.Eof do
                          begin
                            prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);
                            quSpecInT.Next;
                          end;
                          quSpecInT.Active:=False;
                        end;
                      end;
                      quTTNIn.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs1.ViewDocsIn.EndUpdate;
                  end;

                  Memo1.Lines.Add('     - �������� ..'); delay(10);
                  fmDocs2.ViewDocsOut.BeginUpdate;
                  try
                    quTTNOut.Active:=False;
                    quTTNOut.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNOut.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNOut.Active:=True;

                    quTTNOut.First; iC:=0;
                    while not quTTNOut.Eof do
                    begin
                      if (quTTNOutAcStatus.AsInteger=3) and (quTTnOutDepart.AsInteger=4) then
                      begin
                        quSpecOut1.Active:=False;
                        quSpecOut1.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                        quSpecOut1.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                        quSpecOut1.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                        quSpecOut1.Active:=True;

                        quSpecOut1.First;  //��������� ����� ���  � ��������� ��������������
                        while not quSpecOut1.Eof do
                        begin
                          prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut1Kol.AsFloat*-1));

                          quSpecOut1.Next;
                        end;
                        quSpecOut1.Active:=False;

                        if quTTnOutSummaTara.AsFloat<>0 then
                        begin //�������� �� ����
                          quSpecOutT.Active:=False;
                          quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                          quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                          quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                          quSpecOutT.Active:=True;

                          quSpecOutT.First;  // ��������������
                          while not quSpecOutT.Eof do
                          begin
                            prAddTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1),quSpecOutTCenaTara.AsFloat);

                            quSpecOutT.Next;
                          end;
                          quSpecOutT.Active:=False;
                        end;
                      end;
                      quTTNOut.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs2.ViewDocsOut.EndUpdate;
                  end;

                  Memo1.Lines.Add('     - ����������� ..'); delay(10);

                  fmDocs4.ViewDocsVN.BeginUpdate;
                  try
                    quTTNVn.Active:=False;
                    quTTNVn.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNVn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNVn.Active:=True;

                    quTTNVn.First; iC:=0;
                    while not quTTNVn.Eof do
                    begin
                      if (quTTnVnAcStatusP.AsInteger=3)and(quTTnVnDepart.AsInteger=4) then
                      begin
                        quSpecVn1.Active:=False;
                        quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
                        quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
                        quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
                        quSpecVn1.Active:=True;

                        quSpecVn1.First;  //��������� ����� ���  � ��������� ��������������
                        while not quSpecVn1.Eof do
                        begin
                          prAddTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(-1)*quSpecVn1Kol.AsFloat);

                          if (quTTnVnDepart.AsInteger<>quTTnVnFlowDepart.AsInteger) and (pos('��������',quTTnVnNameD2.AsString)=0) then //������ ������ - ����� ������
                          begin
                            prAddTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);
                          end;

                          quSpecVn1.Next;
                        end;
                        quSpecVn1.Active:=False;
                      end;
                      quTTNVn.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs4.ViewDocsVN.EndUpdate;
                  end;

                  Memo1.Lines.Add('     - ���������� ..'); delay(10);

                  ptCn.Active:=False;
                  ptCn.TableName:='cn'+FormatDateTime('yyyymm',iCurD);
                  try
                    ptCn.Active:=True;
                    ptCn.CancelRange;
                    CurD:=iCurD;
                    ptCn.SetRange([CurD],[CurD]);
                    ptCn.First;

                    iC:=0;
                    while not ptCn.Eof do
                    begin
                      if ptCnDepart.AsInteger=4 then
                      begin
                        StrWk:=FormatDateTime('mmdd',iCurD);
                        sY:=FormatDateTime('yy',iCurD);
                        iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

                        prAddTMoveId(ptCnDepart.AsInteger,ptCnCode.AsInteger,iCurD,iD,7,(-1)*ptCnQuant.AsFloat);
                      end;
                      ptCn.Next; inc(iC);
                      if iC mod 100 = 0 then
                      begin
                        fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                        delay(20);
                      end;
                    end;
                  except
                    ptCn.Active:=False;
                  end;
                  Memo1.Lines.Add('     - �������������� ..'); delay(10);

                  fmDocs5.ViewDocsInv.BeginUpdate;
                  try
                    quTTNInv.Active:=False;
                    quTTNInv.ParamByName('DATEB').AsDate:=iCurD;
                    quTTNInv.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
                    quTTNInv.Active:=True;

                    quTTnInv.First; iC:=0;
                    while not quTTnInv.Eof do
                    begin
                      if (quTTnInvStatus.AsString='�')and(quTTnInvDepart.AsInteger=4) then
                      begin
                        quSpecInv.Active:=False;
                        quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
                        quSpecInv.ParamByName('ID').AsInteger:=quTTnInvInventry.AsInteger;
                        quSpecInv.Active:=True;

                        iC:=0;
                        quSpecInv.First;  //��������� ����� ���  � ��������� ��������������
                        while not quSpecInv.Eof do
                        begin
                          if abs(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat)>0.001 then
                          begin
// ������� �� ���� � ��� ���� ������ �������  prDelTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
                            prAddTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
                          end;
                          quSpecInv.Next;
                          inc(iC);
                          if iC mod 100 = 0 then
                          begin
                            fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                            delay(20);
                          end;
                        end;
                        quSpecInv.Active:=False;
                      end;
                      quTTnInv.Next;
                      inc(iC);
                      fmSt.StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
                    end;
                  finally
                    fmDocs5.ViewDocsInv.EndUpdate;
                  end;

                  Memo1.Lines.Add('   ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD)+'  ��.'); delay(10);

                  Memo1.Lines.Add('');delay(10);
                end;
              finally
                cxButton1.Enabled:=True;
              end;
              Memo1.Lines.Add('��.');
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acTovPeriodExecute(Sender: TObject);
var
  y1,y2:integer;
  m1,m2:integer;
  q:integer;
begin
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;

  fmPeriod.cxLabel3.Visible:=False;
  fmPeriod.cxButtonEdit1.Visible:=false;
  fmPeriod.cxCheckBox1.Visible:=false;

  fmPeriod.cxCheckBox7.Checked:=True;
  fmPeriod.cxLabel9.Visible:=False;
  fmPeriod.cxCheckBox7.Visible:=False;

  fmPeriod.ShowModal;

  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);

    y1:=strtoint(FormatDateTime('yyyy',CommonSet.DateBeg));
    y2:=strtoint(FormatDateTime('yyyy',CommonSet.DateEnd));
    m1:=strtoint(FormatDateTime('mm',CommonSet.DateBeg));
    m2:=strtoint(FormatDateTime('mm',CommonSet.DateEnd));

    closeTe(fmRepTovarPeriod.mPeriod);
    fmRepTovarPeriod.mPeriod.Active:=True;

    while y1<=y2 do
      begin

        while (y1*100+m1)<=(y2*100+m2) do
          begin
            fmRepTovarPeriod.mPeriod.Append;
            if m1<10 then fmRepTovarPeriod.mPeriodDat.AsString:=inttostr(y1)+'0'+inttostr(m1)
                     else fmRepTovarPeriod.mPeriodDat.AsString:=inttostr(y1)+inttostr(m1);
            fmRepTovarPeriod.mPeriod.Post;

            if m1<12 then inc(m1) else begin m1:=1;inc(y1) end;
          end;
        inc(y1);
      end;


  fmRepTovarPeriod.mPeriod.First;
  dmmc.quRepTovarPeriod.Active:=False;
  dmmc.quRepTovarPeriod.SQL.Clear;
  q:=0;
  while not fmRepTovarPeriod.mPeriod.Eof do
    begin
      //ShowMessage(fmRepTovarPeriod.mPeriodDat.AsString);
      if q>0 then dmmc.quRepTovarPeriod.SQL.Add('Union all');
      dmmc.quRepTovarPeriod.SQL.Add('select   Rdate');
      dmmc.quRepTovarPeriod.SQL.Add('		,Code');
      dmmc.quRepTovarPeriod.SQL.Add('		,Quant');
      dmmc.quRepTovarPeriod.SQL.Add('		,RSumCor');
      dmmc.quRepTovarPeriod.SQL.Add('		,gd.Name as GoodsName');
      dmmc.quRepTovarPeriod.SQL.Add('		,gr3.Name as gr3Name');
      dmmc.quRepTovarPeriod.SQL.Add('		,gr2.Name as gr2Name');
      dmmc.quRepTovarPeriod.SQL.Add('		,gr1.Name as gr1Name');
      dmmc.quRepTovarPeriod.SQL.Add('		,d.Name as DepartName');
      dmmc.quRepTovarPeriod.SQL.Add('		,br.NameBrand');
      dmmc.quRepTovarPeriod.SQL.Add('		,cat.SID');
      dmmc.quRepTovarPeriod.SQL.Add('from "cn'+fmRepTovarPeriod.mPeriodDat.AsString+'" cn');
      dmmc.quRepTovarPeriod.SQL.Add('left join Goods gd on cn.Code=gd.ID');
      dmmc.quRepTovarPeriod.SQL.Add('left join SubGroup gr3 on gr3.ID=gd.GoodsGroupId');
      dmmc.quRepTovarPeriod.SQL.Add('left join SubGroup gr2 on gr2.ID=gr3.GoodsGroupID');
      dmmc.quRepTovarPeriod.SQL.Add('left join SubGroup gr1 on gr1.ID=gr2.GoodsGroupID');
      dmmc.quRepTovarPeriod.SQL.Add('left join Depart d on d.ID=cn.Depart');
      dmmc.quRepTovarPeriod.SQL.Add('left join A_Brands br on br.ID=gd.v01');
      dmmc.quRepTovarPeriod.SQL.Add('left join A_Categ cat on cat.ID=gd.v02');

      dmmc.quRepTovarPeriod.SQL.Add('where Rdate between '''+ds(CommonSet.DateBeg)+''' and '''+ds(CommonSet.DateEnd)+''' ');


      if ObPar.iDepart>0 then
         dmmc.quRepTovarPeriod.SQL.Add('and cn.Depart = '+its(ObPar.iDepart)+'');

      if ObPar.iGroup>0 then
         dmmc.quRepTovarPeriod.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

      if ObPar.iSGroup>0 then
         dmmc.quRepTovarPeriod.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

      if ObPar.iBrand>0 then
         dmmc.quRepTovarPeriod.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

      if ObPar.iCat>0 then
         dmmc.quRepTovarPeriod.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

      fmRepTovarPeriod.mPeriod.Next; inc(q);
    end;



  fmRepTovarPeriod.Show;
  fmRepTovarPeriod.Memo1.Text:='���� ������������ ������. �����';
  delay(10);
  fmRepTovarPeriod.PivotGridTovarPeriod.BeginUpdate;
    dmmc.quRepTovarPeriod.Active:=True;
    //dmmc.quRepTovarPeriod.SQL.SaveToFile('c:\z.txt');
  fmRepTovarPeriod.PivotGridTovarPeriod.EndUpdate;
  fmRepTovarPeriod.Memo1.Text:='������������ ���������';
  end;

  fmPeriod.cxLabel3.Visible:=True;
  fmPeriod.cxButtonEdit1.Visible:=True;
  fmPeriod.cxCheckBox1.Visible:=True;

  fmPeriod.Release;

end;


procedure TfmMainMCryst.acStatusExecute(Sender: TObject);
begin
  fmCateg.Show;
end;

procedure TfmMainMCryst.acRecalcCassirExecute(Sender: TObject);
Var DateB,DateE:INteger;
    StrWk:String;
begin
// �������� ������� �� ��������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    with dmMT do
    begin
      DateB:=Trunc(CommonSet.DateBeg);
      DateE:=Trunc(CommonSet.DateEnd);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
        try
          Memo1.Lines.Add(fmt+'  -- ������ ������.');
          if ptCassir.Active=False then ptCassir.Active:=True;
          ptCassir.CancelRange;
          ptCassir.SetRange([CommonSet.DateBeg],[CommonSet.DateEnd]);
          ptCassir.First;
          while not ptCassir.Eof do ptCassir.Delete;

          Memo1.Lines.Add(fmt+'  -- ��������� ������ (���).');

          quSel.Active:=False;
          quSel.SQL.Clear;
          quSel.SQL.Add('select sd.DateSale, sd.CassaNumber,sd.Employee,SUM(sd.Summa) as CSUMMA');
          quSel.SQL.Add('from "SaleByDepart" sd');
          quSel.SQL.Add('where sd.DateSale>='''+ds(DateB)+'''');
          quSel.SQL.Add('and sd.DateSale<='''+ds(DateE)+'''');
          quSel.SQL.Add('group by sd.DateSale, sd.CassaNumber,sd.Employee');
          quSel.SQL.Add('order by sd.DateSale');
          quSel.Active:=True;

          Memo1.Lines.Add(fmt+'  -- ����� ������ (���).');

          quSel.First;
          while not quSel.Eof do
          begin
            ptCassir.Append;
            ptCassirDateDay.AsDateTime:=quSel.FieldByName('DateSale').asDateTime;
            ptCassirCassaNumber.AsInteger:=quSel.FieldByName('CassaNumber').asINteger;
            ptCassirCodeKadr.AsInteger:=quSel.FieldByName('Employee').asINteger;
            ptCassirDepart.AsInteger:=0;
            ptCassirPrihod.AsFloat:=rv(quSel.FieldByName('CSUMMA').asFloat);
            ptCassirSumma.AsFloat:=rv(quSel.FieldByName('CSUMMA').asFloat);
            ptCassir.Post;

            quSel.Next;
          end;

          quSel.Active:=False;

          Memo1.Lines.Add(fmt+'  -- ��������� ������ (�/���).');

          quSel.Active:=False;
          quSel.SQL.Clear;
          quSel.SQL.Add('select sd.DateSale, sd.CassaNumber,sd.Employee,SUM(sd.Summa) as CSUMMA');
          quSel.SQL.Add('from "SaleByDepartBn" sd');
          quSel.SQL.Add('where sd.DateSale>='''+ds(DateB)+'''');
          quSel.SQL.Add('and sd.DateSale<='''+ds(DateE)+'''');
          quSel.SQL.Add('group by sd.DateSale, sd.CassaNumber,sd.Employee');
          quSel.SQL.Add('order by sd.DateSale');
          quSel.Active:=True;

          Memo1.Lines.Add(fmt+'  -- ����� ������ (�/���).');

          quSel.First;
          while not quSel.Eof do
          begin
            if ptCassir.FindKey([quSel.FieldByName('DateSale').asDateTime,0,quSel.FieldByName('CassaNumber').asINteger,quSel.FieldByName('Employee').asINteger]) then
            begin
              ptCassir.Edit;
              ptCassirPrihod.AsFloat:=rv(ptCassirPrihod.AsFloat+quSel.FieldByName('CSUMMA').asFloat);
              ptCassirSumma.AsFloat:=rv(ptCassirSumma.AsFloat+quSel.FieldByName('CSUMMA').asFloat);
              ptCassir.Post;
            end else
            begin
              ptCassir.Append;
              ptCassirDateDay.AsDateTime:=quSel.FieldByName('DateSale').asDateTime;
              ptCassirCassaNumber.AsInteger:=quSel.FieldByName('CassaNumber').asINteger;
              ptCassirCodeKadr.AsInteger:=quSel.FieldByName('Employee').asINteger;
              ptCassirDepart.AsInteger:=0;
              ptCassirPrihod.AsFloat:=rv(quSel.FieldByName('CSUMMA').asFloat);
              ptCassirSumma.AsFloat:=rv(quSel.FieldByName('CSUMMA').asFloat);
              ptCassir.Post;
            end;

            quSel.Next;
          end;

          quSel.Active:=False;




          ptCassir.Active:=False;
        finally
          Memo1.Lines.Add(fmt+'������� ��������.');
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acCalcPostAssortimentExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk:String;
    iC:INteger;
    dCurD:TDateTime;
begin
  //����������� ���������� �����������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    with dmMT do
    begin
//      DateB:=Trunc(CommonSet.DateBeg);
//      DateE:=Trunc(CommonSet.DateEnd);
      DateB:=Trunc(date-365);
      DateE:=Trunc(date);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
        try
          Memo1.Lines.Add(fmt+'  ������ ...');
          ptCliGoods.Active:=False; ptCliGoods.Active:=True;
          ptCliGoods.First;  iC:=0;
          while not ptCliGoods.Eof do
          begin
            ptCliGoods.delete;
            inc(iC);
            if iC mod 500 = 0 then
            begin
              delay(100);
              StatusBar1.Panels[0].Text:=its(iC);
            end;
          end;

          delay(100);

          Memo1.Lines.Add(fmt+'  ������� ...');

          ptTTNIn.Active:=False; ptTTNIn.Active:=True;
          ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';

          ptInLn.Active:=False; ptINLn.Active:=True;

          for iCurD:=DateB to DateE do
          begin
            Memo1.Lines.Add(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
            dCurD:=iCurD;
            ptTTNIn.CancelRange;
            ptTTNIn.SetRange([dCurD],[dCurD]);
            ptTTNIn.First;  iC:=0;
            while not ptTTNIn.Eof do
            begin
              ptInLn.CancelRange;
              ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
              ptInLn.First;
              while not ptInLn.Eof do
              begin
                if ptCliGoods.FindKey([ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,ptInLnCodeTovar.AsInteger]) then
                begin
                  ptCliGoods.Edit;
                  ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptCliGoods.Post;
                end else
                begin
                  ptCliGoods.Append;
                  ptCliGoodsGoodsID.AsInteger:=ptInLnCodeTovar.AsInteger;
                  ptCliGoodsClientObjTypeID.AsInteger:=ptTTNInIndexPost.AsInteger;
                  ptCliGoodsClientID.AsInteger:=ptTTNInCodePost.AsInteger;
                  ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptCliGoods.Post;
                end;
                ptInLn.Next;
              end;

              ptTTNIn.Next; inc(iC);
              if iC mod 10 = 0 then
              begin
                StatusBar1.Panels[0].Text:=its(iC);
                delay(100);
              end;
            end;
          end;
        finally
          ptTTNIn.Active:=False;
          ptInLn.Active:=False;
          ptCliGoods.Active:=False;

          Memo1.Lines.Add(fmt+'������� ��������.');
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acRepTovZapasExecute(Sender: TObject);
Var rQr,rSpeedDay,rQDay:Real;
    iStep,iC,iGr,i:INteger;
begin
//�� ���� ��������
  if fmclients.Showing then fmclients.Close;
  fmPeriodR:=TfmPeriodR.Create(Application);
  fmPeriodR.ShowModal;
  if fmPeriodR.ModalResult=mrOk then
  begin
    //�������
    with dmMc do
    with dmMt do
    begin
      fmRDayVed.PBar1.Position:=0;
      fmRDayVed.Caption:='�������  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmRDayVed.ptDaysCols(True);

      fmRDayVed.Show;
      fmRDayVed.PBar1.Visible:=True;
      with fmRDayVed do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewRDayVed.BeginUpdate;
        CloseTe(fmRDayVed.teRDayVed);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;

        fmRDayVed.PBar1.Position:=10; delay(10);

        quRepR.Active:=False;
        quRepR.SQL.Clear;
        quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');
        quRepR.SQL.Add('from "Goods" gd');
        quRepR.SQL.Add('where  Status<100');

        if ObPar.iPost>0 then
        begin
{
          quRepR.SQL.Add('and gd.ID in (select CodeTovar from "TTNInLn"');
          quRepR.SQL.Add('where DateInvoice>='''+ds(Date-150)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepR.SQL.Add('group by CodeTovar');
          quRepR.SQL.Add(')');
}
          quRepR.SQL.Add('and gd.ID in (select GoodsID from "GdsClient"');
          quRepR.SQL.Add('where ClientObjTypeID='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepR.SQL.Add(')');

        end;

        if ObPar.iSGroup>0 then quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepR.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepR.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepR.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepR.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepR.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;


        quRepR.SQL.Add('order by gd.ID');
        quRepR.Active:=True;

        fmRDayVed.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������. ('+its(quRepR.RecordCount)+')'); delay(10);

        quRepR.First;

        iStep:=quRepR.RecordCount div 60;
        if iStep=0 then iStep:=quRepR.RecordCount;
        iC:=0;

        while not quRepR.Eof do
        begin
          rQr:=prFindTabSpeedRemn(quRepRID.AsInteger,0,rSpeedDay,rQDay);
          prFillReal10(quRepRID.AsInteger,Trunc(Date-1),10);

          teRDayVed.Append;
          teRDayVedIdCode1.AsInteger:=quRepRID.AsInteger;
          teRDayVedNameC.AsString:=quRepRName.AsString;
          teRDayVediM.AsInteger:=quRepREdIzm.AsInteger;
          teRDayVedQR.AsFloat:=rQr;
          teRDayVedIdGroup.AsInteger:=quRepRGoodsGroupID.AsInteger;
          teRDayVedIdSGroup.AsInteger:=0;
          teRDayVedNameGr1.AsString:='';
          teRDayVedNameGr2.AsString:='';
          teRDayVedNameGr3.AsString:='';
          teRDayVediBrand.AsInteger:=quRepRV01.AsInteger;
          teRDayVediCat.AsInteger:=quRepRV02.AsInteger;
          teRDayVedPrice.AsFloat:=quRepRCena.AsFloat;
          teRDayVedAvrSpeed.AsFloat:=rSpeedDay;
          teRDayVedQRDay.AsFloat:=rQDay;

          iGr:=quRepRGoodsGroupID.AsInteger;

          if teClass.Locate('Id',iGr,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teRDayVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRDayVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRDayVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;

          if quRepRV04.AsInteger=0 then teRDayVedsTop.AsString:=' ' else teRDayVedsTop.AsString:='T';
          if quRepRV05.AsInteger=0 then teRDayVedsNov.AsString:=' ' else
          begin
            teRDayVedsNov.AsString:='N';
            teRDayVedDayN.AsInteger:=RoundEx(Date-prFindTFirstDate(quRepRID.AsInteger));
            if teRDayVedDayN.AsInteger>91 then teRDayVedDayN.AsInteger:=0;
          end;

          for i:=1 to 10 do teRDayVed.FieldByName('D'+its(i-1)).AsFloat:=arRD[i];

          teRDayVed.Post;

          quRepR.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmRDayVed.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        quRepR.Active:=True;

        fmRDayVed.PBar1.Position:=100; delay(100);

        quRepR.Active:=False;
        ViewRDayVed.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmRDayVed.PBar1.Visible:=False;
      end;
    end;
    fmPeriodR.Release;
  end else
    fmPeriodR.Release;
end;

procedure TfmMainMCryst.acNoUserExecute(Sender: TObject);
Var sName:String;
    LDate:TDateTime;
    iC,iD:INteger;
    rQ:Real;
    iStatus,iNov,iLastDayIn:INteger;
begin
  //������� � ��������������
  with dmMt do
  begin
    if Pos('�����',Person.Name)>0 then
    if MessageDlg('�������� �������� �������������� �� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='������������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      if taCards.Active=False then taCards.Active:=True;

      iD:=0;
      iC:=0;

      taCards.First;
      while not taCards.Eof do
      begin
        iNov:=0;


        if taCardsStatus.asinteger<100 then
        begin
          rQ:=prFindLastDateRemn(taCardsID.AsInteger,LDate);
//              rQ:=prFindTabRemn(taCardsID.AsInteger);
//              if abs(rQ-taCardsReserv1.AsFloat)>0.1 then prSetR(taCardsID.AsInteger,rQ);
          iStatus:=1;

          if abs(rQ)<0.002 then rQ:=0;

          iLastDayIn:=prFindTFirstDate(taCardsID.AsInteger);
          if iLastDayIn=0 then iNov:=1 //�������
           else
            if (Date-iLastDayIn)< CommonSet.DaysNoMove then iNov:=1; //91 �� ���������

          sName:=OemToAnsiConvert(taCardsName.AsString);
          if sName[1]='*' then
          begin
            if abs(rQ)<0.002 then
            begin
              if (Date-LDate)> CommonSet.DaysNoMove then   //91 �� ���������
              begin
                iStatus:=255;  //����� ����� ���-��
                inc(iD);
              end;
            end;
          end else
          begin
            if CommonSet.Single=1 then //���������� �� �������� ���� ����� ��� �������� � ��������������
            begin
              if abs(rQ)<0.002 then
              begin
                if (Date-LDate)> CommonSet.DaysNoMove+100 then   // +100 �� ������ ������
                begin
                  iStatus:=255;  //����� ����� ���-��
                  inc(iD);
                end;
              end;
            end;
          end;
        end else
        begin
          iStatus:=255;
          rQ:=0;
        end;

        taCards.Edit;
        taCardsStatus.asinteger:=iStatus;
        taCardsReserv1.AsFloat:=rQ;
        taCardsV05.asinteger:=iNov;
        taCards.Post;
       
        taCards.Next;
        inc(iC);
        if iC mod 100 = 0 then
        begin
          StatusBar1.Panels[0].Text:=IntToStr(iC);
          StatusBar1.Panels[1].Text:=IntToStr(iD);
          delay(10);
        end;
      end;

      StatusBar1.Panels[0].Text:='Ok'; delay(10);
    end;
  end;
end;

procedure TfmMainMCryst.acRepHourExecute(Sender: TObject);
begin
  //���������� �� �����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit2.Date);
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      StatusBar1.Panels[0].Text:='����� ���� ������������.'; Delay(10);

      fmRealH.ViewRealH.BeginUpdate;
      quRepHour.Active:=False;
      quRepHour.ParamByName('DATEB').Value:=Trunc(CommonSet.DateBeg);
      quRepHour.ParamByName('DATEE').Value:=Trunc(CommonSet.DateEnd);
      quRepHour.Active:=True;
      fmRealH.ViewRealH.EndUpdate;

      StatusBar1.Panels[0].Text:=''; Delay(10);

      fmRealH.Caption:='���������� �� ����� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+'     �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd);
      fmRealH.Show;
    end;
  end;
end;

procedure TfmMainMCryst.acRepCliSumExecute(Sender: TObject);
begin
  // ����� �� ����������� � ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod1.cxDateEdit2.Date);
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      StatusBar1.Panels[0].Text:='����� ���� ������������.'; Delay(10);

      quRepSumCli.Active:=False;
      quRepSumCli.ParamByName('DATEB').Value:=Trunc(CommonSet.DateBeg);
      quRepSumCli.ParamByName('DATEE').Value:=Trunc(CommonSet.DateEnd);
      quRepSumCli.Active:=True;


      with fmGraf3 do
      begin
        Chart1.Title.Text.Clear;
        Chart1.Title.Text.Add('���������� �� ����������� �� ������ � '+FormatDateTime('dd mmmm yyyy',CommonSet.DateBeg)+'     �� '+FormatDateTime('dd mmmm yyyy',CommonSet.DateEnd));
        Chart1.LeftAxis.Title.Caption:='���-�� �����������.';
        Chart1.BottomAxis.Title.Caption:='�����.';
        Series1.Active:=False;
        Series2.Active:=False;
        Series3.Active:=False;
        Series4.Active:=False;
        Series5.Active:=False;
        Series6.Active:=False;
        Series7.Active:=False;
        Series8.Active:=False;
        Series9.Active:=False;
        Series10.Active:=False;
        Series11.Active:=False;
        Series12.Active:=False;
        Series13.Active:=False;
        Series14.Active:=False;
        Series15.Active:=False;

        quRepSumCli.First;

        Series1.Active:=True;

        Series1.AddXY(1,quRepSumCliS20.AsInteger,'�� 20 ���.');
        Series1.AddXY(2,quRepSumCliS50.AsInteger,'�� 50 ���.');
        Series1.AddXY(3,quRepSumCliS100.AsInteger,'�� 100 ���.');
        Series1.AddXY(4,quRepSumCliS200.AsInteger,'�� 200 ���.');
        Series1.AddXY(5,quRepSumCliS500.AsInteger,'�� 500 ���.');
        Series1.AddXY(6,quRepSumCliS1000.AsInteger,'�� 1000 ���.');
        Series1.AddXY(7,quRepSumCliSMORE.AsInteger,'����� 1000 ���.');
      end;
      fmGraf3.Show;
    end;
  end;
end;

procedure TfmMainMCryst.acDelCardsExecute(Sender: TObject);
Var LDate,CurDate:TDateTime;
    iC,iD:INteger;
    bDel:Boolean;
begin
  //������� � ��������������
  with dmMt do
  begin
    if MessageDlg('������� �������������� �������� (500 ����)?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='������������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      if taCards.Active=False then taCards.Active:=True;

      iD:=0;
      iC:=0;
      taCards.First;
      while not taCards.Eof do
      begin
        bDel:=False;
        if taCardsStatus.asinteger>=100 then
        begin
          if abs(prFindLastDateRemn(taCardsID.AsInteger,LDate))<0.0001 then //����� ����� ���-��
          begin  //��������� ����� ���� ��������� ��������
            CurDate:=Date;
            if (CurDate-LDate)>500 then //������
            begin
              bDel:=True;
              inc(iD);
              taCards.Delete;
            end;
          end
        end;
        if bDel=False then taCards.Next;
        inc(iC);
        if iC mod 100 = 0 then
        begin
          StatusBar1.Panels[0].Text:=IntToStr(iC);
          StatusBar1.Panels[1].Text:=IntToStr(iD);
          delay(50);
        end;
      end;

      StatusBar1.Panels[0].Text:='Ok'; delay(10);
    end;
  end;
end;

procedure TfmMainMCryst.acDelMove0Execute(Sender: TObject);
Var iC,iD:INteger;
begin
  //�������� �������� ������������� ��������
  bStopDelMove:=False;
  with dmMT do
  begin
    if ptCG.Active=False then ptCG.Active:=True;
    if ptCM.Active=False then ptCM.Active:=True;
    if taCards.Active=False then taCards.Active:=True;

    iC:=0;
    iD:=0;

    ptCG.First;
    while (ptCG.eof=False) and (bStopDelMove=False) do
    begin
      if taCards.FindKey([ptCGITEM.AsInteger])=False then  //��� ����� �������� - �������
      begin
        ptCM.First;
        while not ptCM.Eof do ptCM.Delete;
        ptCG.Delete;

        inc(iD);

      end else ptCG.Next;

      delay(30);

      inc(iC);
      if iC mod 100 = 0 then
      begin
        StatusBar1.Panels[0].Text:=IntToStr(iC);
        StatusBar1.Panels[1].Text:=IntToStr(iD);
        delay(50);
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acDelStopExecute(Sender: TObject);
begin
  bStopDelMove:=True;
end;

procedure TfmMainMCryst.acZadListExecute(Sender: TObject);
begin
  //������ �������
  with dmMC do
  begin
    fmZadan.ViewZad.BeginUpdate;
    quZad.Active:=False;
    quZad.ParamByName('DATEB').AsDate:=Date-10;
    quZad.ParamByName('DATEE').AsDate:=Date;
    quZad.Active:=True;
    fmZadan.ViewZad.EndUpdate;
    fmZadan.Show;
  end;
end;

procedure TfmMainMCryst.TimerZadTimer(Sender: TObject);
Var iSt,iStTab:INteger;
    bDo:Boolean;
begin
  bDo:=True;
  if pos('BUH',Person.Name)>0 then bDo:=False;
  if pos('�����',Person.Name)>0 then bDo:=False;
  if pos('OPER CB',Person.Name)>0 then bDo:=False;
  if pos('OPERZAK',Person.Name)>0 then bDo:=False;
  if pos('Torg1',Person.Name)>0 then bDo:=False;

  if bDo then
  begin

  TimerZad.Enabled:=False;
  with dmMC do
  begin
    quZadSel.Active:=False;
    quZadSel.Active:=True;
    quZadSel.First;
    while not quZadSel.Eof do
    begin
      fmZadMessage.Label2.Caption:=quZadSelComment.AsString;
      fmZadMessage.Label3.Caption:='� '+quZadSelId.AsString;
      if quZadSelStatus.AsInteger=0 then
      begin
        fmZadMessage.cxButton1.Enabled:=True;
        fmZadMessage.cxButton2.Enabled:=False;
      end else
      begin
        fmZadMessage.cxButton1.Enabled:=False;
        fmZadMessage.cxButton2.Enabled:=True;
      end;
      fmZadMessage.ShowModal;
      iSt:=quZadSelStatus.AsInteger;
      if fmZadMessage.ModalResult=mrOk then inc(iSt);

      //����� �������� ������� ��� ��� ��������
      iStTab:=0;
      quZadRec.Active:=False;
      quZadRec.ParamByName('IDZ').AsInteger:=quZadSelId.AsInteger;
      quZadRec.Active:=True;
      if quZadRec.RecordCount>0 then iStTab:=quZadRecStatus.AsInteger;
      quZadRec.Active:=False;

      if iStTab<iSt then
      begin
        quZ.Active:=False;
        quZ.SQL.Clear;
        quZ.SQL.Add('Update "A_ZADANIYA" Set ');
        quZ.SQL.Add('Status='+its(iSt)+',');
        quZ.SQL.Add('PersonId='+its(Person.Id)+',');
        quZ.SQL.Add('PersonName='''+Person.Name+''',');
        quZ.SQL.Add('FormDate='''+ds(date)+''',');
        quZ.SQL.Add('FormTime='''+ts(time)+'''');
        quZ.SQL.Add('where Id='+its(quZadSelId.AsINteger));
        quZ.ExecSQL;
      end;

      break;
//      quZadSel.Next;
    end;
    quZadSel.Active:=False;
  end;
  TimerZad.Enabled:=True;

  end;
end;

procedure TfmMainMCryst.acTestRecalcMove1Execute(Sender: TObject);
Var iC,iD,iRes,iCode:INteger;
begin
//��������� ������������ ��������������
  with dmMt do
  begin
    iCode:=StrToINtDef(fmCards.TextEdit1.Text,0);
    if MessageDlg('����������� ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='�������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      if taCards.Active=False then taCards.Active:=True;

      iD:=0;
      iC:=0;
      iRes:=0;
      taCards.First;
      while not taCards.Eof do
      begin
        if (taCardsStatus.AsInteger<=100)and(taCardsID.AsInteger>=iCode)  then
        begin
          WriteHistory('  - '+its(iC)+'   '+its(taCardsID.AsInteger)+'   ������� '+its(iRes));
          if prReCalcTMoveLine(taCardsID.AsInteger)=1 then begin inc(iD); iRes:=1; end else begin iRes:=0; end;
        end;

        taCards.Next;
        inc(iC);
      //  if iC mod 100 = 0 then
      //  begin
        StatusBar1.Panels[0].Text:=IntToStr(iC);
        StatusBar1.Panels[1].Text:=IntToStr(iD);
        delay(20);
      //  end;
      end;

      StatusBar1.Panels[0].Text:='Ok'; delay(10);
    end;
  end;
end;

procedure TfmMainMCryst.acRecalcStExecute(Sender: TObject);
Var iStatus,iNov,iTop,iFirstDayIn,iC:INteger;
begin
 // �������� ��������
  with dmMT do
  begin
    if taCards.Active=False then taCards.Active:=True;
    taCards.First;
    iC:=0;
    while not taCards.Eof do
    begin
      if taCardsStatus.asinteger<100 then
      begin
        iStatus:=taCardsV02.asinteger;
        iNov:=0;
        iTop:=0;

        iFirstDayIn:=prFindTFirstDate(taCardsID.AsInteger);
        if iFirstDayIn=0 then iNov:=1
        else
          if (Date-iFirstDayIn)<91 then iNov:=1;

        if taCardsV02.AsInteger=3 then begin iStatus:=1; iTop:=1; end;
        if taCardsV02.AsInteger=5 then begin iStatus:=1; iTop:=0; end;
        if taCardsV02.AsInteger=6 then begin iStatus:=1; iTop:=0; end;
        if taCardsV02.AsInteger=7 then begin iStatus:=1; iTop:=0; end;
        if taCardsV04.asinteger=1 then iTop:=1;

        taCards.Edit;
        taCardsV02.asinteger:=iStatus;
        taCardsV04.asinteger:=iTop;
        taCardsV05.asinteger:=iNov;
        taCards.Post;
      end;
      taCards.Next;
      inc(iC);
      if iC mod 100=0 then
      begin
        StatusBar1.Panels[0].Text:=its(iC);
        delay(10);
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acRecalcSSInExecute(Sender: TObject);
Var DateB,DateE:INteger;
begin
  //�������� �������������

  fmPeriod1.cxDateEdit1.Date:=prGetTA;
  fmPeriod1.cxDateEdit2.Date:=Date;

  fmPeriod1.Label3.Caption:='�������� �� ��� ������� �� '+FormatDateTime('dd.mm.yyyy',prGetTA)+' !!!';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    with dmMC do
    with dmMT do
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

//      if (DateB>prGetTA)or(DateE<Trunc(Date)) then
      if (DateB=0)then
      begin
        showmessage('������������ ������ �������. ������� ������ ����� !!!');
      end else
      begin

        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;

        try
          fmDocs2.ViewDocsOut.BeginUpdate;
          fmDocs4.ViewDocsVn.BeginUpdate;
          fmDocs5.ViewDocsInv.BeginUpdate;

          prRecalcSS(DateB,DateE,fmSt.Memo1);
        finally
          fmDocs2.ViewDocsOut.EndUpdate;
          fmDocs4.ViewDocsVn.EndUpdate;
          fmDocs5.ViewDocsInv.EndUpdate;
        end;  

        fmSt.cxButton1.Enabled:=True;
      end; //prGetTA
    end;
  end;
end;

procedure TfmMainMCryst.acRepPribExecute(Sender: TObject);
Var rQr,rSumIn,rSumOut,rNac,rpNac:Real;
    iStep,iC,iGr:INteger;
    iDateB,iDateE:Integer;
begin
  //����� �� �������
  if fmclients.Showing then fmclients.Close;
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod.cxTextEdit1.Text:='';

  fmPeriod.cxCheckBox7.Checked:=True;
  fmPeriod.cxLabel9.Visible:=False;
  fmPeriod.cxCheckBox7.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);
    iDateB:=Trunc(CommonSet.DateBeg);
    iDateE:=Trunc(CommonSet.DateEnd);

    //�������
    with dmMc do
    with dmMt do
    begin
      fmPrib1.PBar1.Position:=0;
      fmPrib1.Caption:='����� �� ������� '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmPrib1.Show;
      fmPrib1.PBar1.Visible:=True;
      with fmPrib1 do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewPrib1.BeginUpdate;
        CloseTe(fmPrib1.teRPrib);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;

        fmPrib1.PBar1.Position:=10; delay(10);

        quRepPrib.Active:=False;
        quRepPrib.SQL.Clear;
        quRepPrib.SQL.Add('select');
        quRepPrib.SQL.Add('po.ARTICUL,po.IDSTORE,po.ITYPE,po.SINN,');
        quRepPrib.SQL.Add('gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.V03,gd.V04,gd.V05,gd.Cena,gd.NDS,');
        quRepPrib.SQL.Add('de.Name as NameDep,de.Status5,');
        quRepPrib.SQL.Add('SUM(po.QUANT) as QUANT,');
        quRepPrib.SQL.Add('SUM(po.SUMIN) as SUMIN,');
        quRepPrib.SQL.Add('SUM(po.SUMOUT) as SUMOUT');
        quRepPrib.SQL.Add('from "A_PARTOUT" po');
        quRepPrib.SQL.Add('left join "Goods" gd on gd.ID=po.ARTICUL');
        quRepPrib.SQL.Add('left join "Depart" de on de.ID=po.IDSTORE');
        quRepPrib.SQL.Add('where po.ITYPE=7');
        quRepPrib.SQL.Add('and po.IDATE>='+its(iDateB));
        quRepPrib.SQL.Add('and po.IDATE<='+its(iDateE));

        if ObPar.iDepart>0 then
//          quRepOB.SQL.Add('and po.IDSTORE in ('+its(ObPar.iDepart)+')');
           quRepOB.SQL.Add('and po.IDSTORE = '+its(ObPar.iDepart));

        if ObPar.iPost>0 then quRepOB.SQL.Add('and po.SINN='''+fmPeriod.cxTextEdit1.Text+'''');

        if ObPar.iSGroup>0 then quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepOB.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepOB.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        quRepPrib.SQL.Add('group by');
        quRepPrib.SQL.Add('po.ARTICUL,po.IDSTORE,po.ITYPE,po.SINN,');
        quRepPrib.SQL.Add('gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.V03,gd.V04,gd.V05,gd.Cena,gd.NDS,de.Name,de.Status5');

        quRepPrib.SQL.Add('order by po.ARTICUL,po.IDSTORE');

{
select
po.ARTICUL,po.IDSTORE,po.ITYPE,
gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.V03,gd.V04,gd.V05,gd.Cena,gd.NDS,
de.Name as NameDep,
SUM(po.QUANT) as QUANT,
SUM(po.SUMIN) as SUMIN,
SUM(po.SUMOUT) as SUMOUT
from "A_PARTOUT" po
left join "Goods" gd on gd.ID=po.ARTICUL
left join "Depart" de on de.ID=po.IDSTORE
where po.ARTICUL>=0
group by
po.ARTICUL,po.IDSTORE,po.ITYPE,
gd.Name,gd.EdIzm,gd.GoodsGroupID,gd.SubGroupID,gd.V01,gd.V02,gd.V03,gd.V04,gd.V05,gd.Cena,gd.NDS,de.Name
order by po.ARTICUL,po.IDSTORE
}
        quRepPrib.Active:=True;

        fmPrib1.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

        quRepPrib.First;

        iStep:=quRepPrib.RecordCount div 60;
        if iStep=0 then iStep:=quRepPrib.RecordCount;
        iC:=0;

        while not quRepPrib.Eof do
        begin
          rQr:=0; //������� ���� �� ���������
          rSumOut:=rv(quRepPribSUMOUT.AsFloat);

          rSumIn:=quRepPribSUMIN.AsFloat;
          if quRepPribStatus5.AsInteger=2 then rSumIn:=rv(quRepPribSUMIN.AsFloat/100*(100+quRepPribNDS.AsFloat));

//          prCalcPrib(quRepPribItem.AsInteger,iDateB,iDateE,quRepPribDepart.AsInteger,rQReal,rSumIn,rSumOut);

          rNac:=rSumOut-rSumIn;
          if rSumIn>0 then rPNac:=rv((rSumOut-rSumIn)/rSumIn*100) else rpNac:=0;

          teRPrib.Append;
          teRPribIdCode1.AsInteger:=quRepPribARTICUL.AsInteger;
          teRPribNameC.AsString:=quRepPribName.AsString;
          teRPribiM.AsInteger:=quRepPribEdIzm.AsInteger;
          teRPribQR.AsFloat:=rQr;
          teRPribIdGroup.AsInteger:=quRepPribGoodsGroupID.AsInteger;
          teRPribIdSGroup.AsInteger:=0;
          teRPribNameGr1.AsString:='';
          teRPribNameGr2.AsString:='';
          teRPribNameGr3.AsString:='';
          teRPribiBrand.AsInteger:=quRepPribV01.AsInteger;
          teRPribiCat.AsInteger:=quRepPribV02.AsInteger;
          teRPribPrice.AsFloat:=quRepPribCena.AsFloat;

          iGr:=quRepPribGoodsGroupID.AsInteger;

          if teClass.Locate('Id',iGr,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teRPribNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRPribNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRPribNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;

          if quRepPribV04.AsInteger=0 then teRPribsTop.AsString:=' ' else teRPribsTop.AsString:='T';
          if quRepPribV05.AsInteger=0 then teRPribsNov.AsString:=' ' else teRPribsNov.AsString:='N';

          teRPribrSumIn.AsFloat:=rSumIn;
          teRPribrSumOut.AsFloat:=rSumOut;
          teRPribrNac.AsFloat:=rv(rNac);
          teRPribpNac.AsFloat:=rpNac;
          teRPribrQReal.AsFloat:=quRepPribQUANT.AsFloat;
          teRPribiDep.AsInteger:=quRepPribIDSTORE.AsInteger;
          teRPribsDep.AsString:=quRepPribNameDep.AsString;
          teRPribrSumSS.AsFloat:=quRepPribSUMIN.AsFloat;
          teRPribrSumNDS.AsFloat:=rv(rSumIn-quRepPribSUMIN.AsFloat);
          teRPribSInn.AsString:=quRepPribSINN.AsString;
          teRPribSPost.AsString:=prFindCliName(quRepPribSINN.AsString);
          teRPribNDS.AsFloat:=quRepPribNDS.AsFloat;
          teRPrib.Post;

          quRepPrib.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmPrib1.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        quRepPrib.Active:=True;

        fmPrib1.PBar1.Position:=100; delay(100);

        ViewPrib1.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmPrib1.PBar1.Visible:=False;
      end;
    end;
    fmPeriod.Release;
  end else
    fmPeriod.Release;
end;

procedure TfmMainMCryst.acPostZ1Execute(Sender: TObject);
begin
  //������ ����������
  with dmMC do
  begin
    fmDocs7.ViewZak.BeginUpdate;
    try
      quZakH.Active:=False;
      quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quZakH.Active:=True;
    finally
      fmDocs7.ViewZak.EndUpdate;
    end;
  end;
  fmDocs7.Caption:='����� ������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs7.Show;
end;

procedure TfmMainMCryst.acAkciyaExecute(Sender: TObject);
begin
//�������� �����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
   //�������
    with dmMc do
    with dmMT do
    begin
      fmAkciya.LevelAkciya.Visible:=True;
      fmAkciya.LevelAkciya1.Visible:=False;

      fmAkciya.Caption:='����� �� ������� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
      delay(10);

      fmAkciya.Show;
      delay(10);
      fmAkciya.ViewAkciya.BeginUpdate;
      quAkciya.Active:=False;
      quAkciya.ParamByName('DATEB1').AsDate:=CommonSet.DateBeg;
      quAkciya.ParamByName('DATEB2').AsDate:=CommonSet.DateBeg;
      quAkciya.ParamByName('DATEB3').AsDate:=CommonSet.DateBeg;

      quAkciya.ParamByName('DATEE1').AsDate:=CommonSet.DateEnd;
      quAkciya.ParamByName('DATEE2').AsDate:=CommonSet.DateEnd;
      quAkciya.ParamByName('DATEE3').AsDate:=CommonSet.DateEnd;
      quAkciya.Active:=True;
      //quAkciya.SQL.SaveToFile('c:\1.sql');
      fmAkciya.ViewAkciya.EndUpdate;
      delay(10)
    end;
  end;
end;

procedure TfmMainMCryst.acExchExecute(Sender: TObject);
begin
  //�����
  with dmMC do
  begin
    fmDocs6.ViewObm.BeginUpdate;
    try
      quObmH.Active:=False;
      quObmH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
      quObmH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
      quObmH.Active:=True;
    finally
      fmDocs6.ViewObm.EndUpdate;
    end;
  end;
  fmDocs6.Caption:='��������� �� ����� ������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs6.Show;
end;

procedure TfmMainMCryst.acOnlyTestRecalcMoveExecute(Sender: TObject);
Var iC,iD:INteger;
begin
//��������� ������������ ��������������
  with dmMt do
  begin
    if MessageDlg('��������� ���������� ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='�������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      if taCards.Active=False then taCards.Active:=True;

      iD:=0;
      iC:=0;
      taCards.First;
      while not taCards.Eof do
      begin
        if taCardsStatus.AsInteger<=100 then
          if prTestReCalcTMoveLine(taCardsID.AsInteger)=1 then
          begin
            WriteHistory(';'+its(taCardsID.AsInteger)+';');
            prReCalcTMoveLine(taCardsID.AsInteger);
            inc(iD); delay(20);
          end;

        taCards.Next;
        inc(iC);
      //  if iC mod 100 = 0 then
      //  begin
        StatusBar1.Panels[0].Text:=IntToStr(iC);
        StatusBar1.Panels[1].Text:=IntToStr(iD);
//        delay(20);
      //  end;
      end;

      StatusBar1.Panels[0].Text:='Ok'; delay(10);
    end;
  end;
end;

procedure TfmMainMCryst.acMoveBZExecute(Sender: TObject);
Var rQIn1,rQOut1,rQReal1,rQvnIn1,rQVnOut1,rQInv1,rQb1,rQe:Real;
    iStep,iC,iEdizm,iGr,iSGr,iCat,DayN,bCat,bCatD,bTop,bTopD:INteger;
    sName,sTop,sNov:String;
begin
  //�������� ������� - ��������� ���������
  if fmclients.Showing=true then fmclients.Close;
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod.DateEdit2.Date:=CommonSet.DateEnd;
  
  fmPeriod.cxLookupComboBox2.Enabled:=True;
  fmPeriod.cxButton2.Enabled:=True;

  fmPeriod.cxCheckBox7.Checked:=True;
  fmPeriod.cxLabel9.Visible:=False;
  fmPeriod.cxCheckBox7.Visible:=False;

//  fmPeriod.cxLookupComboBox4.Visible:=False; fmPeriod.cxCheckBox4.Visible:=False; fmPeriod.cxLabel6.Visible:=False;
//  fmPeriod.cxLookupComboBox5.Visible:=False; fmPeriod.cxCheckBox5.Visible:=False; fmPeriod.cxLabel7.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod.DateEdit2.Date);
    //�������
    with dmMc do
    with dmMt do
    begin
      fmObVed.PBar1.Position:=0;
      fmObVed.Caption:='��������� ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd)+'  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmObVed.Tag:=1;
      fmObVed.Show;
      fmObVed.PBar1.Visible:=True;
      with fmObVed do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewObVed.BeginUpdate;
        CloseTe(fmObVed.teObVed);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;


        fmObVed.PBar1.Position:=10; delay(10);

        quRepOB.Active:=False;
        quRepOB.SQL.Clear;
        quRepOB.SQL.Add('select');
        quRepOB.SQL.Add('0 as Card,');
        quRepOB.SQL.Add('gdscli.GoodsID as Item,');
        //quRepOB.SQL.Add('cg.Item,');                                          //���������� 20110121 �������
        quRepOB.SQL.Add('gd.Name,');
        quRepOB.SQL.Add('gd.EdIzm,');
        quRepOB.SQL.Add('0 as Depart,');
        quRepOB.SQL.Add(''''' as NameDep,');
        quRepOB.SQL.Add('gd.GoodsGroupID,');
        quRepOB.SQL.Add('gd.SubGroupID,');
        quRepOB.SQL.Add('gd.V01,');
        quRepOB.SQL.Add('gd.V02,');
        quRepOB.SQL.Add('gd.V03,');
        quRepOB.SQL.Add('gd.V04,');
        quRepOB.SQL.Add('gd.V05,');
        quRepOB.SQL.Add('gd.Cena');
        //quRepOB.SQL.Add('from "CardGoods" cg');                               //���������� 20110121 �������
        //quRepOB.SQL.Add('left join "Goods" gd on gd.ID=cg.Item');             //���������� 20110121 �������
        //quRepOB.SQL.Add('left join "Depart" de on de.ID=cg.Depart');          //���������� 20110121 �������
        //quRepOB.SQL.Add('where cg.Item>0 and gd.Status<100 ');                //���������� 20110121 �������
        quRepOB.SQL.Add('from "A_GDSCLI" gdscli');
        quRepOB.SQL.Add('left join "Goods" gd on gd.ID=gdscli.GoodsID');
//        quRepOB.SQL.Add('left join "CardGoods" cg on cg.Item=gd.ID');
//        quRepOB.SQL.Add('left join "Depart" de on de.ID=cg.Depart');
        quRepOB.SQL.Add('where Status<=100 ');
        quRepOB.SQL.Add('and gd.Name not like ''*%''');

        if ObPar.iPost>0 then
        begin
{          quRepOB.SQL.Add('and cg.Item in (select CodeTovar from "TTNInLn"');
          quRepOB.SQL.Add('where DateInvoice>='''+ds(Date-150)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepOB.SQL.Add('group by CodeTovar');
          quRepOB.SQL.Add(')');
}
          //quRepOB.SQL.Add('and cg.Item in (select GoodsID from "A_GDSCLI"');  //���������� 20110121 �������
          //quRepOB.SQL.Add('where ClientTypeId='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));  //���������� 20110121 �������
          quRepOB.SQL.Add('and gdscli.ClientTypeId='+its(ObPar.iPostT)+' and gdscli.ClientID='+its(ObPar.iPost));
          //quRepOB.SQL.Add(')');                                               //���������� 20110121 �������
        end;

//        if ObPar.iDepart>0 then
//          quRepOB.SQL.Add('and cg.Depart in ('+its(ObPar.iDepart)+')');

        if ObPar.iSGroup>0 then quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepOB.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepOB.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepOB.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepOB.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepOB.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepOB.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;

        //quRepOB.SQL.Add('order by cg.Item,cg.Depart');   //���������� 20110121 �������
        quRepOB.SQL.Add('order by gdscli.GoodsID');

        //quRepOB.SQL.SaveToFile('c:\1.sql');
        quRepOB.Active:=True;

        fmObVed.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������.'); delay(10);

        //������ ��������� ��������� �������� ������� -  ������� ����������

        quRepOb.First;
        while not quRepOB.Eof do
        begin
          if teObVed.Locate('IdCode1',quRepOBItem.AsInteger,[])=False then
          begin
            rQIn1:=0;
            rQOut1:=0;
            rQVnIn1:=0;
            rQVnOut1:=0;
            rQReal1:=0;
            rQInv1:=0;
            rQb1:=0;

            iEdizm:=quRepOBEdIzm.AsInteger;
            iGr:=quRepOBGoodsGroupID.AsInteger;
            iSGr:=quRepOBSubGroupID.AsInteger;
            sName:=quRepOBName.AsString;
            iCat:=quRepOBV02.AsInteger;

            if quRepOBV04.AsInteger=0 then sTop:=' ' else sTop:='T';
            if quRepOBV05.AsInteger=0 then sNov:=' ' else sNov:='N';

            rQe:=rQb1+rQIn1+rQvnIn1+rQInv1-rQVnOut1-rQReal1-rQOut1;

            teObVed.Append;
            teObVedIdCode1.AsInteger:=quRepOBItem.AsInteger;
            teObVedNameC.AsString:=sName;
            teObVediM.AsInteger:=iEdizm;
            teObVedIdGroup.AsInteger:=iGr;
            teObVedIdSGroup.AsInteger:=iSGr;
            teObVedQBeg.AsFloat:=rQb1;
            teObVedQIn.AsFloat:=rQIn1;
            teObVedQVnIn.AsFloat:=rQvnIn1;
            teObVedQVnOut.AsFloat:=rQVnOut1;
            teObVedQReal.AsFloat:=rQReal1;
            teObVedQOut.AsFloat:=rQOut1;
            teObVedQRes.AsFloat:=rQe;
            teObVedQInv.AsFloat:=rQInv1;
            teObVedNAMEDEP.AsString:='All';

            teObVedNameGr1.AsString:='';
            teObVedNameGr2.AsString:='';
            teObVedNameGr3.AsString:='';

            if teClass.Locate('Id',iGr,[]) then
            begin
              iGr:=teClassIdParent.AsInteger;
              teObVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                iGr:=teClassIdParent.AsInteger;
              end;
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teObVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
            end;

            teObVediCat.AsInteger:=iCat;
            teObVedsTop.AsString:=sTop;
            teObVedsNov.AsString:=sNov;
            if sNov>' ' then
            begin
              DayN:=RoundEx(Date-prFindTFirstDate(quRepOBItem.AsInteger));
              if DayN>91 then DayN:=0;
              teObVedDayN.AsInteger:=DayN;
            end;

            bCat:=prFindBAkciya1(quRepOBItem.AsInteger,Trunc(Date+1),bCatD,bTopD,bTop);

            teObVedBStatus.AsInteger:=bCat;
            if bCatD>1000 then teObVedBStatusD.AsDateTime:=bCatD
            else teObVedBStatusD.AsString:='';

            teObVedBTop.AsInteger:=bTop;

            if bTopD>1000 then teObVedBTopD.AsDateTime:=bTopD
            else teObVedBTopD.AsString:='';

            teObVedsCat.AsString:=fSCat(iCat);
            if bCat<=1 then teObVedsBStatus.AsString:='' else teObVedsBStatus.AsString:=fSCat(bCat);
            if bTop>0 then teObVedsBTop.AsString:='BT' else teObVedsBTop.AsString:='';

            teObVed.Post;

          end;
          quRepOb.Next;
        end;

        quRepOb.Active:=False;


        iStep:=teObVed.RecordCount div 60;
        if iStep=0 then iStep:=teObVed.RecordCount;
        iC:=0;
        teObVed.First;

        while not teObVed.Eof do
        begin

          prCalcTMoveAllDep(teObVedIdCode1.AsInteger,ObPar.iDepart,rQIn1,rQOut1,rQReal1,rQvnIn1,rQVnOut1,rQInv1,rQb1);

          rQe:=rQb1+rQIn1+rQvnIn1+rQInv1-rQVnOut1-rQReal1-rQOut1;

          teObVed.Edit;
          teObVedQBeg.AsFloat:=rQb1;
          teObVedQIn.AsFloat:=rQIn1;
          teObVedQVnIn.AsFloat:=rQvnIn1;
          teObVedQVnOut.AsFloat:=rQVnOut1;
          teObVedQReal.AsFloat:=rQReal1;
          teObVedQOut.AsFloat:=rQOut1;
          teObVedQRes.AsFloat:=rQe;
          teObVedQInv.AsFloat:=rQInv1;
          teObVed.Post;

          teObVed.Next;   inc(iC);

          if (iC mod iStep = 0) then
          begin
            fmObVed.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        fmObVed.PBar1.Position:=100; delay(100);

        quRepOB.Active:=False;
        teClass.Active:=False;
        ViewObVed.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmObVed.PBar1.Visible:=False;
      end;
    end;
    fmPeriod.Release;
  end else
    fmPeriod.Release;
end;

procedure TfmMainMCryst.acRemnDayBZExecute(Sender: TObject);
Var rQr,rSpeedDay,rQDay:Real;
    iStep,iC,iGr,i:INteger;
begin
//�� ���� ��������
  if fmclients.Showing then fmclients.Close;
  fmPeriodR:=TfmPeriodR.Create(Application);
  fmPeriodR.ShowModal;
  if fmPeriodR.ModalResult=mrOk then
  begin
    //�������
    with dmMc do
    with dmMt do
    begin
      fmRDayVed.PBar1.Position:=0;
      fmRDayVed.Caption:='�������  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;
      fmRDayVed.ptDaysCols(True);
      fmRDayVed.Show;
      fmRDayVed.PBar1.Visible:=True;
      with fmRDayVed do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewRDayVed.BeginUpdate;
        CloseTe(fmRDayVed.teRDayVed);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;

        fmRDayVed.PBar1.Position:=10; delay(10);

        quRepR.Active:=False;
        quRepR.SQL.Clear;
        {quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');
        quRepR.SQL.Add('from "Goods" gd');
        quRepR.SQL.Add('where  Status<100');} //���������� 20110124 �������

{        quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');

        quRepR.SQL.Add('from "A_GDSCLI" gdscli');
        quRepR.SQL.Add('left join "Goods" gd on gd.ID=gdscli.GoodsID');
        quRepOB.SQL.Add('where Status<=100 ');
        }

        quRepR.Active:=False;
        quRepR.SQL.Clear;
        quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');
        quRepR.SQL.Add('from "Goods" gd');
        quRepR.SQL.Add('where  Status<100');


        if ObPar.iPost>0 then
        begin
          quRepR.SQL.Add('and gd.ID in (select GoodsID from "A_GDSCLI"');
          quRepR.SQL.Add('where ClientTypeId='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepR.SQL.Add('group by GoodsID)');

{         quRepR.SQL.Add('and cg.Item in (select GoodsID from "A_GDSCLI"');
          quRepR.SQL.Add('where ClientTypeId='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepR.SQL.Add(')');} //���������� 20110124 �������

        end;

        if ObPar.iSGroup>0 then quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepR.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepR.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepR.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepR.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepR.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;


        quRepR.SQL.Add('order by gd.ID');
        //quRepR.SQL.SaveToFile('c:\2.sql');
        quRepR.Active:=True;

        fmRDayVed.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������. ('+its(quRepR.RecordCount)+')'); delay(10);

        quRepR.First;

        iStep:=quRepR.RecordCount div 60;
        if iStep=0 then iStep:=quRepR.RecordCount;
        iC:=0;

        while not quRepR.Eof do
        begin
          if quRepRID.AsInteger>0 then
          begin
            rQr:=prFindTabSpeedRemn(quRepRID.AsInteger,0,rSpeedDay,rQDay);
            prFillReal10(quRepRID.AsInteger,Trunc(Date-1),10);

            teRDayVed.Append;
            teRDayVedIdCode1.AsInteger:=quRepRID.AsInteger;
            teRDayVedNameC.AsString:=quRepRName.AsString;
            teRDayVediM.AsInteger:=quRepREdIzm.AsInteger;
            teRDayVedQR.AsFloat:=rQr;
            teRDayVedIdGroup.AsInteger:=quRepRGoodsGroupID.AsInteger;
            teRDayVedIdSGroup.AsInteger:=0;
            teRDayVedNameGr1.AsString:='';
            teRDayVedNameGr2.AsString:='';
            teRDayVedNameGr3.AsString:='';
            teRDayVediBrand.AsInteger:=quRepRV01.AsInteger;
            teRDayVediCat.AsInteger:=quRepRV02.AsInteger;
            teRDayVedPrice.AsFloat:=quRepRCena.AsFloat;
            teRDayVedAvrSpeed.AsFloat:=rSpeedDay;
            teRDayVedQRDay.AsFloat:=rQDay;

            iGr:=quRepRGoodsGroupID.AsInteger;

            if teClass.Locate('Id',iGr,[]) then
            begin
              iGr:=teClassIdParent.AsInteger;
              teRDayVedNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teRDayVedNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                iGr:=teClassIdParent.AsInteger;
              end;
            end;
            if iGr>0 then
            begin
              if teClass.Locate('Id',iGr,[]) then
              begin
                teRDayVedNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
            end;

            if quRepRV04.AsInteger=0 then teRDayVedsTop.AsString:=' ' else teRDayVedsTop.AsString:='T';
            if quRepRV05.AsInteger=0 then teRDayVedsNov.AsString:=' ' else
            begin
              teRDayVedsNov.AsString:='N';
              teRDayVedDayN.AsInteger:=RoundEx(Date-prFindTFirstDate(quRepRID.AsInteger));
              if teRDayVedDayN.AsInteger>91 then teRDayVedDayN.AsInteger:=0;
            end;

            for i:=1 to 10 do teRDayVed.FieldByName('D'+its(i-1)).AsFloat:=arRD[i];

            teRDayVed.Post;
          end;
          quRepR.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmRDayVed.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        fmRDayVed.PBar1.Position:=100; delay(100);

        quRepR.Active:=False;
        ViewRDayVed.EndUpdate;

        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmRDayVed.PBar1.Visible:=False;
      end;
    end;
    fmPeriodR.Release;
  end else
    fmPeriodR.Release;
end;

procedure TfmMainMCryst.acTestNoUse1Execute(Sender: TObject);
Var iD,iC:INteger;
    rQ:Real;
begin
  with dmMc do
  with dmMt do
  begin
    try
      fmSt.Memo1.Clear;
      fmSt.Show;
      with fmSt do
      begin
//        cxButton1.Enabled:=False;
          prWH('',Memo1);
          prWH('  �������� �������������� ... �����.',Memo1);

          if taCards.Active=False then taCards.Active:=True;

          iD:=0;
          iC:=0;
          taCards.First;
          while not taCards.Eof do
          begin
            if taCardsStatus.asinteger>=100 then
            begin
              rQ:=prFindTabRemn(taCardsID.AsInteger);
              if abs(rQ)>0.0001 then
              begin
                prWH('On;'+its(taCardsID.AsInteger)+';'+fs(rQ),Memo1);
                taCards.Edit;
                taCardsStatus.asinteger:=1;
                taCardsReserv1.AsFloat:=rQ;
                taCards.Post;
                inc(iD);
              end;
            end;

            taCards.Next;
            inc(iC);
            if iC mod 100 = 0 then
            begin
              fmSt.StatusBar1.Panels[0].Text:='���������� - '+its(iC)+' '+its(iD);
//              prWH('���������� - '+its(iC)+' '+its(iD),Memo1);
              delay(10);
            end;
          end;
      end;

    except
    end;
  end;
end;

procedure TfmMainMCryst.acPartCorrExecute(Sender: TObject);
Var iC:Integer;
begin
//��������� ����������� �����
  with dmMc do
  with dmMT do
  begin
    if MessageDlg('�������� �������� ����������� ����� �� ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      StatusBar1.Panels[0].Text:='������������'; StatusBar1.Panels[1].Text:='0'; delay(10);
      iC:=0;

      taCards.First;
      while not taCards.Eof do
      begin
        if taCardsStatus.AsInteger<100 then  prCorrPartIn(taCardsID.AsInteger);

        taCards.Next;  inc(iC);
        if iC mod 100 = 0 then
        begin
          StatusBar1.Panels[0].Text:=IntToStr(iC);
          delay(10);
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.Label3DblClick(Sender: TObject);
Var sNameDep:String;
begin
  //����� ������
  with dmMT do
  begin
    with fmDepSel do
    begin
      closete(teDeps);
      if ptDep.Active=False then ptDep.Active:=True;
      ptDep.Refresh;
      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          sNameDep:=Trim(OemToAnsiConvert(ptDepFullName.AsString));
          if sNameDep>'' then
          begin
            teDeps.Append;
            teDepsID.AsInteger:=ptDepID.AsInteger;
            teDepsName.AsString:=OemToAnsiConvert(ptDepName.AsString);
            teDepsNameFull.AsString:=OemToAnsiConvert(ptDepFullName.AsString);
            teDeps.Post;
          end;  
        end;
        ptDep.Next;
      end;
    end;
    fmDepSel.ShowModal;
    if fmDepSel.ModalResult=mrOk then
    begin
      Label3.Caption:=Copy(fmDepSel.teDepsNameFull.AsString,1,50);
      Label3.Tag:=fmDepSel.teDepsID.AsInteger;
    end;
  end;
end;

procedure TfmMainMCryst.acOborExecute(Sender: TObject);
Var DateB,DateE,CurB,CurE,CurMM,iC,iDep:Integer;
    rOb:Array[1..10] of Real;
    i,iSS:Integer;
    rTZ,rReal:Real;
    rTZ_,rReal_:Real;
    dCurB,dCurE:TDateTime;

begin
  //��������������� �������� �������
  with dmMc do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
      fmOborRep.Memo1.Clear;
      fmOborRep.Memo1.Lines.Add('����� ���� ������������ ...');
      fmOborRep.Show;
      fmOborRep.ViewObor.BeginUpdate;

      with fmOborRep do
      begin
        CloseTe(teObor);
        CurMM:=prMM(DateB);
        CurB:=DateB;
        CurE:=DateB;
        iC:=1;
        if ptDep.Active=False then ptDep.Active:=True;
        ptDep.Refresh;

        if ptTO.Active=False then ptTO.Active:=True;
        ptTO.Refresh;

        //��������� �������
        while DateB<=DateE do
        begin
          if CurMM<>prMM(DateB) then
          begin
            //����
            iDep:=1;
            for i:=1 to 10 do rOb[i]:=0;

            rTZ_:=0;
            rReal_:=0;

            ptDep.First;
            while (ptDep.Eof=False)and(iDep<=10) do
            begin
              if ptDepStatus1.AsInteger=9 then
              begin
                //������ �� ������
                rTZ:=0;
                rReal:=0;
                iSS:=ptDepStatus5.AsInteger;

                dCurB:=CurB;
                dCurE:=CurE;

                ptTO.CancelRange;
                ptTO.SetRange([ptDepID.AsInteger,dCurB],[ptDepID.AsInteger,dCurE]);
                ptTO.First;
                while not ptTO.Eof do
                begin
                  rTZ:=rTZ+ptTONewOstatokTovar.AsFloat; //�������� ��� �������
                  rTZ_:=rTZ_+ptTONewOstatokTovar.AsFloat;
                  if iSS=0 then
                  begin
                     rReal:=rReal+ptTORealTovar.AsFloat+ptTOSenderSumma.AsFloat; //��� + ������
                     rReal_:=rReal_+ptTORealTovar.AsFloat+ptTOSenderSumma.AsFloat; //��� + ������
                  end
                  else
                  begin
                    rReal:=rReal+ptTOxRezerv.AsFloat;
                    rReal_:=rReal_+ptTOxRezerv.AsFloat;
                  end;

                  ptTO.Next;
                end;

                if rReal>0 then
                begin
                  rOb[iDep]:=rv(rTZ/rReal);
                end else
                begin
                  rOb[iDep]:=0;
                end;

                inc(iDep);
              end;

              ptDep.Next;
            end;

            teObor.Append;
            teOborNum.AsInteger:=iC;
            teOborsPer.AsString:='C '+ds1(CurB)+' �� '+ds1(CurE);
            teOborDep1.AsFloat:=rOb[1];
            teOborDep2.AsFloat:=rOb[2];
            teOborDep3.AsFloat:=rOb[3];
            teOborDep4.AsFloat:=rOb[4];
            teOborDep5.AsFloat:=rOb[5];
            teOborDep6.AsFloat:=rOb[6];
            teOborDep7.AsFloat:=rOb[7];
            teOborDep8.AsFloat:=rOb[8];
            teOborDep9.AsFloat:=rOb[9];
            teOborDep10.AsFloat:=rOb[10];
            if rReal_>0 then teOborItog.AsFloat:=rv(rTZ_/rReal_)
            else teOborItog.AsFloat:=0;
            teObor.Post;

            CurMM:=prMM(DateB);
            CurB:=DateB;
            inc(iC);
          end;
          CurE:=DateB;
          inc(DateB);
        end;

        CurE:=DateE;
      //����
        rTZ_:=0;
        rReal_:=0;

        iDep:=1;
        for i:=1 to 10 do rOb[i]:=0;

        ptDep.First;
        while (ptDep.Eof=False)and(iDep<=10) do
        begin
          if ptDepStatus1.AsInteger=9 then
          begin
           //������ �� ������
            rTZ:=0;
            rReal:=0;
            iSS:=ptDepStatus5.AsInteger;

            dCurB:=CurB;
            dCurE:=CurE;

            ptTO.CancelRange;
            ptTO.SetRange([ptDepID.AsInteger,dCurB],[ptDepID.AsInteger,dCurE]);
            ptTO.First;
            while not ptTO.Eof do
            begin
              rTZ:=rTZ+ptTONewOstatokTovar.AsFloat; //�������� ��� �������
              rTZ_:=rTZ_+ptTONewOstatokTovar.AsFloat;

              if iSS=0 then
              begin
                rReal:=rReal+ptTORealTovar.AsFloat+ptTOSenderSumma.AsFloat; //��� + ������
                rReal_:=rReal_+ptTORealTovar.AsFloat+ptTOSenderSumma.AsFloat; //��� + ������
              end
              else
              begin
                rReal:=rReal+ptTOxRezerv.AsFloat;
                rReal_:=rReal_+ptTOxRezerv.AsFloat;
              end;

              ptTO.Next;
            end;

            if rReal>0 then
            begin
              rOb[iDep]:=rv(rTZ/rReal);
            end else
            begin
              rOb[iDep]:=0;
            end;

            inc(iDep);
          end;

          ptDep.Next;
        end;

        teObor.Append;
        teOborNum.AsInteger:=iC;
        teOborsPer.AsString:='C '+ds1(CurB)+' �� '+ds1(CurE);
        teOborDep1.AsFloat:=rOb[1];
        teOborDep2.AsFloat:=rOb[2];
        teOborDep3.AsFloat:=rOb[3];
        teOborDep4.AsFloat:=rOb[4];
        teOborDep5.AsFloat:=rOb[5];
        teOborDep6.AsFloat:=rOb[6];
        teOborDep7.AsFloat:=rOb[7];
        teOborDep8.AsFloat:=rOb[8];
        teOborDep9.AsFloat:=rOb[9];
        teOborDep10.AsFloat:=rOb[10];
        if rReal_>0 then teOborItog.AsFloat:=rv(rTZ_/rReal_)
        else teOborItog.AsFloat:=0;
        teObor.Post;

        ViewOborDep1.Visible:=False;
        ViewOborDep2.Visible:=False;
        ViewOborDep3.Visible:=False;
        ViewOborDep4.Visible:=False;
        ViewOborDep5.Visible:=False;
        ViewOborDep6.Visible:=False;
        ViewOborDep7.Visible:=False;
        ViewOborDep8.Visible:=False;
        ViewOborDep9.Visible:=False;
        ViewOborDep10.Visible:=False;

        iDep:=1;
        ptDep.First;
        while (ptDep.Eof=False)and(iDep<=10) do
        begin
          if ptDepStatus1.AsInteger=9 then
          begin
            Case iDep of
            1: begin ViewOborDep1.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep1.Visible:=True; end;
            2: begin ViewOborDep2.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep2.Visible:=True; end;
            3: begin ViewOborDep3.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep3.Visible:=True; end;
            4: begin ViewOborDep4.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep4.Visible:=True; end;
            5: begin ViewOborDep5.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep5.Visible:=True; end;
            6: begin ViewOborDep6.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep6.Visible:=True; end;
            7: begin ViewOborDep7.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep7.Visible:=True; end;
            8: begin ViewOborDep8.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep8.Visible:=True; end;
            9: begin ViewOborDep9.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep9.Visible:=True; end;
            10: begin ViewOborDep10.Caption:=OemToAnsiConvert(ptDepName.AsString); ViewOborDep10.Visible:=True; end;
            end;

            inc(iDep);
          end;

          ptDep.Next;
        end;

      end;
      fmOborRep.ViewObor.EndUpdate;
      fmOborRep.Memo1.Lines.Add('��');

    end;
  end;
end;

procedure TfmMainMCryst.acActionsRExecute(Sender: TObject);
Var StrWk:String;
    iStep,iC:INteger;
    rSpDay:Real;
//    iDateB,iDateE:Integer;
    rQb,rQe,rQIn,rQReal:Real;

  Function AtoS(AType:INteger):String;
  begin
    with dmMC do
    begin
      Result:='-';
      if AType=1 then
      begin
        Result:='�('+quAkciyaPrice.AsString+')';
      end;
      if AType=2 then
      begin
        if quAkciyaDStop.AsInteger=0 then Result:='C(��.�� ������.)'
        else Result:='C(��.������.)';
      end;
      if AType=3 then
      begin
        if quAkciyaDStop.AsInteger<>-3 then Result:='�('+quAkciyaSID.AsString+')'
                                       else Result:='�(����� ���)';
        if quAkciyaDStop.AsInteger = 3 then Result:='�(���.���)';
      end;
      if AType=4 then Result:='%('+FloatToStr(quAkciyaDStop.AsInteger/100)+')';
    end;

{
  //������
  Case quAkciyaAType.AsInteger of
  1:begin
      quAkciyaVal1.AsFloat:=rv(quAkciyaPrice.AsInteger);
      quAkciyaVal2.AsString:=quAkciyaPrice.AsString;
    end;
  2:begin
      quAkciyaVal1.AsFloat:=roundEx(quAkciyaDStop.AsInteger);

      if quAkciyaDStop.AsInteger=0 then quAkciyaVal2.AsString:='��. �� ������.';
      if quAkciyaDStop.AsInteger=1 then quAkciyaVal2.AsString:='��. ������.';
    end;
  3:begin
      quAkciyaVal1.AsFloat:=roundEx(quAkciyaDStop.AsInteger);
      if quAkciyaDStop.AsInteger<>-3 then quAkciyaVal2.AsString:=quAkciyaSID.AsString
                                     else quAkciyaVal2.AsString:='����� ���';
      if quAkciyaDStop.AsInteger = 3 then quAkciyaVal2.AsString:='���������� ���';
    end;
  4:begin
      quAkciyaVal1.AsFloat:=rv(quAkciyaDStop.AsInteger/100);
      quAkciyaVal2.AsString:=FloatToStr(quAkciyaDStop.AsInteger/100);
    end;
  end;
}


  end;

begin
//����� �����
//�������� �����
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
   //�������
    with dmMc do
    with dmMT do
    begin
      fmAkciya.Caption:='����� �� ������� (�����) �� ������ � '+FormatDateTime('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',CommonSet.DateEnd);
      delay(10);
      fmAkciya.LevelAkciya.Visible:=False;
      fmAkciya.LevelAkciya1.Visible:=True; delay(10);
      fmAkciya.Memo1.Clear; delay(10);

      fmAkciya.PBar1.Position:=0;
      fmAkciya.Show;
      fmAkciya.PBar1.Visible:=True;

      fmAkciya.Memo1.Lines.Add('����� ���� ������������ ������..'); delay(10);
      fmAkciya.ViewAkciya1.BeginUpdate;

      quAkciya.Active:=False;
      quAkciya.ParamByName('DATEB1').AsDate:=CommonSet.DateBeg;
      quAkciya.ParamByName('DATEB2').AsDate:=CommonSet.DateBeg;
      quAkciya.ParamByName('DATEB3').AsDate:=CommonSet.DateBeg;

      quAkciya.ParamByName('DATEE1').AsDate:=CommonSet.DateEnd;
      quAkciya.ParamByName('DATEE2').AsDate:=CommonSet.DateEnd;
      quAkciya.ParamByName('DATEE3').AsDate:=CommonSet.DateEnd;
      quAkciya.Active:=True;

      fmAkciya.PBar1.Position:=10; delay(10);

      CloseTe(fmAkciya.teA);
      with fmAkciya do
      begin
        quAkciya.First;
        while not quAkciya.Eof do
        begin
          if quAkciyaStatusOf.AsInteger=1 then
          begin
            if teA.Locate('Code',quAkciyaCode.AsInteger,[]) then
            begin //�����������
              StrWk:=teAAType.AsString+AtoS(quAkciyaAType.AsInteger)+' ';
              teA.Edit;
              teAAType.AsString:=StrWk;
              teA.Post;
            end else
            begin //����������
              teA.Append;
              teACode.AsInteger:=quAkciyaCode.AsInteger;
              teADateB.AsDateTime:=quAkciyaDateB.AsDateTime;
              teADateE.AsDateTime:=quAkciyaDateE.AsDateTime;
              teAAType.AsString:=AtoS(quAkciyaAType.AsInteger)+' ';
              teAStOn.AsInteger:=quAkciyaStatusOn.AsInteger;
              teAStOf.AsInteger:=quAkciyaStatusOf.AsInteger;
              teAVReal1.AsFloat:=0;
              teAVReal2.AsFloat:=0;
              teAVReal3.AsFloat:=0;
              teAName.AsString:=quAkciyaName.AsString;
              teAEdIzm.AsString:='';
              teA.Post;
            end;
          end;
          quAkciya.Next;
        end;
        fmAkciya.Memo1.Lines.Add('����� ���� ������ ������..'); delay(10);
        fmAkciya.PBar1.Position:=20; delay(10);

        iStep:=teA.RecordCount div 60;
        if iStep=0 then iStep:=teA.RecordCount;
        iC:=0;

        teA.First;
        while not teA.Eof do
        begin
         //������ ���� ������ �� ����������� ������

// 1. �������� �������� ������ �� ����� �� ����� ��������
//
          if teAStOf.AsInteger=1 then
          begin
            prFindTabSpeedRemn2(teACode.AsInteger,Trunc(teADateB.AsDateTime-31),Trunc(teADateB.AsDateTime-1),rSpDay,rQIn,rQReal);

            rQb:=prFindTRemnDepD(teACode.AsInteger,-1,Trunc(teADateB.AsDateTime-1));
            rQe:=prFindTRemnDepD(teACode.AsInteger,-1,Trunc(teADateE.AsDateTime));

            teA.Edit;
            teAVReal1.AsFloat:=rSpDay;
            teARemn1.AsFloat:=rQb;
            teARemn2.AsFloat:=rQe;
            teA.Post;

// 2. �������� �������� ������ �� ����� �����
//
            prFindTabSpeedRemn2(teACode.AsInteger,Trunc(teADateB.AsDateTime),Trunc(teADateE.AsDateTime),rSpDay,rQIn,rQReal);
            //rQIn,rQReal


            teA.Edit;
            teAVReal2.AsFloat:=rSpDay;
            teAQIn.AsFloat:=rQIn;
            teAQReal.AsFloat:=rQReal;
            teA.Post;

// 3. �������� �������� ������ ����� �����
//
            prFindTabSpeedRemn2(teACode.AsInteger,Trunc(teADateE.AsDateTime+1),Trunc(teADateE.AsDateTime+31),rSpDay,rQIn,rQReal);

            teA.Edit;
            teAVReal3.AsFloat:=rSpDay;
            teA.Post;

          end;

          teA.Next; inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmAkciya.PBar1.Position:=20+(iC div iStep);
            delay(10);
          end;
        end;

      end;

      fmAkciya.PBar1.Position:=100;  delay(20);
      fmAkciya.PBar1.Visible:=False;

      //quAkciya.SQL.SaveToFile('c:\1.sql');
      fmAkciya.ViewAkciya1.EndUpdate;

      fmAkciya.Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end;
end;

procedure TfmMainMCryst.acLoadClassifExecute(Sender: TObject);
begin
  //�������� ����
  //�������� ������ �� �����
  with dmMt do
  with dmMC do
  begin
    try
//     if fmSt.cxButton1.Enabled=False then exit;

      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show; delay(33);

      fmSt.Memo1.Lines.Add('���� �������� ����..');

      if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX.');
      try

        if ptSGr.Active=False then ptSGr.Active:=True;
        ptSGr.Refresh;

        ptSGr.First;

        while not ptSGr.Eof do
        begin
          prAddCashLoad(ptSGrID.AsInteger,3,0,0);
          prAddPosFB(3,ptSGrID.AsInteger,0,0,ptSGrGoodsGroupID.AsInteger,0,1,'','',OemToAnsiConvert(ptSGrName.AsString),'','',0,0,0,0);
          ptSGr.Next;
        end;
        fmSt.Memo1.Lines.Add('Ok.');

      except
        fmSt.Memo1.Lines.Add('������ ������������ ������.');
        fmSt.Memo1.Lines.Add('  �������� ����������!!!');
      end;
    finally
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

procedure TfmMainMCryst.acAssortimentPostExecute(Sender: TObject);
begin
//����������� �����������
  if fmclients.Showing then fmclients.Close;
  fmAssPost.Show;

end;

procedure TfmMainMCryst.acRemnDaysExecute(Sender: TObject);
Var rQr,rSpeedDay,rQDay:Real;
    iStep,iC,iGr,i:INteger;
begin
//�� ���� ��������   //������� �� ����
  if fmclients.Showing then fmclients.Close;
  fmPeriodR:=TfmPeriodR.Create(Application);
  fmPeriodR.ShowModal;
  if fmPeriodR.ModalResult=mrOk then
  begin
    //�������
    with dmMc do
    with dmMt do
    begin
      fmRDayVedWide.PBar1.Position:=0;
      fmRDayVedWide.Caption:='�������  '+ObPar.Post+', '+ObPar.Depart+', '+ObPar.Group;

      fmRDayVedWide.ptDaysColsW;

      fmRDayVedWide.Show;
      fmRDayVedWide.PBar1.Visible:=True;
      with fmRDayVedWide do
      begin
        Memo1.Clear;
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.'); delay(10);
        ViewRDayVedW.BeginUpdate;
        CloseTe(fmRDayVedWide.teRDayVedW);

        CloseTe(teClass);
        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            teClass.Append;
            teClassId.AsInteger:=ptSGrID.AsInteger;
            teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            teClassNameClass.AsString:=ptSGrName.AsString;
            teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;

        fmRDayVedWide.PBar1.Position:=10; delay(10);

        quRepR.Active:=False;
        quRepR.SQL.Clear;
        quRepR.SQL.Add('select');
        quRepR.SQL.Add('gd.GoodsGroupID,');
        quRepR.SQL.Add('gd.ID,');
        quRepR.SQL.Add('gd.Name,');
        quRepR.SQL.Add('gd.BarCode,');
        quRepR.SQL.Add('gd.TovarType,');
        quRepR.SQL.Add('gd.EdIzm,');
        quRepR.SQL.Add('gd.Cena,');
        quRepR.SQL.Add('gd.FullName,');
        quRepR.SQL.Add('gd.Status,');
        quRepR.SQL.Add('gd.Reserv1,');
        quRepR.SQL.Add('gd.V01,');
        quRepR.SQL.Add('gd.V02,');
        quRepR.SQL.Add('gd.V03,');
        quRepR.SQL.Add('gd.V04,');
        quRepR.SQL.Add('gd.V05');
        quRepR.SQL.Add('from "Goods" gd');
        quRepR.SQL.Add('where  Status<100');

        if ObPar.iPost>0 then
        begin
{
          quRepR.SQL.Add('and gd.ID in (select CodeTovar from "TTNInLn"');
          quRepR.SQL.Add('where DateInvoice>='''+ds(Date-150)+''' and IndexPost='+its(ObPar.iPostT)+' and CodePost='+its(ObPar.iPost));
          quRepR.SQL.Add('group by CodeTovar');
          quRepR.SQL.Add(')');
}
          quRepR.SQL.Add('and gd.ID in (select GoodsID from "GdsClient"');
          quRepR.SQL.Add('where ClientObjTypeID='+its(ObPar.iPostT)+' and ClientID='+its(ObPar.iPost));
          quRepR.SQL.Add(')');

        end;

        if ObPar.iSGroup>0 then quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')')
        else
          if ObPar.iGroup>0 then
            quRepR.SQL.Add('and gd.GoodsGroupID in ('+ObPar.SSGr+')');

        if ObPar.iBrand>0 then
          quRepR.SQL.Add('and gd.V01 in ('+its(ObPar.iBrand)+')');

        if ObPar.iCat>0 then
          quRepR.SQL.Add('and gd.V02 in ('+its(ObPar.iCat)+')');

        Case ObPar.iTop of
        0: begin //������
           end;
        1: begin //������ ����
             quRepR.SQL.Add('and gd.V04=1');
           end;
        2: begin  //������ �������
             quRepR.SQL.Add('and gd.V05=1');
           end;
        3: begin
             quRepR.SQL.Add('and (gd.V05=1 or gd.V04=1)');
           end;
        end;


        quRepR.SQL.Add('order by gd.ID');
        quRepR.Active:=True;

        fmRDayVedWide.PBar1.Position:=40; delay(10);
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'  - ������. ('+its(quRepR.RecordCount)+')'); delay(10);

        quRepR.First;

        iStep:=quRepR.RecordCount div 60;
        if iStep=0 then iStep:=quRepR.RecordCount;
        iC:=0;

        while not quRepR.Eof do
        begin
          rQr:=prFindTabSpeedRemn(quRepRID.AsInteger,0,rSpeedDay,rQDay);

        //  prFillReal10(quRepRID.AsInteger,Trunc(Date-1),10);
          prFillRemn30(quRepRID.AsInteger,Trunc(Date-1),30);

          teRDayVedW.Append;
          teRDayVedWIdCode1.AsInteger:=quRepRID.AsInteger;
          teRDayVedWNameC.AsString:=quRepRName.AsString;
          teRDayVedWiM.AsInteger:=quRepREdIzm.AsInteger;
          teRDayVedWQR.AsFloat:=rQr;
          teRDayVedWIdGroup.AsInteger:=quRepRGoodsGroupID.AsInteger;
          teRDayVedWIdSGroup.AsInteger:=0;
          teRDayVedWNameGr1.AsString:='';
          teRDayVedWNameGr2.AsString:='';
          teRDayVedWNameGr3.AsString:='';
          teRDayVedWiBrand.AsInteger:=quRepRV01.AsInteger;
          teRDayVedWiCat.AsInteger:=quRepRV02.AsInteger;
          teRDayVedWPrice.AsFloat:=quRepRCena.AsFloat;
          teRDayVedWAvrSpeed.AsFloat:=rSpeedDay;
          teRDayVedWQRDay.AsFloat:=rQDay;

          iGr:=quRepRGoodsGroupID.AsInteger;

          if teClass.Locate('Id',iGr,[]) then
          begin
            iGr:=teClassIdParent.AsInteger;
            teRDayVedWNameGr3.AsString:=OemToAnsiConvert(teClassNameClass.AsString);;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRDayVedWNameGr2.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              iGr:=teClassIdParent.AsInteger;
            end;
          end;
          if iGr>0 then
          begin
            if teClass.Locate('Id',iGr,[]) then
            begin
              teRDayVedWNameGr1.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
            end;
          end;

          if quRepRV04.AsInteger=0 then teRDayVedWsTop.AsString:=' ' else teRDayVedWsTop.AsString:='T';
          if quRepRV05.AsInteger=0 then teRDayVedWsNov.AsString:=' ' else
          begin
            teRDayVedWsNov.AsString:='N';
            teRDayVedWDayN.AsInteger:=RoundEx(Date-prFindTFirstDate(quRepRID.AsInteger));
            if teRDayVedWDayN.AsInteger>91 then teRDayVedWDayN.AsInteger:=0;
          end;



          For i:=30 downto 1 do
          begin
            if arRD[i]=0 then arRD[i]:=rQr
            else rQr:=arRD[i];
          end;

          for i:=1 to 30 do
          begin
            if (rSpeedDay>0)and(arRD[i]<>0) then
            begin
              teRDayVedW.FieldByName('D'+its00(i)).AsFloat:=(arRD[i]/rSpeedDay);
//              teRDayVedW.FieldByName('D'+its00(i)).AsFloat:=(arRD[i]);
            end else
            begin
              teRDayVedW.FieldByName('D'+its00(i)).AsFloat:=0;
            end;
          end;

          teRDayVedW.Post;

          quRepR.Next;   inc(iC);
          if (iC mod iStep = 0) then
          begin
            fmRDayVedWide.PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;

        quRepR.Active:=True;

        fmRDayVedWide.PBar1.Position:=100; delay(100);

        quRepR.Active:=False;
        ViewRDayVedW.EndUpdate;
        
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������ Ok.'); delay(10);
        fmRDayVedWide.PBar1.Visible:=False;
      end;
    end;
    fmPeriodR.Release;
  end else
    fmPeriodR.Release;
end;

procedure TfmMainMCryst.acRecalcAssPostExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk:String;
    iC:INteger;
    dCurD:TDateTime;
begin
  //����������� ���������� �����������
  if CommonSet.Single=1 then
  begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    with dmMT do
    begin
//      DateB:=Trunc(CommonSet.DateBeg);
//      DateE:=Trunc(CommonSet.DateEnd);
      DateB:=Trunc(date-365);
      DateE:=Trunc(date);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
        try
          Memo1.Lines.Add(fmt+'  ������ ...');
          ptGdsCli.Active:=False; ptGdsCli.Active:=True;
          ptGdsCli.First;  iC:=0;
          while not ptGdsCli.Eof do
          begin
            ptGdsCli.delete;
            inc(iC);
            if iC mod 500 = 0 then
            begin
              delay(100);
              StatusBar1.Panels[0].Text:=its(iC);
            end;
          end;

          delay(100);

          Memo1.Lines.Add(fmt+'  ������� ...');

          ptTTNIn.Active:=False; ptTTNIn.Active:=True;
          ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';

          ptInLn.Active:=False; ptINLn.Active:=True;

          for iCurD:=DateB to DateE do
          begin
            Memo1.Lines.Add(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
            dCurD:=iCurD;
            ptTTNIn.CancelRange;
            ptTTNIn.SetRange([dCurD],[dCurD]);
            ptTTNIn.First;  iC:=0;
            while not ptTTNIn.Eof do
            begin
              ptInLn.CancelRange;
              ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
              ptInLn.First;
              while not ptInLn.Eof do
              begin
                if ptGdsCli.FindKey([ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,ptInLnCodeTovar.AsInteger]) then
                begin
                  ptGdsCli.Edit;
                  ptGdsClixDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptGdsCliPriceIn.AsFloat:=ptInLnCenaTovar.AsFloat;
                  ptGdsCli.Post;
                end else
                begin
                  ptGdsCli.Append;
                  ptGdsCliGoodsID.AsInteger:=ptInLnCodeTovar.AsInteger;
                  ptGdsCliClientTypeId.AsInteger:=ptTTNInIndexPost.AsInteger;
                  ptGdsCliClientID.AsInteger:=ptTTNInCodePost.AsInteger;
                  ptGdsClixDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptGdsCliPriceIn.AsFloat:=ptInLnCenaTovar.AsFloat;
                  ptGdsCli.Post;
                end;
                ptInLn.Next;
              end;

              ptTTNIn.Next; inc(iC);
              if iC mod 10 = 0 then
              begin
                StatusBar1.Panels[0].Text:=its(iC);
                delay(100);
              end;
            end;
          end;
        finally
          ptTTNIn.Active:=False;
          ptInLn.Active:=False;
          ptGdsCli.Active:=False;

          Memo1.Lines.Add(fmt+'������� ��������.');
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
  end;
end;

procedure SetDecimalSeparator( Ch: String );
var
  DefLCID: LCID;
//  Buffer: PChar;
begin
   Application.UpdateFormatSettings := true;
//   StrPCopy( Buffer, Ch );
   DefLCID := GetThreadLocale;
   if SetLocaleInfo( DefLCID, LOCALE_SDECIMAL,PChar(Ch)) then
//      DecimalSeparator := StrPas( Buffer )[1];
   DecimalSeparator := Ch[1];
   Application.UpdateFormatSettings := false;
end;


procedure TfmMainMCryst.acCashFBLoadExecute(Sender: TObject);
begin
  //������ ��������� FB ����
  fmCashFBLoad.cxProgressBar1.Visible:=False;
  fmCashFBLoad.Memo1.Clear;
  fmCashFBLoad.cxButton2.Enabled:=False;
  fmCashFBLoad.ShowModal;
end;

procedure TfmMainMCryst.acRepCassirExecute(Sender: TObject);
Var iMb,iMe,iMCur:INteger;
    par:Variant;
    NameCassir,NameDep:String;
    iCas:INteger;
begin
  //����� �� ��������

  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    iMb:=DateToMM(CommonSet.DateBeg);
    iMe:=DateToMM(CommonSet.DateEnd);
    iMCur:=iMb;

    fmCassirs.Show;
    with fmCassirs do
    with dmMt do
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������ ...');
      CloseTe(teCassirs);
      bCalc:=True;
      ViewCassir.BeginUpdate;

      //������ ������� �� �������

      par := VarArrayCreate([0,3], varInteger);

      ptKadri.Active:=False;
      ptKadri.Active:=True;
      if ptDep.Active=False then ptDep.Active:=True;

      while iMCur<=iMe do
      begin
        Memo1.Lines.Add('  ������ '+its(iMCur));

        with dmMC do
        begin
          quRepCassir.Active:=False;
          quRepCassir.SQL.Clear;
          quRepCassir.SQL.Add('select GrCode,Cassir,Cash_Code,DateOperation,Count(*) as CountPos,SUM(Summa) as Summa from "cq'+its(iMCur)+'"');
          quRepCassir.SQL.Add('where DateOperation>='''+ds(CommonSet.DateBeg)+'''');
          quRepCassir.SQL.Add('and DateOperation<='''+ds(CommonSet.DateEnd)+'''');
          quRepCassir.SQL.Add('and Operation=''P''');
          quRepCassir.SQL.Add('group by GrCode,Cassir,Cash_Code,DateOperation');
          delay(10);
          quRepCassir.Active:=True;

          quRepCassir.First;
          while not quRepCassir.Eof do
          begin
            par[0]:=StrToIntDef(quRepCassirCassir.AsString,0);
            par[1]:=quRepCassirGrCode.AsInteger;
            par[2]:=quRepCassirCash_Code.AsInteger;
            par[3]:=Trunc(quRepCassirDateOperation.AsDateTime);
            if teCassirs.Locate('Id;iDep;CashNum;iDate',par,[]) then
            begin //����� - �����������
              teCassirs.Edit;
              teCassirsCountPos.AsInteger:=teCassirsCountPos.AsInteger+quRepCassirCountPos.AsInteger;
              teCassirsRSum.AsFloat:=teCassirsRSum.AsFloat+quRepCassirSumma.AsFloat;
              teCassirs.Post;
            end else
            begin  // ���������
              NameCassir:='';
              NameDep:='';
              if ptDep.FindKey([quRepCassirGrCode.AsInteger]) then NameDep:=OemToAnsiConvert(ptDepFullName.AsString);

              iCas:=StrToIntDef(quRepCassirCassir.AsString,0);
              if ptKadri.FindKey([iCas]) then NameCassir:=OemToAnsiConvert(ptKadriFamilia.AsString+' '+ptKadriIma.AsString);

              teCassirs.Append;
              teCassirsId.AsInteger:=StrToIntDef(quRepCassirCassir.AsString,0);
              teCassirsName.AsString:=NameCassir;
              teCassirsRSum.AsFloat:=quRepCassirSumma.AsFloat;
              teCassirsCountPos.AsInteger:=quRepCassirCountPos.AsInteger;
              teCassirsDepart.AsString:=NameDep;
              teCassirsiDep.AsInteger:=quRepCassirGrCode.AsInteger;
              teCassirsCashNum.AsInteger:=quRepCassirCash_Code.AsInteger;
              teCassirsiDate.AsInteger:=Trunc(quRepCassirDateOperation.AsDateTime);
              teCassirsdDate.AsDateTime:=Trunc(quRepCassirDateOperation.AsDateTime);
              teCassirs.Post;
            end;

            quRepCassir.Next;
          end;

          quRepCassir.Active:=False;
        end;


        iMCur:=incmm(iMCur);
      end;
      ptKadri.Active:=False;

      ViewCassir.EndUpdate;
      bCalc:=False;
      Memo1.Lines.Add('������ ��');
    end;
  end;
end;

procedure TfmMainMCryst.acGrafPostavokExecute(Sender: TObject);
begin
  //������ ��������
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    begin
      quGrafPost.Active:=False;
      quGrafPost.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quGrafPost.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);
      quGrafPost.Active:=True;

      fmGrafPostavok:=tfmGrafPostavok.Create(Application);
      fmGrafPostavok.ShowModal;
      fmGrafPostavok.Release;

      quGrafPost.Active:=False;

    end;
  end;
end;

procedure TfmMainMCryst.acPartFil0Execute(Sender: TObject);
Var iC,iCli,iCh:Integer;
    rPr0,rPr:Real;
    CurD:TDateTime;
begin
//��������� Price0 � ��������� ������� � Sum0 � ��������� - ������� ����������� Price � SumIn
  with dmMt do
  with dmMC do
  begin
    try
      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('��������� ������.');
      fmSt.Memo1.Lines.Add('   ���� ���������� ... �����');

      fmAddDoc2.Vi1.BeginUpdate;

      if ptPartIN.Active=False then ptPartIN.Active:=True else ptPartIN.Refresh;
       ptPartIn.IndexFieldNames:='ID';

      if ptTTnInLn.Active=False then ptTTnInLn.Active:=True else ptTTnInLn.Refresh;
      ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

      if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
      ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';


      fmSt.Memo1.Lines.Add('     ����� - '+its(ptPartIN.RecordCount));
      delay(10);
      iC:=0;
      ptPartIN.First;
      while not ptPartIN.Eof do
      begin
        if ptPartINIDATE.AsInteger>=40544 then //c 2011
        begin
          rPr0:=ptPartINPRICEIN.AsFloat;
          rPr:=ptPartINPRICEIN.AsFloat;
          iCli:=prFindCliCodecb(ptPartINSINNCLI.AsString);

          //����� ������ ����������� ���� �� ��������� � 2012 ���� ����� ������ ��������
          if ptPartINIDATE.AsInteger>=40909 then
          begin
            if ptPartINDTYPE.AsInteger=1 then //������� - ��������� ���� � ������ ������ ���� ��� ���
            begin
              CurD:=ptPartINIDATE.AsInteger;
              ptTTnInLn.CancelRange;
              ptTTnInLn.SetRange([ptPartINARTICUL.AsInteger,CurD],[ptPartINARTICUL.AsInteger,CurD+1]);
              ptTTnInLn.First;
              while not ptTTnInLn.Eof do
              begin
                try
                  rPr:=ptTTnInLnCenaTovar.AsFloat;
                  rPr0:=ptTTnInLnOutNDSSum.AsFloat/ptTTnInLnKol.AsFloat; // ��� ���
                except
                  rPr0:=rPr;
                end;
                Break; //����� ������ �������� -  �� ��������� ��� . ����� ������� ���� ��������� �������� ...
                ptTTnInLn.Next;
              end;
            end;
          end;

          ptPartIN.Edit;
          ptPartINPRICEIN.AsFloat:=rPr;
          ptPartINPRICEIN0.AsFloat:=rPr0;
          ptPartINIDC.AsInteger:=iCli;
          ptPartIN.Post;

        end;

        inc(iC);
        if iC mod 100 = 0 then
        begin
          fmSt.StatusBar1.Panels[0].Text:='�� - '+its(iC);
          delay(10);
        end;
        ptPartIN.Next;
      end;
      fmSt.Memo1.Lines.Add('�� ��.');
      fmSt.Memo1.Lines.Add('');
      fmSt.Memo1.Lines.Add('��������� ������.');

      if ptPartO.Active=False then ptPartO.Active:=True else ptPartO.Refresh;
      ptPartO.IndexFieldNames:='ITYPE;IDDOC;ARTICUL';

      fmSt.Memo1.Lines.Add('     ����� - '+its(ptPartO.RecordCount));
      delay(10);
      iC:=0;
      ptPartO.First;
      while not ptPartO.Eof do
      begin
        if ptPartOIDATE.AsInteger>=40544 then //������ 2011 ���� �����������
        begin
          rPr:=ptPartOSUMIN.AsFloat;
          rPr0:=ptPartOSUMIN.AsFloat;
          iCli:=prFindCliCodecb(ptPartOSINN.AsString);

          if (ptPartOIDATE.AsInteger>=40909)and(ptPartOPARTIN.AsInteger>0) then //������ 2012 ���� ����������� ������
          begin
            if ptPartIn.FindKey([ptPartOPARTIN.AsInteger]) then
            begin
              rPr:=ptPartOQUANT.AsFloat*ptPartINPRICEIN.AsFloat;
              rPr0:=ptPartOQUANT.AsFloat*ptPartINPRICEIN0.AsFloat;
            end;
          end;

          ptPartO.Edit;
          ptPartOSUMIN.AsFloat:=rPr;
          ptPartOSUMIN0.AsFloat:=rPr0;
          ptPartOIDC.AsInteger:=iCli;
          ptPartO.Post;

          inc(iC);
          if iC mod 100 = 0 then
          begin
            fmSt.StatusBar1.Panels[0].Text:='�� - '+its(iC);
            delay(10);
          end;
        end;
        ptPartO.Next;
      end;

      fmSt.Memo1.Lines.Add('��������� ������ ��.');

      //����� ���������� ��� ���� ��� ��� � ������� � ����� 3 - ���

      fmSt.Memo1.Lines.Add('');
      fmSt.Memo1.Lines.Add('��������� ��������� - ��� (3)');

      iC:=0;    iCh:=0;

      quSOutH3.Active:=False;
      quSOutH3.SQL.Clear;
      quSOutH3.SQL.Add('select * from "TTNOut"');
      quSOutH3.SQL.Add('where DateInvoice>='''+ds(40909)+'''');  //2012 ���
//      quSOutH3.SQL.Add('and "Depart"='+its(iD));
      quSOutH3.SQL.Add('and "AcStatus"=3');
//      quSOutH3.SQL.Add('and "ProvodType"=3');
      quSOutH3.SQL.Add('and "ProvodType"<>4'); //��� ���������
      quSOutH3.SQL.Add('Order by Depart, DateInvoice, Number');


      quSOutH3.Active:=True;
      quSOutH3.First;
      while not quSOutH3.Eof do
      begin
        ptOutLn.CancelRange;
        ptOutLn.SetRange([quSOutH3Depart.AsInteger,quSOutH3DateInvoice.AsDateTime,AnsiToOemConvert(quSOutH3Number.AsString)],[quSOutH3Depart.AsInteger,quSOutH3DateInvoice.AsDateTime,AnsiToOemConvert(quSOutH3Number.AsString)]);
        ptOutLn.First;
        while not ptOutLn.Eof do
        begin //����� �� ������������ ��� ���������� ���� ��� ���
          rPr0:=0;
          if ptPartO.FindKey([2,quSOutH3ID.AsInteger,ptOutLnCodeTovar.AsInteger]) then
          begin
            if ptPartOPARTIN.AsInteger>0 then
            begin
              if abs(ptPartOQUANT.AsFloat)>0.001 then rPr0:=ptPartOSUMIN0.AsFloat/ptPartOQUANT.AsFloat;
            end;
          end;
          if rPr0=0 then //�� ��������� ������� ���������� �� ������� ���� �������
          begin
            quPost5.Active:=False;
            quPost5.ParamByName('IDCARD').Value:=ptOutLnCodeTovar.AsInteger;
            quPost5.ParamByName('DDATE').Value:=quSOutH3DateInvoice.AsDateTime;
            quPost5.Active:=True;

            quPost5.First;
            while not quPost5.Eof do
            begin
              rPr0:=quPost5CenaTovar0.AsFloat; //���� ������ ��� ���
              break;
            end;
            quPost5.Active:=False;
          end;

          if rPr0=0 then //����� ���������� �� �������
          begin
            rPr0:=ptOutLnCenaTovar.AsFloat;
          end;

          if rPr0>0 then
          begin
            ptOutLn.Edit;
            ptOutLnCenaTaraSpis.AsFloat:=rPr0;
            ptOutLn.Post;

            inc(iCh);
          end;

          ptOutLn.Next;
        end;

        inc(iC);
        if iC mod 100 = 0 then
        begin
          fmSt.StatusBar1.Panels[0].Text:='��3 - '+its(iC)+' ���. - '+its(iCh);
          delay(10);
        end;

        quSOutH3.Next;
      end;


      fmSt.Memo1.Lines.Add('��� (3) - ��');
      fmSt.Memo1.Lines.Add('');

      fmSt.Memo1.Lines.Add('������� ��������.');
    finally
      fmAddDoc2.Vi1.EndUpdate;
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;

end;

procedure TfmMainMCryst.TimerTestInvTimer(Sender: TObject);
begin
  TimerTestInv.Enabled:=False;
  with dmMC do //���� ���������������� ��������������
  begin
    quTestInv.Active:=False;
    quTestInv.SQL.Clear;
    quTestInv.SQL.Add('select * from "InventryHead"');
    quTestInv.SQL.Add('where DateInventry>='''+ds(Date-30)+'''');
    quTestInv.SQL.Add('and DateInventry<='''+ds(Date-2)+'''');
//    quTestInv.SQL.Add('and DateInventry<='''+ds(Date)+'''');
    quTestInv.SQL.Add('and Number like ''Auto%''');
    quTestInv.SQL.Add('and Status=''�''');
    quTestInv.SQL.Add('Order by DateInventry');
    quTestInv.Active:=True;

    if quTestInv.RecordCount>0 then //���� ���������������� ��������������
    begin  //��� ������� �� ���� - ��������������
      quTestInv.First;
      while not quTestInv.Eof do
      begin
        quTestZadInv.Active:=False;
        quTestZadInv.ParamByName('IID').AsInteger:=quTestInvInventry.AsInteger;
        quTestZadInv.Active:=True;
        if quTestZadInv.RecordCount>0 then
        begin
          quTestZadInv.First;

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_ZADANIYA" Set ');
          quE.SQL.Add('Status=1');
          quE.SQL.Add('where IType=2 and IdDoc='+its(quTestInvInventry.AsInteger));
          quE.ExecSQL;

        end else prAddZadan(2,quTestInvInventry.AsInteger,'����������� ��� �������������� � '+quTestInvNumber.AsString+' �� '+ds1(quTestInvDateInventry.AsDateTime));
        quTestZadInv.Active:=False;

        quTestInv.Next;
      end;
    end;
    quTestInv.Active:=False;
  end;
  TimerTestInv.Enabled:=True;
end;

procedure TfmMainMCryst.acCatStopListExecute(Sender: TObject);
begin
  if fmCatDisc.Showing then fmCatDisc.Show
  else
  begin
    with dmMT do
    with dmMC do
    begin
      fmCatDisc.Show;
      if (quCatDisc.Active=False) then quCatDisc.Active:=True;
      if (teCatDisc.Active=False) then teCatDisc.Active:=true;
      fmCatDisc.ViewCatDisc.BeginUpdate;
      dsquCatDisc.DataSet:=nil;
      if quCatDisc.RecordCount>0 then
      begin
        try
          quCatDisc.First;
          while not quCatDisc.Eof do
          begin
            teCatDisc.Append;
            teCatDiscIDgr.AsInteger:=quCatDiscIDgr.AsInteger;
            teCatDiscIDsg.AsInteger:=quCatDiscIDsg.AsInteger;
            teCatDiscID.AsInteger:=quCatDiscID.AsInteger;
            teCatDiscName.AsString:=quCatDiscName.AsString;
            teCatDiscV01.AsInteger:=quCatDiscV01.AsInteger;
            quCatDisc.Next;
          end;
          teCatDisc.Post;
          teCatDisc.First;
        finally
          dsquCatDisc.DataSet:=teCatDisc;
          fmCatDisc.ViewCatDisc.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmMainMCryst.acAlgClassExecute(Sender: TObject);
begin
  //������������� ����� ���������
  with dmMt do
  with dmMc do
  begin
    try
      fmAlgClass:=tfmAlgClass.Create(Application);
      if taAlgClass.Active=False then taAlgClass.Active:=True;
      quAlgClass.Active:=False; quAlgClass.Active:=True;

      fmAlgClass.ShowModal;
    finally
      taAlgClass.Active:=False;
      quAlgClass.Active:=False;

      fmAlgClass.Release;
    end;
  end;
end;

procedure TfmMainMCryst.acAlgExecute(Sender: TObject);
Var iStep,iC,iType:INteger;
    iDateB,iDateE,iDep:Integer;
    iMax:INteger;
    rQB,rQIn,rQR,rQRet,rQE:Real;
    kVol:Real;
    iE:Integer;
    par:Variant;
    StrWk:String;
    iVb,iVe,IdDH:INteger;
    Sinn,Skpp:String;
    iCliCB:INteger;
    LicSerN,LicOrg:String;
    LicDateB,LicDateE:String;
    dDateB,dDateE:TDateTime;
begin
  with dmMT do
  with dmMC do
  begin
    fmPreAlg.quDeps.Active:=False;
    fmPreAlg.quDeps.Active:=True;
    fmPreAlg.quDeps.First;
    fmPreAlg.cxLookupComboBox2.EditValue:=fmPreAlg.quDepsID.AsInteger;

    fmPreAlg.ShowModal;
    if fmPreAlg.ModalResult=mrOk then
    begin
      iDep:=fmPreAlg.cxLookupComboBox2.EditValue;
      iDateB:=Trunc(fmPreAlg.cxDateEdit1.Date);
      iDateE:=Trunc(fmPreAlg.cxDateEdit2.Date);
      dDateB:=iDateB;
      dDateE:=iDateE;
      fmPreAlg.quDeps.Active:=False;

      iType:=0;
      if fmPreAlg.cxRadioButton1.Checked then iType:=1;
      if fmPreAlg.cxRadioButton2.Checked then iType:=2;
      if fmPreAlg.cxRadioButton3.Checked then iType:=3;
      if fmPreAlg.cxRadioButton4.Checked then iType:=4;

      fmAlg.Memo1.Clear;


      fmAlg.PBar1.Position:=0;
      fmAlg.Caption:='����� �� �������� �� ������ c '+ds1(iDateB)+' �� '+ds1(iDateE);
      fmAlg.PBar1.Visible:=True;

      if (iType=1)or(iType=3) then
      begin
        fmAlg.LevelAlg1.Visible:=True;
        fmAlg.LevelAlg2.Visible:=False;
      end else
      begin
        fmAlg.LevelAlg1.Visible:=False;
        fmAlg.LevelAlg2.Visible:=True;
      end;

      CloseTe(fmAlg.teAlg1);
      CloseTe(fmAlg.teAlg2);

      fmAlg.Show;
      fmAlg.Memo1.Lines.Add('����� ... ���� ������');
      fmAlg.Memo1.Lines.Add('   ��� '+its(iType));
      fmAlg.Memo1.Lines.Add('   ����� '+its(iDep));

      bStopAlg:=False;

      fmAlg.Tag:=iType;

      if (iType=1)or(iType=3) then  //������ �����
      begin
        if taCards.Active=False then taCards.Active:=True;
        iMax:=taCards.RecordCount;
        iStep:=iMax div 100;
        fmAlg.Memo1.Lines.Add('   ����� '+its(iMax));
        iC:=0;

        par := VarArrayCreate([0,1], varInteger);


        try
          fmAlg.ViewAlg1.BeginUpdate;

          if ptCG.Active=False then ptCG.Active:=True;
          if ptCM.Active=False then ptCM.Active:=True;
          if ptDep.Active=False then ptDep.Active:=True;

          taCards.First;
          while (taCards.Eof=False)and(bStopAlg=False) do
          begin
            //������ ��������
            if (taCardsV10.AsInteger>0) //�����
            and (taCardsV11.AsInteger>0) //��� ���������
            and (taCardsV12.AsInteger>0) //�������������
            then    //��������� ����������
            begin
              iVb:=0; iVe:=500;
              if iType=1 then begin iVb:=0; iVe:=500; end;
              if iType=3 then begin iVb:=500; iVe:=550; end;

              if (taCardsV11.AsInteger>=iVb)and(taCardsV11.AsInteger<iVe) then
              begin //�������� ���� - ��� �������� ���������
                kVol:=taCardsV10.AsInteger/1000;
                rQB:=0;rQIn:=0;rQRet:=0;rQE:=0;
//                rQR:=0;
                iE:=0; //��������� ������ �������

                ptCG.CancelRange;
                ptCG.SetRange([0,taCardsID.AsInteger],[0,taCardsID.AsInteger]);
                ptCG.First;
                while not ptCG.Eof do
                begin
                  if ptCGDEPART.AsInteger=iDep then
                  begin
                    ptCM.Last;
                    while not ptCM.Bof do
                    begin
                      if (iDateE>=ptCMMOVEDATE.AsDateTime) then
                      begin
                        if iE=0 then
                        begin
                          rQE:=ptCMQUANT_REST.AsFloat;  //���� ������ ������ � ����� ������� �������� c rjywf
                          iE:=1; //���� ������ ���� ��� - ����� ���� 0
                        end;
                        if iDateB>ptCMMOVEDATE.AsDateTime then
                        begin
                          rQB:=ptCMQUANT_REST.AsFloat; //��������� �������� ���� ����� ����� - ������� �� ����� � �� ������ ����������
                          break; //���� �� ������� �� ����� �.�. ����� � �� ����
                        end else
                        begin   //�� � ��������� - ����� ������� �����
                          rQB:=ptCMQUANT_REST.AsFloat-ptCMQUANT_MOVE.AsFloat;
                          if ptCMDOC_TYPE.AsInteger=1 then rQIn:=rQIn+ptCMQUANT_MOVE.AsFloat;
                          if ptCMDOC_TYPE.AsInteger=2 then rQRet:=rQRet+abs(ptCMQUANT_MOVE.AsFloat);  //������ ������� �������� abs
                        end;
                      end;
                      ptCM.Prior;
                    end;
                  end;
                  ptCG.Next;
                end;

                rQR:=rQB+rQIn-rQRet-rQE; //���������� ��������� �����

                if (abs(rQB)>0.001)
                or (abs(rQIn)>0.001)
                or (abs(rQRet)>0.001)
                or (abs(rQR)>0.001)
                or (abs(rQE)>0.001) then
                begin //���� �������� - ����� ��������� � �������
                  par[0]:=taCardsV11.AsInteger;
                  par[1]:=taCardsV12.AsInteger;
                  with fmAlg do
                  begin
                    if teAlg1.Locate('iVid;ProdId',par,[]) then
                    begin //�����������
                      teAlg1.Edit;
                      teAlg1QuantB.AsFloat:=teAlg1QuantB.AsFloat+(rQB*kVol)/10;
                      teAlg1QuantIn.AsFloat:=teAlg1QuantIn.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantIn1.AsFloat:=teAlg1QuantIn1.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantIn2.AsFloat:=teAlg1QuantIn2.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantR.AsFloat:=teAlg1QuantR.AsFloat+(rQR*kVol)/10;
                      teAlg1QuantRet.AsFloat:=teAlg1QuantRet.AsFloat+(rQRet*kVol)/10;
                      teAlg1QuantOut.AsFloat:=teAlg1QuantOut.AsFloat+((rQR+rQRet)*kVol)/10;
                      teAlg1QuantE.AsFloat:=teAlg1QuantE.AsFloat+(rQE*kVol)/10;
                      teAlg1.Post;
                    end else
                    begin  //��������� ������
                      teAlg1.Append;
                      teAlg1sVid.AsString:=' ';
                      teAlg1iVid.AsInteger:=taCardsV11.AsInteger;
                      teAlg1sIVid.AsString:=its(teAlg1iVid.AsInteger);
                      teAlg1ProdId.AsInteger:=taCardsV12.AsInteger;
                      teAlg1Prod.AsString:=' ';
                      teAlg1ProdInn.AsString:=' ';
                      teAlg1ProdKpp.AsString:=' ';
                      teAlg1QuantB.AsFloat:=(rQB*kVol)/10;
                      teAlg1QuantIn.AsFloat:=(rQIn*kVol)/10;
                      teAlg1Col1_1.AsString:=' ';
                      teAlg1Col1_2.AsString:=' ';
                      teAlg1QuantIn1.AsFloat:=(rQIn*kVol)/10;
                      teAlg1Col1_3.AsString:=' ';
                      teAlg1Col1_4.AsString:=' ';
                      teAlg1Col1_5.AsString:=' ';
                      teAlg1QuantIn2.AsFloat:=(rQIn*kVol)/10;
                      teAlg1QuantR.AsFloat:=(rQR*kVol)/10;
                      teAlg1Col2_1.AsString:=' ';
                      teAlg1QuantRet.AsFloat:=(rQRet*kVol)/10;
                      teAlg1Col2_2.AsString:=' ';
                      teAlg1QuantOut.AsFloat:=((rQR+rQRet)*kVol)/10;
                      teAlg1QuantE.AsFloat:=(rQE*kVol)/10;
                      teAlg1.Post;
                    end;
                  end;
                end;
              end;
            end;

            taCards.Next; inc(iC);
            if iC mod iStep = 0 then
            begin
              fmAlg.PBar1.Position:=iC div iStep;
              delay(10);
            end;
          end;

          with fmAlg do
          begin
            ptMakers.Active:=False;
            ptMakers.Active:=True;
            taAlgClass.Active:=False;
            taAlgClass.Active:=True;
            teAlg1.First;
            while not teAlg1.Eof do
            begin
              if ptMakers.Locate('ID',teAlg1ProdId.AsInteger,[]) then
              begin
                if taAlgClass.Locate('ID',teAlg1iVid.AsInteger,[]) then
                begin
                  StrWk:=its(teAlg1iVid.AsInteger);
                  while length(StrWk)<3 do StrWk:='0'+StrWk;

                  teAlg1.Edit;
                  teAlg1sVid.AsString:=OemToAnsiConvert(taAlgClassNAMECLA.AsString);
                  teAlg1sIVid.AsString:=StrWk;
                  teAlg1Prod.AsString:=OemToAnsiConvert(ptMakersNAMEM.AsString);
                  teAlg1ProdInn.AsString:=OemToAnsiConvert(ptMakersINNM.AsString);
                  teAlg1ProdKpp.AsString:=OemToAnsiConvert(ptMakersKPPM.AsString);
                  teAlg1.Post;
                end;
              end;

              teAlg1.Next;
            end;
            ptMakers.Active:=False;
            taAlgClass.Active:=False;
          end;
        finally
          fmAlg.ViewAlg1.EndUpdate;
        end;
      end;

      if (iType=2)or(iType=4) then  //������ �����
      begin
        if taCards.Active=False then taCards.Active:=True;
        iMax:=taCards.RecordCount;
        iStep:=iMax div 100;
        fmAlg.Memo1.Lines.Add('   ����� '+its(iMax));
        iC:=0;

        par := VarArrayCreate([0,2], varInteger);


        try
          fmAlg.ViewAlg2.BeginUpdate;

          if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;
          if ptTTnIn.Active=False then ptTTnIn.Active:=True;
          if ptCliLic.Active=False then ptCliLic.Active:=True;

          taCards.First;
          while (taCards.Eof=False)and(bStopAlg=False) do
          begin
            //������ ��������
            if (taCardsV10.AsInteger>0) //�����
            and (taCardsV11.AsInteger>0) //��� ���������
            and (taCardsV12.AsInteger>0) //�������������
            then    //��������� ����������
            begin
              iVb:=0; iVe:=500;
              if iType=2 then begin iVb:=0; iVe:=500; end;
              if iType=4 then begin iVb:=500; iVe:=550; end;

              if (taCardsV11.AsInteger>=iVb)and(taCardsV11.AsInteger<iVe) then
              begin //�������� ���� - ��� �������� ���������
                kVol:=taCardsV10.AsInteger/1000;

                ptTTnInLn.CancelRange;
                ptTTnInLn.SetRange([taCardsID.AsInteger,dDateB],[taCardsID.AsInteger,dDateE]);
                ptTTnInLn.First;
                while not ptTTnInLn.Eof do
                begin
                  if ptTTnInLnDepart.AsInteger=iDep then
                  begin
                    if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                    begin
                      IdDH:=ptTTNInID.AsInteger;

                      par[0]:=taCardsV11.AsInteger;
                      par[1]:=taCardsV12.AsInteger;
                      par[2]:=IdDH;

                      rQIn:=ptTTnInLnKol.AsFloat;

                      with fmAlg do
                      begin
                        if teAlg2.Locate('iVid;ProdId;DocId',par,[]) then
                        begin //�����������
                          teAlg2.Edit;
                          teAlg2Quant.AsFloat:=teAlg2Quant.AsFloat+(rQIn*kVol)/10;
                          teAlg2.Post;
                        end else
                        begin  //��������� ������  -  ����� ���� ������
                          LicSerN:='';
                          LicOrg:='';
                          LicDateB:='';
                          LicDateE:='';

                          ptCliLic.CancelRange;
                          ptCliLic.SetRange([1,ptTTNInCodePost.AsInteger],[1,ptTTNInCodePost.AsInteger]);
                          ptCliLic.First;
                          while not ptCliLic.Eof do
                          begin
                            LicSerN:=OemToAnsiConvert(ptCliLicSER.AsString)+' '+OemToAnsiConvert(ptCliLicSNUM.AsString);
                            LicOrg:=OemToAnsiConvert(ptCliLicORGAN.AsString);
                            LicDateB:=ds1(ptCliLicDDATEB.AsDateTime);
                            LicDateE:=ds1(ptCliLicDDATEE.AsDateTime);;

                            ptCliLic.Next;
                          end;

                          teAlg2.Append;
                          teAlg2sVid.AsString:=' ';
                          teAlg2iVid.AsInteger:=taCardsV11.AsInteger;
                          teAlg2sIVid.AsString:=its(teAlg1iVid.AsInteger);
                          teAlg2ProdId.AsInteger:=taCardsV12.AsInteger;
                          teAlg2Prod.AsString:=' ';
                          teAlg2ProdInn.AsString:=' ';
                          teAlg2ProdKpp.AsString:=' ';
                          teAlg2iCli.AsInteger:=ptTTNInCodePost.AsInteger;
                          teAlg2CliName.AsString:=prFindCliName2(ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,Sinn,Skpp,iCliCB);
                          teAlg2CliInn.AsString:=Sinn;
                          teAlg2CliKpp.AsString:=Skpp;
                          teAlg2LicSerN.AsString:=LicSerN;
                          teAlg2LicDateB.AsString:=LicDateB;
                          teAlg2LicDateE.AsString:=LicDateE;
                          teAlg2LicOrg.AsString:=LicOrg;
                          teAlg2DocId.AsInteger:=IdDh;
                          teAlg2DocDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                          teAlg2DocNum.AsString:=OemToAnsiConvert(ptTTnInLnNumber.AsString);
                          teAlg2DocGTD.AsString:=' ';
                          teAlg2Quant.AsFloat:=(rQIn*kVol)/10;
                          teAlg2.Post;
                        end;
                      end;
                    end;
                  end;

                  ptTTnInLn.Next;
                end;
                
              end;
            end;

            taCards.Next; inc(iC);
            if iC mod iStep = 0 then
            begin
              fmAlg.PBar1.Position:=iC div iStep;
              delay(10);
            end;
          end;

          with fmAlg do
          begin
            ptMakers.Active:=False;
            ptMakers.Active:=True;
            taAlgClass.Active:=False;
            taAlgClass.Active:=True;
            teAlg2.First;
            while not teAlg2.Eof do
            begin
              if ptMakers.Locate('ID',teAlg2ProdId.AsInteger,[]) then
              begin
                if taAlgClass.Locate('ID',teAlg2iVid.AsInteger,[]) then
                begin
                  StrWk:=its(teAlg2iVid.AsInteger);
                  while length(StrWk)<3 do StrWk:='0'+StrWk;

                  teAlg2.Edit;
                  teAlg2sVid.AsString:=OemToAnsiConvert(taAlgClassNAMECLA.AsString);
                  teAlg2sIVid.AsString:=StrWk;
                  teAlg2Prod.AsString:=OemToAnsiConvert(ptMakersNAMEM.AsString);
                  teAlg2ProdInn.AsString:=OemToAnsiConvert(ptMakersINNM.AsString);
                  teAlg2ProdKpp.AsString:=OemToAnsiConvert(ptMakersKPPM.AsString);
                  teAlg2.Post;
                end;
              end;

              teAlg2.Next;
            end;
            ptMakers.Active:=False;
            taAlgClass.Active:=False;
          end;

          ptTTnInLn.Active:=False;
          ptTTnIn.Active:=False;
          ptCliLic.Active:=False;


        finally
          fmAlg.ViewAlg2.EndUpdate;
        end;
      end;

      fmAlg.Memo1.Lines.Add('������ ��');

      fmAlg.PBar1.Position:=100;
      delay(1000);
      fmAlg.PBar1.Visible:=False;

    end else fmPreAlg.quDeps.Active:=False;
  end;
end;

procedure TfmMainMCryst.acKassirExecute(Sender: TObject);
begin
  with dmMt do
  with dmMc do
  begin
    try
      fmKadr:=tfmKadr.Create(Application);
      if ptKadr.Active=False then ptKadr.Active:=True;
      with fmKadr do
      begin
        fmKadr.ViKassir.BeginUpdate;
        CloseTe(teKadr);
        ptKadr.First;
        while not ptKadr.Eof do
        begin
          if (ptKadrISPEC.AsInteger=0)and(ptKadrIKASSIR.AsInteger=1) then
          begin
            teKadr.Append;
            teKadrID.AsInteger:=ptKadrID.AsInteger;
            teKadrNAMECASSIR.AsString:=OemToAnsiConvert(ptKadrNAMECASSIR.asstring);
            teKadrPASSW.AsString:=OemToAnsiConvert(ptKadrPASSW.AsString);
            teKadrIACTIVE.AsInteger:=ptKadrIACTIVE.AsInteger;
            teKadrITYPE.AsInteger:=ptKadrITYPE.AsInteger;
            teKadrSDOLG.AsString:=AnsiUpperCase(OemToAnsiConvert(ptKadrSDOLG.AsString));
            teKadr.Post;
          end;

          ptKadr.Next;
        end;
        fmKadr.ViKassir.EndUpdate;
      end;

      fmKadr.ShowModal;
    finally
      ptKadr.Active:=False;
      fmKadr.Release;
    end;
  end;
end;

procedure TfmMainMCryst.acReasonVozExecute(Sender: TObject);
begin
 fmReasonVoz.show;
 dmP.quReasV.Active:=false;
 dmP.quReasV.Active:=true;
end;

procedure TfmMainMCryst.acGrafVozExecute(Sender: TObject);
begin
 //������ ��������
//  ������
//  if fmGrafVozvrat.Showing then fmGrafVozvrat.Close;
  fmPeriod1.cxDateEdit1.Date:=Date;
  fmPeriod1.cxDateEdit2.Date:=Date+180;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    fmGrafVozvrat.ViewGrafVozvrat.BeginUpdate;
    CommonSet.DateBeg:=fmPeriod1.cxDateEdit1.Date;
    CommonSet.DateEnd:=fmPeriod1.cxDateEdit2.Date;
    with dmP do
    begin
      dsquGrafVozvrat.DataSet:=nil;
      quGrafVozvrat.Active:=False;

      quGrafVozvrat.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quGrafVozvrat.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);

      quGrafVozvrat.Active:=True;
      dsquGrafVozvrat.DataSet:=quGrafVozvrat;

      delay(50);

      fmGrafVozvrat:=tfmGrafVozvrat.Create(Application);
      fmGrafVozvrat.CheckBox1.Checked:=false;

      //fmGrafVozvrat.Show;

    end;
    fmGrafVozvrat.ViewGrafVozvrat.EndUpdate;
    fmGrafVozvrat.Show;
  end;

end;

procedure TfmMainMCryst.acDocHVozExecute(Sender: TObject);
begin
 if fmDocs8.Showing then fmDocs8.Close;

  if trunc(fmPeriod1.cxDateEdit1.Date)>0 then
  begin
   CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
   CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
  end
  else
  begin
   CommonSet.DateBeg:=Trunc(Date);
   CommonSet.DateEnd:=Trunc(Date);
  end;

  //fmDocs8.Caption:='���������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  fmDocs8.Show;
  prloadpredz;

end;

procedure TfmMainMCryst.acAlcoNewExecute(Sender: TObject);
begin
  with dmAlg do
  begin
    fmAlcoE.Caption:='������ ����������� ��������� �� ������ � '+ds1(CommonSet.ADateBeg)+' �� '+ds1(CommonSet.ADateEnd);
    fmAlcoE.Memo1.Clear;

    fmAlcoE.Show;
    fmAlcoE.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);
    fmAlcoE.ViewAlco.BeginUpdate;
    quAlcoDH.Active:=False;
    quAlcoDH.ParamByName('IDATEB').Value:=Trunc(CommonSet.ADateBeg);
    quAlcoDH.ParamByName('IDATEE').Value:=Trunc(CommonSet.ADateEnd);
    quAlcoDH.Active:=True;

    fmAlcoE.ViewAlco.EndUpdate;
    fmAlcoE.Memo1.Lines.Add('������������ ������ ��.'); delay(10);
  end;
end;

procedure TfmMainMCryst.acCalcRecalcExecute(Sender: TObject);
Var DateB,DateE:INteger;
    rRemn,rSumR:Real;
    iC:INteger;
begin
  //�������� �������������

  fmPeriod1.cxDateEdit1.Date:=prGetTA;
  fmPeriod1.cxDateEdit2.Date:=Date;

  fmPeriod1.Label3.Caption:='��������� �������� ����������?';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    with dmMC do
    with dmMT do
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

//      if (DateB>prGetTA)or(DateE<Trunc(Date)) then
      if (DateB=0)then
      begin
        showmessage('������������ ������ �������. ������� ������ ����� !!!');
      end else
      begin

        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;
        fmSt.Memo1.Lines.Add('������.');

        fmDocs5.ViewDocsInv.BeginUpdate;
        try
          quTTNInv.Active:=False;
          quTTNInv.ParamByName('DATEB').AsDate:=DateB;
          quTTNInv.ParamByName('DATEE').AsDate:=DateE;  // ��� <=
          quTTNInv.Active:=True;

          if ptInvLine1.Active=False then ptInvLine1.Active:=True;

          quTTNInv.First;
          while not quTTNInv.Eof do
          begin
            if quTTNInvItem.AsInteger<>1 then //���� �������� 333   //�� ��������
            begin
//              ptInvLine1.Refresh;

              fmSt.Memo1.Lines.Add(' - �������� '+quTTnInvNumber.AsString);

              iC:=0;

              ptInvLine1.CancelRange;
              ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
              ptInvLine1.First;
              while not ptInvLine1.Eof do
              begin
                if (ptInvLine1ITEM.AsInteger>0) then
                begin
                  rRemn:=prFindTRemnDepDInv(ptInvLine1ITEM.AsInteger,quTTnInvDepart.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime))
                end else rRemn:=0;

                if abs(rRemn-ptInvLine1QUANTR.AsFloat)>0.001 then
                begin
                  prCalcSumInRemn(ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvDepart.AsInteger,rRemn,rSumR);

                  ptInvLine1.Edit;
                  ptInvLine1QUANTR.AsFloat:=rRemn;
                  ptInvLine1QUANTD.AsFloat:=ptInvLine1QUANTF.AsFloat-rREmn;
                  ptInvLine1SUMR.AsFloat:=rv(rREmn*ptInvLine1PRICER.AsFloat);
                  ptInvLine1SUMD.AsFloat:=ptInvLine1SUMF.AsFloat-rv(rREmn*ptInvLine1PRICER.AsFloat);
                  ptInvLine1SUMRSS.AsFloat:=rSumR;
                  ptInvLine1SUMDSS.AsFloat:=ptInvLine1SUMFSS.AsFloat-rSumR;
                  ptInvLine1QUANTC.AsFloat:=0;
                  ptInvLine1SUMC.AsFloat:=0;
                  ptInvLine1SUMRSC.AsFloat:=0;
                  ptInvLine1.Post;
                end;
                ptInvLine1.Next;   inc(iC);

                if iC mod 300 =0 then
                begin
                  fmSt.Memo1.Lines.Add('     ���������� '+its(iC)); delay(10);
                end;
              end;
              fmSt.Memo1.Lines.Add('     ���������� '+its(iC)); delay(10);
            end;
            quTTNInv.Next; delay(10);
          end;

          quTTNInv.Active:=True;
        finally
          fmDocs5.ViewDocsInv.EndUpdate;
        end;

        fmSt.Memo1.Lines.Add('�������� �������.');

        fmSt.cxButton1.Enabled:=True;
      end; //prGetTA
    end;
  end;
end;



procedure TfmMainMCryst.acTypeVozExecute(Sender: TObject);
begin
 fmTypeVoz.show;
 dmP.quTypeV.Active:=false;
 dmP.quTypeV.Active:=true;
end;

procedure TfmMainMCryst.acMV31Execute(Sender: TObject);
  var d:Tdate;
      t,user:string;
      Year, Month, Day:Word;
      iNum,dv,iStep,iC,icli:integer;
      Q,PriceIn,RSumIn,RPrice,RSUMNDS:real;
begin
  d:=date;
  DecodeDate(d, Year, Month, Day);
  Month:=12;
  Day:=31;
  d:=EncodeDate(Year, Month, Day);

  CommonSet.DateBeg:=Date;
  CommonSet.DateEnd:=Date;

  fmDocs8.PBar1.Position:=0;
  fmDocs8.Show;
  fmDocs8.PBar1.Visible:=True;

  fmDocs8.Memo1.Clear;
  fmDocs8.Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ���� ������������ ������.');
  delay(20);


  with dmP do
  with dmMC do
  begin
    if  PTSVOZ.Active=false then PTSVOZ.Active:=true;

    fmDocs8.PBar1.Position:=10; delay(50);

    quTTNOut.Active:=False;
    quTTNOut.ParamByName('DATEB').AsDate:=d;
    quTTNOut.ParamByName('DATEE').AsDate:=d;
    quTTNOut.Active:=True;

    fmDocs8.PBar1.Position:=20; delay(50);

    if (quTTNOut.RecordCount>0) then
    begin
      quTTNOut.First;

     iStep:=quTTNOut.RecordCount div 80;
     if iStep=0 then iStep:=quTTNOut.RecordCount;
     iC:=0;

      while not quTTNOut.Eof do
      begin
         //���� ��������� ����� ��������� � �����������

        iNum:=prFindNum(25)+1;

        t:=ts(now());
        user:=Person.Name;
        try
          //��������� ����� ���������
          if (iNum=1) then
          begin
           quE.Active:=false;
           quE.SQL.Clear;
           quE.SQL.Add('Insert into "IdPool" values (25,1)');
           quE.ExecSQL;
          end
          else
          begin
            quE.SQL.Clear;
            quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
            quE.SQL.Add('where CodeObject=25');
            quE.ExecSQL;
          end;

        finally
          //��������� ��������� � ����������
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_DocHRet" values (');
          quA.SQL.Add(its(iNum)+',');                                                       //id ���������
          quA.SQL.Add(''+its(iNum)+',');                                                    //DocNum
          quA.SQL.Add(its(Trunc(Date))+',');                                                   //DocDate
          quA.SQL.Add(''''+t+''',');                                                        //DocTime
          quA.SQL.Add(''''+'������� ������ �� ������� c 31.12 � ���������� ('+quTTNOutNamePost.AsString+')'+''',');         //Comment
          quA.SQL.Add(''''+user+''','+'''0'''+')');                                         //User  � ������ �������
        //  quA.SQL.SaveToFile('C:\1.txt');
          quA.ExecSQL;


       {   quDateVoz.Active:=False;
          quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
          quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
          quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
          quDateVoz.ParamByName('IDCli').AsInteger:=quTTnOutIDCLICB.AsInteger;
          quDateVoz.Active:=True;

          if  (quDateVoz.RecordCount>0) then
          dv:=quDateVozIDATEV.AsInteger
          else }dv:=trunc(date)+14;

          PTSVOZ.CancelRange;                    // ��������� ������ � ������������
          PTSVOZ.SetRangeStart;
          PTSVOZDepart.AsInteger := quTTNOutDepart.AsInteger;
          PTSVOZDateInvoice.AsDateTime := d;
          PTSVOZNumber.AsString := quTTnOutNumber.asstring;
          PTSVOZ.SetRangeEnd;
          PTSVOZDepart.AsInteger := quTTNOutDepart.AsInteger;
          PTSVOZDateInvoice.AsDateTime := d;
          PTSVOZNumber.AsString := quTTnOutNumber.asstring;
          PTSVOZ.ApplyRange;

          PTSVOZ.First;
          while not PTSVOZ.Eof do
          begin
            if (PTSVOZKol.AsFloat)>0 then
            begin
              quTypeCode.Active:=false;
              quTypeCode.ParamByName('CODE').AsInteger:=PTSVOZCodeTovar.AsInteger;
              quTypeCode.Active:=true;
              if (quTypeCode.RecordCount>0) and (quTypeCodeTypeVoz.AsInteger<>4) then           //���� ���� ����� ����� � ��� �������� �� ����� "����� � ���������"
              begin
                icli:=quTTnOutIDCLICB.AsInteger;           // ��� ����������
                if (quTypeCodeTypeVoz.AsInteger=3) then icli:=2471       //���� ��� �������� � ������ ��������� �� ��� ���������� "������������ �����"
                else                                                     //�����
                begin
                  if PTCV.Active=false then PTCV.Active:=true;
                  if PTCV.Locate('IDCB',quTTnOutIDCLICB.AsInteger,[]) then    //���� ��� �������� � ���������� ��������� �� ��� ���������� "������������ �����"
                  begin
                    if (PTCVIDTYPEVOZ.AsInteger=3) then icli:=2471;
                  end;
                end;

                Q:=PTSVOZKol.AsFloat;
                PriceIn:=PTSVOZCenaTovar.AsFloat;
                RSumIn:=PriceIn*Q;
                RPrice:=PTSVOZCenaTovarSpis.AsFloat;
                RSUMNDS:=RPrice*Q;


                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "A_DocSRet" values (');
                quA.SQL.Add(its(iNum)+','''',');                                                   //id ����� � ����-���
                quA.SQL.Add(its(PTSVOZCodeTovar.AsInteger)+',');                                    //id ������
                quA.SQL.Add(its(PTSVOZDepart.AsInteger)+',');                                       //id ������
                quA.SQL.Add(''''+FloatToStrF(Q,ffFixed,10,3)+''',');                                 //���-�� �� �������
                quA.SQL.Add('''0'''+',');                                                          //���� ���� ��� ���
                quA.SQL.Add(''''+FloatToStrF(PriceIn,ffFixed,10,2)+''',');                          //���� ���� � ���
                quA.SQL.Add('''0'''+',');                                                          //����� ���� ��� ���
                quA.SQL.Add(''''+FloatToStrF(RSumIn,ffFixed,10,2)+''',');                          //����� ���� � ���
                quA.SQL.Add(''''+FloatToStrF(RSUMNDS,ffFixed,10,2)+''',');                        //����� �������� � ���
                quA.SQL.Add(''''+FloatToStrF(RPrice,ffFixed,10,2)+''',');                         //���� ��������
                quA.SQL.Add(its(icli)+',');                                                         //id �������
                quA.SQL.Add('''1'''+',');                                                              //������� �����
                quA.SQL.Add('''0'''+',');                                                            //���-�� �������
                quA.SQL.Add('''0'''+',');                                                          //������ �������
                quA.SQL.Add(''''+FloatToStrF(Q,ffFixed,10,3)+''',');                              //���-�� ��������
                quA.SQL.Add(''''+its(dv)+''',');                                                  //���� ��������
                quA.SQL.Add(''''+its(trunc(Date))+''')');                                     //���� ���������
             //   quE.SQL.SaveToFile('C:\1.txt');
                quA.ExecSQL;
                delay(500);
              end;
            end;
            PTSVOZ.Next;
          end;
        end;

        inc(iC);
        if (iC mod iStep = 0) then
        begin
          fmDocs8.PBar1.Position:=20+(iC div iStep);
          delay(10);
        end;

        delay(500);
        quTTNOut.Next;
      end;

       //������� �� ������� ��������� �������� ������� ���������
      quD.Active:=False;
      quD.SQL.Clear;
      quD.SQL.Add('Delete from "TTNOut" where DateInvoice='''+ds(d)+'''');
      quD.ExecSQL;
      // ������ ����-��� �� ������� ���������
      quD.Active:=False;
      quD.SQL.Clear;
      quD.SQL.Add('Delete from "TTNOutLn" where DateInvoice='''+ds(d)+'''');
      quD.ExecSQL;

      fmDocs8.PBar1.Position:=100; delay(50);
      fmDocs8.Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'��������� "�������" � '''+ds(d)+''' ����������� �� �������'); delay(10);
      fmDocs8.PBar1.Visible:=False;

   //   Showmessage('��������� "�������" � '''+ds(d)+''' ����������� �� �������');
      fmDocs8.Close;
      fmDocs8.Show;
    end
    else
    begin
      fmDocs8.PBar1.Position:=100; delay(50);
      fmDocs8.Memo1.Lines.Add('��� ���������� �������� ��� �������� � ����������');
      fmDocs8.PBar1.Visible:=False;
    end;
  end;
end;

procedure TfmMainMCryst.acObrPredzExecute(Sender: TObject);
begin
 updallform;
end;

procedure TfmMainMCryst.acDelayVozExecute(Sender: TObject);
begin
  fmDelayV.VDelayVGr.Visible:=false;
  fmDelayV.VDelayVSGr.Visible:=false;
  fmDelayV.VDelayVCat.Visible:=false;

 { with dmMT do
  begin
    CloseTe(teClass);
    teClass.Active:=True;
    if ptSGr.Active=False then ptSGr.Active:=True;

    ptSGr.First;
    while not ptSGr.Eof do
    begin
     teClass.Append;
     teClassId.AsInteger:=ptSGrID.AsInteger;
     teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
     teClassNameClass.AsString:=ptSGrName.AsString;
     teClass.Post;
     ptSGr.next;
    end;
    ptSGr.Active:=False;
  end;     }

  prprosroch;

end;

procedure TfmMainMCryst.acAltPostExecute(Sender: TObject);
begin
 
 dmP.quAltPost.Active:=false;
 dmP.quAltPost.Active:=true;
 fmAltPost.show;
end;

procedure TfmMainMCryst.acInvVozExecute(Sender: TObject);
begin
  fmInvVoz.Show;
end;

procedure TfmMainMCryst.acTestRemnExecute(Sender: TObject);
Var iC,iBad:Integer;
    rQ:Real;
begin
  with dmMt do
  begin
    if MessageDlg('�������� �������� �������� �� ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if taCards.Active=False then taCards.Active:=False else taCards.Refresh;


      fmSt.StatusBar1.Panels[0].Text:='0  0'; delay(10);

      iC:=0; iBad:=0;

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;

      fmSt.Memo1.Lines.Add('���� � �������� ���������.');

      taCards.First;
      while not taCards.Eof do
      begin
        if (taCardsEdIzm.AsInteger=1)and(taCardsStatus.AsInteger<100) then
        begin
          rQ:=taCardsReserv1.AsFloat;
          rQ:=rQ-Trunc(rQ);
          if abs(rQ)>0.01 then
          begin
            inc(iBad);
            fmSt.Memo1.Lines.Add(its(taCardsID.AsInteger));
          end;
        end;


        taCards.Next; inc(iC);
        if iC mod 100=0 then
        begin
          fmSt.StatusBar1.Panels[0].Text:= its(iC)+'   '+its(iBad); delay(10);
        end;
      end;

      fmSt.Memo1.Lines.Add('�������� ���������.');
      fmSt.cxButton1.Enabled:=True;

    end;
  end;
end;

procedure TfmMainMCryst.acDocInvVozExecute(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=Date;
  fmPeriod1.cxDateEdit2.Date:=Date;
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOK then
  begin
    fmSInvVoz.ViewSInvVoz.BeginUpdate;
    dmP.quDInvV.Active:=false;
    dmP.quDInvV.ParamByName('DDateB').AsDate:=fmPeriod1.cxDateEdit1.Date;
    dmP.quDInvV.ParamByName('DDateE').AsDate:=fmPeriod1.cxDateEdit2.Date;
    dmP.quDInvV.Active:=true;
    dmP.quDInvV.First;
    fmSInvVoz.ViewSInvVoz.EndUpdate;
    fmSInvVoz.Show;
  end;

end;

procedure TfmMainMCryst.acHistOpExecute(Sender: TObject);
begin
//������� ��������
  fmHistOp.Show;
  GetHistOp(0,''); //��������� ������� �������� �� �����
end;

procedure TfmMainMCryst.acUpLicaExecute(Sender: TObject);
begin
  with dmE do
  begin
    fmUpLica.ViewUPL.BeginUpdate;
    quUPL.Active:=False;
    quUPL.Active:=True;
    quUPL.First;
    fmUpLica.ViewUPL.EndUpdate;
    fmUpLica.Show;
  end;
end;

procedure TfmMainMCryst.acSyncCashExecute(Sender: TObject);
begin
  //������������� ������ �� ������
  fmSyncCash:=TfmSyncCash.Create(Application);
  fmSyncCash.Memo1.Clear;
  CloseTe(fmSyncCash.meC);
  with dmE do
  with fmSyncCash do
  begin
    quCashList.Active:=False;
    quCashList.Active:=True;
    quCashList.First;
    while not quCashList.Eof do
    begin
      meC.Append;
      meCCashNum.AsInteger:=quCashListID.AsInteger;
      meCCashName.AsString:=quCashListName.AsString;
      meCCashStart.AsInteger:=0;
      meCPosCards.AsInteger:=0;
      meCPosBar.AsInteger:=0;
      meCPosDisc.AsInteger:=0;
      meCPosClass.AsInteger:=0;
      meCPosPers.AsInteger:=0;
      meCCashPath.AsString:=quCashListPATH.AsString;
      meC.Post;


      quCashList.Next;
    end;
    quCashList.Active:=False;
  end;
  
  fmSyncCash.ShowModal;
  fmSyncCash.Release;

end;

procedure TfmMainMCryst.acRealAlcoExecute(Sender: TObject);
Var  iDateB,iDateE,iDep:Integer;
     dDateB,dDateE:TDateTime;
     IDATE,IDREP:Integer;
     iMb,iMe,iMCur:Integer;
begin
  // ������ ���������� ����������� ���������
  with dmMT do
  with dmMC do
  begin
    fmPreRealAlco.quDeps.Active:=False;
    fmPreRealAlco.quDeps.Active:=True;
    fmPreRealAlco.quDeps.First;
    fmPreRealAlco.cxLookupComboBox2.EditValue:=fmPreRealAlco.quDepsID.AsInteger;

    fmPreRealAlco.ShowModal;
    if fmPreRealAlco.ModalResult=mrOk then
    begin
      iDep:=fmPreRealAlco.cxLookupComboBox2.EditValue;
      iDateB:=Trunc(fmPreRealAlco.cxDateEdit1.Date);
      iDateE:=Trunc(fmPreRealAlco.cxDateEdit2.Date);
      dDateB:=iDateB;
      dDateE:=iDateE;

      IDREP:=prGetANum(2);
      IDATE:=Trunc(Date);


      fmPreRealAlco.quDeps.Active:=False;

      fmRepSaleAP.iDep:=iDep;
      fmRepSaleAP.iDateB:=iDateB;
      fmRepSaleAP.iDateE:=iDateE;
      fmRepSaleAP.dDateB:=dDateB;
      fmRepSaleAP.dDateE:=dDateE;
      fmRepSaleAP.iDateRep:=IDATE;
      fmRepSaleAP.IDREP:=IDREP;

      //��������� � ������� ��

      fmRepSaleAP.Memo1.Clear;
      fmRepSaleAP.Show;
      fmRepSaleAP.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

      //���� ��������� ����
      quD.SQL.Clear;
      quD.SQL.Add('delete from "A_REALALCO" where IDATE<'+its(IDATE));
      quD.ExecSQL;

      //�������� ��������� ��� ����������
      iMb:=DateToMM(dDateB);
      iMe:=DateToMM(dDateE);
      iMCur:=iMb;
      while iMCur<=iMe do
      begin
        fmRepSaleAP.Memo1.Lines.Add('  ������ '+its(iMCur));

        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('insert into  "A_REALALCO" ("IDATE","IDREP","IDEP","DATEOPER","TIMEOP","ICODE","BARCODE","FULLNAME","QUANT","AVID","VOL")');
        quA.SQL.Add('(select '+its(IDATE)+','+its(IDREP)+','+its(iDep)+',ch.DateOperation,ch.Times,ch.Code,ca.BarCode,ca.FullName,ch.Quant,ca.V11,ca.V10/1000 from "cq'+its(iMCur)+'" ch');
        quA.SQL.Add('left join Goods ca on ca.ID=ch.Code');
        quA.SQL.Add('where ca.V11>0 and ca.V11<999');
        quA.SQL.Add('and ch.GrCode='+its(iDep)+')');
        quA.ExecSQL;

        iMCur:=incmm(iMCur);
      end;

      fmRepSaleAP.ViewRepSaleAP.BeginUpdate;
      quRepRealAP.Active:=False;
      quRepRealAP.ParamByName('IDATE').AsInteger:=IDATE;
      quRepRealAP.ParamByName('IDREP').AsInteger:=IDREP;
      quRepRealAP.ParamByName('DATEB').AsDateTime:=dDateB;
      quRepRealAP.ParamByName('DATEE').AsDateTime:=dDateE+1;
      quRepRealAP.Active:=True;
      fmRepSaleAP.ViewRepSaleAP.EndUpdate;

      fmRepSaleAP.Memo1.Lines.Add('������� ��������.'); delay(10);
{
      if IDREP=0 then
      begin
        ShowMessage('');
      end;}
    end;
  end;
end;

procedure TfmMainMCryst.acCashFBLoadAlcoExecute(Sender: TObject);
begin
  // �������� ������ ����������� ���������   fmCashFBLoadAVid
  fmCashFBLoadAVid.cxProgressBar1.Visible:=False;
  fmCashFBLoadAVid.Memo1.Clear;
//  fmCashFBLoadAVid.cxButton2.Enabled:=False;
  fmCashFBLoadAVid.ShowModal;
end;

end.
