unit Move;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmMove = class(TForm)
    FormPlacement1: TFormPlacement;
    ViewM: TcxGridDBTableView;
    LevelM: TcxGridLevel;
    GridM: TcxGrid;
    ViewMCARD: TcxGridDBColumn;
    ViewMMOVEDATE: TcxGridDBColumn;
    ViewMDOC_TYPE: TcxGridDBColumn;
    ViewMQUANT_MOVE: TcxGridDBColumn;
    ViewMQUANT_REST: TcxGridDBColumn;
    ViewMName: TcxGridDBColumn;
    ViewMDEPART: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    amMove: TActionManager;
    acExit: TAction;
    LevelP: TcxGridLevel;
    ViewP: TcxGridDBTableView;
    ViewPDepart: TcxGridDBColumn;
    ViewPName: TcxGridDBColumn;
    ViewPDateInvoice: TcxGridDBColumn;
    ViewPNumber: TcxGridDBColumn;
    ViewPCodeTovar: TcxGridDBColumn;
    ViewPCodeEdIzm: TcxGridDBColumn;
    ViewPBarCode: TcxGridDBColumn;
    ViewPNDSProc: TcxGridDBColumn;
    ViewPNDSSum: TcxGridDBColumn;
    ViewPKol: TcxGridDBColumn;
    ViewPCenaTovar: TcxGridDBColumn;
    ViewPProcent: TcxGridDBColumn;
    ViewPNewCenaTovar: TcxGridDBColumn;
    ViewPSumCenaTovarPost: TcxGridDBColumn;
    ViewPSumCenaTovar: TcxGridDBColumn;
    ViewPNameCli: TcxGridDBColumn;
    ViewMQIN: TcxGridDBColumn;
    ViewMQOUT: TcxGridDBColumn;
    ViewMNum: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewPDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMove: TfmMove;

implementation

uses Un1, MDB, Period1, AddDoc1, AddDoc2, MT, AddDoc4, AddDoc6;

{$R *.dfm}

procedure TfmMove.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridM.Align:=AlClient;
  ViewM.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewP.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmMove.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmMove.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMove.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      with dmMC do
      begin
        if fmMove.LevelM.Visible then
        begin
          fmMove.ViewM.BeginUpdate;
          prFillMove(fmMove.GridM.Tag);

          quMove.Active:=False;
          quMove.ParamByName('IDCARD').Value:=fmMove.GridM.Tag;
          quMove.ParamByName('DATEB').Value:=CommonSet.DateBeg;
          quMove.ParamByName('DATEE').Value:=CommonSet.DateEnd;
          quMove.Active:=True;
          quMove.First;

          fmMove.ViewM.EndUpdate;
          fmMove.Caption:=quCardsName.AsString+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
        end;
        if fmMove.LevelP.Visible then
        begin
          fmMove.ViewP.BeginUpdate;
          quPost.Active:=False;
          quPost.ParamByName('IDCARD').Value:=fmMove.GridM.Tag;
          quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
          quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
          quPost.Active:=True;
          quPost.First;
          fmMove.ViewP.EndUpdate;

          fmMove.Caption:=quCardsName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
        end;
      end;
  end;
end;

procedure TfmMove.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  if fmMove.LevelM.Visible then prNExportExel5(ViewM)
  else prNExportExel5(ViewP);
end;

procedure TfmMove.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMove.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewM.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewP.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  bDrIn:=False;
  bDrOut:=False;
  bDrVn:=False;
end;

procedure TfmMove.ViewPDblClick(Sender: TObject);
Var rQ:Real;
    rPriceM:Real;
    rNac:Real;
    iSS:INteger;
    rPriceIn0,rSumIn0:Real;
begin
  with dmMC do
  begin
    if bDrIn then
    begin
      if quPost.RecordCount>0 then
      begin
        if fmAddDoc1.Visible and fmAddDoc1.taSpecIn.Active and fmAddDoc1.cxButton1.Enabled then
        begin
          with fmAddDoc1 do
          begin
//            rPriceM:=prFindPrice(taSpecInCodeTovar.AsInteger);
            rQ:=taSpecInKolMest.AsFloat*taSpecInKolWithMest.AsFloat-taSpecInNecond.AsFloat-taSpecInZemlia.AsFloat-taSpecInOthodi.AsFloat;

            iCol:=0;
            taSpecIn.Edit;
            taSpecInCenaTovar.AsFloat:=quPostCenaTovar.AsFloat;
  //          taSpecInNewCenaTovar.AsFloat:=rPriceM;
            taSpecInSumCenaTovarPost.AsFloat:=rQ*quPostCenaTovar.AsFloat;
//            taSpecInSumCenaTovar.AsFloat:=rv(rQ*rPriceM);
            taSpecInNDSSum.AsFloat:=quPostCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*taSpecInNDSProc.AsFloat;
            taSpecInOutNDSSum.AsFloat:=quPostNewCenaTovar.AsFloat*rQ/(100+taSpecInNDSProc.AsFloat)*100;
            taSpecIn.Post;
          end;
        end;
      end;
      Close;
    end;
    if bDrOut then
    begin
      if quPost.RecordCount>0 then
      begin
        if fmAddDoc2.Visible and fmAddDoc2.taSpecOut.Active and fmAddDoc2.cxButton1.Enabled then
        begin
          with fmAddDoc2 do
          begin
            rQ:=taSpecOutKolMest.AsFloat*taSpecOutKolWithMest.AsFloat;
            rPriceM:=prFindPrice(taSpecOutCodeTovar.AsInteger);

            iCol:=0;
            taSpecOut.Edit;
            taSpecOutCenaTovar.AsFloat :=quPostCenaTovar.AsFloat;  //���� ����������
            taSpecOutSumCenaTovar.AsFloat:=rQ*quPostCenaTovar.AsFloat;
            if taSpecOutCenaTovarSpis.AsFloat=0 then
            begin
              taSpecOutCenaTovarSpis.AsFloat:=rPriceM; //���� ��������
              taSpecOutSumCenaTovarSpis.AsFloat:=rv(rQ*rPriceM);
            end;

            if (CommonSet.RC=0) or (cxComboBox1.ItemIndex<>3) then
            begin
              taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
              taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
            end else
            begin
              taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovarSpis.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
              taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovarSpis.AsFloat*rQ)/(100+taSpecOutNDSProc.AsFloat)*100;
            end;

            taSpecOutPostDate.AsDateTime:=quPostDateInvoice.AsDateTime;
            taSpecOutPostNum.AsString:=quPostNumber.AsString;
            taSpecOutCenaTaraSpis.AsFloat:=quPostCenaTovar0.AsFloat;
            taSpecOut.Post;
          end;
        end;
      end;
      close;
    end;
    if bDrVn then
    begin
      if quPost.RecordCount>0 then
      begin
        if fmAddDoc4.Visible and fmAddDoc4.taSpecVn.Active and fmAddDoc4.cxButton1.Enabled then
        begin
          with fmAddDoc4 do
          begin
            iCol:=1;

            iSS:=ViewP.Tag;

            rPriceIn0:=quPostCenaTovar.AsFloat;
            rSumIn0:=quPostSumCenaTovarPost.AsFloat-quPostNDSSum.AsFloat;
            if quPostKol.AsFloat<>0 then rPriceIn0:=rv(rSumIn0/quPostKol.AsFloat);

            taSpecVn.Edit;

//            taSpecVnCena.AsFloat:=quPostCenaTovar.AsFloat;
//            taSpecVnSumCena.AsFloat:=RoundVal(quPostCenaTovar.AsFloat*taSpecVnKol.AsFloat);

//            taSpecVnCenaSS.AsFloat:=quPostCenaTovar.AsFloat;
//            taSpecVnSumCenaSS.AsFloat:=RoundVal(quPostCenaTovar.AsFloat*taSpecVnKol.AsFloat);

            if iSS<>2 then
            begin
              taSpecVnCena.AsFloat:=quPostCenaTovar.AsFloat;
              taSpecVnSumCena.AsFloat:=RoundVal(quPostCenaTovar.AsFloat*taSpecVnKol.AsFloat);
            end else
            begin
              taSpecVnCena.AsFloat:=rPriceIn0;
              taSpecVnSumCena.AsFloat:=RoundVal(rPriceIn0*taSpecVnKol.AsFloat);
            end;
            taSpecVn.Post;
          end;
        end;
      end;
      Close;
    end;
    if bDrObm then
    begin
      if quPost.RecordCount>0 then
      begin
        if fmAddDoc6.Visible and fmAddDoc6.teSpecObm.Active and fmAddDoc6.cxButton1.Enabled then
        begin
          with fmAddDoc6 do
          begin
            iCol:=1;
            rQ:=teSpecObmQuant.AsFloat;
            rNac:=0;
            if teSpecObmSumOut.AsFloat<>0 then
            begin
              if rv(rQ*quPostCenaTovar.AsFloat)<>0 then
              begin
                rNac:=(teSpecObmSumOut.AsFloat-rv(rQ*quPostCenaTovar.AsFloat))/rv(rQ*quPostCenaTovar.AsFloat)*100;
              end;
            end;

            teSpecObm.Edit;
            teSpecObmPriceIn.AsFloat:=quPostCenaTovar.AsFloat;  //���� ����������
            teSpecObmSumIn.AsFloat:=rv(rQ*quPostCenaTovar.AsFloat);
            teSpecObmNac.AsFloat:=rNac;
            teSpecObm.Post;
          end;
        end;
      end;
      Close;
    end;
  end;
end;

end.
