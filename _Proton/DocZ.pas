unit DocZ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, cxTimeEdit,
  cxCalendar;

type
  TfmDocs7 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    amDocsObm: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc7: TAction;
    acEditDoc7: TAction;
    acViewDoc7: TAction;
    acDelDoc7: TAction;
    acOnDoc7: TAction;
    SpeedItem7: TSpeedItem;
    PopupMenu1: TPopupMenu;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewZak: TcxGridDBTableView;
    LevelZak: TcxGridLevel;
    GridZak: TcxGrid;
    ViewZakID: TcxGridDBColumn;
    ViewZakDOCDATE: TcxGridDBColumn;
    ViewZakDOCNUM: TcxGridDBColumn;
    ViewZakINSUMIN: TcxGridDBColumn;
    ViewZakNameDep: TcxGridDBColumn;
    ViewZakNameCli: TcxGridDBColumn;
    ViewZakINN: TcxGridDBColumn;
    ViewZakIACTIVE: TcxGridDBColumn;
    N1: TMenuItem;
    acSendPost1: TMenuItem;
    acSendPost: TAction;
    ViewZakTIMEEDIT: TcxGridDBColumn;
    ViewZakPERSON: TcxGridDBColumn;
    ViewZakSDATEEDIT: TcxGridDBColumn;
    ViewZaksDatePost: TcxGridDBColumn;
    ViewZakINSUMOUT: TcxGridDBColumn;
    ViewZakCLIQUANT: TcxGridDBColumn;
    ViewZakCLISUMIN0: TcxGridDBColumn;
    ViewZakCLISUMIN: TcxGridDBColumn;
    ViewZakCLIQUANTN: TcxGridDBColumn;
    ViewZakCLISUMIN0N: TcxGridDBColumn;
    ViewZakCLISUMINN: TcxGridDBColumn;
    ViewZakSNUMNACL: TcxGridDBColumn;
    ViewZakSNUMSCHF: TcxGridDBColumn;
    N2: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc7Execute(Sender: TObject);
    procedure acEditDoc7Execute(Sender: TObject);
    procedure acViewDoc7Execute(Sender: TObject);
    procedure acDelDoc7Execute(Sender: TObject);
    procedure acOnDoc7Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure ViewZakDblClick(Sender: TObject);
    procedure acSendPost1Click(Sender: TObject);
    procedure acSendPostExecute(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc7(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs7: TfmDocs7;
  bClearDocObm:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, MT, TBuff, u2fdk, AddDoc6, AddDoc7,
  SetStatusBZ, DateP;

{$R *.dfm}

procedure TfmDocs7.prSetValsAddDoc7(iT:SmallINt); //0-����������, 1-��������������, 2-��������
begin
  with dmMC do
  begin
    bOpen:=True;
    if iT=0 then
    begin
      fmAddDoc7.Caption:='���������: ����������.';
      fmAddDoc7.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc7.cxTextEdit1.Text:=its(prFindNum(22)+1);
      fmAddDoc7.cxTextEdit1.Tag:=0;
      fmAddDoc7.Label1.Tag:=prFindNum(22)+1;

      fmAddDoc7.cxDateEdit1.Date:=date;
      fmAddDoc7.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc7.cxButtonEdit1.Tag:=0;
      fmAddDoc7.cxButtonEdit1.EditValue:=0;
      fmAddDoc7.cxButtonEdit1.Text:='';
      fmAddDoc7.cxButtonEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc7.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmAddDoc7.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmAddDoc7.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc7.cxLabel1.Enabled:=True;
      fmAddDoc7.cxLabel2.Enabled:=True;
      fmAddDoc7.cxLabel6.Enabled:=True;
      fmAddDoc7.cxButton1.Enabled:=True;
      fmAddDoc7.cxLabel3.Enabled:=True;
      fmAddDoc7.cxLabel4.Enabled:=True;

      fmAddDoc7.ViewDoc7.OptionsData.Editing:=True;
      fmAddDoc7.ViewDoc7.OptionsData.Deleting:=True;

      fmAddDoc7.cxCalcEdit1.Value:=1;
    end;
    if iT=1 then
    begin
      fmAddDoc7.Caption:='���������: ��������������.';
      fmAddDoc7.cxTextEdit1.Text:=quZakHDOCNUM.AsString;
      fmAddDoc7.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc7.cxTextEdit1.Tag:=quZakHID.AsInteger;
      fmAddDoc7.Label1.Tag:=0;

      fmAddDoc7.cxDateEdit1.Date:=quZakHDOCDATE.AsDateTime;
      fmAddDoc7.cxDateEdit1.Properties.ReadOnly:=False;

      fmAddDoc7.Label4.Tag:=quZakHCLITYPE.AsInteger;
      fmAddDoc7.cxButtonEdit1.Tag:=quZakHCLICODE.AsInteger;
      fmAddDoc7.cxButtonEdit1.Text:=quZakHNameCli.AsString;
      fmAddDoc7.cxButtonEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True;

      fmAddDoc7.cxLookupComboBox1.EditValue:=quZakHDepart.AsInteger;
      fmAddDoc7.cxLookupComboBox1.Text:=quZakHNameDep.AsString;
      fmAddDoc7.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc7.cxLabel1.Enabled:=True;
      fmAddDoc7.cxLabel2.Enabled:=True;
      fmAddDoc7.cxLabel6.Enabled:=True;
      fmAddDoc7.cxButton1.Enabled:=True;
      fmAddDoc7.cxLabel3.Enabled:=True;
      fmAddDoc7.cxLabel4.Enabled:=True;

      fmAddDoc7.ViewDoc7.OptionsData.Editing:=True;
      fmAddDoc7.ViewDoc7.OptionsData.Deleting:=True;

      fmAddDoc7.cxCalcEdit1.Value:=1;
    end;
    if iT=2 then
    begin
      fmAddDoc7.Caption:='���������: ��������.';
      fmAddDoc7.cxTextEdit1.Text:=quZakHDOCNUM.AsString;
      fmAddDoc7.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc7.cxTextEdit1.Tag:=quZakHID.AsInteger;
      fmAddDoc7.Label1.Tag:=0;

      fmAddDoc7.cxDateEdit1.Date:=quZakHDOCDATE.AsDateTime;
      fmAddDoc7.cxDateEdit1.Properties.ReadOnly:=True;

      fmAddDoc7.Label4.Tag:=quZakHCLITYPE.AsInteger;
      fmAddDoc7.cxButtonEdit1.Tag:=quZakHCLICODE.AsInteger;
      fmAddDoc7.cxButtonEdit1.Text:=quZakHNameCli.AsString;
      fmAddDoc7.cxButtonEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc7.cxLookupComboBox1.EditValue:=quZakHDepart.AsInteger;
      fmAddDoc7.cxLookupComboBox1.Text:=quZakHNameDep.AsString;
      fmAddDoc7.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc7.cxLabel1.Enabled:=False;
      fmAddDoc7.cxLabel2.Enabled:=False;
      fmAddDoc7.cxLabel6.Enabled:=False;
      fmAddDoc7.cxButton1.Enabled:=False;
      fmAddDoc7.cxLabel3.Enabled:=False;
      fmAddDoc7.cxLabel4.Enabled:=False;

      fmAddDoc7.ViewDoc7.OptionsData.Editing:=False;
      fmAddDoc7.ViewDoc7.OptionsData.Deleting:=False;

      fmAddDoc7.cxCalcEdit1.Value:=1;
    end;
    bOpen:=False;
  end;
end;

procedure TfmDocs7.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs7.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs7.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.IniSection:='DocObm';
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridZak.Align:=AlClient;
//  ViewZak.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs7.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewZak.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocs7.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelZak.Visible then
      begin
        fmDocs7.Caption:='������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs7.ViewZak.BeginUpdate;
        try
          quZakH.Active:=False;
          quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quZakH.Active:=True;
        finally
          fmDocs7.ViewZak.EndUpdate;
        end;

      end;
    end;
  end;
end;

procedure TfmDocs7.acAddDoc7Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddDocZ') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc7(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc7.teSpecZ);
    fmAddDoc7.acSaveDoc.Enabled:=True;
    fmAddDoc7.Show;
  end;
end;

procedure TfmDocs7.acEditDoc7Execute(Sender: TObject);
//Var //IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
begin
  //�������������
  if not CanDo('prEditDocZ') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quZakH.RecordCount>0 then //���� ��� �������������
    begin
      if quZakHIACTIVE.AsInteger=0 then
      begin
        prSetValsAddDoc7(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmAddDoc7.teSpecZ);

        fmAddDoc7.acSaveDoc.Enabled:=True;

        if ptZSpec.Active=False then ptZSpec.Active:=True;
        ptZSpec.CancelRange;
        ptZSpec.IndexFieldNames:='IDH;IDS';
        ptZSpec.SetRange([quZakHID.AsInteger],[quZakHID.AsInteger]);
        with fmAddDoc7 do
        begin
          fmAddDoc7.ViewDoc7.BeginUpdate;
          try
//          iC:=1;
          ptZSpec.First;
          while not ptZSpec.Eof do
          begin
            if taCards.FindKey([ptZSpecCODE.AsInteger]) then
            begin
              teSpecZ.Append;
              teSpecZCode.AsInteger:=taCardsID.AsInteger;
              teSpecZName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teSpecZEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              teSpecZFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
              teSpecZiCat.AsInteger:=taCardsV02.AsInteger;
              teSpecZiTop.AsInteger:=taCardsV04.AsInteger;
              teSpecZiN.AsInteger:=taCardsV05.AsInteger;
              teSpecZRemn1.AsFloat:=ptZSpecREMN1.AsFloat;
              teSpecZvReal.AsFloat:=ptZSpecVREAL.AsFloat;
              teSpecZRemnDay.AsFloat:=ptZSpecREMNDAY.AsFloat;
              teSpecZRemnMin.AsInteger:=ptZSpecREMNMIN.AsInteger;
              teSpecZRemnMax.AsInteger:=ptZSpecREMNMAX.AsInteger;
              teSpecZRemn2.AsFloat:=ptZSpecREMN2.AsFloat;
              teSpecZQuant1.AsFloat:=ptZSpecQUANT1.AsFloat;
              teSpecZPK.AsFloat:=ptZSpecPK.AsFloat;
              teSpecZQuantZ.AsFloat:=ptZSpecQUANTZ.AsFloat;
              teSpecZQuant2.AsFloat:=ptZSpecQUANT2.AsFloat;
              teSpecZQuant3.AsFloat:=ptZSpecQUANT3.AsFloat;
              teSpecZPriceIn.AsFloat:=ptZSpecPRICEIN.AsFloat;
              teSpecZSumIn.AsFloat:=ptZSpecSUMIN.AsFloat;
              teSpecZNDSProc.AsFloat:=ptZSpecNDSPROC.AsFloat;
              teSpecZBarCode.AsString:=taCardsBarCode.AsString;

//              teSpecZPriceIn0.AsFloat:=
//              teSpecZSumIn0.AsFloat:=

              teSpecZCliQuant.AsFloat:=ptZSpecCLIQUANT.AsFloat;
              teSpecZCliPrice.AsFloat:=rv(ptZSpecCLIPRICE.AsFloat);
              teSpecZCliPrice0.AsFloat:=rv(ptZSpecCLIPRICE0.AsFloat);
              teSpecZCliSumIn.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE.AsFloat));
              teSpecZCliSumIn0.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE0.AsFloat));

              teSpecZCliQuantN.AsFloat:=ptZSpecCLIQUANTN.AsFloat;
              teSpecZCliPriceN.AsFloat:=rv(ptZSpecCLIPRICEN.AsFloat);
              teSpecZCliPrice0N.AsFloat:=rv(ptZSpecCLIPRICE0N.AsFloat);
              teSpecZCliSumInN.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICEN.AsFloat));
              teSpecZCliSumIn0N.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICE0N.AsFloat));

              teSpecZCliNNum.AsInteger:=ptZSpecCLINNUM.AsInteger;

              teSpecZ.Post;
//              inc(iC);
            end;
            ptZSpec.Next;
          end;
          finally
            fmAddDoc7.ViewDoc7.EndUpdate;
          end;
        end;

        fmAddDoc7.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs7.acViewDoc7Execute(Sender: TObject);
//Var //IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
begin
  //�������������
  if not CanDo('prViewDocZ') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quZakH.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc7(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmAddDoc7.teSpecZ);

      fmAddDoc7.acSaveDoc.Enabled:=True;

      if ptZSpec.Active=False then ptZSpec.Active:=True;
      ptZSpec.CancelRange;
      ptZSpec.IndexFieldNames:='IDH;IDS';
      ptZSpec.SetRange([quZakHID.AsInteger],[quZakHID.AsInteger]);
      with fmAddDoc7 do
      begin
        fmAddDoc7.ViewDoc7.BeginUpdate;
        try
//          iC:=1;
          ptZSpec.First;
          while not ptZSpec.Eof do
          begin
            if taCards.FindKey([ptZSpecCODE.AsInteger]) then
            begin
              teSpecZ.Append;
              teSpecZCode.AsInteger:=taCardsID.AsInteger;
              teSpecZName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teSpecZEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              teSpecZFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
              teSpecZiCat.AsInteger:=taCardsV02.AsInteger;
              teSpecZiTop.AsInteger:=taCardsV04.AsInteger;
              teSpecZiN.AsInteger:=taCardsV05.AsInteger;
              teSpecZRemn1.AsFloat:=ptZSpecREMN1.AsFloat;
              teSpecZvReal.AsFloat:=ptZSpecVREAL.AsFloat;
              teSpecZRemnDay.AsFloat:=ptZSpecREMNDAY.AsFloat;
              teSpecZRemnMin.AsInteger:=ptZSpecREMNMIN.AsInteger;
              teSpecZRemnMax.AsInteger:=ptZSpecREMNMAX.AsInteger;
              teSpecZRemn2.AsFloat:=ptZSpecREMN2.AsFloat;
              teSpecZQuant1.AsFloat:=ptZSpecQUANT1.AsFloat;
              teSpecZPK.AsFloat:=ptZSpecPK.AsFloat;
              teSpecZQuantZ.AsFloat:=ptZSpecQUANTZ.AsFloat;
              teSpecZQuant2.AsFloat:=ptZSpecQUANT2.AsFloat;
              teSpecZQuant3.AsFloat:=ptZSpecQUANT3.AsFloat;
              teSpecZPriceIn.AsFloat:=ptZSpecPRICEIN.AsFloat;
              teSpecZSumIn.AsFloat:=ptZSpecSUMIN.AsFloat;
              teSpecZNDSProc.AsFloat:=ptZSpecNDSPROC.AsFloat;
              teSpecZBarCode.AsString:=taCardsBarCode.AsString;

//              teSpecZPriceIn0.AsFloat:=
//              teSpecZSumIn0.AsFloat:=

              teSpecZCliQuant.AsFloat:=ptZSpecCLIQUANT.AsFloat;
              teSpecZCliPrice.AsFloat:=rv(ptZSpecCLIPRICE.AsFloat);
              teSpecZCliPrice0.AsFloat:=rv(ptZSpecCLIPRICE0.AsFloat);
              teSpecZCliSumIn.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE.AsFloat));
              teSpecZCliSumIn0.AsFloat:=rv(ptZSpecCLIQUANT.AsFloat*rv(ptZSpecCLIPRICE0.AsFloat));

              teSpecZCliQuantN.AsFloat:=ptZSpecCLIQUANTN.AsFloat;
              teSpecZCliPriceN.AsFloat:=rv(ptZSpecCLIPRICEN.AsFloat);
              teSpecZCliPrice0N.AsFloat:=rv(ptZSpecCLIPRICE0N.AsFloat);
              teSpecZCliSumInN.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICEN.AsFloat));
              teSpecZCliSumIn0N.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICE0N.AsFloat));

              teSpecZCliNNum.AsInteger:=ptZSpecCLINNUM.AsInteger;

              teSpecZ.Post;
//              inc(iC);
            end;
            ptZSpec.Next;
          end;
        finally
          fmAddDoc7.ViewDoc7.EndUpdate;
        end;
      end;
      fmAddDoc7.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs7.acDelDoc7Execute(Sender: TObject);
begin
  if not CanDo('prDelDocZ') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quZakH.RecordCount>0 then //���� ��� �������
    begin
      if quZakHIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quZakHDOCNUM.AsString+' �� '+quZakHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(6,quZakHID.AsInteger,trunc(quZakHDOCDATE.AsDateTime),quZakHDOCNUM.AsString,'Del',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "A_DOCZSPEC"');
          quD.SQL.Add('where IDH='+its(quZakHID.AsInteger));
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "A_DOCZHEAD"');
          quD.SQL.Add('where ID='+its(quZakHID.AsInteger));
          quD.ExecSQL;
          delay(100);

          prRefrID(quZakH,ViewZak,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs7.acOnDoc7Execute(Sender: TObject);
Var IDH:INteger;
begin
//������������
  with dmMC do
  with dmMT do
  begin
    if not CanDo('prOnDocZak') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quZakH.RecordCount>0 then //���� ��� ������������
    begin
      IDH:=quZakHID.AsInteger;

      fmSetStatus.cxRadioGroup1.ItemIndex:=quZakHIACTIVE.AsInteger;
      fmSetStatus.ShowModal;
      if fmSetStatus.ModalResult=mrOk then
      begin
        if ptZHead.Active=False then ptZHead.Active:=True;
        ptZHead.Refresh;

        if ptZHead.FindKey([IDH]) then
        begin
          ptZHead.Edit;
          ptZHeadIACTIVE.AsInteger:=fmSetStatus.cxRadioGroup1.ItemIndex;
          ptZHead.Post;
          delay(20);
        end;

        quZakH.Active:=False;
        quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
        quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
        quZakH.Active:=True;

        quZakH.Locate('ID',IDH,[]);

        ViewZak.Controller.FocusRecord(ViewZak.DataController.FocusedRowIndex,True);
      end;
    end else ShowMessage('�������� ��������.');
  end;
end;

procedure TfmDocs7.Timer1Timer(Sender: TObject);
begin
  if bClearDocObm=True then begin StatusBar1.Panels[0].Text:=''; bClearDocObm:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocObm:=True;
end;

procedure TfmDocs7.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs7.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  prNExportExel5(ViewZak);
end;

procedure TfmDocs7.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs7.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewZak);
end;

procedure TfmDocs7.acEdit1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocObm') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc7(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc7.teSpecZ);

    fmAddDoc7.acSaveDoc.Enabled:=True;
    fmAddDoc7.Show;
  end;
end;

procedure TfmDocs7.ViewZakDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quZakHIACTIVE.AsInteger=0 then acEditDoc7.Execute //��������������
    else acViewDoc7.Execute; //��������}
  end;
end;

procedure TfmDocs7.acSendPost1Click(Sender: TObject);
begin
//��������� �� ����������� �����
end;

procedure TfmDocs7.acSendPostExecute(Sender: TObject);
Var IDH:INteger;
begin
//��������� ����������
  with dmMC do
  with dmMT do
  begin
    if not CanDo('prSendDocZak') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quZakH.RecordCount>0 then //���� ��� ������������
    begin
      IDH:=quZakHID.AsInteger;

      if quZakHIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('��������� ���������� �������� �'+quZakHDOCNUM.AsString+' �� '+quZakHNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if ptZHead.Active=False then ptZHead.Active:=True;
          ptZHead.Refresh;

          if ptZHead.FindKey([IDH]) then
          begin
            ptZHead.Edit;
            ptZHeadIACTIVE.AsInteger:=1;
            ptZHead.Post;
            delay(20);
          end;

          quZakH.Active:=False;
          quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quZakH.Active:=True;

          quZakH.Locate('ID',IDH,[]);

          ViewZak.Controller.FocusRecord(ViewZak.DataController.FocusedRowIndex,True);
        end;
      end else ShowMessage('�������� ������ ���������.');
    end else ShowMessage('�������� ��������.');
  end;
end;

procedure TfmDocs7.N2Click(Sender: TObject);
  var user:String;
      idh:integer;
begin
  user:=Person.Name;
  if (user='OPER CB') or (user='BUH') or (user='�����') or (user='OPERZAK')
  then
  begin
    fmDateP.ShowModal;
    if fmDateP.ModalResult=mrOk then
    begin
      with dmMC do
      begin
     //   showmessage('���� '+FormatDateTime('dd.mm.yyyy ',fmDateP.cxDateEdit1.Date));

     //  showmessage('���� '+ds1(trunc(fmDateP.cxDateEdit1.Date)));
        idh:=quZakHID.AsInteger;

        quE.Active:=false;
        quE.SQL.Clear;
        quE.SQL.Add('Update "A_DOCZHEAD" set IDATENACL='+its(trunc(fmDateP.cxDateEdit1.Date))+'');
        quE.SQL.Add('where ID='+its(idh));
        quE.ExecSQL;

        fmDocs7.Caption:='������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs7.ViewZak.BeginUpdate;
        try
          quZakH.Active:=False;
          quZakH.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quZakH.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quZakH.Active:=True;
        finally
          quZakH.Locate('ID',idh,[]);
          fmDocs7.ViewZak.EndUpdate;
        end;

      end;
    end;
  end
  else  showmessage('��� ����!');
end;

end.
