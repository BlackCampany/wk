object fmPreInvView: TfmPreInvView
  Left = 479
  Top = 401
  BorderStyle = bsDialog
  Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
  ClientHeight = 229
  ClientWidth = 271
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 171
    Width = 271
    Height = 58
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 36
      Top = 12
      Width = 85
      Height = 37
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 160
      Top = 12
      Width = 85
      Height = 37
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 8
    Top = 8
    Caption = ' '#1042#1080#1076' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1072' '
    Properties.Items = <
      item
        Caption = #1042#1089#1077
        Value = 0
      end
      item
        Caption = #1041#1077#1079' '#1085#1091#1083#1077#1074#1086#1075#1086' '#1082#1086#1083'-'#1074#1072
        Value = '1'
      end
      item
        Caption = #1058#1086#1083#1100#1082#1086' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
        Value = 2
      end>
    ItemIndex = 0
    TabOrder = 1
    Height = 157
    Width = 257
  end
end
