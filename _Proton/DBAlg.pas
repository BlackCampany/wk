unit DBAlg;

interface

uses
  SysUtils, Classes, DB, pvtables, sqldataset, Un1, FIBQuery, pFIBQuery;

type
  TdmAlg = class(TDataModule)
    quAlcoDH: TPvQuery;
    quAlcoDHIdOrg: TIntegerField;
    quAlcoDHId: TIntegerField;
    quAlcoDHIDateB: TIntegerField;
    quAlcoDHIDateE: TIntegerField;
    quAlcoDHDDateB: TDateField;
    quAlcoDHDDateE: TDateField;
    quAlcoDHIType: TIntegerField;
    quAlcoDHsType: TStringField;
    quAlcoDHSPers: TStringField;
    quAlcoDHName: TStringField;
    quAlcoDHFullName: TStringField;
    dsquAlcoDH: TDataSource;
    quOrgList: TPvQuery;
    quOrgListID: TSmallintField;
    quOrgListName: TStringField;
    quOrgListFullName: TStringField;
    dsquOrgList: TDataSource;
    quDelDS: TPvQuery;
    quAlcoDHIT1: TIntegerField;
    quAlcoDHIT2: TIntegerField;
    quAlcoDS1: TPvQuery;
    quAlcoDS2: TPvQuery;
    quAlcoDS1IdH: TIntegerField;
    quAlcoDS1Id: TAutoIncField;
    quAlcoDS1iDep: TIntegerField;
    quAlcoDS1iNum: TIntegerField;
    quAlcoDS1iVid: TIntegerField;
    quAlcoDS1iProd: TIntegerField;
    quAlcoDS1rQIn1: TFloatField;
    quAlcoDS1rQIn2: TFloatField;
    quAlcoDS1rQIn3: TFloatField;
    quAlcoDS1rQInIt1: TFloatField;
    quAlcoDS1rQIn4: TFloatField;
    quAlcoDS1rQIn5: TFloatField;
    quAlcoDS1rQIn6: TFloatField;
    quAlcoDS1rQInIt: TFloatField;
    quAlcoDS1rQOut1: TFloatField;
    quAlcoDS1rQOut2: TFloatField;
    quAlcoDS1rQOut3: TFloatField;
    quAlcoDS1rQOut4: TFloatField;
    quAlcoDS1rQOutIt: TFloatField;
    quAlcoDS1rQe: TFloatField;
    quAlcoDS1NameShop: TStringField;
    quAlcoDS1NameVid: TStringField;
    quAlcoDS1NameProducer: TStringField;
    quAlcoDS1INN: TStringField;
    quAlcoDS1KPP: TStringField;
    quAlcoDS2IdH: TIntegerField;
    quAlcoDS2Id: TAutoIncField;
    quAlcoDS2iDep: TIntegerField;
    quAlcoDS2iVid: TIntegerField;
    quAlcoDS2iProd: TIntegerField;
    quAlcoDS2iPost: TIntegerField;
    quAlcoDS2iLic: TIntegerField;
    quAlcoDS2DocDate: TDateField;
    quAlcoDS2DocNum: TStringField;
    quAlcoDS2GTD: TStringField;
    quAlcoDS2rQIn: TFloatField;
    quAlcoDS2LicNum: TStringField;
    quAlcoDS2LicDateB: TDateField;
    quAlcoDS2LicDateE: TDateField;
    quAlcoDS2LicOrg: TStringField;
    quAlcoDS2IdHdr: TIntegerField;
    quAlcoDS2NameShop: TStringField;
    quAlcoDS2NameVid: TStringField;
    quAlcoDS2NameProducer: TStringField;
    quAlcoDS2INN: TStringField;
    quAlcoDS2KPP: TStringField;
    quAlcoDS1rQb: TFloatField;
    procedure quAlcoDHCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmAlg: TdmAlg;

implementation

uses MDB;


{$R *.dfm}


procedure TdmAlg.quAlcoDHCalcFields(DataSet: TDataSet);
begin
  quAlcoDHIT1.AsInteger:=quAlcoDHIType.AsInteger div 10;
  quAlcoDHIT2.AsInteger:=quAlcoDHIType.AsInteger mod 10;
end;

end.

