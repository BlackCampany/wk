unit AddMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfBackGround, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxDropDownEdit, cxCalc, cxCheckBox;

type
  TfmAddMessage = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxTextEdit1: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddMessage: TfmAddMessage;

implementation

{$R *.dfm}

procedure TfmAddMessage.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
