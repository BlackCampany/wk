unit AddUPL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, StdCtrls, cxButtons, ExtCtrls, cxGraphics, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, DB,
  pvtables, sqldataset;

type
  TfmAddUPL = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxLookupComboBox2: TcxLookupComboBox;
    Label1: TLabel;
    Label3: TLabel;
    cxComboBox1: TcxComboBox;
    quDeps: TPvQuery;
    quDepsID: TSmallintField;
    quDepsName: TStringField;
    dsquDeps: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddUPL: TfmAddUPL;

implementation

uses DBE;

{$R *.dfm}

end.
