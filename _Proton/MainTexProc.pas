unit MainTexProc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ComCtrls, cxMemo, ActnList,
  XPStyleActnCtrls, ActnMan, ExtCtrls, cxCheckBox, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar;

type
  TfmMainTexProc = class(TForm)
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Timer1: TTimer;
    amCalcSS: TActionManager;
    acStartTexProc: TAction;
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartTexProcExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainTexProc: TfmMainTexProc;

implementation

uses Un1, MT, MDB;

{$R *.dfm}

procedure TfmMainTexProc.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date-1;
  Timer1.Enabled:=True;
  ReadIni;
  CommonSet.PathCryst:=Btr1Path;
end;

procedure TfmMainTexProc.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStartTexProc.Execute;
    Close;
  end;
end;

procedure TfmMainTexProc.acStartTexProcExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk:String;
    iC:INteger;
    dCurD:TDateTime;
begin
  //�������
  with dmMt do
  with dmMC do
  begin
    try
      prWH('������ - ��������� ����.',Memo1);
      PvSQL.Connected:=False;
      PvSQL.Connected:=True;
      if PvSQL.Connected=False then prWH('�� ������� ������������ � ���� ������.',Memo1)
      else
      begin
        prWH('���� �������. �������� ��������� ������.',Memo1);  delay(10);
        prWH('',Memo1);  delay(10);

        DateE:=trunc(Date);
        DateB:=Trunc(Date-365);
        prWH('-----������ ������� � '+ds1(DateB)+' �� '+ds1(DateE),Memo1);  delay(10);

        prWH(fmt+'������ ('+StrWk+') ... �����.',Memo1);
        try
          prWH(fmt+'  ������ ...',Memo1);
          ptCliGoods.Active:=False; ptCliGoods.Active:=True;
          ptCliGoods.First;  iC:=0;
          while not ptCliGoods.Eof do
          begin
            ptCliGoods.delete;
            inc(iC);
            if iC mod 500 = 0 then
            begin
              delay(100);
              StatusBar1.Panels[0].Text:=its(iC);
            end;
          end;

          delay(100);

          prWH(fmt+'  ������� ...',Memo1);

          ptTTNIn.Active:=False; ptTTNIn.Active:=True;
          ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';

          ptInLn.Active:=False; ptINLn.Active:=True;

          for iCurD:=DateB to DateE do
          begin
            prWH(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD),Memo1);
            dCurD:=iCurD;
            ptTTNIn.CancelRange;
            ptTTNIn.SetRange([dCurD],[dCurD]);
            ptTTNIn.First;  iC:=0;
            while not ptTTNIn.Eof do
            begin
              ptInLn.CancelRange;
              ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
              ptInLn.First;
              while not ptInLn.Eof do
              begin
                if ptCliGoods.FindKey([ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,ptInLnCodeTovar.AsInteger]) then
                begin
                  ptCliGoods.Edit;
                  ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptCliGoods.Post;
                end else
                begin
                  ptCliGoods.Append;
                  ptCliGoodsGoodsID.AsInteger:=ptInLnCodeTovar.AsInteger;
                  ptCliGoodsClientObjTypeID.AsInteger:=ptTTNInIndexPost.AsInteger;
                  ptCliGoodsClientID.AsInteger:=ptTTNInCodePost.AsInteger;
                  ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                  ptCliGoods.Post;
                end;
                ptInLn.Next;
              end;

              ptTTNIn.Next; inc(iC);
              if iC mod 10 = 0 then
              begin
                StatusBar1.Panels[0].Text:=its(iC);
                delay(100);
              end;
            end;
          end;
        finally
          ptTTNIn.Active:=False;
          ptInLn.Active:=False;
          ptCliGoods.Active:=False;

          prWH(fmt+'������� ��������.',Memo1);
        end;
        prWH('',Memo1);  delay(10);
        prWH('----- Ok',Memo1);  delay(10);
      end;
    except
      prWH('������ ��������� ������.',Memo1);
    end;
  end;
end;

end.
