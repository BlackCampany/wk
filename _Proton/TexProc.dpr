program TexProc;

uses
  Forms,
  MainTexProc in 'MainTexProc.pas' {fmMainTexProc},
  MT in 'MT.pas' {dmMT: TDataModule},
  Un1 in 'Un1.pas',
  MDB in 'MDB.pas' {dmMC: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainTexProc, fmMainTexProc);
  Application.CreateForm(TdmMT, dmMT);
  Application.CreateForm(TdmMC, dmMC);
  Application.Run;
end.
