unit Remn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfmRemn = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    ViewR: TcxGridDBTableView;
    LevelR: TcxGridLevel;
    GridR: TcxGrid;
    ViewRName: TcxGridDBColumn;
    ViewRRemn: TcxGridDBColumn;
    ViewRRemnVoz: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRemn: TfmRemn;

implementation

uses Un1, MDB;

{$R *.dfm}

procedure TfmRemn.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridR.Align:=AlClient;
end;

procedure TfmRemn.cxButton1Click(Sender: TObject);
begin
  close;
end;

end.
