object fmBufPrice: TfmBufPrice
  Left = 434
  Top = 234
  Width = 942
  Height = 585
  Caption = #1041#1091#1092#1077#1088' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1094#1077#1085
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 525
    Width = 929
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 405
    Width = 929
    Height = 120
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 320
      Top = 24
      Width = 50
      Height = 13
      Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
    end
    object Label2: TLabel
      Left = 344
      Top = 52
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object cxLabel2: TcxLabel
      Left = 4
      Top = 8
      Cursor = crHandPoint
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1094#1077#1085#1091' '#1074' '#1082#1072#1088#1090#1086#1095#1082#1077' ('#1074#1077#1089#1072#1093') '#1080' '#1087#1088#1086#1075#1088#1091#1079#1080#1090#1100' '#1082#1072#1089#1089#1099'  F5'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 4
      Top = 28
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074'    F7'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxCheckBox2: TcxCheckBox
      Left = 132
      Top = 28
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1077#1088#1077#1076' '#1087#1077#1095#1072#1090#1100#1102
      TabOrder = 6
      Width = 161
    end
    object cxLabel4: TcxLabel
      Left = 4
      Top = 68
      Cursor = crHandPoint
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel4Click
    end
    object Gr1: TcxGrid
      Left = 489
      Top = 2
      Width = 438
      Height = 116
      Align = alRight
      TabOrder = 8
      LookAndFeel.Kind = lfOffice11
      object Vi1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMC.dsquPost2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object Vi1Name: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'Name'
          Visible = False
          Width = 56
        end
        object Vi1NameCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'NameCli'
          Width = 101
        end
        object Vi1DateInvoice: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'DateInvoice'
        end
        object Vi1Number: TcxGridDBColumn
          Caption = #8470
          DataBinding.FieldName = 'Number'
          Width = 49
        end
        object Vi1Kol: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Kol'
          Width = 52
        end
        object Vi1CenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'CenaTovar'
          Width = 50
        end
        object Vi1Procent: TcxGridDBColumn
          Caption = '% '#1085#1072#1094
          DataBinding.FieldName = 'Procent'
          Width = 49
        end
        object Vi1NewCenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'NewCenaTovar'
          Width = 47
        end
        object Vi1SumCenaTovarPost: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
          DataBinding.FieldName = 'SumCenaTovarPost'
          Visible = False
        end
        object Vi1SumCenaTovar: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'SumCenaTovar'
          Visible = False
        end
      end
      object ViCenn: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMC.dstaCen
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViCennRecId: TcxGridDBColumn
          Caption = #8470' '#1087#1087
          DataBinding.FieldName = 'RecId'
          Visible = False
        end
        object ViCennIdCard: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'IdCard'
          Width = 53
        end
        object ViCennFullName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'FullName'
          Width = 145
        end
        object ViCennEdIzm: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084
          DataBinding.FieldName = 'EdIzm'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1096#1090'.'
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1082#1075'.'
              Value = 2
            end>
          Width = 46
        end
        object ViCennPrice1: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'Price1'
          Styles.Content = dmMC.cxStyle5
        end
        object ViCennCountry: TcxGridDBColumn
          Caption = #1057#1090#1088#1072#1085#1072
          DataBinding.FieldName = 'Country'
          Width = 84
        end
        object ViCennBarCode: TcxGridDBColumn
          Caption = #1064#1090#1088#1080#1093#1082#1086#1076
          DataBinding.FieldName = 'BarCode'
          Width = 53
        end
        object ViCennQuant: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Quant'
        end
      end
      object le1: TcxGridLevel
        GridView = Vi1
      end
      object Le2: TcxGridLevel
        GridView = ViCenn
        Visible = False
      end
    end
    object cxButton1: TcxButton
      Left = 136
      Top = 80
      Width = 109
      Height = 29
      Caption = #1042#1099#1093#1086#1076'   F10'
      TabOrder = 9
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 376
      Top = 20
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 109
    end
    object cxDateEdit2: TcxDateEdit
      Left = 376
      Top = 48
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 2
      Width = 109
    end
    object cxButton2: TcxButton
      Left = 376
      Top = 80
      Width = 109
      Height = 29
      Action = acPeriod
      TabOrder = 3
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox1: TcxCheckBox
      Left = 316
      Top = 84
      Caption = #1042#1089#1077
      TabOrder = 4
      Width = 49
    end
    object cxLabel5: TcxLabel
      Left = 4
      Top = 48
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100' '#1101#1090#1080#1082#1077#1090#1086#1082'    F2'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlue
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = cxLabel5Click
    end
  end
  object GridBufPr: TcxGrid
    Left = 192
    Top = 4
    Width = 737
    Height = 401
    PopupMenu = PopupMenu1
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewBufPr: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewBufPrCustomDrawCell
      OnSelectionChanged = ViewBufPrSelectionChanged
      DataController.DataSource = dmMC.dsquBufPr
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewBufPrIDCARD: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'IDCARD'
        Width = 49
      end
      object ViewBufPrName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
      end
      object ViewBufPrNUMDOC: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMDOC'
        Width = 54
      end
      object ViewBufPrDATEDOC: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DATEDOC'
      end
      object ViewBufPrTYPEDOC: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'TYPEDOC'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmMC.ImageList1
        Properties.Items = <
          item
            Description = #1055#1088#1080#1093#1086#1076
            Value = 1
          end
          item
            Description = #1055#1077#1088#1077#1086#1094#1077#1085#1082#1072
            Value = 12
          end
          item
            Description = #1056#1091#1095#1085'.'
            Value = '-1'
          end>
      end
      object ViewBufPrIDDOC: TcxGridDBColumn
        Caption = #1042#1085'.'#1082#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IDDOC'
        Visible = False
      end
      object ViewBufPrCena: TcxGridDBColumn
        Caption = #1058#1077#1082#1091#1097#1072#1103' '#1094#1077#1085#1072
        DataBinding.FieldName = 'Cena'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewBufPrNEWP: TcxGridDBColumn
        Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
        DataBinding.FieldName = 'NEWP'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewBufPrOLDP: TcxGridDBColumn
        Caption = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
        DataBinding.FieldName = 'OLDP'
      end
      object ViewBufPrSTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'STATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmMC.ImageList1
        Properties.Items = <
          item
            Description = #1085#1077' '#1086#1090#1088#1072#1073'.'
            Value = 0
          end
          item
            Description = #1054#1082
            ImageIndex = 18
            Value = 1
          end
          item
            Description = #1086#1090#1083#1086#1078'.'
            ImageIndex = 17
            Value = 2
          end>
      end
      object ViewBufPrReserv1: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Reserv1'
        Styles.Content = dmMC.cxStyle1
      end
    end
    object LevelBufPr: TcxGridLevel
      GridView = ViewBufPr
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 0
    Align = alLeft
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 405
    Width = 185
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 276
    Top = 88
  end
  object acBufPrice: TActionManager
    Left = 404
    Top = 124
    StyleName = 'XP Style'
    object acExit: TAction
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acLoadCash: TAction
      Caption = 'acLoadCash'
      ShortCut = 116
      OnExecute = acLoadCashExecute
    end
    object acPeriod: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100
      OnExecute = acPeriodExecute
    end
    object acPrintCenn: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074
      ShortCut = 118
      OnExecute = acPrintCennExecute
    end
    object acSetNo: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1090#1072#1090#1091#1089' "'#1085#1077' '#1086#1090#1088#1072#1073'."'
      OnExecute = acSetNoExecute
    end
    object acSeOtloj: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1090#1072#1090#1091#1089' "'#1054#1090#1083#1086#1078'."'
      OnExecute = acSeOtlojExecute
    end
    object acSetOk: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1090#1072#1090#1091#1089' "'#1054#1082'"'
      OnExecute = acSetOkExecute
    end
    object acMoveT: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072' '
      ShortCut = 32885
      SecondaryShortCuts.Strings = (
        'Ctrl+F6')
      OnExecute = acMoveTExecute
    end
    object acRemnT: TAction
      Caption = #1054#1089#1090#1072#1090#1086#1082
      ShortCut = 117
      OnExecute = acRemnTExecute
    end
    object acPrintCen: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1101#1090#1080#1082#1077#1090#1086#1082
      ShortCut = 113
      OnExecute = acPrintCenExecute
    end
    object acPost: TAction
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080
      ShortCut = 32884
      OnExecute = acPostExecute
    end
    object acAddToScale: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1090#1086#1074#1072#1088#1099' '#1074' '#1074#1077#1089#1099
      OnExecute = acAddToScaleExecute
    end
    object acFindPluNum: TAction
      Caption = #1050#1086#1076' '#1074' '#1074#1077#1089#1072#1093
      ShortCut = 16498
      OnExecute = acFindPluNumExecute
    end
  end
  object taLoad: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 276
    Top = 140
    object taLoadIdCard: TStringField
      FieldName = 'IdCard'
    end
  end
  object frBarCodeObject1: TfrBarCodeObject
    Left = 356
    Top = 232
  end
  object PopupMenu1: TPopupMenu
    Left = 528
    Top = 100
    object N1: TMenuItem
      Action = acSetNo
    end
    object N3: TMenuItem
      Action = acSeOtloj
    end
    object N2: TMenuItem
      Action = acSetOk
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = acMoveT
    end
    object N6: TMenuItem
      Action = acRemnT
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Action = acPrintCen
    end
    object N10: TMenuItem
      Action = acFindPluNum
    end
    object N9: TMenuItem
      Action = acAddToScale
    end
  end
end
