object fmAddUPL: TfmAddUPL
  Left = 201
  Top = 741
  BorderStyle = bsDialog
  Caption = #1059#1087#1086#1083#1085#1086#1084#1086#1095#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
  ClientHeight = 137
  ClientWidth = 559
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 24
    Width = 82
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
  end
  object Label1: TLabel
    Left = 12
    Top = 56
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label3: TLabel
    Left = 12
    Top = 92
    Width = 34
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089
  end
  object Panel1: TPanel
    Left = 441
    Top = 0
    Width = 118
    Height = 137
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 104
    Top = 52
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 321
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 104
    Top = 20
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'Name'
      end>
    Properties.ListOptions.AnsiSort = True
    Properties.ListSource = dsquDeps
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    Style.PopupBorderStyle = epbsDefault
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Width = 185
  end
  object cxComboBox1: TcxComboBox
    Left = 104
    Top = 88
    Properties.Items.Strings = (
      #1053#1077' '#1072#1082#1090#1080#1074#1077#1085
      #1040#1082#1090#1080#1074#1077#1085)
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 3
    Text = #1053#1077' '#1072#1082#1090#1080#1074#1077#1085
    Width = 121
  end
  object quDeps: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'SELECT ID,Name FROM "Depart"'
      'where Status1<>13'
      'Order by Name'
      '')
    Params = <>
    Left = 308
    Top = 84
    object quDepsID: TSmallintField
      FieldName = 'ID'
    end
    object quDepsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquDeps: TDataSource
    DataSet = quDeps
    Left = 356
    Top = 84
  end
end
