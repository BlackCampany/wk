unit DelayV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxCalendar, cxImageComboBox,
  cxDBLookupComboBox, Menus, cxProgressBar, cxContainer, cxTextEdit, cxMemo,
  dxmdaset, StdCtrls;

type
  TfmDelayV = class(TForm)
    VDelayV: TcxGridDBTableView;
    LDelayV: TcxGridLevel;
    GrDelayV: TcxGrid;
    VDelayVFullName: TcxGridDBColumn;
    VDelayVIDATEV: TcxGridDBColumn;
    VDelayVName: TcxGridDBColumn;
    VDelayVICLI: TcxGridDBColumn;
    VDelayVDocDate: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem10: TSpeedItem;
    VDelayVCode: TcxGridDBColumn;
    VDelayVNameCode: TcxGridDBColumn;
    VDelayVQRem: TcxGridDBColumn;
    VDelayVQOut: TcxGridDBColumn;
    VDelayVQ: TcxGridDBColumn;
    VDelayVProcent: TcxGridDBColumn;
    VDelayVDelay: TcxGridDBColumn;
    VDelayVDepart: TcxGridDBColumn;
    VDelayVDepName: TcxGridDBColumn;
    VDelayVUser: TcxGridDBColumn;
    VDelayVReason: TcxGridDBColumn;
    VDelayVMOLVOZ: TcxGridDBColumn;
    VDelayVPhone: TcxGridDBColumn;
    VDelayVEMAILV: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Memo1: TcxMemo;
    PBar1: TcxProgressBar;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    VDelayVGr: TcxGridDBColumn;
    VDelayVSGR: TcxGridDBColumn;
    VDelayVCat: TcxGridDBColumn;
    VDelayVRSUMIN: TcxGridDBColumn;
    teDelayV: TdxMemData;
    teDelayVCode: TIntegerField;
    teDelayVNameCode: TStringField;
    teDelayVDepartName: TStringField;
    teDelayVDepart: TIntegerField;
    teDelayVIDCLI: TIntegerField;
    teDelayVNamecli: TStringField;
    teDelayVFullNamecli: TStringField;
    teDelayVQUANT: TFloatField;
    teDelayVQUANTOUT: TFloatField;
    teDelayVQRem: TFloatField;
    teDelayVDOCDATE: TIntegerField;
    teDelayVDateVoz: TIntegerField;
    teDelayVUSER: TStringField;
    teDelayVIDReason: TIntegerField;
    teDelayVMOLVOZ: TStringField;
    teDelayVPHONEVOZ: TStringField;
    teDelayVEMAILVOZ: TStringField;
    teDelayVProcent: TFloatField;
    teDelayVDelay: TIntegerField;
    teDelayVGR: TStringField;
    teDelayVSGR: TStringField;
    teDelayVCat: TStringField;
    teDelayVRSUMIN: TFloatField;
    dsteDelayV: TDataSource;
    CheckBox1: TCheckBox;
    VDelayVhead: TcxGridDBColumn;
    teDelayVhead: TIntegerField;
    VDelayVPriceIn: TcxGridDBColumn;
    teDelayVPriceIn: TFloatField;
    VDelayVTypeV: TcxGridDBColumn;
    teDelayVTypeVoz: TStringField;
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure VDelayVCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  procedure prcat;
  procedure prprosroch;
  procedure prallvoz;
var
  fmDelayV: TfmDelayV;

implementation

uses MDB, u2fdk, Un1, Period1, dmPS, MT;

{$R *.dfm}
procedure prcat;
  var igr,iC, iStep:integer;
begin
with fmDelayV do
with dmMT do
  begin
  PBar1.Position:=0;
  if teDelayV.RecordCount<1 then exit;
  VDelayV.BeginUpdate;
  teDelayV.First;
  PBar1.Position:=20;

  iStep:=teDelayV.RecordCount div 80;
  if iStep=0 then iStep:=teDelayV.RecordCount;
  iC:=0;

  while not teDelayV.Eof do
  begin
     teDelayV.Edit;
    iGr:=0;
    if teClass.Locate('Id',teDelayVCODE.AsInteger,[]) then
    begin
      iGr:=teClassIdParent.AsInteger;
      teDelayVCat.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
    end;

    if (iGr>0) then
    begin
      if teClass.Locate('Id',iGr,[]) then
      begin
        teDelayVSGR.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
        iGr:=teClassIdParent.AsInteger;
      end;
    end;
    if iGr>0 then
    begin
      if teClass.Locate('Id',iGr,[]) then
      begin
        teDelayVGR.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
      end;
    end;
    delay(10);
    teDelayV.Post;

    inc(iC);

    if (iC mod iStep = 0) then
    begin
      PBar1.Position:=20+(iC div iStep);
      delay(10);
    end;


    teDelayV.Next;
  end;
  teDelayV.First;
  VDelayV.EndUpdate;
  end;
end;

procedure prallvoz;
 var iC, iStep:integer;
begin
  with dmP do
  with fmDelayV do
  begin
   if teDelayV.Active=true then
   begin
     closete(teDelayV);
     VDelayV.BeginUpdate;
     VDelayV.EndUpdate;
   end;

   VDelayV.BeginUpdate;
   fmDelayV.Caption:='��� ��������';
   PBar1.Position:=0;
   PBar1.Visible:=True;

   Memo1.Clear;
   Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ������������ ������.'); delay(10);

   PBar1.Position:=10; delay(10);
   dmP.dsquDelayVoz.DataSet:=nil;
   dmP.quallVoz.Active:=False;
   dmP.quallVoz.Active:=True;
   dmP.dsquDelayVoz.DataSet:=dmP.quAllVoz;

   CloseTe(teDelayV);

   PBar1.Position:=20; delay(10);

   iStep:=quAllVoz.RecordCount div 80;
   if iStep=0 then iStep:=quAllVoz.RecordCount;
   iC:=0;

   if quAllVoz.RecordCount>0 then
   begin
     quAllVoz.First;

     while not quAllVoz.Eof do
     begin
       teDelayV.Append;
       teDelayVhead.AsInteger:=quAllVozIDHEAD.AsInteger;
       teDelayVCode.AsInteger:=quAllVozCODE.AsInteger;
       teDelayVNameCode.AsString:=quAllVozNameCode.AsString;
       teDelayVDepart.AsInteger:=quAllVozDEPART.AsInteger;
       teDelayVDepartName.AsString:=quAllVozDepartName.AsString;
       teDelayVIDCLI.AsInteger:= quAllVozIDCLI.AsInteger;
       teDelayVNamecli.AsString:= quAllVozNamecli.AsString;
       teDelayVFullNamecli.AsString:=quAllVozFullNamecli.AsString;
       teDelayVQUANT.AsFloat:=quAllVozQUANT.AsFloat;
       teDelayVQUANTOUT.AsFloat:= quAllVozQUANTOUT.AsFloat;
       teDelayVQRem.AsFloat:= quAllVozQRem.AsFloat;
      // teDelayVStatus.AsString:= quDelayVozStatus.AsString ;
       teDelayVDOCDATE.AsInteger:= quAllVozDOCDATE.AsInteger;
       teDelayVDateVoz.AsInteger:= quAllVozDateVoz.AsInteger;
       teDelayVUSER.AsString:= quAllVozUSER.AsString;
       teDelayVIDReason.AsInteger:=quAllVozIDReason.AsInteger;
       teDelayVMOLVOZ.AsString:= quAllVozMOLVOZ.AsString;
       teDelayVPHONEVOZ.AsString:= quAllVozPHONEVOZ.AsString;
       teDelayVEMAILVOZ.AsString:= quAllVozEMAILVOZ.AsString;
       teDelayVProcent.AsFloat:= quAllVozProcent.AsFloat;
       teDelayVDelay.AsInteger:= quAllVozDelay.AsInteger;
       teDelayVPriceIn.AsFloat:= quAllVozPriceIn.AsFloat;
       teDelayVRSUMIN.AsFloat:= quAllVozRSUMIN.AsFloat;
       teDelayVTypeVoz.AsString:=quAllVozTypeVoz.AsString;
       teDelayVGR.AsString:='';
       teDelayVSGR.AsString:='';
       teDelayVCat.AsString:='';

       quAllVoz.Next;

       inc(iC);
       if (iC mod iStep = 0) then
       begin
         PBar1.Position:=20+(iC div iStep);
         delay(10);
       end;
     end;

     teDelayV.Post;
     teDelayV.First;
   end;
   dmP.quallVoz.Active:=False;
   VDelayVNameCode.Focused:=true;

    Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������.'); ;
    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;

    VDelayV.EndUpdate;
  end;
end;


procedure prprosroch;
 var iStep,ic:integer;
begin
  with dmP do
  with fmDelayV do
  begin
   if teDelayV.Active=true then
   begin
     closete(teDelayV);
     VDelayV.BeginUpdate;
     VDelayV.EndUpdate;
   end;

   VDelayV.BeginUpdate;

   fmDelayV.Caption:='������������ ��������';

   PBar1.Position:=0;
   fmDelayV.Show;
   PBar1.Visible:=True;

   Memo1.Clear;
   Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'����� .. ������������ ������.'); delay(10);

   PBar1.Position:=10; delay(10);
   dsquDelayVoz.DataSet:=nil;
   quDelayVoz.Active:=False;
   quDelayVoz.ParamByName('IDATE').AsInteger:=Trunc(Date);
   quDelayVoz.Active:=True;
   dsquDelayVoz.DataSet:=dmP.quDelayVoz;

   CloseTe(teDelayV);

   PBar1.Position:=20; delay(10);

   iStep:=quDelayVoz.RecordCount div 80;
   if iStep=0 then iStep:=quDelayVoz.RecordCount;
   iC:=0;

   if quDelayVoz.RecordCount>0 then
   begin
     quDelayVoz.First;

     while not quDelayVoz.Eof do
     begin
       teDelayV.Append;
       teDelayVhead.AsInteger:=quDelayVozIDHEAD.AsInteger;
       teDelayVCode.AsInteger:=quDelayVozCODE.AsInteger;
       teDelayVNameCode.AsString:=quDelayVozNameCode.AsString;
       teDelayVDepart.AsInteger:=quDelayVozDEPART.AsInteger;
       teDelayVDepartName.AsString:=quDelayVozDepartName.AsString;
       teDelayVIDCLI.AsInteger:= quDelayVozIDCLI.AsInteger;
       teDelayVNamecli.AsString:= quDelayVozNamecli.AsString;
       teDelayVFullNamecli.AsString:=quDelayVozFullNamecli.AsString;
       teDelayVQUANT.AsFloat:=quDelayVozQUANT.AsFloat;
       teDelayVQUANTOUT.AsFloat:= quDelayVozQUANTOUT.AsFloat;
       teDelayVQRem.AsFloat:= quDelayVozQRem.AsFloat;
      // teDelayVStatus.AsString:= quDelayVozStatus.AsString ;
       teDelayVDOCDATE.AsInteger:= quDelayVozDOCDATE.AsInteger;
       teDelayVDateVoz.AsInteger:= quDelayVozDateVoz.AsInteger;
       teDelayVUSER.AsString:= quDelayVozUSER.AsString;
       teDelayVIDReason.AsInteger:=quDelayVozIDReason.AsInteger;
       teDelayVMOLVOZ.AsString:= quDelayVozMOLVOZ.AsString;
       teDelayVPHONEVOZ.AsString:= quDelayVozPHONEVOZ.AsString;
       teDelayVEMAILVOZ.AsString:= quDelayVozEMAILVOZ.AsString;
       teDelayVProcent.AsFloat:= quDelayVozProcent.AsFloat;
       teDelayVDelay.AsInteger:= quDelayVozDelay.AsInteger;
       teDelayVPriceIn.AsFloat:= quDelayVozPriceIn.AsFloat;
       teDelayVRSUMIN.AsFloat:= quDelayVozRSUMIN.AsFloat;
       teDelayVTypeVoz.AsString:=quDelayVozTypeVoz.AsString;
       teDelayVGR.AsString:='';
       teDelayVSGR.AsString:='';
       teDelayVCat.AsString:='';

       quDelayVoz.Next;
     //  delay(10);

       inc(iC);
       if (iC mod iStep = 0) then
       begin
         PBar1.Position:=20+(iC div iStep);
         delay(10);
       end;
     end;

     teDelayV.Post;
     teDelayV.First;
   end;
   quDelayVoz.Active:=False;

   VDelayVNameCode.Focused:=true;

   Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+'������������.'); delay(10);
   PBar1.Position:=100; delay(30);
   PBar1.Visible:=False;
   VDelayV.EndUpdate;
  end;
end;


procedure TfmDelayV.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDelayV.SpeedItem10Click(Sender: TObject);
begin
   //������� � ������
  prNExportExel5(VDelayV);
end;

procedure TfmDelayV.FormCreate(Sender: TObject);
begin
  GrDelayV.Align:=AlClient;
  VDelayV.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmDelayV.FormShow(Sender: TObject);

begin
  if dmP.quReasV.Active=false then dmP.quReasV.Active:=true;

end;

procedure TfmDelayV.N1Click(Sender: TObject);
begin
prprosroch;
end;

procedure TfmDelayV.VDelayVCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin
if AViewInfo.GridRecord.Selected then
 begin
    ACanvas.Canvas.Brush.Color := $00FFB1A4;
    if AViewInfo.Focused=true then
    begin
      ACanvas.Canvas.Brush.Color := clwhite;
      ACanvas.Canvas.Font.Color := clblack;
    end;
  end;
end;

procedure TfmDelayV.SpeedItem3Click(Sender: TObject);
begin
prprosroch;
end;

procedure TfmDelayV.SpeedItem2Click(Sender: TObject);
begin
prallvoz;
end;

procedure TfmDelayV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   dmMT.teClass.active:=false;
end;

procedure TfmDelayV.CheckBox1Click(Sender: TObject);
begin
//prcat;

end;

end.
