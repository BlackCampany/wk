unit PreRealAlco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxRadioGroup,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxButtons, ExtCtrls, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, DB, pvtables, sqldataset;

type
  TfmPreRealAlco = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    quDeps: TPvQuery;
    quDepsID: TSmallintField;
    quDepsName: TStringField;
    dsquDeps: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPreRealAlco: TfmPreRealAlco;

implementation

uses MDB;

{$R *.dfm}

procedure TfmPreRealAlco.FormCreate(Sender: TObject);
begin
  cxDateEdit1.Date:=Date;
  cxDateEdit2.Date:=Date;
end;

end.
