unit ObMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan, StdCtrls;

type
  TfmObVed = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridObVed: TcxGrid;
    LevelObVed: TcxGridLevel;
    dsObVed: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepOb: TfrReport;
    frtaObVed: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    teObVed: TdxMemData;
    teObVedIdCode1: TIntegerField;
    teObVedNameC: TStringField;
    teObVediM: TSmallintField;
    teObVedIdGroup: TIntegerField;
    teObVedQBeg: TFloatField;
    teObVedQIn: TFloatField;
    teObVedQOut: TFloatField;
    teObVedQRes: TFloatField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teObVedNameGr1: TStringField;
    teObVedNameGr2: TStringField;
    teObVedIdSGroup: TIntegerField;
    teObVedQReal: TFloatField;
    teObVedQVnIn: TCurrencyField;
    teObVedQVnOut: TFloatField;
    teObVedQInv: TFloatField;
    ViewObVed: TcxGridDBTableView;
    teObVediBrand: TIntegerField;
    teObVediCat: TSmallintField;
    amRepOb: TActionManager;
    acMoveRepOb: TAction;
    teObVedNameGr3: TStringField;
    ViewObVedRecId: TcxGridDBColumn;
    ViewObVedIdCode1: TcxGridDBColumn;
    ViewObVedNameC: TcxGridDBColumn;
    ViewObVedQBeg: TcxGridDBColumn;
    ViewObVedQIn: TcxGridDBColumn;
    ViewObVedQVnIn: TcxGridDBColumn;
    ViewObVedQVnOut: TcxGridDBColumn;
    ViewObVedQReal: TcxGridDBColumn;
    ViewObVedQOut: TcxGridDBColumn;
    ViewObVedQRes: TcxGridDBColumn;
    ViewObVedQInv: TcxGridDBColumn;
    ViewObVedNameGr1: TcxGridDBColumn;
    ViewObVedNameGr2: TcxGridDBColumn;
    ViewObVedNameGr3: TcxGridDBColumn;
    ViewObVediBrand: TcxGridDBColumn;
    teObVedsTop: TStringField;
    teObVedsNov: TStringField;
    ViewObVedsTop: TcxGridDBColumn;
    ViewObVedsNov: TcxGridDBColumn;
    teObVedDayN: TIntegerField;
    ViewObVedDayN: TcxGridDBColumn;
    teObVedNAMEDEP: TStringField;
    teObVedBStatus: TIntegerField;
    teObVedBStatusD: TDateField;
    teObVedBTop: TIntegerField;
    teObVedBTopD: TDateField;
    teObVedsCat: TStringField;
    teObVedsBStatus: TStringField;
    teObVedsBTop: TStringField;
    ViewObVedsCat: TcxGridDBColumn;
    ViewObVedsBTop: TcxGridDBColumn;
    ViewObVedBTopD: TcxGridDBColumn;
    ViewObVedsBStatus: TcxGridDBColumn;
    ViewObVedBStatusD: TcxGridDBColumn;
    Panel2: TPanel;
    Panel9: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label15: TLabel;
    Label14: TLabel;
    Label8: TLabel;
    ViewPosts: TcxGridDBTableView;
    LevelPosts: TcxGridLevel;
    GridPosts: TcxGrid;
    ViewPostsNameCli: TcxGridDBColumn;
    teObVedQRemVoz: TFloatField;
    ViewObVedQRemVoz: TcxGridDBColumn;
    teObVedQNVoz: TFloatField;
    ViewObVedQNVoz: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewObVedCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViewObVedDblClick(Sender: TObject);
    procedure teObVedCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmObVed: TfmObVed;

implementation

uses Un1, MDB, MT, Move, dmPS;

{$R *.dfm}

procedure TfmObVed.FormCreate(Sender: TObject);
begin
  GridObVed.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewObVed.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmObVed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewObVed.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  teObVed.Active:=False;
end;

procedure TfmObVed.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  if fmObVed.tag=0 then GridPosts.Visible:=False else GridPosts.Visible:=True;
end;

procedure TfmObVed.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmObVed.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewObVed);
end;

procedure TfmObVed.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewObVed.BeginUpdate;
  teObVed.Filter:=ViewObVed.DataController.Filter.FilterText;
  teObVed.Filtered:=True;

  frRepOb.LoadFromFile(CurDir + 'ObVed.frf');

  frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

//  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewObVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepOb.ReportName:='��������� ���������.';
  frRepOb.PrepareReport;
  frRepOb.ShowPreparedReport;

  teObVed.Filter:='';
  teObVed.Filtered:=False;
  ViewObVed.EndUpdate;
end;

procedure TfmObVed.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepObVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmObVed.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmObVed.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmObVed.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewObVed.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teObVedIdCode1.AsInteger;
      prFillMove(teObVedIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teObVedNameC.AsString+'('+teObVedIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmObVed.ViewObVedCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    StrCat,StrName,StrBStat:String;
begin
  StrCat:='';
  StrName:='';
  StrBStat:='';
  for i:=0 to ViewObVed.ColumnCount-1 do
  begin
    if ViewObVed.Columns[i].Name='ViewObVedNameC' then StrName:=AViewInfo.GridRecord.DisplayTexts[i];
    if ViewObVed.Columns[i].Name='ViewObVedsCat' then StrCat:=AViewInfo.GridRecord.DisplayTexts[i];
    if ViewObVed.Columns[i].Name='ViewObVedsBStatus' then StrBStat:=AViewInfo.GridRecord.DisplayTexts[i];
  end;

  if pos('*',StrName)=1 then
  begin
    ACanvas.Canvas.Brush.Color := $00B3B3FF; //�����������
  end else
  begin
    if StrBStat>'' then
    begin
      ACanvas.Canvas.Brush.Color := $00AAFFFF;  //������
      ACanvas.Font.Color:=clBlack;
    end else
    begin
      if StrCat>'' then
      begin
        if (StrCat<>'00')and(StrCat<>'A')and(StrCat<>'B')and(StrCat<>'C')and(StrCat<>'D') then
          ACanvas.Canvas.Brush.Color := $00BFFFBF; // �������
      end;
    end;
  end;
end;

{
Var iNac,i:Integer;
    StrWk:String;
begin
  iNac:=0; StrWk:='';
  for i:=0 to ViewDoc1.ColumnCount-1 do
  begin
    if ViewDoc1.Columns[i].Name='ViewDoc1iNac' then
      iNac:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
    if ViewDoc1.Columns[i].Name='ViewDoc1iCat' then
      StrWk:=AViewInfo.GridRecord.DisplayTexts[i];
  end;

  //if (StrWk<>'0') then showMessage(StrWk);

  if (StrWk='M') or (StrWk='S') or (StrWk='O') then
  begin
    if (StrWk='M') or (StrWk='O') then ACanvas.Canvas.Brush.Color := $00FFD1A4; //�������
    if (StrWk='S') then
      begin
        ACanvas.Canvas.Brush.Color := $00AAFFFF;  //������
        ACanvas.Font.Color:=clBlack;
      end
  end;

  if iNac=1 then ACanvas.Canvas.Brush.Color := $00B3B3FF; //��� ��������� �������
  if iNac=2 then ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� ������� �������
end;

}

procedure TfmObVed.ViewObVedDblClick(Sender: TObject);
begin
  if fmObVed.tag=0 then exit;
  with dmMC do
  begin
    if teObVed.RecordCount>0 then
    begin
      try
        ViewPosts.BeginUpdate;
        quPost6.Active:=False;
        quPost6.ParamByName('IDC').AsInteger:=teObVedIdCode1.AsInteger;
        quPost6.Active:=True;
      finally
        ViewPosts.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmObVed.teObVedCalcFields(DataSet: TDataSet);
begin
  with dmP do
  begin
    quAllRem.Active:=false;
    quAllRem.ParamByName('CODE').AsInteger:= teObVedIdCode1.AsInteger;
    quAllRem.Active:=true;
    teObVedQRemVoz.AsFloat:=quAllRemQRem.AsFloat;
    quAllRem.Active:=false;
    teObVedQNVoz.AsFloat:=teObVedQRes.AsFloat-teObVedQRemVoz.AsFloat;
  end;
end;

end.
