object fmChangePost: TfmChangePost
  Left = 443
  Top = 280
  BorderStyle = bsDialog
  Caption = #1047#1072#1084#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  ClientHeight = 293
  ClientWidth = 424
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 20
    Top = 52
    Width = 118
    Height = 13
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' '
  end
  object Label3: TLabel
    Left = 152
    Top = 52
    Width = 39
    Height = 13
    Caption = 'Label3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 24
    Top = 84
    Width = 12
    Height = 13
    Caption = #1085#1072
  end
  object Panel1: TPanel
    Left = 0
    Top = 236
    Width = 424
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765348
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 68
      Top = 12
      Width = 85
      Height = 33
      Caption = #1047#1072#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 276
      Top = 12
      Width = 85
      Height = 33
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Memo1: TcxMemo
    Left = 16
    Top = 112
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 113
    Width = 389
  end
  object cxButtonEdit1: TcxButtonEdit
    Tag = 1
    Left = 72
    Top = 80
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.BorderStyle = ebsOffice11
    TabOrder = 2
    Text = 'cxButtonEdit1'
    Width = 297
  end
end
