unit RepVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, ActnList, XPStyleActnCtrls, ActnMan,
  Placemnt, dxPSCore, dxPScxCommon, dxPScxGridLnk, dxmdaset, FR_DSet,
  FR_DBSet, FR_Class, cxProgressBar, cxContainer, cxTextEdit, cxMemo,
  ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar;

type
  TfmRepVoz = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GridRVoz: TcxGrid;
    ViewRVoz: TcxGridDBTableView;
    LevelRVed: TcxGridLevel;
    Panel1: TPanel;
    Memo1: TcxMemo;
    PBar1: TcxProgressBar;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    le1: TcxGridLevel;
    frRepVoz: TfrReport;
    frtaRVoz: TfrDBDataSet;
    teRVoz: TdxMemData;
    teRVozIdGoods: TIntegerField;
    teRVozNameG: TStringField;
    teRVozEdIzm: TSmallintField;
    teRVozQ: TFloatField;
    teRVozIdGroup: TIntegerField;
    teRVozIdSGroup: TIntegerField;
    teRVozPriceCliNDS: TFloatField;
    teRVozSumCliNDS: TFloatField;
    dsRVoz: TDataSource;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    FormPlacement1: TFormPlacement;
    amRepOb: TActionManager;
    acMoveRepOb: TAction;
    teRVozIdSklad: TIntegerField;
    teRVozNameS: TStringField;
    teRVozPriceCli: TIntegerField;
    teRVozPriceShop: TIntegerField;
    teRVozSumCli: TIntegerField;
    teRVozProcNDS: TIntegerField;
    teRVozSumNDS: TStringField;
    teRVozIdPost: TIntegerField;
    teRVozNameP: TStringField;
    ViewRVozIdSklad: TcxGridDBColumn;
    ViewRVozNameS: TcxGridDBColumn;
    ViewRVozIdGoods: TcxGridDBColumn;
    ViewRVozNameG: TcxGridDBColumn;
    ViewRVozIdGroup: TcxGridDBColumn;
    ViewRVozIdSGroup: TcxGridDBColumn;
    ViewRVozEdIzm: TcxGridDBColumn;
    ViewRVozQ: TcxGridDBColumn;
    ViewRVozPriceCliNDS: TcxGridDBColumn;
    ViewRVozPriceCli: TcxGridDBColumn;
    ViewRVozPriceShop: TcxGridDBColumn;
    ViewRVozSumCliNDS: TcxGridDBColumn;
    ViewRVozSumCli: TcxGridDBColumn;
    ViewRVozProcNDS: TcxGridDBColumn;
    ViewRVozSumNDS: TcxGridDBColumn;
    ViewRVozIdPost: TcxGridDBColumn;
    ViewRVozNameP: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepVoz: TfmRepVoz;

implementation

{$R *.dfm}

end.
