unit uGraf2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ExtCtrls, SpeedBar, ComCtrls, TeeProcs, TeEngine,
  Chart, Series, ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmGraf2 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Panel1: TPanel;
    Chart1: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    amGraf1: TActionManager;
    acPrintGraf: TAction;
    SpeedItem2: TSpeedItem;
    acGr1: TAction;
    acGr2: TAction;
    acGr3: TAction;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    FormPlacement1: TFormPlacement;
    Series3: TLineSeries;
    Series4: TLineSeries;
    Series5: TLineSeries;
    Series6: TLineSeries;
    Series7: TLineSeries;
    Series8: TLineSeries;
    Series9: TLineSeries;
    Series10: TLineSeries;
    Series11: TLineSeries;
    Series12: TLineSeries;
    Series13: TLineSeries;
    Series14: TLineSeries;
    Series15: TLineSeries;
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPrintGrafExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGraf2: TfmGraf2;

implementation

uses Un1;

{$R *.dfm}

procedure TfmGraf2.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGraf2.acPrintGrafExecute(Sender: TObject);
//Var rRect:TRect;
begin
//������
 { rRect.Left:=5;
  rRect.Right:=
  0,Printer.PageWidth-1,Printer.PageHeight-1}

  Chart1.PrintLandscape;
end;

procedure TfmGraf2.FormCreate(Sender: TObject);
begin
  Chart1.Visible:=True;
  Chart1.Align:=alClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

end.
