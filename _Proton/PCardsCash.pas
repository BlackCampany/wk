unit PCardsCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxCurrencyEdit, cxControls, cxContainer, cxEdit, cxTextEdit;

type
  TfmPCardsRn = class(TForm)
    Panel2: TPanel;
    cxButton10: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton14: TcxButton;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxButton2: TcxButton;
    Label5: TLabel;
    procedure FormShow(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxCurrencyEdit4Enter(Sender: TObject);
    procedure cxTextEdit1Enter(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCardsRn: TfmPCardsRn;

implementation

uses Un1, prdb, Dm;

{$R *.dfm}

procedure TfmPCardsRn.FormShow(Sender: TObject);
begin
  cxCurrencyEdit4.Value:=0;
  cxTextEdit1.Clear;
  cxTextEdit1.SetFocus;
  cxTextEdit1.Tag:=1;
  cxCurrencyEdit4.Tag:=0;

  cxCurrencyEdit1.Value:=0;
  cxCurrencyEdit2.Value:=0;
  cxCurrencyEdit3.Value:=0;
  Label5.Caption:='';
end;

procedure TfmPCardsRn.cxButton10Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'0';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+0;
  end;
end;

procedure TfmPCardsRn.cxButton3Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'1';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+1;
  end;
end;

procedure TfmPCardsRn.cxButton4Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'2';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+2;
  end;
end;

procedure TfmPCardsRn.cxButton5Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'3';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+3;
  end;

end;

procedure TfmPCardsRn.cxButton6Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'4';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+4;
  end;
end;

procedure TfmPCardsRn.cxButton7Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'5';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+5;
  end;
end;

procedure TfmPCardsRn.cxButton8Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'6';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+6;
  end;
end;

procedure TfmPCardsRn.cxButton9Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'7';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+7;
  end;
end;

procedure TfmPCardsRn.cxButton11Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'8';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+8;
  end;
end;

procedure TfmPCardsRn.cxButton12Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'9';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+9;
  end;
end;

procedure TfmPCardsRn.cxCurrencyEdit4Enter(Sender: TObject);
begin
  cxTextEdit1.Tag:=0;
  cxCurrencyEdit4.Tag:=1;
  cxCurrencyEdit4.SelectAll;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.SetFocus;
end;

procedure TfmPCardsRn.cxTextEdit1Enter(Sender: TObject);
begin
  cxTextEdit1.Tag:=1;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.Tag:=0;
  cxTextEdit1.SetFocus;
end;

procedure TfmPCardsRn.cxButton14Click(Sender: TObject);
Var DiscountBar:String;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
begin
  cxTextEdit1.Tag:=0;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.Tag:=1;
  cxCurrencyEdit4.SelectAll;
  cxCurrencyEdit4.SetFocus;

  if cxTextEdit1.Text>'' then
  begin
    DiscountBar:=SOnlyDigit(cxTextEdit1.Text);
    prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
    cxCurrencyEdit1.Value:=rBalans;
    cxCurrencyEdit2.Value:=rBalansDay;
    cxCurrencyEdit3.Value:=DLimit;
    Label5.Caption:=CliName+' ('+CliType+')';
//    Label5.Tag:=CliType;
//    Showmessage('     �������� - '+CliName+#$0D+'     ��� - '+CliType+#$0D+'     ������ - '+fts(rv(rBalans))+#$0D+'     ������ ������� - '+fts(rv(rBalansDay))+#$0D+'     ������� ����� - '+fts(rv(DLimit)));
  end;
end;

procedure TfmPCardsRn.cxButton2Click(Sender: TObject);
Var DiscountBar:String;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
begin
  if not CanDo('prEditBalancePC') then begin  Showmessage('��� ����'); exit; end;
  with dmC do
  with dmPC do
  begin
    DiscountBar:=SOnlyDigit(cxTextEdit1.Text);
    if (DiscountBar>'')and(cxCurrencyEdit4.Value>0) then
    begin
      prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
      if CliName>'' then
      begin
        if MessageDlg('������ ����� '+fts(cxCurrencyEdit4.Value)+' �� ����� '+CliName,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//EXECUTE PROCEDURE PRSAVEPC1 (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME)
          prInpMoney.ParamByName('PCBAR').AsString:=DiscountBar;
          prInpMoney.ParamByName('PCSUM').AsFloat:=cxCurrencyEdit4.Value;
          prInpMoney.ParamByName('STATION').AsINteger:=CommonSet.Station;
          prInpMoney.ParamByName('IDPERSON').AsINteger:=Person.Id;
          prInpMoney.ParamByName('PERSNAME').AsString:=Person.Name;
          prInpMoney.ParamByName('DATEOP').AsDateTime:=now;

          prInpMoney.ExecProc;
          delay(100);

          prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
          cxCurrencyEdit1.Value:=rBalans;
          cxCurrencyEdit2.Value:=rBalansDay;
          cxCurrencyEdit3.Value:=DLimit;
          Label5.Caption:=CliName+' ('+CliType+')';
        end;
      end;
    end;
  end;
end;

end.
