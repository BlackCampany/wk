unit SpecInvVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxTextEdit, cxMemo,
  cxContainer, cxProgressBar, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ComCtrls, dxmdaset, SpeedBar, ExtCtrls;

type
  TfmSpecInvVoz = class(TForm)
    GrSpecInvVoz: TcxGrid;
    ViewSpecInvVoz: TcxGridDBTableView;
    ViewSpecInvVozCode: TcxGridDBColumn;
    ViewSpecInvVozNameC: TcxGridDBColumn;
    ViewSpecInvVozQ: TcxGridDBColumn;
    ViewSpecInvVozQFact: TcxGridDBColumn;
    ViewSpecInvVozQRazn: TcxGridDBColumn;
    ViewSpecInvVozQOTHERCLI: TcxGridDBColumn;
    ViewSpecInvVozNEWCLI: TcxGridDBColumn;
    LevSpecInvVoz: TcxGridLevel;
    ViewSpecInvVozDepart: TcxGridDBColumn;
    ViewSpecInvVozDepartName: TcxGridDBColumn;
    ViewSpecInvVozNamecli: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    teSp: TdxMemData;
    teSpCode: TIntegerField;
    teSpNameC: TStringField;
    dsteSp: TDataSource;
    teSpDepart: TIntegerField;
    teSpDepartName: TStringField;
    teSpQ: TFloatField;
    teSpQFact: TFloatField;
    teSpQRazn: TFloatField;
    teSpQOTHERCLI: TFloatField;
    teSpNEWCLI: TIntegerField;
    teSpNamecli: TStringField;
    ViewSpecInvVozN: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSpecInvVoz: TfmSpecInvVoz;

implementation

uses dmPS,Un1;

{$R *.dfm}

procedure TfmSpecInvVoz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmP.quSpInvV.Active:=false;
end;

procedure TfmSpecInvVoz.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewSpecInvVoz);
end;

procedure TfmSpecInvVoz.SpeedItem1Click(Sender: TObject);
begin
 close;
end;

end.
