object fmAddCliLic: TfmAddCliLic
  Left = 362
  Top = 307
  Width = 581
  Height = 154
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1083#1080#1094#1077#1085#1079#1080#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 16
    Width = 31
    Height = 13
    Caption = #1057#1077#1088#1080#1103
  end
  object Label1: TLabel
    Left = 212
    Top = 16
    Width = 34
    Height = 13
    Caption = #1053#1086#1084#1077#1088
  end
  object Label3: TLabel
    Left = 12
    Top = 80
    Width = 62
    Height = 13
    Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085#1072
  end
  object Label4: TLabel
    Left = 12
    Top = 48
    Width = 64
    Height = 13
    Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
  end
  object Label5: TLabel
    Left = 212
    Top = 48
    Width = 82
    Height = 13
    Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
  end
  object Panel1: TPanel
    Left = 455
    Top = 0
    Width = 118
    Height = 120
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 56
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 125
  end
  object cxTextEdit2: TcxTextEdit
    Left = 260
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 161
  end
  object cxTextEdit3: TcxTextEdit
    Left = 84
    Top = 76
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 337
  end
  object cxDateEdit1: TcxDateEdit
    Left = 84
    Top = 44
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 4
    Width = 109
  end
  object cxDateEdit2: TcxDateEdit
    Left = 308
    Top = 44
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Width = 113
  end
end
