object fmCliPars: TfmCliPars
  Left = 416
  Top = 204
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
  ClientHeight = 392
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 343
    Width = 479
    Height = 49
    Align = alBottom
    BevelInner = bvLowered
    Color = 16752029
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 96
      Top = 12
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 272
      Top = 12
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLabel1: TcxLabel
    Left = 24
    Top = 24
    Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
    Transparent = True
  end
  object cxLabel3: TcxLabel
    Left = 24
    Top = 52
    Caption = #1048#1053#1053
    Transparent = True
  end
  object cxLabel6: TcxLabel
    Left = 24
    Top = 80
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
    Transparent = True
  end
  object cxLabel7: TcxLabel
    Left = 24
    Top = 108
    Caption = #1040#1076#1088#1077#1089' '#1075#1088#1091#1079#1086#1087#1086#1083#1091#1095'.'
    Transparent = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 124
    Top = 20
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 5
    Text = 'cxTextEdit1'
    Width = 333
  end
  object cxTextEdit3: TcxTextEdit
    Left = 64
    Top = 48
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 6
    Text = 'cxTextEdit3'
    Width = 125
  end
  object cxTextEdit6: TcxTextEdit
    Left = 128
    Top = 76
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 7
    Text = 'cxTextEdit6'
    Width = 337
  end
  object cxTextEdit7: TcxTextEdit
    Left = 128
    Top = 108
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 8
    Text = 'cxTextEdit7'
    Width = 337
  end
  object cxLabel10: TcxLabel
    Left = 324
    Top = 188
    Caption = #1041#1080#1082
    Transparent = True
  end
  object cxLabel11: TcxLabel
    Left = 28
    Top = 188
    Caption = #1056#1072#1089#1095'.'#1089#1095#1077#1090
    Transparent = True
  end
  object cxTextEdit11: TcxTextEdit
    Left = 108
    Top = 184
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 11
    Text = 'cxTextEdit11'
    Width = 153
  end
  object cxLabel12: TcxLabel
    Left = 28
    Top = 220
    Caption = #1041#1072#1085#1082
    Transparent = True
  end
  object cxTextEdit12: TcxTextEdit
    Left = 108
    Top = 216
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 13
    Text = 'cxTextEdit12'
    Width = 345
  end
  object cxLabel13: TcxLabel
    Left = 28
    Top = 256
    Caption = #1050#1086#1088#1088'. '#1089#1095#1077#1090
    Transparent = True
  end
  object cxTextEdit13: TcxTextEdit
    Left = 108
    Top = 252
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 15
    Text = 'cxTextEdit13'
    Width = 149
  end
  object cxTextEdit14: TcxTextEdit
    Left = 356
    Top = 184
    Properties.MaxLength = 9
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 16
    Text = 'cxTextEdit14'
    Width = 89
  end
  object cxLabel4: TcxLabel
    Left = 24
    Top = 140
    Caption = #1050#1086#1084#1091' '#1074#1099#1089#1090'. '#1087#1088#1077#1090#1077#1085#1079#1080#1102
    Transparent = True
  end
  object cxTextEdit4: TcxTextEdit
    Left = 152
    Top = 140
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 18
    Text = 'cxTextEdit4'
    Width = 313
  end
  object cxLabel2: TcxLabel
    Left = 28
    Top = 304
    Caption = #1044#1086#1075#1086#1074#1086#1088' '#8470
    Transparent = True
  end
  object cxTextEdit2: TcxTextEdit
    Left = 108
    Top = 300
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 20
    Text = 'cxTextEdit11'
    Width = 161
  end
  object cxLabel5: TcxLabel
    Left = 292
    Top = 304
    Caption = #1076#1072#1090#1072
    Transparent = True
  end
  object cxDateEdit1: TcxDateEdit
    Left = 332
    Top = 300
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 22
    Width = 121
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16752029
    BkColor.EndColor = 16770790
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 440
    Top = 176
  end
end
