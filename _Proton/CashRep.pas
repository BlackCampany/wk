unit CashRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan, cxGridCustomPopupMenu, cxGridPopupMenu;

type
  TfmCashRep = class(TForm)
    FormPlacement1: TFormPlacement;
    ViewCashRep: TcxGridDBTableView;
    LevelCashRep: TcxGridLevel;
    GridCashRep: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    ViewCashRepCashNumber: TcxGridDBColumn;
    ViewCashRepZNumber: TcxGridDBColumn;
    ViewCashRepZSale: TcxGridDBColumn;
    ViewCashRepZReturn: TcxGridDBColumn;
    ViewCashRepZDiscount: TcxGridDBColumn;
    ViewCashRepZDate: TcxGridDBColumn;
    ViewCashRepZCrSale: TcxGridDBColumn;
    ViewCashRepZCrReturn: TcxGridDBColumn;
    ViewCashRepSaleRet: TcxGridDBColumn;
    ViewCashRepSaleN: TcxGridDBColumn;
    SpeedItem3: TSpeedItem;
    ViewCashRepZCrDiscount: TcxGridDBColumn;
    ViewCashRepCurMoney: TcxGridDBColumn;
    ViewCashRepCheckSum: TcxGridDBColumn;
    amCashRep: TActionManager;
    acCalcCheck: TAction;
    acRecalcDeps: TAction;
    acCashCorr: TAction;
    SpeedItem4: TSpeedItem;
    acCashDep: TAction;
    SpeedItem5: TSpeedItem;
    ViewCashRepSaleBn: TcxGridDBColumn;
    ViewCashRepRDiscAll: TcxGridDBColumn;
    acRecalcStat: TAction;
    RecalcCountCli: TAction;
    acRecalcZ: TAction;
    acMoveZ: TAction;
    acDelCashZ: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    Z1: TMenuItem;
    Z2: TMenuItem;
    acMoveCheck: TAction;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acCalcCheckExecute(Sender: TObject);
    procedure acRecalcDepsExecute(Sender: TObject);
    procedure acCashCorrExecute(Sender: TObject);
    procedure acCashDepExecute(Sender: TObject);
    procedure acRecalcStatExecute(Sender: TObject);
    procedure ViewCashRepDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure RecalcCountCliExecute(Sender: TObject);
    procedure acRecalcZExecute(Sender: TObject);
    procedure acMoveZExecute(Sender: TObject);
    procedure acDelCashZExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acMoveCheckExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashRep: TfmCashRep;

implementation

uses Un1, MDB, Period1, MT, Corr, CashDep, Status, SetNewDate;

{$R *.dfm}

procedure TfmCashRep.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridCashRep.Align:=AlClient;
end;

procedure TfmCashRep.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmCashRep.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCashRep.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      fmCashRep.ViewCashRep.BeginUpdate;

      quCashRep.Active:=False;
      quCashRep.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quCashRep.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quCashRep.Active:=True;

      fmCashRep.ViewCashRep.EndUpdate;
      fmCashRep.Caption:='����� �� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmCashRep.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(ViewCashRep);
end;

procedure TfmCashRep.acCalcCheckExecute(Sender: TObject);
Var iNum:INteger;
begin
  //����������� ������� ���
  with dmMc do
  begin
    ViewCashRep.BeginUpdate;
    quCashRep.First;
    while not quCashRep.Eof do
    begin
      quCheckNumber.Active:=False;
      quCheckNumber.ParamByName('CashNum').AsInteger:=quCashRepCashNumber.AsInteger;
      quCheckNumber.ParamByName('ZNum').AsInteger:=quCashRepZNumber.AsInteger;
      quCheckNumber.Active:=True;
      iNum:=quCheckNumberNums.AsInteger;
      quCheckNumber.Active:=False;

      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "Zreport" Set ');
      quE.SQL.Add('CurMoney='+its(iNUm));
      quE.SQL.Add('where CashNumber='+its(quCashRepCashNumber.AsInteger));
      quE.SQL.Add('and ZNumber='+its(quCashRepZNumber.AsInteger));
      quE.ExecSQL;

      quCashRep.Next;
    end;

    quCashRep.Active:=False;
    quCashRep.Active:=True;

    ViewCashRep.EndUpdate;
  end;
end;

procedure TfmCashRep.acRecalcDepsExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrTa:String;
    dCurD:TDateTime;
    rSumN,rSumBn,rSumDN,rSumDbn:Real;
    rSumN1,rSumBn1,rSumDN1,rSumDbn1:Real;
    bErr:Boolean;
    iDep:INteger;
    iCassir:Integer;
begin
  //�������� ���������� �� �������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    with dmMT do
    begin

      DateB:=Trunc(CommonSet.DateBeg);
      DateE:=Trunc(CommonSet.DateEnd);

      prSetTA(DateB); //��� ��� ������� ���-��

      //� zreport ��� ������� - ����� ��� SaleByDepart (bn)
      for iCurD:=DateB to DateE do
      begin
        StrTa:='cq'+formatDateTime('yyyymm',iCurD);
        dCurD:=iCurD;

        quD.SQL.Clear;
        quD.SQL.Add('delete from "SaleByDepart" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
        quD.ExecSQL;
        quD.SQL.Clear;
        quD.SQL.Add('delete from "SaleByDepartBN" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
        quD.ExecSQL;

        //������� ����� ��������� ��������� � CQ- ������� ��� ��������� � ������� �� ������


        ptZRep.Active:=False;
        ptZRep.Active:=True;
        ptZRep.CancelRange;
        ptZRep.SetRange([dCurD],[dCurD]);
        ptZRep.First;
        while not ptZRep.Eof do
        begin

          quCQZ.Active:=False;
          quCQZ.SQL.Clear;
          quCQZ.SQL.Add('select cq.Ck_Card, SUM(cq.Ck_Disc)as CorSum, sum(cq.Summa) as RSum');
          quCQZ.SQL.Add('from  "'+StrTa+'" cq');
          quCQZ.SQL.Add('left join Goods gd on gd.ID=cq.Code');
          quCQZ.SQL.Add('where cq.DateOperation='''+ds(iCurD)+''' and cq.Cash_Code='+its(ptZRepCashNumber.AsInteger)+' and cq.NSmena='+its(ptZRepZNumber.AsInteger));
          quCQZ.SQL.Add('and (cq.Ck_Number<'+its(CommonSet.CorrCheckNum)+' or cq.Ck_Number>'+its(CommonSet.CorrCheckNum+1)+')');
          quCQZ.SQL.Add('group by cq.Ck_Card');
          quCQZ.Active:=True;

          //��� ����� � ���������

          rSumN1:=0; rSumBn1:=0; rSumDN1:=0; rSumDbn1:=0;


          quCQZ.First;
          while not quCQZ.Eof do
          begin
            if quCQZCk_Card.AsInteger=0 then  begin rSumN1:=rv(quCQZRSum.AsFloat); rSumDN1:=rv(quCQZCorSum.AsFloat);  end;
            if quCQZCk_Card.AsInteger=1 then  begin rSumBn1:=rv(quCQZRSum.AsFloat); rSumDBn1:=rv(quCQZCorSum.AsFloat);  end;
            quCQZ.Next;
          end;
          quCQZ.Active:=False;


          rSumN:=rv(ptZRepZSale.AsFloat-ptZRepZCrSale.AsFloat-ptZRepZReturn.AsFloat+ptZRepZCrReturn.AsFloat);
          rSumBn:=rv(ptZRepZCrSale.AsFloat-ptZRepZCrReturn.AsFloat);
          rSumDN:=rv(ptZRepZDiscount.AsFloat);
          rSumDbn:=rv(ptZRepZCrDiscount.AsFloat);
{
(zr.ZSale-zr.ZCrSale-zr.ZReturn+zr.ZCrReturn) as SaleN,
(zr.ZCrSale-zr.ZCrReturn) as SaleBn,
}
          {
          bErr:=False;
          if rSumN<>rSumN1 then bErr:=True;
          if rSumBn<>rSumBn1 then bErr:=True;
          if rSumDN<>rSumDN1 then bErr:=True;
          if rSumDbn<>rSumDbn1 then bErr:=True;
          }
          bErr:=True; //������ ������

          if bErr then //���� ����������� - ����� ������ ��������� ������
          begin
            ptCQ.Active:=False;
            ptCQ.DatabaseName:=Btr1Path;
            ptCQ.TableName:=StrTa;
            ptCQ.Active:=True;

            ptCQ.CancelRange;
            ptCQ.SetRange([ptZRepZDate.AsDateTime,ptZRepCashNumber.AsInteger],[ptZRepZDate.AsDateTime,ptZRepCashNumber.AsInteger]);
            ptCQ.Last;

            iDep:=4;
            quCash.Active:=False;
            quCash.Active:=True;
            if quCash.Locate('Number',ptZRepCashNumber.AsInteger,[]) then iDep:=RoundEx(quCashRezerv.AsFloat);
            quCash.Active:=False;

            if ptCQ.Locate('Ck_Number',CommonSet.CorrCheckNum,[]) then ptCQ.Delete;
            if ptCQ.Locate('Ck_Number',CommonSet.CorrCheckNum+1,[]) then ptCQ.Delete;
            delay(10);

            if (rSumN<>rSumN1)or(rSumDN<>rSumDN1) then
            begin //��� ��� ������ ���
              ptCQ.Append;
              ptCQQuant.AsFloat:=1;
              ptCQOperation.AsString:='P';
              ptCQDateOperation.AsDateTime:=ptZRepZDate.AsDateTime;
              ptCQPrice.AsFloat:=rSumN-rSumN1;
              ptCQStore.AsString:='';
              ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum;
              ptCQCk_Curs.AsFloat:=0;
              ptCQCk_CurAbr.AsInteger:=0;
              ptCQCk_Disg.AsFloat:=0;
              ptCQCk_Disc.AsFloat:=rSumDN-rSumDN1;
              ptCQQuant_S.AsFloat:=0;
              ptCQGrCode.AsFloat:=iDep;
              ptCQCode.AsInteger:=CommonSet.CorrArt;
              ptCQCassir.AsString:='10'+ptZRepCashNumber.AsString;
              ptCQCash_Code.AsInteger:=ptZRepCashNumber.AsInteger;
              ptCQCk_Card.AsInteger:=0;//���
              ptCQContr_Code.AsString:='';
              ptCQContr_Cost.AsFloat:=0;
              ptCQSeria.AsString:='';
              ptCQBestB.AsString:='';
              ptCQNDSx1.AsFloat:=0;
              ptCQNDSx2.AsFloat:=0;
              ptCQTimes.AsDateTime:=StrToTime('07:10');
              ptCQSumma.AsFloat:=rSumN-rSumN1;
              ptCQSumNDS.AsFloat:=0;
              ptCQSumNSP.AsFloat:=0;
              ptCQPriceNSP.AsFloat:=0;
              ptCQNSmena.AsInteger:=ptZRepZNumber.AsInteger;
              ptCQ.Post;
            end;

            if (rSumBn<>rSumBn1)or(rSumDbn<>rSumDbn1) then
            begin //��� ��� ������ ���
              ptCQ.Append;
              ptCQQuant.AsFloat:=1;
              ptCQOperation.AsString:='P';
              ptCQDateOperation.AsDateTime:=ptZRepZDate.AsDateTime;
              ptCQPrice.AsFloat:=rSumBn-rSumBn1;
              ptCQStore.AsString:='';
              ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum+1;
              ptCQCk_Curs.AsFloat:=0;
              ptCQCk_CurAbr.AsInteger:=0;
              ptCQCk_Disg.AsFloat:=0;
              ptCQCk_Disc.AsFloat:=rSumDbn-rSumDbn1;
              ptCQQuant_S.AsFloat:=0;
              ptCQGrCode.AsFloat:=iDep;
              ptCQCode.AsInteger:=CommonSet.CorrArt;
              ptCQCassir.AsString:='10'+ptZRepCashNumber.AsString;
              ptCQCash_Code.AsInteger:=ptZRepCashNumber.AsInteger;
              ptCQCk_Card.AsInteger:=1;//�-���
              ptCQContr_Code.AsString:='';
              ptCQContr_Cost.AsFloat:=0;
              ptCQSeria.AsString:='';
              ptCQBestB.AsString:='';
              ptCQNDSx1.AsFloat:=0;
              ptCQNDSx2.AsFloat:=0;
              ptCQTimes.AsDateTime:=StrToTime('07:10');
              ptCQSumma.AsFloat:=rSumBn-rSumBn1;
              ptCQSumNDS.AsFloat:=0;
              ptCQSumNSP.AsFloat:=0;
              ptCQPriceNSP.AsFloat:=0;
              ptCQNSmena.AsInteger:=ptZRepZNumber.AsInteger;
              ptCQ.Post;
            end;
            delay(100);
            ptCQ.Active:=False;
          end;

          ptZRep.Next;
        end;

        ptZRep.Active:=False;

        quCq.Active:=False;
        quCq.SQL.Clear;
        quCq.SQL.Add('select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card,');
        quCq.SQL.Add('SUM(cq.Ck_Disc)as CorSum');
        quCq.SQL.Add(', sum(cq.Summa) as RSum');
        quCq.SQL.Add(', SUM(if(gd.NDS<=13,cq.Ck_Disc,0))as CorSum10');
        quCq.SQL.Add(', sum(if(gd.NDS<=13,cq.Summa,0))as RSum10');
        quCq.SQL.Add(', SUM(if(gd.NDS>13,cq.Ck_Disc,0))as CorSum18');
        quCq.SQL.Add(', sum(if(gd.NDS>13,cq.Summa,0))as RSum18');
        quCq.SQL.Add('from "'+StrTa+'" cq');
        quCq.SQL.Add('left join Goods gd on gd.ID=cq.Code');
        quCq.SQL.Add('where cq.DateOperation='''+ds(iCurD)+'''');
        quCq.SQL.Add('group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card');
        quCq.SQL.Add('order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card');

        quCq.Active:=True;

        quCq.First;
        while not quCq.Eof do
        begin
          iCassir:=quCqCassir.AsInteger;
          if iCassir>1000000 then iCassir:=(iCassir-1000000)*-1;
          quA.SQL.Clear;
          if quCqCk_Card.AsInteger=0 then //���
          begin
            quA.SQL.Add('INSERT into "SaleByDepart" values (');
          end else //������
          begin
            quA.SQL.Add('INSERT into "SaleByDepartBN" values (');
          end;
          quA.SQL.Add(''''+ds(iCurD)+''','); //DateSale
          quA.SQL.Add(its(quCqCash_Code.AsInteger)+',');  //CassaNumber
          quA.SQL.Add(its(quCqGrCode.AsInteger)+',');//Otdel
          quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat))+',');//Summa
          quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat))+',');//SummaCor
          quA.SQL.Add(its(iCassir)+',');//Employee
//          quA.SQL.Add(fs(roundVal(quCqRSum10.AsFloat))+',');//SummaNDSx10
          quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx10
          quA.SQL.Add(fs(roundVal(quCqRSum18.AsFloat))+',');//SummaNDSx20
          quA.SQL.Add(fs(roundVal(quCqCorSum10.AsFloat))+',');//CorNDSx10
          quA.SQL.Add(fs(roundVal(quCqCorSum18.AsFloat))+',');//CorNDSx20
          quA.SQL.Add('0,');//SummaNSPx10
          quA.SQL.Add('0,');//SummaNSPx20
          quA.SQL.Add('0,');//SummaOblNSP
          quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum10.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx0
          quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat-quCqCorSum10.AsFloat-quCqCorSum18.AsFloat))+',');//CorNDSx0
          quA.SQL.Add('0,');//SummaNSP0
          quA.SQL.Add('0,');//NDSx10
          quA.SQL.Add('0,');//NDSx20
          quA.SQL.Add('0,');//CorNSPx0
          quA.SQL.Add('0,');//CorNSPx10
          quA.SQL.Add('0,');//CorNSPx20
          quA.SQL.Add('0,');//Rezerv
          quA.SQL.Add('3,');//ShopIndex
          quA.SQL.Add('0,');//RezervSumma
          quA.SQL.Add('0,');//RezervSumNSP
          quA.SQL.Add('0,');//RezervSkid
          quA.SQL.Add('0,');//RezervSkidNSP
          quA.SQL.Add('0,');//SenderSumma
          quA.SQL.Add('0,');//SenderSumNSP
          quA.SQL.Add('0,');//SenderSkid
          quA.SQL.Add('0');//SenderSkidNSP
          quA.SQL.Add(')');
          quA.ExecSQL;

          quCq.Next;
        end;
      end;
      showmessage('�������� ��������. �� �������� ��������������� ��.');
    end;
  end;
end;

procedure TfmCashRep.acCashCorrExecute(Sender: TObject);
Var iDep,iCurD:Integer;
    rSaleN,rSaleBn,rDiscN,rDiscBn,rDif:Real;
    StrTa :String;
    bClosePer:Boolean;
    iCassir:Integer;
begin
  //��������� �� ������
  with dmMC do
  with dmMT do
  begin
    if (pos('�����',Person.Name)=0)and(pos('OPER CB',Person.Name)=0)and(pos('OPERZAK',Person.Name)=0) then exit;

    bClosePer:=False;
    if quCashRepZDate.AsDateTime<=prOpenDate then bClosePer:=True;
    if ViewCashRep.Controller.SelectedRowCount>0 then
    begin
      StrTa:='cq'+formatDateTime('yyyymm',quCashRepZDate.AsDateTime);

      quCashSum0.Active:=False;
      quCashSum0.SQL.Clear;
      quCashSum0.SQL.Add('select Ck_Card, Sum(Ck_Disc) as DSum, Sum(Summa) as RSum from "'+StrTa+'"');
      quCashSum0.SQL.Add('where DateOperation='''+ds(quCashRepZDate.AsDateTime)+''' and Cash_Code='+its(quCashRepCashNumber.AsInteger)+' and NSmena='+its(quCashRepZNumber.AsInteger));
      quCashSum0.SQL.Add(' and (Ck_Number<'+its(CommonSet.CorrCheckNum)+' or Ck_Number>'+its(CommonSet.CorrCheckNum+100)+')');
      quCashSum0.SQL.Add('group by Ck_Card');
      quCashSum0.Active:=True;

      rSaleBn:=0; rSaleN:=0; rDiscN:=0; rDiscBn:=0;

      quCashSum0.First;
      while not quCashSum0.Eof do
      begin
        if quCashSum0Ck_Card.AsInteger=0 then   begin  rSaleN:=rv(quCashSum0RSum.AsFloat);  rDiscN:=rv(quCashSum0DSum.AsFloat); end;
        if quCashSum0Ck_Card.AsInteger=1 then   begin  rSaleBn:=rv(quCashSum0RSum.AsFloat); rDiscBn:=rv(quCashSum0DSum.AsFloat); end;
        quCashSum0.Next;
      end;

      quCashSum0.Active:=False;

      fmCorr:=tfmCorr.Create(Application);
      fmCorr.cxCurrencyEdit1.Value:=rSaleN;
      fmCorr.cxCurrencyEdit2.Value:=RoundVal(quCashRepSaleN.AsFloat);
      fmCorr.cxCurrencyEdit3.Value:=rSaleBn;
      fmCorr.cxCurrencyEdit4.Value:=RoundVal(quCashRepSaleBn.AsFloat);
      fmCorr.cxCurrencyEdit5.Value:=rDiscN;
      fmCorr.cxCurrencyEdit6.Value:=RoundVal(quCashRepZDiscount.AsFloat);
      fmCorr.cxCurrencyEdit7.Value:=rDiscBn;
      fmCorr.cxCurrencyEdit8.Value:=RoundVal(quCashRepZCrDiscount.AsFloat);

      fmCorr.ShowModal;
      if fmCorr.ModalResult=mrOk then
      begin //���������
        if bClosePer=False then
        begin
        ptCQ.Active:=False;
        ptCQ.DatabaseName:=Btr1Path;
        ptCQ.TableName:='cq'+formatDateTime('yyyymm',quCashRepZDate.AsDateTime);
        ptCQ.Active:=True;

        ptCQ.CancelRange;
        ptCQ.SetRange([quCashRepZDate.AsDateTime,quCashRepCashNumber.AsInteger],[quCashRepZDate.AsDateTime,quCashRepCashNumber.AsInteger]);
//        ptCQ.Last;

        iDep:=4;
        quCash.Active:=False;
        quCash.Active:=True;
        if quCash.Locate('Number',quCashRepCashNumber.AsInteger,[]) then iDep:=RoundEx(quCashRezerv.AsFloat);
        quCash.Active:=False;

        if ptCQ.Locate('Ck_Number',CommonSet.CorrCheckNum,[]) then ptCQ.Delete;
        if ptCQ.Locate('Ck_Number',CommonSet.CorrCheckNum+1,[]) then ptCQ.Delete;

        delay(100);

        if (abs(fmCorr.cxCurrencyEdit1.Value-fmCorr.cxCurrencyEdit2.Value)>0.001)or (abs(fmCorr.cxCurrencyEdit5.Value-fmCorr.cxCurrencyEdit6.Value)>0.001) then
        begin //��� ��� ������ ���
          ptCQ.Append;
          ptCQQuant.AsFloat:=1;
          ptCQOperation.AsString:='P';
          ptCQDateOperation.AsDateTime:=quCashRepZDate.AsDateTime;
          ptCQPrice.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit2.Value-fmCorr.cxCurrencyEdit1.Value);
          ptCQStore.AsString:='';
          ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum;
          ptCQCk_Curs.AsFloat:=0;
          ptCQCk_CurAbr.AsInteger:=0;
          ptCQCk_Disg.AsFloat:=0;
          ptCQCk_Disc.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit6.Value-fmCorr.cxCurrencyEdit5.Value);
          ptCQQuant_S.AsFloat:=0;
          ptCQGrCode.AsFloat:=iDep;
          ptCQCode.AsInteger:=CommonSet.CorrArt;
          ptCQCassir.AsString:='10'+quCashRepCashNumber.AsString;
          ptCQCash_Code.AsInteger:=quCashRepCashNumber.AsInteger;
          ptCQCk_Card.AsInteger:=0;//���
          ptCQContr_Code.AsString:='';
          ptCQContr_Cost.AsFloat:=0;
          ptCQSeria.AsString:='';
          ptCQBestB.AsString:='';
          ptCQNDSx1.AsFloat:=0;
          ptCQNDSx2.AsFloat:=0;
          ptCQTimes.AsDateTime:=StrToTime('07:10');
          ptCQSumma.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit2.Value-fmCorr.cxCurrencyEdit1.Value);
          ptCQSumNDS.AsFloat:=0;
          ptCQSumNSP.AsFloat:=0;
          ptCQPriceNSP.AsFloat:=0;
          ptCQNSmena.AsInteger:=quCashRepZNumber.AsInteger;
          ptCQ.Post;
        end;

        if (abs(fmCorr.cxCurrencyEdit4.Value-fmCorr.cxCurrencyEdit3.Value)>0.001)or (abs(fmCorr.cxCurrencyEdit8.Value-fmCorr.cxCurrencyEdit7.Value)>0.001) then
        begin //������ ��� ������ ������
          ptCQ.Append;
          ptCQQuant.AsFloat:=1;
          ptCQOperation.AsString:='P';
          ptCQDateOperation.AsDateTime:=quCashRepZDate.AsDateTime;
          ptCQPrice.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit4.Value-fmCorr.cxCurrencyEdit3.Value);
          ptCQStore.AsString:='';
          ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum+1;
          ptCQCk_Curs.AsFloat:=0;
          ptCQCk_CurAbr.AsInteger:=0;
          ptCQCk_Disg.AsFloat:=0;
          ptCQCk_Disc.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit8.Value-fmCorr.cxCurrencyEdit7.Value);
          ptCQQuant_S.AsFloat:=0;
          ptCQGrCode.AsFloat:=iDep;
          ptCQCode.AsInteger:=CommonSet.CorrArt;
          ptCQCassir.AsString:='10'+quCashRepCashNumber.AsString;
          ptCQCash_Code.AsInteger:=quCashRepCashNumber.AsInteger;
          ptCQCk_Card.AsInteger:=1;//�-���
          ptCQContr_Code.AsString:='';
          ptCQContr_Cost.AsFloat:=0;
          ptCQSeria.AsString:='';
          ptCQBestB.AsString:='';
          ptCQNDSx1.AsFloat:=0;
          ptCQNDSx2.AsFloat:=0;
          ptCQTimes.AsDateTime:=StrToTime('07:10');
          ptCQSumma.AsFloat:=RoundVal(fmCorr.cxCurrencyEdit4.Value-fmCorr.cxCurrencyEdit3.Value);
          ptCQSumNDS.AsFloat:=0;
          ptCQSumNSP.AsFloat:=0;
          ptCQPriceNSP.AsFloat:=0;
          ptCQNSmena.AsInteger:=quCashRepZNumber.AsInteger;
          ptCQ.Post;
        end;
        delay(100);
        ptCQ.Active:=False;

        if (abs(fmCorr.cxCurrencyEdit1.Value-fmCorr.cxCurrencyEdit2.Value)>0.001)or (abs(fmCorr.cxCurrencyEdit4.Value-fmCorr.cxCurrencyEdit3.Value)>0.001) then
        begin
          if NeedPost(Trunc(RoundEx(quCashRepZDate.AsDateTime))) then
          begin
            rDif:=rv((fmCorr.cxCurrencyEdit1.Value-fmCorr.cxCurrencyEdit2.Value)+(fmCorr.cxCurrencyEdit4.Value-fmCorr.cxCurrencyEdit3.Value));
            iNumPost:=PostNum(0)*100+8;

            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'CN;ON;'+its(iDep)+r+ds(quCashRepZDate.AsDateTime)+r+fs(rDif));

            finally
              closefile(fPost);
            end;

            //������ ����� - ���� ��������
            prTr(sNumPost(iNumPost),nil);
          end;
        end;


        //���� �������� -  ����� ���������� � ����� ������
        rSaleBn:=fmCorr.cxCurrencyEdit4.Value;
        rSaleN:=fmCorr.cxCurrencyEdit2.Value+rSaleBn+quCashRepZReturn.AsFloat; //� SQL - �������� ���������� - ������ ������ ���������
        rDiscN:=fmCorr.cxCurrencyEdit6.Value;         //quCashRepZReturn.AsFloat - ��� ��� �������� � ��� � ��
        rDiscBn:=fmCorr.cxCurrencyEdit8.Value;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "Zreport" Set ');
        quE.SQL.Add('ZSale='+fs(rSaleN)+',');
        quE.SQL.Add('ZDiscount='+fs(rDiscN)+',');
        quE.SQL.Add('ZCrSale='+fs(rSaleBn+quCashRepZCrReturn.AsFloat)+','); //� SQL �������� ����������
        quE.SQL.Add('ZCrDiscount='+fs(rDiscBn));
        quE.SQL.Add('where CashNumber='+its(quCashRepCashNumber.AsInteger));
        quE.SQL.Add('and ZNumber='+its(quCashRepZNumber.AsInteger));
        quE.ExecSQL;

        quCashRep.Active:=False;
        quCashRep.Active:=True;

        //����� ����� ������ ��������� �� �������

//        StrTa:='cq'+formatDateTime('yyyymm',iCurD);

        iCurD:=Trunc(quCashRepZDate.AsDateTime);

        quD.SQL.Clear;
        quD.SQL.Add('delete from "SaleByDepart" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
        quD.ExecSQL;
        quD.SQL.Clear;
        quD.SQL.Add('delete from "SaleByDepartBN" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
        quD.ExecSQL;

        quCq.Active:=False;
        quCq.SQL.Clear;
        quCq.SQL.Add('select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card,');
        quCq.SQL.Add('SUM(cq.Ck_Disc)as CorSum');
        quCq.SQL.Add(', sum(cq.Summa) as RSum');
        quCq.SQL.Add(', SUM(if(gd.NDS<=13,cq.Ck_Disc,0))as CorSum10');
        quCq.SQL.Add(', sum(if(gd.NDS<=13,cq.Summa,0))as RSum10');
        quCq.SQL.Add(', SUM(if(gd.NDS>13,cq.Ck_Disc,0))as CorSum18');
        quCq.SQL.Add(', sum(if(gd.NDS>13,cq.Summa,0))as RSum18');
        quCq.SQL.Add('from "'+StrTa+'" cq');
        quCq.SQL.Add('left join Goods gd on gd.ID=cq.Code');
        quCq.SQL.Add('where cq.DateOperation='''+ds(iCurD)+'''');
        quCq.SQL.Add('group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card');
        quCq.SQL.Add('order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card');

        quCq.Active:=True;

        quCq.First;
        while not quCq.Eof do
        begin
          iCassir:=quCqCassir.AsInteger;
          if iCassir>1000000 then iCassir:=(iCassir-1000000)*-1;
          quA.SQL.Clear;
          if quCqCk_Card.AsInteger=0 then //���
          begin
            quA.SQL.Add('INSERT into "SaleByDepart" values (');
          end else //������
          begin
            quA.SQL.Add('INSERT into "SaleByDepartBN" values (');
          end;
          quA.SQL.Add(''''+ds(iCurD)+''','); //DateSale
          quA.SQL.Add(its(quCqCash_Code.AsInteger)+',');  //CassaNumber
          quA.SQL.Add(its(quCqGrCode.AsInteger)+',');//Otdel
          quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat))+',');//Summa
          quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat))+',');//SummaCor
          quA.SQL.Add(its(iCassir)+',');//Employee
//          quA.SQL.Add(fs(roundVal(quCqRSum10.AsFloat))+',');//SummaNDSx10
          quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx10
          quA.SQL.Add(fs(roundVal(quCqRSum18.AsFloat))+',');//SummaNDSx20
          quA.SQL.Add(fs(roundVal(quCqCorSum10.AsFloat))+',');//CorNDSx10
          quA.SQL.Add(fs(roundVal(quCqCorSum18.AsFloat))+',');//CorNDSx20
          quA.SQL.Add('0,');//SummaNSPx10
          quA.SQL.Add('0,');//SummaNSPx20
          quA.SQL.Add('0,');//SummaOblNSP
          quA.SQL.Add('0,');//SummaNDSx0
          quA.SQL.Add('0,');//CorNDSx0
          quA.SQL.Add('0,');//SummaNSP0
          quA.SQL.Add('0,');//NDSx10
          quA.SQL.Add('0,');//NDSx20
          quA.SQL.Add('0,');//CorNSPx0
          quA.SQL.Add('0,');//CorNSPx10
          quA.SQL.Add('0,');//CorNSPx20
          quA.SQL.Add('0,');//Rezerv
          quA.SQL.Add('3,');//ShopIndex
          quA.SQL.Add('0,');//RezervSumma
          quA.SQL.Add('0,');//RezervSumNSP
          quA.SQL.Add('0,');//RezervSkid
          quA.SQL.Add('0,');//RezervSkidNSP
          quA.SQL.Add('0,');//SenderSumma
          quA.SQL.Add('0,');//SenderSumNSP
          quA.SQL.Add('0,');//SenderSkid
          quA.SQL.Add('0');//SenderSkidNSP
          quA.SQL.Add(')');
          quA.ExecSQL;

          quCq.Next;
        end;

//�������
        if NeedPost(iCurD) then
        begin

          iNumPost:=PostNum(0)*100+9;
//        prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
          try
            assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
            rewrite(fPost);

            fmCashDep.ViewCDep.BeginUpdate ;

            quCDep.Active:=False;
            quCDep.SQL.Clear;
            quCDep.SQL.Add('Select');
            quCDep.SQL.Add('sd.DateSale,');
            quCDep.SQL.Add('sd.CassaNumber,');
            quCDep.SQL.Add('sd.Otdel,');
            quCDep.SQL.Add('sd.Summa,');
            quCDep.SQL.Add('sd.SummaCor,');
            quCDep.SQL.Add('sd.Employee,');
            quCDep.SQL.Add('sd.SummaNDSx10,');
            quCDep.SQL.Add('sd.SummaNDSx20,');
            quCDep.SQL.Add('sd.SummaNDSx0,');
            quCDep.SQL.Add('sd.CorNDSx10,');
            quCDep.SQL.Add('sd.CorNDSx20,');
            quCDep.SQL.Add('sd.CorNDSx0,');
            quCDep.SQL.Add('de.Name');
            quCDep.SQL.Add('from "SaleByDepart" sd');
            quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
            quCDep.SQL.Add('where DateSale>='''+ds(iCurD)+'''');
            quCDep.SQL.Add('and DateSale<='''+ds(iCurD)+'''');
            quCDep.Active:=True;

            quCDep.First;
            while not quCDep.Eof do
            begin
              StrPost:='DEPN;'+ds(quCDepDateSale.AsDateTime)+r+its(quCDepCassaNumber.AsInteger)+r+its(quCDepOtdel.AsInteger)+r+its(quCDepEmployee.AsInteger)+r+fs(quCDepSumma.AsFloat)+r+fs(quCDepSummaCor.AsFloat)+r+fs(quCDepSummaNDSx10.AsFloat)+r+fs(quCDepSummaNDSx20.AsFloat)+r+fs(quCDepSummaNDSx0.AsFloat)+r+fs(quCDepCorNDSx10.AsFloat)+r+fs(quCDepCorNDSx20.AsFloat)+r+fs(quCDepCorNDSx0.AsFloat);

              writeln(fPost,StrPost);
              quCDep.Next;
            end;

            quCDep.Active:=False;
            quCDep.SQL.Clear;
            quCDep.SQL.Add('Select');
            quCDep.SQL.Add('sd.DateSale,');
            quCDep.SQL.Add('sd.CassaNumber,');
            quCDep.SQL.Add('sd.Otdel,');
            quCDep.SQL.Add('sd.Summa,');
            quCDep.SQL.Add('sd.SummaCor,');
            quCDep.SQL.Add('sd.Employee,');
            quCDep.SQL.Add('sd.SummaNDSx10,');
            quCDep.SQL.Add('sd.SummaNDSx20,');
            quCDep.SQL.Add('sd.SummaNDSx0,');
            quCDep.SQL.Add('sd.CorNDSx10,');
            quCDep.SQL.Add('sd.CorNDSx20,');
            quCDep.SQL.Add('sd.CorNDSx0,');
            quCDep.SQL.Add('de.Name');
            quCDep.SQL.Add('from "SaleByDepartBn" sd');
            quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
            quCDep.SQL.Add('where DateSale>='''+ds(iCurD)+'''');
            quCDep.SQL.Add('and DateSale<='''+ds(iCurD)+'''');
            quCDep.Active:=True;

            quCDep.First;
            while not quCDep.Eof do
            begin
              StrPost:='DEPB;'+ds(quCDepDateSale.AsDateTime)+r+its(quCDepCassaNumber.AsInteger)+r+its(quCDepOtdel.AsInteger)+r+its(quCDepEmployee.AsInteger)+r+fs(quCDepSumma.AsFloat)+r+fs(quCDepSummaCor.AsFloat)+r+fs(quCDepSummaNDSx10.AsFloat)+r+fs(quCDepSummaNDSx20.AsFloat)+r+fs(quCDepSummaNDSx0.AsFloat)+r+fs(quCDepCorNDSx10.AsFloat)+r+fs(quCDepCorNDSx20.AsFloat)+r+fs(quCDepCorNDSx0.AsFloat);
//              StrPost:='DEPN;'+ds(quCDepDateSale.AsDateTime)+r+its(quCDepCassaNumber.AsInteger)+r+its(quCDepEmployee.AsInteger)+r+fs(quCDepSumma.AsFloat)+r+fs(quCDepSummaCor.AsFloat)+r+fs(quCDepSummaNDSx10.AsFloat)+r+fs(quCDepSummaNDSx20.AsFloat)+r+fs(quCDepSummaNDSx0.AsFloat)+r+fs(quCDepCorNDSx10.AsFloat)+r+fs(quCDepCorNDSx20.AsFloat)+r+fs(quCDepCorNDSx0.AsFloat);

              writeln(fPost,StrPost);
              quCDep.Next;
            end;

            quCDep.Active:=False;
            fmCashDep.ViewCDep.EndUpdate;

          finally
            closefile(fPost);
          end;
          //������ ����� - ���� ��������
          prTr(sNumPost(iNumPost),nil);
        end;

        if (trunc(Date)-iCurD)>=1 then prRecalcTO(iCurD,trunc(Date)-1,nil,0,0);
        end else showmessage('������ ������.');
      end;

      fmCorr.Release;
    end;
  end;
end;

procedure TfmCashRep.acCashDepExecute(Sender: TObject);
begin
  //���������� �� �������
  with dmMc do
  begin
    //��������
    fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
    fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      fmCashDep.ViewCDep.BeginUpdate ;

      CloseTe(taCDep);

      quCDep.Active:=False;
      quCDep.SQL.Clear;
      quCDep.SQL.Add('Select');
      quCDep.SQL.Add('sd.DateSale,');
      quCDep.SQL.Add('sd.CassaNumber,');
      quCDep.SQL.Add('sd.Otdel,');
      quCDep.SQL.Add('sd.Summa,');
      quCDep.SQL.Add('sd.SummaCor,');
      quCDep.SQL.Add('sd.Employee,');
      quCDep.SQL.Add('sd.SummaNDSx10,');
      quCDep.SQL.Add('sd.SummaNDSx20,');
      quCDep.SQL.Add('sd.SummaNDSx0,');
      quCDep.SQL.Add('sd.CorNDSx10,');
      quCDep.SQL.Add('sd.CorNDSx20,');
      quCDep.SQL.Add('sd.CorNDSx0,');
      quCDep.SQL.Add('de.Name');
      quCDep.SQL.Add('from "SaleByDepart" sd');
      quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
      quCDep.SQL.Add('where DateSale>='''+ds(CommonSet.DateBeg)+'''');
      quCDep.SQL.Add('and DateSale<='''+ds(CommonSet.DateEnd)+'''');
      quCDep.Active:=True;

      quCDep.First;
      while not quCDep.Eof do
      begin
        taCDep.Append;
        taCDepValT.AsInteger:=1;
        taCDepiDep.AsInteger:=quCDepOtdel.AsInteger;
        taCDepsDep.AsString:=quCDepName.AsString;
        taCDepDateSale.AsDateTime:=quCDepDateSale.AsDateTime;
        taCDepCashNum.AsInteger:=quCDepCassaNumber.AsInteger;
        taCDepCassir.AsInteger:=quCDepEmployee.AsInteger;
        taCDepRSum.AsFloat:=quCDepSumma.AsFloat;
        taCDepRSumD.AsFloat:=quCDepSummaCor.AsFloat;
        taCDepRSumN10.AsFloat:=quCDepSummaNDSx10.AsFloat;
        taCDepRSumN20.AsFloat:=quCDepSummaNDSx20.AsFloat;
        taCDeprSumN0.AsFloat:=quCDepSummaNDSx0.AsFloat;
        taCDepRSumD10.AsFloat:=quCDepCorNDSx10.AsFloat;
        taCDepRSumD20.AsFloat:=quCDepCorNDSx20.AsFloat;
        taCDepRSumD0.AsFloat:=quCDepCorNDSx0.AsFloat;
        taCDep.Post;

        quCDep.Next;
      end;


      quCDep.Active:=False;
      quCDep.SQL.Clear;
      quCDep.SQL.Add('Select');
      quCDep.SQL.Add('sd.DateSale,');
      quCDep.SQL.Add('sd.CassaNumber,');
      quCDep.SQL.Add('sd.Otdel,');
      quCDep.SQL.Add('sd.Summa,');
      quCDep.SQL.Add('sd.SummaCor,');
      quCDep.SQL.Add('sd.Employee,');
      quCDep.SQL.Add('sd.SummaNDSx10,');
      quCDep.SQL.Add('sd.SummaNDSx20,');
      quCDep.SQL.Add('sd.SummaNDSx0,');
      quCDep.SQL.Add('sd.CorNDSx10,');
      quCDep.SQL.Add('sd.CorNDSx20,');
      quCDep.SQL.Add('sd.CorNDSx0,');
      quCDep.SQL.Add('de.Name');
      quCDep.SQL.Add('from "SaleByDepartBn" sd');
      quCDep.SQL.Add('left join Depart de on de.ID=sd.Otdel');
      quCDep.SQL.Add('where DateSale>='''+ds(CommonSet.DateBeg)+'''');
      quCDep.SQL.Add('and DateSale<='''+ds(CommonSet.DateEnd)+'''');
      quCDep.Active:=True;

      quCDep.First;
      while not quCDep.Eof do
      begin
        taCDep.Append;
        taCDepValT.AsInteger:=2;
        taCDepiDep.AsInteger:=quCDepOtdel.AsInteger;
        taCDepsDep.AsString:=quCDepName.AsString;
        taCDepDateSale.AsDateTime:=quCDepDateSale.AsDateTime;
        taCDepCashNum.AsInteger:=quCDepCassaNumber.AsInteger;
        taCDepCassir.AsInteger:=quCDepEmployee.AsInteger;
        taCDepRSum.AsFloat:=quCDepSumma.AsFloat;
        taCDepRSumD.AsFloat:=quCDepSummaCor.AsFloat;
        taCDepRSumN10.AsFloat:=quCDepSummaNDSx10.AsFloat;
        taCDepRSumN20.AsFloat:=quCDepSummaNDSx20.AsFloat;
        taCDeprSumN0.AsFloat:=quCDepSummaNDSx0.AsFloat;
        taCDepRSumD10.AsFloat:=quCDepCorNDSx10.AsFloat;
        taCDepRSumD20.AsFloat:=quCDepCorNDSx20.AsFloat;
        taCDepRSumD0.AsFloat:=quCDepCorNDSx0.AsFloat;
        taCDep.Post;

        quCDep.Next;
      end;
      fmCashDep.ViewCDep.EndUpdate;
//      fmCashDep:=TfmCashDep.Create(Application);
      fmCashDep.Show;
//      fmCashDep.Release;
    end;
  end;
end;

procedure TfmCashRep.acRecalcStatExecute(Sender: TObject);
  //�������� ����������
type tsale = record
     ch:INteger;
     su,ma:Real;
     end;

Var StrWk:String;
    DateB,DateE,iCurD:Integer;
    iC:INteger;
    CurD:TDateTime;
    asale:Array[1..24] of TSale;
    allpos,allcli:INteger;
    allsu,allma:Real;
    Cli20,Cli50,cli100,cli200,cli500,cli1000,cliM:INteger;
    n:INteger;
    iCh,iH:INteger;
    rSumCh:Real;
    CashN:INteger;

begin

  with dmMc do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

      if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
      else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      try
        try
          fmSt.Memo1.Lines.Add(fmt+'������ ��������� .'+StrWk);
          for iCurD:=DateB to DateE do
          begin
            iC:=0;

            fmSt.Memo1.Lines.Add(fmt+'');
            fmSt.Memo1.Lines.Add(fmt+'  ��������� ����  '+ FormatDateTime('dd.mm.yyyy',iCurD)); delay(10);

            // ������� ��� �������
            for n:=1 to 24 do
            begin
              asale[n].ch:=0;
              asale[n].su:=0;
              asale[n].ma:=0;
            end;

            allpos:=0; allcli:=0;
            allsu:=0; allma:=0;
            Cli20:=0;
            Cli50:=0;
            cli100:=0;
            cli200:=0;
            cli500:=0;
            cli1000:=0;
            cliM:=0;

            CurD:=iCurD;
{
            quCQCheck.Active:=False;
            quCQCheck.SQL.Clear;
            quCQCheck.SQL.Add('select DateOperation,Cash_Code,NSmena,Times,Ck_Number,Operation,SUM(Summa) as CheckSum, Count(*) as QuantPos');
            quCQCheck.SQL.Add('from "cq'+FormatDateTime('yyyymm',CurD)+'"');
            quCQCheck.SQL.Add('where DateOperation='''+ds(CurD)+'''');
            quCQCheck.SQL.Add('group by DateOperation,Cash_Code,NSmena,Times,Ck_Number,Operation');
            quCQCheck.Active:=True;
}

            quCQCheck.Active:=False;
            quCQCheck.SQL.Clear;
            quCQCheck.SQL.Add('select DateOperation,Cash_Code,NSmena,Ck_Number,Operation,SUM(Summa) as CheckSum, Count(*) as QuantPos,max(Times) as Times ');
            quCQCheck.SQL.Add('from "cq'+FormatDateTime('yyyymm',CurD)+'"');
            quCQCheck.SQL.Add('where DateOperation='''+ds(CurD)+'''');
//            quCQCheck.SQL.Add('where DateOperation='''+ds(CurD)+''' and Operation=''P''');
            quCQCheck.SQL.Add('group by DateOperation,Cash_Code,NSmena,Ck_Number,Operation');
            quCQCheck.Active:=True;


            
            quCQCheck.First;
            CashN:=quCQCheckCash_Code.AsInteger;
            fmSt.Memo1.Lines.Add(fmt+'    ����� � '+IntToStr(CashN)); delay(10);

            while not quCQCheck.Eof do
            begin
              if CashN<>quCQCheckCash_Code.AsInteger then //����� ��������� - ���� ���������� � ��������
              begin
                //������
                quD.SQL.Clear;
                quD.SQL.Add('Delete from "CashDay"' );
                quD.SQL.Add('where DateSale='''+ds(iCurD)+'''');
                quD.SQL.Add('and Number='+its(CashN));
                quD.ExecSQL;
                delay(100);

                //���������
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "CashDay" values (');
                quA.SQL.Add(its(CashN)+',');
                quA.SQL.Add(''''+ds(iCurD)+''',');

                for n:=1 to 24 do
                begin
                  quA.SQL.Add(its(asale[n].ch)+',');
                  quA.SQL.Add(fs(rv(asale[n].su))+',');
                  quA.SQL.Add(fs(rv(asale[n].ma))+',');
                  if asale[n].ch>0 then quA.SQL.Add(fs(rv(asale[n].su/asale[n].ch))+',')
                  else quA.SQL.Add('0,');
                end;

                quA.SQL.Add(its(allpos)+','); //QuantSale
                quA.SQL.Add(its(allcli)+',');//QuantClient
                quA.SQL.Add(fs(rv(allsu))+',');//SumSale
                quA.SQL.Add(fs(rv(allma))+',');//SumMax
                if allcli>0 then quA.SQL.Add(fs(rv(allsu/allcli))+',')
                else quA.SQL.Add('0,');//SumAverage
                quA.SQL.Add(its(Cli20)+',');//Client_20
                quA.SQL.Add(its(Cli50)+',');//Client_50
                quA.SQL.Add(its(Cli100)+',');//Client_100
                quA.SQL.Add(its(Cli200)+',');//Client_200
                quA.SQL.Add(its(Cli500)+',');//Client_500
                quA.SQL.Add(its(Cli1000)+',');//Client_1000
                quA.SQL.Add(its(CliM));//Client_Greater
                quA.SQL.Add(')');

{                fmSt.Memo1.Lines.Add('');
                for n:=0 to quA.SQL.Count-1 do fmSt.Memo1.Lines.Add(quA.SQL.Strings[n]);
                fmSt.Memo1.Lines.Add('');
}
                quA.ExecSQL;

                //������� ��� ���������
                for n:=1 to 24 do
                begin
                  asale[n].ch:=0;
                  asale[n].su:=0;
                  asale[n].ma:=0;
                end;

                allpos:=0; allcli:=0;
                allsu:=0; allma:=0;
                Cli20:=0;
                Cli50:=0;
                cli100:=0;
                cli200:=0;
                cli500:=0;
                cli1000:=0;
                cliM:=0;

                CashN:=quCQCheckCash_Code.AsInteger;
                fmSt.Memo1.Lines.Add(fmt+'    ����� � '+IntToStr(CashN)); delay(10);
              end;

              iH:=StrToINtDef(FormatDateTime('hh',quCQCheckTimes.AsDateTime),0)+1;
              if quCQCheckOperation.AsString='P' then //�������
              begin
                asale[iH].su:=asale[iH].su+quCQCheckCheckSum.AsFloat;
                allsu:=allsu+quCQCheckCheckSum.AsFloat;
                allpos:=allpos+quCQCheckQuantPos.AsInteger;

                rSumCh:=quCQCheckCheckSum.AsFloat;

                inc(asale[iH].ch);
                inc(allcli);

                if (rSumCh>=0) and (rSumCh<20) then inc(Cli20);
                if (rSumCh>=20) and (rSumCh<50) then inc(Cli50);
                if (rSumCh>=50) and (rSumCh<100) then inc(Cli100);
                if (rSumCh>=100) and (rSumCh<200) then inc(Cli200);
                if (rSumCh>=200) and (rSumCh<500) then inc(Cli500);
                if (rSumCh>=500) and (rSumCh<1000) then inc(Cli1000);
                if (rSumCh>=1000) then inc(CliM);


                if rSumCh>asale[iH].ma then asale[iH].ma:=rSumCh;
                if rSumCh>allma then allma:=rSumCh;

              end else   //�������
              begin
                asale[iH].su:=asale[iH].su+quCQCheckCheckSum.AsFloat;
                allsu:=allsu+quCQCheckCheckSum.AsFloat;
              end;

              quCQCheck.Next;

              inc(iC); if iC mod 100=0 then
              begin
//                fmSt.Memo1.Lines.Add(fmt+'  ..'+INtToStr(iC));
                delay(10);
              end;
            end;

            // ���� ��������� �����

            //������
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "CashDay"' );
            quD.SQL.Add('where DateSale='''+ds(iCurD)+'''');
            quD.SQL.Add('and Number='+its(CashN));
            quD.ExecSQL;
            delay(100);

            //���������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "CashDay" values (');
            quA.SQL.Add(its(CashN)+',');
            quA.SQL.Add(''''+ds(iCurD)+''',');

            for n:=1 to 24 do
            begin
              quA.SQL.Add(its(asale[n].ch)+',');
              quA.SQL.Add(fs(rv(asale[n].su))+',');
              quA.SQL.Add(fs(rv(asale[n].ma))+',');
              if asale[n].ch>0 then quA.SQL.Add(fs(rv(asale[n].su/asale[n].ch))+',')
              else quA.SQL.Add('0,');
            end;

            quA.SQL.Add(its(allpos)+','); //QuantSale
            quA.SQL.Add(its(allcli)+',');//QuantClient
            quA.SQL.Add(fs(rv(allsu))+',');//SumSale
            quA.SQL.Add(fs(rv(allma))+',');//SumMax
            if allcli>0 then quA.SQL.Add(fs(rv(allsu/allcli))+',')
            else quA.SQL.Add('0,');//SumAverage
            quA.SQL.Add(its(Cli20)+',');//Client_20
            quA.SQL.Add(its(Cli50)+',');//Client_50
            quA.SQL.Add(its(Cli100)+',');//Client_100
            quA.SQL.Add(its(Cli200)+',');//Client_200
            quA.SQL.Add(its(Cli500)+',');//Client_500
            quA.SQL.Add(its(Cli1000)+',');//Client_1000
            quA.SQL.Add(its(CliM));//Client_Greater
            quA.SQL.Add(')');
            quA.ExecSQL; delay(100);

            quCQCheck.Active:=False;

            fmSt.Memo1.Lines.Add(fmt+'  ��'); delay(10);
          end;
        except;
        end;
      finally
        fmSt.Memo1.Lines.Add(fmt+'�������� �������.'); delay(10);
        fmSt.cxButton1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TfmCashRep.ViewCashRepDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
//  prCalcSumNac(3,4,5,ViewCashRep);
  prCalcSumAv(1,8,11,ViewCashRep);
  prCalcDefGroupAv(1,8,11,-1,ViewCashRep);
end;

procedure TfmCashRep.RecalcCountCliExecute(Sender: TObject);
Var iMaxCli:INteger;
begin
  //�������� ����� �����������
  with dmMc do
  with dmMt do
  begin
    quCashRep.First;
    while not quCashRep.Eof do
    begin
      quCQCount.SQL.Clear;
      quCQCount.SQL.Add('select (max(Ck_Number)) as MaxCh from "cq'+FormatDateTime('yyyymm',quCashRepZDate.AsDateTime)+'"');
      quCQCount.SQL.Add('where');
      quCQCount.SQL.Add('DateOperation='''+ds(quCashRepZDate.AsDateTime)+'''');
      quCQCount.SQL.Add('and Cash_Code='+its(quCashRepCashNumber.AsInteger));
      quCQCount.SQL.Add('and NSmena='+its(quCashRepZNumber.AsInteger));
      quCQCount.SQL.Add('and (Ck_Number<1000 or Ck_Number>1020)');
      quCQCount.Active:=True;
      quCQCount.First;

      iMaxCli:=0;

      if quCQCount.RecordCount>0 then iMaxCli:=quCQCountMaxCh.AsInteger;

      if ptZRep.Active=False then ptZRep.Active:=True;
      ptZRep.CancelRange;
      ptZRep.SetRange([quCashRepZDate.AsDateTime,quCashRepCashNumber.AsInteger],[quCashRepZDate.AsDateTime,quCashRepCashNumber.AsInteger]);
      ptZRep.First;
      while not ptZRep.Eof do
      begin
        if ptZRepZNumber.AsInteger=quCashRepZNumber.AsInteger then
        begin
          ptZRep.Edit;
          ptZRepCurMoney.AsFloat:=iMaxCli;
          ptZRep.Post;
        end;
        ptZRep.Next;
      end;

      quCashRep.Next;
    end;

    fmCashRep.ViewCashRep.BeginUpdate;
    quCashRep.Active:=False;
    quCashRep.Active:=True;
    fmCashRep.ViewCashRep.EndUpdate;

  end;
end;

procedure TfmCashRep.acRecalcZExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrTa:String;
    dCurD:TDateTime;
    rSumN,rSumBn,rSumDN,rSumDbn:Real;
    rSumRetN,rSumRetBn,rSumRetDN,rSumRetDBN:Real;
    nCash,NumSm,CountCh:INteger;
begin
//�������� Z -��
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMC do
    with dmMT do
    begin
      DateB:=Trunc(CommonSet.DateBeg);
      DateE:=Trunc(CommonSet.DateEnd);
      for iCurD:=DateB to DateE do
      begin
        StrTa:='cq'+formatDateTime('yyyymm',iCurD);
        dCurD:=iCurD;

        ViewCashRep.BeginUpdate;

        ptZRep.Active:=False;
        ptZRep.Active:=True;
        ptZRep.CancelRange;
        ptZRep.SetRange([dCurD],[dCurD]);
        ptZRep.First;
        while not ptZRep.Eof do ptZRep.Delete; //���� �������

        //��� �����
        quZCQ.Active:=False;
        quZCQ.SQL.Clear;


{select Cash_Code, NSmena, Operation, SUM(Summa) as RSUM, SUM(Ck_Disc) as DSUM  from "cq201104"
where DateOperation='2011-04-04'
group by Cash_Code, NSmena, Operation
order by Cash_Code, NSmena, Operation
}
        quZCQ.SQL.Add('select Cash_Code, NSmena, Operation,Ck_Card, SUM(Summa) as RSUM, SUM(Ck_Disc) as DSUM, Count(*) as CountCh  from  "'+StrTa+'"');
        quZCQ.SQL.Add('where DateOperation='''+ds(iCurD)+'''');
        quZCQ.SQL.Add('group by Cash_Code, NSmena, Operation, Ck_Card');
        quZCQ.SQL.Add('order by Cash_Code, NSmena, Operation');
        quZCQ.Active:=True;

        for nCash:=1 to 30 do
        begin
          rSumN:=0; rSumBn:=0; rSumDN:=0; rSumDbn:=0;
          rSumRetN:=0; rSumRetBn:=0; rSumRetDN:=0; rSumRetDBN:=0;
          NumSm:=0; CountCh:=0;

          quZCQ.First;
          while not quZCQ.Eof do
          begin
            if quZCQCash_Code.AsInteger=nCash then
            begin
              if NumSm=0 then NumSm:=quZCQNSmena.AsInteger;
              if NumSm=quZCQNSmena.AsInteger then
              begin //����� ���� �������� ��������
                if quZCQOperation.AsString ='P' then //�������
                begin
                  if quZCQCk_Card.AsInteger=0 then //���
                  begin
                    rSumN:=rSumN+rv(quZCQRSUM.AsFloat);
                    rSumDN:=rSumDN+rv(quZCQDSUM.AsFloat);
                    CountCh:=CountCh+quZCQCountCh.AsInteger;
                  end else //������
                  begin
                    rSumBn:=rSumBn+rv(quZCQRSUM.AsFloat);
                    rSumDBn:=rSumDBn+rv(quZCQDSUM.AsFloat);
                    CountCh:=CountCh+quZCQCountCh.AsInteger;
                  end;
                end else //��������
                begin
                  if quZCQCk_Card.AsInteger=0 then //���
                  begin
                    rSumRetN:=rSumRetN+rv(quZCQRSUM.AsFloat);
                    rSumRetDN:=rSumRetDN+rv(quZCQDSUM.AsFloat);
                  end else //������
                  begin
                    rSumRetBn:=rSumRetBn+rv(quZCQRSUM.AsFloat);
                    rSumRetDBn:=rSumRetDBn+rv(quZCQDSUM.AsFloat);
                  end;
                end;
              end else //����� ���� ����� ���������
              begin
                //����� ����������, �������� �������� � �������
                if (rSumN<>0) or (rSumBn<>0) or (rSumDN<>0) or (rSumDBn<>0)or(rSumRetN<>0) or (rSumRetBn<>0) or (rSumRetDN<>0) or (rSumRetDBn<>0) then
                begin //����� ������
                  quA.SQL.Clear;
                  quA.SQL.Add('INSERT into "Zreport" values (');
                  quA.SQL.Add(its(nCash)+',');//CashNumber
                  quA.SQL.Add(its(NumSm)+',');//ZNumber
                  quA.SQL.Add(fs(rSumN)+',');//ZSale
                  quA.SQL.Add(fs(rSumRetN*(-1))+',');//ZReturn
                  quA.SQL.Add(fs(rSumDN-rSumRetDN)+',');//ZDiscount  //������������� ������ ����� ������ � ��������� - ��� ��� >0
                  quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
                  quA.SQL.Add('''00:00'',');//ZTime
                  quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
                  quA.SQL.Add(fs(rSumRetBn*(-1))+',');//ZCrReturn
                  quA.SQL.Add(fs(rSumDBn-rSumRetDBn)+',');//ZCrDiscount
                  quA.SQL.Add('0,');//TSale
                  quA.SQL.Add('0,');//TReturn
                  quA.SQL.Add('0,');//CrSale
                  quA.SQL.Add('0,');//CrReturn
                  quA.SQL.Add(its(CountCh));//CurMoney
                  quA.SQL.Add(')');
                  quA.ExecSQL;
                end;
                rSumN:=0; rSumBn:=0; rSumDN:=0; rSumDbn:=0;
                rSumRetN:=0; rSumRetBn:=0; rSumRetDN:=0; rSumRetDBN:=0;
                CountCh:=0;

                NumSm:=quZCQNSmena.AsInteger;
              end;
            end;
            quZCQ.Next;
          end;

          //����� ����������, �������� �������� � �������
          if (rSumN<>0) or (rSumBn<>0) or (rSumDN<>0) or (rSumDBn<>0)or(rSumRetN<>0) or (rSumRetBn<>0) or (rSumRetDN<>0) or (rSumRetDBn<>0) then
          begin //����� ������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "Zreport" values (');
            quA.SQL.Add(its(nCash)+',');//CashNumber
            quA.SQL.Add(its(NumSm)+',');//ZNumber
            quA.SQL.Add(fs(rSumN)+',');//ZSale
            quA.SQL.Add(fs(rSumRetN*(-1))+',');//ZReturn
            quA.SQL.Add(fs(rSumDN-rSumRetDN)+',');//ZDiscount  //������������� ������ ����� ������ � ��������� - ��� ��� >0
            quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
            quA.SQL.Add('''00:00'',');//ZTime
            quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
            quA.SQL.Add(fs(rSumRetBn*(-1))+',');//ZCrReturn
            quA.SQL.Add(fs(rSumDBn-rSumRetDBn)+',');//ZCrDiscount
            quA.SQL.Add('0,');//TSale
            quA.SQL.Add('0,');//TReturn
            quA.SQL.Add('0,');//CrSale
            quA.SQL.Add('0,');//CrReturn
            quA.SQL.Add(its(CountCh));//CurMoney
            quA.SQL.Add(')');
            quA.ExecSQL;
          end;


        end;

        quZCQ.Active:=False;

        ViewCashRep.EndUpdate;
      end;
      showmessage('�������� ��������.');
    end;
  end;
end;

procedure TfmCashRep.acMoveZExecute(Sender: TObject);
Var iCashNum,iZNum:INteger;
    dZDate,dZNewDate:TDateTime;
    StrTa,StrWk:String;
begin
  //��������� Z-����� �� ����
  with dmMC do
  begin
    if ViewCashRep.Controller.SelectedRecordCount>0 then
    begin
      iCashNum:=quCashRepCashNumber.AsInteger;
      iZNum:=quCashRepZNumber.AsInteger;
      dZDate:=quCashRepZDate.AsDateTime;
      dZNewDate:=0;
      try
        fmSetNewDate:=TfmSetNewDate.Create(Application);

        fmSetNewDate.cxDateEdit1.Date:=dZDate;
        fmSetNewDate.Label1.Caption:='��������� ����� '+its(iZNum)+' ����� '+its(iCashNum)+' �� ';

        fmSetNewDate.ShowModal;
        if fmSetNewDate.ModalResult=mrOk then
        begin
          dZNewDate:=fmSetNewDate.cxDateEdit1.Date;
        end;
      finally
        fmSetNewDate.Release;
      end;

      if (dZNewDate>0)and(dZNewDate<>dZDate) then
      begin
        if MessageDlg('��������� ����� - '+its(iZNum)+' �� ����� - '+its(iCashNum)+' �� ���� '+ds1(dZNewDate)+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          try
            StrTa:='cq'+formatDateTime('yyyymm',dZNewDate);

            StrWk:=formatDateTime('mm',dZNewDate);
            if StrWk='10' then StrWk:='a0';
            if StrWk='11' then StrWk:='b0';
            if StrWk='12' then StrWk:='c0';

            StrWk:=formatDateTime('yyyy',dZNewDate)+StrWk;

            if (Fileexists(Btr1Path+'\POSDATA\'+'cq'+StrWk+'.btr')=True)
            or (Fileexists(Btr1Path+'\POSDATA\'+'cq'+StrWk+'.mkd')=True)
            or (Fileexists(Btr1Path+'\POSDATA\'+StrTa+'.mkd')=True)
            then
            begin //���� ������ �����
              //������ ����  �� ���� ����

              quD.Active:=False;
              quD.SQL.Clear;
              quD.SQL.Add('Delete from "'+StrTa+'"' );
              quD.SQL.Add('where  DateOperation ='''+ds(dZNewDate)+'''');
              quD.SQL.Add('and Cash_Code ='+its(iCashNum));
              quD.SQL.Add('and NSmena ='+its(iZNum));
              quD.ExecSQL;

              StrTa:='cq'+formatDateTime('yyyymm',dZDate);

              //����������� ����
              quCQZDay.Active:=False;
              quCQZDay.SQL.Clear;
              quCQZDay.SQL.Add('select');
              quCQZDay.SQL.Add('Quant,Operation,DateOperation,Price,Store,Ck_Number,Ck_Curs,');
              quCQZDay.SQL.Add('Ck_CurAbr,Ck_Disg,Ck_Disc,Quant_S,GrCode,Code,Cassir,Cash_Code,');
              quCQZDay.SQL.Add('Ck_Card,Contr_Code,Contr_Cost,Seria,BestB,NDSx1,');
              quCQZDay.SQL.Add('NDSx2,Times,Summa,SumNDS,SumNSP,PriceNSP,NSmena');
              quCQZDay.SQL.Add('from "'+StrTa+'"');
              quCQZDay.SQL.Add('where');
              quCQZDay.SQL.Add('DateOperation='''+ds(dZDate)+'''');
              quCQZDay.SQL.Add('and Cash_Code='+its(iCashNum));
              quCQZDay.SQL.Add('and Nsmena='+its(iZNum));
              quCQZDay.Active:=True;


              StrTa:='cq'+formatDateTime('yyyymm',dZNewDate);

              quCQZDay.First;
              while not quCQZDay.Eof do
              begin
                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "'+StrTa+'" values (');
                quA.SQL.Add(fs(quCQZDayQuant.AsFloat)+',');//Quant
                quA.SQL.Add(''''+quCQZDayOperation.AsString+''',');//Operation
                quA.SQL.Add(''''+ds(dZNewDate)+''',');//DateOperation
                quA.SQL.Add(fs(quCQZDayPrice.AsFloat)+',');//Price
                quA.SQL.Add(''''',');//Store
                quA.SQL.Add(its(quCQZDayCk_Number.AsInteger)+',');//Ck_Number
                quA.SQL.Add('0,');//Ck_Curs
                quA.SQL.Add('0,');//Ck_CurAbr
                quA.SQL.Add(fs(quCQZDayCk_Disg.AsFloat)+',');//Ck_Disg  % ������
                quA.SQL.Add(fs(quCQZDayCk_Disc.AsFloat)+',');//Ck_Disc
                quA.SQL.Add('0,');//Quant_S
                quA.SQL.Add(its(quCQZDayGrCode.AsInteger)+',');//GrCode  ����� - �� ��������� ��������
                quA.SQL.Add(its(quCQZDayCode.AsInteger)+',');//Code
                quA.SQL.Add(''''+quCQZDayCassir.AsString+''',');//Cassir
                quA.SQL.Add(its(quCQZDayCash_Code.AsInteger)+',');//Cash_Code
                quA.SQL.Add(its(quCQZDayCk_Card.AsInteger)+','); //Ck_Card
                quA.SQL.Add(''''',');//Contr_Code
                quA.SQL.Add('0,');//Contr_Cost
                quA.SQL.Add(''''',');//Seria
                quA.SQL.Add(''''',');//BestB
                quA.SQL.Add('0,');//NDSx1
                quA.SQL.Add('0,');//NDSx2

                StrWk:=FormatDateTime('hh:nn',quCQZDayTimes.AsDateTime);
                quA.SQL.Add(''''+StrWk+''',');//Times
                quA.SQL.Add(fs(quCQZDaySumma.AsFloat)+',');//Summa
                quA.SQL.Add('0,');//SumNDS
                quA.SQL.Add('0,');//SumNSP
                quA.SQL.Add('0,');//PriceNSP
                quA.SQL.Add(its(quCQZDayNSmena.AsInteger));//NSmena
                quA.SQL.Add(')');
                quA.ExecSQL;

                quCQZDay.Next;
              end;
              quCQZDay.Active:=False;

              //������ ����  �� ���� ������
              StrTa:='cq'+formatDateTime('yyyymm',dZDate);

              quD.Active:=False;
              quD.SQL.Clear;
              quD.SQL.Add('Delete from "'+StrTa+'"' );
              quD.SQL.Add('where  DateOperation ='''+ds(dZDate)+'''');
              quD.SQL.Add('and Cash_Code ='+its(iCashNum));
              quD.SQL.Add('and NSmena ='+its(iZNum));
              quD.ExecSQL;

            end;

            //������� ���������

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "Zreport"' );
            quE.SQL.Add('Set "Zdate" = '''+ds(dZNewDate)+'''');
            quE.SQL.Add('where  "Zdate" ='''+ds(dZDate)+'''');
            quE.SQL.Add('and CashNumber ='+its(iCashNum));
            quE.SQL.Add('and Znumber ='+its(iZNum));
            quE.ExecSQL;

            fmCashRep.ViewCashRep.BeginUpdate;

            quCashRep.Active:=False;
            quCashRep.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
            quCashRep.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
            quCashRep.Active:=True;

            fmCashRep.ViewCashRep.EndUpdate;


            ShowMessage('������� ��. �� �������� ��������������� ���������� �� ������� � ��.');
          except
            ShowMessage('�������� ������ ��� ��������.');
          end;
        end;
      end;
    end else
      ShowMessage('�������� ����� ��� ��������.');
  end;
end;

procedure TfmCashRep.acDelCashZExecute(Sender: TObject);
Var iCashNum,iZNum:INteger;
    dZDate:TDateTime;
    StrTa,StrWk:String;
begin
//������� Z-�����
  with dmMC do
  begin
    if ViewCashRep.Controller.SelectedRecordCount>0 then
    begin
      iCashNum:=quCashRepCashNumber.AsInteger;
      iZNum:=quCashRepZNumber.AsInteger;
      dZDate:=quCashRepZDate.AsDateTime;

      if MessageDlg('������� ����� - '+its(iZNum)+' �� ����� - '+its(iCashNum)+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          //������ ����

          StrTa:='cq'+formatDateTime('yyyymm',dZDate);
          StrWk:=formatDateTime('yyyymm',dZDate);

          if CommonSet.PathCryst[length(CommonSet.PathCryst)]<>'\' then CommonSet.PathCryst:=CommonSet.PathCryst+'\';

          quD.Active:=False;
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "'+StrTa+'"' );
          quD.SQL.Add('where  DateOperation ='''+ds(dZDate)+'''');
          quD.SQL.Add('and Cash_Code ='+its(iCashNum));
          quD.SQL.Add('and NSmena ='+its(iZNum));
          quD.ExecSQL;

          //������ ���������

          quD.Active:=False;
          quD.SQL.Clear;
          quD.SQL.Add('delete from "Zreport"' );
          quD.SQL.Add('where  "Zdate" ='''+ds(dZDate)+'''');
          quD.SQL.Add('and CashNumber ='+its(iCashNum));
          quD.SQL.Add('and Znumber ='+its(iZNum));
          quD.ExecSQL;

          fmCashRep.ViewCashRep.BeginUpdate;

          quCashRep.Active:=False;
          quCashRep.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
          quCashRep.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
          quCashRep.Active:=True;

          fmCashRep.ViewCashRep.EndUpdate;

          ShowMessage('�������� ��. �� �������� ��������������� ���������� �� ������� � ��.');
        except
          ShowMessage('�������� ������ ��� ��������.');
        end;
      end;
    end else
      ShowMessage('�������� ����� ��� ��������.');
  end;
end;

procedure TfmCashRep.FormShow(Sender: TObject);
begin
  if Pos('����',Person.Name)=0 then
  begin
    acDelCashZ.Enabled:=False;
    acMoveZ.Enabled:=False;
  end else
  begin
    acDelCashZ.Enabled:=True;
    acMoveZ.Enabled:=True;
  end;
end;

procedure TfmCashRep.acMoveCheckExecute(Sender: TObject);
Var iCashNum,iZNum:INteger;
    dZDate,dZNewDate:TDateTime;
    StrTa,StrWk:String;
    bTrans:Boolean;
begin
  //��������� Z-����� �� ����
  with dmMC do
  begin
    if ViewCashRep.Controller.SelectedRecordCount>0 then
    begin
      iCashNum:=quCashRepCashNumber.AsInteger;
      iZNum:=quCashRepZNumber.AsInteger;
      dZDate:=quCashRepZDate.AsDateTime;
      dZNewDate:=0;
      try
        fmSetNewDate:=TfmSetNewDate.Create(Application);

        fmSetNewDate.cxDateEdit1.Date:=dZDate;
        fmSetNewDate.Label1.Caption:='��������� ���� ����� '+its(iZNum)+' ����� '+its(iCashNum)+' �� ';

        fmSetNewDate.ShowModal;
        if fmSetNewDate.ModalResult=mrOk then
        begin
          dZNewDate:=fmSetNewDate.cxDateEdit1.Date;
        end;
      finally
        fmSetNewDate.Release;
      end;

      if (dZNewDate>0)and(dZNewDate<>dZDate) then
      begin
        if MessageDlg('��������� ���� ����� - '+its(iZNum)+' �� ����� - '+its(iCashNum)+' �� ���� '+ds1(dZNewDate)+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          try
            StrTa:='cq'+formatDateTime('yyyymm',dZNewDate);

            StrWk:=formatDateTime('mm',dZNewDate);
            if StrWk='10' then StrWk:='a0';
            if StrWk='11' then StrWk:='b0';
            if StrWk='12' then StrWk:='c0';

            StrWk:=formatDateTime('yyyy',dZNewDate)+StrWk;

            if (Fileexists(Btr1Path+'\POSDATA\'+'cq'+StrWk+'.btr')=True)
            or (Fileexists(Btr1Path+'\POSDATA\'+'cq'+StrWk+'.mkd')=True)
            or (Fileexists(Btr1Path+'\POSDATA\'+StrTa+'.mkd')=True)
            then  bTrans:=True
            else
            begin
              if MessageDlg('���� ����� �� ������. ('+Btr1Path+'\POSDATA\'+StrTa+'.mkd'+') ���������� �������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
                bTrans:=True
              else
                bTrans:=False;
            end;

            if bTrans then
            begin //���� ������ �����
              //�� ������ ����  �� ���� ����

              StrTa:='cq'+formatDateTime('yyyymm',dZDate);

              //����������� ����
              quCQZDay.Active:=False;
              quCQZDay.SQL.Clear;
              quCQZDay.SQL.Add('select');
              quCQZDay.SQL.Add('Quant,Operation,DateOperation,Price,Store,Ck_Number,Ck_Curs,');
              quCQZDay.SQL.Add('Ck_CurAbr,Ck_Disg,Ck_Disc,Quant_S,GrCode,Code,Cassir,Cash_Code,');
              quCQZDay.SQL.Add('Ck_Card,Contr_Code,Contr_Cost,Seria,BestB,NDSx1,');
              quCQZDay.SQL.Add('NDSx2,Times,Summa,SumNDS,SumNSP,PriceNSP,NSmena');
              quCQZDay.SQL.Add('from "'+StrTa+'"');
              quCQZDay.SQL.Add('where');
              quCQZDay.SQL.Add('DateOperation='''+ds(dZDate)+'''');
              quCQZDay.SQL.Add('and Cash_Code='+its(iCashNum));
              quCQZDay.SQL.Add('and Nsmena='+its(iZNum));
              quCQZDay.Active:=True;


              StrTa:='cq'+formatDateTime('yyyymm',dZNewDate);

              quCQZDay.First;
              while not quCQZDay.Eof do
              begin
                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "'+StrTa+'" values (');
                quA.SQL.Add(fs(quCQZDayQuant.AsFloat)+',');//Quant
                quA.SQL.Add(''''+quCQZDayOperation.AsString+''',');//Operation
                quA.SQL.Add(''''+ds(dZNewDate)+''',');//DateOperation
                quA.SQL.Add(fs(quCQZDayPrice.AsFloat)+',');//Price
                quA.SQL.Add(''''',');//Store
                quA.SQL.Add(its(quCQZDayCk_Number.AsInteger)+',');//Ck_Number
                quA.SQL.Add('0,');//Ck_Curs
                quA.SQL.Add('0,');//Ck_CurAbr
                quA.SQL.Add(fs(quCQZDayCk_Disg.AsFloat)+',');//Ck_Disg  % ������
                quA.SQL.Add(fs(quCQZDayCk_Disc.AsFloat)+',');//Ck_Disc
                quA.SQL.Add('0,');//Quant_S
                quA.SQL.Add(its(quCQZDayGrCode.AsInteger)+',');//GrCode  ����� - �� ��������� ��������
                quA.SQL.Add(its(quCQZDayCode.AsInteger)+',');//Code
                quA.SQL.Add(''''+quCQZDayCassir.AsString+''',');//Cassir
                quA.SQL.Add(its(quCQZDayCash_Code.AsInteger)+',');//Cash_Code
                quA.SQL.Add(its(quCQZDayCk_Card.AsInteger)+','); //Ck_Card
                quA.SQL.Add(''''',');//Contr_Code
                quA.SQL.Add('0,');//Contr_Cost
                quA.SQL.Add(''''',');//Seria
                quA.SQL.Add(''''',');//BestB
                quA.SQL.Add('0,');//NDSx1
                quA.SQL.Add('0,');//NDSx2

                StrWk:=FormatDateTime('hh:nn',quCQZDayTimes.AsDateTime);
                quA.SQL.Add(''''+StrWk+''',');//Times
                quA.SQL.Add(fs(quCQZDaySumma.AsFloat)+',');//Summa
                quA.SQL.Add('0,');//SumNDS
                quA.SQL.Add('0,');//SumNSP
                quA.SQL.Add('0,');//PriceNSP
                quA.SQL.Add(its(quCQZDayNSmena.AsInteger));//NSmena
                quA.SQL.Add(')');
                quA.ExecSQL;

                quCQZDay.Next;
              end;
              quCQZDay.Active:=False;

              //������ ����  �� ���� ������
              StrTa:='cq'+formatDateTime('yyyymm',dZDate);

              quD.Active:=False;
              quD.SQL.Clear;
              quD.SQL.Add('Delete from "'+StrTa+'"' );
              quD.SQL.Add('where  DateOperation ='''+ds(dZDate)+'''');
              quD.SQL.Add('and Cash_Code ='+its(iCashNum));
              quD.SQL.Add('and NSmena ='+its(iZNum));
              quD.ExecSQL;

              fmCashRep.ViewCashRep.BeginUpdate;

              quCashRep.Active:=False;
              quCashRep.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
              quCashRep.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
              quCashRep.Active:=True;

              fmCashRep.ViewCashRep.EndUpdate;

              ShowMessage('������� ��.');

            end;
          except
            ShowMessage('�������� ������ ��� ��������.');
          end;
        end;
      end;
    end else
      ShowMessage('�������� ����� ��� ��������.');
  end;
end;

end.
