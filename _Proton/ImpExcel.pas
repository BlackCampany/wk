unit ImpExcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, StdCtrls, cxButtons, ExtCtrls,
  cxButtonEdit;

type
  TfmImpExcel = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label2: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    OpenDialog1: TOpenDialog;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmImpExcel: TfmImpExcel;

implementation

{$R *.dfm}

procedure TfmImpExcel.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit1.Text:=OpenDialog1.fileName;
end;

end.
