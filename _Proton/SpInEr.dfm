object fmSpInEr: TfmSpInEr
  Left = 223
  Top = 233
  Width = 892
  Height = 299
  Caption = #1054#1096#1080#1073#1082#1080' '#1087#1088#1080' '#1086#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085#1080#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrSpEr: TcxGrid
    Left = 0
    Top = 0
    Width = 884
    Height = 225
    Align = alClient
    TabOrder = 0
    object ViewSpEr: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaSpEr
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.Selection = dmMC.cxStyle24
      object ViewSpErNum: TcxGridDBColumn
        Caption = #1057#1090#1088#1086#1082#1072
        DataBinding.FieldName = 'Num'
        Width = 47
      end
      object ViewSpErCodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
      end
      object ViewSpErNameCode: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'NameCode'
        Width = 189
      end
      object ViewSpErTender: TcxGridDBColumn
        Caption = #1058#1077#1085#1076#1077#1088
        DataBinding.FieldName = 'Tender'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        Width = 46
      end
      object ViewSpErCodeEdIzm: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'CodeEdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1064#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1050#1075'.'
            Value = 2
          end>
        Width = 34
      end
      object ViewSpErkolZ: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1069'-'#1085#1072#1082#1083'.'
        DataBinding.FieldName = 'kolZ'
        Width = 85
      end
      object ViewSpErkolSp: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1074' '#1087#1088#1080#1093#1086#1076#1077
        DataBinding.FieldName = 'kolSp'
        Width = 75
      end
      object ViewSpErkolRaz: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072' '#1087#1086' '#1082#1086#1083'-'#1074#1091
        DataBinding.FieldName = 'kolRaz'
        Width = 85
      end
      object ViewSpErCenaZ: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1069'-'#1085#1072#1082#1083'.'
        DataBinding.FieldName = 'CenaZ'
        Width = 75
      end
      object ViewSpErCenaSp: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1074' '#1087#1088#1080#1093#1086#1076#1077
        DataBinding.FieldName = 'CenaSp'
        Width = 75
      end
      object ViewSpErCenaRaz: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072' '#1087#1086' '#1094#1077#1085#1077
        DataBinding.FieldName = 'CenaRaz'
        Styles.Content = dmMC.cxStyle5
        Width = 90
      end
    end
    object LevSpEr: TcxGridLevel
      GridView = ViewSpEr
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 225
    Width = 884
    Height = 41
    Align = alBottom
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 248
      Top = 8
      Width = 113
      Height = 25
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 520
      Top = 8
      Width = 113
      Height = 25
      Hint = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object taSpEr: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 180
    Top = 64
    object taSpErNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpErCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object taSpErNameCode: TStringField
      FieldName = 'NameCode'
      Size = 50
    end
    object taSpErTender: TIntegerField
      FieldName = 'Tender'
    end
    object taSpErCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object taSpErkolZ: TFloatField
      FieldName = 'kolZ'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpErkolSp: TFloatField
      FieldName = 'kolSp'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpErkolRaz: TFloatField
      FieldName = 'kolRaz'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpErCenaZ: TFloatField
      FieldName = 'CenaZ'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSpErCenaSp: TFloatField
      FieldName = 'CenaSp'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSpErCenaRaz: TFloatField
      FieldName = 'CenaRaz'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
  end
  object dstaSpEr: TDataSource
    DataSet = taSpEr
    Left = 180
    Top = 124
  end
end
