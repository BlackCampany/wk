unit AddDoc4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxCheckBox,
  FR_Class, FR_DSet, FR_DBSet, cxRadioGroup, dxmdaset;

type
  TfmAddDoc4 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label5: TLabel;
    Label12: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpecVnNoNeed: TClientDataSet;
    dstaSpecVn: TDataSource;
    cxLabel3: TcxLabel;
    amDocVn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    taSpecVnNoNeedCodeTovar: TIntegerField;
    taSpecVnNoNeedCodeEdIzm: TSmallintField;
    taSpecVnNoNeedBarCode: TStringField;
    taSpecVnNoNeedNDSProc: TFloatField;
    taSpecVnNoNeedKol: TFloatField;
    taSpecVnNoNeedNum: TIntegerField;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc4: TcxGrid;
    ViewDoc4: TcxGridDBTableView;
    LevelDoc4: TcxGridLevel;
    acSetPrice: TAction;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    frRepDVn: TfrReport;
    frtaSpecVn: TfrDBDataSet;
    acPrintDoc: TAction;
    taSpecVnNoNeedName: TStringField;
    taSpecVnNoNeedCena: TFloatField;
    taSpecVnNoNeedSumCena: TFloatField;
    taSpecVnNoNeedSumNewCena: TFloatField;
    taSpecVnNoNeedSumRazn: TFloatField;
    taSpecVnNoNeedFullName: TStringField;
    Label2: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    taSpecVnNoNeedKolMest: TFloatField;
    taSpecVnNoNeedKolWithMest: TFloatField;
    taSpecVnNoNeedNewCena: TFloatField;
    taSpecVnNoNeedProc: TFloatField;
    ViewDoc4Num: TcxGridDBColumn;
    ViewDoc4CodeTovar: TcxGridDBColumn;
    ViewDoc4Name: TcxGridDBColumn;
    ViewDoc4FullName: TcxGridDBColumn;
    ViewDoc4CodeEdIzm: TcxGridDBColumn;
    ViewDoc4BarCode: TcxGridDBColumn;
    ViewDoc4QuantM: TcxGridDBColumn;
    ViewDoc4Kol: TcxGridDBColumn;
    ViewDoc4Cena: TcxGridDBColumn;
    ViewDoc4NewCena: TcxGridDBColumn;
    ViewDoc4SumCena: TcxGridDBColumn;
    ViewDoc4SumNewCena: TcxGridDBColumn;
    ViewDoc4SumRazn: TcxGridDBColumn;
    cxButton8: TcxButton;
    acPrintTermo: TAction;
    acPrintCenn: TAction;
    taSpecVnNoNeedRemn: TFloatField;
    ViewDoc4Remn: TcxGridDBColumn;
    acSetRemn: TAction;
    acPostav: TAction;
    cxLabel6: TcxLabel;
    GroupBox1: TGroupBox;
    teSpecIn: TdxMemData;
    dsteSpecIn: TDataSource;
    teSpecInNum: TIntegerField;
    teSpecInID: TIntegerField;
    teSpecInName: TStringField;
    teSpecInFullName: TStringField;
    teSpecInCdeEdIzm: TIntegerField;
    teSpecInBarCode: TStringField;
    teSpecInNDSProc: TFloatField;
    teSpecInKol: TFloatField;
    teSpecInCena: TFloatField;
    teSpecInNewCena: TFloatField;
    teSpecInCenaSS: TFloatField;
    teSpecInProc: TFloatField;
    teSpecInSumCena: TFloatField;
    teSpecInSumNewCena: TFloatField;
    teSpecInSumCenaSS: TFloatField;
    teSpecInRemn: TFloatField;
    taSpecVnNoNeedCenaSS: TFloatField;
    taSpecVnNoNeedSumCenaSS: TFloatField;
    teSpecInCodeTovar: TIntegerField;
    taSpecVnNoNeedId: TIntegerField;
    ViewDoc4Id: TcxGridDBColumn;
    GrPVn: TcxGrid;
    ViewPVn: TcxGridDBTableView;
    ViewPVnARTICUL: TcxGridDBColumn;
    ViewPVnIDATE: TcxGridDBColumn;
    ViewPVnQPART: TcxGridDBColumn;
    ViewPVnQREMN: TcxGridDBColumn;
    ViewPVnPRICEIN: TcxGridDBColumn;
    ViewPVnSSTORE: TcxGridDBColumn;
    ViewPVnSCLI: TcxGridDBColumn;
    ViewPVnID: TcxGridDBColumn;
    ViewPVnIDSTORE: TcxGridDBColumn;
    ViewPVnIDDOC: TcxGridDBColumn;
    ViewPVnDTYPE: TcxGridDBColumn;
    ViewPVnPRICEOUT: TcxGridDBColumn;
    ViewPVnSINNCLI: TcxGridDBColumn;
    LePVn: TcxGridLevel;
    tePartIn1: TdxMemData;
    tePartIn1ID: TIntegerField;
    tePartIn1IDSTORE: TIntegerField;
    tePartIn1SSTORE: TStringField;
    tePartIn1IDATE: TIntegerField;
    tePartIn1IDDOC: TIntegerField;
    tePartIn1ARTICUL: TIntegerField;
    tePartIn1DTYPE: TIntegerField;
    tePartIn1QPART: TFloatField;
    tePartIn1QREMN: TFloatField;
    tePartIn1PRICEIN: TFloatField;
    tePartIn1PRICEOUT: TFloatField;
    tePartIn1SINNCLI: TStringField;
    tePartIn1SCLI: TStringField;
    dstePartIn1: TDataSource;
    Label3: TLabel;
    PopupMenu1: TPopupMenu;
    acAddPosIn: TAction;
    N1: TMenuItem;
    taSpecVn: TdxMemData;
    taSpecVnNum: TIntegerField;
    taSpecVnCodeTovar: TIntegerField;
    taSpecVnName: TStringField;
    taSpecVnFullName: TStringField;
    taSpecVnCodeEdIzm: TIntegerField;
    taSpecVnBarCode: TStringField;
    taSpecVnNDSProc: TFloatField;
    taSpecVnKol: TFloatField;
    taSpecVnCena: TFloatField;
    taSpecVnNewCena: TFloatField;
    taSpecVnProc: TFloatField;
    taSpecVnSumCena: TFloatField;
    taSpecVnSumNewCena: TFloatField;
    taSpecVnSumRazn: TFloatField;
    taSpecVnRemn: TFloatField;
    taSpecVnCenaSS: TFloatField;
    taSpecVnSumCenaSS: TFloatField;
    taSpecVnId: TIntegerField;
    taSpecPr: TClientDataSet;
    taSpecPrNum: TIntegerField;
    taSpecPrCodeTovar: TIntegerField;
    taSpecPrName: TStringField;
    taSpecPrFullName: TStringField;
    taSpecPrKol: TFloatField;
    taSpecPrCena: TFloatField;
    taSpecPrSumCena: TFloatField;
    taSpecPrNewCena: TFloatField;
    taSpecPrSumNewCena: TFloatField;
    taSpecPrId: TIntegerField;
    taSpecPrEdIzm: TSmallintField;
    GroupBox2: TGroupBox;
    cxLabel5: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    acAddPosOut: TAction;
    N2: TMenuItem;
    taSpecPrNDS: TFloatField;
    taSpecPrNDSSum: TFloatField;
    taSpecVnQuantN: TFloatField;
    taSpecVnProcBrak: TFloatField;
    ViewDoc4QuantN: TcxGridDBColumn;
    ViewDoc4ProcBrak: TcxGridDBColumn;
    acViewPrihod: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    taSpecVnQuantM: TFloatField;
    tePartIn1PRICEIN0: TFloatField;
    ViewPVnPRICEIN0: TcxGridDBColumn;
    taSpecVnCena0: TFloatField;
    taSpecVnSumCena0: TFloatField;
    ViewDoc4Cena0: TcxGridDBColumn;
    ViewDoc4SumCena0: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc4DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc4DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewDoc4Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel3Click(Sender: TObject);
    procedure ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure ViewDoc4DblClick(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure taSpecVnNoNeedCenaChange(Sender: TField);
    procedure taSpecVnNoNeedNewCenaChange(Sender: TField);
    procedure taSpecVnNoNeedSumCenaChange(Sender: TField);
    procedure taSpecVnNoNeedSumNewCenaChange(Sender: TField);
    procedure taSpecVnNoNeedKolMestChange(Sender: TField);
    procedure taSpecVnNoNeedKolWithMestChange(Sender: TField);
    procedure cxButton8Click(Sender: TObject);
    procedure acPrintTermoExecute(Sender: TObject);
    procedure acPrintCennExecute(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure acSetRemnExecute(Sender: TObject);
    procedure acPostavExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure ViewDoc4SelectionChanged(Sender: TcxCustomGridTableView);
    procedure ViewPVnDblClick(Sender: TObject);
    procedure acAddPosInExecute(Sender: TObject);
    procedure ViewDoc4CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure taSpecVnKolMestChange(Sender: TField);
    procedure taSpecVnKolWithMestChange(Sender: TField);
    procedure taSpecVnCenaChange(Sender: TField);
    procedure taSpecVnNewCenaChange(Sender: TField);
    procedure taSpecVnSumCenaChange(Sender: TField);
    procedure taSpecVnSumNewCenaChange(Sender: TField);
    procedure cxLabel10Click(Sender: TObject);
    procedure cxLabel11Click(Sender: TObject);
    procedure acAddPosOutExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure acViewPrihodExecute(Sender: TObject);
    procedure taSpecVnQuantMChange(Sender: TField);
    procedure taSpecVnKolChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc4: TfmAddDoc4;
  bAdd:Boolean = False;
  iTPos:SmallINt;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, DocsIn, sumprops, DocsVn,
  QuantMess, u2fdk, MT, Move, MainMCryst;

{$R *.dfm}

procedure TfmAddDoc4.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc4.Align:=alClient;
  ViewDoc4.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmAddDoc4.cxLabel1Click(Sender: TObject);
begin
  iTPos:=0;
  acAddList.Execute;
end;

procedure TfmAddDoc4.ViewDoc4DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=5 then  Accept:=True;
end;

procedure TfmAddDoc4.ViewDoc4DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    rPr1,rPrm:Real;
begin
//
  if iDirect=5 then
  begin
    iDirect:=0; //������ ���������

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc4.taSpecVn.First;
        if not fmAddDoc4.taSpecVn.Eof then
        begin
          fmAddDoc4.taSpecVn.Last;
          iMax:=fmAddDoc4.taSpecVnNum.AsInteger+1;
        end;

        with dmMC do
        begin
          ViewDoc4.BeginUpdate;
          try

          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            quFindC1.Active:=False;
            quFindC1.SQL.Clear;
            quFindC1.SQL.Add('SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
            quFindC1.SQL.Add('FROM "Goods"');
            quFindC1.SQL.Add('where "Goods"."ID"='+INtToStr(iNum));
            quFindC1.Active:=True;

            if quFindC1.RecordCount=1 then
            begin
              rPr1:=prFLP(quFindC1ID.AsInteger,rPrm);

              taSpecVn.Append;
              taSpecVnNum.AsInteger:=iMax;
              taSpecVnId.AsInteger:=0;
              taSpecVnCodeTovar.AsInteger:=quFindC1ID.AsInteger;
              taSpecVnCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
              taSpecVnBarCode.AsString:=quFindC1BarCode.AsString;
              taSpecVnNDSProc.AsFloat:=quFindC1NDS.AsFloat;
//              taSpecVnKolMest.AsFloat:=1;
//              taSpecVnKolWithMest.AsFloat:=1;
              taSpecVnKol.AsFloat:=1;
              taSpecVnName.AsString:=quFindC1Name.AsString;
              taSpecVnFullName.AsString:=quFindC1FullName.AsString;
              taSpecVnCena.AsFloat:=rPr1;
              taSpecVnNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecVnProc.AsFloat:=0;
              taSpecVnSumCena.AsFloat:=rPr1;
              taSpecVnSumNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecVnSumRazn.AsFloat:=0;
              taSpecVn.Post;

              inc(iMax);
            end;
          end;

          finally
            ViewDoc4.EndUpdate;
          end;  
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.ViewDoc4Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4QuantM' then iCol:=1;  //���-��
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Kol' then iCol:=2;  //���-��
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Cena' then iCol:=3;  //������ ����
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4NewCena' then iCol:=4;  //����� ����
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumCena' then iCol:=5;   //������ �����
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumNewCena' then iCol:=6;   //����� ����� �����
end;

procedure TfmAddDoc4.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc4.SetFocus;
  if fmAddDoc4.Tag=3 then
  begin
    ViewDoc4QuantM.Visible:=True;
    ViewDoc4QuantN.Visible:=True;
    ViewDoc4ProcBrak.Visible:=True;
  end else
  begin
    ViewDoc4QuantM.Visible:=False;
    ViewDoc4QuantN.Visible:=False;
    ViewDoc4ProcBrak.Visible:=False;
  end;
end;

procedure TfmAddDoc4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      bDrVn:=False;
      ViewDoc4.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    bDrVn:=False;
    ViewDoc4.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  end;
end;

procedure TfmAddDoc4.cxLabel3Click(Sender: TObject);
begin
  iTPos:=0;
  acAddPos.Execute;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
//    rK:Real;
//    iM:Integer;
//    Sm:String;

    bAdd:Boolean;
    rPr1,rPrm:Real;
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCode));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          bAdd:=True;
          if quFindCStatus.AsInteger>100 then
          begin
            bAdd:=False;
            if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
          end;
          if bAdd then
          begin
              rPr1:=prFLP(iCode,rPrm);
              taSpecVn.Edit;
              taSpecVnCodeTovar.AsInteger:=iCode;
              taSpecVnCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
              taSpecVnBarCode.AsString:=quFindCBarCode.AsString;
              taSpecVnNDSProc.AsFloat:=quFindCNDS.AsFloat;
              taSpecVnName.AsString:=quFindCName.AsString;
              taSpecVnCena.AsFloat:=rPr1;
              taSpecVnNewCena.AsFloat:=quFindCCena.AsFloat;
              taSpecVnSumCena.AsFloat:=rPr1;
              taSpecVnSumNewCena.AsFloat:=quFindCCena.AsFloat;
              taSpecVnFullName.AsString:=quFindCFullName.AsString;

//              taSpecVnKolMest.AsFloat:=1;
//              taSpecVnKolWithMest.AsFloat:=1;
//              taSpecVnKol.AsFloat:=1;
//              taSpecVnProc.AsFloat:=0;
//              taSpecVnSumRazn.AsFloat:=0;
              taSpecVn.Post;
          end;
          GridDoc4.SetFocus;
          ViewDoc4Kol.Focused:=True;
        end;
      end;
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Name' then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if (sName>'')and(Length(sName)>=3) then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if CountRec1(quFindC) then
            begin
              bAdd:=True;
              if quFindCStatus.AsInteger>100 then
              begin
                bAdd:=False;
                if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
              end;
              if bAdd then
              begin
                  rPr1:=prFLP(quFindCID.AsInteger,rPrm);

                  taSpecVn.Edit;
                  taSpecVnCodeTovar.AsInteger:=quFindCID.AsInteger;
                  taSpecVnCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
                  taSpecVnBarCode.AsString:=quFindCBarCode.AsString;
                  taSpecVnNDSProc.AsFloat:=quFindCNDS.AsFloat;
                  taSpecVnName.AsString:=quFindCName.AsString;
                  taSpecVnCena.AsFloat:=rPr1;
                  taSpecVnNewCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecVnSumCena.AsFloat:=rPr1;
                  taSpecVnSumNewCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecVnFullName.AsString:=quFindCFullName.AsString;
//                  taSpecVnKolMest.AsFloat:=1;
//                  taSpecVnKolWithMest.AsFloat:=1;
//                  taSpecVnKol.AsFloat:=1;
//                  taSpecVnProc.AsFloat:=0;
//                  taSpecVnSumRazn.AsFloat:=0;
                  taSpecVn.Post;
              end;
              GridDoc4.SetFocus;
              ViewDoc4Kol.Focused:=True;
            end;
          end;
        end;
      end;
    end else
      if (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4CodeTovar') or (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4CodeTara') then
        if fTestKey(Key)=False then
          if taSpecVn.State in [dsEdit,dsInsert] then taSpecVn.Cancel;
  end;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var // Km:Real;
    sName:String;
    rPr1,rPrm:Real;
begin
  if bAdd then exit;
  with dmMC do
  begin
    if (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Name') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if (sName>'')and(Length(sName)>=3) then
      begin
        quFindC1.Active:=False;
        quFindC1.SQL.Clear;
        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status", "Goods"."V02"');
        quFindC1.SQL.Add('FROM "Goods"');
        quFindC1.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
        quFindC1.Active:=True;

        if quFindC1.RecordCount=1 then //����� ���� ������
        begin
          rPr1:=prFLP(quFindC1ID.AsInteger,rPrm);

          taSpecVn.Edit;
          taSpecVnCodeTovar.AsInteger:=quFindC1ID.AsInteger;
          taSpecVnCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
          taSpecVnBarCode.AsString:=quFindC1BarCode.AsString;
          taSpecVnNDSProc.AsFloat:=quFindC1NDS.AsFloat;
          taSpecVnName.AsString:=quFindC1Name.AsString;
          taSpecVnFullName.AsString:=quFindC1FullName.AsString;
          taSpecVnNewCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecVnSumCena.AsFloat:=rPr1;
          taSpecVnSumNewCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecVnCena.AsFloat:=rPr1;
//          taSpecVnKolMest.AsFloat:=1;
//          taSpecVnKolWithMest.AsFloat:=1;
//          taSpecVnKol.AsFloat:=1;
//          taSpecVnSumRazn.AsFloat:=0;
//          taSpecVnProc.AsFloat:=0;
          taSpecVn.Post;

          AEdit.SelectAll;
          ViewDoc4Name.Options.Editing:=False;
          ViewDoc4Name.Focused:=True;
          Key:=#0;
        end;//}
      end;
    end;
  end;//}
end;

procedure TfmAddDoc4.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  iMax:=1;
  ViewDoc4.BeginUpdate;

  taSpecVn.First;
  if not taSpecVn.Eof then
  begin
    taSpecVn.Last;
    iMax:=taSpecVnNum.AsInteger+1;
  end;

  taSpecVn.Append;
  taSpecVnNum.AsInteger:=iMax;
  taSpecVnId.AsInteger:=iTPos;
  taSpecVnCodeTovar.AsInteger:=0;
  taSpecVnName.AsString:='';
  taSpecVnFullName.AsString:='';
  taSpecVnCodeEdIzm.AsInteger:=1;
  taSpecVnBarCode.AsString:='';
  if iTPos=0 then
  begin
    taSpecVnNDSProc.AsFloat:=0;
//    taSpecVnKolMest.AsFloat:=1;
//    taSpecVnKolWithMest.AsFloat:=1;
    taSpecVnKol.AsFloat:=1;
    taSpecVnCena.AsFloat:=0;
    taSpecVnNewCena.AsFloat:=0;
    taSpecVnProc.AsFloat:=0;
    taSpecVnSumCena.AsFloat:=0;
    taSpecVnSumNewCena.AsFloat:=0;
    taSpecVnSumRazn.AsFloat:=0;
  end else
  begin
    taSpecVnNDSProc.AsFloat:=0;
//    taSpecVnKolMest.AsFloat:=-1;
//    taSpecVnKolWithMest.AsFloat:=1;
    taSpecVnKol.AsFloat:=-1;
    taSpecVnCena.AsFloat:=0;
    taSpecVnNewCena.AsFloat:=0;
    taSpecVnProc.AsFloat:=0;
    taSpecVnSumCena.AsFloat:=0;
    taSpecVnSumNewCena.AsFloat:=0;
    taSpecVnSumRazn.AsFloat:=0;
  end;

  taSpecVn.Post;

  ViewDoc4.EndUpdate;
  GridDoc4.SetFocus;

  ViewDoc4CodeTovar.Options.Editing:=True;
  ViewDoc4Name.Options.Editing:=True;
  ViewDoc4Name.Focused:=True;
end;

procedure TfmAddDoc4.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if fmAddDoc4.Tag=3 then exit;
  if cxButton1.Enabled then
  begin
    if taSpecVn.RecordCount>0 then
    begin
      taSpecVn.Delete;
    end;  
  end;
end;

procedure TfmAddDoc4.acAddListExecute(Sender: TObject);
begin
  iDirect:=5;
  fmCards.Show;
end;

procedure TfmAddDoc4.cxButton1Click(Sender: TObject);
Var IdH:Int64;
    rSum1,rSum2,rSum10,rSum20,rSum10n,rSum20n:Real;
    iNum:Integer;
    rSumSS,rPrSS,rSumSS1:Real;
    iPM,iP:INteger;
    rQp:Real;
    bWr:Boolean;
    rPrCard:Real;
    iCp:INteger;

begin
  //��������
  with dmMC do
  with dmMt do
  begin
    if cxDateEdit1.Date<=prOpenDate then begin ShowMessage('������ ������.'); exit; end;
    if fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False then begin ShowMessage('������ ������ (��������������).'); exit; end;

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton8.Enabled:=False;
    ViewDoc4.BeginUpdate;

    IdH:=cxTextEdit1.Tag;

    if taSpecVn.State in [dsEdit,dsInsert] then taSpecVn.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "TTNSelf"');
      quC.SQL.Add('where Depart='+INtToStr(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateInvoice='''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='+cxTextEdit1.Text);
      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        cxButton1.Enabled:=True; cxButton2.Enabled:=True;
        exit;
      end;

      iNum:=StrToINtDef(cxTextEdit1.Text,0);
      if (iNum>0) and (iNum=Label1.Tag) then
      begin //��������� �����
        quE.SQL.Clear;
        quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
        quE.SQL.Add('where CodeObject=12');
        quE.ExecSQL;
      end;

    end;
    try
      bWr:=True;

      if fmAddDoc4.Tag=1 then  //������������
      begin
        iPM:=0;

        //����� ����� ��������� ����������
        cxLookupComboBox2.EditValue:=cxLookupComboBox1.EditValue;

        taSpecVn.First;
        while not taSpecVn.Eof do //���������� �������������
        begin
          if taSpecVnNum.AsInteger>iPm then iPm:=taSpecVnNum.AsInteger;
          if taSpecVnId.AsInteger=0 then  //������
          begin
            if taSpecVnKol.AsFloat<0 then    //������ ������ ���� ������ �������������
            begin
              taSpecVn.Edit;
              taSpecVnKol.AsFloat:=taSpecVnKol.AsFloat*(-1);

              taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
              taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
              taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

              if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
              else taSpecVnProcBrak.AsFloat:=0;

              taSpecVn.Post;
            end
          end else                     //������
          begin
            if taSpecVnKol.AsFloat>0 then  //������ ������ ���� ������ �������������
            begin
              taSpecVn.Edit;
              taSpecVnKol.AsFloat:=taSpecVnKol.AsFloat*(-1);
              taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
              taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
              taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

              if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
              else taSpecVnProcBrak.AsFloat:=0;
              taSpecVn.Post;
            end
          end;
          taSpecVn.Next;
        end;

        for iP:=1 to iPm do
        begin
          rSumSS:=0;
          rQp:=0;
          iCp:=0;
          taSpecVn.First; //��������� ���-�� �������
          while not taSpecVn.Eof do //���������� �������������
          begin
            if taSpecVnNum.AsInteger=iP then
            begin
              if taSpecVnKol.AsFloat>0 then rSumSS:=rSumSS+taSpecVnSumCena.AsFloat;
              if taSpecVnKol.AsFloat<0 then rQp:=rQp+(-1)*taSpecVnKol.AsFloat; //����� ���-�� �������
              inc(iCP);
            end;
            taSpecVn.Next;
          end;
          if (iCp>0) then //���-�� ������� � ������� - �������� �� ���������
          begin
            if (rQp>0)and(rSumSS>0) then //���� ������ �� ���� ������� � �����
            begin
              rSumSS:=rv(rSumSS);
//              rPrSS:=rv(rSumSS/rQp);
              rPrSS:=rSumSS/rQp;
              taSpecVn.First; // �������� ������������� �������
              while not taSpecVn.Eof do
              begin
                if taSpecVnNum.AsInteger=iP then
                begin
                  if taSpecVnKol.AsFloat<0 then
                  begin
                    rSumSS1:=rv(rPrSS*(-1)*taSpecVnKol.AsFloat);
                    if abs(rSumSS1-rSumSS)<0.1 then
                    begin
                      rSumSS1:=rSumSS;
                      rSumSS:=0;
                    end else
                      rSumSS:=rSumSS-rSumSS1;


//                  prCalcPriceOut(taSpecVnCodeTovar.AsInteger,taSpecVnCena.AsFloat,(taSpecVnKol.AsFloat*(-1)),rn1,rn2,rn3,rPr);

                    rPrCard:=0;
                    if taCards.FindKey([taSpecVnCodeTovar.AsInteger]) then rPrCard:=taCardsCena.AsFloat;

                    taSpecVn.Edit;
                    taSpecVnCena.AsFloat:=rPrSS;
                    taSpecVnSumCena.AsFloat:=(-1)*rSumSS1;
                    taSpecVnCenaSS.AsFloat:=rPrSS;
                    taSpecVnSumCenaSS.AsFloat:=(-1)*rSumSS1;
                    taSpecVnNewCena.AsFloat:=rPrCard;
                    taSpecVnSumNewCena.AsFloat:=rPrCard*taSpecVnKol.AsFloat;
                    taSpecVn.Post;

                  end;
                end;

                taSpecVn.Next;
              end;

              if rSumSS<>0 then  //���� ���-�� �������� - �� ����������� �� 1-�� ������� ������� � ����� � ��� ���� ��� �����
              begin
                taSpecVn.Last; // �������� ������������� �������
                while not taSpecVn.Bof do
                begin
                  if taSpecVnNum.AsInteger=iP then
                  begin
                    if taSpecVnKol.AsFloat<0 then //������ ������ � �����
                    begin
                      rSumSS1:=(-1)*taSpecVnSumCenaSS.AsFloat+rSumSS;

                      taSpecVn.Edit;
                      taSpecVnSumCena.AsFloat:=(-1)*rSumSS1;
                      taSpecVnSumCenaSS.AsFloat:=(-1)*rSumSS1;
                      taSpecVn.Post;

                      Break;

                    end;
                  end;
                  taSpecVn.Prior;
                end;
              end;

            end else
            begin
              if (rQp=0)or(rSumSS<=0) then
                 ShowMessage('�������� !!! ������ ���������� ������������ ���������, ��������� ���� � ���������� ������� ... ���������� ���������.');
              bWr:=False;
            end;
          end;
        end;
        if bWr=False then
        begin
          ShowMessage('�������� !!! ���������� ��������� �� �����������. ��������� ������ � ��������� ��������');
        end;
      end;

      if fmAddDoc4.Tag=2 then  //��������
      begin
      end;

      iCol:=0;
      if bWr then
      begin
        rSum1:=0; rSum2:=0; rSum10:=0; rSum20:=0;  rSum10n:=0; rSum20n:=0;
        taSpecVn.First;
        while not taSpecVn.Eof do
        begin
          //����������

          taSpecVn.Edit;
          taSpecVnCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat);
          taSpecVnNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat);
          taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnSumCena.AsFloat);
          taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnSumNewCena.AsFloat);
          taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnSumNewCena.AsFloat)-RoundVal(taSpecVnSumCena.AsFloat);
          taSpecVn.Post;

          rSum1:=rSum1+taSpecVnSumCena.AsFloat;
          rSum2:=rSum2+taSpecVnSumNewCena.AsFloat;

          if taSpecVnNDSProc.AsFloat>=15 then
          begin
            rSum20:=rSum20+taSpecVnSumCena.AsFloat;
            rSum20n:=rSum20n+taSpecVnSumNewCena.AsFloat;
          end else
          begin
            if taSpecVnNDSProc.AsFloat>=8 then
            begin
              rSum10:=rSum10+taSpecVnSumCena.AsFloat;
              rSum10n:=rSum20n+taSpecVnSumNewCena.AsFloat;
            end;
          end;
          taSpecVn.Next;
        end;

        if cxTextEdit1.Tag=0 then
        begin
          IDH:=prMax('DocsVn')+1;
//        cxTextEdit1.Tag:=IDH;  //� �����
        end else //��� �� ���������� , ����� ������� ��� ��� ���� ����� ������� ��� �� 100 ��� �����
        begin
  //    ������  ������� ����� ���� ����

          quD.SQL.Clear;
          quD.SQL.Add('delete from "TTNSelfLn"');
          quD.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
          quD.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
          quD.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
          quD.ExecSQL;

        end;

        //����� �������� ���������
        if cxTextEdit1.Tag=0 then //����������
        begin
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "TTNSelf" values (');

          quA.SQL.Add(its(cxLookupComboBox1.EditValue)+',');       //Depart
          quA.SQL.Add(''''+ds(cxDateEdit1.Date)+''','); //DateInvoice
          quA.SQL.Add(''''+cxTextEdit1.Text+''','); //Number
          quA.SQL.Add(its(cxLookupComboBox2.EditValue)+','); // FlowDepart
          quA.SQL.Add(fs(RV(rSum1))+','); // SummaTovarSpis
          quA.SQL.Add(fs(RV(rSum2))+','); // SummaTovarMove
          quA.SQL.Add(fs(RV(rSum1))+','); // SummaTovarNew
          quA.SQL.Add('0,'); // SummaTara
          quA.SQL.Add('2,'); // Oprihod
          quA.SQL.Add('0,'); // AcStatusP
          quA.SQL.Add('0,'); // ChekBuhP
          quA.SQL.Add('0,'); // AcStatusR
          quA.SQL.Add('0,'); // ChekBuhR
          quA.SQL.Add(its(fmAddDoc4.tag)+','); // ProvodTypeP
          quA.SQL.Add('0,'); // ProvodTypeR
          quA.SQL.Add(fs(RV(rSum10))+','); // GoodsSpisNDS10
          quA.SQL.Add(fs(RV(rSum20))+','); // GoodsSpisNDS20
          quA.SQL.Add(fs(RV(rSum10n))+','); // GoodsMoveNDS10
          quA.SQL.Add(fs(RV(rSum20n))+','); // GoodsMoveNDS20
          quA.SQL.Add(fs(RV(rSum10))+','); // GoodsNewNDS10
          quA.SQL.Add(fs(RV(rSum20))+','); // GoodsNewNDS20
          quA.SQL.Add(fs(RV(rSum1-rSum10-rSum20))+','); // GoodsLgtNDSSpis
          quA.SQL.Add(fs(RV(rSum2-rSum10n-rSum20n))+','); // GoodsLgtNDSMove
          quA.SQL.Add(fs(RV(rSum1-rSum10-rSum20))+','); // GoodsLgtNDSNew
          quA.SQL.Add('0,'); // GoodsNSPSpis10
          quA.SQL.Add('0,'); // GoodsNSPSpis20
          quA.SQL.Add('0,'); // GoodsNSPNew10
          quA.SQL.Add('0,'); // GoodsNSPNew20
          quA.SQL.Add('0,'); // GoodsNSP0Spis
          quA.SQL.Add('0,'); // GoodsNSP0New
          quA.SQL.Add(its(IDH)+','); // ID
          quA.SQL.Add('0,'); // LinkInvoice
          quA.SQL.Add('0,'); // StartTransfer
          quA.SQL.Add('0,'); // EndTransfer
          quA.SQL.Add(''''''); // Rezerv
          quA.SQL.Add(')');
          quA.ExecSQL;

        end else //����������
        begin
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('Update "TTNSelf" set');
          quA.SQL.Add('Depart='+INtToStr(cxLookupComboBox1.EditValue));
          quA.SQL.Add(',DateInvoice='+''''+ds(cxDateEdit1.Date)+'''');
          quA.SQL.Add(',Number='''+cxTextEdit1.Text+'''');
          quA.SQL.Add(',FlowDepart='+its(cxLookupComboBox2.EditValue)); // FlowDepart
          quA.SQL.Add(',SummaTovarSpis='+fs(rSum1)); // SummaTovarSpis
          quA.SQL.Add(',SummaTovarMove='+fs(rSum2)); // SummaTovarMove
          quA.SQL.Add(',SummaTovarNew='+fs(rSum1));  // SummaTovarNew
          quA.SQL.Add(',SummaTara=0'); // SummaTara
          quA.SQL.Add(',Oprihod=2');   // Oprihod
          quA.SQL.Add(',AcStatusP=0'); // AcStatusP
          quA.SQL.Add(',ChekBuhP=0');  // ChekBuhP
          quA.SQL.Add(',AcStatusR=0'); // AcStatusR
          quA.SQL.Add(',ChekBuhR=0');  // ChekBuhR
          quA.SQL.Add(',ProvodTypeP='+its(fmAddDoc4.tag)); // ProvodTypeP
          quA.SQL.Add(',ProvodTypeR=0'); // ProvodTypeR
          quA.SQL.Add(',GoodsSpisNDS10='+fs(rSum10)); // GoodsSpisNDS10
          quA.SQL.Add(',GoodsSpisNDS20='+fs(rSum20)); // GoodsSpisNDS20
          quA.SQL.Add(',GoodsMoveNDS10='+fs(rSum10n)); // GoodsMoveNDS10
          quA.SQL.Add(',GoodsMoveNDS20='+fs(rSum20n)); // GoodsMoveNDS20
          quA.SQL.Add(',GoodsNewNDS10='+fs(rSum10)); // GoodsNewNDS10
          quA.SQL.Add(',GoodsNewNDS20='+fs(rSum20)); // GoodsNewNDS20
          quA.SQL.Add(',GoodsLgtNDSSpis='+fs(rSum1-rSum10-rSum20)); // GoodsLgtNDSSpis
          quA.SQL.Add(',GoodsLgtNDSMove='+fs(rSum2-rSum10n-rSum20n)); // GoodsLgtNDSMove
          quA.SQL.Add(',GoodsLgtNDSNew='+fs(rSum1-rSum10-rSum20)); // GoodsLgtNDSNew
          quA.SQL.Add(',GoodsNSPSpis10=0'); // GoodsNSPSpis10
          quA.SQL.Add(',GoodsNSPSpis20=0'); // GoodsNSPSpis20
          quA.SQL.Add(',GoodsNSPNew10=0'); // GoodsNSPNew10
          quA.SQL.Add(',GoodsNSPNew20=0'); // GoodsNSPNew20
          quA.SQL.Add(',GoodsNSP0Spis=0'); // GoodsNSP0Spis
          quA.SQL.Add(',GoodsNSP0New=0'); // GoodsNSP0New
          quA.SQL.Add(',ID='+its(IDH)); // ID
          quA.SQL.Add(',LinkInvoice=0'); // LinkInvoice
          quA.SQL.Add(',StartTransfer=0'); // StartTransfer
          quA.SQL.Add(',EndTransfer=0'); // EndTransfer
          quA.SQL.Add(',Rezerv='''''); // Rezerv
          quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
          quA.SQL.Add('and DateInvoice='''+ds(cxDateEdit10.Date)+'''');
          quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
          quA.ExecSQL;
        end;
        //������� �� ������������ - ��� ������ ����������
        taSpecVn.First;
        while not taSpecVn.Eof do
        begin
//        prFindGr(taSpecVnCodeTovar.AsInteger,iGr,iSGr);

          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "TTNSelfLn" values (');
          quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)); //  Depart
          quA.SQL.Add(','''+ds(cxDateEdit1.Date)+''''); //DateInvoice
          quA.SQL.Add(','''+cxTextEdit1.Text+''''); //  Number
          quA.SQL.Add(','+its(taSpecVnNum.AsInteger));// CodeGroup
          quA.SQL.Add(','+its(taSpecVnId.AsInteger));// CodePodgr
          quA.SQL.Add(','+its(taSpecVnCodeTovar.AsInteger));// CodeTovar
          quA.SQL.Add(','+its(taSpecVnCodeEdIzm.AsInteger));// CodeEdIzm
          quA.SQL.Add(','+its(taSpecVnNum.AsInteger));// TovarType
          quA.SQL.Add(','''+taSpecVnBarCode.AsString+'''');// BarCode
          quA.SQL.Add(','+its(RoundEx(taSpecVnQuantM.AsFloat*1000)));// KolMest taSpecVnKolMest.AsFloat
          quA.SQL.Add(',0');// KolEdMest
          quA.SQL.Add(','+fs(0));// KolWithMest  taSpecVnKolWithMest.AsFloat
          quA.SQL.Add(','+fs(taSpecVnKol.AsFloat)); // Kol
          quA.SQL.Add(','+fs(RoundVal(taSpecVnNewCena.AsFloat))); // CenaTovarMove
          quA.SQL.Add(','+fs(RoundVal(taSpecVnCena.AsFloat))); // CenaTovarSpis
          quA.SQL.Add(','+fs(taSpecVnProc.AsFloat)); // Procent
          quA.SQL.Add(','+fs(RoundVal(taSpecVnCena.AsFloat))); // NewCenaTovar
          quA.SQL.Add(','+fs(RoundVal(taSpecVnSumCena.AsFloat))); // SumCenaTovarSpis
          quA.SQL.Add(','+fs(RoundVal(taSpecVnSumCena.AsFloat))); // SumNewCenaTovar
          quA.SQL.Add(',0');// CodeTara
          quA.SQL.Add(',0');// VesTara
          quA.SQL.Add(',0');// CenaTara
          quA.SQL.Add(','+fs(taSpecVnQuantN.AsFloat));// SumVesTara   - ���-�� �� ���������
          quA.SQL.Add(',0');// SumCenaTara
          quA.SQL.Add(',2');// ChekBuhPrihod
          quA.SQL.Add(',2');// ChekBuhRashod
          quA.SQL.Add(')');
          quA.ExecSQL;

          taSpecVn.Next;
        end;

        cxTextEdit1.Tag:=IDH;
        cxTextEdit10.Tag:=IDH;
        cxTextEdit10.Text:=cxTextEdit1.Text;
        cxDateEdit10.Date:=cxDateEdit1.Date;
        cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;
      end; //bWr
    finally
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      prRefrID(quTTNVn,fmDocs4.ViewDocsVn,0);
      ViewDoc4.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc4.acDelAllExecute(Sender: TObject);
begin
  if fmAddDoc4.Tag=3 then exit;
  if cxButton1.Enabled then
  begin
    if taSpecVn.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        taSpecVn.First; while not taSpecVn.Eof do taSpecVn.Delete;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc4.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc4.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc4.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc4.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc4.acReadBarExecute(Sender: TObject);
begin
  if fmAddDoc4.Tag=3 then exit;
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc4.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC,iMax:INteger;
begin
  if fmAddDoc4.Tag=3 then exit;
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if taSpecVn.RecordCount>0 then
    if taSpecVn.Locate('BarCode',sBar,[]) then
    begin
      ViewDoc4.Controller.FocusRecord(ViewDoc4.DataController.FocusedRowIndex,True);
      GridDoc4.SetFocus;
      exit;
    end;

  if prFindBarC(sBar,0,iC) then
  begin
    iMax:=1;
    if taSpecVn.RecordCount>0 then begin taSpecVn.Last; iMax:=taSpecVnNum.AsInteger+1; end;

    with dmMC do
    begin
      taSpecVn.Append;
      taSpecVnNum.AsInteger:=iMax;
      taSpecVnId.AsInteger:=0;
      taSpecVnCodeTovar.AsInteger:=iC;
      taSpecVnCodeEdIzm.AsInteger:=quCIdEdIzm.AsInteger;
      taSpecVnBarCode.AsString:=sBar;
      taSpecVnNDSProc.AsFloat:=quCIdNDS.AsFloat;
//      taSpecVnKolMest.AsInteger:=1;
//      taSpecVnKolWithMest.AsInteger:=1;
      taSpecVnKol.AsFloat:=1;
      taSpecVnName.AsString:=quCIdName.AsString;
      taSpecVnCena.AsFloat:=quCIdCena.AsFloat;
      taSpecVnNewCena.AsFloat:=quCIdCena.AsFloat;
      taSpecVnProc.AsFloat:=0;
      taSpecVnSumCena.AsFloat:=quCIdCena.AsFloat;
      taSpecVnSumNewCena.AsFloat:=quCIdCena.AsFloat;
      taSpecVnSumRazn.AsFloat:=0;
      taSpecVnFullName.AsString:=quCIdFullName.AsString;
      taSpecVn.Post;

      ViewDoc4.Controller.FocusRecord(ViewDoc4.DataController.FocusedRowIndex,True);
      GridDoc4.SetFocus;
    end;
  end else
    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc4.ViewDoc4DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
    if (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Name') then
    begin
      ViewDoc4Name.Options.Editing:=True;
      ViewDoc4Name.Focused:=True;
    end;
end;

procedure TfmAddDoc4.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc4.acPrintDocExecute(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum1,rSum2,rSum3:Real;
    iSS:INteger;
    sStr:String;
begin
  try
    ViewDoc4.BeginUpdate;

    CloseTa(taSpecPr);

    rSum1:=0; rSum2:=0;  rSum3:=0;
    iSS:=fSS(cxLookupComboBox1.EditValue);

    taSpecVn.First;
    while not taSpecVn.Eof do
    begin
      taSpecVn.Edit;
      taSpecVnCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat);
      taSpecVnNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat);
      taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnSumCena.AsFloat);
      taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnSumNewCena.AsFloat);
      taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnSumNewCena.AsFloat)-RoundVal(taSpecVnSumCena.AsFloat);
      taSpecVn.Post;

      rSum1:=rSum1+RoundVal(taSpecVnSumCena.AsFloat);
      rSum2:=rSum2+RoundVal(taSpecVnSumNewCena.AsFloat);

      taSpecPr.Append;
      taSpecPrNum.AsInteger:=taSpecVnNum.AsInteger;
      taSpecPrId.AsInteger:=taSpecVnId.AsInteger;
      taSpecPrCodeTovar.AsInteger:=taSpecVnCodeTovar.AsInteger;
      taSpecPrName.AsString:=taSpecVnName.AsString;
      taSpecPrFullName.AsString:=taSpecVnFullName.AsString;
      taSpecPrKol.AsFloat:=taSpecVnKol.AsFloat;
      taSpecPrCena.AsFloat:=taSpecVnCena.AsFloat;
      taSpecPrSumCena.AsFloat:=taSpecVnSumCena.AsFloat;
      taSpecPrNewCena.AsFloat:=taSpecVnNewCena.AsFloat;
      taSpecPrSumNewCena.AsFloat:=taSpecVnSumNewCena.AsFloat;
      taSpecPrEdIzm.AsInteger:=taSpecVnCodeEdIzm.AsInteger;
      taSpecPrNDS.AsFloat:=taSpecVnNDSProc.AsFloat;
      if iSS=2 then taSpecPrNDSSum.AsFloat:=rv(taSpecVnSumCena.AsFloat/100*(100+taSpecVnNDSProc.AsFloat))-taSpecVnSumCena.AsFloat
      else taSpecPrNDSSum.AsFloat:=taSpecVnSumCena.AsFloat-rv(taSpecVnSumCena.AsFloat*100/(100+taSpecVnNDSProc.AsFloat));
      taSpecPr.Post;

      rSum3:=rSum3+RoundVal(taSpecPrNDSSum.AsFloat);

      taSpecVn.Next;
    end;

    if fmAddDoc4.tag=1 then
    begin
      frRepDVN.LoadFromFile(CommonSet.Reports + 'ActProiz.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
      frVariables.Variable['SumOld']:=rSum1;
      frVariables.Variable['SumNew']:=rSum2;
      frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum1),True,False);
      frVariables.Variable['SSumMag']:=MoneyToString(abs(rSum2),True,False);

      frRepDVN.ReportName:='��� �����������.';
    end;

    if (fmAddDoc4.tag=2)or(fmAddDoc4.tag=3) then
    begin
      sStr:='��� ��������  ';

      if iSS=2 then frRepDVN.LoadFromFile(CommonSet.Reports + 'ActSpis2.frf')
      else frRepDVN.LoadFromFile(CommonSet.Reports + 'ActSpis.frf');

      if fmAddDoc4.tag=3 then sStr:=sStr+'(� �������)  ';
      sStr:=sStr+'� ';

      frVariables.Variable['DocType']:=sStr;
      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
      frVariables.Variable['Depart1']:=cxLookupComboBox2.Text;
      frVariables.Variable['SumOld']:=rSum1;
      frVariables.Variable['SumNew']:=rSum2;
      frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum1),True,False);
      frVariables.Variable['SSumMag']:=MoneyToString(abs(rSum2),True,False);
      frVariables.Variable['SSumNDS']:=MoneyToString(abs(rSum3),True,False);

      frRepDVN.ReportName:='��� ��������.';
    end;

    frRepDVN.PrepareReport;
    delay(10);
    vPP:=frAll;
    if fmAddDoc4.cxCheckBox1.Checked then frRepDVN.ShowPreparedReport
    else frRepDVN.PrintPreparedReport('',1,False,vPP);
    delay(10);
  finally
    delay(33);
    taSpecPr.Active:=False;
    ViewDoc4.EndUpdate;
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedCenaChange(Sender: TField);
begin
  if iCol=3 then
  begin
    taSpecVnProc.AsFloat:=0;
    if taSpecVnCena.AsFloat<>0 then
    begin
      taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
    end;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedNewCenaChange(Sender: TField);
begin
  if iCol=4 then
  begin
    taSpecVnProc.AsFloat:=0;
    if taSpecVnCena.AsFloat<>0 then
    begin
      taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
    end;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedSumCenaChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if taSpecVnKol.AsFloat<>0 then    //���� ���� �� �����
    begin
//    taSpecVnCena.AsFloat:=taSpecVnSumCena.AsFloat/taSpecVnKol.AsFloat;

      taSpecVnProc.AsFloat:=0;
      if taSpecVnCena.AsFloat<>0 then
      begin
        taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
      end;

      taSpecVnSumRazn.AsFloat:=taSpecVnSumNewCena.AsFloat-taSpecVnSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedSumNewCenaChange(Sender: TField);
begin
  if iCol=6 then
  begin
    if taSpecVnKol.AsFloat<>0 then
    begin
//      taSpecVnNewCena.AsFloat:=taSpecVnSumNewCena.AsFloat/taSpecVnKol.AsFloat;
      taSpecVnProc.AsFloat:=0;
      if taSpecVnCena.AsFloat<>0 then
      begin
        taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
      end;
      taSpecVnSumRazn.AsFloat:=taSpecVnSumNewCena.AsFloat-taSpecVnSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedKolMestChange(Sender: TField);
begin
  if iCol=1 then //���-�� ����
  begin
//    taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.taSpecVnNoNeedKolWithMestChange(Sender: TField);
begin
  if iCol=2 then //���-�� ����
  begin
//    taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc4);
end;

procedure TfmAddDoc4.acPrintTermoExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    sBar:String;
begin
  //������ �� �����
  if ViewDoc4.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if prOpenPrinter(CommonSet.TPrintN) then
          begin
            for i:=0 to ViewDoc4.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewDoc4.Controller.SelectedRecords[i];

              iNum:=0; rPr:=0;
              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewDoc4.Columns[j].Name='ViewDoc4CodeTovar' then iNum:=Rec.Values[j];
                if ViewDoc4.Columns[j].Name='ViewDoc4NewCena' then rPr:=Rec.Values[j];
              end;
              if iNum>0 then
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  for k:=0 to Str.Count-1 do
                  begin
                    StrWk:=Str[k];
                    if pos('@38.3@',StrWk)>0 then
                    begin
                      insert('E30',StrWk,pos('@38.3@',StrWk));
                      delete(StrWk,pos('@38.3@',StrWk),6)
                    end;
                    if pos('@',StrWk)>0 then
                    begin //���� ��� �� ���� ����������??
                      Str1:=Strwk;

                      Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                      delete(Str1,1,pos('@',Str1));//3.30.36@"

                      Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                      delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                      Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                      if Str4='3' then   //��������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4FullName.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='5' then  //������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //5.30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4NameCu.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='7' then  //�������     7.6.36
                      begin
                        Str5:=INtToStr(iNum);
                      end;
                      if Str4='18' then   //����   18.10.36
                      begin
                        Str5:=ToStr(rPr);
                      end;
                      if Str4='31' then    //��
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //31.13.8
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=13;

                        //�� �� ������� ����
                        sBar:=quFindC4BarCode.AsString;
                        if Length(sBar)<13 then
                        begin
                          if Length(sBar)=7 then //������� ����
                          begin
                            sBar:=ToStandart(sBar);
                            l:=13;
                          end;
                        end;
                        Str5:=Copy(sBar,1,l);

                      end;
                      if Str4='39' then   //���-��
                      begin
                        Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                      end;
                      StrWk:=Str2+Str5+Str1;
                    end;
                    prWritePrinter(StrWk+#$0D+#$0A);
                  end;
                end;
                quFindC4.Active:=False;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;

procedure TfmAddDoc4.acPrintCennExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
    rPr:Real;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc4.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc4.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc4.Controller.SelectedRecords[i];

        iNum:=0; rPr:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc4.Columns[j].Name='ViewDoc4CodeTovar' then  iNum:=Rec.Values[j];
          if ViewDoc4.Columns[j].Name='ViewDoc4NewCena' then  rPr:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=rPr;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc4.acSetPriceExecute(Sender: TObject);
Var rPriceIn,rPriceM,rREmn,rPriceIn0:Real;
    //rQ:Real;
    iSS:INteger;
begin
  //����������� ������ ����
  if fmAddDoc4.Tag=3 then exit;
  with dmMT do
  begin
  if cxButton1.Enabled then
  begin
    iCol:=0;
    iSS:=fSS(cxLookupComboBox1.EditValue);
    ViewDoc4.BeginUpdate;
    taSpecVn.First;
    while not taSpecVn.Eof do
    begin
      rPriceIn:=0;

      if iSS=0 then rPriceIn:=prFLP(taSpecVnCodeTovar.AsInteger,rPriceM)
      else
      begin
        if (iSS=1)or(iSS=2) then
        begin
          rPriceIn:=prFindPricePP0(taSpecVnCodeTovar.AsInteger,cxLookupComboBox1.EditValue,rPriceIn0 );
          if (iSS=2) then rPriceIn:=rPriceIn0;
        end;
      end;

      rPriceM:=prFindPriceDateMC(taSpecVnCodeTovar.AsInteger,Trunc(cxDateEdit1.Date+1));

      if (cxLookupComboBox1.EditValue>0)and(taSpecVnCodeTovar.AsInteger>0)
        then rRemn:=prFindTRemnDep(taSpecVnCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
        else rRemn:=0;

//      rQ:=taSpecVnKolWithMest.AsFloat;

      if taSpecVnKol.AsFloat>0 then   //��� ������� �������
      begin
        taSpecVn.Edit;
        taSpecVnCena.AsFloat:=rPriceIn;
        taSpecVnCenaSS.AsFloat:=rPriceIn;
        taSpecVnSumCenaSS.AsFloat:=RoundVal(rPriceIn*taSpecVnKol.AsFloat);
        taSpecVnSumCena.AsFloat:=RoundVal(rPriceIn*taSpecVnKol.AsFloat);
        taSpecVnNewCena.AsFloat:=rPriceM;
        taSpecVnSumNewCena.AsFloat:=rPriceM*taSpecVnKol.AsFloat;
//        taSpecVnKolWithMest.AsFloat:=rQ;   //������ ��� ��������� ���� ���� � ��������
        taSpecVnRemn.AsFloat:=rREmn;
        taSpecVn.Post;
      end else
      begin
        taSpecVn.Edit;
        taSpecVnCena.AsFloat:=0;
        taSpecVnCenaSS.AsFloat:=0;
        taSpecVnSumCenaSS.AsFloat:=0;
        taSpecVnNewCena.AsFloat:=rPriceM;
        taSpecVnSumNewCena.AsFloat:=rPriceM*taSpecVnKol.AsFloat;
//        taSpecVnKolWithMest.AsFloat:=rQ;   //������ ��� ��������� ���� ���� � ��������
        taSpecVnRemn.AsFloat:=rREmn;
        taSpecVn.Post;
      end;
      taSpecVn.Next;
      delay(10);
    end;
    iCol:=0;
    taSpecVn.First;
    ViewDoc4.EndUpdate;
  end;
  end;
end;

procedure TfmAddDoc4.acSetRemnExecute(Sender: TObject);
Var rRemn:Real;
begin
  //����������� �������
  if cxButton1.Enabled then
  begin
    ViewDoc4.BeginUpdate;
    taSpecVn.First;
    while not taSpecVn.Eof do
    begin
      if (cxLookupComboBox1.EditValue>0)and(taSpecVnCodeTovar.AsInteger>0)
        then rRemn:=prFindTRemnDep(taSpecVnCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
        else rRemn:=0;


      taSpecVn.Edit;
      taSpecVnRemn.AsFloat:=rREmn;
      taSpecVn.Post;

      taSpecVn.Next;
      delay(10);
    end;
    taSpecVn.First;
    ViewDoc4.EndUpdate;
  end;
end;

procedure TfmAddDoc4.acPostavExecute(Sender: TObject);
Var iSS:Integer;
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if taSpecVn.RecordCount>0 then
    begin
      bDrVn:=True;

      iSS:=fSS(cxLookupComboBox1.EditValue);
      fmMove.ViewP.Tag:=iSS;

      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=taSpecVnCodeTovar.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=taSpecVnCodeTovar.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=taSpecVnName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel6Click(Sender: TObject);
begin
  acSetPrice.Execute;
end;

procedure TfmAddDoc4.ViewDoc4SelectionChanged(
  Sender: TcxCustomGridTableView);
Var iDateE,iDateB:Integer;
    iSS:INteger;
begin
  if ViewDoc4.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  with dmMt do
  begin
//    quPost5.ParamByName('IDCARD').Value:=taSpecVnCodeTovar.AsInteger;
    try
      ViewPVn.BeginUpdate;
      CloseTe(tePartIn1);
      iDateE:=Trunc(Date);
      iDateB:=Trunc(Date-60);

      iSS:=fSS(cxLookupComboBox1.EditValue);

      if iSS>=1 then
      begin
        if ptPartIN.Active=False then ptPartIN.Active:=True;
        if ptDep.Active=False then ptDep.Active:=True;

        ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';

        ptDep.First;
        while not ptDep.Eof do
        begin
          if (ptDepStatus5.AsInteger>=1) then
          begin
            ptPartIn.IndexFieldNames:='IDSTORE;ARTICUL;IDATE';
            ptPartIN.SetRange([ptDepID.AsInteger,taSpecVnCodeTovar.AsInteger,iDateE],[ptDepID.AsInteger,taSpecVnCodeTovar.AsInteger,iDateB]);
            ptPartIn.First;
            while not ptPartIn.Eof do
            begin
              tePartIn1.Append;
              tePartIn1ID.AsInteger:=ptPartINID.AsInteger;
              tePartIn1IDSTORE.AsInteger:=ptDepID.AsInteger;
              tePartIn1SSTORE.AsString:=OemToAnsiConvert(ptDepName.AsString);
              tePartIn1IDATE.AsInteger:=ptPartINIDATE.AsInteger;
              tePartIn1IDDOC.AsInteger:=ptPartINIDDOC.AsInteger;
              tePartIn1ARTICUL.AsInteger:=ptPartINARTICUL.AsInteger;
              tePartIn1DTYPE.AsInteger:=ptPartINDTYPE.AsInteger;
              tePartIn1QPART.AsFloat:=ptPartINQPART.AsFloat;
              tePartIn1QREMN.AsFloat:=ptPartINQREMN.AsFloat;
              tePartIn1PRICEIN.AsFloat:=ptPartINPRICEIN.AsFloat;
              tePartIn1PRICEIN0.AsFloat:=ptPartINPRICEIN0.AsFloat;
              tePartIn1PRICEOUT.AsFloat:=ptPartINPRICEOUT.AsFloat;
              tePartIn1SINNCLI.AsString:=ptPartINSINNCLI.AsString;
              tePartIn1SCLI.AsString:=prFindCliName(ptPartINSINNCLI.AsString);
              tePartIn1.Post;

              ptPartIn.Next;
            end;
          end;
          ptDep.Next;
        end;

        ptPartIn.IndexFieldNames:='ID';
        ptPartIN.CancelRange;
      end else
      begin
        quPost1.Active:=False;
        quPost1.ParamByName('IDCARD').Value:=taSpecVnCodeTovar.AsInteger;
        quPost1.Active:=True;
        quPost1.First;
        while not quPost1.Eof do
        begin
          tePartIn1.Append;
          tePartIn1ID.AsInteger:=0;
          tePartIn1IDSTORE.AsInteger:=quPost1Depart.AsInteger;
          tePartIn1SSTORE.AsString:=quPost1Name.AsString;
          tePartIn1IDATE.AsInteger:=Trunc(quPost1DateInvoice.AsDateTime);
          tePartIn1IDDOC.AsInteger:=0;
          tePartIn1ARTICUL.AsInteger:=quPost1CodeTovar.AsInteger;
          tePartIn1DTYPE.AsInteger:=1;
          tePartIn1QPART.AsFloat:=quPost1Kol.AsFloat;
          tePartIn1QREMN.AsFloat:=quPost1Kol.AsFloat;
          tePartIn1PRICEIN.AsFloat:=quPost1CenaTovar.AsFloat;
          tePartIn1PRICEIN0.AsFloat:=quPost1CenaTovar.AsFloat;
          tePartIn1PRICEOUT.AsFloat:=quPost1NewCenaTovar.AsFloat;
          tePartIn1SINNCLI.AsString:=quPost1sInn.AsString;
          tePartIn1SCLI.AsString:=quPost1NameCli.AsString;
          tePartIn1.Post;

          quPost1.Next;
        end;
        quPost1.Active:=False;
        if (tePartIn1.RecordCount=0) or (tePartIn1IDATE.AsInteger<(Date-30)) then
        begin //������ ��� ���� ������ - �������� ��� ������� �� ��. ������������.
          quPostVn.Active:=False;
          quPostVn.ParamByName('IDCARD').Value:=taSpecVnCodeTovar.AsInteger;
          quPostVn.Active:=True;
          quPostVn.First;
          while not quPostVn.Eof do
          begin
            tePartIn1.Append;
            tePartIn1ID.AsInteger:=0;
            tePartIn1IDSTORE.AsInteger:=quPostVnDepart.AsInteger;
            tePartIn1SSTORE.AsString:=quPostVnName.AsString;
            tePartIn1IDATE.AsInteger:=Trunc(quPostVnDateInvoice.AsDateTime);
            tePartIn1IDDOC.AsInteger:=0;
            tePartIn1ARTICUL.AsInteger:=quPostVnCodeTovar.AsInteger;
            tePartIn1DTYPE.AsInteger:=3;
            tePartIn1QPART.AsFloat:=quPostVnKol.AsFloat;
            tePartIn1QREMN.AsFloat:=quPostVnKol.AsFloat;
            tePartIn1PRICEIN.AsFloat:=quPostVnCenaTovarSpis.AsFloat;
            tePartIn1PRICEIN0.AsFloat:=quPostVnCenaTovarSpis.AsFloat;
            tePartIn1PRICEOUT.AsFloat:=quPostVnCenaTovarMove.AsFloat;
            tePartIn1SINNCLI.AsString:='';
            tePartIn1SCLI.AsString:='';
            tePartIn1.Post;

            quPostVn.Next;
          end;
          quPostVn.Active:=False;
        end;

      end;
    finally
      ViewPVn.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc4.ViewPVnDblClick(Sender: TObject);
Var bSet:Boolean;
    iSS:INteger;
begin
  //������� �������
  if cxButton1.Enabled=False then exit;
  if fmAddDoc4.Tag=3 then exit;
  if tePartIn1.RecordCount>0 then
  begin
    if taSpecVn.RecordCount>0 then
    begin
      if taSpecVnKol.AsFloat>0 then
      begin
        bSet:=True;
        if tePartIn1PRICEIN.AsFloat<0.01 then
        begin
          bSet:=False;
          if MessageDlg('���� ��������� ������ ����� 0, ��� ����� �������?',mtConfirmation, [mbYes, mbNo], 0) = 3
          then bSet:=True;
        end;

        if tePartIn1QREMN.AsFloat<0.01 then
        begin
          bSet:=False;
          if MessageDlg('������� ��������� ������ ����� 0, ��� ����� �������?',mtConfirmation, [mbYes, mbNo], 0) = 3
          then bSet:=True;
        end;

        if bSet then
        begin
          iSS:=fSS(cxLookupComboBox1.EditValue);

          if iSS=1 then
          begin
            taSpecVn.Edit;
//          taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
            taSpecVnCena.AsFloat:=tePartIn1PRICEIN.AsFloat;
            taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

            taSpecVnCenaSS.AsFloat:=tePartIn1PRICEIN.AsFloat;
            taSpecVnSumCenaSS.AsFloat:=RoundVal(taSpecVnCenaSS.AsFloat*taSpecVnKol.AsFloat);

            taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
            taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
            taSpecVn.Post;
          end;
          if iSS=2 then
          begin
            taSpecVn.Edit;
//          taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
            taSpecVnCena.AsFloat:=tePartIn1PRICEIN0.AsFloat;
            taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

            taSpecVnCenaSS.AsFloat:=tePartIn1PRICEIN0.AsFloat;
            taSpecVnSumCenaSS.AsFloat:=RoundVal(taSpecVnCenaSS.AsFloat*taSpecVnKol.AsFloat);

            taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
            taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
            taSpecVn.Post;
          end;
        end;

      end else
      begin
        Showmessage('��������, ��� ������� ������������� ������� ��������� ��������.');
      end;
    end;
  end;
end;

procedure TfmAddDoc4.acAddPosInExecute(Sender: TObject);
begin
  if fmAddDoc4.tag=3 then exit;
  if cxButton1.Enabled then
  begin
    if (taSpecVn.RecordCount>0) and (taSpecVnKol.AsFloat>0) then
    begin
      if fmAddDoc4.tag=1 then
      begin
        iTPos:=1;
        iDirect:=7;
        fmCards.Show;
      end else showmessage('� �������� "��������" ��������� ������ ��������.');
    end else ShowMessage('�������� ������� ������� � ������� ����� �������� ������ ...');
  end;
end;

procedure TfmAddDoc4.ViewDoc4CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var sTp:String;
    i:INteger;
begin
  if fmAddDoc4.tag<>1 then exit;
  sTp:='';
  for i:=0 to ViewDoc4.ColumnCount-1 do
  begin
    if ViewDoc4.Columns[i].Name='ViewDoc4Id' then
    begin
      sTp:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;
  if Pos('����',sTp)>0 then
  begin
     ACanvas.Canvas.Brush.Color := $00BFFFBF; //�����������
     ACanvas.Font.Color:=clGreen;
  end;
end;

procedure TfmAddDoc4.taSpecVnKolMestChange(Sender: TField);
begin
  if iCol=1 then //���-�� ����
  begin
{    taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

    if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
    else taSpecVnProcBrak.AsFloat:=0;}
  end;
end;

procedure TfmAddDoc4.taSpecVnKolWithMestChange(Sender: TField);
begin
  if iCol=2 then //���-�� � �����
  begin
{    taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*taSpecVnKolWithMest.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

    if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
    else taSpecVnProcBrak.AsFloat:=0;}
  end;
end;

procedure TfmAddDoc4.taSpecVnCenaChange(Sender: TField);
begin
  if iCol=3 then
  begin
    taSpecVnProc.AsFloat:=0;
    if taSpecVnCena.AsFloat<>0 then
    begin
      taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
    end;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.taSpecVnNewCenaChange(Sender: TField);
begin
  if iCol=4 then
  begin
    taSpecVnProc.AsFloat:=0;
    if taSpecVnCena.AsFloat<>0 then
    begin
      taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
    end;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
  end;
end;

procedure TfmAddDoc4.taSpecVnSumCenaChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if taSpecVnKol.AsFloat<>0 then    //���� ���� �� �����
    begin
//    taSpecVnCena.AsFloat:=taSpecVnSumCena.AsFloat/taSpecVnKol.AsFloat;

      taSpecVnProc.AsFloat:=0;
      if taSpecVnCena.AsFloat<>0 then
      begin
        taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
      end;

      taSpecVnSumRazn.AsFloat:=taSpecVnSumNewCena.AsFloat-taSpecVnSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc4.taSpecVnSumNewCenaChange(Sender: TField);
begin
  if iCol=6 then
  begin
    if taSpecVnKol.AsFloat<>0 then
    begin
//      taSpecVnNewCena.AsFloat:=taSpecVnSumNewCena.AsFloat/taSpecVnKol.AsFloat;
      taSpecVnProc.AsFloat:=0;
      if taSpecVnCena.AsFloat<>0 then
      begin
        taSpecVnProc.AsFloat:=(taSpecVnNewCena.AsFloat-taSpecVnCena.AsFloat)/taSpecVnCena.AsFloat*100;
      end;
      taSpecVnSumRazn.AsFloat:=taSpecVnSumNewCena.AsFloat-taSpecVnSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel10Click(Sender: TObject);
begin
  iTPos:=1;
  acAddPos.Execute;
end;

procedure TfmAddDoc4.cxLabel11Click(Sender: TObject);
begin
  iTPos:=1;
  acAddList.Execute;
end;

procedure TfmAddDoc4.acAddPosOutExecute(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    if (taSpecVn.RecordCount>0) and (taSpecVnKol.AsFloat<0) then
    begin
      if fmAddDoc4.tag=1 then
      begin
        iTPos:=0; //������
        iDirect:=7;
        fmCards.Show;
      end else showmessage('� �������� "��������" ��������� ������ ��������.');
    end else ShowMessage('�������� ������� ������� � ������� ����� �������� ������ ...');
  end;
end;

procedure TfmAddDoc4.PopupMenu1Popup(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    if (taSpecVn.RecordCount>0) then
    begin
      if taSpecVnId.AsInteger=0 then
      begin //������  - ������ ������ ��������
        acAddPosOut.Enabled:=False;
        acAddPosIn.Enabled:=True;
      end else
      begin  // 1 ������ - ������ ������ ��������
        acAddPosOut.Enabled:=True;
        acAddPosIn.Enabled:=False;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc4.cxLabel4Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddDoc4.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  fmAddDoc4.ViewDoc4SelectionChanged(ViewDoc4);
end;

procedure TfmAddDoc4.acViewPrihodExecute(Sender: TObject);
Var sNum:String;
    dDate:TDateTime;
    iDep,iDate,IdH:INteger;
begin
 //������� ������
  with dmMC do
  with dmMt do
  begin
    if fmAddDoc4.Tag=3 then //����� ������
    begin
      sNum:=cxTextEdit1.Text;
      dDate:=cxDateEdit1.Date;
      iDep:=cxLookupComboBox1.EditValue;

      if ptTTNIn.Active=False then ptTTNIn.Active:=True;
      ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';

      iDate:=0;

      if ptTTnIn.FindKey([iDep,dDate,sNum]) then iDate:=Trunc(ptTTnInDateInvoice.AsDateTime);
      if iDate=0 then
        if ptTTnIn.FindKey([iDep,dDate-1,sNum]) then iDate:=Trunc(ptTTnInDateInvoice.AsDateTime);

      if iDate>0 then
      begin
        iDh:=ptTTnInId.AsInteger;
        fmDocs1.LevelDocsIn.Visible:=True;
        fmDocs1.LevelCards.Visible:=False;

        fmDocs1.ViewDocsIn.BeginUpdate;
        try
          quTTNIn.Active:=False;
          quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd); // ��� <=
          quTTNIn.Active:=True;
        finally
          fmDocs1.ViewDocsIn.EndUpdate;
        end;

        fmDocs1.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        if quTTNIn.Locate('Id',iDh,[]) then
          fmDocs1.acViewDoc1.Execute;

      end else showmessage('��������� �������� �� ������.');
    end else showmessage('�������� ��������.');
  end;
end;

procedure TfmAddDoc4.taSpecVnQuantMChange(Sender: TField);
begin
  if iCol=1 then //���-�� ��� �������� �� ��������
  begin
    taSpecVnKol.AsFloat:=RoundEx(taSpecVnQuantM.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

    if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
    else taSpecVnProcBrak.AsFloat:=0;
  end;
end;

procedure TfmAddDoc4.taSpecVnKolChange(Sender: TField);
begin
  if iCol=2 then //���-��
  begin
//    taSpecVnKol.AsFloat:=RoundEx(taSpecVnKolMest.AsFloat*1000)/1000;
    taSpecVnSumCena.AsFloat:=RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumNewCena.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat);
    taSpecVnSumRazn.AsFloat:=RoundVal(taSpecVnNewCena.AsFloat*taSpecVnKol.AsFloat)-RoundVal(taSpecVnCena.AsFloat*taSpecVnKol.AsFloat);

    if taSpecVnQuantN.AsFloat>0 then taSpecVnProcBrak.AsFloat:=rv(taSpecVnKol.AsFloat/taSpecVnQuantN.AsFloat*100)
    else taSpecVnProcBrak.AsFloat:=0;
  end;
end;

end.
