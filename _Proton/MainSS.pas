unit MainSS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxMemo, cxCheckBox, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan,
  ComCtrls, DB, pvtables, btvtables, dxmdaset, sqldataset, pvsqltables;

type
  TfmManager = class(TForm)
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    amCalcSS: TActionManager;
    acStartSS: TAction;
    cxCheckBox2: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acStartSSExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmManager: TfmManager;

implementation

uses Un1,u2fdk, MT, PXDB, Status, MDB;

{$R *.dfm}

procedure TfmManager.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date-1;
  Timer1.Enabled:=True;
  ReadIni;

  CommonSet.PathCryst:=Btr1Path;
end;

procedure TfmManager.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStartSS.Execute;
    Close;
  end;
end;

procedure TfmManager.acStartExecute(Sender: TObject);
begin
//
end;

procedure TfmManager.cxButton1Click(Sender: TObject);
begin
  acStartSS.Execute;
end;

procedure TfmManager.acStartSSExecute(Sender: TObject);
Var DateB,DateE,DateCloseBuh:INteger;
begin
  //�������
  with dmMt do
  with dmMC do
  begin
    try
      prWH('������ - ��������� ����.',Memo1);
      PvSQL.Connected:=False;
      PvSQL.Connected:=True;
      if PvSQL.Connected=False then prWH('�� ������� ������������ � ���� ������.',Memo1)
      else
      begin
        prWH('���� �������. �������� ��������� ������.',Memo1);  delay(10);
        prWH('',Memo1);  delay(10);


        DateB:=prGetTA;
        DateE:=trunc(Date);
        if Trunc(cxDateEdit1.Date)<DateB then DateB:=Trunc(cxDateEdit1.Date);
        prWH('-----������ ������� � '+ds1(DateB)+' �� '+ds1(DateE),Memo1);  delay(10);

        DateCloseBuh:=DateB;

        quCloseHBuh.Active:=False;
        quCloseHBuh.Active:=True;
        if quCloseHBuh.RecordCount>0 then
          DateCloseBuh:=Trunc(quCloseHBuhDateClose.AsDateTime);
        quCloseHBuh.Active:=False;


        if DateB<(DateCloseBuh+1) then
        begin
          prWH('   ����������� ������ ����������� � '+ds1(DateCloseBuh+1),Memo1);  delay(10);
          prWH('',Memo1);  delay(10);

          DateB:=DateCloseBuh+1;
        end;

        prRecalcSS(DateB,DateE,Memo1);

        prWH('',Memo1);  delay(10);
//        prWH('----- �������� ��',Memo1);  delay(10);

        if cxCheckBox2.Checked then
        begin
          prWH('----- �������� ��',Memo1);  delay(10);
          prRecalcTO(DateB,DateE,Memo1,0,7); //�������� �������� �������
        end;

        prWH('----- Ok',Memo1);  delay(10);

      end;
    except
      prWH('������ ��������� ������.',Memo1);
    end;
  end;
end;

end.
