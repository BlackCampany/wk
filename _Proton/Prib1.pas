unit Prib1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmPrib1 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridPrib1: TcxGrid;
    LevelPrib1: TcxGridLevel;
    dsteRPrib: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepPrib: TfrReport;
    frteRPrib: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    teRPrib: TdxMemData;
    teRPribIdCode1: TIntegerField;
    teRPribNameC: TStringField;
    teRPribiM: TSmallintField;
    teRPribIdGroup: TIntegerField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teRPribNameGr1: TStringField;
    teRPribNameGr2: TStringField;
    teRPribIdSGroup: TIntegerField;
    ViewPrib1: TcxGridDBTableView;
    ViewPrib1RecId: TcxGridDBColumn;
    ViewPrib1IdCode1: TcxGridDBColumn;
    ViewPrib1NameC: TcxGridDBColumn;
    ViewPrib1iM: TcxGridDBColumn;
    ViewPrib1IdGroup: TcxGridDBColumn;
    ViewPrib1IdSGroup: TcxGridDBColumn;
    ViewPrib1QR: TcxGridDBColumn;
    ViewPrib1NameGr1: TcxGridDBColumn;
    ViewPrib1NameGr2: TcxGridDBColumn;
    teRPribiBrand: TIntegerField;
    teRPribiCat: TSmallintField;
    ViewPrib1iCat: TcxGridDBColumn;
    amRepPrib: TActionManager;
    acMoveRepOb: TAction;
    teRPribNameGr3: TStringField;
    ViewPrib1NameGr3: TcxGridDBColumn;
    teRPribQR: TFloatField;
    teRPribPrice: TFloatField;
    ViewPrib1Price: TcxGridDBColumn;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teRPribsTop: TStringField;
    teRPribsNov: TStringField;
    ViewPrib1sTop: TcxGridDBColumn;
    ViewPrib1sNov: TcxGridDBColumn;
    teRPribRSum: TFloatField;
    ViewPrib1RSum: TcxGridDBColumn;
    teRPribrSumIn: TFloatField;
    teRPribrSumOut: TFloatField;
    teRPribrNac: TFloatField;
    teRPribpNac: TFloatField;
    ViewPrib1rSumIn: TcxGridDBColumn;
    ViewPrib1rQReal: TcxGridDBColumn;
    ViewPrib1rSumOut: TcxGridDBColumn;
    ViewPrib1rNac: TcxGridDBColumn;
    teRPribrQReal: TFloatField;
    ViewPrib1pNac: TcxGridDBColumn;
    teRPribiDep: TIntegerField;
    teRPribsDep: TStringField;
    ViewPrib1sDep: TcxGridDBColumn;
    acPart: TAction;
    teRPribrSumSS: TFloatField;
    teRPribrSumNDS: TFloatField;
    teRPribSInn: TStringField;
    teRPribSPost: TStringField;
    teRPribNDS: TFloatField;
    ViewPrib1rSumSS: TcxGridDBColumn;
    ViewPrib1rSumNDS: TcxGridDBColumn;
    ViewPrib1SInn: TcxGridDBColumn;
    ViewPrib1SPost: TcxGridDBColumn;
    ViewPrib1NDS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure ViewPrib1SelectionChanged(Sender: TcxCustomGridTableView);
    procedure teRPribCalcFields(DataSet: TDataSet);
    procedure ViewPrib1DataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure acPartExecute(Sender: TObject);
    procedure ViewPrib1StartDrag(Sender: TObject;
      var DragObject: TDragObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPrib1: TfmPrib1;

implementation

uses Un1, MDB, MT, Move;

{$R *.dfm}

procedure TfmPrib1.FormCreate(Sender: TObject);
begin
  GridPrib1.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewPrib1.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmPrib1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPrib1.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  teRPrib.Active:=False;
end;

procedure TfmPrib1.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmPrib1.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPrib1.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewPrib1);
end;

procedure TfmPrib1.SpeedItem4Click(Sender: TObject);
//Var StrWk:String;
//    i:Integer;
begin
//������
  ViewPrib1.BeginUpdate;
  ViewPrib1.EndUpdate;
end;

procedure TfmPrib1.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepPrib1') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmPrib1.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmPrib1.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmPrib1.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewPrib1.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teRPribIdCode1.AsInteger;
      prFillMove(teRPribIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teRPribNameC.AsString+'('+teRPribIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmPrib1.ViewPrib1SelectionChanged(Sender: TcxCustomGridTableView);
begin
  if ViewPrib1.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teRPribIdCode1.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmPrib1.teRPribCalcFields(DataSet: TDataSet);
begin
  teRPribRSum.AsFloat:=rv(teRPribQR.AsFloat*teRPribPrice.AsFloat);
end;

procedure TfmPrib1.ViewPrib1DataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  prCalcSumNac(3,4,6,ViewPrib1);
  prCalcDefGroup(3,4,6,-1,ViewPrib1);
end;

procedure TfmPrib1.acPartExecute(Sender: TObject);
begin
  if teRPrib.RecordCount>0 then prFillPart(teRPribIdCode1.AsInteger,teRPribNameC.AsString);
end;

procedure TfmPrib1.ViewPrib1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  iDirect:=9;
end;

end.
