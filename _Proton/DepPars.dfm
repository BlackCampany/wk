object fmDepPars: TfmDepPars
  Left = 684
  Top = 93
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1076#1077#1083#1072
  ClientHeight = 738
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 689
    Width = 479
    Height = 49
    Align = alBottom
    BevelInner = bvLowered
    Color = 16752029
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 96
      Top = 12
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 272
      Top = 12
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLabel1: TcxLabel
    Left = 24
    Top = 24
    Caption = #1054#1090#1076#1077#1083
    Transparent = True
  end
  object cxLabel2: TcxLabel
    Left = 24
    Top = 48
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074' '#1057#1063#1060#1050
    Transparent = True
  end
  object cxLabel3: TcxLabel
    Left = 24
    Top = 76
    Caption = #1048#1053#1053
    Transparent = True
  end
  object cxLabel4: TcxLabel
    Left = 232
    Top = 76
    Caption = #1050#1055#1055
    Transparent = True
  end
  object cxLabel5: TcxLabel
    Left = 24
    Top = 104
    Caption = #1040#1076#1088#1077#1089
    Transparent = True
  end
  object cxLabel6: TcxLabel
    Left = 24
    Top = 132
    Caption = #1043#1088#1091#1079#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
    Transparent = True
  end
  object cxLabel7: TcxLabel
    Left = 28
    Top = 156
    Caption = #1040#1076#1088#1077#1089' '#1075#1088#1091#1079#1086#1086#1090#1087#1088'.'
    Transparent = True
  end
  object cxLabel8: TcxLabel
    Left = 28
    Top = 184
    Caption = #1054#1043#1056#1053
    Transparent = True
  end
  object cxLabel9: TcxLabel
    Left = 28
    Top = 208
    Caption = #1054#1090#1074'. '#1083#1080#1094#1086
    Transparent = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 88
    Top = 20
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 10
    Text = 'cxTextEdit1'
    Width = 229
  end
  object cxTextEdit2: TcxTextEdit
    Left = 132
    Top = 44
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 11
    Text = 'cxTextEdit2'
    Width = 333
  end
  object cxTextEdit3: TcxTextEdit
    Left = 60
    Top = 72
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 12
    Text = 'cxTextEdit3'
    Width = 125
  end
  object cxTextEdit4: TcxTextEdit
    Left = 272
    Top = 72
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 13
    Text = 'cxTextEdit4'
    Width = 129
  end
  object cxTextEdit5: TcxTextEdit
    Left = 68
    Top = 100
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 14
    Text = 'cxTextEdit5'
    Width = 397
  end
  object cxTextEdit6: TcxTextEdit
    Left = 128
    Top = 128
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 15
    Text = 'cxTextEdit6'
    Width = 337
  end
  object cxTextEdit7: TcxTextEdit
    Left = 128
    Top = 152
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 16
    Text = 'cxTextEdit7'
    Width = 337
  end
  object cxTextEdit8: TcxTextEdit
    Left = 72
    Top = 180
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 17
    Text = 'cxTextEdit8'
    Width = 165
  end
  object cxTextEdit9: TcxTextEdit
    Left = 108
    Top = 204
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 19
    Text = 'cxTextEdit9'
    Width = 349
  end
  object cxLabel10: TcxLabel
    Left = 316
    Top = 268
    Caption = #1041#1080#1082
    Transparent = True
  end
  object cxTextEdit10: TcxTextEdit
    Left = 108
    Top = 232
    Properties.MaxLength = 0
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 21
    Text = 'cxTextEdit10'
    Width = 345
  end
  object cxLabel11: TcxLabel
    Left = 28
    Top = 268
    Caption = #1056#1072#1089#1095'.'#1089#1095#1077#1090
    Transparent = True
  end
  object cxTextEdit11: TcxTextEdit
    Left = 108
    Top = 264
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 23
    Text = 'cxTextEdit11'
    Width = 153
  end
  object cxLabel12: TcxLabel
    Left = 28
    Top = 292
    Caption = #1041#1072#1085#1082
    Transparent = True
  end
  object cxTextEdit12: TcxTextEdit
    Left = 108
    Top = 288
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 25
    Text = 'cxTextEdit12'
    Width = 345
  end
  object cxLabel13: TcxLabel
    Left = 28
    Top = 316
    Caption = #1050#1086#1088#1088'. '#1089#1095#1077#1090
    Transparent = True
  end
  object cxTextEdit13: TcxTextEdit
    Left = 108
    Top = 312
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 27
    Text = 'cxTextEdit13'
    Width = 153
  end
  object cxLabel14: TcxLabel
    Left = 28
    Top = 236
    Caption = #1050#1072#1082' '#1092#1080#1079'.'#1083#1080#1094#1086
    Transparent = True
  end
  object cxTextEdit14: TcxTextEdit
    Left = 356
    Top = 264
    Properties.MaxLength = 9
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 29
    Text = 'cxTextEdit14'
    Width = 89
  end
  object cxLabel15: TcxLabel
    Left = 28
    Top = 352
    Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
    Transparent = True
  end
  object cxLabel16: TcxLabel
    Left = 252
    Top = 352
    Caption = #1043#1083'.'#1073#1091#1093#1075#1072#1083#1090#1077#1088
    Transparent = True
  end
  object cxTextEdit15: TcxTextEdit
    Left = 108
    Top = 348
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 32
    Text = 'cxTextEdit15'
    Width = 133
  end
  object cxTextEdit16: TcxTextEdit
    Left = 328
    Top = 348
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 33
    Text = 'cxTextEdit16'
    Width = 133
  end
  object cxTextEdit17: TcxTextEdit
    Left = 108
    Top = 376
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 34
    Text = 'cxTextEdit17'
    Width = 133
  end
  object cxTextEdit18: TcxTextEdit
    Left = 108
    Top = 400
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 35
    Text = 'cxTextEdit18'
    Width = 133
  end
  object cxTextEdit19: TcxTextEdit
    Left = 108
    Top = 424
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 36
    Text = 'cxTextEdit19'
    Width = 133
  end
  object cxTextEdit20: TcxTextEdit
    Left = 328
    Top = 376
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 37
    Text = 'cxTextEdit20'
    Width = 133
  end
  object cxTextEdit21: TcxTextEdit
    Left = 328
    Top = 400
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 38
    Text = 'cxTextEdit21'
    Width = 133
  end
  object cxTextEdit22: TcxTextEdit
    Left = 328
    Top = 424
    Properties.MaxLength = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 39
    Text = 'cxTextEdit22'
    Width = 133
  end
  object cxLabel17: TcxLabel
    Left = 52
    Top = 380
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Transparent = True
  end
  object cxLabel18: TcxLabel
    Left = 268
    Top = 376
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Transparent = True
  end
  object cxLabel19: TcxLabel
    Left = 52
    Top = 404
    Caption = #1048#1084#1103
    Transparent = True
  end
  object cxLabel20: TcxLabel
    Left = 268
    Top = 400
    Caption = #1048#1084#1103
    Transparent = True
  end
  object cxLabel21: TcxLabel
    Left = 52
    Top = 428
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
    Transparent = True
  end
  object cxLabel22: TcxLabel
    Left = 268
    Top = 428
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
    Transparent = True
  end
  object cxLabel23: TcxLabel
    Left = 28
    Top = 464
    Caption = #1059#1083#1080#1094#1072
    Transparent = True
  end
  object cxTextEdit23: TcxTextEdit
    Left = 108
    Top = 460
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 47
    Text = 'cxTextEdit23'
    Width = 345
  end
  object cxLabel24: TcxLabel
    Left = 28
    Top = 488
    Caption = #1044#1086#1084
    Transparent = True
  end
  object cxTextEdit24: TcxTextEdit
    Left = 108
    Top = 484
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 49
    Text = 'cxTextEdit24'
    Width = 57
  end
  object cxLabel25: TcxLabel
    Left = 168
    Top = 488
    Caption = #1050#1086#1088#1087
    Transparent = True
  end
  object cxTextEdit25: TcxTextEdit
    Left = 200
    Top = 484
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 51
    Text = 'cxTextEdit25'
    Width = 41
  end
  object cxLabel26: TcxLabel
    Left = 248
    Top = 488
    Caption = #1051#1080#1090#1077#1088#1072
    Transparent = True
  end
  object cxTextEdit26: TcxTextEdit
    Left = 288
    Top = 484
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 53
    Text = 'cxTextEdit26'
    Width = 33
  end
  object cxLabel27: TcxLabel
    Left = 28
    Top = 552
    Caption = #1042#1080#1076' '#1083#1080#1094#1077#1085#1079#1080#1080
    Transparent = True
  end
  object cxLabel28: TcxLabel
    Left = 332
    Top = 488
    Caption = #1048#1085#1076#1077#1082#1089
    Transparent = True
  end
  object cxTextEdit27: TcxTextEdit
    Left = 376
    Top = 484
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 56
    Text = 'cxTextEdit27'
    Width = 77
  end
  object cxTextEdit28: TcxTextEdit
    Left = 108
    Top = 548
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 57
    Text = 'cxTextEdit28'
    Width = 349
  end
  object cxLabel29: TcxLabel
    Left = 28
    Top = 580
    Caption = #1057#1077#1088#1080#1103
    Transparent = True
  end
  object cxTextEdit29: TcxTextEdit
    Left = 108
    Top = 576
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 59
    Text = 'cxTextEdit28'
    Width = 149
  end
  object cxLabel30: TcxLabel
    Left = 264
    Top = 580
    Caption = #1053#1086#1084#1077#1088
    Transparent = True
  end
  object cxTextEdit30: TcxTextEdit
    Left = 308
    Top = 576
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 61
    Text = 'cxTextEdit29'
    Width = 149
  end
  object cxLabel31: TcxLabel
    Left = 28
    Top = 608
    Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    Transparent = True
  end
  object cxDateEdit1: TcxDateEdit
    Left = 112
    Top = 608
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 63
    Width = 121
  end
  object cxLabel32: TcxLabel
    Left = 240
    Top = 612
    Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
    Transparent = True
  end
  object cxDateEdit2: TcxDateEdit
    Left = 336
    Top = 608
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 65
    Width = 121
  end
  object cxLabel33: TcxLabel
    Left = 28
    Top = 520
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Transparent = True
  end
  object cxTextEdit31: TcxTextEdit
    Left = 108
    Top = 516
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 67
    Text = 'cxTextEdit31'
    Width = 149
  end
  object cxLabel34: TcxLabel
    Left = 264
    Top = 520
    Caption = 'EMail'
    Transparent = True
  end
  object cxTextEdit32: TcxTextEdit
    Left = 308
    Top = 516
    Properties.MaxLength = 150
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 69
    Text = 'cxTextEdit32'
    Width = 149
  end
  object cxLabel35: TcxLabel
    Left = 280
    Top = 184
    Caption = 'GLN'
    Transparent = True
  end
  object cxTextEdit33: TcxTextEdit
    Left = 316
    Top = 180
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 18
    Text = 'cxTextEdit33'
    Width = 141
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16752029
    BkColor.EndColor = 16770790
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 412
    Top = 212
  end
end
