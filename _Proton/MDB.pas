unit MDB;

interface

uses
  SysUtils, Classes, DB, pvtables, pvsqltables, btvtables, sqldataset, ComCtrls,
  ImgList, Controls, cxStyles,cxGridDBTableView, frexpimg, frRtfExp,
  frTXTExp, frXMLExl, frOLEExl, frHTMExp, FR_E_HTML2, FR_E_HTM, FR_E_CSV,
  FR_E_RTF, FR_Class, FR_E_TXT, dxmdaset, FR_BarC, FR_DSet, FR_DBSet,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, Forms, cxMemo,
  IdFTP,FileUtil,Un1;

type
  TdmMC = class(TDataModule)
    quPassw: TPvQuery;
    PvSQL: TPvSqlDatabase;
    quPasswID: TIntegerField;
    quPasswID_PARENT: TIntegerField;
    quPasswNAME: TStringField;
    quPasswUVOLNEN: TSmallintField;
    quPasswPASSW: TStringField;
    quPasswMODUL1: TSmallintField;
    quPasswMODUL2: TSmallintField;
    quPasswMODUL3: TSmallintField;
    quPasswMODUL4: TSmallintField;
    quPasswMODUL5: TSmallintField;
    quPasswMODUL6: TSmallintField;
    quPasswBARCODE: TStringField;
    dsquPassw: TDataSource;
    quGr: TPvQuery;
    quGrID: TSmallintField;
    quGrName: TStringField;
    quSGr: TPvQuery;
    SmallintField1: TSmallintField;
    StringField1: TStringField;
    quCountry: TPvQuery;
    quCountryID: TSmallintField;
    quCountryName: TStringField;
    dsquCountry: TDataSource;
    quCanDo: TPvQuery;
    quCanDoPREXEC: TSmallintField;
    quA: TPvQuery;
    quC: TPvQuery;
    quCRecCount: TAutoIncField;
    quE: TPvQuery;
    quD: TPvQuery;
    quBrands: TPvQuery;
    quBrandsID: TIntegerField;
    quBrandsNAMEBRAND: TStringField;
    dsquBrands: TDataSource;
    quMakers: TPvQuery;
    dsquMakers: TDataSource;
    quDeparts: TPvQuery;
    dsquDeparts: TDataSource;
    quDepartsID: TSmallintField;
    quDepartsName: TStringField;
    quDepartsStatus1: TSmallintField;
    quDepartsStatus2: TSmallintField;
    quDepartsParentID: TSmallintField;
    quDepartsObjectTypeID: TSmallintField;
    quDepParent: TPvQuery;
    quDepParentID: TSmallintField;
    quEU: TPvQuery;
    dsquEU: TDataSource;
    quEUCode: TSmallintField;
    quEUName: TStringField;
    quEUProcent1: TFloatField;
    quEUProcent2: TFloatField;
    quEUProcent3: TFloatField;
    quEUProcent4: TFloatField;
    quEUMax: TPvQuery;
    quEUMaxId: TSmallintField;
    quCards: TPvQuery;
    quCardsID: TIntegerField;
    quCardsName: TStringField;
    quCardsBarCode: TStringField;
    quCardsShRealize: TSmallintField;
    quCardsTovarType: TSmallintField;
    quCardsEdIzm: TSmallintField;
    quCardsNDS: TFloatField;
    quCardsOKDP: TFloatField;
    quCardsWeght: TFloatField;
    quCardsSrokReal: TSmallintField;
    quCardsTareWeight: TSmallintField;
    quCardsKritOst: TFloatField;
    quCardsBHTStatus: TSmallintField;
    quCardsUKMAction: TSmallintField;
    quCardsDataChange: TDateField;
    quCardsNSP: TFloatField;
    quCardsWasteRate: TFloatField;
    quCardsCena: TFloatField;
    quCardsKol: TFloatField;
    quCardsFullName: TStringField;
    quCardsCountry: TSmallintField;
    quCardsStatus: TSmallintField;
    quCardsPrsision: TFloatField;
    quCardsPriceState: TWordField;
    quCardsSetOfGoods: TWordField;
    quCardsPrePacking: TStringField;
    quCardsMargGroup: TStringField;
    quCardsObjectTypeID: TSmallintField;
    quCardsFixPrice: TFloatField;
    quCardsPrintLabel: TWordField;
    quCardsImageID: TIntegerField;
    quCardsNAMEBRAND: TStringField;
    dsquCards: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    quGrFind: TPvQuery;
    quGrFindGoodsGroupID: TSmallintField;
    quCardsNameCountry: TStringField;
    quCardsGoodsGroupID: TSmallintField;
    quCardsSubGroupID: TSmallintField;
    quGrFindName: TStringField;
    quCardsV01: TIntegerField;
    quCardsV02: TIntegerField;
    quCardsV03: TIntegerField;
    quCardsV04: TIntegerField;
    quId: TPvQuery;
    quIdID: TIntegerField;
    quFindBar: TPvQuery;
    quFindBarID: TStringField;
    quFindEU: TPvQuery;
    quFindEUCode: TSmallintField;
    quFindEUCodeTovar: TIntegerField;
    quFindEUName: TStringField;
    dsquEU1: TDataSource;
    quFindC: TPvQuery;
    quFindCID: TIntegerField;
    quFindCName: TStringField;
    quFindCBarCode: TStringField;
    quFindCTovarType: TSmallintField;
    quFindCEdIzm: TSmallintField;
    quFindCCena: TFloatField;
    quFindCStatus: TSmallintField;
    quFindCSubGroupName: TStringField;
    dsquFindC: TDataSource;
    quFindCGoodsGroupID: TSmallintField;
    quFindCSubGroupID: TSmallintField;
    quFindBarGoodsID: TIntegerField;
    quM: TPvQuery;
    quBars: TPvQuery;
    quBarsGoodsID: TIntegerField;
    quBarsID: TStringField;
    quBarsQuant: TFloatField;
    quBarsBarFormat: TSmallintField;
    quBarsDateChange: TDateField;
    quBarsBarStatus: TSmallintField;
    quBarsCost: TFloatField;
    quBarsStatus: TSmallintField;
    dsquBars: TDataSource;
    dsquBrands1: TDataSource;
    quCateg: TPvQuery;
    quCategID: TIntegerField;
    quCategSID: TStringField;
    quCategCOMMENT: TStringField;
    dsquCateg: TDataSource;
    quTTnIn: TPvQuery;
    dsquTTnIn: TDataSource;
    quTTnInDepart: TSmallintField;
    quTTnInDateInvoice: TDateField;
    quTTnInNumber: TStringField;
    quTTnInSCFNumber: TStringField;
    quTTnInIndexPost: TSmallintField;
    quTTnInCodePost: TSmallintField;
    quTTnInSummaTovarPost: TFloatField;
    quTTnInReturnTovar: TFloatField;
    quTTnInSummaTovar: TFloatField;
    quTTnInNacenka: TFloatField;
    quTTnInReturnTara: TFloatField;
    quTTnInSummaTara: TFloatField;
    quTTnInAcStatus: TWordField;
    quTTnInChekBuh: TWordField;
    quTTnInSCFDate: TDateField;
    quTTnInOrderNumber: TIntegerField;
    quTTnInID: TIntegerField;
    quTTnInNamePost: TStringField;
    quTTnInNameInd: TStringField;
    quTTnInNameDep: TStringField;
    quTTnInNameCli: TStringField;
    dsquDeparts1: TDataSource;
    quSpecIn: TPvQuery;
    quSpecInDepart: TSmallintField;
    quSpecInDateInvoice: TDateField;
    quSpecInNumber: TStringField;
    quSpecInIndexPost: TSmallintField;
    quSpecInCodePost: TSmallintField;
    quSpecInCodeGroup: TSmallintField;
    quSpecInCodePodgr: TSmallintField;
    quSpecInCodeTovar: TIntegerField;
    quSpecInCodeEdIzm: TSmallintField;
    quSpecInTovarType: TSmallintField;
    quSpecInBarCode: TStringField;
    quSpecInMediatorCost: TFloatField;
    quSpecInNDSProc: TFloatField;
    quSpecInNDSSum: TFloatField;
    quSpecInOutNDSSum: TFloatField;
    quSpecInBestBefore: TDateField;
    quSpecInInvoiceQuant: TFloatField;
    quSpecInKolMest: TIntegerField;
    quSpecInKolEdMest: TFloatField;
    quSpecInKolWithMest: TFloatField;
    quSpecInKolWithNecond: TFloatField;
    quSpecInKol: TFloatField;
    quSpecInCenaTovar: TFloatField;
    quSpecInProcent: TFloatField;
    quSpecInNewCenaTovar: TFloatField;
    quSpecInSumCenaTovarPost: TFloatField;
    quSpecInSumCenaTovar: TFloatField;
    quSpecInProcentN: TFloatField;
    quSpecInNecond: TFloatField;
    quSpecInCenaNecond: TFloatField;
    quSpecInSumNecond: TFloatField;
    quSpecInProcentZ: TFloatField;
    quSpecInZemlia: TFloatField;
    quSpecInProcentO: TFloatField;
    quSpecInOthodi: TFloatField;
    quSpecInCodeTara: TIntegerField;
    quSpecInVesTara: TFloatField;
    quSpecInCenaTara: TFloatField;
    quSpecInSumVesTara: TFloatField;
    quSpecInSumCenaTara: TFloatField;
    quSpecInChekBuh: TBooleanField;
    quSpecInSertBeginDate: TDateField;
    quSpecInSertEndDate: TDateField;
    quSpecInSertNumber: TStringField;
    quSpecInName: TStringField;
    quCId: TPvQuery;
    quCIdID: TIntegerField;
    quCIdName: TStringField;
    quCIdBarCode: TStringField;
    quCIdShRealize: TSmallintField;
    quCIdTovarType: TSmallintField;
    quCIdEdIzm: TSmallintField;
    quCIdNDS: TFloatField;
    quCIdOKDP: TFloatField;
    quCIdWeght: TFloatField;
    quCIdSrokReal: TSmallintField;
    quCIdTareWeight: TSmallintField;
    quCIdKritOst: TFloatField;
    quCIdBHTStatus: TSmallintField;
    quCIdUKMAction: TSmallintField;
    quCIdDataChange: TDateField;
    quCIdNSP: TFloatField;
    quCIdWasteRate: TFloatField;
    quCIdCena: TFloatField;
    quCIdKol: TFloatField;
    quCIdFullName: TStringField;
    quCIdCountry: TSmallintField;
    quCIdStatus: TSmallintField;
    quCIdPrsision: TFloatField;
    quCIdPriceState: TWordField;
    quCIdSetOfGoods: TWordField;
    quCIdPrePacking: TStringField;
    quCIdMargGroup: TStringField;
    quCIdObjectTypeID: TSmallintField;
    quCIdFixPrice: TFloatField;
    quCIdPrintLabel: TWordField;
    quCIdImageID: TIntegerField;
    quCIdGoodsGroupID: TSmallintField;
    quCIdSubGroupID: TSmallintField;
    quCIdV01: TIntegerField;
    quCIdV02: TIntegerField;
    quCIdV03: TIntegerField;
    quCIdV04: TIntegerField;
    quCli: TPvQuery;
    dsquCli: TDataSource;
    quCliVendor: TSmallintField;
    quCliName: TStringField;
    quCliPayDays: TSmallintField;
    quCliPayForm: TWordField;
    quCliPayWaste: TWordField;
    quCliFirmName: TStringField;
    quCliPhone1: TStringField;
    quCliPhone2: TStringField;
    quCliPhone3: TStringField;
    quCliPhone4: TStringField;
    quCliFax: TStringField;
    quCliTreatyDate: TDateField;
    quCliTreatyNumber: TStringField;
    quCliPostIndex: TStringField;
    quCliGorod: TStringField;
    quCliRaion: TSmallintField;
    quCliStreet: TStringField;
    quCliHouse: TStringField;
    quCliBuild: TStringField;
    quCliKvart: TStringField;
    quCliOKPO: TStringField;
    quCliOKONH: TStringField;
    quCliINN: TStringField;
    quCliFullName: TStringField;
    quCliCodeUchet: TStringField;
    quCliUnTaxedNDS: TWordField;
    quCliStatusByte: TWordField;
    quCliStatus: TSmallintField;
    quIP: TPvQuery;
    dsquIP: TDataSource;
    quIPCode: TSmallintField;
    quIPName: TStringField;
    quIPFirmName: TStringField;
    quIPFamilia: TStringField;
    quIPIma: TStringField;
    quIPOtchestvo: TStringField;
    quIPVidDok: TStringField;
    quIPSerPasp: TStringField;
    quIPNumberPasp: TIntegerField;
    quIPKemPasp: TStringField;
    quIPDatePasp: TDateField;
    quIPCountry: TStringField;
    quIPPhone1: TStringField;
    quIPPhone2: TStringField;
    quIPPhone3: TStringField;
    quIPPhone4: TStringField;
    quIPFax: TStringField;
    quIPEMail: TStringField;
    quIPPostIndex: TStringField;
    quIPGorod: TStringField;
    quIPRaion: TSmallintField;
    quIPStreet: TStringField;
    quIPHouse: TStringField;
    quIPBuild: TStringField;
    quIPKvart: TStringField;
    quIPINN: TStringField;
    quIPNStrach: TStringField;
    quIPNumberSvid: TStringField;
    quIPDateSvid: TDateField;
    quIPFullName: TStringField;
    quIPDateBorn: TDateField;
    quIPCodeUchet: TStringField;
    quIPStatus: TIntegerField;
    cxStyle23: TcxStyle;
    quFindCNDS: TFloatField;
    quFindC1: TPvQuery;
    quFindC1ID: TIntegerField;
    quFindC1Name: TStringField;
    quFindC1BarCode: TStringField;
    quFindC1TovarType: TSmallintField;
    quFindC1EdIzm: TSmallintField;
    quFindC1NDS: TFloatField;
    quFindC1Cena: TFloatField;
    quFindC1Status: TSmallintField;
    quFindT: TPvQuery;
    quFindTCode: TSmallintField;
    quFindTName: TStringField;
    quTara: TPvQuery;
    dsquTara: TDataSource;
    quTaraCode: TSmallintField;
    quTaraName: TStringField;
    quTTnOut: TPvQuery;
    dsquTTnOut: TDataSource;
    quTTnOutDepart: TSmallintField;
    quTTnOutDateInvoice: TDateField;
    quTTnOutNumber: TStringField;
    quTTnOutSCFNumber: TStringField;
    quTTnOutIndexPoluch: TSmallintField;
    quTTnOutCodePoluch: TSmallintField;
    quTTnOutSummaTovar: TFloatField;
    quTTnOutSummaTovarSpis: TFloatField;
    quTTnOutNacenkaTovar: TFloatField;
    quTTnOutSummaTara: TFloatField;
    quTTnOutSummaTaraSpis: TFloatField;
    quTTnOutNacenkaTara: TFloatField;
    quTTnOutAcStatus: TWordField;
    quTTnOutChekBuh: TWordField;
    quTTnOutSCFDate: TDateField;
    quTTnOutID: TIntegerField;
    quTTnOutNamePost: TStringField;
    quTTnOutNameInd: TStringField;
    quTTnOutNameDep: TStringField;
    quTTnOutNameCli: TStringField;
    quFCli: TPvQuery;
    quFIP: TPvQuery;
    quFCliVendor: TSmallintField;
    quFIPCode: TSmallintField;
    quFCliName: TStringField;
    quFIPName: TStringField;
    quSpecOut: TPvQuery;
    quSpecOutDepart: TSmallintField;
    quSpecOutDateInvoice: TDateField;
    quSpecOutNumber: TStringField;
    quSpecOutCodeGroup: TSmallintField;
    quSpecOutCodePodgr: TSmallintField;
    quSpecOutCodeTovar: TIntegerField;
    quSpecOutCodeEdIzm: TSmallintField;
    quSpecOutTovarType: TSmallintField;
    quSpecOutBarCode: TStringField;
    quSpecOutOKDP: TFloatField;
    quSpecOutNDSProc: TFloatField;
    quSpecOutNDSSum: TFloatField;
    quSpecOutOutNDSSum: TFloatField;
    quSpecOutAkciz: TFloatField;
    quSpecOutKolMest: TIntegerField;
    quSpecOutKolEdMest: TFloatField;
    quSpecOutKolWithMest: TFloatField;
    quSpecOutKol: TFloatField;
    quSpecOutCenaTovar: TFloatField;
    quSpecOutCenaTovarSpis: TFloatField;
    quSpecOutCenaPost: TFloatField;
    quSpecOutSumCenaTovarSpis: TFloatField;
    quSpecOutSumCenaTovar: TFloatField;
    quSpecOutCodeTara: TIntegerField;
    quSpecOutVesTara: TFloatField;
    quSpecOutCenaTara: TFloatField;
    quSpecOutCenaTaraSpis: TFloatField;
    quSpecOutSumVesTara: TFloatField;
    quSpecOutSumCenaTara: TFloatField;
    quSpecOutSumCenaTaraSpis: TFloatField;
    quSpecOutChekBuh: TBooleanField;
    quSpecOutName: TStringField;
    quFindC2: TPvQuery;
    quFindC2GoodsGroupID: TSmallintField;
    quFindC2SubGroupID: TSmallintField;
    quFC: TPvQuery;
    quFCCena: TFloatField;
    quSpecInCena: TFloatField;
    quFLastPrice: TPvQuery;
    quFLastPriceCenaTovar: TFloatField;
    quFLastPriceNewCenaTovar: TFloatField;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frHTMLTableExport1: TfrHTMLTableExport;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBMPExport1: TfrBMPExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    quSpecOutFullName: TStringField;
    quDeparts1: TPvQuery;
    quDeparts1ID: TSmallintField;
    quDeparts1Name: TStringField;
    quDeparts1FullName: TStringField;
    quDeparts1EnglishFullName: TStringField;
    quFCli1: TPvQuery;
    quFCli1Vendor: TSmallintField;
    quFCli1Name: TStringField;
    quFCli1PayDays: TSmallintField;
    quFCli1PayForm: TWordField;
    quFCli1PayWaste: TWordField;
    quFCli1FirmName: TStringField;
    quFCli1Phone1: TStringField;
    quFCli1Phone2: TStringField;
    quFCli1Phone3: TStringField;
    quFCli1Phone4: TStringField;
    quFCli1Fax: TStringField;
    quFCli1TreatyDate: TDateField;
    quFCli1TreatyNumber: TStringField;
    quFCli1PostIndex: TStringField;
    quFCli1Gorod: TStringField;
    quFCli1Raion: TSmallintField;
    quFCli1Street: TStringField;
    quFCli1House: TStringField;
    quFCli1Build: TStringField;
    quFCli1Kvart: TStringField;
    quFCli1OKPO: TStringField;
    quFCli1OKONH: TStringField;
    quFCli1INN: TStringField;
    quFCli1FullName: TStringField;
    quFCli1CodeUchet: TStringField;
    quFCli1UnTaxedNDS: TWordField;
    quFCli1StatusByte: TWordField;
    quFCli1Status: TSmallintField;
    quFIP1: TPvQuery;
    quFIP1Code: TSmallintField;
    quFIP1Name: TStringField;
    quFIP1FirmName: TStringField;
    quFIP1Familia: TStringField;
    quFIP1Ima: TStringField;
    quFIP1Otchestvo: TStringField;
    quFIP1VidDok: TStringField;
    quFIP1SerPasp: TStringField;
    quFIP1NumberPasp: TIntegerField;
    quFIP1KemPasp: TStringField;
    quFIP1DatePasp: TDateField;
    quFIP1Country: TStringField;
    quFIP1Phone1: TStringField;
    quFIP1Phone2: TStringField;
    quFIP1Phone3: TStringField;
    quFIP1Phone4: TStringField;
    quFIP1Fax: TStringField;
    quFIP1EMail: TStringField;
    quFIP1PostIndex: TStringField;
    quFIP1Gorod: TStringField;
    quFIP1Raion: TSmallintField;
    quFIP1Street: TStringField;
    quFIP1House: TStringField;
    quFIP1Build: TStringField;
    quFIP1Kvart: TStringField;
    quFIP1INN: TStringField;
    quFIP1NStrach: TStringField;
    quFIP1NumberSvid: TStringField;
    quFIP1DateSvid: TDateField;
    quFIP1FullName: TStringField;
    quFIP1DateBorn: TDateField;
    quFIP1CodeUchet: TStringField;
    quFIP1Status: TIntegerField;
    quMiMax: TIntegerField;
    quTTNAC: TPvQuery;
    dsquTTNAC: TDataSource;
    quTTNACDepart: TSmallintField;
    quTTNACDateAct: TDateField;
    quTTNACNumber: TStringField;
    quTTNACMatOldSum: TFloatField;
    quTTNACMatNewSum: TFloatField;
    quTTNACMatRaznica: TFloatField;
    quTTNACRaznOldSum: TFloatField;
    quTTNACRaznNewSum: TFloatField;
    quTTNACRaznRaznica: TFloatField;
    quTTNACChekBuh: TWordField;
    quTTNACNDS10OldSum: TFloatField;
    quTTNACNDS10NewSum: TFloatField;
    quTTNACNDS10Raznica: TFloatField;
    quTTNACNDS20OldSum: TFloatField;
    quTTNACNDS20NewSum: TFloatField;
    quTTNACNDS20Raznica: TFloatField;
    quTTNACAcStatus: TWordField;
    quTTNACNDS0OldSum: TFloatField;
    quTTNACNDS0NewSum: TFloatField;
    quTTNACNDS0Raznica: TFloatField;
    quTTNACNSP10OldSum: TFloatField;
    quTTNACNSP10NewSum: TFloatField;
    quTTNACNSP10Raznica: TFloatField;
    quTTNACNSP20OldSum: TFloatField;
    quTTNACNSP20NewSum: TFloatField;
    quTTNACNSP20Raznica: TFloatField;
    quTTNACNSP0OldSum: TFloatField;
    quTTNACNSP0NewSum: TFloatField;
    quTTNACNSP0Raznica: TFloatField;
    quTTNACID: TIntegerField;
    quTTNACLinkInvoice: TWordField;
    quTTNACStartTransfer: TWordField;
    quTTNACEndTransfer: TWordField;
    quTTNACrezerv: TWordField;
    quTTNACNameDep: TStringField;
    quSpecAc: TPvQuery;
    quSpecAcDepart: TSmallintField;
    quSpecAcDateAct: TDateField;
    quSpecAcNumber: TStringField;
    quSpecAcGGroup: TSmallintField;
    quSpecAcPodgr: TSmallintField;
    quSpecAcTovar: TIntegerField;
    quSpecAcEdIzm: TSmallintField;
    quSpecAcTovarType: TSmallintField;
    quSpecAcBarCode: TStringField;
    quSpecAcKol: TFloatField;
    quSpecAcPostCenaOldSum: TFloatField;
    quSpecAcPostCenaNewSum: TFloatField;
    quSpecAcPostCenaRaznica: TFloatField;
    quSpecAcMatCenaOldSum: TFloatField;
    quSpecAcMatCenaNewSum: TFloatField;
    quSpecAcMatCenaRaznica: TFloatField;
    quSpecAcRaznCenaOldSum: TFloatField;
    quSpecAcRaznCenaNewSum: TFloatField;
    quSpecAcRaznCenaRaznica: TFloatField;
    quSpecAcPostSumOldSum: TFloatField;
    quSpecAcPostSumNewSum: TFloatField;
    quSpecAcPostSumRaznica: TFloatField;
    quSpecAcMatSumOldSum: TFloatField;
    quSpecAcMatSumNewSum: TFloatField;
    quSpecAcMatSumRaznica: TFloatField;
    quSpecAcRaznSumOldSum: TFloatField;
    quSpecAcRaznSumNewSum: TFloatField;
    quSpecAcRaznSumRaznica: TFloatField;
    quSpecAcName: TStringField;
    quSpecAcFullName: TStringField;
    quFindC1FullName: TStringField;
    quFindCFullName: TStringField;
    quTTnVn: TPvQuery;
    dsquTTnVn: TDataSource;
    quTTnVnDepart: TSmallintField;
    quTTnVnDateInvoice: TDateField;
    quTTnVnNumber: TStringField;
    quTTnVnFlowDepart: TSmallintField;
    quTTnVnSummaTovarSpis: TFloatField;
    quTTnVnSummaTovarMove: TFloatField;
    quTTnVnSummaTovarNew: TFloatField;
    quTTnVnSummaTara: TFloatField;
    quTTnVnOprihod: TBooleanField;
    quTTnVnAcStatusP: TWordField;
    quTTnVnChekBuhP: TWordField;
    quTTnVnAcStatusR: TWordField;
    quTTnVnChekBuhR: TWordField;
    quTTnVnProvodTypeP: TWordField;
    quTTnVnProvodTypeR: TWordField;
    quTTnVnGoodsSpisNDS10: TFloatField;
    quTTnVnGoodsSpisNDS20: TFloatField;
    quTTnVnGoodsMoveNDS10: TFloatField;
    quTTnVnGoodsMoveNDS20: TFloatField;
    quTTnVnGoodsNewNDS10: TFloatField;
    quTTnVnGoodsNewNDS20: TFloatField;
    quTTnVnGoodsLgtNDSSpis: TFloatField;
    quTTnVnGoodsLgtNDSMove: TFloatField;
    quTTnVnGoodsLgtNDSNew: TFloatField;
    quTTnVnGoodsNSPSpis10: TFloatField;
    quTTnVnGoodsNSPSpis20: TFloatField;
    quTTnVnGoodsNSPNew10: TFloatField;
    quTTnVnGoodsNSPNew20: TFloatField;
    quTTnVnGoodsNSP0Spis: TFloatField;
    quTTnVnGoodsNSP0New: TFloatField;
    quTTnVnID: TIntegerField;
    quTTnVnLinkInvoice: TWordField;
    quTTnVnStartTransfer: TWordField;
    quTTnVnEndTransfer: TWordField;
    quTTnVnRezerv: TStringField;
    quTTnVnNameD1: TStringField;
    quTTnVnNameD2: TStringField;
    quSpecVn: TPvQuery;
    quSpecVnDepart: TSmallintField;
    quSpecVnDateInvoice: TDateField;
    quSpecVnNumber: TStringField;
    quSpecVnCodeGroup: TSmallintField;
    quSpecVnCodePodgr: TSmallintField;
    quSpecVnCodeTovar: TIntegerField;
    quSpecVnCodeEdIzm: TSmallintField;
    quSpecVnTovarType: TSmallintField;
    quSpecVnBarCode: TStringField;
    quSpecVnKolMest: TIntegerField;
    quSpecVnKolEdMest: TFloatField;
    quSpecVnKolWithMest: TFloatField;
    quSpecVnKol: TFloatField;
    quSpecVnCenaTovarMove: TFloatField;
    quSpecVnCenaTovarSpis: TFloatField;
    quSpecVnProcent: TFloatField;
    quSpecVnNewCenaTovar: TFloatField;
    quSpecVnSumCenaTovarSpis: TFloatField;
    quSpecVnSumNewCenaTovar: TFloatField;
    quSpecVnCodeTara: TIntegerField;
    quSpecVnVesTara: TFloatField;
    quSpecVnCenaTara: TFloatField;
    quSpecVnSumVesTara: TFloatField;
    quSpecVnSumCenaTara: TFloatField;
    quSpecVnChekBuhPrihod: TBooleanField;
    quSpecVnChekBuhRashod: TBooleanField;
    quSpecVnName: TStringField;
    quSpecVnFullName: TStringField;
    quSpecIn1: TPvQuery;
    quSpecIn1Depart: TSmallintField;
    quSpecIn1CodeTovar: TIntegerField;
    quSpecIn1Kol: TFloatField;
    quSpecIn1CenaTovar: TFloatField;
    quSpecIn1NewCenaTovar: TFloatField;
    quSpecIn1SumCenaTovarPost: TFloatField;
    quSpecIn1SumCenaTovar: TFloatField;
    quBufPr: TPvQuery;
    quBufPrIDCARD: TIntegerField;
    quBufPrIDDOC: TIntegerField;
    quBufPrTYPEDOC: TSmallintField;
    quBufPrNEWP: TFloatField;
    quBufPrSTATUS: TSmallintField;
    quBufPrNUMDOC: TStringField;
    quBufPrDATEDOC: TDateField;
    quBufPrName: TStringField;
    quBufPrCena: TFloatField;
    dsquBufPr: TDataSource;
    quCashList: TPvQuery;
    dsquCashList: TDataSource;
    quCash: TPvQuery;
    quCashNumber: TIntegerField;
    quCashName: TStringField;
    quCashShRealiz: TIntegerField;
    quCashAddGoods: TIntegerField;
    quCashTechnolog: TIntegerField;
    quCashFlags: TSmallintField;
    quCashRezerv: TFloatField;
    quCashShopIndex: TSmallintField;
    quCashLoad: TSmallintField;
    quCashTake: TSmallintField;
    quFindC3: TPvQuery;
    quFindC3ID: TIntegerField;
    quFindC3Name: TStringField;
    quFindC3FullName: TStringField;
    quFindC3BarCode: TStringField;
    quFindC3TovarType: TSmallintField;
    quFindC3EdIzm: TSmallintField;
    quFindC3Cena: TFloatField;
    quFindC3Status: TSmallintField;
    quFindC3GoodsGroupID: TSmallintField;
    quFindC3SubGroupID: TSmallintField;
    quBufPrOLDP: TFloatField;
    taCen: TdxMemData;
    taCenIdCard: TIntegerField;
    taCenFullName: TStringField;
    taCenCountry: TStringField;
    taCenPrice1: TFloatField;
    taCenPrice2: TFloatField;
    taCenDiscount: TFloatField;
    taCenBarCode: TStringField;
    taCenEdIzm: TSmallintField;
    dstaCen: TDataSource;
    quFindC4: TPvQuery;
    quFindC4ID: TIntegerField;
    quFindC4Name: TStringField;
    quFindC4FullName: TStringField;
    quFindC4BarCode: TStringField;
    quFindC4TovarType: TSmallintField;
    quFindC4EdIzm: TSmallintField;
    quFindC4Cena: TFloatField;
    quFindC4Status: TSmallintField;
    quFindC4GoodsGroupID: TSmallintField;
    quFindC4SubGroupID: TSmallintField;
    quFindC4Country: TSmallintField;
    quFindC4NameCu: TStringField;
    frBarCodeObject1: TfrBarCodeObject;
    RepCenn: TfrReport;
    frtaCen: TfrDBDataSet;
    quCG: TPvQuery;
    quCM: TPvQuery;
    quCGCARD: TIntegerField;
    quCGITEM: TIntegerField;
    quCGDEPART: TSmallintField;
    quCMQUANT_REST: TFloatField;
    quRemn: TPvQuery;
    quRemnCARD: TIntegerField;
    quRemnITEM: TIntegerField;
    quRemnDEPART: TSmallintField;
    quRemnName: TStringField;
    dsquRemn: TDataSource;
    quRemnRemn: TFloatField;
    quMove: TPvQuery;
    quMoveCARD: TIntegerField;
    quMoveMOVEDATE: TDateField;
    quMoveDOC_TYPE: TSmallintField;
    quMoveDOCUMENT: TIntegerField;
    quMoveQUANT_MOVE: TFloatField;
    quMoveQUANT_REST: TFloatField;
    quMoveName: TStringField;
    quMoveDEPART: TSmallintField;
    dsquMove: TDataSource;
    quCardsReserv1: TFloatField;
    quPool: TPvQuery;
    quPoolCodeObject: TSmallintField;
    quPoolId: TIntegerField;
    quM1: TPvQuery;
    quM1CARD: TIntegerField;
    quM1MOVEDATE: TDateField;
    quM1DOC_TYPE: TSmallintField;
    quM1DOCUMENT: TIntegerField;
    quM1PRICE: TFloatField;
    quM1QUANT_MOVE: TFloatField;
    quM1SUM_MOVE: TFloatField;
    quM1QUANT_REST: TFloatField;
    quCM1: TPvQuery;
    quCM1QUANT_REST: TFloatField;
    quCM2: TPvQuery;
    quCM2CARD: TIntegerField;
    quCM2MOVEDATE: TDateField;
    quCM2DOC_TYPE: TSmallintField;
    quCM2DOCUMENT: TIntegerField;
    quCM2PRICE: TFloatField;
    quCM2QUANT_MOVE: TFloatField;
    quCM2SUM_MOVE: TFloatField;
    quCM2QUANT_REST: TFloatField;
    quSpecOut1: TPvQuery;
    quSpecOut1Depart: TSmallintField;
    quSpecOut1CodeTovar: TIntegerField;
    quSpecOut1Kol: TFloatField;
    quCashRep: TPvQuery;
    dsquCashRep: TDataSource;
    quCashRepCashNumber: TIntegerField;
    quCashRepZNumber: TIntegerField;
    quCashRepZSale: TFloatField;
    quCashRepZReturn: TFloatField;
    quCashRepZDiscount: TFloatField;
    quCashRepZDate: TDateField;
    quCashRepZTime: TTimeField;
    quCashRepZCrSale: TFloatField;
    quCashRepZCrReturn: TFloatField;
    quCashRepZCrDiscount: TFloatField;
    quCashRepSaleRet: TFloatField;
    quCashRepSaleBn: TFloatField;
    quCashGood: TPvQuery;
    quGens: TPvQuery;
    dsquGens: TDataSource;
    quGensDepart: TSmallintField;
    quGensDateReport: TDateField;
    quGensSumma: TFloatField;
    quGensSkidka: TFloatField;
    quGensChekBuh: TWordField;
    quGensName: TStringField;
    quCashGoodGrCode: TSmallintField;
    quCashGoodCode: TIntegerField;
    quCashGoodName: TStringField;
    quCashGoodQuant: TFloatField;
    quCashGoodRSumD: TFloatField;
    quCashGoodRSum: TFloatField;
    quM2: TPvQuery;
    quM2CARD: TIntegerField;
    quM2MOVEDATE: TDateField;
    quM2DOC_TYPE: TSmallintField;
    quM2DOCUMENT: TIntegerField;
    quM2PRICE: TFloatField;
    quM2QUANT_MOVE: TFloatField;
    quM2SUM_MOVE: TFloatField;
    quM2QUANT_REST: TFloatField;
    quCG1: TPvQuery;
    quCG1CARD: TIntegerField;
    quCG1ITEM: TIntegerField;
    quCG1DEPART: TSmallintField;
    quReports: TPvQuery;
    dsquReports: TDataSource;
    quReportsOtdel: TSmallintField;
    quReportsName: TStringField;
    quReportsxDate: TDateField;
    quReportsOldOstatokTovar: TFloatField;
    quReportsPrihodTovar: TFloatField;
    quReportsSelfPrihodTovar: TFloatField;
    quReportsPrihodTovarSumma: TFloatField;
    quReportsRashodTovar: TFloatField;
    quReportsSelfRashodTovar: TFloatField;
    quReportsRealTovar: TFloatField;
    quReportsSkidkaTovar: TFloatField;
    quReportsSenderSumma: TFloatField;
    quReportsSenderSkidka: TFloatField;
    quReportsOutTovar: TFloatField;
    quReportsRashodTovarSumma: TFloatField;
    quReportsPereoTovarSumma: TFloatField;
    quReportsNewOstatokTovar: TFloatField;
    quReportsOldOstatokTara: TFloatField;
    quReportsPrihodTara: TFloatField;
    quReportsSelfPrihodTara: TFloatField;
    quReportsPrihodTaraSumma: TFloatField;
    quReportsRashodTara: TFloatField;
    quReportsSelfRashodTara: TFloatField;
    quReportsRezervSumma: TFloatField;
    quReportsRezervSkidka: TFloatField;
    quReportsOutTara: TFloatField;
    quReportsRashodTaraSumma: TFloatField;
    quReportsxRezerv: TFloatField;
    quReportsNewOstatokTara: TFloatField;
    quReportsOprihod: TBooleanField;
    quCG2: TPvQuery;
    quCG2CARD: TIntegerField;
    quCG2ITEM: TIntegerField;
    quCG2DEPART: TSmallintField;
    quCG2Name: TStringField;
    dsquCG2: TDataSource;
    quRepTO: TPvQuery;
    quRepTOOtdel: TSmallintField;
    quRepTOxDate: TDateField;
    quRepTOOldOstatokTovar: TFloatField;
    quRepTOPrihodTovar: TFloatField;
    quRepTOSelfPrihodTovar: TFloatField;
    quRepTOPrihodTovarSumma: TFloatField;
    quRepTORashodTovar: TFloatField;
    quRepTOSelfRashodTovar: TFloatField;
    quRepTORealTovar: TFloatField;
    quRepTOSkidkaTovar: TFloatField;
    quRepTOSenderSumma: TFloatField;
    quRepTOSenderSkidka: TFloatField;
    quRepTOOutTovar: TFloatField;
    quRepTORashodTovarSumma: TFloatField;
    quRepTOPereoTovarSumma: TFloatField;
    quRepTONewOstatokTovar: TFloatField;
    quRepTOOldOstatokTara: TFloatField;
    quRepTOPrihodTara: TFloatField;
    quRepTOSelfPrihodTara: TFloatField;
    quRepTOPrihodTaraSumma: TFloatField;
    quRepTORashodTara: TFloatField;
    quRepTOSelfRashodTara: TFloatField;
    quRepTORezervSumma: TFloatField;
    quRepTORezervSkidka: TFloatField;
    quRepTOOutTara: TFloatField;
    quRepTORashodTaraSumma: TFloatField;
    quRepTOxRezerv: TFloatField;
    quRepTONewOstatokTara: TFloatField;
    quRepTOOprihod: TBooleanField;
    quS: TPvQuery;
    quSRSum: TFloatField;
    quPost: TPvQuery;
    dsquPost: TDataSource;
    quPostDepart: TSmallintField;
    quPostName: TStringField;
    quPostDateInvoice: TDateField;
    quPostNumber: TStringField;
    quPostIndexPost: TSmallintField;
    quPostCodePost: TSmallintField;
    quPostNamePost: TStringField;
    quPostNameInd: TStringField;
    quPostCodeTovar: TIntegerField;
    quPostCodeEdIzm: TSmallintField;
    quPostBarCode: TStringField;
    quPostNDSProc: TFloatField;
    quPostNDSSum: TFloatField;
    quPostKol: TFloatField;
    quPostCenaTovar: TFloatField;
    quPostProcent: TFloatField;
    quPostNewCenaTovar: TFloatField;
    quPostSumCenaTovarPost: TFloatField;
    quPostSumCenaTovar: TFloatField;
    quPostNameCli: TStringField;
    quRepOB: TPvQuery;
    quMGrFind: TPvQuery;
    quMGrFindID: TSmallintField;
    quMGrFindName: TStringField;
    quPost1: TPvQuery;
    dsquPost1: TDataSource;
    quPost1Depart: TSmallintField;
    quPost1Name: TStringField;
    quPost1DateInvoice: TDateField;
    quPost1Number: TStringField;
    quPost1IndexPost: TSmallintField;
    quPost1CodePost: TSmallintField;
    quPost1NamePost: TStringField;
    quPost1NameInd: TStringField;
    quPost1CodeTovar: TIntegerField;
    quPost1CodeEdIzm: TSmallintField;
    quPost1BarCode: TStringField;
    quPost1Kol: TFloatField;
    quPost1CenaTovar: TFloatField;
    quPost1Procent: TFloatField;
    quPost1NewCenaTovar: TFloatField;
    quPost1SumCenaTovarPost: TFloatField;
    quPost1SumCenaTovar: TFloatField;
    quPost1NameCli: TStringField;
    quCashRepCurMoney: TFloatField;
    quCashRepCheckSum: TFloatField;
    quCheckNumber: TPvQuery;
    quCheckNumberNums: TIntegerField;
    quCq: TPvQuery;
    quCqDateOperation: TDateField;
    quCqGrCode: TSmallintField;
    quCqCassir: TStringField;
    quCqCash_Code: TIntegerField;
    quCqCk_Card: TIntegerField;
    quCqCorSum: TFloatField;
    quCqRSum: TFloatField;
    quCqCorSum10: TFloatField;
    quCqRSum10: TFloatField;
    quCqCorSum18: TFloatField;
    quCqRSum18: TFloatField;
    quRepOb1: TPvQuery;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    SmallintField2: TSmallintField;
    SmallintField3: TSmallintField;
    StringField3: TStringField;
    SmallintField4: TSmallintField;
    SmallintField5: TSmallintField;
    SmallintField6: TSmallintField;
    FloatField1: TFloatField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    quTTnInv: TPvQuery;
    dsquTTnInv: TDataSource;
    quTTnInvInventry: TIntegerField;
    quTTnInvDepart: TSmallintField;
    quTTnInvDateInventry: TDateField;
    quTTnInvNumber: TStringField;
    quTTnInvCardIndex: TWordField;
    quTTnInvInputMode: TWordField;
    quTTnInvPriceMode: TWordField;
    quTTnInvDetail: TWordField;
    quTTnInvQuantActual: TFloatField;
    quTTnInvQuantRecord: TFloatField;
    quTTnInvSumRecord: TFloatField;
    quTTnInvSumActual: TFloatField;
    quTTnInvStatus: TStringField;
    quTTnInvNameDep: TStringField;
    quSpecInv: TPvQuery;
    quSpecInvInventry: TIntegerField;
    quSpecInvItem: TIntegerField;
    quSpecInvPrice: TFloatField;
    quSpecInvQuantRecord: TFloatField;
    quSpecInvQuantActual: TFloatField;
    quSpecInvQuantLost: TFloatField;
    quSpecInvDepart: TSmallintField;
    quSpecInvIndexPost: TSmallintField;
    quSpecInvCodePost: TSmallintField;
    quSpecInvName: TStringField;
    quSpecInvCena: TFloatField;
    quSpecInvEdIzm: TSmallintField;
    quSpecInvNDS: TFloatField;
    quCGInv: TPvQuery;
    quCGInvCARD: TIntegerField;
    quCGInvITEM: TIntegerField;
    quCGInvCena: TFloatField;
    quCGInvName: TStringField;
    quSpecInvGoodsGroupID: TSmallintField;
    quTTnInvItem: TIntegerField;
    quTTnInvSumD: TFloatField;
    quODate: TPvQuery;
    quODateDateClose: TDateField;
    quLabs: TPvQuery;
    quLabsID: TIntegerField;
    quLabsNAME: TStringField;
    quLabsFILEN: TStringField;
    dsquLabs: TDataSource;
    quLab1: TPvQuery;
    quLab1ID: TIntegerField;
    quLab1NAME: TStringField;
    quLab1FILEN: TStringField;
    quRepTO1: TPvQuery;
    quRepTO1Otdel: TSmallintField;
    quRepTO1xDate: TDateField;
    quRepTO1OldOstatokTovar: TFloatField;
    quRepTO1PrihodTovar: TFloatField;
    quRepTO1SelfPrihodTovar: TFloatField;
    quRepTO1PrihodTovarSumma: TFloatField;
    quRepTO1RashodTovar: TFloatField;
    quRepTO1SelfRashodTovar: TFloatField;
    quRepTO1RealTovar: TFloatField;
    quRepTO1SkidkaTovar: TFloatField;
    quRepTO1SenderSumma: TFloatField;
    quRepTO1SenderSkidka: TFloatField;
    quRepTO1OutTovar: TFloatField;
    quRepTO1RashodTovarSumma: TFloatField;
    quRepTO1PereoTovarSumma: TFloatField;
    quRepTO1NewOstatokTovar: TFloatField;
    quRepTO1OldOstatokTara: TFloatField;
    quRepTO1PrihodTara: TFloatField;
    quRepTO1SelfPrihodTara: TFloatField;
    quRepTO1PrihodTaraSumma: TFloatField;
    quRepTO1RashodTara: TFloatField;
    quRepTO1SelfRashodTara: TFloatField;
    quRepTO1RezervSumma: TFloatField;
    quRepTO1RezervSkidka: TFloatField;
    quRepTO1OutTara: TFloatField;
    quRepTO1RashodTaraSumma: TFloatField;
    quRepTO1xRezerv: TFloatField;
    quRepTO1NewOstatokTara: TFloatField;
    quRepTO1Oprihod: TBooleanField;
    taCenQuant: TIntegerField;
    quScSpr: TPvQuery;
    quScSprNumber: TStringField;
    quScSprModel: TStringField;
    quScSprName: TStringField;
    quScSprHubNumber: TWordField;
    quScSprPLUCount: TSmallintField;
    quScSprRezerv1: TIntegerField;
    quScSprRezerv2: TIntegerField;
    quScSprRezerv3: TIntegerField;
    quScSprRezerv4: TIntegerField;
    quScSprRezerv5: TIntegerField;
    quScSprRezerv6: TIntegerField;
    quScSprRezerv7: TIntegerField;
    quScSprRezerv8: TIntegerField;
    quScSprRezerv9: TIntegerField;
    quScSprRezerv10: TIntegerField;
    quScale: TPvQuery;
    quScaleNumber: TStringField;
    quScaleModel: TStringField;
    quScaleName: TStringField;
    quScaleHubNumber: TWordField;
    quScalePLUCount: TSmallintField;
    quScaleRezerv1: TIntegerField;
    quScaleRezerv2: TIntegerField;
    quScaleRezerv3: TIntegerField;
    quScaleRezerv4: TIntegerField;
    quScaleRezerv5: TIntegerField;
    quScaleRezerv6: TIntegerField;
    quScaleRezerv7: TIntegerField;
    quScaleRezerv8: TIntegerField;
    quScaleRezerv9: TIntegerField;
    quScaleRezerv10: TIntegerField;
    dsquScale: TDataSource;
    quScaleItems: TPvQuery;
    dsquScaleItems: TDataSource;
    quScaleItemsNumber: TStringField;
    quScaleItemsDepart: TSmallintField;
    quScaleItemsPLU: TSmallintField;
    quScaleItemsGoodsItem: TIntegerField;
    quScaleItemsFirstName: TStringField;
    quScaleItemsLastName: TStringField;
    quScaleItemsPrice: TIntegerField;
    quScaleItemsShelfLife: TSmallintField;
    quScaleItemsTareWeight: TSmallintField;
    quScaleItemsGroupCode: TSmallintField;
    quScaleItemsMessageNo: TSmallintField;
    quScaleItemsStatus: TSmallintField;
    DigiClient: TIdTCPClient;
    quFindPlu: TPvQuery;
    quFindPluNumber: TStringField;
    quFindPluPLU: TSmallintField;
    quFindPluStatus: TSmallintField;
    taCenPluScale: TStringField;
    quDepsTO: TPvQuery;
    quDepsTOID: TSmallintField;
    quDepsTOName: TStringField;
    quDepsTOStatus1: TSmallintField;
    quDepsTOStatus2: TSmallintField;
    quDepsTOStatus3: TSmallintField;
    quDepsTOStatus4: TIntegerField;
    quDepsTOStatus5: TIntegerField;
    quDepsTOParentID: TSmallintField;
    quDepsTOObjectTypeID: TSmallintField;
    quDepsTOClientID: TIntegerField;
    quDepsTOClientObjTypeID: TSmallintField;
    quDepsTOImageID: TIntegerField;
    quDepsTOFullName: TStringField;
    quDepsTOEnglishFullName: TStringField;
    quDepsTOPath: TStringField;
    quDepsTOV01: TIntegerField;
    quDepsTOV02: TIntegerField;
    quDepsTOV03: TIntegerField;
    quDepsTOV04: TIntegerField;
    quDepsTOV05: TIntegerField;
    quDepsTOV06: TIntegerField;
    quDepsTOV07: TIntegerField;
    quDepsTOV08: TIntegerField;
    quDepsTOV09: TIntegerField;
    quDepsTOV10: TIntegerField;
    quDepsTOV11: TIntegerField;
    quDepsTOV12: TIntegerField;
    quDepsTOV13: TIntegerField;
    quDepsTOV14: TIntegerField;
    dsquScSpr: TDataSource;
    taToScale: TdxMemData;
    taToScaleId: TIntegerField;
    taToScaleBarcode: TStringField;
    taToScaleName: TStringField;
    taToScaleSrok: TIntegerField;
    taToScalePrice: TFloatField;
    quFPlu: TPvQuery;
    quFPluPLU: TSmallintField;
    quScaleItemsrPr: TFloatField;
    quDepPars: TPvQuery;
    quDepParsID: TSmallintField;
    quDepParsNAMEDEP: TStringField;
    quDepParsNAMEOTPR: TStringField;
    quDepParsADRDEP: TStringField;
    quDepParsADROTPR: TStringField;
    quDepParsLICO: TStringField;
    quDepParsOGRN: TStringField;
    quDepParsINN: TStringField;
    quDepParsKPP: TStringField;
    quDepParsFL: TStringField;
    quCds: TPvQuery;
    quMoveQIN: TFloatField;
    quMoveQOUT: TFloatField;
    quCdsID: TIntegerField;
    quCdsReserv1: TFloatField;
    quCDS1: TPvQuery;
    IntegerField4: TIntegerField;
    FloatField2: TFloatField;
    quPriorD: TPvQuery;
    quPriorDID: TSmallintField;
    quPriorDV01: TIntegerField;
    quPost2: TPvQuery;
    dsquPost2: TDataSource;
    quPost2Depart: TSmallintField;
    quPost2Name: TStringField;
    quPost2DateInvoice: TDateField;
    quPost2Number: TStringField;
    quPost2IndexPost: TSmallintField;
    quPost2CodePost: TSmallintField;
    quPost2NamePost: TStringField;
    quPost2NameInd: TStringField;
    quPost2CodeTovar: TIntegerField;
    quPost2CodeEdIzm: TSmallintField;
    quPost2BarCode: TStringField;
    quPost2Kol: TFloatField;
    quPost2CenaTovar: TFloatField;
    quPost2Procent: TFloatField;
    quPost2NewCenaTovar: TFloatField;
    quPost2SumCenaTovarPost: TFloatField;
    quPost2SumCenaTovar: TFloatField;
    quPost2NameCli: TStringField;
    quSaleDep: TPvQuery;
    quSaleDeprSum: TFloatField;
    quSaleDeprSumD: TFloatField;
    quRepOBITEM: TIntegerField;
    quRepOBName: TStringField;
    quRepOBEdIzm: TSmallintField;
    quRepOBDEPART: TSmallintField;
    quRepOBNameDep: TStringField;
    quRepOBGoodsGroupID: TSmallintField;
    quRepOBSubGroupID: TSmallintField;
    quRepOBV01: TIntegerField;
    quRepOBV02: TIntegerField;
    quRepOBCARD: TIntegerField;
    quBufPrReserv1: TFloatField;
    quSpecVn1: TPvQuery;
    quSpecVn1Depart: TSmallintField;
    quSpecVn1CodeTovar: TIntegerField;
    quSpecVn1Kol: TFloatField;
    quFindC4SrokReal: TSmallintField;
    quSc: TPvQuery;
    quScNumber: TStringField;
    quScI: TPvQuery;
    quScINumber: TStringField;
    quScIDepart: TSmallintField;
    quScIPLU: TSmallintField;
    quScIGoodsItem: TIntegerField;
    quScIFirstName: TStringField;
    quScILastName: TStringField;
    quScIPrice: TIntegerField;
    quScIShelfLife: TSmallintField;
    quScITareWeight: TSmallintField;
    quScIGroupCode: TSmallintField;
    quScIMessageNo: TSmallintField;
    quScIStatus: TSmallintField;
    quSpecVn1NewCena: TFloatField;
    taCDep: TdxMemData;
    dstaCDep: TDataSource;
    taCDepValT: TSmallintField;
    taCDepDateSale: TDateField;
    taCDepCashNum: TIntegerField;
    taCDepiDep: TIntegerField;
    taCDepsDep: TStringField;
    taCDepRSum: TFloatField;
    taCDepRSumD: TFloatField;
    taCDepRSumN10: TFloatField;
    taCDepRSumD10: TFloatField;
    taCDepCassir: TIntegerField;
    quCDep: TPvQuery;
    quCDepDateSale: TDateField;
    quCDepCassaNumber: TIntegerField;
    quCDepOtdel: TSmallintField;
    quCDepSumma: TFloatField;
    quCDepSummaCor: TFloatField;
    quCDepEmployee: TSmallintField;
    quCDepSummaNDSx10: TFloatField;
    quCDepSummaNDSx20: TFloatField;
    quCDepCorNDSx10: TFloatField;
    quCDepCorNDSx20: TFloatField;
    quCDepCorNDSx0: TFloatField;
    quCDepSummaNDSx0: TFloatField;
    quCDepName: TStringField;
    taCDeprSumN0: TFloatField;
    taCDepRSumN20: TFloatField;
    taCDepRSumD20: TFloatField;
    taCDepRSumD0: TFloatField;
    taCenReceipt: TStringField;
    quTTnOutProvodType: TWordField;
    quRepReal: TPvQuery;
    quRepRealCode: TIntegerField;
    quRepRealGrCode: TSmallintField;
    quRepRealOperation: TStringField;
    quRepRealCk_Card: TIntegerField;
    quRepRealName: TStringField;
    quRepRealEdIzm: TSmallintField;
    quRepRealGoodsGroupID: TSmallintField;
    quRepRealV01: TIntegerField;
    quRepRealV02: TIntegerField;
    quRepRealNameDep: TStringField;
    quRepRealQuant: TFloatField;
    quRepRealDisc: TFloatField;
    quRepRealSumma: TFloatField;
    quCashRepSaleN: TFloatField;
    quSpecTest: TPvQuery;
    quSpecTestDepart: TSmallintField;
    quSpecTestDateInvoice: TDateField;
    quSpecTestNumber: TStringField;
    quSpecTestIndexPost: TSmallintField;
    quSpecTestCodePost: TSmallintField;
    quSpecTestCodeGroup: TSmallintField;
    quSpecTestCodePodgr: TSmallintField;
    quSpecTestCodeTovar: TIntegerField;
    quSpecTestCodeEdIzm: TSmallintField;
    quSpecTestTovarType: TSmallintField;
    quSpecTestBarCode: TStringField;
    quSpecTestMediatorCost: TFloatField;
    quSpecTestNDSProc: TFloatField;
    quSpecTestNDSSum: TFloatField;
    quSpecTestOutNDSSum: TFloatField;
    quSpecTestBestBefore: TDateField;
    quSpecTestInvoiceQuant: TFloatField;
    quSpecTestKolMest: TIntegerField;
    quSpecTestKolEdMest: TFloatField;
    quSpecTestKolWithMest: TFloatField;
    quSpecTestKolWithNecond: TFloatField;
    quSpecTestKol: TFloatField;
    quSpecTestCenaTovar: TFloatField;
    quSpecTestProcent: TFloatField;
    quSpecTestNewCenaTovar: TFloatField;
    quSpecTestSumCenaTovarPost: TFloatField;
    quSpecTestSumCenaTovar: TFloatField;
    quSpecTestProcentN: TFloatField;
    quSpecTestNecond: TFloatField;
    quSpecTestCenaNecond: TFloatField;
    quSpecTestSumNecond: TFloatField;
    quSpecTestProcentZ: TFloatField;
    quSpecTestZemlia: TFloatField;
    quSpecTestProcentO: TFloatField;
    quSpecTestOthodi: TFloatField;
    quSpecTestCodeTara: TIntegerField;
    quSpecTestVesTara: TFloatField;
    quSpecTestCenaTara: TFloatField;
    quSpecTestSumVesTara: TFloatField;
    quSpecTestSumCenaTara: TFloatField;
    quSpecTestChekBuh: TBooleanField;
    quSpecTestSertBeginDate: TDateField;
    quSpecTestSertEndDate: TDateField;
    quSpecTestSertNumber: TStringField;
    quTTnInNotNDSTovar: TFloatField;
    quTTnInNDS10: TFloatField;
    quTTnInOutNDS10: TFloatField;
    quTTnInNDS20: TFloatField;
    quTTnInOutNDS20: TFloatField;
    quRealTMP: TPvQuery;
    quRealTMPCARD: TIntegerField;
    quRealTMPMOVEDATE: TDateField;
    quRealTMPDOC_TYPE: TSmallintField;
    quRealTMPDOCUMENT: TIntegerField;
    quRealTMPPRICE: TFloatField;
    quRealTMPQUANT_MOVE: TFloatField;
    quRealTMPSUM_MOVE: TFloatField;
    quRealTMPQUANT_REST: TFloatField;
    quFindC5: TPvQuery;
    quFindC5ID: TIntegerField;
    quFindC5Name: TStringField;
    quFindC5FullName: TStringField;
    quFindC5BarCode: TStringField;
    quFindC5TovarType: TSmallintField;
    quFindC5EdIzm: TSmallintField;
    quFindC5Cena: TFloatField;
    quFindC5Status: TSmallintField;
    quFindC5GoodsGroupID: TSmallintField;
    quFindC5SubGroupID: TSmallintField;
    quCashSum0: TPvQuery;
    quCashSum0Ck_Card: TIntegerField;
    quCashSum0DSum: TFloatField;
    quCashSum0RSum: TFloatField;
    quPriorDV02: TIntegerField;
    quSpecInTest: TPvQuery;
    quSpecInTestKol: TFloatField;
    quSpecInTestID: TIntegerField;
    quCashRepRDiscAll: TFloatField;
    quFindC5Prsision: TFloatField;
    quFindC3Prsision: TFloatField;
    quSpecInT: TPvQuery;
    quSpecInTDepart: TSmallintField;
    quSpecInTCodeTara: TIntegerField;
    quSpecInTKolMest: TFloatField;
    quSpecInTCenaTara: TFloatField;
    quSpecInTSumCenaTara: TFloatField;
    quSpecOutT: TPvQuery;
    quSpecOutTDepart: TSmallintField;
    quSpecOutTCodeTara: TIntegerField;
    quSpecOutTKolMest: TFloatField;
    quSpecOutTCenaTara: TFloatField;
    taCenQuanrR: TFloatField;
    quDepartsSt: TPvQuery;
    quDepartsStID: TSmallintField;
    quDepartsStName: TStringField;
    quDepartsStStatus1: TSmallintField;
    quDepartsStStatus2: TSmallintField;
    quDepartsStStatus3: TSmallintField;
    quDepartsStParentID: TSmallintField;
    quDepartsStObjectTypeID: TSmallintField;
    dsquDepartsSt: TDataSource;
    quDepParsRSch: TStringField;
    quDepParsKSCh: TStringField;
    quDepParsBank: TStringField;
    quDepParsBik: TStringField;
    quCliPars: TPvQuery;
    quCliParsINN: TStringField;
    quCliParsNAMEOTP: TStringField;
    quCliParsADROTPR: TStringField;
    quCliParsRSch: TStringField;
    quCliParsKSch: TStringField;
    quCliParsBank: TStringField;
    quCliParsBik: TStringField;
    quPost3: TPvQuery;
    quPost3Depart: TSmallintField;
    quSInv: TPvQuery;
    quSInvSumRecord: TFloatField;
    quSInvSumActual: TFloatField;
    quSInvDetail: TWordField;
    quSpecIn2: TPvQuery;
    quSpecIn2Depart: TSmallintField;
    quSpecIn2DateInvoice: TDateField;
    quSpecIn2Number: TStringField;
    quSpecIn2IndexPost: TSmallintField;
    quSpecIn2CodePost: TSmallintField;
    quSpecIn2CodeGroup: TSmallintField;
    quSpecIn2CodePodgr: TSmallintField;
    quSpecIn2CodeTovar: TIntegerField;
    quSpecIn2CodeEdIzm: TSmallintField;
    quSpecIn2TovarType: TSmallintField;
    quSpecIn2BarCode: TStringField;
    quSpecIn2MediatorCost: TFloatField;
    quSpecIn2NDSProc: TFloatField;
    quSpecIn2NDSSum: TFloatField;
    quSpecIn2OutNDSSum: TFloatField;
    quSpecIn2BestBefore: TDateField;
    quSpecIn2InvoiceQuant: TFloatField;
    quSpecIn2KolMest: TIntegerField;
    quSpecIn2KolEdMest: TFloatField;
    quSpecIn2KolWithMest: TFloatField;
    quSpecIn2KolWithNecond: TFloatField;
    quSpecIn2Kol: TFloatField;
    quSpecIn2CenaTovar: TFloatField;
    quSpecIn2Procent: TFloatField;
    quSpecIn2NewCenaTovar: TFloatField;
    quSpecIn2SumCenaTovarPost: TFloatField;
    quSpecIn2SumCenaTovar: TFloatField;
    quSpecIn2CodeTara: TIntegerField;
    quSpecIn2VesTara: TFloatField;
    quSpecIn2CenaTara: TFloatField;
    quSpecIn2SumVesTara: TFloatField;
    quSpecIn2SumCenaTara: TFloatField;
    quSpecIn2ChekBuh: TBooleanField;
    quSpecIn2Name: TStringField;
    quSpecIn2Cena: TFloatField;
    quSpecIn2V02: TIntegerField;
    quSpecIn2Reserv1: TFloatField;
    quSpecInV02: TIntegerField;
    quFindC1V02: TIntegerField;
    quSpecIn1Cena: TFloatField;
    quRepR: TPvQuery;
    quRepRGoodsGroupID: TSmallintField;
    quRepRID: TIntegerField;
    quRepRName: TStringField;
    quRepRBarCode: TStringField;
    quRepRTovarType: TSmallintField;
    quRepREdIzm: TSmallintField;
    quRepRCena: TFloatField;
    quRepRFullName: TStringField;
    quRepRStatus: TSmallintField;
    quRepRReserv1: TFloatField;
    quRepRV01: TIntegerField;
    quRepRV02: TIntegerField;
    quRepRV03: TIntegerField;
    quRepRV04: TIntegerField;
    quRepRV05: TIntegerField;
    quB: TPvQuery;
    quFindCGroupName: TStringField;
    quTTnInNDSTovar: TFloatField;
    quTTnOutNDSTovar: TFloatField;
    quRepTovarPeriod: TPvQuery;
    quRepTovarPeriodRDate: TDateField;
    quRepTovarPeriodCode: TIntegerField;
    quRepTovarPeriodQuant: TFloatField;
    quRepTovarPeriodRSumCor: TFloatField;
    quRepTovarPeriodGoodsName: TStringField;
    quRepTovarPeriodgr3Name: TStringField;
    quRepTovarPeriodgr2Name: TStringField;
    quRepTovarPeriodgr1Name: TStringField;
    quRepTovarPeriodDepartName: TStringField;
    quRepTovarPeriodNAMEBRAND: TStringField;
    quRepTovarPeriodSID: TStringField;
    quRepTovarPeriodMonth: TStringField;
    quRepTovarPeriodyear: TStringField;
    quRepTovarPeriodkvartal: TStringField;
    quSel: TPvQuery;
    quCardsReserv2: TFloatField;
    cxStyle24: TcxStyle;
    quRepTO2: TPvQuery;
    quRepTO2OldOstatokTovar: TFloatField;
    quRepTO2PrihodTovar: TFloatField;
    quRepTO2SelfPrihodTovar: TFloatField;
    quRepTO2PrihodTovarSumma: TFloatField;
    quRepTO2RashodTovar: TFloatField;
    quRepTO2SelfRashodTovar: TFloatField;
    quRepTO2RealTovar: TFloatField;
    quRepTO2SkidkaTovar: TFloatField;
    quRepTO2SenderSumma: TFloatField;
    quRepTO2SenderSkidka: TFloatField;
    quRepTO2OutTovar: TFloatField;
    quRepTO2RashodTovarSumma: TFloatField;
    quRepTO2PereoTovarSumma: TFloatField;
    quRepTO2NewOstatokTovar: TFloatField;
    quRepTO2OldOstatokTara: TFloatField;
    quRepTO2PrihodTara: TFloatField;
    quRepTO2SelfPrihodTara: TFloatField;
    quRepTO2PrihodTaraSumma: TFloatField;
    quRepTO2RashodTara: TFloatField;
    quRepTO2SelfRashodTara: TFloatField;
    quRepTO2RezervSumma: TFloatField;
    quRepTO2RezervSkidka: TFloatField;
    quRepTO2OutTara: TFloatField;
    quRepTO2RashodTaraSumma: TFloatField;
    quRepTO2xRezerv: TFloatField;
    quRepTO2NewOstatokTara: TFloatField;
    quPost4: TPvQuery;
    dsquPost4: TDataSource;
    quPost4Depart: TSmallintField;
    quPost4Name: TStringField;
    quPost4DateInvoice: TDateField;
    quPost4Number: TStringField;
    quPost4IndexPost: TSmallintField;
    quPost4CodePost: TSmallintField;
    quPost4NamePost: TStringField;
    quPost4NameInd: TStringField;
    quPost4CodeTovar: TIntegerField;
    quPost4CodeEdIzm: TSmallintField;
    quPost4BarCode: TStringField;
    quPost4Kol: TFloatField;
    quPost4CenaTovar: TFloatField;
    quPost4Procent: TFloatField;
    quPost4NewCenaTovar: TFloatField;
    quPost4SumCenaTovarPost: TFloatField;
    quPost4SumCenaTovar: TFloatField;
    quPost4NameCli: TStringField;
    IdFTP1: TIdFTP;
    quTTnInINN: TStringField;
    quTTnInINNPost: TStringField;
    quTTnInINNInd: TStringField;
    quSpecIn1NDSSum: TFloatField;
    quTTnOutINNPost: TStringField;
    quTTnOutINNInd: TStringField;
    quTTnOutINN: TStringField;
    quSpecOut1NDSProc: TFloatField;
    quSpecOut1CodeTara: TIntegerField;
    quSpecOut1SumCenaTovarSpis: TFloatField;
    quSpecOut1SumCenaTovar: TFloatField;
    quSpecOut1KolMest: TFloatField;
    quSpecOut1SumCenaTara: TFloatField;
    quSpecOut1SumCenaTaraSpis: TFloatField;
    quSpecOut1NDSSum: TFloatField;
    quSpecVn1CodeTara: TIntegerField;
    quSpecVn1SumCenaTovarSpis: TFloatField;
    quSpecVn1SumCenaTara: TFloatField;
    quReportsV03: TIntegerField;
    quReportSvod: TPvQuery;
    quReportSvodOtdel: TSmallintField;
    quReportSvodName: TStringField;
    quReportSvodV03: TIntegerField;
    quReportSvodxDate: TDateField;
    quReportSvodOldOstatokTovar: TFloatField;
    quReportSvodPrihodTovar: TFloatField;
    quReportSvodSelfPrihodTovar: TFloatField;
    quReportSvodPrihodTovarSumma: TFloatField;
    quReportSvodRashodTovar: TFloatField;
    quReportSvodSelfRashodTovar: TFloatField;
    quReportSvodRealTovar: TFloatField;
    quReportSvodSkidkaTovar: TFloatField;
    quReportSvodSenderSumma: TFloatField;
    quReportSvodSenderSkidka: TFloatField;
    quReportSvodOutTovar: TFloatField;
    quReportSvodRashodTovarSumma: TFloatField;
    quReportSvodPereoTovarSumma: TFloatField;
    quReportSvodNewOstatokTovar: TFloatField;
    quReportSvodOldOstatokTara: TFloatField;
    quReportSvodPrihodTara: TFloatField;
    quReportSvodSelfPrihodTara: TFloatField;
    quReportSvodPrihodTaraSumma: TFloatField;
    quReportSvodRashodTara: TFloatField;
    quReportSvodSelfRashodTara: TFloatField;
    quReportSvodRezervSumma: TFloatField;
    quReportSvodRezervSkidka: TFloatField;
    quReportSvodOutTara: TFloatField;
    quReportSvodRashodTaraSumma: TFloatField;
    quReportSvodxRezerv: TFloatField;
    quReportSvodNewOstatokTara: TFloatField;
    quReportSvodOprihod: TBooleanField;
    quRepHour: TPvQuery;
    quRepHourCl00: TFloatField;
    quRepHourS00: TFloatField;
    quRepHourCl01: TFloatField;
    quRepHourS01: TFloatField;
    quRepHourCl02: TFloatField;
    quRepHourS02: TFloatField;
    quRepHourCl03: TFloatField;
    quRepHourS03: TFloatField;
    quRepHourCl04: TFloatField;
    quRepHourS04: TFloatField;
    quRepHourCl05: TFloatField;
    quRepHourS05: TFloatField;
    quRepHourCl06: TFloatField;
    quRepHourS06: TFloatField;
    quRepHourCl07: TFloatField;
    quRepHourS07: TFloatField;
    quRepHourCl08: TFloatField;
    quRepHourS08: TFloatField;
    quRepHourCl09: TFloatField;
    quRepHourS09: TFloatField;
    quRepHourCl10: TFloatField;
    quRepHourS10: TFloatField;
    quRepHourCl11: TFloatField;
    quRepHourS11: TFloatField;
    quRepHourCl12: TFloatField;
    quRepHourS12: TFloatField;
    quRepHourCl13: TFloatField;
    quRepHourS13: TFloatField;
    quRepHourCl14: TFloatField;
    quRepHourS14: TFloatField;
    quRepHourCl15: TFloatField;
    quRepHourS15: TFloatField;
    quRepHourCl16: TFloatField;
    quRepHourS16: TFloatField;
    quRepHourCl17: TFloatField;
    quRepHourS17: TFloatField;
    quRepHourCl18: TFloatField;
    quRepHourS18: TFloatField;
    quRepHourCl19: TFloatField;
    quRepHourS19: TFloatField;
    quRepHourCl20: TFloatField;
    quRepHourS20: TFloatField;
    quRepHourCl21: TFloatField;
    quRepHourS21: TFloatField;
    quRepHourCl22: TFloatField;
    quRepHourS22: TFloatField;
    quRepHourCl23: TFloatField;
    quRepHourS23: TFloatField;
    quRepHourM00: TFloatField;
    quRepHourM01: TFloatField;
    quRepHourM02: TFloatField;
    quRepHourM03: TFloatField;
    quRepHourM04: TFloatField;
    quRepHourM05: TFloatField;
    quRepHourM06: TFloatField;
    quRepHourM07: TFloatField;
    quRepHourM08: TFloatField;
    quRepHourM09: TFloatField;
    quRepHourM10: TFloatField;
    quRepHourM11: TFloatField;
    quRepHourM12: TFloatField;
    quRepHourM13: TFloatField;
    quRepHourM14: TFloatField;
    quRepHourM15: TFloatField;
    quRepHourM16: TFloatField;
    quRepHourM17: TFloatField;
    quRepHourM18: TFloatField;
    quRepHourM19: TFloatField;
    quRepHourM20: TFloatField;
    quRepHourM21: TFloatField;
    quRepHourM22: TFloatField;
    quRepHourM23: TFloatField;
    quRepHourQS: TFloatField;
    quRepHourQC: TFloatField;
    quRepHourSS: TFloatField;
    quRepHourSM: TFloatField;
    dsquRepHour: TDataSource;
    quRepHourA01: TFloatField;
    quRepHourA00: TFloatField;
    quRepHourA02: TFloatField;
    quRepHourA03: TFloatField;
    quRepHourA04: TFloatField;
    quRepHourA05: TFloatField;
    quRepHourA06: TFloatField;
    quRepHourA07: TFloatField;
    quRepHourA08: TFloatField;
    quRepHourA09: TFloatField;
    quRepHourA10: TFloatField;
    quRepHourA11: TFloatField;
    quRepHourA12: TFloatField;
    quRepHourA13: TFloatField;
    quRepHourA14: TFloatField;
    quRepHourA15: TFloatField;
    quRepHourA16: TFloatField;
    quRepHourA17: TFloatField;
    quRepHourA18: TFloatField;
    quRepHourA19: TFloatField;
    quRepHourA20: TFloatField;
    quRepHourA21: TFloatField;
    quRepHourA22: TFloatField;
    quRepHourA23: TFloatField;
    quRepHourAITOG: TFloatField;
    quRepSumCli: TPvQuery;
    quRepSumCliS20: TFloatField;
    quRepSumCliS50: TFloatField;
    quRepSumCliS100: TFloatField;
    quRepSumCliS200: TFloatField;
    quRepSumCliS500: TFloatField;
    quRepSumCliS1000: TFloatField;
    quRepSumCliSMORE: TFloatField;
    quCQCheck: TPvQuery;
    quCQCheckDateOperation: TDateField;
    quCQCheckCash_Code: TIntegerField;
    quCQCheckNSmena: TIntegerField;
    quCQCheckCk_Number: TIntegerField;
    quCQCheckCheckSum: TFloatField;
    quCQCheckQuantPos: TIntegerField;
    quZad: TPvQuery;
    quZadId: TStringField;
    quZadIType: TSmallintField;
    quZadComment: TStringField;
    quZadStatus: TSmallintField;
    quZadZDate: TDateField;
    quZadZTime: TTimeField;
    quZadPersonId: TIntegerField;
    quZadPersonName: TStringField;
    quZadFormDate: TDateField;
    quZadFormTime: TTimeField;
    quZadIdDoc: TStringField;
    dsquZad: TDataSource;
    quZadSel: TPvQuery;
    quZadSelId: TStringField;
    quZadSelIType: TSmallintField;
    quZadSelComment: TStringField;
    quZadSelStatus: TSmallintField;
    quZadSelZDate: TDateField;
    quZadSelZTime: TTimeField;
    quZadSelPersonId: TIntegerField;
    quZadSelPersonName: TStringField;
    quZadSelFormDate: TDateField;
    quZadSelFormTime: TTimeField;
    quZadSelIdDoc: TStringField;
    quZ: TPvQuery;
    quCQCheckOperation: TStringField;
    quCardsReserv3: TFloatField;
    quCardsReserv4: TFloatField;
    quCardsReserv5: TFloatField;
    quSpecInV01: TIntegerField;
    quSpecInV03: TIntegerField;
    quSpecInV04: TIntegerField;
    quSpecInReserv1: TFloatField;
    quSpecInReserv2: TFloatField;
    quSpecInReserv3: TFloatField;
    quSpecInReserv4: TFloatField;
    quSpecInReserv5: TFloatField;
    quSpecIn2V01: TIntegerField;
    quSpecIn2V03: TIntegerField;
    quSpecIn2V04: TIntegerField;
    quSpecIn2Reserv2: TFloatField;
    quSpecIn2Reserv3: TFloatField;
    quSpecIn2Reserv4: TFloatField;
    quSpecIn2Reserv5: TFloatField;
    quCardsV05: TIntegerField;
    quSpecInV05: TIntegerField;
    quSpecIn2V05: TIntegerField;
    quRepOBV03: TIntegerField;
    quRepOBV04: TIntegerField;
    quRepOBV05: TIntegerField;
    quCQZ: TPvQuery;
    quCQZCk_Card: TIntegerField;
    quCQZCorSum: TFloatField;
    quCQZRSum: TFloatField;
    quRepOBCena: TFloatField;
    quAkciya: TPvQuery;
    quAkciyaCode: TIntegerField;
    quAkciyaAType: TSmallintField;
    quAkciyaDateB: TDateField;
    quAkciyaDateE: TDateField;
    quAkciyaPrice: TFloatField;
    quAkciyaDStop: TSmallintField;
    quAkciyaDProc: TFloatField;
    quAkciyaStatusOn: TSmallintField;
    quAkciyaStatusOf: TSmallintField;
    quAkciyaName: TStringField;
    quAkciyaGoodsGroupID: TSmallintField;
    quAkciyaNameGr: TStringField;
    dsquAkciya: TDataSource;
    quAkciyaVal1: TFloatField;
    quObmH: TPvQuery;
    quObmHID: TAutoIncField;
    quObmHDOCDATE: TDateField;
    quObmHDOCNUM: TStringField;
    quObmHDEPART: TIntegerField;
    quObmHITYPE: TSmallintField;
    quObmHINSUMIN: TFloatField;
    quObmHINSUMOUT: TFloatField;
    quObmHOUTSUMIN: TFloatField;
    quObmHOUTSUMOUT: TFloatField;
    quObmHCLITYPE: TSmallintField;
    quObmHNamePost: TStringField;
    quObmHNameInd: TStringField;
    quObmHNameDep: TStringField;
    quObmHINNPost: TStringField;
    quObmHINNInd: TStringField;
    dsquObmH: TDataSource;
    quObmHNameCli: TStringField;
    quObmHINN: TStringField;
    quObmHIACTIVE: TSmallintField;
    quObmHCLICODE: TIntegerField;
    quZakH: TPvQuery;
    dsquZakH: TDataSource;
    quDepartsStStatus4: TIntegerField;
    quDepartsStStatus5: TIntegerField;
    quSale: TPvQuery;
    quSaleNDS: TFloatField;
    quSaleSUMIN: TFloatField;
    quCQCount: TPvQuery;
    quCQCountMaxCh: TIntegerField;
    quPost5: TPvQuery;
    dsquPost5: TDataSource;
    quPost5Depart: TSmallintField;
    quPost5Name: TStringField;
    quPost5DateInvoice: TDateField;
    quPost5Number: TStringField;
    quPost5IndexPost: TSmallintField;
    quPost5CodePost: TSmallintField;
    quPost5NamePost: TStringField;
    quPost5NameInd: TStringField;
    quPost5CodeTovar: TIntegerField;
    quPost5CodeEdIzm: TSmallintField;
    quPost5BarCode: TStringField;
    quPost5Kol: TFloatField;
    quPost5CenaTovar: TFloatField;
    quPost5Procent: TFloatField;
    quPost5NewCenaTovar: TFloatField;
    quPost5SumCenaTovarPost: TFloatField;
    quPost5SumCenaTovar: TFloatField;
    quPost5NameCli: TStringField;
    quSpecVn2: TPvQuery;
    quSpecVn2Depart: TSmallintField;
    quSpecVn2CodeTovar: TIntegerField;
    quSpecVn2CodeGroup: TSmallintField;
    quSpecVn2CodePodgr: TSmallintField;
    quSpecVn2CodeTara: TIntegerField;
    quSpecVn2Kol: TFloatField;
    quSpecVn2CenaTovarSpis: TFloatField;
    quSpecVn2SumCenaTovarSpis: TFloatField;
    quSpecVn2SumNewCenaTovar: TFloatField;
    quSpecVn2SumCenaTara: TFloatField;
    quSpecVn2CenaTovarMove: TFloatField;
    quSpecVn2NewCenaTovar: TFloatField;
    quPost1InnP: TStringField;
    quPost1InnI: TStringField;
    quPost1sInn: TStringField;
    quPostVn: TPvQuery;
    quPostVnDepart: TSmallintField;
    quPostVnName: TStringField;
    quPostVnDateInvoice: TDateField;
    quPostVnNumber: TStringField;
    quPostVnCodeTovar: TIntegerField;
    quPostVnCodeEdIzm: TSmallintField;
    quPostVnKol: TFloatField;
    quPostVnCenaTovarSpis: TFloatField;
    quPostVnCenaTovarMove: TFloatField;
    quFLastPriceVN: TPvQuery;
    quFLastPriceVNCenaTovarSpis: TFloatField;
    quFLastPriceVNCenaTovarMove: TFloatField;
    quTTnInvxDopStatus: TWordField;
    quAkciyaSID: TStringField;
    quAkciyaVal2: TStringField;
    ImageList1: TImageList;
    quSOut3: TPvQuery;
    quSOut3Depart: TSmallintField;
    quSOut3DateInvoice: TDateField;
    quSOut3Number: TStringField;
    quSOut3CodeGroup: TSmallintField;
    quSOut3CodePodgr: TSmallintField;
    quSOut3CodeTovar: TIntegerField;
    quSOut3CodeEdIzm: TSmallintField;
    quSOut3TovarType: TSmallintField;
    quSOut3BarCode: TStringField;
    quSOut3OKDP: TFloatField;
    quSOut3NDSProc: TFloatField;
    quSOut3NDSSum: TFloatField;
    quSOut3OutNDSSum: TFloatField;
    quSOut3Akciz: TFloatField;
    quSOut3KolMest: TIntegerField;
    quSOut3KolEdMest: TFloatField;
    quSOut3KolWithMest: TFloatField;
    quSOut3Kol: TFloatField;
    quSOut3CenaPost: TFloatField;
    quSOut3CenaTovar: TFloatField;
    quSOut3CenaTovarSpis: TFloatField;
    quSOut3SumCenaTovar: TFloatField;
    quSOut3SumCenaTovarSpis: TFloatField;
    quSOut3CodeTara: TIntegerField;
    quSOut3VesTara: TFloatField;
    quSOut3CenaTara: TFloatField;
    quSOut3CenaTaraSpis: TFloatField;
    quSOut3SumVesTara: TFloatField;
    quSOut3SumCenaTara: TFloatField;
    quSOut3SumCenaTaraSpis: TFloatField;
    quSOut3ChekBuh: TBooleanField;
    quSOut3Depart_1: TSmallintField;
    quSOut3DateInvoice_1: TDateField;
    quSOut3Number_1: TStringField;
    quSOut3SCFNumber: TStringField;
    quSOut3IndexPoluch: TSmallintField;
    quSOut3CodePoluch: TSmallintField;
    quSOut3SummaTovar: TFloatField;
    quSOut3NDS10: TFloatField;
    quSOut3OutNDS10: TFloatField;
    quSOut3NDS20: TFloatField;
    quSOut3OutNDS20: TFloatField;
    quSOut3LgtNDS: TFloatField;
    quSOut3OutLgtNDS: TFloatField;
    quSOut3NDSTovar: TFloatField;
    quSOut3OutNDSTovar: TFloatField;
    quSOut3NotNDSTovar: TFloatField;
    quSOut3OutNDSSNTovar: TFloatField;
    quSOut3SummaTovarSpis: TFloatField;
    quSOut3NacenkaTovar: TFloatField;
    quSOut3SummaTara: TFloatField;
    quSOut3SummaTaraSpis: TFloatField;
    quSOut3NacenkaTara: TFloatField;
    quSOut3AcStatus: TWordField;
    quSOut3ChekBuh_1: TWordField;
    quSOut3NAvto: TStringField;
    quSOut3NPList: TStringField;
    quSOut3FIOVod: TStringField;
    quSOut3PrnDate: TDateField;
    quSOut3PrnNumber: TStringField;
    quSOut3PrnKol: TFloatField;
    quSOut3PrnAkt: TStringField;
    quSOut3ProvodType: TWordField;
    quSOut3NotNDS10: TFloatField;
    quSOut3NotNDS20: TFloatField;
    quSOut3WithNDS10: TFloatField;
    quSOut3WithNDS20: TFloatField;
    quSOut3Crock: TFloatField;
    quSOut3Discount: TFloatField;
    quSOut3NDS010: TFloatField;
    quSOut3NDS020: TFloatField;
    quSOut3NDS_10: TFloatField;
    quSOut3NDS_20: TFloatField;
    quSOut3NSP_10: TFloatField;
    quSOut3NSP_20: TFloatField;
    quSOut3SCFDate: TDateField;
    quSOut3GoodsNSP0: TFloatField;
    quSOut3GoodsCostNSP: TFloatField;
    quSOut3OrderNumber: TIntegerField;
    quSOut3ID: TIntegerField;
    quSOut3LinkInvoice: TWordField;
    quSOut3StartTransfer: TWordField;
    quSOut3EndTransfer: TWordField;
    quSOut3REZERV: TStringField;
    quRepPrib: TPvQuery;
    quRepPribARTICUL: TIntegerField;
    quRepPribIDSTORE: TSmallintField;
    quRepPribITYPE: TSmallintField;
    quRepPribName: TStringField;
    quRepPribEdIzm: TSmallintField;
    quRepPribGoodsGroupID: TSmallintField;
    quRepPribSubGroupID: TSmallintField;
    quRepPribV01: TIntegerField;
    quRepPribV02: TIntegerField;
    quRepPribV03: TIntegerField;
    quRepPribV04: TIntegerField;
    quRepPribV05: TIntegerField;
    quRepPribCena: TFloatField;
    quRepPribNDS: TFloatField;
    quRepPribNameDep: TStringField;
    quRepPribQUANT: TFloatField;
    quRepPribSUMIN: TFloatField;
    quRepPribSUMOUT: TFloatField;
    quRepPribSINN: TStringField;
    quRepPribStatus5: TIntegerField;
    quSpecVnNDS: TFloatField;
    quInvCodeSum: TPvQuery;
    quInvCodeSumDateInventry: TDateField;
    quInvCodeSumSUMFSS: TFloatField;
    quInvCodeSumSUMDSS: TFloatField;
    quSpecVn1SumCenaTovarMove: TFloatField;
    taCenDepName: TStringField;
    quDepartsFullName: TStringField;
    quZadRec: TPvQuery;
    quZadRecId: TStringField;
    quZadRecIType: TSmallintField;
    quZadRecComment: TStringField;
    quZadRecStatus: TSmallintField;
    quZadRecZDate: TDateField;
    quZadRecZTime: TTimeField;
    quZadRecPersonId: TIntegerField;
    quZadRecPersonName: TStringField;
    quZadRecFormDate: TDateField;
    quZadRecFormTime: TTimeField;
    quZadRecIdDoc: TStringField;
    taCensDisc: TStringField;
    quZCQ: TPvQuery;
    quZCQCash_Code: TIntegerField;
    quZCQNSmena: TIntegerField;
    quZCQOperation: TStringField;
    quZCQRSUM: TFloatField;
    quZCQDSUM: TFloatField;
    quZCQCk_Card: TIntegerField;
    quZCQCountCh: TIntegerField;
    quCardsV06: TIntegerField;
    quCardsV07: TIntegerField;
    quAssPost: TPvQuery;
    quAssPostClientTypeId: TSmallintField;
    quAssPostClientId: TIntegerField;
    quAssPostGoodsId: TIntegerField;
    quAssPostPriceIn: TFloatField;
    quAssPostQuantIn: TFloatField;
    quAssPostxDate: TDateField;
    quAssPostsMess: TStringField;
    quAssPostName: TStringField;
    quAssPostFullName: TStringField;
    quAssPostEdIzm: TSmallintField;
    quZakHID: TAutoIncField;
    quZakHDOCDATE: TDateField;
    quZakHDOCNUM: TStringField;
    quZakHDEPART: TIntegerField;
    quZakHITYPE: TSmallintField;
    quZakHINSUMIN: TFloatField;
    quZakHCLITYPE: TSmallintField;
    quZakHCLICODE: TIntegerField;
    quZakHIACTIVE: TSmallintField;
    quZakHNamePost: TStringField;
    quZakHNameInd: TStringField;
    quZakHNameDep: TStringField;
    quZakHINNPost: TStringField;
    quZakHINNInd: TStringField;
    quZakHPERSON: TStringField;
    quZakHTIMEEDIT: TStringField;
    quZakHNameCli: TStringField;
    quZakHINN: TStringField;
    quPost6: TPvQuery;
    dsquPost6: TDataSource;
    quPost6GoodsId: TIntegerField;
    quPost6ClientTypeId: TSmallintField;
    quPost6ClientId: TIntegerField;
    quPost6NamePost: TStringField;
    quPost6NameInd: TStringField;
    quPost6NameCli: TStringField;
    quAkciyaOldDStop: TSmallintField;
    quAkciyaOldPrice: TFloatField;
    quAkciyaDocCode: TIntegerField;
    quAkciyaDocDate: TDateField;
    quAkciyaDocTime: TIntegerField;
    quAkciyaVal2After: TStringField;
    quAkciyaDateTime: TStringField;
    quPost7: TPvQuery;
    quPost7Depart: TSmallintField;
    quPost7Name: TStringField;
    quPost7DateInvoice: TDateField;
    quPost7Number: TStringField;
    quPost7IndexPoluch: TSmallintField;
    quPost7CodePoluch: TSmallintField;
    quPost7NamePost: TStringField;
    quPost7NameInd: TStringField;
    quPost7CodeTovar: TIntegerField;
    quPost7CodeEdIzm: TSmallintField;
    quPost7BarCode: TStringField;
    quPost7Kol: TFloatField;
    quPost7CenaTovar: TFloatField;
    quPost7CenaTovarSpis: TFloatField;
    quPost7SumCenaTovarSpis: TFloatField;
    quPost7SumCenaTovar: TFloatField;
    quPost7NDSProc: TFloatField;
    quPost7NDSSum: TFloatField;
    quPost7NameCli: TStringField;
    dsquPost7: TDataSource;
    quCardsV08: TIntegerField;
    taCenScaleKey: TIntegerField;
    quFindC4V01: TIntegerField;
    quFindC4V02: TIntegerField;
    quFindC4V03: TIntegerField;
    quFindC4V04: TIntegerField;
    quFindC4V05: TIntegerField;
    quFindC4V06: TIntegerField;
    quFindC4V07: TIntegerField;
    quFindC4V08: TIntegerField;
    quFindCBar: TPvQuery;
    quFindCBarID: TIntegerField;
    quFindCBarName: TStringField;
    quFindCBarFullName: TStringField;
    quFindCBarBarCode: TStringField;
    quFindCBarTovarType: TSmallintField;
    quFindCBarEdIzm: TSmallintField;
    quFindCBarNDS: TFloatField;
    quFindCBarCena: TFloatField;
    quFindCBarStatus: TSmallintField;
    quFindCBarV02: TIntegerField;
    taToScaleNumF: TSmallintField;
    quZakHSDATEEDIT: TStringField;
    quZakHDATEEDIT: TDateField;
    quPOut: TPvQuery;
    quPOutARTICUL: TIntegerField;
    quPOutIDATE: TIntegerField;
    quPOutIDSTORE: TSmallintField;
    quPOutITYPE: TSmallintField;
    quPOutIDDOC: TIntegerField;
    quPOutPARTIN: TIntegerField;
    quPOutID: TAutoIncField;
    quPOutQUANT: TFloatField;
    quPOutSUMIN: TFloatField;
    quPOutSUMOUT: TFloatField;
    quPOutSINN: TStringField;
    quCQZDay: TPvQuery;
    quCQZDayQuant: TFloatField;
    quCQZDayOperation: TStringField;
    quCQZDayDateOperation: TDateField;
    quCQZDayPrice: TFloatField;
    quCQZDayStore: TStringField;
    quCQZDayCk_Number: TIntegerField;
    quCQZDayCk_Curs: TFloatField;
    quCQZDayCk_CurAbr: TIntegerField;
    quCQZDayCk_Disg: TFloatField;
    quCQZDayCk_Disc: TFloatField;
    quCQZDayQuant_S: TFloatField;
    quCQZDayGrCode: TSmallintField;
    quCQZDayCode: TIntegerField;
    quCQZDayCassir: TStringField;
    quCQZDayCash_Code: TIntegerField;
    quCQZDayCk_Card: TIntegerField;
    quCQZDayContr_Code: TStringField;
    quCQZDayContr_Cost: TFloatField;
    quCQZDaySeria: TStringField;
    quCQZDayBestB: TStringField;
    quCQZDayNDSx1: TFloatField;
    quCQZDayNDSx2: TFloatField;
    quCQZDayTimes: TTimeField;
    quCQZDaySumma: TFloatField;
    quCQZDaySumNDS: TFloatField;
    quCQZDaySumNSP: TFloatField;
    quCQZDayPriceNSP: TFloatField;
    quCQZDayNSmena: TIntegerField;
    quTTnOutCrock: TFloatField;
    quCloseHBuh: TPvQuery;
    quCloseHBuhID: TIntegerField;
    quCloseHBuhDateClose: TDateField;
    quCloseHBuhIdPers: TIntegerField;
    quCloseHBuhDateEdit: TDateTimeField;
    cxStyle25: TcxStyle;
    quZakHCLIQUANT: TFloatField;
    quZakHCLISUMIN0: TFloatField;
    quZakHCLISUMIN: TFloatField;
    quZakHCLIQUANTN: TFloatField;
    quZakHCLISUMIN0N: TFloatField;
    quZakHCLISUMINN: TFloatField;
    quZakHIDATENACL: TIntegerField;
    quZakHSNUMNACL: TStringField;
    quZakHIDATESCHF: TIntegerField;
    quZakHSNUMSCHF: TStringField;
    quZakHsDatePost: TStringField;
    quZakHINSUMOUT: TFloatField;
    quSpecOut1SumVesTara: TFloatField;
    quTTnInProvodType: TWordField;
    quSpecOut1ChekBuh: TBooleanField;
    quRepCassir: TPvQuery;
    quRepCassirGrCode: TSmallintField;
    quRepCassirCassir: TStringField;
    quRepCassirCash_Code: TIntegerField;
    quRepCassirCountPos: TIntegerField;
    quRepCassirSumma: TFloatField;
    quRepCassirDateOperation: TDateField;
    quSpecInFullName: TStringField;
    quTTnInRaion: TSmallintField;
    quZakHPost: TPvQuery;
    quZakHPostID: TAutoIncField;
    quZakHPostDOCDATE: TDateField;
    quZakHPostDOCNUM: TStringField;
    quZakHPostDEPART: TIntegerField;
    quZakHPostITYPE: TSmallintField;
    quZakHPostINSUMIN: TFloatField;
    quZakHPostINSUMOUT: TFloatField;
    quZakHPostOUTSUMIN: TFloatField;
    quZakHPostOUTSUMOUT: TFloatField;
    quZakHPostCLITYPE: TSmallintField;
    quZakHPostCLICODE: TIntegerField;
    quZakHPostIACTIVE: TSmallintField;
    quZakHPostDATEEDIT: TDateField;
    quZakHPostTIMEEDIT: TStringField;
    quZakHPostPERSON: TStringField;
    quZakHPostCLIQUANT: TFloatField;
    quZakHPostCLISUMIN0: TFloatField;
    quZakHPostCLISUMIN: TFloatField;
    quZakHPostCLIQUANTN: TFloatField;
    quZakHPostCLISUMIN0N: TFloatField;
    quZakHPostCLISUMINN: TFloatField;
    quZakHPostIDATENACL: TIntegerField;
    quZakHPostSNUMNACL: TStringField;
    quZakHPostIDATESCHF: TIntegerField;
    quZakHPostSNUMSCHF: TStringField;
    quZakHPostName: TStringField;
    dsquZakHPost: TDataSource;
    quFCliRaion: TSmallintField;
    quZakHNum: TPvQuery;
    quZakHNumID: TAutoIncField;
    quZakHNumDOCDATE: TDateField;
    quZakHNumDOCNUM: TStringField;
    quZakHNumDEPART: TIntegerField;
    quZakHNumITYPE: TSmallintField;
    quZakHNumINSUMIN: TFloatField;
    quZakHNumINSUMOUT: TFloatField;
    quZakHNumOUTSUMIN: TFloatField;
    quZakHNumOUTSUMOUT: TFloatField;
    quZakHNumCLITYPE: TSmallintField;
    quZakHNumCLICODE: TIntegerField;
    quZakHNumIACTIVE: TSmallintField;
    quZakHNumDATEEDIT: TDateField;
    quZakHNumTIMEEDIT: TStringField;
    quZakHNumPERSON: TStringField;
    quZakHNumCLIQUANT: TFloatField;
    quZakHNumCLISUMIN0: TFloatField;
    quZakHNumCLISUMIN: TFloatField;
    quZakHNumCLIQUANTN: TFloatField;
    quZakHNumCLISUMIN0N: TFloatField;
    quZakHNumCLISUMINN: TFloatField;
    quZakHNumIDATENACL: TIntegerField;
    quZakHNumSNUMNACL: TStringField;
    quZakHNumIDATESCHF: TIntegerField;
    quZakHNumSNUMSCHF: TStringField;
    quGrafPost: TPvQuery;
    quGrafPostICLI: TIntegerField;
    quGrafPostIDEP: TIntegerField;
    quGrafPostIDATEP: TIntegerField;
    quGrafPostIDATEZ: TIntegerField;
    quGrafPostFullName: TStringField;
    quGrafPostDDATEP: TDateField;
    quGrafPostDDATEZ: TDateField;
    dsquGrafPost: TDataSource;
    quGrafPostName: TStringField;
    quSOutH3: TPvQuery;
    quSOutH3Depart: TSmallintField;
    quSOutH3DateInvoice: TDateField;
    quSOutH3Number: TStringField;
    quSOutH3SCFNumber: TStringField;
    quSOutH3IndexPoluch: TSmallintField;
    quSOutH3CodePoluch: TSmallintField;
    quSOutH3SummaTovar: TFloatField;
    quSOutH3NDS10: TFloatField;
    quSOutH3OutNDS10: TFloatField;
    quSOutH3NDS20: TFloatField;
    quSOutH3OutNDS20: TFloatField;
    quSOutH3LgtNDS: TFloatField;
    quSOutH3OutLgtNDS: TFloatField;
    quSOutH3NDSTovar: TFloatField;
    quSOutH3OutNDSTovar: TFloatField;
    quSOutH3NotNDSTovar: TFloatField;
    quSOutH3OutNDSSNTovar: TFloatField;
    quSOutH3SummaTovarSpis: TFloatField;
    quSOutH3NacenkaTovar: TFloatField;
    quSOutH3SummaTara: TFloatField;
    quSOutH3SummaTaraSpis: TFloatField;
    quSOutH3NacenkaTara: TFloatField;
    quSOutH3AcStatus: TWordField;
    quSOutH3ChekBuh: TWordField;
    quSOutH3NAvto: TStringField;
    quSOutH3NPList: TStringField;
    quSOutH3FIOVod: TStringField;
    quSOutH3PrnDate: TDateField;
    quSOutH3PrnNumber: TStringField;
    quSOutH3PrnKol: TFloatField;
    quSOutH3PrnAkt: TStringField;
    quSOutH3ProvodType: TWordField;
    quSOutH3NotNDS10: TFloatField;
    quSOutH3NotNDS20: TFloatField;
    quSOutH3WithNDS10: TFloatField;
    quSOutH3WithNDS20: TFloatField;
    quSOutH3Crock: TFloatField;
    quSOutH3Discount: TFloatField;
    quSOutH3NDS010: TFloatField;
    quSOutH3NDS020: TFloatField;
    quSOutH3NDS_10: TFloatField;
    quSOutH3NDS_20: TFloatField;
    quSOutH3NSP_10: TFloatField;
    quSOutH3NSP_20: TFloatField;
    quSOutH3SCFDate: TDateField;
    quSOutH3GoodsNSP0: TFloatField;
    quSOutH3GoodsCostNSP: TFloatField;
    quSOutH3OrderNumber: TIntegerField;
    quSOutH3ID: TIntegerField;
    quSOutH3LinkInvoice: TWordField;
    quSOutH3StartTransfer: TWordField;
    quSOutH3EndTransfer: TWordField;
    quSOutH3REZERV: TStringField;
    quPost5OutNDSSum: TFloatField;
    quPost5CenaTovar0: TFloatField;
    quFLastPriceOutNDSSum: TFloatField;
    quFLastPriceKol: TFloatField;
    quPost7CenaTaraSpis: TFloatField;
    quPostOutNDSSum: TFloatField;
    quPostCenaTovar0: TFloatField;
    quFCliUnTaxedNDS: TWordField;
    quSpecInvBarCode: TStringField;
    quPost5SertNumber: TStringField;
    quFLastPriceSertNumber: TStringField;
    quSaleSUMIN0: TFloatField;
    quTestInv: TPvQuery;
    quTestInvInventry: TIntegerField;
    quTestInvDepart: TSmallintField;
    quTestInvDateInventry: TDateField;
    quTestInvNumber: TStringField;
    quTestInvCardIndex: TWordField;
    quTestInvInputMode: TWordField;
    quTestInvPriceMode: TWordField;
    quTestInvDetail: TWordField;
    quTestInvGGroup: TSmallintField;
    quTestInvSubGroup: TSmallintField;
    quTestInvItem: TIntegerField;
    quTestInvCountLost: TBooleanField;
    quTestInvDateLost: TDateField;
    quTestInvQuantRecord: TFloatField;
    quTestInvSumRecord: TFloatField;
    quTestInvQuantActual: TFloatField;
    quTestInvSumActual: TFloatField;
    quTestInvNumberOfLines: TIntegerField;
    quTestInvDateStart: TDateField;
    quTestInvTimeStart: TTimeField;
    quTestInvPresident: TStringField;
    quTestInvPresidentPost: TStringField;
    quTestInvMember1: TStringField;
    quTestInvMember1_Post: TStringField;
    quTestInvMember2: TStringField;
    quTestInvMember2_Post: TStringField;
    quTestInvMember3: TStringField;
    quTestInvMember3_Post: TStringField;
    quTestInvxDopStatus: TWordField;
    quTestInvResponse1: TStringField;
    quTestInvResponse1_Post: TStringField;
    quTestInvResponse2: TStringField;
    quTestInvResponse2_Post: TStringField;
    quTestInvResponse3: TStringField;
    quTestInvResponse3_Post: TStringField;
    quTestInvReason: TStringField;
    quTestInvCASHER: TStringField;
    quTestInvStatus: TStringField;
    quTestInvLinkInvoice: TWordField;
    quTestInvStartTransfer: TWordField;
    quTestInvEndTransfer: TWordField;
    quTestInvRezerv: TStringField;
    quTestZadInv: TPvQuery;
    quTestZadInvId: TStringField;
    quTestZadInvIType: TSmallintField;
    quTestZadInvComment: TStringField;
    quTestZadInvStatus: TSmallintField;
    quTestZadInvZDate: TDateField;
    quTestZadInvZTime: TTimeField;
    quTestZadInvPersonId: TIntegerField;
    quTestZadInvPersonName: TStringField;
    quTestZadInvFormDate: TDateField;
    quTestZadInvFormTime: TTimeField;
    quTestZadInvIdDoc: TStringField;
    quCardsV09: TIntegerField;
    quCardsV10: TIntegerField;
    quCardsVol: TFloatField;
    quFindCli: TPvQuery;
    dsquFindCli: TDataSource;
    quFindCliVendor: TSmallintField;
    quFindCliName: TStringField;
    quFindCliPayDays: TSmallintField;
    quFindCliPayForm: TWordField;
    quFindCliPayWaste: TWordField;
    quFindCliFirmName: TStringField;
    quFindCliPhone1: TStringField;
    quFindCliPhone2: TStringField;
    quFindCliPhone3: TStringField;
    quFindCliPhone4: TStringField;
    quFindCliFax: TStringField;
    quFindCliTreatyDate: TDateField;
    quFindCliTreatyNumber: TStringField;
    quFindCliPostIndex: TStringField;
    quFindCliGorod: TStringField;
    quFindCliRaion: TSmallintField;
    quFindCliStreet: TStringField;
    quFindCliHouse: TStringField;
    quFindCliBuild: TStringField;
    quFindCliKvart: TStringField;
    quFindCliOKPO: TStringField;
    quFindCliOKONH: TStringField;
    quFindCliINN: TStringField;
    quFindCliFullName: TStringField;
    quFindCliCodeUchet: TStringField;
    quFindCliUnTaxedNDS: TWordField;
    quFindCliStatusByte: TWordField;
    quFindCliStatus: TSmallintField;
    quCatDisc: TPvQuery;
    quCatDiscGoodsGroupID: TSmallintField;
    quCatDiscID: TSmallintField;
    quCatDiscName: TStringField;
    quCatDiscIDsg: TSmallintField;
    quCatDiscNameSg: TStringField;
    quCatDiscGoodsGroupIDsg: TSmallintField;
    quCatDiscIDgr: TSmallintField;
    quCatDiscNamegr: TStringField;
    quCatDiscV01: TIntegerField;
    taCensDate: TStringField;
    quAlgClass: TPvQuery;
    dsquAlgClass: TDataSource;
    quAlgClassID: TIntegerField;
    quAlgClassNAMECLA: TStringField;
    quMakersID: TAutoIncField;
    quMakersNAMEM: TStringField;
    quMakersINNM: TStringField;
    quMakersKPPM: TStringField;
    dsquMakers1: TDataSource;
    dsquAlgClass1: TDataSource;
    quCardsV11: TIntegerField;
    quCardsV12: TIntegerField;
    quCardsNAMEM: TStringField;
    quSpecInv1: TPvQuery;
    quSpecInv1IDH: TStringField;
    quSpecInv1ITEM: TIntegerField;
    quSpecInv1QUANTR: TFloatField;
    quSpecInv1QUANTF: TFloatField;
    quSpecInv1QUANTD: TFloatField;
    quSpecInv1PRICER: TFloatField;
    quSpecInv1SUMR: TFloatField;
    quSpecInv1SUMF: TFloatField;
    quSpecInv1SUMD: TFloatField;
    quSpecInv1SUMRSS: TFloatField;
    quSpecInv1SUMFSS: TFloatField;
    quSpecInv1SUMDSS: TFloatField;
    quSpecInv1NUM: TIntegerField;
    quSpecInv1QUANTC: TFloatField;
    quSpecInv1SUMC: TFloatField;
    quSpecInv1SUMRSC: TFloatField;
    quAkciyaNAMEBRAND: TStringField;
    quFindBarQuant: TFloatField;
    quFindBarBarFormat: TSmallintField;
    quFindBarDateChange: TDateField;
    quFindBarBarStatus: TSmallintField;
    quFindBarCost: TFloatField;
    quFindBarStatus: TSmallintField;
    quDepParsRukovod: TStringField;
    quDepParsGlBuh: TStringField;
    quDepParsStreet: TStringField;
    quDepParsDom: TStringField;
    quDepParsKorp: TStringField;
    quDepParsLitera: TStringField;
    quDepParsIndex: TStringField;
    quDepParsTel: TStringField;
    quDepParssEmail: TStringField;
    quDepParsDir1: TStringField;
    quDepParsDir2: TStringField;
    quDepParsDir3: TStringField;
    quDepParsGb1: TStringField;
    quDepParsGb2: TStringField;
    quDepParsGb3: TStringField;
    quDepParsLicVid: TStringField;
    quDepParsLicSer: TStringField;
    quDepParsLicNum: TStringField;
    quDepParsLicDB: TDateField;
    quDepParsLicDE: TDateField;
    quCardsV14: TIntegerField;
    quCardsTYPEVOZ: TStringField;
    quCliMOLVOZ: TStringField;
    quCliPHONEVOZ: TStringField;
    quCliEMAILVOZ: TStringField;
    quCliSTBLOCK: TSmallintField;
    quCliTYPEVOZ: TStringField;
    quTTnOutIDCLICB: TSmallintField;
    quCardsQremn: TFloatField;
    quRemnQRem: TFloatField;
    quRemnStatus1: TSmallintField;
    cxStyle26: TcxStyle;
    taCenV02: TIntegerField;
    quSpecOut1CenaTaraSpis: TFloatField;
    taCenV12: TIntegerField;
    taCenMaker: TStringField;
    quFindC4V12: TIntegerField;
    quFindC4NAMEM: TStringField;
    quTTnInStatus: TSmallintField;
    quSpecOut2: TPvQuery;
    quSpecOut2CodeTovar: TIntegerField;
    quSpecOut2ChekBuh: TBooleanField;
    quSpecOut2Kol: TFloatField;
    quSpecOut2SumVesTara: TFloatField;
    quCIdV14: TIntegerField;
    quCashListNumber: TIntegerField;
    quCashListName: TStringField;
    quCashListShRealiz: TIntegerField;
    quCashListAddGoods: TIntegerField;
    quCashListTechnolog: TIntegerField;
    quCashListFlags: TSmallintField;
    quCashListRezerv: TFloatField;
    quCashListShopIndex: TSmallintField;
    quCashListPATH: TStringField;
    quCashListLoad: TIntegerField;
    quCashListTake: TIntegerField;
    quCashPath: TPvQuery;
    quCashPathID: TIntegerField;
    quCashPathPATH: TStringField;
    quCliParsPRETCLI: TStringField;
    quCliParsDOGNUM: TStringField;
    quCliParsDOGDATE: TDateField;
    quCQCheckTimes: TTimeField;
    quDepParsGLN: TStringField;
    quSpecInV11: TIntegerField;
    quSpecInV10: TIntegerField;
    quF: TPvQuery;
    quRepRealAP: TPvQuery;
    dsquRepRealAP: TDataSource;
    quRepRealAPIDATE: TIntegerField;
    quRepRealAPIDREP: TIntegerField;
    quRepRealAPID: TAutoIncField;
    quRepRealAPIDEP: TIntegerField;
    quRepRealAPIDATER: TIntegerField;
    quRepRealAPDATEOPER: TDateField;
    quRepRealAPICODE: TIntegerField;
    quRepRealAPBARCODE: TStringField;
    quRepRealAPFULLNAME: TStringField;
    quRepRealAPQUANT: TFloatField;
    quRepRealAPAVID: TIntegerField;
    quRepRealAPVOL: TFloatField;
    quRepRealAPTIMEOP: TTimeField;
    quRepRealAPDATETIMEOP: TDateTimeField;
    quRepRealItog: TPvQuery;
    quRepRealItogIDATE: TIntegerField;
    quRepRealItogIDREP: TIntegerField;
    quRepRealItogIDEP: TIntegerField;
    quRepRealItogICODE: TIntegerField;
    quRepRealItogBARCODE: TStringField;
    quRepRealItogFULLNAME: TStringField;
    quRepRealItogAVID: TIntegerField;
    quRepRealItogVOL: TFloatField;
    quRepRealItogQUANTITOG: TFloatField;
    quGetAlcoList: TPvQuery;
    quGetAlcoListOtdel: TSmallintField;
    quGetAlcoListGoodsGroupID: TSmallintField;
    quGetAlcoListSubGroupID: TSmallintField;
    quGetAlcoListID: TIntegerField;
    quGetAlcoListName: TStringField;
    quGetAlcoListBarCode: TStringField;
    quGetAlcoListShRealize: TSmallintField;
    quGetAlcoListTovarType: TSmallintField;
    quGetAlcoListEdIzm: TSmallintField;
    quGetAlcoListNDS: TFloatField;
    quGetAlcoListOKDP: TFloatField;
    quGetAlcoListWeght: TFloatField;
    quGetAlcoListSrokReal: TSmallintField;
    quGetAlcoListTareWeight: TSmallintField;
    quGetAlcoListKritOst: TFloatField;
    quGetAlcoListBHTStatus: TSmallintField;
    quGetAlcoListUKMAction: TSmallintField;
    quGetAlcoListDataChange: TDateField;
    quGetAlcoListNSP: TFloatField;
    quGetAlcoListWasteRate: TFloatField;
    quGetAlcoListCena: TFloatField;
    quGetAlcoListKol: TFloatField;
    quGetAlcoListFullName: TStringField;
    quGetAlcoListCountry: TSmallintField;
    quGetAlcoListStatus: TSmallintField;
    quGetAlcoListPrsision: TFloatField;
    quGetAlcoListPriceState: TWordField;
    quGetAlcoListSetOfGoods: TWordField;
    quGetAlcoListPrePacking: TStringField;
    quGetAlcoListPrintLabel: TWordField;
    quGetAlcoListMargGroup: TStringField;
    quGetAlcoListFixPrice: TFloatField;
    quGetAlcoListReserv1: TFloatField;
    quGetAlcoListReserv2: TFloatField;
    quGetAlcoListReserv3: TFloatField;
    quGetAlcoListReserv4: TFloatField;
    quGetAlcoListReserv5: TFloatField;
    quGetAlcoListObjectTypeID: TSmallintField;
    quGetAlcoListImageID: TIntegerField;
    quGetAlcoListV01: TIntegerField;
    quGetAlcoListV02: TIntegerField;
    quGetAlcoListV03: TIntegerField;
    quGetAlcoListV04: TIntegerField;
    quGetAlcoListV05: TIntegerField;
    quGetAlcoListV06: TIntegerField;
    quGetAlcoListV07: TIntegerField;
    quGetAlcoListV08: TIntegerField;
    quGetAlcoListV09: TIntegerField;
    quGetAlcoListV10: TIntegerField;
    quGetAlcoListV11: TIntegerField;
    quGetAlcoListV12: TIntegerField;
    quGetAlcoListV13: TIntegerField;
    quGetAlcoListV14: TIntegerField;
    quGetAVid: TPvQuery;
    quGetAVidID: TIntegerField;
    quGetAVidNAMECLA: TStringField;
    quGetAVidGetAM: TSmallintField;
    quAlgClassGetAM: TSmallintField;
    quFindC3V11: TIntegerField;
    quFindC5V11: TIntegerField;
    
    procedure quTTnInCalcFields(DataSet: TDataSet);
    procedure quTTnOutCalcFields(DataSet: TDataSet);
    procedure quCashListCalcFields(DataSet: TDataSet);
    procedure quCashCalcFields(DataSet: TDataSet);
    procedure quRemnCalcFields(DataSet: TDataSet);
    procedure quPostCalcFields(DataSet: TDataSet);
    procedure quPost1CalcFields(DataSet: TDataSet);
    procedure quTTnInvCalcFields(DataSet: TDataSet);
    procedure quScaleItemsCalcFields(DataSet: TDataSet);
    procedure quMoveCalcFields(DataSet: TDataSet);
    procedure quPost2CalcFields(DataSet: TDataSet);
    procedure quFindCCalcFields(DataSet: TDataSet);
    procedure quRepTovarPeriodCalcFields(DataSet: TDataSet);
    procedure quPost4CalcFields(DataSet: TDataSet);
    procedure quRepHourCalcFields(DataSet: TDataSet);
    procedure quCashRepCalcFields(DataSet: TDataSet);
    procedure quAkciyaCalcFields(DataSet: TDataSet);
    procedure quObmHCalcFields(DataSet: TDataSet);
    procedure quPost5CalcFields(DataSet: TDataSet);
    procedure quZakHCalcFields(DataSet: TDataSet);
    procedure quPost6CalcFields(DataSet: TDataSet);
    procedure quPost7CalcFields(DataSet: TDataSet);
    procedure quGrafPostCalcFields(DataSet: TDataSet);
    procedure quCardsCalcFields(DataSet: TDataSet);
    procedure quRepRealAPCalcFields(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
    Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView);
    Procedure ClassExpNew( Node : TTreeNode; Tree:TTreeView);
  end;


type

  TArray4Bytes = array [1..4] of byte;

  TAddPluRecord = packed record
     RecID:word;
     No:TArray4Bytes;
     NullByte:byte;
     RecSize:byte;
     Status:word; //const=$0094
     Status2:Array[1..3]of byte;
     UnitPrice:TArray4Bytes;
     LabelFormat:byte;
     BarCodeFormat:byte;
     BarCodeData:TArray4Bytes;
     DateCfg:Array[1..3]of byte;
     MgCode:word;
     SellByDate:word;
     NullWord:word;
     Str1Font:byte;
     Str1Length:byte;
     Str1:array[1..28]of byte;
     CrLf:byte;  //const = $0D
     Str2Font:byte;
     Str2Length:byte;
     Str2:array[1..28]of byte;
     EndRec:byte; //const = $0C
     CheckByte:byte; //-?
  end;


  TDelPluRecord = packed record
     RecID:word;
     No:TArray4Bytes;
  end;

Function CanDo(Name_F:String):Boolean;
Function CountRec(Name_T:String):Integer;
Function CountRec1(quR:TPvQuery):Boolean;
procedure prRefreshCards(iGr:Integer;bNoActive:Boolean);
Function prFindNewArt(bVes:Boolean; Var sM:String):Integer;

Function prFindNameGr(iGr:Integer;Var IdGr:Integer):String;
Function prFindNameMGr(iGr:Integer):String;

function prFindBar(sBar:String;iC:Integer;Var IDC:INteger):Boolean; //���� ������ ����
function prFindBarC(sBar:String;iC:Integer;Var IDC:INteger):Boolean; //���� ������������� quCId �� ������

function prFindEU(Id:INteger;Var S:String):Integer;
function prMax(sTab:String):INteger;
function fTara(iC:INteger;Var S:String):Boolean;
Procedure prFindGr(Id:INteger;Var iGr,iSGr:Integer);
Procedure prRefrID(quT:TPvQuery;Vi: TcxGridDBTableView;IDH:INteger);
Function prFC(Id:INteger):Real;  //������� ���� �� ��������

Function prFLP(Id:INteger;Var rPriceM:Real):Real;
Function prFLP0(Id:INteger;Var rPriceM,rPriceIn0:Real;Var sGTD:String):Real;

Procedure prFDepINN(Id:INteger;Var SInn,SAdr:String);
Procedure prFCliINN(Id,iT:INteger;Var SInn,SAdr,sFName,sInnF:String);
Procedure prFParCli(sInnF:String; Var sPretCli,sDogNum,sDogDate:String);
function prFindGLNDep(iDep:INteger):String;

procedure prGds(iArt,iDate,Depart:Integer;QIN,QOUT,QVNIN,QVNOUT,QINV,QREAL:Real);
procedure prPriceBuf(IdCard,iDate,IdDoc,iTypeDoc:Integer;DocNum:String;Pr1,Pr2:Real);
procedure prClearBuf(iDDoc,DocType,iDate,iOp:Integer);
function prFindRemn(IdCard,Depart:INteger):Real; //������ �� �������
function prFindRemn1(IdCard:INteger):Real;       //� ���������� ������
function prFindRemn2(IdCard:INteger):Real;       //�����
function prFindRemn3(IdCard,iDate,iDepart:INteger):Real; //������� �� ����� �� ������ ��� �����
function prFindRemn4(CardG,iDate:INteger):Real; //������� �� ����� �� ������ ��� �����  �� CG

function prFindNum(TypeD:Integer):Integer;       //����� ��������
procedure prDelMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
procedure prAddMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
procedure prDelMoveIdC(IdCard,iDate,iDoc,iTD:Integer);
function prFindIdCG(iCg:INteger):INteger;
function prFindCG(iItem,Depart:INteger):Integer;

function prOpenDate:TDateTime;
procedure prAddHist(iTd,IdDoc,DocDate:INteger;DocNum,NameOp:String;secpost:integer);
procedure prLogPrice(IdCard,iSt:INteger;OldPrice,NewPrice:Real);

function prFindSUMTO(iDep,iDate:Integer):Real;

function prTestScale(sAdr:String):Boolean;
Function AddPlu(i,PluCode,sDate:word;ItemCode,Name1,Name2:String;Price:Real;bF:byte):Boolean;
// Function DelPlu(i:word):Boolean;
function LoadScale(sAdr:String;iC:Integer):Boolean;
function prFindPlu(iC:INteger):String;
function prFindPluSc(iC:INteger;sSc:String):Boolean;
procedure prAddToScale(sSc:String);

function prTestGen(iCurDate,Id:INteger):Boolean;

function fGetSlave(Id:INteger):String; //������ ������ ������� ���� ������� � ��������������
procedure prRecalcTO(iDateB,iDateE:Integer;Memo1:TcxMemo;iTa,iDays:INteger); //�������� �������� �������
procedure prRecalcTOPos(iCode,iSkl,iDateB,iDateE:Integer;memo1:tcxMemo;Var rSumR:Real); //�������� ���� �� �� �� ��������� ���

Function CanTr:Boolean;
Function prTr(NameF:String;Memo1:TcxMemo):Boolean;

//���� ������� �������� �� �� �� ������ � ���� ����� �� ���
Function prFindRemnSumSSInv(DateB,DateE,iSkl,Code:INteger;Var DateB1:Integer):Real;

function prFindBarCMaker(sBar:String;iC:Integer;Var IDC,IDMAKER:INteger):Boolean;
function prGetOrg(iOrg,iSh:INteger):TOrg;

procedure prRecalcTOSv(iDateB,iDateE:Integer;Memo1:TcxMemo;iDays:INteger); //�������� �������� �������

procedure prflagcheck(i:integer);

var
  dmMC: TdmMC;
  flagcheck:boolean=false;
  AddPluRec:TAddPluRecord;
//  DelPluRec:TDelPluRecord;
  ArrPlu:array[1..100] of TAddPluRecord;
//  ArrDel:array[1..100] of TDelPluRecord;


implementation


uses MainMCryst, Func, MT, ToRep, dmPS;

{$R *.dfm}

function prFindGLNDep(iDep:INteger):String;
begin
  with dmMC do
  begin
    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iDep;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then Result:=quDepParsGLN.AsString else Result:='';
    quDepPars.Active:=False;
  end;
end;

Procedure prFParCli(sInnF:String; Var sPretCli,sDogNum,sDogDate:String);
begin
  with dmMC do
  begin
    sPretCli:='';
    sDogNum:='';
    sDogDate:='';

    quCliPars.Active:=False;
    quCliPars.ParamByName('SINN').AsString:=sInnF;
    quCliPars.Active:=True;

    if quCliPars.RecordCount>0 then
    begin
      sPretCli:=quCliParsPRETCLI.AsString;
      sDogNum:=quCliParsDOGNUM.AsString;
      sDogDate:=ds1(quCliParsDOGDATE.AsDateTime);
    end;

    quCliPars.Active:=False;
  end;
end;


procedure prflagcheck(i:integer);
begin
    if i=1 then flagcheck:=true
    else flagcheck:=false;
end;

Function prFindRemnSumSSInv(DateB,DateE,iSkl,Code:INteger;Var DateB1:Integer):Real;
begin
  DateB1:=DateB;
  Result:=0;
  with dmMC do
  begin
    quInvCodeSum.Active:=False;
    quInvCodeSum.SQL.Clear;
    quInvCodeSum.SQL.Add('select top 1 invh.DateInventry,invln.SUMFSS,invln.SUMDSS from "InventryHead" invh');
    quInvCodeSum.SQL.Add('left join A_INVLN invln on invln.IDH=invh.Inventry');
    quInvCodeSum.SQL.Add('where');
    quInvCodeSum.SQL.Add('invh.Depart='+its(iSkl));
    quInvCodeSum.SQL.Add('and invh.Item=1'); //����� ��������
    quInvCodeSum.SQL.Add('and invh.DateInventry>='''+ds(DateB)+'''');
    quInvCodeSum.SQL.Add('and invh.DateInventry<='''+ds(DateE)+'''');
    quInvCodeSum.SQL.Add('and invln.ITEM='+its(Code));
    quInvCodeSum.SQL.Add('and invh.Status<>''�''');
    quInvCodeSum.SQL.Add('order by DateInventry desc');
    quInvCodeSum.SQL.Add('');
    quInvCodeSum.Active:=True;
    if quInvCodeSum.RecordCount>0 then
    begin
      quInvCodeSum.First;
      DateB1:=Trunc(quInvCodeSumDateInventry.AsDateTime)+1;
      Result:=quInvCodeSumSUMFSS.AsFloat;
    end;
    quInvCodeSum.Active:=False;
  end;
end;

Function prTr(NameF:String;Memo1:TcxMemo):Boolean;
var
  FList,Dest,PList : TStrings;
  i,k,m:INteger;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;

  procedure prM(S:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(S);
  end;

begin
  with dmMc do
  begin
    Result:=True;
    bExit:=False;
    if CommonSet.TrToFtp=1 then
    begin
      prM('  ���������.');

    //�������� ������ ���� ������ ��� ��������
      FList := TStringList.Create;

    //�������� ����������� �� ���
      Dest := TStringList.Create;

    //�������� ����� �������� �� ���� �� ���
      pList := TStringList.Create;
      StrWk:=CommonSet.FtpExport;
      While StrWk>'' do
      begin
        if Pos(r,strwk)>0 then
        begin
          StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
          Delete(StrWk,1,Pos(r,strwk));
        end else
        begin
          StrWk1:=StrWk;
          StrWk:='';
        end;
        PList.Add(StrWk1);
      end;
      try
      //�������� ��� ��� ������
        prM('  ��������� ftp.');
        try
          IDFtp1.DisConnect;
          IdFtp1.Host:=CommonSet.HostIp;
          IdFtp1.Username:=CommonSet.HostUser;
          IdFtp1.Password:=CommonSet.HostPassw;
          IdFTP1.Connect;
          IDFtp1.List( Dest );
        except
          prM('  ������.');
          Result:=False;
          bExit:=True;
        end;

        if bExit then goto ExitProc;

        FList.Add(NameF); // ���������� � ������ �������� ����� ��� ��������

      //������ ������ ������������ -
      //1. �������� - 100 ��
      //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
      //3. ����������
      //4. ��������
      //5. ��� ������ ���������� � �����
      //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

      //1.
        delay(100);
        For i:=0 to FList.Count-1 do
        begin
          try
            For k:=0 to PList.Count-1 do
            begin
              StrWk:=PList.Strings[k];
              m:=0;
              BadPath:=False;
              While StrWk>'' do
              begin
                if Pos('\',strwk)>0 then
                begin
                  StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                  Delete(StrWk,1,Pos('\',strwk));
                end else
                begin
                  StrWk1:=StrWk;
                  StrWk:='';
                end;
                if strwk1>'' then
                begin
                  prM('    ���� ��� �������� - '+StrWk1);
                  try
                    IdFTP1.ChangeDir(StrWk1);
                    inc(m);
                    prM('    ���� ��.'+StrWk1);
                  except
                    Result:=False;
                    prM('    ������ ��� �������� - '+StrWk1);
                    BadPath:=True;
                  end;
                end;
              end;

              if not BadPath then //���� ���������� ����.
              begin
                try
                  prM('      �������� ����� - '+FList.Strings[i]);
//                IDFtp1.Put(CurDir+FList.Strings[i]+TrExt, FList.Strings[i]+TrExt, False );
                  IDFtp1.Put(CommonSet.TmpPath+FList.Strings[i], FList.Strings[i], False );
                  prM('      �������� Ok.');
                except
                  Result:=False;
                  prM('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
                end;
              end else Result:=False;

              While m>0 do
              begin
                IdFTP1.ChangeDir('..');//�� ������� ������� ���������� - ��  ������� ���������.
                m:=m-1;
              end;
            end;

          except
            Result:=False;
//          WrMess(' ������ ��� ������.');
          end;
        end;
        delay(100);
        ExitProc:
      finally
        IDFtp1.DisConnect;
        FList.Free;
        Dest.Free;
        PList.Free;
      end;
    end;
    if CommonSet.TrToFtp=0 then //� ��������� �����
    begin
      prM('  �������� ��������� - '+CommonSet.PathExport00);
      try
        if DirectoryExists(CommonSet.PathExport00)=False then
        begin
          prM('    ������� ����� - '+CommonSet.PathExport00);
          createdir(CommonSet.PathExport00);
        end;

        MoveFile(CommonSet.TmpPath+NameF, CommonSet.PathExport00+NameF);

      except
        prM('  ������.');
      end;
    end;
  end;
end;



Function CanTr:Boolean;
var
  FList,Dest,PList,DList: TStrings;
  k,m:INteger;
  bExit:Boolean;
  StrWk,StrWk1:String;
begin
  with dmMC do
  begin
  Result:=True;
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  PList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      Result:=False;
      bExit:=True;
    end;

    //�������� ���� , ������ ���� �� �������� �.�. ����� ��������� ���������� ������������� � ��������� ��������� ������.
    if bExit=False then
    begin
    For k:=0 to PList.Count-1 do
    begin
      StrWk:=PList.Strings[k];
      m:=0;
      While StrWk>'' do
      begin
        if Pos('\',strwk)>0 then
        begin
          StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
          Delete(StrWk,1,Pos('\',strwk));
        end else
        begin
          StrWk1:=StrWk;
          StrWk:='';
        end;
        if strwk1>'' then
        begin
//        WrMess('    ���� ��� �������� - '+StrWk1);
          try
            IdFTP1.ChangeDir(StrWk1);
            inc(m);
//            WrMess('    ���� ��.'+StrWk1);
          except
            Result:=False;
//                  WrMess('    ������ ��� �������� - '+StrWk1);
          end;
        end;
      end;

      While m>0 do
      begin
        IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
        m:=m-1;
      end;
    end;
    end;
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
//    WrMess('  �������� �������� ���������.');
  end;
  end;
end;

procedure prRecalcTOPos(iCode,iSkl,iDateB,iDateE:Integer;memo1:tcxMemo;Var rSumR:Real); //�������� ���� �� �� �� ��������� ���
Var
    iSS,iDateB1:Integer;
    rSumRemn:Real;
begin
  with dmMC do
  begin
    try
      //��������� ������������ �� �������
      if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+'  ������ ����� ��',Memo1);

      iSS:=fSS(iSkl);
      rSumR:=0;

      //���� ��������� �������������� �� ����� ������ � DateB �� �����
      //��� ���� ������� � ������ ������� �� ����������
      //iDateB-1 - ���� ��������� �������������� ���� ����
      //iDateE-1 - ���� ������� �� ������

      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

      rSumRemn:=prFindRemnSumSSInv(iDateB-1,iDateE-1,iSkl,iCode,iDateB1);
      if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' ���. �� ��� (���. 4) '+fs(RoundVal(rSumRemn)),Memo1);

      if (iDateB1)>iDateB then iDateB:=iDateB1; //���� �������������� - �� ��� � ������

      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

      rSumR:=rSumRemn+rSumR; //�������� �������

//          prM('    ������� ������...');
      if iSS>0 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        if iSS=1 then //����� � ���
          quS.SQL.Add('Select SUM("SumCenaTovarPost") as RSum from "TTNInLn" ds');
        if iSS=2 then //����� ��� ���
          quS.SQL.Add('Select SUM("OutNDSSum") as RSum from "TTNInLn" ds');

        quS.SQL.Add('left join "TTNIn" dh on (ds.Depart=dh.Depart)and(ds.DateInvoice=dh.DateInvoice)and(ds.Number=dh.Number)');
        quS.SQL.Add('where ds.DateInvoice>='''+ds(iDateB)+'''');
        quS.SQL.Add('and ds.DateInvoice<='''+ds(iDateE)+'''');
        quS.SQL.Add('and ds."Depart"='+its(iSkl));
        quS.SQL.Add('and ds."CodeTovar"='+its(iCode));
        quS.SQL.Add('and ds."CodeTovar" not in (50168,50184,21161,21163)');
        quS.SQL.Add('and dh.AcStatus=3');
{
left join "TTNIn" dh on (ds.Depart=dh.Depart)and(ds.DateInvoice=dh.DateInvoice)and(ds.Number=dh.Number)
where ds.DateInvoice>='2013-01-01'
and ds.DateInvoice<='2013-02-01'
and ds."Depart"=4
and ds."CodeTovar"=1481
and ds."CodeTovar" not in (50168,50184,21161,21163)
and dh.AcStatus=3
}
        quS.Active:=True;
        rSumR:=rSumR+RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 1 - ������: '+fs(RoundVal(quSRSum.AsFloat))+'  �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;

      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

//          prM('    ������� ������...');

      if iSS>0 then //��� ������� ������
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        if iSS=1 then //����� � ���
          quS.SQL.Add('Select SUM(ln."SumCenaTovar") as RSum from "TTNOutLn" ln'); //� ���
        if iSS=2 then //����� ��� ���
//          quS.SQL.Add('Select SUM(ln."SumCenaTovar"-(ln."SumCenaTovar"*ln."NDSProc"/(100+ln."NDSProc"))) as RSum from "TTNOutLn" ln');
          quS.SQL.Add('Select SUM(ln."OutNDSSum") as RSum from "TTNOutLn" ln');    //��� ���

        quS.SQL.Add('left join "TTNOut" hd on');
        quS.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');

        quS.SQL.Add('where ln.DateInvoice>='''+ds(iDateB)+'''');
        quS.SQL.Add('and ln.DateInvoice<='''+ds(iDateE)+'''');
        quS.SQL.Add('and ln."Depart"='+its(iSkl));
        quS.SQL.Add('and ln."CodeTovar"='+its(iCode));
        quS.SQL.Add('and hd."AcStatus"=3');

        if iSS=2 then
          quS.SQL.Add('and hd."ProvodType"<>3');  //��� ISS=1  "SumCenaTovar" ������ ����������

        quS.Active:=True;
        rSumR:=rSumR-RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 2 - �������: '+fs(RoundVal(quSRSum.AsFloat))+' �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;

      //��� ������� ������ ����������
      if iSS=2 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;

        quS.SQL.Add('select SUM(ln."CenaTaraSpis"*ln."Kol") as RSum from "TTNOutLn" ln');
        quS.SQL.Add('left join "TTNOut" hd on');
        quS.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');
        quS.SQL.Add('where ln.DateInvoice>='''+ds(iDateB)+'''');
        quS.SQL.Add('and ln.DateInvoice<='''+ds(iDateE)+'''');
        quS.SQL.Add('and ln."CodeTovar"='+its(iCode));
        quS.SQL.Add('and ln."Depart"='+its(iSkl));
        quS.SQL.Add('and hd."AcStatus"=3');
        quS.SQL.Add('and hd."ProvodType"=3');
        quS.Active:=True;

        rSumR:=rSumR-RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 2 - ������� (3,4): '+fs(RoundVal(quSRSum.AsFloat))+' �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;


      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

//          prM('    ������� ����������� ...');

      if iSS>0 then //��� iSS=1 � iSS=2 ����� ���������
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('Select SUM("SumCenaTovarSpis") as RSum from "TTNSelfLn"');
        quS.SQL.Add('where DateInvoice>='''+ds(iDateB)+'''');
        quS.SQL.Add('and DateInvoice<='''+ds(iDateE)+'''');
        quS.SQL.Add('and "Depart"='+its(iSkl));
        quS.SQL.Add('and "CodeTovar"='+its(iCode));
//        quS.SQL.Add('and "CodeTovar" not in (50168,50184,21161,21163)');
        quS.Active:=True;
        rSumR:=rSumR-RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 3 - ��.���: '+fs(RoundVal(quSRSum.AsFloat))+' �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;

      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

      //�������������� �� ���� - �� ��� � ������

{      if iSS>0 then //������  //��� iSS=1 � iSS=2 ����� ���������
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('select SUM(ln.SUMFSS-ln.SUMRTO) as RSum from "A_INVLN" ln');
        quS.SQL.Add('left join "InventryHead" hd on hd.Inventry=Ln.IDH');
        quS.SQL.Add('where hd.DateInventry>='''+ds(iDateB)+'''');
        quS.SQL.Add('and hd.DateInventry<='''+ds(iDateE-1)+'''');
        quS.SQL.Add('and hd."Depart"='+its(iSkl));
        quS.SQL.Add('and Ln."ITEM"='+its(iCode));
        quS.Active:=True;
        rSumR:=rSumR+RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 4 '+fs(RoundVal(quSRSum.AsFloat))+' '+fs(rSumR),nil);
        quS.Active:=False;
      end;}


//    prM('    ����� ...');

      if iSS=1 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('select SUM(SUMIN) as RSUM from "A_PARTOUT"');
        quS.SQL.Add('where  IDATE>='+its(iDateB));
        quS.SQL.Add('and IDATE<='+its(iDateE));
        quS.SQL.Add('and IDSTORE='+its(iSkl));
        quS.SQL.Add('and ARTICUL='+its(iCode));
        quS.SQL.Add('and ITYPE=7');
        quS.Active:=True;
        rSumR:=rSumR-RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 5 - ����������: '+fs(RoundVal(quSRSum.AsFloat))+' �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;

      if iSS=2 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('select SUM(SUMIN0) as RSUM from "A_PARTOUT"');
        quS.SQL.Add('where  IDATE>='+its(iDateB));
        quS.SQL.Add('and IDATE<='+its(iDateE));
        quS.SQL.Add('and IDSTORE='+its(iSkl));
        quS.SQL.Add('and ARTICUL='+its(iCode));
        quS.SQL.Add('and ITYPE=7');
        quS.Active:=True;
        rSumR:=rSumR-RoundVal(quSRSum.AsFloat);
        if iCode=CommonSet.TestCodeSS then prWH(its(CommonSet.TestCodeSS)+' 5 - ����������: '+fs(RoundVal(quSRSum.AsFloat))+' �����: '+fs(rSumR),Memo1);
        quS.Active:=False;
      end;

      if iCode=CommonSet.TestCodeSS then prWH('c '+ds(iDateB)+' �� '+ds(iDateE),Memo1);

    except
    end;
  end;
end;



procedure prRecalcTO(iDateB,iDateE:Integer;Memo1:TcxMemo;iTa,iDays:INteger); //�������� �������� �������
Var iCurD:INteger;
    rSumB,rSumIn,rSumOut,rSumReal,rSumDisc,rSumAC,rSumRealBn,rSumDiscBn,rSumSelfIn,rSumSelfOut,rSumInv,rSumSelfInSS,rSumSelfOutSS:Real;
    rSumBT,rSumInT,rSumOutT:Real;
    iD,iTInv:INteger;
    IdMain:INteger;
    iSS:Integer;
    rSumRealSS:Real;
    bBonus:Boolean;
    rSumOutDoc,rSumOutPart:Real;

  procedure prM(S:String);
  begin
    if Memo1<>nil then  Memo1.Lines.Add(S);
    WriteHistory(S);
  end;

begin
  with dmMC do
  begin
    if CommonSet.NeedRecalcTO=0 then exit;

    prM('������ ������������ ������� ... �����.');

    if iDays=0 then
    begin
      if iDateB<=(prOpenDate) then //������� ����������� ������� �� �� 2-� ��� ����� ��������
      begin
        prM('������ ������. (prRecalcTO)');
        exit;
      end;
    end else
    begin
      if iDateB<Trunc(date-iDays) then //������� ����������� ������� �� �� 2-� ��� ����� ��������
      begin
        prM('������ ������. (prRecalcTO)');
        exit;
      end;
    end;

    if iTa=1 then prSetTA(iDateB); //������������ ����� ������������ ��� ������� �������������

    prAddHist(100,0,iDateB,DateToStr(iDateE),'TO',0);


    if fShopB=0 then bBonus:=False else bBonus:=True; // bBonus=True -  ��� ����� �������� ��������

    try
      //��������� ������������ �� �������
      quDepsTO.Active:=False;
      quDepsTO.Active:=True;
      quDepsTO.First;
      while not quDepsTO.Eof do
      begin
        prM('  '+quDepsTOName.AsString);
        quDepsTO.Next;
      end;

      quDepsTO.First;
      while not quDepsTO.Eof do
      begin
        iD:=quDepsTOID.AsInteger;
        iSS:=quDepsTOStatus5.AsInteger; //0 - ��� ������. 1- � ����� ������ � ���. 2 - � ����� ������ ��� ���

        prM('');
        prM('  ��������� �� '+quDepsTOName.AsString+'. �����...');

        with dmMt do   //���� ��������� ������ ����� ��� ������� ������� ��� ����������� ���������� ���
        begin
          if ptPartO.Active=False then ptPartO.Active:=True;
          ptPartO.Refresh;
          ptPartO.IndexFieldNames:='ITYPE;IDDOC;ARTICUL';
        end;

        for iCurD:=iDateB to iDateE do
        begin
          //��� ����������, ���� ����� ������� �� ������
          prM('    ���� ������� �� ������ '+ds(iCurD)+'  ...');
          quRepTO.Active:=False;
          quRepTO.ParamByName('DATEB').AsDate:=iCurD;
          quRepTO.ParamByName('IDDEP').AsInteger:=iD;
          quRepTO.Active:=True;
          if quRepTO.RecordCount>0 then
          begin
            rSumB:=rv(quRepTONewOstatokTovar.AsFloat);
            rSumBT:=rv(quRepTONewOstatokTara.AsFloat);
          end  else
          begin
            rSumB:=0;
            rSumBT:=0;
          end;
          quRepTO.Active:=False;

          //������ ��� �����������
          prM('    ������� ���������...');
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "REPORTS"');
          quD.SQL.Add('where "xDate">='''+ds(iCurD)+'''');
          quD.SQL.Add('and Otdel='+its(iD));
          quD.ExecSQL;

          rSumIn:=0;

          prM('    ������� ������...');
          if iSS=0 then   //� ��������� �����
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("SummaTovar") as RSum from "TTNIn"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            if bBonus then //����� �� ����������� ��� �������
            begin
              quS.SQL.Add('and "SummaTovarPost">0');
            end;
            quS.Active:=True;
            rSumIn:=RoundVal(quSRSum.AsFloat);
            quS.Active:=False;
          end;
          if iSS=1 then //����� � ���
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("SummaTovarPost") as RSum from "TTNIn"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            if bBonus then //����� �� ����������� ��� �������
            begin
              quS.SQL.Add('and "SummaTovarPost">0');
            end;
            quS.Active:=True;
            rSumIn:=quSRSum.AsFloat;
            quS.Active:=False;
          end;
          if iSS=2 then //����� ��� ���
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("OutNDSTovar"+"NotNDSTovar") as RSum from "TTNIn"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            if bBonus then //����� �� ����������� ��� �������
            begin
              quS.SQL.Add('and "SummaTovarPost">0');
            end;
            quS.Active:=True;
            rSumIn:=quSRSum.AsFloat;
            quS.Active:=False;
          end;

          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('Select SUM("SummaTara") as RSum from "TTNIn"');
          quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
          quS.SQL.Add('and "Depart"='+its(iD));
          quS.SQL.Add('and "AcStatus"=3');
          quS.Active:=True;
          rSumInT:=quSRSum.AsFloat;
          quS.Active:=False;

          rSumOut:=0;

          prM('    ������� ������...');

          prM('      ��� - '+its(iSS));
          //�����������
          if iSS=0 then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("SummaTovarSpis") as RSum from "TTNOut"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            quS.Active:=True;
            rSumOut:=rSumOut+quSRSum.AsFloat;
            quS.Active:=False;
          end;
          if iSS=1 then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("SummaTovar") as RSum from "TTNOut"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            quS.Active:=True;
            rSumOut:=quSRSum.AsFloat;
            quS.Active:=False;
          end;
          //�����������

          if iSS=2 then
          begin
            prM('      ��������.');
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("OutNDSTovar"+"NotNDSTovar") as RSum from "TTNOut"');
            quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            quS.SQL.Add('and "ProvodType"<>3');
            quS.SQL.Add('and "ProvodType"<>4');
            quS.Active:=True;
            rSumOut:=quSRSum.AsFloat;
            quS.Active:=False;

            prM('      ��.');
          end;

          //��� ������� �������

          if iSS=2 then    //��������� �������
          begin
            prM('      �� ����� 3,4');
            quSOut3.Active:=False;
            quSOut3.SQL.Clear;

            quSOut3.SQL.Add('select * from "TTNOutLn" ln');
            quSOut3.SQL.Add('left join "TTNOut" hd on');
            quSOut3.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');
            quSOut3.SQL.Add('where ln.DateInvoice='''+ds(iCurD)+'''');
            quSOut3.SQL.Add('and ln."Depart"='+its(iD));
            quSOut3.SQL.Add('and hd."AcStatus"=3');
            quSOut3.SQL.Add('and (hd."ProvodType"=3 or hd."ProvodType"=4)');
//            quSOut3.SQL.Add('and hd."ProvodType"=3');

            quSOut3.Active:=True;
            quSOut3.First;
            while not quSOut3.Eof do
            begin            //��� ����� ���� ���� - �� ���������� ��� ������ ����, � �� ������� ����� � �� ����....
              rSumOutDoc:=rv(quSOut3CenaTaraSpis.AsFloat*quSOut3Kol.AsFloat); //CenaTaraSpis ������ ���� ����������� ���� ��� ���
//              rSumOutDoc:=quSOut3CenaTaraSpis.AsFloat*quSOut3Kol.AsFloat; //CenaTaraSpis ������ ���� ����������� ���� ��� ���
              rSumOut:=rSumOut+rSumOutDoc;
              quSOut3.Next;
            end;
            prM('      ��.');
          end;

   {       if iSS=2 then    //������������ ������� - �� ��������
          begin
            quSOutH3.Active:=False;
            quSOutH3.SQL.Clear;
            quSOutH3.SQL.Add('select * from "TTNOut"');
            quSOutH3.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
            quSOutH3.SQL.Add('and "Depart"='+its(iD));
            quSOutH3.SQL.Add('and "AcStatus"=3');
            quSOutH3.SQL.Add('and ("ProvodType"=3 or "ProvodType"=4)');
            quSOutH3.SQL.Add('Order by Depart, DateInvoice, Number');

            quSOutH3.Active:=True;
            quSOutH3.First;
            while not quSOutH3.Eof do
            begin
              rSumOutDoc:=0;
              rSumOutPart:=0;
              //���� ����� �� ������� ���������
              with dmMt do
              begin
//                'ITYPE;IDDOC;ARTICUL'
                ptPartO.CancelRange;
                ptPartO.SetRange([2,quSOutH3ID.AsInteger],[2,quSOut3ID.AsInteger]);
                ptPartO.First;
                while not ptPartO.Eof do
                begin
                  rSumOutPart:=rSumOutPart+ptPartOSUMIN.AsFloat;  //���� ��� ���
                  ptPartO.Next;
                end;
              end;
              if rSumOutPart>0 then rSumOutDoc:=rSumOutPart //����� ������������� ����� �� ���������� �����
              else
              begin
                quSOut3.Active:=False;
                quSOut3.SQL.Clear;       //��� �� ����������� ���������

                quSOut3.SQL.Add('select * from "TTNOutLn" ln');
                quSOut3.SQL.Add('left join "TTNOut" hd on');
                quSOut3.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');
                quSOut3.SQL.Add('where ln.DateInvoice='''+ds(iCurD)+'''');
                quSOut3.SQL.Add('and ln."Depart"='+its(iD));
//                quSOut3.SQL.Add('and hd."AcStatus"=3');
//                quSOut3.SQL.Add('and (hd."ProvodType"=3 or hd."ProvodType"=4)');
                quSOut3.SQL.Add('and hd."ID"='+its(quSOutH3ID.AsInteger));

                quSOut3.Active:=True;
                quSOut3.First;
                while not quSOut3.Eof do
                begin
                  rSumOutDoc:=rSumOutDoc+quSOut3SumCenaTovar.AsFloat;
                  quSOut3.Next;
                end;
              end;

              rSumOut:=rSumOut+rSumOutDoc;

              quSOutH3.Next;
            end;
            quSOutH3.Active:=True;
          end; }

          prM('    ������� ������ ���� ...');
          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('Select SUM("SummaTaraSpis") as RSum from "TTNOut"');
          quS.SQL.Add('where DateInvoice='''+ds(iCurD)+'''');
          quS.SQL.Add('and "Depart"='+its(iD));
          quS.SQL.Add('and "AcStatus"=3');
          quS.Active:=True;
          rSumOutT:=quSRSum.AsFloat;
          quS.Active:=False;

          rSumAC:=0;
          if iSS=0 then
          begin
            prM('    ������� ���������� ...');
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select SUM("MatRaznica") as RSum from "TTNOvr"');
            quS.SQL.Add('where DateAct='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatus"=3');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumAC:=rSumAC+rv(quSRSum.AsFloat);
              quS.Next;
            end;
            quS.Active:=False;
          end;

          prM('    ������� ����������� ...');

          rSumSelfIn:=0;
          rSumSelfOut:=0;

          if iSS=0 then //
          begin
            if iCurD<40575 then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('Select "SummaTovarSpis" as RSum from "TTNSelf"');
              quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
              quS.SQL.Add('and "Depart"='+its(iD));
              quS.SQL.Add('and "AcStatusR"=3');
              quS.Active:=True;

              quS.First;
              while not quS.Eof do
              begin
                rSumSelfOut:=rSumSelfOut+RoundVal(quSRSum.AsFloat); //�� ������������� - ����� ���������� � �������
                quS.Next;
              end;
              quS.Active:=False;


              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('Select "SummaTovarMove" as RSum from "TTNSelf"');
              quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
              quS.SQL.Add('and "FlowDepart"='+its(iD)+' and "Depart"<>'+its(iD));
              quS.SQL.Add('and "AcStatusR"=3');
              quS.Active:=True;

              quS.First;
              while not quS.Eof do
              begin
                rSumSelfIn:=rSumSelfIn+RoundVal(quSRSum.AsFloat);
                quS.Next;
              end;
              quS.Active:=False;
            end else
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('Select "SummaTovarMove" as RSum from "TTNSelf"');
              quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
              quS.SQL.Add('and "Depart"='+its(iD));
              quS.SQL.Add('and "AcStatusR"=3');
              quS.Active:=True;

              quS.First;
              while not quS.Eof do
              begin
                rSumSelfOut:=rSumSelfOut+RoundVal(quSRSum.AsFloat); //�� ������������� - ����� ���������� � �������
                quS.Next;
              end;
              quS.Active:=False;


              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('Select "SummaTovarSpis" as RSum from "TTNSelf"');
              quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
              quS.SQL.Add('and "FlowDepart"='+its(iD)+' and "Depart"<>'+its(iD));
              quS.SQL.Add('and "AcStatusR"=3');
              quS.Active:=True;

              quS.First;
              while not quS.Eof do
              begin
                rSumSelfIn:=rSumSelfIn+RoundVal(quSRSum.AsFloat);
                quS.Next;
              end;
              quS.Active:=False;
            end;
          end;

          //��� ��������� ��� ���� �� ������ �.�. ������ ���-�� ������ ��� � ������ ���� �������
          //�� ��� �����   2012-03-27  ��������
//-----------------------------------------------------------------------------------------------
          rSumSelfInSS:=0;
          rSumSelfOutSS:=0;

          if iSS>0 then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('Select "SummaTovarSpis" as RSum from "TTNSelf"');
            quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
            quS.SQL.Add('and "Depart"='+its(iD));
            quS.SQL.Add('and "AcStatusR"=3');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumSelfOutSS:=rSumSelfOutSS+RoundVal(quSRSum.AsFloat);
              quS.Next;
            end;
            quS.Active:=False;
          end;

          //�� ���������� ������ ����� �������� ��� ������ �� ��������� Crock

          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('Select "Crock" as RSum from "TTNOut"');
          quS.SQL.Add('where  DateInvoice='''+ds(iCurD)+'''');
          quS.SQL.Add('and "Depart"='+its(iD));
          quS.SQL.Add('and "AcStatus"=3');
          quS.SQL.Add('and "ProvodType"=3');
          quS.SQL.Add('and "Crock"<>0');
          quS.Active:=True;

          quS.First;
          while not quS.Eof do
          begin
            rSumSelfOutSS:=rSumSelfOutSS+RoundVal(quSRSum.AsFloat);
            quS.Next;
          end;
          quS.Active:=False;


{            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('select SUM(SUMIN) as RSUM from "A_PARTOUT"');
            quS.SQL.Add('where  IDATE='+its(iCurD));
            quS.SQL.Add('and IDSTORE='+its(iD));
            quS.SQL.Add('and ITYPE=3');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumSelfOutSS:=rSumSelfOutSS+rv(quSRSum.AsFloat); // ������������� ������� �����������
              quS.Next;
            end;
            quS.Active:=False;

            quS.Active:=False; //
            quS.SQL.Clear;
            quS.SQL.Add('select SUM(QPART*PRICEIN) as RSum from "A_PARTIN"');
            quS.SQL.Add('where  IDATE='+its(iCurD));
            quS.SQL.Add('and IDSTORE='+its(iD));
            quS.SQL.Add('and DTYPE=3');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumSelfInSS:=rSumSelfInSS+rv(quSRSum.AsFloat);   // ������������� ������� �����������
              quS.Next;
            end;
            quS.Active:=False;
          end;                }
//-----------------------------------------------------------------------------------------------

          //���� �������������� ��� ���������

          rSumInv:=0;
          iTInv:=0;

          if iSS<100 then //������
          begin
            quSInv.Active:=False;

            quSInv.SQL.Clear;
            quSInv.SQL.Add('select "SumRecord" , "SumActual" , "Detail"  from "InventryHead"');
            quSInv.SQL.Add('where  DateInventry='''+ds(iCurD)+'''');
            quSInv.SQL.Add('and "Depart"='+its(iD));
            quSInv.SQL.Add('and Item=1');  //������� ��������� � ��
            quSInv.SQL.Add('and Status<>''�''');  //������������
            quSInv.Active:=True;
            if quSInv.RecordCount>0 then
            begin
              if quSInvDetail.AsInteger=0 then
              begin
                rSumInv:=RV(quSInvSumActual.AsFloat);
                iTInv:=1;
              end
              else
              begin
                rSumInv:=RV(quSInvSumActual.AsFloat-quSInvSumRecord.AsFloat);
                iTInv:=2;
              end;
            end;
            quSInv.Active:=False;
          end;

          prM('    ����� ...');

//����� Zreport ������ - ��� ��� �������
//����� ����� SaleByDep

          quSaleDep.Active:=False;
          quSaleDep.SQL.Clear;
          quSaleDep.SQL.Add('Select SUM(Summa)as rSum ,SUM(SummaCor)as rSumD from "SaleByDepart"');
          quSaleDep.SQL.Add('where DateSale='''+ds(iCurD)+''' and Otdel='+its(iD));
          quSaleDep.Active:=True;

          rSumReal:=rv(quSaleDeprSum.asfloat);
          rSumDisc:=rv(quSaleDeprSumD.AsFloat);

          quSaleDep.Active:=False;
          quSaleDep.SQL.Clear;
          quSaleDep.SQL.Add('Select SUM(Summa)as rSum ,SUM(SummaCor)as rSumD from "SaleByDepartBN"');
          quSaleDep.SQL.Add('where DateSale='''+ds(iCurD)+''' and Otdel='+its(iD));
          quSaleDep.Active:=True;

          rSumRealBn:=rv(quSaleDeprSum.asfloat);
          rSumDiscBn:=rv(quSaleDeprSumD.AsFloat);

          quSaleDep.Active:=False;

          rSumRealSS:=0;

          prM('    ��� �� = '+its(iSS));

          if iSS=2 then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('select SUM(SUMIN0) as RSUM from "A_PARTOUT"');
            quS.SQL.Add('where  IDATE='+its(iCurD));
            quS.SQL.Add('and IDSTORE='+its(iD));
            quS.SQL.Add('and ITYPE=7');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumRealSS:=rSumRealSS+rv(quSRSum.AsFloat);
              quS.Next;
            end;
            quS.Active:=False;
          end;

          if iSS=1 then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('select SUM(SUMIN) as RSUM from "A_PARTOUT"');
            quS.SQL.Add('where  IDATE='+its(iCurD));
            quS.SQL.Add('and IDSTORE='+its(iD));
            quS.SQL.Add('and ITYPE=7');
            quS.Active:=True;

            quS.First;
            while not quS.Eof do
            begin
              rSumRealSS:=rSumRealSS+rv(quSRSum.AsFloat);
              quS.Next;
            end;
            quS.Active:=False;
          end;

          prM('       ����� '+fs(rv(rSumRealSS)));


          prM('    ��������� ...');

          {������� - �������� ���}
          if rSumInv<>100000000 then //�������� -  ����  rSumInv<>1
          begin
            if iTInv=1 then  //������ �������������� -  ������� �� ����� �� ����� ��
            begin
              if iSS=0 then
                rSumInv:=rv(rSumInv)-(rSumB+RSumIn-rSumOut-rSumReal-rSumDisc-rSumRealBn-rSumDiscBn+rSumAC-rSumSelfOut+rSumSelfIn); //�������� ������� �� ����� �� ����� ��� ��������������
              if iSS>0 then
                rSumInv:=rv(rSumInv)-(rSumB+RSumIn-rSumOut-rSumRealSS-rSumSelfOutSS); //�������� ������� �� ����� �� ����� ��� ��������������

            end;    //� ���� �� 1 �� ��� �������� - ��������� � �� ��� ����
          end;

          //�������----------------------------------------------------------------------------------
          //if rSumInv<>0 then
          //begin
          //if iTInv=1 then  //������ �������������� -  ������� �� ����� �� ����� ��
          //rSumInv:=rSumInv;//-(rSumB+RSumIn-rSumOut-rSumReal-rSumDisc+rSumAC-rSumSelfOut+rSumSelfIn); //�������� ������� �� ����� �� ����� ��� ��������������

          //� ���� �� 1 �� ��� �������� - ��������� � �� ��� ����
          //end;
          //-----------------------------------------------------------------------------------------

          // ��� ��������� - ��������� ������
          if iSS=0 then
          begin
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "REPORTS" values (');
            quA.SQL.Add(its(iD)+',');//Otdel,
            quA.SQL.Add(''''+ds(iCurD)+''',');//xDate,
            quA.SQL.Add(fs(rv(rSumB))+',');//OldOstatokTovar,
            quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovar,
            quA.SQL.Add(fs(rv(rSumSelfIn))+',');//SelfPrihodTovar,
            quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovarSumma,
            quA.SQL.Add(fs(rv(rSumOut))+',');//RashodTovar,
            quA.SQL.Add(fs(rv(rSumSelfOut))+',');//SelfRashodTovar,
            quA.SQL.Add(fs(rv(rSumReal))+',');//RealTovar,
            quA.SQL.Add(fs(rv(rSumDisc))+',');//SkidkaTovar,
            quA.SQL.Add(fs(rv(rSumRealBn))+',');//SenderSumma,
            quA.SQL.Add(fs(rv(rSumDiscBn))+',');//SenderSkidka,
            quA.SQL.Add(fs(rv(rSumInv))+',');//OutTovar, //��������������
            quA.SQL.Add(fs(rv(rSumReal+rSumDisc+rSumOut+rSumRealBn+rSumDiscBn+rSumSelfOut))+',');//RashodTovarSumma,
            quA.SQL.Add(fs(rv(rSumAC))+',');//PereoTovarSumma,
            quA.SQL.Add(fs(rv(rSumB+RSumIn-rSumOut-rSumReal-rSumDisc-rSumRealBn-rSumDiscBn+rSumAC-rSumSelfOut+rSumSelfIn+rSumInv))+',');//NewOstatokTovar,
            quA.SQL.Add(fs(rv(rSumBT))+',');//OldOstatokTara,
            quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTara,
            quA.SQL.Add(fs(0)+',');//SelfPrihodTara,
            quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTaraSumma,
            quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTara,
            quA.SQL.Add(fs(0)+',');//SelfRashodTara,
            quA.SQL.Add(fs(rSumSelfOutSS)+',');//RezervSumma, //�������������  ����������� �������
            quA.SQL.Add(fs(rSumSelfInSS)+',');//RezervSkidka, //������������� ����������� �������
            quA.SQL.Add(fs(0)+',');//OutTara,
            quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTaraSumma,
            quA.SQL.Add(fs(rSumRealSS)+',');//xRezerv,     //������������� ������ �� �����
            quA.SQL.Add(fs(rv(rSumBT+rSumInT-rSumOutT))+',');//NewOstatokTara,
            quA.SQL.Add('0');//Oprihod
            quA.SQL.Add(')');
          end;

          if (iSS=1)or(iSS=2) then
          begin
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "REPORTS" values (');
            quA.SQL.Add(its(iD)+',');//Otdel,
            quA.SQL.Add(''''+ds(iCurD)+''',');//xDate,
            quA.SQL.Add(fs(rv(rSumB))+',');//OldOstatokTovar,
            quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovar,
            quA.SQL.Add(fs(rv(rSumSelfInSS))+',');//SelfPrihodTovar,
            quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovarSumma,
            quA.SQL.Add(fs(rv(rSumOut))+',');//RashodTovar,
            quA.SQL.Add(fs(rv(rSumSelfOutSS))+',');//SelfRashodTovar,
            quA.SQL.Add(fs(rv(rSumReal))+',');//RealTovar,
            quA.SQL.Add(fs(rv(rSumDisc))+',');//SkidkaTovar,
            quA.SQL.Add(fs(rv(rSumRealBn))+',');//SenderSumma,
            quA.SQL.Add(fs(rv(rSumDiscBn))+',');//SenderSkidka,
            quA.SQL.Add(fs(rv(rSumInv))+',');//OutTovar, //��������������
            quA.SQL.Add(fs(rv(rSumRealSS+rSumOut+rSumSelfOutSS))+',');//RashodTovarSumma,
            quA.SQL.Add(fs(rv(0))+',');//PereoTovarSumma,
            quA.SQL.Add(fs(rSumB+RSumIn-rSumOut-rSumRealSS-rSumSelfOutSS+rSumSelfInSS+rSumInv)+',');//NewOstatokTovar,
            quA.SQL.Add(fs(rv(rSumBT))+',');//OldOstatokTara,
            quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTara,
            quA.SQL.Add(fs(0)+',');//SelfPrihodTara,
            quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTaraSumma,
            quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTara,
            quA.SQL.Add(fs(0)+',');//SelfRashodTara,
            quA.SQL.Add(fs(rSumSelfOutSS)+',');//RezervSumma, //�������������  ����������� �������
            quA.SQL.Add(fs(rSumSelfInSS)+',');//RezervSkidka, //������������� ����������� �������
            quA.SQL.Add(fs(0)+',');//OutTara,
            quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTaraSumma,
            quA.SQL.Add(fs(rSumRealSS)+',');//xRezerv,     //������������� ������ �� �����
            quA.SQL.Add(fs(rv(rSumBT+rSumInT-rSumOutT))+',');//NewOstatokTara,
            quA.SQL.Add('0');//Oprihod
            quA.SQL.Add(')');
          end;
          quA.ExecSQL;
        end;
        prM('    ��.');

        quDepsTO.Next;
        prM('     ��.'+its(quDepsTOID.AsInteger));
      end;

          //
      IdMain:=0;
      quDepsTO.First;
      while not quDepsTO.Eof do
      begin
        if quDepsTOV03.AsInteger=1 then IdMain:=quDepsTOID.AsInteger;
        quDepsTO.Next;
      end;
      if IdMain>0 then
      begin
        prM('');
        prM('  ��������� �����. �����...');

        for iCurD:=iDateB to iDateE do
        begin
          //��� ����������, ���� ����� ������� �� ������
          prM('    ���� ������� �� ������ '+ds(iCurD)+'  ...');
          quRepTO.Active:=False;
          quRepTO.ParamByName('DATEB').AsDate:=iCurD;
          quRepTO.ParamByName('IDDEP').AsInteger:=IdMain;
          quRepTO.Active:=True;
          if quRepTO.RecordCount>0 then
          begin
            rSumB:=rv(quRepTONewOstatokTovar.AsFloat);
            rSumBT:=rv(quRepTONewOstatokTara.AsFloat);
          end else
          begin
            rSumB:=0;
            rSumBT:=0;
          end;
          quRepTO.Active:=False;

          rSumIn:=0;
          rSumInT:=0;
          rSumOut:=0;
          rSumOutT:=0;
          rSumAC:=0;
          rSumSelfOut:=0;
          rSumSelfIn:=0;
          rSumInv:=0;
          rSumReal:=0;
          rSumDisc:=0;
          rSumRealBn:=0;
          rSumDiscBn:=0;

          //������ ��� �����������
          prM('    ������� ���������...');
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "REPORTS"');
          quD.SQL.Add('where "xDate">='''+ds(iCurD)+'''');
          quD.SQL.Add('and Otdel='+its(IdMain));
          quD.ExecSQL;

          prM('    ������� ���������...');

          quRepTO2.Active:=False;
          quRepTO2.ParamByName('IDATE').AsDate:=iCurD;
          quRepTO2.Active:=True;
          if quRepTO2.RecordCount>0 then
          begin
            rSumIn:=rv(quRepTO2PrihodTovar.AsFloat);
            rSumInT:=rv(quRepTO2PrihodTara.AsFloat);
            rSumOut:=rv(quRepTO2RashodTovar.AsFloat);
            rSumOutT:=rv(quRepTO2RashodTara.AsFloat);
            rSumAC:=rv(quRepTO2PereoTovarSumma.AsFloat);
            rSumSelfOut:=rv(quRepTO2SelfRashodTovar.AsFloat);
            rSumSelfIn:=rv(quRepTO2SelfPrihodTovar.AsFloat);
            rSumInv:=rv(quRepTO2OutTovar.AsFloat);
            rSumReal:=rv(quRepTO2RealTovar.AsFloat);
            rSumDisc:=rv(quRepTO2SkidkaTovar.AsFloat);
            rSumRealBn:=rv(quRepTO2SenderSumma.AsFloat);
            rSumDiscBn:=rv(quRepTO2SenderSkidka.AsFloat);
          end;

          // ��� ��������� -  ��������� ������
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "REPORTS" values (');
          quA.SQL.Add(its(IdMain)+',');//Otdel,
          quA.SQL.Add(''''+ds(iCurD)+''',');//xDate,
          quA.SQL.Add(fs(rv(rSumB))+',');//OldOstatokTovar,
          quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovar,
          quA.SQL.Add(fs(rv(rSumSelfIn))+',');//SelfPrihodTovar,
          quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovarSumma,
          quA.SQL.Add(fs(rv(rSumOut))+',');//RashodTovar,
          quA.SQL.Add(fs(rv(rSumSelfOut))+',');//SelfRashodTovar,
          quA.SQL.Add(fs(rv(rSumReal))+',');//RealTovar,
          quA.SQL.Add(fs(rv(rSumDisc))+',');//SkidkaTovar,
          quA.SQL.Add(fs(rv(rSumRealBn))+',');//SenderSumma,
          quA.SQL.Add(fs(rv(rSumDiscBn))+',');//SenderSkidka,
          quA.SQL.Add(fs(rv(rSumInv))+',');//OutTovar, //��������������
          quA.SQL.Add(fs(rv(rSumReal+rSumDisc+rSumOut+rSumRealBn+rSumDiscBn+rSumSelfOut))+',');//RashodTovarSumma,
          quA.SQL.Add(fs(rv(rSumAC))+',');//PereoTovarSumma,
          quA.SQL.Add(fs(rv(rSumB+RSumIn-rSumOut-rSumReal-rSumDisc-rSumRealBn-rSumDiscBn+rSumAC-rSumSelfOut+rSumSelfIn+rSumInv))+',');//NewOstatokTovar,
          quA.SQL.Add(fs(rv(rSumBT))+',');//OldOstatokTara,
          quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTara,
          quA.SQL.Add(fs(0)+',');//SelfPrihodTara,
          quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTaraSumma,
          quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTara,
          quA.SQL.Add(fs(0)+',');//SelfRashodTara,
          quA.SQL.Add(fs(0)+',');//RezervSumma,
          quA.SQL.Add(fs(0)+',');//RezervSkidka,
          quA.SQL.Add(fs(0)+',');//OutTara,
          quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTaraSumma,
          quA.SQL.Add(fs(0)+',');//xRezerv,
          quA.SQL.Add(fs(rv(rSumBT+rSumInT-rSumOutT))+',');//NewOstatokTara,
          quA.SQL.Add('0');//Oprihod

          quA.SQL.Add(')');
          quA.ExecSQL;

          prM('    ��.');
        end;
      end;
      //�������
      if NeedPost(iDateB) then
      begin

        iNumPost:=PostNum(0)*100;
        prM('  ������������ ��������� ��� �� ('+its(iNumPost)+')');
        try
          assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
          rewrite(fPost);


          dsquReports.DataSet:=nil;
          quReports.Active:=False;
          quReports.ParamByName('DATEB').AsDate:=iDateB;
          quReports.ParamByName('DATEE').AsDate:=iDateE;
          quReports.Active:=True;

          quReports.First;
          while not quReports.Eof do
          begin
            StrPost:='TO;ON;'+its(quReportsOtdel.AsInteger)+r+ds(quReportsxDate.AsDateTime);
            StrPost:=StrPost+r+fs(quReportsOldOstatokTovar.AsFloat)+r+fs(quReportsPrihodTovar.AsFloat)+r+fs(quReportsSelfPrihodTovar.AsFloat)+r+fs(quReportsPrihodTovarSumma.AsFloat)+r+fs(quReportsRashodTovar.AsFloat)+r+fs(quReportsSelfRashodTovar.AsFloat);
            StrPost:=StrPost+r+fs(quReportsRealTovar.AsFloat)+r+fs(quReportsSkidkaTovar.AsFloat)+r+fs(quReportsSenderSumma.AsFloat)+r+fs(quReportsSenderSkidka.AsFloat)+r+fs(quReportsOutTovar.AsFloat)+r+fs(quReportsRashodTovarSumma.AsFloat);
            StrPost:=StrPost+r+fs(quReportsPereoTovarSumma.AsFloat)+r+fs(quReportsNewOstatokTovar.AsFloat)+r+fs(quReportsOldOstatokTara.AsFloat)+r+fs(quReportsPrihodTara.AsFloat)+r+fs(quReportsSelfPrihodTara.AsFloat)+r+fs(quReportsRashodTara.AsFloat);
            StrPost:=StrPost+r+fs(quReportsSelfRashodTara.AsFloat)+r+fs(quReportsOutTara.AsFloat)+r+fs(quReportsNewOstatokTara.AsFloat)+r;

            writeln(fPost,StrPost);
            quReports.Next;
          end;
          dsquReports.DataSet:=quReports;
        finally
          closefile(fPost);
        end;
              //������ ����� - ���� ��������
        prM('  ���������. ');
        if prTr(sNumPost(iNumPost),Memo1) then prM(' ��') else prM(' ������');
      end;
    except
    end;
  end;
end;

procedure prRecalcTOSv(iDateB,iDateE:Integer;Memo1:TcxMemo;iDays:INteger); //�������� �������� �������
Var iCurD:INteger;
    rSumIn,rSumOut,rSumReal,rSumDisc,rSumAC,rSumRealBn,rSumDiscBn,rSumSelfIn,rSumSelfOut,rSumInv:Real;
    rSumBT,rSumInT,rSumOutT:Real;
    IdMain:INteger;
    rSumBeg,rSumEnd:Real;

  procedure prM(S:String);
  begin
    if Memo1<>nil then  Memo1.Lines.Add(S);
    WriteHistory(S);
  end;

begin
  with dmMC do
  begin
    if CommonSet.NeedRecalcTO=0 then exit;

    prM('  ������ ������������ ������� ... �����.');

    try
      //��������� ������������ �� �������
      prM('    ���� ��������');

      IdMain:=0;

      quDepsTO.Active:=False;
      quDepsTO.Active:=True;

      quDepsTO.First;
      while not quDepsTO.Eof do
      begin
        if quDepsTOV03.AsInteger=1 then IdMain:=quDepsTOID.AsInteger;
        quDepsTO.Next;
      end;
      if IdMain>0 then
      begin
        prM('    �������� �� - '+its(IdMain));

        prM('');
        prM('  ��������� �����. �����...'); delay(10);

        for iCurD:=iDateB to iDateE do
        begin
          //��� ����������, ���� ����� ������� �� ������
          {
          prM('    ���� ������� �� ������ '+ds(iCurD)+'  ...');
          quRepTO.Active:=False;
          quRepTO.ParamByName('DATEB').AsDate:=iCurD;
          quRepTO.ParamByName('IDDEP').AsInteger:=IdMain;
          quRepTO.Active:=True;
          if quRepTO.RecordCount>0 then
          begin
            rSumB:=rv(quRepTONewOstatokTovar.AsFloat);
            rSumBT:=rv(quRepTONewOstatokTara.AsFloat);
          end else
          begin
            rSumB:=0;
            rSumBT:=0;
          end;
          quRepTO.Active:=False;
          }
          rSumBeg:=0;
          rSumIn:=0;
          rSumInT:=0;
          rSumOut:=0;
          rSumOutT:=0;
          rSumAC:=0;
          rSumSelfOut:=0;
          rSumSelfIn:=0;
          rSumInv:=0;
          rSumReal:=0;
          rSumDisc:=0;
          rSumRealBn:=0;
          rSumDiscBn:=0;
          rSumEnd:=0;
          rSumBT:=0;

          //������ ��� �����������
          prM('    ������� ���������...');
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "REPORTS"');
          quD.SQL.Add('where "xDate">='''+ds(iCurD)+'''');
          quD.SQL.Add('and Otdel='+its(IdMain));
          quD.ExecSQL;

          prM('    ������� ���������...');

          quRepTO2.Active:=False;
          quRepTO2.ParamByName('IDATE').AsDate:=iCurD;
          quRepTO2.Active:=True;
          if quRepTO2.RecordCount>0 then
          begin
            rSumBeg:=rv(quRepTO2OldOstatokTovar.AsFloat);
            rSumIn:=rv(quRepTO2PrihodTovar.AsFloat);
            rSumInT:=rv(quRepTO2PrihodTara.AsFloat);
            rSumOut:=rv(quRepTO2RashodTovar.AsFloat);
            rSumOutT:=rv(quRepTO2RashodTara.AsFloat);
            rSumAC:=rv(quRepTO2PereoTovarSumma.AsFloat);
            rSumSelfOut:=rv(quRepTO2SelfRashodTovar.AsFloat);
            rSumSelfIn:=rv(quRepTO2SelfPrihodTovar.AsFloat);
            rSumInv:=rv(quRepTO2OutTovar.AsFloat);
            rSumReal:=rv(quRepTO2RealTovar.AsFloat);
            rSumDisc:=rv(quRepTO2SkidkaTovar.AsFloat);
            rSumRealBn:=rv(quRepTO2SenderSumma.AsFloat);
            rSumDiscBn:=rv(quRepTO2SenderSkidka.AsFloat);
            rSumEnd:=rv(quRepTO2NewOstatokTovar.AsFloat);
            rSumBT:=rv(quRepTO2OldOstatokTara.AsFloat);
          end;

          // ��� ��������� -  ��������� ������
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "REPORTS" values (');
          quA.SQL.Add(its(IdMain)+',');//Otdel,
          quA.SQL.Add(''''+ds(iCurD)+''',');//xDate,
          quA.SQL.Add(fs(rv(rSumBeg))+',');//OldOstatokTovar,
          quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovar,
          quA.SQL.Add(fs(rv(rSumSelfIn))+',');//SelfPrihodTovar,
          quA.SQL.Add(fs(rv(rSumIn))+',');//PrihodTovarSumma,
          quA.SQL.Add(fs(rv(rSumOut))+',');//RashodTovar,
          quA.SQL.Add(fs(rv(rSumSelfOut))+',');//SelfRashodTovar,
          quA.SQL.Add(fs(rv(rSumReal))+',');//RealTovar,
          quA.SQL.Add(fs(rv(rSumDisc))+',');//SkidkaTovar,
          quA.SQL.Add(fs(rv(rSumRealBn))+',');//SenderSumma,
          quA.SQL.Add(fs(rv(rSumDiscBn))+',');//SenderSkidka,
          quA.SQL.Add(fs(rv(rSumInv))+',');//OutTovar, //��������������
          quA.SQL.Add(fs(rv(rSumReal+rSumDisc+rSumOut+rSumRealBn+rSumDiscBn+rSumSelfOut))+',');//RashodTovarSumma,
          quA.SQL.Add(fs(rv(rSumAC))+',');//PereoTovarSumma,
//          quA.SQL.Add(fs(rv(rSumB+RSumIn-rSumOut-rSumReal-rSumDisc-rSumRealBn-rSumDiscBn+rSumAC-rSumSelfOut+rSumSelfIn+rSumInv))+',');//NewOstatokTovar,
          quA.SQL.Add(fs(rv(rSumEnd))+',');//NewOstatokTovar,
          quA.SQL.Add(fs(rv(rSumBT))+',');//OldOstatokTara,
          quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTara,
          quA.SQL.Add(fs(0)+',');//SelfPrihodTara,
          quA.SQL.Add(fs(rv(rSumInT))+',');//PrihodTaraSumma,
          quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTara,
          quA.SQL.Add(fs(0)+',');//SelfRashodTara,
          quA.SQL.Add(fs(0)+',');//RezervSumma,
          quA.SQL.Add(fs(0)+',');//RezervSkidka,
          quA.SQL.Add(fs(0)+',');//OutTara,
          quA.SQL.Add(fs(rv(rSumOutT))+',');//RashodTaraSumma,
          quA.SQL.Add(fs(0)+',');//xRezerv,
          quA.SQL.Add(fs(rv(rSumBT+rSumInT-rSumOutT))+',');//NewOstatokTara,
          quA.SQL.Add('0');//Oprihod

          quA.SQL.Add(')');
          quA.ExecSQL;

          prM('    ��.');
        end;
      end else
      begin
        prM('    �������� �� ������');
      end;
    except
    end;
  end;
end;


function fGetSlave(Id:INteger):String;  //������ ������ ������� ���� ������� � ��������������
Var StrWk:String;
    quSlave:TPvQuery;
begin
  with dmMC do
  begin
    quSlave:=TPvQuery.Create(Application);
    quSlave.Active:=False;
    quSlave.DatabaseName:='PSQL';
    quSlave.SQL.Clear;
    quSlave.SQL.Add('select ID from "SubGroup"');
    quSlave.SQL.Add('where GoodsGroupID='+IntToStr(Id));
    quSlave.Active:=True;
    quSlave.First;
    while not quSlave.Eof do
    begin
      StrWk:=StrWk+INtToStr(quSlave.FieldByName('ID').AsInteger)+','+fGetSlave(quSlave.FieldByName('ID').AsInteger);
      quSlave.Next;
    end;
    quSlave.Active:=False;
    quSlave.Free;
  end;
  Result:=StrWk;
end;



Procedure TdmMC.ClassExpNew( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode : TTreeNode;
    quC:TPvQuery;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);

  if CommonSet.ClassType=1 then
  begin
    quC:=TPvQuery.Create(Application);
    quC.Active:=False;
    quC.DatabaseName:='PSQL';
    quC.SQL.Clear;
    quC.SQL.Add('select  GoodsGroupID,ID,Name,Reserv1 from "SubGroup"');
    quC.SQL.Add('where GoodsGroupID='+IntToStr(Id));
    quC.SQL.Add('order by Name');
    quC.Active:=True;
    quC.First;
    while not quC.Eof do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node, quC.FieldByName('Name').AsString+' ('+fs(quC.FieldByName('Reserv1').AsFloat)+'%)', Pointer(quC.FieldByName('Id').AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      ClassExpNew(TreeNode,Tree);

      quC.Next;
    end;
    quC.Active:=False;
    quC.Free;
  end;

  if CommonSet.ClassType=0 then
  begin
    quC:=TPvQuery.Create(Application);
    quC.Active:=False;
    quC.DatabaseName:='PSQL';
    quC.SQL.Clear;

    if ID=0 then
    begin
      quC.SQL.Add('select 0 as GoodsGroupID,(ID+10000) as ID,Name,Reserv1  from "GdsGroup"');
      quC.SQL.Add('order by Name');
    end else
    begin
      quC.SQL.Add('select  GoodsGroupID,ID,Name,Reserv1 from "SubGroup"');
      quC.SQL.Add('where GoodsGroupID='+IntToStr(Id-10000));
      quC.SQL.Add('order by Name');
    end;

    quC.Active:=True;
    quC.First;
    while not quC.Eof do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node, quC.FieldByName('Name').AsString+' ( ���. '+fs(quC.FieldByName('Reserv1').AsFloat)+'%)', Pointer(quC.FieldByName('Id').AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      ClassExpNew(TreeNode,Tree);

      quC.Next;
    end;
    quC.Active:=False;
    quC.Free;
  end;

end;


function prTestGen(iCurDate,Id:INteger):Boolean;
begin
  Result:=True;
end;

procedure prAddToScale(sSc:String);
Var sN1,sN2,s:String;
    iPlu,l:Integer;
    rPr:Real;
    iPr:INteger;
    sPr:String;
    NumF:Integer;
begin
  with dmMC do
  with dmMt do
  begin
    if taToScale.Active then
    begin
      taToScale.First;
      while not taToScale.Eof do
      begin

        sN1:=Copy(taToScaleName.AsString,1,29);
        l:=28;
        if (Pos(' ',sN1)>0) and(length(taToScaleName.AsString)>28) then
        begin
          s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
          while s<>' ' do
          begin
            dec(l);
            s:=sN1[l];
          end;
        end;

        sN1:=Copy(taToScaleName.AsString,1,l);
        if Length(taToScaleName.AsString)>l then sN2:=Copy(taToScaleName.AsString,(l+1),28)
                                            else sN2:='';
        NumF:=0;
        if taCards.FindKey([taToScaleId.AsInteger]) then NumF:=taCardsV09.AsInteger;

        if prFindPluSc(taToScaleId.AsInteger,sSc) then
        begin
          //�������
          rPr:=RoundVal(taToScalePrice.AsFloat)*100;
          iPr:=RoundEx(rPr);
          sPr:=its(iPr);
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "ScalePLU" Set ');
          quE.SQL.Add('FirstName='''+sN1+''',');
          quE.SQL.Add('LastName='''+sN2+''',');
          quE.SQL.Add('Price='+sPr+',');
          quE.SQL.Add('ShelfLife='+its(taToScaleSrok.AsInteger)+',');
          quE.SQL.Add('Status=0,');
          quE.SQL.Add('MessageNo='+its(NumF));
          quE.SQL.Add('where Number='''+sSc+''' and GoodsItem='+its(taToScaleId.AsInteger));
          quE.ExecSQL;
        end else
        begin
          //�������� ��� ��������� ��� ������� - ����
          quFPlu.Active:=False;
          quFPlu.ParamByName('SNUM').AsString:=sSc;
          quFPlu.Active:=True;
          iPlu:=1;
          quFPlu.First;
          while not quFPlu.Eof do
          begin
            if iPlu<>quFPluPLU.AsInteger then break; //����� �����
            quFPlu.Next; inc(iPlu);
          end;
          quFPlu.Active:=False;

          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "ScalePLU" values (');
          quA.SQL.Add(''''+sSc+''',');//Number
          quA.SQL.Add('4,');//Depart
          quA.SQL.Add(its(iPlu)+',');//PLU
          quA.SQL.Add(its(taToScaleId.AsInteger)+',');//GoodsItem
          quA.SQL.Add(''''+sN1+''',');//FirstName
          quA.SQL.Add(''''+sN2+''',');//LastName
          quA.SQL.Add(its(RoundEx(taToScalePrice.AsFloat*100))+',');//Price
          quA.SQL.Add(its(taToScaleSrok.AsInteger)+',');//ShelfLife
          quA.SQL.Add('0,');//TareWeight
          quA.SQL.Add('22,');//GroupCode
          quA.SQL.Add(its(NumF)+',');//MessageNo
          quA.SQL.Add('0');//Status
          quA.SQL.Add(')');
          quA.ExecSQL;
        end;

        taToScale.Next;
      end;
    end;
  end;
end;

function prFindPluSc(iC:INteger;sSc:String):Boolean;
begin
  Result:=False;
  with dmMC do
  begin
    quFindPlu.Active:=False;
    quFindPlu.ParamByName('IDCARD').AsInteger:=iC;
    quFindPlu.Active:=True;
    if quFindPlu.RecordCount>0 then
    begin
      quFindPlu.First;
      while not quFindPlu.Eof do
      begin
        if quFindPluNumber.AsString=sSc then Result:=True;
        quFindPlu.Next;
      end;
    end;
    quFindPlu.Active:=False;
  end;
end;

function prFindPlu(iC:INteger):String;
Var Str1,StrSt:String;
begin
  Result:='';
  with dmMC do
  begin
    quFindPlu.Active:=False;
    quFindPlu.ParamByName('IDCARD').AsInteger:=iC;
    quFindPlu.Active:=True;
    Str1:='';
    if quFindPlu.RecordCount>0 then
    begin
      quFindPlu.First;
      while not quFindPlu.Eof do
      begin
        StrSt:='';
//        if quFindPluStatus.AsInteger=1 then StrSt:=' (����.)' else StrSt:=' (�� ����.)';
        Str1:=Str1+quFindPluNumber.AsString+'-'+quFindPluPLU.AsString+StrSt+' , ';
        quFindPlu.Next;
      end;
      Result:=Copy(Str1,1,length(Str1)-2);
    end;
    quFindPlu.Active:=False;
  end;
end;

function prFindSUMTO(iDep,iDate:Integer):Real;
begin
  with dmMC do
  begin
    Result:=0;
    quRepTO1.Active:=False;
    quRepTO1.ParamByName('IDEP').AsInteger:=iDep;
    quRepTO1.ParamByName('IDATE').AsDate:=iDate;
    quRepTO1.Active:=True;
    if quRepTO1.RecordCount>0 then
    begin
      quRepTO1.First;
      Result:=quRepTO1NewOstatokTovar.AsFloat-quRepTO1OutTovar.AsFloat;
    end;
    quRepTO1.Active:=False;
  end;
end;

procedure prLogPrice(IdCard,iSt:INteger;OldPrice,NewPrice:Real);
begin
  with dmMC do  //
  begin
    quA.SQL.Clear;
    quA.SQL.Add('INSERT into "GdcPrice" values (');
    quA.SQL.Add(its(IdCard)+','); //GoodsID
    quA.SQL.Add(fs(OldPrice)+',');  //OldPrice
    quA.SQL.Add(fs(NewPrice)+',');  //NewPrice
    quA.SQL.Add(''''+dts(Date)+''',');    //DateBegin
    quA.SQL.Add(''''+ts(now)+''','); //TimeBegin
    quA.SQL.Add(its(Person.Id)+',');      //xUser
    quA.SQL.Add(its(iSt)+',');         //Status
    quA.SQL.Add('4');      //DepartID
    quA.SQL.Add(')');
    quA.ExecSQL;
  end;
end;

procedure prAddHist(iTd,IdDoc,DocDate:INteger;DocNum,NameOp:String;secpost:integer);
// var idop:integer;
begin
  with dmMC do //
  begin
   { if NameOp='�����.' then idop:=1
    else idop:=0; }

    try
      quA.SQL.Clear;
      quA.SQL.Add('INSERT into "A_DOCHIST" values (');
      quA.SQL.Add(its(prMax('Hist')+1)+','); //ID
      quA.SQL.Add(its(iTd)+','); //TYPED
      quA.SQL.Add(its(Person.Id)+',');    //IDPERS
      quA.SQL.Add(''''+dts(Now)+''',');   //DATEEDIT
      quA.SQL.Add(''''+ds(DocDate)+''',');//DOCDATE
      quA.SQL.Add(''''+DocNum+''',');     //DOCNUM
      quA.SQL.Add(its(IdDoc)+','); //DOCID
      quA.SQL.Add(''''+NameOp+''',');     //IOP
      quA.SQL.Add(''''+CommonSet.Ip+'('+Person.Name+')'',');//NAMEPERS
      quA.SQL.Add(its(secpost)+'');     //IOP
      quA.SQL.Add(')');
      quA.ExecSQL;

    except
    end;
  end;
end;


function prOpenDate:TDateTime;
begin
  with dmMC do  //
  begin
    Result:=0;
    quODate.Active:=False;
    quODate.Active:=True;
    if quODate.RecordCount>0 then result:=Trunc(quODateDateClose.AsDateTime);
    quODate.Active:=False;
  end;
end;


function prFindCG(iItem,Depart:INteger):Integer;
Var iCardG:INteger;
begin
  with dmMC do  //
  begin
    iCardG:=0;
    quCG.Active:=False;
    quCG.ParamByName('IDCARD').AsInteger:=iItem;
    quCG.Active:=True;
    quCG.First;
    while not quCG.Eof do
    begin
      if quCGDEPART.AsInteger=Depart then
      begin
        iCardG:=quCGCARD.AsInteger;
        Break;
      end;
      quCG.Next;
    end;
    quCG.Active:=False;

    if iCardG=0 then //�������� , � ������� ������
    begin
      iCardG:=prMax('CG')+1;

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into "CardGoods" values (');
      quA.SQL.Add(its(iCardG)+','); //CARD
      quA.SQL.Add(its(iItem)+',');  //ITEM
      quA.SQL.Add('0,');      //COST
      quA.SQL.Add(its(Depart)+',');    //DEPART
      quA.SQL.Add('0,');            //VENDOR
      quA.SQL.Add('0'); //ATTRIBUTE
      quA.SQL.Add(')');
      quA.ExecSQL;
    end;

    Result:=iCardG;
  end;
end;


function prFindIdCG(iCg:INteger):INteger;
begin
  with dmMC do  //
  begin
    Result:=0;
    quCG1.Active:=False;
    quCG1.ParamByName('IDCARD').AsInteger:=iCg;
    quCG1.Active:=True;
    quCG1.First;
    if quCG1.RecordCount>0 then  Result:=quCG1ITEM.AsInteger;
    quCG1.Active:=False;
  end;
end;

procedure prDelMoveIdC(IdCard,iDate,iDoc,iTD:Integer);
Var iCardG,iC:INteger;
    rQR,rQm:Real;
begin
  with dmMC do  //
  begin
    iCardG:=IdCard;
    if iCardG>0 then
    begin
//      �������� �� ������� - ������� ������ � ������������� ��� ������� ����� ���� ���� �� ���������.

      quD.Active:=False;
      quD.SQL.Clear;
      quD.SQL.Add('Delete from "CardGoodsMove"' );
      quD.SQL.Add('where CARD='+its(iCardG));
      quD.SQL.Add('and DOCUMENT='+its(iDoc));
      quD.SQL.Add('and DOC_TYPE='+its(iTD));
      quD.ExecSQL;

// ���� ������� �� ����

      rQr:=0;
      quCM1.Active:=False;
      quCM1.ParamByName('IDCARD').AsInteger:=iCardG;
      quCM1.ParamByName('DATEE').AsString:=ds(iDate); //��� ������
      quCM1.Active:=True;
      if quCM1.RecordCount>0 then rQr:=quCM1QUANT_REST.AsFloat;
      quCM1.Active:=False;

// ���� ��� �������� ����� ���� � ������������� ���
      quCM2.Active:=False;
      quCM2.ParamByName('IDCARD').AsInteger:=iCardG;
      quCM2.ParamByName('DATEE').AsString:=ds(iDate-1); //��� ������
      quCM2.Active:=True;
      quCM2.First;
      while not quCM2.Eof do
      begin
        rQm:=quCM2QUANT_MOVE.AsFloat;
        rQR:=rQr+rQM;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "CardGoodsMove" Set QUANT_REST='+fs(rQR));
        quE.SQL.Add('where CARD='+its(iCardG));
        quE.SQL.Add('and DOCUMENT='+its(quCM2DOCUMENT.AsInteger));
        quE.SQL.Add('and DOC_TYPE='+its(quCM2DOC_TYPE.AsInteger));
        quE.ExecSQL;

        quCM2.Next;
      end;
      quCM2.Active:=False;

 // ��������� � ��������

      iC:=prFindIdCG(iCardG);
      rQr:=prFindTabRemn(iC);
      prSetR(iC,rQR);

{      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update Goods Set ');
      quE.SQL.Add('Reserv1='+fs(rQR));
      quE.SQL.Add('where ID='+IntToStr(prFindIdCG(iCardG)));
      quE.ExecSQL;}
    end;
  end;
end;


procedure prAddMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
Var iCardG:INteger;
    rQR,rQm:Real;
begin
  with dmMC do  //
  begin
    iCardG:=prFindCG(IdCard,Depart);
    
    if iCardG>0 then
    begin
//    �������� �� ������� - ������� �� ������ ������, ��������� ������ � ������������� ��� ������� ����� ���� ���� �� ���������.

      quD.Active:=False;
      quD.SQL.Clear;
      quD.SQL.Add('Delete from "CardGoodsMove"' );
      quD.SQL.Add('where CARD='+its(iCardG));
      quD.SQL.Add('and DOCUMENT='+its(iDoc));
      quD.SQL.Add('and DOC_TYPE='+its(iTD));
      quD.ExecSQL;


      quA.SQL.Clear;
      quA.SQL.Add('INSERT into "CardGoodsMove" values (');
      quA.SQL.Add(its(iCardG)+',');
      quA.SQL.Add(''''+ds(iDate)+''',');
      quA.SQL.Add(its(iTD)+',');
      quA.SQL.Add(its(iDoc)+',');
      quA.SQL.Add('0,');
      quA.SQL.Add(fs(rQ)+','); //QUANT_MOVE
      quA.SQL.Add('0,');
      quA.SQL.Add('0');
      quA.SQL.Add(')'); // QUANT_REST
      quA.ExecSQL;

// CARD MOVEDATE DOC_TYPE DOCUMENT PRICE QUANT_MOVE SUM_MOVE QUANT_REST

// ���� ������� �� ����

      rQr:=0;
      quCM1.Active:=False;
      quCM1.ParamByName('IDCARD').AsInteger:=iCardG;
      quCM1.ParamByName('DATEE').AsString:=ds(iDate);  //��� ������
      quCM1.Active:=True;
      if quCM1.RecordCount>0 then rQr:=quCM1QUANT_REST.AsFloat;
      quCM1.Active:=False;

// ���� ��� �������� ����� ���� � ������������� ���
      quCM2.Active:=False;
      quCM2.ParamByName('IDCARD').AsInteger:=iCardG;
      quCM2.ParamByName('DATEE').AsString:=ds(iDate-1); //��� ������
      quCM2.Active:=True;
      quCM2.First;
      while not quCM2.Eof do
      begin
        rQm:=quCM2QUANT_MOVE.AsFloat;
        rQR:=rQr+rQM;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "CardGoodsMove" Set QUANT_REST='+fs(rQR));
        quE.SQL.Add('where CARD='+its(iCardG));
        quE.SQL.Add('and DOCUMENT='+its(quCM2DOCUMENT.AsInteger));
        quE.SQL.Add('and DOC_TYPE='+its(quCM2DOC_TYPE.AsInteger));
        quE.ExecSQL;

        quCM2.Next;
      end;
      quCM2.Active:=False;

//��������� � ��������

      rQr:=prFindTabRemn(IdCard);
      prSetR(IdCard,rQR);
{
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update Goods Set ');
      quE.SQL.Add('Reserv1='+fs(rQR));
      quE.SQL.Add('where ID='+IntToStr(IdCard));
      quE.ExecSQL;
}
    end;
  end;
end;


procedure prDelMoveId(Depart,IdCard,iDate,iDoc,iTD:Integer; rQ:Real);
Var iCardG:INteger;
    rQR,rQm:Real;
begin
  with dmMC do  //
  begin
    iCardG:=0;
    quCG.Active:=False;
    quCG.ParamByName('IDCARD').AsInteger:=IdCard;
    quCG.Active:=True;
    quCG.First;
    while not quCG.Eof do
    begin
      if quCGDEPART.AsInteger=Depart then
      begin
        iCardG:=quCGCARD.AsInteger;
        Break;
      end;
      quCG.Next;
    end;
    quCG.Active:=False;

    if iCardG>0 then
    begin
      quM1.Active:=False;
      quM1.ParamByName('IDCARD').AsInteger:=iCardG;
      quM1.ParamByName('IDDOC').AsInteger:=iDoc;
      quM1.ParamByName('DT').AsInteger:=iTD;
      quM1.Active:=True;
      if quM1.RecordCount>0 then
      begin
//      �������� �� ������� - ������� ������ � ������������� ��� ������� ����� ���� ���� �� ���������.

        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from "CardGoodsMove"' );
        quD.SQL.Add('where CARD='+its(iCardG));
        quD.SQL.Add('and DOCUMENT='+its(iDoc));
        quD.SQL.Add('and DOC_TYPE='+its(iTD));
        quD.ExecSQL;

// ���� ������� �� ����

        rQr:=0;
        quCM1.Active:=False;
        quCM1.ParamByName('IDCARD').AsInteger:=iCardG;
        quCM1.ParamByName('DATEE').AsString:=ds(iDate); //��� ������
        quCM1.Active:=True;
        if quCM1.RecordCount>0 then rQr:=quCM1QUANT_REST.AsFloat;
        quCM1.Active:=False;

// ���� ��� �������� ����� ���� � ������������� ���
        quCM2.Active:=False;
        quCM2.ParamByName('IDCARD').AsInteger:=iCardG;
        quCM2.ParamByName('DATEE').AsString:=ds(iDate-1); //��� ������
        quCM2.Active:=True;
        quCM2.First;
        while not quCM2.Eof do
        begin
          rQm:=quCM2QUANT_MOVE.AsFloat;
          rQR:=rQr+rQM;

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "CardGoodsMove" Set QUANT_REST='+fs(rQR));
          quE.SQL.Add('where CARD='+its(iCardG));
          quE.SQL.Add('and DOCUMENT='+its(quCM2DOCUMENT.AsInteger));
          quE.SQL.Add('and DOC_TYPE='+its(quCM2DOC_TYPE.AsInteger));
          quE.ExecSQL;

          quCM2.Next;
        end;
        quCM2.Active:=False;

 //��������� � ��������
        rQr:=prFindTabRemn(IdCard);
        prSetR(IdCard,rQR);

{        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update Goods Set ');            

        quE.SQL.Add('Reserv1='+fs(rQR));
        quE.SQL.Add('where ID='+IntToStr(IdCard));
        quE.ExecSQL;
}
      end;
      quM1.Active:=False;
    end;
  end;
end;

function prFindNum(TypeD:Integer):Integer;       //����� ��������
Var iNum:INteger;
begin
  with dmMC do  //����� ���������
  begin
    Result:=0;
    if TypeD=2 then   //�������
    begin
      quPool.Active:=False;
      quPool.ParamByName('TD').AsInteger:=11;
      quPool.Active:=True;
      iNum:=quPoolId.AsInteger;
      quPool.Active:=False;

      quE.SQL.Clear;
      quE.SQL.Add('Update "IdPool" Set ID='+its(iNum+1));
      quE.SQL.Add('where CodeObject=11');
      quE.ExecSQL;

      Result:=iNum;
    end;
    if TypeD=3 then   //��. �����������
    begin
      quPool.Active:=False;
      quPool.ParamByName('TD').AsInteger:=12;
      quPool.Active:=True;
      Result:=quPoolId.AsInteger;
      quPool.Active:=False;
    end;
    if TypeD=4 then   //����������
    begin
      quPool.Active:=False;
      quPool.ParamByName('TD').AsInteger:=13;
      quPool.Active:=True;
      Result:=quPoolId.AsInteger;
      quPool.Active:=False;
    end;
    if TypeD=22 then   //������
    begin
      quPool.Active:=False;
      quPool.ParamByName('TD').AsInteger:=22;
      quPool.Active:=True;
      Result:=quPoolId.AsInteger;
      quPool.Active:=False;
    end;
    if TypeD=25 then   //���������� �� �������
    begin
      quPool.Active:=False;
      quPool.ParamByName('TD').AsInteger:=25;
      quPool.Active:=True;
      if quPool.RecordCount>0 then
      begin
        dmP.quNUM.Active:=false;
        dmP.quNUM.Active:=true;
        dmP.quNUMID.AsInteger;

        iNum:=quPoolId.AsInteger+1;
        if dmP.quNUM.RecordCount>0 then
        begin
          if iNum=dmP.quNUMID.AsInteger then iNum:=iNum+1;
          if iNum<dmP.quNUMID.AsInteger then iNum:=dmP.quNUMID.AsInteger+1;
        end;
        dmP.quNUM.Active:=false;

        quE.SQL.Clear;
        quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
        quE.SQL.Add('where CodeObject=25');
        quE.ExecSQL;
      end
      else
      begin
        dmP.quNUM.Active:=false;
        dmP.quNUM.Active:=true;
        dmP.quNUMID.AsInteger;

        iNum:=1;
        if dmP.quNUM.RecordCount>0 then
        begin
          if iNum=dmP.quNUMID.AsInteger then iNum:=iNum+1;
          if iNum<dmP.quNUMID.AsInteger then iNum:=dmP.quNUMID.AsInteger+1;
        end;
        dmP.quNUM.Active:=false;

        quA.Active:=false;
        quA.SQL.Clear;
        quA.SQL.Add('Insert into "IdPool" values (25,'+its(iNum)+')');
        quA.ExecSQL;
      end;

      Result:=iNum;
      quPool.Active:=False;

    end;
  end;
end;

function prFindRemn3(IdCard,iDate,iDepart:INteger):Real; //������� �� ����� �� ������ ��� �����
Var rSum:Real;
begin
  with dmMC do  //��������� ������� �� ������
  begin
    rSum:=0;
    quCG.Active:=False;
    quCG.ParamByName('IDCARD').AsInteger:=IdCard;
    quCG.Active:=True;
    quCG.First;
    while not quCG.Eof do
    begin
      if (iDepart=0)or(iDepart=quCGDEPART.AsInteger) then
      begin
        quCM1.Active:=False;
        quCM1.ParamByName('IDCARD').AsInteger:=quCGCARD.AsInteger;
        quCM1.ParamByName('DATEE').AsDate:=iDate+1;
{
Select top 1 gm.QUANT_REST
from "CardGoodsMove" gm
where
gm.CARD=:IDCARD
and gm.MOVEDATE<:DATEE
order by gm.MOVEDATE DESC
}
        quCM1.Active:=True;
        if quCM1.RecordCount>0 then rSum:=rSum+quCM1QUANT_REST.AsFloat;
        quCM1.Active:=False;
      end;
      quCG.Next;
    end;
    quCG.Active:=False;
    Result:=rSum;
  end;
end;


function prFindRemn4(CardG,iDate:INteger):Real; //������� �� ����� �� ������ ��� �����  �� CG
Var rSum:Real;
begin
  with dmMC do  //��������� ������� �� ������
  begin
    rSum:=0;
    quCM1.Active:=False;
    quCM1.ParamByName('IDCARD').AsInteger:=CardG;
    quCM1.ParamByName('DATEE').AsDate:=iDate+1;
{
Select top 1 gm.QUANT_REST
from "CardGoodsMove" gm
where
gm.CARD=:IDCARD
and gm.MOVEDATE<:DATEE
order by gm.MOVEDATE DESC , DOC_TYPE
}
    quCM1.Active:=True;
    if quCM1.RecordCount>0 then rSum:=quCM1QUANT_REST.AsFloat;
    quCM1.Active:=False;

    Result:=rSum;
  end;
end;



function prFindRemn2(IdCard:INteger):Real;
Var rSum:Real;
begin
  with dmMC do  //��������� ������� �� ������
  begin
    rSum:=0;
    quCG.Active:=False;
    quCG.ParamByName('IDCARD').AsInteger:=IdCard;
    quCG.Active:=True;
    quCG.First;
    while not quCG.Eof do
    begin
      quCM.Active:=False;
      quCM.ParamByName('IDCARD').AsInteger:=quCGCARD.AsInteger;
      quCM.Active:=True;
      if quCM.RecordCount>0 then rSum:=rSum+quCMQUANT_REST.AsFloat;
      quCM.Active:=False;

      quCG.Next;
    end;
    quCG.Active:=False;
    Result:=rSum;
  end;
end;


function prFindRemn1(IdCard:INteger):Real;
begin
  with dmMC do  //��������� ������� �� ������
  begin
    Result:=0;
    quCM.Active:=False;
    quCM.ParamByName('IDCARD').AsInteger:=IdCard;
    quCM.Active:=True;
    if quCM.RecordCount>0 then Result:=quCMQUANT_REST.AsFloat;
    quCM.Active:=False;
  end;
end;

function prFindRemn(IdCard,Depart:INteger):Real;
begin
  with dmMC do  //��������� ������� �� ������
  begin
    Result:=0;
    quCG.Active:=False;
    quCG.ParamByName('IDCARD').AsInteger:=IdCard;
    quCG.Active:=True;
    quCG.First;
    while not quCG.Eof do
    begin
      if quCGDEPART.AsInteger=Depart then
      begin
        quCM.Active:=False;
        quCM.ParamByName('IDCARD').AsInteger:=quCGCARD.AsInteger;
        quCM.Active:=True;
        if quCM.RecordCount>0 then Result:=quCMQUANT_REST.AsFloat;
        quCM.Active:=False;
      end;
      quCG.Next;
    end;
    quCG.Active:=False;
  end;
end;

procedure prClearBuf(iDDoc,DocType,iDate,iOp:Integer);
begin
  with dmMC do
  begin
    quD.Active:=False;
    quD.SQL.Clear;
    quD.SQL.Add('Delete from A_BUFPRICE where');
    quD.SQL.Add('IDDOC = '+its(iDDoc));
    quD.SQL.Add('and TYPEDOC = '+its(DocType));

    //������������ ������� ������ ��� �������������, ��� ������ ��������� ������ ������������
    if iOp=0 then quD.SQL.Add('and STATUS=0');
//    quD.SQL.Add('and DATEDOC = '''+ds(iDate)+'''');
    quD.ExecSQL;
    delay(50);
  end;
end;

procedure prPriceBuf(IdCard,iDate,IdDoc,iTypeDoc:Integer;DocNum:String;Pr1,Pr2:Real);
begin
  with dmMC do
  begin
//IDCARD   IDDOC   TYPEDOC   OLDP   NEWP   STATUS   NUMDOC  DATEDOC
    try
      quA.Active:=False;
      quA.SQL.Clear;
      quA.SQL.Add('INSERT into "A_BUFPRICE" values (');
      quA.SQL.Add(its(IdCard)+',');
      quA.SQL.Add(its(IdDoc)+',');
      quA.SQL.Add(its(iTypeDoc)+',');
      quA.SQL.Add(fs(Pr1)+',');
      quA.SQL.Add(fs(Pr2)+',');
      quA.SQL.Add('0,');
      quA.SQL.Add(''''+DocNum+''',');
      quA.SQL.Add(''''+ds(iDate)+'''');
      quA.SQL.Add(')');
      quA.ExecSQL;
    except
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "A_BUFPRICE" Set ');
      quE.SQL.Add('OLDP='+fs(Pr1)+',');
      quE.SQL.Add('NEWP='+fs(Pr2)+',');
      quE.SQL.Add('STATUS=0,');
      quE.SQL.Add('NUMDOC='''+DocNum+''',');
      quE.SQL.Add('DATEDOC='''+ds(iDate)+'''');
      quE.SQL.Add('where IDCARD='+its(IdCard));
      quE.SQL.Add('and IDDOC='+its(IdDoc));
      quE.SQL.Add('and TYPEDOC='+its(iTypeDoc));
      quE.ExecSQL;
    end;
  end;
end;

procedure prGds(iArt,iDate,Depart:Integer;QIN,QOUT,QVNIN,QVNOUT,QINV,QREAL:Real);
begin
  with dmMC do
  begin

  end;
end;

Procedure prFCliINN(Id,iT:INteger;Var SInn,SAdr,sFName,sInnF:String);

  function fInn(sInn:String;iT:Integer):String;
  begin
    Result:=sInn;
    if iT=1 then Result:=Copy(sInn,1,Length(sInn)-2);
  end;

begin
  with dmMC do
  begin
    SInn:='';
    SAdr:='';
    sFName:='';
    if iT=1 then
    begin
      quFCli1.Active:=False;
      quFCli1.ParamByName('IDC').AsInteger:=Id;
      quFCli1.Active:=True;
      quFCli1.First;
      if quFCli1.RecordCount>0 then
      begin
        sInnF:=quFCli1INN.AsString;
        SInn:=fInn(quFCli1INN.AsString,quFCli1PayDays.AsInteger)+'/'+quFCli1OKPO.AsString;
        SAdr:=quFCli1PostIndex.AsString+' �.'+quFCli1Gorod.AsString+',��.'+quFCli1Street.AsString+',�.'+quFCli1House.AsString+'-'+quFCli1Kvart.AsString;
        sFName:=quFCli1FullName.AsString;
      end;
      quFCli1.Active:=False;
    end;
    if iT=2 then
    begin
      quFIP1.Active:=False;
      quFIP1.ParamByName('IDC').AsInteger:=Id;
      quFIP1.Active:=True;
      quFIP1.First;
      if quFIP1.RecordCount>0 then
      begin
        SInn:=quFIP1INN.AsString;
        sInnF:=quFIP1INN.AsString;
        SAdr:=quFIP1PostIndex.AsString+' �.'+quFIP1Gorod.AsString+',��.'+quFIP1Street.AsString+',�.'+quFIP1House.AsString+'-'+quFIP1Kvart.AsString;
        sFName:=quFIP1FullName.AsString;
      end;
      quFIP1.Active:=False;
    end;
  end;
end;


Procedure prFDepINN(Id:INteger;Var SInn,SAdr:String);
begin
  with dmMC do
  begin
    SInn:='';
    SAdr:='';
    quDeparts1.Active:=False;
    quDeparts1.ParamByName('IDC').AsInteger:=Id;
    quDeparts1.Active:=True;
    if quDeparts1.RecordCount>0 then
    begin
      SInn:=quDeparts1FullName.AsString;
      SAdr:=quDeparts1EnglishFullName.AsString;
    end;
    quDeparts1.Active:=False;
  end;
end;


Function prFLP(Id:INteger;Var rPriceM:Real):Real;
Var PriceIn:Real;
begin
  with dmMC do
  begin
    PriceIn:=0;
    rPriceM:=0;

    quFLastPrice.Active:=False;
    quFLastPrice.ParamByName('IDC').AsInteger:=Id;
    quFLastPrice.ParamByName('IDATE').AsDate:=Date-300;
    quFLastPrice.Active:=True;
    if quFLastPrice.RecordCount>0 then
    begin
      PriceIn:=quFLastPriceCenaTovar.AsFloat;
      rPriceM:=quFLastPriceNewCenaTovar.AsFloat;
    end;
    quFLastPrice.Active:=False;

    if PriceIn<0.1 then
    begin
      quFLastPriceVn.Active:=False;
      quFLastPriceVn.ParamByName('IDC').AsInteger:=Id;
      quFLastPriceVn.ParamByName('IDATE').AsDate:=Date-300;
      quFLastPriceVn.Active:=True;
      if quFLastPriceVn.RecordCount>0 then
      begin
        PriceIn:=quFLastPriceVNCenaTovarSpis.AsFloat;
      end;
      quFLastPriceVn.Active:=False;
    end;

    Result:=PriceIn;
  end;
end;

Function prFLP0(Id:INteger;Var rPriceM,rPriceIn0:Real;Var sGTD:String):Real;
Var PriceIn:Real;
begin
  with dmMC do
  begin
    PriceIn:=0;
    rPriceM:=0;
    rPriceIn0:=0;
    sGTD:='';

    quFLastPrice.Active:=False;
    quFLastPrice.ParamByName('IDC').AsInteger:=Id;
    quFLastPrice.ParamByName('IDATE').AsDate:=Date-300;
    quFLastPrice.Active:=True;
    if quFLastPrice.RecordCount>0 then
    begin
      PriceIn:=quFLastPriceCenaTovar.AsFloat;
      rPriceM:=quFLastPriceNewCenaTovar.AsFloat;
      if quFLastPriceKol.AsFloat<>0 then rPriceIn0:=rv(quFLastPriceOutNDSSum.AsFloat/quFLastPriceKol.AsFloat);
      sGTD:=quFLastPriceSertNumber.AsString;
    end;
    quFLastPrice.Active:=False;

    if PriceIn<0.1 then
    begin
      quFLastPriceVn.Active:=False;
      quFLastPriceVn.ParamByName('IDC').AsInteger:=Id;
      quFLastPriceVn.ParamByName('IDATE').AsDate:=Date-300;
      quFLastPriceVn.Active:=True;
      if quFLastPriceVn.RecordCount>0 then
      begin
        PriceIn:=quFLastPriceVNCenaTovarSpis.AsFloat;    //��� ��������� ���� ��� � ��� ��� ��� ��� �� ���� ��������� ������ ���� ���������
        rPriceIn0:=quFLastPriceVNCenaTovarSpis.AsFloat;
      end;
      quFLastPriceVn.Active:=False;
    end;

    Result:=PriceIn;
  end;
end;


Function prFC(Id:INteger):Real;
begin
  with dmMC do
  begin
    Result:=0;

    quFC.Active:=False;
    quFC.ParamByName('IDC').AsInteger:=Id;
    quFC.Active:=True;
    if quFC.RecordCount>0 then Result:=quFCCena.AsFloat;
    quFC.Active:=False;
  end;
end;


Procedure prRefrID(quT:TPvQuery;Vi: TcxGridDBTableView;IDH:INteger);
Var ID:Integer;
begin
  Vi.BeginUpdate;  delay(10);
//  ID:=quT.FieldByName('ID').AsInteger;
  if IDH>0 then ID:=IDH
  else ID:=quT.FieldByName('ID').AsInteger;
  quT.Active:=False;
  quT.Active:=True;
  quT.Locate('ID',ID,[]);
  Vi.EndUpdate;    delay(10);
end;

Procedure prFindGr(Id:INteger;Var iGr,iSGr:Integer);
begin
  with dmMC do
  begin
    iGr:=0;
    iSgr:=0;

    quFindC2.Active:=False;
    quFindC2.ParamByName('IDC').AsInteger:=Id;
    quFindC2.Active:=True;
    if CountRec1(quFindC2) then
    begin
      iGr:=quFindC2GoodsGroupID.AsInteger;
      iSgr:=quFindC2SubGroupID.AsInteger;
    end;
    quFindC2.Active:=False;
  end;
end;

function fTara(iC:INteger;Var S:String):Boolean;
begin
//  Result:=False;
  with dmMC do
  begin
    quFindT.Active:=False;
    quFindT.SQL.Clear;
    quFindT.SQL.Add('select * from "RPack"');
    quFindT.SQL.Add('where "RPack"."Code"='+inttostr(iC));
    quFindT.Active:=True;

    if quFindT.RecordCount>0 then
    begin
      S:=quFindTName.AsString;
      Result:=True;
    end
    else
    begin
      S:='';
      Result:=True;
    end;

    quFindT.Active:=False;
  end;
end;

function prMax(sTab:String):INteger;
begin
  Result:=1;
  with dmMC do
  begin
    quM.Close;
    quM.SQL.Clear;
    if sTab='CG' then quM.SQL.Add('select max(CARD) as iMax from "CardGoods"');
    if sTab='Group' then quM.SQL.Add('select max(ID) as iMax from "SubGroup" where ID<1000');
    if sTab='SGroup' then quM.SQL.Add('select max(ID) as iMax from "SubGroup"');
    if sTab='DocsIn' then quM.SQL.Add('select max(ID) as iMax from "TTNIn"');
    if sTab='DocsInv' then quM.SQL.Add('select max(Inventry) as iMax from "InventryHead"');
    if sTab='Cli' then quM.SQL.Add('select top 1 Vendor as iMax from "RVendor" order by Vendor DESC');
    if sTab='ChP' then quM.SQL.Add('select top 1 Code as iMax from "RIndividual" order by Code DESC');
    if sTab='DocsOut' then quM.SQL.Add('select max(ID) as iMax from "TTNOut"');
    if sTab='DocsAc' then quM.SQL.Add('select max(ID) as iMax from "TTNOvr"');
    if sTab='DocsVn' then quM.SQL.Add('select max(ID) as iMax from "TTNSelf"');
    if sTab='Hist' then quM.SQL.Add('select max(ID) as iMax from "A_DOCHIST"');
    if sTab='Close' then quM.SQL.Add('select max(ID) as iMax from "A_CLOSEHIST"');
    if sTab='CT' then quM.SQL.Add('select max(CARD) as iMax from "CardPak"');
    if sTab='Maker' then quM.SQL.Add('select max(ID) as iMax from "A_MAKER"');
    if sTab='AlcoH' then quM.SQL.Add('select max(Id) as iMax from "A_ALCGDH"');
    quM.Open; quM.First;
    if quM.RecordCount>0 then Result:=quMiMax.AsInteger;
    quM.Close;
  end;
end;

function prFindEU(Id:INteger;Var S:String):Integer;
begin
  with dmMC do
  begin
    quFindEU.Active:=False;
    quFindEU.ParamByName('IDC').AsInteger:=Id;
    quFindEU.Active:=True;
    if CountRec1(quFindEU) then
    begin
      Result:=quFindEUCode.AsInteger;
      S:=quFindEUName.AsString;
    end else
    begin
      Result:=32000;
      S:='���';
    end;
    quFindEU.Active:=False;
  end;
end;

function prFindBarC(sBar:String;iC:Integer;Var IDC:INteger):Boolean;
begin
  with dmMC do
  begin
    IDC:=0;
    Result:=False;
    if sBar='' then exit;
    quFindBar.Close;
    quFindBar.ParamByName('SBAR').AsString:=sBar;
    quFindBar.ParamByName('IC').AsInteger:=iC;
    quFindBar.Open;
    if CountRec1(quFindBar) then
    begin
      IDC:=quFindBarGoodsID.AsInteger;
      quCId.Active:=False;
      quCId.ParamByName('IDC').AsInteger:=IDC;
      quCId.Active:=True;
      if quCId.RecordCount>0 then result:=True;
    end;
    quFindBar.Close;
  end;
end;

function prFindBarCMaker(sBar:String;iC:Integer;Var IDC,IDMAKER:INteger):Boolean;
begin
  with dmMC do
  begin
    IDC:=0;
    IDMAKER:=0;
    
    Result:=False;
    if sBar='' then exit;
    quFindBar.Close;
    quFindBar.ParamByName('SBAR').AsString:=sBar;
    quFindBar.ParamByName('IC').AsInteger:=iC;
    quFindBar.Open;
    if CountRec1(quFindBar) then
    begin
      IDC:=quFindBarGoodsID.AsInteger;
      IDMAKER:=quFindBarStatus.AsInteger;
      quCId.Active:=False;
      quCId.ParamByName('IDC').AsInteger:=IDC;
      quCId.Active:=True;
      if quCId.RecordCount>0 then result:=True;
    end;
    quFindBar.Close;
  end;
end;



function prFindBar(sBar:String;iC:Integer;Var IDC:INteger):Boolean;
begin
  with dmMC do
  begin
    IDC:=0;
    Result:=False;
    quFindBar.Close;
    if sBar='' then exit;
    quFindBar.ParamByName('SBAR').AsString:=sBar;
    quFindBar.ParamByName('IC').AsInteger:=iC;
    quFindBar.Open;
    if CountRec1(quFindBar) then
    begin
      result:=True;
      IDC:=quFindBarGoodsID.AsInteger;
    end;
    quFindBar.Close;

    if IDC=0 then
    begin
      quFindCBar.Active:=False;
      quFindCBar.ParamByName('SBAR').AsString:=sBar;
      quFindCBar.Active:=True;
      if quFindCBar.RecordCount>0 then
      begin
        quFindCBar.First;
        result:=True;
        IDC:=quFindCBarID.AsInteger;
      end;
      quFindCBar.Active:=False;
    end;
  end;
end;

Function CountRec1(quR:TPvQuery):Boolean;
begin
  if quR.RecordCount>0 then Result:=True else Result:=False;
{  if quR.Active then
  begin
    quR.First;
    if not quR.Eof then Result:=True
    else Result:=False;
  end else Result:=False;}
end;

Function prFindNameMGr(iGr:Integer):String;
begin
  result:='';
  with dmMC do
  begin
    quMGrFind.Close;
    quMGrFind.ParamByName('ID').AsInteger:=iGr;
    quMGrFind.Open;
    if quMGrFind.RecordCount>0 then
    begin
      quMGrFind.First;
      result:=quMGrFindName.AsString;
    end;
    quMGrFind.Close;
  end;
end;


Function prFindNameGr(iGr:Integer;Var IdGr:Integer):String;
begin
  result:='';
  IdGr:=0;
  with dmMC do
  begin
    quGrFind.Close;
    quGrFind.ParamByName('ID').AsInteger:=iGr;
    quGrFind.Open;
    if CountRec1(quGrFind) then
    begin
      quGrFind.First;
      result:=quGrFindName.AsString;
      IdGr:=quGrFindGoodsGroupID.AsInteger;  //��������
    end;
    quGrFind.Close;
  end;
end;



Function prFindNewArt(bVes:Boolean; Var sM:String):Integer;
Var i:Integer;
begin
  Result:=0;
  sM:='';
  with dmMC do
  begin
    quId.Active:=False;
    quId.SQL.Clear;
    quId.SQL.Add('SElect Id from Goods');
    if bVes then
    begin
      quId.SQL.Add('Where Id>='+INtToStr(CommonSet.iMinVes));
      quId.SQL.Add('And Id<='+INtToStr(CommonSet.iMaxVes));
    end else
    begin
      quId.SQL.Add('Where Id>='+INtToStr(CommonSet.iMin));
      quId.SQL.Add('And Id<='+INtToStr(CommonSet.iMax));
    end;
    quId.SQL.Add('Order by Id');
    quId.Active:=True;
    quId.First;
    i:=quIdId.AsInteger;
    while not quId.eof do
    begin
      if i<quIdId.AsInteger then
      begin //����
        Result:=i;
        break;
      end;
      quId.Next; inc(i);
    end;
    quId.Active:=False;
    if Result=0 then
    begin
      if bVes then sM:='���������� �������� � ��������� �� '+INtToStr(CommonSet.iMinVes)+' �� '+INtToStr(CommonSet.iMaxVes)+' ���.'
      else sM:='���������� �������� � ��������� �� '+INtToStr(CommonSet.iMin)+' �� '+INtToStr(CommonSet.iMax)+' ���.';
    end;
  end;
end;


procedure prRefreshCards(iGr:Integer;bNoActive:Boolean);
Var StrCl:String;
begin
  with dmMC do
  begin
    StrCl:=fGetSlave(iGr)+INtToStr(iGr);

    quCards.Active:=False;
    quCards.SQL.Clear;

    quCards.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."BarCode", "Goods"."ShRealize", "Goods"."TovarType",');
    quCards.SQL.Add('"Goods"."EdIzm", "Goods"."NDS", "Goods"."OKDP", "Goods"."Weght", "Goods"."SrokReal",');
    quCards.SQL.Add('"Goods"."TareWeight", "Goods"."KritOst", "Goods"."BHTStatus", "Goods"."UKMAction", "Goods"."DataChange",');
    quCards.SQL.Add('"Goods"."NSP", "Goods"."WasteRate", "Goods"."Cena", "Goods"."Kol", "Goods"."FullName",');
    quCards.SQL.Add('"Goods"."Country", "Goods"."Status", "Goods"."Prsision", "Goods"."PriceState", "Goods"."SetOfGoods",');
    quCards.SQL.Add('"Goods"."PrePacking", "Goods"."MargGroup", "Goods"."ObjectTypeID", "Goods"."FixPrice",');
    quCards.SQL.Add('"Goods"."PrintLabel", "Goods"."ImageID", "Country"."Name" as NameCountry, "A_BRANDS"."NAMEBRAND",');
    quCards.SQL.Add('"Goods"."GoodsGroupId","Goods"."SubGroupId",');
    quCards.SQL.Add('"Goods"."V01","Goods"."V02","Goods"."V03","Goods"."V04","Goods"."V05","Goods"."V06","Goods"."V07","Goods"."V08","Goods"."V09","Goods"."V10",');
    quCards.SQL.Add('"Goods"."V11","Goods"."V12","Goods"."V14",');
    quCards.SQL.Add('"Goods"."Reserv1","Goods"."Reserv2","Goods"."Reserv3","Goods"."Reserv4","Goods"."Reserv5",');
    quCards.SQL.Add('"A_MAKER"."NAMEM"');
    quCards.SQL.Add(',"A_TYPEVOZ"."TYPEVOZ"');
    quCards.SQL.Add('FROM "Goods"');
    quCards.SQL.Add('left join "A_BRANDS" on "A_BRANDS"."ID" = "Goods"."V01" ');
    quCards.SQL.Add('left join "Country" on "Country"."ID" = "Goods"."Country" ');
    quCards.SQL.Add('left join "A_MAKER" on "A_MAKER"."ID" = "Goods"."V12" ');
    quCards.SQL.Add('left join "A_TYPEVOZ" on "A_TYPEVOZ"."ID" = "Goods"."V14" ');

    if CommonSet.ClassType=0 then  quCards.SQL.Add('where "Goods"."SubGroupID" in ('+StrCl+')')
    else
    begin
      if CommonSet.ClassOld=0 then quCards.SQL.Add('where "Goods"."GoodsGroupId" in ('+StrCl+')');
      if CommonSet.ClassOld=1 then quCards.SQL.Add('where "Goods"."SubGroupID" in ('+StrCl+')');
    end;

    if bNoActive then quCards.SQL.Add('and "Goods"."Status">100')
    else quCards.SQL.Add('and "Goods"."Status"<=100');

    quCards.Active:=True;
  end;
end;


Function CountRec(Name_T:String):Integer;
begin
  with dmMC do
  begin
    try
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from '+Name_T);
      quC.Active:=True;
      Result:=quCRecCount.AsInteger;
    except
      Result:=0;
    end;
  end;
end;


Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmMC do
  begin
    quCanDo.Active:=False;
    quCanDo.Params.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.Params.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoPREXEC.AsInteger=1 then Result:=False;
  end;
end;


Procedure TdmMC.CardsExpandLevel( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode,TreeSGNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  if ID=0 then
  begin
    quGr.Close;
    quGr.Open;

    Tree.Items.BeginUpdate;
    // ��� ������ ������ �� ����������� ������ ������
    // ��������� ����� � TreeView, ��� �������� ����� � ���,
    // ������� �� ������ ��� "��������"
    quGr.First;
    while not quGr.Eof do
    begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
      TreeNode:=Tree.Items.AddChildObject(Node, quGr.FieldByName('Name').AsString, Pointer(quGr.FieldByName('Id').AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;
      // ������� ��������� (������) �������� ����� ������ ��� ����,
      // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������


      //����� ��� ��������� ���� �� ������ ���� �� �����


      quSGr.Close;
      quSGr.Params.ParamByName('ParentID').Value:=quGr.FieldByName('Id').AsInteger;
      quSGr.Open;

      Tree.Items.BeginUpdate;
      quSGr.First;
      while not quSGr.Eof do
      begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
        TreeSGNode:=Tree.Items.AddChildObject(TreeNode,quSGr.FieldByName('Name').AsString, Pointer(quSGr.FieldByName('Id').AsInteger));
        TreeSGNode.ImageIndex:=12;
        TreeSGNode.SelectedIndex:=11;
        quSGr.Next;
      end;
      Tree.Items.EndUpdate;
      quSGr.Close;

      quGr.Next;
    end;
    Tree.Items.EndUpdate;
    quGr.Close;
  end;

end;


//  Text:=prOA(Sender.AsString);

procedure TdmMC.quTTnInCalcFields(DataSet: TDataSet);
begin
  quTTnInNameCli.AsString:='';
  if quTTnInIndexPost.AsInteger=1 then
  begin
    quTTnInNameCli.AsString:=quTTnInNamePost.AsString;
    quTTnInINN.AsString:=quTTnInINNPost.AsString;
  end;
  if quTTnInIndexPost.AsInteger=2 then
  begin
    quTTnInNameCli.AsString:=quTTnInNameInd.AsString;
    quTTnInINN.AsString:=quTTnInINNInd.AsString;
  end;
end;

procedure TdmMC.quTTnOutCalcFields(DataSet: TDataSet);
begin
  quTTnOutNameCli.AsString:='';
  if quTTnOutIndexPoluch.AsInteger=1 then
  begin
    quTTnOutNameCli.AsString:=quTTnOutNamePost.AsString;
    quTTnOutINN.AsString:=quTTnOutINNPost.AsString;
  end;
  if quTTnOutIndexPoluch.AsInteger=2 then
  begin
    quTTnOutNameCli.AsString:=quTTnOutNameInd.AsString;
    quTTnOutINN.AsString:=quTTnOutINNInd.AsString;
  end;
end;

procedure TdmMC.quCashListCalcFields(DataSet: TDataSet);
Var Fl,iLoad,iTake:SmallInt;
begin
  Fl:=quCashListFlags.AsInteger;
  iLoad:=Fl and $80;
  iTake:=Fl and $10;
  if iLoad<>0 then  quCashListLoad.AsInteger:=0 else quCashListLoad.AsInteger:=1; //���� �� ���� �� ������� �� ����
  if iTake<>0 then  quCashListTake.AsInteger:=0 else quCashListTake.AsInteger:=1;
end;

procedure TdmMC.quCashCalcFields(DataSet: TDataSet);
Var Fl,iLoad,iTake:SmallInt;
begin
  Fl:=quCashFlags.AsInteger;
  iLoad:=Fl and $80;
  iTake:=Fl and $10;
  if iLoad<>0 then  quCashLoad.AsInteger:=0 else quCashLoad.AsInteger:=1; //���� �� ���� �� ������� �� ����
  if iTake<>0 then  quCashTake.AsInteger:=0 else quCashTake.AsInteger:=1;
end;

procedure TdmMC.quRemnCalcFields(DataSet: TDataSet);
begin
//  quRemnRemn.AsFloat:=prFindRemn1(quRemnCARD.AsInteger);
  if quRemnStatus1.AsInteger=9 then
  with dmMT do quRemnRemn.AsFloat:=prFindTCGRemn(quRemnCARD.AsInteger)
  else quRemnRemn.AsFloat:=0;
end;

procedure TdmMC.quPostCalcFields(DataSet: TDataSet);
begin
  quPostNameCli.AsString:='';
  if quPostIndexPost.AsInteger=1 then quPostNameCli.AsString:=quPostNamePost.AsString;
  if quPostIndexPost.AsInteger=2 then quPostNameCli.AsString:=quPostNameInd.AsString;
  quPostCenaTovar0.AsFloat:=0;
  if abs(quPostKol.AsFloat)>=0.01 then quPostCenaTovar0.AsFloat:=rv(quPostOutNDSSum.AsFloat/quPostKol.AsFloat);
end;

procedure TdmMC.quPost1CalcFields(DataSet: TDataSet);
begin
  quPost1NameCli.AsString:='';
  if quPost1IndexPost.AsInteger=1 then
  begin
    quPost1NameCli.AsString:=quPost1NamePost.AsString;
    quPost1sInn.AsString:=quPost1InnP.AsString;
  end;
  if quPost1IndexPost.AsInteger=2 then
  begin
    quPost1NameCli.AsString:=quPost1NameInd.AsString;
    quPost1sInn.AsString:=quPost1InnI.AsString;
  end;
end;

procedure TdmMC.quTTnInvCalcFields(DataSet: TDataSet);
begin
  quTTnInvSumD.AsFloat:=quTTnInvSumActual.AsFloat-quTTnInvSumRecord.AsFloat;
end;

function prTestScale(sAdr:String):Boolean;
Var sPort:String;
    StrWk:String;
    bByte:Byte;
begin
  with dmMC do
  begin
    Result:=False;
    try
      if DigiClient.Connected then DigiClient.Disconnect;
      sPort:=sAdr;
      while pos('.',sPort)>0 do delete(sPort,1,pos('.',sPort));
      DigiClient.Host:=sAdr;
      DigiClient.Port:=2000+StrToINtDef(sPort,0);
      DigiClient.Connect;
      delay(100);
      if DigiClient.Connected then
      begin
        Strwk:='1000';
//        DigiClient.Write(StrWk);
        DigiClient.Write(StrWk);
        delay(100);
        DigiClient.ReadBuffer(bByte, 1);
        if bByte<>0 then Result:=True;
        DigiClient.Disconnect;
      end;
    except
      Result:=False;
    end;
  end;
end;


Function AddPlu(i,PluCode,sDate:word;ItemCode,Name1,Name2:String;Price:Real;bF:byte):Boolean;
var j:byte;
    StrBC:String;
begin
  Result:=true;
  with dmMC do
  begin
  try
    bF:=17+bF;
    ArrPlu[i].RecID:=$25F1;
    PluNumToBCD(PluCode,ArrPlu[i].No);
    ArrPlu[i].NullByte:=0;
    ArrPlu[i].RecSize:=SizeOf(ArrPlu[i])-2;
    ArrPlu[i].Status:=$94;
    ArrPlu[i].Status2[1]:=$1D; ArrPlu[i].Status2[2]:=$20; ArrPlu[i].Status2[3]:=$9;
    PluUnitPriceToBCD(Price,ArrPlu[i].UnitPrice);
//    ArrPlu[i].LabelFormat:=17;
    ArrPlu[i].LabelFormat:=bF;
    ArrPlu[i].BarCodeFormat:=5;

    StrBC:=ItemCode;
    while length(StrBC)<5 do StrBC:='0'+StrBC;
    PluBarCodeToBCD('22'+StrBC,ArrPlu[i].BarCodeData);

    ArrPlu[i].DateCfg[1]:=$BF; ArrPlu[i].DateCfg[2]:=$BC; ArrPlu[i].DateCfg[3]:=1;
    ArrPlu[i].MgCode:=WordToBCD(1);
    ArrPlu[i].SellByDate:=WordToBCD(sDate);
    ArrPlu[i].NullWord:=0;
    ArrPlu[i].Str1Font:=4;
    ArrPlu[i].Str1Length:=28;
    for j:=1 to 28 do
    begin
      if j<= Length(Name1) then ArrPlu[i].Str1[j]:=Ord(Name1[j])
      else ArrPlu[i].Str1[j]:=0;
      if j<= Length(Name2) then ArrPlu[i].Str2[j]:=Ord(Name2[j])
      else ArrPlu[i].Str2[j]:=0;
    end;
    ArrPlu[i].CrLf:=$D;
    ArrPlu[i].Str2Font:=4;
    ArrPlu[i].Str2Length:=28;
    ArrPlu[i].EndRec:=$C;
    ArrPlu[i].CheckByte:=$0;
  except on E:Exception do Result:=false;
  end;{of try}
  end;
end;


function LoadScale(sAdr:String;iC:Integer):Boolean;
Var m:Integer;
    sPort,StrWk:String;
//    Recieved:Array[1..6]of byte;
    bByte:Byte;
    bErr:Boolean;
begin
  with dmMC do
  begin
//    Memo1.Lines.Add('   '+FormatDateTime('hh:nn:ss ----',now));
    Result:=True;
    bErr:=False;

    sPort:=sAdr;
    while pos('.',sPort)>0 do delete(sPort,1,pos('.',sPort));

    if DigiClient.Connected then DigiClient.Disconnect;

    DigiClient.Host:=sAdr;
    DigiClient.Port:=2000+StrToINtDef(sPort,0);
    try
      DigiClient.Connect;
      delay(100);
      if DigiClient.Connected=False then
      begin
        Result:=False;
        exit;
      end;
      Strwk:='1000';
      DigiClient.Write(StrWk);
      bByte:=0;
      delay(100);
      try
        DigiClient.ReadBuffer(bByte,1);
      except
        bByte:=0;
      end;
//      if bByte<>0 then
      if bByte>=0 then           //�������� - ��� � ����
      begin
        for m:=1 to iC do
        begin
          if not bErr then
          begin
            DigiClient.WriteBuffer(ArrPlu[m],SizeOf(ArrPlu[m]));
//          delay(10);
            try
              DigiClient.ReadBuffer(bByte,1);
            except
              bByte:=0;
              bErr:=True;
            end;
//            StrWk:=intToStr(bByte);
          end;
//          Memo1.Lines.Add('   '+FormatDateTime('hh:nn:ss ----',now)+intToStr(m)+' +���� '+StrWk);
        end;
      end else bErr:=True;

      DigiClient.Disconnect;
    except
      bErr:=True;
      DigiClient.Disconnect;
    end;
  end;
  if bErr then Result:=False;
end;


procedure TdmMC.quScaleItemsCalcFields(DataSet: TDataSet);
begin
  quScaleItemsrPr.AsFloat:=quScaleItemsPrice.AsInteger/100;
end;

procedure TdmMC.quMoveCalcFields(DataSet: TDataSet);
begin
  if quMoveDOC_TYPE.AsInteger in [1,3,20] then
  begin
    quMoveQIN.AsFloat:=quMoveQUANT_MOVE.AsFloat;
    quMoveQOUT.AsFloat:=0;
  end;
  if quMoveDOC_TYPE.AsInteger in [2,4,7] then
  begin
    quMoveQIN.AsFloat:=0;
    quMoveQOUT.AsFloat:=quMoveQUANT_MOVE.AsFloat;
  end;
end;

procedure TdmMC.quPost2CalcFields(DataSet: TDataSet);
begin
  quPost2NameCli.AsString:='';
  if quPost2IndexPost.AsInteger=1 then quPost2NameCli.AsString:=quPost2NamePost.AsString;
  if quPost2IndexPost.AsInteger=2 then quPost2NameCli.AsString:=quPost2NameInd.AsString;
end;

procedure TdmMC.quFindCCalcFields(DataSet: TDataSet);
begin
  quFindCGroupName.AsString:=prFindTNameGr(quFindCGoodsGroupID.AsInteger);
end;

procedure TdmMC.quRepTovarPeriodCalcFields(DataSet: TDataSet);
begin
  quRepTovarPeriodMonth.AsString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime)+'-'+FormatDateTime('mm',quRepTovarPeriodRDate.AsDateTime);
  quRepTovarPeriodyear.AsString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime);

  if strtoint(FormatDateTime('mm',quRepTovarPeriodRDate.AsDateTime)) in [1..3] then  quRepTovarPeriodkvartal.asString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime)+' 1 ��. ';
  if strtoint(FormatDateTime('mm',quRepTovarPeriodRDate.AsDateTime)) in [4..6] then  quRepTovarPeriodkvartal.asString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime)+' 2 ��. ';
  if strtoint(FormatDateTime('mm',quRepTovarPeriodRDate.AsDateTime)) in [7..9] then  quRepTovarPeriodkvartal.asString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime)+' 3 ��. ';
  if strtoint(FormatDateTime('mm',quRepTovarPeriodRDate.AsDateTime)) in [10..12] then  quRepTovarPeriodkvartal.asString:=FormatDateTime('yyyy',quRepTovarPeriodRDate.AsDateTime)+' 4 ��. ';
end;

procedure TdmMC.quPost4CalcFields(DataSet: TDataSet);
begin
  quPost4NameCli.AsString:='';
  if quPost4IndexPost.AsInteger=1 then quPost4NameCli.AsString:=quPost4NamePost.AsString;
  if quPost4IndexPost.AsInteger=2 then quPost4NameCli.AsString:=quPost4NameInd.AsString;
end;

procedure TdmMC.quRepHourCalcFields(DataSet: TDataSet);
begin
  quRepHourA00.AsFloat:=0; if quRepHourCl00.AsInteger>0 then quRepHourA00.AsFloat:=quRepHourS00.AsFloat/quRepHourCl00.AsInteger;
  quRepHourA01.AsFloat:=0; if quRepHourCl01.AsInteger>0 then quRepHourA01.AsFloat:=quRepHourS01.AsFloat/quRepHourCl01.AsInteger;
  quRepHourA02.AsFloat:=0; if quRepHourCl02.AsInteger>0 then quRepHourA02.AsFloat:=quRepHourS02.AsFloat/quRepHourCl02.AsInteger;
  quRepHourA03.AsFloat:=0; if quRepHourCl03.AsInteger>0 then quRepHourA03.AsFloat:=quRepHourS03.AsFloat/quRepHourCl03.AsInteger;
  quRepHourA04.AsFloat:=0; if quRepHourCl04.AsInteger>0 then quRepHourA04.AsFloat:=quRepHourS04.AsFloat/quRepHourCl04.AsInteger;
  quRepHourA05.AsFloat:=0; if quRepHourCl05.AsInteger>0 then quRepHourA05.AsFloat:=quRepHourS05.AsFloat/quRepHourCl05.AsInteger;
  quRepHourA06.AsFloat:=0; if quRepHourCl06.AsInteger>0 then quRepHourA06.AsFloat:=quRepHourS06.AsFloat/quRepHourCl06.AsInteger;
  quRepHourA07.AsFloat:=0; if quRepHourCl07.AsInteger>0 then quRepHourA07.AsFloat:=quRepHourS07.AsFloat/quRepHourCl07.AsInteger;
  quRepHourA08.AsFloat:=0; if quRepHourCl08.AsInteger>0 then quRepHourA08.AsFloat:=quRepHourS08.AsFloat/quRepHourCl08.AsInteger;
  quRepHourA09.AsFloat:=0; if quRepHourCl09.AsInteger>0 then quRepHourA09.AsFloat:=quRepHourS09.AsFloat/quRepHourCl09.AsInteger;
  quRepHourA10.AsFloat:=0; if quRepHourCl10.AsInteger>0 then quRepHourA10.AsFloat:=quRepHourS10.AsFloat/quRepHourCl10.AsInteger;
  quRepHourA11.AsFloat:=0; if quRepHourCl11.AsInteger>0 then quRepHourA11.AsFloat:=quRepHourS11.AsFloat/quRepHourCl11.AsInteger;
  quRepHourA12.AsFloat:=0; if quRepHourCl12.AsInteger>0 then quRepHourA12.AsFloat:=quRepHourS12.AsFloat/quRepHourCl12.AsInteger;
  quRepHourA13.AsFloat:=0; if quRepHourCl13.AsInteger>0 then quRepHourA13.AsFloat:=quRepHourS13.AsFloat/quRepHourCl13.AsInteger;
  quRepHourA14.AsFloat:=0; if quRepHourCl14.AsInteger>0 then quRepHourA14.AsFloat:=quRepHourS14.AsFloat/quRepHourCl14.AsInteger;
  quRepHourA15.AsFloat:=0; if quRepHourCl15.AsInteger>0 then quRepHourA15.AsFloat:=quRepHourS15.AsFloat/quRepHourCl15.AsInteger;
  quRepHourA16.AsFloat:=0; if quRepHourCl16.AsInteger>0 then quRepHourA16.AsFloat:=quRepHourS16.AsFloat/quRepHourCl16.AsInteger;
  quRepHourA17.AsFloat:=0; if quRepHourCl17.AsInteger>0 then quRepHourA17.AsFloat:=quRepHourS17.AsFloat/quRepHourCl17.AsInteger;
  quRepHourA18.AsFloat:=0; if quRepHourCl18.AsInteger>0 then quRepHourA18.AsFloat:=quRepHourS18.AsFloat/quRepHourCl18.AsInteger;
  quRepHourA19.AsFloat:=0; if quRepHourCl19.AsInteger>0 then quRepHourA19.AsFloat:=quRepHourS19.AsFloat/quRepHourCl19.AsInteger;
  quRepHourA20.AsFloat:=0; if quRepHourCl20.AsInteger>0 then quRepHourA20.AsFloat:=quRepHourS20.AsFloat/quRepHourCl20.AsInteger;
  quRepHourA21.AsFloat:=0; if quRepHourCl21.AsInteger>0 then quRepHourA21.AsFloat:=quRepHourS21.AsFloat/quRepHourCl21.AsInteger;
  quRepHourA22.AsFloat:=0; if quRepHourCl22.AsInteger>0 then quRepHourA22.AsFloat:=quRepHourS22.AsFloat/quRepHourCl22.AsInteger;
  quRepHourA23.AsFloat:=0; if quRepHourCl23.AsInteger>0 then quRepHourA23.AsFloat:=quRepHourS23.AsFloat/quRepHourCl23.AsInteger;
  quRepHourAITOG.AsFloat:=0; if quRepHourQC.AsInteger>0 then quRepHourAITOG.AsFloat:=quRepHourSS.AsFloat/quRepHourQC.AsInteger;
end;

procedure TdmMC.quCashRepCalcFields(DataSet: TDataSet);
begin
  if quCashRepCurMoney.AsFloat>0 then
    quCashRepCheckSum.AsFloat:=RoundVal(quCashRepSaleRet.AsFloat/quCashRepCurMoney.AsFloat)
  else quCashRepCheckSum.AsFloat:=0;
  quCashRepRDiscAll.AsFloat:=rv(quCashRepZDiscount.AsFloat)+rv(quCashRepZCrDiscount.AsFloat);
end;

procedure TdmMC.quAkciyaCalcFields(DataSet: TDataSet);
begin
  quAkciyaDateTime.AsString:=FormatDateTime('dd.mm.yyyy',quAkciyaDocDate.AsDateTime)+' ';
  if Length(quAkciyaDocTime.AsString)=4 then
    quAkciyaDateTime.AsString:=quAkciyaDateTime.AsString+Copy(quAkciyaDocTime.AsString,0,2)+':'+Copy(quAkciyaDocTime.AsString,Length(quAkciyaDocTime.AsString)-1,2)
    else
    quAkciyaDateTime.AsString:=quAkciyaDateTime.AsString+Copy(quAkciyaDocTime.AsString,0,1)+':'+Copy(quAkciyaDocTime.AsString,Length(quAkciyaDocTime.AsString)-1,2);

  //������
  Case quAkciyaAType.AsInteger of
  1:begin //���
      quAkciyaVal1.AsFloat:=rv(quAkciyaPrice.AsInteger);
      quAkciyaVal2.AsString:=quAkciyaPrice.AsString;
      quAkciyaVal2After.AsString:=quAkciyaOldPrice.AsString;
    end;
  2:begin  //����
      quAkciyaVal1.AsFloat:=roundEx(quAkciyaDStop.AsInteger);

      if quAkciyaDStop.AsInteger=0 then quAkciyaVal2.AsString:='��. �� ������.';
      if quAkciyaDStop.AsInteger=1 then quAkciyaVal2.AsString:='��. ������.';

      if quAkciyaOldDStop.AsInteger=0 then quAkciyaVal2After.AsString:='��. �� ������.';
      if quAkciyaOldDStop.AsInteger=1 then quAkciyaVal2After.AsString:='��. ������.';

    end;
  3:begin //������
      quAkciyaVal1.AsFloat:=roundEx(quAkciyaDStop.AsInteger);
      if quAkciyaDStop.AsInteger<>-3 then quAkciyaVal2.AsString:=quAkciyaSID.AsString
                                     else quAkciyaVal2.AsString:='����� ���';
      if quAkciyaDStop.AsInteger = 3 then quAkciyaVal2.AsString:='���������� ���';
      quAkciyaVal2After.AsString:='';
    end;
  4:begin //�����
      quAkciyaVal1.AsFloat:=rv(quAkciyaDStop.AsInteger/100);
      quAkciyaVal2.AsString:=FloatToStr(quAkciyaDStop.AsInteger/100);
      quAkciyaVal2After.AsString:='';
    end;
  end;
end;

procedure TdmMC.quObmHCalcFields(DataSet: TDataSet);
begin
  quObmHNameCli.AsString:='';
  if quObmHCLITYPE.AsInteger=1 then
  begin
    quObmHNameCli.AsString:=quObmHNamePost.AsString;
    quObmHINN.AsString:=quObmHINNPost.AsString;
  end;
  if quObmHCLITYPE.AsInteger=2 then
  begin
    quObmHNameCli.AsString:=quObmHNameInd.AsString;
    quObmHINN.AsString:=quObmHINNInd.AsString;
  end;
end;

procedure TdmMC.quPost5CalcFields(DataSet: TDataSet);
begin
  quPost5NameCli.AsString:='';
  if quPost5IndexPost.AsInteger=1 then quPost5NameCli.AsString:=quPost5NamePost.AsString;
  if quPost5IndexPost.AsInteger=2 then quPost5NameCli.AsString:=quPost5NameInd.AsString;
  quPost5CenaTovar0.AsFloat:=0;
  if abs(quPost5Kol.AsFloat)>=0.01 then quPost5CenaTovar0.AsFloat:=rv(quPost5OutNDSSum.AsFloat/quPost5Kol.AsFloat);
end;

procedure TdmMC.quZakHCalcFields(DataSet: TDataSet);
begin
  quZakHNameCli.AsString:='';
  if quZakHCLITYPE.AsInteger=1 then
  begin
    quZakHNameCli.AsString:=quZakHNamePost.AsString;
    quZakHINN.AsString:=quZakHINNPost.AsString;
  end;
  if quZakHCLITYPE.AsInteger=2 then
  begin
    quZakHNameCli.AsString:=quZakHNameInd.AsString;
    quZakHINN.AsString:=quZakHINNInd.AsString;
  end;
  try
    quZakHSDATEEDIT.AsString:=DateToStr(quZakHDATEEDIT.AsDateTime);
  except
  end;
  quZakHsDatePost.AsString:=ds1(quZakHIDATENACL.AsInteger);
end;

procedure TdmMC.quPost6CalcFields(DataSet: TDataSet);
begin
  quPost6NameCli.AsString:='';
  if quPost6ClientTypeId.AsInteger=1 then quPost6NameCli.AsString:=quPost6NamePost.AsString;
  if quPost6ClientTypeId.AsInteger=2 then quPost6NameCli.AsString:=quPost6NameInd.AsString;
end;

procedure TdmMC.quPost7CalcFields(DataSet: TDataSet);
begin
  quPost7NameCli.AsString:='';
  if quPost7IndexPoluch.AsInteger=1 then quPost7NameCli.AsString:=quPost7NamePost.AsString;
  if quPost7IndexPoluch.AsInteger=2 then quPost7NameCli.AsString:=quPost7NameInd.AsString;
end;

procedure TdmMC.quGrafPostCalcFields(DataSet: TDataSet);
begin
  quGrafPostDDATEP.AsDateTime:=quGrafPostIDATEP.AsInteger;
  quGrafPostDDATEZ.AsDateTime:=quGrafPostIDATEZ.AsInteger;
end;

procedure TdmMC.quCardsCalcFields(DataSet: TDataSet);
begin
  quCardsVol.AsFloat:=quCardsV10.AsInteger/1000;
  with dmP do
  begin
    if (flagcheck=true)  then
    begin
      quAllRem.Active:=false;
      quAllRem.ParamByName('CODE').AsInteger:= quCardsID.AsInteger;
      quAllRem.Active:=true;
      quCardsQremn.AsFloat:=quAllRemQRem.AsFloat;
      quAllRem.Active:=false;
    end;
  end;

end;

function prGetOrg(iOrg,iSh:INteger):TOrg;
Var rOrg:TOrg;
begin
  with dmMC do
  begin
    rOrg.Name:='';
    rOrg.FullName:='';
    rOrg.iShop:=iSh;
    rOrg.sAdr:='';
    rOrg.iDep:=0;
    rOrg.IdOrg:=iOrg;
    rOrg.Inn:='';
    rOrg.KPP:='';
    rOrg.sMail:='';
    rOrg.sTel:='';
    rOrg.CodeCountry:='';
    rOrg.CodeReg:='';
    rOrg.Sity:='';
    rOrg.Street:='';
    rOrg.House:='';
    rOrg.Korp:='';
    rOrg.Dir1:='';
    rOrg.Dir2:='';
    rOrg.Dir3:='';
    rOrg.gb1:='';
    rOrg.gb2:='';
    rOrg.gb3:='';
    rOrg.LicVid:='';
    rOrg.LicSer:='';
    rOrg.LicNum:='';
    rOrg.dDateB:=date;
    rOrg.dDateE:=date;
    rOrg.ip:=0;

    quDepPars.Active:=False;
    quDepPars.ParamByName('ID').AsInteger:=iOrg;
    quDepPars.Active:=True;
    if quDepPars.RecordCount>0 then
    begin
      quDepPars.First;
      rOrg.Name:=quDepParsNAMEDEP.AsString;
      rOrg.FullName:=quDepParsNAMEDEP.AsString;
      rOrg.iShop:=iSh;
      rOrg.sAdr:=quDepParsADRDEP.AsString;
      rOrg.iDep:=quDepParsID.AsInteger;
      rOrg.IdOrg:=iOrg;
      rOrg.Inn:=quDepParsINN.AsString;
      rOrg.KPP:=quDepParsKPP.AsString;
      rOrg.sMail:=quDepParssEmail.AsString;
      rOrg.sTel:=quDepParsTel.AsString;
      rOrg.CodeCountry:='643';
      rOrg.CodeReg:='66';
      rOrg.Sity:='������������';
      rOrg.Street:=quDepParsStreet.AsString;
      rOrg.House:=quDepParsDom.AsString;
      rOrg.Korp:=quDepParsKorp.AsString;
      rOrg.Dir1:=quDepParsDir1.AsString;
      rOrg.Dir2:=quDepParsDir2.AsString;
      rOrg.Dir3:=quDepParsDir3.AsString;
      rOrg.gb1:=quDepParsGb1.AsString;
      rOrg.gb2:=quDepParsGb2.AsString;
      rOrg.gb3:=quDepParsGb3.AsString;
      rOrg.LicVid:=quDepParsLicVid.AsString;
      rOrg.LicSer:=quDepParsLicSer.AsString;
      rOrg.LicNum:=quDepParsLicNum.AsString;
      rOrg.dDateB:=quDepParsLicDB.AsDateTime;
      rOrg.dDateE:=quDepParsLicDE.AsDateTime;

      if pos('�� ',rOrg.FullName)=1 then rOrg.ip:=1;
    end;

    Result:=rOrg;
  end;
end;

procedure TdmMC.quRepRealAPCalcFields(DataSet: TDataSet);
begin
  quRepRealAPDATETIMEOP.AsDateTime:=Trunc(quRepRealAPDATEOPER.AsDateTime)+frac(quRepRealAPTIMEOP.AsDateTime);
end;

end.
