unit MainClosePer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, pvtables, pvsqltables, sqldataset, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfmClosePer = class(TForm)
    PvSQL: TPvSqlDatabase;
    quPassw: TPvQuery;
    quPasswID: TIntegerField;
    quPasswID_PARENT: TIntegerField;
    quPasswNAME: TStringField;
    quPasswUVOLNEN: TSmallintField;
    quPasswPASSW: TStringField;
    quPasswMODUL1: TSmallintField;
    quPasswMODUL2: TSmallintField;
    quPasswMODUL3: TSmallintField;
    quPasswMODUL4: TSmallintField;
    quPasswMODUL5: TSmallintField;
    quPasswMODUL6: TSmallintField;
    quPasswBARCODE: TStringField;
    dsquPassw: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViClose: TcxGridDBTableView;
    LevelClose: TcxGridLevel;
    GrClose: TcxGrid;
    quCloseH: TPvQuery;
    dsquCloseH: TDataSource;
    quCloseHID: TIntegerField;
    quCloseHDateClose: TDateField;
    quCloseHIdPers: TIntegerField;
    quCloseHDateEdit: TDateTimeField;
    quCloseHNAME: TStringField;
    ViCloseID: TcxGridDBColumn;
    ViCloseDateClose: TcxGridDBColumn;
    ViCloseIdPers: TcxGridDBColumn;
    ViCloseDateEdit: TcxGridDBColumn;
    ViCloseNAME: TcxGridDBColumn;
    quA: TPvQuery;
    quM: TPvQuery;
    quMiMax: TIntegerField;
    Timer1: TTimer;
    Timer2: TTimer;
    quCloseHBuh: TPvQuery;
    quCloseHBuhID: TIntegerField;
    quCloseHBuhDateClose: TDateField;
    quCloseHBuhIdPers: TIntegerField;
    quCloseHBuhDateEdit: TDateTimeField;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function prMax(sTab:String):INteger;
  end;


var
  fmClosePer: TfmClosePer;
  sStart:String = '';

implementation

uses Un1, PasswMainCard;

{$R *.dfm}

procedure TfmClosePer.FormShow(Sender: TObject);
Var StrErr:String;
begin
  Person.Modul:='Crd';
  StrErr:='';
  try
    PvSQL.Connected:=False;
    PvSQL.Connected:=True;
    if PvSQL.Connected=False then StrErr:='�� ������� ������������ � ���� ������.';
  except
    StrErr:='������ ��� ����������� � ���� ������.';
  end;
  if (StrErr='') and (sStart='') then
  begin
    fmPerA:=tfmPerA.Create(Application);
    fmPerA.ShowModal;
    if fmPerA.ModalResult=mrOk then Caption:=Caption+'   : '+Person.Name
      else StrErr:='������ ������������� ������������';
    fmPerA.Release;
  end;
  if StrErr>'' then
  begin
    ShowMessage(StrErr);
    Close;
  end else writeini;

  ViClose.BeginUpdate;
  quCloseH.Active:=False;
  quCloseH.Active:=True;
  quCloseH.First;
  ViClose.EndUpdate;
end;

procedure TfmClosePer.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  sStart := ParamStr(1);
//  showmessage(sStart);
  ReadIni;
  cxDateEdit1.Date:=Date-CommonSet.CloseDay;
  grClose.Align:=AlClient;
  if pos('A',sStart)>0 then
  begin
    cxButton1.Enabled:=False;
    Timer1.Enabled:=True;
  end;
end;

procedure TfmClosePer.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmClosePer.cxButton1Click(Sender: TObject);
Var DateClose,DateCloseBuh:TDateTime;
    bMay:Boolean;
begin
  DateClose:=cxDateEdit1.Date;
  bMay:=True;

  quCloseHBuh.Active:=False;
  quCloseHBuh.Active:=True;
  if quCloseHBuh.RecordCount>0 then
  begin
    DateCloseBuh:=quCloseHBuhDateClose.AsDateTime;
    if DateCloseBuh>DateClose then
    begin
      bMay:=False;
      ShowMessage('�������� ��������� !!! ���������� ���� �������� - '+ds(DateCloseBuh));
    end;
  end;
  quCloseHBuh.Active:=False;

  if bMay then
  begin
    quA.Active:=False;
    quA.SQL.Clear;
    quA.SQL.Add('INSERT into "A_CLOSEHIST" values (');
    quA.SQL.Add(its(prMax('Close')+1)+',');
    quA.SQL.Add(''''+ds(cxDateEdit1.Date)+''',');
    quA.SQL.Add(its(Person.Id)+',');
    quA.SQL.Add(''''+dts(now)+'''');
    quA.SQL.Add(')');
    quA.ExecSQL;

    ViClose.BeginUpdate;
    quCloseH.Active:=False;
    quCloseH.Active:=True;
    quCloseH.First;
    ViClose.EndUpdate;
  end;
end;


function TfmClosePer.prMax(sTab:String):INteger;
begin
  Result:=1;

  quM.Close;
    quM.SQL.Clear;
    if sTab='CG' then quM.SQL.Add('select max(CARD) as iMax from "CardGoods"');
    if sTab='Group' then quM.SQL.Add('select max(ID) as iMax from "GdsGroup"');
    if sTab='SGroup' then quM.SQL.Add('select max(ID) as iMax from "SubGroup"');
    if sTab='DocsIn' then quM.SQL.Add('select max(ID) as iMax from "TTNIn"');
    if sTab='DocsInv' then quM.SQL.Add('select max(Inventry) as iMax from "InventryHead"');
    if sTab='Cli' then quM.SQL.Add('select top 1 Vendor as iMax from "RVendor" order by Vendor DESC');
    if sTab='ChP' then quM.SQL.Add('select top 1 Code as iMax from "RIndividual" order by Code DESC');
    if sTab='DocsOut' then quM.SQL.Add('select max(ID) as iMax from "TTNOut"');
    if sTab='DocsAc' then quM.SQL.Add('select max(ID) as iMax from "TTNOvr"');
    if sTab='DocsVn' then quM.SQL.Add('select max(ID) as iMax from "TTNSelf"');
    if sTab='Hist' then quM.SQL.Add('select max(ID) as iMax from "A_DOCHIST"');
    if sTab='Close' then quM.SQL.Add('select max(ID) as iMax from "A_CLOSEHIST"');
    quM.Open; quM.First;
    if quM.RecordCount>0 then Result:=quMiMax.AsInteger;
  quM.Close;

end;


procedure TfmClosePer.Timer1Timer(Sender: TObject);
begin
  quA.Active:=False;
  quA.SQL.Clear;
  quA.SQL.Add('INSERT into "A_CLOSEHIST" values (');
  quA.SQL.Add(its(prMax('Close')+1)+',');
  quA.SQL.Add(''''+ds(Date-CommonSet.CloseDay)+''',');
  quA.SQL.Add(its(0)+',');
  quA.SQL.Add(''''+dts(now)+'''');
  quA.SQL.Add(')');
  quA.ExecSQL;

  ViClose.BeginUpdate;
  quCloseH.Active:=False;
  quCloseH.Active:=True;
  quCloseH.First;
  ViClose.EndUpdate;
  delay(1000);
  Close;
end;

procedure TfmClosePer.Timer2Timer(Sender: TObject);
begin
  ViClose.BeginUpdate;
  quCloseH.Active:=False;
  quCloseH.Active:=True;
  quCloseH.First;
  ViClose.EndUpdate;
end;

end.
