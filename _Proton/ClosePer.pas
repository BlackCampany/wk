unit ClosePer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase, ComCtrls,
  Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, cxButtons,
  ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmClosePer = class(TForm)
    RnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    taPersonal: TpFIBDataSet;
    taPersonalID: TFIBIntegerField;
    taPersonalID_PARENT: TFIBIntegerField;
    taPersonalNAME: TFIBStringField;
    taPersonalUVOLNEN: TFIBBooleanField;
    taPersonalPASSW: TFIBStringField;
    taPersonalMODUL1: TFIBBooleanField;
    taPersonalMODUL2: TFIBBooleanField;
    taPersonalMODUL3: TFIBBooleanField;
    taPersonalMODUL4: TFIBBooleanField;
    taPersonalMODUL5: TFIBBooleanField;
    taPersonalMODUL6: TFIBBooleanField;
    taPersonalBARCODE: TFIBStringField;
    dsPer: TDataSource;
    dsPersonal: TDataSource;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    ViewClose: TcxGridDBTableView;
    LevelClose: TcxGridLevel;
    GridClose: TcxGrid;
    taCloseHist: TpFIBDataSet;
    dstaCloseHist: TDataSource;
    taCloseHistID: TFIBIntegerField;
    taCloseHistDATEDOC: TFIBDateField;
    taCloseHistIDP: TFIBIntegerField;
    taCloseHistVALEDIT: TFIBDateTimeField;
    taCloseHistPNAME: TFIBStringField;
    ViewCloseID: TcxGridDBColumn;
    ViewCloseDATEDOC: TcxGridDBColumn;
    ViewCloseIDP: TcxGridDBColumn;
    ViewCloseVALEDIT: TcxGridDBColumn;
    ViewClosePNAME: TcxGridDBColumn;
    Label2: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHList: TpFIBDataSet;
    quMHListID: TFIBIntegerField;
    quMHListPARENT: TFIBIntegerField;
    quMHListITYPE: TFIBIntegerField;
    quMHListNAMEMH: TFIBStringField;
    quMHListDEFPRICE: TFIBIntegerField;
    quMHListISS: TFIBSmallIntField;
    dsquMHList: TDataSource;
    taCloseHistISKL: TFIBIntegerField;
    taCloseHistNAMEMH: TFIBStringField;
    ViewCloseISKL: TcxGridDBColumn;
    ViewCloseNAMEMH: TcxGridDBColumn;
    quMaxIdCH: TpFIBDataSet;
    quMaxIdCHMAXID: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClosePer: TfmClosePer;

implementation

uses Un1, PerA_Close;

{$R *.dfm}

procedure TfmClosePer.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;

  RnDb.Connected:=False;
  RnDb.DBName:=CommonSet.OfficeDb;
  try
    sErr:=CommonSet.OfficeDb;
    RnDb.Open;

    taPersonal.Active:=True;
  except
    sErr:='���� �� ����������� - '+CommonSet.OfficeDb;
  end;
end;

procedure TfmClosePer.FormShow(Sender: TObject);
begin
  fmPerA_Close:=TfmPerA_Close.Create(Application);
  fmPerA_Close.StatusBar1.Panels[0].Text:=sErr;
  fmPerA_Close.ShowModal;
  if fmPerA_Close.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
  end
  else
  begin
    delay(100);
    close;
    delay(100);
  end;
  fmPerA_Close.Release;

  quMHList.Active:=False;
  quMHList.ParamByName('IDPERSON').AsInteger:=Person.Id;
  quMHList.Active:=True;
  quMHList.First;
  cxLookupComboBox1.EditValue:=0;

  if quMHList.RecordCount>0 then
  begin
    cxLookupComboBox1.EditValue:=quMHListID.AsInteger;
    taCloseHist.Active:=False;
    taCloseHist.ParamByName('ISKL').AsInteger:=quMHListID.AsInteger;
    taCloseHist.Active:=True;
    taCloseHist.First;
  end;

  cxDateEdit1.Date:=date;
end;

procedure TfmClosePer.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmClosePer.cxButton1Click(Sender: TObject);
Var iMax:INteger;
begin
  if cxLookupComboBox1.EditValue=0 then
  begin
    ShowMessage('�������� ����� ��������.');
    exit;
  end;
  if MessageDlg('�� ������������� ������ ������� ��� ��������� ������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������ ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    cxButton2.Enabled:=False;
    ViewClose.BeginUpdate;

    iMax:=1;

    quMaxIdCH.Active:=False;
    quMaxIdCH.Active:=True;
    if quMaxIdCH.RecordCount>0 then iMax:=quMaxIdCHMAXID.AsInteger+1;
    quMaxIdCH.Active:=False;


    taCloseHist.Append;
    taCloseHistID.AsInteger:=iMax;
    taCloseHistDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    taCloseHistIDP.AsInteger:=Person.Id;
    taCloseHistVALEDIT.AsDateTime:=Now;
    taCloseHistISKL.AsInteger:=cxLookupComboBox1.EditValue;
    taCloseHist.Post;

//    taCloseHist.FullRefresh;
    taCloseHist.Refresh;

//    taCloseHist.First;

    ViewClose.EndUpdate;
    cxButton2.Enabled:=True;

    showmessage('������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������, ������ ��� ���������.');
  end;
end;

procedure TfmClosePer.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  //
  ViewClose.BeginUpdate;
  taCloseHist.Active:=False;
  taCloseHist.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
  taCloseHist.Active:=True;
  taCloseHist.First;
  ViewClose.EndUpdate;
end;

end.
