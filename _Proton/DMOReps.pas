unit DMOReps;

interface

uses
  SysUtils, Classes, DB, DBClient, frexpimg, frRtfExp, frTXTExp, frXMLExl,
  frOLEExl, FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT,
  FR_DSet, FR_DBSet, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  FIBQuery, pFIBQuery, pFIBStoredProc,Dialogs, dxmdaset, Un1;

type
  TdmORep = class(TDataModule)
    taPartTest: TClientDataSet;
    taPartTestNum: TIntegerField;
    taPartTestIdGoods: TIntegerField;
    taPartTestNameG: TStringField;
    taPartTestIM: TIntegerField;
    taPartTestSM: TStringField;
    taPartTestQuant: TFloatField;
    taPartTestPrice1: TCurrencyField;
    taPartTestiRes: TSmallintField;
    dsPartTest: TDataSource;
    taCalc: TClientDataSet;
    taCalcArticul: TIntegerField;
    taCalcName: TStringField;
    taCalcIdm: TIntegerField;
    taCalcsM: TStringField;
    taCalcKm: TFloatField;
    taCalcQuant: TFloatField;
    taCalcQuantFact: TFloatField;
    taCalcQuantDiff: TFloatField;
    dsCalc: TDataSource;
    frRep1: TfrReport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBMPExport1: TfrBMPExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    taCalcSumUch: TFloatField;
    taCalcSumIn: TFloatField;
    taCalcB: TClientDataSet;
    taCalcBCODEB: TIntegerField;
    taCalcBNAMEB: TStringField;
    taCalcBQUANT: TFloatField;
    taCalcBPRICEOUT: TFloatField;
    taCalcBSUMOUT: TFloatField;
    taCalcBID: TIntegerField;
    taCalcBIDCARD: TIntegerField;
    taCalcBNAMEC: TStringField;
    taCalcBQUANTC: TFloatField;
    taCalcBPRICEIN: TFloatField;
    taCalcBSUMIN: TFloatField;
    taCalcBIM: TIntegerField;
    taCalcBSM: TStringField;
    dsCalcB: TDataSource;
    frdsCalcB: TfrDBDataSet;
    taCalcBSB: TStringField;
    taTORep: TClientDataSet;
    dsTORep: TDataSource;
    taTORepsType: TStringField;
    taTORepsDocType: TStringField;
    taTORepsCliName: TStringField;
    taTORepsDate: TStringField;
    taTORepsDocNum: TStringField;
    taTOReprSum: TCurrencyField;
    taTOReprSum1: TCurrencyField;
    frdsTORep: TfrDBDataSet;
    taTOReprSumO: TCurrencyField;
    taTOReprSumO1: TCurrencyField;
    taCMove: TClientDataSet;
    dstaCMove: TDataSource;
    taCMoveARTICUL: TIntegerField;
    taCMoveIDATE: TIntegerField;
    taCMoveIDSTORE: TIntegerField;
    taCMoveNAMEMH: TStringField;
    taCMoveRB: TFloatField;
    taCMovePOSTIN: TFloatField;
    taCMovePOSTOUT: TFloatField;
    taCMoveVNIN: TFloatField;
    taCMoveVNOUT: TFloatField;
    taCMoveINV: TFloatField;
    taCMoveQREAL: TFloatField;
    taCMoveRE: TFloatField;
    quDocsInvSel: TpFIBDataSet;
    quDocsInvSelID: TFIBIntegerField;
    quDocsInvSelDATEDOC: TFIBDateField;
    quDocsInvSelNUMDOC: TFIBStringField;
    quDocsInvSelIDSKL: TFIBIntegerField;
    quDocsInvSelNAMEMH: TFIBStringField;
    quDocsInvSelIACTIVE: TFIBIntegerField;
    quDocsInvSelITYPE: TFIBIntegerField;
    quDocsInvSelSUM1: TFIBFloatField;
    quDocsInvSelSUM11: TFIBFloatField;
    quDocsInvSelSUM2: TFIBFloatField;
    quDocsInvSelSUM21: TFIBFloatField;
    quDocsInvSelSUMDIFIN: TFIBFloatField;
    quDocsInvSelSUMDIFUCH: TFIBFloatField;
    dsquDocsInvSel: TDataSource;
    quDocsInvId: TpFIBDataSet;
    quDocsInvIdID: TFIBIntegerField;
    quDocsInvIdDATEDOC: TFIBDateField;
    quDocsInvIdNUMDOC: TFIBStringField;
    quDocsInvIdIDSKL: TFIBIntegerField;
    quDocsInvIdIACTIVE: TFIBIntegerField;
    quDocsInvIdITYPE: TFIBIntegerField;
    quDocsInvIdSUM1: TFIBFloatField;
    quDocsInvIdSUM11: TFIBFloatField;
    quDocsInvIdSUM2: TFIBFloatField;
    quDocsInvIdSUM21: TFIBFloatField;
    quSpecInv: TpFIBDataSet;
    quSpecInvIDHEAD: TFIBIntegerField;
    quSpecInvID: TFIBIntegerField;
    quSpecInvNUM: TFIBIntegerField;
    quSpecInvIDCARD: TFIBIntegerField;
    quSpecInvQUANT: TFIBFloatField;
    quSpecInvSUMIN: TFIBFloatField;
    quSpecInvSUMUCH: TFIBFloatField;
    quSpecInvIDMESSURE: TFIBIntegerField;
    quSpecInvQUANTFACT: TFIBFloatField;
    quSpecInvSUMINFACT: TFIBFloatField;
    quSpecInvSUMUCHFACT: TFIBFloatField;
    quSpecInvKM: TFIBFloatField;
    quSpecInvTCARD: TFIBSmallIntField;
    quSpecInvIDGROUP: TFIBIntegerField;
    quSpecInvNAMESHORT: TFIBStringField;
    quSpecInvNAMECL: TFIBStringField;
    quSpecInvNAME: TFIBStringField;
    quSpecInvC: TpFIBDataSet;
    quSpecInvCIDHEAD: TFIBIntegerField;
    quSpecInvCID: TFIBIntegerField;
    quSpecInvCNUM: TFIBIntegerField;
    quSpecInvCIDCARD: TFIBIntegerField;
    quSpecInvCQUANT: TFIBFloatField;
    quSpecInvCSUMIN: TFIBFloatField;
    quSpecInvCSUMUCH: TFIBFloatField;
    quSpecInvCIDMESSURE: TFIBIntegerField;
    quSpecInvCQUANTFACT: TFIBFloatField;
    quSpecInvCSUMINFACT: TFIBFloatField;
    quSpecInvCSUMUCHFACT: TFIBFloatField;
    quSpecInvCKM: TFIBFloatField;
    quSpecInvCTCARD: TFIBSmallIntField;
    quSpecInvCIDGROUP: TFIBIntegerField;
    quSpecInvCNAMESHORT: TFIBStringField;
    quSpecInvCNAMECL: TFIBStringField;
    quSpecInvCNAME: TFIBStringField;
    trSelRep: TpFIBTransaction;
    quDocOutBPrint: TpFIBDataSet;
    quDocOutBPrintIDB: TFIBIntegerField;
    quDocOutBPrintCODEB: TFIBIntegerField;
    quDocOutBPrintQUANT: TFIBFloatField;
    quDocOutBPrintSUMOUT: TFIBFloatField;
    quDocOutBPrintIDSKL: TFIBIntegerField;
    quDocOutBPrintSUMIN: TFIBFloatField;
    quDocOutBPrintNAMEB: TFIBStringField;
    quDocOutBPrintPRICER: TFIBFloatField;
    quSelInSum: TpFIBDataSet;
    quSelNamePos: TpFIBDataSet;
    quSelNamePosNAMECL: TFIBStringField;
    quSelNamePosPARENT: TFIBIntegerField;
    quSelNamePosNAME: TFIBStringField;
    quSelNamePosSM: TFIBStringField;
    quSelNamePosKOEF: TFIBFloatField;
    quSelNamePosIMESSURE: TFIBIntegerField;
    quSelOutSum: TpFIBDataSet;
    quSpecInvNOCALC: TFIBSmallIntField;
    quSelNameCl: TpFIBDataSet;
    quSelNameClPARENT: TFIBIntegerField;
    quSelNameClNAMECL: TFIBStringField;
    quSpecInvBC: TpFIBDataSet;
    quSpecInvBCIDHEAD: TFIBIntegerField;
    quSpecInvBCIDB: TFIBIntegerField;
    quSpecInvBCID: TFIBIntegerField;
    quSpecInvBCCODEB: TFIBIntegerField;
    quSpecInvBCNAMEB: TFIBStringField;
    quSpecInvBCQUANT: TFIBFloatField;
    quSpecInvBCPRICEOUT: TFIBFloatField;
    quSpecInvBCSUMOUT: TFIBFloatField;
    quSpecInvBCIDCARD: TFIBIntegerField;
    quSpecInvBCNAMEC: TFIBStringField;
    quSpecInvBCQUANTC: TFIBFloatField;
    quSpecInvBCPRICEIN: TFIBFloatField;
    quSpecInvBCSUMIN: TFIBFloatField;
    quSpecInvBCIM: TFIBIntegerField;
    quSpecInvBCSM: TFIBStringField;
    quSpecInvBCSB: TFIBStringField;
    quDocsCompl: TpFIBDataSet;
    dsquDocsCompl: TDataSource;
    quDocsComplID: TFIBIntegerField;
    quDocsComplDATEDOC: TFIBDateField;
    quDocsComplNUMDOC: TFIBStringField;
    quDocsComplIDSKL: TFIBIntegerField;
    quDocsComplSUMIN: TFIBFloatField;
    quDocsComplSUMUCH: TFIBFloatField;
    quDocsComplSUMTAR: TFIBFloatField;
    quDocsComplPROCNAC: TFIBFloatField;
    quDocsComplIACTIVE: TFIBIntegerField;
    quDocsComplOPER: TFIBStringField;
    quDocsComplNAMEMH: TFIBStringField;
    quDocsComlRec: TpFIBDataSet;
    quSpecCompl: TpFIBDataSet;
    quSpecComplC: TpFIBDataSet;
    quSpecComplCB: TpFIBDataSet;
    quDocsComlRecID: TFIBIntegerField;
    quDocsComlRecDATEDOC: TFIBDateField;
    quDocsComlRecNUMDOC: TFIBStringField;
    quDocsComlRecIDSKL: TFIBIntegerField;
    quDocsComlRecSUMIN: TFIBFloatField;
    quDocsComlRecSUMUCH: TFIBFloatField;
    quDocsComlRecSUMTAR: TFIBFloatField;
    quDocsComlRecPROCNAC: TFIBFloatField;
    quDocsComlRecIACTIVE: TFIBIntegerField;
    quDocsComlRecOPER: TFIBStringField;
    quSpecComplIDHEAD: TFIBIntegerField;
    quSpecComplID: TFIBIntegerField;
    quSpecComplIDCARD: TFIBIntegerField;
    quSpecComplQUANT: TFIBFloatField;
    quSpecComplIDM: TFIBIntegerField;
    quSpecComplKM: TFIBFloatField;
    quSpecComplPRICEIN: TFIBFloatField;
    quSpecComplSUMIN: TFIBFloatField;
    quSpecComplPRICEINUCH: TFIBFloatField;
    quSpecComplSUMINUCH: TFIBFloatField;
    quSpecComplTCARD: TFIBSmallIntField;
    quSpecComplNAMESHORT: TFIBStringField;
    quSpecComplNAME: TFIBStringField;
    quSpecComplCIDHEAD: TFIBIntegerField;
    quSpecComplCID: TFIBIntegerField;
    quSpecComplCIDCARD: TFIBIntegerField;
    quSpecComplCQUANT: TFIBFloatField;
    quSpecComplCIDM: TFIBIntegerField;
    quSpecComplCKM: TFIBFloatField;
    quSpecComplCPRICEIN: TFIBFloatField;
    quSpecComplCSUMIN: TFIBFloatField;
    quSpecComplCPRICEINUCH: TFIBFloatField;
    quSpecComplCSUMINUCH: TFIBFloatField;
    quSpecComplCNAMESHORT: TFIBStringField;
    quSpecComplCNAME: TFIBStringField;
    quSpecComplCBIDHEAD: TFIBIntegerField;
    quSpecComplCBIDB: TFIBIntegerField;
    quSpecComplCBID: TFIBIntegerField;
    quSpecComplCBCODEB: TFIBIntegerField;
    quSpecComplCBNAMEB: TFIBStringField;
    quSpecComplCBQUANT: TFIBFloatField;
    quSpecComplCBIDCARD: TFIBIntegerField;
    quSpecComplCBNAMEC: TFIBStringField;
    quSpecComplCBQUANTC: TFIBFloatField;
    quSpecComplCBPRICEIN: TFIBFloatField;
    quSpecComplCBSUMIN: TFIBFloatField;
    quSpecComplCBIM: TFIBIntegerField;
    quSpecComplCBSM: TFIBStringField;
    quSpecComplCBSB: TFIBStringField;
    taHeadDoc: TClientDataSet;
    taSpecDoc: TClientDataSet;
    taHeadDocIType: TSmallintField;
    taHeadDocDateDoc: TIntegerField;
    taHeadDocNumDoc: TStringField;
    taHeadDocIdCli: TIntegerField;
    taHeadDocNameCli: TStringField;
    taHeadDocIdSkl: TIntegerField;
    taHeadDocNameSkl: TStringField;
    taHeadDocSumIN: TFloatField;
    taHeadDocSumUch: TFloatField;
    taHeadDocId: TIntegerField;
    taSpecDocIType: TSmallintField;
    taSpecDocIdHead: TIntegerField;
    taSpecDocNum: TIntegerField;
    taSpecDocIdCard: TIntegerField;
    taSpecDocQuant: TFloatField;
    taSpecDocPriceIn: TFloatField;
    taSpecDocSumIn: TFloatField;
    taSpecDocPriceUch: TFloatField;
    taSpecDocSumUch: TFloatField;
    taSpecDocIdNds: TIntegerField;
    taSpecDocSumNds: TFloatField;
    taSpecDocNameC: TStringField;
    taSpecDocSm: TStringField;
    taSpecDocIdM: TIntegerField;
    dsHeadDoc: TDataSource;
    dsSpecDoc: TDataSource;
    quDocsActs: TpFIBDataSet;
    dsquDocsActs: TDataSource;
    quDocsActsId: TpFIBDataSet;
    quDocsActsID2: TFIBIntegerField;
    quDocsActsDATEDOC: TFIBDateField;
    quDocsActsNUMDOC: TFIBStringField;
    quDocsActsIDSKL: TFIBIntegerField;
    quDocsActsSUMIN: TFIBFloatField;
    quDocsActsSUMUCH: TFIBFloatField;
    quDocsActsIACTIVE: TFIBIntegerField;
    quDocsActsOPER: TFIBStringField;
    quDocsActsIdID: TFIBIntegerField;
    quDocsActsIdDATEDOC: TFIBDateField;
    quDocsActsIdNUMDOC: TFIBStringField;
    quDocsActsIdIDSKL: TFIBIntegerField;
    quDocsActsIdSUMIN: TFIBFloatField;
    quDocsActsIdSUMUCH: TFIBFloatField;
    quDocsActsIdIACTIVE: TFIBIntegerField;
    quDocsActsIdOPER: TFIBStringField;
    quDocsActsNAMEMH: TFIBStringField;
    quDocsVnSel: TpFIBDataSet;
    dsDocsVnSel: TDataSource;
    quSpecVnSel: TpFIBDataSet;
    quDocsVnId: TpFIBDataSet;
    quDocsVnSelID: TFIBIntegerField;
    quDocsVnSelDATEDOC: TFIBDateField;
    quDocsVnSelNUMDOC: TFIBStringField;
    quDocsVnSelIDSKL_FROM: TFIBIntegerField;
    quDocsVnSelIDSKL_TO: TFIBIntegerField;
    quDocsVnSelSUMIN: TFIBFloatField;
    quDocsVnSelSUMUCH: TFIBFloatField;
    quDocsVnSelSUMUCH1: TFIBFloatField;
    quDocsVnSelSUMTAR: TFIBFloatField;
    quDocsVnSelPROCNAC: TFIBFloatField;
    quDocsVnSelIACTIVE: TFIBIntegerField;
    quDocsVnSelNAMEMH: TFIBStringField;
    quDocsVnSelNAMEMH1: TFIBStringField;
    quDocsVnIdID: TFIBIntegerField;
    quDocsVnIdDATEDOC: TFIBDateField;
    quDocsVnIdNUMDOC: TFIBStringField;
    quDocsVnIdIDSKL_FROM: TFIBIntegerField;
    quDocsVnIdIDSKL_TO: TFIBIntegerField;
    quDocsVnIdSUMIN: TFIBFloatField;
    quDocsVnIdSUMUCH: TFIBFloatField;
    quDocsVnIdSUMUCH1: TFIBFloatField;
    quDocsVnIdSUMTAR: TFIBFloatField;
    quDocsVnIdPROCNAC: TFIBFloatField;
    quDocsVnIdIACTIVE: TFIBIntegerField;
    quSpecVnSelIDHEAD: TFIBIntegerField;
    quSpecVnSelID: TFIBIntegerField;
    quSpecVnSelNUM: TFIBIntegerField;
    quSpecVnSelIDCARD: TFIBIntegerField;
    quSpecVnSelQUANT: TFIBFloatField;
    quSpecVnSelPRICEIN: TFIBFloatField;
    quSpecVnSelSUMIN: TFIBFloatField;
    quSpecVnSelPRICEUCH: TFIBFloatField;
    quSpecVnSelSUMUCH: TFIBFloatField;
    quSpecVnSelPRICEUCH1: TFIBFloatField;
    quSpecVnSelSUMUCH1: TFIBFloatField;
    quSpecVnSelIDM: TFIBIntegerField;
    quSpecVnSelNAMEC: TFIBStringField;
    quSpecVnSelSM: TFIBStringField;
    quSpecVnSelKM: TFIBFloatField;
    taSpecDocKm: TFloatField;
    taSpecDocPriceUch1: TFloatField;
    taSpecDocSumUch1: TFloatField;
    quSpecAO: TpFIBDataSet;
    quSpecAOIDHEAD: TFIBIntegerField;
    quSpecAOID: TFIBIntegerField;
    quSpecAOIDCARD: TFIBIntegerField;
    quSpecAOQUANT: TFIBFloatField;
    quSpecAOIDM: TFIBIntegerField;
    quSpecAOKM: TFIBFloatField;
    quSpecAOPRICEIN: TFIBFloatField;
    quSpecAOSUMIN: TFIBFloatField;
    quSpecAOPRICEINUCH: TFIBFloatField;
    quSpecAOSUMINUCH: TFIBFloatField;
    quSpecAOTCARD: TFIBSmallIntField;
    quSpecAONAMESHORT: TFIBStringField;
    quSpecAONAME: TFIBStringField;
    quSpecAI: TpFIBDataSet;
    quSpecAIIDHEAD: TFIBIntegerField;
    quSpecAIID: TFIBIntegerField;
    quSpecAIIDCARD: TFIBIntegerField;
    quSpecAIQUANT: TFIBFloatField;
    quSpecAIIDM: TFIBIntegerField;
    quSpecAIKM: TFIBFloatField;
    quSpecAIPRICEIN: TFIBFloatField;
    quSpecAISUMIN: TFIBFloatField;
    quSpecAIPRICEINUCH: TFIBFloatField;
    quSpecAISUMINUCH: TFIBFloatField;
    quSpecAITCARD: TFIBSmallIntField;
    quSpecAINAMESHORT: TFIBStringField;
    quSpecAINAME: TFIBStringField;
    quSpecAIPROCPRICE: TFIBFloatField;
    taSpecB: TClientDataSet;
    taSpecBID: TIntegerField;
    taSpecBIDCARD: TIntegerField;
    taSpecBQUANT: TFloatField;
    taSpecBIDM: TIntegerField;
    taSpecBSIFR: TIntegerField;
    taSpecBNAMEB: TStringField;
    taSpecBCODEB: TStringField;
    taSpecBKB: TFloatField;
    taSpecBPRICER: TFloatField;
    taSpecBDSUM: TCurrencyField;
    taSpecBRSUM: TCurrencyField;
    taSpecBNAME: TStringField;
    taSpecBNAMESHORT: TStringField;
    taSpecBTCARD: TSmallintField;
    taSpecBKM: TFloatField;
    dstaSpecB: TDataSource;
    quDocOutBPrintNAME: TFIBStringField;
    quDocsActsCOMMENT: TFIBStringField;
    quDocsActsIdCOMMENT: TFIBStringField;
    quOperT: TpFIBDataSet;
    quOperTABR: TFIBStringField;
    quOperTID: TFIBIntegerField;
    quOperTDOCTYPE: TFIBSmallIntField;
    quOperTNAMEOPER: TFIBStringField;
    quOperTNAMEDOC: TFIBStringField;
    dsquOperT: TDataSource;
    taDocType: TpFIBDataSet;
    taDocTypeID: TFIBSmallIntField;
    taDocTypeNAMEDOC: TFIBStringField;
    dstaDocType: TDataSource;
    taOperT: TpFIBDataSet;
    taOperTABR: TFIBStringField;
    taOperTID: TFIBIntegerField;
    taOperD: TpFIBDataSet;
    taOperDABR: TFIBStringField;
    taOperDID: TFIBIntegerField;
    taOperDDOCTYPE: TFIBSmallIntField;
    taOperDNAMEOPER: TFIBStringField;
    dstaOperD: TDataSource;
    taSpecDoc1: TClientDataSet;
    taSpecDoc1IType: TSmallintField;
    taSpecDoc1IdHead: TIntegerField;
    taSpecDoc1Num: TIntegerField;
    taSpecDoc1IdCard: TIntegerField;
    taSpecDoc1Quant: TFloatField;
    taSpecDoc1PriceIn: TFloatField;
    taSpecDoc1SumIn: TFloatField;
    taSpecDoc1PriceUch: TFloatField;
    taSpecDoc1SumUch: TFloatField;
    taSpecDoc1IdNDS: TIntegerField;
    taSpecDoc1SumNds: TFloatField;
    taSpecDoc1NameC: TStringField;
    taSpecDoc1SM: TStringField;
    taSpecDoc1IDM: TIntegerField;
    taSpecDoc1Km: TFloatField;
    taSpecDoc1PriceUch1: TFloatField;
    taSpecDoc1SumUch1: TFloatField;
    taSpecDocProcPrice: TFloatField;
    prAddPartOutT: TpFIBStoredProc;
    prAddPartIn1T: TpFIBStoredProc;
    quSpecVnGrPos: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    FIBFloatField5: TFIBFloatField;
    FIBFloatField6: TFIBFloatField;
    FIBFloatField7: TFIBFloatField;
    FIBIntegerField5: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField8: TFIBFloatField;
    prTestSpecPart: TpFIBStoredProc;
    prResetSum: TpFIBStoredProc;
    quCardPO: TpFIBDataSet;
    quCardPOQUANT: TFIBFloatField;
    quCardPORSUM: TFIBFloatField;
    prCalcSumRemn: TpFIBStoredProc;
    quDocsRSel: TpFIBDataSet;
    quDocsRSelID: TFIBIntegerField;
    quDocsRSelDATEDOC: TFIBDateField;
    quDocsRSelNUMDOC: TFIBStringField;
    quDocsRSelDATESF: TFIBDateField;
    quDocsRSelNUMSF: TFIBStringField;
    quDocsRSelIDCLI: TFIBIntegerField;
    quDocsRSelIDSKL: TFIBIntegerField;
    quDocsRSelSUMIN: TFIBFloatField;
    quDocsRSelSUMUCH: TFIBFloatField;
    quDocsRSelSUMTAR: TFIBFloatField;
    quDocsRSelSUMNDS0: TFIBFloatField;
    quDocsRSelSUMNDS1: TFIBFloatField;
    quDocsRSelSUMNDS2: TFIBFloatField;
    quDocsRSelPROCNAC: TFIBFloatField;
    quDocsRSelIACTIVE: TFIBIntegerField;
    quDocsRSelNAMECL: TFIBStringField;
    quDocsRSelNAMEMH: TFIBStringField;
    dsquDocsRSel: TDataSource;
    quDocsRId: TpFIBDataSet;
    quDocsRIdID: TFIBIntegerField;
    quDocsRIdDATEDOC: TFIBDateField;
    quDocsRIdNUMDOC: TFIBStringField;
    quDocsRIdDATESF: TFIBDateField;
    quDocsRIdNUMSF: TFIBStringField;
    quDocsRIdIDCLI: TFIBIntegerField;
    quDocsRIdIDSKL: TFIBIntegerField;
    quDocsRIdSUMIN: TFIBFloatField;
    quDocsRIdSUMUCH: TFIBFloatField;
    quDocsRIdSUMTAR: TFIBFloatField;
    quDocsRIdSUMNDS0: TFIBFloatField;
    quDocsRIdSUMNDS1: TFIBFloatField;
    quDocsRIdSUMNDS2: TFIBFloatField;
    quDocsRIdPROCNAC: TFIBFloatField;
    quDocsRIdIACTIVE: TFIBIntegerField;
    quSpecRSel: TpFIBDataSet;
    quSpecRSelIDHEAD: TFIBIntegerField;
    quSpecRSelID: TFIBIntegerField;
    quSpecRSelIDCARD: TFIBIntegerField;
    quSpecRSelQUANT: TFIBFloatField;
    quSpecRSelIDM: TFIBIntegerField;
    quSpecRSelKM: TFIBFloatField;
    quSpecRSelPRICEIN: TFIBFloatField;
    quSpecRSelSUMIN: TFIBFloatField;
    quSpecRSelTCARD: TFIBSmallIntField;
    quSpecRSelNAMEC: TFIBStringField;
    quSpecRSelSM: TFIBStringField;
    quSpecRSelPRICER: TFIBFloatField;
    quSpecRSelSUMR: TFIBFloatField;
    quSpecRSelIDNDS: TFIBIntegerField;
    quSpecRSelSUMNDS: TFIBFloatField;
    quSpecRSelNAMENDS: TFIBStringField;
    quSpecRDay: TpFIBDataSet;
    quSpecRDayIDCARD: TFIBIntegerField;
    quSpecRDayIDM: TFIBIntegerField;
    quSpecRDayKM: TFIBFloatField;
    quSpecRDayQUANT: TFIBFloatField;
    quSpecRDaySUMR: TFIBFloatField;
    quSpecRDayNAME: TFIBStringField;
    quSpecRDayNAMESHORT: TFIBStringField;
    quDocsInvIdSUM3: TFIBFloatField;
    quDocsInvIdSUM31: TFIBFloatField;
    quDocsInvSelSUM3: TFIBFloatField;
    quDocsInvSelSUM31: TFIBFloatField;
    quDocsInvSelSUMDIFINF: TFIBFloatField;
    quSpecInvSUMINR: TFIBFloatField;
    quSpecInvPRICER: TFIBFloatField;
    quSpecInvCSUMINR: TFIBFloatField;
    taHeadDocComment: TStringField;
    taDelCard: TpFIBDataSet;
    taDelCardID: TFIBIntegerField;
    taDelCardSIFR: TFIBIntegerField;
    taDelCardNAME: TFIBStringField;
    taDelCardTTYPE: TFIBIntegerField;
    taDelCardIMESSURE: TFIBIntegerField;
    prRECALCGDS: TpFIBStoredProc;
    trD: TpFIBTransaction;
    quClass: TpFIBDataSet;
    quClassID: TFIBIntegerField;
    quClassNAME: TFIBStringField;
    quClassPARENT: TFIBIntegerField;
    quClassIMESSURE: TFIBIntegerField;
    quClassNAMECL: TFIBStringField;
    quFindPart: TpFIBDataSet;
    quFindPartID: TFIBIntegerField;
    quFindPartIDSTORE: TFIBIntegerField;
    quFindPartIDDOC: TFIBIntegerField;
    quFindPartARTICUL: TFIBIntegerField;
    quFindPartIDCLI: TFIBIntegerField;
    quFindPartDTYPE: TFIBIntegerField;
    quFindPartQPART: TFIBFloatField;
    quFindPartQREMN: TFIBFloatField;
    quFindPartPRICEIN: TFIBFloatField;
    quFindPartIDATE: TFIBIntegerField;
    quFindPartNAMECL: TFIBStringField;
    quMainClass: TpFIBDataSet;
    quMainClassID: TFIBIntegerField;
    quFindPartNAMEMH: TFIBStringField;
    pr_CalcLastPrice: TpFIBStoredProc;
    quMovePer: TpFIBDataSet;
    quMovePerPOSTIN: TFIBFloatField;
    quMovePerPOSTOUT: TFIBFloatField;
    quMovePerVNIN: TFIBFloatField;
    quMovePerVNOUT: TFIBFloatField;
    quMovePerINV: TFIBFloatField;
    quMovePerQREAL: TFIBFloatField;
    quSelInSumIDATE: TFIBIntegerField;
    quSelInSumQPART: TFIBFloatField;
    quSelInSumPRICEIN: TFIBFloatField;
    quSelOutSumIDDATE: TFIBIntegerField;
    quSelOutSumQUANT: TFIBFloatField;
    quSelOutSumPRICEIN: TFIBFloatField;
    quSelOutSumSUMOUT: TFIBFloatField;
    quDocsInvSelCOMMENT: TFIBStringField;
    quDocsInvIdCOMMENT: TFIBStringField;
    quFindSebReal: TpFIBDataSet;
    quFindSebRealIDM: TFIBIntegerField;
    quFindSebRealKM: TFIBFloatField;
    quFindSebRealPRICEIN: TFIBFloatField;
    dsquFindSebReal: TDataSource;
    quFindCl: TpFIBDataSet;
    quFindClID_PARENT: TFIBIntegerField;
    quFindClNAMECL: TFIBStringField;
    quFindMassa: TpFIBDataSet;
    quFindMassaPOUTPUT: TFIBStringField;
    quOperTTPRIB: TFIBSmallIntField;
    quOperTTSPIS: TFIBSmallIntField;
    quFindTSpis: TpFIBDataSet;
    quFindTSpisTSPIS: TFIBSmallIntField;
    quSpecRDaySUMIN: TFIBFloatField;
    quDB: TpFIBDataSet;
    quDBID: TFIBIntegerField;
    quDBNAMEDB: TFIBStringField;
    quDBPATHDB: TFIBStringField;
    dsquDB: TDataSource;
    quClosePartIn: TpFIBQuery;
    trU: TpFIBTransaction;
    quPODoc: TpFIBDataSet;
    quPODocSUMIN: TFIBFloatField;
    quFindCli: TpFIBDataSet;
    quFindCliID: TFIBIntegerField;
    quFindCliNAMECL: TFIBStringField;
    quSpecRSelOPER: TFIBSmallIntField;
    quSpecRSel1: TpFIBDataSet;
    quSpecRSel1IDHEAD: TFIBIntegerField;
    quSpecRSel1ID: TFIBIntegerField;
    quSpecRSel1IDCARD: TFIBIntegerField;
    quSpecRSel1QUANT: TFIBFloatField;
    quSpecRSel1IDM: TFIBIntegerField;
    quSpecRSel1KM: TFIBFloatField;
    quSpecRSel1PRICEIN: TFIBFloatField;
    quSpecRSel1SUMIN: TFIBFloatField;
    quSpecRSel1PRICER: TFIBFloatField;
    quSpecRSel1SUMR: TFIBFloatField;
    quSpecRSel1TCARD: TFIBSmallIntField;
    quSpecRSel1IDNDS: TFIBIntegerField;
    quSpecRSel1SUMNDS: TFIBFloatField;
    quSpecRSel1NAMEC: TFIBStringField;
    quSpecRSel1SM: TFIBStringField;
    quSpecRSel1NAMENDS: TFIBStringField;
    quSpecRSel1OPER: TFIBSmallIntField;
    taCardM: TdxMemData;
    taCardMIdDoc: TIntegerField;
    taCardMTypeD: TIntegerField;
    taCardMDateD: TDateField;
    taCardMsFrom: TStringField;
    taCardMsTo: TStringField;
    taCardMiM: TIntegerField;
    taCardMsM: TStringField;
    taCardMQuant: TFloatField;
    taCardMQRemn: TFloatField;
    taCardMComment: TStringField;
    dstaCardM: TDataSource;
    quTestInput: TpFIBDataSet;
    dsquTestInput: TDataSource;
    quTestInputID: TFIBIntegerField;
    quTestInputDATEDOC: TFIBDateField;
    quTestInputNUMDOC: TFIBStringField;
    quTestInputIDCLI: TFIBIntegerField;
    quTestInputIDSKL: TFIBIntegerField;
    quTestInputSUMIN: TFIBFloatField;
    quTestInputIACTIVE: TFIBIntegerField;
    quTestInputOPER: TFIBStringField;
    quTestInputCOMMENT: TFIBStringField;
    quTestInputNAMEMH: TFIBStringField;
    quDocsRSelIDFROM: TFIBIntegerField;
    quDocsRSelNAMECLFROM: TFIBStringField;
    quDocsRIdIDFROM: TFIBIntegerField;
    quSpecVnSelCATEGORY: TFIBIntegerField;
    quSpecInvCATEGORY: TFIBIntegerField;
    quSpecInvCCATEGORY: TFIBIntegerField;
    quDocsInvSelSUMTARAR: TFIBFloatField;
    quDocsInvSelSUMTARAF: TFIBFloatField;
    quDocsInvSelSUMTARAD: TFIBFloatField;
    quDocsInvIdSUMTARAR: TFIBFloatField;
    quDocsInvIdSUMTARAF: TFIBFloatField;
    quDocsInvIdSUMTARAD: TFIBFloatField;
    taTOReprSumPr: TFloatField;
    taTORepPriceType: TStringField;
    taTORepsSumPr: TStringField;
    quDocsInvSelIDETAL: TFIBSmallIntField;
    quDocsInvIdIDETAL: TFIBSmallIntField;
    quDocsComplIDSKLTO: TFIBIntegerField;
    quDocsComplIDFROM: TFIBIntegerField;
    quDocsComplTOREAL: TFIBSmallIntField;
    quDocsComplNAMEMH1: TFIBStringField;
    quDocsComlRecIDFROM: TFIBIntegerField;
    quDocsComlRecTOREAL: TFIBSmallIntField;
    quDocsComlRecIDSKLTO: TFIBIntegerField;
    quSpecRSelPROC: TFIBFloatField;
    quSpecRSelOUTNDSSUM: TFIBFloatField;
    teCalcB1: TdxMemData;
    teCalcB1ID: TIntegerField;
    teCalcB1CODEB: TIntegerField;
    teCalcB1NAMEB: TStringField;
    teCalcB1QUANT: TFloatField;
    teCalcB1PRICEOUT: TFloatField;
    teCalcB1SUMOUT: TFloatField;
    teCalcB1IDCARD: TIntegerField;
    teCalcB1NAMEC: TStringField;
    teCalcB1QUANTC: TFloatField;
    teCalcB1PRICEIN: TFloatField;
    teCalcB1SUMIN: TFloatField;
    teCalcB1IM: TIntegerField;
    teCalcB1SM: TStringField;
    teCalcB1SB: TStringField;
    dsteCalcB1: TDataSource;
    quDocsRCard: TpFIBDataSet;
    quDocsRCardIDCARD: TFIBIntegerField;
    quDocsRCardNAME: TFIBStringField;
    quDocsRCardIDM: TFIBIntegerField;
    quDocsRCardNAMESHORT: TFIBStringField;
    quDocsRCardKM: TFIBFloatField;
    quDocsRCardCATEGORY: TFIBIntegerField;
    quDocsRCardDATEDOC: TFIBDateField;
    quDocsRCardIDCLI: TFIBIntegerField;
    quDocsRCardIDSKL: TFIBIntegerField;
    quDocsRCardIDFROM: TFIBIntegerField;
    quDocsRCardMHTO: TFIBStringField;
    quDocsRCardMHFROM: TFIBStringField;
    quDocsRCardNAMECL: TFIBStringField;
    quDocsRCardIDATE: TFIBIntegerField;
    quDocsRCardKVART: TFIBSmallIntField;
    quDocsRCardWEEKOFM: TFIBIntegerField;
    quDocsRCardQUANT: TFIBFloatField;
    quDocsRCardSUMIN: TFIBFloatField;
    quDocsRCardSUMR: TFIBFloatField;
    dsquDocsRCard: TDataSource;
    quDocsRCardRNac: TFloatField;
    quDocsRCardDAYWEEK: TFIBSmallIntField;
    quDocsRCardOPER: TSmallintField;
    quSpecRSelNAMECTO: TFIBStringField;
    quSpecRSelID1: TFIBIntegerField;
    quSpecRSelCATEGORY: TFIBIntegerField;
    quSpecRSel2: TpFIBDataSet;
    quSpecRSel2IDHEAD: TFIBIntegerField;
    quSpecRSel2ID: TFIBIntegerField;
    quSpecRSel2IDCARD: TFIBIntegerField;
    quSpecRSel2QUANT: TFIBFloatField;
    quSpecRSel2IDM: TFIBIntegerField;
    quSpecRSel2KM: TFIBFloatField;
    quSpecRSel2PRICEIN: TFIBFloatField;
    quSpecRSel2SUMIN: TFIBFloatField;
    quSpecRSel2PRICER: TFIBFloatField;
    quSpecRSel2SUMR: TFIBFloatField;
    quSpecRSel2TCARD: TFIBSmallIntField;
    quSpecRSel2IDNDS: TFIBIntegerField;
    quSpecRSel2SUMNDS: TFIBFloatField;
    quSpecRSel2NAMEC: TFIBStringField;
    quSpecRSel2CATEGORY: TFIBIntegerField;
    quSpecRSel2SM: TFIBStringField;
    quSpecRSel2NAMENDS: TFIBStringField;
    quSpecRSel2OPER: TFIBSmallIntField;
    quSpecRSel2PROC: TFIBFloatField;
    quSpecRSel2OUTNDSSUM: TFIBFloatField;
    quSpecRSel2ID1: TFIBIntegerField;
    quSpecRSel2NAMECTO: TFIBStringField;
    teCalcB1PRICEIN0: TFloatField;
    teCalcB1SUMIN0: TFloatField;
    quSpecComplPRICEIN0: TFIBFloatField;
    quSpecComplSUMIN0: TFIBFloatField;
    quSpecComplCBPRICEIN0: TFIBFloatField;
    quSpecComplCBSUMIN0: TFIBFloatField;
    quSpecComplCPRICEIN0: TFIBFloatField;
    quSpecComplCSUMIN0: TFIBFloatField;
    quSpecRSelPRICEIN0: TFIBFloatField;
    quSpecRSelSUMIN0: TFIBFloatField;
    quFindSebRealPRICEIN0: TFIBFloatField;
    quSpecRSel1PRICEIN0: TFIBFloatField;
    quSpecRSel1SUMIN0: TFIBFloatField;
    quSpecAOPRICEIN0: TFIBFloatField;
    quSpecAOSUMIN0: TFIBFloatField;
    quSpecAOINDS: TFIBIntegerField;
    quSpecAORNDS: TFIBFloatField;
    quSpecAONAMENDS: TFIBStringField;
    quSpecAIPRICEIN0: TFIBFloatField;
    quSpecAISUMIN0: TFIBFloatField;
    quSpecAIINDS: TFIBIntegerField;
    quSpecAIRNDS: TFIBFloatField;
    quSpecAINAMENDS: TFIBStringField;
    taCalcB1: TdxMemData;
    taCalcB1ID: TIntegerField;
    taCalcB1CODEB: TIntegerField;
    taCalcB1NAMEB: TStringField;
    taCalcB1QUANT: TFloatField;
    taCalcB1PRICEOUT: TFloatField;
    taCalcB1SUMOUT: TFloatField;
    taCalcB1IDCARD: TIntegerField;
    taCalcB1NAMEC: TStringField;
    taCalcB1QUANTC: TFloatField;
    taCalcB1PRICEIN: TFloatField;
    taCalcB1SUMIN: TFloatField;
    taCalcB1IM: TIntegerField;
    taCalcB1SB: TStringField;
    taCalcB1SM: TStringField;
    quSpecInvSUMIN0: TFIBFloatField;
    quSpecInvSUMINFACT0: TFIBFloatField;
    quSpecInvNDSPROC: TFIBFloatField;
    quSpecInvNDSSUM: TFIBFloatField;
    quSpecInvCSUMIN0: TFIBFloatField;
    quSpecInvCSUMINFACT0: TFIBFloatField;
    quSpecInvCNDSPROC: TFIBFloatField;
    quSpecInvCNDSSUM: TFIBFloatField;
    taCalcB1PRICEIN0: TFloatField;
    taCalcB1SUMIN0: TFloatField;
    quSpecInvBCPRICEIN0: TFIBFloatField;
    quSpecInvBCSUMIN0: TFIBFloatField;
    quSpecRSelSUMOUT0: TFIBFloatField;
    quSpecRSelSUMNDSOUT: TFIBFloatField;
    quSpecRSel1SUMOUT0: TFIBFloatField;
    quSpecRSel1SUMNDSOUT: TFIBFloatField;
    quSpecRSel2SUMOUT0: TFIBFloatField;
    quSpecRSel2SUMNDSOUT: TFIBFloatField;
    taTORepsSumNDS: TStringField;
    quDocsRCardRCATEGORY: TFIBIntegerField;
    quDocsRCardNAMECAT: TFIBStringField;
    quSpecRSelINDS: TFIBIntegerField;
    quDocOutBPrintNAMESHORT: TFIBStringField;
    quDocsRSelBZTYPE: TFIBSmallIntField;
    quDocsRSelBZSTATUS: TFIBSmallIntField;
    quDocsRSelBZSUMR: TFIBFloatField;
    quDocsRSelBZSUMF: TFIBFloatField;
    quDocsRSelBZSUMS: TFIBFloatField;
    quDocsRSelIEDIT: TFIBSmallIntField;
    quDocsRSelPERSEDIT: TFIBStringField;
    quDocsRSelIPERSEDIT: TFIBIntegerField;
    quSetSync: TpFIBQuery;
    quDocsRIdIEDIT: TFIBSmallIntField;
    quDocsRIdPERSEDIT: TFIBStringField;
    quDocsRIdIPERSEDIT: TFIBIntegerField;
    quGetSync: TpFIBDataSet;
    trSetSync: TpFIBTransaction;
    trGetSync: TpFIBTransaction;
    quGetSyncID: TFIBIntegerField;
    quGetSyncDOCTYPE: TFIBSmallIntField;
    quGetSyncIDSKL: TFIBIntegerField;
    quGetSyncIPERS: TFIBIntegerField;
    quGetSyncTIMEEDIT: TFIBDateTimeField;
    quGetSyncSTYPEEDIT: TFIBStringField;
    quGetSyncIDDOC: TFIBIntegerField;
    quSpecRSelBZQUANTR: TFIBFloatField;
    quSpecRSelBZQUANTF: TFIBFloatField;
    quSpecRSelBZQUANTS: TFIBFloatField;
    quDocsRIdBZTYPE: TFIBSmallIntField;
    quDocsRIdBZSTATUS: TFIBSmallIntField;
    quDocsRIdBZSUMR: TFIBFloatField;
    quDocsRIdBZSUMF: TFIBFloatField;
    quDocsRIdBZSUMS: TFIBFloatField;
    quDocsRSelIDHCB: TFIBIntegerField;
    quSpecRToCB: TpFIBDataSet;
    quSpecRToCBIDHEAD: TFIBIntegerField;
    quSpecRToCBID: TFIBIntegerField;
    quSpecRToCBIDCARD: TFIBIntegerField;
    quSpecRToCBQUANT: TFIBFloatField;
    quSpecRToCBIDM: TFIBIntegerField;
    quSpecRToCBKM: TFIBFloatField;
    quSpecRToCBPRICEIN: TFIBFloatField;
    quSpecRToCBSUMIN: TFIBFloatField;
    quSpecRToCBPRICER: TFIBFloatField;
    quSpecRToCBSUMR: TFIBFloatField;
    quSpecRToCBTCARD: TFIBSmallIntField;
    quSpecRToCBIDNDS: TFIBIntegerField;
    quSpecRToCBSUMNDS: TFIBFloatField;
    quSpecRToCBOPER: TFIBSmallIntField;
    quSpecRToCBPRICEIN0: TFIBFloatField;
    quSpecRToCBSUMIN0: TFIBFloatField;
    quSpecRToCBSUMOUT0: TFIBFloatField;
    quSpecRToCBSUMNDSOUT: TFIBFloatField;
    quSpecRToCBBZQUANTR: TFIBFloatField;
    quSpecRToCBBZQUANTF: TFIBFloatField;
    quSpecRToCBBZQUANTS: TFIBFloatField;
    quSpecRToCBCODEZAK: TFIBIntegerField;
    quSpecRToCBCATEGORY: TFIBIntegerField;
    quSpecRToCBPROC: TFIBFloatField;
    quAlgClass: TpFIBDataSet;
    dsquAlgClass: TDataSource;
    quAlgClassID: TFIBIntegerField;
    quAlgClassNAMECLA: TFIBStringField;
    quMakers: TpFIBDataSet;
    quMakersID: TFIBIntegerField;
    quMakersNAMEM: TFIBStringField;
    quMakersINNM: TFIBStringField;
    quMakersKPPM: TFIBStringField;
    dsquMakers: TDataSource;
    dsquAlgClass1: TDataSource;
    dsquMakers1: TDataSource;
    dsquCliLic: TDataSource;
    quCardsAlg: TpFIBDataSet;
    quCardsAlgID: TFIBIntegerField;
    quCardsAlgPARENT: TFIBIntegerField;
    quCardsAlgNAME: TFIBStringField;
    quCardsAlgTTYPE: TFIBIntegerField;
    quCardsAlgIMESSURE: TFIBIntegerField;
    quCardsAlgINDS: TFIBIntegerField;
    quCardsAlgMINREST: TFIBFloatField;
    quCardsAlgLASTPRICEIN: TFIBFloatField;
    quCardsAlgLASTPRICEOUT: TFIBFloatField;
    quCardsAlgLASTPOST: TFIBIntegerField;
    quCardsAlgIACTIVE: TFIBIntegerField;
    quCardsAlgTCARD: TFIBIntegerField;
    quCardsAlgCATEGORY: TFIBIntegerField;
    quCardsAlgSPISSTORE: TFIBStringField;
    quCardsAlgBB: TFIBFloatField;
    quCardsAlgGG: TFIBFloatField;
    quCardsAlgU1: TFIBFloatField;
    quCardsAlgU2: TFIBFloatField;
    quCardsAlgEE: TFIBFloatField;
    quCardsAlgRCATEGORY: TFIBIntegerField;
    quCardsAlgCOMMENT: TFIBStringField;
    quCardsAlgCTO: TFIBIntegerField;
    quCardsAlgCODEZAK: TFIBIntegerField;
    quCardsAlgALGCLASS: TFIBIntegerField;
    quCardsAlgALGMAKER: TFIBIntegerField;
    quCardsAlgNAMECLA: TFIBStringField;
    quCardsAlgNAMEM: TFIBStringField;
    quCardsAlgINNM: TFIBStringField;
    quCardsAlgKPPM: TFIBStringField;
    quInLnAlg: TpFIBDataSet;
    quInLnAlgIDHEAD: TFIBIntegerField;
    quInLnAlgID: TFIBIntegerField;
    quInLnAlgNUM: TFIBIntegerField;
    quInLnAlgIDCARD: TFIBIntegerField;
    quInLnAlgQUANT: TFIBFloatField;
    quInLnAlgPRICEIN: TFIBFloatField;
    quInLnAlgSUMIN: TFIBFloatField;
    quInLnAlgPRICEUCH: TFIBFloatField;
    quInLnAlgSUMUCH: TFIBFloatField;
    quInLnAlgIDNDS: TFIBIntegerField;
    quInLnAlgSUMNDS: TFIBFloatField;
    quInLnAlgIDM: TFIBIntegerField;
    quInLnAlgKM: TFIBFloatField;
    quInLnAlgPRICE0: TFIBFloatField;
    quInLnAlgSUM0: TFIBFloatField;
    quInLnAlgNDSPROC: TFIBFloatField;
    quInLnAlgDATEDOC: TFIBDateField;
    quInLnAlgNUMDOC: TFIBStringField;
    quInLnAlgIDCLI: TFIBIntegerField;
    quInLnAlgIDSKL: TFIBIntegerField;
    quInLnAlgIACTIVE: TFIBIntegerField;
    quInLnAlgFULLNAMECL: TFIBStringField;
    quInLnAlgINN: TFIBStringField;
    quInLnAlgKPP: TFIBStringField;
    quCliLicMax: TpFIBDataSet;
    quCliLicMaxICLI: TFIBIntegerField;
    quCliLicMaxIDATEB: TFIBIntegerField;
    quCliLicMaxDDATEB: TFIBDateField;
    quCliLicMaxIDATEE: TFIBIntegerField;
    quCliLicMaxDDATEE: TFIBDateField;
    quCliLicMaxSER: TFIBStringField;
    quCliLicMaxSNUM: TFIBStringField;
    quCliLicMaxORGAN: TFIBStringField;
    quCardsAlgVOL: TFIBFloatField;
    quDocsRCard1: TpFIBDataSet;
    quDocsRCard1IDCARD: TFIBIntegerField;
    quDocsRCard1NAME: TFIBStringField;
    quDocsRCard1IDCLI: TFIBIntegerField;
    quDocsRCard1IDSKL: TFIBIntegerField;
    quDocsRCard1MHTO: TFIBStringField;
    quDocsRCard1NAMECL: TFIBStringField;
    quDocsRCard1QUANT: TFIBFloatField;
    quDocsRCard1BZQUANTF: TFIBFloatField;
    quDocsRCard1GRNAME: TFIBStringField;
    quDocsRCard2: TpFIBDataSet;
    quDocsRCard2IDCARD: TFIBIntegerField;
    quDocsRCard2NAME: TFIBStringField;
    quDocsRCard2GRNAME: TFIBStringField;
    quDocsRCard2IDSKL: TFIBIntegerField;
    quDocsRCard2MHTO: TFIBStringField;
    quDocsRCard2QUANT: TFIBFloatField;
    quDocsRCard2BZQUANTF: TFIBFloatField;
    quDocsRCard2NAMESHORT: TFIBStringField;
    quDocsRCli: TpFIBDataSet;
    quDocsRCliIDCLI: TFIBIntegerField;
    quDocsRCliNAMECL: TFIBStringField;
    quDocsRCard2PARENT: TFIBIntegerField;
    quDocsRCliGr: TpFIBDataSet;
    quDocsRCliGrPARENT: TFIBIntegerField;
    quDocsRCliGrIDCLI: TFIBIntegerField;
    quDocsRCliGrBZQUANTF: TFIBFloatField;
    quTara: TpFIBDataSet;
    quTaraID: TFIBIntegerField;
    quTaraPARENT: TFIBIntegerField;
    quTaraNAME: TFIBStringField;
    quTaraTTYPE: TFIBIntegerField;
    quTaraIMESSURE: TFIBIntegerField;
    quTaraINDS: TFIBIntegerField;
    quTaraMINREST: TFIBFloatField;
    quTaraLASTPRICEIN: TFIBFloatField;
    quTaraLASTPRICEOUT: TFIBFloatField;
    quTaraLASTPOST: TFIBIntegerField;
    quTaraIACTIVE: TFIBIntegerField;
    quTaraTCARD: TFIBIntegerField;
    quTaraCATEGORY: TFIBIntegerField;
    quTaraSPISSTORE: TFIBStringField;
    quTaraBB: TFIBFloatField;
    quTaraGG: TFIBFloatField;
    quTaraU1: TFIBFloatField;
    quTaraU2: TFIBFloatField;
    quTaraEE: TFIBFloatField;
    quTaraRCATEGORY: TFIBIntegerField;
    quTaraCOMMENT: TFIBStringField;
    quTaraCTO: TFIBIntegerField;
    quTaraCODEZAK: TFIBIntegerField;
    quTaraALGCLASS: TFIBIntegerField;
    quTaraALGMAKER: TFIBIntegerField;
    quTaraVOL: TFIBFloatField;
    dsquTara: TDataSource;
    trSelTara: TpFIBTransaction;
    quSpecRToCBNAME: TFIBStringField;
    quCliLicMaxID: TFIBIntegerField;
    quCliLic: TpFIBDataSet;
    quCliLicID: TFIBIntegerField;
    quCliLicICLI: TFIBIntegerField;
    quCliLicIDATEB: TFIBIntegerField;
    quCliLicDDATEB: TFIBDateField;
    quCliLicIDATEE: TFIBIntegerField;
    quCliLicDDATEE: TFIBDateField;
    quCliLicSER: TFIBStringField;
    quCliLicSNUM: TFIBStringField;
    quCliLicORGAN: TFIBStringField;
    quAlcoDH: TpFIBDataSet;
    quAlcoDS1: TpFIBDataSet;
    quAlcoDS2: TpFIBDataSet;
    dsquAlcoDH: TDataSource;
    dsquAlcoDS1: TDataSource;
    dsquAlcoDS2: TDataSource;
    quAlcoDHIDORG: TFIBIntegerField;
    quAlcoDHID: TFIBIntegerField;
    quAlcoDHIDATEB: TFIBIntegerField;
    quAlcoDHIDATEE: TFIBIntegerField;
    quAlcoDHDDATEB: TFIBDateField;
    quAlcoDHDDATEE: TFIBDateField;
    quAlcoDHSPERS: TFIBStringField;
    quAlcoDHIACTIVE: TFIBSmallIntField;
    quAlcoDHIT1: TFIBSmallIntField;
    quAlcoDHIT2: TFIBSmallIntField;
    quAlcoDHINUMCORR: TFIBSmallIntField;
    quAlcoDHNAME: TFIBStringField;
    quAlcoDHINN: TFIBStringField;
    quAlcoDS1IDH: TFIBIntegerField;
    quAlcoDS1ID: TFIBIntegerField;
    quAlcoDS1IDEP: TFIBIntegerField;
    quAlcoDS1INUM: TFIBIntegerField;
    quAlcoDS1IVID: TFIBIntegerField;
    quAlcoDS1IPROD: TFIBIntegerField;
    quAlcoDS1RQB: TFIBFloatField;
    quAlcoDS1RQIN1: TFIBFloatField;
    quAlcoDS1RQIN2: TFIBFloatField;
    quAlcoDS1RQIN3: TFIBFloatField;
    quAlcoDS1RQINIT1: TFIBFloatField;
    quAlcoDS1RQIN4: TFIBFloatField;
    quAlcoDS1RQIN5: TFIBFloatField;
    quAlcoDS1RQIN6: TFIBFloatField;
    quAlcoDS1RQINIT: TFIBFloatField;
    quAlcoDS1RQOUT1: TFIBFloatField;
    quAlcoDS1RQOUT2: TFIBFloatField;
    quAlcoDS1RQOUT3: TFIBFloatField;
    quAlcoDS1RQOUT4: TFIBFloatField;
    quAlcoDS1RQOUTIT: TFIBFloatField;
    quAlcoDS1RQE: TFIBFloatField;
    quAlcoDS1NAMESHOP: TFIBStringField;
    quAlcoDS1NAMEVID: TFIBStringField;
    quAlcoDS1NAMEPRODUCER: TFIBStringField;
    quAlcoDS1INN: TFIBStringField;
    quAlcoDS1KPP: TFIBStringField;
    quAlcoDS2IDH: TFIBIntegerField;
    quAlcoDS2ID: TFIBIntegerField;
    quAlcoDS2IDEP: TFIBIntegerField;
    quAlcoDS2IVID: TFIBIntegerField;
    quAlcoDS2IPROD: TFIBIntegerField;
    quAlcoDS2IPOST: TFIBIntegerField;
    quAlcoDS2ILIC: TFIBIntegerField;
    quAlcoDS2DOCDATE: TFIBDateField;
    quAlcoDS2DOCNUM: TFIBStringField;
    quAlcoDS2GTD: TFIBStringField;
    quAlcoDS2RQIN: TFIBFloatField;
    quAlcoDS2NAMESHOP: TFIBStringField;
    quAlcoDS2NAMEPRODUCER: TFIBStringField;
    quAlcoDS2INN: TFIBStringField;
    quAlcoDS2KPP: TFIBStringField;
    quAlcoDS2NAMECLI: TFIBStringField;
    quAlcoDS2INNCLI: TFIBStringField;
    quAlcoDS2KPPCLI: TFIBStringField;
    quAlcoDS2SER: TFIBStringField;
    quAlcoDS2SNUM: TFIBStringField;
    quAlcoDS2DDATEB: TFIBDateField;
    quAlcoDS2DDATEE: TFIBDateField;
    quAlcoDS2ORGAN: TFIBStringField;
    quAlcoDS2NAMEVID: TFIBStringField;
    quDelDS1: TpFIBQuery;
    quDelDS2: TpFIBQuery;
    quFindDH: TpFIBDataSet;
    quFindDHIDORG: TFIBIntegerField;
    quFindDHID: TFIBIntegerField;
    quFindDHIDATEB: TFIBIntegerField;
    quFindDHIDATEE: TFIBIntegerField;
    quFindDHDDATEB: TFIBDateField;
    quFindDHDDATEE: TFIBDateField;
    quFindDHSPERS: TFIBStringField;
    quFindDHIACTIVE: TFIBSmallIntField;
    quFindDHIT1: TFIBSmallIntField;
    quFindDHIT2: TFIBSmallIntField;
    quFindDHINUMCORR: TFIBSmallIntField;
    quAlcoDS2IDDH: TFIBIntegerField;
    procedure taSpecBDSUMChange(Sender: TField);
    procedure taSpecBPRICERChange(Sender: TField);
    procedure taSpecBRSUMChange(Sender: TField);
    procedure quDocsRCardCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
function fRecalcSumDoc(Idh,dType,ResT:Integer):Real;
procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch,PriceIn0:Real);
procedure prFind2group(IdCl:Integer;Var S1,S2:String);
function prFindMassaTC(IdCard:Integer):String;
function FindTSpis(sOp:String):Integer;
procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
procedure prFormQuDocsR;
function prGetOrg(iOrg,iSh:INteger):TOrg;

var
  dmORep: TdmORep;


implementation

uses dmOffice, DOBSpec, TOSel;

{$R *.dfm}


function prGetOrg(iOrg,iSh:INteger):TOrg;
Var rOrg:TOrg;
begin
  with dmORep do
  begin
    rOrg.Name:='';
    rOrg.FullName:='';
    rOrg.iShop:=iSh;
    rOrg.sAdr:='';
    rOrg.iDep:=0;
    rOrg.IdOrg:=iOrg;
    rOrg.Inn:='';
    rOrg.KPP:='';
    rOrg.sMail:='';
    rOrg.sTel:='';
    rOrg.CodeCountry:='';
    rOrg.CodeReg:='';
    rOrg.Sity:='';
    rOrg.Street:='';
    rOrg.House:='';
    rOrg.Korp:='';
    rOrg.Dir1:='';
    rOrg.Dir2:='';
    rOrg.Dir3:='';
    rOrg.gb1:='';
    rOrg.gb2:='';
    rOrg.gb3:='';
    rOrg.LicVid:='';
    rOrg.LicSer:='';
    rOrg.LicNum:='';
    rOrg.dDateB:=date;
    rOrg.dDateE:=date;
    rOrg.ip:=0;
    {
    quAcloOrg.Active:=False;
    quAcloOrg.Parameters.ParamByName('IORG').Value:=iOrg;
    quAcloOrg.Parameters.ParamByName('ISHOP').Value:=iSh;
    quAcloOrg.Active:=True;
    if quAcloOrg.RecordCount>0 then
    begin
      quAcloOrg.First;

      rOrg.Name:=quAcloOrgName.AsString;
      rOrg.FullName:=quAcloOrgFullName.AsString;
      rOrg.iShop:=iSh;
      rOrg.sAdr:=quAcloOrgAddres.AsString;
      rOrg.iDep:=quAcloOrgId.AsInteger;
      rOrg.IdOrg:=iOrg;
      rOrg.Inn:=quAcloOrgINN.AsString;
      rOrg.KPP:=quAcloOrgKPP.AsString;
      rOrg.sMail:=quAcloOrgmail.AsString;
      rOrg.sTel:=quAcloOrgTelefon.AsString;
      rOrg.CodeCountry:=quAcloOrgadr_code_country.AsString;
      rOrg.CodeReg:=quAcloOrgadr_code_reg.AsString;
      rOrg.Sity:=quAcloOrgadr_city.AsString;
      rOrg.Street:=quAcloOrgadr_street.AsString;
      rOrg.House:=quAcloOrgadr_house.AsString;
      rOrg.Korp:=quAcloOrgadr_housing.AsString;
      rOrg.Dir1:=quAcloOrgchief_fam.AsString;
      rOrg.Dir2:=quAcloOrgchief_name.AsString;
      rOrg.Dir3:=quAcloOrgchief_otch.AsString;
      rOrg.gb1:=quAcloOrgbuh_fam.AsString;
      rOrg.gb2:=quAcloOrgbuh_name.AsString;
      rOrg.gb3:=quAcloOrgbuh_otch.AsString;
      rOrg.LicVid:=quAcloOrglicense_vid.AsString;
      rOrg.LicSer:=quAcloOrglicense_series.AsString;
      rOrg.LicNum:=quAcloOrglicense_number.AsString;
      rOrg.dDateB:=quAcloOrglicdb.AsDateTime;
      rOrg.dDateE:=quAcloOrglicde.AsDateTime;
      rOrg.ip:=quAcloOrgip.AsInteger;
    end;
    quAcloOrg.Active:=False;}

    Result:=rOrg;
  end;
end;


procedure prFormQuDocsR;
begin
  with dmORep do
  begin
    quDocsRSel.Active:=False;
    quDocsRSel.SelectSQL.Clear;
    quDocsRSel.SelectSQL.Add('');

    quDocsRSel.SelectSQL.Add('SELECT dh.ID,dh.IDHCB,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,dh.SUMIN,');
    quDocsRSel.SelectSQL.Add('       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.PROCNAC,dh.IACTIVE,');
    quDocsRSel.SelectSQL.Add('       dh.BZTYPE,dh.BZSTATUS,dh.BZSUMR,dh.BZSUMF,dh.BZSUMS,dh.IEDIT,dh.PERSEDIT,dh.IPERSEDIT,');
    quDocsRSel.SelectSQL.Add('       cl.NAMECL,mh.NAMEMH,dh.IDFROM,cl1.NAMECL as NAMECLFROM');
    quDocsRSel.SelectSQL.Add('FROM OF_DOCHEADOUTR dh');
    quDocsRSel.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=dh.IDCLI');
    quDocsRSel.SelectSQL.Add('left join OF_CLIENTS cl1 on cl1.ID=dh.IDFROM');
    quDocsRSel.SelectSQL.Add('left join OF_MH mh on mh.ID=dh.IDSKL');

    quDocsRSel.SelectSQL.Add('Where dh.DATEDOC>='''+ds1(CommonSet.DateFrom)+''' and dh.DATEDOC<='''+ds1(CommonSet.DateTo)+'''');
    quDocsRSel.SelectSQL.Add('and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON='+its(Person.Id)+')');
    if Person.sCli>'' then
    begin
      quDocsRSel.SelectSQL.Add('and dh.IDCLI in ('+Person.sCli+')');
    end;

    quDocsRSel.Active:=True;
  end;
end;

procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
begin
  with dmORep do
  begin
    quSetSync.SQL.Clear;
    if sDocT='DocR' then
    begin
      quSetSync.SQL.Add('insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)');
      quSetSync.SQL.Add('values (8,'+its(ISKL)+','+its(Person.Id)+','''+formatdatetime('dd.mm.yyyy hh:nn',now)+''','''+sOp+''','+its(IDH)+')');

//insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)
//values (4,5,6,'23.07.2012 18:00','OPN',10000)

    end;
    quSetSync.ExecQuery;
  end;
end;

function FindTSpis(sOp:String):Integer;
begin
  Result:=1;
  with dmORep do
  begin
    quFindTSpis.Active:=False;
    quFindTSpis.ParamByName('ABR').AsString:=sOp;
    quFindTSpis.Active:=True;

    if quFindTSpis.RecordCount>0 then Result:=quFindTSpisTSPIS.AsInteger;

    quFindTSpis.Active:=False;
  end;
end;


function prFindMassaTC(IdCard:Integer):String;
begin
  Result:='';
  with dmORep do
  begin
    quFindMassa.Active:=False;
    quFindMassa.ParamByName('IDCARD').AsInteger:=IdCard;
    quFindMassa.Active:=True;

    if quFindMassa.RecordCount>0 then Result:=quFindMassaPOUTPUT.AsString;

    quFindMassa.Active:=False;
  end;
end;

procedure prFind2group(IdCl:Integer;Var S1,S2:String);
Var Id:INteger;
begin
  with dmORep do
  begin
    S1:=''; S2:=''; Id:=IdCl;
    while iD<>0 do
    begin
      quFindCl.Active:=False;
      quFindCl.ParamByName('IDCL').AsInteger:=Id;
      quFindCl.Active:=True;
      S2:=S1;
      S1:=Copy(quFindClNAMECL.AsString,1,50);
      Id:=quFindClID_PARENT.AsInteger;
      quFindCl.Active:=False;
    end;
  end;
end;  

procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch,PriceIn0:Real);
begin
  with dmORep do
  begin
//    EXECUTE PROCEDURE PR_CALCLASTPRICE2 (?IDGOOD, ?IDSKL, ?IDATE)
    pr_CalcLastPrice.ParamByName('IDGOOD').AsInteger:=IdCard;
    pr_CalcLastPrice.ParamByName('IDSKL').AsInteger:=IdSkl;
    pr_CalcLastPrice.ParamByName('IDATE').AsInteger:=IDate;
    pr_CalcLastPrice.ExecProc;
    PriceIn0:=pr_CalcLastPrice.ParamByName('PRICEIN0').AsFloat;
    PriceIn:=pr_CalcLastPrice.ParamByName('PRICEIN').AsFloat;
    PriceUch:=pr_CalcLastPrice.ParamByName('PRICEUCH').AsFloat;

    //���� � ������� ��
  end;
end;

function fRecalcSumDoc(Idh,dType,ResT:Integer):Real;
Var rQ,rSum:Real;
begin
  Result:=0;
  with dmORep do
  with dmO do
  begin
    if ResT=2 then
    begin //����������� ������ � ����������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
    if ResT=1 then //����������� �� ����������� ����� �� �������������
    begin
      if dType=1 then //������
      begin //������ �� ����� �.�. ��� ������������ ������������ � ��������� �������
            { ����� ���� ��������� ���������� ������� � ���������� ���-��� � �� ����� ����
            , � ��� ��� ��������� ������ �� ������ ������� ��� �� � ���������� ����������� ������.
            ���� ���� ������� � 0 � ���-��, � -900 � �����, � 0=KM
            ����� �������� ������ �������� �������� � �����������
            }
      end;
      if dType=2 then //����������
      begin
        taDobSpec1.Active:=False;
        taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
        taDobSpec1.Active:=True;
        taDobSpec1.First;
        while not taDobSpec1.Eof do
        begin
          quCardPO.Active:=False;
          quCardPO.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
          quCardPO.ParamByName('IDH').AsInteger:=IDH;
          quCardPO.ParamByName('DT').AsInteger:=dType;
          quCardPO.Active:=True;
          if quCardPO.RecordCount>0 then
          begin
            rQ:=quCardPOQUANT.AsFloat;
            rSum:=quCardPORSUM.AsFloat;

            if abs(rQ-taDobSpec1KM.AsFloat*taDobSpec1QUANT.AsFloat)<0.001 then
            begin
              //���-�� ��������� - ������������ �����
              if abs(rSum-taDobSpec1SUMIN.AsFloat)>0.01 then
              begin
                prWH('       ���. ����� - ���: '+IntToStr(taDobSpec1ARTICUL.AsInteger),fmTO.Memo1);
                prWH('          c���� �� - '+FloatToStr(taDobSpec1SUMIN.AsFloat),fmTO.Memo1);
                prWH('          c���� ����� - '+FloatToStr(rSum),fmTO.Memo1);

                taDobSpec1.Edit;
                taDobSpec1SUMIN.AsFloat:=rSum;
                taDobSpec1.Post;
                delay(20);
                
              end;
            end else prWH('       ����������� ���-�� ����. ������ - '+IntToStr(taDobSpec1ARTICUL.AsInteger),fmTO.Memo1);
          end else prWH('       ����������� ����. ������ - '+IntToStr(taDobSpec1ARTICUL.AsInteger),fmTO.Memo1);

          quCardPO.Active:=False;

          taDobSpec1.Next;
        end;
        taDobSpec1.Active:=False;
      end;
      if dType=4 then //�����
      begin

      end;
      if dType=5 then //��� �����������
      begin

      end;
      if dType=6 then //������������
      begin

      end;
      if dType=7 then //�������
      begin
  {      quSpecOutSel.Active:=False;
        quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecOutSel.Active:=True;

        quSpecOutSel.First;
        while not quSpecOutSel.Eof do
        begin



          quSpecOutSel.Next;
        end;
        quSpecOutSel.Active:=False;
        }
      end;


      //��� ���� �� ������ ������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
  end;
end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
begin
  with dmORep do
  begin
    prTestSpecPart.ParamByName('IDDOC').AsInteger:=IDH;
    prTestSpecPart.ParamByName('DTYPE').AsInteger:=dType;
    prTestSpecPart.ParamByName('DSUM').AsFloat:=rSum;
    prTestSpecPart.ExecProc;
    Result:=prTestSpecPart.ParamByName('RESULT').AsInteger;
  end;
end;


procedure TdmORep.taSpecBDSUMChange(Sender: TField);
begin
  //���������� �����
  with fmDOBSpec do
  begin
    if iCol=1 then
    begin
      taSpecBRSUM.AsFloat:=taSpecBPRICER.AsFloat*taSpecBQUANT.AsFloat;
    end;
  end;
end;

procedure TdmORep.taSpecBPRICERChange(Sender: TField);
begin
  //���������� ����
  with fmDOBSpec do
  begin
    if iCol=2 then
    begin
      taSpecBRSUM.AsFloat:=taSpecBPRICER.AsFloat*taSpecBQUANT.AsFloat;
    end;
  end;
end;

procedure TdmORep.taSpecBRSUMChange(Sender: TField);
begin
  //���������� �����
  with fmDOBSpec do
  begin
    if iCol=3 then
    begin
      if taSpecBQUANT.AsFloat<>0 then
        taSpecBPRICER.AsFloat:=taSpecBRSUM.AsFloat/taSpecBQUANT.AsFloat;
    end;
  end;

end;

procedure TdmORep.quDocsRCardCalcFields(DataSet: TDataSet);
begin
  quDocsRCardRNac.AsFloat:=quDocsRCardSUMR.AsFloat-quDocsRCardSUMIN.AsFloat;
  if quDocsRCardQUANT.AsFloat>=0 then quDocsRCardOPER.AsInteger:=1 else quDocsRCardOPER.AsInteger:=2;
end;

end.



