unit mCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, RXSplit, ExtCtrls, Placemnt, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan,
  Menus, cxContainer, cxTextEdit, cxMaskEdit, cxDataStorage,
  cxImageComboBox, cxCheckBox, ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  cxProgressBar,FR_Class, cxDBLookupComboBox, dxmdaset;

type
  TfmCards = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    RxSplitter1: TRxSplitter;
    Panel3: TPanel;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    CardsTree: TTreeView;
    SpeedBar1: TSpeedBar;
    acCard: TActionManager;
    acExit: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acAdd: TAction;
    acEdit: TAction;
    acView: TAction;
    acDel: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    TextEdit1: TcxTextEdit;
    acFindBar: TAction;
    cxButton1: TcxButton;
    CardsView: TcxGridDBTableView;
    CardLevel: TcxGridLevel;
    CardGr: TcxGrid;
    acBarView: TAction;
    SpeedItem6: TSpeedItem;
    acJurnal: TAction;
    SpeedItem7: TSpeedItem;
    N5: TMenuItem;
    N6: TMenuItem;
    acMove: TAction;
    Label1: TLabel;
    acNotUse: TAction;
    SpeedItem8: TSpeedItem;
    PopupMenu2: TPopupMenu;
    N7: TMenuItem;
    acMove1: TAction;
    SpeedItem9: TSpeedItem;
    Timer2: TTimer;
    N8: TMenuItem;
    N9: TMenuItem;
    acTransGr: TAction;
    acRemns2: TAction;
    SpeedItem10: TSpeedItem;
    acCardUse: TAction;
    acFilials: TAction;
    acTransList: TAction;
    acExcel: TAction;
    SpeedItem11: TSpeedItem;
    acManCat: TAction;
    acManCat1: TAction;
    N10: TMenuItem;
    N11: TMenuItem;
    acTPrint: TAction;
    CardsViewID: TcxGridDBColumn;
    CardsViewName: TcxGridDBColumn;
    CardsViewBarCode: TcxGridDBColumn;
    CardsViewShRealize: TcxGridDBColumn;
    CardsViewTovarType: TcxGridDBColumn;
    CardsViewEdIzm: TcxGridDBColumn;
    CardsViewNDS: TcxGridDBColumn;
    CardsViewOKDP: TcxGridDBColumn;
    CardsViewWeght: TcxGridDBColumn;
    CardsViewSrokReal: TcxGridDBColumn;
    CardsViewTareWeight: TcxGridDBColumn;
    CardsViewKritOst: TcxGridDBColumn;
    CardsViewBHTStatus: TcxGridDBColumn;
    CardsViewUKMAction: TcxGridDBColumn;
    CardsViewDataChange: TcxGridDBColumn;
    CardsViewNSP: TcxGridDBColumn;
    CardsViewWasteRate: TcxGridDBColumn;
    CardsViewCena: TcxGridDBColumn;
    CardsViewFullName: TcxGridDBColumn;
    CardsViewStatus: TcxGridDBColumn;
    CardsViewPrsision: TcxGridDBColumn;
    CardsViewMargGroup: TcxGridDBColumn;
    CardsViewNameCountry: TcxGridDBColumn;
    CardsViewNAMEBRAND: TcxGridDBColumn;
    cxCheckBox1: TcxCheckBox;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    acFindName: TAction;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    acCashLoad: TAction;
    acPrintCen: TAction;
    N17: TMenuItem;
    CardsViewReserv1: TcxGridDBColumn;
    acCalcRemns: TAction;
    acCardsGoods: TAction;
    acPostTov: TAction;
    Vi1: TcxGridDBTableView;
    le1: TcxGridLevel;
    Gr1: TcxGrid;
    Vi1Name: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Le2: TcxGridLevel;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    acAddPrint: TAction;
    acPrintQuPrint: TAction;
    CardsViewV02: TcxGridDBColumn;
    acTermFile: TAction;
    N12: TMenuItem;
    ViCennQuant: TcxGridDBColumn;
    cxButton7: TcxButton;
    N13: TMenuItem;
    acScaleIn: TAction;
    N14: TMenuItem;
    acAddInScale: TAction;
    acAddInScale1: TMenuItem;
    acRecalcRemn1: TAction;
    acLoadScale: TAction;
    N15: TMenuItem;
    N16: TMenuItem;
    acReceipt: TAction;
    N18: TMenuItem;
    cxButton8: TcxButton;
    ViCennQuanrR: TcxGridDBColumn;
    acSelectAll: TAction;
    CardsViewReserv2: TcxGridDBColumn;
    CardsViewFixPrice: TcxGridDBColumn;
    acViewLoad: TAction;
    N19: TMenuItem;
    acCalcMoveLine: TAction;
    CardsViewV04: TcxGridDBColumn;
    CardsViewV05: TcxGridDBColumn;
    acPartSel: TAction;
    acSearchCode: TAction;
    CardsViewKol: TcxGridDBColumn;
    CardsViewV06: TcxGridDBColumn;
    CardsViewV07: TcxGridDBColumn;
    N20: TMenuItem;
    acSetGroup: TAction;
    teBuff: TdxMemData;
    teBuffCode: TIntegerField;
    N21: TMenuItem;
    acGetVReal: TAction;
    CardsViewV08: TcxGridDBColumn;
    CardsViewVol: TcxGridDBColumn;
    CardsViewV11: TcxGridDBColumn;
    CardsViewNAMEM: TcxGridDBColumn;
    acLoadSDisc: TAction;
    N22: TMenuItem;
    acPrintCutTail: TAction;
    N23: TMenuItem;
    acSaveAlco: TAction;
    CardsViewTypeV: TcxGridDBColumn;
    CardsViewRemn: TcxGridDBColumn;
    cxCheckBox2: TcxCheckBox;
    acAddList: TAction;
    cxButton9: TcxButton;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure CardsTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure acFindBarExecute(Sender: TObject);
    procedure CardsViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acBarViewExecute(Sender: TObject);
    procedure acJurnalExecute(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure TextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acMove1Execute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure acTransGrExecute(Sender: TObject);
    procedure acRemns2Execute(Sender: TObject);
    procedure acTransListExecute(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
    procedure TextEdit1Click(Sender: TObject);
    procedure acFindNameExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure acCashLoadExecute(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acCalcRemnsExecute(Sender: TObject);
    procedure acCardsGoodsExecute(Sender: TObject);
    procedure acPostTovExecute(Sender: TObject);
    procedure CardsViewSelectionChanged(Sender: TcxCustomGridTableView);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure acAddPrintExecute(Sender: TObject);
    procedure acPrintQuPrintExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acTermFileExecute(Sender: TObject);
    procedure acTPrintExecute(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acScaleInExecute(Sender: TObject);
    procedure acAddInScaleExecute(Sender: TObject);
    procedure acRecalcRemn1Execute(Sender: TObject);
    procedure acLoadScaleExecute(Sender: TObject);
    procedure acReceiptExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acSelectAllExecute(Sender: TObject);
    procedure acViewLoadExecute(Sender: TObject);
    procedure CardsViewStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure CardsTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure CardsTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acCalcMoveLineExecute(Sender: TObject);
    procedure acPartSelExecute(Sender: TObject);
    procedure acSearchCodeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acSetGroupExecute(Sender: TObject);
    procedure acGetVRealExecute(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acLoadSDiscExecute(Sender: TObject);
    procedure acPrintCutTailExecute(Sender: TObject);
    procedure acSaveAlcoExecute(Sender: TObject);
    procedure cxCheckBox2Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure CardsViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton9Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;
procedure typevoz(icli:integer);
procedure typenevoz(icli:integer);
Procedure SetGoodsDef(iPar:Integer);  //0-����������  +-��������������  - ��������;
Procedure SetGoodsVal(iPar:Integer);  //0-����������  +-��������������  - ��������
Function ToStandart(S:String):String;

type
   TCardRec = record
   Name,NameF,Bar:String;
   Price:Real;
   end;


var
  fmCards: TfmCards;
  bClear1:Boolean;
  CardRec:TCardRec;
  bChGroup:Boolean=False;
  onEnterCard:Boolean=false;

implementation

uses Un1, MDB, mAddCard, mFind, u2fdk, MainMCryst, AddClassif, Bar,
  AddDoc1, AddDoc2, AddDoc3, AddDoc4, BufPrice, Status, PXDB, Remn, Move,
  CG, AddDoc5, QuantMess, AddInScale, MT, Receipt, BarcodeNo, GdsLoad, MFB,
  PartSel, AddDoc6, Parts, SetCardsParams, AssortPost, AddDoc7, AvSpeed,
  AddDoc8, dmPS, PostGood, InvVoz;

{$R *.dfm}
procedure typevoz(icli:integer);
  var  i:integer;
begin
  with dmP do
  begin
    quDateVoz.Active:=False;
    quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
    quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
    quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
    quDateVoz.ParamByName('IDCli').AsInteger:=icli;
    quDateVoz.Active:=True;
                                                         //��������� ���� �� ������ ��������
    if quDateVoz.RecordCount<1 then
    begin
      if MessageDlg('� ���������� ���������� ��� ������� ���������, ��������� �� ������� ����?',mtConfirmation,[mbYes, mbNo], 0)=mrYes
      then predztoday(1,0,icli)
      else
      begin                                          //�������� ���� �� ������ ���������� ��� ��������
       // icli:=quPostVozidcli.AsInteger;
        i:=0;
        CommonSet.DateBeg:=Date-730;
        CommonSet.DateEnd:=Date;
        fmP.VP.BeginUpdate;
        quPost2.Active:=False;
        quPost2.ParamByName('IDCARD').AsInteger:=quPostVozCode.AsInteger;
        quPost2.ParamByName('DDATEB').AsDate:=trunc(Date-730);
        quPost2.ParamByName('DDATEE').AsDate:=trunc(Date);
        quPost2.Active:=True;
        fmP.VP.EndUpdate;
        if quPost2.RecordCount<1 then exit       //���� ������ ���
        else
        begin
          while not quPost2.Eof do
          begin
            if quPost2idcli.AsInteger<>icli then
            begin
              i:=1;                //���� ������ ����������
            end;
            quPost2.Next;
          end;

          if i=1 then                 // ���� ����
          begin
            if MessageDlg('�������� ����� ������ ����������� ��� ��������?',mtConfirmation,[mbYes, mbNo], 0)=mrYes then
            begin
              fmP.Caption:=quPost2NameCode.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
              bDrVoz:=false;
              fmP.Show;
            end else exit;
          end
          else exit;   //��� ������ ����������� ��� ��������
        end;
      end;
    end
    else  predzgrafvoz(1,0,icli);
  end;
end;

procedure typenevoz(icli:integer);
begin
  with dmP do
  begin
    CommonSet.DateBeg:=Date-730;
    CommonSet.DateEnd:=Date;
    fmP.VP.BeginUpdate;               //���� ���� �� ������ ���������� ����������
    quPost2.Active:=False;
    quPost2.ParamByName('IDCARD').AsInteger:=quPostVozCode.AsInteger;
    quPost2.ParamByName('DDATEB').AsDate:=trunc(Date-730);
    quPost2.ParamByName('DDATEE').AsDate:=trunc(Date);
    quPost2.Active:=True;
    fmP.VP.EndUpdate;
    if quPost2.RecordCount<1 then                //���� ���
    begin

      if MessageDlg('��� �����������,������� ����� ������� �����. ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=mrYes
      then  predztoday(2,0,icli)
      else  exit;
    end
    else            //���� ���� �� ���������� ������� ����� � ���������� ������������
    begin
      showmessage('��������� ��������� �� ����� ������� ��������, '+chr(13)+' �������� ���������� ��� ��������');
      fmP.Caption:=quPost2NameCode.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      bDrVoz:=false;
      fmP.Show;
    end;
  end;
end;

Function ToStandart(S:String):String;
var StrWk,Str1:String;
    iBar: array[1..13] of integer;
    n,c,n1:Integer;
begin
  StrWk:=S;
  while Length(StrWk)<12 do StrWk:=StrWk+'0';
  for n:=1 to 12 do
  begin
    str1:=Copy(StrWk,n,1);
    iBar[n]:=StrToIntDef(Str1,0);
  end;
  //220123401000C
  c:=0;
  n:=(iBar[2]+iBar[4]+iBar[6]+iBar[8]+iBar[10]+iBar[12])*3+(iBar[1]+iBar[3]+iBar[5]+iBar[7]+iBar[9]+iBar[11]);
  for n1:=0 to 9 do
  begin
    if ((n+n1) mod 10)=0 then
    begin
      c:=n1;
      break;
    end;
  end;
  iBar[13]:=c;
  Str1:='';
  for n:=1 to 13 do str1:=Str1+IntToStr(iBar[n]);

  strwk:=Str1;
  Result:=StrWk;
end;

Procedure SetGoodsVal(iPar:Integer);  //0-����������  +-��������������  - ��������
Var iItem,iC:Integer;
    sBar,StrWk:String;
begin
  with fmAddCard do
  begin

    with dmMC do
    begin
      bCreate:=True;
      if quEU.Active=False then quEU.Open;
      if quBrands.Active=False then quBrands.Open;
      if quCateg.Active=False then quCateg.Open;
      if quAlgClass.Active=False then quAlgClass.Open;


      if iPar=0 then cxButton4.Visible:=True;
      if iPar>0 then cxButton4.Visible:=False;
      if iPar<0 then cxButton4.Visible:=False;

      LookUpComboBox1.EditValue:=32000;
      LookUpComboBox3.EditValue:=1;
      LookUpComboBox4.EditValue:=1;
      cxLookUpComboBox1.EditValue:=0;
      cxLookUpComboBox2.EditValue:=0;

      iItem:=0;

      cxCalcEdit3.EditValue:=0;
      cxSpinEdit1.Value:=0;
      cxSpinEdit2.Value:=0;

      if quCards.RecordCount>0 then
      begin
        RadioGroup1.ItemIndex:=quCardsTovarType.AsInteger;
        RadioGroup2.ItemIndex:=quCardsShRealize.AsInteger;
        TextEdit1.Text:=quCardsName.AsString;
        TextEdit2.Text:=quCardsFullName.AsString;
        ButtonEdit1.Tag:=quCardsCountry.AsInteger;
        ButtonEdit1.Text:=quCardsNameCountry.AsString;

        ButtonEdit2.Tag:=quCardsGoodsGroupID.AsInteger;
//        ButtonEdit2.Text:=prFindNameGr(quCardsGoodsGroupID.AsInteger,iItem);
        ButtonEdit2.Text:=prFindTNameGroup(quCardsGoodsGroupID.AsInteger);

        CalcEdit1.EditValue:=quCardsCena.AsFloat;
        CalcEdit2.EditValue:=quCardsNDS.AsFloat;
        CalcEdit3.EditValue:=quCardsPrsision.AsFloat;
        CalcEdit4.EditValue:=quCardsSrokReal.AsInteger;
        CalcEdit5.EditValue:=quCardsWasteRate.AsFloat;
        CalcEdit6.EditValue:=quCardsKritOst.AsFloat;
        cxCalcEdit1.EditValue:=quCardsReserv2.AsFloat;
        cxCalcEdit2.EditValue:=quCardsFixPrice.AsFloat;

        cxCalcEdit4.EditValue:=quCardsVol.AsFloat;

        LookUpComboBox1.EditValue:=prFindEU(quCardsID.AsInteger,StrWk);
//        LookUpComboBox2.EditValue:=quCardsManCat.AsInteger; // ��� ����
        LookUpComboBox3.EditValue:=quCardsV02.AsInteger; //V02 ���������
        LookUpComboBox4.EditValue:=quCardsV01.AsInteger; //V01  �����

        cxLookUpComboBox1.EditValue:=quCardsV12.AsInteger; //�������������
        cxLookUpComboBox2.EditValue:=quCardsV11.AsInteger; //��� ���������

        cxCalcEdit3.EditValue:=quCardsKol.AsFloat;
        cxSpinEdit1.Value:=quCardsV06.AsInteger;
        cxSpinEdit2.Value:=quCardsV07.AsInteger;
        cxSpinEdit3.Value:=quCardsV08.AsInteger;
      end;

      if MaskEdit1.Tag=0 then
      begin  //���������� ������� � ��

        //��������� ��� ��
        if RadioGroup1.ItemIndex=0 then
        begin //������� �����
          iItem:=prFindNewArt(False,StrWk);
          //������ ��������
          if RadioGroup2.ItemIndex=0 then sBar:='';
          if RadioGroup2.ItemIndex=1 then
          begin
            sBar:=IntToStr(iItem);
            while Length(sBar)<5 do sBar:='0'+sBar;
            sBar:=CommonSet.Prefix+sBar;
            sBar:=ToStandart(sBar);
          end;
          if RadioGroup2.ItemIndex=2 then
          begin
            if not prFindBar(IntToStr(iItem),iItem,iC) then sBar:=IntToStr(iItem)
            else sBar:='';
          end;
        end
        else
        begin //������� �����
          iItem:=prFindNewArt(True,StrWk);
        //������ ��������

          StrWk:=IntToStr(iItem);
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
          sBar:=CommonSet.PrefixVes+StrWk;
          sBar:=ToStandart(sBar);  //�������� � 7 ������
        end;
      end;

      //��������������  � ��������
      if MaskEdit1.Tag<>0 then
      begin
        iItem:=quCardsID.AsInteger;
        sBar:=quCardsBarCode.AsString;
      end;

      MaskEdit1.Text:=IntToStr(iItem);
      MaskEdit2.Text:=sBar;

      bCreate:=False;
    end;
  end;
end;

Procedure SetGoodsDef(iPar:Integer);  //0-����������  +-��������������  - ��������
begin
  with fmAddCard do
  begin
    bCreate:=True;  //���������� �� �������� ������������ �� (���� ��� ���������)

    RadioGroup1.ItemIndex:=0;
    RadioGroup2.ItemIndex:=0;
    MaskEdit1.Text:='';
    MaskEdit1.Tag:=iPar; //������� ����������� ��������� ������ �����
    MaskEdit2.Text:='';
    TextEdit1.Text:='';
    TextEdit2.Text:='';
    ButtonEdit1.Tag:=0;
    ButtonEdit1.Text:='';
    ButtonEdit2.Tag:=0;
    ButtonEdit2.Text:='';
    CalcEdit1.EditValue:=0;
    CalcEdit2.EditValue:=0;
    CalcEdit3.EditValue:=1;
    CalcEdit4.EditValue:=0;
    CalcEdit5.EditValue:=0;
    CalcEdit6.EditValue:=0;
    cxCalcEdit1.EditValue:=0;
    cxCalcEdit2.EditValue:=0;
    cxCalcEdit3.EditValue:=0;
    LookUpComboBox1.EditValue:=0;
    LookUpComboBox3.EditValue:=0;
    LookUpComboBox4.EditValue:=0;

    cxLookUpComboBox1.EditValue:=0;
    cxLookUpComboBox2.EditValue:=0;

    cxSpinEdit1.Value:=0;
    cxSpinEdit2.Value:=0;
    cxSpinEdit3.Value:=0;

    cxCalcEdit4.Value:=0;   //�����

    if iPar=0 then
    begin
      RadioGroup1.Properties.ReadOnly:=False;
      RadioGroup2.Properties.ReadOnly:=False;
      MaskEdit1.Properties.ReadOnly:=False;
      MaskEdit2.Properties.ReadOnly:=False;
      TextEdit1.Properties.ReadOnly:=False;
      TextEdit2.Properties.ReadOnly:=False;

      ButtonEdit1.Properties.ReadOnly:=False;
      ButtonEdit1.Properties.Buttons[0].Visible:=True;

      ButtonEdit2.Properties.ReadOnly:=False;
      ButtonEdit2.Properties.Buttons[0].Visible:=True;

      CalcEdit1.Properties.ReadOnly:=False;
      CalcEdit2.Properties.ReadOnly:=False;
      CalcEdit3.Properties.ReadOnly:=False;
      CalcEdit4.Properties.ReadOnly:=False;
      CalcEdit5.Properties.ReadOnly:=False;
      CalcEdit6.Properties.ReadOnly:=False;
      cxCalcEdit1.Properties.ReadOnly:=False;
      cxCalcEdit2.Properties.ReadOnly:=False;
      cxButton2.Visible:=True;

      LookUpComboBox1.Properties.ReadOnly:=False;
      LookUpComboBox3.Properties.ReadOnly:=False;
      LookUpComboBox4.Properties.ReadOnly:=False;

      cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.Properties.ReadOnly:=False;

      cxCalcEdit3.Properties.ReadOnly:=False;
      cxSpinEdit1.Properties.ReadOnly:=False;
      cxSpinEdit2.Properties.ReadOnly:=False;
      cxCalcEdit4.Properties.ReadOnly:=False;    //�����

    end;
    if iPar>0 then
    begin
      RadioGroup1.Properties.ReadOnly:=False;
      RadioGroup2.Properties.ReadOnly:=False;
      MaskEdit1.Properties.ReadOnly:=True;
      MaskEdit2.Properties.ReadOnly:=False;
      TextEdit1.Properties.ReadOnly:=False;
      TextEdit2.Properties.ReadOnly:=False;

      ButtonEdit1.Properties.ReadOnly:=False;
      ButtonEdit1.Properties.Buttons[0].Visible:=True;

      ButtonEdit2.Properties.ReadOnly:=False;
      ButtonEdit2.Properties.Buttons[0].Visible:=True;

      CalcEdit1.Properties.ReadOnly:=False;
      CalcEdit2.Properties.ReadOnly:=False;
      CalcEdit3.Properties.ReadOnly:=False;
      CalcEdit4.Properties.ReadOnly:=False;
      CalcEdit5.Properties.ReadOnly:=False;
      CalcEdit6.Properties.ReadOnly:=False;
      cxCalcEdit1.Properties.ReadOnly:=False;
      cxCalcEdit2.Properties.ReadOnly:=False;

      cxButton2.Visible:=True;

      LookUpComboBox1.Properties.ReadOnly:=False;
      LookUpComboBox3.Properties.ReadOnly:=False;
      LookUpComboBox4.Properties.ReadOnly:=False;
      cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.Properties.ReadOnly:=False;

      cxCalcEdit3.Properties.ReadOnly:=False;
      cxSpinEdit1.Properties.ReadOnly:=False;
      cxSpinEdit2.Properties.ReadOnly:=False;
      cxSpinEdit3.Properties.ReadOnly:=False;

      cxCalcEdit4.Properties.ReadOnly:=False;  //�����

    end;
    if iPar<0 then
    begin
      RadioGroup1.Properties.ReadOnly:=True;
      RadioGroup2.Properties.ReadOnly:=True;
      MaskEdit1.Properties.ReadOnly:=True;
      MaskEdit2.Properties.ReadOnly:=True;
      TextEdit1.Properties.ReadOnly:=True;
      TextEdit2.Properties.ReadOnly:=True;

      ButtonEdit1.Properties.ReadOnly:=True;
      ButtonEdit1.Properties.Buttons[0].Visible:=False;

      ButtonEdit2.Properties.ReadOnly:=True;
      ButtonEdit2.Properties.Buttons[0].Visible:=False;

      CalcEdit1.Properties.ReadOnly:=True;
      CalcEdit2.Properties.ReadOnly:=True;
      CalcEdit3.Properties.ReadOnly:=True;
      CalcEdit4.Properties.ReadOnly:=True;
      CalcEdit5.Properties.ReadOnly:=True;
      CalcEdit6.Properties.ReadOnly:=True;
      cxCalcEdit1.Properties.ReadOnly:=True;
      cxCalcEdit2.Properties.ReadOnly:=True;

      cxButton2.Visible:=False;

      LookUpComboBox1.Properties.ReadOnly:=True;
      LookUpComboBox3.Properties.ReadOnly:=True;
      LookUpComboBox4.Properties.ReadOnly:=True;
      cxLookUpComboBox1.Properties.ReadOnly:=True;
      cxLookUpComboBox2.Properties.ReadOnly:=True;

      cxCalcEdit3.Properties.ReadOnly:=True;
      cxSpinEdit1.Properties.ReadOnly:=True;
      cxSpinEdit2.Properties.ReadOnly:=True;
      cxSpinEdit3.Properties.ReadOnly:=True;

      cxCalcEdit4.Properties.ReadOnly:=True;   //�����
    end;
    bCreate:=False;
  end;
end;

procedure TfmCards.Timer1Timer(Sender: TObject);
begin
  if bClear1=True then begin StatusBar1.Panels[0].Text:=''; bClear1:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear1:=True;
end;

procedure TfmCards.FormCreate(Sender: TObject);
begin
  //�������� ������� �������
//  dmMC.CardsExpandLevel(nil,CardsTree);
  CardsTree.Items.BeginUpdate;
  prReadClass;
  ClassExpNewT(nil,CardsTree);
//  dmMC.ClassExpNew(nil,CardsTree);
  CardsTree.Items.EndUpdate;

  CardsView.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Panel3.Align:=AlClient;
end;

procedure TfmCards.CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
{  CardsView.BeginUpdate;
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
//    CardsExpandLevel(Node,CardsTree,dmMC.quClassif);
  end;
  CardsView.EndUpdate;}
end;

procedure TfmCards.CardsTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bRefreshCard=False then exit;
  with dmMC do
  begin
    try
      CardsView.BeginUpdate;
      prRefreshCards(Integer(Node.Data),cxCheckBox1.Checked);
      cxCheckBox2.Checked:=false;            //p
      CardsViewRemn.Visible:=false;
    finally
      CardsView.EndUpdate;
    end;  
  end;
end;

procedure TfmCards.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CardsView.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  dmP.quAllRem.Active:=false
end;

procedure TfmCards.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCards.acAddExecute(Sender: TObject);
Var iItem,i,iC:Integer;
    StrWk:String;
    Id_Group,Id_SubGroup:Integer;
begin
  //add
  if CommonSet.Single<>1 then begin StatusBar1.Panels[0].Text:='�������� ������.';  exit end;
  if not CanDo('prAddCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quCards.Active=False then prRefreshCards(Integer(CardsTree.Selected.Data),cxCheckBox1.Checked);

    SetGoodsDef(0); //maskedit1.tag =0 ���������
    SetGoodsVal(0);

    //��� �������  - ����������
    CardRec.Name:='';
    CardRec.NameF:='';
    CardRec.Bar:='';
    CardRec.Price:=0;


    fmAddCard.ShowModal;
    if fmAddCard.ModalResult=mrOk then
    begin
//      Id_Group:=0;

      iItem:=StrToIntDef(fmAddCard.MaskEdit1.Text,0);
      if (iItem>0)and(fmAddCard.MaskEdit2.Text>'')  then
      begin
        quId.Active:=False;
        quId.SQL.Clear;
        quId.SQL.Add('SElect Id from Goods where ID='+INtToStr(iItem));
        quId.Active:=True;
        if quId.RecordCount>0 then
        begin
          quId.Active:=False;
          showmessage('��� ��� ����� ...');
          exit;
        end else
        begin   //��������
          quId.Active:=False;
          if prFindBar(fmAddCard.MaskEdit2.Text,iItem,iC) then
          begin  //�� ����
            showmessage('�� ����, �������� ('+IntToStr(iC)+').');
            exit;
          end else
          begin
            quA.Close;
            quA.SQL.Clear;
            StrWk:='INSERT into Goods values (4';

//            Id_SubGroup:=fmAddCard.ButtonEdit2.Tag;
//            S1:=prFindNameGr(Id_SubGroup,Id_Group);
            Id_SubGroup:=0;
            Id_Group:=fmAddCard.ButtonEdit2.Tag;
//            S1:=prFindTNameGr(quCardsGoodsGroupID.AsInteger);

            StrWk:=StrWk+','+its(Id_Group)+','+its(Id_SubGroup)+','+its(iItem)+','''+Copy(fmAddCard.TextEdit1.Text,1,30)+''','''+fmAddCard.MaskEdit2.Text+'''';
            StrWk:=StrWk+','+its(fmAddCard.RadioGroup2.ItemIndex)+','+its(fmAddCard.RadioGroup1.ItemIndex)+','+its(fmAddCard.RadioGroup1.ItemIndex+1);
                             //         ShReliase                           TovarType                                     EdIzm
            StrWk:=StrWk+','+its(Trunc(fmAddCard.CalcEdit2.EditValue))+',0,'+its(fmAddCard.RadioGroup1.ItemIndex)+','+its(Trunc(fmAddCard.CalcEdit4.EditValue));
                             //        NDS                                  OKDP     Weght                                       SrokReal
            StrWk:=StrWk+',0,'+fs(fmAddCard.CalcEdit6.EditValue)+',0,0,'''+FormatDateTime('yyyy-mm-dd',Date)+''',0,'+fs(fmAddCard.CalcEdit5.EditValue);
                       //TareWeght  KritOst                BHTStatus UKMAction       DataChange                         NSP   WasteRate
            StrWk:=StrWk+','+fs(fmAddCard.CalcEdit1.EditValue)+','+fs(fmAddCard.cxCalcEdit3.EditValue)+','''+Copy(fmAddCard.TextEdit2.Text,1,60)+''','+its(fmAddCard.ButtonEdit1.Tag);
                      //     cena                                      Kol    FullName                                 Country
            StrWk:=StrWk+',4,'+fs(fmAddCard.CalcEdit3.EditValue)+',1,0,'''',1,'''','+fs(fmAddCard.cxCalcEdit2.EditValue)+','+'0,'+fs(fmAddCard.cxCalcEdit1.EditValue)+',0,0,0,'+'0,0';
                     // Status �   Prsision          PriceState,SetOfGoods,PrePacking           Reserv       ObjType,ImgT
            StrWk:=StrWk+','+its(fmAddCard.LookupComboBox4.EditValue)+','+its(fmAddCard.LookupComboBox3.EditValue)+',0,0,0,'+its(fmAddCard.cxSpinEdit1.Value)+','+its(fmAddCard.cxSpinEdit2.Value)+','+its(fmAddCard.cxSpinEdit3.Value)+',0,'+its(RoundEx(fmAddCard.cxCalcEdit4.EditValue*1000))+','+its(fmAddCard.cxLookupComboBox2.EditValue)+','+its(fmAddCard.cxLookupComboBox1.EditValue)+',0,0'; //13,14
                        //   Brand                                                   Categ     V06-����������� ����� � ����   V07-������������ ����� � ����
            StrWk:=StrWk+')';

            quA.SQL.Add(StrWk);

//            TextEdit1.Text:=StrWk;

            quA.ExecSQL;

            quA.Close;
            quA.SQL.Clear;
            StrWk:='INSERT into Barcode values ('+its(iItem)+','+fmAddCard.MaskEdit2.Text+',1,'+its(fmAddCard.RadioGroup2.ItemIndex);
            StrWk:=StrWk+','''+FormatDateTime('yyyy-mm-dd',Date)+''',0,0,0';
            StrWk:=StrWk+')';

            quA.SQL.Add(StrWk);
            quA.ExecSQL;

            //������ �������� �� �������� �������
            StrWk:=FloatToStr(now);
            Delete(StrWk,1,2);
            Delete(StrWk,pos(',',StrWk),1);
            StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

            //����� �������� � ����� ��� ��������� �� �����

            prTPriceBuf(iItem,Trunc(Date),StrToInt(StrWk),-1,StrWk,0,fmAddCard.CalcEdit1.EditValue);

            if fmAddCard.CalcEdit1.EditValue<>CardRec.Price then prLogPrice(iItem,2,CardRec.Price,fmAddCard.CalcEdit1.EditValue);

            bRefreshCard:=False;
            for i:=0 to CardsTree.Items.Count-1 Do
            begin
              if Integer(CardsTree.Items[i].Data) = Id_Group Then
              begin
                CardsTree.Items[i].Expand(False);
                CardsTree.Repaint;
                CardsTree.Items[i].Selected:=True;
                try
                  CardsView.BeginUpdate;
                  prRefreshCards(Id_Group,cxCheckBox1.Checked);
                  quCards.Locate('ID',iItem,[]);
                finally
                  CardsView.EndUpdate;
                  CardGr.SetFocus;
                  CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
                end;
                Break;
              End;
            end;
            bRefreshCard:=True;
          end;
        end;

      end;//}
    end;
  end;
end;

procedure TfmCards.acEditExecute(Sender: TObject);
Var iItem,i,iC:Integer;
    Id_Group,Id_SubGroup:Integer;
    sBar,StrWk:String;
    rOldPrice:Real;
begin
  if not CanDo('prEditCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if CommonSet.Single=0 then exit;

  //�������������
  with dmMC do
  begin
    if quCards.RecordCount=0 then begin ShowMessage('�������� ������� ��� ��������������.');  exit; end;

    rOldPrice:=quCardsCena.AsFloat;

    SetGoodsDef(1);
    SetGoodsVal(1);
    sBar:=quCardsBarCode.AsString;

    //��� �������  - ��������������
    CardRec.Name:=quCardsName.AsString;
    CardRec.NameF:=quCardsFullName.AsString;
    CardRec.Bar:=quCardsBarCode.AsString;
    CardRec.Price:=quCardsCena.AsFloat;

    fmAddCard.ShowModal;
    if fmAddCard.ModalResult=mrOk then
    begin
//      Id_SubGroup:=fmAddCard.ButtonEdit2.Tag;
//      S1:=prFindNameGr(Id_SubGroup,Id_Group);
      Id_Group:=fmAddCard.ButtonEdit2.Tag;
      Id_SubGroup:=0;

      iItem:=quCardsID.AsInteger;

      quE.Active:=False;
      quE.SQL.Clear;

      quE.SQL.Add('Update Goods Set ');
      quE.SQL.Add('GoodsGroupID='+its(Id_Group));
      quE.SQL.Add(',SubGroupID='+its(Id_SubGroup));
      quE.SQL.Add(',Name='''+Copy(fmAddCard.TextEdit1.Text,1,30)+'''');
      quE.SQL.Add(',BarCode='''+fmAddCard.MaskEdit2.Text+'''');
      quE.SQL.Add(',ShRealize='+its(fmAddCard.RadioGroup2.ItemIndex));
      quE.SQL.Add(',TovarType='+its(fmAddCard.RadioGroup1.ItemIndex));
      quE.SQL.Add(',EdIzm='+its(fmAddCard.RadioGroup1.ItemIndex+1));
      quE.SQL.Add(',NDS='+its(Trunc(fmAddCard.CalcEdit2.EditValue)));
      quE.SQL.Add(',SrokReal='+its(Trunc(fmAddCard.CalcEdit4.EditValue)));
      quE.SQL.Add(',KritOst='+fs(fmAddCard.CalcEdit6.EditValue));
//      quE.SQL.Add(',DataChange='''+FormatDateTime('yyyy-mm-dd',Date)+''''); //��� ���� �������� ������ ��� ����������� �������
      quE.SQL.Add(',WasteRate='+fs(fmAddCard.CalcEdit5.EditValue));
      quE.SQL.Add(',cena='+fs(fmAddCard.CalcEdit1.EditValue));
      quE.SQL.Add(',FixPrice='+fs(fmAddCard.cxCalcEdit2.EditValue));
      quE.SQL.Add(',FullName='''+Copy(fmAddCard.TextEdit2.Text,1,60)+'''');
      quE.SQL.Add(',Country='+its(fmAddCard.ButtonEdit1.Tag));
      quE.SQL.Add(',Status=4');
      quE.SQL.Add(',Prsision='+fs(fmAddCard.CalcEdit3.EditValue));
      quE.SQL.Add(',V01='+its(fmAddCard.LookupComboBox4.EditValue));
      quE.SQL.Add(',V02='+its(fmAddCard.LookupComboBox3.EditValue));
      quE.SQL.Add(',Reserv2='+fs(fmAddCard.cxCalcEdit1.EditValue));
      quE.SQL.Add(',Kol='+fs(fmAddCard.cxCalcEdit3.EditValue));
      quE.SQL.Add(',V06='+its(fmAddCard.cxSpinEdit1.Value));
      quE.SQL.Add(',V07='+its(fmAddCard.cxSpinEdit2.Value));
      quE.SQL.Add(',V08='+its(fmAddCard.cxSpinEdit3.Value));
      quE.SQL.Add(',V10='+its(RoundEx(fmAddCard.cxCalcEdit4.EditValue*1000)));
      quE.SQL.Add(',V11='+its(fmAddCard.cxLookupComboBox2.EditValue));
      quE.SQL.Add(',V12='+its(fmAddCard.cxLookupComboBox1.EditValue));
      quE.SQL.Add('where ID='+its(iItem));
      quE.ExecSQL;

      //������ �� �����������
      if fmAddCard.MaskEdit2.Text<>sBar then
      begin
        //�������� ���������
        //������ ������ , �������� �����

        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from Barcode where Bar='''+sBar+'''');
        quD.ExecSQL;

        sBar:=fmAddCard.MaskEdit2.Text;

        if not prFindBar(sBar,iItem,iC) then
        begin
          if not prFindBar(sBar,0,iC) then
          begin  // ���� �� ������ ��� � ������� �� ��������
            quA.Close;
            quA.SQL.Clear;
            StrWk:='INSERT into Barcode values ('+its(iItem)+','+sBar+',1,'+its(fmAddCard.RadioGroup2.ItemIndex);
            StrWk:=StrWk+','''+FormatDateTime('yyyy-mm-dd',Date)+''',0,0,0';
            StrWk:=StrWk+')';
            quA.SQL.Add(StrWk);
            quA.ExecSQL;
          end;
        end else
        begin
          ShowMessage('�������� "'+sBar+'" ��� ���� � ������� ('+its(iC)+'). ������������ ���������.');
        end;
      end;

      //����� �������� � ����� ��������� ����

      //������ �������� �� �������� �������
      StrWk:=FloatToStr(now);
      Delete(StrWk,1,2);
      Delete(StrWk,pos(',',StrWk),1);
      StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

      //����� �������� � ����� ��� ��������� �� �����

      prTPriceBuf(iItem,Trunc(Date),StrToInt(StrWk),-1,StrWk,rOldPrice,fmAddCard.CalcEdit1.EditValue);
      if fmAddCard.CalcEdit1.EditValue<>CardRec.Price then prLogPrice(iItem,2,CardRec.Price,fmAddCard.CalcEdit1.EditValue);

      bRefreshCard:=False;
      for i:=0 to CardsTree.Items.Count-1 Do
      begin
        if Integer(CardsTree.Items[i].Data) = Id_Group Then
        begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Repaint;
          CardsTree.Items[i].Selected:=True;
          try
            CardsView.BeginUpdate;
            prRefreshCards(Id_Group,cxCheckBox1.Checked);
            quCards.Locate('ID',iItem,[]);
          finally
            CardsView.EndUpdate;
            CardGr.SetFocus;
            CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCard:=True;
    end;
  end;
end;

procedure TfmCards.acViewExecute(Sender: TObject);
begin
  //View
  if not CanDo('prViewCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quCards.RecordCount=0 then begin ShowMessage('�������� ������� ��� ��������������.');  exit; end;

    SetGoodsDef(-1);
    SetGoodsVal(-1);

    fmAddCard.ShowModal;
  end;
end;

procedure TfmCards.acDelExecute(Sender: TObject);
Var iItem:Integer;
begin
  if not CanDo('prDelCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    //Del
    if MessageDlg('�� ������������� ������ ������� ����� - ���:'+quCardsId.AsString+'  '+quCardsName.AsString+' ?',
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iItem:=quCardsId.AsInteger;
      try
        //������ ������� ��� �� ���� ��������
        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from Barcode where GoodsId='+INtToStr(iItem));
        quD.ExecSQL;

        //������ ��������
        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('Delete from Goods where Id='+INtToStr(iItem));
        quD.ExecSQL;

        showmessage('�������� ������� ��.');
      except
        showmessage('������ ��������.');
      end;
      prRefreshCards(Integer(CardsTree.Selected.data),cxCheckBox1.Checked);
    end;
  end;
end;


procedure TfmCards.N1Click(Sender: TObject);
Var iNum,i:Integer;
     TreeNode : TTreeNode;
begin
  //�������� ������
  if CommonSet.Single=0 then exit;

  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ����������';
  fmAddClassif.Label1.Caption:='���������� ������ �������.';
  fmAddClassif.TextEdit1.Text:='';
  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    //�������� ������
    with dmMC do
    begin
      iNum:=prMax('Group')+1;

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into SubGroup values (0,0,'+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.ExecSQL;

      CardsTree.Items.BeginUpdate;

      TreeNode:=CardsTree.Items.AddChildObject(nil,fmAddClassif.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;
      // ������� ��������� (������) �������� ����� ������ ��� ����,
      // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
      CardsTree.Items.AddChildObject(TreeNode,'', nil);

      CardsTree.Items.EndUpdate;


      FOR i:=0 To CardsTree.Items.Count-1 Do
        IF Integer(CardsTree.Items[i].Data) = iNum Then
        Begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Items[i].Selected:=True;
          CardsTree.Repaint;
          Break;
        End;

    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmCards.N2Click(Sender: TObject);
Var iNum,i,iParent:Integer;
    TreeNode : TTreeNode;
    CurNode:TTreeNode;
begin
  //�������� ���������
  if CommonSet.Single=0 then exit;
  
  with dmMC do
  begin
    CurNode:=CardsTree.Selected;
    CurNode.Expand(False);
{    if CurNode.Parent=Nil then
    begin
      iParent:=INteger(CurNode.Data);
      fmAddClassif:=TfmAddClassif.Create(Application);
      fmAddClassif.Caption:='�������������';
      fmAddClassif.Label1.Caption:='���������� ��������� ������� � ������ "'+CurNode.Text+'"';
      fmAddClassif.TextEdit1.Text:='';
      fmAddClassif.ShowModal;
    end
    else
    begin
      showmessage('������ ��������� �������� 3-��� ������.');
      exit;
    end;
}
   //�������� �� ������������
   
      iParent:=INteger(CurNode.Data);
      fmAddClassif:=TfmAddClassif.Create(Application);
      fmAddClassif.Caption:='�������������';
      fmAddClassif.Label1.Caption:='���������� ��������� ������� � ������ "'+CurNode.Text+'"';
      fmAddClassif.TextEdit1.Text:='';
      fmAddClassif.ShowModal;

    if fmAddClassif.ModalResult=mrOk then
    begin
    //�������� ���������
      iNum:=prMax('SGroup')+1;

      quA.SQL.Clear;
      quA.SQL.Add('INSERT into SubGroup values (0,'+INtToStr(iParent)+','+IntToStr(iNum)+','''+Copy(fmAddClassif.TextEdit1.Text,1,30)+''','''''+',14,0,'''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
      quA.ExecSQL;

      //�������, ��� ������

      CardsTree.Items.BeginUpdate;

      TreeNode:=CardsTree.Items.AddChildObject(CurNode,fmAddClassif.TextEdit1.Text,Pointer(iNum));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;
      // ������� ��������� (������) �������� ����� ������ ��� ����,
      // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
      CardsTree.Items.AddChildObject(TreeNode,'', nil);

      CardsTree.Items.EndUpdate;

      FOR i:=0 To CardsTree.Items.Count-1 Do
        IF Integer(CardsTree.Items[i].Data) = iNum Then
        Begin
          CardsTree.Items[i].Expand(False);
          CardsTree.Repaint;
          CardsTree.Items[i].Selected:=True;
          Break;
        End;
    end;
  end;
  fmAddClassif.Release;
end;

procedure TfmCards.N4Click(Sender: TObject);
Var iNum:Integer;
    CurNode:TTreeNode;
begin
  //��������
  if CommonSet.Single=0 then exit;

  with dmMC do
  begin
    CurNode:=CardsTree.Selected;
    if CurNode=nil then exit;

    if MessageDlg('�� ������������� ������ ������� ������ - '+CardsTree.Selected.Text+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      iNum:=Integer(CurNode.Data);
      if CurNode.getFirstChild=nil then
      begin
        if quCards.RecordCount>0 then
        begin
          ShowMessage('�������� ���������� - � ������ ���� �������� ������.');
          exit;
        end else
        begin
          quD.Close;
          quD.SQL.Clear;
          quD.SQL.Add('delete from "SubGroup" where ID='+IntToStr(iNum));
          quD.ExecSQL;

          CardsTree.Selected.Delete;
        end;
      end
      else
      begin
        ShowMessage('�������� ���������� - ���� ������� ������.');
        exit;
      end;
    end;
  end;
end;

procedure TfmCards.acFindBarExecute(Sender: TObject);
Var sBar:String;
    iC,i:Integer;
begin
//����� �� ��
  if Length(TextEdit1.Text)<3 then exit;
  with dmMC do
  begin
    sBar:=TextEdit1.Text;
    if sBar>'' then
    begin
      if prFindBar(sBar,0,iC) then
      begin
        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS","Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iC));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          StatusBar1.Panels[0].Text:='�� ����, �������� ('+IntToStr(iC)+').';
          bRefreshCard:=False;
          for i:=0 to CardsTree.Items.Count-1 Do
          begin
            if Integer(CardsTree.Items[i].Data) = quFindCGoodsGroupID.AsInteger Then
            begin
              CardsTree.Items[i].Expand(False);
              CardsTree.Repaint;
              CardsTree.Items[i].Selected:=True;
              try
                CardsView.BeginUpdate;
                prRefreshCards(quFindCGoodsGroupID.AsInteger,cxCheckBox1.Checked);
                quCards.Locate('ID',iC,[]);
                cxCheckBox2.Checked:=false;              //p
                CardsViewRemn.Visible:=false;
              finally
                CardsView.EndUpdate;
                CardGr.SetFocus;
                CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
              end;
              Break;
            End;
          end;
          bRefreshCard:=True;

          if quFindCStatus.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������.');
          end;

        end else StatusBar1.Panels[0].Text:='�� ����, �������� ('+IntToStr(iC)+') ���.';

      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
//        showmessage('�� '+sBar+' �� ������.');
    end;
  end;
end;

procedure TfmCards.CardsViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);

begin
 if cxCheckBox1.Checked then
 begin
   ACanvas.Canvas.Brush.Color := clWhite;
   ACanvas.Canvas.Font.Color := clGray;
 end;

{
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to CardsView.ColumnCount-1 do
  begin
    if CardsView.Columns[i].Name='CardsViewStatus' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType>5  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;
}
end;

procedure TfmCards.acBarViewExecute(Sender: TObject);
begin
  with dmMC do
  begin
    if quCards.RecordCount=0 then exit;

    quBars.Active:=False;
    quBars.ParamByName('GOODSID').Value:=quCardsId.AsInteger;
    quBars.Active:=True;
  end;
  fmBar.ShowModal;
end;

procedure TfmCards.acJurnalExecute(Sender: TObject);
begin
// ������ ��������
{  with dmMC do
  begin
    quPr.Active:=False;
    quPr.SQL.Clear;
    quPr.SQL.Add('Select pk.Id,pk.Id_Obj,pk.Str_Obj,pk.TName,pk.Operation,pk.Istatus,pk.Id_Person,pk.OpDate,rp.Name');
    quPr.SQL.Add('from Proto�ol pk');
    quPr.SQL.Add('left join RPersonal rp on rp.Id=pk.Id_Person');
    quPr.SQL.Add('where Istatus=0');
    quPr.SQL.Add('Order by pk.Id');
    quPr.Active:=True;

    fmJurnal.ShowModal;
    quPr.Active:=False;
  end;}
end;

procedure TfmCards.N6Click(Sender: TObject);
Var iNum:Integer;
    TreeNode : TTreeNode;
    sName:String;
begin
  if CommonSet.Single=0 then exit;

  TreeNode:=CardsTree.Selected;
  if TreeNode=nil then exit;
  sName:=TreeNode.Text;
  //�������� ��������
  fmAddClassif:=TfmAddClassif.Create(Application);
  fmAddClassif.Caption:='������������� - ���������';
  fmAddClassif.Label1.Caption:='�������� �������� ������ �������.';
  fmAddClassif.TextEdit1.Text:=sName;
  fmAddClassif.ShowModal;
  if fmAddClassif.ModalResult=mrOk then
  begin
    if fmAddClassif.TextEdit1.Text<>sName then
    begin //�������� ���������� - �������� � �������������� � � ������
      CardsTree.Items.BeginUpdate;
      TreeNode.Text:=fmAddClassif.TextEdit1.Text;
      CardsTree.Items.EndUpdate;

      iNum:=Integer(CardsTree.Selected.Data);
      with dmMC do
      begin
        quE.SQL.Clear;
        quE.SQL.Add('Update "SubGroup" Set Name='''+Copy(fmAddClassif.TextEdit1.Text,1,30)+'''');
        quE.SQL.Add('where ID='+IntToStr(iNum));
        quE.ExecSQL;
      end;
    end;
  end;
  fmAddClassif.Release;
end;


procedure TfmCards.TextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{Var bCh:Byte;
    iArt:Integer;
    sBar:String;
    i:INteger;}
begin
{  bCh:=ord(Key);
  if bCh = 13 then
  begin
    with dmMC do
    begin
      if TextEdit2.Text='' then
      begin
        exit;
      end;
      iArt:=StrToIntDef(TextEdit2.Text,0);
      if iArt=0 then
      begin
        TextEdit2.Text:='';
        exit;
      end;

      quCardFind.Active:=False;
      quCardFind.SQL.Clear;

      quCardFind.SQL.Add('SELECT ca.[Id],ca.[Id_Group],ca.[Id_SubGroup],ca.[Name],ca.[FullName]');
      quCardFind.SQL.Add(',ca.[BarCode],ca.[EdIzm],ca.[KritOst],ca.[iStatus]');
      quCardFind.SQL.Add(',ca.[ManCat],ca.[Categ],ca.[Brand],ca.[ABC]');
      quCardFind.SQL.Add(',cl1.[Name] as NameGr, cl2.[Name] as NameSGr');
      quCardFind.SQL.Add('  FROM [Goods] ca');
      quCardFind.SQL.Add('left join Classif cl1 on cl1.[Id]=ca.[Id_Group]');
      quCardFind.SQL.Add('left join Classif cl2 on cl2.[Id]=ca.[Id_SubGroup]+10000');
      quCardFind.SQL.Add('where ca.[Id]= '+IntToStr(iArt));
      quCardFind.Active:=True;

      quCardFind.First;
      if not quCardFind.Eof then
      begin
        //����� �� ����
//        CardsView.BeginUpdate;

        sBar:=quCardFindBarCode.AsString;
        for i:=0 to CardsTree.Items.Count-1 Do
          if Integer(CardsTree.Items[i].Data) = quCardFindId_Group.AsInteger Then
          begin
            CardsTree.Items[i].Expand(False);
            CardsTree.Repaint;
            CardsTree.Items[i].Selected:=True;
            Break;
          End;
        delay(10);
        quCards.First;
        quCards.locate('Id',iArt,[]);
//        CardsView.EndUpdate;

        Panel3.SetFocus;
        CardGr.SetFocus;
        CardsView.Focused:=True;
        delay(10);

      end else showmessage('������ ���� "'+IntToStr(iArt)+'" � ���� ���.');

      TextEdit2.Text:='';
      quCardFind.Active:=False;
    end;
  end;}
end;

procedure TfmCards.acMove1Execute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if not quCards.Eof then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=quCardsId.AsInteger;
      prFillMove(quCardsId.AsInteger);
{
      quMove.Active:=False;
      quMove.ParamByName('IDCARD').Value:=quCardsId.AsInteger;
      quMove.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quMove.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quMove.Active:=True;

      quMove.First;
}

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=quCardsName.AsString+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmCards.Timer2Timer(Sender: TObject);
begin
  if bClearStatusBar=True then begin StatusBar1.Panels[0].Text:=''; Delay(10); end;
  if StatusBar1.Panels[0].Text>'' then bClearStatusBar:=True;
end;

procedure TfmCards.acTransGrExecute(Sender: TObject);
{Var Id:INteger;
    StrWk:String;
    iCount:Integer;}
begin
{  if not CanDo('prTransferGr') then
  begin
    StatusBar1.Panels[0].Text:='��� ����.';
  end else
  begin
    bTransp:=True;
    with dmMC do
    begin
      try
        Id:=Integer(CardsTree.Selected.Data);

        fmTransp:=TfmTransp.Create(Application);
        fmTransp.Show;
        delay(10);
        WrMess('������ �������� ������.');
        WrMess('');
        WrMess('�������� ��������������.');

        trClassif(Id,1); //1 - ��������� (���� ���, �� ����������)


        if Id<10000 then
        begin //������

          quClassifTr.Active:=False;
          quClassifTr.Parameters.ParamByName('ID').Value:=Id;
          quClassifTr.Active:=True;

          taSubGroup.Active:=False;
          taSubGroup.DatabaseName:=CommonSet.PathCryst;
          taSubGroup.Active:=True;

          iCount:=0;
          quClassifTr.First;
          while not quClassifTr.Eof do
          begin

            inc(iCount);
            StatusBar1.Panels[0].Text:=INtToStr(quClassifTrId.AsInteger-10000)+' '+quClassifTrName.AsString;
            StatusBar1.Panels[1].Text:=INtToStr(iCount);
            delay(10);

            if taSubGroup.Locate('ID',quClassifTrId.AsInteger-10000,[]) then
            begin
              taSubGroup.Edit;
            end else
            begin //���� �� ��� �� ��������
              taSubGroup.Append;
              taSubGroupID.AsInteger:=quClassifTrId.AsInteger-10000;
            end;

            taSubGroupGoodsGroupID.AsInteger:=Id;
            taSubGroupName.AsString:=AnsiToOemConvert(quClassifTrName.AsString);
            taSubGroupFullName.AsString:='';
            taSubGroupObjectTypeID.AsInteger:=14;
            taSubGroupImageID.AsInteger:=0;
            taSubGroupNDS.AsString:='';
            taSubGroupNSP.AsString:='';
            taSubGroupGroupEU.AsInteger:=0;
            taSubGroup.Post;

            delay(100);
            quClassifTr.Next;
          end;

          taSubGroup.Active:=False;
          quClassifTr.Active:=False;
        end;
        WrMess('�������� ��.');
        fmTransp.Show;

        WrMess('');
        WrMess('������������ ������ �������� ��� ��������.');


        quGoodsTr.Active:=False;
        quGoodsTr.SQL.Clear;
        quGoodsTr.SQL.Add('Select  Id, Id_Group, Id_SubGroup, Name, FullName, BarCode, BarType, TovarType, EdIzm, NDS, OKDP, Weght, SrokReal, TareWeght, KritOst, WasteRate, Kol, Country, iStatus, Prsision, MargGroup, Cena, Id_Eu from Goods');
        quGoodsTr.SQL.Add('where iStatus<250');

        if Id<10000 then
        begin //������
          quGoodsTr.SQL.Add('and Id_Group='+INtToStr(Id));
        end else
        begin //��������
          quGoodsTr.SQL.Add('and Id_SubGroup='+INtToStr(Id-10000));
        end;

        quGoodsTr.Active:=True;

        WrMess('�������� ��.');
        WrMess('');
        fmTransp.Show;

        WrMess('�������� ������ �������.');

        tcGoods.Active:=false;
        tcGoods.DatabaseName:=CommonSet.PathCryst;
        tcGoods.Active:=True;

        tcDeparts.Active:=False;
        tcDeparts.DatabaseName:=CommonSet.PathCryst;
        tcDeparts.Active:=True;

        tcLnkDp_Gd.Active:=False;
        tcLnkDp_Gd.DatabaseName:=CommonSet.PathCryst;
        tcLnkDp_Gd.Active:=True;

        tcEuTrans.Active:=False;
        tcEuTrans.DatabaseName:=CommonSet.PathCryst;
        tcEuTrans.Active:=True;

        iCount:=0;
        quGoodsTr.First;
        while not quGoodsTr.Eof do
        begin
          inc(iCount);
          StatusBar1.Panels[0].Text:=INtToStr(quGoodsTrId.AsInteger)+' '+quGoodsTrName.AsString;
          StatusBar1.Panels[1].Text:=INtToStr(iCount);
          delay(10);

          if tcGoods.Locate('BarCode',quGoodsTrBarCode.AsString,[]) then
          begin //��������� ������������ �� ��
            if  tcGoodsID.AsInteger<>quGoodsTrId.AsInteger then
            begin
              tcGoods.Edit;
              tcGoodsBarCode.AsString:=IntToStr(tcGoodsID.AsInteger);
              tcGoods.Post;
            end;
          end;

          if tcGoods.Locate('ID',quGoodsTrId.AsInteger,[]) then
          begin
            tcGoods.Edit;
            tcGoodsGoodsGroupID.AsInteger:=quGoodsTrId_Group.AsInteger;
            tcGoodsSubGroupID.AsInteger:=quGoodsTrId_SubGroup.AsInteger;
            tcGoodsName.AsString:=AnsiToOemConvert(quGoodsTrName.AsString);
            tcGoodsFullName.AsString:=AnsiToOemConvert(quGoodsTrFullName.AsString);
            StrWk:=quGoodsTrBarCode.AsString;
            if Length(StrWk)>13 then StrWk:=Copy(StrWk,1,13);
            tcGoodsBarCode.AsString:=StrWk;
            tcGoodsShRealize.AsInteger:=quGoodsTrBarType.AsInteger;
            tcGoodsTovarType.AsInteger:=quGoodsTrTovarType.AsInteger;
            tcGoodsEdIzm.AsInteger:=quGoodsTrEdIzm.AsInteger;
            tcGoodsNDS.AsFloat:=quGoodsTrNDS.AsFloat;
            tcGoodsOKDP.AsFloat:=quGoodsTrOKDP.AsFloat;
            tcGoodsSrokReal.AsInteger:=quGoodsTrSrokReal.AsInteger;
            tcGoodsWeght.AsFloat:=quGoodsTrWeght.AsFloat;
            tcGoodsTareWeight.AsInteger:=quGoodsTrTareWeght.AsInteger;
            tcGoodsKritOst.AsFloat:=quGoodsTrKritOst.AsFloat;
            tcGoodsBHTStatus.AsInteger:=0;
            tcGoodsUKMAction.AsInteger:=0;
            tcGoodsDataChange.AsDateTime:=now;
            tcGoodsNSP.AsFloat:=0;
            tcGoodsWasteRate.AsFloat:=quGoodsTrWasteRate.AsFloat;
            tcGoodsKol.AsFloat:=0;
            tcGoodsCountry.AsInteger:=quGoodsTrCountry.AsInteger;
            tcGoodsStatus.AsInteger:=4;  // �
            tcGoodsPrsision.AsFloat:=quGoodsTrPrsision.AsFloat;
            tcGoodsPriceState.AsInteger:=1;
            tcGoodsSetOfGoods.AsInteger:=0;
            tcGoodsPrePacking.AsString:='';
            tcGoodsPrintLabel.AsInteger:=1;
            tcGoodsMargGroup.AsString:=AnsiToOemConvert(quGoodsTrMargGroup.AsString);
            tcGoods.Post;

            //�������� � �������  ��� �� ����


            //�� ������� - ������������ �����
            if quGoodsTrId_Eu.AsInteger<>32000 then
            begin    //���������� - ����� ����������
              if tcEuTrans.Locate('CodeTovar',quGoodsTrId.AsInteger,[]) then
              begin
                tcEuTrans.Edit;
              end else
              begin
                tcEuTrans.Append;
                tcEuTransCodeTovar.AsInteger:=quGoodsTrId.AsInteger;
              end;
              tcEuTransCode.AsInteger:=quGoodsTrId_Eu.AsInteger;
              tcEuTrans.Post;
            end;

          end
          else //����� �������� ��� ��� - ���� ���������
          begin
            tcDeparts.First;
            while (tcDepartsStatus1.AsInteger<>9)and(not tcDeparts.EOF) do tcDeparts.Next;

            tcGoods.Append;

            tcGoodsOtdel.AsInteger:=tcDepartsID.AsInteger;
            tcGoodsID.AsInteger:=quGoodsTrId.AsInteger;
            tcGoodsCena.AsFloat:=quGoodsTrCena.AsFloat;

            tcGoodsGoodsGroupID.AsInteger:=quGoodsTrId_Group.AsInteger;
            tcGoodsSubGroupID.AsInteger:=quGoodsTrId_SubGroup.AsInteger;
            tcGoodsName.AsString:=AnsiToOemConvert(quGoodsTrName.AsString);
            tcGoodsFullName.AsString:=AnsiToOemConvert(quGoodsTrFullName.AsString);
            StrWk:=quGoodsTrBarCode.AsString;
            if Length(StrWk)>13 then StrWk:=Copy(StrWk,1,13);
            tcGoodsBarCode.AsString:=StrWk;
            tcGoodsShRealize.AsInteger:=quGoodsTrBarType.AsInteger;
            tcGoodsTovarType.AsInteger:=quGoodsTrTovarType.AsInteger;
            tcGoodsEdIzm.AsInteger:=quGoodsTrEdIzm.AsInteger;
            tcGoodsNDS.AsFloat:=quGoodsTrNDS.AsFloat;
            tcGoodsOKDP.AsFloat:=quGoodsTrOKDP.AsFloat;
            tcGoodsSrokReal.AsInteger:=quGoodsTrSrokReal.AsInteger;
            tcGoodsWeght.AsFloat:=quGoodsTrWeght.AsFloat;
            tcGoodsTareWeight.AsInteger:=quGoodsTrTareWeght.AsInteger;
            tcGoodsKritOst.AsFloat:=quGoodsTrKritOst.AsFloat;
            tcGoodsBHTStatus.AsInteger:=0;
            tcGoodsUKMAction.AsInteger:=0;
            tcGoodsDataChange.AsDateTime:=now;
            tcGoodsNSP.AsFloat:=0;
            tcGoodsWasteRate.AsFloat:=quGoodsTrWasteRate.AsFloat;
            tcGoodsKol.AsFloat:=0;
            tcGoodsCountry.AsInteger:=quGoodsTrCountry.AsInteger;
            tcGoodsStatus.AsInteger:=4;  // �
            tcGoodsPrsision.AsFloat:=quGoodsTrPrsision.AsFloat;
            tcGoodsPriceState.AsInteger:=1;
            tcGoodsSetOfGoods.AsInteger:=0;
            tcGoodsPrePacking.AsString:='';
            tcGoodsPrintLabel.AsInteger:=1;
            tcGoodsMargGroup.AsString:=AnsiToOemConvert(quGoodsTrMargGroup.AsString);
            tcGoods.Post;

            //�������� �������, ������ �������� � �������
            while not tcDeparts.EOF do
            begin
              if tcDepartsStatus1.AsInteger=9 then
              begin
                tcLnkDp_Gd.Append;
                tcLnkDp_GdGoodsID.AsInteger:=quGoodsTrId.AsInteger;
                tcLnkDp_GdDepartID.AsInteger:=tcDepartsID.AsInteger;
                tcLnkDp_GdPrice.AsFloat:=quGoodsTrCena.AsFloat;
                tcLnkDp_GdNSP.AsFloat:=0;
                tcLnkDp_GdExtrRest.AsFloat:=0;
                tcLnkDp_GdPresision.AsFloat:=quGoodsTrPrsision.AsFloat;
                tcLnkDp_GdMarkUpProc.AsFloat:=0;
                tcLnkDp_GdMarkUpMin.AsFloat:=0;
                tcLnkDp_GdMarkUpMax.AsFloat:=0;
                tcLnkDp_Gd.Post;
              end;
              tcDeparts.Next;
            end;

            //�� ������� - ������������ �����
            if quGoodsTrId_Eu.AsInteger<>32000 then
            begin    //���������� - ����� ����������
              if tcEuTrans.Locate('CodeTovar',quGoodsTrId.AsInteger,[]) then
              begin
                tcEuTrans.Edit;
              end else
              begin
                tcEuTrans.Append;
                tcEuTransCodeTovar.AsInteger:=quGoodsTrId.AsInteger;
              end;
              tcEuTransCode.AsInteger:=quGoodsTrId_Eu.AsInteger;
              tcEuTrans.Post;
            end;
          end;

          quGoodsTr.next;
          delay(10);
        end;

        WrMess('�������� ��.');
        WrMess('');
        fmTransp.Show;

        tcGoods.Active:=false;
        tcDeparts.Active:=False;
        tcLnkDp_Gd.Active:=False;
        tcEuTrans.Active:=False;

        quGoodsTr.Active:=False;

        WrMess('');
        WrMess('������������ ������ ����������.');

        quBarTr.Active:=False;
        quBarTr.SQL.Clear;
        quBarTr.SQL.Add('Select b.Bar, b.GoodsId, b.Quant, b.BarFormat, b.BarStatus, b.Cost, b.Status from Goods gd');
        quBarTr.SQL.Add('left join BarCode b on b.GoodsId=gd.Id');
        quBarTr.SQL.Add('where gd.iStatus<250');
        quBarTr.SQL.Add('and b.GoodsId>0');

        if Id<10000 then
        begin //������
          quBarTr.SQL.Add('and gd.Id_Group='+INtToStr(Id));
        end else
        begin //��������
          quBarTr.SQL.Add('and gd.Id_SubGroup='+INtToStr(Id-10000));
        end;

        quBarTr.Active:=True;

        WrMess('�������� ��.');
        WrMess('');
        fmTransp.Show;

        WrMess('�������� ����������.');

        tcBar.Active:=false;
        tcBar.DatabaseName:=CommonSet.PathCryst;
        tcBar.Active:=True;

        iCount:=0;
        quBarTr.First;
        while not quBarTr.Eof do
        begin

          inc(iCount);
          StatusBar1.Panels[0].Text:=INtToStr(quBarTrGoodsId.AsInteger)+' '+quBarTrBar.AsString;
          StatusBar1.Panels[1].Text:=INtToStr(iCount);
          delay(10);

          if tcBar.Locate('ID',quBarTrBar.AsString,[]) then
          begin
            tcBar.Edit;
          end else //� ���� ����� ��� ��� �� ���������
          begin
            tcBar.Append;
            tcBarID.AsString:=quBarTrBar.AsString;
          end;

          tcBarGoodsID.AsInteger:=quBarTrGoodsId.AsInteger;
          tcBarQuant.AsFloat:=quBarTrQuant.AsFloat;
          tcBarBarFormat.AsInteger:=quBarTrBarFormat.AsInteger;
          tcBarDateChange.AsDateTime:=Now;
          tcBarBarStatus.AsInteger:=quBarTrBarStatus.AsInteger;
          tcBarCost.AsFloat:=quBarTrCost.AsFloat;
          tcBarStatus.AsInteger:=quBarTrStatus.AsInteger;
          tcBar.Post;

          quBarTr.Next;
          delay(10);
        end;

        WrMess('�������� ��.');
        WrMess('');
        fmTransp.Show;

        quBarTr.Active:=False;
        tcBar.Active:=false;

      finally
        WrMess('');
        WrMess('�������� ���������.');

        taSubGroup.Active:=False;
        tcGoods.Active:=false;
        tcDeparts.Active:=False;
        tcLnkDp_Gd.Active:=False;
        tcEuTrans.Active:=False;
        tcBar.Active:=false;


        quClassifTr.Active:=False;
        quGoodsTr.Active:=False;
        quBarTr.Active:=False;

        delay(5000);
        StatusBar1.Panels[0].Text:='';
        StatusBar1.Panels[1].Text:='';
        delay(10);

        fmTransp.Release;
        bTransp:=False;
      end;
    end;
  end;}
end;

procedure TfmCards.acRemns2Execute(Sender: TObject);
begin
  with dmMC do
  begin
    fmRemn.cxButton1.Enabled:=False;
    quRemn.Active:=False;
    delay(20);
    fmRemn.ViewR.BeginUpdate;
    quRemn.ParamByName('IDCARD').AsInteger:=quCardsID.AsInteger;
    quRemn.Active:=True;
    fmRemn.ViewR.EndUpdate;

    fmRemn.Caption:='�������: '+quCardsId.AsString+'  '+quCardsName.AsString;
    fmRemn.cxButton1.Enabled:=True;
    fmRemn.Show;
  end;
end;

procedure TfmCards.acTransListExecute(Sender: TObject);
begin
  //�������� �� �����
  //����� ��������� ���
  with dmMC do
  begin
    fmBufPrice.ViewBufPr.BeginUpdate;
    quBufPr.Active:=False;
    quBufPr.ParamByName('DATEB').AsDate:=Trunc(fmBufPrice.cxDateEdit1.Date);
    quBufPr.ParamByName('DATEE').AsDate:=Trunc(fmBufPrice.cxDateEdit2.Date);
    quBufPr.ParamByName('IST').AsInteger:=0;
    quBufPr.Active:=True;

    fmBufPrice.ViewBufPr.EndUpdate;
    fmBufPrice.Show;
  end;
end;

procedure TfmCards.acExcelExecute(Sender: TObject);
begin
//������� � Excel
  prNExportExel5(CardsView);
end;

procedure TfmCards.TextEdit1Click(Sender: TObject);
begin
  TextEdit1.SetFocus;
  TextEdit1.SelectAll;
end;

procedure TfmCards.acFindNameExecute(Sender: TObject);
Var i:Integer;
    iCod:INteger;
    s1:String;
begin
//����� �� ��������
  if Length(TextEdit1.Text)<3 then exit;
  with dmMC do
  begin
    fmFindC.ViewFind.BeginUpdate;
    dsquFindC.DataSet:=nil;
    try
      quFindC.Active:=False;
      quFindC.SQL.Clear;
      quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
      quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
      quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
      quFindC.SQL.Add('FROM "Goods"');
      quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
//      s1:=TextEdit1.Text;
      s1:=AnsiUpperCase(TextEdit1.Text);
      quFindC.SQL.Add('where "Goods"."Name" like ''%'+s1+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
      quFindC.Active:=True;
    finally
      dsquFindC.DataSet:=quFindC;
      fmFindC.ViewFind.EndUpdate;
    end;
    fmFindC.ShowModal;

    if fmFindC.ModalResult=mrOk then
    begin
      if CountRec1(quFindC) then
      begin
        bRefreshCard:=False;
        for i:=0 to CardsTree.Items.Count-1 Do
        begin
          if Integer(CardsTree.Items[i].Data) = quFindCGoodsGroupID.AsInteger Then
          begin
            CardsTree.Items[i].Expand(False);
            CardsTree.Repaint;
            CardsTree.Items[i].Selected:=True;
            try
              CardsView.BeginUpdate;
              prRefreshCards(quFindCGoodsGroupID.AsInteger,cxCheckBox1.Checked);
              cxCheckBox2.Checked:=false;             //p
              CardsViewRemn.Visible:=false;
              iCod:=quFindCID.AsInteger;
              quCards.Locate('ID',iCod,[]);
            finally
              CardsView.EndUpdate;
              CardGr.SetFocus;
              CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
            end;
            Break;
          End;
        end;
        bRefreshCard:=True;

        if quFindCStatus.AsInteger>100 then
        begin
          Showmessage('�������� ����� ��������� � ��������������.');
        end;
      end;
    end;
  end;
end;

procedure TfmCards.cxButton1Click(Sender: TObject);
Var iCod,i:INteger;
begin
  if Length(TextEdit1.Text)<1 then exit;
  with dmMC do
  begin
    iCod:=StrToINtDef(TextEdit1.Text,0);
    if iCod>0 then
    begin
      fmFindC.ViewFind.BeginUpdate;
      dsquFindC.DataSet:=nil;
      try
        quFindC.Active:=False;
        quFindC.SQL.Clear;
        quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
        quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
        quFindC.SQL.Add('FROM "Goods"');
        quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
        quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCod));
        quFindC.Active:=True;
      finally
        dsquFindC.DataSet:=quFindC;
        fmFindC.ViewFind.EndUpdate;
      end;
      if CountRec1(quFindC) then
      begin
        bRefreshCard:=False;
        for i:=0 to CardsTree.Items.Count-1 Do
        begin
          if Integer(CardsTree.Items[i].Data) = quFindCGoodsGroupID.AsInteger Then
          begin
            CardsTree.Items[i].Expand(False);
            CardsTree.Repaint;
            CardsTree.Items[i].Selected:=True;
            try
              CardsView.BeginUpdate;
              prRefreshCards(quFindCGoodsGroupID.AsInteger,cxCheckBox1.Checked);
              quCards.Locate('ID',iCod,[]);
              cxCheckBox2.Checked:=false;          //p
              CardsViewRemn.Visible:=false;
            finally
              CardsView.EndUpdate;
              CardGr.SetFocus;
              CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
            end;
            Break;
          End;
        end;
        bRefreshCard:=True;
        if quFindCStatus.AsInteger>100 then
        begin
          Showmessage('�������� ����� ��������� � ��������������.');
        end;
      end else showmessage('����� � ����� '+IntToStr(iCod)+' �� ������.');
    end;
  end;
end;

procedure TfmCards.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmCards.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    IC,i:INteger;
begin
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
  TextEdit1.Text:=sBar;
    with dmMC do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iC));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          bRefreshCard:=False;
          for i:=0 to CardsTree.Items.Count-1 Do
          begin
            if Integer(CardsTree.Items[i].Data) = quFindCGoodsGroupID.AsInteger Then
            begin
              CardsTree.Items[i].Expand(False);
              CardsTree.Repaint;
              CardsTree.Items[i].Selected:=True;
              try
                CardsView.BeginUpdate;
                prRefreshCards(quFindCGoodsGroupID.AsInteger,cxCheckBox1.Checked);
                fmCards.cxCheckBox2.Checked:=false;
                if quCards.Locate('ID',iC,[])=False then showmessage('����� '+sBar+' �� ������, ��������� ��������������.');
              finally
                CardsView.EndUpdate;
                CardGr.SetFocus;
                CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
              end;
              Break;
            End;
          end;
          bRefreshCard:=True;

          if quFindCStatus.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������.');
          end;
        end else StatusBar1.Panels[0].Text:='�� ����, �������� ('+IntToStr(iC)+') ���.';
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
//        showmessage('�� '+sBar+' �� ������.');
      end;
    end;
end;

procedure TfmCards.acCashLoadExecute(Sender: TObject);
Var bOk:Boolean;
    sPath,StrWk,sCards,sBar,sNum,sCashLdd,sMessur,sCSz,sName:String;
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    NumPost:INteger;
    fc,fb:TextFile;
    sPos,sB,sM,sr,sDel:String;
begin
  //�������� ����
  with dmMc do
  with dmMt do
  begin
    try
      sr:=',';
      if CardsView.Controller.SelectedRecordCount=0 then exit;
      if fmSt.cxButton1.Enabled=False then exit;

      if taCards.Active=False then taCards.Active:=True;

      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('���� �������� ����..');  delay(10);

      if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX.');

      bOk:=True;

      dmPx.Session1.Active:=False;
      dmPx.Session1.NetFileDir:=CommonSet.NetPath;
      dmPx.Session1.Active:=True;

      //������ �������� �� �������� �������
      StrWk:=FloatToStr(now);
      Delete(StrWk,1,2);
      Delete(StrWk,pos(',',StrWk),1);
      StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

      NumPost:=StrToINtDef(StrWk,0);
      sCards:='_'+IntToHex(NumPost,7);
      sBar:='_'+IntToHex(NumPost+1,7);

      //����� �������� ������
      CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Bars.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
      CopyFile(PChar(CommonSet.TrfPath+'Bars.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

      assignfile(fc,CommonSet.TmpPath+'PLUCASH.DAT');
      assignfile(fb,CommonSet.TmpPath+'BAR.DAT');

      if FileExists(CommonSet.TmpPath+'PLUCASH.DAT') then reset(fc) else rewrite(fc);
      if FileExists(CommonSet.TmpPath+'BAR.DAT') then reset(fb) else rewrite(fb);

      try
        with dmPx do
        begin
          taCard.Active:=False;
          taCard.DatabaseName:=CommonSet.TmpPath;
          taCard.TableName:=sCards+'.db';
          taCard.Active:=True;

          taBar.Active:=False;
          taBar.DatabaseName:=CommonSet.TmpPath;
          taBar.TableName:=sBar+'.db';
          taBar.Active:=True;

          for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=CardsView.Controller.SelectedRecords[i];

            iNum:=0;
            rPr:=0;

            for j:=0 to Rec.ValueCount-1 do
            begin
              if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
              if CardsView.Columns[j].Name='CardsViewCena' then rPr:=Rec.Values[j];
            end;

            quFindC3.Active:=False;
            quFindC3.ParamByName('IDC').AsInteger:=iNum;
            quFindC3.Active:=True;

            quBars.Active:=False;
            quBars.ParamByName('GOODSID').AsInteger:=iNum;
            quBars.Active:=True;

            prAddCashLoad(iNum,1,rPr,0);

            if quFindC3EdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

            sName:=prRetDName(quFindC3ID.AsInteger)+quFindC3FullName.AsString;

            if  quFindC3Status.AsInteger>=100 then prAddPosFB(1,quFindC3ID.AsInteger,quFindC3V11.AsInteger,0,quFindC3GoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),0,'','',sName,sMessur,'',0,0,RoundEx(quFindC3Prsision.AsFloat*1000)/1000,rv(rPr))
            else
            begin
              prAddPosFB(1,quFindC3ID.AsInteger,quFindC3V11.AsInteger,0,quFindC3GoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),1,'','',sName,sMessur,'',0,0,RoundEx(quFindC3Prsision.AsFloat*1000)/1000,rv(rPr));
              if ptDStop.FindKey([quFindC3ID.AsInteger]) then
                prAddPosFB(18,quFindC3ID.AsInteger,0,0,0,0,1,'','','','','',0,ptDStopPercent.AsFloat,0,0);
            end;

            sPos:='';

            taCard.Append;
            taCardArticul.AsString:=quFindC3ID.AsString;
            taCardName.AsString:=AnsiToOemConvert(sName);
            taCardMesuriment.AsString:=AnsiToOemConvert(sMessur);
            taCardAdd1.AsString:='';
            taCardAdd2.AsString:='';
            taCardAdd3.AsString:='';
            taCardAddNum1.AsFloat:=0;
            taCardAddNum2.AsFloat:=0;
            taCardAddNum3.AsFloat:=0;
            taCardScale.AsString:='NOSIZE';
            taCardGroop1.AsInteger:=quFindC3GoodsGroupID.AsInteger;
            taCardGroop2.AsInteger:=quFindC3SubGroupID.AsInteger;
            taCardGroop3.AsInteger:=0;
            taCardGroop4.AsInteger:=0;
            taCardGroop5.AsInteger:=0;
            taCardMesPresision.AsFloat:=RoundEx(quFindC3Prsision.AsFloat*1000)/1000;
            taCardPriceRub.AsFloat:=rv(rPr);
            taCardPriceCur.AsFloat:=0;
            taCardClientIndex.AsInteger:=Trunc(quCashRezerv.AsFloat);
            taCardCommentary.AsString:='';

            if quFindC3Status.AsInteger>=100 then taCardDeleted.AsInteger:=0 //��������
            else taCardDeleted.AsInteger:=1;

            taCardModDate.AsDateTime:=Trunc(Date);
            taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
            taCardModPersonIndex.AsInteger:=0;
            taCard.Post;
            delay(10);

            if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';

            if quFindC3Status.AsInteger>=100 then sDel:='0' else sDel:='1';
            sPos:=s1(quFindC3ID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+'0'+sr+sr+sDel+sr+sr+sr+sr;
            try
              writeln(fc,AnsiToOemConvert(sPos));
            except
            end;

            quBars.First;
            while not quBars.Eof do
            begin
              if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
              else sCSz:='QUANTITY';

              prAddPosFB(2,quFindC3ID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(rPr));

              if taBar.Locate('BarCode',quBarsID.AsString,[])=False then
              begin
                sB:='';

                taBar.Append;
                taBarBarCode.AsString:=quBarsID.AsString;
                taBarCardArticul.AsString:=quBarsGoodsID.AsString;
                taBarCardSize.AsString:=sCSz;
                taBarQuantity.AsFloat:=quBarsQuant.AsFloat;
                taBar.Post;

                sB:=quBarsID.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                try
                  writeln(fb,AnsiToOemConvert(sB));
                except
                end;  
              end;

              quBars.Next;
            end;

            quFindC3.Active:=False;
            quBars.Active:=False;
          end;
          taCard.Active:=False;
          taBar.Active:=False;
        end;
      except
        fmSt.Memo1.Lines.Add('������ ������������ ������.');
        fmSt.Memo1.Lines.Add('  �������� ����������!!!');
        bOk:=False;
      end;

      closefile(fc);
      closefile(fb);

      quCash.Active:=False;
      quCash.Active:=True;
      while not quCash.Eof do
      begin
        if quCashLoad.AsInteger=1 then
        begin
          fmSt.Memo1.Lines.Add(' '+quCashName.AsString);
          sPath:=CommonSet.CashPath+'\'+quCashNumber.AsString+'\';

          StrWk:=quCashNumber.AsString;
          while length(StrWk)<2 do strwk:='0'+StrWk;
          StrWk:='cash'+StrWk+'.ldd';
          sCashLdd:=StrWk;

          if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
          begin
            fmSt.Memo1.Lines.Add('');
            fmSt.Memo1.Lines.Add('  ����� ������������ ������.');
            fmSt.Memo1.Lines.Add('  �������� ����������!!!');
            fmSt.Memo1.Lines.Add('  ���������� �����...');
            fmSt.Memo1.Lines.Add('');
            bOk:=False;
          end else
          begin //��� ����� ���������� �������� �����
            //���� �����������
            try
              with dmPx do
              begin
                sNum:=quCashNumber.AsString+'\';

                //���������
                CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                Delay(50);

                CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                StrWk:=quCashNumber.AsString;
                while length(StrWk)<3 do strwk:='0'+StrWk;
                StrWk:='cash'+StrWk+'.db';

                if Fileexists(CommonSet.CashPath+StrWk)=False then
                begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                  CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                  MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                end;

                taList.Active:=False;
                taList.DatabaseName:=CommonSet.TmpPath;
                taList.TableName:=CommonSet.CashPath+sNum+StrWk;
                taList.Active:=True;

                taList.Append;
                taListTName.AsString:=sCards;
                taListDataType.AsInteger:=1;
                taListOperation.AsInteger:=1;
                taList.Post;

                taList.Append;
                taListTName.AsString:=sBar;
                taListDataType.AsInteger:=2;
                taListOperation.AsInteger:=1;
                taList.Post;

                taList.Active:=False;

                if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                delay(50);
                if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
              end; //dmPx
            except
              fmSt.Memo1.Lines.Add('  ������ ������ � �������..');
              bOk:=False;
            end;
          end;

          fmSt.Memo1.Lines.Add(' ��');
        end else
        begin
        //  fmSt.Memo1.Lines.Add(' '+quCashName.AsString+' �� �������.');
        end;

        quCash.Next;
      end;

        //������ �����

      if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
      if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
      if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
      if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

      if bOk then
      begin
         //��������� - ����� �������
        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];

          iNum:=0; rPr:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
            if CardsView.Columns[j].Name='CardsViewCena' then rPr:=Rec.Values[j];
          end;

          //��������
          if iNum>0 then
          begin
            if taCards.FindKey([quCardsID.AsInteger]) then
            begin
              if taCardsStatus.AsInteger>=100 then
              begin
                taCards.Edit;
                taCardsStatus.AsInteger:=250;
                taCards.Post;
              end else
              begin
                taCards.Edit;
                taCardsStatus.AsInteger:=1;
                taCards.Post;
              end;
            end;
          end;

{          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "Goods" Set Status=1');
          quE.SQL.Add('where ID='+its(iNum));
          quE.ExecSQL;}

          //����� ����� � �����

//          rPr:=prFindPrice(iNum);

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "ScalePLU" Set ');
          quE.SQL.Add('Price='+its(RoundEx(rPr*100))+',');
          quE.SQL.Add('Status=0');
          quE.SQL.Add('where GoodsItem='+its(iNum));
          quE.ExecSQL;

        end;
        CardsView.Controller.ClearSelection;

        fmSt.Memo1.Lines.Add('�������� ��������� ��.');
      end
      else //����� ������� ������ �� ����
        fmSt.Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
    finally
      quCash.Active:=False;
      cxButton1.Enabled:=True;
      cxButton2.Enabled:=True;
      dmPx.Session1.Active:=False;
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

procedure TfmCards.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
begin
  //������ ��������
  with dmMc do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;
    try
      ViCenn.BeginUpdate;
      CloseTe(taCen);
      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenReceipt.AsString:=prFindReceipt(iNum);
            taCenDepName.AsString:=fmMainMCryst.Label3.Caption;
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      CloseTe(taCen);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
      ViCenn.EndUpdate;
    end;
  end;
end;

procedure TfmCards.acCalcRemnsExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rSum:Real;
    iC:Integer;
begin
  //�������� ��������
  with dmMc do
  begin
    try
      iC:=0;
      if CardsView.Controller.SelectedRecordCount=0 then exit;
      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
//          rSum:=prFindRemn2(iNum);
          rSum:=prFindTabRemn(iNum);

          quE.Active:=False;
          quE.SQL.Clear;

          quE.SQL.Add('Update Goods Set ');
          quE.SQL.Add('Reserv1='+fs(rSum));
          quE.SQL.Add('where ID='+IntToStr(iNum));
          quE.ExecSQL;

        end;
        inc(iC);
        StatusBar1.Panels[0].Text:=IntToStr(iC); Delay(10);
      end;
    finally
       StatusBar1.Panels[0].Text:='��';
    end;
  end;
end;

procedure TfmCards.acCardsGoodsExecute(Sender: TObject);
begin
// �������� �� �������
  with dmMC do
  begin
    if quCards.RecordCount>0 then
    begin
      quCG2.Active:=False;
      quCG2.ParamByName('IDCARD').AsInteger:=quCardsID.AsInteger;
      quCG2.Active:=True;
      fmCG:=TfmCG.Create(Application);
      fmCG.ShowModal;
    end;
  end;
end;

procedure TfmCards.acPostTovExecute(Sender: TObject);
begin
//���������� ������
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if not quCards.Eof then
    begin
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=quCardsId.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=quCardsId.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=quCardsName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmCards.CardsViewSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
// ����� ���
//  ShowMessage('���');

  if CardsView.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost1.Active:=False;
    quPost1.ParamByName('IDCARD').Value:=quCardsId.AsInteger;
    quPost1.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmCards.cxButton4Click(Sender: TObject);
begin
  if le1.Visible then
  begin
    Le1.Visible:=False;
    Le2.Visible:=True;
  end else
  begin
    Le1.Visible:=True;
    Le2.Visible:=False;
  end;
end;

procedure TfmCards.cxButton6Click(Sender: TObject);
begin
  ViCenn.BeginUpdate;
  CloseTe(dmMC.taCen);
  ViCenn.EndUpdate;
  StatusBar1.Panels[1].Text:='������� �����.';
end;

procedure TfmCards.acAddPrintExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rQ:Real;
begin
  with dmMc do
  begin
    if taCen.Active=False then CloseTe(taCen);
    ViCenn.BeginUpdate;
    if CardsView.Controller.SelectedRecordCount=0 then exit;
    for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
    begin
      Rec:=CardsView.Controller.SelectedRecords[i];

      iNum:=0;
      rQ:=0;
      for j:=0 to Rec.ValueCount-1 do
      begin
        if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewReserv1' then rQ:=Rec.Values[j];
      end;

      if iNum>0 then
      begin
        quFindC4.Active:=False;
        quFindC4.ParamByName('IDC').AsInteger:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          taCen.Append;
          taCenIdCard.AsInteger:=iNum;
          taCenFullName.AsString:=quFindC4FullName.AsString;
          taCenCountry.AsString:=quFindC4NameCu.AsString;
          taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
          taCenPrice2.AsFloat:=0;
          taCenDiscount.AsFloat:=0;
          if Length(quFindC4BarCode.AsString)=7 then taCenBarCode.AsString:=ToStandart(quFindC4BarCode.AsString)
          else taCenBarCode.AsString:=quFindC4BarCode.AsString;
          taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
          if quFindC4EdIzm.AsInteger=2 then taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
          taCenReceipt.AsString:=prFindReceipt(iNum);
          taCenQuanrR.AsFloat:=rQ;
          taCenDepName.AsString:=fmMainMCryst.Label3.Caption;
          taCensDisc.AsString:=prSDisc(iNum);
          taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
          taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
          taCenV02.AsInteger:=quFindC4V02.AsInteger;
          taCenV12.AsInteger:=quFindC4V12.AsInteger;
          taCenMaker.AsString:=quFindC4NAMEM.AsString;
          taCen.Post;
        end;

        quFindC4.Active:=False;
      end;
    end;

    ViCenn.EndUpdate;

    StatusBar1.Panels[1].Text:='� ������� - '+IntToStr(taCen.RecordCount)+' ��������.'
  end;
end;

procedure TfmCards.acPrintQuPrintExecute(Sender: TObject);
//var vPP:TfrPrintPages;
begin
  with dmMc do
  begin
    if taCen.Active and (taCen.RecordCount>0) then
    begin
      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      RepCenn.ShowPreparedReport;

      fmMainMCryst.cxDEdit1.Date:=Date;
      if MessageDlg('�������� �������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        CloseTe(taCen);
        StatusBar1.Panels[1].Text:='������� �����.';
      end;
    end;

//    vPP:=frAll;
//    RepCenn.PrintPreparedReport('',1,False,vPP);
//    CloseTe(taCen);
//    StatusBar1.Panels[1].Text:='������� �����.';
//      fmMainMCryst.cxDEdit1.Date:=Date;

  end;
end;

procedure TfmCards.cxButton5Click(Sender: TObject);
begin
  acPrintQuPrint.Execute;
end;

procedure TfmCards.acTermFileExecute(Sender: TObject);
Var CName:String;
    i,j,iNum,iC:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    f:TextFile;
    Str1,Str2:String;
begin
 //���������� ����� ��� ���������
 iC:=0;
  with dmMc do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    try
      assignfile(f,CurDir+'Referenc.dat');
      rewrite(f);

      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;
        rPr:=0;
        CName:='';

        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
          if CardsView.Columns[j].Name='CardsViewCena' then rPr:=Rec.Values[j];
          if CardsView.Columns[j].Name='CardsViewName' then CName:=Rec.Values[j];
        end;


        quBars.Active:=False;
        quBars.ParamByName('GOODSID').AsInteger:=iNum;
        quBars.Active:=True;
        quBars.First;
        while not quBars.Eof do
        begin

          Str1:=quBarsID.AsString;
          while Length(Str1)<13 do Str1:=Str1+' ';
          Str1:=Str1+intToStr(iNum);
          while Length(Str1)<18 do Str1:=Str1+' ';

          Str2:=FloatToStr(RoundVal(rPr));
          while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
          Str1:=Str1+Str2;
          while Length(Str1)<27 do Str1:=Str1+' ';

          Str1:=Str1+Copy(AnsiToOemConvert(CName),1,22);
          while Length(Str1)<49 do Str1:=Str1+' ';

          writeln(f,Str1);

          inc(iC);

          quBars.Next;
        end;

        quBars.Active:=False;
      end;
      CardsView.Controller.ClearSelection;
    finally
      CloseFile(f);
      ShowMessage('�������� � ���� - '+IntToStr(iC)+' ����������.');
    end;
  end;
end;

procedure TfmCards.acTPrintExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;
begin
  //������ �� �����
  if CardsView.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
              //              prWritePrinter(StrWk+#$0D+#$0A); ��� ��� ��������
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if fmQuant.cxCheckBox1.Checked then
          begin  //� �������
            if taCen.Active=False then CloseTe(taCen);
            ViCenn.BeginUpdate;
            for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
            begin
              Rec:=CardsView.Controller.SelectedRecords[i];

              iNum:=0;
              for j:=0 to Rec.ValueCount-1 do
                if CardsView.Columns[j].Name='CardsViewID' then begin  iNum:=Rec.Values[j]; break; end;

              if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  taCen.Append;
                  taCenIdCard.AsInteger:=iNum;
                  taCenFullName.AsString:=quFindC4FullName.AsString;
                  taCenCountry.AsString:=quFindC4NameCu.AsString;
                  taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
                  taCenPrice2.AsFloat:=0;
                  taCenDiscount.AsFloat:=0;
                  taCenBarCode.AsString:=quFindC4BarCode.AsString;
                  taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
                  taCenQuant.AsInteger:=fmQuant.cxSpinEdit1.Value;
                  if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
                  taCenReceipt.AsString:=prFindReceipt(iNum);
                  taCenDepName.AsString:=fmMainMCryst.Label3.Caption;
                  taCensDisc.AsString:=prSDisc(iNum);
                  taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
                  taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
                  taCenV02.AsInteger:=quFindC4V02.AsInteger;
                  taCenV12.AsInteger:=quFindC4V12.AsInteger;
                  taCenMaker.AsString:=quFindC4NAMEM.AsString;
                  taCen.Post;
                end;
                quFindC4.Active:=False;
              end;
            end;

            ViCenn.EndUpdate;

            StatusBar1.Panels[1].Text:='� ������� - '+IntToStr(taCen.RecordCount)+' ��������.'
          end else //������ -  ������ ������ �� �������
          begin
            if prOpenPrinter(CommonSet.TPrintN) then
            begin
              for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
              begin
                Rec:=CardsView.Controller.SelectedRecords[i];

                iNum:=0;
                for j:=0 to Rec.ValueCount-1 do
                  if CardsView.Columns[j].Name='CardsViewID' then begin  iNum:=Rec.Values[j]; break; end;

                if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
                begin
                  quFindC4.Active:=False;
                  quFindC4.ParamByName('IDC').AsInteger:=iNum;
                  quFindC4.Active:=True;
                  if quFindC4.RecordCount>0 then
                  begin
                    for k:=0 to Str.Count-1 do
                    begin
                      StrWk:=Str[k];
                      if pos('@38.3@',StrWk)>0 then
                      begin
                        insert('E30',StrWk,pos('@38.3@',StrWk));
                        delete(StrWk,pos('@38.3@',StrWk),6)
                      end;

                      if pos('@',StrWk)>0 then
                      begin //���� ��� �� ���� ����������??
                        Str1:=Strwk;

                        Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                        delete(Str1,1,pos('@',Str1));//3.30.36@"

                        Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                        delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                        Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                        if Str4='3' then   //��������
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //30.36
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=20;
                          Str5:=Copy(quFindC4FullName.AsString,1,l);
                          if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                        end;
                        if Str4='5' then  //������
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //5.30.36
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=20;
                          Str5:=Copy(quFindC4NameCu.AsString,1,l);
                          if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                        end;
                        if Str4='7' then  //�������     7.6.36
                        begin
                          Str5:=INtToStr(iNum);
                        end;
                        if Str4='18' then   //����   18.10.36
                        begin
                          Str5:=ToStr(quFindC4Cena.AsFloat);
                        end;
                        if Str4='31' then    //��
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //31.13.8
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=13;

                          //�� �� ������� ����
                          sBar:=quFindC4BarCode.AsString;
                          if Length(sBar)<13 then
                          begin
                            if Length(sBar)=7 then //������� ����
                            begin
                              sBar:=ToStandart(sBar);
                              l:=13;
                            end;
                          end;
                          Str5:=Copy(sBar,1,l);
                        end;
                        if Str4='39' then   //���-��
                        begin
                          Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                        end;
                        StrWk:=Str2+Str5+Str1;
                      end;
                      prWritePrinter(StrWk+#$0D+#$0A);
                    end;
                  end;
                  quFindC4.Active:=False;
                end;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;


procedure TfmCards.cxButton7Click(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    k,l:INteger;
    sBar:String;
begin
  with dmMc do
  begin
    if taCen.Active and (taCen.RecordCount>0) then
    begin
      Str:=TStringList.Create;
      try
        if FileExists(CurDir+'TRF\Termo.trf') then
        begin
          assignfile(f,CurDir+'TRF\Termo.trf');
          try
            reset(f);
            while not EOF(f) do
            begin
              ReadLn(f,StrWk);
              if StrWk[1]<>'#' then
              begin
                if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
                begin //��� ����
                  if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
                end else
                begin // � �����
                  Str.Add(StrWk);
                end;
 //              prWritePrinter(StrWk+#$0D+#$0A);
              end;
            end;
          finally
            closefile(f);
          end;
          if prOpenPrinter(CommonSet.TPrintN) then
          begin
            taCen.First;
            while not taCen.Eof do
            begin
              for k:=0 to Str.Count-1 do
              begin
                StrWk:=Str[k];
                if pos('@38.3@',StrWk)>0 then
                begin
                  insert('E30',StrWk,pos('@38.3@',StrWk));
                  delete(StrWk,pos('@38.3@',StrWk),6)
                end;

                if pos('@',StrWk)>0 then
                begin //���� ��� �� ���� ����������??
                  Str1:=Strwk;

                  Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                  delete(Str1,1,pos('@',Str1));//3.30.36@"

                  Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                  delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                  Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                  if Str4='3' then   //��������
                  begin
                    Str5:=Str3;
                    delete(Str5,1,pos('.',Str5)); //30.36
                    Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                    l:=StrToIntDef(Str5,0);
                    if l=0 then l:=20;
                    Str5:=Copy(taCenFullName.AsString,1,l);
                    if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                  end;
                  if Str4='5' then  //������
                  begin
                    Str5:=Str3;
                    delete(Str5,1,pos('.',Str5)); //5.30.36
                    Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                    l:=StrToIntDef(Str5,0);
                    if l=0 then l:=20;
                    Str5:=Copy(taCenCountry.AsString,1,l);
                    if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                  end;
                  if Str4='7' then  //�������     7.6.36
                  begin
                    Str5:=INtToStr(taCenIdCard.AsInteger);
                  end;
                  if Str4='18' then   //����   18.10.36
                  begin
                    Str5:=ToStr(taCenPrice1.AsFloat);
                  end;
                  if Str4='31' then    //��
                  begin
                    Str5:=Str3;
                    delete(Str5,1,pos('.',Str5)); //31.13.8
                    Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                    l:=StrToIntDef(Str5,0);
                    if l=0 then l:=13;

                    //�� �� ������� ����
                    sBar:=taCenBarCode.AsString;
                    if Length(sBar)<13 then
                    begin
                      if Length(sBar)=7 then //������� ����
                      begin
                        sBar:=ToStandart(sBar);
                        l:=13;
                      end;
                    end;
                    Str5:=Copy(sBar,1,l);
                  end;
                  if Str4='39' then   //���-��
                  begin
                    Str5:=IntToStr(taCenQuant.AsInteger);
                  end;
                  StrWk:=Str2+Str5+Str1;
                end;
                prWritePrinter(StrWk+#$0D+#$0A);
              end;
              taCen.Next;
            end;
            prClosePrinter(CommonSet.TPrintN);
          end;
        end;
      finally
        Str.Free;
      end;
      if MessageDlg('�������� �������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        CloseTe(taCen);
        StatusBar1.Panels[1].Text:='������� �����.';
      end;
    end;
  end;
end;

procedure TfmCards.acScaleInExecute(Sender: TObject);
Var iNum,j:INteger;
    Rec:TcxCustomGridRecord;
    Str1:String;
begin
 //� �����
  if CardsView.Controller.SelectedRecordCount>0 then
  begin
    Rec:=CardsView.Controller.SelectedRecords[0];

    iNum:=0;
     for j:=0 to Rec.ValueCount-1 do
     begin
       if CardsView.Columns[j].Name='CardsViewID' then begin iNum:=Rec.Values[j]; break; end;
     end;

     Str1:=prFindPlu(iNum);

     showmessage('��� ������ '+INtToStr(iNum)+' � �����: '+Str1);
  end;
end;

procedure TfmCards.acAddInScaleExecute(Sender: TObject);
Var
    i,j,iNum,iSrok:INteger;
    Rec:TcxCustomGridRecord;
    sName,sBar:String;
    rPrice:Real;
begin
  //�������� � ����
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMC do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
    begin
      Rec:=CardsView.Controller.SelectedRecords[i];

      iNum:=0;
      iSrok:=0;
      rPrice:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewBarCode' then sBar:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewFullName' then sName:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewSrokReal' then iSrok:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewCena' then rPrice:=Rec.Values[j];
      end;

      if (iNum>0)and(sBar[1]='2')and((sBar[2]='2')or(sBar[2]='1')or(sBar[2]='3')) then
      begin
        taToScale.Append;
        taToScaleId.AsInteger:=iNum;
        taToScaleBarcode.AsString:=sBar;
        taToScaleName.AsString:=sName;
        taToScaleSrok.AsInteger:=iSrok;
        taToScalePrice.AsFloat:=rPrice;
        taToScale.Post;
      end;
    end;
    quScSpr.Active:=False;
    quScSpr.Active:=True;
    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then  prAddToScale(quScSprNumber.AsString);

    quScSpr.Active:=False;
    taToScale.Active:=False;
    CardsView.Controller.ClearSelection;
  end;
  fmAddInScale.Release;
end;

procedure TfmCards.acRecalcRemn1Execute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rSum:Real;
    iC:Integer;
begin
  //�������� ��������
  with dmMc do
  begin
    try
      iC:=0;
      if CardsView.Controller.SelectedRecordCount=0 then exit;
      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then
          begin
            iNum:=Rec.Values[j];
            break;
          end;
        end;

        if iNum>0 then
        begin
          rSum:=prFindTabRemn(iNum);
          prSetR(iNum,rSum);

{          quE.Active:=False;
          quE.SQL.Clear;

          quE.SQL.Add('Update Goods Set ');
          quE.SQL.Add('Reserv1='+fs(rSum));
          quE.SQL.Add('where ID='+IntToStr(iNum));
          quE.ExecSQL;
}
        end;
        inc(iC);
        StatusBar1.Panels[0].Text:=IntToStr(iC); Delay(10);
      end;
    finally
       StatusBar1.Panels[0].Text:='��';
    end;
  end;
end;

procedure TfmCards.acLoadScaleExecute(Sender: TObject);
Var i,j,iNum,iSrok:INteger;
    Rec:TcxCustomGridRecord;
    sBar,sItem:String;
    bErr:Boolean;
    iL,iP,n:INteger;
    StrIp:String;
    rPrice:Real;
    bFormat:byte;
begin
//�������� � ���� ����������� ������
  with dmMC do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    sItem:='';

    for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
    begin
      Rec:=CardsView.Controller.SelectedRecords[i];

      iNum:=0; iSrok:=0; rPrice:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewBarCode' then sBar:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewSrokReal' then iSrok:=Rec.Values[j];
        if CardsView.Columns[j].Name='CardsViewCena' then rPrice:=Rec.Values[j];
      end;
      if (iNum>0)and(sBar[1]='2')and((sBar[2]='2')or(sBar[2]='1')or(sBar[2]='3')) then
      begin
        //������� �������� -  ����� ����������� ���������
        sItem:=sItem+','+INtToStr(iNum);

        //����� ����� ���� � ����
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "ScalePLU" Set ');
        quE.SQL.Add('Price='+its(RoundEx(rPrice*100))+',');
        quE.SQL.Add('ShelfLife='+its(iSrok));
        quE.SQL.Add('where GoodsItem='+its(iNum));
        quE.ExecSQL; //���� ���� - �� ���� ���������
      end;
    end;
    if length(sItem)>500 then
    begin
      showmessage('������� ����� ������ �������, ��������� ���������. �������� ����������.');
      exit;
    end;

    if sItem>'' then delete(sItem,1,1); //������ ������ �������
    //����������� ������ �����
    //��� ����
    quSc.Active:=False;
    quSc.SQL.Clear;
    quSc.SQL.Add('select Number from "ScalePLU"');
    quSc.SQL.Add('where GoodsItem in ('+sItem+')');
    quSc.SQL.Add('group by Number');
    quSc.SQL.Add('Order by Number');
    quSc.Active:=True;
    if quSc.RecordCount>0 then
    begin
      bErr:=False;
      
      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('�������� �����.');
      fmSt.Memo1.Lines.Add(' --------');
      fmSt.Memo1.Lines.Add(sItem);
      fmSt.Memo1.Lines.Add(' --------');
      quSc.First;
      while not quSc.Eof do
      begin
        fmSt.Memo1.Lines.Add('  ���� � '+quScNUmber.AsString);
        if quScale.Active=False then quScale.Active:=True;
        quScale.Locate('Number',quScNUmber.AsString,[]);
        if quScaleModel.AsString='DIGI_TCP' then
        begin
          for n:=1 to 5 do
          begin
            StrIp:='';
            if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
            if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
            if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
            if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
            if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
            iP:=StrToINtDef(StrIp,0);

            if iP>0 then //���-�� ���������� - ������
            begin
              fmSt.Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP); delay(10);
              iL:=0;

              fmSt.Memo1.Lines.Add('    ��������� ������ ..'+quScNUmber.AsString);

              quScI.Active:=False;
              quScI.SQL.Clear;
              quScI.SQL.Add('select * from "ScalePLU"');
              quScI.SQL.Add('where GoodsItem in ('+sItem+')');
              quScI.SQL.Add('and NUmber ='''+quScNUmber.AsString+'''');
              quScI.SQL.Add('Order by GoodsItem');
              quScI.Active:=True;
              quScI.First;
              while not quScI.Eof do
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  bFormat:=quScIMessageNo.AsInteger;
                  AddPlu(iL,quScIPLU.AsInteger,quScIShelfLife.AsInteger,quScIGoodsItem.AsString,AnsiToOemConvert(quScIFirstName.AsString),AnsiToOemConvert(quScILastName.AsString),quScIPrice.asfloat/100,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  fmSt.Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    fmSt.Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                end;
                quScI.Next;
              end;
              quScI.Active:=False;
              if iL>0 then
              begin
                if LoadScale(CommonSet.sMask+StrIP,iL) then  fmSt.Memo1.Lines.Add('      ....')
                else
                begin
                  bErr:=True;
                  fmSt.Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                end;
                delay(10);
              end;
            end;
          end;
        end;
        quSc.Next;
      end;
      if not bErr then
      begin
        fmSt.Memo1.Lines.Add('��.');

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "ScalePLU" Set ');
        quE.SQL.Add('Status=1');
        quE.SQL.Add('where GoodsItem in ('+sItem+')');
        quE.ExecSQL;

      end else
      begin
        fmSt.Memo1.Lines.Add('��� �������� �������� ������.');
      end;
      fmSt.cxButton1.Enabled:=True;
    end else
    begin
      showmessage('��������� ������, �������� ������� ���� � ���� ('+sItem+')');
    end;
    quSc.Active:=False;

    CardsView.Controller.ClearSelection;
  end;
end;

procedure TfmCards.acReceiptExecute(Sender: TObject);
Var iNum,j:Integer;
    Rec:TcxCustomGridRecord;
begin
  //������
  if CardsView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CardsView.Controller.SelectedRecords[0];

  iNum:=0;

  for j:=0 to Rec.ValueCount-1 do
    if CardsView.Columns[j].Name='CardsViewID' then begin iNum:=Rec.Values[j]; break; end;

  if iNUm>0 then
  begin
    fmReceipt:=TfmReceipt.Create(Application);
    try
      with dmMT do
      begin
        if taACards.Active=False then taACards.Active:=True;
        if taACards.FindKey([iNum]) then fmReceipt.cxTextEdit1.Text:=taACardsRECEIPT.AsString
        else fmReceipt.cxTextEdit1.Text:='';
        fmReceipt.ShowModal;

        if taACards.FindKey([iNum]) then
        begin
          taACards.Edit;
          taACardsID.AsInteger:=iNum;
          taACardsRECEIPT.AsString:=fmReceipt.cxTextEdit1.Text;
          taACards.Post;
        end else
        begin
          taACards.Append;
          taACardsID.AsInteger:=iNum;
          taACardsRECEIPT.AsString:=fmReceipt.cxTextEdit1.Text;
          taACards.Post;
        end;
      end;
    finally
      fmReceipt.Release;
    end;
  end;
end;

procedure TfmCards.cxButton8Click(Sender: TObject);
begin
//������� � Excel
  prNExportExel5(ViCenn);
end;

procedure TfmCards.acSelectAllExecute(Sender: TObject);
begin
  //��������� ���
  CardsView.Controller.SelectAll;

end;

procedure TfmCards.acViewLoadExecute(Sender: TObject);
Var iDate:INteger;
begin
  if CardsView.Controller.SelectedRecordCount=0 then exit;
  with dmMt do
  with dmMC do
  begin
    if ptGdsLoadSel.Active=False then ptGdsLoadSel.Active:=True;
    iDate:=Trunc(Date);
    ptGdsLoadSel.CancelRange;
    ptGdsLoadSel.SetRange([quCardsID.AsInteger,iDate-10],[quCardsID.AsInteger,iDate+1]);

    fmGdsLoad:=tfmGdsLoad.Create(Application);
    fmGdsLoad.ViewLoad.Tag:=quCardsID.AsInteger;
    fmGdsLoad.ShowModal;
    fmGdsLoad.Release;
  end;
end;

procedure TfmCards.CardsViewStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
//  iDirect:=10;
  bChGroup:=True;
end;

procedure TfmCards.CardsTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bChGroup then  Accept:=True;
end;

procedure TfmCards.CardsTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j,iR: Integer;
    iNum,iGr: Integer;
    Rec:TcxCustomGridRecord;
    sGr:String;
begin
  //��������
  bChGroup:=False;

  if CommonSet.Single=0 then exit;

  sGr:=CardsTree.DropTarget.Text;
  iGr:=Integer(CardsTree.DropTarget.data);

  iCo:=CardsView.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    iR:= MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0);
    if iR=6 then
    begin
      with dmMT do
      begin
        if taCards.Active=False then taCards.Active:=True;

        CardsView.BeginUpdate;
        try
          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            if taCards.FindKey([iNum]) then
            begin
              taCards.Edit;
              taCardsGoodsGroupID.AsInteger:=iGr;
              taCards.Post;
            end;
          end;
        finally
          delay(100);
          prRefreshCards(Integer(CardsTree.Selected.Data),cxCheckBox1.Checked);
          CardsView.EndUpdate;
          fmCards.cxCheckBox2.Checked:=false;
        end;
      end;
    end else showmessage('�� '+its(iR));
  end;
end;

procedure TfmCards.acCalcMoveLineExecute(Sender: TObject);
begin
  with dmMC do
  with dmMT do
  begin
    if not quCards.Eof then
    begin
      if MessageDlg('����������� �������� �� '+its(quCardsId.AsInteger)+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        prReCalcTMoveLine(quCardsId.AsInteger);
        StatusBar1.Panels[0].Text:='��';
      end;
    end
  end;
end;

procedure TfmCards.acPartSelExecute(Sender: TObject);
begin
  if not CanDo('prViewPartCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  if not dmMc.quCards.Eof then prFillPart(dmMc.quCardsID.AsInteger,dmMc.quCardsName.AsString);
end;

procedure TfmCards.acSearchCodeExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmCards.FormShow(Sender: TObject);
begin
  if CommonSet.Single=1 then
  begin
    CardsViewKol.Visible:=True;
    CardsViewV06.Visible:=True;
    CardsViewV07.Visible:=True;
    N21.Visible:=True;
  end else
  begin
    CardsViewKol.Visible:=False;
    CardsViewV06.Visible:=False;
    CardsViewV07.Visible:=False;
    N21.Visible:=False;
  end;
  CardsViewRemn.Visible:=false;        //p
  cxcheckbox2.Checked:=false;
end;

procedure TfmCards.acSetGroupExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
begin
  //��������� ��������� �� ���������
  if not CanDo('prSetSelectCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  if CommonSet.Single<>1 then begin StatusBar1.Panels[0].Text:='�������� ������.';  exit; end;
  with dmMC do
  with dmMT do
  begin
    if CardsView.Controller.SelectedRecordCount=0 then exit;

    if quAlgClass.Active=False then quAlgClass.Active:=True;

{
    fmSetCardsParams.cxCheckBox1.Checked:=False;
    fmSetCardsParams.cxCheckBox2.Checked:=False;
    fmSetCardsParams.cxCheckBox3.Checked:=False;
    fmSetCardsParams.cxCheckBox4.Checked:=False;
    fmSetCardsParams.cxCheckBox5.Checked:=False;
    fmSetCardsParams.cxCheckBox6.Checked:=False;
}
    fmSetCardsParams.Label1.Caption:='�������� - ' +its(CardsView.Controller.SelectedRecordCount)+' ��������.';
    fmSetCardsParams.ShowModal;
    if fmSetCardsParams.ModalResult=mrOk then
    begin
      if fmSetCardsParams.cxCheckBox1.Checked
      or fmSetCardsParams.cxCheckBox2.Checked
      or fmSetCardsParams.cxCheckBox3.Checked
      or fmSetCardsParams.cxCheckBox4.Checked
      or fmSetCardsParams.cxCheckBox5.Checked
      or fmSetCardsParams.cxCheckBox6.Checked
      then
      begin
        CloseTe(teBuff);

        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];
          iNum:=0;
          for j:=0 to Rec.ValueCount-1 do
            if CardsView.Columns[j].Name='CardsViewID' then begin iNum:=Rec.Values[j]; break; end;;

          if (iNum>0) then
          begin
            teBuff.Append; teBuffCode.AsInteger:=iNum; teBuff.Post;
          end;
        end;

        fmCards.StatusBar1.Panels[0].Text:='����� ���� ��������� �������� ��������.'; delay(10);

        taCards.Refresh;

        teBuff.First;
        while not teBuff.Eof do
        begin
          if taCards.FindKey([teBuffCode.AsInteger]) then
          begin
            taCards.Edit;
            if fmSetCardsParams.cxCheckBox1.Checked then begin taCardsV06.AsInteger:=fmSetCardsParams.cxSpinEdit1.Value end;
            if fmSetCardsParams.cxCheckBox2.Checked then begin taCardsV07.AsInteger:=fmSetCardsParams.cxSpinEdit2.Value end;
            if fmSetCardsParams.cxCheckBox3.Checked then begin taCardsKol.AsInteger:=fmSetCardsParams.cxCalcEdit3.EditValue end;
            if fmSetCardsParams.cxCheckBox4.Checked then begin taCardsV10.AsInteger:=RoundEx(fmSetCardsParams.cxCalcEdit1.EditValue*1000) end;
            if fmSetCardsParams.cxCheckBox5.Checked then
            begin
              if fmSetCardsParams.cxLookupComboBox1.EditValue>0 then
                taCardsV11.AsInteger:=fmSetCardsParams.cxLookupComboBox1.EditValue
              else
                taCardsV11.AsInteger:=0;
            end;
            if fmSetCardsParams.cxCheckBox6.Checked then
            begin
              if fmSetCardsParams.cxButtonEdit1.Tag>0 then
                taCardsV12.AsInteger:=fmSetCardsParams.cxButtonEdit1.Tag
              else
                taCardsV12.AsInteger:=0;
            end;
            taCards.Post;
          end;

          teBuff.Next;
        end;

        teBuff.Active:=False;

        try
          CardsView.BeginUpdate;
          CardsView.Controller.ClearSelection;
          prRefreshCards(Integer(CardsTree.Selected.Data),cxCheckBox1.Checked);
          fmCards.cxCheckBox2.Checked:=false;
        finally
          CardsView.EndUpdate;
        end;
      end;
      fmCards.StatusBar1.Panels[0].Text:='Ok.'; delay(10);
    end;
  end;
end;

procedure TfmCards.acGetVRealExecute(Sender: TObject);
Var // iDate:INteger;
    rRemn,rSpeedDay,rQDay:Real;
    iCard:Integer;
begin
  //�������� ������
  with dmMt do
  begin
    iCard:=dmMc.quCardsID.AsInteger;
    if taCards.FindKey([iCard]) then
    begin
//      iDate:=Trunc(Date);
      CloseTe(teAvSp);
      rRemn:=prFindTabRemn(iCard);
      prFindTabSpeedRemn(iCard,1,rSpeedDay,rQDay);
      fmAvSpeed.Label3.Caption:=OemToAnsiConvert(taCardsName.AsString);
      fmAvSpeed.Label4.Caption:=its(iCard);
      fmAvSpeed.cxCalcEdit1.Value:=rSpeedDay;
      fmAvSpeed.cxCalcEdit2.Value:=rRemn;
      fmAvSpeed.cxCalcEdit3.Value:=rQDay;
      fmAvSpeed.Label9.Caption:=its(CommonSet.SpeedReadPrihods);
      fmAvSpeed.Label11.Caption:=its(CommonSet.SpeedRealDays);
      fmAvSpeed.Label13.Caption:=its(CommonSet.SpeedRealNotLasDays);
      fmAvSpeed.Label16.Caption:=its(CommonSet.SpeedRealKDov);

      fmAvSpeed.ShowModal;
    end;
  end;
end;

procedure TfmCards.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    acReadBar1.Execute;
  end;
end;

procedure TfmCards.acLoadSDiscExecute(Sender: TObject);
Var i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
begin
  //�������� ����
  with dmMc do
  with dmMt do
  begin
    try
      if CardsView.Controller.SelectedRecordCount=0 then exit;
      if fmSt.cxButton1.Enabled=False then exit;

      if taCards.Active=False then taCards.Active:=True;
      if ptDStop.Active=False then ptDStop.Active:=True;

      fmSt.cxButton1.Enabled:=False;
      fmSt.Memo1.Clear;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('���� �������� ����..');

      if dmFB.Cash.Connected then fmSt.Memo1.Lines.Add('  FB ����� ����.') else fmSt.Memo1.Lines.Add('  ������ PX.');

      for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
      begin
        Rec:=CardsView.Controller.SelectedRecords[i];

        iNum:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
        end;

        if ptDStop.FindKey([iNum]) then
        begin
          prAddPosFB(18,iNum,0,0,0,0,1,'','','','','',0,ptDStopPercent.AsFloat,0,0);

          if ptDStopPercent.AsFloat>99 then fmSt.Memo1.Lines.Add('  ��� '+its(iNum)+' ������ �� ���������')
          else fmSt.Memo1.Lines.Add('  ��� '+its(iNum)+' ������ ���������');

        end
        else
        begin
          prAddPosFB(18,iNum,0,0,0,0,1,'','','','','',0,0,0,0);
          fmSt.Memo1.Lines.Add('  ��� '+its(iNum)+' ������ ���������');
        end;
      end;

      fmSt.Memo1.Lines.Add('�������� ��������� ��.');

    finally
      cxButton1.Enabled:=True;
      cxButton2.Enabled:=True;
      fmSt.cxButton1.Enabled:=True;
      fmSt.cxButton1.SetFocus;
    end;
  end;
end;

procedure TfmCards.acPrintCutTailExecute(Sender: TObject);
 //������ �������� �� �������
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    j,iNum,k,m:INteger;
    Rec:TcxCustomGridRecord;
    sBar,sBarCode:String;
    iCount,iNumRec:INteger;
begin
  //������ �� �����
  if CardsView.Controller.SelectedRecordCount=0 then exit;

  Rec:=CardsView.Controller.SelectedRecords[0]; //������ ����������

  iNum:=0;
  for j:=0 to Rec.ValueCount-1 do
  begin
    if CardsView.Columns[j].Name='CardsViewID' then iNum:=Rec.Values[j];
    if CardsView.Columns[j].Name='CardsViewBarCode' then sBar:=Rec.Values[j];
  end;

  if iNum=CommonSet.CutTailCode then //��� �������
  begin
    fmQuant:=TfmQuant.Create(Application);
    fmQuant.cxSpinEdit1.Value:=1;
    fmQuant.cxCheckBox1.Checked:=False;
    fmQuant.ShowModal;
    if fmQuant.ModalResult=mrOk then
    begin
      Str:=TStringList.Create;
      try
        if FileExists(CurDir+'TRF\Termo.trf') then
        begin
          assignfile(f,CurDir+'TRF\Termo.trf');
          try
            reset(f);
            while not EOF(f) do
            begin
              ReadLn(f,StrWk);
              if StrWk[1]<>'#' then
              begin
                if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
                begin //��� ����
                  if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
                end else
                begin // � �����
                  Str.Add(StrWk);
                end;
              //              prWritePrinter(StrWk+#$0D+#$0A); ��� ��� ��������
              end;
            end;
          finally
            closefile(f);
          end;

          iCount:=fmQuant.cxSpinEdit1.Value;
          if (iCount>0)and(iCount<100)then
          begin
            if prOpenPrinter(CommonSet.TPrintN) then
            begin
              for m:=1 to iCount do
              begin
                //��������� �� �� �������
                iNumRec:=prGetNum(101); //����� �������
                if iNumRec<100000 then
                begin
                  Str1:=Its(iNumRec);
                  while length(Str1)<5 do Str1:='0'+Str1;

                  sBarCode:=sBar+Str1; //������ ���� ��� 12 ��������
                  sBarCode:=ToStandart(sBarCode); //�������� �� ������������

                  //����� ��������

                  for k:=0 to Str.Count-1 do
                  begin
                    StrWk:=Str[k];
                    if pos('@38.3@',StrWk)>0 then
                    begin
                      insert('E30',StrWk,pos('@38.3@',StrWk));
                      delete(StrWk,pos('@38.3@',StrWk),6)
                    end;

                    if pos('@',StrWk)>0 then
                    begin //���� ��� �� ���� ����������??
                      Str1:=Strwk;

                      Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                      delete(Str1,1,pos('@',Str1));//3.30.36@"

                      Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                      delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                      Str4:=Copy(Str3,1,pos('.',Str3)-1); //3

                      if Str4='3' then   //��������
                      begin
                        Str5:='������� � '+its(iNumRec);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;

                      if Str4='5' then  //������
                      begin
                        Str5:='';
                      end;
                      if Str4='7' then  //�������     7.6.36
                      begin
                        Str5:=INtToStr(iNum);
                      end;
                      if Str4='18' then   //����   18.10.36
                      begin
                        Str5:='';
                      end;
                      if Str4='31' then    //��
                      begin
                        Str5:=sBarCode;
                      end;
                      if Str4='39' then   //���-��
                      begin
                        Str5:='1';
                      end;
                      StrWk:=Str2+Str5+Str1;
                    end;
                    prWritePrinter(StrWk+#$0D+#$0A);
                  end;
                end;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      finally
        Str.Free;
      end;
    end;
    fmQuant.Release;
  end;
end;

procedure TfmCards.acSaveAlcoExecute(Sender: TObject);
begin
  with dmMt do
  begin
    ptCardsSaveAlco.Active:=False;
    ptCardsSaveAlco.Active:=True;
    if taCards.Active=False then taCards.Active:=True;

    taCards.First;
    while not taCards.Eof do
    begin
      if ptCardsSaveAlco.FindKey([taCardsID.AsInteger]) then ptCardsSaveAlco.Edit
      else
      begin
        ptCardsSaveAlco.Append;
        ptCardsSaveAlcoID.AsInteger:=taCardsID.AsInteger;
      end;

      ptCardsSaveAlcoIGR1.AsInteger:=taCardsGoodsGroupID.AsInteger;
      ptCardsSaveAlcoIGR2.AsInteger:=taCardsSubGroupID.AsInteger;
      ptCardsSaveAlcoIGR3.AsInteger:=0;
      ptCardsSaveAlcoV10.AsInteger:=taCardsV10.AsInteger;
      ptCardsSaveAlcoV11.AsInteger:=taCardsV11.AsInteger;
      ptCardsSaveAlcoV12.AsInteger:=taCardsV12.AsInteger;
      ptCardsSaveAlco.Post;

      taCards.Next;
    end;

    ptCardsSaveAlco.Active:=False;

    ShowMessage('��');

  end;
end;

procedure TfmCards.cxCheckBox2Click(Sender: TObject);        //p
begin
  with dmMC do
  with dmP do
  begin
    if (quCards.Active=false) then exit;
    if (cxCheckBox2.Checked=true) then
    begin

      CardsView.BeginUpdate;
      prflagcheck(1);
      quCards.First;
      while not quCards.Eof do
      begin
        quCardsQremn.Calculated;
        delay(20);
        quCards.Next;
      end;
      quCards.First;
      CardsViewRemn.Visible:=true;
      CardsView.EndUpdate;
    end
    else
    begin
      CardsView.BeginUpdate;
      prflagcheck(0);
      delay(20);
      CardsViewRemn.Visible:=false;
      CardsView.EndUpdate;
    end;
  end;
end;


procedure TfmCards.acAddListExecute(Sender: TObject);
Var iMax,fl:Integer;
    rn1,rn2,rn3,rPr,rNac,rPr1,rPrA:Real;
    iSS,icli:INteger;
    rPrIn0:Real;
    sCCode,sCountry:String;
begin

  if ((iDirect=2) and (fmAddDoc1.Showing)) or ((iDirect=3) and (fmAddDoc2.Showing)) or ((iDirect=4) and (fmAddDoc3.Showing))
  or ((iDirect=5) and (fmAddDoc4.Showing)) or ((iDirect=6) and (fmAddDoc5.Showing)) or ((iDirect=7) and (fmAddDoc4.Showing))
  or ((iDirect=20) and (fmAddDoc6.Showing)) or ((iDirect=21) and (fmAssPost.Showing)) or ((iDirect=22) and (fmAddDoc7.Showing))
  or ((iDirect=25) and (fmAddDoc8.Showing))  or  ((iDirect=26) and (fmInvVoz.Showing)) then
  begin      //�������� � ������������
    with dmMC do
    with dmMT do
    begin
      iCol:=0;
      if iDirect=2 then //�������� � ������������ ��������� ���������
      begin
        iMax:=1;
        fmAddDoc1.taSpecIn.First;
        if not fmAddDoc1.taSpecIn.Eof then
        begin
          fmAddDoc1.taSpecIn.Last;
          iMax:=fmAddDoc1.taSpecInNum.AsInteger+1;
        end;
        with fmAddDoc1 do
        begin
          prCalcPriceOut(quCardsID.AsInteger,Trunc(Date),0,0,rn1,rn2,rn3,rPr,rPrA);

          taSpecIn.Append;
          taSpecInNum.AsInteger:=iMax;
          taSpecInCodeTovar.AsInteger:=quCardsID.AsInteger;
          taSpecInCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
          taSpecInBarCode.AsString:=quCardsBarCode.AsString;
          taSpecInNDSProc.AsFloat:=quCardsNDS.AsFloat*fmAddDoc1.Label17.Tag;
          taSpecInNDSSum.AsFloat:=0;
          taSpecInOutNDSSum.AsFloat:=0;
          taSpecInBestBefore.AsDateTime:=Date;
          taSpecInKolMest.AsInteger:=1;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=0;
          taSpecInKolWithNecond.AsFloat:=0;
          taSpecInKol.AsFloat:=0;
          taSpecInCenaTovar.AsFloat:=0;
          taSpecInProcent.AsFloat:=0;
          taSpecInNewCenaTovar.AsFloat:=rPr;
          taSpecInSumCenaTovarPost.AsFloat:=0;
          taSpecInSumCenaTovar.AsFloat:=0;
          taSpecInProcentN.AsFloat:=0;
          taSpecInNecond.AsFloat:=0;
          taSpecInCenaNecond.AsFloat:=0;
          taSpecInSumNecond.AsFloat:=0;
          taSpecInProcentZ.AsFloat:=0;
          taSpecInZemlia.AsFloat:=0;
          taSpecInProcentO.AsFloat:=0;
          taSpecInOthodi.AsFloat:=0;
          taSpecInName.AsString:=quCardsName.AsString;
          taSpecInCodeTara.AsInteger:=0;
          taSpecInVesTara.AsFloat:=0;
          taSpecInCenaTara.AsFloat:=0;
          taSpecInSumCenaTara.AsFloat:=0;
          taSpecInRealPrice.AsFloat:=quCardsCena.AsFloat;
          taSpecInRemn.AsFloat:=quCardsReserv1.AsFloat;
          taSpecInNac1.AsFloat:=rn1;
          taSpecInNac2.AsFloat:=rn2;
          taSpecInNac3.AsFloat:=rn3;
          taSpecIniCat.AsINteger:=quCardsV02.AsInteger;
          taSpecInPriceNac.AsFloat:=quCardsReserv5.AsFloat;
          if quCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
          if quCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
          taSpecInCenaTovarAvr.AsFloat:=rPrA;
          taSpecInsMaker.AsString:=quCardsNAMEM.AsString;
          taSpecInSumVesTara.AsFloat:=quCardsV12.AsInteger;
          taSpecInAVid.AsInteger:=quCardsV11.AsInteger;
          taSpecInVol1.AsFloat:=quCardsV10.AsInteger/1000;
          taSpecIn.Post;
        end;
      end;
      if iDirect=3 then //�������� � ������������ ��������� ���������
      begin
        iMax:=1;
        fmAddDoc2.taSpecOut.First;
        if not fmAddDoc2.taSpecOut.Eof then
        begin
          fmAddDoc2.taSpecOut.Last;
          iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
        end;
        with fmAddDoc2 do
        begin
          sCCode:='';
          sCountry:='';

          if quCardsID.AsInteger>0 then prFindTCountry(quCardsID.AsInteger,sCCode,sCountry);

          taSpecOut.Append;

          taSpecOutCCode.AsString:=sCCode;
          taSpecOutCountry.AsString:=sCountry;

          taSpecOutNum.AsInteger:=iMax;
          taSpecOutCodeTovar.AsInteger:=quCardsID.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
          taSpecOutBarCode.AsString:=quCardsBarCode.AsString;
          //taSpecOutNDSProc.AsFloat:=0;
          taSpecOutNDSSum.AsFloat:=0;
          taSpecOutOutNDSSum.AsFloat:=0;
          taSpecOutKolMest.AsInteger:=1;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=1;
          taSpecOutKol.AsFloat:=1;
          taSpecOutCenaTovar.AsFloat:=0;
          taSpecOutCenaTovarSpis.AsFloat:=quCardsCena.AsFloat;
          taSpecOutSumCenaTovar.AsFloat:=0;
          taSpecOutSumCenaTovarSpis.AsFloat:=quCardsCena.AsFloat;
          taSpecOutName.AsString:=quCardsName.AsString;
          taSpecOutCodeTara.AsInteger:=0;
          taSpecOutNameTara.AsString:='';
          taSpecOutVesTara.AsFloat:=0;
          taSpecOutCenaTara.AsFloat:=0;
          taSpecOutCenaTaraSpis.AsFloat:=0; //���� ������ ��� ���   (���� ���� ��� ���)
          taSpecOutSumVesTara.AsFloat:=0;
          taSpecOutSumCenaTara.AsFloat:=0;
          taSpecOutSumCenaTaraSpis.AsFloat:=0;
          taSpecOutFullName.AsString:=quCardsFullName.AsString;

          if (iTypeDO<>99)  then
          begin
            dmP.quAllDepart.Locate('ID',cxLookupComboBox1.EditValue,[]);
            if (dmP.quAllDepartStatus3.AsInteger=1) then taSpecOutNDSProc.AsFloat:=quCardsNDS.AsFloat
            else taSpecOutNDSProc.AsFloat:=0;

            if (CommonSet.RC=0) or ((cxComboBox1.ItemIndex<>3) and (cxComboBox1.ItemIndex<>100))  then
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*100;
                taSpecOutNDSSum.AsFloat:=rv(taSpecOutCenaTovar.AsFloat*taSpecOutKol.AsFloat)/(100+taSpecOutNDSProc.AsFloat)*taSpecOutNDSProc.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovar.AsFloat;

            end else
            begin
              if (taSpecOutNDSProc.AsFloat>0) then
              begin
                taSpecOutOutNDSSum.AsFloat:=rv(taSpecOutSumCenaTovarSpis.AsFloat/(100+taSpecOutNDSProc.AsFloat)*100);
                taSpecOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat-taSpecOutOutNDSSum.AsFloat;
              end
              else taSpecOutOutNDSSum.AsFloat:=taSpecOutSumCenaTovarSpis.AsFloat;

            end;
            if taSpecOutKol.AsFloat>0 then taSpecOutCenaTaraSpis.AsFloat:= (taSpecOutOutNDSSum.AsFloat/taSpecOutKol.AsFloat); //���� ����� ������ ��� ���
          end;

          taSpecOutBrak.AsFloat:=0;
          taSpecOutCheckCM.AsInteger:=0;
          taSpecOutKolF.AsFloat:=1;
          taSpecOutKolSpis.AsFloat:=0;

          taSpecOut.Post;
          cxLabel5.Tag:=cxLookupComboBox1.EditValue;
        end;
        fmCards.Close;
      end;
      if iDirect=4 then //�������� � ������������ ���� ����������
      begin
        iMax:=1;
        fmAddDoc3.taSpecAc.First;
        if not fmAddDoc3.taSpecAc.Eof then
        begin
          fmAddDoc3.taSpecAc.Last;
          iMax:=fmAddDoc3.taSpecAcNum.AsInteger+1;
        end;
        with fmAddDoc3 do
        begin
          taSpecAc.Append;
          taSpecAcNum.AsInteger:=iMax;
          taSpecAcCodeTovar.AsInteger:=quCardsID.AsInteger;
          taSpecAcCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
          taSpecAcBarCode.AsString:=quCardsBarCode.AsString;
          taSpecAcNDSProc.AsFloat:=quCardsNDS.AsFloat;
          taSpecAcKol.AsFloat:=1;
          taSpecAcName.AsString:=quCardsName.AsString;
          taSpecAcFullName.AsString:=quCardsFullName.AsString;
          taSpecAcCena.AsFloat:=quCardsCena.AsFloat;
          taSpecAcNewCena.AsFloat:=quCardsCena.AsFloat;
          taSpecAcRazn.AsFloat:=0;
          taSpecAcProc.AsFloat:=0;
          taSpecAcSumCena.AsFloat:=quCardsCena.AsFloat;
          taSpecAcSumNewCena.AsFloat:=quCardsCena.AsFloat;
          taSpecAcSumRazn.AsFloat:=0;
          taSpecAc.Post;
        end;
      end;
      if iDirect=5 then //�������� � ������������ �����������
      begin
        iMax:=1;
        fmAddDoc4.taSpecVn.First;
        if not fmAddDoc4.taSpecVn.Eof then
        begin
          fmAddDoc4.taSpecVn.Last;
          iMax:=fmAddDoc4.taSpecVnNum.AsInteger+1;
        end;
        with fmAddDoc4 do
        begin
          rPr1:=0;
          iSS:=fSS(fmAddDoc4.cxLookupComboBox1.EditValue);
          if iSS=0 then rPr1:=prFLP(quCardsID.AsInteger,rPr);
          if (iSS=1)or(iSS=2) then
          begin
            rPr1:=prFindPricePP0(quCardsID.AsInteger,fmAddDoc4.cxLookupComboBox1.EditValue,rPrIn0); //�� �������
            if (iSS=2) then rPr1:=rPrIn0;
          end;

          taSpecVn.Append;
          taSpecVnNum.AsInteger:=iMax;
          taSpecVnId.AsInteger:=iTPos;
          taSpecVnCodeTovar.AsInteger:=quCardsID.AsInteger;
          taSpecVnCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
          taSpecVnBarCode.AsString:=quCardsBarCode.AsString;
          taSpecVnNDSProc.AsFloat:=quCardsNDS.AsFloat;
          if iTPos=0 then //������
          begin
  //          taSpecVnKolMest.AsFloat:=1;
  //          taSpecVnKolWithMest.AsFloat:=1;
            taSpecVnKol.AsFloat:=1;
            taSpecVnCena.AsFloat:=rPr1;
            taSpecVnNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnProc.AsFloat:=0;
            taSpecVnSumCena.AsFloat:=rPr1;
            taSpecVnSumNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnSumRazn.AsFloat:=0;
          end else  //������
          begin
  //          taSpecVnKolMest.AsFloat:=-1;
  //          taSpecVnKolWithMest.AsFloat:=1;
            taSpecVnKol.AsFloat:=-1;
            taSpecVnCena.AsFloat:=0;
            taSpecVnNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnProc.AsFloat:=0;
            taSpecVnSumCena.AsFloat:=0;
            taSpecVnSumNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnSumRazn.AsFloat:=0;
          end;

          taSpecVnName.AsString:=quCardsName.AsString;
          taSpecVnFullName.AsString:=quCardsFullName.AsString;
          taSpecVn.Post;
        end;
      end;
      if iDirect=6 then //�������� � ������������ ��������� ���������
      begin
        with fmAddDoc5 do
        begin
          if taSpecInv.Locate('CodeTovar',quCardsID.AsInteger,[])= False then
          begin
            taSpecInv.Append;
            taSpecInvCodeTovar.AsInteger:=quCardsID.AsInteger;
            taSpecInvName.AsString:=quCardsName.AsString;
            taSpecInvCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
            taSpecInvNDSProc.AsFloat:=quCardsNDS.AsFloat;
            taSpecInvPrice.AsFloat:=quCardsCena.AsFloat;
            taSpecInvQuantR.AsFloat:=0;
            taSpecInvQuantF.AsFloat:=0;
            taSpecInvSumR.AsFloat:=0;
            taSpecInvSumF.AsFloat:=0;
            taSpecInvQuantD.AsFloat:=0;
            taSpecInvSumD.AsFloat:=0;
            taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
            taSpecInv.Post;
          end;
        end;
      end;
      if iDirect=7 then //�������� � ������������ ����������� - ��������� ������ ��� ������
      begin
        iMax:=fmAddDoc4.taSpecVnNum.AsInteger;

        rPr1:=prFLP(quCardsID.AsInteger,rPr);

        with fmAddDoc4 do
        begin
          taSpecVn.Append;
          taSpecVnNum.AsInteger:=iMax;
          taSpecVnId.AsInteger:=iTPos;
          taSpecVnCodeTovar.AsInteger:=quCardsID.AsInteger;
          taSpecVnCodeEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
          taSpecVnBarCode.AsString:=quCardsBarCode.AsString;
          taSpecVnNDSProc.AsFloat:=quCardsNDS.AsFloat;
          if iTPos=1 then  //������
          begin
  //          taSpecVnKolMest.AsFloat:=-1;
  //          taSpecVnKolWithMest.AsFloat:=1;
            taSpecVnKol.AsFloat:=-1;
            taSpecVnCena.AsFloat:=0;
            taSpecVnNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnProc.AsFloat:=0;
            taSpecVnSumCena.AsFloat:=0;
            taSpecVnSumNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnSumRazn.AsFloat:=0;
          end else //=0 ������
          begin
  //          taSpecVnKolMest.AsFloat:=1;
  //          taSpecVnKolWithMest.AsFloat:=1;
            taSpecVnKol.AsFloat:=1;
            taSpecVnCena.AsFloat:=rPr1;
            taSpecVnNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnProc.AsFloat:=0;
            taSpecVnSumCena.AsFloat:=rPr1;
            taSpecVnSumNewCena.AsFloat:=quCardsCena.AsFloat;
            taSpecVnSumRazn.AsFloat:=0;
          end;

          taSpecVnName.AsString:=quCardsName.AsString;
          taSpecVnFullName.AsString:=quCardsFullName.AsString;
          taSpecVn.Post;
        end;
      end;
      if iDirect=20 then //�������� � ������������ ��������� ���������
      begin
        with fmAddDoc6 do
        begin
          if teSpecObm.Locate('Code',quCardsID.AsInteger,[])= False then
          begin
            if taCards.FindKey([quCardsID.AsInteger]) then
            begin
              iMax:=1;
              teSpecObm.First;
              if not teSpecObm.Eof then
              begin
                teSpecObm.Last;
                iMax:=teSpecObmNum.AsInteger+1;
              end;

              rPr:=prFLPriceDateT(quCardsID.AsInteger,RoundEx(Date),rPrIn0);

              rNac:=0;
              if rPr<>0 then
              begin
                rNac:=(quCardsCena.AsFloat-rPr)/rPr*100;
              end;

              teSpecObm.Append;
              teSpecObmNum.AsInteger:=iMax;
              teSpecObmCode.AsInteger:=quCardsID.AsInteger;
              teSpecObmsBar.AsString:=quCardsBarCode.AsString;
              teSpecObmName.AsString:=quCardsName.AsString;
              teSpecObmiM.AsInteger:=quCardsEdIzm.AsInteger;
              teSpecObmsType.AsInteger:=0;
              teSpecObmQuant.AsFloat:=0;
              teSpecObmPriceIn.AsFloat:=rPr;
              teSpecObmNac.AsFloat:=rNac;
              teSpecObmPriceOut.AsFloat:=quCardsCena.AsFloat;
              teSpecObmSumIn.AsFloat:=0;
              teSpecObmSumOut.AsFloat:=0;
              teSpecObmQRemn.AsFloat:=taCardsReserv1.AsFloat;
              teSpecObmPriceM.AsFloat:=quCardsCena.AsFloat;
              teSpecObmFixPrice.AsFloat:=taCardsFixPrice.AsFloat;
              teSpecObmGdsNac.AsFloat:=taCardsReserv2.AsFloat;
              teSpecObm.Post;

            end;
          end;
        end;
      end;
      if iDirect=21 then //�������� � ����������� ����������
      begin
        with fmAssPost do
        begin
          if mePost.Locate('Code',quCardsID.AsInteger,[])= False then
          begin
            mePost.Append;
            mePostCode.AsInteger:=quCardsID.AsInteger;
            mePostName.AsString:=quCardsName.AsString;
            mePostEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
            mePostPriceDog.AsFloat:=0;
            mePostPricePP.AsFloat:=0;
            mePostNameFull.AsString:=quCardsFullName.AsString;
            mePostsMess.AsString:='';
            mePost.Post;
          end;
        end;
      end;
      if iDirect=22 then //�������� � �����
      begin
        with fmAddDoc7 do
        begin
          if teSpecZ.Locate('Code',quCardsID.AsInteger,[])= False then
          begin
            teSpecZ.Append;
            teSpecZCode.AsInteger:=quCardsID.AsInteger;
            teSpecZName.AsString:=quCardsName.AsString;
            teSpecZEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
            teSpecZFullName.AsString:=quCardsFullName.AsString;
            teSpecZiCat.AsInteger:=quCardsV02.AsInteger;
            teSpecZiTop.AsInteger:=quCardsV04.AsInteger;
            teSpecZiN.AsInteger:=quCardsV05.AsInteger;
            teSpecZRemn1.AsFloat:=0;
            teSpecZvReal.AsFloat:=0;
            teSpecZRemnDay.AsFloat:=0;
            teSpecZRemnMin.AsInteger:=quCardsV06.AsInteger;
            teSpecZRemnMax.AsInteger:=quCardsV07.AsInteger;
            teSpecZRemn2.AsFloat:=0;
            teSpecZQuant1.AsFloat:=0;
            teSpecZPK.AsFloat:=fmAddDoc7.cxCalcEdit1.Value;
            teSpecZQuantZ.AsFloat:=0;
            teSpecZQuant2.AsFloat:=0;
            teSpecZQuant3.AsFloat:=0;
            teSpecZPriceIn.AsFloat:=0;
            teSpecZSumIn.AsFloat:=0;
            teSpecZNDSProc.AsFloat:=quCardsNDS.AsFloat;
            teSpecZBarCode.AsString:=quCardsBarCode.AsString;
            teSpecZ.Post;
          end;
        end;
      end;


      if (iDirect=25) and
         (fmAddDoc8.Showing) and
         (fmAddDoc8.cxButton1.Enabled) then //�������� � ������������ ���������� �� �������
      begin
        icli:=0;

        with dmP do
        begin
          quPostVoz.Active:=False;
          quPostVoz.ParamByName('IDCARD').AsInteger:=quCardsId.AsInteger;
          quPostVoz.Active:=True;

          if (quPostVoz.RecordCount>0) then
          begin
            quPostVoz.Last;

            if (quPostVozIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
            begin
              fl:=1;                                   //���� ���� ��������� �����������
              quTypeCli.Active:=false;
              quTypeCli.ParamByName('ICLI').AsInteger:=quPostVozIDALTCLI.AsInteger;
              quTypeCli.Active:=True;
              if quTypeCli.RecordCount>0 then         //���� �� ���������� ���� ������ (����������, ������������)
              begin
                if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPostVozIDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
                else icli:=0;                          //���� ��� �� ��������
              end
              else icli:=0;                            //��� ������, ����� ���� ��������
            end
            else
            begin
              fl:=0;                                 //���� ��� ��������� ������������
              icli:=quPostVozidcli.AsInteger;        //����������� ����. ����������
            end;

            if (quPostVozTypeVoz.AsInteger=4) then
            begin
              showmessage('���������� �� ������ ����� ������� ������, �� �������� ������ � ���������!');
              exit;
            end;
            if (quPostVozTypeVoz.AsInteger=3) then      //��������� ��� �������� � ������  1-�������,2-�����,3-�������������� 4-����� � ���������
            begin
              if MessageDlg('����� �� �������� "������������". ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=mrYES
              then
              begin
                predztoday(2,0,icli);
                exit;
              end
              else  exit;
            end;           //���� ����� ����������
                           //��������� ���� � ����. ���������� ������ �������� "�� ������������",��


            if fl=0 then    //���� � ���������� ���������� ��� ���������� ���������� ������ �������� �� ����
            begin
              if (quPostVozCliTypeV.AsInteger=3) then  typenevoz(icli)  //���� ��� ������������ ���������
              else typevoz(icli);     //���� � ���������� ���� ������ ��������
            end
            else
            begin
              if icli=0 then typenevoz(icli)  //���� ��� � ���������� ���������� ������������
              else  typevoz(icli);
            end;
          end
          else
          begin
            if MessageDlg('��� ������ �� ������ ��� ��������. ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=mrYES
            then  predztoday(3,0,icli);
          end;

        end;
      end;
      if iDirect=26 then //�������� � ��������������
      begin
        with fmInvVoz do
        with dmP do
        begin
          if teInvVoz.Locate('IdCode1',quCardsId.AsInteger,[]) then
          begin
            fmInvVoz.Show;
            showmessage('���� ����� ��� ���� � ��������������');
            iDirect:=0;
            ViewInvVoz.Controller.FocusRecord(ViewInvVoz.DataController.FocusedRowIndex,True);
            exit;
          end;
          if quCardsV14.AsInteger<>4 then
          begin
            teInvVoz.Append;
            teInvVozIdCode1.AsInteger:=quCardsId.AsInteger;
            teInvVozNameC.AsString:=quCardsFullName.AsString;
            teInvVozEdIzm.AsInteger:=quCardsEdIzm.AsInteger;
            teInvVozQOut.AsFloat:=0;
            teInvVozTypeVoz.AsInteger:=quCardsV14.AsInteger;
            teInvVozQFact.AsFloat:=1;
            teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);

            teInvVoz.Post;

            teInvVoz.Edit;
            teInvVozRemnCode.AsFloat:=prFindTRemnDepD(teInvVozIdCode1.AsInteger,cxLookupComboBox1.EditValue,Trunc(Date()));//ptRemnCodeReserv1.AsFloat;
            teInvVoz.Post;

            iDirect:=0;
            ViewInvVozQFact.Focused:=true;
            ViewInvVozQFact.Selected:=true;
            ViewInvVozQFact.Options.Editing:=True;
            teInvVoz.Edit;
            fmInvVoz.Show;
            ViewInvVoz.Controller.FocusRecord(ViewInvVoz.DataController.FocusedRowIndex,True);
          end
          else
            showmessage('� ����� ������ ��� �������� - ����� � ���������! ������ ���������');

        end;

      end;
    end;
  end;
end;

procedure TfmCards.CardsViewCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acAddList.Execute;
end;

procedure TfmCards.cxButton9Click(Sender: TObject);
Var ExcelApp,Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iRow:Integer;
    iC:Integer;
    ColumnCount:integer;
    MinRow:INteger;
begin
  //����� - ���� �� �������� �������
  if not IsOLEObjectInstalled('Excel.Application') then exit;

  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  Workbook := ExcelApp.WorkBooks.Add;
  with dmMC do
  begin
    iRow:=taCen.RecordCount;
    MinRow:=iRow;
    if MinRow<10 then MinRow:=10;

    ArrayData := VarArrayCreate([1,MinRow+2, 1, 30], varVariant);

    ArrayData[1,2]:='�����-���� �� '+ds1(date);

    ArrayData[2,2]:='������ ��������';
    ArrayData[2,3]:='����';
    ColumnCount:=3;
    iC:=0;
    taCen.First;
    while not taCen.Eof do
    begin
      ArrayData[iC+3,2] := taCenFullName.AsString;
      ArrayData[iC+3,3] := taCenPrice1.AsFloat;

      taCen.Next; inc(iC);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+2,ColumnCount];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;
    Range.Font.Size := 12;

    WorkBook.WorkSheets[1].Cells[1,2].Font.Size := 12;
//    WorkBook.WorkSheets[1].Cells[1,2].Font.Bold := True;

    Cell1 := WorkBook.WorkSheets[1].Cells[2,2];
    Cell2 := WorkBook.WorkSheets[1].Cells[iRow+2,ColumnCount];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Borders.LineStyle:=1;
    Range.Font.Size := 12;
    Range.HorizontalAlignment:=1;
    Range.VerticalAlignment:=1;

//    WorkBook.WorkSheets[1].Cells[2,2].Font.Bold := True;
//    WorkBook.WorkSheets[1].Cells[2,3].Font.Bold := True;

    WorkBook.WorkSheets[1].Columns[2].ColumnWidth:=65;
    WorkBook.WorkSheets[1].Columns[3].ColumnWidth:=12;
    WorkBook.WorkSheets[1].Columns[3].NumberFormat := '0,00�';

    WorkBook.WorkSheets[1].Name:='�����-���� '+ds1(date);

    ExcelApp.Visible := true;    //prTrEMail
  end;
end;

end.

