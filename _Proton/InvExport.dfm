object fmInvExport: TfmInvExport
  Left = 259
  Top = 301
  BorderStyle = bsDialog
  Caption = 'fmInvExport'
  ClientHeight = 132
  ClientWidth = 480
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 113
    Width = 480
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 58
    Width = 480
    Height = 55
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton2: TcxButton
      Left = 276
      Top = 16
      Width = 93
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 132
      Top = 16
      Width = 101
      Height = 25
      Caption = 'Ok'
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Label1: TcxLabel
    Left = 8
    Top = 18
    Caption = #1060#1072#1081#1083
    Transparent = True
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 128
    Top = 16
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 3
    Text = 'cxButtonEdit2'
    Width = 313
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 24
    Top = 40
  end
end
