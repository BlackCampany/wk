unit CheckEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, cxDropDownEdit, cxCalc,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  StdCtrls, cxButtons;

type
  TfmCheckEdit = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit3: TcxSpinEdit;
    cxSpinEdit4: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    cxSpinEdit6: TcxSpinEdit;
    Label11: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label12: TLabel;
    cxSpinEdit7: TcxSpinEdit;
    Label13: TLabel;
    Label14: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCheckEdit: TfmCheckEdit;

implementation

{$R *.dfm}

end.
