unit DocsIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar;

type
  TfmDocs1 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsIn: TcxGrid;
    ViewDocsIn: TcxGridDBTableView;
    LevelDocsIn: TcxGridLevel;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Excel1: TMenuItem;
    ViewDocsInDepart: TcxGridDBColumn;
    ViewDocsInDateInvoice: TcxGridDBColumn;
    ViewDocsInNumber: TcxGridDBColumn;
    ViewDocsInSCFNumber: TcxGridDBColumn;
    ViewDocsInSummaTovarPost: TcxGridDBColumn;
    ViewDocsInSummaTovar: TcxGridDBColumn;
    ViewDocsInNacenka: TcxGridDBColumn;
    ViewDocsInSummaTara: TcxGridDBColumn;
    ViewDocsInAcStatus: TcxGridDBColumn;
    ViewDocsInChekBuh: TcxGridDBColumn;
    ViewDocsInSCFDate: TcxGridDBColumn;
    ViewDocsInID: TcxGridDBColumn;
    ViewDocsInNameDep: TcxGridDBColumn;
    ViewDocsInNameCli: TcxGridDBColumn;
    acEdit1: TAction;
    acTestMoveDoc: TAction;
    N5: TMenuItem;
    acOprih: TAction;
    ViewDocsInNDSTovar: TcxGridDBColumn;
    acRazrub: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acCheckBuh: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    teInLn: TdxMemData;
    teInLnDepart: TIntegerField;
    teInLnDateInvoice: TIntegerField;
    teInLnNumber: TStringField;
    teInLnCliName: TStringField;
    teInLnCode: TIntegerField;
    teInLniM: TIntegerField;
    teInLnNdsSum: TFloatField;
    teInLnNdsProc: TFloatField;
    teInLnKol: TFloatField;
    teInLnKolMest: TFloatField;
    teInLnKolWithMest: TFloatField;
    teInLnCenaTovar: TFloatField;
    teInLnNewCenaTovar: TFloatField;
    teInLnProc: TFloatField;
    teInLnSumCenaTovar: TFloatField;
    teInLnSumNewCenaTovar: TFloatField;
    teInLnCodeTara: TIntegerField;
    teInLnCenaTara: TFloatField;
    teInLnSumCenaTara: TFloatField;
    teInLnDepName: TStringField;
    teInLnCardName: TStringField;
    teInLniCat: TIntegerField;
    teInLniTop: TIntegerField;
    teInLniNov: TIntegerField;
    dsteInLn: TDataSource;
    ViewCardsDepart: TcxGridDBColumn;
    ViewCardsDepName: TcxGridDBColumn;
    ViewCardsDateInvoice: TcxGridDBColumn;
    ViewCardsNumber: TcxGridDBColumn;
    ViewCardsCliName: TcxGridDBColumn;
    ViewCardsCode: TcxGridDBColumn;
    ViewCardsCardName: TcxGridDBColumn;
    ViewCardsiM: TcxGridDBColumn;
    ViewCardsKol: TcxGridDBColumn;
    ViewCardsKolMest: TcxGridDBColumn;
    ViewCardsKolWithMest: TcxGridDBColumn;
    ViewCardsCenaTovar: TcxGridDBColumn;
    ViewCardsNewCenaTovar: TcxGridDBColumn;
    ViewCardsSumCenaTovar: TcxGridDBColumn;
    ViewCardsSumNewCenaTovar: TcxGridDBColumn;
    ViewCardsProc: TcxGridDBColumn;
    ViewCardsNdsProc: TcxGridDBColumn;
    ViewCardsNdsSum: TcxGridDBColumn;
    ViewCardsCodeTara: TcxGridDBColumn;
    ViewCardsCenaTara: TcxGridDBColumn;
    ViewCardsSumCenaTara: TcxGridDBColumn;
    ViewCardsiCat: TcxGridDBColumn;
    ViewCardsiTop: TcxGridDBColumn;
    ViewCardsiNov: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    acChangePost: TAction;
    N10: TMenuItem;
    ViewDocsInProvodType: TcxGridDBColumn;
    acChangeTypePol: TAction;
    N11: TMenuItem;
    acPrintAddDocs: TAction;
    N12: TMenuItem;
    ViewDocsInOrderNumber: TcxGridDBColumn;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teInLniMaker: TIntegerField;
    teInLnsMaker: TStringField;
    ViewCardssMaker: TcxGridDBColumn;
    PopupMenu2: TPopupMenu;
    acGroupChange: TAction;
    N13: TMenuItem;
    acMoveCard: TAction;
    teBuffLn: TdxMemData;
    teBuffLniDep: TIntegerField;
    teBuffLnNumDoc: TStringField;
    teBuffLnCode: TIntegerField;
    teInLnV11: TIntegerField;
    ViewCardsV11: TcxGridDBColumn;
    teBuffLniMaker: TIntegerField;
    teBuffLnsMaker: TStringField;
    teBuffLniDateDoc: TIntegerField;
    teInLnVolIn: TFloatField;
    teInLnVol: TFloatField;
    teInLniMakerCard: TIntegerField;
    teInLnsMakerCard: TStringField;
    ViewCardsVol: TcxGridDBColumn;
    ViewCardsVolIn: TcxGridDBColumn;
    ViewCardssMakerCard: TcxGridDBColumn;
    acCrVozTara: TAction;
    N14: TMenuItem;
    acPrintSCHF: TAction;
    N15: TMenuItem;
    teInLniGr1: TIntegerField;
    teInLniGr2: TIntegerField;
    teInLnsGr1: TStringField;
    teInLnsGr2: TStringField;
    ViewCardsiGr1: TcxGridDBColumn;
    ViewCardsiGr2: TcxGridDBColumn;
    ViewCardssGr1: TcxGridDBColumn;
    ViewCardssGr2: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acEdit1Execute(Sender: TObject);
    procedure acTestMoveDocExecute(Sender: TObject);
    procedure ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOprihExecute(Sender: TObject);
    procedure acRazrubExecute(Sender: TObject);
    procedure acCheckBuhExecute(Sender: TObject);
    procedure acChangePostExecute(Sender: TObject);
    procedure acChangeTypePolExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
    procedure ViewCardsSelectionChanged(Sender: TcxCustomGridTableView);
    procedure acGroupChangeExecute(Sender: TObject);
    procedure acMoveCardExecute(Sender: TObject);
    procedure acCrVozTaraExecute(Sender: TObject);
    procedure acPrintSCHFExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs1: TfmDocs1;
  bClearDocIn:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, MT, TBuff, u2fdk, ChangeB, ChangePost,
  ChangeT1, PrintAddDocs, Move, SetCardsParams1, dmPS, SpInEr, AddDoc2,
  DocsOut, PrintInSCHF;

{$R *.dfm}

procedure TfmDocs1.prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������
 var user:string;
begin
  with dmMC do
  begin
    bOpen:=True;
    if iT=0 then
    begin
      fmAddDoc1.Caption:='���������: ����������.';
      fmAddDoc1.cxTextEdit1.Text:='';
      fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc1.cxTextEdit1.Tag:=0;

      fmAddDoc1.cxTextEdit10.Text:='';
      fmAddDoc1.cxTextEdit10.Tag:=0;
      fmAddDoc1.cxDateEdit10.Date:=0;
      fmAddDoc1.cxDateEdit10.Tag:=0;

      fmAddDoc1.cxTextEdit2.Text:='';
      fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc1.cxDateEdit1.Date:=date;
      fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc1.cxDateEdit2.Date:=date;
      fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc1.cxCurrencyEdit1.EditValue:=0;
      fmAddDoc1.cxButtonEdit1.Tag:=0;
      fmAddDoc1.cxButtonEdit1.EditValue:=0;
      fmAddDoc1.cxButtonEdit1.Text:='';
      fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=True;

      fmAddDoc1.Label17.Caption:='';
      fmAddDoc1.Label17.Tag:=0;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc1.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmAddDoc1.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc1.cxLabel1.Enabled:=True;
      fmAddDoc1.cxLabel2.Enabled:=True;
      fmAddDoc1.cxLabel3.Enabled:=True;
      fmAddDoc1.cxLabel4.Enabled:=True;

      fmAddDoc1.cxLabel5.Enabled:=True;
      fmAddDoc1.cxLabel6.Enabled:=True;
      fmAddDoc1.cxButton1.Enabled:=True;
      fmAddDoc1.cxButton7.Enabled:=True;

      fmAddDoc1.cxCalcEdit2.Value:=0;
      fmAddDoc1.cxCalcEdit3.Value:=0;
      fmAddDoc1.cxCalcEdit4.Value:=0;
      fmAddDoc1.cxCalcEdit5.Value:=0;
      fmAddDoc1.cxCalcEdit6.Value:=0;
      fmAddDoc1.cxCalcEdit7.Value:=0;

      fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
      fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;
      fmAddDoc1.Panel4.Visible:=True;

      fmAddDoc1.cxComboBox1.ItemIndex:=0;

      fmAddDoc1.cxTextEdit3.Text:='';
      fmAddDoc1.cxTextEdit3.Tag:=0;
      fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=True;
      fmAddDoc1.cxButton8.Enabled:=True;

      fmAddDoc1.cxLabel15.Enabled:=true; //p

    end;
    if iT=1 then
    begin
      fmAddDoc1.Caption:='���������: ��������������.';
      fmAddDoc1.cxTextEdit1.Text:=quTTnInNumber.AsString;
      fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc1.cxTextEdit1.Tag:=quTTnInID.AsInteger;

      fmAddDoc1.cxTextEdit10.Text:=quTTnInNumber.AsString;
      fmAddDoc1.cxTextEdit10.Tag:=quTTnInID.AsInteger;
      fmAddDoc1.cxDateEdit10.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc1.cxDateEdit10.Tag:=quTTnInDepart.AsInteger;

      fmAddDoc1.cxTextEdit2.Text:=quTTnInSCFNumber.AsString;
      fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc1.cxDateEdit1.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc1.cxDateEdit2.Date:=quTTnInSCFDate.AsDateTime;
      fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc1.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;

      fmAddDoc1.Label4.Tag:=quTTnInIndexPost.AsInteger;

      if prCliNDS(quTTnInIndexPost.AsInteger,quTTnInCodePost.AsInteger,0)=1 then
      begin
        fmAddDoc1.Label17.Caption:='���������� ���';
        fmAddDoc1.Label17.Tag:=1;
      end else
      begin
        fmAddDoc1.Label17.Caption:='�� ���������� ���';
        fmAddDoc1.Label17.Tag:=0;
      end;
      
      fmAddDoc1.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
      fmAddDoc1.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
      fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc1.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
      fmAddDoc1.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
      fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc1.cxLabel1.Enabled:=True;
      fmAddDoc1.cxLabel2.Enabled:=True;
      fmAddDoc1.cxLabel3.Enabled:=True;
      fmAddDoc1.cxLabel4.Enabled:=True;
      fmAddDoc1.cxLabel5.Enabled:=True;
      fmAddDoc1.cxLabel6.Enabled:=True;
      fmAddDoc1.cxButton1.Enabled:=True;
      fmAddDoc1.cxButton7.Enabled:=True;

      fmAddDoc1.cxCalcEdit2.Value:=rv(quTTnInSummaTovarPost.AsFloat);
      fmAddDoc1.cxCalcEdit3.Value:=rv(quTTnInNotNDSTovar.AsFloat);
      fmAddDoc1.cxCalcEdit4.Value:=rv(quTTnInOutNDS10.AsFloat);
      fmAddDoc1.cxCalcEdit5.Value:=rv(quTTnInNDS10.AsFloat);
      fmAddDoc1.cxCalcEdit6.Value:=rv(quTTnInOutNDS20.AsFloat);
      fmAddDoc1.cxCalcEdit7.Value:=rv(quTTnInNDS20.AsFloat);

      fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
      fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;
      fmAddDoc1.Panel4.Visible:=True;

      fmAddDoc1.cxComboBox1.ItemIndex:=quTTnInProvodType.AsInteger;;
      fmAddDoc1.cxTextEdit3.Text:=its(quTTnInORDERNUMBER.AsInteger);
      if (quTTnInRaion.AsInteger>0)and(quTTnInRaion.AsInteger<>6) then
      begin
        fmAddDoc1.cxTextEdit3.Tag:=1;
        fmAddDoc1.ViewDoc1CodeTovar.Options.Editing:=False;
        fmAddDoc1.ViewDoc1Name.Options.Editing:=False;
        fmAddDoc1.ViewDoc1BarCode.Options.Editing:=False;
      end else
      begin
        fmAddDoc1.cxTextEdit3.Tag:=0;
        fmAddDoc1.ViewDoc1CodeTovar.Options.Editing:=True;
        fmAddDoc1.ViewDoc1Name.Options.Editing:=True;
        fmAddDoc1.ViewDoc1BarCode.Options.Editing:=True;
      end;
      fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=True;
      fmAddDoc1.cxButton8.Enabled:=True;

      //p beg
      with fmAddDoc1 do
      begin
        user:=Person.Name;
        if (quTTnInRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
        begin
          cxLabel3.Enabled:=false;     //�������� �� ��
    //      if((user='Oper1') or (user='Oper2') or (user='Zaved')) then      //���� ������������ ���, �� ��������� �������������� ���� � ����� ����������
    //      begin
            cxLabel15.Enabled:=true;
            ViewDoc1CenaTovar.Options.Editing:=true;      //false  p  ��� ����������� ����� ����
            ViewDoc1SumCenaTovarPost.Options.Editing:=true;  //false p ��� ����������� ����� �����
        {  end
          else               //���� ������������ ������ �� ��������� �������������� ������ �����
          begin
            if (user='OPER CB') then
            begin
               ViewDoc1CenaTovar.Options.Editing:=true;
               ViewDoc1SumCenaTovarPost.Options.Editing:=true;
               cxLabel5.Enabled:=false; //true ���������� ������
            end
            else
            begin
              ViewDoc1CenaTovar.Options.Editing:=true;
              ViewDoc1SumCenaTovarPost.Options.Editing:=true;  //false p ��� ����������� ����� �����
              cxLabel15.Enabled:=false; //true ���������� ������
            end;
          end; }
        end
        else
        begin
           ViewDoc1CenaTovar.Options.Editing:=true;
           ViewDoc1SumCenaTovarPost.Options.Editing:=true;
           cxLabel3.Enabled:=true;
           cxLabel15.Enabled:=true; //true ���������� ������
        end;
      end;

      quCli.Active:=false;
      quCli.Active:=true;
      quCli.Locate('Vendor',quTTnInCodePost.AsInteger,[]);

      //p end  }
    end;

    if iT=2 then
    begin
      fmAddDoc1.cxLabel15.Enabled:=false; //p

      fmAddDoc1.Caption:='���������: ��������.';
      fmAddDoc1.cxTextEdit1.Text:=quTTnInNumber.AsString;
      fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc1.cxTextEdit1.Tag:=quTTnInID.AsInteger;

      fmAddDoc1.cxTextEdit10.Text:=quTTnInNumber.AsString;
      fmAddDoc1.cxTextEdit10.Tag:=quTTnInID.AsInteger;
      fmAddDoc1.cxDateEdit10.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc1.cxDateEdit10.Tag:=quTTnInDepart.AsInteger;

      fmAddDoc1.cxTextEdit2.Text:=quTTnInSCFNumber.AsString;
      fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc1.cxDateEdit1.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc1.cxDateEdit2.Date:=quTTnInSCFDate.AsDateTime;
      fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc1.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;

      fmAddDoc1.Label4.Tag:=quTTnInIndexPost.AsInteger;

      if prCliNDS(quTTnInIndexPost.AsInteger,quTTnInCodePost.AsInteger,0)=1 then
      begin
        fmAddDoc1.Label17.Caption:='���������� ���';
        fmAddDoc1.Label17.Tag:=1;
      end else
      begin
        fmAddDoc1.Label17.Caption:='�� ���������� ���';
        fmAddDoc1.Label17.Tag:=0;
      end;

      fmAddDoc1.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
      fmAddDoc1.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
      fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Active:=True; quDepartsSt.First;

      fmAddDoc1.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
      fmAddDoc1.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
      fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc1.cxLabel1.Enabled:=False;
      fmAddDoc1.cxLabel2.Enabled:=False;
      fmAddDoc1.cxLabel3.Enabled:=False;
      fmAddDoc1.cxLabel4.Enabled:=False;
      fmAddDoc1.cxLabel5.Enabled:=False;
      fmAddDoc1.cxLabel6.Enabled:=False;
      fmAddDoc1.cxButton1.Enabled:=False;
      fmAddDoc1.cxButton7.Enabled:=False;

      fmAddDoc1.cxCalcEdit2.Value:=rv(quTTnInSummaTovarPost.AsFloat);
      fmAddDoc1.cxCalcEdit3.Value:=rv(quTTnInNotNDSTovar.AsFloat);
      fmAddDoc1.cxCalcEdit4.Value:=rv(quTTnInOutNDS10.AsFloat);
      fmAddDoc1.cxCalcEdit5.Value:=rv(quTTnInNDS10.AsFloat);
      fmAddDoc1.cxCalcEdit6.Value:=rv(quTTnInOutNDS20.AsFloat);
      fmAddDoc1.cxCalcEdit7.Value:=rv(quTTnInNDS20.AsFloat);

      fmAddDoc1.ViewDoc1.OptionsData.Editing:=False;
      fmAddDoc1.ViewDoc1.OptionsData.Deleting:=False;
      fmAddDoc1.Panel4.Visible:=False;

      fmAddDoc1.cxComboBox1.ItemIndex:=quTTnInProvodType.AsInteger;
      fmAddDoc1.cxTextEdit3.Text:=its(quTTnInORDERNUMBER.AsInteger);
      if (quTTnInRaion.AsInteger>0)and(quTTnInRaion.AsInteger<>6) then fmAddDoc1.cxTextEdit3.Tag:=1;
      fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=True;
      fmAddDoc1.cxButton8.Enabled:=False;
    end;
    bOpen:=False;
  end;
end;

procedure TfmDocs1.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs1.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs1.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsIn.Align:=AlClient;
  ViewDocsIn.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
  if CommonSet.Single=1 then
  begin
    acCheckBuh.Enabled:=True;
    acCheckBuh.Visible:=True;
  end else
  begin
    acCheckBuh.Enabled:=False;
    acCheckBuh.Visible:=False;
  end;
end;

procedure TfmDocs1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsIn.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs1.acPeriodExecute(Sender: TObject);
Var iC,iCliCB:INteger;
    Sinn:String;
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    with dmMt do
    begin
      if LevelDocsIn.Visible then
      begin
        fmDocs1.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs1.ViewDocsIn.BeginUpdate;
        try
          quTTNIn.Active:=False;
          quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTNIn.Active:=True;
        finally
          fmDocs1.ViewDocsIn.EndUpdate;
        end;

      end else
      begin
        fmDocs1.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ...');

        ViewCards.BeginUpdate;

        CloseTe(teInLn);
        if ptInLn.Active=False then ptINLn.Active:=True;

        dsteInLn.DataSet:=nil;

      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          Memo1.Lines.Add('   '+OemToAnsiConvert(ptDepName.AsString)); delay(10);

          ptInLn.CancelRange;
          ptInLn.SetRange([ptDepID.AsInteger,CommonSet.DateBeg],[ptDepID.AsInteger,CommonSet.DateEnd]);
          ptInLn.First;

          iC:=0;

          while not ptInLn.Eof do
          begin
            if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
            begin
              teInLn.Append;
              teInLnDepart.AsInteger:=ptInLnDepart.AsInteger;
              teInLnDepName.AsString:=OemToAnsiConvert(ptDepName.AsString);
              teInLnDateInvoice.AsInteger:=Trunc(ptInLnDateInvoice.AsDateTime);
              teInLnNumber.AsString:=OemToAnsiConvert(ptInLnNumber.AsString);
              teInLnCliName.AsString:=prFindCliName1(ptInLnIndexPost.AsInteger,ptInLnCodePost.AsInteger,Sinn,iCliCB);
              teInLnCode.AsInteger:=ptInLnCodeTovar.AsInteger;
              teInLnCardName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teInLniM.AsInteger:=ptInLnCodeEdIzm.AsInteger;
              teInLnKol.AsFloat:=ptInLnKol.AsFloat;
              teInLnKolMest.AsFloat:=ptInLnKolMest.AsFloat;
              teInLnKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
              teInLnCenaTovar.AsFloat:=ptInLnCenaTovar.AsFloat;
              teInLnNewCenaTovar.AsFloat:=ptInLnNewCenaTovar.AsFloat;
              teInLnSumCenaTovar.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
              teInLnSumNewCenaTovar.AsFloat:=ptInLnSumCenaTovar.AsFloat;
              teInLnProc.AsFloat:=ptInLnProcent.AsFloat;
              teInLnNdsProc.AsFloat:=ptInLnNDSProc.AsFloat;
              teInLnNdsSum.AsFloat:=ptInLnNDSSum.AsFloat;
              teInLnCodeTara.AsInteger:=ptInLnCodeTara.AsInteger;
              teInLnCenaTara.AsFloat:=ptInLnCenaTara.AsFloat;
              teInLnSumCenaTara.AsFloat:=ptInLnSumCenaTara.AsFloat;
              teInLniCat.AsInteger:=taCardsV02.AsInteger;
              teInLniTop.AsInteger:=taCardsV04.AsInteger;
              teInLniNov.AsInteger:=taCardsV05.AsInteger;
              teInLniMaker.AsInteger:=Trunc(ptInLnSumVesTara.AsFloat);
              teInLnsMaker.AsString:=prFindMakerName(Trunc(ptInLnSumVesTara.AsFloat));
              teInLnV11.AsInteger:=taCardsV11.AsInteger;
              teInLn.Post;
            end;

            ptInLn.Next;

            inc(iC);

            if (iC mod 100 = 0) then
            begin
              StatusBar1.Panels[0].Text:=its(iC); delay(10);
            end;
          end;
        end;
        ptDep.Next;
      end;

        dsteInLn.DataSet:=teInLn;

        ViewCards.EndUpdate;
        Memo1.Lines.Add('������������ ��.');

      end;
    end;
  end;
end;

procedure TfmDocs1.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmAddDoc1.taSpecIn);
    fmAddDoc1.acSaveDoc.Enabled:=True;
    fmAddDoc1.Show;
  end;
end;

procedure TfmDocs1.acEditDoc1Execute(Sender: TObject);
Var //IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
    StrWk:String;
    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //�������������
  if not CanDo('prEditDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        if quTTnInDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        prSetValsAddDoc1(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmAddDoc1.taSpecIn);

        fmAddDoc1.acSaveDoc.Enabled:=True;

        fmAddDoc1.ViewDoc1.BeginUpdate;
//        IDH:=quTTnInID.AsInteger;

        quSpecIn.Active:=False;
        quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
        quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
        quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
        quSpecIn.Active:=True;

        quSpecIn.First;
        while not quSpecIn.Eof do
        begin
          with fmAddDoc1 do
          begin
            prFindNacGr(quSpecInCodeTovar.AsInteger,rn1,rn2,rn3,rPr);


            taSpecIn.Append;
//            taSpecInNum.AsInteger:=iC;
            taSpecInNum.AsInteger:=Trunc(quSpecInMediatorCost.AsFloat);
            taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
            taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
            taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
            taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
            taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
            taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
            taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
            taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
            taSpecInKolEdMest.AsFloat:=0;
            taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
            taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
            taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
            taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
            taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
            taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
            taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
            taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
            taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
            taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
            taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
            taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
            taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
            taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
            taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
            taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
            taSpecInName.AsString:=quSpecInName.AsString;
            taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

            StrWk:='';
            if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
            taSpecInNameTara.AsString:=StrWk;

            taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
            taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
            taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
            taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
            taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
            taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
            taSpecInRemn.AsFloat:=quSpecInReserv1.AsFloat;
            taSpecInNac1.AsFloat:=rn1;
            taSpecInNac2.AsFloat:=rn2;
            taSpecInNac3.AsFloat:=rn3;
            taSpecIniCat.AsINteger:=quSpecInV02.AsInteger;
            taSpecInPriceNac.AsFloat:=quSpecInReserv5.AsFloat;

            if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
            if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

            prCalcPriceOut(quSpecInCodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecInCenaTovar.AsFloat,quSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

            taSpecInCenaTovarAvr.AsFloat:=rPrA;

            taSpecInGTD.AsString:=quSpecInSertNumber.AsString;

            if quSpecInSumVesTara.AsFloat>0.1 then taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

            taSpecInAVid.AsInteger:=quSpecInV11.AsInteger;
            taSpecInVol1.AsFloat:=quSpecInV10.AsInteger/1000;
            taSpecInVolDL.AsFloat:=(taSpecInVol1.AsFloat*taSpecInKol.AsFloat)/10;

            taSpecIn.Post;
          end;
          quSpecIn.Next;
        end;
        fmAddDoc1.ViewDoc1.EndUpdate;

        fmAddDoc1.prSetNac;

        fmAddDoc1.Show;
        
        with dmP do
        begin
          quPredzCli.Active:=false;
          quPredzCli.ParamByName('ICLI').AsInteger:=quTTnInStatus.AsInteger;
          quPredzCli.Active:=true;
          if quPredzClikol.AsInteger>0 then showmessage('� ������� ���������� ���� ���������� �� �������!');
          quPredzCli.Active:=false;
        end;


      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs1.acViewDoc1Execute(Sender: TObject);
Var //iC:INteger;
    StrWk:String;
    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //��������
  if not CanDo('prViewDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc1(2); //0-����������, 1-��������������, 2-��������

      CloseTe(fmAddDoc1.taSpecIn);

      fmAddDoc1.acSaveDoc.Enabled:=True;

//        IDH:=quTTnInID.AsInteger;
      fmAddDoc1.ViewDoc1.BeginUpdate;

      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      while not quSpecIn.Eof do
      begin
        with fmAddDoc1 do
        begin
          prFindNacGr(quSpecInCodeTovar.AsInteger,rn1,rn2,rn3,rPr);

          prCalcPriceOut(quSpecInCodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecInCenaTovar.AsFloat,quSpecInKol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

          taSpecIn.Append;
//          taSpecInNum.AsInteger:=iC;
          taSpecInNum.AsInteger:=Trunc(quSpecInMediatorCost.AsFloat);
          taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
          taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
          taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
          taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
          taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
          taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
          taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
          taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
          taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
          taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
          taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
          taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
          taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
          taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
          taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
          taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
          taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
          taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
          taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
          taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
          taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
          taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
          taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
          taSpecInName.AsString:=quSpecInName.AsString;
          taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

          StrWk:='';
          if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
          taSpecInNameTara.AsString:=StrWk;

          taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
          taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
          taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
          taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
          taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
          taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
          taSpecIniCat.AsInteger:=quSpecInV02.AsInteger;
          if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
          if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';
          taSpecInCenaTovarAvr.AsFloat:=rPrA;

          taSpecInGTD.AsString:=quSpecInSertNumber.AsString;

          if quSpecInSumVesTara.AsFloat>0.1 then taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

          taSpecInAVid.AsInteger:=quSpecInV11.AsInteger;
          taSpecInVol1.AsFloat:=quSpecInV10.AsInteger/1000;
          taSpecInVolDL.AsFloat:=(taSpecInVol1.AsFloat*taSpecInKol.AsFloat)/10;

          taSpecIn.Post;
        end;
        quSpecIn.Next; 
      end;
      fmAddDoc1.ViewDoc1.EndUpdate;

      fmAddDoc1.prSetNac;

      fmAddDoc1.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs1.ViewDocsInDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quTTnInAcStatus.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocs1.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������
    begin
      if quTTnInAcStatus.AsInteger<3 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(1,quTTnInID.AsInteger,trunc(quTTnInDateInvoice.AsDateTime),quTTnInNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNInLn"');
          quD.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTnIn"');
          quD.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          prRefrID(quTTnIn,ViewDocsIn,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs1.acOnDoc1Execute(Sender: TObject);
Var //iC:INteger;
    bOpr:Boolean;
    bShowErrTab:Boolean;    //������ ������ ������� ������
    iNumPost:INteger;
    sName,sMess:String;
    rn1,rn2,rn3,rPr,rPrA:Real;
//    rQIn,rPrIn:Real;
    SInn,SAdr,sFName,sInnF:String;
    iTestZ:Integer;
    sNumZ:String;
begin
  //������������
  with dmMC do
  with dmMt do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        if quTTnInDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if quTTnInDepart.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;


        if MessageDlg('������������ �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������

        {  quA.Active:=false;
          quA.SQL.Clear;
          quA.SQL.Add('select * from "InventryHead"');
          quA.SQL.Add('where DateInventry>'''+formatDateTime('yyyy-mm-dd',quTTnInDateInvoice.AsDateTime)+'''');
          quA.SQL.Add('and Detail=0');
          quA.SQL.Add('and Depart='+quTTnInDepart.AsString);

          quA.SQL.Add('and Status=''�''');

          quA.Active:=true;
          if (quA.RecordCount>0) then
          begin
            ShowMessage('����� �� ��������, ��������� ������ ��������������');
            quA.Active:=false;
            exit;
          end;
          quA.Active:=false;

          quA.SQL.Clear;
          quA.SQL.Add('select * from "InventryHead"');
          quA.SQL.Add('where DateInventry>='''+formatDateTime('yyyy-mm-dd',quTTnInDateInvoice.AsDateTime)+'''');
          quA.SQL.Add('and Detail>0');
          quA.SQL.Add('and Depart='+quTTnInDepart.AsString);
          quA.SQL.Add('and Status=''�''');

          quA.Active:=true;
          CloseTe(dmMT.teInventryArt);
          dmMT.teInventryArt.Active:=true;

          while not quA.Eof do
          begin
            quB.Active:=False;
            quB.SQL.Clear;
            quB.SQL.Add('select Item from "InventryLine"');
            quB.SQL.Add('where Inventry ='+quA.FieldByName('Inventry').AsString);
            quB.SQL.Add('and Depart='+quTTnInDepart.AsString);

            quB.Active:=True;
            while not quB.Eof do
            begin
              dmMT.teInventryArt.Append;
              dmMT.teInventryArtArticul.AsInteger:=quB.fieldbyName('Item').AsInteger;
              dmMT.teInventryArt.Post;
              quB.Next
            end;
            quB.Active:=False;
            quA.Next;
          end;

          quA.Active:=false;

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;
          while not quSpecIn1.Eof do
          begin
            if dmMT.teInventryArt.Locate('Articul',quSpecIn1.fieldbyname('CodeTovar').AsInteger,[]) then
            begin
              showMessage('����� �� ��������, ��������� ��������� ��������������');
              exit;
            end;
            quSpecIn1.Next;
          end;

          quSpecIn1.Active:=False;
          dmMT.teInventryArt.Active:=false;
          //----------------------------- ������
         {if prTOFind(Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger)=1 then
          end;}

          prButtonSet(False);

          prWH('����� ... ���� ��������.',Memo1);

          bOpr:=True;

          iTestZ:=0;
          if (quTTnInRaion.AsInteger>=1)and(quTTnInRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quTTnInORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
//            quZakHNum.ParamByName('IDATE').AsINteger:=Trunc(quTTnInDateInvoice.AsDateTime);
            quZakHNum.ParamByName('ICLI').AsInteger:=quTTnInCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;

            if iTestZ>0 then
            begin
              CloseTe(fmAddDoc1.teSpecZ);

              if ptZSpec.Active=False then ptZSpec.Active:=True;
              ptZSpec.CancelRange;
              ptZSpec.IndexFieldNames:='IDH;IDS';
              ptZSpec.SetRange([iTestZ],[iTestZ]);
              ptZSpec.First;
              while not ptZSpec.Eof do
              begin
                fmAddDoc1.teSpecZ.Append;
                fmAddDoc1.teSpecZCode.AsInteger:=ptZSpecCODE.AsInteger;
                fmAddDoc1.teSpecZCliQuantN.AsFloat:=ptZSpecCLIQUANTN.AsFloat;
                fmAddDoc1.teSpecZCliPriceN.AsFloat:=rv(ptZSpecCLIPRICEN.AsFloat);
                fmAddDoc1.teSpecZCliPrice0N.AsFloat:=rv(ptZSpecCLIPRICE0N.AsFloat);
                fmAddDoc1.teSpecZCliSumInN.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICEN.AsFloat));
                fmAddDoc1.teSpecZCliSumIn0N.AsFloat:=rv(ptZSpecCLIQUANTN.AsFloat*rv(ptZSpecCLIPRICE0N.AsFloat));
                fmAddDoc1.teSpecZ.Post;

                ptZSpec.Next;
              end;
            end;
          end;



          try
          fmAddDoc1.ViewDoc1.BeginUpdate;

          CloseTe(fmAddDoc1.taSpecIn);

          quSpecIn2.Active:=False;
          quSpecIn2.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn2.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn2.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn2.Active:=True;

          quSpecIn2.First;
          while not quSpecIn2.Eof do
          begin
            with fmAddDoc1 do
            begin
              if {(quSpecIn2V02.AsInteger<>2)and(quSpecIn2V02.AsInteger<>4) and (quSpecIn2V02.AsInteger<>6) and}(quSpecIn2CodeTovar.AsInteger>0) and (quSpecIn2Reserv1.AsFloat>0.001) then
              begin
                prFindNacGr(quSpecIn2CodeTovar.AsInteger,rn1,rn2,rn3,rPr);
                prCalcPriceOut(quSpecIn2CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecIn2CenaTovar.AsFloat,quSpecIn2Kol.AsFloat,rn1,rn2,rn3,rPr,rPrA);

                taSpecIn.Append;
//                taSpecInNum.AsInteger:=iC;
                taSpecInNum.AsInteger:=Trunc(quSpecIn2MediatorCost.AsFloat);
                taSpecInCodeTovar.AsInteger:=quSpecIn2CodeTovar.AsInteger;
                taSpecInName.AsString:=quSpecIn2Name.AsString;
                taSpecInCodeEdIzm.AsInteger:=quSpecIn2CodeEdIzm.AsInteger;
                taSpecInBarCode.AsString:=quSpecIn2BarCode.AsString;
                taSpecInNDSProc.AsFloat:=quSpecIn2NDSProc.AsFloat;
                taSpecInNDSSum.AsFloat:=rv(quSpecIn2NDSSum.AsFloat);
                taSpecInOutNDSSum.AsFloat:=rv(quSpecIn2OutNDSSum.AsFloat);
                taSpecInBestBefore.AsDateTime:=quSpecIn2BestBefore.AsDateTime;
                taSpecInKolMest.AsInteger:=quSpecIn2KolMest.AsInteger;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=quSpecIn2KolWithMest.AsFloat;
                taSpecInKolWithNecond.AsFloat:=quSpecIn2KolWithNecond.AsFloat;
                taSpecInKol.AsFloat:=quSpecIn2Kol.AsFloat;
                taSpecInCenaTovar.AsFloat:=quSpecIn2CenaTovar.AsFloat;
                taSpecInProcent.AsFloat:=quSpecIn2Procent.AsFloat;
                taSpecInNewCenaTovar.AsFloat:=quSpecIn2NewCenaTovar.AsFloat;
                taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecIn2SumCenaTovarPost.AsFloat);
                taSpecInSumCenaTovar.AsFloat:=rv(quSpecIn2SumCenaTovar.AsFloat);
//              taSpecInName.AsString:=quSpecIn2Name.AsString;
                taSpecInCodeTara.AsInteger:=quSpecIn2CodeTara.AsInteger;

            {  StrWk:='';
              if quSpecIn2CodeTara.AsInteger>0 then fTara(quSpecIn2CodeTara.AsInteger,StrWk);
              taSpecInNameTara.AsString:=StrWk;
             }
                taSpecInVesTara.AsFloat:=quSpecIn2VesTara.AsFloat;
                taSpecInCenaTara.AsFloat:=quSpecIn2CenaTara.AsFloat;
                taSpecInSumVesTara.AsFloat:=quSpecIn2SumVesTara.AsFloat;
                taSpecInSumCenaTara.AsFloat:=rv(quSpecIn2SumCenaTara.AsFloat);
                taSpecInTovarType.AsInteger:=quSpecIn2TovarType.AsInteger;
                taSpecInRealPrice.AsFloat:=quSpecIn2Cena.AsFloat;
                taSpecInRemn.AsFloat:=quSpecIn2Reserv1.AsFloat;
                taSpecInNac1.AsFloat:=rn1;
                taSpecInNac2.AsFloat:=rn2;
                taSpecInNac3.AsFloat:=rn3;
                taSpecInCenaTovarAvr.AsFloat:=rPrA;

                taSpecIniCat.AsINteger:=quSpecIn2V02.AsInteger;
                taSpecInPriceNac.AsFloat:=quSpecIn2Reserv5.AsFloat;
                if quSpecIn2V04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if quSpecIn2V05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                if quSpecIn2SumVesTara.AsFloat>0.1 then taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecIn2SumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

                taSpecIn.Post;
              end;
            end;
            sName:=quSpecIn2Name.AsString;
            if sName>'' then
              if sName[1]='*' then     //� ���� = ''    ����� ���������
              begin
                bOpr:=False;
                sMess:='�������� !!! ������ �'+its(fmAddDoc1.taSpecInNum.AsInteger)+' ������������ ������ ������.('+quSpecIn2Name.AsString+') ������������� ����������...';   //p
                prWH(sMess,fmDocs1.Memo1);
              end;

            quSpecIn2.Next; // inc(iC);
            delay(10);
          end;
          finally
            fmAddDoc1.ViewDoc1.EndUpdate;
          end;
          fmAddDoc1.prSetNac;

          //���� ��� ��������, ��� ����� - ��������



          try
            fmAddDoc1.ViewDoc1.BeginUpdate;
            with fmAddDoc1 do
            with dmMt do
            begin
              if taCards.Active=False then taCards.Active:=True;

              prWH(' -------- �������� .',fmDocs1.Memo1);

              bShowErrTab:=false;
              closete(fmSpInEr.taSpEr);

              Memo1.Lines.Add('��������� ���� ��������...'); delay(10);
              fmAddDoc1.acCalcPriceOut.Execute;         //��������� ���� ���.

              taSpecIn.First;
              while (taSpecIn.Eof=False) do      //p
              begin       //�������                 //�������
                if (taSpecIniNac.AsInteger=1)or(taSpecIniNac.AsInteger=2) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then     //p
                begin
                  if CommonSet.RC<>1 then
                  begin
                    bOpr:=False;
                    sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������� �� ������������� �������������� �������. ������������� ����������...'; //p
                    prWH(sMess,fmDocs1.Memo1);
                  end;
                end;

                if fShop<>0 then //���� ����������� �� ������
                begin
                  if (taSpecInCodeTovar.AsInteger>0) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then
                  begin
                    if taCards.FindKey([taSpecInCodeTovar.AsInteger]) then
                    begin
                      sName:=OemToAnsiConvert(taCardsName.AsString);
                    //  prWH(sName,fmDocs1.Memo1);
                      if iTestZ=0 then  //������� ������������� ��� ������
                      begin
                        if sName[1]='*' then
                        begin
                          bOpr:=False;
                          sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ������ ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...'; //p
                          prWH(sMess,fmDocs1.Memo1);
                        end;
                        if taCardsStatus.AsInteger>=100 then
                        begin
                          bOpr:=False;
                          sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ������ ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...'; //p
                          prWH(sMess,fmDocs1.Memo1);
                        end;
                      end else
                      begin
                        if teSpecZ.Locate('CODE',taSpecInCodeTovar.AsInteger,[]) then
                        begin
                          if taCardsStatus.AsInteger>=100 then
                          begin
                            taCards.Edit;
                            taCardsStatus.AsInteger:=1;
                            taCards.Post;
                          end;
                        end;
                      end;

                      //�������� ������
                      //��������� � �������
                      if (iTestZ>0) and ((taSpecInCodeTovar.AsInteger<>56573) and (taSpecInCodeTovar.AsInteger<>71801)) then  //�������� �� ������� � ������������ ������
                      begin
                        if teSpecZ.Locate('CODE',taSpecInCodeTovar.AsInteger,[]) then
                        begin
                          if (dmMC.quTTnInRaion.AsInteger>0) then   //���� � ���������� ������ ���������� ����� ����� ���
                          begin
                            if taCardsBHTStatus.AsInteger=0 then
                            begin //��� �� ������
                              if (teSpecZCliQuantN.AsFloat+0.2)<taSpecInKol.AsFloat then
                              begin
                                bOpr:=False;
                                bShowErrTab:=true;
                                sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+'  ������������ ����������� � ������� �� ����������.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(fmAddDoc1.teSpecZCliQuantN.AsFloat)+' �� ��������� '+fs(taSpecInKol.AsFloat)+') ������������� ����������...';
                                prWH(sMess,fmDocs1.Memo1);
                                with fmSpInEr do
                                begin
                                  if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                                  taSpEr.Append;
                                  taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                                  taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                                  taSpErNameCode.AsString:=taSpecInName.AsString;
                                  taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                                  taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                                  taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                                  taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                                  taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                                  taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                                  taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                                  taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                                  taSpEr.Post;
                                end;

                              end;
                            end else  //=1 ��� ������ + 15%
                            begin
                              if (teSpecZCliQuantN.AsFloat+(0.15)*teSpecZCliQuantN.AsFloat)<taSpecInKol.AsFloat then
                              begin
                                bOpr:=False;
                                bShowErrTab:=true;
                                sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � ������� �� ����������.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(fmAddDoc1.teSpecZCliQuantN.AsFloat)+' �� ��������� '+fs(taSpecInKol.AsFloat)+') ������������� ����������...';
                                prWH(sMess,fmDocs1.Memo1);
                                with fmSpInEr do
                                begin
                                  if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                                  taSpEr.Append;
                                  taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                                  taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                                  taSpErNameCode.AsString:=taSpecInName.AsString;
                                  taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                                  taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                                  taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                                  taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                                  taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                                  taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                                  taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                                  taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                                  taSpEr.Post;
                                end;
                              end;
                            end;
//                            if abs(taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat)>0.05 then

                            //���� ���� ������ ��� � ������ �� 5 ��� ��� ������ �� 1 ����� - ���������

                            if ((taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat)>0.05) or ((teSpecZCliPriceN.AsFloat-taSpecInCenaTovar.AsFloat)>1)then
                            begin
                              bOpr:=False;
                              bShowErrTab:=true;
                              sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � ������� �� ����.( ��� - '+taSpecInCodeTovar.AsString+' �� ������ '+fs(fmAddDoc1.teSpecZCliPriceN.AsFloat)+' �� ��������� '+fs(taSpecInCenaTovar.AsFloat)+') ������������� ����������...';
                              prWH(sMess,fmDocs1.Memo1);
                              with fmSpInEr do
                              begin
                                if taSpEr.RecordCount>0 then fmSpInEr.taSpEr.Last;
                                taSpEr.Append;
                                taSpErNum.AsInteger:=taSpecInNum.AsInteger;
                                taSpErCodeTovar.AsInteger:=taSpecInCodeTovar.AsInteger;
                                taSpErNameCode.AsString:=taSpecInName.AsString;
                                taSpErTender.AsInteger:=taCardsBHTStatus.AsInteger;
                                taSpErCodeEdIzm.AsInteger:=taSpecInCodeEdIzm.AsInteger;
                                taSpErkolZ.AsFloat:=fmAddDoc1.teSpecZCliQuantN.AsFloat;
                                taSpErkolSp.AsFloat:=taSpecInKol.AsFloat;
                                taSpErkolRaz.AsFloat:=taSpErkolSp.AsFloat-taSpErkolZ.AsFloat;
                                taSpErCenaZ.AsFloat:=teSpecZCliPriceN.AsFloat;
                                taSpErCenaSp.AsFloat:=taSpecInCenaTovar.AsFloat;
                                taSpErCenaRaz.AsFloat:=taSpecInCenaTovar.AsFloat-teSpecZCliPriceN.AsFloat;
                                taSpEr.Post;
                              end;
                            end;
                          end;
                        end else
                        begin
                          bOpr:=False;
                          sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ����������� � �������.( ��� - '+taSpecInCodeTovar.AsString+' ����������� � ������) ������������� ����������...';
                          prWH(sMess,fmDocs1.Memo1);
                        end;
                      end;
                    end else
                    begin
                      bOpr:=False;
                      sMess:='�������� !!! ������ �'+its(taSpecInNum.AsInteger)+' ������������ ������ ������.( ��� - '+taSpecInCodeTovar.AsString+') ������������� ����������...';
                      prWH(sMess,fmDocs1.Memo1);
                    end;
                  end;
                end;

                taSpecIn.Next;
                delay(10);
              end;
            end;
          finally
            fmAddDoc1.ViewDoc1.EndUpdate;
          end;

          if bOpr = False then
          begin
         //   prWH(sMess,Memo1);
            Showmessage('���� ������. ������������� ����������!');
            if (bShowErrTab=true) then   //
            begin
              fmSpInEr.taSpEr.First;
              fmSpInEr.Show;
              prButtonSet(True);
              exit;
            end;
            fmSpInEr.taSpEr.Active:=false;
          end else
          begin
            // 1 - �������� ��������������
            // 2 - � ����� ��� �������������
            // 3 - �������� ������

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
            quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
            quSpecIn1.Active:=True;

            bOpr:=True;
            if NeedPost(trunc(quTTnInDateInvoice.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTnInID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger)+r+ds(quTTnInDateInvoice.AsDateTime)+r+quTTnInNumber.AsString+r+quTTnInINN.AsString+r+fs(quTTnInSummaTovarPost.AsFloat)+r+fs(quTTnInSummaTovar.AsFloat)+r+fs(quTTnInSummaTara.AsFloat)+r+fs(quTTnInNDS10.AsFloat)+r+fs(quTTnInNDS20.AsFloat)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              prWH('����� ... ���� ������.',Memo1);

              prAddHist(1,quTTnInID.AsInteger,trunc(quTTnInDateInvoice.AsDateTime),quTTnInNumber.AsString,'�����.',1);

              //������� ������ �� ����� ��������� �� ������ ������
           //   prDelPartIn(quTTnInID.AsInteger,1,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger);

              prFCliINN(quTTnInCodePost.AsInteger,quTTnInIndexPost.AsInteger,SInn,SAdr,sFName,sInnF);
              sInn:=sInnF;

              //�������� ������ ���������

//            prClearBuf(quTTnInID.AsInteger,1,Trunc(quTTnInDateInvoice.AsDateTime),1);
//            ������� ����� �� ���� - ���� ������ �������� �� �������������
              dmMT.ptCliGoods.Active:=False; dmMT.ptCliGoods.Active:=True;

              quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecIn1.Eof do
              begin
                if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quTTnInNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);

//                rQIn:=quSpecIn1Kol.AsFloat;
//                rPrIn:=0;
//                if rQIn<>0 then rPrIn:=(quSpecIn1SumCenaTovarPost.AsFloat-quSpecIn1NDSSum.AsFloat)/rQIn; //��� �� ������ ����� ��� ���
//                prAddPartIn(quTTnInID.AsInteger,1,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger,sInn,quSpecIn1Kol.AsFloat,quSpecIn1CenaTovar.AsFloat,quSpecIn1NewCenaTovar.AsFloat,rPrIn);

                //�������� � ����������� ����������
                if CommonSet.AssPostFromPrihod=1 then
                begin
                  with dmMT do
                  begin
                    if ptCliGoods.FindKey([quTTnInIndexPost.AsInteger,quTTnInCodePost.AsInteger,quSpecIn1CodeTovar.AsInteger]) then
                    begin
                      ptCliGoods.Edit;
                      ptCliGoodsxDate.AsDateTime:=quTTnInDateInvoice.AsDateTime;
                      ptCliGoods.Post;
                    end else
                    begin
                      ptCliGoods.Append;
                      ptCliGoodsGoodsID.AsInteger:=quSpecIn1CodeTovar.AsInteger;
                      ptCliGoodsClientObjTypeID.AsInteger:=quTTnInIndexPost.AsInteger;
                      ptCliGoodsClientID.AsInteger:=quTTnInCodePost.AsInteger;
                      ptCliGoodsxDate.AsDateTime:=quTTnInDateInvoice.AsDateTime;
                      ptCliGoods.Post;
                    end;
                  end;
                end;

                quSpecIn1.Next;
              end;
              quSpecIn1.Active:=False;

              dmMT.ptCliGoods.Active:=False;

              if quTTnInSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecInT.Active:=False;
                quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                quSpecInT.Active:=True;

                quSpecInT.First;  // ��������������
                while not quSpecInT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat);
                  prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                  quSpecInT.Next;
                end;
                quSpecInT.Active:=False;
              end;

             // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
              quE.ExecSQL;


              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quTTnIn,ViewDocsIn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTnInDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnInDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs1.acOffDoc1Execute(Sender: TObject);
//Var iCountPartOut:Integer;
//    bStart:Boolean;
//    StrWk:String;

Var   bOpr:Boolean;
      bExit:Boolean;

begin
//��������
  with dmMC do
  begin
    if not CanDo('prOffDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTnInDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if pos('EPRO',quTTnInNumber.AsString)=1 then
    begin
      bExit:=True;
      if Pos('OPER CB',Person.Name)>0 then bExit:=False;
      if Pos('OPERZAK',Person.Name)>0 then bExit:=False;
      if Pos('�����',Person.Name)>0 then bExit:=False;
      if bExit then
      begin
        StatusBar1.Panels[0].Text:='��� ����.';
        SpeedItem8.Enabled:=True;
        exit;
      end;
    end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger=3 then
      begin
        if quTTnInDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('�������� �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);
          prAddHist(1,quTTnInID.AsInteger,trunc(quTTnInDateInvoice.AsDateTime),quTTnInNumber.AsString,'�����.',0);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn1.Active:=True;


          bOpr:=True;
          if NeedPost(trunc(quTTnInDateInvoice.AsDateTime)) then
          begin
            iNumPost:=PostNum(quTTnInID.AsInteger)*100+1;
            prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'TTNIN;OF;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger)+r+ds(quTTnInDateInvoice.AsDateTime)+r+quTTnInNumber.AsString+r+quTTnInINN.AsString+r+fs(quTTnInSummaTovarPost.AsFloat)+r+fs(quTTnInSummaTovar.AsFloat)+r+fs(quTTnInSummaTara.AsFloat)+r+fs(quTTnInNDS10.AsFloat)+r+fs(quTTnInNDS20.AsFloat)+r);

              quSpecIn1.First;
              while not quSpecIn1.Eof do
              begin
                StrPost:='TTNINLN;OF;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger);
                StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                writeln(fPost,StrPost);
                quSpecIn1.Next;
              end;
            finally
              closefile(fPost);
            end;

            //������ ����� - ���� ��������
            bOpr:=prTr(sNumPost(iNumPost),Memo1);
          end;

          if bOpr then
          begin
            //������� ������ �� ���������
      //      prDelPartIn(quTTnInID.AsInteger,1,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger);

            quSpecIn1.First;  // ��������������
            while not quSpecIn1.Eof do
            begin
              prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);

{            if quSpecIn1CodeTara.AsInteger>0 then
              prDelTaraMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1KolMest.AsFloat);
}
              quSpecIn1.Next;
            end;

            quSpecIn1.Active:=False;

            if quTTnInSummaTara.AsFloat<>0 then
            begin //�������� �� ����
              quSpecInT.Active:=False;
              quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
              quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
              quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
              quSpecInT.Active:=True;

              quSpecInT.First;  // ��������������
              while not quSpecInT.Eof do
              begin
                prDelTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat);
                quSpecInT.Next;
              end;
              quSpecInT.Active:=False;
            end;

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "TTNIn" Set AcStatus=0');
            quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
            quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
            quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
            quE.ExecSQL;

            prRefrID(quTTnIn,ViewDocsIn,0);

           //�������� ��
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnInDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnInDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

            prWh('������ ��.',Memo1);
          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          prButtonSet(True);
        end;
      end;
      if quTTnInAcStatus.AsInteger=2 then
      begin
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set AcStatus=0');
        quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTnIn,ViewDocsIn,0);

        prWh('������ ��.',Memo1);
        prButtonSet(True);
      end;
    end;
  end;
end;

procedure TfmDocs1.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocs1.acVidExecute(Sender: TObject);
Var iC,iCliCB:INteger;
    Sinn:String;
    iGr:Integer;
begin
  //���
  with dmMC do
  with dmMt do
  begin
    if LevelDocsIn.Visible then
    begin
      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;
      Gr1.Visible:=True;

      fmDocs1.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      ViewCards.BeginUpdate;
      dsteInLn.DataSet:=nil;

      if ptSGr.Active=False then ptSGr.Active:=True;
      ptSGr.IndexFieldNames:='ID';

      CloseTe(teInLn);
      if ptInLn.Active=False then ptINLn.Active:=True;

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ...');

      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          Memo1.Lines.Add('   '+OemToAnsiConvert(ptDepName.AsString)); delay(10);

          ptInLn.CancelRange;
          ptInLn.SetRange([ptDepID.AsInteger,CommonSet.DateBeg],[ptDepID.AsInteger,CommonSet.DateEnd]);
          ptInLn.First;

          iC:=0;
          while not ptInLn.Eof do
          begin
            if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
            begin
              teInLn.Append;
              teInLnDepart.AsInteger:=ptInLnDepart.AsInteger;
              teInLnDepName.AsString:=OemToAnsiConvert(ptDepName.AsString);
              teInLnDateInvoice.AsInteger:=Trunc(ptInLnDateInvoice.AsDateTime);
              teInLnNumber.AsString:=OemToAnsiConvert(ptInLnNumber.AsString);
              teInLnCliName.AsString:=prFindCliName1(ptInLnIndexPost.AsInteger,ptInLnCodePost.AsInteger,Sinn,iCliCB);
              teInLnCode.AsInteger:=ptInLnCodeTovar.AsInteger;
              teInLnCardName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teInLniM.AsInteger:=ptInLnCodeEdIzm.AsInteger;
              teInLnKol.AsFloat:=ptInLnKol.AsFloat;
              teInLnKolMest.AsFloat:=ptInLnKolMest.AsFloat;
              teInLnKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
              teInLnCenaTovar.AsFloat:=ptInLnCenaTovar.AsFloat;
              teInLnNewCenaTovar.AsFloat:=ptInLnNewCenaTovar.AsFloat;
              teInLnSumCenaTovar.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
              teInLnSumNewCenaTovar.AsFloat:=ptInLnSumCenaTovar.AsFloat;
              teInLnProc.AsFloat:=ptInLnProcent.AsFloat;
              teInLnNdsProc.AsFloat:=ptInLnNDSProc.AsFloat;
              teInLnNdsSum.AsFloat:=ptInLnNDSSum.AsFloat;
              teInLnCodeTara.AsInteger:=ptInLnCodeTara.AsInteger;
              teInLnCenaTara.AsFloat:=ptInLnCenaTara.AsFloat;
              teInLnSumCenaTara.AsFloat:=ptInLnSumCenaTara.AsFloat;
              teInLniCat.AsInteger:=taCardsV02.AsInteger;
              teInLniTop.AsInteger:=taCardsV04.AsInteger;
              teInLniNov.AsInteger:=taCardsV05.AsInteger;
              teInLniMaker.AsInteger:=Trunc(ptInLnSumVesTara.AsFloat);
              teInLnsMaker.AsString:=prFindMakerName(Trunc(ptInLnSumVesTara.AsFloat));
              teInLnV11.AsInteger:=taCardsV11.AsInteger;
              teInLnVol.AsFloat:=taCardsV10.AsInteger/1000;
              teInLnVolIn.AsFloat:=(taCardsV10.AsInteger/1000)*ptInLnKol.AsFloat;
              teInLniMakerCard.AsInteger:=taCardsV12.AsInteger;
              teInLnsMakerCard.AsString:=prFindMakerName(taCardsV12.AsInteger);
              teInLniGr2.AsInteger:=taCardsGoodsGroupID.AsInteger;
              teInLnsGr2.AsString:='';

              teInLniGr1.AsInteger:=0;
              teInLnsGr1.AsString:='';

              if ptSGr.FindKey([taCardsGoodsGroupID.AsInteger]) then
              begin
                teInLnsGr2.AsString:=OemToAnsiConvert(ptSGrName.AsString);
                iGr:=ptSGrGoodsGroupID.AsInteger;
                teInLniGr1.AsInteger:=iGr;
                if ptSGr.FindKey([iGr]) then teInLnsGr1.AsString:=OemToAnsiConvert(ptSGrName.AsString);
              end;

              teInLn.Post;
            end;

            ptInLn.Next;
            inc(iC);
            if (iC mod 100 = 0) then
            begin
              StatusBar1.Panels[0].Text:=its(iC); delay(10);
            end;
          end;
        end;
        ptDep.Next;
      end;

      dsteInLn.DataSet:=teInLn;

      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

      Memo1.Lines.Add('������������ ��.');

    end else
    begin
      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;
      Gr1.Visible:=False;

      fmDocs1.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      fmDocs1.ViewDocsIn.BeginUpdate;
      try
        quTTNIn.Active:=False;
        quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
        quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
        quTTNIn.Active:=True;
      finally
        fmDocs1.ViewDocsIn.EndUpdate;
      end;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;
    end;
  end;
end;

procedure TfmDocs1.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs1.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsIn.Visible=False then exit;
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTnInNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnInNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnInNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs1.acCopyExecute(Sender: TObject);
var Par:Variant;
    iC:INteger;
begin
  //����������
  with dmMT do
  with dmMC do
  begin
    if ViewDocsIn.Controller.SelectedRowCount=0 then exit;
    
{    if (quTTNInRaion.AsInteger>0)and(quTTnInRaion.AsInteger<>6) then
    begin
      showmessage('����������� ���������� ����������� EDI ���������.');
      exit;
    end;
}
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnInID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnInID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnInDateInvoice.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnInNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTnInCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnInNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnInDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnInNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnInSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnInSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTnInIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTnInSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      iC:=1;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnInID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecInCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecInKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInNewCenaTovar.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSumCenaTovar.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecInNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecInNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecInCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=quSpecInProcent.AsFloat;
        taSpecDocIdTara.AsInteger:=quSpecInCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecInCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecInBarCode.AsString;
        taSpecDoc.Post;

        quSpecIn.Next;   inc(iC);
      end;
      quSpecIn.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocs1.acInsertDExecute(Sender: TObject);
Var StrWk:String;
    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMC do
  with dmMT do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmAddDoc1.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmAddDoc1.Label4.Tag:=taHeadDociTypeCli.AsInteger;

          if prCliNDS(taHeadDociTypeCli.AsInteger,taHeadDocIdCli.AsInteger,0)=1 then
          begin
            fmAddDoc1.Label17.Caption:='���������� ���';
            fmAddDoc1.Label17.Tag:=1;
          end else
          begin
            fmAddDoc1.Label17.Caption:='�� ���������� ���';
            fmAddDoc1.Label17.Tag:=0;
          end;

          fmAddDoc1.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc1.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Active:=True;

          fmAddDoc1.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc1.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmAddDoc1.cxLabel1.Enabled:=True;
          fmAddDoc1.cxLabel2.Enabled:=True;
          fmAddDoc1.cxLabel3.Enabled:=True;
          fmAddDoc1.cxLabel4.Enabled:=True;
          fmAddDoc1.cxLabel5.Enabled:=True;
          fmAddDoc1.cxLabel6.Enabled:=True;
          fmAddDoc1.cxButton1.Enabled:=True;

          fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;
          fmAddDoc1.Panel4.Visible:=True;

          CloseTe(fmAddDoc1.taSpecIn);

          if taG.Active=False then taG.Active:=True;

          fmAddDoc1.ViewDoc1.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc1 do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecIn.Append;
                  taSpecInNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecInCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecInCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecInBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecInNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecInNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecInOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecInBestBefore.AsDateTime:=Date;
                  taSpecInKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecInKolEdMest.AsFloat:=0;
                  taSpecInKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecInKolWithNecond.AsFloat:=0;
                  taSpecInKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecInCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecInProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecInNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecInSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecInProcentN.AsFloat:=0;
                  taSpecInNecond.AsFloat:=0;
                  taSpecInCenaNecond.AsFloat:=0;
                  taSpecInSumNecond.AsFloat:=0;
                  taSpecInProcentZ.AsFloat:=0;
                  taSpecInZemlia.AsFloat:=0;
                  taSpecInProcentO.AsFloat:=0;
                  taSpecInOthodi.AsFloat:=0;
                  taSpecInName.AsString:=taSpecDocNameC.AsString;
                  taSpecInCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecInNameTara.AsString:=StrWk;

                  taSpecInVesTara.AsFloat:=0;
                  taSpecInCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecInSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecInTovarType.AsInteger:=0;

                  taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecInNac1.AsFloat:=rn1;
                  taSpecInNac2.AsFloat:=rn2;
                  taSpecInNac3.AsFloat:=rn3;
                  taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInsMaker.AsString:='';
                  end;
                  taSpecIn.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmAddDoc1.ViewDoc1.EndUpdate;

          fmAddDoc1.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmAddDoc1.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocs1.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsIn.Visible then
  begin
    prNExportExel5(ViewDocsIn);
  end else
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocs1.FormShow(Sender: TObject);
begin
  if (Pos('����',Person.Name)=0)and(Pos('OPER CB',Person.Name)=0)and(Pos('OPERZAK',Person.Name)=0) then
  begin
    acChangePost.Enabled:=False;
  end else
  begin
    acChangePost.Enabled:=True;
  end;
  Memo1.Clear;
end;

procedure TfmDocs1.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsIn);
end;

procedure TfmDocs1.acEdit1Execute(Sender: TObject);
begin
{  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    if FileExists(CurDir+'SpecIn.cds') then fmAddDoc1.taSpecIn.Active:=True
    else fmAddDoc1.taSpecIn.CreateDataSet;

    fmAddDoc1.acSaveDoc.Enabled:=True;
    fmAddDoc1.Show;
  end;}
end;

procedure TfmDocs1.acTestMoveDocExecute(Sender: TObject);
begin
  //�������� ��������������
  with dmMC do
  begin
    if not CanDo('prTestMoveDoc') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ���������
    begin
      if quTTnInAcStatus.AsInteger=3 then
      begin
        if MessageDlg('��������� �������������� �� ��������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ��������.',Memo1);
         // 1 - �������� ��������������
         // 2 - � ����� ��� �������������
         // 3 - �������� ������

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecIn1.Eof do
          begin
//            prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
            prWh('  -- ��� '+its(quSpecIn1CodeTovar.AsInteger),Memo1); delay(10);

            //������ ��� �������� �� ���� �� ���� ���������� � ���������� ��������
            prDelTMoveDate(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),1);

            //�������� ��� ������� ������ �� ���� � �������� �������� �� ����
            quSpecInTest.Active:=False;
            quSpecInTest.SQL.Clear;
            quSpecInTest.SQL.Add('select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln');
            quSpecInTest.SQL.Add('left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoice=hd.DateInvoice) and(Ln.Number=hd.Number)');
            quSpecInTest.SQL.Add('where ln.DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
            quSpecInTest.SQL.Add('and ln.CodeTovar='+its(quSpecIn1CodeTovar.AsInteger));
            quSpecInTest.SQL.Add('and ln.Depart='+its(quTTnInDepart.AsInteger));
            quSpecInTest.SQL.Add('and hd.AcStatus=3');
            quSpecInTest.SQL.Add('group by hd.ID');
            quSpecInTest.Active:=True;
            quSpecInTest.First;
            while not quSpecInTest.Eof do
            begin
              prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quSpecInTestID.AsInteger,1,quSpecInTestKol.AsFloat);
              delay(20);
              quSpecInTest.Next;
            end;
            quSpecInTest.Active:=False;

            quSpecIn1.Next; delay(20);
          end;
          quSpecIn1.Active:=False;

         // 4 - �������� ������

          prWh('�������� ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs1.ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA,sO:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsIn.ColumnCount-1 do
  begin
    if ViewDocsIn.Columns[i].Name='ViewDocsInAcStatus' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;
    if ViewDocsIn.Columns[i].Name='ViewDocsInChekBuh' then
    begin
      sO:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;


  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

  if pos('�����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00E6E6CA; //����������� - ������� ��������
  if pos('����',sO)>0  then  ACanvas.Canvas.Brush.Color := $00F4F4EA; //����������� - �������� �������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocs1.acOprihExecute(Sender: TObject);
Var bOpr:Boolean;
    iTestZ:INteger;
    sNumZ:String;
begin
//������������
  with dmMC do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        if quTTnInDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          bOpr:=True;

          iTestZ:=0;
          if (quTTnInRaion.AsInteger>=1)and(quTTnInRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quTTnInORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
            quZakHNum.ParamByName('ICLI').AsInteger:=quTTnInCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;
          end;

          if bOpr = False then
          begin
            prWH('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...',Memo1);
            Showmessage('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...');
          end else
          begin
            prWH('����� ... ���� ������.',Memo1);

            prAddHist(1,quTTnInID.AsInteger,trunc(quTTnInDateInvoice.AsDateTime),quTTnInNumber.AsString,'�����.',1);

           // 1 - �������� ��������������
           // 2 - � ����� ��� �������������
           // 3 - �������� ������

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
            quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
            quSpecIn1.Active:=True;

//            prClearBuf(quTTnInID.AsInteger,1,Trunc(quTTnInDateInvoice.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quTTnInDateInvoice.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTnInID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger)+r+ds(quTTnInDateInvoice.AsDateTime)+r+quTTnInNumber.AsString+r+quTTnInINN.AsString+r+fs(quTTnInSummaTovarPost.AsFloat)+r+fs(quTTnInSummaTovar.AsFloat)+r+fs(quTTnInSummaTara.AsFloat)+r+fs(quTTnInNDS10.AsFloat)+r+fs(quTTnInNDS20.AsFloat)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quTTnInDepart.AsInteger)+r+its(quTTnInID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecIn1.Eof do
              begin
                if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quTTnInNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                prDelTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);

                quSpecIn1.Next;
              end;
              quSpecIn1.Active:=False;


              if quTTnInSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecInT.Active:=False;
                quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                quSpecInT.Active:=True;

                quSpecInT.First;  // ��������������
                while not quSpecInT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat);
                  prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                  quSpecInT.Next;
                end;
                quSpecInT.Active:=False;
              end;

           // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
              quE.ExecSQL;

              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quTTnIn,ViewDocsIn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTnInDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnInDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs1.acRazrubExecute(Sender: TObject);
Var iC:INteger;
    StrWk:String;
    rSumP,rQ,rNac,rSumNDS,rSumOutNDS:Real;
begin
// ������������ �������� �� ������
  with dmMC do
  with dmMt do
  begin
    if not CanDo('prRazrIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger<3 then
      begin
        if quTTnInDateInvoice.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTnInDepart.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������
          prButtonSet(False);

          prWH('����� ... ���� ������������.',Memo1);

          // 4 - �������� ������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=2');
          quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTnIn,ViewDocsIn,0);

          prWh('������ ��.',Memo1);

          prButtonSet(True);


          prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmAddDoc1.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;
          fmAddDoc1.cxTextEdit1.Text:=quTTnInNumber.AsString+'rzr';
          fmAddDoc1.Label4.Tag:=quTTnInIndexPost.AsInteger;

          if prCliNDS(quTTnInIndexPost.AsInteger,quTTnInCodePost.AsInteger,0)=1 then
          begin
            fmAddDoc1.Label17.Caption:='���������� ���';
            fmAddDoc1.Label17.Tag:=1;
          end else
          begin
            fmAddDoc1.Label17.Caption:='�� ���������� ���';
            fmAddDoc1.Label17.Tag:=0;
          end;

          fmAddDoc1.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
          fmAddDoc1.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
          fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Active:=True;

          fmAddDoc1.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
          fmAddDoc1.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
          fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmAddDoc1.cxLabel1.Enabled:=True;
          fmAddDoc1.cxLabel2.Enabled:=True;
          fmAddDoc1.cxLabel3.Enabled:=True;
          fmAddDoc1.cxLabel4.Enabled:=True;
          fmAddDoc1.cxLabel5.Enabled:=True;
          fmAddDoc1.cxLabel6.Enabled:=True;
          fmAddDoc1.cxButton1.Enabled:=True;

          fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;
          fmAddDoc1.Panel4.Visible:=True;

          CloseTe(fmAddDoc1.taSpecIn);

          if taG.Active=False  then taG.Active:=True;
          if ptTTK.Active=False  then ptTTK.Active:=True;

          fmAddDoc1.ViewDoc1.BeginUpdate;

          quSpecIn.Active:=False;
          quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
          quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
          quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
          quSpecIn.Active:=True;

          quSpecIn.First;
          iC:=1;
          while not quSpecIn.Eof do
          begin
            with fmAddDoc1 do
            begin
              if ptTTK.Active=False then ptTTK.Active:=True;
              ptTTK.CancelRange;
              if ptTTK.locate('ID',quSpecInCodeTovar.AsInteger,[]) then
              begin
                rSumP:=rv(quSpecInSumCenaTovarPost.AsFloat);
                rSumNDS:=rv(quSpecInNDSSum.AsFloat);
                rSumOutNDS:=rv(quSpecInOutNDSSum.AsFloat);

                ptTTK.CancelRange;
                ptTTK.SetRange([quSpecInCodeTovar.AsInteger,0],[quSpecInCodeTovar.AsInteger,100]);
                ptTTK.First;
                while not ptTTK.Eof do
                begin
                  rNac:=ptTTKPROC2.AsFloat;
                  if rNac<1 then rNac:=rNac*100-100;

                  if dmMT.taG.FindKey([ptTTKIDC.AsInteger]) then
                  begin
                    rQ:=R1000((quSpecInKolWithMest.AsFloat*quSpecInKolMest.AsInteger)*ptTTKPROC1.AsFloat/100);

                    taSpecIn.Append;
                    taSpecInNum.AsInteger:=iC;
                    taSpecInCodeTovar.AsInteger:=ptTTKIDC.AsInteger;
                    taSpecInCodeEdIzm.AsInteger:=dmMt.taGEdIzm.AsInteger;
                    taSpecInBarCode.AsString:=dmMt.taGBarCode.AsString;
                    taSpecInNDSProc.AsFloat:=dmMt.taGNDS.AsFloat;
                    taSpecInNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInOutNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInBestBefore.AsDateTime:=date+dmMt.taGSrokReal.AsInteger;
                    taSpecInKolMest.AsInteger:=1;
                    taSpecInKolEdMest.AsFloat:=0;
                    taSpecInKolWithMest.AsFloat:=rQ;
                    taSpecInKolWithNecond.AsFloat:=rQ;
                    taSpecInKol.AsFloat:=rQ;
                    taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                    taSpecInProcent.AsFloat:=rNac;
                    taSpecInNewCenaTovar.AsFloat:=rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInSumCenaTovarPost.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat);
                    taSpecInSumCenaTovar.AsFloat:=rQ*rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInProcentN.AsFloat:=0;
                    taSpecInNecond.AsFloat:=0;
                    taSpecInCenaNecond.AsFloat:=0;
                    taSpecInSumNecond.AsFloat:=0;
                    taSpecInProcentZ.AsFloat:=0;
                    taSpecInZemlia.AsFloat:=0;
                    taSpecInProcentO.AsFloat:=0;
                    taSpecInOthodi.AsFloat:=0;
                    taSpecInName.AsString:=OemToAnsiConvert(dmMt.taGName.AsString);
                    taSpecInCodeTara.AsInteger:=0;
                    taSpecInNameTara.AsString:='';

                    taSpecInVesTara.AsFloat:=0;
                    taSpecInCenaTara.AsFloat:=0;
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInSumCenaTara.AsFloat:=0;
                    taSpecInTovarType.AsInteger:=dmMt.taGTovarType.AsInteger;
                    taSpecInRealPrice.AsFloat:=dmMt.taGCena.AsFloat;
                    taSpecIniCat.AsInteger:=dmMt.taGV02.AsInteger;
                    if taGV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                    if taGV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                    taSpecInsMaker.AsString:='';
                    taSpecIn.Post;

                    rSumP:=rSumP-rv(rQ*quSpecInCenaTovar.AsFloat);
                    rSumNDS:=rSumNDS-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    rSumOutNDS:=rSumOutNDS-(rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat)));
                  end;

                  ptTTK.Next; inc(iC);
                end;
                //��������� ������� ��������� ���� ����
                if abs(rSumP)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat+rSumP;
                  taSpecIn.Post;
                end;
                if abs(rSumNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInNDSSum.AsFloat:=taSpecInNDSSum.AsFloat+rSumNDS;
                  taSpecIn.Post;
                end;
                if abs(rSumOutNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat+rSumOutNDS;
                  taSpecIn.Post;
                end;

              end else
              begin
                taSpecIn.Append;
                taSpecInNum.AsInteger:=iC;
                taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
                taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
                taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
                taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
                taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
                taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
                taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
                taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
                taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
                taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
                taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
                taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
                taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
                taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
                taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
                taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
                taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
                taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
                taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
                taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
                taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
                taSpecInName.AsString:=quSpecInName.AsString;
                taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

                StrWk:='';
                if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
                taSpecInNameTara.AsString:=StrWk;

                taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
                taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
                taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
                taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
                taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
                taSpecIniCat.AsInteger:=quSpecInV02.AsInteger;
                if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
                if quSpecInSumVesTara.AsFloat>0.1 then  taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

                taSpecIn.Post;

                inc(iC);
              end;
            end;
            quSpecIn.Next;
          end;

          ptTTK.Active:=False;

          fmAddDoc1.ViewDoc1.EndUpdate;

          fmAddDoc1.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmAddDoc1.Show;
        end;
      end else
      begin
        showmessage('�������� ��������� �.�. �������� �����������.');
      end;
    end;
  end;
end;

procedure TfmDocs1.acCheckBuhExecute(Sender: TObject);
Var iSt:INteger;
begin
//
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then
    begin
      if quTTnInChekBuh.AsInteger=0 then fmChangeB.cxRadioButton1.Checked:=True;
      if quTTnInChekBuh.AsInteger=1 then fmChangeB.cxRadioButton2.Checked:=True;
      if quTTnInChekBuh.AsInteger=2 then fmChangeB.cxRadioButton3.Checked:=True;

      fmChangeB.ShowModal;
      if fmChangeB.ModalResult=mrOk then
      begin
        iSt:=0;
        if fmChangeB.cxRadioButton2.Checked then iSt:=1;
        if fmChangeB.cxRadioButton3.Checked then iSt:=2;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ChekBuh='+its(iSt));
        quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTnIn,ViewDocsIn,0);

        showmessage('��.');
      end;
    end else
      showmessage('�������� ��������');
  end;
end;

procedure TfmDocs1.acChangePostExecute(Sender: TObject);
begin
// �������� ����������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������
    begin
      try
        fmChangePost:=tfmChangePost.Create(Application);
        fmChangePost.Label1.Caption:='�������� ���������� � ��������� � '+quTTnInNumber.AsString;
        fmChangePost.Label3.Caption:=quTTnInNameCli.AsString;
        fmChangePost.cxButtonEdit1.Text:='';
        fmChangePost.cxButtonEdit1.Tag:=0;
        fmChangePost.Label4.Tag:=0;

        fmAddDoc1.Label17.Caption:='';
        fmAddDoc1.Label17.Tag:=0;

        fmChangePost.ShowModal;
      finally
        fmChangePost.Release;
      end;
    end;
  end;
end;

procedure TfmDocs1.acChangeTypePolExecute(Sender: TObject);
begin
  //�������� ����������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quTTnInNumber.AsString+' �� '+DateToStr(quTTnInDateInvoice.AsDateTime)+' ������ ��� '+its(quTTnInProvodType.AsInteger);
      if quTTnInProvodType.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ProvodType='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTnIn,ViewDocsIn,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');
  end;
end;

procedure TfmDocs1.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMC do
  with dmMT do
  begin
    if ViewDocsIn.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsIn.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=1;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;
  end;
end;

procedure TfmDocs1.ViewCardsSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewCards.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost4.Active:=False;
    quPost4.ParamByName('IDCARD').Value:=teInLnCode.AsInteger;
    quPost4.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmDocs1.acGroupChangeExecute(Sender: TObject);
Var
    i,j:INteger;
    Rec:TcxCustomGridRecord;
    iCode,iDep,iDateDoc:INteger;
    sNumDoc:String;
    par:Variant;
    dDateDoc:TDateTime;
begin
  //��������� ��������� �� ���������
  if ViewCards.Visible=False then exit;

  if not CanDo('prSetSelectCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  if CommonSet.Single<>1 then begin StatusBar1.Panels[0].Text:='�������� ������.';  exit; end;
  with dmMC do
  with dmMT do
  begin
    if ViewCards.Controller.SelectedRecordCount=0 then exit;

    fmSetCardsParams1.Label1.Caption:='�������� - ' +its(ViewCards.Controller.SelectedRecordCount)+' ��������.';
    Memo1.Clear;
    Memo1.Lines.Add('��������� ���������.');
    Memo1.Lines.Add('�������� - ' +its(ViewCards.Controller.SelectedRecordCount)+' ��������.');
    fmSetCardsParams1.ShowModal;
    if fmSetCardsParams1.ModalResult=mrOk then
    begin
      Memo1.Lines.Add('  ����� ... ���� ��������� ���������.');
      if fmSetCardsParams1.cxCheckBox6.Checked
      then
      begin
        CloseTe(teBuffLn);

        for i:=0 to ViewCards.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewCards.Controller.SelectedRecords[i];
          iCode:=0; iDep:=0; iDateDoc:=0; sNumDoc:='';
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewCards.Columns[j].Name='ViewCardsCode' then begin iCode:=Rec.Values[j];  end;
            if ViewCards.Columns[j].Name='ViewCardsDepart' then begin iDep:=Rec.Values[j]; end;
            if ViewCards.Columns[j].Name='ViewCardsNumber' then begin sNumDoc:=Rec.Values[j]; end;
            if ViewCards.Columns[j].Name='ViewCardsDateInvoice' then begin iDateDoc:=Rec.Values[j]; end;
          end;

          if (iCode>0) then
          begin
            teBuffLn.Append;
            teBuffLniDep.AsInteger:=iDep;
            teBuffLniDateDoc.AsInteger:=iDateDoc;
            teBuffLnNumDoc.AsString:=sNumDoc;
            teBuffLnCode.AsInteger:=iCode;
            teBuffLniMaker.AsInteger:=fmSetCardsParams1.cxButtonEdit1.Tag;
            teBuffLnsMaker.AsString:=fmSetCardsParams1.cxButtonEdit1.Text;
            teBuffLn.Post;
          end;
        end;

        ViewCards.BeginUpdate;

        par := VarArrayCreate([0,3],varVariant);

        teBuffLn.First;
        while not teBuffLn.Eof do
        begin
          par[0]:=teBuffLniDep.AsInteger;
          par[1]:=teBuffLniDateDoc.AsInteger;
          par[2]:=teBuffLnNumDoc.AsString;
          par[3]:=teBuffLnCode.AsInteger;

          if teInLn.Locate('Depart;DateInvoice;Number;Code',par,[]) then
          begin
            Memo1.Lines.Add('  ���. ����:'+ds1(teBuffLniDateDoc.AsInteger)+' �:'+teBuffLnNumDoc.AsString+' �����:'+its(teBuffLniDep.AsInteger)+' ���:'+its(teBuffLnCode.AsInteger)+'  ����� ������������� "'+fmSetCardsParams1.cxButtonEdit1.Text+'"' );

            dDateDoc:=teBuffLniDateDoc.AsInteger;

            ptInLn.CancelRange;
            ptInLn.SetRange([teBuffLniDep.AsInteger,dDateDoc,AnsiToOemConvert(teBuffLnNumDoc.AsString)],[teBuffLniDep.AsInteger,dDateDoc,AnsiToOemConvert(teBuffLnNumDoc.AsString)]);
            ptInLn.First;
            while not ptInLn.Eof do
            begin
              if ptInLnCodeTovar.AsInteger=teBuffLnCode.AsInteger then
              begin
                try
                  ptInLn.Edit;
                  ptInLnSumVesTara.AsFloat:=teBuffLniMaker.AsInteger;
                  ptInLn.Post;
                except
                end;

                break;
              end;
              ptInLn.Next;
            end;


            teInLn.Edit;
            teInLniMaker.AsInteger:=teBuffLniMaker.AsInteger;
            teInLnsMaker.AsString:=teBuffLnsMaker.AsString;
            teInLn.Post;


          end;

          teBuffLn.Next;
        end;
        teBuffLn.Active:=False;

        ViewCards.EndUpdate;
      end;
      Memo1.Lines.Add('������� ��������.');
      fmDocs1.StatusBar1.Panels[0].Text:='Ok.'; delay(10);
    end;
  end;
end;

procedure TfmDocs1.acMoveCardExecute(Sender: TObject);
begin
  if ViewCards.Visible then
  begin
  //��������������
    with dmMC do
    with dmMT do
    begin
      if ViewCards.Controller.SelectedRecordCount>0 then
      begin
        fmMove.ViewM.BeginUpdate;
        fmMove.GridM.Tag:=teInLnCode.AsInteger;
        prFillMove(teInLnCode.AsInteger);

        fmMove.ViewM.EndUpdate;

        fmMove.Caption:=teInLnCardName.AsString+'('+teInLnCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

        fmMove.LevelP.Visible:=False;
        fmMove.LevelM.Visible:=True;

        fmMove.Show;
      end;
    end;
  end;
end;

procedure TfmDocs1.acCrVozTaraExecute(Sender: TObject);
Var iC:INteger;
    StrWk:String;
begin
  with dmMT do
  with dmMC do
  begin
    if CanDo('prAddDocOut') then
    begin
      if quTTnIn.RecordCount>0 then
      begin
        iTypeDO:=99;
        fmDocs2.prSetValsAddDoc2(0);

        if ptInLn.Active=False then ptINLn.Active:=True else ptINLn.Refresh;
        ptInLn.CancelRange;
        ptInLn.SetRange([quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,Trim(AnsiToOemConvert(quTTnInNumber.AsString))],[quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,Trim(AnsiToOemConvert(quTTnInNumber.AsString))]);

        CloseTa(fmAddDoc2.taSpecOut);

        fmAddDoc2.Show;

        fmAddDoc2.ViewDoc2.BeginUpdate;

        fmAddDoc2.cxButtonEdit1.Tag:=1;
        fmAddDoc2.cxButtonEdit1.EditValue:=quTTnInCodePost.AsInteger;
        fmAddDoc2.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
        fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

        fmAddDoc2.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;   //p
        fmAddDoc2.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;       //p
        fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

        iC:=1;

        ptInLn.First;
        while not ptInLn.Eof do
        begin
          if ptInLnCodeTara.AsInteger>0 then
          begin
            with fmAddDoc2 do
            begin
              taSpecOut.Append;
              taSpecOutNum.AsInteger:=iC;
              taSpecOutCodeTovar.AsInteger:=0;
              taSpecOutCodeEdIzm.AsInteger:=0;
              taSpecOutBarCode.AsString:='';
              taSpecOutTovarType.AsInteger:=0;
              taSpecOutNDSProc.AsFloat:=0;
              taSpecOutNDSSum.AsFloat:=0;
              taSpecOutOutNDSSum.AsFloat:=0;
              taSpecOutKolMest.AsInteger:=ptInLnKolMest.AsInteger;
              taSpecOutKolEdMest.AsFloat:=0;
              taSpecOutKolWithMest.AsFloat:=0;
              taSpecOutKol.AsFloat:=0;
              taSpecOutCenaTovar.AsFloat:=0;
              taSpecOutCenaTovarSpis.AsFloat:=0;
              taSpecOutCenaPost.AsFloat:=0;
              taSpecOutSumCenaTovarSpis.AsFloat:=0;
              taSpecOutSumCenaTovar.AsFloat:=0;
              taSpecOutName.AsString:='';

              taSpecOutCodeTara.AsInteger:=ptInLnCodeTara.AsInteger;
              fTara(ptInLnCodeTara.AsInteger,StrWk);
              taSpecOutNameTara.AsString:=StrWk;

              taSpecOutVesTara.AsFloat:=0;
              taSpecOutCenaTara.AsFloat:=ptInLnCenaTara.AsFloat;
              taSpecOutCenaTaraSpis.AsFloat:=ptInLnCenaTara.AsFloat;
              taSpecOutSumVesTara.AsFloat:=0;
              taSpecOutSumCenaTara.AsFloat:=rv(ptInLnSumCenaTara.AsFloat);
              taSpecOutSumCenaTaraSpis.AsFloat:=rv(ptInLnSumCenaTara.AsFloat);
              taSpecOutFullName.AsString:='';
              taSpecOutPostDate.AsDateTime:=quTTnInDateInvoice.AsDateTime;
              taSpecOutPostNum.AsString:='';
              taSpecOutPostQuant.AsFloat:=0;

              taSpecOutBrak.AsFloat:=0;
              taSpecOutCheckCM.AsInteger:=1;
              taSpecOutKolF.AsFloat:=0;
              taSpecOutKolSpis.AsFloat:=0;

              taSpecOutGTD.AsString:=''; //� ���
              taSpecOut.Post;
            end;
            inc(iC);

          end;
          ptInLn.Next;
        end;
        fmAddDoc2.ViewDoc2.EndUpdate;
      end;
    end;

  {
  if CanDo('prAddDocOut') then
  begin
    iNum:=prFindNum(2)+1;

    if iNum>0 then
    begin
      fmAddDoc2.cxTextEdit1.Text:=its(iNum);
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit1.Tag:=0;

      fmAddDoc2.cxTextEdit2.Text:=its(iNum);
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit2.Tag:=iNum;
    end else
    begin
      fmAddDoc2.cxTextEdit1.Text:='';
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit1.Tag:=0;

      fmAddDoc2.cxTextEdit2.Text:='';
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit2.Tag:=0;
    end;

    fmAddDoc2.cxTextEdit10.Text:='';
    fmAddDoc2.cxTextEdit10.Tag:=0;
    fmAddDoc2.cxDateEdit10.Date:=0;
    fmAddDoc2.cxDateEdit10.Tag:=0;

    fmadddoc2.cxButton1.Tag:=0;

    fmAddDoc2.cxDateEdit1.Date:=date;
    fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddDoc2.cxDateEdit2.Date:=date;
    fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;
    fmAddDoc2.cxCurrencyEdit1.EditValue:=0;
    fmAddDoc2.cxButtonEdit1.Tag:=1;
    fmAddDoc2.cxButtonEdit1.EditValue:=0;
    fmAddDoc2.cxButtonEdit1.Text:='';
    fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

    dmP.quAllDepart.Active:=False; dmP.quAllDepart.Active:=True; dmP.quAllDepart.First;

    fmAddDoc2.cxButton3.Enabled:=False;
    fmAddDoc2.cxButton4.Enabled:=False;
    fmAddDoc2.cxButton5.Enabled:=False;
    fmAddDoc2.cxButton11.Enabled:=False;
    fmAddDoc2.cxButton13.Enabled:=false;


    fmAddDoc2.cxLookupComboBox1.EditValue:=dmP.quAllDepartID.AsInteger;   //p
    fmAddDoc2.cxLookupComboBox1.Text:=dmP.quAllDepartName.AsString;       //p
    fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;


    fmAddDoc2.cxButton1.Enabled:=True;

    iTypeDO=99;

    fmAddDoc2.cxComboBox1.ItemIndex:=10

    fmAddDoc2.cxLabel15.Enabled:=true;

    fmAddDoc2.cxLabel1.Enabled:=False;
    fmAddDoc2.cxLabel2.Enabled:=True;        //������� �������
    fmAddDoc2.cxLabel3.Enabled:=True;        //�������� ��������
    fmAddDoc2.cxLabel4.Enabled:=False;
    fmAddDoc2.cxLabel5.Enabled:=true;        //�������� �������
    fmAddDoc2.cxLabel6.Enabled:=True;        //��������
    fmAddDoc2.cxLabel7.Enabled:=False;
    fmAddDoc2.cxLabel8.Enabled:=False;
    fmAddDoc2.cxLabel9.Enabled:=False;
    fmAddDoc2.cxLabel10.Enabled:=False;
    fmAddDoc2.cxLabel11.Enabled:=False;
    fmAddDoc2.cxLabel12.Enabled:=False;
    fmAddDoc2.cxLabel15.Enabled:=False;    //�������
    prEnableTara(1);
    prEnableTovar(0);


    fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
    fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

    fmAddDoc2.cxComboBox3.ItemIndex:=0;

    if quTTnIn.RecordCount>0 then
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      while not quSpecIn.Eof do
      begin




        fmAddDoc2.ViewDoc2.BeginUpdate;
        taSpecDoc.First;
        while not taSpecDoc.Eof do
        begin
          if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
          begin
            with fmAddDoc2 do
            begin
              taSpecOut.Append;
              taSpecOutNum.AsInteger:=taSpecDocNum.AsInteger;

           //   taSpecOutCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
              taSpecOutCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
              taSpecOutBarCode.AsString:=taSpecDocBarCode.AsString;
              taSpecOutNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
              taSpecOutNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;
              taSpecOutOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
              taSpecOutKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
              taSpecOutKolEdMest.AsFloat:=0;
              taSpecOutKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
              taSpecOutKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
              taSpecOutCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
              taSpecOutSumCenaTovar.AsFloat:=taSpecDocSumIn.AsFloat;

              taSpecOutCenaTaraSpis.AsFloat:=taSpecDocPriceIn.AsFloat; //���� ���������� ��� ��� - �������� ����������, �� ���� ���

              taSpecOutCenaTovarSpis.AsFloat:=taSpecDocPriceUch.AsFloat;
              taSpecOutSumCenaTovarSpis.AsFloat:=taSpecDocSumUch.AsFloat;
              taSpecOutName.AsString:=taSpecDocNameC.AsString;
              taSpecOutCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

              StrWk:='';
              if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
              taSpecOutNameTara.AsString:=StrWk;

              taSpecOutVesTara.AsFloat:=0;
              taSpecOutCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
              taSpecOutSumVesTara.AsFloat:=0;
              taSpecOutSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
              taSpecOutTovarType.AsInteger:=0;

              quCId.Active:=False;
              quCId.ParamByName('IDC').AsInteger:=taSpecDocIdCard.AsInteger;
              quCId.Active:=True;
              if quCId.RecordCount>0 then taSpecOutFullName.AsString:=quCIdFullName.AsString;
              quCId.Active:=False;

              taSpecOutBrak.AsFloat:=0;
              taSpecOutCheckCM.AsInteger:=0;
              taSpecOutKolF.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
              taSpecOutKolSpis.AsFloat:=0;
              taSpecOut.Post;
            end;
          end;
          taSpecDoc.Next;
        end;
        taSpecDoc.Active:=False;

        fmAddDoc2.ViewDoc2.EndUpdate;

        fmAddDoc2.acSaveDoc.Enabled:=True;

        bOpen:=False; //���������� �� ������������

        fmAddDoc2.Show;
    end;
  end else showmessage('��� ����.');
  }
  end;  //with
end;

procedure TfmDocs1.acPrintSCHFExecute(Sender: TObject);
begin
  fmPrintInSCHF.ShowModal;
end;

end.
