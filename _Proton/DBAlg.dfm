object dmAlg: TdmAlg
  OldCreateOrder = False
  Left = 355
  Top = 438
  Height = 217
  Width = 477
  object quAlcoDH: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quAlcoDHCalcFields
    SQL.Strings = (
      'SELECT dh.IdOrg'
      '      ,dh.Id'
      '      ,dh.IDateB'
      '      ,dh.IDateE'
      '      ,dh.DDateB'
      '      ,dh.DDateE'
      '      ,dh.IType'
      '      ,dh.sType'
      '      ,dh.SPers'
      '      ,cli.Name'
      '      ,cli.FullName'
      '  FROM A_ALCGDH dh'
      '  left join Depart cli on cli.ID = dh.IdOrg'
      '  where '
      '  dh.IDateB>=:IDATEB'
      '  and dh.IDateE<=:IDATEE')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 24
    Top = 12
    object quAlcoDHIdOrg: TIntegerField
      FieldName = 'IdOrg'
    end
    object quAlcoDHId: TIntegerField
      FieldName = 'Id'
    end
    object quAlcoDHIDateB: TIntegerField
      FieldName = 'IDateB'
    end
    object quAlcoDHIDateE: TIntegerField
      FieldName = 'IDateE'
    end
    object quAlcoDHDDateB: TDateField
      FieldName = 'DDateB'
    end
    object quAlcoDHDDateE: TDateField
      FieldName = 'DDateE'
    end
    object quAlcoDHIType: TIntegerField
      FieldName = 'IType'
    end
    object quAlcoDHsType: TStringField
      FieldName = 'sType'
      Size = 120
    end
    object quAlcoDHSPers: TStringField
      FieldName = 'SPers'
      Size = 100
    end
    object quAlcoDHName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quAlcoDHFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quAlcoDHIT1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IT1'
      Calculated = True
    end
    object quAlcoDHIT2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IT2'
      Calculated = True
    end
  end
  object dsquAlcoDH: TDataSource
    DataSet = quAlcoDH
    Left = 24
    Top = 68
  end
  object quOrgList: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select ID,Name,FullName'
      'from "Depart"'
      'where Status1=9')
    Params = <>
    Left = 108
    Top = 12
    object quOrgListID: TSmallintField
      FieldName = 'ID'
    end
    object quOrgListName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quOrgListFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
  end
  object dsquOrgList: TDataSource
    DataSet = quOrgList
    Left = 108
    Top = 68
  end
  object quDelDS: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    Params = <>
    Left = 176
    Top = 12
  end
  object quAlcoDS1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quAlcoDHCalcFields
    SQL.Strings = (
      'SELECT ds1.IdH'
      '      ,ds1.Id'
      '      ,ds1.iDep'
      '      ,ds1.iNum'
      '      ,ds1.iVid'
      '      ,ds1.iProd'
      '      ,ds1.rQb'
      '      ,ds1.rQIn1'
      '      ,ds1.rQIn2'
      '      ,ds1.rQIn3'
      '      ,ds1.rQInIt1'
      '      ,ds1.rQIn4'
      '      ,ds1.rQIn5'
      '      ,ds1.rQIn6'
      '      ,ds1.rQInIt'
      '      ,ds1.rQOut1'
      '      ,ds1.rQOut2'
      '      ,ds1.rQOut3'
      '      ,ds1.rQOut4'
      '      ,ds1.rQOutIt'
      '      ,ds1.rQe'
      '      ,sh.Name as NameShop'
      '      ,vid.NAMECLA as NameVid'
      '      ,pr.NAMEM as NameProducer'
      '      ,pr.INNM as INN '
      '      ,pr.KPPM as KPP'
      '  FROM A_ALCGDS1 ds1'
      '  left join Depart sh on sh.Id=ds1.iDep '
      '  left join A_CLASSIFALG vid on vid.ID=ds1.iVid'
      '  left join A_MAKER pr on pr.ID=ds1.iProd'
      '  where ds1.IdH=:IDH'
      '  order by ds1.iVid,pr.NAMEM'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDH'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 244
    Top = 12
    object quAlcoDS1IdH: TIntegerField
      FieldName = 'IdH'
    end
    object quAlcoDS1Id: TAutoIncField
      FieldName = 'Id'
    end
    object quAlcoDS1iDep: TIntegerField
      FieldName = 'iDep'
    end
    object quAlcoDS1iNum: TIntegerField
      FieldName = 'iNum'
    end
    object quAlcoDS1iVid: TIntegerField
      FieldName = 'iVid'
    end
    object quAlcoDS1iProd: TIntegerField
      FieldName = 'iProd'
    end
    object quAlcoDS1rQIn1: TFloatField
      FieldName = 'rQIn1'
    end
    object quAlcoDS1rQIn2: TFloatField
      FieldName = 'rQIn2'
    end
    object quAlcoDS1rQIn3: TFloatField
      FieldName = 'rQIn3'
    end
    object quAlcoDS1rQInIt1: TFloatField
      FieldName = 'rQInIt1'
    end
    object quAlcoDS1rQIn4: TFloatField
      FieldName = 'rQIn4'
    end
    object quAlcoDS1rQIn5: TFloatField
      FieldName = 'rQIn5'
    end
    object quAlcoDS1rQIn6: TFloatField
      FieldName = 'rQIn6'
    end
    object quAlcoDS1rQInIt: TFloatField
      FieldName = 'rQInIt'
    end
    object quAlcoDS1rQOut1: TFloatField
      FieldName = 'rQOut1'
    end
    object quAlcoDS1rQOut2: TFloatField
      FieldName = 'rQOut2'
    end
    object quAlcoDS1rQOut3: TFloatField
      FieldName = 'rQOut3'
    end
    object quAlcoDS1rQOut4: TFloatField
      FieldName = 'rQOut4'
    end
    object quAlcoDS1rQOutIt: TFloatField
      FieldName = 'rQOutIt'
    end
    object quAlcoDS1rQe: TFloatField
      FieldName = 'rQe'
    end
    object quAlcoDS1NameShop: TStringField
      FieldName = 'NameShop'
      Size = 30
    end
    object quAlcoDS1NameVid: TStringField
      FieldName = 'NameVid'
      Size = 200
    end
    object quAlcoDS1NameProducer: TStringField
      FieldName = 'NameProducer'
      Size = 200
    end
    object quAlcoDS1INN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object quAlcoDS1KPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
    object quAlcoDS1rQb: TFloatField
      FieldName = 'rQb'
    end
  end
  object quAlcoDS2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quAlcoDHCalcFields
    SQL.Strings = (
      'select '
      'ds2.IdH'
      ',ds2.Id'
      ',ds2.iDep'
      ',ds2.iVid'
      ',ds2.iProd'
      ',ds2.iPost'
      ',ds2.iLic'
      ',ds2.DocDate'
      ',ds2.DocNum'
      ',ds2.GTD'
      ',ds2.rQIn'
      ',ds2.LicNum'
      ',ds2.LicDateB'
      ',ds2.LicDateE'
      ',ds2.LicOrg'
      ',ds2.IdHdr'
      ',sh.Name as NameShop'
      ',vid.NAMECLA as NameVid'
      ',pr.NAMEM as NameProducer'
      ',pr.INNM as INN '
      ',pr.KPPM as KPP'
      'from "A_ALCGDS2" ds2'
      '  left join Depart sh on sh.Id=ds2.iDep '
      '  left join A_CLASSIFALG vid on vid.ID=ds2.iVid'
      '  left join A_MAKER pr on pr.ID=ds2.iProd'
      'where IDH=:IDH'
      'order by ds2.iVid,pr.NAMEM'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDH'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 324
    Top = 12
    object quAlcoDS2IdH: TIntegerField
      FieldName = 'IdH'
    end
    object quAlcoDS2Id: TAutoIncField
      FieldName = 'Id'
    end
    object quAlcoDS2iDep: TIntegerField
      FieldName = 'iDep'
    end
    object quAlcoDS2iVid: TIntegerField
      FieldName = 'iVid'
    end
    object quAlcoDS2iProd: TIntegerField
      FieldName = 'iProd'
    end
    object quAlcoDS2iPost: TIntegerField
      FieldName = 'iPost'
    end
    object quAlcoDS2iLic: TIntegerField
      FieldName = 'iLic'
    end
    object quAlcoDS2DocDate: TDateField
      FieldName = 'DocDate'
    end
    object quAlcoDS2DocNum: TStringField
      FieldName = 'DocNum'
    end
    object quAlcoDS2GTD: TStringField
      FieldName = 'GTD'
      Size = 50
    end
    object quAlcoDS2rQIn: TFloatField
      FieldName = 'rQIn'
    end
    object quAlcoDS2LicNum: TStringField
      FieldName = 'LicNum'
      Size = 100
    end
    object quAlcoDS2LicDateB: TDateField
      FieldName = 'LicDateB'
    end
    object quAlcoDS2LicDateE: TDateField
      FieldName = 'LicDateE'
    end
    object quAlcoDS2LicOrg: TStringField
      FieldName = 'LicOrg'
      Size = 200
    end
    object quAlcoDS2IdHdr: TIntegerField
      FieldName = 'IdHdr'
    end
    object quAlcoDS2NameShop: TStringField
      FieldName = 'NameShop'
      Size = 30
    end
    object quAlcoDS2NameVid: TStringField
      FieldName = 'NameVid'
      Size = 200
    end
    object quAlcoDS2NameProducer: TStringField
      FieldName = 'NameProducer'
      Size = 200
    end
    object quAlcoDS2INN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object quAlcoDS2KPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
  end
end
