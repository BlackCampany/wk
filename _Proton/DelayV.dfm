object fmDelayV: TfmDelayV
  Left = 292
  Top = 461
  Width = 1020
  Height = 577
  Caption = #1055#1088#1086#1089#1088#1086#1095#1077#1085#1085#1099#1077' '#1074#1086#1079#1074#1088#1072#1090#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrDelayV: TcxGrid
    Left = 0
    Top = 48
    Width = 1012
    Height = 419
    Align = alClient
    PopupMenu = PopupMenu1
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object VDelayV: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = VDelayVCustomDrawCell
      DataController.DataSource = dsteDelayV
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          Column = VDelayVQ
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          Column = VDelayVQOut
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          Column = VDelayVQRem
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          Column = VDelayVRSUMIN
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Column = VDelayVQ
        end
        item
          Format = '0.000'
          Kind = skSum
          Column = VDelayVQOut
        end
        item
          Format = '0.000'
          Kind = skSum
          Column = VDelayVQRem
        end
        item
          Format = '0.00'
          Kind = skSum
          Column = VDelayVRSUMIN
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      object VDelayVhead: TcxGridDBColumn
        Caption = #8470' '#1076#1086#1082'-'#1090#1072
        DataBinding.FieldName = 'head'
      end
      object VDelayVCode: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'Code'
        Width = 48
      end
      object VDelayVGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'GR'
      end
      object VDelayVSGR: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGR'
      end
      object VDelayVCat: TcxGridDBColumn
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
        DataBinding.FieldName = 'Cat'
      end
      object VDelayVNameCode: TcxGridDBColumn
        Caption = #1058#1086#1074#1072#1088
        DataBinding.FieldName = 'NameCode'
        Styles.Content = dmMC.cxStyle1
        Width = 328
      end
      object VDelayVICLI: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCLI'
        Visible = False
        Width = 41
      end
      object VDelayVName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '
        DataBinding.FieldName = 'Namecli'
        Width = 228
      end
      object VDelayVFullName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' ('#1087#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077')'
        DataBinding.FieldName = 'FullNamecli'
        Styles.Content = dmMC.cxStyle1
        Width = 230
      end
      object VDelayVDepart: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'Depart'
        Visible = False
      end
      object VDelayVDepName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'DepartName'
        Width = 108
      end
      object VDelayVQ: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1085#1072#1095'.'
        DataBinding.FieldName = 'QUANT'
        Styles.Content = dmMC.cxStyle1
        Width = 73
      end
      object VDelayVQOut: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1074#1077#1088#1085#1091#1083#1080
        DataBinding.FieldName = 'QUANTOUT'
        Styles.Content = dmMC.cxStyle1
        Width = 73
      end
      object VDelayVQRem: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
        DataBinding.FieldName = 'QRem'
        Styles.Content = dmMC.cxStyle1
        Width = 76
      end
      object VDelayVProcent: TcxGridDBColumn
        Caption = '% '#1074#1086#1079#1074#1088#1072#1090#1072' '#1086#1090' '#1079#1072#1103#1074#1082#1080
        DataBinding.FieldName = 'Procent'
      end
      object VDelayVPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PriceIn'
      end
      object VDelayVRSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUMIN'
      end
      object VDelayVDocDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DOCDATE'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.SaveTime = False
        Properties.ShowTime = False
        Options.Editing = False
        Width = 107
      end
      object VDelayVIDATEV: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
        DataBinding.FieldName = 'DateVoz'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.SaveTime = False
        Properties.ShowTime = False
        Options.Editing = False
        Styles.Content = dmMC.cxStyle1
        Width = 94
      end
      object VDelayVDelay: TcxGridDBColumn
        Caption = #1044#1085#1077#1081' '#1087#1088#1086#1089#1088#1086#1095#1077#1085#1086
        DataBinding.FieldName = 'Delay'
        Width = 88
      end
      object VDelayVUser: TcxGridDBColumn
        Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        DataBinding.FieldName = 'USER'
        Width = 111
      end
      object VDelayVReason: TcxGridDBColumn
        Caption = #1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088'.'
        DataBinding.FieldName = 'IDReason'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'Reason'
          end>
        Properties.ListSource = dmP.dsquReasV
        Properties.ReadOnly = True
        Width = 110
      end
      object VDelayVMOLVOZ: TcxGridDBColumn
        Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1080#1090#1077#1083#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'MOLVOZ'
        Styles.Content = dmMC.cxStyle1
        Width = 194
      end
      object VDelayVPhone: TcxGridDBColumn
        Caption = #1058#1077#1083#1077#1092#1086#1085
        DataBinding.FieldName = 'PHONEVOZ'
        Styles.Content = dmMC.cxStyle1
        Width = 123
      end
      object VDelayVEMAILV: TcxGridDBColumn
        Caption = 'EMAIL '
        DataBinding.FieldName = 'EMAILVOZ'
        Styles.Content = dmMC.cxStyle1
        Width = 149
      end
      object VDelayVTypeV: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
        DataBinding.FieldName = 'TypeVoz'
        Options.Editing = False
      end
    end
    object LDelayV: TcxGridLevel
      GridView = VDelayV
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1012
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16765348
    TabOrder = 1
    InternalVer = 1
    object CheckBox1: TCheckBox
      Left = 240
      Top = 24
      Width = 97
      Height = 17
      Caption = 'CheckBox1'
      TabOrder = 0
      Visible = False
      OnClick = CheckBox1Click
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 844
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      Caption = 'SpeedItem10'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Spacing = 1
      Left = 114
      Top = 4
      Visible = True
      OnClick = SpeedItem10Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1042#1089#1077
      Caption = #1042#1089#1077
      Hint = #1042#1089#1077
      Spacing = 1
      Left = 584
      Top = 4
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1055#1088#1086#1089#1088#1086#1095'.'
      Caption = #1055#1088#1086#1089#1088#1086#1095'.'
      Hint = #1055#1088#1086#1089#1088#1086#1095'.'
      Spacing = 1
      Left = 374
      Top = 4
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 488
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    Properties.ReadOnly = True
    Style.BorderStyle = ebsOffice11
    TabOrder = 2
    Height = 56
    Width = 1012
  end
  object PBar1: TcxProgressBar
    Left = 0
    Top = 467
    Align = alBottom
    ParentColor = False
    Position = 100.000000000000000000
    Properties.BarBevelOuter = cxbvRaised
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clBlue
    Properties.EndColor = 16745090
    Properties.PeakValue = 100.000000000000000000
    Style.Color = clWhite
    TabOrder = 3
    Visible = False
    Width = 1012
  end
  object PopupMenu1: TPopupMenu
    Left = 184
    Top = 176
    object N1: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnClick = N1Click
    end
  end
  object teDelayV: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 64
    Top = 144
    object teDelayVhead: TIntegerField
      FieldName = 'head'
    end
    object teDelayVCode: TIntegerField
      FieldName = 'Code'
    end
    object teDelayVNameCode: TStringField
      FieldName = 'NameCode'
      Size = 50
    end
    object teDelayVDepart: TIntegerField
      FieldName = 'Depart'
    end
    object teDelayVDepartName: TStringField
      FieldName = 'DepartName'
      Size = 50
    end
    object teDelayVIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object teDelayVNamecli: TStringField
      FieldName = 'Namecli'
      Size = 50
    end
    object teDelayVFullNamecli: TStringField
      FieldName = 'FullNamecli'
      Size = 100
    end
    object teDelayVQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object teDelayVQUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
      DisplayFormat = '0.000'
    end
    object teDelayVQRem: TFloatField
      FieldName = 'QRem'
      DisplayFormat = '0.000'
    end
    object teDelayVProcent: TFloatField
      FieldName = 'Procent'
      DisplayFormat = '0.00'
    end
    object teDelayVRSUMIN: TFloatField
      FieldName = 'RSUMIN'
      DisplayFormat = '0.00'
    end
    object teDelayVDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object teDelayVDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object teDelayVUSER: TStringField
      FieldName = 'USER'
    end
    object teDelayVIDReason: TIntegerField
      FieldName = 'IDReason'
    end
    object teDelayVMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 50
    end
    object teDelayVPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object teDelayVEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 50
    end
    object teDelayVDelay: TIntegerField
      FieldName = 'Delay'
    end
    object teDelayVGR: TStringField
      FieldName = 'GR'
    end
    object teDelayVSGR: TStringField
      FieldName = 'SGR'
      Size = 50
    end
    object teDelayVCat: TStringField
      FieldName = 'Cat'
      Size = 50
    end
    object teDelayVPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object teDelayVTypeVoz: TStringField
      FieldName = 'TypeVoz'
    end
  end
  object dsteDelayV: TDataSource
    DataSet = teDelayV
    Left = 68
    Top = 204
  end
end
