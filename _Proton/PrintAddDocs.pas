unit PrintAddDocs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxTextEdit, cxMaskEdit, cxSpinEdit, cxControls, cxContainer,
  cxEdit, cxCheckBox, StdCtrls, Menus, cxLookAndFeelPainters, cxMemo,
  cxButtons,cxGridCustomTableView, FR_DSet, FR_DBSet, FR_Class, DB,
  dxmdaset;

type
  TfmPrintAdd = class(TForm)
    Label1: TLabel;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxSpinEdit1: TcxSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    cxSpinEdit2: TcxSpinEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Memo1: TcxMemo;
    frRepADDDoc: TfrReport;
    frtePrintD: TfrDBDataSet;
    tePrintD: TdxMemData;
    tePrintDNum: TIntegerField;
    tePrintDFullName: TStringField;
    tePrintDCode: TIntegerField;
    tePrintDEdIzm: TIntegerField;
    tePrintDKolMest: TFloatField;
    tePrintDKolWithMest: TFloatField;
    tePrintDKol: TFloatField;
    tePrintDPrice0: TFloatField;
    tePrintDPrice: TFloatField;
    tePrintDPrNDS: TFloatField;
    tePrintDSumNDS: TFloatField;
    tePrintDRSum0: TFloatField;
    tePrintDRSum: TFloatField;
    tePrintDMassa: TFloatField;
    cxCheckBox3: TcxCheckBox;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPrintAdd: TfmPrintAdd;

implementation

uses MDB, MT, DocsIn, Un1, u2fdk, sumprops, DocsOut;

{$R *.dfm}

procedure TfmPrintAdd.cxButton2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmPrintAdd.cxButton1Click(Sender: TObject);
Var i,j,iNum,iC:INteger;
    Rec:TcxCustomGridRecord;
    sStr:String;
    rNds:Real;
    rSum0,rSumNDS:Real;
begin
  if fmPrintAdd.Tag=1 then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('������ ������.'); delay(10);
    with dmMC do
    with dmMT do
    with fmDocs1 do
    begin
      if ptInLn.Active=False then ptINLn.Active:=True else ptInLn.Refresh;
      ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';
      if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

      for i:=0 to fmDocs1.ViewDocsIn.Controller.SelectedRecordCount-1 do
      begin
        Rec:=fmDocs1.ViewDocsIn.Controller.SelectedRecords[i];
        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
          if fmDocs1.ViewDocsIn.Columns[j].Name='ViewDocsInID' then begin iNum:=Rec.Values[j]; break; end;

        if (iNum>0) then //���������� ID
        begin
          if quTTnIn.Locate('ID',iNum,[]) then //����� ������ - ���� ��������
          begin
            if quTTNInProvodType.AsInteger=1 then
            begin
              Memo1.Lines.Add('  ������ ���� � '+quTTnInNumber.AsString+' �� '+quTTnInDateInvoice.AsString+ ' ���������� - '+CommonSet.NamePost);

              closete(tePrintD);

              ptInLn.CancelRange;
              ptInLn.SetRange([quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)],[quTTnInDepart.AsInteger,quTTnInDateInvoice.AsDateTime,AnsiToOemConvert(quTTnInNumber.AsString)]);
              ptInLn.First;
              while not ptInLn.Eof do
              begin
                if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
                begin
                  rNDS:=ptInLnNDSProc.AsFloat;
                  rSumNDS:=ptInLnNDSSum.AsFloat;
                  rSum0:=ptInLnOutNDSSum.AsFloat;

                  if abs(rNDS)<0.01 then //��� �� ���������
                  begin
                    rNDS:=10;
                    rSumNDS:=rv(ptInLnSumCenaTovarPost.AsFloat/(100+rNDS)*rNDS);
                    rSum0:=ptInLnSumCenaTovarPost.AsFloat-rSumNDS;
                  end;

                  tePrintD.Append;
                  tePrintDNum.AsInteger:=Trunc(ptInLnMediatorCost.AsFloat);
                  tePrintDFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                  tePrintDCode.AsInteger:=ptInLnCodeTovar.AsInteger;
                  tePrintDEdIzm.AsInteger:=ptInLnCodeEdIzm.AsInteger;
                  tePrintDKolMest.AsFloat:=ptInLnKolMest.AsInteger;
                  tePrintDKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
                  tePrintDKol.AsFloat:=ptInLnKol.AsFloat;
                  tePrintDPrice0.AsFloat:=0;
                  if ptInLnKol.AsFloat<>0 then tePrintDPrice0.AsFloat:=rv(rSum0/ptInLnKol.AsFloat);
                  tePrintDPrice.AsFloat:=ptInLnCenaTovar.AsFloat;
                  tePrintDPrNDS.AsFloat:=rNDS;
                  tePrintDSumNDS.AsFloat:=rSumNDS;
                  tePrintDRSum0.AsFloat:=rSum0;
                  tePrintDRSum.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
                  tePrintDMassa.AsFloat:=0;
                  tePrintD.Post;

                end;
                ptInLn.Next;
              end;

              if cxCheckBox1.Checked then //���������
              begin
                frRepADDDoc.LoadFromFile(CommonSet.Reports + 'torg12in.frf');

                frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnInDateInvoice.AsDateTime)+quTTnInNumber.AsString;
                frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime);
                frVariables.Variable['RSum']:=quTTnInSummaTovarPost.AsFloat;
                frVariables.Variable['RSum0']:=quTTnInSummaTovarPost.AsFloat-quTTnInNDSTovar.AsFloat;
                frVariables.Variable['RSumNDS']:=quTTnInNDSTovar.AsFloat;
                frVariables.Variable['SSum']:=MoneyToString(quTTnInSummaTovarPost.AsFloat,True,False);
                frVariables.Variable['TypeP']:=1;

                sStr:=MoneyToString(tePrintD.RecordCount,True,False);
                if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
                frVariables.Variable['SQStr']:=' ('+sStr+')';

                frVariables.Variable['Osnovanie']:='�������� �������';

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=1;
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;
                end else
                begin
                  frVariables.Variable['Cli1Name']:='';
                  frVariables.Variable['Cli1Inn']:='';
                  frVariables.Variable['Cli1Adr']:='';
                  frVariables.Variable['Otpr']:='';
                  frVariables.Variable['OtprAdr']:='';
                  frVariables.Variable['Cli1RSch']:='';
                  frVariables.Variable['Cli1Bank']:='';
                  frVariables.Variable['Cli1KSch']:='';
                  frVariables.Variable['Cli1Bik']:='';
                end;
                quDepPars.Active:=False;

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=quTTnInDepart.AsInteger; //
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;
                end else
                begin
                  frVariables.Variable['Cli2Name']:='';
                  frVariables.Variable['Cli2Inn']:='';
                  frVariables.Variable['Cli2Adr']:='';
                  frVariables.Variable['Poluch']:='';
                  frVariables.Variable['PoluchAdr']:='';
                  frVariables.Variable['Cli2RSch']:='';
                  frVariables.Variable['Cli2Bank']:='';
                  frVariables.Variable['Cli2KSch']:='';
                  frVariables.Variable['Cli2Bik']:='';
                end;
                quDepPars.Active:=False;

{
[Cli1Name] ��� [Cli1Inn],[OtprAdr], �.��. [Cli1RSch], � [Cli1Bank], ����.��. [Cli1KSch], ��� [Cli1Bik]
[Cli2Name], ��� [Cli2Inn], [PoluchAdr],  �.��. [Cli2RSch], � [Cli2Bank], ����.��. [Cli2KSch], ��� [Cli2Bik]
}

                frRepADDDoc.ReportName:='���������.';
                frRepADDDoc.PrepareReport;

//                frRepADDDoc.ShowPreparedReport;
                frRepADDDoc.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);
              end;

              if cxCheckBox2.Checked then //���� �������
              begin
                if cxCheckBox3.Checked then frRepADDDoc.LoadFromFile(CommonSet.Reports + 'schfin_star.frf')
                else frRepADDDoc.LoadFromFile(CommonSet.Reports + 'schfin.frf');

                frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnInDateInvoice.AsDateTime)+quTTnInNumber.AsString;
                frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime);
                frVariables.Variable['RSum']:=quTTnInSummaTovarPost.AsFloat;
                frVariables.Variable['RSum0']:=quTTnInSummaTovarPost.AsFloat-quTTnInNDSTovar.AsFloat;
                frVariables.Variable['RSumNDS']:=quTTnInNDSTovar.AsFloat;
{                frVariables.Variable['SSum']:=MoneyToString(quTTnInSummaTovarPost.AsFloat,True,False);
                frVariables.Variable['TypeP']:=1;
                sStr:=MoneyToString(tePrintD.RecordCount,True,False);
                if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
                frVariables.Variable['SQStr']:=' ('+sStr+')';
}

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=1;
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
                  frVariables.Variable['FL']:=quDepParsFL.AsString;

                  {                  frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;}
                end else
                begin
                  frVariables.Variable['Cli1Name']:='';
                  frVariables.Variable['Cli1Inn']:='';
                  frVariables.Variable['Cli1Adr']:='';
                  frVariables.Variable['Otpr']:='';
                  frVariables.Variable['OtprAdr']:='';
                  frVariables.Variable['OGRN']:='';
                  frVariables.Variable['FL']:='';
{                  frVariables.Variable['Cli1RSch']:='';
                  frVariables.Variable['Cli1Bank']:='';
                  frVariables.Variable['Cli1KSch']:='';
                  frVariables.Variable['Cli1Bik']:='';}
                end;
                quDepPars.Active:=False;

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=quTTnInDepart.AsInteger; //
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
{                  frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;}
                end else
                begin
                  frVariables.Variable['Cli2Name']:='';
                  frVariables.Variable['Cli2Inn']:='';
                  frVariables.Variable['Cli2Adr']:='';
                  frVariables.Variable['Poluch']:='';
                  frVariables.Variable['PoluchAdr']:='';
{                  frVariables.Variable['Cli2RSch']:='';
                  frVariables.Variable['Cli2Bank']:='';
                  frVariables.Variable['Cli2KSch']:='';
                  frVariables.Variable['Cli2Bik']:='';}
                end;
                quDepPars.Active:=False;

                frRepADDDoc.ReportName:='����.';
                frRepADDDoc.PrepareReport;

//                frRepADDDoc.ShowPreparedReport;
                frRepADDDoc.PrintPreparedReport('',cxSpinEdit2.Value,False,frAll);
              end;

//              closete(tePrintD);
            end else
              Memo1.Lines.Add('  ���������� ���� � '+quTTnInNumber.AsString+' �� '+quTTnInDateInvoice.AsString+ ' ���������� - �������.');
          end;
        end;
      end;
    end;
    Memo1.Lines.Add('������ ���������.'); delay(10);
  end;

  if fmPrintAdd.Tag=2 then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('������ ������.'); delay(10);
    with dmMC do
    with dmMT do
    with fmDocs2 do
    begin
      if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
      ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

      if taCards.Active=False then taCards.Active:=True else taCards.Refresh;

      for i:=0 to fmDocs2.ViewDocsOut.Controller.SelectedRecordCount-1 do
      begin
        Rec:=fmDocs2.ViewDocsOut.Controller.SelectedRecords[i];
        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
          if fmDocs2.ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin iNum:=Rec.Values[j]; break; end;

        if (iNum>0) then //���������� ID
        begin
          if quTTnOut.Locate('ID',iNum,[]) then //����� ������ - ���� ��������
          begin
            if quTTnOutChekBuh.AsInteger=1 then
            begin
              Memo1.Lines.Add('  ������ ���� � '+quTTnOutNumber.AsString+' �� '+quTTnOutDateInvoice.AsString+ ' ���������� - '+CommonSet.NamePost);

              closete(tePrintD);
              iC:=1;

              ptOutLn.CancelRange;
              ptOutLn.SetRange([quTTnOutDepart.AsInteger,quTTnOutDateInvoice.AsDateTime,AnsiToOemConvert(quTTnOutNumber.AsString)],[quTTnOutDepart.AsInteger,quTTnOutDateInvoice.AsDateTime,AnsiToOemConvert(quTTnOutNumber.AsString)]);
              ptOutLn.First;
              while not ptOutLn.Eof do
              begin
                if taCards.FindKey([ptOutLnCodeTovar.AsInteger]) then
                begin
                  rNDS:=0;
                  rSumNDS:=0;
                  rSum0:=ptOutLnSumCenaTovar.AsFloat;

                  tePrintD.Append;
                  tePrintDNum.AsInteger:=iC;
                  tePrintDFullName.AsString:=OemToAnsiConvert(taCardsFullName.AsString);
                  tePrintDCode.AsInteger:=ptOutLnCodeTovar.AsInteger;
                  tePrintDEdIzm.AsInteger:=ptOutLnCodeEdIzm.AsInteger;
                  tePrintDKolMest.AsFloat:=ptOutLnKolMest.AsInteger;
                  tePrintDKolWithMest.AsFloat:=ptOutLnKolWithMest.AsFloat;
                  tePrintDKol.AsFloat:=ptOutLnKol.AsFloat;
                  tePrintDPrice0.AsFloat:=0;
                  if ptOutLnKol.AsFloat<>0 then tePrintDPrice0.AsFloat:=rv(rSum0/ptOutLnKol.AsFloat);
                  tePrintDPrice.AsFloat:=ptOutLnCenaTovar.AsFloat; //
                  tePrintDPrNDS.AsFloat:=rNDS;
                  tePrintDSumNDS.AsFloat:=rSumNDS;
                  tePrintDRSum0.AsFloat:=rSum0;
                  tePrintDRSum.AsFloat:=ptOutLnSumCenaTovar.AsFloat; //
                  tePrintDMassa.AsFloat:=0;
                  tePrintD.Post;
                end;
                ptOutLn.Next; inc(iC);
              end;

              if cxCheckBox1.Checked then //���������
              begin
                frRepADDDoc.LoadFromFile(CommonSet.Reports + 'torg12in.frf');

                frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnOutDateInvoice.AsDateTime)+quTTnOutNumber.AsString;
                frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnOutDateInvoice.AsDateTime);
                frVariables.Variable['RSum']:=quTTnOutSummaTovar.AsFloat;
                frVariables.Variable['RSum0']:=quTTnOutSummaTovar.AsFloat-quTTnOutNDSTovar.AsFloat;
                frVariables.Variable['RSumNDS']:=quTTnOutNDSTovar.AsFloat;
                frVariables.Variable['SSum']:=MoneyToString(quTTnOutSummaTovar.AsFloat,True,False);
                frVariables.Variable['TypeP']:=1;

                sStr:=MoneyToString(tePrintD.RecordCount,True,False);
                if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
                frVariables.Variable['SQStr']:=' ('+sStr+')';

                frVariables.Variable['Osnovanie']:='�������� �������';

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=quTTnOutDepart.AsInteger;
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;
                end else
                begin
                  frVariables.Variable['Cli1Name']:='';
                  frVariables.Variable['Cli1Inn']:='';
                  frVariables.Variable['Cli1Adr']:='';
                  frVariables.Variable['Otpr']:='';
                  frVariables.Variable['OtprAdr']:='';
                  frVariables.Variable['Cli1RSch']:='';
                  frVariables.Variable['Cli1Bank']:='';
                  frVariables.Variable['Cli1KSch']:='';
                  frVariables.Variable['Cli1Bik']:='';
                end;
                quDepPars.Active:=False;

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=1; //
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;
                end else
                begin
                  frVariables.Variable['Cli2Name']:='';
                  frVariables.Variable['Cli2Inn']:='';
                  frVariables.Variable['Cli2Adr']:='';
                  frVariables.Variable['Poluch']:='';
                  frVariables.Variable['PoluchAdr']:='';
                  frVariables.Variable['Cli2RSch']:='';
                  frVariables.Variable['Cli2Bank']:='';
                  frVariables.Variable['Cli2KSch']:='';
                  frVariables.Variable['Cli2Bik']:='';
                end;
                quDepPars.Active:=False;

{
[Cli1Name] ��� [Cli1Inn],[OtprAdr], �.��. [Cli1RSch], � [Cli1Bank], ����.��. [Cli1KSch], ��� [Cli1Bik]
[Cli2Name], ��� [Cli2Inn], [PoluchAdr],  �.��. [Cli2RSch], � [Cli2Bank], ����.��. [Cli2KSch], ��� [Cli2Bik]
}

                frRepADDDoc.ReportName:='���������.';
                frRepADDDoc.PrepareReport;

//                frRepADDDoc.ShowPreparedReport;
                frRepADDDoc.PrintPreparedReport('',cxSpinEdit1.Value,False,frAll);
              end;

              if cxCheckBox2.Checked then //���� �������
              begin
                frRepADDDoc.LoadFromFile(CommonSet.Reports + 'schfin.frf');

                frVariables.Variable['DocNum']:=FormatDateTime('dd',quTTnOutDateInvoice.AsDateTime)+quTTnOutNumber.AsString;
                frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnOutDateInvoice.AsDateTime);
                frVariables.Variable['RSum']:=quTTnOutSummaTovar.AsFloat;
                frVariables.Variable['RSum0']:=quTTnOutSummaTovar.AsFloat-quTTnOutNDSTovar.AsFloat;
                frVariables.Variable['RSumNDS']:=quTTnOutNDSTovar.AsFloat;
{                frVariables.Variable['SSum']:=MoneyToString(quTTnOutSummaTovarPost.AsFloat,True,False);
                frVariables.Variable['TypeP']:=1;
                sStr:=MoneyToString(tePrintD.RecordCount,True,False);
                if pos('���',sStr)>0 then SStr:=Copy(sStr,1,pos('���',sStr)-1);
                frVariables.Variable['SQStr']:=' ('+sStr+')';
}

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=quTTnOutDepart.AsInteger;
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;
                  frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
                  frVariables.Variable['FL']:=quDepParsFL.AsString;

                  {                  frVariables.Variable['Cli1RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli1Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli1KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli1Bik']:=quDepParsBik.AsString;}
                end else
                begin
                  frVariables.Variable['Cli1Name']:='';
                  frVariables.Variable['Cli1Inn']:='';
                  frVariables.Variable['Cli1Adr']:='';
                  frVariables.Variable['Otpr']:='';
                  frVariables.Variable['OtprAdr']:='';
                  frVariables.Variable['OGRN']:='';
                  frVariables.Variable['FL']:='';
{                  frVariables.Variable['Cli1RSch']:='';
                  frVariables.Variable['Cli1Bank']:='';
                  frVariables.Variable['Cli1KSch']:='';
                  frVariables.Variable['Cli1Bik']:='';}
                end;
                quDepPars.Active:=False;

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=1; //
                quDepPars.Active:=True;

                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli2Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli2Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli2Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Poluch']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['PoluchAdr']:=quDepParsADROTPR.AsString;
{                  frVariables.Variable['Cli2RSch']:=quDepParsRSch.AsString;
                  frVariables.Variable['Cli2Bank']:=quDepParsBank.AsString;
                  frVariables.Variable['Cli2KSch']:=quDepParsKSch.AsString;
                  frVariables.Variable['Cli2Bik']:=quDepParsBik.AsString;}
                end else
                begin
                  frVariables.Variable['Cli2Name']:='';
                  frVariables.Variable['Cli2Inn']:='';
                  frVariables.Variable['Cli2Adr']:='';
                  frVariables.Variable['Poluch']:='';
                  frVariables.Variable['PoluchAdr']:='';
{                  frVariables.Variable['Cli2RSch']:='';
                  frVariables.Variable['Cli2Bank']:='';
                  frVariables.Variable['Cli2KSch']:='';
                  frVariables.Variable['Cli2Bik']:='';}
                end;
                quDepPars.Active:=False;

                frRepADDDoc.ReportName:='����.';
                frRepADDDoc.PrepareReport;

//                frRepADDDoc.ShowPreparedReport;
                frRepADDDoc.PrintPreparedReport('',cxSpinEdit2.Value,False,frAll);
              end;

//              closete(tePrintD);
            end else
              Memo1.Lines.Add('  ���������� ���� � '+quTTnOutNumber.AsString+' �� '+quTTnOutDateInvoice.AsString+ ' ���������� - �������.');
          end;
        end;
      end;
    end;
    Memo1.Lines.Add('������ ���������.'); delay(10);
  end;
end;

end.
