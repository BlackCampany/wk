unit TestPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons;

type
  TfmTestPrice = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    ViewTP: TcxGridDBTableView;
    LevelTP: TcxGridLevel;
    GridTP: TcxGrid;
    Label1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewTPNum: TcxGridDBColumn;
    ViewTPIdGoods: TcxGridDBColumn;
    ViewTPNameG: TcxGridDBColumn;
    ViewTPSM: TcxGridDBColumn;
    ViewTPQuant: TcxGridDBColumn;
    ViewTPPrice1: TcxGridDBColumn;
    ViewTPPricePP: TcxGridDBColumn;
    ViewTPDif: TcxGridDBColumn;
    cxButton3: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTestPrice: TfmTestPrice;

implementation

uses AddDoc1, dmOffice, Un1;

{$R *.dfm}

procedure TfmTestPrice.FormCreate(Sender: TObject);
begin
  GridTP.Align:=AlClient;
end;

procedure TfmTestPrice.cxButton3Click(Sender: TObject);
begin
  prNExportExel5(ViewTP);
end;

end.
