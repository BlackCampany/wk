unit RnOffice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar;

type
  TfmMainRnOffice = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acMenu: TAction;
    acModify: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    acCateg: TAction;
    SpeedItem5: TSpeedItem;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure acModifyExecute(Sender: TObject);
    procedure acCategExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainRnOffice: TfmMainRnOffice;

implementation

uses Un1, dmRnEdit, PerA, MenuCr, ModifCr, Categories;

{$R *.dfm}

procedure TfmMainRnOffice.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainRnOffice.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

procedure TfmMainRnOffice.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainRnOffice.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmC do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul6=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA:=TfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA.Release;

    bMenuList:=False;
    bMenuListMo:=False;
    fmMenuCr:=tfmMenuCr.Create(Application);
    fmModCr:=tfmModCr.Create(Application);

    fmMenuCr.Show;
    bMenuList:=True;
    bMenuListMo:=True;

  end
  else
  begin
    fmPerA.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRnOffice.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRnOffice.acMenuExecute(Sender: TObject);
begin
  //Menu
  fmMenuCr.Show;
end;

procedure TfmMainRnOffice.acModifyExecute(Sender: TObject);
begin
  //modify
  fmModCr.Show;
end;

procedure TfmMainRnOffice.acCategExecute(Sender: TObject);
begin
  fmCateg.Show;
end;

end.
