unit Zadan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox;

type
  TfmZadan = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ViewZad: TcxGridDBTableView;
    LevelZad: TcxGridLevel;
    GrZad: TcxGrid;
    ViewZadId: TcxGridDBColumn;
    ViewZadIType: TcxGridDBColumn;
    ViewZadComment: TcxGridDBColumn;
    ViewZadStatus: TcxGridDBColumn;
    ViewZadZDate: TcxGridDBColumn;
    ViewZadZTime: TcxGridDBColumn;
    ViewZadPersonName: TcxGridDBColumn;
    ViewZadFormDate: TcxGridDBColumn;
    ViewZadFormTime: TcxGridDBColumn;
    ViewZadIdDoc: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmZadan: TfmZadan;

implementation

uses MDB, Un1, Period1;

{$R *.dfm}

procedure TfmZadan.FormCreate(Sender: TObject);
begin
  GrZad.Align:=AlClient;
end;

procedure TfmZadan.SpeedItem1Click(Sender: TObject);
begin
  dmMC.quZad.Active:=False;
  Close;
end;

procedure TfmZadan.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewZad);
end;

procedure TfmZadan.SpeedItem2Click(Sender: TObject);
begin
//������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      fmZadan.Caption:='������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      ViewZad.BeginUpdate;
      try
        quZad.Active:=False;
        quZad.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
        quZad.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);
        quZad.Active:=True;
      finally
        ViewZad.EndUpdate;
      end;
    end;
  end;
 end;

end.
