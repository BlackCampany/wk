unit AddCassir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit, StdCtrls,
  cxButtons, ExtCtrls, cxCheckBox, cxButtonEdit;

type
  TfmAddCassir = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    Label3: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    OpenDialog1: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddCassir: TfmAddCassir;

implementation

{$R *.dfm}

procedure TfmAddCassir.FormShow(Sender: TObject);
begin
  cxButtonEdit1.Text:='';
end;

procedure TfmAddCassir.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

end.
