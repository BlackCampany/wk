unit InvVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, SpeedBar, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ActnList, XPStyleActnCtrls,
  ActnMan, Placemnt, dxmdaset, cxProgressBar, cxContainer, cxTextEdit,
  cxMemo, cxImageComboBox, Menus, StdCtrls, cxLookAndFeelPainters,
  cxButtons, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLabel, cxMaskEdit, cxButtonEdit;

type
  TfmInvVoz = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    teInvVoz: TdxMemData;
    teInvVozIdCode1: TIntegerField;
    teInvVozNameC: TStringField;
    teInvVozQOut: TFloatField;
    dsteInvVoz: TDataSource;
    FormPlacement1: TFormPlacement;
    amInvVoz: TActionManager;
    teInvVozEdIzm: TIntegerField;
    PBar1: TcxProgressBar;
    acChCli: TAction;
    PopupMenu1: TPopupMenu;
    Panel1: TPanel;
    acRdBar: TAction;
    acRdBar1: TAction;
    Edit1: TEdit;
    GrInvVoz: TcxGrid;
    ViewInvVoz: TcxGridDBTableView;
    ViewInvVozIdCode1: TcxGridDBColumn;
    ViewInvVozNameC: TcxGridDBColumn;
    ViewInvVozQ: TcxGridDBColumn;
    ViewInvVozQFact: TcxGridDBColumn;
    ViewInvVozQRazn: TcxGridDBColumn;
    LevInvVoz: TcxGridLevel;
    OpenDialog1: TOpenDialog;
    cxButton2: TcxButton;
    cxButtonEdit1: TcxButtonEdit;
    cxLabel1: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Label5: TLabel;
    taFact: TdxMemData;
    taFactCode: TIntegerField;
    taFactQF: TFloatField;
    dstaFact: TDataSource;
    teInvVozQFact: TFloatField;
    teInvVozQRazn: TFloatField;
    ViewInvVozRecId: TcxGridDBColumn;
    teInvVozTypeVoz: TIntegerField;
    N3: TMenuItem;
    acShowPost: TAction;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    ViewInvVozEdIzm: TcxGridDBColumn;
    teInvVozRemnCode: TFloatField;
    ViewInvVozRemnCode: TcxGridDBColumn;
    N4: TMenuItem;
    N5: TMenuItem;
    acFactFromPredz: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    cxButton3: TcxButton;
    ViewInvVozLastCli: TcxGridDBColumn;
    teInvVozNameLastCli: TStringField;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewInvVozCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acRdBarExecute(Sender: TObject);
    procedure acRdBar1Execute(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure acShowPostExecute(Sender: TObject);
    procedure ViewInvVozCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure teInvVozQFactChange(Sender: TField);
    procedure N5Click(Sender: TObject);
    procedure acFactFromPredzExecute(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
    procedure prWr(S:String);
    function prnewpredz(iNum,code:integer;NameCode:string;Qobr,Qpr,Qf:real):Boolean;
  //  procedure prnewpredz(code:integer;NameCode:string;Qobr,Qpr,Qf:real);
    procedure clpredz(Code:integer;Qobr:real);
    procedure prFileFact(fName: String);
    procedure prLoadFile(fName: String);
    procedure prLoadFromPredz;
    procedure prLoadFactFromPredz;//�������� ����� ����� �� ��-��� ��� � ���������� (��� ������������� �� ���������� ����������)
    function ClientsTovar(code:integer):Boolean;
    procedure prSaveHead(iNum,i:integer);
var
  fmInvVoz: TfmInvVoz;
  iCol:integer;

implementation

uses Un1,u2fdk,MT, dmPS, Period1, MDB, PostGood, PostGd, ReasonsV,
  ShowPost, mCards, DocsOut;

{$R *.dfm}
procedure prWr(S:String);
begin
  fmInvVoz.Memo1.Lines.Add(FormatDateTime('hh:nn ss:zzz ',now)+S); Delay(10);
end;

function prnewpredz(iNum,code:integer;NameCode:string;Qobr,Qpr,Qf:real):Boolean;
  var ID,idhead,icli,d,Number:integer;
      Dep:string;
      Cena,NewCena,SumCena,SumNewCena,CenaNotNDS,SumCenaNotNDS:real;
      QOtherCli,Qrazn:real;
begin
  with dmMC do
  with dmP do
  with fmInvVoz do
  begin
    quPV.Active:=False;
      quPV.ParamByName('IDCARD').AsInteger:=code;
    quPV.Active:=True;

    if quPV.RecordCount>0 then
    begin
      Dep:=its(fmInvVoz.cxLookupComboBox1.EditValue);
      Qrazn:=Qobr;
      if Qobr<0.001 then Qobr:=0;
      //����� ���� ���������� ����������

      if (quPVIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
      begin                                                 //���� ���� ��������� �����������
        quTypeCli.Active:=false;                           //���� ������ �� ������-�� ����
        quTypeCli.ParamByName('ICLI').AsInteger:=quPVIDALTCLI.AsInteger;
        quTypeCli.Active:=True;
        if (quTypeCli.RecordCount>0) then         //���� �� ���������� ���� ������ (����������, ������������)
        begin
          if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPVIDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
          else
          begin
            if quPVCliTypeV.AsInteger>2 then
              icli:=2471
            else
              icli:=quPVidcli.AsInteger;
          end;
        end
        else
        begin
          if quPVCliTypeV.AsInteger>2 then
            icli:=2471
          else
            icli:=quPVidcli.AsInteger;
        end;                        //��� ������, ����� ����������� ����. ����������
        quTypeCli.Active:=false;

      end
      else
      begin
        if quPVCliTypeV.AsInteger>2 then
          icli:=2471
        else
          icli:=quPVidcli.AsInteger;
      end;        //����������� ����. ����������

      if quPVTypeVoz.AsInteger>2 then  icli:=2471;    //���� ��� �������� � ������ ��������� ���
                                                      //����� � ��������� �� ������ "������������ �����"

      //����� ��������� ���������� � ��������� �����������

      if ClientsTovar(code)=true then
      begin
        quQPost.First;
        QOtherCli:=0;
        while not quQPost.Eof do
        begin
          if quQPostIDCLI.AsInteger<>icli then  //���� ��������� ������
          begin                                 //�� ��������� �� ������ ����������
            quA.Active:=false;                  //� ��������� ���-�� ��� �������� �����
            quA.SQL.Clear;
              quA.SQL.Add('Select DISTINCT IDHEAD from "A_DOCSRET" ');
              quA.SQL.Add('where CODE='+its(code)+' and DEPART='+Dep+'');
              quA.SQL.Add('and IDCLI='+its(quQPostIDCLI.AsInteger)+' and Status<2');
            quA.Active:=true;

            quE.Active:=false;    //
            quE.SQL.Clear;
              quE.SQL.Add('Update "A_DOCSRET" set QRem=0,status=2');
              quE.SQL.Add('where CODE='+its(code)+' and DEPART='+Dep+'');
              quE.SQL.Add('and IDCLI='+its(quQPostIDCLI.AsInteger)+' and Status<2');
            quE.ExecSQL;

            if quA.RecordCount>0 then
            begin
              quA.First;
              while not quA.Eof do
              begin
                idhead:=quA.fieldByName('IDHEAD').Asinteger;

                quE.Active:=false;        //������ ������ � ��������� ����������
                quE.SQL.Clear;
                  quE.SQL.Add('Update "A_DOCHRET" set status=2');
                  quE.SQL.Add('where ID='+its(idhead)+' and (select count(*) from "A_DOCSRET" where IDHEAD='+its(idhead)+' and status<2)=0');
                  quE.SQL.Add(' and status<2');
                quE.ExecSQL;

                delay(10);
                quA.Next;
              end;
            end;

            quE.Active:=false;    //
            quE.SQL.Clear;
              quE.SQL.Add('Update "A_DOCSRET" set QRem=0,status=2');
              quE.SQL.Add('where CODE='+its(code)+' and DEPART='+Dep+'');
              quE.SQL.Add('and IDCLI='+its(quQPostIDCLI.AsInteger)+' and Status<2');
            quE.ExecSQL;

            QOtherCli:=QOtherCli+quQPostQuant.AsFloat;//���-��, ��� ���������� �� ������� ����������

          end;
          delay(10);
          quQPost.Next;
        end;
      end;

      quE.Active:=false;
      quE.SQL.Clear;
      quE.SQL.Add('Select Number from "A_INVENTVOZ"  where Date='''+ds(Now())+'''');
      quE.Active:=true;

      if (quE.RecordCount)>0 then          // quC.RecordCount>0
        Number:=quE.fieldByName('Number').AsInteger
      else
      begin
        quE.Active:=false;
        quE.SQL.Clear;
        quE.SQL.Add('Select max(Number) as Number from "A_INVENTVOZ"');
        quE.Active:=true;
        Number:=quE.fieldByName('Number').AsInteger+1;
      end;
      quE.Active:=false;
      quE.SQL.Clear;
      quE.SQL.Add('Select max(ID) as ID from "A_INVENTVOZ"');
      quE.Active:=true;
      ID:=quE.fieldByName('ID').AsInteger+1;


      quA.Active:=false;
      quA.SQL.Clear;
      quA.SQL.Add('Insert into "A_INVENTVOZ" values ('+its(ID)+','+its(Number)+','''+ds(Date)+''','+Dep+','+its(code)+','''+fs(Qpr)+''',');
      quA.SQL.Add(''''+fs(Qf)+''','''+fs(Qrazn)+''','''+fs(QOtherCli)+''',0,'+its(icli)+')');
      //quA.SQL.SaveToFile('C:\1.txt');
      quA.ExecSQL;

      Qobr:=Qobr+QOtherCli; //���������� ���-�� ��� �������� ����� ����������

      quQPost.Active:=false;
      quE.Active:=false;
      quA.Active:=false;


      if Qobr<0.001 then
      begin
        result:=false;
        exit;
      end;

      result:=true;

      Cena:=RoundVal(quPVPriceIn.AsFloat);      //���� ���� � ���
      NewCena:=RoundVal(quPVRprice.AsFloat);    //���� ���
      CenaNotNDS:=RoundVal(quPVPRICEIN0.AsFloat);   //���� ���� ��� ���

      SumCena:=RoundVal(Cena*Qobr);
      SumNewCena:=RoundVal(NewCena*Qobr);
      SumCenaNotNDS:=RoundVal(CenaNotNDS*Qobr);

      d:=trunc(Date);               //���� ��� �� ������ ������� ����
 
      with dmMC do
      begin
        //�������� ����� ������������ ����������
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "A_DocSRet" values (');
        quA.SQL.Add(its(iNum)+','''',');
        quA.SQL.Add(its(code)+',');  //teInvVozIdCode1.AsInteger
        quA.SQL.Add(its(cxLookupComboBox1.EditValue)+',');
        quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''','); //���-��
        quA.SQL.Add(''''+FloatToStrF(CenaNotNDS,ffFixed,10,2)+''','); //���� ���� ��� ���
        quA.SQL.Add(''''+FloatToStrF(Cena,ffFixed,10,2)+''',');
        quA.SQL.Add(''''+FloatToStrF(SumCenaNotNDS,ffFixed,10,2)+''','); //����� ���� ��� ���
        quA.SQL.Add(''''+FloatToStrF(SumCena,ffFixed,10,2)+''',');
        quA.SQL.Add(''''+FloatToStrF(SumNewCena,ffFixed,10,2)+''',');
        quA.SQL.Add(''''+FloatToStrF(NewCena,ffFixed,10,2)+''',');
        quA.SQL.Add(its(icli)+',');
        quA.SQL.Add('1,');                    //������� ��������
        quA.SQL.Add('0,');     //���-�� �������
        quA.SQL.Add('''0'''+',');
        quA.SQL.Add(''''+FloatToStrF(Qobr,ffFixed,10,3)+''',');   //���-�� ��������
        quA.SQL.Add(its(d)+',');     //���� ��������
        quA.SQL.Add(its(Trunc(Date))+')');    //DocDate
      //  quA.SQL.SaveToFile('C:\1.txt');
        quA.ExecSQL;

      end;
    end
    else
    begin
      prWr(' �� ���� �������� ������ '+ NameCode);
      result:=false;
      //prWr('���������� �� �������');
    end;
  end;
end;
procedure clpredz(Code:integer;Qobr:real);
  var      mindocdate,maxdocdate,pzak:integer;
           Qpredz:real;
           iStep,iC:INteger;
begin

  with dmP do
  with fmInvVoz do
  begin
    if ptHPredz.Active=false then ptHPredz.Active:=true;
    if ptSPredz.Active=false then ptSPredz.Active:=true;

    quPredzV.Active:=false;
    quPredzV.ParamByName('CODE').AsInteger:=Code;//teInvVozIdCode1.AsInteger;
    quPredzV.ParamByName('DEP').AsInteger:=cxLookupComboBox1.EditValue;
    quPredzV.Active:=true;

    PBar1.Position:=0;
    PBar1.Visible:=True;

    Memo1.Clear;
    prWr('����� .. ��������� ������.'); delay(10);

    if (quPredzV.RecordCount>0) then
    begin
      quPredzV.First;
      mindocdate:=quPredzVDOCDATE.AsInteger;
      maxdocdate:=quPredzVDOCDATE.AsInteger;

      while not quPredzV.Eof do
      begin
        if (mindocdate<quPredzVDOCDATE.AsInteger) then maxdocdate:=quPredzVDOCDATE.AsInteger //������� ������ ��� ���������� ���� �� ���������� ��� ���-��
        else maxdocdate:=mindocdate;

        ptSPredz.Locate('ID',quPredzVID.AsInteger,[]);

        Qpredz:=ptSPredzQRem.AsFloat;

        if ((Qpredz-Qobr)<0.001) then //��� ���-�� ��������� ������ ��� ����� ���-�� � ����������
        begin
          Qobr:=(Qobr-Qpredz);

          ptSPredz.Edit;
          ptSPredzQRem.AsFloat:=0;
          ptSPredzStatus.AsString:='2';
          ptSPredz.Post;
        end
        else
        begin
          if ((Qpredz-Qobr)>0) then  //��� ���-�� � ���������� ������ ���-�� ���������
          begin
            ptSPredz.Edit;
            ptSPredzQRem.AsFloat:=Qpredz-Qobr;
            ptSPredzStatus.AsString:='1';
            ptSPredz.Post;
            Qobr:=0;
          end;
        end;
        if (Qobr=0) then break;
        quPredzV.Next;
      end;
      PBar1.Position:=10; delay(10);
                                   //��������� ���� � ��������� ��� ���������� �������, ������ ������ ������ ��������� �������
                                  //���� ����� ������ ������� �� ������ ������ �������� �������
      quDocVoz2.Active:=false;
      quDocVoz2.ParamByName('DATEB').AsInteger:=mindocdate;
      quDocVoz2.ParamByName('DATEE').AsInteger:=maxdocdate;
      quDocVoz2.Active:=true;

      PBar1.Position:=40; delay(30);

      if quDocVoz2.RecordCount>0 then
      begin
        quDocVoz2.First;
        iStep:=quDocVoz2.RecordCount div 60;
        if iStep=0 then iStep:=quDocVoz2.RecordCount;
        iC:=0;
        while not quDocVoz2.Eof do
        begin
          ptHPredz.Locate('ID',quDocVoz2ID.AsInteger,[]);

          quSpecVoz2.Active:=false;
          quSpecVoz2.ParamByName('HEAD').AsInteger:=quDocVoz2ID.AsInteger;
          quSpecVoz2.Active:=true;

          //�������� ������ ��������� ���������� �� ������� ������������
          quSpecVoz2.First;
          pzak:=1;
          while not quSpecVoz2.Eof do
          begin
            if (quSpecVoz2Status.AsString<>'2') then
            begin
              if (quSpecVoz2Status.AsString='1') then
              begin
                pzak:=(-1);
                break;
              end else pzak:=0;
            end
            else
            begin
              if pzak=0 then
              begin
                pzak:=(-1);
                break;
              end;
            end;
            quSpecVoz2.Next;
          end;
          ptHPredz.edit;
          if pzak=0 then ptHPredzSTATUS.AsString:='0';
          if pzak=(-1) then ptHPredzSTATUS.AsString:='1';
          if pzak=1 then ptHPredzSTATUS.AsString:='2';
          ptHPredz.Post;

          //�������� ���������
          quDocVoz2.Next;
          inc(iC);

          if (iC mod iStep = 0) then
          begin
            PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;
        end;
        prWr('��������� ������ ��.'); delay(10);
        PBar1.Position:=100; delay(30);
      end;
    end;
    ptHPredz.Active:=false;
    ptSPredz.Active:=false;
    quPredzV.Active:=false;
    quDocVoz2.Active:=false;
    quSpecVoz2.Active:=false;
  end;
end;

procedure TfmInvVoz.SpeedItem1Click(Sender: TObject);
begin
 close;
end;

procedure TfmInvVoz.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  iCol:=0;
 { if Person.Name='OPER CB' then N1.Visible:=true
  else N1.Visible:=false; }
  dmP.quDepVoz2.Active:=false;
  dmP.quDepVoz2.Active:=true;
  closete(teInvVoz);
  closete(taFact);

end;

procedure TfmInvVoz.FormCreate(Sender: TObject);
begin
  GrInvVoz.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewInvVoz.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);

end;

procedure TfmInvVoz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  iCol:=0;
  ViewInvVoz.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  taFact.Active:=False;
  dmP.quDepVoz2.Active:=false;
  dmP.ptSPredz.Active:=false;
  dmP.ptHPredz.Active:=false;
  dmP.quSelCena.Active:=False;
  dmP.quSpecVoz.Active:=false;
  dmP.quPredzV.Active:=false;
  closete(teInvVoz);
  teInvVoz.Active:=false;
  closete(taFact);
  taFact.Active:=false;
  cxButtonEdit1.Clear;

end;

procedure TfmInvVoz.ViewInvVozCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
 ViewInvVoz.Controller.FocusRecord(ViewInvVoz.DataController.FocusedRowIndex,True);
  if (ViewInvVoz.Controller.FocusedColumn.Name='ViewInvVozQFact') then
  begin
    iCol:=1;
    ViewInvVozQFact.Options.Editing:=True;
    ViewInvVozQFact.Focused:=true;
  end
  else iCol:=0;
end;

procedure TfmInvVoz.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewInvVoz);
end;

procedure TfmInvVoz.acRdBarExecute(Sender: TObject);
begin
  Edit1.SetFocus; Edit1.SelectAll;
end;

procedure TfmInvVoz.acRdBar1Execute(Sender: TObject);
Var sBar:String;
    IDC,ic:INteger;
begin
  if cxLookupComboBox1.Text='' then exit;

  iC:=0;
  Edit1.SetFocus;
  Edit1.SelectAll;

  sBar:=Edit1.Text;
  if sBar='' then exit;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then        //�������
  begin
    sBar:=copy(sBar,1,7);
  end;

  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);


  if sBar='' then exit;

  with dmMC do
  with dmP do
  begin
    quFindBar.Close;
    quFindBar.ParamByName('SBAR').AsString:=sBar;
    quFindBar.ParamByName('IC').AsInteger:=iC;
    quFindBar.Open;
    if quFindBar.RecordCount>0  then
    begin
    //  IDC:=0;
      IDC:=quFindBarGoodsID.AsInteger;
      quCId.Active:=False;
      quCId.ParamByName('IDC').AsInteger:=IDC;
      quCId.Active:=True;
      if quCId.RecordCount>0 then
      begin

        if teInvVoz.Locate('IdCode1',quCIdID.AsInteger,[]) then
        begin
          showmessage('���� ����� ��� ���� � ��������������');
          ViewInvVoz.Controller.FocusRecord(ViewInvVoz.DataController.FocusedRowIndex,True);
          exit;
        end;
        teInvVoz.Append;
        teInvVozIdCode1.AsInteger:=quCIdID.AsInteger;
        teInvVozNameC.AsString:=quCIdFullName.AsString;
        teInvVozEdIzm.AsInteger:=quCIdEdIzm.AsInteger;
        teInvVozQOut.AsFloat:=0;
        teInvVozTypeVoz.AsInteger:=quCIdV14.AsInteger;
        teInvVozQFact.AsFloat:=1;
        teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
        teInvVoz.Post;

        teInvVoz.Edit;
        teInvVozRemnCode.AsFloat:=prFindTRemnDepD(teInvVozIdCode1.AsInteger,cxLookupComboBox1.EditValue,Trunc(Date()));//ptRemnCodeReserv1.AsFloat;
        teInvVoz.Post;

        ViewInvVoz.Controller.FocusRecord(ViewInvVoz.DataController.FocusedRowIndex,True);
        iCol:=1;
        GrInvVoz.SetFocus;
        ViewInvVozQFact.Focused:=true;
        ViewInvVozQFact.Selected:=true;
        ViewInvVozQFact.Options.Editing:=True;
        teInvVoz.Edit;
      end
      else
        Memo1.Lines.Add(formatDateTime('hh.nn ss ',time)+' �������� � �� '+sBar+' ���.');
    end;
    quFindBar.Close;
  end;
end;
procedure TfmInvVoz.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

procedure TfmInvVoz.cxButton2Click(Sender: TObject);
  var  fName: String[80]; // ��� �����
begin
  PBar1.Position:=0;
  PBar1.Visible:=True;
  if (cxLookupComboBox1.Text='') then
  begin
    prWr('�������� �����!');
    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;
    exit;
  end;

  if (cxButtonEdit1.Text='') then
  begin
    prWr('�������� ����!');
    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;
    exit;
  end;


  with dmP do
  begin
    fName:=cxButtonEdit1.Text;

    if FileExists(fName) then
    begin
    //  prWr('  ���� - '+fName+'  ����������.');

      if teInvVoz.RecordCount>0 then
      begin
        if MessageDlg('��������� ������ �� ������ �����?', mtConfirmation, [mbYes, mbNo], 0) = mrYES then
        begin
          if MessageDlg('������������ � ��� ����������� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrYES then
          begin
            prFileFact(fName);  //���������� ������ �� ����� �� ��������� �������
            if PBar1.Visible=False then exit;
            ViewInvVoz.BeginUpdate;
            prLoadFile(fName);  //��������� ������ ����� �� ��������� ���� � �������� ����
          end
          else
          begin
            closete(taFact);
            prFileFact(fName);  //���������� ������ �� ����� �� ��������� �������
            if PBar1.Visible=False then exit;
            prLoadFromPredz;    //��������� ������ �� ����������
            prLoadFile(fName);  //��������� ������ ����� �� ��������� ���� � �������� ����
          end;
        end
        else exit;
      end
      else
      begin
        closete(taFact);
        prFileFact(fName);  //���������� ������ �� ����� �� ��������� �������
        if PBar1.Visible=False then exit;
        prLoadFromPredz;    //��������� ������ �� ����������
        prLoadFile(fName);  //��������� ������ ����� �� ��������� ���� � �������� ����
      end;
    end
    else
    begin
      prWr('���� �� ������ - '+fName);
      PBar1.Position:=100; delay(30);
      PBar1.Visible:=False;
      exit;
    end;
    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;

    prWr('�������� OK');
    teInvVoz.First;
  end;
end;

procedure TfmInvVoz.acShowPostExecute(Sender: TObject);
begin
  if  teInvVoz.RecordCount<1 then exit;
  if ClientsTovar(teInvVozIdCode1.AsInteger)=false then exit;
  fmShowPost.ShowModal;
  dmP.quQPost.Active:=false;
end;

procedure TfmInvVoz.ViewInvVozCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin

 acShowPost.Execute;

end;

procedure TfmInvVoz.cxButton1Click(Sender: TObject);
  var Qobr,Qpr,Qf:real;
      ihead,fl,iNum,Code,iStep,ic,n,m:integer;
      Rec:TcxCustomGridRecord;
      NameCode:string;
begin
    if MessageDlg('����������?',mtConfirmation,[mbYes, mbNo], 0)=mrYES then
  begin
    if ViewInvVoz.Controller.SelectedRecordCount>0 then
    begin
      prWr('������ ������������');
      PBar1.Position:=0;
      PBar1.Visible:=True;
      ViewInvVoz.BeginUpdate;

      with dmMC do
      begin
        fl:=0;
        iNum:=prFindNum(25);
        while fl=0 do
        begin
          fl:=1;
          quC.Active:=False;
          quC.SQL.Clear;
          quC.SQL.Add('Select Count(*) as RecCount from "A_DocHRet" ');
          quC.SQL.Add('where DocNum='+its(iNum));
          quC.Active:=True;

          if quCRecCount.AsInteger>0 then
          begin
           fl:=0;
           iNum:=iNum+1;
          end;
        end;
      end;
      ihead:=0;
      PBar1.Position:=40; delay(10);

      iStep:=ViewInvVoz.Controller.SelectedRecordCount div 60;
      if iStep=0 then iStep:=ViewInvVoz.Controller.SelectedRecordCount;

      iC:=0;


      for n:=0 to ViewInvVoz.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewInvVoz.Controller.SelectedRecords[n];

        for m:=0 to Rec.ValueCount-1 do
        begin
          if ViewInvVoz.Columns[m].Name='ViewInvVozIdCode1' then break;
        end;
        Code:=strtoint(trim(Rec.Values[m]));    //��� ������

        for m:=0 to Rec.ValueCount-1 do
        begin
          if ViewInvVoz.Columns[m].Name='ViewInvVozNameC' then break;
        end;
        NameCode:=trim(Rec.Values[m]);    //�������� ������

        for m:=0 to Rec.ValueCount-1 do
        begin
          if ViewInvVoz.Columns[m].Name='ViewInvVozQRazn' then break;
        end;
        Qobr:=strtofloat(trim(Rec.Values[m]));    //���-�� ��� ���������

        for m:=0 to Rec.ValueCount-1 do
        begin
          if ViewInvVoz.Columns[m].Name='ViewInvVozQ' then break;
        end;
        Qpr:=strtofloat(trim(Rec.Values[m]));    //���-�� �� ����������

        for m:=0 to Rec.ValueCount-1 do
        begin
          if ViewInvVoz.Columns[m].Name='ViewInvVozQFact' then break;
        end;
        Qf:=strtofloat(trim(Rec.Values[m]));    //���-�� ����

        if Qobr<0 then   //���� ����� ���-�� ������ ��� ������
        begin  
          clpredz(Code,abs(Qobr));
        end;

        if (prnewpredz(iNum,Code,NameCode,Qobr,Qpr,Qf)=true) and (ihead=0) then //���� ���� �������
          ihead:=1;                                                           //������������ ����������
                                                                           //�� �������
        inc(iC);
        if (iC mod iStep = 0) then
        begin
          PBar1.Position:=40+(iC div iStep);
          delay(10);
        end;

      end;

      prSaveHead(iNum,ihead);
      if (cxButtonEdit1.Text='') then
      begin
        prWr('����������.');
        PBar1.Position:=100; delay(30);
        PBar1.Visible:=False;
        acFactFromPredz.Execute;
      end
      else
      begin
        prWr('����������. �������� ������');
        PBar1.Position:=100; delay(30);
        PBar1.Visible:=False;
        cxButton2.Click;
      end;
      ViewInvVoz.EndUpdate;      
    end;
  end;
end;

procedure TfmInvVoz.N4Click(Sender: TObject);
begin
  if cxLookupComboBox1.Text>'' then
  begin
    iDirect:=26;
    fmCards.Show;
  end;
end;

procedure TfmInvVoz.teInvVozQFactChange(Sender: TField);
begin

  if (iCol=1) then
  begin
    ViewInvVoz.BeginUpdate;

    teInvVoz.Edit;
    if (teInvVozQFact.AsFloat<0) then teInvVozQFact.AsFloat:=0;

    teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
    teInvVozRemnCode.AsFloat:=prFindTRemnDepD(teInvVozIdCode1.AsInteger,cxLookupComboBox1.EditValue,Trunc(Date()));//ptRemnCodeReserv1.AsFloat;
    teInvVoz.Post;

    teInvVoz.Edit;
    ViewInvVoz.EndUpdate;
  end;
end;

procedure TfmInvVoz.N5Click(Sender: TObject);
begin
  ViewInvVoz.Controller.SelectAll;
end;

procedure prFileFact(fName: String);  //���������� ������ �� ����� �� ��������� �������
  var f:TextFile; // ����
      Str1,Str2:String;
      rQf:real;
      iCode:integer;
begin
  with fmInvVoz do
  begin
    assignfile(f,fName);

    {$IOChecks off}
    Reset(f);  // ������� ��� ������
    if IOResult <> 0 then
    begin
      prWr(' ������ ������� � ����� ' + fName);
      PBar1.Position:=100; delay(30);
      PBar1.Visible:=False;
      exit;
    end;
    {$IOChecks on}

    while (EOF(f)=False) do
    begin
      ReadLn(f,Str1);
      if length(Str1)>=27 then
      begin
        Str2:=Copy(Str1,3,5); while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
        iCode:=StrToINtDef(Str2,0);
        if iCode>0 then
        begin
          Str2:=Copy(Str1,17,10);
          while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
          while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
          rQf:=StrToFloatDef(Str2,0);

          if taFact.Locate('Code',iCode,[])=False then
          begin
            taFact.Append;
            taFactCode.AsInteger:=iCode;
            taFactQF.AsFloat:=rQf;
            taFact.Post;
          end
          else
          begin
            taFact.Edit;
            taFactCode.AsInteger:=iCode;
            taFactQF.AsFloat:=taFactQF.AsFloat+rQf;
            taFact.Post;
          end;
        end;
      end;
    end;
    closefile(f);
  end;
end;
procedure prLoadFile(fName: String);  //��������� ������ ����� �� ��������� ���� � �������� ����
begin
  with fmInvVoz do
  with dmP do
  begin
    prWr('  �������� �� �����.' + fName);

    if (taFact.RecordCount>0) then
    begin
      taFact.First;
      while not taFact.Eof do
      begin
        if teInvVoz.Locate('IdCode1',taFactCode.AsInteger,[]) then
        begin
          teInvVoz.Edit;
          teInvVozQFact.AsFloat:=taFactQF.AsFloat;
          teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
          teInvVoz.Post;
        end
        else
        begin
          quNameCode.Active:=false;
          quNameCode.ParamByName('IDCARD').AsInteger:=taFactCode.AsInteger;
          quNameCode.Active:=true;

          if (quNameCode.RecordCount>0) then
          begin
            if (quNameCodeTypeVoz.AsInteger<>4) then
            begin
              teInvVoz.Append;
              teInvVozIdCode1.AsInteger:=taFactCode.AsInteger;
              teInvVozNameC.AsString:=quNameCodeNameCode.AsString;
              teInvVozEdIzm.AsInteger:=quNameCodeEdIzm.AsInteger;
              teInvVozQOut.AsFloat:=0;
              teInvVozQFact.AsFloat:=taFactQF.AsFloat;
              teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
              teInvVoz.Post;

              teInvVoz.Edit;
              teInvVozRemnCode.AsFloat:=prFindTRemnDepD(teInvVozIdCode1.AsInteger,cxLookupComboBox1.EditValue,Trunc(Date()));//ptRemnCodeReserv1.AsFloat;
              teInvVoz.Post;
            end;
          end;
          quNameCode.Active:=false;
        end;
        taFact.Next;
      end;
    end;
    teInvVoz.First;
    ViewInvVoz.EndUpdate;
  end;
end;

procedure prLoadFromPredz;
  var  iStep,iC:INteger;
begin
  with fmInvVoz do
  with dmP do
  begin
    PBar1.Position:=10; delay(10);

    ViewInvVoz.BeginUpdate;

    quInventV.Active:=false;
    quInventV.ParamByName('IDEP').AsInteger:=cxLookupComboBox1.EditValue;
    quInventV.Active:=true;

    closete(teInvVoz);


    prWr('  �������� �� ����������.');

    PBar1.Position:=40; delay(10);

    if (quInventV.RecordCount>0) then
    begin
      iStep:=quInventV.RecordCount div 60;
      if iStep=0 then iStep:=quInventV.RecordCount;
      iC:=0;

      quInventV.First;
      while not quInventV.Eof do
      begin
        teInvVoz.Append;
        teInvVozIdCode1.AsInteger:=quInventVCODE.AsInteger;
        teInvVozNameC.AsString:=quInventVFNameCode.AsString;
        teInvVozEdIzm.AsInteger:=quInventVEdIzm.AsInteger;
        teInvVozQOut.AsFloat:=quInventVQRem.AsFloat;
        teInvVozTypeVoz.AsInteger:=quInventVTypeVoz.AsInteger;
        teInvVozQFact.AsFloat:=0;
        teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
        teInvVoz.Post;

        teInvVoz.Edit;
        teInvVozRemnCode.AsFloat:=prFindTRemnDepD(teInvVozIdCode1.AsInteger,cxLookupComboBox1.EditValue,Trunc(Date()));//ptRemnCodeReserv1.AsFloat;
        teInvVoz.Post;

        quInventV.Next;

        inc(iC);

        if (iC mod iStep = 0) then
        begin
          PBar1.Position:=40+(iC div iStep);
          delay(10);
        end;

      end;
    end;
    quInventV.Active:=false;
  end;
end;
procedure prLoadFactFromPredz;
  var  iStep,iC:INteger;
begin
  with fmInvVoz do
  begin
    PBar1.Position:=10; delay(10);

    if (teInvVoz.RecordCount>0) then
    begin
      iStep:=teInvVoz.RecordCount div 60;
      if iStep=0 then iStep:=teInvVoz.RecordCount;
      iC:=0;

      teInvVoz.First;
      while not teInvVoz.Eof do
      begin
        teInvVoz.Edit;
          teInvVozQFact.AsFloat:=teInvVozQOut.AsFloat;
          teInvVozQRazn.AsFloat:=0;
        teInvVoz.Post;

        inc(iC);
        if (iC mod iStep = 0) then
        begin
          PBar1.Position:=40+(iC div iStep);
          delay(10);
        end;

        teInvVoz.Next;
      end;
    end;
    ViewInvVoz.EndUpdate;
    PBar1.Position:=100; delay(30);
  end;
end;


procedure TfmInvVoz.acFactFromPredzExecute(Sender: TObject);
begin
  PBar1.Position:=0;
  PBar1.Visible:=True;

  if (cxLookupComboBox1.Text='') then
  begin
    prWr('�������� �����!');
    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;
    exit;
  end;

  closete(taFact);
  if PBar1.Visible=False then exit;

  prLoadFromPredz;     //��������� ������ �� ����������
  prLoadFactFromPredz; //��������� ���� ����� �� ���-��� ��� � ����������

  PBar1.Position:=100; delay(30);
  PBar1.Visible:=False;

  prWr('�������� OK');
  teInvVoz.First;

end;
function ClientsTovar(code:integer):Boolean;
begin
  with dmP do
  begin
    quQPost.Active:=false;
    quQPost.ParamByName('IDEP').AsInteger:=fmInvVoz.cxLookupComboBox1.EditValue;
    quQPost.ParamByName('CODE').AsInteger:=code;
    quQPost.Active:=true;
    if quQPost.RecordCount>0 then
      result:=true
    else
      result:=false;
  end;
end;
procedure TfmInvVoz.N7Click(Sender: TObject);
begin
  with dmMC do
  begin
    if teInvVoz.RecordCount<1 then exit;
    
    fmDocs2.ViewDocsOut.BeginUpdate;
    try
      quTTNOut.Active:=False;
      quTTNOut.ParamByName('DATEB').AsDate:=Trunc(Date);
      quTTNOut.ParamByName('DATEE').AsDate:=Trunc(Date);  // ��� <=
      quTTNOut.Active:=True;
    finally
      fmDocs2.ViewDocsOut.EndUpdate;
    end;
    fmDocs2.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

    iDirect:=26;
    fmDocs2.ShowModal;
    iDirect:=0;
  end;
end;

procedure prSaveHead(iNum,i:integer);
  var user,t:string;
begin
  if i=0 then exit;
  with dmMC do
  begin
    //�������� ����� ��������� ����������
    prWr('������ ����� ���������� �� ������� �'+its(iNum));
    user:=Person.Name;
    t:=ts(now());          

       //�������� �����
    quC.Active:=false;
    quC.SQL.Clear;
    quC.SQL.Add('Select Count(*) as RecCount from "IdPool" ');
    quC.SQL.Add('where CodeObject=25');
    quC.Active:=true;
    if quCRecCount.AsInteger<1 then
    begin
      quA.Active:=false;
      quA.SQL.Clear;
      quA.SQL.Add('Insert into "IdPool" values (25,1) ');
      quA.ExecSQL;
    end
    else
    begin
      quE.Active:=false;
      quE.SQL.Clear;
      quE.SQL.Add('Update "IdPool" Set ID='+its(iNum)+'');
      quE.SQL.Add(' where CodeObject=25');
      quE.ExecSQL;
    end;
    quA.Active:=False;
    quA.SQL.Clear;
    quA.SQL.Add('INSERT into "A_DocHRet" values (');
    quA.SQL.Add(its(iNum)+',');
    quA.SQL.Add(''+its(iNum)+',');                    //DocNum
    quA.SQL.Add(its(Trunc(Date))+',');                //DocDate
    quA.SQL.Add(''''+t+''',');                        //DocTime
    quA.SQL.Add(''''+'�������� ���������� �� ��������������'+''',');   //Comment
    quA.SQL.Add(''''+user+''','+'''0'''+')');                     //User   �������
 //   quA.SQL.SaveToFile('C:\1.txt');
    quA.ExecSQL;
    quC.Active:=false;
    prWr('�������� ���������� OK');
  end;
end;
procedure TfmInvVoz.cxButton3Click(Sender: TObject);
  var icli,iStep,iC:integer;
begin
  if teInvVoz.RecordCount<1 then exit;
  prWr('����� ��������� �����������..');

  PBar1.Position:=0;
  PBar1.Visible:=True;

  ViewInvVoz.BeginUpdate;

   PBar1.Position:=40; delay(10);

  iStep:=teInvVoz.RecordCount div 60;
  if iStep=0 then iStep:=teInvVoz.RecordCount;

  iC:=0;

  with dmP do
  begin
    teInvVoz.First;
    while not teInvVoz.Eof do
    begin
      quPV.Active:=False;
        quPV.ParamByName('IDCARD').AsInteger:=teInvVozIdCode1.AsInteger;
      quPV.Active:=True;

     // icli:=0;

      if quPV.RecordCount>0 then
      begin
        //����� ���� ���������� ����������

        if (quPVIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
        begin                                                 //���� ���� ��������� �����������
          quTypeCli.Active:=false;                           //���� ������ �� ������-�� ����
          quTypeCli.ParamByName('ICLI').AsInteger:=quPVIDALTCLI.AsInteger;
          quTypeCli.Active:=True;
          if (quTypeCli.RecordCount>0) then         //���� �� ���������� ���� ������ (����������, ������������)
          begin
            if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPVIDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
            else
            begin
              if quPVCliTypeV.AsInteger>2 then
                icli:=2471
              else
                icli:=quPVidcli.AsInteger;
            end;
          end
          else
          begin
            if quPVCliTypeV.AsInteger>2 then
              icli:=2471
            else
              icli:=quPVidcli.AsInteger;
          end;                        //��� ������, ����� ����������� ����. ����������
        end
        else
        begin
          if quPVCliTypeV.AsInteger>2 then
            icli:=2471
          else
            icli:=quPVidcli.AsInteger;
        end;        //����������� ����. ����������

        if quPVTypeVoz.AsInteger>2 then  icli:=2471;    //���� ��� �������� � ������ ��������� ���
                                                    //����� � ��������� �� ������ "������������ �����"

      quNameLCli.Active:=false;
        quNameLCli.ParamByName('idcli').AsInteger:=icli;
      quNameLCli.Active:=true;

      teInvVoz.Edit;
        teInvVozNameLastCli.AsString:=quNameLCliName.AsString;
      teInvVoz.Post;

      end;

      inc(iC);
      if (iC mod iStep = 0) then
      begin
        PBar1.Position:=40+(iC div iStep);
        delay(10);
      end;

      teInvVoz.Next;
    end;

    quTypeCli.Active:=false;
    quPV.Active:=False;
    quNameLCli.Active:=false;


    teInvVoz.First;
  end;
  ViewInvVoz.EndUpdate;
  prWr('����� ��������� ����������� ��');
  PBar1.Position:=100; delay(10);
  PBar1.Visible:=False;
end;

end.
