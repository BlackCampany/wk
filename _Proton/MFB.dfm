object dmFB: TdmFB
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 708
  Top = 505
  Height = 409
  Width = 490
  object Cash: TpFIBDatabase
    DBName = 'localhost:E:\PosFb\cash.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelectCash
    DefaultUpdateTransaction = trUpdateCash
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'Cash'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 16
  end
  object trSelectCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdateCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object prAddPos: TpFIBStoredProc
    Transaction = trUpdateCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_ADDPOS (?ITYPE, ?ARTICUL, ?BARCODE, ?CARDSI' +
        'ZE, ?QUANT, ?DSTOP, ?NAME, ?MESSUR, ?PRESISION, ?GROUP1, ?GROUP2' +
        ', ?GROUP3, ?PRICE, ?CLIINDEX, ?DELETED, ?PASSW)')
    StoredProcName = 'PR_ADDPOS'
    Left = 80
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quCashSail: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      'FROM'
      '    CASHS01 '
      'order by  SHOPINDEX, CASHNUMBER,ZNUMBER,CHECKNUMBER,ID')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    Left = 150
    Top = 15
    object quCashSailSHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCashSailCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCashSailZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCashSailCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCashSailID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCashSailCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCashSailARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object quCashSailBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object quCashSailCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object quCashSailQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object quCashSailPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object quCashSailDPROC: TFIBFloatField
      FieldName = 'DPROC'
    end
    object quCashSailDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCashSailTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
    end
    object quCashSailCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCashSailDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object quCashSailOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
  end
  object quCashPay: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,DBAR,max(' +
        'CHDATE) as CHDATE,SUM(CHECKSUM) as CHECKSUM,SUM(PAYSUM) as PAYSU' +
        'M,SUM(DSUM) as DSUM'
      'FROM CASHP01'
      'WHERE CHDATE>='#39'14.02.2011'#39
      'and CHDATE<'#39'15.02.2011'#39
      'group by SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,DBAR'
      'order by  SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    Left = 220
    Top = 16
    poAskRecordCount = True
    object quCashPaySHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCashPayCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCashPayZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCashPayCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCashPayCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCashPayCHECKSUM: TFIBFloatField
      FieldName = 'CHECKSUM'
    end
    object quCashPayPAYSUM: TFIBFloatField
      FieldName = 'PAYSUM'
    end
    object quCashPayDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCashPayDBAR: TFIBStringField
      FieldName = 'DBAR'
      Size = 22
      EmptyStrToNull = True
    end
    object quCashPayCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
  end
  object Casher: TpFIBDatabase
    DBName = '192.168.31.102:E:\_Casher\DB\CASHERDB.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSel1
    DefaultUpdateTransaction = trUpd1
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'CasherDb'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 196
  end
  object trSel1: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 92
    Top = 252
  end
  object trSel2: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 148
    Top = 252
  end
  object trSel3: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 208
    Top = 252
  end
  object trUpd1: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 92
    Top = 308
  end
  object trUpd2: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 148
    Top = 308
  end
  object trUpd3: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 208
    Top = 308
  end
  object trDel: TpFIBTransaction
    DefaultDatabase = Casher
    TimeoutAction = TARollback
    Left = 304
    Top = 264
  end
  object quDel: TpFIBQuery
    Transaction = trDel
    Database = Casher
    Left = 300
    Top = 204
  end
  object taCardsFB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CARDSCLA'
      'SET '
      '    CLASSIF = :CLASSIF,'
      '    DEPART = :DEPART,'
      '    NAME = :NAME,'
      '    CARD_TYPE = :CARD_TYPE,'
      '    MESURIMENT = :MESURIMENT,'
      '    PRICE_RUB = :PRICE_RUB,'
      '    DISCQUANT = :DISCQUANT,'
      '    DISCOUNT = :DISCOUNT,'
      '    SCALE = :SCALE,'
      '    AVID = :AVID,'
      '    ALCVOL = :ALCVOL'
      'WHERE'
      '    ARTICUL = :OLD_ARTICUL'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CARDSCLA'
      'WHERE'
      '        ARTICUL = :OLD_ARTICUL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CARDSCLA('
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE,'
      '    AVID,'
      '    ALCVOL'
      ')'
      'VALUES('
      '    :ARTICUL,'
      '    :CLASSIF,'
      '    :DEPART,'
      '    :NAME,'
      '    :CARD_TYPE,'
      '    :MESURIMENT,'
      '    :PRICE_RUB,'
      '    :DISCQUANT,'
      '    :DISCOUNT,'
      '    :SCALE,'
      '    :AVID,'
      '    :ALCVOL'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE,'
      '    AVID,'
      '    ALCVOL'
      'FROM'
      '    CARDSCLA '
      ''
      ' WHERE '
      '        CARDSCLA.ARTICUL = :OLD_ARTICUL'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE,'
      '    AVID,'
      '    ALCVOL'
      'FROM'
      '    CARDSCLA ')
    Transaction = trSel1
    Database = Casher
    UpdateTransaction = trUpd1
    Left = 94
    Top = 195
    poAskRecordCount = True
    object taCardsFBARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taCardsFBCLASSIF: TFIBIntegerField
      FieldName = 'CLASSIF'
    end
    object taCardsFBDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object taCardsFBNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taCardsFBCARD_TYPE: TFIBIntegerField
      FieldName = 'CARD_TYPE'
    end
    object taCardsFBMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object taCardsFBPRICE_RUB: TFIBFloatField
      FieldName = 'PRICE_RUB'
    end
    object taCardsFBDISCQUANT: TFIBFloatField
      FieldName = 'DISCQUANT'
    end
    object taCardsFBDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
    end
    object taCardsFBSCALE: TFIBStringField
      FieldName = 'SCALE'
      Size = 10
      EmptyStrToNull = True
    end
    object taCardsFBAVID: TFIBIntegerField
      FieldName = 'AVID'
    end
    object taCardsFBALCVOL: TFIBFloatField
      FieldName = 'ALCVOL'
    end
  end
  object taBarFB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BAR'
      'SET '
      '    CARDARTICUL = :CARDARTICUL,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANTITY = :QUANTITY,'
      '    PRICERUB = :PRICERUB'
      'WHERE'
      '    BARCODE = :OLD_BARCODE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    BAR'
      'WHERE'
      '        BARCODE = :OLD_BARCODE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO BAR('
      '    BARCODE,'
      '    CARDARTICUL,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB'
      ')'
      'VALUES('
      '    :BARCODE,'
      '    :CARDARTICUL,'
      '    :CARDSIZE,'
      '    :QUANTITY,'
      '    :PRICERUB'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    BARCODE,'
      '    CARDARTICUL,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB'
      'FROM'
      '    BAR '
      ''
      ' WHERE '
      '        BAR.BARCODE = :OLD_BARCODE'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    BARCODE,'
      '    CARDARTICUL,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB'
      'FROM'
      '    BAR ')
    Transaction = trSel2
    Database = Casher
    UpdateTransaction = trUpd2
    Left = 150
    Top = 195
    poAskRecordCount = True
    object taBarFBBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 14
      EmptyStrToNull = True
    end
    object taBarFBCARDARTICUL: TFIBStringField
      FieldName = 'CARDARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taBarFBCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object taBarFBQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taBarFBPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
  end
  object taDCSTOPFB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE FIXDISC'
      'SET '
      '    PROC = :PROC'
      'WHERE'
      '    SART = :OLD_SART'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    FIXDISC'
      'WHERE'
      '        SART = :OLD_SART'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO FIXDISC('
      '    SART,'
      '    PROC'
      ')'
      'VALUES('
      '    :SART,'
      '    :PROC'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SART,'
      '    PROC'
      'FROM'
      '    FIXDISC '
      ''
      ' WHERE '
      '        FIXDISC.SART = :OLD_SART'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    SART,'
      '    PROC'
      'FROM'
      '    FIXDISC ')
    Transaction = trSel3
    Database = Casher
    UpdateTransaction = trUpd3
    AutoCommit = True
    Left = 210
    Top = 195
    poAskRecordCount = True
    object taDCSTOPFBSART: TFIBStringField
      FieldName = 'SART'
      Size = 30
      EmptyStrToNull = True
    end
    object taDCSTOPFBPROC: TFIBFloatField
      FieldName = 'PROC'
    end
  end
  object quFisSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT CASHNUM,'
      '       ZNUM,'
      '       INOUT,'
      '       ITYPE,'
      '       RSUM,'
      '       IDATE,'
      '       DDATE'
      'FROM ZLISTDET'
      'where CASHNUM=:ICASHNUM'
      'and ZNUM=:IZNUM'
      'and IDATE>=:IDATE')
    Transaction = trSelFis
    Database = Cash
    Left = 298
    Top = 15
    object quFisSumCASHNUM: TFIBSmallIntField
      FieldName = 'CASHNUM'
    end
    object quFisSumZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object quFisSumINOUT: TFIBSmallIntField
      FieldName = 'INOUT'
    end
    object quFisSumITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quFisSumRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quFisSumIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quFisSumDDATE: TFIBDateTimeField
      FieldName = 'DDATE'
    end
  end
  object trSelFis: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 104
    Top = 76
  end
  object taClassFB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CLASSIF'
      'SET '
      '    IACTIVE = :IACTIVE,'
      '    ID_PARENT = :ID_PARENT,'
      '    NAME = :NAME,'
      '    IEDIT = :IEDIT,'
      '    DISCSTOP = :DISCSTOP'
      'WHERE'
      '    TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CLASSIF'
      'WHERE'
      '        TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CLASSIF('
      '    TYPE_CLASSIF,'
      '    ID,'
      '    IACTIVE,'
      '    ID_PARENT,'
      '    NAME,'
      '    IEDIT,'
      '    DISCSTOP'
      ')'
      'VALUES('
      '    :TYPE_CLASSIF,'
      '    :ID,'
      '    :IACTIVE,'
      '    :ID_PARENT,'
      '    :NAME,'
      '    :IEDIT,'
      '    :DISCSTOP'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    TYPE_CLASSIF,'
      '    ID,'
      '    IACTIVE,'
      '    ID_PARENT,'
      '    NAME,'
      '    IEDIT,'
      '    DISCSTOP'
      'FROM'
      '    CLASSIF '
      ''
      ' WHERE '
      '        CLASSIF.TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and CLASSIF.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    TYPE_CLASSIF,'
      '    ID,'
      '    IACTIVE,'
      '    ID_PARENT,'
      '    NAME,'
      '    IEDIT,'
      '    DISCSTOP'
      'FROM'
      '    CLASSIF ')
    Transaction = trSel1
    Database = Casher
    UpdateTransaction = trUpd1
    Left = 26
    Top = 251
    poAskRecordCount = True
    object taClassFBTYPE_CLASSIF: TFIBIntegerField
      FieldName = 'TYPE_CLASSIF'
    end
    object taClassFBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taClassFBIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object taClassFBID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object taClassFBNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taClassFBIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object taClassFBDISCSTOP: TFIBIntegerField
      FieldName = 'DISCSTOP'
    end
  end
  object taTestVes: TpFIBDataSet
    UpdateSQL.Strings = (
      ''
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TESTVES'
      'WHERE'
      '        ARTICUL = :OLD_ARTICUL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TESTVES('
      '    ARTICUL'
      ')'
      'VALUES('
      '    :ARTICUL'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ARTICUL'
      'FROM'
      '    TESTVES '
      ''
      ' WHERE '
      '        TESTVES.ARTICUL = :OLD_ARTICUL'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ARTICUL'
      'FROM'
      '    TESTVES ')
    Transaction = trSel1
    Database = Casher
    UpdateTransaction = trUpd1
    AutoCommit = True
    Left = 366
    Top = 203
    poAskRecordCount = True
    object taTestVesARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object taCardsFBRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CARDSCLA'
      'SET '
      '    CLASSIF = :CLASSIF,'
      '    DEPART = :DEPART,'
      '    NAME = :NAME,'
      '    CARD_TYPE = :CARD_TYPE,'
      '    MESURIMENT = :MESURIMENT,'
      '    PRICE_RUB = :PRICE_RUB,'
      '    DISCQUANT = :DISCQUANT,'
      '    DISCOUNT = :DISCOUNT,'
      '    SCALE = :SCALE'
      'WHERE'
      '    ARTICUL = :OLD_ARTICUL'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CARDSCLA'
      'WHERE'
      '        ARTICUL = :OLD_ARTICUL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CARDSCLA('
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE'
      ')'
      'VALUES('
      '    :ARTICUL,'
      '    :CLASSIF,'
      '    :DEPART,'
      '    :NAME,'
      '    :CARD_TYPE,'
      '    :MESURIMENT,'
      '    :PRICE_RUB,'
      '    :DISCQUANT,'
      '    :DISCOUNT,'
      '    :SCALE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE'
      'FROM'
      '    CARDSCLA '
      ''
      ' WHERE '
      '        CARDSCLA.ARTICUL = :OLD_ARTICUL'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCQUANT,'
      '    DISCOUNT,'
      '    SCALE'
      'FROM'
      '    CARDSCLA '
      'where ARTICUL=:CODE')
    Transaction = trSel1
    Database = Casher
    UpdateTransaction = trUpd1
    AutoCommit = True
    Left = 370
    Top = 267
    poAskRecordCount = True
    object taCardsFBRecARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taCardsFBRecCLASSIF: TFIBIntegerField
      FieldName = 'CLASSIF'
    end
    object taCardsFBRecDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object taCardsFBRecNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taCardsFBRecCARD_TYPE: TFIBIntegerField
      FieldName = 'CARD_TYPE'
    end
    object taCardsFBRecMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object taCardsFBRecPRICE_RUB: TFIBFloatField
      FieldName = 'PRICE_RUB'
    end
    object taCardsFBRecDISCQUANT: TFIBFloatField
      FieldName = 'DISCQUANT'
    end
    object taCardsFBRecDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
    end
    object taCardsFBRecSCALE: TFIBStringField
      FieldName = 'SCALE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object taFixAVid: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE FIXAVID'
      'SET '
      '    AVIDNAME = :AVIDNAME,'
      '    SID = :SID'
      'WHERE'
      '    IID = :OLD_IID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    FIXAVID'
      'WHERE'
      '        IID = :OLD_IID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO FIXAVID('
      '    IID,'
      '    AVIDNAME,'
      '    SID'
      ')'
      'VALUES('
      '    :IID,'
      '    :AVIDNAME,'
      '    :SID'
      ')')
    RefreshSQL.Strings = (
      'select iid, avidname, sid'
      'from fixavid'
      ''
      ' WHERE '
      '        FIXAVID.IID = :OLD_IID'
      '    ')
    SelectSQL.Strings = (
      'select iid, avidname, sid'
      'from fixavid')
    Transaction = trSel2
    Database = Casher
    UpdateTransaction = trUpd2
    AutoCommit = True
    Left = 390
    Top = 123
    poAskRecordCount = True
    object taFixAVidIID: TFIBIntegerField
      FieldName = 'IID'
    end
    object taFixAVidAVIDNAME: TFIBStringField
      FieldName = 'AVIDNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taFixAVidSID: TFIBStringField
      FieldName = 'SID'
      Size = 10
      EmptyStrToNull = True
    end
  end
end
