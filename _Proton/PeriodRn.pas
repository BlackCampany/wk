unit PeriodRn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfBackGround, cxControls, cxContainer, cxEdit, cxLabel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmPeriod = class(TForm)
    cxLabel1: TcxLabel;
    dxfBackGround1: TdxfBackGround;
    cxLabel2: TcxLabel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    DateEdit1: TcxDateEdit;
    DateEdit2: TcxDateEdit;
    procedure cxButton2Click(Sender: TObject);
    procedure DateEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriod: TfmPeriod;

implementation

{$R *.dfm}

procedure TfmPeriod.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPeriod.DateEdit1PropertiesChange(Sender: TObject);
begin
  fmPeriod.Caption:='  ������ � '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit1.Date)+'     �� '+FormatDateTime('dd mmmm yyyy',fmPeriod.DateEdit2.Date);
end;

end.
