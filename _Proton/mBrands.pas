unit mBrands;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmBrands = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewBrands: TcxGridDBTableView;
    LevelBrands: TcxGridLevel;
    GridBrands: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    amBrands: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    ViewBrandsID: TcxGridDBColumn;
    ViewBrandsNAMEBRAND: TcxGridDBColumn;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBrands: TfmBrands;
  bClearBrands:Boolean = False;

implementation

uses Un1, MDB, AddSingle;

{$R *.dfm}

procedure TfmBrands.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmBrands.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmBrands.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmBrands.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmBrands.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.FormCreate(Sender: TObject);
begin
  GridBrands.Align:=AlClient;
  ViewBrands.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmBrands.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewBrands.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmBrands.Timer1Timer(Sender: TObject);
begin
  if bClearBrands=True then begin StatusBar1.Panels[0].Text:=''; bClearBrands:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearBrands:=True;
end;

procedure TfmBrands.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmBrands.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelBrands') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quBrands.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quBrandsNameBrand.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewBrands.BeginUpdate;
        try
          try
            iR:=quBrandsID.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from A_BRANDS');
            quD.SQL.Add('where ID='+IntToStr(iR));
            quD.ExecSQL;
            
            quBrands.Close;
            quBrands.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewBrands.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmBrands.acEditExecute(Sender: TObject);
Var iR:Integer;
begin
//�������������
  if not CanDo('prEditBrands') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quBrands.RecordCount>0 then
    begin
      fmAddSingle.prSetParams('������: ��������������.',0);
      fmAddSingle.cxTextEdit1.Text:=quBrandsNameBrand.AsString;
      fmAddSingle.ShowModal;
      if fmAddSingle.ModalResult=mrOk then
      begin
        ViewBrands.BeginUpdate;
        try
          try
            iR:=quBrandsID.AsInteger;

            quA.SQL.Clear;
            quA.SQL.Add('Update A_BRANDS Set NameBrand='''+Copy(fmAddSingle.cxTextEdit1.Text,1,50)+'''');
            quA.SQL.Add('where ID='+IntToStr(iR));
            quA.ExecSQL;

            quBrands.Active:=False;
            quBrands.Active:=True;
            quBrands.Locate('ID',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewBrands.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmBrands.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prAddBrands') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  fmAddSingle.prSetParams('������: ����������.',0);
  fmAddSingle.cxTextEdit1.Text:='';
  fmAddSingle.ShowModal;
  if fmAddSingle.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewBrands.BeginUpdate;
      try
        if quBrands.Locate('NameBrand',fmAddSingle.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          iMax:=1;
          if quBrands.RecordCount>0 then begin quBrands.Last; iMax:=quBrandsID.AsInteger+1; end;

          quE.SQL.Clear;
          quE.SQL.Add('INSERT into A_BRANDS values ('+IntToStr(iMax)+','''+Copy(fmAddSingle.cxTextEdit1.Text,1,50)+''')');
          quE.ExecSQL;

          quBrands.Active:=False;
          quBrands.Active:=True;
          quBrands.Locate('ID',iMax,[]);
        end;
      finally
        ViewBrands.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmBrands.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmBrands.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmBrands.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmBrands.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmBrands.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmBrands.FormShow(Sender: TObject);
begin
  cxTextEdit1.Clear;
end;

procedure TfmBrands.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  with dmMC do
  begin
    ViewBrands.BeginUpdate;
    try
      quBrands.Locate('NameBrand',cxTextEdit1.Text,[loCaseInsensitive,loPartialKey]);
    finally
      ViewBrands.EndUpdate;
    end;
  end;
end;

end.
