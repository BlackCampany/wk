unit SInvVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxProgressBar, Menus;

type
  TfmSInvVoz = class(TForm)
    GrSInvVoz: TcxGrid;
    ViewSInvVoz: TcxGridDBTableView;
    LevSInvVoz: TcxGridLevel;
    ViewSInvVozNumber: TcxGridDBColumn;
    ViewSInvVozDate: TcxGridDBColumn;
    PBar1: TcxProgressBar;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure ViewSInvVozDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSInvVoz: TfmSInvVoz;

implementation

uses dmPS, SpecInvVoz,Un1, Period1;

{$R *.dfm}

procedure TfmSInvVoz.ViewSInvVozDblClick(Sender: TObject);
  var iStep,iC:integer;

begin
  with dmP do
  begin
    PBar1.Position:=0;
    PBar1.Visible:=True;

    fmSpecInvVoz.ViewSpecInvVoz.BeginUpdate;

    quSpInvV.Active:=false;
    quSpInvV.ParamByName('Num').AsInteger:=quDInvVNumber.AsInteger;
    quSpInvV.ParamByName('DDate').AsDate:=trunc(quDInvVDate.AsDateTime);
    quSpInvV.Active:=true;

    with fmSpecInvVoz do
    begin

      if quSpInvV.RecordCount>0 then
      begin
        PBar1.Position:=40; delay(10);

       iStep:=quSpInvV.RecordCount div 60;
       if iStep=0 then iStep:=quSpInvV.RecordCount;
       iC:=0;



        closete(teSp);
        quSpInvV.First;
        while not quSpInvV.Eof do
        begin
          teSp.Append;
            teSpCode.AsInteger:=quSpInvVCode.AsInteger;
            teSpNameC.AsString:=quSpInvVFullName.AsString;
            teSpDepart.AsInteger:=quSpInvVDepart.AsInteger;
            teSpDepartName.AsString:=quSpInvVDepartName.AsString;
            teSpQ.AsFloat:=quSpInvVQPREDZ.AsFloat;
            teSpQFact.AsFloat:=quSpInvVQFACT.AsFloat;
            teSpQRazn.AsFloat:=quSpInvVQRAZN.AsFloat;
            teSpQOTHERCLI.AsFloat:=quSpInvVQOTHERCLI.AsFloat;
            teSpNEWCLI.AsInteger:=quSpInvVNEWCLI.AsInteger;
            teSpNamecli.AsString:=quSpInvVNamecli.AsString;
          teSp.Post;

          delay(10);

          inc(iC);
          if (iC mod iStep = 0) then
          begin
            PBar1.Position:=40+(iC div iStep);
            delay(10);
          end;

          quSpInvV.Next;

        end;
        quSpInvV.Active:=false;
      end;
    end;

    fmSpecInvVoz.ViewSpecInvVoz.EndUpdate;

    PBar1.Position:=100; delay(30);
    PBar1.Visible:=False;

    fmSpecInvVoz.Show;
    fmSpecInvVoz.Caption:='�������������� ��������� � '+inttostr(quDInvVNumber.AsInteger)+' �� '+Datetostr(quDInvVDate.AsDateTime);
  end;
end;

procedure TfmSInvVoz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmP.quDInvV.Active:=false;
end;

procedure TfmSInvVoz.N1Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=Date;
  fmPeriod1.cxDateEdit2.Date:=Date;
  fmPeriod1.ShowModal;
  fmSInvVoz.ViewSInvVoz.BeginUpdate;
  dmP.quDInvV.Active:=false;
  dmP.quDInvV.ParamByName('DDateB').AsDate:=fmPeriod1.cxDateEdit1.Date;
  dmP.quDInvV.ParamByName('DDateE').AsDate:=fmPeriod1.cxDateEdit2.Date;
  dmP.quDInvV.Active:=true;
  dmP.quDInvV.First;
  fmSInvVoz.ViewSInvVoz.EndUpdate;
  fmSInvVoz.Show;
end;

end.
