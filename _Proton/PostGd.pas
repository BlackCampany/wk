unit PostGd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan, dxmdaset;

type
  TfmPD = class(TForm)
    FormPlacement1: TFormPlacement;
    GPD: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    amPD: TActionManager;
    acExit: TAction;
    LPOST: TcxGridLevel;
    VPOST: TcxGridDBTableView;
    VPOSTNameCli: TcxGridDBColumn;
    LDEP: TcxGridLevel;
    VPOSTIDCB: TcxGridDBColumn;
    VDEP: TcxGridDBTableView;
    VDEPID: TcxGridDBColumn;
    VDEPName: TcxGridDBColumn;
    techcli: TdxMemData;
    techcliidcli: TIntegerField;
    techcliNameCli: TStringField;
    dstechcli: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure VPOSTDblClick(Sender: TObject);
    procedure VDEPDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPD: TfmPD;

implementation

uses Un1,dmPS, AddDoc8, Period1, MDB, ObrPredz, InvVoz;

{$R *.dfm}

procedure TfmPD.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GPD.Align:=AlClient;
  if (fmPD.tag=0) or (fmPD.tag=3) then
    VPOST.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni)
  else
    VDEP.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmPD.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmPD.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPD.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateEnd-730;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=fmPeriod1.cxDateEdit1.Date;
    CommonSet.DateEnd:=fmPeriod1.cxDateEdit2.Date;
    with dmP do
    begin
      if Tag=0 then
      begin
        fmPD.VPOST.BeginUpdate;
        quObPost.Active:=False;
        quObPost.ParamByName('IDCARD').Value:=fmObrPredz.teobrPredzIdCode1.AsInteger;
        quObPost.ParamByName('DDATEB').Value:=CommonSet.DateBeg;
        quObPost.ParamByName('DDATEE').Value:=CommonSet.DateEnd;
        quObPost.Active:=True;
        quObPost.First;
        fmPD.VPOST.EndUpdate;
        fmPD.Caption:='���������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      end
      else
      begin
        fmPD.VDEP.BeginUpdate;
        quDepVoz.Active:=False;
        quDepVoz.Active:=True;
        fmPD.VDEP.EndUpdate;
        fmPD.Caption:='������';
      end;
    end;
  end;
end;

procedure TfmPD.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmPD.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if fmPD.Tag=0 then
    VPOST.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False)
  else
    VDEP.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmPD.VPOSTDblClick(Sender: TObject);
  var Q,Qobr:real;
begin
  with dmP do
  begin
    if (fmPD.tag=0) then
    begin
      if (fmPD.techcliidcli.AsInteger=fmObrPredz.teobrPredziCli.AsInteger) then
      begin
        showmessage('��������� ��� ��. ������ ������!');
        exit;
      end
      else
      begin
        Q:=fmObrPredz.teobrPredzQOut.AsFloat;      //���-��
        Qobr:=fmObrPredz.teobrPredzQObr.AsFloat;   //���-�� �� ���������

        if Qobr>(Q-1) then
        begin
          fmObrPredz.teobrPredz.Edit;
          Qobr:=Q;   //���� ������ ���������� �� ����� ���������� ��� ����� ���� ������
          fmObrPredz.teobrPredzQObr.AsFloat:=Qobr;
          fmObrPredz.teobrPredz.Post;
        end;

        prnewpost(fmPD.techcliidcli.AsInteger,Q,Qobr);    //���������� � ����� �����������
        fmObrPredz.Show;
        updallform;
      end;
    end;

 {   if (fmPD.tag=3) then
    begin
      prnewcli(fmPD.techcliidcli.AsInteger,fmInvVoz.);    //���������� � ����� �����������
      fmInvVoz.Show;
    end;  }
  end;
end;

procedure TfmPD.VDEPDblClick(Sender: TObject);
   var Q,Qobr:real;
begin
  with dmP do
  begin
    if quDepVozID.AsInteger=fmObrPredz.teobrPredzDepart.AsInteger then
    begin
      showmessage('����� ��� ��. ������ ������!');
      exit;
    end
    else
    begin
      Q:=fmObrPredz.teobrPredzQOut.AsFloat;      //���-��
      Qobr:=fmObrPredz.teobrPredzQObr.AsFloat;   //���-�� �� ���������

      if Qobr>(Q-1) then
      begin
        fmObrPredz.teobrPredz.Edit;
        Qobr:=Q;   //���� ������ ���������� �� ����� ���������� ��� ����� ���� ������
        fmObrPredz.teobrPredzQObr.AsFloat:=Qobr;
        fmObrPredz.teobrPredz.Post;
      end;

      prnewdep(quDepVozID.AsInteger,Q,Qobr);    //���������� � ����� �������      

      fmObrPredz.Show;
      updallform;
    end;
  end;
end;

end.
