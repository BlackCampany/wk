unit CliLicense;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, ActnList, XPStyleActnCtrls,
  ActnMan, dxmdaset;

type
  TfmCliLic = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GrCliLic: TcxGrid;
    ViCliLic: TcxGridDBTableView;
    leCliLic: TcxGridLevel;
    amCliLic: TActionManager;
    acAddCliLic: TAction;
    acEditCliLic: TAction;
    acDelCliLic: TAction;
    teLi: TdxMemData;
    teLiITYPE: TIntegerField;
    teLiICLI: TIntegerField;
    teLiIDATEB: TIntegerField;
    teLiDDATEB: TDateField;
    teLiIDATEE: TIntegerField;
    teLiDDATEE: TDateField;
    teLiSER: TStringField;
    teLiSNUM: TStringField;
    teLiORGAN: TStringField;
    teLiIDCB: TIntegerField;
    dsteLi: TDataSource;
    ViCliLicDDATEB: TcxGridDBColumn;
    ViCliLicDDATEE: TcxGridDBColumn;
    ViCliLicSER: TcxGridDBColumn;
    ViCliLicSNUM: TcxGridDBColumn;
    ViCliLicORGAN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acAddCliLicExecute(Sender: TObject);
    procedure acEditCliLicExecute(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acDelCliLicExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCliLic: TfmCliLic;

implementation

uses Un1, u2fdk, MDB, MT, AddCliLic;

{$R *.dfm}

procedure TfmCliLic.FormCreate(Sender: TObject);
begin
  GrCliLic.Align:=AlClient;
end;

procedure TfmCliLic.acAddCliLicExecute(Sender: TObject);
Var iDateB:INteger;
begin
  fmAddCliLic.cxTextEdit1.Text:='';
  fmAddCliLic.cxTextEdit2.Text:='';
  fmAddCliLic.cxTextEdit3.Text:='';
  fmAddCliLic.cxDateEdit1.Date:=Date;
  fmAddCliLic.cxDateEdit1.Properties.ReadOnly:=False;
  fmAddCliLic.cxDateEdit2.Date:=Date;
  fmAddCliLic.ShowModal;
  if fmAddCliLic.ModalResult=mrOk then
  begin
    iDateB:=Trunc(fmAddCliLic.cxDateEdit1.Date);
    if teLi.Locate('IDATEB',iDateB,[]) then
    begin
      Showmessage('����� ���� ������ ��� ����. ���������� ����������.');
    end else
    begin
      with dmMt do
      begin
        if ptCliLic.Active then
        begin
          try
            ptCliLic.Append;
            ptCliLicITYPE.AsInteger:=1;
            ptCliLicICLI.AsInteger:=fmCliLic.Tag;
            ptCliLicIDATEB.AsInteger:=Trunc(fmAddCliLic.cxDateEdit1.Date);
            ptCliLicDDATEB.AsDateTime:=fmAddCliLic.cxDateEdit1.Date;
            ptCliLicIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
            ptCliLicDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
            ptCliLicSER.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit1.Text);
            ptCliLicSNUM.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit2.Text);
            ptCliLicORGAN.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit3.Text);
            ptCliLicIDCLI.AsInteger:=0;
            ptCliLic.Post;

            teLi.Append;
            teLiITYPE.AsInteger:=1;
            teLiICLI.AsInteger:=fmCliLic.Tag;
            teLiIDATEB.AsInteger:=Trunc(fmAddCliLic.cxDateEdit1.Date);
            teLiDDATEB.AsDateTime:=fmAddCliLic.cxDateEdit1.Date;
            teLiIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
            teLiDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
            teLiSER.AsString:=fmAddCliLic.cxTextEdit1.Text;
            teLiSNUM.AsString:=fmAddCliLic.cxTextEdit2.Text;
            teLiORGAN.AsString:=fmAddCliLic.cxTextEdit3.Text;
            teLi.Post;
          except
            showmessage('������ ����������.');
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmCliLic.acEditCliLicExecute(Sender: TObject);
begin
  if teLi.RecordCount>0 then
  begin
    fmAddCliLic.cxTextEdit1.Text:=teLiSER.AsString;
    fmAddCliLic.cxTextEdit2.Text:=teLiSNUM.AsString;
    fmAddCliLic.cxTextEdit3.Text:=teLiORGAN.AsString;
    fmAddCliLic.cxDateEdit1.Date:=teLiIDATEB.AsInteger;
    fmAddCliLic.cxDateEdit1.Properties.ReadOnly:=True;
    fmAddCliLic.cxDateEdit2.Date:=teLiIDATEE.AsInteger;

    fmAddCliLic.ShowModal;
    if fmAddCliLic.ModalResult=mrOk then
    begin
      with dmMt do
      begin
        if ptCliLic.Active then
        begin
          try
            if ptCliLic.FindKey([1,fmCliLic.tag,teLiIDATEB.AsInteger]) then
            begin
              ptCliLic.Edit;
              ptCliLicIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
              ptCliLicDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
              ptCliLicSER.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit1.Text);
              ptCliLicSNUM.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit2.Text);
              ptCliLicORGAN.AsString:=AnsiToOemConvert(fmAddCliLic.cxTextEdit3.Text);
              ptCliLicIDCLI.AsInteger:=0;
              ptCliLic.Post;

              teLi.Edit;
              teLiIDATEE.AsInteger:=Trunc(fmAddCliLic.cxDateEdit2.Date);
              teLiDDATEE.AsDateTime:=fmAddCliLic.cxDateEdit2.Date;
              teLiSER.AsString:=fmAddCliLic.cxTextEdit1.Text;
              teLiSNUM.AsString:=fmAddCliLic.cxTextEdit2.Text;
              teLiORGAN.AsString:=fmAddCliLic.cxTextEdit3.Text;
              teLi.Post;
            end;
          except
            showmessage('������ ����������.');
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmCliLic.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCliLic.acDelCliLicExecute(Sender: TObject);
begin
  if teLi.RecordCount>0 then
  begin
    if MessageDlg('������� ������ �� �������� � '+teLiSNUM.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmMt do
      begin
        if ptCliLic.Active then
        begin
          try
            if ptCliLic.FindKey([1,fmCliLic.tag,teLiIDATEB.AsInteger]) then
            begin
              ptCliLic.Delete;
              teLi.Delete;
            end;
          except
            showmessage('������ ��������.');
          end;
        end;
      end;
    end;
  end;
end;

end.
