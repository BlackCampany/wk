object fmRaschotWarn: TfmRaschotWarn
  Left = 443
  Top = 157
  BorderStyle = bsDialog
  Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1077' '#1086#1087#1083#1072#1090#1091
  ClientHeight = 246
  ClientWidth = 341
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 20
    Width = 106
    Height = 16
    Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1072#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 169
    Height = 16
    Caption = #1054#1087#1083#1072#1090#1072' '#1041#1040#1053#1050'. '#1050#1040#1056#1058#1054#1049
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 116
    Width = 158
    Height = 16
    Caption = #1054#1087#1083#1072#1090#1072' '#1053#1040#1051#1048#1063#1053#1067#1052#1048
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 156
    Width = 51
    Height = 16
    Caption = #1057#1044#1040#1063#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 84
    Width = 161
    Height = 16
    Caption = #1054#1087#1083#1072#1090#1072' '#1044#1045#1041'. '#1050#1040#1056#1058#1054#1049
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 184
    Width = 341
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 16
      Top = 8
      Width = 145
      Height = 49
      Caption = #1042#1089#1077' '#1087#1088#1072#1074#1080#1083#1100#1085#1086
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 216
      Top = 8
      Width = 105
      Height = 49
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ModalResult = 4
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 192
    Top = 16
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 1
    Width = 121
  end
  object cxCurrencyEdit2: TcxCurrencyEdit
    Left = 192
    Top = 48
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 2
    Width = 121
  end
  object cxCurrencyEdit3: TcxCurrencyEdit
    Left = 192
    Top = 112
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 3
    Width = 121
  end
  object cxCurrencyEdit4: TcxCurrencyEdit
    Left = 192
    Top = 148
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clRed
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 4
    Width = 121
  end
  object cxCurrencyEdit5: TcxCurrencyEdit
    Left = 192
    Top = 80
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 5
    Width = 121
  end
end
