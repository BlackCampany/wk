unit NoNeed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ComCtrls, cxMemo, ActnList,
  XPStyleActnCtrls, ActnMan, ExtCtrls, cxCheckBox, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar;

type
  TfmMainTexProc = class(TForm)
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Timer1: TTimer;
    amCalcSS: TActionManager;
    acStartSS: TAction;
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainTexProc: TfmMainTexProc;

implementation

{$R *.dfm}

procedure TfmMainTexProc.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date-1;
  Timer1.Enabled:=True;
  ReadIni;
end;

end.
