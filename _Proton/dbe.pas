unit dbe;

interface

uses
  SysUtils, Classes, DB, pvtables, sqldataset;

type
  TdmE = class(TDataModule)
    dsquUPL: TDataSource;
    quUPL: TPvQuery;
    quUPLID: TIntegerField;
    quUPLIMH: TIntegerField;
    quUPLNAME: TStringField;
    quUPLIACTIVE: TSmallintField;
    quUPLNAMEMH: TStringField;
    dsHistOp: TDataSource;
    quDocHist: TPvQuery;
    quDocHistName: TStringField;
    quDocHistTYPED: TSmallintField;
    quDocHistIDPERS: TIntegerField;
    quDocHistDATEEDIT: TDateTimeField;
    quDocHistDOCDATE: TDateField;
    quDocHistDOCNUM: TStringField;
    quDocHistDOCID: TIntegerField;
    quDocHistIOP: TStringField;
    quDocHistNAMEPERS: TStringField;
    quDocHistIDOP: TSmallintField;
    dsquUPLMh: TDataSource;
    quUPLMh: TPvQuery;
    quUPLMhID: TIntegerField;
    quUPLMhIMH: TIntegerField;
    quUPLMhNAME: TStringField;
    quUPLMhIACTIVE: TSmallintField;
    quCashList: TPvQuery;
    quCashListID: TIntegerField;
    quCashListPATH: TStringField;
    quCashListName: TStringField;
    quLastFullInv: TPvQuery;
    quLastFullInvInventry: TIntegerField;
    quLastFullInvDepart: TSmallintField;
    quLastFullInvDateInventry: TDateField;
    quSumCorr: TPvQuery;
    quSumCorrITEM: TIntegerField;
    quSumCorrRQUANTC: TFloatField;
    quSumCorrRSUMC: TFloatField;
    quSumCorrRSUMRSC: TFloatField;
    quDocHistNAMECASSIR: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmE: TdmE;

implementation

{$R *.dfm}

end.
