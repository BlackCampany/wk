object fmRealH: TfmRealH
  Left = 37
  Top = 283
  Width = 977
  Height = 500
  Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1095#1072#1089#1072#1084
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 969
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C29252925292529252925292529252925292529252925
        1F7C1F7C1F7C1F7C29257A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B29257A6B
        29251F7C1F7C2925292529252925292529252925292529252925292529252925
        7A6B29251F7C29257A6B7A6B7A6B7A6B7A6B7A6BE97FE97FE97F7A6B7A6B2925
        292529251F7C29257A6B7A6B7A6B7A6B7A6B7A6BB556B556B5567A6B7A6B2925
        7A6B29251F7C2925292529252925292529252925292529252925292529252925
        7A6B7A6B292529257A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B29257A6B
        29257A6B29251F7C29252925292529252925292529252925292529257A6B2925
        7A6B292529251F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F29252925
        292529251F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7F29251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925292529252925292529252925292529251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7F29251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925292529252925292529252925292529252925
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        29251F7C1F7C1F7C1F7C1F7C1F7C1F7C29252925292529252925292529252925
        29251F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1077#1095#1072#1090#1100
      Spacing = 1
      Left = 14
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C000200021F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        000200021F7C1F7C000200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        000000021F7C1F7C00021F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00021F7C1F7C00021F7C1F7C1F7C00001F7C1F7C1F7C1F7C00001F7C1F7C
        1F7C00021F7C1F7C00021F7C1F7C000000021F7C000000001F7C000200001F7C
        1F7C00021F7C1F7C00021F7C0000000280030000000000000000800300020000
        1F7C00021F7C1F7C0002000000028003E003E003E003E003E003E00380030002
        000000021F7C1F7C0002000000028003E003E003E003E003E003E00380030002
        000000021F7C1F7C00021F7C0000000280030000000000000000800300020000
        1F7C00021F7C1F7C00021F7C1F7C000000021F7C000000001F7C000200001F7C
        1F7C00021F7C1F7C00021F7C1F7C1F7C00001F7C1F7C1F7C1F7C00001F7C1F7C
        1F7C00021F7C1F7C00021F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00021F7C1F7C000200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        000000021F7C1F7C000200021F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        000200021F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1077#1088#1080#1086#1076
      Spacing = 1
      Left = 84
      Top = 4
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1055#1077#1095'.'#1082#1088#1072#1090#1082#1086
      Caption = #1055#1077#1095'.'#1082#1088#1072#1090#1082#1086
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C29252925292529252925292529252925292529252925
        1F7C1F7C1F7C1F7C29257A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B29257A6B
        29251F7C1F7C2925292529252925292529252925292529252925292529252925
        7A6B29251F7C29257A6B7A6B7A6B7A6B7A6B7A6BE97FE97FE97F7A6B7A6B2925
        292529251F7C29257A6B7A6B7A6B7A6B7A6B7A6BB556B556B5567A6B7A6B2925
        7A6B29251F7C2925292529252925292529252925292529252925292529252925
        7A6B7A6B292529257A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B7A6B29257A6B
        29257A6B29251F7C29252925292529252925292529252925292529257A6B2925
        7A6B292529251F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F29252925
        292529251F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7F29251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925292529252925292529252925292529251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7F29251F7C
        1F7C1F7C1F7C1F7C1F7C1F7C2925292529252925292529252925292529252925
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C2925FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        29251F7C1F7C1F7C1F7C1F7C1F7C1F7C29252925292529252925292529252925
        29251F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1077#1095'.'#1082#1088#1072#1090#1082#1086
      Spacing = 1
      Left = 74
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042104210421042104210421042104210421F7C18631863
        1F7C18631F7C1F7CD4016F016F0129252925104210421042FF7FFF7FFF7F6F01
        6F01D4011F7C1F7C1F7C1F7C1F7CD401D401292529251042FF7FFF7FFF7F1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD401D401D4012925FF7FFF7FFF7FFF7F1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD401D401D4012925FF7FFF7FFF7FFF7F1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD401D401D4012925FF6FFF43BE3BFF031F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD401D401D4012925FF43FF43FF43FF431F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD401D401D4012925FF03FF43FF43FF6F1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C6F016F016F016F016F016F016F016F011F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CAD352925292529252925AD351F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CAD352925292529252925AD351F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 344
      Top = 4
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      BtnCaption = #1055#1086' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103#1084
      Caption = #1055#1086' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103#1084
      Glyph.Data = {
        86050000424D8605000000000000360000002800000016000000140000000100
        18000000000050050000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF808080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFF80
        8080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000000000FFFF
        FFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
        FFFFFF0000FFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF0000FFFFFFFFFFFF
        000000FF0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF
        FFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF0000FFFF
        FF808080000000FFFFFFFF00000000FFFFFFFF0000FFFFFFFFFFFFFFFFFFFF00
        00FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF0000FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF000000FFFFFF0000FFFF0000FFFFFFFFFFFF0000FFFFFFFF
        FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFF808080000000FFFFFF0000FFFFFFFFFF0000FFFFFFFFFF
        FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFF0000000000FFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFF0000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFF0000FFFFFF808080000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FF808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000}
      Hint = #1055#1086' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103#1084'|'
      Spacing = 1
      Left = 184
      Top = 4
      Visible = True
      OnClick = SpeedItem5Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      BtnCaption = #1055#1086' '#1089#1091#1084#1084#1077
      Caption = #1055#1086' '#1089#1091#1084#1084#1077
      Glyph.Data = {
        86050000424D8605000000000000360000002800000016000000140000000100
        18000000000050050000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF808080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFF80
        8080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFF808080FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000000000FFFF
        FFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
        FFFFFF0000FFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF0000FFFFFFFFFFFF
        000000FF0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF
        FFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF0000FFFF
        FF808080000000FFFFFFFF00000000FFFFFFFF0000FFFFFFFFFFFFFFFFFFFF00
        00FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF0000FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF000000FFFFFF0000FFFF0000FFFFFFFFFFFF0000FFFFFFFF
        FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFF808080000000FFFFFF0000FFFFFFFFFF0000FFFFFFFFFF
        FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFF0000000000FFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFF0000FFFFFF808080000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFF0000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFF0000FFFFFF808080000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FF808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000}
      Hint = #1055#1086' '#1089#1091#1084#1084#1077'|'
      Spacing = 1
      Left = 244
      Top = 4
      Visible = True
      OnClick = SpeedItem6Click
      SectionName = 'Untitled (0)'
    end
  end
  object GridRealH: TcxGrid
    Left = 0
    Top = 48
    Width = 969
    Height = 399
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewRealH: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquRepHour
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0'
          Kind = skSum
          FieldName = 'QC'
          Column = ViewRealHQC
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'QS'
          Column = ViewRealHQS
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SS'
          Column = ViewRealHSS
        end
        item
          Format = '0.00'
          Kind = skMax
          FieldName = 'SM'
          Column = ViewRealHSM
        end
        item
          Format = '0.0'
          Column = ViewRealHAITOG
        end>
      DataController.Summary.SummaryGroups = <>
      DataController.Summary.OnAfterSummary = ViewRealHDataControllerSummaryAfterSummary
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Bands = <
        item
          Caption = #1089' 00:00 '#1076#1086' 01:00'
          Width = 100
        end
        item
          Caption = #1089' 01:00 '#1076#1086' 02:00'
          Width = 100
        end
        item
          Caption = #1089' 02:00 '#1076#1086' 03:00'
          Width = 100
        end
        item
          Caption = #1089' 03:00 '#1076#1086' 04:00'
          Width = 100
        end
        item
          Caption = #1089' 04:00 '#1076#1086' 05:00'
          Width = 100
        end
        item
          Caption = #1089' 05:00 '#1076#1086' 06:00'
          Width = 100
        end
        item
          Caption = #1089' 06:00 '#1076#1086' 07:00'
          Width = 100
        end
        item
          Caption = #1089' 07:00 '#1076#1086' 08:00'
          Width = 100
        end
        item
          Caption = #1089' 08:00 '#1076#1086' 09:00'
          Width = 100
        end
        item
          Caption = #1089' 09:00 '#1076#1086' 10:00'
          Width = 100
        end
        item
          Caption = #1089' 10:00 '#1076#1086' 11:00'
          Width = 100
        end
        item
          Caption = #1089' 11:00 '#1076#1086' 12:00'
          Width = 100
        end
        item
          Caption = #1089' 12:00 '#1076#1086' 13:00'
          Width = 100
        end
        item
          Caption = #1089' 13:00 '#1076#1086' 14:00'
          Width = 100
        end
        item
          Caption = #1089' 14:00 '#1076#1086' 15:00'
          Width = 100
        end
        item
          Caption = #1089' 15:00 '#1076#1086' 16:00'
          Width = 100
        end
        item
          Caption = #1089' 16:00 '#1076#1086' 17:00'
          Width = 100
        end
        item
          Caption = #1089' 17:00 '#1076#1086' 18:00'
          Width = 100
        end
        item
          Caption = #1089' 18:00 '#1076#1086' 19:00'
          Width = 100
        end
        item
          Caption = #1089' 19:00 '#1076#1086' 20:00'
          Width = 100
        end
        item
          Caption = #1089' 20:00 '#1076#1086' 21:00'
          Width = 100
        end
        item
          Caption = #1089' 21:00 '#1076#1086' 22:00'
          Width = 100
        end
        item
          Caption = #1089' 22:00 '#1076#1086' 23:00'
          Width = 100
        end
        item
          Caption = #1089' 23:00 '#1076#1086' 24:00'
          Width = 132
        end
        item
          Caption = #1048#1090#1086#1075#1086
          FixedKind = fkRight
          Width = 113
        end>
      object ViewRealHCl00: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl00'
        Styles.Content = dmMC.cxStyle13
        Width = 39
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS00: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S00'
        Styles.Content = dmMC.cxStyle2
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM00: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M00'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl01: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl01'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS01: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S01'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM01: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M01'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl02: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl02'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS02: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S02'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM02: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M02'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl03: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl03'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS03: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S03'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM03: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M03'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl04: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl04'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS04: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S04'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM04: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M04'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl05: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl05'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS05: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S05'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM05: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M05'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl06: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl06'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS06: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S06'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM06: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M06'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl07: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl07'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS07: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S07'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM07: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M07'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl08: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl08'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS08: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S08'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM08: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M08'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl09: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl09'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 9
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS09: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S09'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 9
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM09: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M09'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 9
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl10: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl10'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 10
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS10: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S10'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 10
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM10: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M10'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 10
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl11: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl11'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 11
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS11: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S11'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 11
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM11: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M11'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 11
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl12: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl12'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 12
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS12: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S12'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 12
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM12: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M12'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 12
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl13: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl13'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 13
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS13: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S13'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 13
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM13: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M13'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 13
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl14: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl14'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 14
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS14: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S14'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 14
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM14: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M14'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 14
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl15: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl15'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 15
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS15: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S15'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 15
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM15: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M15'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 15
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl16: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl16'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 16
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS16: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S16'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 16
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM16: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M16'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 16
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl17: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl17'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 17
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS17: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S17'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 17
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM17: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M17'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 17
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl18: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl18'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 18
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS18: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S18'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 18
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM18: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M18'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 18
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl19: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl19'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 19
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS19: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S19'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 19
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM19: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M19'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 19
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl20: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl20'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 20
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS20: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S20'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 20
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM20: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M20'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 20
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl21: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl21'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 21
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS21: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S21'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 21
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM21: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M21'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 21
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl22: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl22'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 22
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS22: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S22'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 22
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM22: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M22'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 22
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHCl23: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'Cl23'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 23
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHS23: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'S23'
        Styles.Content = dmMC.cxStyle24
        Position.BandIndex = 23
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHM23: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'M23'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 23
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHQS: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1086#1082
        DataBinding.FieldName = 'QS'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 24
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewRealHQC: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'QC'
        Styles.Content = dmMC.cxStyle13
        Position.BandIndex = 24
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewRealHSS: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'SS'
        Styles.Content = dmMC.cxStyle2
        Position.BandIndex = 24
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewRealHSM: TcxGridDBBandedColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1095#1077#1082
        DataBinding.FieldName = 'SM'
        Styles.Content = dmMC.cxStyle12
        Position.BandIndex = 24
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object ViewRealHA00: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A00'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA01: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A01'
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA02: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A02'
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA03: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A03'
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA04: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A04'
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA05: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A05'
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA06: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A06'
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA07: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A07'
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA08: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A08'
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA09: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A09'
        Position.BandIndex = 9
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA10: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A10'
        Position.BandIndex = 10
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA11: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A11'
        Position.BandIndex = 11
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA12: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A12'
        Position.BandIndex = 12
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA13: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A13'
        Position.BandIndex = 13
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA14: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A14'
        Position.BandIndex = 14
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA15: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A15'
        Position.BandIndex = 15
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA16: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A16'
        Position.BandIndex = 16
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA17: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A17'
        Position.BandIndex = 17
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA18: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A18'
        Position.BandIndex = 18
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA19: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A19'
        Position.BandIndex = 19
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA20: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A20'
        Position.BandIndex = 20
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA21: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A21'
        Position.BandIndex = 21
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA22: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A22'
        Position.BandIndex = 22
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHA23: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'A23'
        Position.BandIndex = 23
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
      object ViewRealHAITOG: TcxGridDBBandedColumn
        Caption = #1057#1088#1077#1076#1085#1080#1081' '#1095#1077#1082
        DataBinding.FieldName = 'AITOG'
        Position.BandIndex = 24
        Position.ColIndex = 0
        Position.RowIndex = 3
      end
    end
    object levelRealH: TcxGridLevel
      GridView = ViewRealH
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 447
    Width = 969
    Height = 19
    Panels = <
      item
        Width = 400
      end
      item
        Width = 50
      end>
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 560
    Top = 200
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 96
    Top = 168
    ReportForm = {19000000}
  end
  object frRealH: TfrDBDataSet
    DataSet = dmMC.quRepHour
    Left = 176
    Top = 168
  end
end
