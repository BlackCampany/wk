object fmPartSel: TfmPartSel
  Left = 377
  Top = 375
  Width = 551
  Height = 440
  Caption = #1055#1072#1088#1090#1080#1086#1085#1085#1099#1081' '#1091#1095#1077#1090
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 543
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Caption = #1058#1086#1074#1072#1088
    Color = clWhite
    TabOrder = 0
  end
  object GridPart: TcxGrid
    Left = 32
    Top = 48
    Width = 353
    Height = 329
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewPartIn: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object ViewPartOut: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object LePartIn: TcxGridLevel
      Caption = #1055#1088#1080#1093#1086#1076
      GridView = ViewPartIn
    end
    object LePartOut: TcxGridLevel
      Caption = #1056#1072#1089#1093#1086#1076
      GridView = ViewPartOut
    end
  end
  object Panel2: TPanel
    Left = 414
    Top = 41
    Width = 129
    Height = 365
    Align = alRight
    BevelInner = bvLowered
    Color = 16771797
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 12
      Top = 20
      Width = 101
      Height = 33
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
end
