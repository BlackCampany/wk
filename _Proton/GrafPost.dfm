object fmGrafPost: TfmGrafPost
  Left = 454
  Top = 400
  BorderStyle = bsDialog
  Caption = #1043#1088#1072#1092#1080#1082' '#1087#1086#1089#1090#1072#1074#1086#1082
  ClientHeight = 500
  ClientWidth = 529
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Transparent = True
  end
  object Label2: TLabel
    Left = 100
    Top = 16
    Width = 325
    Height = 13
    AutoSize = False
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' ......'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object cxDateNavigator1: TcxDateNavigator
    Left = 12
    Top = 64
    Width = 505
    Height = 425
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    LookAndFeel.Kind = lfOffice11
    Storage = Storage1
    Styles.Selection = dmMC.cxStyle3
    TabOrder = 0
  end
  object cxButton1: TcxButton
    Left = 452
    Top = 8
    Width = 65
    Height = 45
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton1: TcxRadioButton
    Left = 24
    Top = 40
    Width = 81
    Height = 17
    Caption = #1055#1086#1089#1090#1072#1074#1082#1080
    Checked = True
    TabOrder = 2
    TabStop = True
    OnClick = cxRadioButton1Click
    LookAndFeel.Kind = lfOffice11
    Transparent = True
  end
  object cxRadioButton2: TcxRadioButton
    Left = 116
    Top = 40
    Width = 105
    Height = 17
    Caption = #1047#1072#1082#1072#1079
    TabOrder = 3
    OnClick = cxRadioButton1Click
    LookAndFeel.Kind = lfOffice11
    Transparent = True
  end
  object Storage1: TcxSchedulerStorage
    CustomFields = <>
    Resources.Images = dmMC.ImageList1
    Resources.Items = <
      item
      end>
    Left = 24
    Top = 120
  end
  object amGrafZ: TActionManager
    Left = 36
    Top = 176
    StyleName = 'XP Style'
    object acGrafSet: TAction
      Caption = 'acGrafSet'
      OnExecute = acGrafSetExecute
    end
  end
end
