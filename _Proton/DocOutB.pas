unit DocOutB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCurrencyEdit, cxContainer, cxTextEdit, cxMemo, FR_Class, FR_DSet,
  FR_DBSet, Menus;

type
  TfmDocsOutB = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOutB: TcxGrid;
    ViewDocsOutB: TcxGridDBTableView;
    LevelDocsOutB: TcxGridLevel;
    ViewDocsOutBID: TcxGridDBColumn;
    ViewDocsOutBDATEDOC: TcxGridDBColumn;
    ViewDocsOutBNUMDOC: TcxGridDBColumn;
    ViewDocsOutBIDSKL: TcxGridDBColumn;
    ViewDocsOutBSUMIN: TcxGridDBColumn;
    ViewDocsOutBSUMUCH: TcxGridDBColumn;
    ViewDocsOutBNAMEMH: TcxGridDBColumn;
    ViewDocsOutBIACTIVE: TcxGridDBColumn;
    amDocsOutB: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    ViewDocsOutBOPER: TcxGridDBColumn;
    acAddDocManual: TAction;
    acSebBl: TAction;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    Memo1: TcxMemo;
    acPrintMin: TAction;
    RepDocOutB: TfrReport;
    frquDocOutBPrint: TfrDBDataSet;
    SpeedItem11: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acCopy: TAction;
    acPast: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewDocsOutBCOMMENT: TcxGridDBColumn;
    N3: TMenuItem;
    Excel1: TMenuItem;
    ViewDocsOutBIDCLI: TcxGridDBColumn;
    acPrintActSp: TAction;
    N4: TMenuItem;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    acExit: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutBDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddDocManualExecute(Sender: TObject);
    procedure acSebBlExecute(Sender: TObject);
    procedure acAddDocBExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acPrintMinExecute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acPastExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure ViewDocsOutBCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acPrintActSpExecute(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prOpen(IdH,iNew:Integer;Memo1:TcxMemo);
    procedure prOn(IdH,IdSkl,iDate:Integer;var rSum1,rSum2:Real);
  end;

procedure prSetDOBSpecPar(sOper:String;iType:Integer);

var
  fmDocsOutB: TfmDocsOutB;
  bClearDocOutB:Boolean = false;
  bStartOpen:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, Period3, DOBSpec, SelPartIn1,
  DMOReps, TBuff, MainRnOffice, sumprops;

{$R *.dfm}

procedure TfmDocsOutB.prOn(IdH,IdSkl,iDate:Integer;var rSum1,rSum2:Real);
Var rQP,rQs,PriceSp,rQ,rSum,rSumP,rMessure,PriceSp0,rSumP0,rSum0:Real;
    IdCli:Integer;
    iSS:INteger;


//    rSum - ����� ������ � ��� �� ���������
//    rSum0  - ����� ������ ��� ��� �� ���������
//    rSumP - ����� ������ � ��� �� �������
//    rSumP0  - ����� ������ ��� ��� �� �������

begin
  with dmO do
  with dmORep do
  begin
    iSS:=prISS(IdSkl);

    taDobSpec1.Active:=False;
    taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
    taDobSpec1.Active:=True;

    taDobSpec2.Active:=False;
    taDobSpec2.ParamByName('IDH').AsInteger:=IDH;
    taDobSpec2.Active:=True;

    CloseTa(taPartTest);
    rSum:=0;
    rSum0:=0;

    taDobSpec1.First;
    while not taDobSpec1.Eof do
    begin
      PriceSp:=0;
      PriceSp0:=0;
      IdCli:=0;
      rSumP:=0;
      rSumP0:=0;


//              rQs:=taDobSpec1QUANT.AsFloat; //taDobSpec1 - � �������� �������� ���������
// ������ �� � �������� � � ������� - ���� ���������� � ��������
// ���������
      rQs:=taDobSpec1QUANT.AsFloat*taDobSpec1KM.AsFloat;

      prSelPartIn(taDobSpec1ARTICUL.AsInteger,IdSkl,0,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
         //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;

                //��������� ��������� ������
                //����������� ��������� ������
//?ARTICUL, ?IDDATE, ?IDSTORE, ?IDPARTIN, ?IDDOC, ?IDCLI, ?DTYPE, ?QUANT, ?PRICEIN, ?SUMOUT)
          prAddPartOut.ParamByName('ARTICUL').AsInteger:=taDobSpec1ARTICUL.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=taDobSpec1IDHEAD.AsInteger;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=2;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceSp0:=quSelPartInPRICEIN0.AsFloat;

          if iSS<>2 then  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp
          else  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;

          prAddPartOut.ParamByName('SUMOUT').AsFloat:=quSelPartInPRICEOUT.AsFloat*rQ;
          prAddPartout.ExecProc;


          IdCli:=quSelPartInIDCLI.AsInteger;
          rSum:=rSum+RoundVal(PriceSp*rQ);
          rSum0:=rSum0+RoundVal(PriceSp0*rQ);
          rSumP:=rSumP+RoundVal(PriceSp*rQ);
          rSumP0:=rSumP0+RoundVal(PriceSp0*rQ);
//                WriteTestLog('N;'+taDobSpec1ARTICUL.AsString+';'+taDobSpec1NAME.AsString+';'+FloatToStr(PriceSp)+';'+FloatToStr(rQ)+';'+FloatToStr(PriceSp*rQ));

          quSelPartIn.Next;
        end;

        quSelPartIn.Active:=False;

        //��������� �������� � ������������� ������
        if rQs>0 then //�������� ������������� ������, �������� � ������
        begin
          if (PriceSp=0) or (PriceSp0=0) then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
            prCalcLastPrice1.ParamByName('ISKL').AsInteger:=IdSkl;
            prCalcLastPrice1.ExecProc;

            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then
            begin
              PriceSp:=PriceSp/rMessure;
              PriceSp0:=PriceSp0/rMessure;
            end;
            IdCli:=prCalcLastPrice1.ParamByName('IDCLI').AsInteger;
          end;

         //��������� ��� ����� ���������
          prAddPartOut.ParamByName('ARTICUL').AsInteger:=taDobSpec1ARTICUL.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=taDobSpec1IDHEAD.AsInteger;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=IdCli;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=2;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;

          if iSS<>2 then  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp
          else  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;

          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSp*rQs;
          prAddPartout.ExecProc;

          rSum:=rSum+RoundVal(PriceSp*rQs);
          rSum0:=rSum0+RoundVal(PriceSp0*rQs);
          rSumP:=rSumP+RoundVal(PriceSp*rQs);
          rSumP0:=rSumP0+RoundVal(PriceSp0*rQs);
//                WriteTestLog('B;'+taDobSpec1ARTICUL.AsString+';'+taDobSpec1NAME.AsString+';'+FloatToStr(PriceSp)+';'+FloatToStr(rQs)+';'+FloatToStr(PriceSp*rQs));

          taPartTest.Append;
          taPartTestNum.AsInteger:=0;
          taPartTestIdGoods.AsInteger:=taDobSpec1ARTICUL.AsInteger;
          taPartTestNameG.AsString:=taDobSpec1NAME.AsString;
          taPartTestIM.AsInteger:=taDobSpec1IDM.AsInteger;
          taPartTestSM.AsString:=taDobSpec1SM.AsString;
          taPartTestQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
          taPartTestPrice1.AsFloat:=0;
          taPartTestiRes.AsInteger:=0;
          taPartTest.Post;
        end else
        begin
          taPartTest.Append;
          taPartTestNum.AsInteger:=0;
          taPartTestIdGoods.AsInteger:=taDobSpec1ARTICUL.AsInteger;
          taPartTestNameG.AsString:=taDobSpec1NAME.AsString;
          taPartTestIM.AsInteger:=taDobSpec1IDM.AsInteger;
          taPartTestSM.AsString:=taDobSpec1SM.AsString;
          taPartTestQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
          taPartTestPrice1.AsFloat:=0;
          taPartTestiRes.AsInteger:=1;
          taPartTest.Post;
        end;
      end;

      if rQs<0 then //�������� ��� ������
      begin  //���� �� ����������� ��� ���� ��� ���

//      IDSTORE=:IDSKL and ARTICUL=:IDCARD and IDATE>=:IDATE

        quSelPartIn1.Active:=False;
        quSelPartIn1.ParamByName('IDATE').AsInteger:=iDate;
        quSelPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl;
        quSelPartIn1.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
        quSelPartIn1.Active:=True;

        quSelPartIn1.First;
        if quSelPartIn1.RecordCount>0 then
        begin
          if iSS<>2 then PriceSp:=quSelPartIn1PRICEIN.AsFloat
          else PriceSp:=quSelPartIn1PRICEIN0.AsFloat;

          rSumP:=RoundVal(PriceSp*rQs);
          rSum:=rSum+RoundVal(PriceSp*rQs);
          //����������� ��������� ������
          rQ:=(-1)*rQs+quSelPartIn1QREMN.AsFloat;
          quSelPartIn1.Edit;
          quSelPartIn1QREMN.AsFloat:=rQ;
          quSelPartIn1.Post;

        //�������� ��������������
//EXECUTE PROCEDURE PR_GDSMOVE (?IART, ?IDATE, ?IDSTORE, ?RPOSTIN, ?RPOSTOUT, ?RVNIN, ?RVNOUT, ?RINV, ?RQREAL)
          prGdsMove.ParamByName('IART').AsInteger:=taDobSpec1ARTICUL.AsInteger;
          prGdsMove.ParamByName('IDATE').AsInteger:=iDate;
          prGdsMove.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prGdsMove.ParamByName('RPOSTIN').AsFloat:=(-1)*rQs;
          prGdsMove.ParamByName('RPOSTOUT').AsFloat:=0;
          prGdsMove.ParamByName('RVNIN').AsFloat:=0;
          prGdsMove.ParamByName('RVNOUT').AsFloat:=0;
          prGdsMove.ParamByName('RINV').AsFloat:=0;
          prGdsMove.ParamByName('RQREAL').AsFloat:=0;
          prGdsMove.ExecProc;

        end;
        quSelPartIn1.Active:=False;
      end;


      taDobSpec1.Edit;
      taDobSpec1SUMIN.AsFloat:=rv(rSumP);
      taDobSpec1SUMIN0.AsFloat:=rv(rSumP0);
      taDobSpec1.Post;

      taDobSpec1.Next;
    end;

    taDobSpec2.First;
    while not taDobSpec2.Eof do
    begin
      if taDobSpec1.Locate('ARTICUL',taDobSpec2IDCARD.AsInteger,[]) then
      if taDobSpec1QUANT.AsFloat<>0 then
      begin
        taDobSpec2.Edit;
        taDobSpec2PRICEIN.AsFloat:=taDobSpec1SUMIN.AsFloat/taDobSpec1QUANT.AsFloat;
        taDobSpec2SUMIN.AsFloat:=rv(taDobSpec1SUMIN.AsFloat/taDobSpec1QUANT.AsFloat*taDobSpec2QUANTC.AsFloat);
        taDobSpec2PRICEIN0.AsFloat:=taDobSpec1SUMIN0.AsFloat/taDobSpec1QUANT.AsFloat;
        taDobSpec2SUMIN0.AsFloat:=rv(taDobSpec1SUMIN0.AsFloat/taDobSpec1QUANT.AsFloat*taDobSpec2QUANTC.AsFloat);
        taDobSpec2.Post;
      end else
      begin
        taDobSpec2.Edit;
        taDobSpec2PRICEIN.AsFloat:=0;
        taDobSpec2SUMIN.AsFloat:=0;
        taDobSpec2PRICEIN0.AsFloat:=0;
        taDobSpec2SUMIN0.AsFloat:=0;
        taDobSpec2.Post;
      end;
      taDobSpec2.Next;
    end;

    taDobSpec1.Active:=False;
    taDobSpec2.Active:=False;

    if iSS<>2 then
    begin
      rSum1:=rSum;
      rSum2:=rSum;
    end
    else
    begin
      rSum1:=rSum0;
      rSum2:=rSum0;
    end;
  end;
end;

procedure TfmDocsOutB.prOpen(IdH,iNew:Integer;Memo1:TcxMemo);
Var bT:TDateTime;
    i:Integer;
begin
  with dmO do
  with dmORep do
  with fmDobSpec do
  begin
    bT:=Now;

    prAllViewOff;

//    Memo1.Lines.Add('        '+'������'); Delay(10);
    taDobSpec.Active:=False;
    taDobSpec.ParamByName('IDH').AsInteger:=IdH;
    taDobSpec.Active:=True;
//    Memo1.Lines.Add('        '+' -1:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);

    CloseTe(teSpecB);

    taDobSpec.First;
    while not taDOBSpec.Eof do
    begin
      teSpecB.Append;
      teSpecBID.AsInteger:=taDobSpecID.AsInteger;
      teSpecBIDCARD.AsInteger:=taDobSpecIDCARD.AsInteger;
      teSpecBQUANT.AsFloat:=taDobSpecQUANT.AsFloat;
      teSpecBIDM.AsInteger:=taDobSpecIDM.AsInteger;
      teSpecBSIFR.AsInteger:=taDobSpecSIFR.AsInteger;
      teSpecBNAMEB.AsString:=taDobSpecNAMEB.AsString;
      teSpecBCODEB.AsString:=taDobSpecCODEB.AsString;
      teSpecBKB.AsFloat:=taDobSpecKB.AsFloat;
      teSpecBPRICER.AsFloat:=taDobSpecPRICER.AsFloat;
      teSpecBDSUM.AsFloat:=taDobSpecDSUM.AsFloat;
      teSpecBRSUM.AsFloat:=taDobSpecRSUM.AsFloat;
      teSpecBNAME.AsString:=taDobSpecNAME.AsString;
      teSpecBNAMESHORT.AsString:=taDobSpecNAMESHORT.AsString;
      teSpecBTCARD.AsInteger:=taDobSpecTCARD.AsInteger;
      teSpecBKM.AsFloat:=taDobSpecKM.AsFloat;
      teSpecBSALET.AsInteger:=taDobSpecSALET.AsInteger;
      teSpecBSSALET.AsString:=taDobSpecSSALET.AsString;
      teSpecB.Post;

      taDOBSpec.Next; Delay(10);
    end;
 //   Memo1.Lines.Add('        '+' -2:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);


    CloseTe(teCalc);
//    Memo1.Lines.Add('        '+' -3:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);

//    if iNew=0 then   //��� ��������� ��� ����� �� ����, ��� �������� �������
    if iNew<100 then   //��� ��������� ����
    begin
      taDobSpec1.Active:=False;
      taDobSpec1.ParamByName('IDH').AsInteger:=IdH;
      taDobSpec1.Active:=True;
//      Memo1.Lines.Add('        '+' -4:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);
      taDobSpec1.First;
      while not taDobSpec1.Eof do
      begin
        teCalc.Append;
        teCalcArticul.AsInteger:=taDobSpec1ARTICUL.AsInteger;
        teCalcName.AsString:=taDobSpec1NAME.AsString;
        teCalcIdm.AsInteger:=taDobSpec1IDM.AsInteger;
        teCalcsM.AsString:=taDobSpec1SM.AsString;
        teCalcKm.AsFloat:=taDobSpec1KM.AsFloat;
        teCalcQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
        teCalcQuantFact.AsFloat:=0;
        teCalcQuantDiff.AsFloat:=0;
        teCalcSumIn.AsFloat:=taDobSpec1SUMIN.AsFloat;
        teCalcSumUch.AsFloat:=0;
        teCalcSumIn0.AsFloat:=taDobSpec1SUMIN0.AsFloat;
        teCalc.Post;

        taDobSpec1.Next;
      end;
      taDobSpec1.Active:=False;
 //     Memo1.Lines.Add('        '+' -5:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);

      taDobSpec2.Active:=False;
      taDobSpec2.ParamByName('IDH').AsInteger:=IdH;
      taDobSpec2.Active:=True;

      CloseTe(teCalcB);
      i:=1;
 //     Memo1.Lines.Add('        '+' -6:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);
      taDobSpec2.First;
      while not taDobSpec2.Eof do
      begin
        teCalcB.Append;
        teCalcBID.AsInteger:=taDobSpec2IDB.AsInteger;
        teCalcBCODEB.AsInteger:=taDobSpec2CODEB.AsInteger;
        teCalcBNAMEB.AsString:=taDobSpec2NAMEB.AsString;
        teCalcBQUANT.AsFloat:=taDobSpec2QUANT.AsFloat;
        teCalcBPRICEOUT.AsFloat:=taDobSpec2PRICEOUT.AsFloat;
        teCalcBSUMOUT.AsFloat:=taDobSpec2SUMOUT.AsFloat;
        teCalcBIDCARD.AsInteger:=taDobSpec2IDCARD.AsInteger;
        teCalcBNAMEC.AsString:=taDobSpec2NAMEC.AsString;
        teCalcBQUANTC.AsFloat:=taDobSpec2QUANTC.AsFloat;
        teCalcBPRICEIN.AsFloat:=taDobSpec2PRICEIN.AsFloat;
        teCalcBSUMIN.AsFloat:=taDobSpec2SUMIN.AsFloat;
        teCalcBIM.AsInteger:=taDobSpec2IM.AsInteger;
        teCalcBSM.AsString:=taDobSpec2SM.AsString;
        teCalcBSB.AsString:=taDobSpec2SB.AsString;
        teCalcBPRICEIN0.AsFloat:=taDobSpec2PRICEIN0.AsFloat;
        teCalcBSUMIN0.AsFloat:=taDobSpec2SUMIN0.AsFloat;
        teCalcB.Post;
//        if i mod 10 =0 then Memo1.Lines.Add('        '+IntToStr(i)+FormatDateTime(' nn:ss', Now-bT)); Delay(10);
        inc(i);
        taDobSpec2.Next;
      end;
      Memo1.Lines.Add('        '+IntToStr(i)+FormatDateTime(' nn:ss', Now-bT)); Delay(10);
 //     Memo1.Lines.Add('        '+' -7:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);
      taDobSpec.Active:=False;
      taDobSpec1.Active:=False;
      taDobSpec2.Active:=False;
    end;
  //  Memo1.Lines.Add('        '+' -8:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);

    prAllViewOn;
 //   Memo1.Lines.Add('        '+' -9:'+FormatDateTime('nn:ss', Now-bT)); Delay(10);

  end;
end;

procedure prSetDOBSpecPar(sOper:String;iType:Integer);
begin
  with dmO do
  with dmORep do
  with fmDobSpec do
  begin
    cxTextEdit1.Text:=quDocsOutBNUMDOC.AsString;
    cxTextEdit2.Text:=quDocsOutBCOMMENT.AsString;
    
    taOperD.Active:=False;
    taOperD.ParamByName('IDT').AsInteger:=2;
    taOperD.Active:=True;
    taOperD.Locate('ABR',quDocsOutBOPER.AsString,[]);

    cxLookupComboBox2.EditValue:=quDocsOutBOPER.AsString;
    cxLookupComboBox2.Text:=taOperDNAMEOPER.AsString;

    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;

    CurVal.IdMH:=quDocsOutBIDSKL.AsInteger;
    CurVal.NAMEMH:=quDocsOutBNAMEMH.AsString;

    if CurVal.IdMH<>0 then
    begin
      cxLookupComboBox1.EditValue:=CurVal.IdMH;
      cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         cxLookupComboBox1.EditValue:=CurVal.IdMH;
         cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;


    cxDateEdit1.Date:=quDocsOutBDATEDOC.AsDateTime;

    if iType=0 then //����������
    begin
      cxButton2.Visible:=True;
      cxButton2.Enabled:=True;

      cxTextEdit1.Tag:=0;
      cxTextEdit1.Text:=prGetNum(2,0);
      cxTextEdit1.Properties.ReadOnly:=False;
//      cxTextEdit2.Text:='';
      cxTextEdit2.Properties.ReadOnly:=False;

      cxLookupComboBox2.Properties.ReadOnly:=False;
      cxLookupComboBox1.Properties.ReadOnly:=False;

      cxDateEdit1.Date:=Date;
      cxDateEdit1.Properties.ReadOnly:=False;

      ViewBSIFR.Editing:=False;
      ViewBNAMEB.Editing:=False;
      ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
      ViewBKB.Editing:=True;
      ViewBQUANT.Editing:=True;
      ViewBPRICER.Editing:=True;
      ViewBDSUM.Editing:=True;
      ViewBRSUM.Editing:=True;
      ViewBIDCARD.Editing:=True; // ViewBIDCARD.Properties.ReadOnly:=False;
      ViewBNAME.Editing:=True;

      ViewBNAMESHORT.Editing:=False;
      ViewBNAMESHORT.Properties.ReadOnly:=False;

      ViewBTCARD.Editing:=False;
      ViewBKM.Editing:=False;

      Label11.Enabled:=True;
      Label12.Enabled:=True;
      Label13.Enabled:=True;
      Label3.Enabled:=True;

      acClear.Enabled:=True;
      acInsPos.Enabled:=True;
      acAddList.Enabled:=True;
      acDelPos.Enabled:=True;
      N2.Enabled:=True;
    end;

    if iType=1 then //��������������
    begin
      cxButton2.Visible:=True;
      cxButton2.Enabled:=True;
      cxTextEdit1.Tag:=quDocsOutBID.AsInteger;

      if (sOper ='Del')
         or (sOper ='Sale')
         or (sOper ='SaleBank')
//         or (sOper ='SalePC')
         or (sOper ='Ret')
         or (sOper ='RetBank') then //���������������� ���������
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=False;
        cxLookupComboBox2.Properties.ReadOnly:=True;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=False;
        ViewBPRICER.Editing:=False;
        ViewBDSUM.Editing:=False;
        ViewBRSUM.Editing:=False;
        ViewBIDCARD.Editing:=True; //ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True;

        ViewBNAMESHORT.Editing:=False;
        ViewBNAMESHORT.Properties.ReadOnly:=False;

        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        Label11.Enabled:=False;
        Label12.Enabled:=False;
        Label13.Enabled:=False;
        Label3.Enabled:=False;

        acClear.Enabled:=False;
        acInsPos.Enabled:=False;
        acAddList.Enabled:=False;
        acDelPos.Enabled:=False;
        N2.Enabled:=True;

      end else
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=False;
        cxLookupComboBox2.Properties.ReadOnly:=False;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=True;
        ViewBPRICER.Editing:=True;
        ViewBDSUM.Editing:=True;
        ViewBRSUM.Editing:=True;
        ViewBIDCARD.Editing:=True; //ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True;

        ViewBNAMESHORT.Editing:=False;
        ViewBNAMESHORT.Properties.ReadOnly:=False;

        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        Label11.Enabled:=True;
        Label12.Enabled:=True;
        Label13.Enabled:=True;
        Label3.Enabled:=True;

        acClear.Enabled:=True;
        acInsPos.Enabled:=True;
        acAddList.Enabled:=True;
        acDelPos.Enabled:=True;
        N2.Enabled:=True;
      end;
    end;
    if iType=2 then //��������
    begin
      cxTextEdit1.Properties.ReadOnly:=True;
      cxTextEdit2.Properties.ReadOnly:=True;
      cxTextEdit1.Tag:=quDocsOutBID.AsInteger;
      cxLookupComboBox2.Properties.ReadOnly:=True;
      cxLookupComboBox1.Properties.ReadOnly:=True;
      cxDateEdit1.Properties.ReadOnly:=True;

      ViewBSIFR.Editing:=False;
      ViewBNAMEB.Editing:=False;
      ViewBCODEB.Editing:=False; ViewBCODEB.Properties.ReadOnly:=True;
      ViewBKB.Editing:=False;
      ViewBQUANT.Editing:=False;
      ViewBPRICER.Editing:=False;
      ViewBDSUM.Editing:=False;
      ViewBRSUM.Editing:=False;
      ViewBIDCARD.Editing:=False; //ViewBIDCARD.Properties.ReadOnly:=True;
      ViewBNAME.Editing:=False;

      ViewBNAMESHORT.Editing:=False;
      ViewBNAMESHORT.Properties.ReadOnly:=True;

      ViewBTCARD.Editing:=False;
      ViewBKM.Editing:=False;

      cxButton2.Visible:=False;
      cxButton2.Enabled:=False;

      Label11.Enabled:=False;
      Label12.Enabled:=False;
      Label13.Enabled:=False;
      Label3.Enabled:=False;

      acClear.Enabled:=False;
      acInsPos.Enabled:=False;
      acAddList.Enabled:=False;
      acDelPos.Enabled:=False;
      N2.Enabled:=False;
    end;
  end;
end;


procedure TfmDocsOutB.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOutB.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOutB.Align:=AlClient;
  StatusBar1.Color:=$00FFCACA;
  ViewDocsOutB.RestoreFromIniFile(CurDir+GridIni);
  Memo1.Clear;

  SpeedBar1.Color := UserColor.TTnOutB;
  StatusBar1.Color:= UserColor.TTnOutB;
end;

procedure TfmDocsOutB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewDocsOutB.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsOutB.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmDocsOutB.Caption:='���������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmDocsOutB.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    with dmO do
    begin
      ViewDocsOutB.BeginUpdate;
      quDocsOutB.Active:=False;
      quDocsOutB.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsOutB.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsOutB.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsOutB.Active:=True;
      ViewDocsOutB.EndUpdate;
    end;
  end;
end;

procedure TfmDocsOutB.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prTakeDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  fmSelPerSkl1:=tfmSelPerSkl1.Create(Application);
  with fmSelPerSkl1 do
  begin
    if CommonSet.DateTo>=iMaxDate-1 then CommonSet.DateTo:=Date;
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=CommonSet.DateTo;
    cxTimeEdit1.Time:=StrToTimeDef(CommonSet.ZTimeShift,0);
    cxTimeEdit2.Time:=StrToTimeDef(CommonSet.ZTimeShift,0);

    if dmORep.quDB.Active then
    begin
      cxLookupComboBox1.EditValue:=dmORep.quDBID.AsInteger;
      cxLookupComboBox1.Text:=dmORep.quDBNAMEDB.AsString;
      DBName:=dmORep.quDBPATHDB.AsString;
    end;

    Label4.Caption:='';

{    cxLookupComboBox1.Visible:=True;
    cxLookupComboBox1.Enabled:=True;
    Label3.Visible:=True;}
  end;
  fmSelPerSkl1.ShowModal;
  if fmSelPerSkl1.ModalResult=mrOk then
  begin
    ViewDocsOutB.BeginUpdate;
    dmO.quDocsOutB.Active:=False;
    dmO.quDocsOutB.Active:=True;
    ViewDocsOutB.EndUpdate;
    dmO.quDocsOutB.Last;
  end;
  fmSelPerSkl1.quMHAll1.Active:=False;
  fmSelPerSkl1.Release;
end;

procedure TfmDocsOutB.acEditDoc1Execute(Sender: TObject);
begin
  //�������������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prEditDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmDobSpec.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        bStartOpen:=True;
        prSetDOBSpecPar(quDocsOutBOPER.AsString,1); //�������������� 1

        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);
        prOpen(quDocsOutBID.AsInteger,0,Memo1); //0 - ��������� ������������ ������� � �������
        Memo1.Lines.Add('������.');Delay(10);

        bStartOpen:=False;
        fmDobSpec.Show;

      end else showmessage('������������� �������������� �������� ������.');
    end;
  end;
end;

procedure TfmDocsOutB.acViewDoc1Execute(Sender: TObject);
begin
  //��������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prViewDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmDobSpec.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      bStartOpen:=True;
      prSetDOBSpecPar(quDocsOutBOPER.AsString,2); //2 - ��������

      Memo1.Clear;
      Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);
      prOpen(quDocsOutBID.AsInteger,0,Memo1);
      Memo1.Lines.Add('������.');Delay(10);

      bStartOpen:=False;
      fmDobSpec.Show;
    end;
  end;
end;

procedure TfmDocsOutB.ViewDocsOutBDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsOutBIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocsOutB.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if quDocsOutB.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsOutB.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsOutB.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSum:Real;
begin
//������������
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount=0 then  exit;
    if not CanDo('prOnDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsOutB.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutBNAMEMH.AsString+' � '+quDocsOutBNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

          // 1 - ������� ��������� ������
          // 2 - �������� ��������������
          // 3 - �������� ������

          IDH:=quDocsOutBID.AsInteger;
          if quDocsOutBIDSKL.AsInteger>0 then
          begin
            taDobSpec1.Active:=False;
            taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec1.Active:=True;

            if taDobSpec1.RecordCount=0 then
            begin
              Memo1.Clear;
              Memo1.Lines.Add('������������ ���������� !!!');
              Memo1.Lines.Add('  �� ���������� "������ ���������� ��� ��������".');

              taDobSpec1.Active:=False;
              exit;
            end;

            taDobSpec2.Active:=False;
            taDobSpec2.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec2.Active:=True;

            if taDobSpec2.RecordCount=0 then
            begin
              Memo1.Clear;
              Memo1.Lines.Add('������������ ���������� !!!');
              Memo1.Lines.Add('  �� ���������� "������ ������������� ����".');
              taDobSpec1.Active:=False;
              taDobSpec2.Active:=False;
              exit;
            end;

            prOn(IdH,quDocsOutBIDSKL.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),rSum,rSum);

            // 3 - �������� ������
            quDocsOutB.Edit;
            quDocsOutBIACTIVE.AsInteger:=1;
            quDocsOutBSUMIN.AsFloat:=RoundVal(rSum);
            quDocsOutB.Post;
            quDocsOutB.Refresh;

            fmPartIn1:=tfmPartIn1.Create(Application);
            fmPartIn1.Label5.Caption:=quDocsOutBNAMEMH.AsString;
            fmPartIn1.Label6.Caption:='�� ����.';
            fmPartIn1.ViewPartInPrice1.Visible:=False;
            fmPartIn1.ViewPartInNum.Visible:=False;

            fmPartIn1.ShowModal;
            fmPartIn1.Release;
            taPartTest.Active:=False;
          end else showmessage('���������� ����� �������� ���������.');
        end;
      end;
    end;
  end;
end;


procedure TfmDocsOutB.acOffDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rQs,rQ:Real;
begin
//��������
  with dmO do
  begin
    if quDocsOutB.RecordCount=0 then  exit;
 
    if not CanDo('prOffDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsOutB.RecordCount>0 then //���� ��� ����������
    begin
      if quDocsOutBIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutBNAMEMH.AsString+' � '+quDocsOutBNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;
          //������� ��� �������� ������
//          ?IDDOC, ?DTYPE
          if pos('Ret',quDocsOutBOPER.AsString)=0 then
          begin
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutBID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=2;
            prDelPartOut.ExecProc;
          end
          else //��� ����� ��������
          begin
            IDH:=quDocsOutBID.AsInteger;

            taDobSpec1.Active:=False;
            taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec1.Active:=True;

            taDobSpec1.First;
            while not taDobSpec1.Eof do
            begin
//              rQs:=taDobSpec1QUANT.AsFloat; //taDobSpec1 - � �������� �������� ���������
//������ �� � �������� � � ������� - ���������

              rQs:=taDobSpec1QUANT.AsFloat*taDobSpec1KM.AsFloat; //taDobSpec1 - � �������� �������� ���������

              quSelPartIn1.Active:=False;
              quSelPartIn1.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
              quSelPartIn1.ParamByName('IDSKL').AsInteger:=quDocsOutBIDSKL.AsInteger;
              quSelPartIn1.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
              quSelPartIn1.Active:=True;

              quSelPartIn1.First;
              if quSelPartIn1.RecordCount>0 then
              begin
                  //��������� ������� ��������� ������
                rQ:=rQs+quSelPartIn1QREMN.AsFloat;
                quSelPartIn1.Edit;
                quSelPartIn1QREMN.AsFloat:=rQ;
                quSelPartIn1.Post;

                  //�������� ��������������
//EXECUTE PROCEDURE PR_GDSMOVE (?IART, ?IDATE, ?IDSTORE, ?RPOSTIN, ?RPOSTOUT, ?RVNIN, ?RVNOUT, ?RINV, ?RQREAL)
                prGdsMove.ParamByName('IART').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                prGdsMove.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                prGdsMove.ParamByName('IDSTORE').AsInteger:=quDocsOutBIDSKL.AsInteger;
                prGdsMove.ParamByName('RPOSTIN').AsFloat:=rQs;
                prGdsMove.ParamByName('RPOSTOUT').AsFloat:=0;
                prGdsMove.ParamByName('RVNIN').AsFloat:=0;
                prGdsMove.ParamByName('RVNOUT').AsFloat:=0;
                prGdsMove.ParamByName('RINV').AsFloat:=0;
                prGdsMove.ParamByName('RQREAL').AsFloat:=0;
                prGdsMove.ExecProc;

              end;
              quSelPartIn1.Active:=False;

              taDobSpec1.Next;
            end;
            taDobSpec1.Active:=False;
          end;

          quDocsOutB.Edit;
          quDocsOutBIACTIVE.AsInteger:=0;
          quDocsOutB.Post;
          quDocsOutB.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsOutB.Timer1Timer(Sender: TObject);
begin
  if bClearDocOutB=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOutB:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOutB:=True;
end;

procedure TfmDocsOutB.acAddDocManualExecute(Sender: TObject);
begin
//  �������� �������� �������
  if not CanDo('prAddDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmDobSpec.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  with dmO do
  with fmDobSpec do
  begin
    CloseTe(teSpecB);
    CloseTe(teCalc);
    CloseTe(teCalcB);

    prSetDOBSpecPar(quDocsOutBOPER.AsString,0); //���������� 0

    fmDobSpec.Show;
  end;
end;

procedure TfmDocsOutB.acSebBlExecute(Sender: TObject);
begin
 //�������������
  if not CanDo('prDocOutBSeb') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with fmDobSpec do
  begin
    begin
      Memo1.Lines.Add('�����, ���� ������.'); delay(10);

      CloseTe(teCalcB);

      taDobSpec2.Active:=False;
      taDobSpec2.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      taDobSpec2.Active:=True;
      taDobSpec2.First;
      while not taDobSpec2.Eof do
      begin
        teCalcB.Append;
        teCalcBID.AsInteger:=taDobSpec2IDB.AsInteger;
        teCalcBCODEB.AsInteger:=taDobSpec2CODEB.AsInteger;
        teCalcBNAMEB.AsString:=taDobSpec2NAMEB.AsString;
        teCalcBQUANT.AsFloat:=taDobSpec2QUANT.AsFloat;
        teCalcBPRICEOUT.AsFloat:=taDobSpec2PRICEOUT.AsFloat;
        teCalcBSUMOUT.AsFloat:=taDobSpec2SUMOUT.AsFloat;
        teCalcBIDCARD.AsInteger:=taDobSpec2IDCARD.AsInteger;
        teCalcBNAMEC.AsString:=taDobSpec2NAMEC.AsString;
        teCalcBQUANTC.AsFloat:=taDobSpec2QUANTC.AsFloat;
        teCalcBPRICEIN.AsFloat:=taDobSpec2PRICEIN.AsFloat;
        teCalcBSUMIN.AsFloat:=taDobSpec2SUMIN.AsFloat;
        teCalcBIM.AsInteger:=taDobSpec2IM.AsInteger;
        teCalcBSM.AsString:=taDobSpec2SM.AsString;
        teCalcBSB.AsString:=taDobSpec2SB.AsString;
        teCalcB.Post;

        taDobSpec2.Next;
      end;
      taDobSpec2.Active:=False;

      Memo1.Lines.Add('������ ��������.'); delay(10);
      with dmORep do
      begin
        frRepDOB.LoadFromFile(CurDir + 'BludaSeb1.frf');

        frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
        frVariables.Variable['SumOut']:=quDocsOutBSUMUCH.AsFloat;
        frVariables.Variable['DocOper']:=quDocsOutBOPER.AsString;
        frVariables.Variable['SumIn']:=quDocsOutBSUMIN.AsFloat;

        frRepDOB.ReportName:='������������� ����.';
        frRepDOB.PrepareReport;
        frRepDOB.ShowPreparedReport;
      end;
    end;
  end;
end;

procedure TfmDocsOutB.acAddDocBExecute(Sender: TObject);
begin
//��������

end;

procedure TfmDocsOutB.FormShow(Sender: TObject);
begin
  bStartOpen:=False;
end;

procedure TfmDocsOutB.acPrintMinExecute(Sender: TObject);
Var IdPar:Integer;
begin
  //������������� ������
  if not CanDo('prDocOutBSeb') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      if quDocsOutBIACTIVE.AsInteger=1 then
      begin
        idPar:=GetId('Pars');
        taParams.Active:=False;
        taParams.Active:=True;
        taParams.Append;
        taParamsID.AsInteger:=idPar;
        taParamsIDATEB.AsInteger:=Trunc(Date);
        taParamsIDATEE.AsInteger:=Trunc(Date);
        taParamsIDSTORE.AsInteger:=quDocsOutBID.AsInteger;
        taParams.Post;
        taParams.Active:=False;

        quDocOutBPrint.Active:=False;
        quDocOutBPrint.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
        quDocOutBPrint.Active:=True;

        RepDocOutB.LoadFromFile(CurDir + 'DocOutBSeb.frf');

        frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
        frVariables.Variable['DocStore']:=quDocsOutBNAMEMH.AsString;
        frVariables.Variable['Depart']:=CommonSet.DepartName;
        frVariables.Variable['SumIn']:=quDocsOutBSUMIN.AsFloat;
        frVariables.Variable['Comment']:=quDocsOutBCOMMENT.AsString;
        frVariables.Variable['Operation']:=quDocsOutBOPER.AsString;

        RepDocOutB.ReportName:='�������� ����.';
        RepDocOutB.PrepareReport;
        RepDocOutB.ShowPreparedReport;

        quDocOutBPrint.Active:=False;
      end else showmessage('������ ����������, �������� ������ ���� �����������.');
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsOutB.acCopyExecute(Sender: TObject);
var Par:Variant;
    IDH:INteger;
    iNum:INteger;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=2; //����������
    par[1]:=quDocsOutBID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=2;
      taHeadDocId.AsInteger:=quDocsOutBID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsOutBNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=0;
      taHeadDocNameCli.AsString:=quDocsOutBOPER.AsString;
      taHeadDocIdSkl.AsInteger:=quDocsOutBIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsOutBNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsOutBSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsOutBSUMUCH.AsFloat;
      taHeadDocComment.AsString:=quDocsOutBCOMMENT.AsString;
      taHeadDoc.Post;

      IDH:=quDocsOutBID.AsInteger;


      taDobSpec.Active:=False;
      taDobSpec.ParamByName('IDH').AsInteger:=IDH;
      taDobSpec.Active:=True;

      taDobSpec.First;  iNum:=1;
      while not taDOBSpec.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=2;
        taSpecDocIdHead.AsInteger:=IDH;
        taSpecDocNum.AsInteger:=iNum;
        taSpecDocIdCard.AsInteger:=taDobSpecIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=taDobSpecQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=0;
        taSpecDocSumIn.AsFloat:=0;
        taSpecDocPriceUch.AsFloat:=taDobSpecPRICER.AsFloat;
        taSpecDocSumUch.AsFloat:=taDobSpecRSUM.AsFloat;
        taSpecDocIdNds.AsInteger:=0;
        taSpecDocSumNds.AsFloat:=0;
        taSpecDocNameC.AsString:=Copy(taDobSpecNAME.AsString,1,30);
        taSpecDocSm.AsString:=taDobSpecNAMESHORT.AsString;
        taSpecDocIdM.AsInteger:=taDobSpecIDM.AsInteger;
        taSpecDocKm.AsFloat:=taDobSpecKM.AsFloat;
        taSpecDocProcPrice.AsFloat:=taDobSpecDSUM.AsFloat;
        taSpecDoc.Post;

        inc(iNum);

        taDobSpec.Next;
      end;
      taDobSpec.Active:=False;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsOutB.acPastExecute(Sender: TObject);
Var iNum:Integer;
begin
//��������
  with dmO do
  with dmORep do
  with fmDobSpec do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

//    taSpecDoc1.Active:=False;  //��� �������
//    taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
//    if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
//    else taSpecDoc1.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocOutB') then
        begin
          prAllViewOff;

          CloseTe(teSpecB);
          CloseTe(teCalc);
          CloseTe(teCalcB);

          prSetDOBSpecPar(taHeadDocNameCli.AsString,0); //���������� 0
          fmDobSpec.cxTextEdit2.Text:=taHeadDocComment.AsString;

          taSpecDoc.First; iNum:=1;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin

              teSpecB.Append;
              teSpecBID.AsInteger:=iNum;
              teSpecBIDCARD.AsInteger:=taSpecDocIdCard.AsInteger;
              teSpecBQUANT.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
              teSpecBIDM.AsInteger:=taSpecDocIdM.AsInteger;
              teSpecBSIFR.AsInteger:=0;
              teSpecBNAMEB.AsString:='';
              teSpecBCODEB.AsString:='';
              teSpecBKB.AsFloat:=1;
              teSpecBPRICER.AsFloat:=taSpecDocPriceUch.AsFloat;
              teSpecBDSUM.AsFloat:=taSpecDocProcPrice.AsFloat;
              teSpecBRSUM.AsFloat:=taSpecDocSumUch.AsFloat;
              teSpecBNAME.AsString:=taSpecDocNameC.AsString;
              teSpecBNAMESHORT.AsString:=taSpecDocSm.AsString;
              teSpecBTCARD.AsInteger:=prTypeTC(taSpecDocIdCard.AsInteger);
              teSpecBKM.AsFloat:=taSpecDocKm.AsFloat;
              teSpecBSALET.AsInteger:=0;
              teSpecBSSALET.AsString:='';
              teSpecB.Post;

              inc(iNum);
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;   
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmDobSpec.Show;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsOutB.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsOutB);
end;

procedure TfmDocsOutB.ViewDocsOutBCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA,sI:String;
    iT:INteger;
begin
  sA:=' ';
  for i:=0 to ViewDocsOutB.ColumnCount-1 do
  begin
    if ViewDocsOutB.Columns[i].Name='ViewDocsOutBIACTIVE' then sA:=AViewInfo.GridRecord.DisplayTexts[i];
    if ViewDocsOutB.Columns[i].Name='ViewDocsOutBIDCLI' then sI:=AViewInfo.GridRecord.DisplayTexts[i];
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;

//  if pos('0',sI)>0 then ACanvas.Canvas.Brush.Color := $00FFBC79;
  iT:=StrToIntDef(sI,-1);
  if iT>=0 then ACanvas.Canvas.Brush.Color := $00FFBC79;
  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF;

end;

procedure TfmDocsOutB.acPrintActSpExecute(Sender: TObject);
Var IdPar:INteger;
begin
  //������ ���� �� ��������
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then //���� ��� ��������
    begin
      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(Date);
      taParamsIDATEE.AsInteger:=Trunc(Date);
      taParamsIDSTORE.AsInteger:=quDocsOutBID.AsInteger;
      taParams.Post;
      taParams.Active:=False;

      quDocOutBPrint.Active:=False;
      quDocOutBPrint.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      quDocOutBPrint.Active:=True;

      RepDocOutB.LoadFromFile(CurDir + 'DocOutBAct.frf');

      frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quDocsOutBNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['SumIn']:=quDocsOutBSUMIN.AsFloat;
      frVariables.Variable['Comment']:=quDocsOutBCOMMENT.AsString;
      frVariables.Variable['SSumIn']:=MoneyToString(quDocsOutBSUMIN.AsFloat,True,False);

      RepDocOutB.ReportName:='��� ��������.';
      RepDocOutB.PrepareReport;
      RepDocOutB.ShowPreparedReport;

      quDocOutBPrint.Active:=False;

    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsOutB.N5Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnOutB := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsOutB.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
