unit DepPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxLabel, ExtCtrls,
  dxfBackGround, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmDepPars = class(TForm)
    dxfBackGround1: TdxfBackGround;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    cxTextEdit7: TcxTextEdit;
    cxTextEdit8: TcxTextEdit;
    cxTextEdit9: TcxTextEdit;
    cxLabel10: TcxLabel;
    cxTextEdit10: TcxTextEdit;
    cxLabel11: TcxLabel;
    cxTextEdit11: TcxTextEdit;
    cxLabel12: TcxLabel;
    cxTextEdit12: TcxTextEdit;
    cxLabel13: TcxLabel;
    cxTextEdit13: TcxTextEdit;
    cxLabel14: TcxLabel;
    cxTextEdit14: TcxTextEdit;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    cxTextEdit15: TcxTextEdit;
    cxTextEdit16: TcxTextEdit;
    cxTextEdit17: TcxTextEdit;
    cxTextEdit18: TcxTextEdit;
    cxTextEdit19: TcxTextEdit;
    cxTextEdit20: TcxTextEdit;
    cxTextEdit21: TcxTextEdit;
    cxTextEdit22: TcxTextEdit;
    cxLabel17: TcxLabel;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxLabel21: TcxLabel;
    cxLabel22: TcxLabel;
    cxLabel23: TcxLabel;
    cxTextEdit23: TcxTextEdit;
    cxLabel24: TcxLabel;
    cxTextEdit24: TcxTextEdit;
    cxLabel25: TcxLabel;
    cxTextEdit25: TcxTextEdit;
    cxLabel26: TcxLabel;
    cxTextEdit26: TcxTextEdit;
    cxLabel27: TcxLabel;
    cxLabel28: TcxLabel;
    cxTextEdit27: TcxTextEdit;
    cxTextEdit28: TcxTextEdit;
    cxLabel29: TcxLabel;
    cxTextEdit29: TcxTextEdit;
    cxLabel30: TcxLabel;
    cxTextEdit30: TcxTextEdit;
    cxLabel31: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel32: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxLabel33: TcxLabel;
    cxTextEdit31: TcxTextEdit;
    cxLabel34: TcxLabel;
    cxTextEdit32: TcxTextEdit;
    cxLabel35: TcxLabel;
    cxTextEdit33: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDepPars: TfmDepPars;

implementation

{$R *.dfm}

end.
