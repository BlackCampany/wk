object fmAddEU: TfmAddEU
  Left = 454
  Top = 304
  BorderStyle = bsDialog
  Caption = 'fmAddEU'
  ClientHeight = 274
  ClientWidth = 368
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 218
    Width = 368
    Height = 56
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 64
      Top = 16
      Width = 97
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 200
      Top = 16
      Width = 97
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072'  Esc'
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLabel1: TcxLabel
    Left = 16
    Top = 24
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 96
    Top = 22
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Text = 'cxTextEdit1'
    Width = 241
  end
  object cxLabel2: TcxLabel
    Left = 24
    Top = 68
    Caption = 'C 01.03 '#1087#1086' 31.05'
    Transparent = True
    Visible = False
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 168
    Top = 64
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 4
    Width = 121
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 168
    Top = 96
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 5
    Width = 121
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 168
    Top = 128
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 6
    Width = 121
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 168
    Top = 160
    EditValue = 0.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 7
    Width = 121
  end
  object cxLabel3: TcxLabel
    Left = 24
    Top = 100
    Caption = 'C 01.06 '#1087#1086' 31.08'
    Transparent = True
    Visible = False
  end
  object cxLabel4: TcxLabel
    Left = 24
    Top = 132
    Caption = 'C 01.09 '#1087#1086' 30.11'
    Transparent = True
    Visible = False
  end
  object cxLabel5: TcxLabel
    Left = 24
    Top = 164
    Caption = 'C 01.12 '#1087#1086' 28.02'
    Transparent = True
    Visible = False
  end
  object amAddEU: TActionManager
    Left = 312
    Top = 96
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      OnExecute = acExitExecute
    end
  end
end
