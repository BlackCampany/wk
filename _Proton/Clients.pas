unit Clients;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, Placemnt, cxContainer, cxLabel, cxTextEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxImageComboBox, cxSchedulerStorage,
  cxCheckBox;

type
  TfmClients = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    ViewCli: TcxGridDBTableView;
    LevelCli: TcxGridLevel;
    GridCli: TcxGrid;
    LevelIP: TcxGridLevel;
    ViewIP: TcxGridDBTableView;
    ViewCliVendor: TcxGridDBColumn;
    ViewCliName: TcxGridDBColumn;
    ViewCliPhone1: TcxGridDBColumn;
    ViewCliOKPO: TcxGridDBColumn;
    ViewCliINN: TcxGridDBColumn;
    ViewCliFullName: TcxGridDBColumn;
    ViewIPCode: TcxGridDBColumn;
    ViewIPName: TcxGridDBColumn;
    ViewIPCountry: TcxGridDBColumn;
    ViewIPINN: TcxGridDBColumn;
    ViewIPFullName: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxButton3: TcxButton;
    amCli: TActionManager;
    acAddCli: TAction;
    acEditCli: TAction;
    acDelCli: TAction;
    acViewCli: TAction;
    cxLabel4: TcxLabel;
    acFindCli: TAction;
    acUpCli: TAction;
    acDownCli: TAction;
    cxLabel5: TcxLabel;
    PopupMenu1: TPopupMenu;
    acRecalcAss: TAction;
    N1: TMenuItem;
    ViewCliPhone2: TcxGridDBColumn;
    ViewCliFax: TcxGridDBColumn;
    ViewCliCodeUchet: TcxGridDBColumn;
    ViewCliStatus: TcxGridDBColumn;
    ViewCliStatusByte: TcxGridDBColumn;
    ViewCliRaion: TcxGridDBColumn;
    acGrafPost: TAction;
    cxLabel6: TcxLabel;
    N2: TMenuItem;
    N3: TMenuItem;
    cxButton4: TcxButton;
    cxButton1: TcxButton;
    Label1: TLabel;
    GridFindCli: TcxGrid;
    ViewFindCli: TcxGridDBTableView;
    ViewFindCliVendor: TcxGridDBColumn;
    ViewFindCliName: TcxGridDBColumn;
    ViewFindCliINN: TcxGridDBColumn;
    ViewFindCliFullName: TcxGridDBColumn;
    ViewFindCliPhone1: TcxGridDBColumn;
    ViewFindCliOKPO: TcxGridDBColumn;
    ViewFindCliPhone2: TcxGridDBColumn;
    ViewFindCliFax: TcxGridDBColumn;
    ViewFindCliCodeUchet: TcxGridDBColumn;
    ViewFindCliStatus: TcxGridDBColumn;
    ViewFindCliStatusByte: TcxGridDBColumn;
    ViewFindCliRaion: TcxGridDBColumn;
    LevelFindCli: TcxGridLevel;
    N4: TMenuItem;
    ViewCliPhone4: TcxGridDBColumn;
    ViewIPPhone4: TcxGridDBColumn;
    acEditLic: TAction;
    ViewCliMailVoz: TcxGridDBColumn;
    ViewCliMolVoz: TcxGridDBColumn;
    ViewCliPhoneVoz: TcxGridDBColumn;
    ViewCliBlockV: TcxGridDBColumn;
    ViewCliTypeV: TcxGridDBColumn;
    procedure GridCliFocusedViewChanged(Sender: TcxCustomGrid;
      APrevFocusedView, AFocusedView: TcxCustomGridView);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton2Click(Sender: TObject);
    procedure ViewCliDblClick(Sender: TObject);
    procedure ViewIPDblClick(Sender: TObject);
    procedure acEditCliExecute(Sender: TObject);
    procedure acViewCliExecute(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acAddCliExecute(Sender: TObject);
    procedure acDelCliExecute(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure acFindCliExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLabel5Click(Sender: TObject);
    procedure acRecalcAssExecute(Sender: TObject);
    procedure ViewCliCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acGrafPostExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxTextEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure ViewCliKeyPress(Sender: TObject; var Key: Char);
    procedure ViewFindCliKeyPress(Sender: TObject; var Key: Char);
    procedure ViewFindCliDblClick(Sender: TObject);
    procedure ViewFindCliCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N4Click(Sender: TObject);
    procedure acEditLicExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClients: TfmClients;

implementation

uses Un1, MDB, AddCli, CliPars, Period1, MT, Status, GrafPost, CliLicense,
  u2fdk;

{$R *.dfm}

procedure TfmClients.GridCliFocusedViewChanged(Sender: TcxCustomGrid;
  APrevFocusedView, AFocusedView:TcxCustomGridView);
begin
{  if AFocusedView=ViewCli then ShowMessage('Cli');
  if AFocusedView=ViewIP then ShowMessage('Ip');}
end;

procedure TfmClients.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;

  ViewCli.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewIP.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);

  GridCli.Align:=AlClient;
  //GridFindCli.Align:=AlClient;
  cxTextEdit1.Text:='';
  end;

procedure TfmClients.cxButton1Click(Sender: TObject);
Var S:String;
begin
//  if bOpen then exit;
  s:=AnsiUpperCase(cxTextEdit1.Text);
  if Length(S)<1 then exit;

 { if  (cxCheckBox1.Checked = false) then                      //������ �����
  begin
    with dmMC do
    begin
      fmClients.ViewFindCli.BeginUpdate;
      dsquFindCli.DataSet:=nil;
      try
        quFindCli.Active:=False;
        quFindCli.SQL.Clear;
        quFindCli.SQL.Add('select * from RVendor where Name like ''%'+S+'%''');
        quFindCli.SQL.Add('and Name not like ''��'+'%''');
        quFindCli.SQL.Add('and Name not like ''� ��'+'%''');
        quFindCli.SQL.Add('and Status not like ''-'+'%''');
        quFindCli.SQL.Add('order by Name asc');
        quFindCli.Active:=True;
      finally
        dsquFindCli.DataSet:=quFindCli;
        if quFindCli.RecordCount>0 then GridFindCli.SetFocus;
        fmClients.ViewFindCli.EndUpdate;
      end;
    end;
  end
  else
  begin   }
    with dmMC do
    begin
      fmClients.ViewFindCli.BeginUpdate;
      dsquFindCli.DataSet:=nil;
      try
        quFindCli.Active:=False;
        quFindCli.SQL.Clear;
        quFindCli.SQL.Add('select * from RVendor ');
        quFindCli.SQL.Add('where Name like ''%'+S+'%''');
        quFindCli.SQL.Add('order by Name asc');
        quFindCli.Active:=True;
      finally
        dsquFindCli.DataSet:=quFindCli;
        if quFindCli.RecordCount>0 then GridFindCli.SetFocus;
        fmClients.ViewFindCli.EndUpdate;
      end;
     end;
 //  end;
end;

procedure TfmClients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCli.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewIP.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;   

procedure TfmClients.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmClients.ViewCliDblClick(Sender: TObject);
begin
  if ViewCli.Controller.FocusedColumn.Name='ViewCliPhone4' then acEditLic.Execute
  else ModalResult:=mrOk;
end;

procedure TfmClients.ViewIPDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmClients.acEditCliExecute(Sender: TObject);
var iNDS:Integer;
begin
  //�������������
  if not CanDo('prEditCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if CommonSet.Single=0 then
  begin
    exit;
//    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  //��� ���������
  with dmMC do
  begin
    fmAddCli.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit3.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit6.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit7.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit10.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit8.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit11.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit9.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit4.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit5.Properties.ReadOnly:=False;
    fmAddCli.cxCheckBox1.Properties.ReadOnly:=False;
    fmAddCli.cxButton1.Enabled:=True;

    fmAddCli.cxComboBox1.Properties.ReadOnly:=False;
    fmAddCli.cxComboBox2.Properties.ReadOnly:=False;
    fmAddCli.Label16.Caption:=''; //��� ��
    fmAddCli.cxComboBox1.ItemIndex:=0;  //��� �� ����
    fmAddCli.cxComboBox2.ItemIndex:=0;  //��� ������

    if GridCli.ActiveView=ViewCli then
    begin //���
      if quCli.RecordCount>0 then
      begin
        fmAddCli.cxTextEdit1.Tag:=quCliVendor.AsInteger; //���
        fmAddCli.cxTextEdit1.Text:=quCliName.AsString;   //��������
        fmAddCli.cxTextEdit2.Text:=quCliFullName.AsString; //������ ��������
        fmAddCli.cxTextEdit3.Text:=quCliINN.AsString;     //���

        fmAddCli.cxTextEdit6.Text:=quCliOKPO.AsString; //���
        fmAddCli.cxTextEdit13.Text:=quCliCodeUchet.AsString; // ��� �����

        fmAddCli.cxTextEdit7.Text:=quCliGorod.AsString; //�����
        fmAddCli.cxTextEdit10.Text:=quCliPostIndex.AsString;
        fmAddCli.cxTextEdit8.Text:=quCliStreet.AsString; //�����
        fmAddCli.cxTextEdit11.Text:=quCliHouse.AsString; //�����
        fmAddCli.cxTextEdit9.Text:=quCliKvart.AsString; //�����

        fmAddCli.cxTextEdit4.Text:=quCliPhone1.AsString; //�������
        fmAddCli.cxTextEdit5.Text:=quCliPhone2.AsString; //�������������
        fmAddCli.cxTextEdit12.Text:=quCliFax.AsString;   //EMail

        if quCliUnTaxedNDS.AsInteger=0 then  fmAddCli.cxCheckBox1.Checked:=True
        else fmAddCli.cxCheckBox1.Checked:=False;

        fmAddCli.Label16.Caption:=its(quCliStatus.AsInteger); //��� ��
        fmAddCli.cxComboBox1.ItemIndex:=quCliStatusByte.AsInteger;  //��� �� ����
        fmAddCli.cxComboBox2.ItemIndex:=quCliRaion.AsInteger;  //��� ������

        fmAddCli.ShowModal;
        if fmAddCli.ModalResult=mrOk then
        begin
           if fmAddCli.cxCheckBox1.Checked then iNds:=0 else iNds:=1;   // iNds=0 ������ ����������

          quE.Active:=False;
          quE.SQL.Clear;

          quE.SQL.Add('Update "RVendor" Set ');
          quE.SQL.Add('Name='''+fmAddCli.cxTextEdit1.Text+'''');
//          quE.SQL.Add(',PayDays=0');
          quE.SQL.Add(',PayForm=0');
          quE.SQL.Add(',PayWaste=0');
//          quE.SQL.Add(',FirmName='''+Copy(fmAddCli.cxTextEdit4.Text,1,16)+'''');
          quE.SQL.Add(',Phone1='''+Copy(fmAddCli.cxTextEdit4.Text,1,16)+'''');
          quE.SQL.Add(',Phone2='''+Copy(fmAddCli.cxTextEdit5.Text,1,16)+'''');
          quE.SQL.Add(',Phone3=''''');
//          quE.SQL.Add(',Phone4=''''');
          quE.SQL.Add(',Fax='''+Copy(fmAddCli.cxTextEdit12.Text,1,16)+'''');
          quE.SQL.Add(',TreatyDate='''+ds(date)+'''');
          quE.SQL.Add(',TreatyNumber=0');
          quE.SQL.Add(',PostIndex='''+fmAddCli.cxTextEdit10.Text+'''');
          quE.SQL.Add(',Gorod='''+Copy(fmAddCli.cxTextEdit7.Text,1,30)+'''');
          quE.SQL.Add(',Raion='+its(fmAddCli.cxComboBox2.ItemIndex));
          quE.SQL.Add(',Street='''+Copy(fmAddCli.cxTextEdit8.Text,1,30)+'''');
          quE.SQL.Add(',House='''+Copy(fmAddCli.cxTextEdit11.Text,1,6)+'''');
          quE.SQL.Add(',Build=''''');
          quE.SQL.Add(',Kvart='''+Copy(fmAddCli.cxTextEdit9.Text,1,4)+'''');
          quE.SQL.Add(',OKPO='''+fmAddCli.cxTextEdit6.Text+'''');
          quE.SQL.Add(',OKONH=''''');
          quE.SQL.Add(',INN='''+fmAddCli.cxTextEdit3.Text+'''');
          quE.SQL.Add(',FullName='''+fmAddCli.cxTextEdit2.Text+'''');
          quE.SQL.Add(',CodeUchet='''+fmAddCli.cxTextEdit13.Text+'''');
          quE.SQL.Add(',UnTaxedNDS='+its(iNds));
          quE.SQL.Add(',StatusByte='+its(fmAddCli.cxComboBox1.ItemIndex));
//          quE.SQL.Add(',Status=0'); //����������� ��� ������� �� �� ������
          quE.SQL.Add('where Vendor='+Its(fmAddCli.cxTextEdit1.Tag));
          quE.ExecSQL;

          ViewCli.BeginUpdate;
          quCli.Active:=False;
          quCli.Active:=True;
          quCli.Locate('Vendor',fmAddCli.cxTextEdit1.Tag,[]);
          ViewCli.EndUpdate;
        end;
      end;
    end else
    begin //��
      if quIP.RecordCount>0 then
      begin
        fmAddCli.cxTextEdit1.Tag:=quIPCode.AsInteger; //���
        fmAddCli.cxTextEdit1.Text:=quIPName.AsString;   //��������
        fmAddCli.cxTextEdit2.Text:=quIPFullName.AsString; //������ ��������
        fmAddCli.cxTextEdit3.Text:=quIPINN.AsString;     //���
        fmAddCli.cxTextEdit6.Text:=quIPCodeUchet.AsString; //���

        fmAddCli.cxTextEdit7.Text:=quIPGorod.AsString; //�����
        fmAddCli.cxTextEdit10.Text:=quIPPostIndex.AsString; //�����
        fmAddCli.cxTextEdit8.Text:=quIPStreet.AsString; //�����
        fmAddCli.cxTextEdit11.Text:=quIPHouse.AsString; //�����
        fmAddCli.cxTextEdit9.Text:=quIPKvart.AsString; //�����

        fmAddCli.cxTextEdit4.Text:=quIPPhone1.AsString; //�������
        fmAddCli.cxTextEdit5.Text:=quIPPhone2.AsString; //�������������
        fmAddCli.cxTextEdit12.Text:=quIPFax.AsString; //Fax

        if quIPStatus.AsInteger>0 then  fmAddCli.cxCheckBox1.Checked:=True
        else fmAddCli.cxCheckBox1.Checked:=False;

        fmAddCli.ShowModal;
        if fmAddCli.ModalResult=mrOk then
        begin
          quE.Active:=False;
          quE.SQL.Clear;

          quE.SQL.Add('Update "RIndividual" Set ');
          quE.SQL.Add('Name='''+fmAddCli.cxTextEdit1.Text+'''');
          quE.SQL.Add(',FirmName=''''');
          quE.SQL.Add(',Familia='''+fmAddCli.cxTextEdit1.Text+'''');
          quE.SQL.Add(',Ima=''''');
          quE.SQL.Add(',Otchestvo=''''');
          quE.SQL.Add(',VidDok=''''');
          quE.SQL.Add(',SerPasp=''''');
          quE.SQL.Add(',NumberPasp=''''');
          quE.SQL.Add(',KemPasp=''''');
          quE.SQL.Add(',DatePasp='''+ds(date)+'''');
          quE.SQL.Add(',Country=''''');
          quE.SQL.Add(',Phone1='''+Copy(fmAddCli.cxTextEdit5.Text,1,16)+'''');
          quE.SQL.Add(',Phone2=''''');
          quE.SQL.Add(',Phone3=''''');
//          quE.SQL.Add(',Phone4=''''');
          quE.SQL.Add(',Fax='''+Copy(fmAddCli.cxTextEdit12.Text,1,16)+'''');
          quE.SQL.Add(',EMail=''''');
          quE.SQL.Add(',PostIndex='''+fmAddCli.cxTextEdit10.Text+'''');
          quE.SQL.Add(',Gorod='''+Copy(fmAddCli.cxTextEdit7.Text,1,30)+'''');
          quE.SQL.Add(',Raion=0');
          quE.SQL.Add(',Street='''+Copy(fmAddCli.cxTextEdit8.Text,1,30)+'''');
          quE.SQL.Add(',House='''+Copy(fmAddCli.cxTextEdit11.Text,1,6)+'''');
          quE.SQL.Add(',Build=''''');
          quE.SQL.Add(',Kvart='''+Copy(fmAddCli.cxTextEdit9.Text,1,4)+'''');
          quE.SQL.Add(',INN='''+fmAddCli.cxTextEdit3.Text+'''');
          quE.SQL.Add(',NStrach=''''');
          quE.SQL.Add(',NumberSvid=''''');
          quE.SQL.Add(',DateSvid='''+ds(date)+'''');
          quE.SQL.Add(',FullName='''+fmAddCli.cxTextEdit2.Text+'''');
          quE.SQL.Add(',DateBorn='''+ds(date)+'''');
          quE.SQL.Add(',CodeUchet='''+fmAddCli.cxTextEdit6.Text+'''');
          quE.SQL.Add(',Status=0');
          quE.SQL.Add('where Code='+Its(fmAddCli.cxTextEdit1.Tag));
          quE.ExecSQL;

          ViewIP.BeginUpdate;
          quIP.Active:=False;
          quIP.Active:=True;
          quIP.Locate('Code',fmAddCli.cxTextEdit1.Tag,[]);
          ViewIP.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmClients.acViewCliExecute(Sender: TObject);
begin
  //��������
  if not CanDo('prViewCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    fmAddCli.cxTextEdit1.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit2.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit3.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit6.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit7.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit10.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit8.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit11.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit9.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit4.Properties.ReadOnly:=True;
    fmAddCli.cxTextEdit5.Properties.ReadOnly:=True;
    fmAddCli.cxCheckBox1.Properties.ReadOnly:=True;
    fmAddCli.cxButton1.Enabled:=False;

    fmAddCli.cxComboBox1.Properties.ReadOnly:=True;
    fmAddCli.cxComboBox2.Properties.ReadOnly:=True;
    fmAddCli.Label16.Caption:=''; //��� ��
    fmAddCli.cxComboBox1.ItemIndex:=0;  //��� �� ����
    fmAddCli.cxComboBox2.ItemIndex:=0;  //��� ������

    if GridCli.ActiveView=ViewCli then
    begin //���
      if quCli.RecordCount>0 then
      begin
        fmAddCli.cxTextEdit1.Tag:=quCliVendor.AsInteger; //���
        fmAddCli.cxTextEdit1.Text:=quCliName.AsString;   //��������
        fmAddCli.cxTextEdit2.Text:=quCliFullName.AsString; //������ ��������
        fmAddCli.cxTextEdit3.Text:=quCliINN.AsString;     //���

        fmAddCli.cxTextEdit6.Text:=quCliOKPO.AsString; //���
        fmAddCli.cxTextEdit13.Text:=quCliCodeUchet.AsString; //��� �����

        fmAddCli.cxTextEdit7.Text:=quCliGorod.AsString; //�����
        fmAddCli.cxTextEdit10.Text:=quCliPostIndex.AsString;
        fmAddCli.cxTextEdit8.Text:=quCliStreet.AsString; //�����
        fmAddCli.cxTextEdit11.Text:=quCliHouse.AsString; //�����
        fmAddCli.cxTextEdit9.Text:=quCliKvart.AsString; //�����

        fmAddCli.cxTextEdit4.Text:=quCliPhone1.AsString; //�������
        fmAddCli.cxTextEdit5.Text:=quCliPhone2.AsString; //�������������
        fmAddCli.cxTextEdit12.Text:=quCliFax.AsString;   //Fax

        if quCliUnTaxedNDS.AsInteger=0 then  fmAddCli.cxCheckBox1.Checked:=True
        else fmAddCli.cxCheckBox1.Checked:=False;

        fmAddCli.Label16.Caption:=its(quCliStatus.AsInteger); //��� ��
        fmAddCli.cxComboBox1.ItemIndex:=quCliStatusByte.AsInteger;  //��� �� ����
        fmAddCli.cxComboBox2.ItemIndex:=quCliRaion.AsInteger;  //��� ������

        fmAddCli.ShowModal;

      end;
    end else
    begin //��
      if quCli.RecordCount>0 then
      begin
        fmAddCli.cxTextEdit1.Tag:=quIPCode.AsInteger; //���
        fmAddCli.cxTextEdit1.Text:=quIPName.AsString;   //��������
        fmAddCli.cxTextEdit2.Text:=quIPFullName.AsString; //������ ��������
        fmAddCli.cxTextEdit3.Text:=quIPINN.AsString;     //���
        fmAddCli.cxTextEdit6.Text:=quIPCodeUchet.AsString; //���

        fmAddCli.cxTextEdit7.Text:=quIPGorod.AsString; //�����
        fmAddCli.cxTextEdit10.Text:=quIPPostIndex.AsString; //�����
        fmAddCli.cxTextEdit8.Text:=quIPStreet.AsString; //�����
        fmAddCli.cxTextEdit11.Text:=quIPHouse.AsString; //�����
        fmAddCli.cxTextEdit9.Text:=quIPKvart.AsString; //�����

        fmAddCli.cxTextEdit4.Text:=quIPPhone1.AsString; //�������
        fmAddCli.cxTextEdit5.Text:=quIPPhone2.AsString; //�������������
        fmAddCli.cxTextEdit12.Text:=quIPFax.AsString; //Fax
        if quIPStatus.AsInteger>0 then  fmAddCli.cxCheckBox1.Checked:=True
        else fmAddCli.cxCheckBox1.Checked:=False;

        fmAddCli.ShowModal;

      end;
    end;
  end;
end;

procedure TfmClients.cxLabel2Click(Sender: TObject);
begin
//  exit;
  acEditCli.Execute;
end;

procedure TfmClients.cxLabel4Click(Sender: TObject);
begin
  acViewCli.Execute;
end;

procedure TfmClients.acAddCliExecute(Sender: TObject);
Var iMax,iNds:INteger;
begin
  //����������
  if not CanDo('prAddCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if CommonSet.Single=0 then
  begin
    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0)and(pos('OPERZAK',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  //��� ���������
  with dmMC do
  begin
    fmAddCli.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit3.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit6.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit7.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit10.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit8.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit11.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit9.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit4.Properties.ReadOnly:=False;
    fmAddCli.cxTextEdit5.Properties.ReadOnly:=False;
    fmAddCli.cxCheckBox1.Properties.ReadOnly:=False;

    fmAddCli.cxComboBox1.Properties.ReadOnly:=False;
    fmAddCli.cxComboBox2.Properties.ReadOnly:=False;

    fmAddCli.cxButton1.Enabled:=True;

    fmAddCli.cxTextEdit1.Tag:=0; //���
    fmAddCli.cxTextEdit1.Text:='';   //��������
    fmAddCli.cxTextEdit2.Text:=''; //������ ��������
    fmAddCli.cxTextEdit3.Text:='';     //���
    fmAddCli.cxTextEdit6.Text:=''; //���
    fmAddCli.cxTextEdit13.Text:=''; //���

    fmAddCli.cxTextEdit7.Text:=''; //�����
    fmAddCli.cxTextEdit10.Text:=''; //������
    fmAddCli.cxTextEdit8.Text:=''; //�����
    fmAddCli.cxTextEdit11.Text:=''; //���
    fmAddCli.cxTextEdit9.Text:=''; //��������

    fmAddCli.cxTextEdit4.Text:=''; //�������
    fmAddCli.cxTextEdit12.Text:=''; //����
    fmAddCli.cxTextEdit5.Text:=''; //�������������
    fmAddCli.cxCheckBox1.Checked:=True;

    fmAddCli.Label16.Caption:=''; //��� ��
    fmAddCli.cxComboBox1.ItemIndex:=0;
    fmAddCli.cxComboBox2.ItemIndex:=0;

    if GridCli.ActiveView=ViewCli then
    begin //���
      fmAddCli.ShowModal;
      if fmAddCli.ModalResult=mrOk then
      begin
        iMax:=prMax('Cli')+1;
        if fmAddCli.cxCheckBox1.Checked then iNds:=0 else iNds:=1;

        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "RVendor" values (');
        quA.SQL.Add(its(iMax)+','''+fmAddCli.cxTextEdit1.Text+''',0,0,0,');
        //Vendor   Name  PayDays   PayForm   PayWaste
        quA.SQL.Add(''''','''+Copy(fmAddCli.cxTextEdit4.Text,1,16)+''','''+Copy(fmAddCli.cxTextEdit5.Text,1,16)+''','''','''','''+Copy(fmAddCli.cxTextEdit12.Text,1,16)+''',');
        // FirmName   Phone1   Phone2  Phone3   Phone4  Fax
        quA.SQL.Add(''''+ds(date)+''',0,'''+fmAddCli.cxTextEdit10.Text+''','''+Copy(fmAddCli.cxTextEdit7.Text,1,30)+''',');

        quA.SQL.Add(its(fmAddCli.cxComboBox2.ItemIndex)+','); //Raion - ��� ������

        quA.SQL.Add(''''+Copy(fmAddCli.cxTextEdit8.Text,1,30)+''','''+Copy(fmAddCli.cxTextEdit11.Text,1,6)+''',');
        quA.SQL.Add(''''','''+Copy(fmAddCli.cxTextEdit9.Text,1,4)+''','''','''','''+fmAddCli.cxTextEdit3.Text+''','''+fmAddCli.cxTextEdit2.Text+''',');
        //  Build Kvart OKPO OKONH  INN  FullName

        quA.SQL.Add(''''+fmAddCli.cxTextEdit6.Text+''','); // CodeUchet
        quA.SQL.Add(its(iNds)+','); //UnTaxedNDS
        quA.SQL.Add(its(fmAddCli.cxComboBox1.ItemIndex)+',0'); //StatusByte Status
        quA.SQL.Add(')');
        quA.ExecSQL;

        ViewCli.BeginUpdate;
        quCli.Active:=False;
        quCli.Active:=True;
        quCli.Locate('Vendor',iMax,[]);
        ViewCli.EndUpdate;
      end;
    end else
    begin //��
      fmAddCli.ShowModal;
      if fmAddCli.ModalResult=mrOk then
      begin
        iMax:=prMax('ChP')+1;
//        if fmAddCli.cxCheckBox1.Checked then iNds:=0 else iNds:=1;

        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "RIndividual" values (');
        quA.SQL.Add(its(iMax)+','''+fmAddCli.cxTextEdit1.Text+''','''','''+fmAddCli.cxTextEdit1.Text+''','''','''',');
        //Code   Name  FirmName   Familia Ima Otchestvo
        quA.SQL.Add(''''','''',0,'''','''+ds(date)+''','''','''+Copy(fmAddCli.cxTextEdit4.Text,1,16)+''','''+Copy(fmAddCli.cxTextEdit5.Text,1,16)+''','''','''','''+Copy(fmAddCli.cxTextEdit12.Text,1,16)+''',');
        // VidDok SerPasp  NumberPasp   KemPasp  DatePasp Country  Phone1   Phone2  Phone3   Phone4  Fax
        quA.SQL.Add(''''','''+fmAddCli.cxTextEdit10.Text+''','''+Copy(fmAddCli.cxTextEdit7.Text,1,30)+''',0,'''+Copy(fmAddCli.cxTextEdit8.Text,1,30)+''','''+Copy(fmAddCli.cxTextEdit11.Text,1,6)+''',');
        // EMail  PostIndex   Gorod  Raion   Street  House
        quA.SQL.Add(''''','''+Copy(fmAddCli.cxTextEdit9.Text,1,4)+''','''+fmAddCli.cxTextEdit3.Text+''','''','''','''+ds(date)+''','''+fmAddCli.cxTextEdit2.Text+''',');
        //  Build Kvart INN  NStrach  NumberSvid   DateSvid   FullName

        quA.SQL.Add(''''+ds(date)+''','''+fmAddCli.cxTextEdit6.Text+''',0)');
        //DateBorn   CodeUchet Status
        quA.ExecSQL;

        ViewIP.BeginUpdate;
        quIp.Active:=False;
        quIp.Active:=True;
        quIp.Locate('Code',iMax,[]);
        ViewIP.EndUpdate;
      end;

    end;
  end;
end;

procedure TfmClients.acDelCliExecute(Sender: TObject);
begin
  if not CanDo('prDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if CommonSet.Single=0 then
  begin
    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  //���� ����� �� ���������
{  with dmMC do
  begin
    if GridCli.ActiveView=ViewCli then
    begin //���
      if quCli.RecordCount>0 then
      begin
        if MessageDlg('�� ������������� ������ ������� '+quCliName.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "RVendor" where Vendor='+its(quCliVendor.AsInteger));
          quD.ExecSQL;

          ViewCli.BeginUpdate;
          quCli.Active:=False;
          quCli.Active:=True;
          ViewCli.EndUpdate;
        end;
      end;
    end else
    begin //��
      if quIP.RecordCount>0 then
      begin
        if MessageDlg('�� ������������� ������ ������� '+quIPName.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "RIndividual" where Code='+its(quIPCode.AsInteger));
          quD.ExecSQL;

          ViewIP.BeginUpdate;
          quIp.Active:=False;
          quIp.Active:=True;
          ViewIP.EndUpdate;
        end;
      end;
    end;
  end;}
end;

procedure TfmClients.cxLabel1Click(Sender: TObject);
begin
  acAddCli.Execute;
end;

procedure TfmClients.cxLabel3Click(Sender: TObject);
begin
  acDelCli.Execute;
end;

procedure TfmClients.acFindCliExecute(Sender: TObject);
Var sp:String;
begin
  //����� ��������
  sp:=cxTextEdit1.Text;
  with dmMC do
  begin




  end;
end;

procedure TfmClients.FormShow(Sender: TObject);
begin
  if bopen then close
  else
  begin
   cxTextEdit1.SetFocus;
   cxTextEdit1.SelectAll;
  end;
  If CommonSet.Single=1 then ViewCliPhone4.Visible:=False;
end;

procedure TfmClients.cxTextEdit1PropertiesChange(Sender: TObject);

begin

   {   if quFCli.RecordCount>0 then
      begin
        quCli.Locate('Vendor',quFCliVendor.AsInteger,[]);
        LevelCli.Active:=True;
        LevelIP.Active:=False;
      end else
      begin
        quFIP.Active:=False;
        quFIP.SQL.Clear;
        quFIP.SQL.Add('select top 3 Code,Name from "RIndividual"');
        quFIP.SQL.Add('where Name like ''%'+S+'%''');
        quFIP.SQL.Add('and Name not like ''%��%''');

        quFIP.Active:=True;
        if quFIP.RecordCount>0 then
        begin
          quIP.Locate('Code',quFIPCode.AsInteger,[]);
          LevelCli.Active:=False;
          LevelIP.Active:=True;
        end;
      end;

    end;     }

end;

procedure TfmClients.cxButton3Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmClients.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//Var S:String;
begin
  if (Key=38)or(Key=40)then
  begin
    GridFindCli.SetFocus;
//    CardsView.Controller.FocusRecord(cardsview.DataController.FocusedRowIndex,True);
  end;
end;

procedure TfmClients.cxLabel5Click(Sender: TObject);
Var sInn,sName:String;
begin
  with dmMc do
  begin
    sInn:='';
    sName:='';
    if GridCli.ActiveView=ViewCli then
    begin //���
      if quCli.RecordCount>0 then
      begin
        sName:=quCliFullName.AsString; //������ ��������
        sInn:=quCliINN.AsString;
      end;
    end else
    begin //��
      if quIP.RecordCount>0 then
      begin
        sName:=quIPFullName.AsString; //������ ��������
        sInn:=quIPINN.AsString;     //���
      end;
    end;

    if sInn>'' then
    begin
      fmCliPars:=TfmCliPars.Create(Application);
      quCliPars.Active:=False;
      quCliPars.ParamByName('SINN').AsString:=sInn;
      quCliPars.Active:=True;
      if quCliPars.RecordCount>0 then
      begin
        with fmCliPars do
        begin

          cxTextEdit1.Text:=sName;
          cxTextEdit1.Tag:=1;
          cxTextEdit3.Text:=sInn;
          cxTextEdit6.Text:=quCliParsNAMEOTP.AsString;
          cxTextEdit7.Text:=quCliParsADROTPR.AsString;
          cxTextEdit11.Text:=quCliParsRSch.AsString;
          cxTextEdit12.Text:=quCliParsBank.AsString;
          cxTextEdit13.Text:=quCliParsKSch.AsString;
          cxTextEdit14.Text:=quCliParsBik.AsString;
          cxTextEdit4.Text:=quCliParsPRETCLI.AsString;
          cxTextEdit2.Text:=quCliParsDOGNUM.AsString;
          cxDateEdit1.Date:=quCliParsDOGDATE.AsDateTime;

        end;
      end else
      begin
        with fmCliPars do
        begin
          cxTextEdit1.Text:=sName;
          cxTextEdit1.Tag:=0;
          cxTextEdit3.Text:=sInn;
          cxTextEdit6.Text:='';
          cxTextEdit7.Text:='';
          cxTextEdit11.Text:='';
          cxTextEdit12.Text:='';
          cxTextEdit13.Text:='';
          cxTextEdit14.Text:='';
          cxTextEdit4.Text:='';
          cxTextEdit2.Text:='';
          cxDateEdit1.Date:=date;

        end;
      end;
      quCliPars.Active:=False;

      fmCliPars.ShowModal;
      if fmCliPars.ModalResult=mrOk then
      begin //���������
        with fmCliPars do
        begin
          if cxTextEdit1.Tag=0 then
          begin // ��������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "A_CLIENTS" values (');
            quA.SQL.Add(''''+sInn+''',');//INN
            quA.SQL.Add(''''+cxTextEdit6.Text+''',');//NAMEOTP
            quA.SQL.Add(''''+cxTextEdit7.Text+''',');//ADROTPR
            quA.SQL.Add(''''+cxTextEdit11.Text+''',');//RSch
            quA.SQL.Add(''''+cxTextEdit13.Text+''',');//KSch
            quA.SQL.Add(''''+cxTextEdit12.Text+''',');//Bank
            quA.SQL.Add(''''+cxTextEdit14.Text+''',');//Bik
            quA.SQL.Add(''''+cxTextEdit4.Text+''',');  //PRETCLI
            quA.SQL.Add(''''+cxTextEdit2.Text+''',');  //DOGNUM
            quA.SQL.Add(''''+ds(cxDateEdit1.Date)+'''');  //PRETCLI
            quA.SQL.Add(')');
            quA.ExecSQL;

          end;
          if cxTextEdit1.Tag=1 then
          begin // �������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "A_CLIENTS" Set ');
            quE.SQL.Add('NAMEOTP='''+cxTextEdit6.Text+''',');
            quE.SQL.Add('ADROTPR='''+cxTextEdit7.Text+''',');
            quE.SQL.Add('RSch='''+cxTextEdit11.Text+''',');
            quE.SQL.Add('KSch='''+cxTextEdit13.Text+''',');
            quE.SQL.Add('Bank='''+cxTextEdit12.Text+''',');
            quE.SQL.Add('Bik='''+cxTextEdit14.Text+''',');
            quE.SQL.Add('PRETCLI='''+cxTextEdit4.Text+''',');
            quE.SQL.Add('DOGNUM='''+cxTextEdit2.Text+''',');
            quE.SQL.Add('DOGDATE='''+ds(cxDateEdit1.Date)+'''');
            quE.SQL.Add('where INN='''+sInn+'''');
            quE.ExecSQL;
          end;//}
        end;
      end;

      fmCliPars.Release;
    end;
  end;
end;

procedure TfmClients.acRecalcAssExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk:String;
    iC,TypeCli,CodeCli:INteger;
    dCurD:TDateTime;
begin
  //����������� ���������� �����������
  with dmMC do
  with dmMT do
  begin
//    TypeCli:=0;
    CodeCli:=0;
    if GridCli.ActiveView=fmClients.ViewCli then
    begin
      TypeCli:=1;
      if quCli.RecordCount>0 then CodeCli:=quCliVendor.AsInteger;
    end else
    begin
      TypeCli:=2;
      if quIP.RecordCount>0 then CodeCli:=quIPCode.AsInteger;
    end;

    if (CodeCli>0) and (TypeCli>0) then
    begin
      fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
      fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
      fmPeriod1.Label3.Caption:='';
      fmPeriod1.ShowModal;
      if fmPeriod1.ModalResult=mrOk then
      begin
        CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
        CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
        DateB:=Trunc(CommonSet.DateBeg);
        DateE:=Trunc(CommonSet.DateEnd);

        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show;
        with fmSt do
        begin
          Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
          try
            Memo1.Lines.Add(fmt+'  ������ ...');
            ptCliGoods.Active:=False; ptCliGoods.Active:=True;

            ptCliGoods.CancelRange;
            ptCliGoods.SetRange([TypeCli,CodeCli],[TypeCli,CodeCli]);

            ptCliGoods.First;  iC:=0;
            while not ptCliGoods.Eof do
            begin
              ptCliGoods.delete;
              inc(iC);
              if iC mod 500 = 0 then
              begin
                delay(100);
                StatusBar1.Panels[0].Text:=its(iC);
              end;
            end;

            delay(100);

            Memo1.Lines.Add(fmt+'  ������� ...');

            ptTTNIn.Active:=False; ptTTNIn.Active:=True;
            ptTTnIn.IndexFieldNames:='DateInvoice;Depart;IndexPost;CodePost;Number';

            ptInLn.Active:=False; ptINLn.Active:=True;

            for iCurD:=DateB to DateE do
            begin
              Memo1.Lines.Add(fmt+'   ���� - '+FormatDateTime('dd.mm.yyyy',iCurD));
              dCurD:=iCurD;
              ptTTNIn.CancelRange;
              ptTTNIn.SetRange([dCurD],[dCurD]);
              ptTTNIn.First;  iC:=0;
              while not ptTTNIn.Eof do
              begin
                if (ptTTNInIndexPost.AsInteger=TypeCli) and (ptTTNInCodePost.AsInteger=CodeCli) then
                begin
                  ptInLn.CancelRange;
                  ptInLn.SetRange([ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString],[ptTTNInDepart.AsInteger,ptTTNInDateInvoice.AsDateTime,ptTTNInNumber.AsString]);
                  ptInLn.First;
                  while not ptInLn.Eof do
                  begin
                    if ptCliGoods.FindKey([ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,ptInLnCodeTovar.AsInteger]) then
                    begin
                      ptCliGoods.Edit;
                      ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                      ptCliGoods.Post;
                    end else
                    begin
                      ptCliGoods.Append;
                      ptCliGoodsGoodsID.AsInteger:=ptInLnCodeTovar.AsInteger;
                      ptCliGoodsClientObjTypeID.AsInteger:=ptTTNInIndexPost.AsInteger;
                      ptCliGoodsClientID.AsInteger:=ptTTNInCodePost.AsInteger;
                      ptCliGoodsxDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                      ptCliGoods.Post;
                    end;
                    ptInLn.Next;
                  end;
                end;
                ptTTNIn.Next; inc(iC);
                if iC mod 100 = 0 then
                begin
                  StatusBar1.Panels[0].Text:=its(iC);
                  delay(100);
                end;
              end;
            end;
          finally
            ptTTNIn.Active:=False;
            ptInLn.Active:=False;
            ptCliGoods.Active:=False;

            Memo1.Lines.Add(fmt+'������� ��������.');
            fmSt.cxButton1.Enabled:=True;
          end;
        end;

      end;
    end;
  end;
end;

procedure TfmClients.ViewCliCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var sType:String;
    i:Integer;
begin
 //
  if AViewInfo.GridRecord.Selected then
  begin
    ACanvas.Canvas.Brush.Color := $00FFB1A4; //�������� ��������� �����
    exit;
  end;
  sType:='';
  for i:=0 to ViewCli.ColumnCount-1 do
  begin
    if ViewCli.Columns[i].Name='ViewCliRaion' then
    begin
      sType:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;
  if sType<>'���' then
  begin
    ACanvas.Canvas.Brush.Color := $00FFD1A4; //�������
  end;
end;

procedure TfmClients.acGrafPostExecute(Sender: TObject);
Var iCli:Integer;
    vItem:tCxschedulerStorageResourceItem;
    dDate:TDateTime;
begin
  with dmMc do
  with dmMT do
  begin
    if quCli.RecordCount>0 then
    begin
      fmGrafPost:=TfmGrafPost.Create(Application);

      iCli:=quCliStatus.AsInteger;
      fmGrafPost.Label2.Caption:=quCliName.AsString;
      fmGrafPost.Label2.Tag:=iCli;

      fmGrafPost.Storage1.BeginUpdate;
      fmGrafPost.Storage1.Clear;
      fmGrafPost.Storage1.Resources.Items.BeginUpdate;
      fmGrafPost.Storage1.Resources.Items.Clear;

      vItem:=fmGrafPost.Storage1.Resources.Items.Add;
      vItem.ResourceID:=1;
      vItem.Index:=0;
      vItem.Name:='';

      if ptGrafPost.Active=False then ptGrafPost.Active:=True;
      ptGrafPost.CancelRange;
      ptGrafPost.SetRange([iCli],[iCli]);
      ptGrafPost.First;
      while not ptGrafPost.Eof do
      begin
        with fmGrafPost.Storage1.createEvent do
        begin
          dDate:=ptGrafPostIDATEP.AsInteger;
          Start :=dDate;
          Finish :=dDate+0.001;
          Caption :='';
          ResourceID:=1 ;
          ParentId:=ptGrafPostIDATEP.AsInteger;
          fmGrafPost.Storage1.PostEvents;
        end;
        ptGrafPost.Next;
      end;

      fmGrafPost.Storage1.Resources.Items.EndUpdate;
      fmGrafPost.Storage1.EndUpdate;

      fmGrafPost.ShowModal;
      fmGrafPost.Release;
    end;
  end;  
end;

procedure TfmClients.cxLabel6Click(Sender: TObject);
begin
  acGrafPost.Execute;
end;

procedure TfmClients.cxButton4Click(Sender: TObject);
begin
  if GridCli.ActiveView=ViewCli then
  begin //���
    prNExportExel5(ViewCli);
  end else
  begin
    prNExportExel5(ViewIP);
  end;
end;

procedure TfmClients.cxCheckBox1Click(Sender: TObject);
{var S:String;
    n: integer;  }
begin
{
  n:=Length(cxTextEdit1.Text);
  s:=AnsiUpperCase(cxTextEdit1.Text);
 if (cxCheckBox1.Checked = true) then
 begin
   with dmMC do
   begin
    fmClients.ViewCli.BeginUpdate;
    dsquCli.DataSet:=nil;
    try
      quCli.Active:=False;
      quCli.SQL.Clear;
      quCli.SQL.Add('select * from RVendor ');
      quCli.SQL.Add('order by Name asc');
      quCli.Active:=True;
    finally
      dsquCli.DataSet:=quCli;
      fmClients.ViewCli.EndUpdate;
    end;
    GridCli.SetFocus;
    quCli.Locate('Vendor',quFindCliVendor.AsInteger,[]);
    if (n>1) then
    begin
      fmClients.ViewFindCli.BeginUpdate;
      dsquFindCli.DataSet:=nil;
      try
        quFindCli.Active:=False;
        quFindCli.SQL.Clear;
        quFindCli.SQL.Add('select * from RVendor ');
        quFindCli.SQL.Add('where Name like ''%'+S+'%''');
        quFindCli.SQL.Add('order by Name asc');
        quFindCli.Active:=True;
      finally
        dsquFindCli.DataSet:=quFindCli;
        fmClients.ViewFindCli.EndUpdate;
      end;
      quFindCli.Locate('Vendor',quCliVendor.AsInteger,[]);
    end;
   end;
 end
 else
 begin
     with dmMC do
     begin
      fmClients.ViewCli.BeginUpdate;
      dsquCli.DataSet:=nil;
      try
        quCli.Active:=False;
        quCli.SQL.Clear;
        quCli.SQL.Add('select * from RVendor where ');
        quCli.SQL.Add('Name not like ''��'+'%''');
        quCli.SQL.Add('and Name not like ''� ��'+'%''');
        quCli.SQL.Add('and Status not like ''-'+'%''');
        quCli.SQL.Add('order by Name asc');
        quCli.Active:=True;
      finally
        dsquCli.DataSet:=quCli;
        fmClients.ViewCli.EndUpdate;
      end;
      GridCli.SetFocus;
      quCli.Locate('Vendor',quFindCliVendor.AsInteger,[]);
      if (n>1) then
      begin
        fmClients.ViewFindCli.BeginUpdate;
        dsquFindCli.DataSet:=nil;
        try
          quFindCli.Active:=False;
          quFindCli.SQL.Clear;
          quFindCli.SQL.Add('select * from RVendor where Name like ''%'+S+'%''');
          quFindCli.SQL.Add('and Name not like ''��'+'%''');
          quFindCli.SQL.Add('and Name not like ''� ��'+'%''');
          quFindCli.SQL.Add('and Status not like ''-'+'%''');
          quFindCli.SQL.Add('order by Name asc');
          quFindCli.Active:=True;
        finally
          dsquFindCli.DataSet:=quFindCli;
          fmClients.ViewFindCli.EndUpdate;
        end;
        quFindCli.Locate('Vendor',quCliVendor.AsInteger,[]);
      end;
     end;
 end;   }
end;

procedure TfmClients.cxTextEdit1KeyPress(Sender: TObject; var Key: Char);
Var S:String;
begin
if (ord(key)=13) then
 begin
   // if bOpen then exit;
    s:=AnsiUpperCase(cxTextEdit1.Text);
    if Length(S)<1 then exit;
 {   if  (cxCheckBox1.Checked = false) then                      //������ �����
    begin
      with dmMC do
      begin
        fmClients.ViewFindCli.BeginUpdate;
        dsquFindCli.DataSet:=nil;
        try
          quFindCli.Active:=False;
          quFindCli.SQL.Clear;
          quFindCli.SQL.Add('select * from RVendor where Name like ''%'+S+'%''');
          quFindCli.SQL.Add('and Name not like ''��'+'%''');
          quFindCli.SQL.Add('and Name not like ''� ��'+'%''');
          quFindCli.SQL.Add('and Status not like ''-'+'%''');
          quFindCli.SQL.Add('order by Name asc');
          quFindCli.Active:=True;
        finally
          dsquFindCli.DataSet:=quFindCli;
          if quFindCli.RecordCount>0 then GridFindCli.SetFocus;
          fmClients.ViewFindCli.EndUpdate;
        end;
      end;
    end
    else
    begin    }
      with dmMC do
      begin
        fmClients.ViewFindCli.BeginUpdate;
        dsquFindCli.DataSet:=nil;
        try
          quFindCli.Active:=False;
          quFindCli.SQL.Clear;
          quFindCli.SQL.Add('select * from RVendor ');
          quFindCli.SQL.Add('where Name like ''%'+S+'%''');
          quFindCli.SQL.Add('order by Name asc');
          quFindCli.Active:=True;
        finally
          dsquFindCli.DataSet:=quFindCli;
          if quFindCli.RecordCount>0 then GridFindCli.SetFocus;
          fmClients.ViewFindCli.EndUpdate;
        end;
      end;
  //end;
  end;
end;

procedure TfmClients.ViewCliKeyPress(Sender: TObject; var Key: Char);
begin
if ((getasynckeystate(VK_CONTROL)<>0) and (getasynckeystate(VK_RETURN)<>0)) then      //������� ������� ������� ctrl+enter
ModalResult:=mrOk;
end;

procedure TfmClients.ViewFindCliKeyPress(Sender: TObject; var Key: Char);
begin
 if (ord(key)=13) then
 begin
   with dmMC do
   begin
     fmClients.ViewCli.BeginUpdate;
     GridCli.SetFocus;
     quCli.Locate('Vendor',quFindCliVendor.AsInteger,[]);
     fmClients.ViewCli.EndUpdate;
   end;
 end;
end;

procedure TfmClients.ViewFindCliDblClick(Sender: TObject);
begin
with dmMC do
 begin
 fmClients.ViewCli.BeginUpdate;
 GridCli.SetFocus;
 quCli.Locate('Vendor',quFindCliVendor.AsInteger,[]);
 fmClients.ViewCli.EndUpdate;
 end;
end;

procedure TfmClients.ViewFindCliCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin

 if AViewInfo.GridRecord.Selected then
 ACanvas.Canvas.Brush.Color := $00FFB1A4; //�������� ��������� �����

end;

procedure TfmClients.N4Click(Sender: TObject);
begin
with dmMC do
  begin
    fmClients.ViewCli.BeginUpdate;
    dsquCli.DataSet:=nil;
    try
      quCli.Active:=False;
      quCli.SQL.Clear;
      quCli.SQL.Add('select * from "RVendor" v left join A_CLIVOZ cv on v.Status=cv.IDCB ');
      quCli.SQL.Add('left join A_TypeVoz t on cv.IdTypeVoz = t.ID ');
 {     quCli.SQL.Add('where Name not like ''��'+'%'' and Name not like ''� ��'+'%''');
      quCli.SQL.Add('and Status not like ''-'+'%''');   }
      quCli.SQL.Add('order by Name asc');
      quCli.Active:=True;
    finally
      dsquCli.DataSet:=quCli;
      fmClients.ViewCli.EndUpdate;
    end;
  end;
end;

procedure TfmClients.acEditLicExecute(Sender: TObject);
Var iType,iCli:Integer;
begin
 //������������� ��������
  with dmMC do
  with dmMT do
  begin
    if quCli.RecordCount>0 then
    begin
      try
        iType:=1;
        iCli:=quCliVendor.AsInteger;

        fmCliLic:=tfmCliLic.Create(Application);

        if ptCliLic.Active=False then ptCliLic.Active:=True;
        ptCliLic.CancelRange;
        ptCliLic.SetRange([iType,iCli],[iType,iCli]);

        CloseTe(fmCliLic.teLi);
        fmCliLic.Tag:=iCli;

        ptCliLic.First;
        while not ptCliLic.Eof do
        begin
          with fmCliLic do
          begin
            teLi.Append;
            teLiITYPE.AsInteger:=1;
            teLiICLI.AsInteger:=iCli;
            teLiIDATEB.AsInteger:=ptCliLicIDATEB.AsInteger;
            teLiDDATEB.AsDateTime:=ptCliLicDDATEB.AsDateTime;
            teLiIDATEE.AsInteger:=ptCliLicIDATEE.AsInteger;
            teLiDDATEE.AsDateTime:=ptCliLicDDATEE.AsDateTime;
            teLiSER.AsString:=OemToAnsiConvert(ptCliLicSER.AsString);
            teLiSNUM.AsString:=OemToAnsiConvert(ptCliLicSNUM.AsString);
            teLiORGAN.AsString:=OemToAnsiConvert(ptCliLicORGAN.AsString);
            teLiIDCB.AsInteger:=0;
            teLi.Post;
          end;

          ptCliLic.Next;
        end;

        fmCliLic.ShowModal;
      finally
        fmCliLic.Release;
      end;
    end;
  end;
end;

end.
