unit AddDoc3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxCheckBox,
  FR_Class, FR_DSet, FR_DBSet;

type
  TfmAddDoc3 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label5: TLabel;
    Label12: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpecAc: TClientDataSet;
    dstaSpecIn: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    amDocAc: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    taSpecAcCodeTovar: TIntegerField;
    taSpecAcCodeEdIzm: TSmallintField;
    taSpecAcBarCode: TStringField;
    taSpecAcNDSProc: TFloatField;
    taSpecAcKol: TFloatField;
    taSpecAcNum: TIntegerField;
    Edit1: TEdit;
    acReadBar: TAction;
    acReadBar1: TAction;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit10: TcxDateEdit;
    GridDoc3: TcxGrid;
    ViewDoc3: TcxGridDBTableView;
    LevelDoc3: TcxGridLevel;
    acSetPrice: TAction;
    acZPrice: TAction;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    frRepDAC: TfrReport;
    frtaSpecAc: TfrDBDataSet;
    acPrintDoc: TAction;
    taSpecAcName: TStringField;
    taSpecAcCena: TFloatField;
    taSpecAcNewCena: TFloatField;
    taSpecAcRazn: TFloatField;
    taSpecAcProc: TFloatField;
    taSpecAcSumCena: TFloatField;
    taSpecAcSumNewCena: TFloatField;
    taSpecAcSumRazn: TFloatField;
    ViewDoc3Num: TcxGridDBColumn;
    ViewDoc3CodeTovar: TcxGridDBColumn;
    ViewDoc3CodeEdIzm: TcxGridDBColumn;
    ViewDoc3BarCode: TcxGridDBColumn;
    ViewDoc3NDSProc: TcxGridDBColumn;
    ViewDoc3Kol: TcxGridDBColumn;
    ViewDoc3Name: TcxGridDBColumn;
    ViewDoc3Cena: TcxGridDBColumn;
    ViewDoc3NewCena: TcxGridDBColumn;
    ViewDoc3Razn: TcxGridDBColumn;
    ViewDoc3Proc: TcxGridDBColumn;
    ViewDoc3SumCena: TcxGridDBColumn;
    ViewDoc3SumNewCena: TcxGridDBColumn;
    ViewDoc3SumRazn: TcxGridDBColumn;
    taSpecAcFullName: TStringField;
    acPrintCen: TAction;
    taSpecAcRemn: TFloatField;
    ViewDoc3Remn: TcxGridDBColumn;
    cxLabel5: TcxLabel;
    cxButton8: TcxButton;
    acPrintTermo: TAction;
    acPostView: TAction;
    taSpecAcNac1: TFloatField;
    taSpecAcNac2: TFloatField;
    taSpecAcNac3: TFloatField;
    ViewDoc3Nac1: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    acAddDiscStop: TAction;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc3DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc3DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure ViewDoc3Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel3Click(Sender: TObject);
    procedure ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure ViewDoc3DblClick(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure taSpecAcKolChange(Sender: TField);
    procedure taSpecAcCenaChange(Sender: TField);
    procedure taSpecAcNewCenaChange(Sender: TField);
    procedure taSpecAcSumCenaChange(Sender: TField);
    procedure taSpecAcSumNewCenaChange(Sender: TField);
    procedure acPrintCenExecute(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acPrintTermoExecute(Sender: TObject);
    procedure acPostViewExecute(Sender: TObject);
    procedure acAddDiscStopExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prSetNac;
  end;

var
  fmAddDoc3: TfmAddDoc3;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, DocsIn, sumprops, DocsAC,
  QuantMess, u2fdk, MT, Move, MainMCryst, Prib1;

{$R *.dfm}

procedure TfmAddDoc3.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc3.Align:=alClient;
  ViewDoc3.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  if CommonSet.Single=1 then
  begin
    acAddDiscStop.Visible:=True;
    acAddDiscStop.Enabled:=True;
  end else
  begin
    acAddDiscStop.Visible:=False;
    acAddDiscStop.Enabled:=False;
  end;
end;

procedure TfmAddDoc3.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc3.ViewDoc3DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if iDirect=4 then  Accept:=True;
  if iDirect=9 then  Accept:=True;
end;

procedure TfmAddDoc3.ViewDoc3DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//

  if iDirect=4 then
  begin
    iDirect:=0; //������ ���������

    iCo:=fmCards.CardsView.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        iMax:=1;
        fmAddDoc3.taSpecAc.First;
        if not fmAddDoc3.taSpecAc.Eof then
        begin
          fmAddDoc3.taSpecAc.Last;
          iMax:=fmAddDoc3.taSpecAcNum.AsInteger+1;
        end;

        with dmMC do
        begin
          ViewDoc3.BeginUpdate;
          try

          for i:=0 to fmCards.CardsView.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmCards.CardsView.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmCards.CardsView.Columns[j].Name='CardsViewID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            quFindC1.Active:=False;
            quFindC1.SQL.Clear;
            quFindC1.SQL.Add('SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
            quFindC1.SQL.Add('FROM "Goods"');
            quFindC1.SQL.Add('where "Goods"."ID"='+INtToStr(iNum));
            quFindC1.Active:=True;

            if quFindC1.RecordCount=1 then
            begin
              taSpecAc.Append;
              taSpecAcNum.AsInteger:=iMax;
              taSpecAcCodeTovar.AsInteger:=quFindC1ID.AsInteger;
              taSpecAcCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
              taSpecAcBarCode.AsString:=quFindC1BarCode.AsString;
              taSpecAcNDSProc.AsFloat:=quFindC1NDS.AsFloat;
              taSpecAcKol.AsFloat:=1;
              taSpecAcName.AsString:=quFindC1Name.AsString;
              taSpecAcFullName.AsString:=quFindC1FullName.AsString;
              taSpecAcCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcRazn.AsFloat:=0;
              taSpecAcProc.AsFloat:=0;
              taSpecAcSumCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcSumNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcSumRazn.AsFloat:=0;
              taSpecAc.Post;

              inc(iMax);
            end;
          end;

          finally
            ViewDoc3.EndUpdate;
          end;
        end;
      end;
    end;
  end;
  if iDirect=9 then
  begin
    iDirect:=0; //������ ���������

    iCo:=fmPrib1.ViewPrib1.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = 3 then
      begin
        iMax:=1;
        fmAddDoc3.taSpecAc.First;
        if not fmAddDoc3.taSpecAc.Eof then
        begin
          fmAddDoc3.taSpecAc.Last;
          iMax:=fmAddDoc3.taSpecAcNum.AsInteger+1;
        end;

        with dmMC do
        begin
          ViewDoc3.BeginUpdate;
          try

          for i:=0 to fmPrib1.ViewPrib1.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmPrib1.ViewPrib1.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmPrib1.ViewPrib1.Columns[j].Name='ViewPrib1IdCode1' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ���

            quFindC1.Active:=False;
            quFindC1.SQL.Clear;
            quFindC1.SQL.Add('SELECT "Goods"."ID", "Goods"."Name","Goods"."FullName","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
            quFindC1.SQL.Add('FROM "Goods"');
            quFindC1.SQL.Add('where "Goods"."ID"='+INtToStr(iNum));
            quFindC1.Active:=True;

            if quFindC1.RecordCount=1 then
            begin
              taSpecAc.Append;
              taSpecAcNum.AsInteger:=iMax;
              taSpecAcCodeTovar.AsInteger:=quFindC1ID.AsInteger;
              taSpecAcCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
              taSpecAcBarCode.AsString:=quFindC1BarCode.AsString;
              taSpecAcNDSProc.AsFloat:=quFindC1NDS.AsFloat;
              taSpecAcKol.AsFloat:=1;
              taSpecAcName.AsString:=quFindC1Name.AsString;
              taSpecAcFullName.AsString:=quFindC1FullName.AsString;
              taSpecAcCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcRazn.AsFloat:=0;
              taSpecAcProc.AsFloat:=0;
              taSpecAcSumCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcSumNewCena.AsFloat:=quFindC1Cena.AsFloat;
              taSpecAcSumRazn.AsFloat:=0;
              taSpecAc.Post;

              inc(iMax);
            end;
          end;

          finally
            ViewDoc3.EndUpdate;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfmAddDoc3.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc3.ViewDoc3Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Kol' then iCol:=1;  //���-��
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Cena' then iCol:=2;  //������ ����
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3NewCena' then iCol:=3;  //����� ����
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3SumCena' then iCol:=4;   //������ �����
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3SumNewCena' then iCol:=5;   //����� ����� �����
end;

procedure TfmAddDoc3.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc3.SetFocus;
end;

procedure TfmAddDoc3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      ViewDoc3.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    ViewDoc3.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  end;
end;

procedure TfmAddDoc3.cxLabel3Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc3.ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
//    rK:Real;
//    iM:Integer;
//    Sm:String;
    bAdd:Boolean;

begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        fmFindC.ViewFind.BeginUpdate;
        dsquFindC.DataSet:=nil;
        try
          quFindC.Active:=False;
          quFindC.SQL.Clear;
          quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
          quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
          quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
          quFindC.SQL.Add('FROM "Goods"');
          quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
          quFindC.SQL.Add('where "Goods"."ID" ='+inttostr(iCode));
          quFindC.Active:=True;
        finally
          dsquFindC.DataSet:=quFindC;
          fmFindC.ViewFind.EndUpdate;
        end;
        if CountRec1(quFindC) then
        begin
          bAdd:=True;
          if quFindCStatus.AsInteger>100 then
          begin
            bAdd:=False;
            if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
          end;
          if bAdd then
          begin
              taSpecAc.Edit;
              taSpecAcCodeTovar.AsInteger:=iCode;
              taSpecAcCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
              taSpecAcBarCode.AsString:=quFindCBarCode.AsString;
              taSpecAcNDSProc.AsFloat:=quFindCNDS.AsFloat;
              taSpecAcKol.AsFloat:=1;
              taSpecAcName.AsString:=quFindCName.AsString;
              taSpecAcCena.AsFloat:=quFindCCena.AsFloat;
              taSpecAcNewCena.AsFloat:=quFindCCena.AsFloat;
              taSpecAcRazn.AsFloat:=0;
              taSpecAcProc.AsFloat:=0;
              taSpecAcSumCena.AsFloat:=quFindCCena.AsFloat;
              taSpecAcSumNewCena.AsFloat:=quFindCCena.AsFloat;
              taSpecAcSumRazn.AsFloat:=0;
              taSpecAcFullName.AsString:=quFindCFullName.AsString;
              taSpecAc.Post;
          end;
          GridDoc3.SetFocus;
          ViewDoc3Kol.Focused:=True;

        end;
        prSetNac;
      end;
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Name' then
      begin
        try
          sName:=VarAsType(AEdit.EditingValue, varString);
        except
          sName:='';
        end;
        if (sName>'')and(Length(sName)>=3) then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT "Goods"."ID", "Goods"."Name", "Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
            quFindC.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."GoodsGroupID","Goods"."SubGroupID",');
            quFindC.SQL.Add('"SubGroup"."Name" as SubGroupName');
            quFindC.SQL.Add('FROM "Goods"');
            quFindC.SQL.Add('left join "SubGroup" on "SubGroup"."ID"="Goods"."GoodsGroupID"');
            quFindC.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if CountRec1(quFindC) then
            begin
              bAdd:=True;
              if quFindCStatus.AsInteger>100 then
              begin
                bAdd:=False;
                if MessageDlg('�������� ����� ��������� � ��������������. ����������?', mtConfirmation, [mbYes, mbNo], 0) = 3 then bAdd:=True;
              end;
              if bAdd then
              begin
                  taSpecAc.Edit;
                  taSpecAcCodeTovar.AsInteger:=quFindCID.AsInteger;
                  taSpecAcCodeEdIzm.AsInteger:=quFindCEdIzm.AsInteger;
                  taSpecAcBarCode.AsString:=quFindCBarCode.AsString;
                  taSpecAcNDSProc.AsFloat:=quFindCNDS.AsFloat;
                  taSpecAcKol.AsFloat:=1;
                  taSpecAcName.AsString:=quFindCName.AsString;
                  taSpecAcCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecAcNewCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecAcRazn.AsFloat:=0;
                  taSpecAcProc.AsFloat:=0;
                  taSpecAcSumCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecAcSumNewCena.AsFloat:=quFindCCena.AsFloat;
                  taSpecAcSumRazn.AsFloat:=0;
                  taSpecAcFullName.AsString:=quFindCFullName.AsString;
                  taSpecAc.Post;
              end;
              GridDoc3.SetFocus;
              ViewDoc3Kol.Focused:=True;
            end;
          end;
        end;
        prSetNac;
      end;
    end else
      if (ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3CodeTovar') or (ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3CodeTara') then
        if fTestKey(Key)=False then
          if taSpecAc.State in [dsEdit,dsInsert] then taSpecAc.Cancel;
  end;


end;

procedure TfmAddDoc3.ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var // Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
    if (ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Name') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if (sName>'')and(Length(sName)>=3) then
      begin
        quFindC1.Active:=False;
        quFindC1.SQL.Clear;
        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status","Goods"."V02"');
        quFindC1.SQL.Add('FROM "Goods"');
        quFindC1.SQL.Add('where "Goods"."Name" like ''%'+sName+'%'''); //������� ������� ������� (upper) �� �������� �� ��������� ������ (�,� � �.�.)
        quFindC1.Active:=True;

        if quFindC1.RecordCount=1 then //����� ���� ������
        begin
          taSpecAc.Edit;
          taSpecAcCodeTovar.AsInteger:=quFindC1ID.AsInteger;
          taSpecAcCodeEdIzm.AsInteger:=quFindC1EdIzm.AsInteger;
          taSpecAcBarCode.AsString:=quFindC1BarCode.AsString;
          taSpecAcNDSProc.AsFloat:=quFindC1NDS.AsFloat;
          taSpecAcKol.AsFloat:=1;
          taSpecAcName.AsString:=quFindC1Name.AsString;
          taSpecAcFullName.AsString:=quFindC1FullName.AsString;
          taSpecAcCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecAcNewCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecAcRazn.AsFloat:=0;
          taSpecAcProc.AsFloat:=0;
          taSpecAcSumCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecAcSumNewCena.AsFloat:=quFindC1Cena.AsFloat;
          taSpecAcSumRazn.AsFloat:=0;
          taSpecAc.Post;

          AEdit.SelectAll;
          ViewDoc3Name.Options.Editing:=False;
          ViewDoc3Name.Focused:=True;
          Key:=#0;
        end;//}
      end;
    end;
  end;//}
end;

procedure TfmAddDoc3.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  iMax:=1;
  ViewDoc3.BeginUpdate;

  taSpecAc.First;
  if not taSpecAc.Eof then
  begin
    taSpecAc.Last;
    iMax:=taSpecAcNum.AsInteger+1;
  end;

  taSpecAc.Append;
  taSpecAcNum.AsInteger:=iMax;
  taSpecAcCodeTovar.AsInteger:=0;
  taSpecAcCodeEdIzm.AsInteger:=1;
  taSpecAcBarCode.AsString:='';
  taSpecAcNDSProc.AsFloat:=10;
  taSpecAcKol.AsFloat:=1;
  taSpecAcName.AsString:='';
  taSpecAcCena.AsFloat:=0;
  taSpecAcNewCena.AsFloat:=0;
  taSpecAcRazn.AsFloat:=0;
  taSpecAcProc.AsFloat:=0;
  taSpecAcSumCena.AsFloat:=0;
  taSpecAcSumNewCena.AsFloat:=0;
  taSpecAcSumRazn.AsFloat:=0;
  taSpecAcFullName.AsString:='';
  taSpecAc.Post;

  ViewDoc3.EndUpdate;
  GridDoc3.SetFocus;

  ViewDoc3CodeTovar.Options.Editing:=True;
  ViewDoc3Name.Options.Editing:=True;
  ViewDoc3Name.Focused:=True;
end;

procedure TfmAddDoc3.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecAc.RecordCount>0 then
  begin
    taSpecAc.Delete;
  end;
end;

procedure TfmAddDoc3.acAddListExecute(Sender: TObject);
begin
  iDirect:=4;
  fmCards.Show;
end;

procedure TfmAddDoc3.cxButton1Click(Sender: TObject);
Var IdH:Int64;
    rSum1,rSum2,rSum10,rSum20,rSum10n,rSum20n:Real;
    iGr,iSGr:Integer;
    iNum:Integer;
begin
  //��������
  with dmMC do
  begin
    if cxDateEdit1.Date<=prOpenDate then  begin ShowMessage('������ ������.');  exit; end;
    if fTestInv(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date))=False then begin ShowMessage('������ ������ (��������������).'); exit; end;

    cxButton1.Enabled:=False; cxButton2.Enabled:=False; cxButton8.Enabled:=False;
    ViewDoc3.BeginUpdate;

    IdH:=cxTextEdit1.Tag;

    if taSpecAc.State in [dsEdit,dsInsert] then taSpecAc.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    if cxTextEdit1.Tag=0 then
    begin
      //�������� ������������
      quC.Active:=False;
      quC.SQL.Clear;
      quC.SQL.Add('Select Count(*) as RecCount from "TTNOvr"');
      quC.SQL.Add('where Depart='+INtToStr(cxLookupComboBox1.EditValue));
      quC.SQL.Add('and DateAct='''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+'''');
      quC.SQL.Add('and Number='+cxTextEdit1.Text);
      if quCRecCount.AsInteger>0 then
      begin
        showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
        cxButton1.Enabled:=True; cxButton2.Enabled:=True;
        exit;
      end;

      iNum:=StrToINtDef(cxTextEdit1.Text,0);
      if (iNum>0) and (iNum=Label1.Tag) then
      begin //��������� �����
        quE.SQL.Clear;
        quE.SQL.Add('Update "IdPool" Set ID='+its(iNum));
        quE.SQL.Add('where CodeObject=13');
        quE.ExecSQL;
      end;

    end;
    try
      //������� ������ ����

      //������������ ��� ������� � �����������
      iCol:=0;

      rSum1:=0; rSum2:=0; rSum10:=0; rSum20:=0;  rSum10n:=0; rSum20n:=0;
      taSpecAc.First;
      while not taSpecAc.Eof do
      begin
        //����������

        taSpecAc.Edit;
        taSpecAcCena.AsFloat:=RoundVal(taSpecAcCena.AsFloat);
        taSpecAcNewCena.AsFloat:=RoundVal(taSpecAcNewCena.AsFloat);
        taSpecAcSumCena.AsFloat:=RoundVal(taSpecAcSumCena.AsFloat);
        taSpecAcSumNewCena.AsFloat:=RoundVal(taSpecAcSumNewCena.AsFloat);
        taSpecAcRazn.AsFloat:=RoundVal(taSpecAcNewCena.AsFloat)-RoundVal(taSpecAcCena.AsFloat);
        taSpecAcSumRazn.AsFloat:=RoundVal(taSpecAcSumNewCena.AsFloat)-RoundVal(taSpecAcSumCena.AsFloat);
        taSpecAc.Post;

        rSum1:=rSum1+taSpecAcSumCena.AsFloat;
        rSum2:=rSum2+taSpecAcSumNewCena.AsFloat;

        if taSpecAcNDSProc.AsFloat>=15 then
        begin
          rSum20:=rSum20+taSpecAcSumCena.AsFloat;
          rSum20n:=rSum20n+taSpecAcSumNewCena.AsFloat;
        end else
        begin
          if taSpecAcNDSProc.AsFloat>=8 then
          begin
            rSum10:=rSum10+taSpecAcSumCena.AsFloat;;
            rSum10n:=rSum20n+taSpecAcSumNewCena.AsFloat;
          end;
        end;
        taSpecAc.Next;
      end;

      if cxTextEdit1.Tag=0 then
      begin
        IDH:=prMax('DocsAc')+1;
//        cxTextEdit1.Tag:=IDH;  //� �����
      end else //��� �� ���������� , ����� ������� ��� ��� ���� ����� ������� ��� �� 100 ��� �����
      begin
//    ������ ������������
//    ������  ������ ����� ���� ���� 100-������ ��������

     {   quD.SQL.Clear;
        quD.SQL.Add('delete from "TTNOvrLn"');
        quD.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quD.SQL.Add('and DateAct='''+ds(cxDateEdit10.Date-36500)+'''');
        quD.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quD.ExecSQL;

        // ��������� ������ ������������ �� 100 ��� ����� �� ������ ������

        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNOvrLn" set');
        quE.SQL.Add('DateAct='''+ds(cxDateEdit10.Date-36500)+'''');
        quE.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quE.SQL.Add('and DateAct='''+ds(cxDateEdit10.Date)+'''');
        quE.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quE.ExecSQL;
       }
//    ������  ������� ����� ���� ����

        quD.SQL.Clear;
        quD.SQL.Add('delete from "TTNOvrLn"');
        quD.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quD.SQL.Add('and DateAct='''+ds(cxDateEdit10.Date)+'''');
        quD.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quD.ExecSQL;

      end;

      //����� �������� ���������
      if cxTextEdit1.Tag=0 then //����������
      begin
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "TTNOvr" values (');

        quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)+','''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+''','''+cxTextEdit1.Text+''',');
//      Depart DateAct   Number

        if (rSum1-rSum2)<0 then      //������
          quA.SQL.Add(fs(rSum1)+','+fs(rSum2)+','+fs(rSum2-rSum1)+',')
          //PostOldSum PostNewSum PostRaznica
        else
          quA.SQL.Add('0,0,0,');

        quA.SQL.Add(fs(rSum1)+','+fs(rSum2)+','+fs(rSum2-rSum1)+',');
        //MatOldSum MatNewSum MatRaznica

        if (rSum1-rSum2)>0 then      //�������
          quA.SQL.Add(fs(rSum1)+','+fs(rSum2)+','+fs(rSum2-rSum1)+',')
          //RaznOldSum RaznNewSum RaznRaznica
        else
          quA.SQL.Add('0,0,0,');

        quA.SQL.Add('0,');
        //ChekBuh
        quA.SQL.Add(fs(rSum10)+','+fs(rSum10n)+','+fs(rSum10n-rSum10)+',');
        //NDS10OldSum NDS10NewSum NDS10Raznica
        quA.SQL.Add(fs(rSum20)+','+fs(rSum20n)+','+fs(rSum20n-rSum20)+',');
        //NDS20OldSum NDS20NewSum NDS20Raznica
        quA.SQL.Add('0,');
        //AcStatus
        quA.SQL.Add(fs(rSum1-rSum10-rSum20)+','+fs(rSum2-rSum10n-rSum20n)+','+fs((rSum2-rSum10n-rSum20n)-(rSum1-rSum10-rSum20))+',');
        //NDS0OldSum NDS0NewSum NDS0Raznica
        quA.SQL.Add('0,0,0,0,0,0,0,0,0,');
        //NSP10OldSum  NSP10NewSum NSP10Raznica NSP20OldSum NSP20NewSum NSP20Raznica NSP0OldSum NSP0NewSum NSP0Raznica
        quA.SQL.Add(its(IDH)+',0,0,0,0');
        //ID LinkInvoice StartTransfer EndTransfer rezerv

        quA.SQL.Add(')');

        quA.ExecSQL;

      end else //����������
      begin
        quA.Active:=False;
        quA.SQL.Clear;
        quA.SQL.Add('Update "TTNOvr" set');
        quA.SQL.Add('Depart='+INtToStr(cxLookupComboBox1.EditValue));
        quA.SQL.Add(',DateAct='+''''+ds(cxDateEdit1.Date)+'''');
        quA.SQL.Add(',Number='''+cxTextEdit1.Text+'''');

        if (rSum1-rSum2)<0 then      //������
        begin
          quA.SQL.Add(',PostOldSum='+fs(rSum1));
          quA.SQL.Add(',PostNewSum='+fs(rSum2));
          quA.SQL.Add(',PostRaznica='+fs(rSum2-rSum1));
        end
        else
        begin
          quA.SQL.Add(',PostOldSum=0');
          quA.SQL.Add(',PostNewSum=0');
          quA.SQL.Add(',PostRaznica=0');
        end;

        quA.SQL.Add(',MatOldSum='+fs(rSum1));
        quA.SQL.Add(',MatNewSum='+fs(rSum2));
        quA.SQL.Add(',MatRaznica='+fs(rSum2-rSum1));

        if (rSum1-rSum2)>=0 then      //�������
        begin
          quA.SQL.Add(',RaznOldSum='+fs(rSum1));
          quA.SQL.Add(',RaznNewSum='+fs(rSum2));
          quA.SQL.Add(',RaznRaznica='+fs(rSum2-rSum1));
        end
        else
        begin
          quA.SQL.Add(',RaznOldSum=0');
          quA.SQL.Add(',RaznNewSum=0');
          quA.SQL.Add(',RaznRaznica=0');
        end;

        quA.SQL.Add(',ChekBuh=0');

        quA.SQL.Add(',NDS10OldSum='+fs(rSum10));
        quA.SQL.Add(',NDS10NewSum='+fs(rSum10n));
        quA.SQL.Add(',NDS10Raznica='+fs(rSum10n-rSum10));

        quA.SQL.Add(',NDS20OldSum='+fs(rSum20));
        quA.SQL.Add(',NDS20NewSum='+fs(rSum20n));
        quA.SQL.Add(',NDS20Raznica='+fs(rSum20n-rSum20));

        quA.SQL.Add(',AcStatus=0');

        quA.SQL.Add(',NDS0OldSum='+fs(rSum1-rSum10-rSum20));
        quA.SQL.Add(',NDS0NewSum='+fs(rSum2-rSum10n-rSum20n));
        quA.SQL.Add(',NDS0Raznica='+fs((rSum2-rSum10n-rSum20n)-(rSum1-rSum10-rSum20)));
        quA.SQL.Add(',NSP10OldSum=0');
        quA.SQL.Add(',NSP10NewSum=0');
        quA.SQL.Add(',NSP10Raznica=0');
        quA.SQL.Add(',NSP20OldSum=0');
        quA.SQL.Add(',NSP20NewSum=0');
        quA.SQL.Add(',NSP20Raznica=0');
        quA.SQL.Add(',NSP0OldSum=0');
        quA.SQL.Add(',NSP0NewSum=0');
        quA.SQL.Add(',NSP0Raznica=0');
        quA.SQL.Add(',ID='+its(IDH));
//        quA.SQL.Add(',LinkInvoice=0');
        quA.SQL.Add(',StartTransfer=0');
        quA.SQL.Add(',EndTransfer=0');
        quA.SQL.Add(',rezerv=0');
        quA.SQL.Add('where Depart='+INtToStr(cxDateEdit10.Tag));
        quA.SQL.Add('and DateAct='''+ds(cxDateEdit10.Date)+'''');
        quA.SQL.Add('and Number='''+cxTextEdit10.Text+'''');
        quA.ExecSQL;
      end;
      //������� �� ������������ - ��� ������ ����������
      taSpecAc.First;
      while not taSpecAc.Eof do
      begin
        prFindGr(taSpecAcCodeTovar.AsInteger,iGr,iSGr);

        quA.SQL.Clear;
        quA.SQL.Add('INSERT into "TTNOvrLn" values (');
        quA.SQL.Add(INtToStr(cxLookupComboBox1.EditValue)); //  Depart
        quA.SQL.Add(','''+FormatDateTime(sCrystDate,cxDateEdit1.Date)+''''); //DateAct
        quA.SQL.Add(','''+cxTextEdit1.Text+''''); //  Number

        quA.SQL.Add(','+its(iGr));  // GGroup
        quA.SQL.Add(','+its(iSGr));  // Podgr
        quA.SQL.Add(','+its(taSpecAcCodeTovar.AsInteger));  // Tovar
        quA.SQL.Add(','+its(taSpecAcCodeEdIzm.AsInteger));  // EdIzm
        quA.SQL.Add(','+its(taSpecAcNum.AsInteger));  // TovarType
        quA.SQL.Add(','''+taSpecAcBarCode.AsString+'''');  // BarCode
        quA.SQL.Add(','+fs(taSpecAcKol.AsFloat));  // Kol

        if taSpecAcNewCena.AsFloat>=taSpecAcCena.AsFloat then
        begin
          quA.SQL.Add(',0');  // PostCenaOldSum
          quA.SQL.Add(',0');  // PostCenaNewSum
          quA.SQL.Add(',0');  // PostCenaRaznica
        end else
        begin
          quA.SQL.Add(','+fs(taSpecAcCena.AsFloat));  // PostCenaOldSum
          quA.SQL.Add(','+fs(taSpecAcNewCena.AsFloat));  // PostCenaNewSum
          quA.SQL.Add(','+fs(taSpecAcRazn.AsFloat));  // PostCenaRaznica
        end;

        quA.SQL.Add(','+fs(taSpecAcCena.AsFloat));  // MatCenaOldSum
        quA.SQL.Add(','+fs(taSpecAcNewCena.AsFloat));  // MatCenaNewSum
        quA.SQL.Add(','+fs(taSpecAcRazn.AsFloat));  // MatCenaRaznica

        if taSpecAcNewCena.AsFloat>=taSpecAcCena.AsFloat then
        begin
          quA.SQL.Add(','+fs(taSpecAcCena.AsFloat));  // RaznCenaOldSum
          quA.SQL.Add(','+fs(taSpecAcNewCena.AsFloat));  // RaznCenaNewSum
          quA.SQL.Add(','+fs(taSpecAcRazn.AsFloat));  // RaznCenaRaznica
        end else
        begin
          quA.SQL.Add(',0');  // RaznCenaOldSum
          quA.SQL.Add(',0');  // RaznCenaNewSum
          quA.SQL.Add(',0');  // RaznCenaRaznica
        end;

        if taSpecAcNewCena.AsFloat>=taSpecAcCena.AsFloat then
        begin
          quA.SQL.Add(',0');  // PostSumOldSum
          quA.SQL.Add(',0');  // PostSumNewSum
          quA.SQL.Add(',0');  // PostSumRaznica
        end else
        begin
          quA.SQL.Add(','+fs(taSpecAcSumCena.AsFloat));  // PostSumOldSum
          quA.SQL.Add(','+fs(taSpecAcSumNewCena.AsFloat));  // PostSumNewSum
          quA.SQL.Add(','+fs(taSpecAcSumRazn.AsFloat));  // PostSumRaznica
        end;

        quA.SQL.Add(','+fs(taSpecAcSumCena.AsFloat));  // MatSumOldSum
        quA.SQL.Add(','+fs(taSpecAcSumNewCena.AsFloat));  // MatSumNewSum
        quA.SQL.Add(','+fs(taSpecAcSumRazn.AsFloat));  // MatSumRaznica


        if taSpecAcNewCena.AsFloat>=taSpecAcCena.AsFloat then
        begin
          quA.SQL.Add(','+fs(taSpecAcSumCena.AsFloat));  // RaznSumOldSum
          quA.SQL.Add(','+fs(taSpecAcSumNewCena.AsFloat));  // RaznSumNewSum
          quA.SQL.Add(','+fs(taSpecAcSumRazn.AsFloat));  // RaznSumRaznica
        end else
        begin
          quA.SQL.Add(',0');  // RaznSumOldSum
          quA.SQL.Add(',0');  // RaznSumNewSum
          quA.SQL.Add(',0');  // RaznSumRaznica
        end;

        quA.SQL.Add(')');
        quA.ExecSQL;

        taSpecAc.Next;
      end;

      cxTextEdit1.Tag:=IDH;
      cxTextEdit10.Tag:=IDH;
      cxTextEdit10.Text:=cxTextEdit1.Text;
      cxDateEdit10.Date:=cxDateEdit1.Date;
      cxDateEdit10.Tag:=cxLookupComboBox1.EditValue;
    finally
      cxButton1.Enabled:=True; cxButton2.Enabled:=True; cxButton8.Enabled:=True;
      prRefrID(quTTNAC,fmDocs3.ViewDocsAC,0);
      ViewDoc3.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc3.acDelAllExecute(Sender: TObject);
begin
  if taSpecAc.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpecAc.First; while not taSpecAc.Eof do taSpecAc.Delete;
    end;
  end;
end;

procedure TfmAddDoc3.cxLabel4Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc3.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc3.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc3.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc3.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  with dmMC do
  begin
    if (Key=$0D) then
    begin
    end;
  end;
end;

procedure TfmAddDoc3.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
//Var Km:Real;
 //   sName:String;
begin
  if bAdd then exit;
  with dmMC do
  begin
  end;//}
end;

procedure TfmAddDoc3.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc3.acReadBar1Execute(Sender: TObject);
Var sBar:String;
    iC,iMax:INteger;
begin
  sBar:=Edit1.Text;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if taSpecAc.RecordCount>0 then
    if taSpecAc.Locate('BarCode',sBar,[]) then
    begin
      ViewDoc3.Controller.FocusRecord(ViewDoc3.DataController.FocusedRowIndex,True);
      GridDoc3.SetFocus;
      exit;
    end;


  if prFindBarC(sBar,0,iC) then
  begin
    iMax:=1;
    if taSpecAc.RecordCount>0 then begin taSpecAc.Last; iMax:=taSpecAcNum.AsInteger+1; end;

    with dmMC do
    begin
      taSpecAc.Append;
      taSpecAcNum.AsInteger:=iMax;
      taSpecAcCodeTovar.AsInteger:=iC;
      taSpecAcCodeEdIzm.AsInteger:=quCIdEdIzm.AsInteger;
      taSpecAcBarCode.AsString:=sBar;
      taSpecAcNDSProc.AsFloat:=quCIdNDS.AsFloat;
      taSpecAcKol.AsFloat:=1;
      taSpecAcName.AsString:=quCIdName.AsString;
      taSpecAcCena.AsFloat:=quCIdCena.AsFloat;
      taSpecAcNewCena.AsFloat:=quCIdCena.AsFloat;
      taSpecAcRazn.AsFloat:=0;
      taSpecAcProc.AsFloat:=0;
      taSpecAcSumCena.AsFloat:=quCIdCena.AsFloat;
      taSpecAcSumNewCena.AsFloat:=quCIdCena.AsFloat;
      taSpecAcSumRazn.AsFloat:=0;
      taSpecAcFullName.AsString:=quCIdFullName.AsString;
      taSpecAc.Post;

      ViewDoc3.Controller.FocusRecord(ViewDoc3.DataController.FocusedRowIndex,True);
      GridDoc3.SetFocus;
    
    end;
  end else
    StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc3.ViewDoc3DblClick(Sender: TObject);
begin
  if cxButton1.Enabled then
    if (ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Name') then
    begin
      ViewDoc3Name.Options.Editing:=True;
      ViewDoc3Name.Focused:=True;
    end;
end;

procedure TfmAddDoc3.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;

procedure TfmAddDoc3.acPrintDocExecute(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum1,rSum2,rSum3:Real;
begin
  try
    ViewDoc3.BeginUpdate;

    rSum1:=0; rSum2:=0; rSum3:=0;
    taSpecAc.First;
    while not taSpecAc.Eof do
    begin
      rSum1:=rSum1+taSpecAcSumCena.AsFloat;
      rSum2:=rSum2+taSpecAcSumNewCena.AsFloat;
      rSum3:=rSum3+taSpecAcSumRazn.AsFloat;

      taSpecAc.Next;
    end;

    frRepDAC.LoadFromFile(CommonSet.Reports + 'ActP.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
    frVariables.Variable['SumOld']:=rSum1;
    frVariables.Variable['SumNew']:=rSum2;
    frVariables.Variable['SumNac']:=rSum3;
    frVariables.Variable['SSumOld']:=MoneyToString(abs(rSum1),True,False);
    frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum2),True,False);
    frVariables.Variable['SSumNac']:=MoneyToString(abs(rSum3),True,False);

    frRepDAC.ReportName:='��� ����������.';
    frRepDAC.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDAC.ShowPreparedReport
    else frRepDAC.PrintPreparedReport('',1,False,vPP);
  finally
    ViewDoc3.EndUpdate;
  end;
end;

procedure TfmAddDoc3.taSpecAcKolChange(Sender: TField);
begin
  if iCol=1 then //���-��
  begin
    taSpecAcSumCena.AsFloat:=taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumNewCena.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumRazn.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat-taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
  end;
end;

procedure TfmAddDoc3.taSpecAcCenaChange(Sender: TField);
begin
  if iCol=2 then
  begin
    taSpecAcRazn.AsFloat:=taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat;
    taSpecAcProc.AsFloat:=0;
    if taSpecAcCena.AsFloat<>0 then
    begin
      taSpecAcProc.AsFloat:=(taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat)/taSpecAcCena.AsFloat*100;
    end;
    taSpecAcSumCena.AsFloat:=taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumNewCena.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumRazn.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat-taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
  end;
end;

procedure TfmAddDoc3.taSpecAcNewCenaChange(Sender: TField);
begin
  if iCol=3 then
  begin
    taSpecAcRazn.AsFloat:=taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat;
    taSpecAcProc.AsFloat:=0;
    if taSpecAcCena.AsFloat<>0 then
    begin
      taSpecAcProc.AsFloat:=(taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat)/taSpecAcCena.AsFloat*100;
    end;
    taSpecAcSumCena.AsFloat:=taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumNewCena.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat;
    taSpecAcSumRazn.AsFloat:=taSpecAcNewCena.AsFloat*taSpecAcKol.AsFloat-taSpecAcCena.AsFloat*taSpecAcKol.AsFloat;
  end;
end;

procedure TfmAddDoc3.taSpecAcSumCenaChange(Sender: TField);
begin
  if iCol=4 then
  begin
    if taSpecAcKol.AsFloat<>0 then
    begin
      taSpecAcCena.AsFloat:=taSpecAcSumCena.AsFloat/taSpecAcKol.AsFloat;
      taSpecAcProc.AsFloat:=0;
      if taSpecAcCena.AsFloat<>0 then
      begin
        taSpecAcProc.AsFloat:=(taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat)/taSpecAcCena.AsFloat*100;
      end;
      taSpecAcRazn.AsFloat:=taSpecAcNewCena.AsFloat-taSpecAcSumCena.AsFloat/taSpecAcKol.AsFloat;
      taSpecAcSumRazn.AsFloat:=taSpecAcSumNewCena.AsFloat-taSpecAcSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc3.taSpecAcSumNewCenaChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if taSpecAcKol.AsFloat<>0 then
    begin
      taSpecAcNewCena.AsFloat:=taSpecAcSumNewCena.AsFloat/taSpecAcKol.AsFloat;
      taSpecAcProc.AsFloat:=0;
      if taSpecAcCena.AsFloat<>0 then
      begin
        taSpecAcProc.AsFloat:=(taSpecAcNewCena.AsFloat-taSpecAcCena.AsFloat)/taSpecAcCena.AsFloat*100;
      end;
      taSpecAcRazn.AsFloat:=taSpecAcSumNewCena.AsFloat/taSpecAcKol.AsFloat-taSpecAcCena.AsFloat;
      taSpecAcSumRazn.AsFloat:=taSpecAcSumNewCena.AsFloat-taSpecAcSumCena.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc3.acPrintCenExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
    rPr:Real;
begin
  //������ ��������
  with dmMc do
  begin
    try
      if ViewDoc3.Controller.SelectedRecordCount=0 then exit;
      CloseTe(taCen);
      for i:=0 to ViewDoc3.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc3.Controller.SelectedRecords[i];

        iNum:=0; rPr:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc3.Columns[j].Name='ViewDoc3CodeTovar' then  iNum:=Rec.Values[j];
          if ViewDoc3.Columns[j].Name='ViewDoc3NewCena' then  rPr:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          quFindC4.Active:=False;
          quFindC4.ParamByName('IDC').AsInteger:=iNum;
          quFindC4.Active:=True;
          if quFindC4.RecordCount>0 then
          begin
            taCen.Append;
            taCenIdCard.AsInteger:=iNum;
            taCenFullName.AsString:=quFindC4FullName.AsString;
            taCenCountry.AsString:=quFindC4NameCu.AsString;
            taCenPrice1.AsFloat:=rPr;
            taCenPrice2.AsFloat:=0;
            taCenDiscount.AsFloat:=0;
            taCenBarCode.AsString:=quFindC4BarCode.AsString;
            taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
            if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
            taCenDepName.AsString:=prFindFullName(cxLookupComboBox1.EditValue);
            taCensDisc.AsString:=prSDisc(iNum);
            taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
            taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
            taCenV02.AsInteger:=quFindC4V02.AsInteger;
            taCenV12.AsInteger:=quFindC4V12.AsInteger;
            taCenMaker.AsString:=quFindC4NAMEM.AsString;
            taCen.Post;
          end;

          quFindC4.Active:=False;
        end;
      end;

      RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);
      RepCenn.ReportName:='�������.';
      RepCenn.PrepareReport;
      vPP:=frAll;
      RepCenn.PrintPreparedReport('',1,False,vPP);
      fmMainMCryst.cxDEdit1.Date:=Date;
    finally
    end;
  end;
end;

procedure TfmAddDoc3.acSetPriceExecute(Sender: TObject);
Var rRemn:Real;
begin
  //����������� �������
  if cxButton1.Enabled then
  begin
    ViewDoc3.BeginUpdate;
    taSpecAc.First;
    while not taSpecAc.Eof do
    begin
      if (cxLookupComboBox1.EditValue>0)and(taSpecAcCodeTovar.AsInteger>0)
        then rRemn:=prFindTRemnDep(taSpecAcCodeTovar.AsInteger,cxLookupComboBox1.EditValue)
        else rRemn:=0;

      taSpecAc.Edit;
      taSpecAcRemn.AsFloat:=rREmn;
      taSpecAc.Post;

      taSpecAc.Next;
      delay(10);
    end;
    taSpecAc.First;
    ViewDoc3.EndUpdate;
    prSetNac;
  end;
end;

procedure TfmAddDoc3.cxLabel5Click(Sender: TObject);
begin
  acSetPrice.Execute;
end;

procedure TfmAddDoc3.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc3);
end;

procedure TfmAddDoc3.acPrintTermoExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    sBar:String;
begin
  //������ �� �����
  if ViewDoc3.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
//              prWritePrinter(StrWk+#$0D+#$0A);
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if prOpenPrinter(CommonSet.TPrintN) then
          begin
            for i:=0 to ViewDoc3.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewDoc3.Controller.SelectedRecords[i];

              iNum:=0; rPr:=0;
              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewDoc3.Columns[j].Name='ViewDoc3CodeTovar' then iNum:=Rec.Values[j];
                if ViewDoc3.Columns[j].Name='ViewDoc3NewCena' then rPr:=Rec.Values[j];
              end;
              if iNum>0 then
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  for k:=0 to Str.Count-1 do
                  begin
                    StrWk:=Str[k];
                    if pos('@38.3@',StrWk)>0 then
                    begin
                      insert('E30',StrWk,pos('@38.3@',StrWk));
                      delete(StrWk,pos('@38.3@',StrWk),6)
                    end;
                    if pos('@',StrWk)>0 then
                    begin //���� ��� �� ���� ����������??
                      Str1:=Strwk;

                      Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                      delete(Str1,1,pos('@',Str1));//3.30.36@"

                      Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                      delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                      Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                      if Str4='3' then   //��������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4FullName.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='5' then  //������
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //5.30.36
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=20;
                        Str5:=Copy(quFindC4NameCu.AsString,1,l);
                        if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                      end;
                      if Str4='7' then  //�������     7.6.36
                      begin
                        Str5:=INtToStr(iNum);
                      end;
                      if Str4='18' then   //����   18.10.36
                      begin
                        Str5:=ToStr(rPr);
                      end;
                      if Str4='31' then    //��
                      begin
                        Str5:=Str3;
                        delete(Str5,1,pos('.',Str5)); //31.13.8
                        Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                        l:=StrToIntDef(Str5,0);
                        if l=0 then l:=13;

                        //�� �� ������� ����
                        sBar:=quFindC4BarCode.AsString;
                        if Length(sBar)<13 then
                        begin
                          if Length(sBar)=7 then //������� ����
                          begin
                            sBar:=ToStandart(sBar);
                            l:=13;
                          end;
                        end;
                        Str5:=Copy(sBar,1,l);
                      end;
                      if Str4='39' then   //���-��
                      begin
                        Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                      end;
                      StrWk:=Str2+Str5+Str1;
                    end;
                    prWritePrinter(StrWk+#$0D+#$0A);
                  end;
                end;
                quFindC4.Active:=False;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;

procedure TfmAddDoc3.acPostViewExecute(Sender: TObject);
begin
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if taSpecAc.RecordCount>0 then
    begin
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=taSpecAcCodeTovar.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=taSpecAcCodeTovar.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;
      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=taSpecAcName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmAddDoc3.prSetNac;
Var n1,n2,n3,rPrNac:Real;
begin
  with dmMt do
  begin
    try
      ViewDoc3.BeginUpdate;
      taSpecAc.First;
      while not taSpecAc.Eof do
      begin
        prFindNacGr(taSpecAcCodeTovar.AsInteger,n1,n2,n3,rPrNac);

        taSpecAc.Edit;
        taSpecAcNac1.AsFloat:=n1;
        taSpecAcNac2.AsFloat:=n2;
        taSpecAcNac3.AsFloat:=n3;
        taSpecAc.Post;

        taSpecAc.Next;
      end;
    finally
      ViewDoc3.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc3.acAddDiscStopExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iC:INteger;
begin
  //�������� � �������� ������
  with dmMt do
  begin
    try
      if ViewDoc3.Controller.SelectedRecordCount=0 then exit;
      iC:=0;
      if ptPluLim.Active=False then ptPluLim.Active:=True;
      ptPluLim.Refresh;

      for i:=0 to ViewDoc3.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc3.Controller.SelectedRecords[i];


        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDoc3.Columns[j].Name='ViewDoc3CodeTovar' then begin iNum:=Rec.Values[j]; Break; end;
        end;

        if iNum>0 then
        begin
          if ptPluLim.FindKey([iNum]) then ptPluLim.Edit
          else ptPluLim.Append;

          ptPluLimCode.AsInteger:=iNum;
          ptPluLimPercent.AsFloat:=100;
          ptPluLim.Post;

          inc(iC);
          delay(10);
        end;
      end;
      showmessage(its(iC)+' ������� �������� � ����-���� ������. �� �������� ���������� ����� ������ �� �����.');
    finally
    end;
  end;
end;

end.
