unit InvExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxTextEdit, cxMaskEdit, cxButtonEdit, cxControls, cxContainer,
  cxEdit, cxLabel;

type
  TfmInvExport = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TcxLabel;
    cxButtonEdit2: TcxButtonEdit;
    OpenDialog1: TOpenDialog;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInvExport: TfmInvExport;

implementation

uses AddDoc5, Un1, MDB, MT, u2fdk;

{$R *.dfm}

procedure TfmInvExport.cxButton1Click(Sender: TObject);
Var f:TextFile;
    Str1,Str2:String;
    iCode:INteger;
    rQ,rQf,rPrice,rQ1,rQr,rSum1,rSumR:Real;
    sName:String;
    iM:INteger;
    rNDS:Real;
    iC:Integer;
    sINv,sFile:String;
    NameF:String;
    rSumF,rSumRSS,rSumFSS,rSumRTO,rQuantC,rSumC,rSumRSC:Real;
    rSumF1,rSumRSS1,rSumFSS1,rSumRTO1,rQuantC1,rSumC1,rSumRSC1:Real;
    rSumWithNDS:Real;
    rVal:Real;
begin
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  delay(30);
  

  if cxButtonEdit2.Tag=0 then //��������
  begin
    if fmInvExport.Tag>=1 then //��������� ��� - �� ������ ���� //2 ����������� ��������
    begin
      if fmInvExport.Tag=1 then //��������� ��� - �� ������ ����
      begin

        NameF:=cxButtonEdit2.Text;

        if MessageDlg('��������� ���� (1) � '+NameF+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if NameF[length(NameF)]=';' then delete(NameF,length(NameF),1);
          try
            assignfile(f,NameF);
            rewrite(f);
            with fmAddDoc5 do
            begin
              ViewDoc5.BeginUpdate;
              dstaSpecInv.DataSet:=nil;

              taSpecInv.First;
              while not taSpecInv.Eof do
              begin
                if (taSpecInvQuantF.AsFloat<>0)
                or(taSpecInvQuantR.AsFloat<>0)
                or(taSpecInvSumR.AsFloat<>0)
                or(taSpecInvSumF.AsFloat<>0)
                or(taSpecInvSumRSS.AsFloat<>0)
                or(taSpecInvSumFSS.AsFloat<>0)
                or(taSpecInvSumRTO.AsFloat<>0)
                or(taSpecInvQuantC.AsFloat<>0)
                or(taSpecInvSumC.AsFloat<>0)
                or(taSpecInvSumRSC.AsFloat<>0)
                then
                begin

                  Str1:='01'; //�� �� ���� �����

                Str1:=Str1+taSpecInvCodeTovar.AsString; //��� ������
                while length(Str1)<7 do Str1:=Str1+' ';

//                Str2:=taSpecInvPrice.AsString; //����

                rVal:=R1000(taSpecInvPrice.AsFloat);
                Str2:=FloatToStr(rVal); //����
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<16 do Str1:=Str1+' ';

//                Str2:=taSpecInvQuantF.AsString; //���-��
                rVal:=R1000(taSpecInvQuantF.AsFloat);
                Str2:=FloatToStr(rVal); //����
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<26 do Str1:=Str1+' ';     // 26

                Str1:=Str1+'1'; //������ - ����� �� ����� ����   //27

//                Str2:=taSpecInvQuantR.AsString; //���-�� ����
                rVal:=R1000(taSpecInvQuantR.AsFloat);
                Str2:=FloatToStr(rVal); //����
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<37 do Str1:=Str1+' ';   //37

//                Str2:=taSpecInvSumR.AsString; //����� ���������
                rVal:=R1000(taSpecInvSumR.AsFloat);
                Str2:=FloatToStr(rVal); //����
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<47 do Str1:=Str1+' ';   //47


//                if fmAddDoc5.ViewDoc5.Tag=1 then
//                begin
{                  taSpecInvSumF.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat);
                  taSpecInvSumRSS.AsFloat:=ptInvLine1SUMRSS.AsFloat;
                  taSpecInvSumFSS.AsFloat:=ptInvLine1SUMFSS.AsFloat;
                  taSpecInvSumRTO.AsFloat:=ptInvLine1SUMDSS.AsFloat; }

//                  Str2:=taSpecInvSumF.AsString;
                  rVal:=R1000(taSpecInvSumF.AsFloat);
                  Str2:=FloatToStr(rVal); //����
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<57 do Str1:=Str1+' ';   //57

//                  Str2:=taSpecInvSumRSS.AsString;
                  rVal:=R1000(taSpecInvSumRSS.AsFloat);
                  Str2:=FloatToStr(rVal); //����
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<67 do Str1:=Str1+' ';   //67

//                  Str2:=taSpecInvSumFSS.AsString;
                  rVal:=R1000(taSpecInvSumFSS.AsFloat);
                  Str2:=FloatToStr(rVal); //����
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<77 do Str1:=Str1+' ';   //77

//                  Str2:=taSpecInvSumRTO.AsString;

                  rVal:=R1000(taSpecInvSumRTO.AsFloat);
                  Str2:=FloatToStr(rVal); //����
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<87 do Str1:=Str1+' ';   //87

                  rVal:=R1000(taSpecInvQuantC.AsFloat);
                  Str2:=FloatToStr(rVal); //����� ���������
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<97 do Str1:=Str1+' ';   //97

                  rVal:=R1000(taSpecInvSumC.AsFloat);
                  Str2:=FloatToStr(rVal); //����� ���������
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<107 do Str1:=Str1+' ';   //107

                  rVal:=R1000(taSpecInvSumRSC.AsFloat);
                  Str2:=FloatToStr(rVal); //����
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<117 do Str1:=Str1+' ';   //117

                  writeln(f,Str1);

                end;

                taSpecInv.Next;
                delay(10);
              end;
              taSpecInv.First;
            end;
          finally
            fmAddDoc5.dstaSpecInv.DataSet:=fmAddDoc5.taSpecInv;
            fmAddDoc5.ViewDoc5.EndUpdate;
            closefile(f);
            showmessage('�������� ���������.');
          end;
        end;
      end;
      if fmInvExport.Tag=2 then //��������� ��� - �� ������ ���� ����� � �������� ���
      begin

        NameF:=cxButtonEdit2.Text;

        if MessageDlg('��������� ���� (2) � '+NameF+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if NameF[length(NameF)]=';' then delete(NameF,length(NameF),1);
          try
            assignfile(f,NameF);
            rewrite(f);
            with fmAddDoc5 do
            with dmMT do
            begin
              ViewDoc5.BeginUpdate;
              dstaSpecInv.DataSet:=nil;

              taSpecInv.First;
              while not taSpecInv.Eof do
              begin
//            if taSpecInvQuantF.AsFloat<>0 then
//            begin

                rSumWithNDS:=rv(taSpecInvSumFSS.AsFloat);

                if taCards.FindKey([taSpecInvCodeTovar.AsInteger]) then
                  rSumWithNDS:=rv((RoundEx(taCardsNDS.AsFloat)+100)*taSpecInvSumFSS.AsFloat/100);

                Str1:='01'; //�� �� ���� �����

                Str1:=Str1+taSpecInvCodeTovar.AsString; //��� ������
                while length(Str1)<7 do Str1:=Str1+' ';

                Str2:=taSpecInvPrice.AsString; //����
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<16 do Str1:=Str1+' ';

                Str2:=taSpecInvQuantF.AsString; //���-��
                while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                Str1:=Str1+Str2;
                while length(Str1)<26 do Str1:=Str1+' ';     // 26

                Str1:=Str1+'1'; //������ - ����� �� ����� ����   //27

                Str2:='0'; //���-�� ����
                Str1:=Str1+Str2;
                while length(Str1)<37 do Str1:=Str1+' ';   //37

                Str2:='0'; //����� ���������
                Str1:=Str1+Str2;
                while length(Str1)<47 do Str1:=Str1+' ';   //47


                if fmAddDoc5.ViewDoc5.Tag=1 then
                begin
{                  taSpecInvSumF.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat);
                  taSpecInvSumRSS.AsFloat:=ptInvLine1SUMRSS.AsFloat;
                  taSpecInvSumFSS.AsFloat:=ptInvLine1SUMFSS.AsFloat;
                  taSpecInvSumRTO.AsFloat:=ptInvLine1SUMDSS.AsFloat; }

                  Str2:=taSpecInvSumF.AsString; //����� �����������
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<57 do Str1:=Str1+' ';   //57

//                  Str2:=taSpecInvSumRSS.AsString; //����� �������� �� ��
                  Str2:='0'; //����� �������� �� ��
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<67 do Str1:=Str1+' ';   //67

//                  Str2:=taSpecInvSumFSS.AsString;  //����� ���� �� ��
// ����� � ���
                  Str2:=FloatToStr(rSumWithNDS);  //����� ���� �� �� c ���
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<77 do Str1:=Str1+' ';   //77

//                  Str2:=taSpecInvSumRTO.AsString;   //����� �� ��
                  Str2:='0';   //����� �� ��
                  while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
                  Str1:=Str1+Str2;
                  while length(Str1)<87 do Str1:=Str1+' ';   //87

                end;

                writeln(f,Str1);
//            end;

                taSpecInv.Next;
                delay(10);
              end;
              taSpecInv.First;
            end;
          finally
            fmAddDoc5.dstaSpecInv.DataSet:=fmAddDoc5.taSpecInv;
            fmAddDoc5.ViewDoc5.EndUpdate;
            closefile(f);
            showmessage('�������� ���������.');
          end;
        end;
      end;

    end else
    begin
      NameF:=cxButtonEdit2.Text;
      if MessageDlg('��������� ���� � '+NameF+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if NameF[length(NameF)]=';' then delete(NameF,length(NameF),1);
        try
          assignfile(f,NameF);
          rewrite(f);
          with fmAddDoc5 do
          begin
            ViewDoc5.BeginUpdate;
            dstaSpecInv.DataSet:=nil;

            taSpecInv.First;
            while not taSpecInv.Eof do
            begin
//            if taSpecInvQuantF.AsFloat<>0 then
//            begin
              Str1:='01'; //�� �� ���� �����

              Str1:=Str1+taSpecInvCodeTovar.AsString; //��� ������
              while length(Str1)<7 do Str1:=Str1+' ';

              Str2:=taSpecInvPrice.AsString; //����
              while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
              Str1:=Str1+Str2;
              while length(Str1)<16 do Str1:=Str1+' ';

              Str2:=taSpecInvQuantF.AsString; //���-��
              while pos(',',Str2)>0 do Str2[pos(',',Str2)]:='.';
              Str1:=Str1+Str2;
              while length(Str1)<26 do Str1:=Str1+' ';

              Str1:=Str1+'1'; //������ - ����� �� ����� ����

              writeln(f,Str1);
//            end;

              taSpecInv.Next;
              delay(10);
            end;
            taSpecInv.First;
          end;
        finally
          fmAddDoc5.dstaSpecInv.DataSet:=fmAddDoc5.taSpecInv;
          fmAddDoc5.ViewDoc5.EndUpdate;
          closefile(f);
          showmessage('�������� ���������.');
        end;
      end;
    end;
  end;
  if cxButtonEdit2.Tag=1 then //��������
  begin
    if fmInvExport.Tag=1 then //��������� ��� - �� ������ ����
    begin
      sInv:=cxButtonEdit2.Text;
      fmAddDoc5.Memo1.Clear;
      if pos(';',sINv)=0 then sInv:=sINv+';';
      while sInv>'' do
      begin
        sFile := Copy(sInv,1,pos(';',sINv)-1);
        delete(sInv,1,pos(';',sINv));
        fmAddDoc5.Memo1.lines.Add('  ����� ���� �������� ����� - '+sFile);
        try
          fmAddDoc5.ViewDoc5.BeginUpdate;
          fmAddDoc5.dstaSpecInv.DataSet:=nil;

          if FileExists(sFile) then
          begin
            assignfile(f,sFile);
            reset(f); iC:=0;
            with fmAddDoc5 do
            begin

              while not EOF(f) do
              begin
                ReadLn(f,Str1); inc(iC);
                if length(Str1)>=27 then
                begin
                  Str2:=Copy(Str1,3,5); while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                  iCode:=StrToINtDef(Str2,0);
                  if iCode>0 then
                  begin
                    if length(Str1)>=27 then
                    begin
                      Str2:=Copy(Str1,17,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rQf:=StrToFloatDef(Str2,0);
                    end else rQf:=0;

                    if length(Str1)>=17 then
                    begin
                      Str2:=Copy(Str1,8,9);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rPrice:=StrToFloatDef(Str2,0);
                    end else rPrice:=0;

                    if length(Str1)>=38 then
                    begin
                      Str2:=Copy(Str1,28,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rQr:=StrToFloatDef(Str2,0);
                    end else rQr:=0;

                    if length(Str1)>=48 then
                    begin
                      Str2:=Copy(Str1,38,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumR:=StrToFloatDef(Str2,0);
                    end else rSumR:=0;

                    if length(Str1)>=58 then
                    begin
                      Str2:=Copy(Str1,48,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumF:=StrToFloatDef(Str2,0);
                    end else rSumF:=0;

                    if length(Str1)>=68 then
                    begin
                      Str2:=Copy(Str1,58,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumRSS:=StrToFloatDef(Str2,0);
                    end else rSumRSS:=0;

                    if length(Str1)>=78 then
                    begin
                      Str2:=Copy(Str1,68,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumFSS:=StrToFloatDef(Str2,0);
                    end else rSumFSS:=0;

                    if length(Str1)>=88 then
                    begin
                      Str2:=Copy(Str1,78,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumRTO:=StrToFloatDef(Str2,0);
                    end else rSumRTO:=0;

                    if length(Str1)>=98 then
                    begin
                      Str2:=Copy(Str1,88,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rQuantC:=StrToFloatDef(Str2,0);
                    end else rQuantC:=0;

                    if length(Str1)>=108 then
                    begin
                      Str2:=Copy(Str1,98,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumC:=StrToFloatDef(Str2,0);
                    end else rSumC:=0;

                    if length(Str1)>=118 then
                    begin
                      Str2:=Copy(Str1,108,10);
                      while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                      while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                      rSumRSC:=StrToFloatDef(Str2,0);
                    end else rSumRSC:=0;

                    if taSpecInv.Locate('CodeTovar',iCode,[]) then
                    begin
                      rQ:=taSpecInvQuantF.AsFloat;
                      rQ1:=taSpecInvQuantR.AsFloat;
                      rSum1:=taSpecInvSumR.AsFloat;

                      rSumF1:=taSpecInvSumF.AsFloat;
                      rSumRSS1:=taSpecInvSumRSS.AsFloat;
                      rSumFSS1:=taSpecInvSumFSS.AsFloat;
                      rSumRTO1:=taSpecInvSumRTO.AsFloat;
                      rQuantC1:=taSpecInvQuantC.AsFloat;
                      rSumC1:=taSpecInvSumC.AsFloat;
                      rSumRSC1:=taSpecInvSumRSC.AsFloat;

                      taSpecInv.Edit;
                      taSpecInvQuantR.AsFloat:=rQ1+rQr;
                      taSpecInvSumR.AsFloat:=rSum1+rSumR;

{                      rPrice:=0;
                      if abs(rQ1+rQr)>0 then
                      begin
                        try
                          rPrice:=(rSum1+rSumR)/(rQ1+rQr)
                        except
                          rPrice:=0;
                        end;
                      end;}

                      taSpecInvPrice.AsFloat:=rPrice;
                      taSpecInvQuantF.AsFloat:=rQ+rQf;

                      taSpecInvSumF.AsFloat:=rSumF1+rSumF;

                      taSpecInvQuantD.AsFloat:=(rQ+rQf)-(rQ1+rQr)-rQuantC;

                      taSpecInvSumD.AsFloat:=(rSumF1+rSumF)-(rSum1+rSumR);

//                      taSpecInvSumF.AsFloat:=rPrice*(rQ+rQf);
//                      taSpecInvQuantD.AsFloat:=(rQ+rQf)-(rQ1+rQr);
//                      taSpecInvSumD.AsFloat:=rPrice*(rQ+rQf)-(rSum1+rSumR);


                      if rSumF>0 then taSpecInvSumF.AsFloat:=rSumF1+rSumF;

                      rSumRSS:=rSumRSS1+rSumRSS;
                      rSumFSS:=rSumFSS1+rSumFSS;
                      rSumRTO:=rSumRTO1+rSumRTO;

                      rQuantC:=rQuantC1+rQuantC;
                      rSumC:=rSumC1+rSumC;
                      rSumRSC:=rSumRSC1+rSumRSC;

                      taSpecInvSumRSS.AsFloat:=rSumRSS;
                      taSpecInvSumFSS.AsFloat:=rSumFSS;
                      taSpecInvSumRTO.AsFloat:=rSumRTO;
                      taSpecInvSumDSS.AsFloat:=rSumFSS-rSumRSS;
                      taSpecInvSumDTO.AsFloat:=rSumFSS-rSumRTO;

                      taSpecInvQuantC.AsFloat:=rQuantC;
                      taSpecInvSumC.AsFloat:=rSumC;
                      taSpecInvSumRSC.AsFloat:=rSumRSC;
                      taSpecInvSumRSSI.AsFloat:=rSumRSS+rSumRSC;
                      taSpecInvSumRI.AsFloat:=rSum1+rSumR+rSumC;

                      if (rQ1+rQr)<>0 then taSpecInvPriceSS.AsFloat:=rSumRSS/(rQ1+rQr)
                      else taSpecInvPriceSS.AsFloat:=0;

                      taSpecInv.Post;

                    end else
                    begin
//                    rPrice:=0;
                      sName:='';
                      iM:=1;
                      rNDS:=18;

                {      with dmMC do
                      begin
                        quFindC1.Active:=False;
                        quFindC1.SQL.Clear;
                        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
                        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status", "Goods"."V02"');
                        quFindC1.SQL.Add('FROM "Goods"');
                        quFindC1.SQL.Add('where "Goods"."ID" = '+its(iCode));
                        quFindC1.Active:=True;

                        if quFindC1.RecordCount=1 then //����� ���� ������
                        begin
//                        rPrice:=quFindC1Cena.AsFloat;
                          sName:=quFindC1Name.AsString;
                          iM:=quFindC1EdIzm.AsInteger;
                          rNDS:=quFindC1NDS.AsFloat;
                        end;
                        quFindC1.Active:=False;
                      end;}
                      with dmMT do
                      begin
                        if taCards.Active=False then taCards.Active:=True;

                        if taCards.FindKey([iCode]) then
                        begin
                          sName:=OemToAnsiConvert(taCardsName.AsString);
                          iM:=taCardsEdIzm.AsInteger;
                          rNDS:=taCardsNDS.AsFloat;
                        end;
                      end;

                      taSpecInv.Append;
                      taSpecInvCodeTovar.AsInteger:=iCode;
                      taSpecInvName.AsString:=sName;
                      taSpecInvCodeEdIzm.AsInteger:=iM;
                      taSpecInvNDSProc.AsFloat:=rNDS;
                      taSpecInvQuantR.AsFloat:=rQr;
                      taSpecInvSumR.AsFloat:=rSumR;

{                      try
                        rPrice:=rSumR/rQr;
                      except
                        rPrice:=0;
                      end;}

                      taSpecInvPrice.AsFloat:=rPrice;
                      taSpecInvQuantF.AsFloat:=rQf;
//                      taSpecInvSumF.AsFloat:=rQf*rPrice;
                      taSpecInvSumF.AsFloat:=rSumF;
                      taSpecInvQuantD.AsFloat:=rQf-rQr-rQuantC;
                      taSpecInvSumD.AsFloat:=rSumF-rSumR;
                      taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;

                      if rSumF>0 then taSpecInvSumF.AsFloat:=rSumF;

                      taSpecInvSumRSS.AsFloat:=rSumRSS;
                      taSpecInvSumFSS.AsFloat:=rSumFSS;
                      taSpecInvSumRTO.AsFloat:=rSumRTO;
                      taSpecInvSumDSS.AsFloat:=rSumFSS-rSumRSS;
                      taSpecInvSumDTO.AsFloat:=rSumFSS-rSumRTO;

                      taSpecInvQuantC.AsFloat:=rQuantC;
                      taSpecInvSumC.AsFloat:=rSumC;
                      taSpecInvSumRSC.AsFloat:=rSumRSC;
                      taSpecInvSumRSSI.AsFloat:=rSumRSS+rSumRSC;
                      taSpecInvSumRI.AsFloat:=rSumR+rSumC;

                      if rQr<>0 then taSpecInvPriceSS.AsFloat:=rSumRSS/rQr
                      else taSpecInvPriceSS.AsFloat:=0;

                      taSpecInv.Post;
                    end;
                  end;
                end;
                if iC mod 100 = 0 then
                begin
                  fmInvExport.StatusBar1.Panels[0].Text:=IntToStr(iC);
                  delay(20);
                end;
              end;
            end;
            closefile(f);
          end else showmessage('���� �� ������ - '+sFile);
        finally
          fmAddDoc5.dstaSpecInv.DataSet:=fmAddDoc5.taSpecInv;
          fmAddDoc5.ViewDoc5.EndUpdate;
          fmAddDoc5.Memo1.lines.Add('�������� ��������� - '+sFile);
//        showmessage('�������� ���������.');
        end;
      end;
    end else
    begin
      sInv:=cxButtonEdit2.Text;
      fmAddDoc5.Memo1.Clear;
      if pos(';',sINv)=0 then sInv:=sINv+';';
      while sInv>'' do
      begin
        sFile := Copy(sInv,1,pos(';',sINv)-1);
        delete(sInv,1,pos(';',sINv));
        fmAddDoc5.Memo1.lines.Add('  ����� ���� �������� ����� - '+sFile);
        try
          fmAddDoc5.ViewDoc5.BeginUpdate;
          fmAddDoc5.dstaSpecInv.DataSet:=nil;

          if FileExists(sFile) then
          begin
            assignfile(f,sFile);
            reset(f); iC:=0;
            with fmAddDoc5 do
            begin

              while not EOF(f) do
              begin
                ReadLn(f,Str1); inc(iC);
                if length(Str1)>=27 then
                begin
                  Str2:=Copy(Str1,3,5); while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                  iCode:=StrToINtDef(Str2,0);
                  if iCode>0 then
                  begin
                    Str2:=Copy(Str1,17,10);
                    while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                    while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                    rQf:=StrToFloatDef(Str2,0);

                    Str2:=Copy(Str1,8,9);
                    while pos(' ',Str2)>0 do delete(Str2,pos(' ',Str2),1);
                    while pos('.',Str2)>0 do Str2[pos('.',Str2)]:=',';
                    rPrice:=StrToFloatDef(Str2,0);

                    if taSpecInv.Locate('CodeTovar',iCode,[]) then
                    begin
                      rQ:=taSpecInvQuantF.AsFloat;

                      taSpecInv.Edit;
                      taSpecInvQuantF.AsFloat:=rQ+rQf;
                      taSpecInvSumF.AsFloat:=rPrice*(rQ+rQf);
                      taSpecInvQuantD.AsFloat:=(rQ+rQf)-taSpecInvQuantR.AsFloat;
                      taSpecInvSumD.AsFloat:=((rQ+rQf)-taSpecInvQuantR.AsFloat)*taSpecInvPrice.AsFloat;
                      taSpecInv.Post;

                    end else
                    begin
//                    rPrice:=0;
                      sName:='';
                      iM:=1;
                      rNDS:=18;
                      with dmMC do
                      begin
                        quFindC1.Active:=False;
                        quFindC1.SQL.Clear;
                        quFindC1.SQL.Add('SELECT TOP 2 "Goods"."ID", "Goods"."Name","Goods"."FullName", "Goods"."BarCode","Goods"."TovarType",');
                        quFindC1.SQL.Add('"Goods"."EdIzm","Goods"."NDS", "Goods"."Cena", "Goods"."Status", "Goods"."V02"');
                        quFindC1.SQL.Add('FROM "Goods"');
                        quFindC1.SQL.Add('where "Goods"."ID" = '+its(iCode));
                        quFindC1.Active:=True;

                        if quFindC1.RecordCount=1 then //����� ���� ������
                        begin
//                        rPrice:=quFindC1Cena.AsFloat;
                          sName:=quFindC1Name.AsString;
                          iM:=quFindC1EdIzm.AsInteger;
                          rNDS:=quFindC1NDS.AsFloat;
                        end;
                        quFindC1.Active:=False;
                      end;



                      taSpecInv.Append;
                      taSpecInvCodeTovar.AsInteger:=iCode;
                      taSpecInvName.AsString:=sName;
                      taSpecInvCodeEdIzm.AsInteger:=iM;
                      taSpecInvNDSProc.AsFloat:=rNDS;
                      taSpecInvPrice.AsFloat:=rPrice;
                      taSpecInvQuantR.AsFloat:=0;
                      taSpecInvSumR.AsFloat:=0;
                      taSpecInvQuantF.AsFloat:=rQf;
                      taSpecInvSumF.AsFloat:=rQf*rPrice;
                      taSpecInvQuantD.AsFloat:=rQf;
                      taSpecInvSumD.AsFloat:=rQf*rPrice;
                      taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
                      taSpecInv.Post;
                    end;
                  end;
                end;
                if iC mod 100 = 0 then
                begin
                  fmInvExport.StatusBar1.Panels[0].Text:=IntToStr(iC);
                  delay(20);
                end;
              end;
            end;
            closefile(f);
          end else showmessage('���� �� ������ - '+sFile);
        finally
          fmAddDoc5.dstaSpecInv.DataSet:=fmAddDoc5.taSpecInv;
          fmAddDoc5.ViewDoc5.EndUpdate;
          fmAddDoc5.Memo1.lines.Add('�������� ��������� - '+sFile);
//        showmessage('�������� ���������.');
        end;
      end;
    end;
  end;

  if cxButtonEdit2.Tag=2 then //��������
  begin
  end;

  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
end;

procedure TfmInvExport.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmInvExport.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var sInv:String;
    i:INteger;
begin
  OpenDialog1.Execute;
//  cxButtonEdit2.Text:=OpenDialog1.FileName;
  sINv:='';
  for i:=0 to OpenDialog1.Files.Count-1 do
  sINv:=sInv+OpenDialog1.Files.Strings[i]+';';
  cxButtonEdit2.Text:=sInv;
end;

procedure TfmInvExport.FormShow(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:='';
end;

end.
