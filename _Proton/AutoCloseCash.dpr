program AutoCloseCash;

uses
  Forms,
  uAutoCloseCash in 'uAutoCloseCash.pas' {fmMainAutoCloseCash},
  Un1 in 'Un1.pas',
  MDB in 'MDB.pas' {dmMC: TDataModule},
  MT in 'MT.pas' {dmMT: TDataModule},
  PXDB in 'PXDB.pas' {dmPx: TDataModule},
  u2fdk in 'U2FDK.PAS',
  MFB in 'MFB.pas' {dmFB: TDataModule},
  PerToAC in 'PerToAC.pas' {fmPeriodForAC};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainAutoCloseCash, fmMainAutoCloseCash);
  Application.CreateForm(TdmMC, dmMC);
  Application.CreateForm(TdmMT, dmMT);
  Application.CreateForm(TdmPx, dmPx);
  Application.CreateForm(TdmFB, dmFB);
  Application.CreateForm(TfmPeriodForAC, fmPeriodForAC);
  Application.Run;
end.
