unit mDeparts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmDeparts = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewDeparts: TcxGridDBTableView;
    LevelDeparts: TcxGridLevel;
    GridDeparts: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    ViewDepartsID: TcxGridDBColumn;
    ViewDepartsName: TcxGridDBColumn;
    amDeparts: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    ViewDepartsStatus1: TcxGridDBColumn;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure ViewDepartsCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDeparts: TfmDeparts;
  bClearDeparts:Boolean = False;

implementation

uses Un1, MDB, AddSingle, DepPars;

{$R *.dfm}

procedure TfmDeparts.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmDeparts.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmDeparts.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmDeparts.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmDeparts.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.FormCreate(Sender: TObject);
begin
  GridDeparts.Align:=AlClient;
  ViewDeparts.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmDeparts.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDeparts.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDeparts.Timer1Timer(Sender: TObject);
begin
  if bClearDeparts=True then begin StatusBar1.Panels[0].Text:=''; bClearDeparts:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDeparts:=True;
end;

procedure TfmDeparts.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDeparts.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelDeparts') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('Depart')>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quDepartsName.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewDeparts.BeginUpdate;
        try
          try
            iR:=quDepartsID.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from Depart');
            quD.SQL.Add('where ID='+IntToStr(iR));
            quD.ExecSQL;
            
            quDeparts.Close;
            quDeparts.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewDeparts.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmDeparts.acEditExecute(Sender: TObject);
Var iR:Integer;

  function fStatus1:Integer;
  begin
    if fmAddSingle.cxCheckBox1.Checked then Result:=9 else Result:=13;
  end;

begin
//�������������
  if not CanDo('prEditDeparts') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('Depart')>0 then
    begin
      fmAddSingle.prSetParams('������: ��������������.',1);
      fmAddSingle.cxTextEdit1.Text:=quDepartsName.AsString;
      if quDepartsStatus1.AsInteger=9 then fmAddSingle.cxCheckBox1.Checked:=True else fmAddSingle.cxCheckBox1.Checked:=False;
      fmAddSingle.cxTextEdit2.Text:=quDepartsFullName.AsString;
      fmAddSingle.ShowModal;
      if fmAddSingle.ModalResult=mrOk then
      begin
        ViewDeparts.BeginUpdate;
        try
          try
            iR:=quDepartsID.AsInteger;

            quE.SQL.Clear;
            quE.SQL.Add('Update Depart Set Name='''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+''', Status1 = '+IntToStr(fStatus1)+', FullName = '''+fmAddSingle.cxTextEdit2.Text+'''');
            quE.SQL.Add('where ID='+IntToStr(iR));
            quE.ExecSQL;

            quDeparts.Close; quDeparts.Open;
            quDeparts.Locate('ID',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewDeparts.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmDeparts.acAddExecute(Sender: TObject);
Var iMax,iP:Integer;

  function fStatus1:Integer;
  begin
    if fmAddSingle.cxCheckBox1.Checked then Result:=9 else Result:=13;
  end;

begin
//��������
  if not CanDo('prAddDeparts') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  fmAddSingle.prSetParams('������: ����������.',1);
  fmAddSingle.cxTextEdit1.Text:='';
  fmAddSingle.cxTextEdit2.Text:='';
  fmAddSingle.cxCheckBox1.Checked:=True;
  fmAddSingle.ShowModal;
  if fmAddSingle.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewDeparts.BeginUpdate;
      try
        if quDeparts.Locate('Name',fmAddSingle.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          iMax:=1;
          if CountRec('Depart')>0 then begin quDeparts.Last; iMax:=quDepartsID.AsInteger+1; end;

          quDepParent.Open;
          quDepParent.Last;
          iP:=quDepParentID.AsInteger;
          quDepParent.Close;

          quA.SQL.Clear;
          quA.SQL.Add('INSERT into Depart values ('+IntToStr(iMax)+','''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+''','+IntToStr(fStatus1)+',263,0,0,0,'+IntToStr(iP)+',6,0,0,0,'''','''','''',0,0,0,0,0,0,0,0,0,0,0,0,0,0)');
          quA.ExecSQL;

          quDeparts.Close; quDeparts.Open;
          quDeparts.Locate('ID',iMax,[]);
        end;
      finally
        ViewDeparts.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDeparts.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmDeparts.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmDeparts.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmDeparts.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmDeparts.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmDeparts.FormShow(Sender: TObject);
begin
  cxTextEdit1.Clear;
  cxLabel3.Caption:=CommonSet.NamePost;
end;

procedure TfmDeparts.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  with dmMC do
  begin
    ViewDeparts.BeginUpdate;
    try
      quDeparts.Locate('Name',cxTextEdit1.Text,[loCaseInsensitive,loPartialKey]);
    finally
      ViewDeparts.EndUpdate;
    end;
  end;
end;

procedure TfmDeparts.ViewDepartsCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:INteger;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewDeparts.ColumnCount-1 do
  begin
    if ViewDeparts.Columns[i].Name='ViewDepartsStatus1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=13  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmDeparts.cxLabel2Click(Sender: TObject);
begin
  with dmMc do
  begin
    if quDeparts.RecordCount>0 then
    begin
      fmDepPars:=TfmDepPars.Create(Application);
      quDepPars.Active:=False;
      quDepPars.ParamByName('ID').AsInteger:=quDepartsID.AsInteger;
      quDepPars.Active:=True;
      if quDepPars.RecordCount>0 then
      begin
        with fmDepPars do
        begin
          cxTextEdit1.Tag:=1;
          cxTextEdit1.Text:=quDepartsName.AsString;
          cxTextEdit2.Text:=quDepParsNAMEDEP.AsString;
          cxTextEdit3.Text:=quDepParsINN.AsString;
          cxTextEdit4.Text:=quDepParsKPP.AsString;
          cxTextEdit5.Text:=quDepParsADRDEP.AsString;
          cxTextEdit6.Text:=quDepParsNAMEOTPR.AsString;
          cxTextEdit7.Text:=quDepParsADROTPR.AsString;
          cxTextEdit8.Text:=quDepParsOGRN.AsString;
          cxTextEdit9.Text:=quDepParsLICO.AsString;
          cxTextEdit10.Text:=quDepParsFL.AsString;
          cxTextEdit11.Text:=quDepParsRSch.AsString;
          cxTextEdit12.Text:=quDepParsBank.AsString;
          cxTextEdit13.Text:=quDepParsKSCh.AsString;
          cxTextEdit14.Text:=quDepParsBik.AsString;
          cxTextEdit15.Text:=quDepParsRukovod.AsString;
          cxTextEdit16.Text:=quDepParsGlBuh.AsString;

          cxTextEdit17.Text:=quDepParsDir1.AsString;
          cxTextEdit18.Text:=quDepParsDir2.AsString;
          cxTextEdit19.Text:=quDepParsDir3.AsString;
          cxTextEdit20.Text:=quDepParsGb1.AsString;
          cxTextEdit21.Text:=quDepParsGb2.AsString;
          cxTextEdit22.Text:=quDepParsGb3.AsString;
          cxTextEdit23.Text:=quDepParsStreet.AsString;
          cxTextEdit24.Text:=quDepParsDom.AsString;
          cxTextEdit25.Text:=quDepParsKorp.AsString;
          cxTextEdit26.Text:=quDepParsLitera.AsString;
          cxTextEdit27.Text:=quDepParsIndex.AsString;
          cxTextEdit28.Text:=quDepParsLicVid.AsString;
          cxTextEdit29.Text:=quDepParsLicSer.AsString;
          cxTextEdit30.Text:=quDepParsLicNum.AsString;

          cxTextEdit31.Text:=quDepParsTel.AsString;
          cxTextEdit32.Text:=quDepParssEmail.AsString;
          cxTextEdit33.Text:=quDepParsGLN.AsString;

          cxDateEdit1.Date:=quDepParsLicDB.AsDateTime;
          cxDateEdit2.Date:=quDepParsLicDE.AsDateTime;

        end;
      end else
      begin
        with fmDepPars do
        begin
          cxTextEdit1.Text:=quDepartsName.AsString;
          cxTextEdit1.Tag:=0;
          cxTextEdit2.Text:=' ';
          cxTextEdit3.Text:=' ';
          cxTextEdit4.Text:=' ';
          cxTextEdit5.Text:=' ';
          cxTextEdit6.Text:=' ';
          cxTextEdit7.Text:=' ';
          cxTextEdit8.Text:=' ';
          cxTextEdit9.Text:=' ' ;
          cxTextEdit10.Text:=' ';
          cxTextEdit11.Text:=' ';
          cxTextEdit12.Text:=' ';
          cxTextEdit13.Text:=' ';
          cxTextEdit14.Text:=' ';
          cxTextEdit15.Text:=' ';
          cxTextEdit16.Text:=' ';

          cxTextEdit17.Text:=' ';
          cxTextEdit18.Text:=' ';
          cxTextEdit19.Text:=' ';
          cxTextEdit20.Text:=' ';
          cxTextEdit21.Text:=' ';
          cxTextEdit22.Text:=' ';
          cxTextEdit23.Text:=' ';
          cxTextEdit24.Text:=' ';
          cxTextEdit25.Text:=' ';
          cxTextEdit26.Text:=' ';
          cxTextEdit27.Text:=' ';
          cxTextEdit28.Text:=' ';
          cxTextEdit29.Text:=' ';
          cxTextEdit30.Text:=' ';
          cxTextEdit31.Text:=' ';
          cxTextEdit32.Text:=' ';
          cxTextEdit33.Text:=' ';

          cxDateEdit1.Date:=Date;
          cxDateEdit2.Date:=Date;
          
        end;
      end;
      quDepPars.Active:=False;

      fmDepPars.ShowModal;
      if fmDepPars.ModalResult=mrOk then
      begin //���������
        with fmDepPars do
        begin
          if cxTextEdit1.Tag=0 then
          begin // ��������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "A_DEPARTS" values (');
            quA.SQL.Add(its(quDepartsID.AsInteger)+',');//ID
            quA.SQL.Add(''''+cxTextEdit2.Text+''',');//NAMEDEP
            quA.SQL.Add(''''+cxTextEdit6.Text+''',');//NAMEOTPR
            quA.SQL.Add(''''+cxTextEdit5.Text+''',');//ADRDEP
            quA.SQL.Add(''''+cxTextEdit7.Text+''',');//ADROTPR
            quA.SQL.Add(''''+cxTextEdit9.Text+''',');//LICO
            quA.SQL.Add(''''+cxTextEdit8.Text+''',');//OGRN
            quA.SQL.Add(''''+cxTextEdit3.Text+''',');//INN
            quA.SQL.Add(''''+cxTextEdit4.Text+''',');//KPP
            quA.SQL.Add(''''+cxTextEdit10.Text+''',');//FL
            quA.SQL.Add(''''+cxTextEdit11.Text+''',');//RSch
            quA.SQL.Add(''''+cxTextEdit13.Text+''',');//KSch  KSCh
            quA.SQL.Add(''''+cxTextEdit12.Text+''',');//Bank
            quA.SQL.Add(''''+cxTextEdit14.Text+''',');//Bik
            quA.SQL.Add(''''+cxTextEdit15.Text+''',');//Rukovod
            quA.SQL.Add(''''+cxTextEdit16.Text+''',');//GlBuh

            quA.SQL.Add(''''+cxTextEdit23.Text+''',');// "Street" CHAR(100) CASE ,
            quA.SQL.Add(''''+cxTextEdit24.Text+''',');// "Dom" CHAR(10) CASE ,
            quA.SQL.Add(''''+cxTextEdit25.Text+''',');// "Korp" CHAR(10) CASE ,
            quA.SQL.Add(''''+cxTextEdit26.Text+''',');// "Litera" CHAR(10) CASE ,
            quA.SQL.Add(''''+cxTextEdit27.Text+''',');// "Index" CHAR(10) CASE ,
            quA.SQL.Add(''''+cxTextEdit31.Text+''',');// "Tel" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit32.Text+''',');// "sEmail" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit17.Text+''',');// "Dir1" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit18.Text+''',');// "Dir2" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit19.Text+''',');// "Dir3" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit20.Text+''',');// "Gb1" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit21.Text+''',');// "Gb2" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit22.Text+''',');// "Gb3" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit28.Text+''',');// "LicVid" CHAR(200) CASE ,
            quA.SQL.Add(''''+cxTextEdit29.Text+''',');// "LicSer" CHAR(50) CASE ,
            quA.SQL.Add(''''+cxTextEdit30.Text+''',');// "LicNum" CHAR(50) CASE ,
            quA.SQL.Add(''''+ds(cxDateEdit1.Date)+''',');// "LicDB" DATE,
            quA.SQL.Add(''''+ds(cxDateEdit2.Date)+''','); // "LicDE" DATE
            quA.SQL.Add(''''+cxTextEdit33.Text+''''); // "GLN" CHAR(15)
            quA.SQL.Add(')');
            quA.ExecSQL;
          end;
          if cxTextEdit1.Tag=1 then
          begin // �������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "A_DEPARTS" Set ');
            quE.SQL.Add('NAMEDEP='''+cxTextEdit2.Text+''',');
            quE.SQL.Add('NAMEOTPR='''+cxTextEdit6.Text+''',');
            quE.SQL.Add('ADRDEP='''+cxTextEdit5.Text+''',');
            quE.SQL.Add('ADROTPR='''+cxTextEdit7.Text+''',');
            quE.SQL.Add('LICO='''+cxTextEdit9.Text+''',');
            quE.SQL.Add('OGRN='''+cxTextEdit8.Text+''',');
            quE.SQL.Add('INN='''+cxTextEdit3.Text+''',');
            quE.SQL.Add('KPP='''+cxTextEdit4.Text+''',');
            quE.SQL.Add('FL='''+cxTextEdit10.Text+''',');
            quE.SQL.Add('RSch='''+cxTextEdit11.Text+''',');
            quE.SQL.Add('KSch='''+cxTextEdit13.Text+''',');
            quE.SQL.Add('Bank='''+cxTextEdit12.Text+''',');
            quE.SQL.Add('Bik='''+cxTextEdit14.Text+''',');
            quE.SQL.Add('Rukovod='''+cxTextEdit15.Text+''',');
            quE.SQL.Add('GlBuh='''+cxTextEdit16.Text+''',');

            quE.SQL.Add('Street='''+cxTextEdit23.Text+''',');// "Street" CHAR(100) CASE ,
            quE.SQL.Add('Dom='''+cxTextEdit24.Text+''',');// "Dom" CHAR(10) CASE ,
            quE.SQL.Add('Korp='''+cxTextEdit25.Text+''',');// "Korp" CHAR(10) CASE ,
            quE.SQL.Add('Litera='''+cxTextEdit26.Text+''',');// "Litera" CHAR(10) CASE ,
            quE.SQL.Add('"Index"='''+cxTextEdit27.Text+''',');// "Index" CHAR(10) CASE ,
            quE.SQL.Add('Tel='''+cxTextEdit31.Text+''',');// "Tel" CHAR(50) CASE ,
            quE.SQL.Add('sEmail='''+cxTextEdit32.Text+''',');// "sEmail" CHAR(50) CASE ,
            quE.SQL.Add('Dir1='''+cxTextEdit17.Text+''',');// "Dir1" CHAR(50) CASE ,
            quE.SQL.Add('Dir2='''+cxTextEdit18.Text+''',');// "Dir2" CHAR(50) CASE ,
            quE.SQL.Add('Dir3='''+cxTextEdit19.Text+''',');// "Dir3" CHAR(50) CASE ,
            quE.SQL.Add('Gb1='''+cxTextEdit20.Text+''',');// "Gb1" CHAR(50) CASE ,
            quE.SQL.Add('Gb2='''+cxTextEdit21.Text+''',');// "Gb2" CHAR(50) CASE ,
            quE.SQL.Add('Gb3='''+cxTextEdit22.Text+''',');// "Gb3" CHAR(50) CASE ,
            quE.SQL.Add('LicVid='''+cxTextEdit28.Text+''',');// "LicVid" CHAR(200) CASE ,
            quE.SQL.Add('LicSer='''+cxTextEdit29.Text+''',');// "LicSer" CHAR(50) CASE ,
            quE.SQL.Add('LicNum='''+cxTextEdit30.Text+''',');// "LicNum" CHAR(50) CASE ,
            quE.SQL.Add('LicDB='''+ds(cxDateEdit1.Date)+''',');// "LicDB" DATE,
            quE.SQL.Add('LicDE='''+ds(cxDateEdit2.Date)+''',');// "LicDE" DATE
            quE.SQL.Add('GLN='''+cxTextEdit33.Text+''''); // "GLN" CHAR(15)

            quE.SQL.Add('where ID='+its(quDepartsID.AsInteger));
            quE.ExecSQL;
          end;
        end;
      end;

      fmDepPars.Release;
    end;
  end;
end;

procedure TfmDeparts.cxLabel3Click(Sender: TObject);
begin
  with dmMc do
  begin
    if quDeparts.RecordCount>0 then
    begin
      fmDepPars:=TfmDepPars.Create(Application);
      quDepPars.Active:=False;
      quDepPars.ParamByName('ID').AsInteger:=1;    //��������� ����������� �� ��������� - ��������
      quDepPars.Active:=True;
      if quDepPars.RecordCount>0 then
      begin
        with fmDepPars do
        begin
          cxTextEdit1.Tag:=1;
          cxTextEdit1.Text:='';
          cxTextEdit2.Text:=quDepParsNAMEDEP.AsString;
          cxTextEdit3.Text:=quDepParsINN.AsString;
          cxTextEdit4.Text:=quDepParsKPP.AsString;
          cxTextEdit5.Text:=quDepParsADRDEP.AsString;
          cxTextEdit6.Text:=quDepParsNAMEOTPR.AsString;
          cxTextEdit7.Text:=quDepParsADROTPR.AsString;
          cxTextEdit8.Text:=quDepParsOGRN.AsString;
          cxTextEdit9.Text:=quDepParsLICO.AsString;
          cxTextEdit10.Text:=quDepParsFL.AsString;
          cxTextEdit11.Text:=quDepParsRSch.AsString;
          cxTextEdit12.Text:=quDepParsBank.AsString;
          cxTextEdit13.Text:=quDepParsKSCh.AsString;
          cxTextEdit14.Text:=quDepParsBik.AsString;
        end;
      end else
      begin
        with fmDepPars do
        begin
          cxTextEdit1.Text:='';
          cxTextEdit1.Tag:=0;
          cxTextEdit2.Text:='';
          cxTextEdit3.Text:='';
          cxTextEdit4.Text:='';
          cxTextEdit5.Text:='';
          cxTextEdit6.Text:='';
          cxTextEdit7.Text:='';
          cxTextEdit8.Text:='';
          cxTextEdit9.Text:='';
          cxTextEdit10.Text:='';
          cxTextEdit11.Text:='';
          cxTextEdit12.Text:='';
          cxTextEdit13.Text:='';
          cxTextEdit14.Text:='';
        end;
      end;
      quDepPars.Active:=False;

      fmDepPars.ShowModal;
      if fmDepPars.ModalResult=mrOk then
      begin //���������
        with fmDepPars do
        begin
          if cxTextEdit1.Tag=0 then
          begin // ��������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "A_DEPARTS" values (');
            quA.SQL.Add('1,');//ID
            quA.SQL.Add(''''+cxTextEdit2.Text+''',');//NAMEDEP
            quA.SQL.Add(''''+cxTextEdit6.Text+''',');//NAMEOTPR
            quA.SQL.Add(''''+cxTextEdit5.Text+''',');//ADRDEP
            quA.SQL.Add(''''+cxTextEdit7.Text+''',');//ADROTPR
            quA.SQL.Add(''''+cxTextEdit9.Text+''',');//LICO
            quA.SQL.Add(''''+cxTextEdit8.Text+''',');//OGRN
            quA.SQL.Add(''''+cxTextEdit3.Text+''',');//INN
            quA.SQL.Add(''''+cxTextEdit4.Text+''',');//KPP
            quA.SQL.Add(''''+cxTextEdit10.Text+''',');//FL
            quA.SQL.Add(''''+cxTextEdit11.Text+''',');//RSch
            quA.SQL.Add(''''+cxTextEdit13.Text+''',');//KSch  KSCh
            quA.SQL.Add(''''+cxTextEdit12.Text+''',');//Bank
            quA.SQL.Add(''''+cxTextEdit14.Text+'''');//Bik
            quA.SQL.Add(')');
            quA.ExecSQL;
          end;
          if cxTextEdit1.Tag=1 then
          begin // �������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "A_DEPARTS" Set ');
            quE.SQL.Add('NAMEDEP='''+cxTextEdit2.Text+''',');
            quE.SQL.Add('NAMEOTPR='''+cxTextEdit6.Text+''',');
            quE.SQL.Add('ADRDEP='''+cxTextEdit5.Text+''',');
            quE.SQL.Add('ADROTPR='''+cxTextEdit7.Text+''',');
            quE.SQL.Add('LICO='''+cxTextEdit9.Text+''',');
            quE.SQL.Add('OGRN='''+cxTextEdit8.Text+''',');
            quE.SQL.Add('INN='''+cxTextEdit3.Text+''',');
            quE.SQL.Add('KPP='''+cxTextEdit4.Text+''',');
            quE.SQL.Add('FL='''+cxTextEdit10.Text+''',');
            quE.SQL.Add('RSch='''+cxTextEdit11.Text+''',');
            quE.SQL.Add('KSch='''+cxTextEdit13.Text+''',');
            quE.SQL.Add('Bank='''+cxTextEdit12.Text+''',');
            quE.SQL.Add('Bik='''+cxTextEdit14.Text+'''');
            quE.SQL.Add('where ID=1');
            quE.ExecSQL;
          end;
        end;
      end;

      fmDepPars.Release;
    end;
  end;
end;

end.
