unit Scales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  ComCtrls, cxMemo, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, ActnList, XPStyleActnCtrls, ActnMan, ShellApi;

type
  TfmScale = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewScale: TcxGridDBTableView;
    LevelScale: TcxGridLevel;
    GridScale: TcxGrid;
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    ViewScalePLU: TcxGridDBColumn;
    ViewScaleGoodsItem: TcxGridDBColumn;
    ViewScaleFirstName: TcxGridDBColumn;
    ViewScaleLastName: TcxGridDBColumn;
    ViewScalePrice: TcxGridDBColumn;
    ViewScaleShelfLife: TcxGridDBColumn;
    ViewScaleTareWeight: TcxGridDBColumn;
    ViewScaleStatus: TcxGridDBColumn;
    cxButton4: TcxButton;
    amScale: TActionManager;
    acEditPlu: TAction;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    ViewScaleMessageNo: TcxGridDBColumn;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    procedure cxButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acEditPluExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButton(bSt:Boolean);
  end;

function Transfer_Ethernet(S:PChar):Integer; export; stdcall; far; external 'TransferEth.dll' name 'Transfer_Ethernet';

var
  fmScale: TfmScale;

implementation

uses Un1, MDB, Func, u2fdk, EditPlu, MT, MainMCryst;

{$R *.dfm}

procedure TfmScale.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmScale.FormCreate(Sender: TObject);
begin
  GridScale.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewScale.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
//  Memo1.Clear;
end;

procedure TfmScale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewScale.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  if not cxButton3.Enabled then Action := caNone;
end;

procedure TfmScale.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmMC do
  begin
    ViewScale.BeginUpdate;
    if quScale.RecordCount>0 then
    begin
      quScaleItems.Active:=False;
      quScaleItems.ParamByName('SNUM').AsString:=quScaleNumber.AsString;
      quScaleItems.Active:=True;  
    end;
    ViewScale.EndUpdate;
  end;
end;

procedure TfmScale.cxButton4Click(Sender: TObject);
begin
  with dmMC do
  begin
    if (quScale.RecordCount>0)and(cxLookupComboBox1.EditValue>'')then
    begin
      ViewScale.BeginUpdate;
      if quScale.RecordCount>0 then
      begin
        quScaleItems.Active:=False;
        quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
        quScaleItems.Active:=True;
      end;
      ViewScale.EndUpdate;
    end;
  end;
end;

procedure TfmScale.cxButton1Click(Sender: TObject);
Var iL,iP,n,iC:INteger;
    StrIp:String;
    bErr:Boolean;
    bFormat:byte;
    ft,fi:TextFile;
    fName,sBar:String;
begin
  with dmMc do
  begin
    Memo1.Clear;    //�������� ���������

    quScale.Locate('Number',cxLookupComboBox1.EditValue,[]);

    ViewScale.BeginUpdate;
    prButton(False);
    bErr:=False;

    if (quScaleModel.AsString='DIGI_TCP') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (DIGI).'); delay(10);

      quScaleItems.Active:=False;  //�������� �� ������ ������ �� ������
      quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP); delay(10);
          try
            iL:=0;
            quScaleItems.First;
            while not quScaleItems.Eof do
            begin
              if quScaleItemsStatus.AsInteger<>1 then
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  bFormat:=quScaleItemsMessageNo.AsInteger;
                  AddPlu(iL,quScaleItemsPLU.AsInteger,quScaleItemsShelfLife.AsInteger,quScaleItemsGoodsItem.AsString,AnsiToOemConvert(quScaleItemsFirstName.AsString),AnsiToOemConvert(quScaleItemsLastName.AsString),quScaleItemsrPr.asfloat,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                end;
              end;
              quScaleItems.Next;
            end;

            if iL>0 then
            begin
              if LoadScale(CommonSet.sMask+StrIP,iL) then  Memo1.Lines.Add('      ....')
              else
              begin
                bErr:=True;
                Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
              end;
              delay(10);
            end;
          except
          end;
        end; //����� IP
      end;
    end;

    if (quScaleModel.AsString='MT') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (MT).'); delay(10);
      prButton(False);

      quScaleItems.Active:=False;  //�������� �� ������ ������ �� ������
      quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      //��������� ���� ��� �������� �����
      try
        fName:=CurDir+'Plu'+DelP(CommonSet.Ip)+'.txt';
        assignfile(ft,fName);
        Rewrite(ft);

        iC:=0;
        quScaleItems.First;
        while not quScaleItems.Eof do
        begin
          if quScaleItemsStatus.AsInteger<>1 then
          begin
            sBar:=quScaleItemsGoodsItem.AsString;
            while length(sBar)<5 do sBar:='0'+sBar;
            sBar:='22'+sBar;
            writeln(ft,its(quScaleItemsPLU.AsInteger)+','+sBar+',1,'+fs(quScaleItemsrPr.asfloat)+',0,0,0,0,'+its(quScaleItemsShelfLife.AsInteger)+',0,0,0,0,'+DelPoint(AnsiToOemConvert(quScaleItemsFirstName.AsString))+','+DelPoint(AnsiToOemConvert(quScaleItemsLastName.AsString)));
            inc(iC);
          end;  
          quScaleItems.Next;
        end;
      finally
        closefile(ft);
      end;

      if iC>0 then //���� ��� �������� ����� �����������
      begin
        //������ ������� Transscale.ini
        try
          assignfile(fi,CurDir+'Transscale.ini');
          Rewrite(fi);
          writeln(fi,'Plu'+DelP(CommonSet.Ip)+'.txt');

          for n:=1 to 5 do
          begin
            StrIp:='';
            if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
            if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
            if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
            if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
            if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
            iP:=StrToINtDef(StrIp,0);

            if iP>0 then //���-�� ���������� - ������
              if n>1 then  writeln(fi,its(cxLookupComboBox1.EditValue)+its(n))
              else writeln(fi,its(cxLookupComboBox1.EditValue))
          end;
        finally
          closefile(fi);
        end;
        //��������� ��������

        Memo1.Lines.Add('��������');
        //��������� ��������
        if FileExists(curdir+'TransferPLU.exe') then
        begin
          ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','', SW_SHOWNORMAL);
//          ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','',SW_HIDE);
        end else
          Memo1.Lines.Add('�� ���� ����� TransferPLU.exe. ���������� � ��������������.');
//        Memo1.Lines.Add('�������� ���������');
      end;
    end;


    //����� ������ ����������
    if not bErr then
    begin //����� ������ ����������
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "ScalePLU" Set ');
      quE.SQL.Add('Status=1');
      quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and Status=0');
      quE.ExecSQL;
    end;

    quScaleItems.Active:=False;
    quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;

    prButton(True);
    Memo1.Lines.Add('�������� ����� ���������.');
  end;
end;

procedure TfmScale.prButton(bSt:Boolean);
begin
  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  cxButton3.Enabled:=bSt;
  cxButton4.Enabled:=bSt;
  cxButton5.Enabled:=bSt;
  cxButton6.Enabled:=bSt;
  cxButton7.Enabled:=bSt;
  cxButton8.Enabled:=bSt;
end;

procedure TfmScale.cxButton2Click(Sender: TObject);
Var iL,iP,n,i,j:INteger;
    StrIp:String;
    Rec:TcxCustomGridRecord;
    iPlu,iSrok,iC:INteger;
    sItem,Name1,Name2:String;
    rPrice:Real;
    bErr:Boolean;
    bFormat:byte;
    ft,fi:TextFile;
    fName,sBar:String;
begin
  with dmMc do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;

    Memo1.Clear;    //�������� ���������

    ViewScale.BeginUpdate;
    prButton(False);
    bErr:=False;

    quScale.Locate('Number',cxLookupComboBox1.EditValue,[]);
    if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �����.'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP+' ���-�� ������� '+INtToStr(ViewScale.Controller.SelectedRecordCount)); delay(10);
          try
            iL:=0;

            for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewScale.Controller.SelectedRecords[i];

              iPlu:=0;
              iSrok:=0;
              sItem:='';
              Name1:='';
              Name2:='';
              rPrice:=0;
              bFormat:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleGoodsItem' then sItem:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleFirstName' then Name1:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleLastName' then Name2:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScalePrice' then rPrice:=Rec.Values[j];
                if ViewScale.Columns[j].Name='ViewScaleMessageNo' then bFormat:=Rec.Values[j];
              end;

              if iPlu>0 then
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  AddPlu(iL,iPlu,iSrok,sItem,AnsiToOemConvert(Name1),AnsiToOemConvert(Name2),rPrice,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                  delay(10);
                end;
              end;
            end;

            if iL>0 then
            begin
              if LoadScale(CommonSet.sMask+StrIP,iL) then  Memo1.Lines.Add('      ....')
              else
              begin
                bErr:=True;
                Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
              end;
              delay(10);
            end;
          except
            ViewScale.EndUpdate;
          end;
        end; //����� IP
      end;

    end;


    if (quScaleModel.AsString='MT') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (MT).'); delay(10);
                  //��������� ���� ��� �������� �����
      try
        fName:=CurDir+'Plu'+DelP(CommonSet.Ip)+'.txt';
        assignfile(ft,fName);
        Rewrite(ft);

        iC:=0;

        for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewScale.Controller.SelectedRecords[i];

          iPlu:=0;
          iSrok:=0;
          sItem:='';
          Name1:='';
          Name2:='';
          rPrice:=0;
          bFormat:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScaleGoodsItem' then sItem:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScaleFirstName' then Name1:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScaleLastName' then Name2:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScalePrice' then rPrice:=Rec.Values[j];
            if ViewScale.Columns[j].Name='ViewScaleMessageNo' then bFormat:=Rec.Values[j];
          end;

          if iPlu>0 then
          begin
            sBar:=sItem;
            while length(sBar)<5 do sBar:='0'+sBar;
            sBar:='22'+sBar;
            writeln(ft,its(iPlu)+','+sBar+',1,'+fs(rPrice)+',0,0,0,0,'+its(iSrok)+',0,0,0,0,'+DelPoint(AnsiToOemConvert(Name1))+','+DelPoint(AnsiToOemConvert(Name2)));
            inc(iC);
          end;
        end;
      finally
        closefile(ft);
      end;

      if iC>0 then //���� ��� �������� ����� �����������
      begin
        //������ ������� Transscale.ini
        try
          assignfile(fi,CurDir+'Transscale.ini');
          Rewrite(fi);
          writeln(fi,'Plu'+DelP(CommonSet.Ip)+'.txt');

          for n:=1 to 5 do
          begin
            StrIp:='';
            if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
            if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
            if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
            if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
            if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
            iP:=StrToINtDef(StrIp,0);

            if iP>0 then //���-�� ���������� - ������
              if n>1 then  writeln(fi,its(cxLookupComboBox1.EditValue)+its(n))
              else writeln(fi,its(cxLookupComboBox1.EditValue))
          end;
        finally
          closefile(fi);
        end;
        //��������� ��������

        Memo1.Lines.Add('��������');
        //��������� ��������
        if FileExists(curdir+'TransferPLU.exe') then
        begin
          ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','', SW_SHOWNORMAL);
//          ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','',SW_HIDE);
        end else
          Memo1.Lines.Add('�� ���� ����� TransferPLU.exe. ���������� � ��������������.');
//        Memo1.Lines.Add('�������� ���������');
      end;
    end;

    if bErr=False then
    begin //����� ������ ����������
      for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewScale.Controller.SelectedRecords[i];

        iPlu:=0;
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewScale.Columns[j].Name='ViewScalePLU' then begin iPlu:=Rec.Values[j]; break; end;
        end;

        if iPlu>0 then
        begin
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "ScalePLU" Set ');
          quE.SQL.Add('Status=1');
          quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(iPlu));
          quE.ExecSQL;
        end;
      end;
    end;

    quScaleItems.Active:=False;
    quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;
    prButton(True);
    Memo1.Lines.Add('�������� ����� ���������.');

  end;
end;

procedure TfmScale.acEditPluExecute(Sender: TObject);
begin
  //������������� ���
  with dmMC do
  begin
    if quScaleItems.RecordCount>0 then
    begin
      fmEditPlu:=tfmEditPlu.Create(Application);
      fmEditPlu.cxSpinEdit1.EditValue:=quScaleItemsPLU.AsInteger;
      fmEditPlu.cxSpinEdit2.EditValue:=quScaleItemsGoodsItem.AsInteger;
      fmEditPlu.cxTextEdit2.Text:=quScaleItemsFirstName.AsString;
      fmEditPlu.cxTextEdit3.Text:=quScaleItemsLastName.AsString;
      fmEditPlu.cxSpinEdit3.EditValue:=quScaleItemsShelfLife.AsInteger;
      fmEditPlu.cxCalcEdit1.EditValue:=quScaleItemsrPr.AsFloat;
      fmEditPlu.cxComboBox1.ItemIndex:=quScaleItemsMessageNo.AsInteger;
      fmEditPlu.ShowModal;
      if fmEditPlu.ModalResult=mrOk then
      begin //���������
          //�������
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "ScalePLU" Set ');
        quE.SQL.Add('GoodsItem='+its(fmEditPlu.cxSpinEdit2.EditValue)+',');
        quE.SQL.Add('FirstName='''+fmEditPlu.cxTextEdit2.Text+''',');
        quE.SQL.Add('LastName='''+fmEditPlu.cxTextEdit3.Text+''',');
        quE.SQL.Add('Price='+its(RoundEx(fmEditPlu.cxCalcEdit1.EditValue*100))+',');
        quE.SQL.Add('ShelfLife='+its(fmEditPlu.cxSpinEdit3.EditValue)+',');
        quE.SQL.Add('MessageNo='+its(fmEditPlu.cxComboBox1.ItemIndex)+',');
        quE.SQL.Add('Status=0');
        quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(fmEditPlu.cxSpinEdit1.EditValue));
        quE.ExecSQL;
      end;
      ViewScale.BeginUpdate;
      quScaleItems.Active:=False;
      quScaleItems.Active:=True;
      ViewScale.EndUpdate;
      quScaleItems.Locate('PLU',fmEditPlu.cxSpinEdit1.EditValue,[]);

      fmEditPlu.Release;
    end;
  end;
end;

procedure TfmScale.cxButton5Click(Sender: TObject);
begin
  prNExportExel5(ViewScale);
end;

procedure TfmScale.cxButton6Click(Sender: TObject);
Var iC,l:Integer;
    sN1,sN2,sN,s:String;
    bE:Boolean;
begin
  with dmMc do
  with dmMt do
  begin
    Memo1.Clear;    //�������� ���������

    Memo1.Lines.Add('����� ... ���� ���������� ���������� � ������ �� ��������.'); delay(10);
    ViewScale.BeginUpdate;

    prButton(False);

    if taCards.Active=False then taCards.Active:=True;

    quScaleItems.Active:=False;  //�������� �� ������ ������ �� ������
    quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;
    iC:=0;
    quScaleItems.First;
    while not quScaleItems.Eof do
    begin
      if taCards.FindKey([quScaleItemsGoodsItem.AsInteger]) then
      begin
        sN:=OemToAnsiConvert(taCardsFullName.AsString);
        sN1:=Copy(sN,1,29);
//        iFormat:=taCardsV09.AsInteger;
        l:=28;
        if (Pos(' ',sN1)>0) and(length(sN)>28) then
        begin
          s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
          while s<>' ' do
          begin
            dec(l);
            s:=sN1[l];
          end;
        end;

        sN1:=Copy(sN,1,l);
        if Length(sN)>l then sN2:=Copy(sN,(l+1),28) else sN2:='';

        bE:=False;

        sN1:=TrimRight(sN1);
        sN2:=TrimRight(sN2);

//        if sN1[Length(sN1)]=' ' then delete(sN1,Length(sN1),1);
//        if sN2[Length(sN2)]=' ' then delete(sN2,Length(sN2),1);

        if abs(quScaleItemsrPr.AsFloat-taCardsCena.AsFloat)>=0.01 then bE:=True;
        if taCardsSrokReal.AsInteger<>quScaleItemsShelfLife.AsInteger then bE:=True;
        if sN1<>quScaleItemsFirstName.AsString then bE:=True;
        if sN2<>quScaleItemsLastName.AsString then bE:=True;
//        if iFormat<>quScaleItemsMessageNo.AsInteger then bE:=True;

        if bE then
        begin //���� ����������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "ScalePLU" Set ');
          quE.SQL.Add('Price='+its(RoundEx(taCardsCena.AsFloat*100))+',');
          quE.SQL.Add('ShelfLife='+its(taCardsSrokReal.AsInteger)+',');
          quE.SQL.Add('Status=0,');
          quE.SQL.Add('FirstName='''+sN1+''',');
          quE.SQL.Add('LastName='''+sN2+'''');
//          quE.SQL.Add('MessageNo='+its(taCardsV09.AsInteger)); //������ �������
          quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(quScaleItemsPLU.AsInteger));
          quE.ExecSQL;
          delay(30);
        end;
      end else
      begin
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "ScalePLU" Set ');
        quE.SQL.Add('FirstName=''�� ������������'',');
        quE.SQL.Add('LastName='''',');
        quE.SQL.Add('Price=0,');
        quE.SQL.Add('ShelfLife=0,');
        quE.SQL.Add('Status=0');
        quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(quScaleItemsPLU.AsInteger));
        quE.ExecSQL;
        delay(30);
      end;


{      quFindC4.Active:=False;
      quFindC4.ParamByName('IDC').AsInteger:=quScaleItemsGoodsItem.AsInteger;
      quFindC4.Active:=True;
      if quFindC4.RecordCount>0 then
      begin
        if (abs(quScaleItemsrPr.AsFloat-quFindC4Cena.AsFloat)>=0.01)
        or (quFindC4SrokReal.AsInteger<>quScaleItemsShelfLife.AsInteger) then
        begin //���� ����������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "ScalePLU" Set ');
          quE.SQL.Add('Price='+its(RoundEx(quFindC4Cena.AsFloat*100))+',');
          quE.SQL.Add('ShelfLife='+its(quFindC4SrokReal.AsInteger)+',');
          quE.SQL.Add('Status=0');
          quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(quScaleItemsPLU.AsInteger));
          quE.ExecSQL;
          delay(30);
        end;
      end else
      begin
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "ScalePLU" Set ');
        quE.SQL.Add('FirstName=''�� ������������'',');
        quE.SQL.Add('LastName='''',');
        quE.SQL.Add('Price=0,');
        quE.SQL.Add('ShelfLife=0,');
        quE.SQL.Add('Status=0');
        quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(quScaleItemsPLU.AsInteger));
        quE.ExecSQL;
        delay(30);

      end;
      quFindC4.Active:=False;
      }
      quScaleItems.Next; inc(iC);
      if iC mod 100 = 0 then
      begin
        Memo1.Lines.Add('  ���������� - '+its(iC)); delay(100);
      end;
    end;

    quScaleItems.Active:=False;
    quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
    quScaleItems.Active:=True;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;

    prButton(True);
    Memo1.Lines.Add('���������� ���������.');
  end;
end;

procedure TfmScale.cxButton7Click(Sender: TObject);
Var iL,iP,n,i,j:INteger;
    StrIp:String;
    Rec:TcxCustomGridRecord;
    iPlu,iSrok:INteger;
    sItem,Name1,Name2:String;
    rPrice:Real;
    bErr:Boolean;
    bFormat:byte;
begin
   //�������
  with dmMc do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;

    Memo1.Clear;    //�������� ���������

    quScale.Locate('Number',cxLookupComboBox1.EditValue,[]);
    if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �� �����.'); delay(10);
      ViewScale.BeginUpdate;
      prButton(False);
      bErr:=False;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP+' ���-�� ������� '+INtToStr(ViewScale.Controller.SelectedRecordCount)); delay(10);
          try
            iL:=0;

            for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewScale.Controller.SelectedRecords[i];

              iPlu:=0;
              iSrok:=0;
              sItem:='';
              Name1:='';
              Name2:='';
              rPrice:=0;
              bFormat:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewScale.Columns[j].Name='ViewScalePLU' then iPlu:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleShelfLife' then iSrok:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleGoodsItem' then sItem:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleFirstName' then Name1:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScaleLastName' then Name2:=Rec.Values[j];
//                if ViewScale.Columns[j].Name='ViewScalePrice' then rPrice:=Rec.Values[j];
              end;

              if iPlu>0 then
              begin
                if iL<100 then
                begin
                  inc(iL); //1-100
                  AddPlu(iL,iPlu,iSrok,sItem,AnsiToOemConvert(Name1),AnsiToOemConvert(Name2),rPrice,bFormat);
                end else //=100
                begin
                  iL:=0;
                  if LoadScale(CommonSet.sMask+StrIP,100) then  Memo1.Lines.Add('      ....')
                  else
                  begin
                    bErr:=True;
                    Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
                  end;
                  delay(10);
                end;
              end;
            end;

            if iL>0 then
            begin
              if LoadScale(CommonSet.sMask+StrIP,iL) then  Memo1.Lines.Add('      ....')
              else
              begin
                bErr:=True;
                Memo1.Lines.Add('      ������ ��� ��������. ���������, ��� ���� ��������.');
              end;
              delay(10);
            end;
          except
            ViewScale.EndUpdate;
          end;
        end; //����� IP
      end;
      if bErr=False then
      begin //����� ������ ����������  -  ������� �� ����� �� ������
        for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewScale.Controller.SelectedRecords[i];

          iPlu:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewScale.Columns[j].Name='ViewScalePLU' then begin iPlu:=Rec.Values[j]; break; end;
          end;

          if iPlu>0 then
          begin
{            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "ScalePLU" Set ');
            quE.SQL.Add('Status=1');
            quE.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(iPlu));
            quE.ExecSQL;}

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "ScalePLU"');
            quD.SQL.Add('where Number='''+cxLookupComboBox1.EditValue+''' and PLU='+its(iPlu));
            quD.ExecSQL;

          end;
        end;
      end;

      quScaleItems.Active:=False;
      quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      ViewScale.EndUpdate;
      ViewScale.Controller.ClearSelection;
      prButton(True);
      Memo1.Lines.Add('�������� ���������.');
    end;
  end;
end;

procedure TfmScale.cxButton8Click(Sender: TObject);
Var iP,n:INteger;
    StrIp:String;
    bErr:Boolean;
    F:TextFile;
    StrErr:String;
begin
  with dmMc do
  begin
    Memo1.Clear;    //�������� �������� �������� � ��������� ����

    quScale.Locate('Number',cxLookupComboBox1.EditValue,[]);
    if quScaleModel.AsString='DIGI_TCP' then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �������� � ����.'); delay(10);
      prButton(False);
      bErr:=False;

      quScaleItems.Active:=False;  //�������� �� ������ ������ �� ������
      quScaleItems.ParamByName('SNUM').AsString:=cxLookupComboBox1.EditValue;
      quScaleItems.Active:=True;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=IntToIp(quScaleRezerv1.AsInteger);
        if n=2 then StrIP:=IntToIp(quScaleRezerv2.AsInteger);
        if n=3 then StrIP:=IntToIp(quScaleRezerv3.AsInteger);
        if n=4 then StrIP:=IntToIp(quScaleRezerv4.AsInteger);
        if n=5 then StrIP:=IntToIp(quScaleRezerv5.AsInteger);
        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+CommonSet.sMask+StrIP); delay(10);
          try
            if FileExists(CurDir+'twswtcp.exe') then
            begin
              Memo1.Lines.Add('  ������� "twswtcp.exe" ������.'); delay(10);
              if FileExists(CommonSet.TrfPath+'f34_1.dat') then
              begin
                Memo1.Lines.Add('  ������ "'+CommonSet.TrfPath+'f34_1.dat'+'" ������.'); delay(10);
                if FileExists(CommonSet.TrfPath+'f34_2.dat') then
                begin
                  Memo1.Lines.Add('  ������ "'+CommonSet.TrfPath+'f34_2.dat'+'" ������.'); delay(10);

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIp+'f34.dat');

                  Memo1.Lines.Add('  �������� "'+CommonSet.TrfPath+'f34_1.dat'+'" � '+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  CopyFile(PChar(CommonSet.TrfPath+'f34_1.dat'),PChar(CurDir+'sm'+StrIp+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIp),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIp+'f34.dat');
                  Memo1.Lines.Add('  �������� "'+CommonSet.TrfPath+'f34_2.dat'+'" � '+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  CopyFile(PChar(CommonSet.TrfPath+'f34_2.dat'),PChar(CurDir+'sm'+StrIp+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIp),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                end else
                begin
                  Memo1.Lines.Add('  ������ - ������ "'+CommonSet.TrfPath+'f34_2.dat'+'" �� ������.'); delay(10);
                  bErr:=True;
                end;
              end else
              begin
                Memo1.Lines.Add('  ������ - ������ "'+CommonSet.TrfPath+'f34_1.dat'+'" �� ������.'); delay(10);
                bErr:=True;
              end;
            end else
            begin
              Memo1.Lines.Add('  ������ - ������� "'+CurDir+'twswtcp.exe" �� ������.'); delay(10);
              bErr:=True;
            end;
          except
            bErr:=True;
          end;
        end; //����� IP
      end;
//      Memo1.Lines.Add('');
      prButton(True);
      if bErr=False then
        Memo1.Lines.Add('�������� �������� �������� ���������.')
      else
        Memo1.Lines.Add('������ ��� ��������� �������� ��������.');
    end;
  end;
end;




procedure TfmScale.cxButton9Click(Sender: TObject);
begin
  Memo1.Lines.Add('��������');
  //��������� ��������
  try
    if FileExists(curdir+'TransferPLU.exe') then
    begin
      ShellExecute(handle, 'open', PChar('TransferPLU.exe'),'','', SW_SHOWNORMAL);
    end else
      showmessage('�� ���� ����� TransferPLU.exe. ���������� � ��������������.');
  finally
  end;
  Memo1.Lines.Add('�������� ���������');
end;

procedure TfmScale.cxButton10Click(Sender: TObject);
begin
  //�������� � ������������� ����� QLOAD



end;

end.
