unit AlcoSp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Placemnt, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMemo, cxMaskEdit, cxButtonEdit, StdCtrls, cxDropDownEdit, cxCalendar,
  cxGraphics, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxmdaset, Menus,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  DBClient, cxProgressBar, cxSpinEdit, Excel2000, OleServer, ExcelXP, ComObj;

type
  TfmAddAlco = class(TForm)
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Memo1: TcxMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    Label4: TLabel;
    cxComboBox1: TcxComboBox;
    Panel2: TPanel;
    ViAlco1: TcxGridDBTableView;
    LeAlco1: TcxGridLevel;
    GrAddAlco: TcxGrid;
    cxLookupComboBox1: TcxLookupComboBox;
    LeAlco2: TcxGridLevel;
    ViAlco2: TcxGridDBTableView;
    teAlcoOb: TdxMemData;
    teAlcoIn: TdxMemData;
    dsteAlcoOb: TDataSource;
    dsteAlcoIn: TDataSource;
    teAlcoObiDep: TIntegerField;
    teAlcoObsDep: TStringField;
    teAlcoObiVid: TIntegerField;
    teAlcoObsVid: TStringField;
    teAlcoObsVidName: TStringField;
    teAlcoObiProd: TIntegerField;
    teAlcoObsProd: TStringField;
    teAlcoObsProdInn: TStringField;
    teAlcoObsProdKPP: TStringField;
    teAlcoObrQb: TFloatField;
    teAlcoObrQIn1: TFloatField;
    teAlcoObrQIn2: TFloatField;
    teAlcoObrQIn3: TFloatField;
    teAlcoObrQInIt1: TFloatField;
    teAlcoObrQIn4: TFloatField;
    teAlcoObrQIn5: TFloatField;
    teAlcoObrQIn6: TFloatField;
    teAlcoObrQInIt: TFloatField;
    teAlcoObrQOut1: TFloatField;
    teAlcoObrQOut2: TFloatField;
    teAlcoObrQOut3: TFloatField;
    teAlcoObrQOut4: TFloatField;
    teAlcoObrQOutIt: TFloatField;
    teAlcoObrQe: TFloatField;
    ViAlco1iDep: TcxGridDBColumn;
    ViAlco1sDep: TcxGridDBColumn;
    ViAlco1iVid: TcxGridDBColumn;
    ViAlco1sVid: TcxGridDBColumn;
    ViAlco1sVidName: TcxGridDBColumn;
    ViAlco1iProd: TcxGridDBColumn;
    ViAlco1sProd: TcxGridDBColumn;
    ViAlco1sProdInn: TcxGridDBColumn;
    ViAlco1sProdKPP: TcxGridDBColumn;
    ViAlco1rQb: TcxGridDBColumn;
    ViAlco1rQIn1: TcxGridDBColumn;
    ViAlco1rQIn2: TcxGridDBColumn;
    ViAlco1rQIn3: TcxGridDBColumn;
    ViAlco1rQInIt1: TcxGridDBColumn;
    ViAlco1rQIn4: TcxGridDBColumn;
    ViAlco1rQIn5: TcxGridDBColumn;
    ViAlco1rQIn6: TcxGridDBColumn;
    ViAlco1rQInIt: TcxGridDBColumn;
    ViAlco1rQOut1: TcxGridDBColumn;
    ViAlco1rQOut2: TcxGridDBColumn;
    ViAlco1rQOut3: TcxGridDBColumn;
    ViAlco1rQOut4: TcxGridDBColumn;
    ViAlco1rQOutIt: TcxGridDBColumn;
    ViAlco1rQe: TcxGridDBColumn;
    teAlcoIniDep: TIntegerField;
    teAlcoInsDep: TStringField;
    teAlcoIniVid: TIntegerField;
    teAlcoInsVid: TStringField;
    teAlcoInsVidName: TStringField;
    teAlcoIniProd: TIntegerField;
    teAlcoInsProd: TStringField;
    teAlcoInsProdInn: TStringField;
    teAlcoInsProdKPP: TStringField;
    teAlcoIniPost: TIntegerField;
    teAlcoInsPost: TStringField;
    teAlcoInsPostInn: TStringField;
    teAlcoInsPostKpp: TStringField;
    teAlcoIniLic: TIntegerField;
    teAlcoInsLicNumber: TStringField;
    teAlcoInLicDateB: TDateField;
    teAlcoInLicDateE: TDateField;
    teAlcoInLicOrg: TStringField;
    teAlcoInDocDate: TDateField;
    teAlcoInDocNum: TStringField;
    teAlcoInGTD: TStringField;
    teAlcoInrQIn: TFloatField;
    ViAlco2iDep: TcxGridDBColumn;
    ViAlco2sDep: TcxGridDBColumn;
    ViAlco2iVid: TcxGridDBColumn;
    ViAlco2sVid: TcxGridDBColumn;
    ViAlco2sVidName: TcxGridDBColumn;
    ViAlco2iProd: TcxGridDBColumn;
    ViAlco2sProd: TcxGridDBColumn;
    ViAlco2sProdInn: TcxGridDBColumn;
    ViAlco2sProdKPP: TcxGridDBColumn;
    ViAlco2iPost: TcxGridDBColumn;
    ViAlco2sPost: TcxGridDBColumn;
    ViAlco2sPostInn: TcxGridDBColumn;
    ViAlco2sPostKpp: TcxGridDBColumn;
    ViAlco2iLic: TcxGridDBColumn;
    ViAlco2sLicNumber: TcxGridDBColumn;
    ViAlco2LicDateB: TcxGridDBColumn;
    ViAlco2LicDateE: TcxGridDBColumn;
    ViAlco2LicOrg: TcxGridDBColumn;
    ViAlco2DocDate: TcxGridDBColumn;
    ViAlco2DocNum: TcxGridDBColumn;
    ViAlco2GTD: TcxGridDBColumn;
    ViAlco2rQIn: TcxGridDBColumn;
    Label5: TLabel;
    cxComboBox2: TcxComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    amAddAlco: TActionManager;
    acGetRemn1: TAction;
    acGetIn: TAction;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    acGetOutR: TAction;
    cxButton7: TcxButton;
    acGetRet: TAction;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    acExpExls: TAction;
    acExpXML: TAction;
    cxButton10: TcxButton;
    OpenDialog1: TOpenDialog;
    teProds: TdxMemData;
    teProdsiProd: TIntegerField;
    teProdssProd: TStringField;
    teProdssProdInn: TStringField;
    teProdssProdKPP: TStringField;
    tePost: TdxMemData;
    tePostiPost: TIntegerField;
    tePostsPost: TStringField;
    tePostsPostInn: TStringField;
    tePostsPostKpp: TStringField;
    tePostiLic: TIntegerField;
    tePostLicNumber: TStringField;
    tePostLicDateB: TDateField;
    tePostLicDateE: TDateField;
    tePostLicOrg: TStringField;
    teA1: TClientDataSet;
    teA1iDep: TIntegerField;
    teA1sDep: TStringField;
    teA1iVid: TIntegerField;
    teA1sVidName: TStringField;
    teA1iProd: TIntegerField;
    teA1rQb: TFloatField;
    teA1rQIn1: TFloatField;
    teA1rQIn2: TFloatField;
    teA1rQIn3: TFloatField;
    teA1rQInIt1: TFloatField;
    teA1rQIn4: TFloatField;
    teA1rQIn5: TFloatField;
    teA1rQIn6: TFloatField;
    teA1rQInIt: TFloatField;
    teA1rQOut1: TFloatField;
    teA1rQOut2: TFloatField;
    teA1rQOut3: TFloatField;
    teA1rQOut4: TFloatField;
    teA1rQOutIt: TFloatField;
    teA1rQe: TFloatField;
    teA2: TClientDataSet;
    teA2iDep: TIntegerField;
    teA2iVid: TIntegerField;
    teA2iProd: TIntegerField;
    teA2iPost: TIntegerField;
    teA2DocDate: TDateTimeField;
    teA2DocNum: TStringField;
    teA2rQIn: TFloatField;
    dsteA1: TDataSource;
    teA2GTD: TStringField;
    teA2iLic: TIntegerField;
    acGenGUID: TAction;
    teLic: TdxMemData;
    teLiciPost: TIntegerField;
    teLiciLic: TIntegerField;
    teLicLicNumber: TStringField;
    teLicLicDateB: TDateField;
    teLicLicDateE: TDateField;
    teLicLicOrg: TStringField;
    acGetMove: TAction;
    teAlcoInDocId: TIntegerField;
    acRecalc: TAction;
    cxButton11: TcxButton;
    Label6: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label7: TLabel;
    cxSpinEdit2: TcxSpinEdit;
    PBar1: TcxProgressBar;
    acRecalcLic: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acInputPost: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acExpExlsExecute(Sender: TObject);
    procedure acExpXMLExecute(Sender: TObject);
    procedure acGenGUIDExecute(Sender: TObject);
    procedure acGetMoveExecute(Sender: TObject);
    procedure acRecalcExecute(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure acRecalcLicExecute(Sender: TObject);
    procedure acInputPostExecute(Sender: TObject);
  private
    { Private declarations }
    procedure prButtons(bStr:Boolean);
  public
    { Public declarations }
  end;

function prFindProd(sInn,sKpp,sName:String):Integer;
function prFindLic(iCli:INteger;dDateB,dDateE:TDateTime;sOrg,sLicNum:String):INteger;

var
  fmAddAlco: TfmAddAlco;

implementation

uses Un1, DBAlg, MDB, MT, AlcoHd, u2fdk, SelPost;

{$R *.dfm}

function prFindLic(iCli:INteger;dDateB,dDateE:TDateTime;sOrg,sLicNum:String):INteger;
begin
  with dmMC do
  with dmMt do
  begin
    Result:=0;

    quF.Active:=False;
    quF.SQL.Clear;
    quF.SQL.Add('select * from "A_CLIENTSALG"');
    quF.SQL.Add('where ITYPE=1');
    quF.SQL.Add('and ICLI='+its(iCli));
    quF.SQL.Add('and IDATEB='+its(Trunc(dDateB)));
    quF.SQL.Add('and IDATEE='+its(Trunc(dDateE)));
    quF.Active:=True;
    quF.First;
    if quF.RecordCount>0 then Result:=0 else
    begin //�� ����� -  ���������
      try
        if ptCliLic.Active=False then ptCliLic.Active:=True;
        ptCliLic.CancelRange;
        ptCliLic.SetRange([1,iCli],[1,iCli]);

        ptCliLic.Append;
        ptCliLicITYPE.AsInteger:=1;
        ptCliLicICLI.AsInteger:=iCli;
        ptCliLicIDATEB.AsInteger:=Trunc(dDateB);
        ptCliLicDDATEB.AsDateTime:=dDateB;
        ptCliLicIDATEE.AsInteger:=Trunc(dDateE);
        ptCliLicDDATEE.AsDateTime:=dDateE;
        ptCliLicSER.AsString:=AnsiToOemConvert(sLicNum);
        ptCliLicSNUM.AsString:='';
        ptCliLicORGAN.AsString:=AnsiToOemConvert(sOrg);
        ptCliLicIDCLI.AsInteger:=0;
        ptCliLic.Post;
      except
      end;
    end;
    quF.Active:=False;
  end;
end;

function prFindProd(sInn,sKpp,sName:String):Integer;
Var iMax:Integer;
begin
  with dmMC do
  with dmMt do
  begin
    quF.Active:=False;
    quF.SQL.Clear;
    quF.SQL.Add('select * from "A_MAKER"');
    quF.SQL.Add('where INNM='''+sInn+''' and KPPM='''+sKpp+'''');
    quF.Active:=True;
    quF.First;
    if quF.RecordCount>0 then Result:=quF.FieldByName('ID').AsInteger else
    begin //�� ����� -  ���������
      iMax:=prMax('Maker')+1;
      if ptMakers.Active=False then ptMakers.Active:=True;

      ptMakers.Append;
      ptMakersID.AsInteger:=iMax;
      ptMakersNAMEM.AsString:=AnsiToOemConvert(sName);
      ptMakersINNM.AsString:=AnsiToOemConvert(sInn);
      ptMakersKPPM.AsString:=AnsiToOemConvert(sKpp);
      ptMakers.Post;

      result:=iMax;
    end;
    quF.Active:=False;
  end;
end;


procedure TfmAddAlco.prButtons(bStr:Boolean);
begin
  cxButton1.Enabled:=bStr;
  cxButton2.Enabled:=bStr;
  cxButton3.Enabled:=bStr;
  cxButton4.Enabled:=bStr;
  cxButton5.Enabled:=bStr;
  cxButton6.Enabled:=bStr;
  cxButton7.Enabled:=bStr;
  cxButton8.Enabled:=bStr;
  cxButton9.Enabled:=bStr;
  cxButton10.Enabled:=bStr;
  delay(10);
end;

procedure TfmAddAlco.FormCreate(Sender: TObject);
begin
  GrAddAlco.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Memo1.Clear;
end;

procedure TfmAddAlco.cxButton4Click(Sender: TObject);
begin
  if cxButton1.Enabled then close;
end;

procedure TfmAddAlco.cxButton1Click(Sender: TObject);
Var IDH:INteger;
//    par:Variant;
begin
  with dmAlg do
  with dmMC do
  with dmMt do
  begin
    if MessageDlg('��������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ���������� ���������.'); delay(10);
      Memo1.Lines.Add('  - ���������'); delay(10);

      prButtons(False);

      if cxLookupComboBox1.Tag=0 then
      begin //����������
        IDH:=prMax('AlcoH')+1;

        cxLookupComboBox1.Tag:=IDH;

        if ptAlcoDHRec.Active=False then ptAlcoDHRec.Active:=True;

        ptAlcoDHRec.Append;
        ptAlcoDHRecId.AsInteger:=IDH;
        ptAlcoDHRecIdOrg.AsInteger:=cxLookupComboBox1.EditValue;
        ptAlcoDHRecIDateB.AsInteger:=Trunc(cxDateEdit1.Date);
        ptAlcoDHRecIDateE.AsInteger:=Trunc(cxDateEdit2.Date);
        ptAlcoDHRecDDateB.AsDateTime:=cxDateEdit1.Date;
        ptAlcoDHRecDDateE.AsDateTime:=cxDateEdit2.Date;
        ptAlcoDHRecIType.AsInteger:=cxComboBox1.ItemIndex*10+cxComboBox2.ItemIndex;
        ptAlcoDHRecsType.AsString:='';
        ptAlcoDHRecSPers.AsString:=AnsiToOemConvert(FormatDateTime('dd.mm.yy hh:nn',now)+' '+Person.Name);
        ptAlcoDHRec.Post;

      end else //��������������
      begin
        IDH:=cxLookupComboBox1.Tag;

        if ptAlcoDHRec.Active=False then ptAlcoDHRec.Active:=True;

        if ptAlcoDHRec.FindKey([IDH]) then
        begin
          ptAlcoDHRec.Edit;
          ptAlcoDHRecIdOrg.AsInteger:=cxLookupComboBox1.EditValue;
          ptAlcoDHRecIDateB.AsInteger:=Trunc(cxDateEdit1.Date);
          ptAlcoDHRecIDateE.AsInteger:=Trunc(cxDateEdit2.Date);
          ptAlcoDHRecDDateB.AsDateTime:=cxDateEdit1.Date;
          ptAlcoDHRecDDateE.AsDateTime:=cxDateEdit2.Date;
          ptAlcoDHRecIType.AsInteger:=cxComboBox1.ItemIndex*10+cxComboBox2.ItemIndex;
          ptAlcoDHRecsType.AsString:='';
          ptAlcoDHRecSPers.AsString:=AnsiToOemConvert(FormatDateTime('dd.mm.yy hh:nn',now)+' '+Person.Name);
          ptAlcoDHRec.Post;
        end;
      end;

      Memo1.Lines.Add('  - ������������ .'); delay(10);

      quDelDS.SQL.Clear;
      quDelDS.SQL.Add('Delete from "A_ALCGDS1"');
      quDelDS.SQL.Add('where IdH='+its(IDH));
      quDelDS.ExecSQL;

      delay(100);

      if ptAlcoDS1.Active=False then ptAlcoDS1.Active:=True;
      if ptAlcoDS2.Active=False then ptAlcoDS2.Active:=True;

      ptAlcoDS1.Refresh;

      ptAlcoDS1.CancelRange;
      ptAlcoDS1.SetRange([IDH],[IDH]);

      ViAlco1.BeginUpdate;
      teAlcoOb.First;        //����
      while not teAlcoOb.Eof do
      begin
        ptAlcoDS1.Append;
        ptAlcoDS1IdH.AsInteger:=IDH;
        ptAlcoDS1iDep.AsInteger:=teAlcoObiDep.AsInteger;
        ptAlcoDS1iNum.AsInteger:=0;
        ptAlcoDS1iVid.AsInteger:=teAlcoObiVid.AsInteger;
        ptAlcoDS1iProd.AsInteger:=teAlcoObiProd.AsInteger;
        ptAlcoDS1rQb.AsFloat:=teAlcoObrQb.AsFloat;
        ptAlcoDS1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
        ptAlcoDS1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
        ptAlcoDS1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
        ptAlcoDS1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
        ptAlcoDS1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
        ptAlcoDS1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
        ptAlcoDS1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
        ptAlcoDS1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
        ptAlcoDS1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
        ptAlcoDS1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
        ptAlcoDS1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
        ptAlcoDS1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
        ptAlcoDS1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
        ptAlcoDS1rQe.AsFloat:=teAlcoObrQe.AsFloat;
        ptAlcoDS1.Post;

        teAlcoOb.Next;
      end;

      teAlcoOb.First;

      ViAlco1.EndUpdate;


      Memo1.Lines.Add('  - ������������ ..'); delay(10);

      quDelDS.SQL.Clear;
      quDelDS.SQL.Add('Delete from "A_ALCGDS2"');
      quDelDS.SQL.Add('where IdH='+its(IDH));
      quDelDS.ExecSQL;

      delay(100);

      ptAlcoDS2.Refresh;

      ptAlcoDS2.CancelRange;
      ptAlcoDS2.SetRange([IDH],[IDH]);

      ViAlco2.BeginUpdate;
      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        ptAlcoDS2.Append;
        ptAlcoDS2IdH.AsInteger:=IDH;
        ptAlcoDS2iDep.AsInteger:=teAlcoIniDep.AsInteger;
        ptAlcoDS2iVid.AsInteger:=teAlcoIniVid.AsInteger;
        ptAlcoDS2iProd.AsInteger:=teAlcoIniProd.AsInteger;
        ptAlcoDS2iPost.AsInteger:=teAlcoIniPost.AsInteger;
        ptAlcoDS2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
        ptAlcoDS2DocNum.AsString:=AnsiToOemConvert(teAlcoInDocNum.AsString);
        ptAlcoDS2GTD.AsString:=AnsiToOemConvert(teAlcoInGTD.AsString);
        ptAlcoDS2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
        ptAlcoDS2iLic.AsInteger:=teAlcoIniLic.AsInteger;
        ptAlcoDS2LicNum.AsString:=AnsiToOemConvert(teAlcoInsLicNumber.AsString);
        ptAlcoDS2LicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
        ptAlcoDS2LicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
        ptAlcoDS2LicOrg.AsString:=AnsiToOemConvert(teAlcoInLicOrg.AsString);
        ptAlcoDS2IdHdr.AsInteger:=teAlcoInDocId.AsInteger;
        ptAlcoDS2.Post;

        teAlcoIn.Next;
      end;
      teAlcoIn.First;
      ViAlco2.EndUpdate;

      Memo1.Lines.Add('  - ����������'); delay(10);

      fmAlcoE.ViewAlco.BeginUpdate;
      quAlcoDH.Active:=False;
      quAlcoDH.ParamByName('IDATEB').Value:=Trunc(CommonSet.ADateBeg);
      quAlcoDH.ParamByName('IDATEE').Value:=Trunc(CommonSet.ADateEnd);
      quAlcoDH.Active:=True;
      fmAlcoE.ViewAlco.EndUpdate;

      quAlcoDH.Locate('Id',IDH,[]);

      Memo1.Lines.Add('C��������� Ok.'); delay(10);

      prButtons(True);
    end;
  end;
end;

procedure TfmAddAlco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=False then Action := caNone;
end;

procedure TfmAddAlco.cxButton2Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('  ������.'); delay(10);

    with fmAddAlco do
    begin
      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;
      CloseTe(teAlcoOb);
      CloseTe(teAlcoIn);
      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;
    end;

    Memo1.Lines.Add('  ������ ��.'); delay(10);

    prButtons(False);
    acGetRemn1.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton5Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetIn.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton6Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetOutR.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton7Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetRet.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton3Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    Memo1.Clear;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'  ������.'); delay(10);

    with fmAddAlco do
    begin
      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;
      CloseTe(teAlcoOb);
      CloseTe(teAlcoIn);
      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;
    end;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'  ������ ��.'); delay(10);

    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'������.'); delay(10);

    prButtons(False);
    acGetMove.Execute;
    prButtons(True);

    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'������ ��.'); delay(10);
  end;
end;

procedure TfmAddAlco.cxButton8Click(Sender: TObject);
begin
  // �����������
  with dmAlg do
  begin
    if MessageDlg('����������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� �������� ���������.'); delay(10);
      Memo1.Lines.Add('  - ������� ('+Its(teAlcoIn.RecordCount)+')'); delay(10);

      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;

      acRecalc.Execute;

      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;

      Memo1.Lines.Add('�������� ��.'); delay(10);

    end;
  end;
end;

procedure TfmAddAlco.acExpExlsExecute(Sender: TObject);
begin
//������� � ������
  prNExportExel5(ViAlco1);
  prNExportExel5(ViAlco2);
end;

procedure TfmAddAlco.acExpXMLExecute(Sender: TObject);
Var fName:String;
    f:Textfile;
    S:String;
    iShop,iVid,iProd,iPost:INteger;
    sOrg:TOrg;
    iNumVid,iNumProd,iNumPost:Integer;
    rSumVid:Real;

    StrN,StrGuid:String;
    a:TGuid;

  function sfmt(s1:String):String;
  Var Se:String;
  begin
    Se:=s1;
    Se:=Trim(Se);
    while pos('"',Se)>0 do delete(Se,pos('"',Se),1);
    Result:=Se;
  end;

  function rfmt(rSum:Real):String;
  Var Se:String;
  begin
    str(rSum:10:5,Se);
//    Se:=Se+'000';
    while pos(',',Se)>0 do Se[pos(',',Se)]:='.';
    while pos(' ',Se)>0 do delete(Se,pos(' ',Se),1);
    Result:=Se;
  end;

begin
  //������� � XML

  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  if cxComboBox1.ItemIndex=0 then StrN:='R1_' else StrN:='R2_';

  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'003_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

//  Memo1.Lines.Add(StrN);

  fName:=StrN;
  OpenDialog1.InitialDir:=CurDir;
  OpenDialog1.fileName:=StrN;

  if OpenDialog1.Execute then
  begin
    Memo1.Clear;
    fName:=OpenDialog1.fileName;
    Memo1.Lines.Add('������� � XML � ���� - '+fName); delay(10);
    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    ViAlco1.BeginUpdate;
    ViAlco2.BeginUpdate;

    Memo1.Lines.Add('       �������������.'); delay(10);
    CloseTe(teProds);
    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      if teProds.Locate('iProd',teAlcoObiProd.AsInteger,[])=False then
      begin
        teProds.Append;
        teProdsiProd.AsInteger:=teAlcoObiProd.AsInteger;
        teProdssProd.AsString:=sfmt(teAlcoObsProd.AsString);
        teProdssProdInn.AsString:=sfmt(teAlcoObsProdInn.AsString);
        teProdssProdKPP.AsString:=sfmt(teAlcoObsProdKPP.AsString);
        teProds.Post;
      end;
      teAlcoOb.Next;
    end;

    Memo1.Lines.Add('       ����������.'); delay(10);
    CloseTe(tePost);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iPost',teAlcoIniPost.AsInteger,[])=False then
      begin
        tePost.Append;
        tePostiPost.AsInteger:=teAlcoIniPost.AsInteger;
        tePostsPost.AsString:=sfmt(teAlcoInsPost.AsString);
        tePostsPostInn.AsString:=sfmt(teAlcoInsPostInn.AsString);
        tePostsPostKPP.AsString:=sfmt(teAlcoInsPostKPP.AsString);
        tePostiLic.AsInteger:=teAlcoIniLic.AsInteger;
        tePostLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
        tePostLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
        tePostLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
        tePostLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
        tePost.Post;
      end;
      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ��������.'); delay(10);
    CloseTe(teLic);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
      begin
//        Memo1.Lines.Add('�������� '+teAlcoIniLic.AsString+' '+teAlcoIniPost.AsString); delay(10);
        if teLic.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
        begin
          teLic.Append;
          teLiciPost.AsInteger:=teAlcoIniPost.AsInteger;
          teLiciLic.AsInteger:=teAlcoIniLic.AsInteger;
          teLicLicNumber.AsString:=teAlcoInsLicNumber.AsString;
          teLicLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
          teLicLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
          teLicLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
          teLicLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
          teLic.Post;
        end;
      end;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������..'); delay(10);

    CloseTa(teA1);
    CloseTa(teA2);

    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      teA2.Append;
      teA2iDep.AsInteger:=teAlcoIniDep.AsInteger;
      teA2iVid.AsInteger:=teAlcoIniVid.AsInteger;
      teA2iProd.AsInteger:=teAlcoIniProd.AsInteger;
      teA2iPost.AsInteger:=teAlcoIniPost.AsInteger;
      teA2iLic.AsInteger:=teAlcoIniLic.AsInteger;
      teA2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
      teA2DocNum.AsString:=teAlcoInDocNum.AsString;
      teA2GTD.AsString:=teAlcoInGTD.AsString;
      teA2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
      teA2.Post;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������.'); delay(10);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teA1.Append;
      teA1iDep.AsInteger:=teAlcoObiDep.AsInteger;
      teA1sDep.AsString:=teAlcoObsDep.AsString;
      teA1iVid.AsInteger:=teAlcoObiVid.AsInteger;
      teA1sVidName.AsString:=teAlcoObsVidName.AsString;
      teA1iProd.AsInteger:=teAlcoObiProd.AsInteger;
      teA1rQb.AsFloat:=teAlcoObrQb.AsFloat;
      teA1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
      teA1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
      teA1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
      teA1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
      teA1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
      teA1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
      teA1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
      teA1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
      teA1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
      teA1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
      teA1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
      teA1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
      teA1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
      teA1rQe.AsFloat:=teAlcoObrQe.AsFloat;
      teA1.Post;

      teAlcoOb.Next;
    end;


    ViAlco1.EndUpdate;
    ViAlco2.EndUpdate;

    Memo1.Lines.Add('   - ��������� ������'); delay(10);

    assignfile(f,fName);
    rewrite(f);
    try
      Memo1.Lines.Add('     �����������'); delay(10);

      S:='<?xml version="1.0" encoding="windows-1251"?>';
      writeln(f,s);

      S:='<���� �������="'+ds1(date)+'" ��������="4.20" ��������="��������� ���� ������ 4.20.24">';
      writeln(f,s);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='	<�������� �������="11-�" �������������="'+its(cxSpinEdit1.Value)+'" ��������="4" ������������="2012">';
        writeln(f,s);
      end else
      begin
        S:='	<�������� �������="12-�" �������������="'+its(cxSpinEdit1.Value)+'" ��������="4" ������������="2012">';
        writeln(f,s);
      end;

      if cxComboBox2.ItemIndex=0 then S:='		<��������� />' else
      begin
        S:='		<�������������� ���������="'+its(cxSpinEdit2.Value)+'" />'; //����� ��� ����� ���������
      end;
      writeln(f,s);

      S:='	</��������>';
      writeln(f,s);

      S:='	<�����������>'; //������ ������������
      writeln(f,s);

      teProds.First;
      while not teProds.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'" �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          writeln(f,s);
        end else
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'">';
          writeln(f,s);
          if pos('�� ',teProdssProd.AsString)=1 then
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" />';
          end else
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          end;
          writeln(f,s);
          S:='		</����������������������>';
          writeln(f,s);
        end;

        teProds.Next;
      end;

      tePost.First;
      while not tePost.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';
          writeln(f,s);

          S:='			<��������>';   writeln(f,s);

          S:='				<�������� ����������="'+its(tePostiLic.AsInteger)+'" �000000000011="'+tePostLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',tePostLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',tePostLicDateE.AsDateTime)+'" �000000000014="'+tePostLicOrg.AsString+'" />';
          writeln(f,s);

          S:='			</��������>'; writeln(f,s);

          teLic.First;
          while not teLic.Eof do
          begin
            if teLiciPost.AsInteger=tePostiPost.AsInteger then
            begin
              S:='			<��������>';   writeln(f,s);
              S:='				<�������� ����������="'+its(teLiciLic.AsInteger)+'" �000000000011="'+teLicLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',teLicLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',teLicLicDateE.AsDateTime)+'" �000000000014="'+teLicLicOrg.AsString+'" />';
              writeln(f,s);
              S:='			</��������>'; writeln(f,s);

              teLic.Delete;
            end else teLic.Next;
          end;


          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />';
          writeln(f,s);

          S:='		</����������>'; writeln(f,s);
        end else
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';  writeln(f,s);
          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />'; writeln(f,s);
          S:='		</����������>'; writeln(f,s);
        end;
        tePost.Next;
      end;

      S:='	</�����������>'; //����� ������������
      writeln(f,s);

      Memo1.Lines.Add('     ������'); delay(10);

      S:='	<��������>';
      writeln(f,s);

      iShop:=0; iVid:=0; iProd:=0;
      teA1.First;

      sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� ������="'+sfmt(sOrg.FullName)+'" �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<��������>'; writeln(f,s);
        S:='				<�������� �������="14" ������="'+sOrg.LicSer+'" ��������="'+sOrg.LicNum+'" ����������="'+ds1(sOrg.dDateB)+'" �����������="'+ds1(sOrg.dDateE)+'" />'; writeln(f,s);
        S:='			</��������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end else
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� �������="'+sfmt(sOrg.FullName)+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+'</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);

        if sOrg.ip=0 then
        begin
          S:='				<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        end else
        begin
          S:='				<�� �����="'+sOrg.Inn+'"/>'; writeln(f,s);
        end;

//        S:='				<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end;

      iNumVid:=1;
      iNumProd:=1;
      while not teA1.Eof do
      begin
        if iShop<>teA1iDep.AsInteger then
        begin
          if iProd>0 then
          begin
            S:='				</����������������>'; writeln(f,s);
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);

            S:='			</������>'; writeln(f,s);
          end;

          if iShop>0 then
          begin
//            S:='   ������� ����� - '+its(iShop)+';';    writeln(f,s);
            S:='		</������������>'; writeln(f,s);
          end;

//          S:='   ������� ������ - '+teA1iDep.AsString+';'+teA1sDep.AsString;  writeln(f,s);
          iShop:=teA1iDep.AsInteger;
          sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

          if sOrg.ip=0 then
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);
          end else
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" ��������������="true">'; writeln(f,s);
          end;

//          S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);

          S:='			<������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
          S:='				<������>620000</������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='				<�����>������������ �</�����>'; writeln(f,s);
          S:='				<����������> </����������>'; writeln(f,s);
          S:='				<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
          S:='				<���>'+sOrg.House+'</���>'; writeln(f,s);
          if sOrg.Korp>'' then
          begin
            S:='				<������>'+sOrg.Korp+'</������>'; writeln(f,s);
          end else
          begin
            S:='				<������ />'; writeln(f,s);
          end;
          S:='				<������ />'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='			</������>'; writeln(f,s);

          //���������� ���������
          iVid:=0;
          iProd:=0;

          iNumVid:=1;
          iNumProd:=1;
        end;
        if iVid<>teA1iVid.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';'; writeln(f,s);
            S:='			</������>'; writeln(f,s);
          end;

//          S:='         ��� ������ - '+teA1iVid.AsString+';'+teA1sVidName.AsString; writeln(f,s);
          S:='			<������ �N="'+its(iNumVid)+'" �000000000003="'+its(teA1iVid.AsInteger)+'">'; writeln(f,s);


          iVid:=teA1iVid.AsInteger;
          //���������� ���������
          iProd:=0;

          iNumProd:=1;

          inc(iNumVid);
        end;
        if iProd<>teA1iProd.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

//          S:='               ������������� ������ - '+teA1iProd.AsString; writeln(f,s);
          S:='				<���������������� �N="'+its(iNumProd)+'" �����������="'+its(teA1iProd.AsInteger)+'">'; writeln(f,s);

          iProd:=teA1iProd.AsInteger;

        end;

        iNumPost:=1;
        iPost:=0;
        rSumVid:=0;

        teA2.First;
        while not teA2.Eof do
        begin
//          S:='                      ��������� - '+teA2iDep.AsString+';'+teA2iVid.AsString+';'+teA2iProd.AsString+';'+teA2iPost.AsString+';'+teA2iLic.AsString+';'+teA2DocDate.AsString+';'+teA2DocNum.AsString+';'+teA2GTD.AsString+';'+fs(teA2rQIn.AsFloat);         writeln(f,s);
          if iPost<>teA2iPost.AsInteger then
          begin
            if iPost>0 then //����� �������
            begin
              S:='					</���������>'; writeln(f,s);
            end;
            //������ ������
            if cxComboBox1.ItemIndex=0 then
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" ����������="'+its(teA2iLic.AsInteger)+'">';  writeln(f,s);
            end else
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" >';  writeln(f,s);
            end;

            iPost:=teA2iPost.AsInteger;

            inc(iNumPost);
          end;

          // ����� ��������
//          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="'+teA2GTD.AsString+'" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          rSumVid:=rSumVid+teA2rQIn.AsFloat;

          teA2.Next;
        end;;
        if iPost>0 then
        begin
          S:='					</���������>'; writeln(f,s);
        end;

        if abs(rSumVid-teA1rQIn2.AsFloat)>0.000001 then
        begin
          Memo1.Lines.Add('   ������ �����������: ��-�� '+rfmt(teA1rQIn2.AsFloat)+' �� ���������� '+rfmt(rSumVid)); delay(10);
          Memo1.Lines.Add('   '+its(iShop)+';'+its(iVid)+';'+its(iProd)); delay(10);
        end;

//        S:='   '+teA1iDep.AsString+';'+teA1sDep.AsString+';'+teA1iVid.AsString+';'+teA1sVidName.AsString+';'+teA1iProd.AsString+' - ������.';
//        writeln(f,s);

        if cxComboBox1.ItemIndex=0 then
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="0.00000"';
          S:=S+' �100000000014="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000015="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000016="'+rfmt(teA1rQOut2.AsFloat)+'"';
          S:=S+' �100000000017="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000018="0.00000"';
          S:=S+' �100000000019="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000020="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end else
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000014="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000015="'+rfmt(teA1rQOut2.AsFloat)+'"';
          S:=S+' �100000000016="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000017="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000018="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end;

        teA1.Next;   inc(iNumProd);
      end;
      if iProd>0 then
      begin
//        S:='               ������������� ����� - '+its(iProd); writeln(f,s);

        S:='				</����������������>'; writeln(f,s);
      end;
      if iVid>0 then
      begin
//        S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);
        S:='			</������>'; writeln(f,s);
      end;
      if iShop>0 then
      begin
//        S:='   ������� ����� - '+its(iShop)+';';
        S:='		</������������>'; writeln(f,s);
      end;

      S:='	</��������>';
      writeln(f,s);
      S:='</����>';
      writeln(f,s);

    finally
      Memo1.Lines.Add('������������ ���������.'); delay(10);
      teProds.Active:=False;
      tePost.Active:=False;
      teLic.Active:=False;
      closefile(f);
    end;
  end;
end;

procedure TfmAddAlco.acGenGUIDExecute(Sender: TObject);
Var StrN,StrGuid:String;
    sOrg:TOrg;
    a:TGuid;

begin
  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  StrN:='R1_';
  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'_003_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

  Memo1.Lines.Add(StrN);
end;

procedure TfmAddAlco.acGetMoveExecute(Sender: TObject);
Var iStep,iC:INteger;
    iDateB,iDateE,iDep:Integer;
    iMax:INteger;
    rQB,rQIn,rQR,rQRet,rQE:Real;
    kVol:Real;
    iE:Integer;
    par1,par2:Variant;
    StrWk:String;
    iVb,iVe,IdDH,iVb1,iVe1:INteger;
    Sinn,Skpp:String;
    iCliCB:INteger;
    LicSerN,LicOrg:String;
    LicDateB,LicDateE:tDateTime;
    dDateB,dDateE:TDateTime;
begin
  //������� �������� �� ���������
  Memo1.Lines.Add('  ������ ������'); delay(10);

  PBar1.Position:=0;
  PBar1.Visible:=True;

  ViAlco1.BeginUpdate;
  ViAlco2.BeginUpdate;
  iDep:=cxLookupComboBox1.EditValue;
  iDateB:=Trunc(cxDateEdit1.Date);
  iDateE:=Trunc(cxDateEdit2.Date);
  dDateB:=iDateB;
  dDateE:=iDateE;

  CloseTe(teAlcoOb);
  CloseTe(teAlcoIn);
  with dmMt do
  with dmMC do
  with dmAlg do
  begin
    //������� ������� ������� � ������� ������ ����� �������
    Memo1.Lines.Add('  - ������� ������� � ����������'); delay(10);

    par1 := VarArrayCreate([0,2], varInteger);
    par2 := VarArrayCreate([0,3], varInteger);

    if taCards.Active=False then taCards.Active:=True;
    iMax:=taCards.RecordCount;
    iStep:=iMax div 100;
    Memo1.Lines.Add('   ����� '+its(iMax));
    iC:=0;

    //������� ���� ����� �� ����������


    try
      if ptCG.Active=False then ptCG.Active:=True;
      if ptCM.Active=False then ptCM.Active:=True;
      if ptDep.Active=False then ptDep.Active:=True;

      taCards.First;
      while (taCards.Eof=False)and(bStopAlg=False) do
      begin
            //������ ��������
        if (taCardsV10.AsInteger>0) //�����
        and (taCardsV11.AsInteger>0) //��� ���������
        and (taCardsV12.AsInteger>0) //�������������
        then    //��������� ����������
        begin
          if cxComboBox1.ItemIndex=0 then
//          begin   iVb:=1; iVe:=499; end //��������
//          else begin iVb:=500; iVe:=999; end; //����

          begin   iVb:=1; iVe:=260; iVb1:=264; iVe1:=499; end //��������
          else begin iVb:=261; iVe:=263; iVb1:=500; iVe1:=999; end; //����

//          if (taCardsV11.AsInteger>=iVb)and(taCardsV11.AsInteger<iVe) then
          if ((taCardsV11.AsInteger>=iVb)and(taCardsV11.AsInteger<=iVe))or((taCardsV11.AsInteger>=iVb1)and(taCardsV11.AsInteger<=iVe1)) then
          begin //�������� ���
            kVol:=taCardsV10.AsInteger/1000;
            rQB:=0;rQIn:=0;rQRet:=0;rQE:=0;
            iE:=0; //��������� ������ �������

            ptCG.CancelRange;
            ptCG.SetRange([0,taCardsID.AsInteger],[0,taCardsID.AsInteger]);
            ptCG.First;
            while not ptCG.Eof do
            begin
              if ptCGDEPART.AsInteger=iDep then
              begin
                ptCM.Last;
                while not ptCM.Bof do
                begin
                  if (iDateE>=ptCMMOVEDATE.AsDateTime) then
                  begin
                    if iE=0 then
                    begin
                      rQE:=ptCMQUANT_REST.AsFloat;  //���� ������ ������ � ����� ������� �������� c rjywf
                      iE:=1; //���� ������ ���� ��� - ����� ���� 0
                    end;
                    if iDateB>ptCMMOVEDATE.AsDateTime then
                    begin
                      rQB:=ptCMQUANT_REST.AsFloat; //��������� �������� ���� ����� ����� - ������� �� ����� � �� ������ ����������
                      break; //���� �� ������� �� ����� �.�. ����� � �� ����
                    end else
                    begin   //�� � ��������� - ����� ������� �����
                      rQB:=ptCMQUANT_REST.AsFloat-ptCMQUANT_MOVE.AsFloat;
                      if ptCMDOC_TYPE.AsInteger=1 then rQIn:=rQIn+ptCMQUANT_MOVE.AsFloat;
                      if ptCMDOC_TYPE.AsInteger=2 then rQRet:=rQRet+abs(ptCMQUANT_MOVE.AsFloat);  //������ ������� �������� abs
                    end;
                  end;
                  ptCM.Prior;
                end;
              end;
              ptCG.Next;
            end;

            rQR:=rQB+rQIn-rQRet-rQE; //���������� ��������� �����

            if (abs(rQB)>0.001)
            or (abs(rQIn)>0.001)
            or (abs(rQRet)>0.001)
            or (abs(rQR)>0.001)
            or (abs(rQE)>0.001) then
            begin //���� �������� - ����� ��������� � �������
              par1[0]:=iDep;
              par1[1]:=taCardsV11.AsInteger; //vid
              par1[2]:=taCardsV12.AsInteger; //�������������

              if teAlcoOb.Locate('iDep;iVid;iProd',par1,[]) then
              begin //���� ����� ������
                teAlcoOb.Edit;
                teAlcoObrQb.AsFloat:=teAlcoObrQb.AsFloat+(rQb*kVol)/10;
                teAlcoObrQIn1.AsFloat:=0;
                teAlcoObrQIn2.AsFloat:=0;
                teAlcoObrQIn3.AsFloat:=0;
//                teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+(rQIn*kVol)/10;
                teAlcoObrQInIt1.AsFloat:=0;
                teAlcoObrQIn4.AsFloat:=0;
                teAlcoObrQIn5.AsFloat:=0;
                teAlcoObrQIn6.AsFloat:=0;
//                teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+(rQIn*kVol)/10;
//                teAlcoObrQOut1.AsFloat:=teAlcoObrQOut1.AsFloat+(rQR*kVol)/10;
                teAlcoObrQInIt.AsFloat:=0;
                teAlcoObrQOut1.AsFloat:=0;
                teAlcoObrQOut2.AsFloat:=0;
                teAlcoObrQOut3.AsFloat:=teAlcoObrQOut3.AsFloat+(rQRet*kVol)/10;
                teAlcoObrQOut4.AsFloat:=0;
//                teAlcoObrQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat+((rQR+rQRet)*kVol)/10;
                teAlcoObrQOutIt.AsFloat:=0;
                teAlcoObrQe.AsFloat:=teAlcoObrQe.AsFloat+(rQe*kVol)/10;
                teAlcoOb.Post;
              end else
              begin
                teAlcoOb.Append;
                teAlcoObiDep.AsInteger:=iDep;
                teAlcoObsDep.AsString:=cxLookupComboBox1.Text;
                teAlcoObiVid.AsInteger:=taCardsV11.AsInteger;
                teAlcoObsVid.AsString:=taCardsV11.AsString;
                teAlcoObsVidName.AsString:='';
                teAlcoObiProd.AsInteger:=taCardsV12.AsInteger;
                teAlcoObsProd.AsString:='';
                teAlcoObsProdInn.AsString:='';
                teAlcoObsProdKPP.AsString:='';
                teAlcoObrQb.AsFloat:=(rQb*kVol)/10;
                teAlcoObrQIn1.AsFloat:=0;
                teAlcoObrQIn2.AsFloat:=0;//(rQIn*kVol)/10;
                teAlcoObrQIn3.AsFloat:=0;
                teAlcoObrQInIt1.AsFloat:=0;//(rQIn*kVol)/10;
                teAlcoObrQIn4.AsFloat:=0;
                teAlcoObrQIn5.AsFloat:=0;
                teAlcoObrQIn6.AsFloat:=0;
                teAlcoObrQInIt.AsFloat:=0;//(rQIn*kVol)/10;
                teAlcoObrQOut1.AsFloat:=0;//(rQR*kVol)/10;
                teAlcoObrQOut2.AsFloat:=0;
                teAlcoObrQOut3.AsFloat:=(rQRet*kVol)/10;
                teAlcoObrQOut4.AsFloat:=0;
                teAlcoObrQOutIt.AsFloat:=0;//((rQR+rQRet)*kVol)/10;
                teAlcoObrQe.AsFloat:=(rQe*kVol)/10;
                teAlcoOb.Post;
              end;
            end;
          end;
        end;

        taCards.Next; inc(iC);
        if iC mod iStep = 0 then
        begin
          PBar1.Position:=iC div iStep;
          delay(10);
        end;
      end;

    finally
    end;

    Memo1.Lines.Add('  - ������� �������'); delay(10);
    PBar1.Position:=0;

    if taCards.Active=False then taCards.Active:=True;
    iMax:=taCards.RecordCount;
    iStep:=iMax div 100;
    Memo1.Lines.Add('   ����� '+its(iMax));
    iC:=0;

    try
      if ptTTnInLn.Active=False then ptTTnInLn.Active:=True;
      if ptTTnIn.Active=False then ptTTnIn.Active:=True;
      if ptCliLic.Active=False then ptCliLic.Active:=True;

      taCards.First;
      while (taCards.Eof=False)and(bStopAlg=False) do
      begin
        //������ ��������
        if (taCardsV10.AsInteger>0) //�����
        and (taCardsV11.AsInteger>0) //��� ���������
        and (taCardsV12.AsInteger>0) //�������������
        then    //��������� ����������
        begin
          if cxComboBox1.ItemIndex=0 then
          begin   iVb:=1; iVe:=260; iVb1:=262; iVe1:=499; end //��������
          else begin iVb:=261; iVe:=261; iVb1:=500; iVe1:=999; end; //����

//          begin   iVb:=1; iVe:=499; end //��������
//          else begin iVb:=500; iVe:=999; end; //����
          if ((taCardsV11.AsInteger>=iVb)and(taCardsV11.AsInteger<=iVe))or((taCardsV11.AsInteger>=iVb1)and(taCardsV11.AsInteger<=iVe1)) then
          begin //�������� ���� - ��� �������� ���������
            kVol:=taCardsV10.AsInteger/1000;

            ptTTnInLn.CancelRange;
            ptTTnInLn.SetRange([taCardsID.AsInteger,dDateB],[taCardsID.AsInteger,dDateE]);
            ptTTnInLn.First;
            while not ptTTnInLn.Eof do
            begin
              if ptTTnInLnDepart.AsInteger=iDep then
              begin
                if ptTTnIn.FindKey([ptTTnInLnDepart.AsInteger,ptTTnInLnDateInvoice.AsDateTime,ptTTnInLnNumber.AsString]) then
                begin
                  IdDH:=ptTTNInID.AsInteger;

                  rQIn:=ptTTnInLnKol.AsFloat;

                  par2[0]:=iDep;
                  par2[1]:=taCardsV11.AsInteger; //���
                  par2[2]:=taCardsV12.AsInteger; //�������������
                  par2[3]:=IdDH;

                  if teAlcoIn.Locate('iDep;iVid;iProd;DocId',par2,[]) then
                  begin //�����������
                    teAlcoIn.Edit;
                    teAlcoInrQIn.AsFloat:=teAlcoInrQIn.AsFloat+(rQIn*kVol)/10;
                    teAlcoIn.Post;
                  end else //���������
                  begin
                    LicSerN:='';
                    LicOrg:='';
                    LicDateB:=0;
                    LicDateE:=0;

                    ptCliLic.CancelRange;
                    ptCliLic.SetRange([1,ptTTNInCodePost.AsInteger],[1,ptTTNInCodePost.AsInteger]);
                    ptCliLic.First;
                    while not ptCliLic.Eof do
                    begin
//                      if (ptTTNInDateInvoice.AsDateTime>=ptCliLicDDATEB.AsDateTime)
//                      and (ptTTNInDateInvoice.AsDateTime<=ptCliLicDDATEB.AsDateTime)
                      //���� ������ ���������

                      LicSerN:=OemToAnsiConvert(ptCliLicSER.AsString)+' '+OemToAnsiConvert(ptCliLicSNUM.AsString);
                      LicOrg:=OemToAnsiConvert(ptCliLicORGAN.AsString);
                      LicDateB:=ptCliLicDDATEB.AsDateTime;
                      LicDateE:=ptCliLicDDATEE.AsDateTime;

                      ptCliLic.Next;
                    end;

                    teAlcoIn.Append;
                    teAlcoIniDep.AsInteger:=iDep;
                    teAlcoInsDep.AsString:=cxLookupComboBox1.Text;
                    teAlcoIniVid.AsInteger:=taCardsV11.AsInteger;
                    teAlcoInsVid.AsString:=taCardsV11.AsString;
                    teAlcoInsVidName.AsString:='';
                    teAlcoIniProd.AsInteger:=taCardsV12.AsInteger;
                    teAlcoInsProd.AsString:='';
                    teAlcoInsProdInn.AsString:='';
                    teAlcoInsProdKPP.AsString:='';
                    teAlcoIniPost.AsInteger:=ptTTNInCodePost.AsInteger;
                    teAlcoInsPost.AsString:=prFindCliName2(ptTTNInIndexPost.AsInteger,ptTTNInCodePost.AsInteger,Sinn,Skpp,iCliCB);
                    teAlcoInsPostInn.AsString:=Sinn;
                    teAlcoInsPostKpp.AsString:=Skpp;
                    teAlcoIniLic.AsInteger:=0;
                    teAlcoInsLicNumber.AsString:=LicSerN;
                    teAlcoInLicDateB.AsDateTime:=LicDateB;
                    teAlcoInLicDateE.AsDateTime:=LicDateE;
                    teAlcoInLicOrg.AsString:=LicOrg;
                    teAlcoInDocDate.AsDateTime:=ptTTNInDateInvoice.AsDateTime;
                    teAlcoInDocNum.AsString:=OemToAnsiConvert(ptTTnInLnNumber.AsString);
                    teAlcoInGTD.AsString:='';
                    teAlcoInrQIn.AsFloat:=(rQIn*kVol)/10;
                    teAlcoInDocId.AsInteger:=IdDh;
                    teAlcoIn.Post;
                  end;
                end;
              end;

              ptTTnInLn.Next;
            end;
          end;
        end;

        taCards.Next; inc(iC);
        if iC mod iStep = 0 then
        begin
          PBar1.Position:=iC div iStep;
          delay(10);
        end;
      end;

      Memo1.Lines.Add('  - �������� ������'); delay(10);

      acRecalc.Execute;  //g�������� ���

      ptMakers.Active:=False;
      ptMakers.Active:=True;
      taAlgClass.Active:=False;
      taAlgClass.Active:=True;

      teAlcoOb.First;
      while not teAlcoOb.Eof do
      begin
        if ptMakers.Locate('ID',teAlcoObiProd.AsInteger,[]) then
        begin
          if taAlgClass.Locate('ID',teAlcoObiVid.AsInteger,[]) then
          begin
            teAlcoOb.Edit;
            teAlcoObsVidName.AsString:=OemToAnsiConvert(taAlgClassNAMECLA.AsString);
            teAlcoObsProd.AsString:=OemToAnsiConvert(ptMakersNAMEM.AsString);
            teAlcoObsProdInn.AsString:=OemToAnsiConvert(ptMakersINNM.AsString);
            teAlcoObsProdKPP.AsString:=OemToAnsiConvert(ptMakersKPPM.AsString);
            teAlcoOb.Post;
          end;
        end;

        teAlcoOb.Next;
      end;

      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        if ptMakers.Locate('ID',teAlcoIniProd.AsInteger,[]) then
        begin
          if taAlgClass.Locate('ID',teAlcoIniVid.AsInteger,[]) then
          begin
            StrWk:=its(teAlcoIniVid.AsInteger);
            while length(StrWk)<3 do StrWk:='0'+StrWk;

            teAlcoIn.Edit;
            teAlcoInsVidName.AsString:=OemToAnsiConvert(taAlgClassNAMECLA.AsString);
            teAlcoInsProd.AsString:=OemToAnsiConvert(ptMakersNAMEM.AsString);
            teAlcoInsProdInn.AsString:=OemToAnsiConvert(ptMakersINNM.AsString);
            teAlcoInsProdKpp.AsString:=OemToAnsiConvert(ptMakersKPPM.AsString);
            teAlcoIn.Post;
          end;
        end;

        teAlcoIn.Next;
      end;
      ptMakers.Active:=False;
      taAlgClass.Active:=False;

      ptTTnInLn.Active:=False;
      ptTTnIn.Active:=False;
      ptCliLic.Active:=False;

    finally
    end;
  end;
  delay(500);
  PBar1.Visible:=False;

  ViAlco1.EndUpdate;
  ViAlco2.EndUpdate;

  Memo1.Lines.Add('  ������ ��'); delay(10);
end;

procedure TfmAddAlco.acRecalcExecute(Sender: TObject);
Var par,parlic:Variant;
    iC:INteger;
    rQReal:Real;
    iLic,iCurLic:INteger;

  function sfmt(s1:String):String;
  Var Se:String;
  begin
    Se:=s1;
    Se:=Trim(Se);
    while pos('"',Se)>0 do delete(Se,pos('"',Se),1);
    Result:=Se;
  end;
    
begin
  //�������� ������
  Memo1.Lines.Add('  �������� '); delay(10);
  Memo1.Lines.Add('  - ������� ('+Its(teAlcoIn.RecordCount)+')'); delay(10);

 {     par := VarArrayCreate([0,3], varInteger);

      iC:=0;
      // ������� ��� �������� �� ������ ������

      teAlcoOb.First;
      while not teAlcoOb.Eof do
      begin
        teAlcoOb.Edit;
        teAlcoObrQb.AsFloat:=rv(teAlcoObrQb.AsFloat);
        teAlcoObrQIn1.AsFloat:=0;
        teAlcoObrQIn2.AsFloat:=0;
        teAlcoObrQIn3.AsFloat:=0;
        teAlcoObrQIn4.AsFloat:=0;
        teAlcoObrQIn5.AsFloat:=0;
        teAlcoObrQIn6.AsFloat:=0;
        teAlcoObrQInIt.AsFloat:=0;
        teAlcoObrQInIt1.AsFloat:=0;

        teAlcoObrQOut1.AsFloat:=rv(teAlcoObrQOut1.AsFloat);
        teAlcoObrQOut2.AsFloat:=rv(teAlcoObrQOut2.AsFloat);
        teAlcoObrQOut3.AsFloat:=rv(teAlcoObrQOut3.AsFloat);
        teAlcoObrQOut4.AsFloat:=rv(teAlcoObrQOut4.AsFloat);
        teAlcoObrQOutIt.AsFloat:=rv(teAlcoObrQOutIt.AsFloat);

        teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat-teAlcoObrQOutIt.AsFloat;
        teAlcoOb.Post;

        teAlcoOb.Next;
      end;

      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        teAlcoIn.Edit;
        teAlcoInrQIn.AsFloat:=rv(teAlcoInrQIn.AsFloat);
        teAlcoIn.Post;

        par[0]:=teAlcoIniDep.AsInteger;
        par[1]:=teAlcoIniVid.AsInteger;
        par[2]:=teAlcoIniProd.AsInteger;

        if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
        begin //���� ����� ������
          teAlcoOb.Edit;
          teAlcoObrQIn2.AsFloat:=teAlcoObrQIn2.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
          teAlcoOb.Post;
        end else
        begin
          teAlcoOb.Append;
          teAlcoObiDep.AsInteger:=teAlcoIniDep.AsInteger;
          teAlcoObsDep.AsString:=teAlcoInsDep.AsString;
          teAlcoObiVid.AsInteger:=teAlcoIniVid.AsInteger;
          teAlcoObsVid.AsString:=teAlcoIniVid.AsString;
          teAlcoObsVidName.AsString:=teAlcoInsVidName.AsString;
          teAlcoObiProd.AsInteger:=teAlcoIniProd.AsInteger;
          teAlcoObsProd.AsString:=teAlcoInsProd.AsString;
          teAlcoObsProdInn.AsString:=teAlcoInsProdInn.AsString;
          teAlcoObsProdKPP.AsString:=teAlcoInsProdKPP.AsString;
          teAlcoObrQb.AsFloat:=0;
          teAlcoObrQIn1.AsFloat:=0;
          teAlcoObrQIn2.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQInIt1.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQOut1.AsFloat:=0;
          teAlcoObrQOut2.AsFloat:=0;
          teAlcoObrQOut3.AsFloat:=0;
          teAlcoObrQOut4.AsFloat:=0;
          teAlcoObrQOutIt.AsFloat:=0;
          teAlcoObrQe.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoOb.Post;
        end;

        teAlcoIn.Next;  inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('     '+its(iC)); delay(10);
        end;

      end;

      Memo1.Lines.Add('  - ������� ('+Its(teAlcoOb.RecordCount)+')'); delay(10);

      iC:=0;
      
      teAlcoOb.First;
      while not teAlcoOb.Eof do
      begin
        teAlcoOb.Edit;
        teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat;
        teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-(teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat);
        teAlcoOb.Post;

        if teAlcoObrQb.AsFloat<0 then //���� ������� �� ������ <0
        begin
          teAlcoOb.Edit;
          teAlcoObrQb.AsFloat:=0;
          teAlcoObrQe.AsFloat:=teAlcoObrQInIt.AsFloat-(teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat);
          teAlcoOb.Post;
        end;

        if teAlcoObrQe.AsFloat<0 then //���� ������� �� ����� 0
        begin
          teAlcoOb.Edit;
          teAlcoObrQe.AsFloat:=0;
          teAlcoObrQOut1.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOut3.AsFloat;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat;
          teAlcoOb.Post;
        end;

        if teAlcoObrQOut1.AsFloat<0 then //���� ���������� �������������
        begin
          teAlcoOb.Edit;
          teAlcoObrQe.AsFloat:=teAlcoObrQe.AsFloat+teAlcoObrQOut1.AsFloat;
          teAlcoObrQOut1.AsFloat:=0;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut3.AsFloat;
          teAlcoOb.Post;
        end;

        teAlcoOb.Next;  inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('     '+its(iC)); delay(10);
        end;
      end;         }


  par := VarArrayCreate([0,3], varInteger);
  parlic := VarArrayCreate([0,1], varVariant);

  iC:=0;
  iLic:=1;
  CloseTe(teLic);

  teAlcoOb.First;
  while not teAlcoOb.Eof do
  begin
    teAlcoOb.Edit;
    teAlcoObrQIn1.AsFloat:=0;
    teAlcoObrQIn2.AsFloat:=0;
    teAlcoObrQIn3.AsFloat:=0;
    teAlcoObrQIn4.AsFloat:=0;
    teAlcoObrQIn5.AsFloat:=0;
    teAlcoObrQIn6.AsFloat:=0;
    teAlcoObrQInIt.AsFloat:=0;
    teAlcoObrQInIt1.AsFloat:=0;

    teAlcoObrQb.AsFloat:=R100000(teAlcoObrQb.AsFloat);
    teAlcoObrQOut1.AsFloat:=R100000(teAlcoObrQOut1.AsFloat);
    teAlcoObrQOut2.AsFloat:=R100000(teAlcoObrQOut2.AsFloat);
    teAlcoObrQOut3.AsFloat:=R100000(teAlcoObrQOut3.AsFloat);
    teAlcoObrQOut4.AsFloat:=R100000(teAlcoObrQOut4.AsFloat);
    teAlcoObrQOutIt.AsFloat:=R100000(teAlcoObrQOutIt.AsFloat);
    teAlcoObrQe.AsFloat:=R100000(teAlcoObrQe.AsFloat);

    teAlcoOb.Post;

    teAlcoOb.Next;
  end;

  teAlcoIn.First;
  while not teAlcoIn.Eof do
  begin
    parlic[0]:=teAlcoIniPost.AsInteger;
    parlic[1]:=sfmt(teAlcoInsLicNumber.AsString);

    if teLic.Locate('iPost;LicNumber',parlic,[])=false  then
    begin
      teLic.Append;
      teLiciPost.AsInteger:=teAlcoIniPost.AsInteger;
      teLiciLic.AsInteger:=iLic;
      teLicLicNumber.AsString:=teAlcoInsLicNumber.AsString;
      teLicLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
      teLicLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
      teLicLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
      teLicLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
      teLic.Post;

      iCurLic:=iLic;

      inc(iLic);
    end else
    begin
      iCurLic:=teLiciLic.AsInteger;
    end;

    teAlcoIn.Edit;
    teAlcoInrQIn.AsFloat:=R100000(teAlcoInrQIn.AsFloat);
    teAlcoIniLic.AsInteger:=iCurLic;
    teAlcoIn.Post;

    teAlcoIn.Next;
  end;

  teAlcoIn.First;
  while not teAlcoIn.Eof do
  begin
    par[0]:=teAlcoIniDep.AsInteger;
    par[1]:=teAlcoIniVid.AsInteger;
    par[2]:=teAlcoIniProd.AsInteger;

    if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
    begin //���� ����� ������
      teAlcoOb.Edit;
      teAlcoObrQIn2.AsFloat:=teAlcoObrQIn2.AsFloat+teAlcoInrQIn.AsFloat;
      teAlcoObrQIn3.AsFloat:=0;
      teAlcoObrQIn4.AsFloat:=0;
      teAlcoObrQIn5.AsFloat:=0;
      teAlcoObrQIn6.AsFloat:=0;
      teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+teAlcoInrQIn.AsFloat;
      teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+teAlcoInrQIn.AsFloat;
      teAlcoOb.Post;
    end else
    begin
      teAlcoOb.Append;
      teAlcoObiDep.AsInteger:=teAlcoIniDep.AsInteger;
      teAlcoObsDep.AsString:=teAlcoInsDep.AsString;
      teAlcoObiVid.AsInteger:=teAlcoIniVid.AsInteger;
      teAlcoObsVid.AsString:=teAlcoIniVid.AsString;
      teAlcoObsVidName.AsString:=teAlcoInsVidName.AsString;
      teAlcoObiProd.AsInteger:=teAlcoIniProd.AsInteger;
      teAlcoObsProd.AsString:=teAlcoInsProd.AsString;
      teAlcoObsProdInn.AsString:=teAlcoInsProdInn.AsString;
      teAlcoObsProdKPP.AsString:=teAlcoInsProdKPP.AsString;
      teAlcoObrQb.AsFloat:=0;
      teAlcoObrQIn1.AsFloat:=0;
      teAlcoObrQIn2.AsFloat:=teAlcoInrQIn.AsFloat;
      teAlcoObrQIn3.AsFloat:=0;
      teAlcoObrQInIt1.AsFloat:=teAlcoInrQIn.AsFloat;
      teAlcoObrQIn4.AsFloat:=0;
      teAlcoObrQIn5.AsFloat:=0;
      teAlcoObrQIn6.AsFloat:=0;
      teAlcoObrQInIt.AsFloat:=teAlcoInrQIn.AsFloat;
      teAlcoObrQOut1.AsFloat:=0;
      teAlcoObrQOut2.AsFloat:=0;
      teAlcoObrQOut3.AsFloat:=0;
      teAlcoObrQOut4.AsFloat:=0;
      teAlcoObrQOutIt.AsFloat:=0;
      teAlcoObrQe.AsFloat:=teAlcoInrQIn.AsFloat;
      teAlcoOb.Post;
    end;

    teAlcoIn.Next;  inc(iC);
    if iC mod 100 = 0 then
    begin
      Memo1.Lines.Add('     '+its(iC));
      delay(10);
    end;
  end;

  Memo1.Lines.Add('  - ������� ('+Its(teAlcoOb.RecordCount)+')'); delay(10);

  iC:=0;

  teAlcoOb.First;
  while not teAlcoOb.Eof do
  begin
    if teAlcoObrQb.AsFloat<0 then //���� ������� �� ������ <0
    begin            //������� ������ � 0 ���� ������
      teAlcoOb.Edit;
      teAlcoObrQb.AsFloat:=0;
      teAlcoOb.Post;
    end;

    //��������� ����������

    rQReal:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOut3.AsFloat-teAlcoObrQOut2.AsFloat-teAlcoObrQe.AsFloat;
    if rQReal<0 then rQReal:=0;

    teAlcoOb.Edit;
    teAlcoObrQOut1.AsFloat:=rQReal;
    teAlcoObrQOutIt.AsFloat:=rQReal+teAlcoObrQOut3.AsFloat+teAlcoObrQOut2.AsFloat;
    teAlcoOb.Post;

        if teAlcoObrQe.AsFloat<0 then //���� ������� �� ����� 0
        begin
          teAlcoOb.Edit;
          teAlcoObrQe.AsFloat:=0;
          teAlcoObrQOut1.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOut3.AsFloat-teAlcoObrQOut2.AsFloat;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat+teAlcoObrQOut2.AsFloat;
          teAlcoOb.Post;
        end;


    teAlcoOb.Next;  inc(iC);
    if iC mod 100 = 0 then
    begin
      //Memo1.Lines.Add('     '+its(iC));
      delay(10);
    end;
  end;
  Memo1.Lines.Add('  Ok'); delay(10);
end;

procedure TfmAddAlco.cxButton11Click(Sender: TObject);
Var fName:String;
    f:Textfile;
    S:String;
    iShop,iVid,iProd,iPost:INteger;
    sOrg:TOrg;
    iNumVid,iNumProd,iNumPost:Integer;
    rSumVid:Real;

    StrN,StrGuid:String;
    a:TGuid;

  function sfmt(s1:String):String;
  Var Se:String;
  begin
    Se:=s1;
    Se:=Trim(Se);
    while pos('"',Se)>0 do delete(Se,pos('"',Se),1);
    Result:=Se;
  end;

  function rfmt(rSum:Real):String;
  Var Se:String;
  begin
    str(rSum:10:5,Se);
//    Se:=Se+'000';
    while pos(',',Se)>0 do Se[pos(',',Se)]:='.';
    while pos(' ',Se)>0 do delete(Se,pos(' ',Se),1);
    Result:=Se;
  end;

begin
  //������� � XML

  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  if cxComboBox1.ItemIndex=0 then StrN:='R1_' else StrN:='R2_';

  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'_003_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

//  Memo1.Lines.Add(StrN);

  fName:=StrN;
  OpenDialog1.InitialDir:=CurDir;
  OpenDialog1.fileName:=StrN;

  if OpenDialog1.Execute then
  begin
    Memo1.Clear;
    fName:=OpenDialog1.fileName;
    Memo1.Lines.Add('������� � XML � ���� - '+fName); delay(10);
    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    ViAlco1.BeginUpdate;
    ViAlco2.BeginUpdate;

    Memo1.Lines.Add('       �������������.'); delay(10);
    CloseTe(teProds);
    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      if teProds.Locate('iProd',teAlcoObiProd.AsInteger,[])=False then
      begin
        teProds.Append;
        teProdsiProd.AsInteger:=teAlcoObiProd.AsInteger;
        teProdssProd.AsString:=sfmt(teAlcoObsProd.AsString);
        teProdssProdInn.AsString:=sfmt(teAlcoObsProdInn.AsString);
        teProdssProdKPP.AsString:=sfmt(teAlcoObsProdKPP.AsString);
        teProds.Post;
      end;
      teAlcoOb.Next;
    end;

    Memo1.Lines.Add('       ����������.'); delay(10);
    CloseTe(tePost);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iPost',teAlcoIniPost.AsInteger,[])=False then
      begin
        tePost.Append;
        tePostiPost.AsInteger:=teAlcoIniPost.AsInteger;
        tePostsPost.AsString:=sfmt(teAlcoInsPost.AsString);
        tePostsPostInn.AsString:=sfmt(teAlcoInsPostInn.AsString);
        tePostsPostKPP.AsString:=sfmt(teAlcoInsPostKPP.AsString);
        tePostiLic.AsInteger:=teAlcoIniLic.AsInteger;
        tePostLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
        tePostLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
        tePostLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
        tePostLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
        tePost.Post;
      end;
      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ��������.'); delay(10);
    CloseTe(teLic);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
      begin
//        Memo1.Lines.Add('�������� '+teAlcoIniLic.AsString+' '+teAlcoIniPost.AsString); delay(10);
        if teLic.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
        begin
          teLic.Append;
          teLiciPost.AsInteger:=teAlcoIniPost.AsInteger;
          teLiciLic.AsInteger:=teAlcoIniLic.AsInteger;
          teLicLicNumber.AsString:=teAlcoInsLicNumber.AsString;
          teLicLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
          teLicLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
          teLicLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
          teLicLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
          teLic.Post;
        end;  
      end;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������..'); delay(10);

    CloseTa(teA1);
    CloseTa(teA2);

    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      teA2.Append;
      teA2iDep.AsInteger:=teAlcoIniDep.AsInteger;
      teA2iVid.AsInteger:=teAlcoIniVid.AsInteger;
      teA2iProd.AsInteger:=teAlcoIniProd.AsInteger;
      teA2iPost.AsInteger:=teAlcoIniPost.AsInteger;
      teA2iLic.AsInteger:=teAlcoIniLic.AsInteger;
      teA2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
      teA2DocNum.AsString:=teAlcoInDocNum.AsString;
      teA2GTD.AsString:=teAlcoInGTD.AsString;
      teA2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
      teA2.Post;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������.'); delay(10);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teA1.Append;
      teA1iDep.AsInteger:=teAlcoObiDep.AsInteger;
      teA1sDep.AsString:=teAlcoObsDep.AsString;
      teA1iVid.AsInteger:=teAlcoObiVid.AsInteger;
      teA1sVidName.AsString:=teAlcoObsVidName.AsString;
      teA1iProd.AsInteger:=teAlcoObiProd.AsInteger;
      teA1rQb.AsFloat:=teAlcoObrQb.AsFloat;
      teA1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
      teA1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
      teA1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
      teA1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
      teA1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
      teA1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
      teA1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
      teA1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
      teA1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
      teA1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
      teA1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
      teA1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
      teA1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
      teA1rQe.AsFloat:=teAlcoObrQe.AsFloat;
      teA1.Post;

      teAlcoOb.Next;
    end;


    ViAlco1.EndUpdate;
    ViAlco2.EndUpdate;

    Memo1.Lines.Add('   - ��������� ������'); delay(10);

    assignfile(f,fName);
    rewrite(f);
    try
      Memo1.Lines.Add('     �����������'); delay(10);

      S:='<?xml version="1.0" encoding="windows-1251"?>';
      writeln(f,s);

      S:='<���� �������="'+ds1(date)+'" ��������="4.20" ��������="��������� ���� ������ 4.20.24">';
      writeln(f,s);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='	<�������� �������="11-�" �������������="0" ��������="4" ������������="2012">';
        writeln(f,s);
      end else
      begin
        S:='	<�������� �������="12-�" �������������="0" ��������="4" ������������="2012">';
        writeln(f,s);
      end;

      if cxComboBox2.ItemIndex=0 then S:='		<��������� />' else
      begin
        S:='		<�������������� />'; //����� ��� ����� ���������
      end;
      writeln(f,s);

      S:='	</��������>';
      writeln(f,s);

      S:='	<�����������>'; //������ ������������
      writeln(f,s);

      teProds.First;
      while not teProds.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'" �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          writeln(f,s);
        end else
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'">';
          writeln(f,s);
          S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          writeln(f,s);
          S:='		</����������������������>';
          writeln(f,s);
        end;

        teProds.Next;
      end;

      tePost.First;
      while not tePost.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';
          writeln(f,s);

          S:='			<��������>';   writeln(f,s);

          S:='				<�������� ����������="'+its(tePostiLic.AsInteger)+'" �000000000011="'+tePostLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',tePostLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',tePostLicDateE.AsDateTime)+'" �000000000014="'+tePostLicOrg.AsString+'" />';
          writeln(f,s);

          S:='			</��������>'; writeln(f,s);

          teLic.First;
          while not teLic.Eof do
          begin
            if teLiciPost.AsInteger=tePostiPost.AsInteger then
            begin
              S:='			<��������>';   writeln(f,s);
              S:='				<�������� ����������="'+its(teLiciLic.AsInteger)+'" �000000000011="'+teLicLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',teLicLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',teLicLicDateE.AsDateTime)+'" �000000000014="'+teLicLicOrg.AsString+'" />';
              writeln(f,s);
              S:='			</��������>'; writeln(f,s);

              teLic.Delete;
            end else teLic.Next;
          end;


          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />';
          writeln(f,s);

          S:='		</����������>'; writeln(f,s);
        end else
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';  writeln(f,s);
          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />'; writeln(f,s);
          S:='		</����������>'; writeln(f,s);
        end;
        tePost.Next;
      end;

      S:='	</�����������>'; //����� ������������
      writeln(f,s);

      Memo1.Lines.Add('     ������'); delay(10);

      S:='	<��������>';
      writeln(f,s);

      iShop:=0; iVid:=0; iProd:=0;
      teA1.First;

      sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� ������="'+sfmt(sOrg.FullName)+'" �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<��������>'; writeln(f,s);
        S:='				<�������� �������="14" ������="'+sOrg.LicSer+'" ��������="'+sOrg.LicNum+'" ����������="'+ds1(sOrg.dDateB)+'" �����������="'+ds1(sOrg.dDateE)+'" />'; writeln(f,s);
        S:='			</��������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end else
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� �������="'+sfmt(sOrg.FullName)+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+'</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);

        if sOrg.ip=0 then
        begin
          S:='				<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        end else
        begin
          S:='				<�� �����="'+sOrg.Inn+'"/>'; writeln(f,s);
        end;

        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end;

      iNumVid:=1;
      iNumProd:=1;
      while not teA1.Eof do
      begin
        if iShop<>teA1iDep.AsInteger then
        begin
          if iProd>0 then
          begin
            S:='				</����������������>'; writeln(f,s);
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);

            S:='			</������>'; writeln(f,s);
          end;

          if iShop>0 then
          begin
//            S:='   ������� ����� - '+its(iShop)+';';    writeln(f,s);
            S:='		</������������>'; writeln(f,s);
          end;

//          S:='   ������� ������ - '+teA1iDep.AsString+';'+teA1sDep.AsString;  writeln(f,s);
          iShop:=teA1iDep.AsInteger;
          sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

          if sOrg.ip=0 then
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);
          end else
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" ��������������="true">'; writeln(f,s);
          end;

          S:='			<������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
          S:='				<������>620000</������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='				<�����>������������ �</�����>'; writeln(f,s);
          S:='				<����������> </����������>'; writeln(f,s);
          S:='				<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
          S:='				<���>'+sOrg.House+'</���>'; writeln(f,s);
          if sOrg.Korp>'' then
          begin
            S:='				<������>'+sOrg.Korp+'</������>'; writeln(f,s);
          end else
          begin
            S:='				<������ />'; writeln(f,s);
          end;
          S:='				<������ />'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='			</������>'; writeln(f,s);

          //���������� ���������
          iVid:=0;
          iProd:=0;

          iNumVid:=1;
          iNumProd:=1;
        end;
        if iVid<>teA1iVid.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';'; writeln(f,s);
            S:='			</������>'; writeln(f,s);
          end;

//          S:='         ��� ������ - '+teA1iVid.AsString+';'+teA1sVidName.AsString; writeln(f,s);
          S:='			<������ �N="'+its(iNumVid)+'" �000000000003="'+its(teA1iVid.AsInteger)+'">'; writeln(f,s);


          iVid:=teA1iVid.AsInteger;
          //���������� ���������
          iProd:=0;

          iNumProd:=1;

          inc(iNumVid);
        end;
        if iProd<>teA1iProd.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

//          S:='               ������������� ������ - '+teA1iProd.AsString; writeln(f,s);
          S:='				<���������������� �N="'+its(iNumProd)+'" �����������="'+its(teA1iProd.AsInteger)+'">'; writeln(f,s);

          iProd:=teA1iProd.AsInteger;

        end;

        iNumPost:=1;
        iPost:=0;
        rSumVid:=0;

        teA2.First;
        while not teA2.Eof do
        begin
//          S:='                      ��������� - '+teA2iDep.AsString+';'+teA2iVid.AsString+';'+teA2iProd.AsString+';'+teA2iPost.AsString+';'+teA2iLic.AsString+';'+teA2DocDate.AsString+';'+teA2DocNum.AsString+';'+teA2GTD.AsString+';'+fs(teA2rQIn.AsFloat);         writeln(f,s);
          if iPost<>teA2iPost.AsInteger then
          begin
            if iPost>0 then //����� �������
            begin
              S:='					</���������>'; writeln(f,s);
            end;
            //������ ������
            if cxComboBox1.ItemIndex=0 then
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" ����������="'+its(teA2iLic.AsInteger)+'">';  writeln(f,s);
            end else
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" >';  writeln(f,s);
            end;

            iPost:=teA2iPost.AsInteger;

            inc(iNumPost);
          end;

          // ����� ��������
//          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="'+teA2GTD.AsString+'" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          rSumVid:=rSumVid+teA2rQIn.AsFloat;

          teA2.Next;
        end;;
        if iPost>0 then
        begin
          S:='					</���������>'; writeln(f,s);
        end;

        if abs(rSumVid-teA1rQIn2.AsFloat)>0.000001 then
        begin
          Memo1.Lines.Add('   ������ �����������: ��-�� '+rfmt(teA1rQIn2.AsFloat)+' �� ���������� '+rfmt(rSumVid)); delay(10);
          Memo1.Lines.Add('   '+its(iShop)+';'+its(iVid)+';'+its(iProd)); delay(10);
        end;

//        S:='   '+teA1iDep.AsString+';'+teA1sDep.AsString+';'+teA1iVid.AsString+';'+teA1sVidName.AsString+';'+teA1iProd.AsString+' - ������.';
//        writeln(f,s);

        if cxComboBox1.ItemIndex=0 then
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="0.00000"';
          S:=S+' �100000000014="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000015="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000016="'+rfmt(teA1rQOut2.AsFloat)+'"'; //������ ������
          S:=S+' �100000000017="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000018="0.00000"';
          S:=S+' �100000000019="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000020="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end else
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000014="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000015="'+rfmt(teA1rQOut2.AsFloat)+'"';
          S:=S+' �100000000016="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000017="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000018="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end;

        teA1.Next;   inc(iNumProd);
      end;
      if iProd>0 then
      begin
//        S:='               ������������� ����� - '+its(iProd); writeln(f,s);

        S:='				</����������������>'; writeln(f,s);
      end;
      if iVid>0 then
      begin
//        S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);
        S:='			</������>'; writeln(f,s);
      end;
      if iShop>0 then
      begin
//        S:='   ������� ����� - '+its(iShop)+';';
        S:='		</������������>'; writeln(f,s);
      end;

      S:='	</��������>';
      writeln(f,s);
      S:='</����>';
      writeln(f,s);

    finally
      Memo1.Lines.Add('������������ ���������.'); delay(10);
      teProds.Active:=False;
      tePost.Active:=False;
      teLic.Active:=False;
      closefile(f);
    end;
  end;
end;

procedure TfmAddAlco.acRecalcLicExecute(Sender: TObject);
Var LicSerN,LicOrg:String;
    LicDateB,LicDateE:tDateTime;

begin
  Memo1.Clear;
  Memo1.Lines.Add('���� �������������� ������ �������� �����������. ����� ...');
  ViAlco2.BeginUpdate;
  try
    with dmMt do
    begin
      if ptCliLic.Active=False then ptCliLic.Active:=True;
      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        LicSerN:='';
        LicOrg:='';
        LicDateB:=0;
        LicDateE:=0;

        ptCliLic.CancelRange;
        ptCliLic.SetRange([1,teAlcoIniPost.AsInteger],[1,teAlcoIniPost.AsInteger]);
        ptCliLic.First;
        while not ptCliLic.Eof do
        begin
          LicSerN:=OemToAnsiConvert(ptCliLicSER.AsString)+' '+OemToAnsiConvert(ptCliLicSNUM.AsString);
          LicOrg:=OemToAnsiConvert(ptCliLicORGAN.AsString);
          LicDateB:=ptCliLicDDATEB.AsDateTime;
          LicDateE:=ptCliLicDDATEE.AsDateTime;

          ptCliLic.Next;
        end;

        teAlcoIn.Edit;
//        teAlcoIniLic.AsInteger:=0;
        teAlcoInsLicNumber.AsString:=LicSerN;
        teAlcoInLicDateB.AsDateTime:=LicDateB;
        teAlcoInLicDateE.AsDateTime:=LicDateE;
        teAlcoInLicOrg.AsString:=LicOrg;
        teAlcoIn.Post;

        teAlcoIn.Next;
      end;
    end;
  finally
    ViAlco2.EndUpdate;
    Memo1.Lines.Add('�������� ���������.');
    ShowMessage('�������� ���������');
  end;
end;

procedure TfmAddAlco.acInputPostExecute(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    iCli,iTypePost:INteger;
    i,iAdd,iDep,iCliCB:INteger;
    Sinn,Skpp:String;
begin
  //������� ����������
  if fmSelPost=nil then fmSelPost:=tfmSelPost.Create(Application);
  fmSelPost.ShowModal;
  if fmSelPost.ModalResult=mrOk then
  begin
    if MessageDlg('������� ������ �� ���������� '+fmSelPost.cxButtonEdit1.Text+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fName:=fmSelPost.cxButtonEdit2.Text;
      iCli:=fmSelPost.cxButtonEdit1.Tag;
      iTypePost:=fmSelPost.Label4.Tag;
      iDep:=cxLookupComboBox1.EditValue;
      if FileExists(fName) then
      begin
        //����� ���������
        Memo1.Clear;
        Memo1.Lines.Add('������ ��������� ������ �� ����������..'); delay(10);

        ViAlco2.BeginUpdate;
        teAlcoIn.First;
        while not teAlcoIn.Eof do
          if teAlcoIniPost.AsInteger=iCli then teAlcoIn.Delete else teAlcoIn.Next;
        ViAlco2.EndUpdate;
        delay(100);

        Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        Workbook := ExcelApp.WorkBooks.Add(fName);
        ISheet := Workbook.Worksheets.Item[1];

        i:=2;  iAdd:=0;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          {
          sType:=Trim(String(ISheet.Cells.Item[i,1].Value));
          sNumDoc:=Trim(String(ISheet.Cells.Item[i,4].Value));
          sMh:=Trim(String(ISheet.Cells.Item[i,5].Value));
          sDate:=Trim(String(ISheet.Cells.Item[i,3].Value));
          DocDate:=StrToDateDef(sDate,date);
           }

          teAlcoIn.Append;
          teAlcoIniDep.AsInteger:=iDep;
          teAlcoInsDep.AsString:=cxLookupComboBox1.Text;
          teAlcoIniVid.AsInteger:=StrToIntDef(String(ISheet.Cells.Item[i,1].Value),0);
          teAlcoInsVid.AsString:=String(ISheet.Cells.Item[i,1].Value);
          teAlcoInsVidName.AsString:=String(ISheet.Cells.Item[i,2].Value);
          teAlcoIniProd.AsInteger:=prFindProd(String(ISheet.Cells.Item[i,4].Value),String(ISheet.Cells.Item[i,5].Value),String(ISheet.Cells.Item[i,3].Value));
          teAlcoInsProd.AsString:=String(ISheet.Cells.Item[i,3].Value);
          teAlcoInsProdInn.AsString:=String(ISheet.Cells.Item[i,4].Value);
          teAlcoInsProdKPP.AsString:=String(ISheet.Cells.Item[i,5].Value);
          teAlcoIniPost.AsInteger:=iCli;
          teAlcoInsPost.AsString:=prFindCliName2(iTypePost,iCli,Sinn,Skpp,iCliCB);
          teAlcoInsPostInn.AsString:=Sinn;
          teAlcoInsPostKpp.AsString:=Skpp;
          teAlcoIniLic.AsInteger:=prFindLic(iCli,StrToDateDef(String(ISheet.Cells.Item[i,10].Value),date),StrToDateDef(String(ISheet.Cells.Item[i,11].Value),date),String(ISheet.Cells.Item[i,12].Value),String(ISheet.Cells.Item[i,9].Value));
          teAlcoInsLicNumber.AsString:=String(ISheet.Cells.Item[i,9].Value);
          teAlcoInLicDateB.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,10].Value),date);
          teAlcoInLicDateE.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,11].Value),date);
          teAlcoInLicOrg.AsString:=String(ISheet.Cells.Item[i,12].Value);
          teAlcoInDocDate.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,13].Value),date);
          teAlcoInDocNum.AsString:=String(ISheet.Cells.Item[i,14].Value);
          teAlcoInGTD.AsString:=String(ISheet.Cells.Item[i,15].Value);
{
          sQ:=String(ISheet.Cells.Item[i,16].Value);
          while pos(',',sQ)>0 do sQ[pos(',',sQ)]:='.';
}
          teAlcoInrQIn.AsFloat:=StrToFloatDef(String(ISheet.Cells.Item[i,16].Value),0);
          teAlcoInDocId.AsInteger:=0;
          teAlcoIn.Post;

          inc(iAdd);

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('  ��������� '+its(iAdd)); delay(10);
          end;
        end;
        Memo1.Lines.Add('  ��������� '+its(iAdd)); delay(10);
        Memo1.Lines.Add('������� ��������.'); delay(10);

        ExcelApp.Quit;
        ExcelApp:=Unassigned;
      end else showmessage('���� �� ������');
    end;
  end;
end;

end.
