object fmObrPredz: TfmObrPredz
  Left = 371
  Top = 217
  Width = 732
  Height = 402
  Caption = #1054#1073#1088#1072#1073#1086#1090#1082#1072' '#1087#1088#1077#1076#1079#1072#1082#1072#1079#1086#1074' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 724
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'SpeedItem1'
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = 'SpeedItem1|'
      Spacing = 1
      Left = 554
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acPeriod
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Spacing = 1
      Left = 114
      Top = 4
      Visible = True
      OnClick = acPeriodExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel|'
      Spacing = 1
      Left = 324
      Top = 4
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 312
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    Style.BorderStyle = ebsOffice11
    TabOrder = 1
    Height = 56
    Width = 724
  end
  object PBar1: TcxProgressBar
    Left = 0
    Top = 291
    Align = alBottom
    ParentColor = False
    Position = 100.000000000000000000
    Properties.BarBevelOuter = cxbvRaised
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clBlue
    Properties.EndColor = 16745090
    Properties.PeakValue = 100.000000000000000000
    Style.Color = clWhite
    TabOrder = 2
    Visible = False
    Width = 724
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 724
    Height = 243
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 3
    object Edit1: TEdit
      Left = 264
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object GrObrPredz: TcxGrid
      Left = 1
      Top = 1
      Width = 722
      Height = 241
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      object ViewObrPredz: TcxGridDBTableView
        DragMode = dmAutomatic
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        OnCellClick = ViewObrPredzCellClick
        OnEditing = ViewObrPredzEditing
        DataController.DataSource = dsteobrPredz
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object ViewObrPredzRecId: TcxGridDBColumn
          Caption = #8470#1087#1087
          DataBinding.FieldName = 'RecId'
          Visible = False
          Options.Editing = False
          Options.Grouping = False
        end
        object ViewObrPredziCli: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
          DataBinding.FieldName = 'iCli'
          Visible = False
          Options.Editing = False
          Options.Grouping = False
        end
        object ViewObrPredzNameCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'NameCli'
          Options.Editing = False
          Options.Grouping = False
          Width = 203
        end
        object ViewObrPredzDep: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
          DataBinding.FieldName = 'Depart'
          Visible = False
          Options.Editing = False
          Options.Grouping = False
        end
        object ViewObrPredzNameDep: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'NAMEDEP'
          Options.Editing = False
          Options.Grouping = False
          Width = 112
        end
        object ViewObrPredzMolVoz: TcxGridDBColumn
          Caption = #1050#1086#1085#1090#1072#1082#1090#1085#1086#1077' '#1083#1080#1094#1086' '#1087#1086' '#1074#1086#1079#1074#1088'.'
          DataBinding.FieldName = 'MOLVOZ'
          Options.Editing = False
        end
        object ViewObrPredzPhoneVoz: TcxGridDBColumn
          Caption = #1058#1077#1083'. '#1087#1086' '#1074#1086#1079#1074#1088'.'
          DataBinding.FieldName = 'PHONEVOZ'
          Options.Editing = False
        end
        object ViewObrPredzEmailVoz: TcxGridDBColumn
          Caption = 'Email '#1074#1086#1079#1074#1088'.'
          DataBinding.FieldName = 'EMAILVOZ'
          Options.Editing = False
        end
        object ViewObrPredzNameGr1: TcxGridDBColumn
          Caption = #1043#1088#1091#1087#1087#1072
          DataBinding.FieldName = 'NameGr1'
          Options.Editing = False
          Options.Grouping = False
          Width = 150
        end
        object ViewObrPredzNameGr2: TcxGridDBColumn
          Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
          DataBinding.FieldName = 'NameGr2'
          Options.Editing = False
          Options.Grouping = False
          Width = 150
        end
        object ViewObrPredzNameGr3: TcxGridDBColumn
          Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
          DataBinding.FieldName = 'NameGr3'
          Options.Editing = False
          Options.Grouping = False
          Width = 150
        end
        object ViewObrPredzIdCode1: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
          DataBinding.FieldName = 'IdCode1'
          Options.Editing = False
          Options.Grouping = False
          Width = 89
        end
        object ViewObrPredzNameC: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NameC'
          Options.Editing = False
          Options.Grouping = False
          Width = 202
        end
        object ViewObrPredzQOut: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
          DataBinding.FieldName = 'QOut'
          Options.Editing = False
          Options.Grouping = False
          Styles.Content = dmMC.cxStyle5
          Width = 67
        end
        object ViewObrPredzQObr: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1076#1083#1103' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
          DataBinding.FieldName = 'QObr'
          Options.Grouping = False
          Styles.Content = dmMC.cxStyle23
          Width = 75
        end
        object ViewObrPredzQAllRem: TcxGridDBColumn
          Caption = #1054#1073#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'QALLREM'
          Options.Editing = False
          Styles.Content = dmMC.cxStyle23
          Width = 132
        end
        object ViewObrPredzProc: TcxGridDBColumn
          Caption = #1044#1077#1081#1089#1090#1074#1080#1077
          DataBinding.FieldName = 'Proc'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1054#1095#1080#1089#1090#1080#1090#1100
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1057#1084#1077#1085#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
              Value = 2
            end
            item
              Description = #1057#1084#1077#1085#1080#1090#1100' '#1086#1090#1076#1077#1083
              Value = 4
            end
            item
              Description = #1053#1080#1095#1077#1075#1086
              Value = 3
            end>
          Properties.OnCloseUp = ViewObrPredzProcPropertiesCloseUp
          Options.Grouping = False
          Width = 96
        end
        object ViewObrPredzTypeVoz: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
          DataBinding.FieldName = 'TypeVoz'
          Options.Editing = False
        end
      end
      object LevObrPredz: TcxGridLevel
        GridView = ViewObrPredz
      end
    end
  end
  object teobrPredz: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 64
    Top = 144
    object teobrPredzCodePoluch: TIntegerField
      FieldName = 'CodePoluch'
    end
    object teobrPredziCli: TIntegerField
      FieldName = 'iCli'
    end
    object teobrPredzNameCli: TStringField
      FieldName = 'NameCli'
      Size = 50
    end
    object teobrPredzMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 50
    end
    object teobrPredzPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object teobrPredzEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 50
    end
    object teobrPredzDepart: TIntegerField
      FieldName = 'Depart'
    end
    object teobrPredzNAMEDEP: TStringField
      FieldName = 'NAMEDEP'
      Size = 50
    end
    object teobrPredzIdCode1: TIntegerField
      FieldName = 'IdCode1'
    end
    object teobrPredzNameC: TStringField
      FieldName = 'NameC'
      Size = 100
    end
    object teobrPredzEdIzm: TIntegerField
      FieldName = 'EdIzm'
    end
    object teobrPredzQOut: TFloatField
      FieldName = 'QOut'
      DisplayFormat = '0.000'
    end
    object teobrPredzQObr: TFloatField
      FieldName = 'QObr'
      DisplayFormat = '0.000'
    end
    object teobrPredzQALLREM: TFloatField
      FieldName = 'QALLREM'
      DisplayFormat = '0.000'
    end
    object teobrPredzNameGr1: TStringField
      FieldName = 'NameGr1'
      Size = 50
    end
    object teobrPredzNameGr2: TStringField
      FieldName = 'NameGr2'
      Size = 50
    end
    object teobrPredzNameGr3: TStringField
      FieldName = 'NameGr3'
      Size = 50
    end
    object teobrPredzIdGroup: TIntegerField
      FieldName = 'IdGroup'
    end
    object teobrPredzIdSGroup: TIntegerField
      FieldName = 'IdSGroup'
    end
    object teobrPredzProc: TIntegerField
      FieldName = 'Proc'
      OnChange = teobrPredzProcChange
    end
    object teobrPredzTypeVoz: TStringField
      FieldName = 'TypeVoz'
    end
  end
  object dsteobrPredz: TDataSource
    DataSet = teobrPredz
    Left = 68
    Top = 204
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 144
    Top = 136
  end
  object amobrPredz: TActionManager
    Left = 232
    Top = 144
    StyleName = 'XP Style'
    object acPeriod: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      OnExecute = acPeriodExecute
    end
    object acProc: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      OnExecute = acProcExecute
    end
    object acChangeCli: TAction
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      OnExecute = acChangeCliExecute
    end
    object acChangeDep: TAction
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1086#1090#1076#1077#1083
      OnExecute = acChangeDepExecute
    end
    object acAllPost: TAction
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' ('#1083#1102#1073#1086#1075#1086')'
      OnExecute = acAllPostExecute
    end
    object acReadBar: TAction
      ShortCut = 16449
      OnExecute = acReadBarExecute
    end
    object acReadBar1: TAction
      Caption = 'acReadBar1'
      ShortCut = 16450
      OnExecute = acReadBar1Execute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 320
    Top = 208
    object N1: TMenuItem
      Action = acAllPost
    end
    object N2: TMenuItem
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1087#1088#1080#1095#1080#1085#1091' '#1074#1086#1079#1074#1088#1072#1090#1072
      OnClick = N2Click
    end
  end
end
