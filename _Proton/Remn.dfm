object fmRemn: TfmRemn
  Left = 889
  Top = 409
  Width = 334
  Height = 270
  Caption = #1054#1089#1090#1072#1090#1086#1082
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 188
    Width = 326
    Height = 49
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 100
      Top = 12
      Width = 97
      Height = 29
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridR: TcxGrid
    Left = 8
    Top = 20
    Width = 309
    Height = 153
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewR: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMC.dsquRemn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Remn'
          Column = ViewRRemn
        end
        item
          Format = '0.000'
          Kind = skSum
          Column = ViewRRemnVoz
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewRName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'Name'
        Width = 165
      end
      object ViewRRemn: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Remn'
      end
      object ViewRRemnVoz: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
        DataBinding.FieldName = 'QRem'
        Width = 88
      end
    end
    object LevelR: TcxGridLevel
      GridView = ViewR
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 64
    Top = 28
  end
end
