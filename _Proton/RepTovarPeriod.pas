unit RepTovarPeriod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxClasses, cxGraphics, cxCustomData, cxStyles, cxEdit,
  cxCustomPivotGrid, cxPivotGrid, cxProgressBar, cxControls, cxContainer,
  cxTextEdit, cxMemo, ExtCtrls, SpeedBar, cxDBPivotGrid, DB, dxmdaset,
  StdCtrls, ComCtrls, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSCore, dxPScxCommon, dxPScxPivotGrid2Lnk;

type
  TfmRepTovarPeriod = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    PivotGridTovarPeriod: TcxDBPivotGrid;
    mPeriod: TdxMemData;
    dsRepTovarPeriod: TDataSource;
    PivotGridTovarPeriodRDate: TcxDBPivotGridField;
    PivotGridTovarPeriodCode: TcxDBPivotGridField;
    PivotGridTovarPeriodQuant: TcxDBPivotGridField;
    PivotGridTovarPeriodRSumCor: TcxDBPivotGridField;
    PivotGridTovarPeriodGoodsName: TcxDBPivotGridField;
    PivotGridTovarPeriodgr3Name: TcxDBPivotGridField;
    PivotGridTovarPeriodgr2Name: TcxDBPivotGridField;
    PivotGridTovarPeriodgr1Name: TcxDBPivotGridField;
    PivotGridTovarPeriodDepartName: TcxDBPivotGridField;
    PivotGridTovarPeriodNAMEBRAND: TcxDBPivotGridField;
    PivotGridTovarPeriodSID: TcxDBPivotGridField;
    PivotGridTovarPeriodMonth: TcxDBPivotGridField;
    PivotGridTovarPeriodyear: TcxDBPivotGridField;
    PivotGridTovarPeriodkvartal: TcxDBPivotGridField;
    StatusBar1: TStatusBar;
    mPeriodDat: TStringField;
    Print1: TdxComponentPrinter;
    Print1Link1: TcxPivotGridReportLink;
    Panel1: TPanel;
    Memo1: TcxMemo;
    SpeedItem6: TSpeedItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepTovarPeriod: TfmRepTovarPeriod;

implementation

Uses MDB,Un1, ExcelList, SortPar;

{$R *.dfm}

procedure TfmRepTovarPeriod.SpeedItem1Click(Sender: TObject);
begin
  dmmc.quRepTovarPeriod.Active:=false;
  fmRepTovarPeriod.Close;
end;

procedure TfmRepTovarPeriod.SpeedItem5Click(Sender: TObject);
begin
  //StrWk:=fmRepTovarPeriod.Caption;
  Print1Link1.ReportTitle.Text:=fmRepTovarPeriod.Caption;
  Print1.Preview(True,nil);
end;

procedure TfmRepTovarPeriod.SpeedItem3Click(Sender: TObject);
var
  PG:TcxDBPivotGrid;
begin
  //������� � ������
  fmExcelList:=tfmExcelList.Create(Application);
  with fmExcelList do
  begin
    GridEx.Align:=AlClient;
    ViewEx.BeginUpdate;

    dsSummary.PivotGrid:=PivotGridTovarPeriod;
    PG:=PivotGridTovarPeriod;

    dsSummary.CreateData;

    try
      ViewEx.ClearItems;
      ViewEx.DataController.CreateAllItems;
      CreateFooterSummary(ViewEx,PG);
    finally
      ViewEx.EndUpdate;
    end;
  end;
  fmExcelList.ShowModal;
  fmExcelList.dsSummary.PivotGrid:=nil;
  fmExcelList.Release;

end;


procedure TfmRepTovarPeriod.SpeedItem6Click(Sender: TObject);
Var i:INteger;
    StrWk:String;
    fField:TcxPivotGridField;
begin
  if PivotGridTovarPeriod.Visible then
  begin
    with fmSortF do
    begin
      CloseTe(taRowF);
      CloseTe(taColVal);

      for i:=0 to PivotGridTovarPeriod.FieldCount-1 do
      begin
        if PivotGridTovarPeriod.Fields[i].Area=faRow then
        begin
          taRowF.Append;
          taRowFId.AsInteger:=i;
          taRowFCapt.AsString:=PivotGridTovarPeriod.Fields[i].Caption;
          StrWk:=PivotGridTovarPeriod.Fields[i].Name;
//          Delete(StrWk,1,10);
          taRowFNameF.AsString:=StrWk;
          taRowF.Post;
        end;

        if PivotGridTovarPeriod.Fields[i].Area=faData then
        begin
          taColVal.Append;
          taColValId.AsInteger:=i;
          taColValCapt.AsString:=PivotGridTovarPeriod.Fields[i].Caption;
          StrWk:=PivotGridTovarPeriod.Fields[i].Name;
//          Delete(StrWk,1,10);
          taColValNameF.AsString:=StrWk;
          taColVal.Post;
        end;
      end;

      taRowF.Last;
      if taRowF.RecordCount>0 then
      begin
        cxLookupComboBox1.EditValue:=taRowFNameF.AsString;
        cxLookupComboBox1.Text:=taRowFCapt.AsString;
        cxLookupComboBox1.Enabled:=True;
      end else
      begin
        cxLookupComboBox1.EditValue:='';
        cxLookupComboBox1.Text:='��� ��������';
        cxLookupComboBox1.Enabled:=False;
      end;


      taColVal.First;
      if taColVal.RecordCount>0 then
      begin
        cxLookupComboBox2.EditValue:=taColValNameF.AsString;
        cxLookupComboBox2.Text:=taColValCapt.AsString;
        cxLookupComboBox2.Enabled:=True;
      end else
      begin
        cxLookupComboBox2.EditValue:='';
        cxLookupComboBox2.Text:='��� ��������';
        cxLookupComboBox2.Enabled:=False;
      end;
    end;
    fmSortF.ShowModal;
    if fmSortF.ModalResult=mrOk then
    begin
      // sync settings with selected field
      PivotGridTovarPeriod.BeginUpdate;
      fField:=PivotGridTovarPeriod.GetFieldByName(fmSortF.cxLookupComboBox1.Text);
      if fmSortF.cxRadioButton1.Checked then fField.SortOrder:=soDescending;
      if fmSortF.cxRadioButton2.Checked then fField.SortOrder:=soAscending;
      fField.SortBySummaryInfo.Field := PivotGridTovarPeriod.GetFieldByName(fmSortF.cxLookupComboBox2.Text);

      if fmSortF.cxCheckBox1.Checked then
      begin
        fField.TopValueCount:=0;
        fField.TopValueShowOthers:=True;
      end
      else
      begin
        fField.TopValueCount:=fmSortF.cxSpinEdit1.EditValue;
        fField.TopValueShowOthers:=fmSortF.CheckBox1.Checked;
      end;

      PivotGridTovarPeriod.EndUpdate;
    end;
  end;
end;

end.
