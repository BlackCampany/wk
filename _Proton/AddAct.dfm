object fmAddAct: TfmAddAct
  Left = 266
  Top = 114
  Width = 987
  Height = 606
  Caption = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 161
    Top = 81
    Width = 818
    Height = 431
    Align = alClient
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 357
      Width = 814
      Height = 72
      Align = alBottom
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 0
      object Memo1: TcxMemo
        Left = 2
        Top = 2
        Align = alClient
        Lines.Strings = (
          'Memo1')
        ParentFont = False
        Properties.OEMConvert = True
        Properties.ReadOnly = True
        Properties.ScrollBars = ssVertical
        Properties.WordWrap = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Pitch = fpFixed
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        OnDblClick = Memo1DblClick
        Height = 68
        Width = 810
      end
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 814
      Height = 355
      ActivePage = TabSheet1
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = #1056#1072#1089#1093#1086#1076
        object GridAO: TcxGrid
          Left = 0
          Top = 0
          Width = 806
          Height = 324
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewAO: TcxGridDBTableView
            PopupMenu = PopupMenu1
            OnDragDrop = ViewAODragDrop
            OnDragOver = ViewAODragOver
            NavigatorButtons.ConfirmDelete = False
            OnEditing = ViewAOEditing
            OnEditKeyDown = ViewAOEditKeyDown
            OnEditKeyPress = ViewAOEditKeyPress
            DataController.DataSource = dsSpecO
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'Quant'
                Column = ViewAOQuant
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn'
                Column = ViewAOSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumUch'
                Column = ViewAOSumUch
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn0'
                Column = ViewAOSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'RNds'
                Column = ViewAORNds
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'Quant'
                Column = ViewAOQuant
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewAOSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumUch'
                Column = ViewAOSumUch
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn0'
                Column = ViewAOSumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'RNds'
                Column = ViewAORNds
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'RNds'
                Column = ViewAORNds
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewAONum: TcxGridDBColumn
              Caption = #8470' '#1087#1087
              DataBinding.FieldName = 'Num'
              Width = 34
            end
            object ViewAOIdGoods: TcxGridDBColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'IdGoods'
              Width = 57
            end
            object ViewAONameG: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NameG'
              Width = 142
            end
            object ViewAOSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = True
              Properties.OnButtonClick = ViewAOSMPropertiesButtonClick
              Width = 60
            end
            object ViewAOQuant: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'Quant'
            end
            object ViewAOPriceIn: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. (c '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn'
            end
            object ViewAOSumIn: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1089' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn'
            end
            object ViewAOPriceUch: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
              DataBinding.FieldName = 'PriceUch'
            end
            object ViewAOSumUch: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1091#1095#1077#1090#1085'.'
              DataBinding.FieldName = 'SumUch'
            end
            object ViewAOPriceIn0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn0'
            end
            object ViewAOSumIn0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn0'
            end
            object ViewAOSNds: TcxGridDBColumn
              Caption = #1053#1044#1057
              DataBinding.FieldName = 'SNds'
            end
            object ViewAORNds: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057
              DataBinding.FieldName = 'RNds'
            end
          end
          object LevelAO: TcxGridLevel
            GridView = ViewAO
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1055#1088#1080#1093#1086#1076
        ImageIndex = 1
        object GridAI: TcxGrid
          Left = 0
          Top = 0
          Width = 806
          Height = 324
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewAI: TcxGridDBTableView
            PopupMenu = PopupMenu2
            OnDragDrop = ViewAIDragDrop
            OnDragOver = ViewAIDragOver
            NavigatorButtons.ConfirmDelete = False
            OnEditing = ViewAIEditing
            OnEditKeyDown = ViewAIEditKeyDown
            OnEditKeyPress = ViewAIEditKeyPress
            DataController.DataSource = dsSpecI
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'Quant'
                Column = ViewAIQuant
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn'
                Column = ViewAISumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumUch'
                Column = ViewAISumUch
              end
              item
                Format = '0.0'
                Kind = skSum
                Position = spFooter
                FieldName = 'ProcPrice'
                Column = ViewAIProcPrice
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SumIn0'
                Column = ViewAISumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'RNds'
                Column = ViewAIRNds
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'Quant'
                Column = ViewAIQuant
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewAISumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'PriceUch'
                Column = ViewAISumUch
              end
              item
                Format = '0.0'
                Kind = skSum
                FieldName = 'ProcPrice'
                Column = ViewAIProcPrice
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn0'
                Column = ViewAISumIn0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'RNds'
                Column = ViewAIRNds
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewAINum: TcxGridDBColumn
              Caption = #8470
              DataBinding.FieldName = 'Num'
              Width = 43
            end
            object ViewAIIdGoods: TcxGridDBColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'IdGoods'
              Width = 37
            end
            object ViewAINameG: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NameG'
              Width = 158
            end
            object ViewAISM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = True
              Properties.OnButtonClick = ViewAISMPropertiesButtonClick
              Width = 63
            end
            object ViewAIQuant: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'Quant'
            end
            object ViewAIPriceIn: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1089' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn'
            end
            object ViewAISumIn: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1089' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn'
            end
            object ViewAIPriceUch: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
              DataBinding.FieldName = 'PriceUch'
            end
            object ViewAISumUch: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1091#1095#1077#1090#1085'.'
              DataBinding.FieldName = 'SumUch'
            end
            object ViewAIProcPrice: TcxGridDBColumn
              Caption = '% '#1094#1077#1085#1099
              DataBinding.FieldName = 'ProcPrice'
            end
            object ViewAIPriceIn0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'PriceIn0'
            end
            object ViewAISumIn0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
              DataBinding.FieldName = 'SumIn0'
            end
            object ViewAISNds: TcxGridDBColumn
              Caption = #1053#1044#1057
              DataBinding.FieldName = 'SNds'
            end
            object ViewAIRNds: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057
              DataBinding.FieldName = 'RNds'
            end
          end
          object LevelAI: TcxGridLevel
            GridView = ViewAI
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 512
    Width = 979
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label2: TLabel
      Left = 336
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Label2'
      Visible = False
    end
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 113
      Height = 25
      Action = acSave
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 8
      Width = 113
      Height = 25
      Caption = #1042#1099#1093#1086#1076'   F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 553
    Width = 979
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 979
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 48
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label15: TLabel
      Left = 264
      Top = 48
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object Label4: TLabel
      Left = 440
      Top = 16
      Width = 70
      Height = 13
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 44
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 145
    end
    object cxTextEdit2: TcxTextEdit
      Left = 528
      Top = 12
      TabStop = False
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Text = 'cxTextEdit2'
      Width = 305
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 81
    Width = 161
    Height = 431
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label3: TLabel
      Left = 8
      Top = 152
      Width = 94
      Height = 13
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = Label3Click
    end
    object cxLabel1: TcxLabel
      Left = 8
      Top = 8
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 80
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'   F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel7: TcxLabel
      Left = 8
      Top = 24
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'  Ctrl+Ins '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel7Click
    end
    object cxLabel9: TcxLabel
      Left = 8
      Top = 96
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100'     Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel9Click
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 376
    Top = 192
  end
  object dsSpecO: TDataSource
    DataSet = taSpecO
    Left = 224
    Top = 244
  end
  object amAct: TActionManager
    Left = 228
    Top = 328
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acSave: TAction
      Caption = 'acSave'
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acCalc1: TAction
      Caption = 'acCalc1'
      ShortCut = 16505
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
  end
  object dsSpecI: TDataSource
    DataSet = taSpecI
    Left = 460
    Top = 244
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 407
    Top = 330
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Images = dmO.imState
    Left = 487
    Top = 326
    object MenuItem1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = MenuItem1Click
    end
  end
  object taSpecO: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 223
    Top = 190
    object taSpecONum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecOIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecONameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecOIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecOSM: TStringField
      FieldName = 'SM'
    end
    object taSpecOQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecOQuantChange
      DisplayFormat = '0.000'
    end
    object taSpecOPriceIn: TFloatField
      FieldName = 'PriceIn'
      OnChange = taSpecOPriceInChange
      DisplayFormat = '0.00'
    end
    object taSpecOSumIn: TFloatField
      FieldName = 'SumIn'
      OnChange = taSpecOSumInChange
      DisplayFormat = '0.00'
    end
    object taSpecOPriceUch: TFloatField
      FieldName = 'PriceUch'
      OnChange = taSpecOPriceUchChange
      DisplayFormat = '0.00'
    end
    object taSpecOSumUch: TFloatField
      FieldName = 'SumUch'
      OnChange = taSpecOSumUchChange
      DisplayFormat = '0.00'
    end
    object taSpecOKm: TFloatField
      FieldName = 'Km'
      DisplayFormat = '0.000'
    end
    object taSpecOPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      OnChange = taSpecOPriceIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecOSumIn0: TFloatField
      FieldName = 'SumIn0'
      OnChange = taSpecOSumIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecOINds: TSmallintField
      FieldName = 'INds'
    end
    object taSpecOSNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpecORNds: TFloatField
      FieldName = 'RNds'
      DisplayFormat = '0.00'
    end
  end
  object taSpecI: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 459
    Top = 194
    object taSpecINum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecINameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecISM: TStringField
      FieldName = 'SM'
    end
    object taSpecIQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecIQuantChange
    end
    object taSpecIPriceIn: TFloatField
      FieldName = 'PriceIn'
      OnChange = taSpecIPriceInChange
      DisplayFormat = '0.00'
    end
    object taSpecISumIn: TFloatField
      FieldName = 'SumIn'
      OnChange = taSpecISumInChange
      DisplayFormat = '0.00'
    end
    object taSpecIPriceUch: TFloatField
      FieldName = 'PriceUch'
      OnChange = taSpecIPriceUchChange
      DisplayFormat = '0.00'
    end
    object taSpecISumUch: TFloatField
      FieldName = 'SumUch'
      OnChange = taSpecISumUchChange
      DisplayFormat = '0.00'
    end
    object taSpecIKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecIPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      OnChange = taSpecIPriceIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecISumIn0: TFloatField
      FieldName = 'SumIn0'
      OnChange = taSpecISumIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecIProcPrice: TFloatField
      FieldName = 'ProcPrice'
    end
    object taSpecIINds: TSmallintField
      FieldName = 'INds'
    end
    object taSpecISNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpecIRNds: TFloatField
      FieldName = 'RNds'
      DisplayFormat = '0.00'
    end
  end
end
