unit mAddCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, cxControls, cxContainer, cxEdit,
  cxRadioGroup, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalc, StdCtrls,
  cxLookAndFeelPainters, cxButtons, cxButtonEdit, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGroupBox,
  cxCheckBox, cxCheckListBox, cxSpinEdit;

type
  TfmAddCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    RadioGroup1: TcxRadioGroup;
    RadioGroup2: TcxRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit1: TcxMaskEdit;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    MaskEdit2: TcxMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    TextEdit1: TcxTextEdit;
    TextEdit2: TcxTextEdit;
    Label5: TLabel;
    ButtonEdit1: TcxButtonEdit;
    Label6: TLabel;
    ButtonEdit2: TcxButtonEdit;
    CalcEdit2: TcxCalcEdit;
    amCards: TActionManager;
    acBarRead: TAction;
    cxButton1: TcxButton;
    acTextEdit2: TAction;
    acClear: TAction;
    CalcEdit3: TcxCalcEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label7: TLabel;
    CalcEdit1: TcxCalcEdit;
    Label10: TLabel;
    LookupComboBox1: TcxLookupComboBox;
    Label11: TLabel;
    Label12: TLabel;
    CalcEdit4: TcxCalcEdit;
    CalcEdit5: TcxCalcEdit;
    cxButton4: TcxButton;
    Label13: TLabel;
    CalcEdit6: TcxCalcEdit;
    Label15: TLabel;
    LookupComboBox3: TcxLookupComboBox;
    Label16: TLabel;
    LookupComboBox4: TcxLookupComboBox;
    CheckListBox1: TcxCheckListBox;
    acExit: TAction;
    Label14: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label17: TLabel;
    cxCalcEdit2: TcxCalcEdit;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxCalcEdit3: TcxCalcEdit;
    Label21: TLabel;
    cxSpinEdit3: TcxSpinEdit;
    cxCalcEdit4: TcxCalcEdit;
    Label22: TLabel;
    CheckBox1: TcxCheckBox;
    Label23: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Label24: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    procedure ButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure RadioGroup1PropertiesChange(Sender: TObject);
    procedure RadioGroup2PropertiesChange(Sender: TObject);
    procedure acBarReadExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acTextEdit2Execute(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure ButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MaskEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddCard: TfmAddCard;
  bCreate:Boolean = False;

implementation

uses MDB, mCountry, mCards, Un1, ClasSel;


{$R *.dfm}

procedure TfmAddCard.ButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  PosP.Id:=0;;
  PosP.Name:='';
  if fmCountry.Visible then fmCountry.Close;
  delay(100);
  fmCountry.ShowModal;
  if fmCountry.ModalResult=mrOk then
  begin
    ButtonEdit1.Tag:=PosP.Id;
    ButtonEdit1.Text:=PosP.Name;
  end;
end;

procedure TfmAddCard.RadioGroup1PropertiesChange(Sender: TObject);
Var iItem:Integer;
    sBar,StrWk,sM:String;
begin
  if bCreate then exit;
  iItem:=StrToIntDef(MaskEdit1.Text,0);
  if RadioGroup1.ItemIndex=1 then
  begin
    RadioGroup2.ItemIndex:=1;
    RadioGroup2.Enabled:=False;
    CalcEdit3.EditValue:=0.001;
  end
  else
  begin
    RadioGroup2.ItemIndex:=0;
    RadioGroup2.Enabled:=True;
    CalcEdit3.EditValue:=1;
  end;
  with dmMC do
  begin
   //��������� ����
    if RadioGroup1.ItemIndex=0 then
    begin //������� �����

      if MaskEdit1.Tag=0 then
      begin
        iItem:=prFindNewArt(False,sM);

        MaskEdit1.Text:=IntToStr(iItem);
      end;

        //������ ��������
      if RadioGroup2.ItemIndex=0 then
      begin
        sBar:='';
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        sBar:=IntToStr(iItem);
        while Length(sBar)<5 do sBar:='0'+sBar;
        sBar:=CommonSet.Prefix+sBar;
        sBar:=ToStandart(sBar);
      end;
      if RadioGroup2.ItemIndex=2 then
      begin
//        if not taBar.Locate('Bar',IntToStr(iItem),[]) then sBar:=IntToStr(iItem)
//        else sBar:='';
      end;
    end
    else
    begin //������� �����
      if MaskEdit1.Tag=0 then
      begin
        iItem:=prFindNewArt(True,sM);
        MaskEdit1.Text:=IntToStr(iItem);
      end;

      //������ ��������
      StrWk:=IntToStr(iItem);
      while Length(StrWk)<5 do StrWk:='0'+StrWk;
      sBar:=CommonSet.PrefixVes+StrWk;
//      sBar:=CommonSet.PrefixVes+IntToStr(iItem);
//      sBar:=ToStandart(sBar);
    end;
    MaskEdit2.Text:=sBar;
  end;
end;

procedure TfmAddCard.RadioGroup2PropertiesChange(Sender: TObject);
Var iItem:Integer;
    sBar,sM:String;
    iC:INteger;
begin
  with dmMC do
  begin
    if bCreate then exit;
    iItem:=StrToIntDef(MaskEdit1.Text,0);
   //��������� ����
    if RadioGroup1.ItemIndex=0 then
    begin //������� �����
      if MaskEdit1.Tag=0 then
      begin
        iItem:=prFindNewArt(False,sM);
        MaskEdit1.Text:=IntToStr(iItem);
      end;
        //������ ��������
      if RadioGroup2.ItemIndex=0 then
      begin
        sBar:='';
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        sBar:=IntToStr(iItem);
        while Length(sBar)<5 do sBar:='0'+sBar;
        sBar:=CommonSet.Prefix+sBar;
        sBar:=ToStandart(sBar);
      end;
      if RadioGroup2.ItemIndex=2 then
      begin
        if not prFindBar(IntToStr(iItem),iItem,iC) then sBar:=IntToStr(iItem)
        else
        begin
          sBar:='';
          ShowMessage('�� ����� ������� '+IntToStr(iC));
        end;
      end;
    end
    else
    begin //������� �����
      if MaskEdit1.Tag=0 then
      begin
        iItem:=prFindNewArt(True,sM);
        MaskEdit1.Text:=IntToStr(iItem);
      end;

      //������ ��������

      sBar:=IntToStr(iItem);
      while Length(sBar)<5 do sBar:='0'+sBar;
      sBar:=CommonSet.Prefix+sBar;
      sBar:=ToStandart(sBar);
    end;
    MaskEdit2.Text:=sBar;
  end;
end;

procedure TfmAddCard.acBarReadExecute(Sender: TObject);
begin
  if MaskEdit1.Tag=0 then MaskEdit2.SetFocus;
end;

procedure TfmAddCard.cxButton1Click(Sender: TObject);
//Var StrWk:String;
begin
{  if not bViewC then
  begin
    StrWk:=TextEdit1.Text;
    if Length(StrWk)>30 then StrWk:=Copy(StrWk,1,30);
    TextEdit1.Text:=StrWk;

    StrWk:=TextEdit2.Text;
    if Length(StrWk)>60 then StrWk:=Copy(StrWk,1,60);
    TextEdit2.Text:=StrWk;
  end;}
end;

procedure TfmAddCard.acTextEdit2Execute(Sender: TObject);
//Var StrWk:String;
begin
 { if not bViewC then
  begin
    StrWk:=TextEdit1.Text;
    if Length(StrWk)>30 then StrWk:=Copy(StrWk,1,30);
    TextEdit1.Text:=StrWk;

    StrWk:=TextEdit2.Text;
    if Length(StrWk)>60 then StrWk:=Copy(StrWk,1,60);
    TextEdit2.Text:=StrWk;

    TextEdit2.Text:=TextEdit1.Text;
  end;}
end;

procedure TfmAddCard.acClearExecute(Sender: TObject);
begin
  SetGoodsDef(0);
end;

procedure TfmAddCard.ButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  PosP.Id:=0;
  PosP.Name:='';
  if fmClassSel.Visible then fmClassSel.Close;
  delay(100);
  fmClassSel.ShowModal;
  if fmClassSel.ModalResult=mrOk then
  begin
    ButtonEdit2.Tag:=PosP.Id;
    ButtonEdit2.Text:=PosP.Name;
  end;
end;

procedure TfmAddCard.MaskEdit2PropertiesEditValueChanged(Sender: TObject);
Var iNum,iC:INteger;
begin
  if bCreate then exit;
  if MaskEdit1.Tag=0 then
  begin
    iC:=0;
    if prFindBar(MaskEdit2.Text,iC,iNum)=True then
    begin
      ShowMessage('�������� "'+MaskEdit2.Text+'" ��� ���� � ������� ('+IntToStr(iNum)+'). ������������ ���������.');
      MaskEdit2.Text:='';
    end;
  end;
end;

procedure TfmAddCard.cxButton2Click(Sender: TObject);
//Var iItem:Integer;
//    bErr:Boolean;
begin
  fmAddCard.ModalResult:=mrOk;
end;

procedure TfmAddCard.cxButton4Click(Sender: TObject);
Var iItem:Integer;
    sBar,StrWk,sM:String;
begin
  with dmMC do
  begin
    if RadioGroup1.ItemIndex=0 then
    begin //������� �����
      iItem:=prFindNewArt(False,sM);
      //������ ��������
      if RadioGroup2.ItemIndex=0 then
      begin
        sBar:='';
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        sBar:=IntToStr(iItem);
        while Length(sBar)<5 do sBar:='0'+sBar;
        sBar:=CommonSet.Prefix+sBar;
        sBar:=ToStandart(sBar);
      end;
      if RadioGroup2.ItemIndex=2 then
      begin
//        if not taBar.Locate('Bar',IntToStr(iItem),[]) then sBar:=IntToStr(iItem)
//        else sBar:='';
      end;
    end
    else
    begin //������� �����
     { quMax.Active:=False;
      quMax.SQL.Clear;
      quMax.SQL.Add('SElect MaxId=Max(Id) from Goods');
      quMax.SQL.Add('Where Id>='+INtToStr(CommonSet.iMinVes));
      quMax.SQL.Add('And Id<='+INtToStr(CommonSet.iMaxVes));
      quMax.Active:=True;
      iItem:=quMaxMaxId.AsInteger+1;
      if iItem<CommonSet.iMinVes then iItem:=CommonSet.iMinVes;
      quMax.Active:=False;
      }
      iItem:=prFindNewArt(True,sM);

      //������ ��������

      StrWk:=IntToStr(iItem);
      while Length(StrWk)<5 do StrWk:='0'+StrWk;
      sBar:=CommonSet.PrefixVes+StrWk;
//        sBar:=ToStandart(sBar);  //�������� � 7 ������
    end;

    if iItem=0 then
    begin
      Showmessage(sM);
      Showmessage('���������� ����������.');
    end else
    begin
      MaskEdit1.Text:=IntToStr(iItem);
      MaskEdit2.Text:=sBar;
    end;  
  end;
  cxButton2.SetFocus;
end;

procedure TfmAddCard.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrNone;
  Close;
end;

end.
