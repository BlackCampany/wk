unit AddSingle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxTextEdit, cxControls, cxContainer, cxEdit, cxLabel, cxCheckBox,
  ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmAddSingle = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxCheckBox1: TcxCheckBox;
    amAddSingle: TActionManager;
    acExit: TAction;
    cxLabel3: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prSetParams(S:String;i:Integer);
  end;

var
  fmAddSingle: TfmAddSingle;

implementation

{$R *.dfm}
Procedure TfmAddSingle.prSetParams(S:String;i:Integer);
begin
  Caption:=S;
  if i=0 then //������� �������
  begin
    fmAddSingle.cxLabel2.Visible:=False;
    fmAddSingle.cxCheckBox1.Visible:=False;
    fmAddSingle.cxTextEdit1.Enabled:=True;
    fmAddSingle.cxLabel3.Visible:=False;
    fmAddSingle.cxTextEdit2.Visible:=False;
  end;
  if i=1 then  //��� � ��� ������
  begin
    fmAddSingle.cxLabel2.Visible:=True;
    fmAddSingle.cxCheckBox1.Visible:=True;
    fmAddSingle.cxTextEdit1.Enabled:=False;
    fmAddSingle.cxLabel3.Visible:=True;
    fmAddSingle.cxTextEdit2.Visible:=True;
  end;

end;

procedure TfmAddSingle.FormShow(Sender: TObject);
begin
  if cxTextEdit1.Enabled then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end;
  if cxTextEdit2.Visible then
  begin
    cxTextEdit2.SetFocus;
    cxTextEdit2.SelectAll;
  end;
end;

procedure TfmAddSingle.acExitExecute(Sender: TObject);
begin

  ModalResult:=mrCancel;
end;

end.
