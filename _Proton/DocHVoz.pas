unit DocHVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar, cxCheckBox, cxTimeEdit,
  DBClient;

type
  TfmDocs8 = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocVoz: TcxGrid;
    ViewDocVoz: TcxGridDBTableView;
    LevelDocVoz: TcxGridLevel;
    amDocVoz: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem5: TSpeedItem;
    ViewCards: TcxGridDBTableView;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    teDocVoz: TdxMemData;
    dsteDocVoz: TDataSource;
    ViewCardsDepart: TcxGridDBColumn;
    ViewCardsDepName: TcxGridDBColumn;
    ViewCardsDateInvoice: TcxGridDBColumn;
    ViewCardsNumber: TcxGridDBColumn;
    ViewCardsCliName: TcxGridDBColumn;
    ViewCardsCode: TcxGridDBColumn;
    ViewCardsCardName: TcxGridDBColumn;
    ViewCardsiM: TcxGridDBColumn;
    ViewCardsKol: TcxGridDBColumn;
    ViewCardsKolMest: TcxGridDBColumn;
    ViewCardsKolWithMest: TcxGridDBColumn;
    ViewCardsCenaTovar: TcxGridDBColumn;
    ViewCardsNewCenaTovar: TcxGridDBColumn;
    ViewCardsSumCenaTovar: TcxGridDBColumn;
    ViewCardsSumNewCenaTovar: TcxGridDBColumn;
    ViewCardsProc: TcxGridDBColumn;
    ViewCardsNdsProc: TcxGridDBColumn;
    ViewCardsNdsSum: TcxGridDBColumn;
    ViewCardsCodeTara: TcxGridDBColumn;
    ViewCardsCenaTara: TcxGridDBColumn;
    ViewCardsSumCenaTara: TcxGridDBColumn;
    ViewCardsiCat: TcxGridDBColumn;
    ViewCardsiTop: TcxGridDBColumn;
    ViewCardsiNov: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    teDocVozId: TIntegerField;
    teDocVozDocNum: TStringField;
    teDocVozComment: TStringField;
    ViewDocVozId: TcxGridDBColumn;
    ViewDocVozDocDate: TcxGridDBColumn;
    ViewDocVozDocNum: TcxGridDBColumn;
    ViewDocVozComment: TcxGridDBColumn;
    acAddDoc8: TAction;
    acViewDoc8: TAction;
    ViewDocVozTime: TcxGridDBColumn;
    ViewDocVozUser: TcxGridDBColumn;
    teDocVozUser: TStringField;
    ViewDocVozStatus: TcxGridDBColumn;
    teDocVozDocDate: TIntegerField;
    teDocVozDocTime: TStringField;
    teDocVozStatus: TStringField;
    PBar1: TcxProgressBar;
    StatusBar1: TStatusBar;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc8Execute(Sender: TObject);
    procedure acViewDoc8Execute(Sender: TObject);
    procedure ViewDocVozDblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
//    procedure acEdit1Execute(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }

    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc8(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

  procedure prloadpredz;

var
  fmDocs8: TfmDocs8;
  bClearDocIn:Boolean = false;

implementation

uses Un1, MDB, Period1, MT, TBuff, u2fdk, ChangeB, ChangePost,
  ChangeT1, PrintAddDocs,dmPS,AddDoc8;

{$R *.dfm}
procedure prloadpredz;
begin

  with fmDocs8 do
  begin
    if (fmAdddoc8.Showing=false) then
    begin
      speeditem2.Enabled:=true;
      speeditem3.Enabled:=true;
      speeditem5.Enabled:=true;
      speeditem10.Enabled:=true;
    end;

    Memo1.Clear;
    with dmP do
    begin
      fmDocs8.ViewDocVoz.BeginUpdate;
      quDocVoz.Active:=false;
      quDocVoz.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quDocVoz.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateEnd);
      quDocVoz.Active:=true;

   {   if (teDocVoz.Active=False) then teDocVoz.Active:=true
      else teDocVoz.Refresh;

      teDocVoz.first;
      while not teDocVoz.Eof do
      begin
        teDocVoz.Delete;
      end;       }

      if quDocVoz.RecordCount>0 then
      begin
        closete(teDocVoz);
        quDocVoz.First;
        while not quDocVoz.Eof do
        begin
          teDocVoz.Append;
          teDocVozId.AsInteger:=quDocVozID.AsInteger;
          teDocVozDocNum.AsString:=quDocVozDOCNUM.AsString;
          teDocVozDocDate.AsInteger:=quDocVozDOCDATE.AsInteger;
          teDocVozDocTime.AsString:=quDocVozDOCTIME.AsString;
          teDocVozComment.AsString:=quDocVozCOMMENT.AsString;
          teDocVozUser.AsString:=quDocVozUSER.AsString;
          case strtoint(quDocVozSTATUS.AsString) of
          0: teDocVozStatus.AsString:='�������';
          1: teDocVozStatus.AsString:='�������� �������';
          2: teDocVozStatus.AsString:='�������';
          end;
          quDocVoz.Next;
        end;
        teDocVoz.Post;
      end;
      teDocVoz.First;
      fmDocs8.ViewDocVoz.EndUpdate;
      fmDocs8.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
    end;
  end;

end;


procedure TfmDocs8.prSetValsAddDoc8(iT:SmallINt); //0-����������, 1-��������������, 2-��������
Var iNum:Integer;
begin
  bOpen:=True;

  if iT=0 then
  begin
    fmAddDoc8.Caption:='���������: ����������.';
    iNum:=prFindNum(25);
    if iNum>0 then
    begin
      fmAddDoc8.cxTextEdit1.Text:=its(iNum);
      fmAddDoc8.cxTextEdit1.Tag:=0;
    end;

    fmAddDoc8.cxDateEdit1.Date:=date;
    fmAddDoc8.cxTextEdit2.Text:='';
    fmAddDoc8.cxTextEdit2.Properties.ReadOnly:=False;

    fmAddDoc8.cxLabel1.Enabled:=True;
    fmAddDoc8.cxLabel2.Enabled:=True;
    fmAddDoc8.cxLabel6.Enabled:=True;

    fmAddDoc8.cxButton1.Enabled:=True;
    fmAddDoc8.cxButton2.Enabled:=True;
    fmAddDoc8.cxButton8.Enabled:=True;
    fmAddDoc8.ViewDoc8.OptionsData.Editing:=True;
    fmAddDoc8.ViewDoc8.OptionsData.Deleting:=True;
    fmAddDoc8.ViewDoc8Quant.Options.Editing:=true;
    fmAddDoc8.ViewDoc8DepartName.Options.Editing:=true;
    fmAddDoc8.ViewDoc8Reason.Options.Editing:=true;

  end;
  if iT=2 then
  begin
    with dmP do
    begin
     

      fmAddDoc8.Caption:='���������: ��������.';
      fmAddDoc8.cxTextEdit1.Text:=teDocVozDocNum.AsString;
      fmAddDoc8.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc8.cxTextEdit1.Tag:=teDocVozId.AsInteger;

      fmAddDoc8.cxDateEdit1.Date:= teDocVozDocDate.AsInteger;
      fmAddDoc8.cxDateEdit1.Properties.ReadOnly:=True;   

      fmAddDoc8.cxTextEdit2.Text:=teDocVozComment.AsString;
      fmAddDoc8.cxTextEdit2.Properties.ReadOnly:=True;

      fmAddDoc8.cxLabel1.Enabled:=False;
      fmAddDoc8.cxLabel2.Enabled:=False;
      fmAddDoc8.cxLabel6.Enabled:=False;
      fmAddDoc8.cxButton1.Enabled:=False;
      fmAddDoc8.cxButton2.Enabled:=true;
      fmAddDoc8.cxButton8.Enabled:=true;

      fmAddDoc8.ViewDoc8.OptionsData.Editing:=False;
      fmAddDoc8.ViewDoc8.OptionsData.Deleting:=False;
      fmAddDoc8.ViewDoc8Quant.Options.Editing:=false;
      fmAddDoc8.ViewDoc8DepartName.Options.Editing:=false;
      fmAddDoc8.ViewDoc8Reason.Options.Editing:=false;
    end;
  end;
  bOpen:=False;

end;

procedure TfmDocs8.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs8.SpeedItem1Click(Sender: TObject);
begin
  fmDocs8.Close;
  //if fmAddDoc8.Showing then fmAddDoc8.Close;
end;

procedure TfmDocs8.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  //Timer1.Enabled:=True;
  GridDocVoz.Align:=AlClient;
  ViewDocVoz.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
 
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs8.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocVoz.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);

end;

procedure TfmDocs8.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

   // fmdocs8.Close;
 
    delay(10);
    prloadpredz;

  //  fmdocs8.Show;
  end;
end;

procedure TfmDocs8.acAddDoc8Execute(Sender: TObject);
begin
  //�������� ��������

  if not CanDo('prAddDocVoz') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  speeditem2.Enabled:=false;
  speeditem3.Enabled:=false;
  speeditem5.Enabled:=false;
  speeditem10.Enabled:=false;

  if fmAddDoc8.Showing then
  begin
   if MessageDlg('�� ������ ������� ���������� ���� ������������?',mtConfirmation,[mbYes, mbNo], 0)=mrYES then
   begin
    fmAddDoc8.Close;

    prSetValsAddDoc8(0); //0-����������, 1-��������������, 2-��������

 {   if (fmAddDoc8.taSpecVoz.Active=False) then fmAddDoc8.taSpecVoz.Active:=true
    else fmAddDoc8.taSpecVoz.Refresh;
    fmAddDoc8.taSpecVoz.first;
    while not fmAddDoc8.taSpecVoz.Eof do  fmAddDoc8.taSpecVoz.Delete;   }

    closete(fmAddDoc8.taSpecVoz);

    fmAddDoc8.Vi1.BeginUpdate;
    dmP.quPost1.Active:=false;
    fmAddDoc8.Vi1.EndUpdate;

    fmAddDoc8.acSaveDoc.Enabled:=True;        //������ ��������� �������
    fmAddDoc8.Show;
    fmAddDoc8.cxTextEdit2.Properties.ReadOnly:=false;
   end
   else fmAddDoc8.Show;
  end
  else
  begin
    prSetValsAddDoc8(0);

    if (fmAddDoc8.taSpecVoz.Active=False) then fmAddDoc8.taSpecVoz.Active:=true
    else fmAddDoc8.taSpecVoz.Refresh;
    fmAddDoc8.taSpecVoz.first;
    while not fmAddDoc8.taSpecVoz.Eof do  fmAddDoc8.taSpecVoz.Delete;

    fmAddDoc8.Vi1.BeginUpdate;
    dmP.quPost1.Active:=false;
    fmAddDoc8.Vi1.EndUpdate;

    fmAddDoc8.acSaveDoc.Enabled:=True;        //������ ��������� �������
    fmAddDoc8.Show;
    fmAddDoc8.cxTextEdit2.Properties.ReadOnly:=false;
  end;


end;

procedure TfmDocs8.acViewDoc8Execute(Sender: TObject);
Var iC,iStep:INteger;
begin
  //��������
  if not CanDo('prViewDocVoz') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

 // if fmAddDoc8.Showing then fmAddDoc8.Close;
  speeditem2.Enabled:=false;
  speeditem3.Enabled:=false;
  speeditem5.Enabled:=false;
  speeditem10.Enabled:=false;

  //��� ���������
  with fmAddDoc8 do
  with dmP do
  begin
      if teDocVoz.RecordCount>0 then //���� ��� ��������
      begin
        ViewDoc8.BeginUpdate;
        quSpecVoz.Active:=False;
        quSpecVoz.ParamByName('IHEAD').AsInteger:=teDocVozId.AsInteger;
        quSpecVoz.ParamByName('IDATE').AsInteger:=teDocVozDocDate.AsInteger;
        quSpecVoz.Active:=True;

       if quSpecVoz.RecordCount<1 then
       begin
         showmessage('��� ������������, �������� ������');
        //  showmessage('�������� �������� ��� ���������.');
         speeditem2.Enabled:=true;
         speeditem3.Enabled:=true;
         speeditem5.Enabled:=true;
         speeditem10.Enabled:=true;
         ViewDoc8.EndUpdate;
         exit;
       end;

       CloseTe(fmAddDoc8.taSpecVoz);

       ViewDoc8.EndUpdate;
       ViewDoc8.BeginUpdate;
       fmAddDoc8.cxTextEdit1.Text:='';
       fmAddDoc8.cxTextEdit2.Text:='';
       fmAddDoc8.PBar1.Position:=0;
       fmAddDoc8.Show;
       fmAddDoc8.PBar1.Visible:=True;

       fmAddDoc8.PBar1.Position:=10; delay(50);
       fmAddDoc8.PBar1.Position:=20; delay(10);

       iStep:=quSpecVoz.RecordCount div 80;
       if iStep=0 then iStep:=quSpecVoz.RecordCount;

   //    ViewDoc8.BeginUpdate;

        quSpecVoz.First;
        iC:=0;
        while not quSpecVoz.Eof do
        begin
          inc(iC);
          taSpecVoz.Append;
          taSpecVozNumber.AsInteger:=iC;
          taSpecVozDOCDATE.AsInteger:=quSpecVozDOCDATE.AsInteger;
          taSpecVozCode.AsInteger:=quSpecVozCode.AsInteger;
          taSpecVozNameCode.AsString:=quSpecVozNameCode.AsString;
          taSpecVozDepart.AsInteger:=quSpecVozDepart.AsInteger;
          taSpecVozDepartName.AsString:=quSpecVozDepartName.AsString;
          taSpecVozIdCli.AsInteger:=quSpecVozIdCli.AsInteger;
          taSpecVozNameCli.AsString:=quSpecVozNameCli.AsString;
          taSpecVozQuant.AsFloat:=quSpecVozQuant.AsFloat;
          taSpecVozQuantOut.AsFloat:=quSpecVozQuantOut.AsFloat;
          taSpecVozqrem.AsFloat:=quSpecVozQRem.AsFloat;
          taSpecVozPriceIn0.AsFloat:=(quSpecVozPriceIn0.AsFloat);
          taSpecVozPriceIn.AsFloat:=(quSpecVozPriceIn.AsFloat);
          taSpecVozRPrice.AsFloat:=(quSpecVozRPrice.AsFloat);
          taSpecVozRSumIn0.AsFloat:=(quSpecVozRSumIn0.AsFloat);
          taSpecVozRSumIn.AsFloat:=(quSpecVozRSumIn.AsFloat);
          taSpecVozRSumNDS.AsFloat:=(quSpecVozRSumNDS.AsFloat);

          case strtoint(quSpecVozStatus.AsString) of
            0: taSpecVozStatus.AsString:='�������';
            1: taSpecVozStatus.AsString:='�������� �������';
            2: taSpecVozStatus.AsString:='�������';
          end;
        //   taSpecVozStatus.AsString:=quSpecVozStatus.AsString;
          taSpecVozDateVoz.AsInteger:=quSpecVozDateVoz.AsInteger;
          taSpecVozReason.AsInteger:=quSpecVozIDReason.AsInteger;

           if (iC mod iStep = 0) then
          begin
            fmAddDoc8.PBar1.Position:=20+(iC div iStep);
            delay(10);
          end;
           delay(10);
          quSpecVoz.Next;
        end;
        taSpecVoz.Post;

        Vi1.BeginUpdate;
        dmP.quPost1.Active:=false;
        Vi1.EndUpdate;

        prSetValsAddDoc8(2); //0-����������, 1-��������������, 2-��������
        fmAddDoc8.PBar1.Position:=100; delay(20);
        fmAddDoc8.PBar1.Visible:=false;
        ViewDoc8.EndUpdate;
        ViewDoc8.Controller.FocusRecord(ViewDoc8.DataController.FocusedRowIndex,True);
        GridDoc8.SetFocus;

      end
      else
      begin
        showmessage('�������� �������� ��� ���������.');
        speeditem2.Enabled:=true;
        speeditem3.Enabled:=true;
        speeditem5.Enabled:=true;
        speeditem10.Enabled:=true;
      
      end;

  end;

end;

procedure TfmDocs8.ViewDocVozDblClick(Sender: TObject);
begin
  //������� �������
 acViewDoc8.Execute; //��������

end;

procedure TfmDocs8.Timer1Timer(Sender: TObject);
begin
{  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;     }
end;

procedure TfmDocs8.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  prNExportExel5(ViewDocVoz);
end;

procedure TfmDocs8.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocVoz);
end;

end.


