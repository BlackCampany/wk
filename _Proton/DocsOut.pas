unit DocsOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocs2 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOut: TcxGrid;
    ViewDocsOut: TcxGridDBTableView;
    LevelDocsOut: TcxGridLevel;
    amDocsOut: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc2: TAction;
    acEditDoc2: TAction;
    acViewDoc2: TAction;
    acDelDoc2: TAction;
    acOnDoc2: TAction;
    acOffDoc2: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewDocsOutDepart: TcxGridDBColumn;
    ViewDocsOutDateInvoice: TcxGridDBColumn;
    ViewDocsOutNumber: TcxGridDBColumn;
    ViewDocsOutSCFNumber: TcxGridDBColumn;
    ViewDocsOutSummaTovarPost: TcxGridDBColumn;
    ViewDocsOutSummaTovarSpis: TcxGridDBColumn;
    ViewDocsOutNacenka: TcxGridDBColumn;
    ViewDocsOutSummaTara: TcxGridDBColumn;
    ViewDocsOutAcStatus: TcxGridDBColumn;
    ViewDocsOutChekBuh: TcxGridDBColumn;
    ViewDocsOutSCFDate: TcxGridDBColumn;
    ViewDocsOutID: TcxGridDBColumn;
    ViewDocsOutNameDep: TcxGridDBColumn;
    ViewDocsOutNameCli: TcxGridDBColumn;
    acAdd1: TAction;
    ViewDocsOutNDSTovar: TcxGridDBColumn;
    ViewDocsOutProvodType: TcxGridDBColumn;
    acChangeT: TAction;
    N5: TMenuItem;
    ViewDocsOutCrock: TcxGridDBColumn;
    acChangeOtpr: TAction;
    N6: TMenuItem;
    acRecalcMoveSel: TAction;
    acPrintAddDocs: TAction;
    N7: TMenuItem;
    acSetGTDFromIn: TAction;
    N8: TMenuItem;
    acRecalcNDS: TAction;
    acSCHF: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    N12: TMenuItem;
    N11: TMenuItem;
    acNewRecNDS: TAction;
    acTestMove: TAction;
    N13: TMenuItem;
    acCreateIn: TAction;
    N14: TMenuItem;
    acSendPro: TAction;
    N15: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc2Execute(Sender: TObject);
    procedure acEditDoc2Execute(Sender: TObject);
    procedure acViewDoc2Execute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure acDelDoc2Execute(Sender: TObject);
    procedure acOnDoc2Execute(Sender: TObject);
    procedure acOffDoc2Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acAdd1Execute(Sender: TObject);
    procedure ViewDocsOutCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acChangeTExecute(Sender: TObject);
    procedure acChangeOtprExecute(Sender: TObject);
    procedure acRecalcMoveSelExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
    procedure acSetGTDFromInExecute(Sender: TObject);
    procedure acRecalcNDSExecute(Sender: TObject);
    procedure acSCHFExecute(Sender: TObject);
    procedure acNewRecNDSExecute(Sender: TObject);
    procedure acTestMoveExecute(Sender: TObject);
    procedure acCreateInExecute(Sender: TObject);
    procedure acSendProExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc2(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;
  procedure prClearFromInv(Depart:Integer;SDate:TDate;Number:String);
  procedure prEnableTara(n:integer);
  procedure prEnableTovar(n:integer);
var
  fmDocs2: TfmDocs2;
  bClearDocOut:Boolean = false;

implementation


uses Un1, MDB, Period1, AddDoc2, MT, TBuff, ChangeT, ChangeT1,
  PrintAddDocs, u2fdk, dmPS, SetTypeDocOut, SelDep, InvVoz, dbe, dmFbPro;

{$R *.dfm}
procedure prEnableTara(n:integer);
begin
  if n=0 then
  begin
    fmAddDoc2.ViewDoc2CodeTara.Visible:=false;           //���� ���
    fmAddDoc2.ViewDoc2NameTara.Visible:=false;
    fmAddDoc2.ViewDoc2CenaTara.Visible:=false;
    fmAddDoc2.ViewDoc2SumCenaTara.Visible:=false;
    fmAddDoc2.ViewDoc2VesTara.Visible:=false;


    fmAddDoc2.ViewDoc2NameTara.Options.Editing:=false;
    fmAddDoc2.ViewDoc2CenaTara.Options.Editing:=false;
    fmAddDoc2.ViewDoc2SumCenaTara.Options.Editing:=false;
    fmAddDoc2.ViewDoc2VesTara.Options.Editing:=false;       //���� ���
  end;

  if n=1 then
  begin
    fmAddDoc2.ViewDoc2CodeTara.Visible:=true;           //���� ���
    fmAddDoc2.ViewDoc2NameTara.Visible:=true;
    fmAddDoc2.ViewDoc2CenaTara.Visible:=true;
    fmAddDoc2.ViewDoc2SumCenaTara.Visible:=true;
    fmAddDoc2.ViewDoc2VesTara.Visible:=true;
    fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=true;

    fmAddDoc2.ViewDoc2NameTara.Options.Editing:=true;
    fmAddDoc2.ViewDoc2CenaTara.Options.Editing:=true;
    fmAddDoc2.ViewDoc2SumCenaTara.Options.Editing:=true;
    fmAddDoc2.ViewDoc2VesTara.Options.Editing:=true;       //���� ���
  end;


end;
procedure prEnableTovar(n:integer);
begin
  if n=0 then
  begin
    fmAddDoc2.ViewDoc2CodeTovar.Visible:=false;           //����� ���
    fmAddDoc2.ViewDoc2Name.Visible:=false;
    fmAddDoc2.ViewDoc2CodeEdIzm.Visible:=false;
    fmAddDoc2.ViewDoc2BarCode.Visible:=false;
    fmAddDoc2.ViewDoc2CenaTovar.Visible:=false;
    fmAddDoc2.ViewDoc2CenaTovarSpis.Visible:=false;
    fmAddDoc2.ViewDoc2SumCenaTovar.Visible:=false;
    fmAddDoc2.ViewDoc2SumCenaTovarSpis.Visible:=false;
    fmAddDoc2.ViewDoc2NDSProc.Visible:=false;
    fmAddDoc2.ViewDoc2NDSSum.Visible:=false;
    fmAddDoc2.ViewDoc2OutNDSSum.Visible:=false;
    fmAddDoc2.ViewDoc2FullName.Visible:=false;
    fmAddDoc2.ViewDoc2PostDate.Visible:=false;
    fmAddDoc2.ViewDoc2PostNum.Visible:=false;
    fmAddDoc2.ViewDoc2PostQuant.Visible:=false;
    fmAddDoc2.ViewDoc2rNac.Visible:=false;
    fmAddDoc2.ViewDoc2KolSpis.Visible:=false;
    fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=false;
    fmAddDoc2.ViewDoc2CheckCM.Visible:=false;
    fmAddDoc2.ViewDoc2Remn.Visible:=false;           //
    fmAddDoc2.ViewDoc2VesTara.Visible:=false;        //
    fmAddDoc2.ViewDoc2Brak.Visible:=false;           //
    fmAddDoc2.ViewDoc2Kol.Visible:=false;            //
    fmAddDoc2.ViewDoc2KolF.Visible:=false;           //
    fmAddDoc2.ViewDoc2KolWithMest.Visible:=false;    //
    fmAddDoc2.ViewDoc2GTD.Visible:=false;            //



    fmAddDoc2.ViewDoc2CodeTovar.Options.Editing:=false;
    fmAddDoc2.ViewDoc2Name.Options.Editing:=false;
    fmAddDoc2.ViewDoc2BarCode.Options.Editing:=false;
    fmAddDoc2.ViewDoc2CenaTovarSpis.Options.Editing:=false;
    fmAddDoc2.ViewDoc2SumCenaTovarSpis.Options.Editing:=false;
    fmAddDoc2.ViewDoc2NDSProc.Options.Editing:=false;
    fmAddDoc2.ViewDoc2NDSSum.Options.Editing:=false;
    fmAddDoc2.ViewDoc2OutNDSSum.Options.Editing:=false;
    fmAddDoc2.ViewDoc2Remn.Options.Editing:=false;
    fmAddDoc2.ViewDoc2VesTara.Options.Editing:=false;
    fmAddDoc2.ViewDoc2Brak.Options.Editing:=false;
    fmAddDoc2.ViewDoc2Kol.Options.Editing:=false;
    fmAddDoc2.ViewDoc2KolF.Options.Editing:=false;
    fmAddDoc2.ViewDoc2KolWithMest.Options.Editing:=false;
    fmAddDoc2.ViewDoc2GTD.Options.Editing:=false;           //����� ���
  end;
  if n=1 then
  begin
    fmAddDoc2.ViewDoc2CodeTovar.Visible:=True;           //����� ���
    fmAddDoc2.ViewDoc2Name.Visible:=True;
    fmAddDoc2.ViewDoc2CodeEdIzm.Visible:=True;
    fmAddDoc2.ViewDoc2BarCode.Visible:=True;
    fmAddDoc2.ViewDoc2CenaTovar.Visible:=True;
    fmAddDoc2.ViewDoc2CenaTovarSpis.Visible:=True;
    fmAddDoc2.ViewDoc2SumCenaTovar.Visible:=True;
    fmAddDoc2.ViewDoc2SumCenaTovarSpis.Visible:=True;
    fmAddDoc2.ViewDoc2NDSProc.Visible:=True;
    fmAddDoc2.ViewDoc2NDSSum.Visible:=True;
    fmAddDoc2.ViewDoc2OutNDSSum.Visible:=True;
    fmAddDoc2.ViewDoc2FullName.Visible:=True;
    fmAddDoc2.ViewDoc2PostDate.Visible:=True;
    fmAddDoc2.ViewDoc2PostNum.Visible:=True;
    fmAddDoc2.ViewDoc2PostQuant.Visible:=True;
    fmAddDoc2.ViewDoc2rNac.Visible:=True;
    fmAddDoc2.ViewDoc2CheckCM.Visible:=True;
    fmAddDoc2.ViewDoc2KolSpis.Visible:=True;
    fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=true;
    fmAddDoc2.ViewDoc2Remn.Visible:=True;           //
    fmAddDoc2.ViewDoc2VesTara.Visible:=True;        //
    fmAddDoc2.ViewDoc2Brak.Visible:=True;           //
    fmAddDoc2.ViewDoc2Kol.Visible:=true;            //
    fmAddDoc2.ViewDoc2KolF.Visible:=True;           //
    fmAddDoc2.ViewDoc2KolWithMest.Visible:=True;    //
    fmAddDoc2.ViewDoc2GTD.Visible:=True;            //
    {
    fmAddDoc2.ViewDoc2CodeTovar.Options.Editing:=false;//True;
    fmAddDoc2.ViewDoc2Name.Options.Editing:=false;//True;
    fmAddDoc2.ViewDoc2BarCode.Options.Editing:=false;//True;
    }
    fmAddDoc2.ViewDoc2CenaTovarSpis.Options.Editing:=True;
    fmAddDoc2.ViewDoc2SumCenaTovarSpis.Options.Editing:=True;
    fmAddDoc2.ViewDoc2NDSProc.Options.Editing:=True;
    fmAddDoc2.ViewDoc2NDSSum.Options.Editing:=True;
    fmAddDoc2.ViewDoc2OutNDSSum.Options.Editing:=True;
    fmAddDoc2.ViewDoc2Remn.Options.Editing:=True;
    fmAddDoc2.ViewDoc2VesTara.Options.Editing:=True;
    fmAddDoc2.ViewDoc2Brak.Options.Editing:=True;
    fmAddDoc2.ViewDoc2Kol.Options.Editing:=True;
    fmAddDoc2.ViewDoc2KolF.Options.Editing:=True;
    fmAddDoc2.ViewDoc2KolWithMest.Options.Editing:=True;
    fmAddDoc2.ViewDoc2GTD.Options.Editing:=True;

    if pos('Torg',Person.Name)>0 then
      fmAddDoc2.ViewDoc2CheckCM.Options.Editing:=True
    else
      fmAddDoc2.ViewDoc2CheckCM.Options.Editing:=False;


    //����� ���
  end;

end;

procedure TfmDocs2.prSetValsAddDoc2(iT:SmallINt); //0-����������, 1-��������������, 2-��������
Var iNum:Integer;
    StrWk1,StrWk2,StrWk3,SInnF:String;
begin

  with dmMC do
  with dmMT do
  with dmE do
  begin
    bOpen:=True;

    dmP.quAllDepart.Active:=False; dmP.quAllDepart.Active:=True; dmP.quAllDepart.First;

    //���.����
{
    if ptMOL.Active=False then ptMOL.Active:=True;
    fmAddDoc2.cxComboBox2.Properties.Items.Clear;
    fmAddDoc2.cxComboBox2.Properties.Items.Add(''); //������ ����
    ptMol.First;
    while not ptMol.Eof do
    begin
      fmAddDoc2.cxComboBox2.Properties.Items.Add(ptMOLOTVL.AsString);
      ptMol.Next;
    end;
    fmAddDoc2.cxComboBox2.ItemIndex:=ptMOL.RecordCount;
}
    fmAddDoc2.cxButton14.Visible:=False;
    fmAddDoc2.cxButton15.Visible:=False;
    fmAddDoc2.cxButton16.Visible:=False;

    if (iT=0)or(iT=1) then
    begin
      if CommonSet.RC=1 then //��� ��������������� ��������� ���
      begin
        if ptTTnInLn.Active=False then ptTTnInLn.Active:=True else ptTTnInLn.Refresh;
        ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';
      end;
    end;

    if iT=0 then
    begin
      fmAddDoc2.Caption:='���������: ����������.';
      iNum:=prFindNum(2)+1;

      if iNum>0 then
      begin
        fmAddDoc2.cxTextEdit1.Text:=its(iNum);
        fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit1.Tag:=0;

        fmAddDoc2.cxTextEdit2.Text:=its(iNum);
        fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit2.Tag:=iNum;
      end else
      begin
        fmAddDoc2.cxTextEdit1.Text:='';
        fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit1.Tag:=0;

        fmAddDoc2.cxTextEdit2.Text:='';
        fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit2.Tag:=0;
      end;

      fmAddDoc2.cxTextEdit10.Text:='';
      fmAddDoc2.cxTextEdit10.Tag:=0;
      fmAddDoc2.cxDateEdit10.Date:=0;
      fmAddDoc2.cxDateEdit10.Tag:=0;

      fmadddoc2.cxButton1.Tag:=0;

      fmAddDoc2.cxDateEdit1.Date:=date;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc2.cxDateEdit2.Date:=date;
      fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc2.cxCurrencyEdit1.EditValue:=0;
      fmAddDoc2.cxButtonEdit1.Tag:=1;
      fmAddDoc2.cxButtonEdit1.EditValue:=0;
      fmAddDoc2.cxButtonEdit1.Text:='';
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

      fmAddDoc2.cxButton3.Enabled:=False;
      fmAddDoc2.cxButton4.Enabled:=False;
      fmAddDoc2.cxButton5.Enabled:=False;
      fmAddDoc2.cxButton11.Enabled:=False;
      fmAddDoc2.cxButton13.Enabled:=false;


      fmAddDoc2.cxLookupComboBox1.EditValue:=dmP.quAllDepartID.AsInteger;   //p
      fmAddDoc2.cxLookupComboBox1.Text:=dmP.quAllDepartName.AsString;       //p
      fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;


      quUPLMh.Active:=False;
      quUPLMh.ParamByName('IMH').AsInteger:=dmP.quAllDepartID.AsInteger;
      quUPLMh.Active:=True;

      quUPLMh.First;

      if quUPLMh.RecordCount>0 then
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=quUPLMhID.AsInteger;   //p
        fmAddDoc2.cxLookupComboBox2.Text:=quUPLMhNAME.AsString;       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end
      else
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=0;   //p
        fmAddDoc2.cxLookupComboBox2.Text:='';       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end;

      fmAddDoc2.cxButton1.Enabled:=True;

      if iTypeDO=99 then fmAddDoc2.cxComboBox1.ItemIndex:=10
      else fmAddDoc2.cxComboBox1.ItemIndex:=iTypeDO;

      fmAddDoc2.cxLabel15.Enabled:=true;

      if iTypeDO=99 then     //p ���� ������ ������� ���� �� ������� �������� �������
      begin
        fmAddDoc2.cxLabel1.Enabled:=False;
        fmAddDoc2.cxLabel2.Enabled:=True;        //������� �������
        fmAddDoc2.cxLabel3.Enabled:=True;        //�������� ��������
        fmAddDoc2.cxLabel4.Enabled:=False;
        fmAddDoc2.cxLabel5.Enabled:=true;        //�������� �������
        fmAddDoc2.cxLabel6.Enabled:=True;        //��������
        fmAddDoc2.cxLabel7.Enabled:=False;
        fmAddDoc2.cxLabel8.Enabled:=False;
        fmAddDoc2.cxLabel9.Enabled:=False;
        fmAddDoc2.cxLabel10.Enabled:=False;
        fmAddDoc2.cxLabel11.Enabled:=False;
        fmAddDoc2.cxLabel12.Enabled:=False;
        fmAddDoc2.cxLabel15.Enabled:=False;    //�������
        prEnableTara(1);
        prEnableTovar(0);
      end
      else
      begin
        if iTypeDO=1 then     // ���� ������ ������� ������, �� ������� �������� �������
        begin
          fmAddDoc2.cxLabel7.Enabled:=True;
          fmAddDoc2.cxLabel8.Enabled:=True;

          fmAddDoc2.cxLabel1.Enabled:=True;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;
          fmAddDoc2.cxLabel9.Enabled:=True;
          fmAddDoc2.cxLabel10.Enabled:=True;
          fmAddDoc2.cxLabel11.Enabled:=True;
          fmAddDoc2.cxLabel12.Enabled:=True;
          fmAddDoc2.cxLabel15.Enabled:=True;    //�������
          prEnableTara(0);
          prEnableTovar(1);

        end
        else                                      // ���� ��� ����� ������, �� ������� ��� �������
        begin
          fmAddDoc2.cxLabel1.Enabled:=true;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;
          fmAddDoc2.cxLabel7.Enabled:=True;
          fmAddDoc2.cxLabel8.Enabled:=True;
          fmAddDoc2.cxLabel9.Enabled:=True;
          fmAddDoc2.cxLabel10.Enabled:=True;
          fmAddDoc2.cxLabel11.Enabled:=True;
          fmAddDoc2.cxLabel12.Enabled:=True;
          fmAddDoc2.cxLabel15.Enabled:=True;    //�������
          prEnableTara(1);
          prEnableTovar(1);

        end;
      end;

      fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

//      fmAddDoc2.ViewDoc2CheckCM.Visible:=True;

      fmAddDoc2.cxComboBox3.ItemIndex:=0;

    end;
    if iT=1 then
    begin
 //     with dmMC do
//      begin
      fmAddDoc2.Caption:='���������: ��������������.';
      fmAddDoc2.cxTextEdit1.Text:=dmMC.quTTnOutNumber.AsString;
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit1.Tag:=dmMC.quTTNOutID.AsInteger;

      fmadddoc2.cxButton1.Tag:=quTTNOutID.AsInteger;

      fmAddDoc2.cxTextEdit10.Text:=quTTNOutNumber.AsString;
      fmAddDoc2.cxTextEdit10.Tag:=quTTNOutID.AsInteger;
      fmAddDoc2.cxDateEdit10.Date:=quTTNOutDateInvoice.AsDateTime;
      fmAddDoc2.cxDateEdit10.Tag:=quTTNOutDepart.AsInteger;

      fmAddDoc2.cxTextEdit2.Text:=quTTNOutSCFNumber.AsString;
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc2.cxTextEdit2.Tag:=0;

      fmAddDoc2.cxDateEdit1.Date:=quTTNOutDateInvoice.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc2.cxDateEdit2.Date:=quTTNOutSCFDate.AsDateTime;
      fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc2.cxCurrencyEdit1.EditValue:=quTTnOutSummaTovar.AsFloat;

      fmAddDoc2.Label4.Tag:=quTTnOutIndexPoluch.AsInteger;
      fmAddDoc2.cxButtonEdit1.Tag:=quTTnOutCodePoluch.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quTTNOutNameCli.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

      prFCliINN(quTTnOutCodePoluch.AsInteger,quTTnOutIndexPoluch.AsInteger,StrWk1,StrWk2,StrWk3,SInnF);

      prFParCli(SInnF,sPretCli,sDogNum,sDogDate);
      if Trim(sPretCli)>'' then
      begin
        fmAddDoc2.cxButton14.Visible:=True;
        fmAddDoc2.cxButton15.Visible:=True;
        fmAddDoc2.cxButton16.Visible:=True;
      end else
      begin
        fmAddDoc2.cxButton14.Visible:=False;
        fmAddDoc2.cxButton15.Visible:=False;
        fmAddDoc2.cxButton16.Visible:=False;
      end;

      dmP.quAllDepart.Locate('ID',quTTNOutDepart.AsInteger,[]);

      fmAddDoc2.cxButton3.Enabled:=False;
      fmAddDoc2.cxButton4.Enabled:=False;
      fmAddDoc2.cxButton5.Enabled:=False;
      fmAddDoc2.cxButton11.Enabled:=False;
      fmAddDoc2.cxButton13.Enabled:=true;     //������������

      fmAddDoc2.cxLabel15.Enabled:=true;

      fmAddDoc2.cxLookupComboBox1.EditValue:=quTTNOutDepart.AsInteger;
      fmAddDoc2.cxLookupComboBox1.Text:=quTTNOutNameDep.AsString;
      fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

      quUPLMh.Active:=False;
      quUPLMh.ParamByName('IMH').AsInteger:=quTTNOutDepart.AsInteger;
      quUPLMh.Active:=True;

      quUPLMh.First;

      if quUPLMh.RecordCount>0 then
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=quUPLMhID.AsInteger;   //p
        fmAddDoc2.cxLookupComboBox2.Text:=quUPLMhNAME.AsString;       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end
      else
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=0;   //p
        fmAddDoc2.cxLookupComboBox2.Text:='';       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end;

      fmAddDoc2.cxButton1.Enabled:=True;

      if iTypeDO=99 then fmAddDoc2.cxComboBox1.ItemIndex:=10
      else fmAddDoc2.cxComboBox1.ItemIndex:=iTypeDO;
      //fmAddDoc2.cxComboBox1.ItemIndex:=quTTnOutProvodType.AsInteger;
      iTypeDO:=quTTnOutProvodType.AsInteger;

      if iTypeDO=99 then     //p ���� ������ ������� ���� �� ������� �������� �������
      begin
        fmAddDoc2.cxLabel1.Enabled:=False;
        fmAddDoc2.cxLabel2.Enabled:=True;        //������� �������
        fmAddDoc2.cxLabel3.Enabled:=True;        //�������� ��������
        fmAddDoc2.cxLabel4.Enabled:=False;
        fmAddDoc2.cxLabel5.Enabled:=true;        //�������� �������
        fmAddDoc2.cxLabel6.Enabled:=True;        //��������
        fmAddDoc2.cxLabel7.Enabled:=False;
        fmAddDoc2.cxLabel8.Enabled:=False;
        fmAddDoc2.cxLabel9.Enabled:=False;
        fmAddDoc2.cxLabel10.Enabled:=False;
        fmAddDoc2.cxLabel11.Enabled:=False;
        fmAddDoc2.cxLabel12.Enabled:=False;
        fmAddDoc2.cxLabel15.Enabled:=False;    //�������
        prEnableTara(1);
        prEnableTovar(0);
      end
      else
      begin
        if iTypeDO=1 then     // ���� ������ ������� ������, �� ������� �������� �������
        begin
          fmAddDoc2.cxLabel7.Enabled:=True;
          fmAddDoc2.cxLabel8.Enabled:=True;

          fmAddDoc2.cxLabel1.Enabled:=True;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;
          fmAddDoc2.cxLabel9.Enabled:=True;
          fmAddDoc2.cxLabel10.Enabled:=True;
          fmAddDoc2.cxLabel11.Enabled:=True;
          fmAddDoc2.cxLabel12.Enabled:=True;
          fmAddDoc2.cxLabel15.Enabled:=True;    //�������
          prEnableTara(0);
          prEnableTovar(1);
        end
        else                                      // ���� ��� ����� ������, �� ������� ��� �������
        begin
          fmAddDoc2.cxLabel1.Enabled:=True;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;
          fmAddDoc2.cxLabel7.Enabled:=True;
          fmAddDoc2.cxLabel8.Enabled:=True;
          fmAddDoc2.cxLabel9.Enabled:=True;
          fmAddDoc2.cxLabel10.Enabled:=True;
          fmAddDoc2.cxLabel11.Enabled:=True;
          fmAddDoc2.cxLabel12.Enabled:=True;
          fmAddDoc2.cxLabel15.Enabled:=True;    //�������
          prEnableTara(1);
          prEnableTovar(1);
        end;
      end;
      fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

      fmAddDoc2.cxComboBox3.ItemIndex:=quTTnOutChekBuh.AsInteger;

    end;
    if iT=2 then
    begin
      fmAddDoc2.Caption:='���������: ��������.';
      fmAddDoc2.cxTextEdit1.Text:=quTTNOutNumber.AsString;
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxTextEdit1.Tag:=quTTNOutID.AsInteger;

      fmadddoc2.cxButton1.Tag:=0;

      fmAddDoc2.cxTextEdit10.Text:=quTTNOutNumber.AsString;
      fmAddDoc2.cxTextEdit10.Tag:=quTTNOutID.AsInteger;
      fmAddDoc2.cxDateEdit10.Date:=quTTNOutDateInvoice.AsDateTime;
      fmAddDoc2.cxDateEdit10.Tag:=quTTNOutDepart.AsInteger;

      fmAddDoc2.cxTextEdit2.Text:=quTTNOutSCFNumber.AsString;
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc2.cxTextEdit2.Tag:=0;

      fmAddDoc2.cxDateEdit1.Date:=quTTNOutDateInvoice.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxDateEdit2.Date:=quTTNOutSCFDate.AsDateTime;
      fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc2.cxCurrencyEdit1.EditValue:=quTTNOutSummaTovar.AsFloat;

      fmAddDoc2.Label4.Tag:=quTTnOutIndexPoluch.AsInteger;
      fmAddDoc2.cxButtonEdit1.Tag:=quTTnOutCodePoluch.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quTTNOutNameCli.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=True;

      prFCliINN(quTTnOutCodePoluch.AsInteger,quTTnOutIndexPoluch.AsInteger,StrWk1,StrWk2,StrWk3,SInnF);

      prFParCli(SInnF,sPretCli,sDogNum,sDogDate);
      if Trim(sPretCli)>'' then
      begin
        fmAddDoc2.cxButton14.Visible:=True;
        fmAddDoc2.cxButton15.Visible:=True;
        fmAddDoc2.cxButton16.Visible:=True;
      end else
      begin
        fmAddDoc2.cxButton14.Visible:=False;
        fmAddDoc2.cxButton15.Visible:=False;
        fmAddDoc2.cxButton16.Visible:=False;
      end;

      fmAddDoc2.cxButton13.Enabled:=false; //�������������

      dmP.quAllDepart.Locate('ID',quTTNOutDepart.AsInteger,[]);

      if quTTNOutAcStatus.AsInteger=3 then
      begin
        fmAddDoc2.cxButton3.Enabled:=True;
        fmAddDoc2.cxButton4.Enabled:=True;
        fmAddDoc2.cxButton5.Enabled:=True;
        fmAddDoc2.cxButton11.Enabled:=True;
      end;

      fmAddDoc2.cxButton11.Enabled:=True;
      fmAddDoc2.cxLookupComboBox1.EditValue:=quTTNOutDepart.AsInteger;
      fmAddDoc2.cxLookupComboBox1.Text:=quTTNOutNameDep.AsString;
      fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=True;

      quUPLMh.Active:=False;
      quUPLMh.ParamByName('IMH').AsInteger:=quTTNOutDepart.AsInteger;
      quUPLMh.Active:=True;

      quUPLMh.First;

      if quUPLMh.RecordCount>0 then
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=quUPLMhID.AsInteger;   //p
        fmAddDoc2.cxLookupComboBox2.Text:=quUPLMhNAME.AsString;       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end
      else
      begin
        fmAddDoc2.cxLookupComboBox2.EditValue:=0;   //p
        fmAddDoc2.cxLookupComboBox2.Text:='';       //p
        fmAddDoc2.cxLookupComboBox2.Properties.ReadOnly:=False;
      end;

      fmAddDoc2.cxLabel1.Enabled:=False;
      fmAddDoc2.cxLabel2.Enabled:=False;
      fmAddDoc2.cxLabel3.Enabled:=False;
      fmAddDoc2.cxLabel4.Enabled:=False;
      fmAddDoc2.cxLabel5.Enabled:=False;
      fmAddDoc2.cxLabel6.Enabled:=False;
      fmAddDoc2.cxLabel7.Enabled:=False;
      fmAddDoc2.cxLabel8.Enabled:=False;
      fmAddDoc2.cxLabel9.Enabled:=False;
      fmAddDoc2.cxLabel10.Enabled:=False;
      fmAddDoc2.cxLabel11.Enabled:=False;    //p
      fmAddDoc2.cxLabel12.Enabled:=False;    //p
      fmAddDoc2.cxLabel15.Enabled:=false;    //p
      fmAddDoc2.cxButton1.Enabled:=False;

      if iTypeDO=99 then fmAddDoc2.cxComboBox1.ItemIndex:=10
      else fmAddDoc2.cxComboBox1.ItemIndex:=iTypeDO;
     // fmAddDoc2.cxComboBox1.ItemIndex:=quTTnOutProvodType.AsInteger;
      iTypeDO:=quTTnOutProvodType.AsInteger;

      if iTypeDO=99 then     //���� ������ ������� ���� �� ������� �������� �������
      begin
        fmAddDoc2.ViewDoc2CodeTara.Visible:=true;           //���� ���
        fmAddDoc2.ViewDoc2NameTara.Visible:=true;
        fmAddDoc2.ViewDoc2CenaTara.Visible:=true;
        fmAddDoc2.ViewDoc2SumCenaTara.Visible:=true;
        fmAddDoc2.ViewDoc2VesTara.Visible:=true;           //���� ���

        fmAddDoc2.ViewDoc2CodeTovar.Visible:=false;           //����� ���
        fmAddDoc2.ViewDoc2Name.Visible:=false;
        fmAddDoc2.ViewDoc2CodeEdIzm.Visible:=false;
        fmAddDoc2.ViewDoc2BarCode.Visible:=false;
        fmAddDoc2.ViewDoc2CenaTovar.Visible:=false;
        fmAddDoc2.ViewDoc2CenaTovarSpis.Visible:=false;
        fmAddDoc2.ViewDoc2SumCenaTovar.Visible:=false;
        fmAddDoc2.ViewDoc2SumCenaTovarSpis.Visible:=false;
        fmAddDoc2.ViewDoc2NDSProc.Visible:=false;
        fmAddDoc2.ViewDoc2NDSSum.Visible:=false;
        fmAddDoc2.ViewDoc2OutNDSSum.Visible:=false;
        fmAddDoc2.ViewDoc2FullName.Visible:=false;
        fmAddDoc2.ViewDoc2PostDate.Visible:=false;
        fmAddDoc2.ViewDoc2PostNum.Visible:=false;
        fmAddDoc2.ViewDoc2PostQuant.Visible:=false;
        fmAddDoc2.ViewDoc2rNac.Visible:=false;
        fmAddDoc2.ViewDoc2CheckCM.Visible:=false;
        fmAddDoc2.ViewDoc2KolSpis.Visible:=false;
        fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=false;
        fmAddDoc2.ViewDoc2Remn.Visible:=false;           //
        fmAddDoc2.ViewDoc2VesTara.Visible:=false;        //
        fmAddDoc2.ViewDoc2Brak.Visible:=false;           //
        fmAddDoc2.ViewDoc2Kol.Visible:=false;            //
        fmAddDoc2.ViewDoc2KolF.Visible:=false;           //
        fmAddDoc2.ViewDoc2KolWithMest.Visible:=false;    //
        fmAddDoc2.ViewDoc2GTD.Visible:=false;            //

        //����� ���
      end
      else
      begin
        if iTypeDO=1 then     //���� ������ ������� ������ �� ������� �������� �������
        begin
          fmAddDoc2.ViewDoc2CodeTara.Visible:=false;           //���� ���
          fmAddDoc2.ViewDoc2NameTara.Visible:=false;
          fmAddDoc2.ViewDoc2CenaTara.Visible:=false;
          fmAddDoc2.ViewDoc2SumCenaTara.Visible:=false;
          fmAddDoc2.ViewDoc2VesTara.Visible:=false;           //���� ���

          fmAddDoc2.ViewDoc2CodeTovar.Visible:=True;           //����� ���
          fmAddDoc2.ViewDoc2Name.Visible:=True;
          fmAddDoc2.ViewDoc2CodeEdIzm.Visible:=True;
          fmAddDoc2.ViewDoc2BarCode.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTovar.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTovarSpis.Visible:=True;
          fmAddDoc2.ViewDoc2SumCenaTovar.Visible:=True;
          fmAddDoc2.ViewDoc2SumCenaTovarSpis.Visible:=True;
          fmAddDoc2.ViewDoc2NDSProc.Visible:=True;
          fmAddDoc2.ViewDoc2NDSSum.Visible:=True;
          fmAddDoc2.ViewDoc2OutNDSSum.Visible:=True;
          fmAddDoc2.ViewDoc2FullName.Visible:=True;
          fmAddDoc2.ViewDoc2PostDate.Visible:=True;
          fmAddDoc2.ViewDoc2PostNum.Visible:=True;
          fmAddDoc2.ViewDoc2PostQuant.Visible:=True;
          fmAddDoc2.ViewDoc2rNac.Visible:=True;
          fmAddDoc2.ViewDoc2CheckCM.Visible:=True;
          fmAddDoc2.ViewDoc2KolSpis.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=true;
          fmAddDoc2.ViewDoc2Remn.Visible:=True;           //
          fmAddDoc2.ViewDoc2VesTara.Visible:=True;        //
          fmAddDoc2.ViewDoc2Brak.Visible:=True;           //
          fmAddDoc2.ViewDoc2Kol.Visible:=true;            //
          fmAddDoc2.ViewDoc2KolF.Visible:=True;           //
          fmAddDoc2.ViewDoc2KolWithMest.Visible:=True;    //
          fmAddDoc2.ViewDoc2GTD.Visible:=True;            //

              //����� ���
        end
        else
        begin
          fmAddDoc2.ViewDoc2CodeTara.Visible:=true;           //���� ���
          fmAddDoc2.ViewDoc2NameTara.Visible:=true;
          fmAddDoc2.ViewDoc2CenaTara.Visible:=true;
          fmAddDoc2.ViewDoc2SumCenaTara.Visible:=true;
          fmAddDoc2.ViewDoc2VesTara.Visible:=true;      //���� ���

          fmAddDoc2.ViewDoc2CodeTovar.Visible:=True;           //����� ���
          fmAddDoc2.ViewDoc2Name.Visible:=True;
          fmAddDoc2.ViewDoc2CodeEdIzm.Visible:=True;
          fmAddDoc2.ViewDoc2BarCode.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTovar.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTovarSpis.Visible:=True;
          fmAddDoc2.ViewDoc2SumCenaTovar.Visible:=True;
          fmAddDoc2.ViewDoc2SumCenaTovarSpis.Visible:=True;
          fmAddDoc2.ViewDoc2NDSProc.Visible:=True;
          fmAddDoc2.ViewDoc2NDSSum.Visible:=True;
          fmAddDoc2.ViewDoc2OutNDSSum.Visible:=True;
          fmAddDoc2.ViewDoc2FullName.Visible:=True;
          fmAddDoc2.ViewDoc2PostDate.Visible:=True;
          fmAddDoc2.ViewDoc2PostNum.Visible:=True;
          fmAddDoc2.ViewDoc2PostQuant.Visible:=True;
          fmAddDoc2.ViewDoc2rNac.Visible:=True;
          fmAddDoc2.ViewDoc2CheckCM.Visible:=True;
          fmAddDoc2.ViewDoc2KolSpis.Visible:=True;
          fmAddDoc2.ViewDoc2CenaTaraSpis.Visible:=true;
          fmAddDoc2.ViewDoc2Remn.Visible:=True;           //
          fmAddDoc2.ViewDoc2VesTara.Visible:=True;        //
          fmAddDoc2.ViewDoc2Brak.Visible:=True;           //
          fmAddDoc2.ViewDoc2Kol.Visible:=true;            //
          fmAddDoc2.ViewDoc2KolF.Visible:=True;           //
          fmAddDoc2.ViewDoc2KolWithMest.Visible:=True;    //
          fmAddDoc2.ViewDoc2GTD.Visible:=True;            //

                   //����� ���
        end;
      end;


      fmAddDoc2.ViewDoc2.OptionsData.Editing:=False;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=False;

      fmAddDoc2.cxComboBox3.ItemIndex:=quTTnOutChekBuh.AsInteger;
    end;

    if CommonSet.Single=1 then
    begin
      fmAddDoc2.cxButton6.Enabled:=True;
      fmAddDoc2.cxButton7.Enabled:=True;
      fmAddDoc2.cxButton10.Enabled:=True;
      fmAddDoc2.cxButton6.Visible:=True;
      fmAddDoc2.cxButton7.Visible:=True;
      fmAddDoc2.cxButton10.Visible:=True;
      fmAddDoc2.cxComboBox1.Properties.ReadOnly:=False;
    end else
    begin
      fmAddDoc2.cxComboBox1.Properties.ReadOnly:=True;
      fmAddDoc2.cxButton6.Enabled:=False;
      fmAddDoc2.cxButton7.Enabled:=False;
      fmAddDoc2.cxButton10.Enabled:=False;
      fmAddDoc2.cxButton6.Visible:=False;
      fmAddDoc2.cxButton7.Visible:=False;
      fmAddDoc2.cxButton10.Visible:=False;
    end;

    bOpen:=False;
  end;
end;

procedure TfmDocs2.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs2.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs2.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOut.Align:=AlClient;
  ViewDocsOut.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsOut.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs2.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelDocsOut.Visible then
      begin
        fmDocs2.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs2.ViewDocsOut.BeginUpdate;
        try
          quTTNOut.Active:=False;
          quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTNOut.Active:=True;
        finally
          fmDocs2.ViewDocsOut.EndUpdate;
        end;

      end else
      begin
        fmDocs2.Caption:='��������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        ViewCards.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
        quDocsInCard.Active:=True;}
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocs2.acAddDoc2Execute(Sender: TObject);
begin
  //�������� ��������

  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
{
������� ���� ����������� (0)
����������� ������ �� ������ ������� ������ ��.���� (0)
������� ������ ����������� (1)
������� ������ ������������ ��"������"(2)
�������� � �� �� ������� � ������ ����������� (3)
������� � ��������  �� �� (4).
}

    iTypeDO:=1;

    if CommonSet.Single=0 then
    begin
      fmSetTypeDocOut.ShowModal;
      if fmSetTypeDocOut.ModalResult=mrOk then
      begin

      //  if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=0 then exit;// iTypeDO:=0;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=0 then iTypeDO:=99;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=1 then iTypeDO:=0;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=2 then iTypeDO:=1;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=3 then iTypeDO:=2;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=4 then iTypeDO:=3;
        if fmSetTypeDocOut.cxRadioGroup1.ItemIndex=5 then iTypeDO:=4;

      end else exit;
    end;

    prSetValsAddDoc2(0); //0-����������, 1-��������������, 2-��������

    CloseTa(fmAddDoc2.taSpecOut);
    if CommonSet.RC=1 then fmAddDoc2.cxComboBox1.ItemIndex:=3;
    fmAddDoc2.acSaveDoc.Enabled:=True;
    fmAddDoc2.Show;

    delay(500);
    showmessage('�������� ����������!');
    prPostPropButClick;
  end;
end;

procedure TfmDocs2.acEditDoc2Execute(Sender: TObject);
Var  IDH:INteger;
//    rSum1,rSum2:Real;
    iC:INteger;
    StrWk:String;
    user:String;          //p
    edit99:boolean;       //p
    sCCode,sCountry:String;
begin
  //�������������
  user:=Person.Name;
 // edit99:=false;                    //���� ��� �-��=99 � user �� ���� �������������,�� ��������� ������������� � �������� �������
  iTypeDO:=dmMC.quTTnOutProvodType.AsInteger;
  if (iTypeDO=99) and ((user='OPER CB') or (user='BUH') or (user='�����') or (user='OPERZAK')) then edit99:=True
  else edit99:=False;

  if not CanDo('prEditDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMt do
  begin
    if (quTTNOut.RecordCount>0) then //���� ��� �������������
    begin
      if (quTTNOutAcStatus.AsInteger=0) or (edit99=true) then
      begin
        if (quTTNOutDateInvoice.AsDateTime<=prOpenDate) and (edit99=false) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if (fTestInv(quTTNOutDepart.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime))=False) and (edit99=false) then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        prSetValsAddDoc2(1); //0-����������, 1-��������������, 2-��������

        CloseTa(fmAddDoc2.taSpecOut);

        fmAddDoc2.acSaveDoc.Enabled:=True;

        if (iTypeDO>2) and (iTypeDO<>99) then //��������������� ����� ���
        begin
          IDH:=quTTNOutID.AsInteger;
          if ptGTD.Active=False then ptGTD.Active:=True else ptGTD.Refresh;

          ptGTD.CancelRange;
          ptGTD.SetRange([IDH],[IDH]);
        end;

        quSpecOut.Active:=False;
        quSpecOut.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
        quSpecOut.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
        quSpecOut.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
        quSpecOut.Active:=True;

        quSpecOut.First;
        iC:=1;

        fmAddDoc2.ViewDoc2.BeginUpdate;
        while not quSpecOut.Eof do
        begin
          with fmAddDoc2 do
          begin
            sCCode:='';
            sCountry:='';

            if quSpecOutCodeTovar.AsInteger>0 then prFindTCountry(quSpecOutCodeTovar.AsInteger,sCCode,sCountry);

            taSpecOut.Append;
            taSpecOutNum.AsInteger:=iC;
            taSpecOutCodeTovar.AsInteger:=quSpecOutCodeTovar.AsInteger;
            taSpecOutCodeEdIzm.AsInteger:=quSpecOutCodeEdIzm.AsInteger;
            taSpecOutBarCode.AsString:=quSpecOutBarCode.AsString;
            taSpecOutTovarType.AsInteger:=quSpecOutTovarType.AsInteger;
            taSpecOutNDSProc.AsFloat:=quSpecOutNDSProc.AsFloat;
            taSpecOutNDSSum.AsFloat:=rv(quSpecOutNDSSum.AsFloat);
            taSpecOutOutNDSSum.AsFloat:=rv(quSpecOutOutNDSSum.AsFloat);
            taSpecOutKolMest.AsInteger:=quSpecOutKolMest.AsInteger;
            taSpecOutKolEdMest.AsFloat:=0;
            taSpecOutKolWithMest.AsFloat:=quSpecOutKolWithMest.AsFloat+quSpecOutKolEdMest.AsFloat;
            taSpecOutKol.AsFloat:=quSpecOutKol.AsFloat;
            taSpecOutCenaTovar.AsFloat:=quSpecOutCenaTovar.AsFloat;
            taSpecOutCenaTovarSpis.AsFloat:=quSpecOutCenaTovarSpis.AsFloat;
            taSpecOutCenaPost.AsFloat:=quSpecOutCenaPost.AsFloat;
            taSpecOutSumCenaTovarSpis.AsFloat:=rv(quSpecOutSumCenaTovarSpis.AsFloat);
            taSpecOutSumCenaTovar.AsFloat:=rv(quSpecOutSumCenaTovar.AsFloat);
            taSpecOutName.AsString:=quSpecOutName.AsString;
            taSpecOutCodeTara.AsInteger:=quSpecOutCodeTara.AsInteger;
            StrWk:='';
            if quSpecOutCodeTara.AsInteger>0 then fTara(quSpecOutCodeTara.AsInteger,StrWk);
            taSpecOutNameTara.AsString:=StrWk;
            taSpecOutVesTara.AsFloat:=quSpecOutVesTara.AsFloat;
            taSpecOutCenaTara.AsFloat:=quSpecOutCenaTara.AsFloat;
            taSpecOutCenaTaraSpis.AsFloat:=quSpecOutCenaTaraSpis.AsFloat;
            taSpecOutSumVesTara.AsFloat:=quSpecOutSumVesTara.AsFloat;
            taSpecOutSumCenaTara.AsFloat:=rv(quSpecOutSumCenaTara.AsFloat);
            taSpecOutSumCenaTaraSpis.AsFloat:=rv(quSpecOutSumCenaTaraSpis.AsFloat);
            taSpecOutFullName.AsString:=quSpecOutFullName.AsString;
            taSpecOutPostDate.AsDateTime:=quSpecOutCodePodgr.AsInteger+40000;
            taSpecOutPostNum.AsString:=its(Trunc(quSpecOutCenaPost.AsFloat));
            taSpecOutPostQuant.AsFloat:=quSpecOutAkciz.AsFloat;

            taSpecOutBrak.AsFloat:=quSpecOutOKDP.AsFloat;
            if quSpecOutChekBuh.AsBoolean=True then taSpecOutCheckCM.AsInteger:=1 else taSpecOutCheckCM.AsInteger:=0;
            taSpecOutKolF.AsFloat:=quSpecOutSumVesTara.AsFloat;

//            taSpecOutKolSpis.AsFloat:=quSpecOutSumVesTara.AsFloat-quSpecOutKol.AsFloat;

            if quSpecOutChekBuh.AsBoolean=True then
              taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
            else
              taSpecOutKolSpis.AsFloat:=0;

            taSpecOutGTD.AsString:=''; //� ���
            if (iTypeDO>2) and (iTypeDO<>99) then //��������������� ����� ���
            begin
              if (trim(sCCode)<>'643')  //��� ���� ����� ��� �� ������ ����
              and (trim(sCCode)<>'398')
              and (trim(sCCode)<>'112') then
              begin
                ptGTD.First;
                while not ptGTD.Eof do
                begin
                  if (ptGTDCODE.AsInteger=quSpecOutCodeTovar.AsInteger)and(ptGTDID.AsInteger=RoundEx(quSpecOutSumCenaTaraSpis.AsFloat)) then
                  begin
                    taSpecOutGTD.AsString:=OemToAnsiConvert(ptGTDGTD.AsString);
                    Break;
                  end;
                  ptGTD.Next;
                end;
              end;
            end;

            taSpecOutCCode.AsString:=sCCode;
            taSpecOutCountry.AsString:=sCountry;

            taSpecOut.Post;
          end;
          quSpecOut.Next; inc(iC);
        end;
        fmAddDoc2.ViewDoc2.EndUpdate;

        fmAddDoc2.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs2.acViewDoc2Execute(Sender: TObject);
Var iC:INteger;
    StrWk:String;
    IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMt do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� ��������
    begin
      prSetValsAddDoc2(2); //0-����������, 1-��������������, 2-��������

      CloseTa(fmAddDoc2.taSpecOut);

      fmAddDoc2.acSaveDoc.Enabled:=false;            //p  ���� true

      if (quTTnOutProvodType.AsInteger>2) and ((quTTnOutProvodType.AsInteger<>99))then //��������������� ����� ���
      begin
        IDH:=quTTNOutID.AsInteger;
        if ptGTD.Active=False then ptGTD.Active:=True else ptGTD.Refresh;

        ptGTD.CancelRange;
        ptGTD.SetRange([IDH],[IDH]);
      end;

      quSpecOut.Active:=False;
      quSpecOut.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
      quSpecOut.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
      quSpecOut.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
      quSpecOut.Active:=True;

      quSpecOut.First;
      iC:=1;
      fmAddDoc2.ViewDoc2.BeginUpdate;
      while not quSpecOut.Eof do
      begin
        with fmAddDoc2 do
        begin
          taSpecOut.Append;
          taSpecOutNum.AsInteger:=iC;
          taSpecOutCodeTovar.AsInteger:=quSpecOutCodeTovar.AsInteger;
          taSpecOutCodeEdIzm.AsInteger:=quSpecOutCodeEdIzm.AsInteger;
          taSpecOutBarCode.AsString:=quSpecOutBarCode.AsString;
          taSpecOutTovarType.AsInteger:=quSpecOutTovarType.AsInteger;
          taSpecOutNDSProc.AsFloat:=quSpecOutNDSProc.AsFloat;
          taSpecOutNDSSum.AsFloat:=rv(quSpecOutNDSSum.AsFloat);
          taSpecOutOutNDSSum.AsFloat:=rv(quSpecOutOutNDSSum.AsFloat);
          taSpecOutKolMest.AsInteger:=quSpecOutKolMest.AsInteger;
          taSpecOutKolEdMest.AsFloat:=0;
          taSpecOutKolWithMest.AsFloat:=quSpecOutKolWithMest.AsFloat+quSpecOutKolEdMest.AsFloat;
          taSpecOutKol.AsFloat:=quSpecOutKol.AsFloat;
          taSpecOutCenaTovar.AsFloat:=quSpecOutCenaTovar.AsFloat;
          taSpecOutCenaTovarSpis.AsFloat:=quSpecOutCenaTovarSpis.AsFloat;
          taSpecOutCenaPost.AsFloat:=quSpecOutCenaPost.AsFloat;
          taSpecOutSumCenaTovarSpis.AsFloat:=rv(quSpecOutSumCenaTovarSpis.AsFloat);
          taSpecOutSumCenaTovar.AsFloat:=rv(quSpecOutSumCenaTovar.AsFloat);
          taSpecOutName.AsString:=quSpecOutName.AsString;
          taSpecOutCodeTara.AsInteger:=quSpecOutCodeTara.AsInteger;
          StrWk:='';
          if quSpecOutCodeTara.AsInteger>0 then fTara(quSpecOutCodeTara.AsInteger,StrWk);
          taSpecOutNameTara.AsString:=StrWk;
          taSpecOutVesTara.AsFloat:=quSpecOutVesTara.AsFloat;
          taSpecOutCenaTara.AsFloat:=quSpecOutCenaTara.AsFloat;
          taSpecOutCenaTaraSpis.AsFloat:=quSpecOutCenaTaraSpis.AsFloat;
          taSpecOutSumVesTara.AsFloat:=quSpecOutSumVesTara.AsFloat;
          taSpecOutSumCenaTara.AsFloat:=rv(quSpecOutSumCenaTara.AsFloat);
          taSpecOutSumCenaTaraSpis.AsFloat:=rv(quSpecOutSumCenaTaraSpis.AsFloat);
          taSpecOutFullName.AsString:=quSpecOutFullName.AsString;
          taSpecOutPostDate.AsDateTime:=quSpecOutCodePodgr.AsInteger+40000;
          taSpecOutPostNum.AsString:=its(Trunc(quSpecOutCenaPost.AsFloat));
          taSpecOutPostQuant.AsFloat:=quSpecOutAkciz.AsFloat;
          taSpecOutBrak.AsFloat:=quSpecOutOKDP.AsFloat;
          if quSpecOutChekBuh.AsBoolean=True then taSpecOutCheckCM.AsInteger:=1 else taSpecOutCheckCM.AsInteger:=0;
          taSpecOutKolF.AsFloat:=quSpecOutSumVesTara.AsFloat;

          if quSpecOutChekBuh.AsBoolean=True then
             taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
          else
             taSpecOutKolSpis.AsFloat:=0;

          taSpecOutKolSpis.AsFloat:=quSpecOutSumVesTara.AsFloat-quSpecOutKol.AsFloat;

          taSpecOutGTD.AsString:=''; //� ���
          if (quTTnOutProvodType.AsInteger>2) and (quTTnOutProvodType.AsInteger<>99)  then //��������������� ����� ���
          begin
            ptGTD.First;
            while not ptGTD.Eof do
            begin
              if (ptGTDCODE.AsInteger=quSpecOutCodeTovar.AsInteger)and(ptGTDID.AsInteger=RoundEx(quSpecOutSumCenaTaraSpis.AsFloat)) then
              begin
                taSpecOutGTD.AsString:=OemToAnsiConvert(ptGTDGTD.AsString);
                Break;
              end;
              ptGTD.Next;
            end;
          end;

          taSpecOut.Post;
        end;
        quSpecOut.Next; inc(iC);
      end;
      fmAddDoc2.ViewDoc2.EndUpdate;
      if fmAddDoc2.Showing=false then fmAddDoc2.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs2.ViewDocsOutDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if (iDirect=26) and (quTTNOutAcStatus.AsInteger=3) then
    begin
      prClearFromInv(quTTnOutDepart.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutNumber.AsString);
      exit;
    end;

    if quTTNOutAcStatus.AsInteger=0 then acEditDoc2.Execute //��������������
    else acViewDoc2.Execute; //��������}
  end;
end;

procedure TfmDocs2.acDelDoc2Execute(Sender: TObject);
begin
  if not CanDo('prDelDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnOut.RecordCount>0 then //���� ��� �������
    begin
      if quTTnOutAcStatus.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTnOutNumber.AsString+' �� '+quTTnOutNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prAddHist(2,quTTnOutID.AsInteger,trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNOut"');
          quD.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnOutNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNOutLn"');
          quD.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnOutNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          prRefrID(quTTnOut,ViewDocsOut,0);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs2.acOnDoc2Execute(Sender: TObject);
Var bOpr:Boolean;
//    i:Integer;
 //   rQRemn:Real;
    Q,Qpredz:real;        //p
    mindocdate,maxdocdate,pzak,secpost,secpost2:integer;   //p
    rQSpis:Real;
    user:String;          //p
    oprih,edit99:boolean;       //p

begin
  user:=Person.Name;
 // edit99:=false;
  if fmAddDoc2.Showing=false then
    iTypeDO:=dmMC.quTTnOutProvodType.AsInteger;      //���� ��� �-��=99 � user �� ���� �������������,�� ��������� ������������ � �������� �������

  if (iTypeDO=99) and ((user='OPER CB') or (user='BUH') or (user='�����') or (user='OPERZAK')) then edit99:=True
  else edit99:=False;

//������������
  with dmP do
  with dmMC do
  begin
    if not CanDo('prOnDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTNOutDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quTTNOut.RecordCount>0 then //���� ��� ������������
    begin
      if (quTTNOutAcStatus.AsInteger=0) or (edit99=true) then
      begin
        if (quTTnOutDateInvoice.AsDateTime<=prOpenDate) and (edit99=False) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if (fTestInv(quTTNOutDepart.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime))=False) and (edit99=False) then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

//        if CommonSet.Single=0 then
//          if (quTTnOutNacenkaTovar.AsFloat<0)and(CommonSet.RC=0)  then begin StatusBar1.Panels[0].Text:='����� ������� �� ������ ���� �������������. ��������� �������!'; ShowMessage('����� ������� �� ������ ���� �������������. ��������� �������!'); exit; end;
        secpost:=0;

        if (iTypeDO<>99) then         //���� �������� �� ������� ����
        begin
         //p ��������� � ������� ������� ��� �� �������� ����������� �����
          if PtHist.Active=false then PtHist.Active:=true;
          PtHist.CancelRange;                    // ��������� ������
          PtHist.SetRangeStart;
          PtHistTYPED.AsInteger:=2;
          PtHistDOCDATE.AsDateTime:=trunc(quTTnOutDateInvoice.AsDateTime);
          PtHistDOCNUM.AsString:=quTTnOutNumber.asstring;
          PtHist.SetRangeEnd;
          PtHistTYPED.AsInteger:=2;
          PtHistDOCDATE.AsDateTime:=trunc(quTTnOutDateInvoice.AsDateTime);
          PtHistDOCNUM.AsString:=quTTnOutNumber.asstring;
          PtHist.ApplyRange;

          PtHist.First;
          while not PtHist.Eof do
          begin
            if PtHistIDOP.AsInteger>0 then secpost:=secpost+1;
            PtHist.Next;
          end;
          PtHist.Active:=false;
          //p
        end;

        secpost2:=secpost+1;

        if MessageDlg('������������ �������� �'+quTTnOutNumber.AsString+' �� '+quTTnOutNameCli.AsString+'?' ,mtConfirmation, [mbYes, mbNo],0) = mrYES then oprih:=true
        else oprih:=false;

        if oprih=true then
        begin
          prButtonSet(False);

          prAddHist(2,quTTnOutID.AsInteger,trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutNumber.AsString,'�����.',secpost2);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1); Delay(10);

          quSpecOut1.Active:=False;
          quSpecOut1.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
          quSpecOut1.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
          quSpecOut1.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
          quSpecOut1.Active:=True;

          bOpr:=True;

          if bOpr then
          begin
            quSpecOut2.Active:=False;
            quSpecOut2.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
            quSpecOut2.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
            quSpecOut2.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
            quSpecOut2.Active:=True;

            quSpecOut2.First;  //��������� ����� ���  � ��������� ��������������
            while not quSpecOut2.Eof do
            begin

              prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut2Kol.AsFloat*-1));
              prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut2Kol.AsFloat*-1));

              rQSpis:=abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat);
              prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));

              if quSpecOut2ChekBuh.AsBoolean=True then
              begin
                if abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat)>0.001 then
                begin
                  prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
                end;
              end;

              quSpecOut2.Next;
            end;

            quSpecOut2.Active:=False;
            quSpecOut1.Active:=False;

            if quTTnOutSummaTara.AsFloat<>0 then
            begin //�������� �� ����
              quSpecOutT.Active:=False;
              quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
              quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
              quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
              quSpecOutT.Active:=True;

              quSpecOutT.First;  // ��������������
              while not quSpecOutT.Eof do
              begin
// �� ������              prDelTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1));
                prAddTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1),quSpecOutTCenaTara.AsFloat);

                quSpecOutT.Next;
              end;
              quSpecOutT.Active:=False;
            end;


            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "TTNOut" Set AcStatus=3');
            quE.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
            quE.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
            quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
            quE.ExecSQL;

            prRefrID(quTTNOut,ViewDocsOut,0);

           //�������� ��
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnOutDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnOutDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

            prWh('������ ��.',Memo1); Delay(10);

            if fmAddDoc2.Showing=true then
            begin
              ViewDocsOut.BeginUpdate;
              quTTnOut.Active:=False;
              quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
              quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
              quTTnOut.Active:=True;
              quTTnOut.Locate('ID',fmAddDoc2.cxTextEdit1.Tag,[]);
              ViewDocsOut.EndUpdate;
            end;


          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          prButtonSet(True);
        end
        else
        begin
          if fmAddDoc2.Showing=true then
          begin
            ViewDocsOut.BeginUpdate;
            quTTnOut.Active:=False;
            quTTNOut.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
            quTTNOut.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
            quTTnOut.Active:=True;
            quTTnOut.Locate('ID',fmAddDoc2.cxTextEdit1.Tag,[]);
            ViewDocsOut.EndUpdate;
          end;

        end;
      end;
    end;
  end;
end;

procedure TfmDocs2.acOffDoc2Execute(Sender: TObject);
Var bOpr:Boolean;
    rQSpis:Real;
    user:String;          //p
    edit99:boolean;       //p
begin
  user:=Person.Name;
 // edit99:=false;                    //���� ��� �-��=99 � user �� ���� �������������,�� ��������� ������������ � �������� �������
  iTypeDO:=dmMC.quTTnOutProvodType.AsInteger;
  if (iTypeDO=99) and ((user='OPER CB') or (user='BUH') or (user='�����') or (user='OPERZAK')) then edit99:=True
  else edit99:=False;

//��������
  with dmMC do
  begin
    if not CanDo('prOffDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTNOutDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quTTNOut.RecordCount>0 then //���� ���
    begin
      if quTTNOutAcStatus.AsInteger=3 then
      begin
        if (quTTnOutDateInvoice.AsDateTime<=prOpenDate) and (edit99=False) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if (fTestInv(quTTNOutDepart.AsInteger,Trunc(quTTNOutDateInvoice.AsDateTime))=False) and (edit99=False)  then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('�������� �������� �'+quTTnOutNumber.AsString+' �� '+quTTnOutNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         //��� ���������
          prButtonSet(False);
          prAddHist(2,quTTnOutID.AsInteger,trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutNumber.AsString,'�����.',0);
          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1); Delay(10);

          quSpecOut1.Active:=False;
          quSpecOut1.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
          quSpecOut1.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
          quSpecOut1.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
          quSpecOut1.Active:=True;

          bOpr:=True;
          if NeedPost(trunc(quTTnOutDateInvoice.AsDateTime)) then
          begin
            iNumPost:=PostNum(quTTnOutID.AsInteger)*100+2;
            prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
            try
              assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
              rewrite(fPost);

              writeln(fPost,'TTNOUT;OF;'+its(quTTnOutDepart.AsInteger)+r+its(quTTnOutID.AsInteger)+r+ds(quTTnOutDateInvoice.AsDateTime)+r+quTTnOutNumber.AsString+r+quTTnOutINN.AsString+r+fs(quTTnOutSummaTovarSpis.AsFloat)+r+fs(quTTnOutSummaTovar.AsFloat)+r+fs(quTTnOutSummaTara.AsFloat)+r+fs(quTTnOutNDSTovar.AsFloat)+r);

              quSpecOut1.First;
              while not quSpecOut1.Eof do
              begin
                StrPost:='TTNOUTLN;OF;'+its(quTTnOutDepart.AsInteger)+r+its(quTTnOutID.AsInteger);
                StrPost:=StrPost+r+its(quSpecOut1CodeTovar.asINteger)+r+its(quSpecOut1CodeTara.asINteger)+r+fs(quSpecOut1NDSProc.asfloat)+r+fs(quSpecOut1Kol.asfloat)+r+fs(quSpecOut1KolMest.asfloat)+r+fs(quSpecOut1SumCenaTovarSpis.asfloat)+r+fs(quSpecOut1SumCenaTovar.asfloat)+r+fs(quSpecOut1SumCenaTara.asfloat)+r+fs(quSpecOut1SumCenaTaraSpis.asfloat)+r+fs(quSpecOut1NDSSum.asfloat)+r;
                writeln(fPost,StrPost);
                quSpecOut1.Next;
              end;
            finally
              closefile(fPost);
            end;

            //������ ����� - ���� ��������
            bOpr:=prTr(sNumPost(iNumPost),Memo1);
          end;

          if bOpr then
          begin

            quSpecOut1.First;  //��������� ����� ���  � ��������� ��������������
            while not quSpecOut1.Eof do
            begin

              prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut1Kol.AsFloat*-1));

              if abs(quSpecOut1SumVesTara.AsFloat-quSpecOut1Kol.AsFloat)>0.001 then
              begin
                rQSpis:=abs(quSpecOut1SumVesTara.AsFloat-quSpecOut1Kol.AsFloat);
                prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
              end;

              quSpecOut1.Next;
            end;
            quSpecOut1.Active:=False;

            if quTTnOutSummaTara.AsFloat<>0 then
            begin //�������� �� ����
              quSpecOutT.Active:=False;
              quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
              quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
              quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
              quSpecOutT.Active:=True;

              quSpecOutT.First;  // ��������������
              while not quSpecOutT.Eof do
              begin
                prDelTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1));

                quSpecOutT.Next;
              end;
              quSpecOutT.Active:=False;
            end;

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "TTNOut" Set AcStatus=0');
            quE.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
            quE.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
            quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
            quE.ExecSQL;

            prRefrID(quTTNOut,ViewDocsOut,0);

           //�������� ��
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnOutDateInvoice.AsDateTime))>=1 then prRecalcTO(trunc(quTTnOutDateInvoice.AsDateTime),trunc(Date)-1,nil,1,0);

            prWH('������ ��.',Memo1); Delay(10);
          end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);

          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs2.Timer1Timer(Sender: TObject);
begin
  if bClearDocOut=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOut:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOut:=True;
end;

procedure TfmDocs2.acVidExecute(Sender: TObject);
begin
  //���
  with dmMC do
  begin
    {if LevelDocsOut.Visible then
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs2.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs2.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsOut.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs2.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs2.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsOut.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsOut.BeginUpdate;
      quTTNOut.Active:=False;
      quTTNOut.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quTTNOut.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quTTNOut.Active:=True;
      ViewDocsOut.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocs2.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs2.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsOut.Visible=False then exit;
  with dmMC do
  begin
    if quTTNOut.RecordCount>0 then //���� ��� �������������
    begin
      quSpecOut.Active:=False;
      quSpecOut.ParamByName('IDHD').AsInteger:=quTTNOutID.AsInteger;
      quSpecOut.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocOutReestr.frf');

      frVariables.Variable['CliName']:=quTTNOutNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTNOutNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTNOutDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTNOutNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecOut.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs2.acCopyExecute(Sender: TObject);
var Par:Variant;
    iC:INteger;
begin
  //����������
  with dmMT do
  with dmMC do
  begin
    if (quTTnOutProvodType.AsInteger=1) or (quTTnOutProvodType.AsInteger=99) then exit;
    if ViewDocsOut.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=2;
    par[1]:=quTTnOutID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=2;
      taHeadDocId.AsInteger:=quTTnOutID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnOutDateInvoice.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnOutNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTnOutCodePoluch.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnOutNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnOutDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnOutNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnOutSummaTovar.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnOutSummaTovarSpis.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTnOutIndexPoluch.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTnOutSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecOut.Active:=False;
      quSpecOut.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
      quSpecOut.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
      quSpecOut.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
      quSpecOut.Active:=True;

      quSpecOut.First;
      iC:=1;
      while not quSpecOut.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=2;
        taSpecDocIdHead.AsInteger:=quTTnOutID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecOutCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecOutKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecOutKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecOutCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecOutSumCenaTovar.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecOutCenaTovarSpis.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecOutSumCenaTovarSpis.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecOutNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecOutNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecOutName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecOutCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=0;
        taSpecDocIdTara.AsInteger:=quSpecOutCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecOutKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecOutCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecOutSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecOutBarCode.AsString;
        taSpecDoc.Post;

        quSpecOut.Next;   inc(iC);
      end;
      quSpecOut.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocs2.acInsertDExecute(Sender: TObject);
Var StrWk:String;
begin
  // ��������
  with dmMC do
  with dmMT do
  begin
    if (quTTnOutProvodType.AsInteger=1) or (quTTnOutProvodType.AsInteger=99) then exit;
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocOut') then
        begin
          prSetValsAddDoc2(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmAddDoc2.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmAddDoc2.Label4.Tag:=taHeadDociTypeCli.AsInteger;
          fmAddDoc2.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc2.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

          quDeparts.Active:=False; quDeparts.Active:=True;

          fmAddDoc2.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc2.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

         { fmAddDoc2.cxLabel1.Enabled:=True;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;
          fmAddDoc2.cxLabel11.Enabled:=True;    //p
          fmAddDoc2.cxButton1.Enabled:=True;

          fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;  }
          fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc2.taSpecOut);

          fmAddDoc2.ViewDoc2.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc2 do
              begin
                taSpecOut.Append;
                taSpecOutNum.AsInteger:=taSpecDocNum.AsInteger;

                taSpecOutCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecOutCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecOutBarCode.AsString:=taSpecDocBarCode.AsString;
                taSpecOutNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                taSpecOutNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecOutOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                taSpecOutKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                taSpecOutKolEdMest.AsFloat:=0;
                taSpecOutKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecOutKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                taSpecOutCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecOutSumCenaTovar.AsFloat:=taSpecDocSumIn.AsFloat;

                taSpecOutCenaTovarSpis.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecOutSumCenaTovarSpis.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecOutName.AsString:=taSpecDocNameC.AsString;
                taSpecOutCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                StrWk:='';
                if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                taSpecOutNameTara.AsString:=StrWk;

                taSpecOutVesTara.AsFloat:=0;
                taSpecOutCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                taSpecOutSumVesTara.AsFloat:=0;
                taSpecOutSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
                taSpecOutTovarType.AsInteger:=0;

                taSpecOutCenaTaraSpis.AsFloat:=taSpecDocPriceUch.AsFloat;  //����� - ��� ����� ���� ������� ��� ��� ���� ���

                quCId.Active:=False;
                quCId.ParamByName('IDC').AsInteger:=taSpecDocIdCard.AsInteger;
                quCId.Active:=True;
                if quCId.RecordCount>0 then taSpecOutFullName.AsString:=quCIdFullName.AsString;
                quCId.Active:=False;

                taSpecOutBrak.AsFloat:=0;
                taSpecOutCheckCM.AsInteger:=0;
                taSpecOutKolF.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecOutKolSpis.AsFloat:=0;
                taSpecOut.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmAddDoc2.ViewDoc2.EndUpdate;

          fmAddDoc2.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmAddDoc2.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocs2.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsOut.Visible then
  begin
    prNExportExel5(ViewDocsOut);
  end else
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocs2.FormShow(Sender: TObject);
  var user:string;
begin

  user:=Person.Name;

  if (user<>'BUH') and (user<>'�����') then N12.Visible:=false
  else N12.Visible:=true;
  Memo1.Clear;
 
end;

procedure TfmDocs2.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsOut);
end;

procedure TfmDocs2.acAdd1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc2(0); //0-����������, 1-��������������, 2-��������

    if FileExists(CurDir+'SpecOut.cds') then fmAddDoc2.taSpecOut.Active:=True
    else fmAddDoc2.taSpecOut.CreateDataSet;

    fmAddDoc2.acSaveDoc.Enabled:=True;
    fmAddDoc2.Show;
  end;
end;

procedure TfmDocs2.ViewDocsOutCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsOut.ColumnCount-1 do
  begin
    if ViewDocsOut.Columns[i].Name='ViewDocsOutAcStatus' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;
end;

procedure TfmDocs2.acChangeTExecute(Sender: TObject);
begin
  //�������� ��� ���������
  with dmMC do
  begin
    if quTTnOut.RecordCount>0 then
    begin
      fmChangeT.Label1.Caption:='�������� � '+quTTnOutNumber.AsString+' �� '+DateToStr(quTTnOutDateInvoice.AsDateTime)+' ������ ��� '+its(quTTnOutProvodType.AsInteger);
      if quTTnOutProvodType.AsInteger=0 then fmChangeT.cxSpinEdit1.Value:=1 else fmChangeT.cxSpinEdit1.Value:=0;
      fmChangeT.ShowModal;
      if fmChangeT.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNOut" Set ProvodType='+its(fmChangeT.cxSpinEdit1.Value));
        quE.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNOut,ViewDocsOut,0);

        showmessage('��� �������.');
      end;
    end else
      showmessage('�������� ��������');
  end;
end;

procedure TfmDocs2.acChangeOtprExecute(Sender: TObject);
begin
  //�������� �����������
  with dmMC do
  begin
    if quTTnOut.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quTTnOutNumber.AsString+' �� '+DateToStr(quTTnOutDateInvoice.AsDateTime)+' ������ ��� '+its(quTTnOutChekBuh.AsInteger);
      if quTTnOutChekBuh.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNOut" Set ChekBuh='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quTTnOutDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTnOutDateInvoice.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNOutNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNOut,ViewDocsOut,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');
  end;
end;

procedure TfmDocs2.acRecalcMoveSelExecute(Sender: TObject);
Var rQSpis:Real;
begin
  //�������� �������������� �� ���� ���������
  with dmMC do
  with dmMT do
  begin
    if MessageDlg('����������� �������� �� ����� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('������.'); delay(10);
      quTTnOut.First;
      while not quTTnOut.Eof do
      begin
        if (quTTNOutAcStatus.AsInteger=3)and(quTTNOutSummaTovar.AsFloat>0)  then
        begin
          Memo1.Lines.Add('    ����. '+quTTnOutNumber.AsString+' '+quTTnOutDateInvoice.AsString); delay(10);

          quSpecOut1.Active:=False;
          quSpecOut1.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
          quSpecOut1.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
          quSpecOut1.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
          quSpecOut1.Active:=True;

          quSpecOut1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecOut1.Eof do
          begin
            prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut1Kol.AsFloat*-1));
            delay(20);
            prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut1Kol.AsFloat*-1));

            rQSpis:=abs(quSpecOut1SumVesTara.AsFloat-quSpecOut1Kol.AsFloat);
            prDelTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
            delay(20);
            if quSpecOut1ChekBuh.AsBoolean=True then
            begin
              if abs(quSpecOut1SumVesTara.AsFloat-quSpecOut1Kol.AsFloat)>0.001 then
              begin
                prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut1CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
              end;
            end;
            quSpecOut1.Next;
          end;
          quSpecOut1.Active:=False;
        end;
        quTTnOut.Next;
      end;
      Memo1.Lines.Add('������� ��������.'); delay(10);
    end;
  end;
end;

procedure TfmDocs2.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMC do
  with dmMT do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsOut.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=2;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;
  end;
end;

procedure TfmDocs2.acSetGTDFromInExecute(Sender: TObject);
Var i,j,iDep:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH,iPos,iDType:INteger;
    bNeedGTD:Boolean;
    sGTD:String;
begin
  //��������� ��� �� �������� ViewDocsOut
  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('������.');
        ViewDocsOut.BeginUpdate;

        if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
        ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

        if ptGTD.Active=False then ptGTD.Active:=True else ptGTD.Refresh;

        if ptTTnInLn.Active=False then ptTTnInLn.Active:=True else ptTTnInLn.Refresh;
        ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

        for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsOut.Controller.SelectedRecords[i];
          iDep:=0;
          sNum:='';
          dDateDoc:=Date;
          IDH:=0;
          iDType:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutProvodType' then begin iDType:=Rec.Values[j]; end;
          end;

          if (iDep>0)and(sNum>'')and(IDH>0)and(iDType>2) and(iDType<>99) then
          begin
            Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));
            ptGTD.CancelRange;
            ptGTD.SetRange([IDH],[IDH]);

            //������� �� ������������ �������
            ptOutLn.CancelRange;
            ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNum)],[iDep,dDateDoc,AnsiToOemConvert(sNum)]);
            ptOutLn.First;
            while not ptOutLn.Eof do
            begin //����� �� ������������ ��� ���������� ���
              bNeedGTD:=True;
              iPos:=RoundEx(ptOutLnSumCenaTaraSpis.AsFloat);
              if iPos>0 then
              begin
                if ptGTD.FindKey([IDH,iPos]) then
                begin

//                  bNeedGTD:=False;
                  if ptGTDGTD.AsString>='0' then bNeedGTD:=False; //��� ��� ���������� ������ ������ �� �����
                end;
              end;

              if bNeedGTD then //���� ��� � �������� ����� �������
              begin
                sGTD:='';
                ptTTnInLn.CancelRange;
                ptTTnInLn.SetRange([ptOutLnCodeTovar.AsInteger,CommonSet.DateBeg-60],[ptOutLnCodeTovar.AsInteger,Date]);
                ptTTnInLn.Last;   //������� � �����
                while not ptTTnInLn.Bof do
                begin
                  sGTD:=OemToAnsiConvert(Trim(ptTTnInLnSertNumber.AsString));
                  if sGTD>='0' then
                  begin
//                    sGTD:=OemToAnsiConvert(ptTTnInLnSertNumber.AsString);
                    Break;
                  end;
                  ptTTnInLn.Prior;
                end;

                try
                  if (sGTD>'')and(iPos>0) then
                  begin //���� ����������
                    if ptGTD.FindKey([IDH,iPos]) then
                    begin
                      Memo1.Lines.Add('   ���. ��� - '+its(ptOutLnCodeTovar.AsInteger)+' IDH '+its(IDH)+' Pos '+its(iPos)+'   '+sGTD);
                      ptGTD.Edit;
                      ptGTDCODE.AsInteger:=ptOutLnCodeTovar.AsInteger;
                      ptGTDGTD.AsString:=AnsiToOemConvert(sGTD);
                      ptGTD.Post;
                    end else
                    begin
                      Memo1.Lines.Add('   ���. ��� - '+its(ptOutLnCodeTovar.AsInteger)+' IDH '+its(IDH)+' Pos '+its(iPos)+'   '+sGTD);
                      ptGTD.Append;
                      ptGTDIDH.AsInteger:=IDH;
                      ptGTDID.AsInteger:=iPos;
                      ptGTDCODE.AsInteger:=ptOutLnCodeTovar.AsInteger;
                      ptGTDGTD.AsString:=AnsiToOemConvert(sGTD);
                      ptGTD.Post;
                    end;
                  end;
                except
                end;
              end;

              ptOutLn.Next; delay(10);
            end;
            delay(10);
          end;
        end;

        ViewDocsOut.EndUpdate;
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocs2.acRecalcNDSExecute(Sender: TObject);
Var i,j,iDep:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH,iDType:INteger;
begin
  //�����������  ���
  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('������.');
        ViewDocsOut.BeginUpdate;

        if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
        ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

        for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsOut.Controller.SelectedRecords[i];
          iDep:=0;
          sNum:='';
          dDateDoc:=Date;
          IDH:=0;
          iDType:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutProvodType' then begin iDType:=Rec.Values[j]; end;
          end;

          if (iDep>0)and(sNum>'')and(IDH>0)and(iDType=3) then
          begin
            Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));

            //������� �� ������������ �������
            ptOutLn.CancelRange;
            ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNum)],[iDep,dDateDoc,AnsiToOemConvert(sNum)]);
            ptOutLn.First;
            while not ptOutLn.Eof do
            begin //����� �� ������������ � ������� ���
              ptOutLn.Edit;
              ptOutLnOutNDSSum.AsFloat:=rv(ptOutLnSumCenaTovarSpis.AsFloat/(100+ptOutLnNDSProc.AsFloat)*100);
              ptOutLnNDSSum.AsFloat:=ptOutLnSumCenaTovarSpis.AsFloat-ptOutLnOutNDSSum.AsFloat;
              ptOutLn.Post;

              ptOutLn.Next; delay(10);
            end;
            delay(10);
          end;
        end;

        ViewDocsOut.EndUpdate;
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocs2.acSCHFExecute(Sender: TObject);
Var i,j,iDep:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH,iDType,iC:INteger;
    StrWk,sCCode,sCountry,StrWk1,StrWk2,StrWk3,SInnF,SInn,sMoll:String;
    sGTD:String;
begin
// ������ ������ ������
  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('������.');
        ViewDocsOut.BeginUpdate;
        fmAddDoc2.ViewDoc2.BeginUpdate;

       //���.����
        if ptMOL.Active=False then ptMOL.Active:=True;
        ptMol.Last;
        sMoll:=ptMOLOTVL.AsString;


        for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsOut.Controller.SelectedRecords[i];
          iDep:=0;
          sNum:='';
          dDateDoc:=Date;
          IDH:=0;
          iDType:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutProvodType' then begin iDType:=Rec.Values[j]; end;
          end;

          if (iDep>0) and (sNum>'')and (IDH>0)and (iDType>2) and (iDType<>99) then
          begin
            Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));
            if quTTNOut.Locate('ID',IDH,[]) then
            begin
              CloseTa(fmAddDoc2.taSpecOut);

              if (quTTnOutProvodType.AsInteger>2) and (quTTnOutProvodType.AsInteger<>99) then //��������������� ����� ���
              begin
                if ptGTD.Active=False then ptGTD.Active:=True else ptGTD.Refresh;

                ptGTD.CancelRange;
                ptGTD.SetRange([IDH],[IDH]);
              end;

              quSpecOut.Active:=False;
              quSpecOut.ParamByName('IDEP').AsInteger:=quTTNOutDepart.AsInteger;
              quSpecOut.ParamByName('SDATE').AsDate:=Trunc(quTTNOutDateInvoice.AsDateTime);
              quSpecOut.ParamByName('SNUM').AsString:=quTTNOutNumber.AsString;
              quSpecOut.Active:=True;

              quSpecOut.First;
              iC:=1;
              while not quSpecOut.Eof do
              begin
                with fmAddDoc2 do
                begin
                  sCCode:='';
                  sCountry:='';

                  if quSpecOutCodeTovar.AsInteger>0 then prFindTCountry(quSpecOutCodeTovar.AsInteger,sCCode,sCountry);

                  taSpecOut.Append;
                  taSpecOutNum.AsInteger:=iC;
                  taSpecOutCodeTovar.AsInteger:=quSpecOutCodeTovar.AsInteger;
                  taSpecOutCodeEdIzm.AsInteger:=quSpecOutCodeEdIzm.AsInteger;
                  taSpecOutBarCode.AsString:=quSpecOutBarCode.AsString;
                  taSpecOutTovarType.AsInteger:=quSpecOutTovarType.AsInteger;
                  taSpecOutNDSProc.AsFloat:=quSpecOutNDSProc.AsFloat;
                  taSpecOutNDSSum.AsFloat:=rv(quSpecOutNDSSum.AsFloat);
                  taSpecOutOutNDSSum.AsFloat:=rv(quSpecOutOutNDSSum.AsFloat);
                  taSpecOutKolMest.AsInteger:=quSpecOutKolMest.AsInteger;
                  taSpecOutKolEdMest.AsFloat:=0;
                  taSpecOutKolWithMest.AsFloat:=quSpecOutKolWithMest.AsFloat+quSpecOutKolEdMest.AsFloat;
                  taSpecOutKol.AsFloat:=quSpecOutKol.AsFloat;
                  taSpecOutCenaTovar.AsFloat:=quSpecOutCenaTovar.AsFloat;
                  taSpecOutCenaTovarSpis.AsFloat:=quSpecOutCenaTovarSpis.AsFloat;
                  taSpecOutCenaPost.AsFloat:=quSpecOutCenaPost.AsFloat;
                  taSpecOutSumCenaTovarSpis.AsFloat:=rv(quSpecOutSumCenaTovarSpis.AsFloat);
                  taSpecOutSumCenaTovar.AsFloat:=rv(quSpecOutSumCenaTovar.AsFloat);
                  taSpecOutName.AsString:=quSpecOutName.AsString;
                  taSpecOutCodeTara.AsInteger:=quSpecOutCodeTara.AsInteger;
                  StrWk:='';
                  if quSpecOutCodeTara.AsInteger>0 then fTara(quSpecOutCodeTara.AsInteger,StrWk);
                  taSpecOutNameTara.AsString:=StrWk;
                  taSpecOutVesTara.AsFloat:=quSpecOutVesTara.AsFloat;
                  taSpecOutCenaTara.AsFloat:=quSpecOutCenaTara.AsFloat;
                  taSpecOutCenaTaraSpis.AsFloat:=quSpecOutCenaTaraSpis.AsFloat;
                  taSpecOutSumVesTara.AsFloat:=quSpecOutSumVesTara.AsFloat;
                  taSpecOutSumCenaTara.AsFloat:=rv(quSpecOutSumCenaTara.AsFloat);
                  taSpecOutSumCenaTaraSpis.AsFloat:=rv(quSpecOutSumCenaTaraSpis.AsFloat);
                  taSpecOutFullName.AsString:=quSpecOutFullName.AsString;
                  taSpecOutPostDate.AsDateTime:=quSpecOutCodePodgr.AsInteger+40000;
                  taSpecOutPostNum.AsString:=its(Trunc(quSpecOutCenaPost.AsFloat));
                  taSpecOutPostQuant.AsFloat:=quSpecOutAkciz.AsFloat;

                  taSpecOutBrak.AsFloat:=quSpecOutOKDP.AsFloat;
                  if quSpecOutChekBuh.AsBoolean=True then taSpecOutCheckCM.AsInteger:=1 else taSpecOutCheckCM.AsInteger:=0;
                  taSpecOutKolF.AsFloat:=quSpecOutSumVesTara.AsFloat;

//                taSpecOutKolSpis.AsFloat:=quSpecOutSumVesTara.AsFloat-quSpecOutKol.AsFloat;

                  if quSpecOutChekBuh.AsBoolean=True then
                    taSpecOutKolSpis.AsFloat:=taSpecOutKolF.AsFloat-taSpecOutKol.AsFloat
                  else
                    taSpecOutKolSpis.AsFloat:=0;

                  taSpecOutGTD.AsString:=''; //� ���
                  if (quTTnOutProvodType.AsInteger>2) and (quTTnOutProvodType.AsInteger<>99) then //��������������� ����� ���
                  begin
                    ptGTD.First;
                    while not ptGTD.Eof do
                    begin
                      if (ptGTDCODE.AsInteger=quSpecOutCodeTovar.AsInteger)and(ptGTDID.AsInteger=RoundEx(quSpecOutSumCenaTaraSpis.AsFloat)) then
                      begin
                        sGTD:=OemToAnsiConvert(ptGTDGTD.AsString);

                        if pos('������',sCountry)>0 then sGTD:='';
                        if sCCode='' then sGTD:='';

                        taSpecOutGTD.AsString:=sGTD;
                        Break;
                      end;
                      ptGTD.Next;
                    end;
                  end;

                  taSpecOutCCode.AsString:=sCCode;
                  taSpecOutCountry.AsString:=sCountry;

                  taSpecOut.Post;
                end;
                quSpecOut.Next; inc(iC);
              end;

              with fmAddDoc2 do
              begin
                // ��� ��������� - ����� ��������
                if (CommonSet.RC=0) then
                begin
                  fmAddDoc2.frRepDOUT.LoadFromFile(CommonSet.Reports + 'schf.frf');
                end else
                begin
                  fmAddDoc2.frRepDOUT.LoadFromFile(CommonSet.Reports + 'schfRc.frf');
                  frVariables.Variable['Rukovod']:='';
                  frVariables.Variable['GlBuh']:='';
                end;

                frVariables.Variable['DocNum']:=sNum;
                frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnOutDateInvoice.AsDateTime);

                prFDepINN(iDep,StrWk1,StrWk2);
                frVariables.Variable['Cli1Inn']:=StrWk1;
                frVariables.Variable['Cli1Adr']:=StrWk2;

                quDepPars.Active:=False;
                quDepPars.ParamByName('ID').AsInteger:=iDep;
                quDepPars.Active:=True;
                if quDepPars.RecordCount>0 then
                begin
                  frVariables.Variable['Cli1Name']:=quDepParsNAMEDEP.AsString;
                  frVariables.Variable['Cli1Inn']:=quDepParsINN.AsString+' '+quDepParsKPP.AsString;
                  frVariables.Variable['Cli1Adr']:=quDepParsADRDEP.AsString;
                  frVariables.Variable['Otpr']:=quDepParsNAMEOTPR.AsString;
                  frVariables.Variable['OtprAdr']:=quDepParsADROTPR.AsString;

                  if (CommonSet.RC=0) then
                  begin
                    if Trim(quDepParsFL.AsString)>'' then
                    begin
                      frVariables.Variable['FL']:=quDepParsFL.AsString;
                      frVariables.Variable['OGRN']:=quDepParsOGRN.AsString;
                      frVariables.Variable['Rukovod']:='';
                      frVariables.Variable['GlBuh']:='';
                    end else
                    begin
                      frVariables.Variable['FL']:='';
                      frVariables.Variable['OGRN']:='';
                      frVariables.Variable['Rukovod']:='';
                      frVariables.Variable['GlBuh']:='';
                    end;
                  end else
                  begin
                    frVariables.Variable['Rukovod']:=quDepParsRukovod.AsString;
                    frVariables.Variable['GlBuh']:=quDepParsGlBuh.AsString;
                  end;
                end else
                begin
                  frVariables.Variable['Cli1Name']:='';
                  frVariables.Variable['Cli1Inn']:='';
                  frVariables.Variable['Cli1Adr']:='';
                  frVariables.Variable['Otpr']:='';
                  frVariables.Variable['OtprAdr']:='';
                  frVariables.Variable['OGRN']:='';
                  frVariables.Variable['FL']:='';
                end;

                quDepPars.Active:=False;

                prFCliINN(quTTnOutCodePoluch.AsInteger,quTTnOutIndexPoluch.AsInteger,StrWk1,StrWk2,StrWk3,SInnF);

                frVariables.Variable['Cli2Name']:=StrWk3;
                frVariables.Variable['Cli2Inn']:=StrWk1;
                frVariables.Variable['Cli2Adr']:=StrWk2;

                sInn:=StrWk1;
                if pos('/',StrWk1)>0 then sInn:=Copy(StrWk1,1,pos('/',StrWk1)-1);

                quCliPars.Active:=False;
                quCliPars.ParamByName('SINN').AsString:=SInnF;
                quCliPars.Active:=True;

                if quCliPars.RecordCount>0 then
                begin
                  if ((Trim(quCliParsNAMEOTP.AsString)='') OR (quCliParsNAMEOTP.AsString='.')) then
                  begin
                    frVariables.Variable['Poluch']:=frVariables.Variable['Cli2Name'];
                    frVariables.Variable['PoluchAdr']:=frVariables.Variable['Cli2Adr'];
                  end
                  else
                  begin
                    frVariables.Variable['Poluch']:=quCliParsNAMEOTP.AsString;
                    frVariables.Variable['PoluchAdr']:=quCliParsADROTPR.AsString;
                  end;
                end;

                quCliPars.Active:=False;

                frVariables.Variable['Lico']:=sMoll;

                frRepDOUT.ReportName:='����.';
                frRepDOUT.PrepareReport;

                frRepDOUT.PrintPreparedReport('',1,False,frAll);
              end;
            end;

            Memo1.Lines.Add('  - ������ ��');
            delay(10);
          end;
        end;

        fmAddDoc2.ViewDoc2.EndUpdate;
        ViewDocsOut.EndUpdate;
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocs2.acNewRecNDSExecute(Sender: TObject);
Var i,j,iDep:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH,iDType:INteger;
    rSum1,rSum2,rSum3,rSum10,rN10,rSum20,rN20,rSum10M,rSum20M,rSum0:Real;
    iSS:Integer;
    rSumSpis,rPrSpis:Real;
    user:string;
begin
  //�����������  ���
  user:=Person.Name;

  if (user<>'BUH') and (user<>'�����') then exit;      //((user='OPER CB') or

  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('������.');
        ViewDocsOut.BeginUpdate;

        if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
        ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

        for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsOut.Controller.SelectedRecords[i];
          iDep:=0;
          sNum:='';
          dDateDoc:=Date;
          IDH:=0;
          iDType:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutProvodType' then begin iDType:=Rec.Values[j]; end;
          end;

          if (iDep>0)and(sNum>'')and(IDH>0)and(iDType<>3)and(iDType<>0) and (iDType<>99) then
          begin
            Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));

            //������� �� ������������ �������
            ptOutLn.CancelRange;
            ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNum)],[iDep,dDateDoc,AnsiToOemConvert(sNum)]);
            ptOutLn.First;

            rSum1:=0;
            rSum2:=0;
            rSum3:=0;
            rSum10:=0;
            rN10:=0;
            rSum20:=0;
            rN20:=0;
            rSum10M:=0;
            rSum20M:=0;
            rSum0:=0;
            rSumSpis:=0;
            while not ptOutLn.Eof do
            begin //����� �� ������������ � ������� ���
              if ptOutLnCodeTovar.AsInteger>0 then
              begin
                ptOutLn.Edit;
                ptOutLnOutNDSSum.AsFloat:=rv(ptOutLnSumCenaTovar.AsFloat/(100+ptOutLnNDSProc.AsFloat)*100);
                ptOutLnNDSSum.AsFloat:=ptOutLnSumCenaTovar.AsFloat-ptOutLnOutNDSSum.AsFloat;
                ptOutLn.Post;

                rSum1:=rSum1+ptOutLnSumCenaTovar.AsFloat; //� ����� ����������
                rSum2:=rSum2+ptOutLnSumCenaTovarSpis.AsFloat; //� ����� ��������
                rSum3:=rSum3+ptOutLnSumCenaTara.AsFloat;  //���� ����

                iSS:=fSS(ptOutLnDepart.AsInteger);
                if ptOutLnCodeTovar.AsInteger>0 then
                begin
                  if iSS<2 then rPrSpis:=ptOutLnCenaTovar.AsFloat
                  else rPrSpis:=rv(ptOutLnSumCenaTovar.AsFloat*100/(100+ptOutLnNDSProc.AsFloat))/ptOutLnKol.AsFloat;
                  rSumSpis:=rSumSpis+rv(rPrSpis*((ptOutLnKol.AsFloat+ptOutLnKol.AsFloat*ptOutLnOKDP.AsFloat/100)-ptOutLnKol.AsFloat));    //Crock
                end;

                if ptOutLnNDSProc.AsFloat>=15 then
                begin
                  rSum20:=rSum20+RoundVal(ptOutLnOutNDSSum.AsFloat);
                  rN20:=rN20+RoundVal(ptOutLnNDSSum.AsFloat);
                  rSum20M:=rSum20M+RoundVal(ptOutLnSumCenaTovarSpis.AsFloat);
                end else
                begin
                  if ptOutLnNDSProc.AsFloat>=8 then
                  begin
                    rSum10:=rSum10+RoundVal(ptOutLnOutNDSSum.AsFloat);
                    rN10:=rN10+RoundVal(ptOutLnNDSSum.AsFloat);
                    rSum10M:=rSum10M+RoundVal(ptOutLnSumCenaTovarSpis.AsFloat);
                  end else // ��� ���
                  begin
                    rSum0:=rSum0+ptOutLnSumCenaTovar.AsFloat;
                  end;
                end;
              end;

              ptOutLn.Next; delay(10);
            end;

            quA.Active:=False;
            quA.SQL.Clear;
            quA.SQL.Add('Update "TTNOut" set ');
            quA.SQL.Add(' SummaTovar='+fs(rSum1));
            quA.SQL.Add('NDS10='+fs(rN10));
            quA.SQL.Add(',OutNDS10='+fs(rSum10));
            quA.SQL.Add(',NDS20='+fs(rN20));
            quA.SQL.Add(',OutNDS20='+fs(rSum20));
            quA.SQL.Add(',NDSTovar='+fs(rN10+rN20));
            quA.SQL.Add(',OutNDSTovar='+fs(rSum10+rSum20));
            quA.SQL.Add(',NotNDSTovar='+fs(rSum0));
            quA.SQL.Add(',OutNDSSNTovar='+fs(rSum1-rSum10-rSum20));
            quA.SQL.Add(',SummaTovarSpis='+fs(rSum2));
            quA.SQL.Add(',NacenkaTovar='+fs(rSum2-rSum1));
            quA.SQL.Add(',SummaTara='+fs(rSum3));
            quA.SQL.Add(',SummaTaraSpis='+fs(rSum3));
            quA.SQL.Add(',WithNDS10='+fs(rSum10+rN10));
            quA.SQL.Add(',WithNDS20='+fs(rSum20+rN20));
            quA.SQL.Add(',Crock='+fs(rSumSpis));
            quA.SQL.Add(',NDS_10='+fs(rSum10M));
            quA.SQL.Add(',NDS_20='+fs(rSum20M));
            quA.SQL.Add(' where Depart='+its(iDep));
            quA.SQL.Add(' and DateInvoice='''+ds(dDateDoc)+'''');
            quA.SQL.Add(' and Number='''+sNum+'''');
//            quA.SQL.SaveToFile('C:\1.txt');
            quA.ExecSQL;

            delay(10);
          end;
        end;

        ViewDocsOut.EndUpdate;
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocs2.acTestMoveExecute(Sender: TObject);
Var i,j,iDep:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH:INteger;
    user:string;
begin
  //�����������  ���
  user:=Person.Name;

  if (user<>'BUH') and (user<>'�����') and (user<>'OPER CB') and (user<>'OPERZAK') then exit;

  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('������.');
        ViewDocsOut.BeginUpdate;

        if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
        ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

        for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsOut.Controller.SelectedRecords[i];
          iDep:=0;
          sNum:='';
          dDateDoc:=Date;
          IDH:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
            if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
          end;

          if (iDep>0)and(sNum>'')and(IDH>0) then
          begin
            Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));

            quSpecOut2.Active:=False;
            quSpecOut2.ParamByName('IDEP').AsInteger:=iDep;
            quSpecOut2.ParamByName('SDATE').AsDate:=Trunc(dDateDoc);
            quSpecOut2.ParamByName('SNUM').AsString:=sNum;
            quSpecOut2.Active:=True;

            quSpecOut2.First;  // ��������������
            while not quSpecOut2.Eof do
            begin
              prDelTMoveId(iDep,quSpecOut2CodeTovar.AsInteger,Trunc(dDateDoc),IDH,2,(quSpecOut2Kol.AsFloat*-1));
              prAddTMoveId(iDep,quSpecOut2CodeTovar.AsInteger,Trunc(dDateDoc),IDH,2,(quSpecOut2Kol.AsFloat*-1));

              quSpecOut2.Next;
            end;

            quSpecOut2.Active:=False;
            delay(10);
          end;
        end;

        ViewDocsOut.EndUpdate;
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocs2.acCreateInExecute(Sender: TObject);
Var i,j,iDep,iDepN:Integer;
    Rec:TcxCustomGridRecord;
    sNum:String;
    dDateDoc:TDateTime;
    IDH,iDType:INteger;
    bDo:Boolean;
    rSum1,rSum2,rSum10,rN10,rSum20,rN20,rSumNec,rSum10M,rSum20M, rSum0:Real;
    IDHIN,InxPost,CodePost,iNum:INteger;
    rPrIn,rPrM,rNac:Real;
begin
  //������������ ��������� ���������
  bDo:=False;
  if pos('BUH',Person.Name)>0 then bDo:=True;
  if pos('�����',Person.Name)>0 then bDo:=True;
  if pos('OPER CB',Person.Name)>0 then bDo:=True;
  if pos('OPERZAK',Person.Name)>0 then bDo:=True;

  if bDo=False then exit;

  with dmMC do
  with dmMt do
  begin
    if ViewDocsOut.Controller.SelectedRecordCount>0 then
    begin
      if MessageDlg('�������� '+Its(ViewDocsOut.Controller.SelectedRecordCount)+' �������. ���������� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iDepN:=0;
        fmSelDep:=TfmSelDep.Create(Application);
        fmSelDep.quDeps.Active:=False;
        fmSelDep.quDeps.Active:=True;
        fmSelDep.quDeps.First;
        fmSelDep.cxLookupComboBox2.EditValue:=fmSelDep.quDepsID.AsInteger;

        fmSelDep.ShowModal;
        if fmSelDep.ModalResult=mrOk then iDepN:=fmSelDep.cxLookupComboBox2.EditValue;
        fmSelDep.quDeps.Active:=False;
        fmSelDep.Release;

        if iDepN>0 then
        begin
          Memo1.Clear;
          Memo1.Lines.Add('������.');
          ViewDocsOut.BeginUpdate;

          if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
          ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

          if ptTTnInLn.Active=False then ptTTnInLn.Active:=True else ptTTnInLn.Refresh;
          ptTTnInLn.IndexFieldNames:='CodeTovar;DateInvoice';

          for i:=0 to ViewDocsOut.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewDocsOut.Controller.SelectedRecords[i];
            iDep:=0;
            sNum:='';
            dDateDoc:=Date;
            IDH:=0;
            iDType:=0;

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewDocsOut.Columns[j].Name='ViewDocsOutDepart' then begin iDep:=Rec.Values[j];  end;
              if ViewDocsOut.Columns[j].Name='ViewDocsOutNumber' then begin sNum:=Rec.Values[j]; end;
              if ViewDocsOut.Columns[j].Name='ViewDocsOutDateInvoice' then begin dDateDoc:=Rec.Values[j]; end;
              if ViewDocsOut.Columns[j].Name='ViewDocsOutID' then begin IDH:=Rec.Values[j];  end;
              if ViewDocsOut.Columns[j].Name='ViewDocsOutProvodType' then begin iDType:=Rec.Values[j]; end;
            end;

            if (iDep>0)and(sNum>'')and(IDH>0)and(iDType<>99) then
            begin
              if quTTnOut.Locate('ID',IDH,[]) then
              begin
                Memo1.Lines.Add('  �������� '+sNum+' ��  '+ds1(dDateDoc)+' ����� -'+its(iDep)+' ��� - '+its(IDH));

                //�������� ������������
                quC.Active:=False;
                quC.SQL.Clear;
                quC.SQL.Add('Select Count(*) as RecCount from "TTNIn"');
                quC.SQL.Add('where Depart='+INtToStr(iDepN));
                quC.SQL.Add('and DateInvoice='''+FormatDateTime(sCrystDate,dDateDoc)+'''');
                quC.SQL.Add('and Number='''+sNum+'''');
                quC.Active:=True;
                if quCRecCount.AsInteger=0 then
                begin

                //������� �� ������������ ������� - ������� �����
                rSum0:=0; rSum1:=0; rSum2:=0; rN10:=0; rN20:=0; rSum10:=0; rSum20:=0; rSumNec:=0; rSum10M:=0; rSum20M:=0;

                ptOutLn.CancelRange;
                ptOutLn.SetRange([iDep,dDateDoc,AnsiToOemConvert(sNum)],[iDep,dDateDoc,AnsiToOemConvert(sNum)]);
                ptOutLn.First;
                while not ptOutLn.Eof do
                begin //����� �� ������������
                  if taCards.FindKey([ptOutLnCodeTovar.AsInteger]) then
                  begin
                    rSum1:=rSum1+rv(ptOutLnCenaTovar.AsFloat*ptOutLnKol.AsFloat); //����� � ���
                    rSum2:=rSum2+rv(taCardsCena.AsFloat*ptOutLnKol.AsFloat);      //����� ��������

                    if ptOutLnNDSProc.AsFloat>0.001 then
                    begin
                      if ptOutLnNDSProc.AsFloat>=15 then
                      begin
                        rSum20:=rSum20+rv(ptOutLnOutNDSSum.AsFloat);
                        rN20:=rN20+rv(ptOutLnNDSSum.AsFloat);
                        rSum20M:=rSum20M+rv(taCardsCena.AsFloat*ptOutLnKol.AsFloat);
                      end else
                      begin
                        if ptOutLnNDSProc.AsFloat>=8 then
                        begin
                          rSum10:=rSum10+rv(ptOutLnOutNDSSum.AsFloat);
                          rN10:=rN10+rv(ptOutLnNDSSum.AsFloat);
                          rSum10M:=rSum10M+rv(taCardsCena.AsFloat*ptOutLnKol.AsFloat);
                        end else rSum0:=rSum0+rv(ptOutLnSumCenaTovar.AsFloat);
                      end;
                    end else rSum0:=rSum0+rv(ptOutLnSumCenaTovar.AsFloat);
                  end;
                  ptOutLn.Next;
                end;
                delay(10);

                IDHIN:=prMax('DocsIn')+1;

                InxPost:=quTTnOutIndexPoluch.AsInteger;
                CodePost:=quTTnOutCodePoluch.AsInteger;

                //������� ���������
{
                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "TTNIn" values (');
                quA.SQL.Add(its(iDepN)+',');  //      Depart
                quA.SQL.Add(''''+ds(dDateDoc)+''',');    // DateInvoice
                quA.SQL.Add(''''+sNum+''','); // Number
                quA.SQL.Add(''''+sNum+''',');  //      SCFNumber
                quA.SQL.Add(its(quTTnOutIndexPoluch.AsInteger)+',');     // IndexPost
                quA.SQL.Add(its(quTTnOutCodePoluch.AsInteger)+',');  // CodePost
                quA.SQL.Add(fs(rSum1)+',');  // SummaTovarPost ����� ���������� � ���
                quA.SQL.Add(fs(rSum20+rSum10+rN20+rN10)+',');  // OblNDSTovar
                quA.SQL.Add('0,');    // LgtNDSTovar
                quA.SQL.Add(fs(rSum0)+',');   //NotNDSTovar
                quA.SQL.Add(fs(rN10)+',');   //NDS10
                quA.SQL.Add(fs(rSum10)+',');   //OutNDS10
                quA.SQL.Add(fs(rN20)+',');   //NDS20
                quA.SQL.Add(fs(rSum20)+',');   //OutNDS20
                quA.SQL.Add('0,0,');    // LgtNDS //OutLgtNDS
                quA.SQL.Add(fs(rN20+rN10)+',');    //NDSTovar
                quA.SQL.Add(fs(rSum20+rSum10)+',');    //OutNDSTovar
                quA.SQL.Add('0,');                   //ReturnTovar
                quA.SQL.Add(fs(rSum0)+',');  //OutNDSSNTovar
                quA.SQL.Add('0,0,');               //Boy  //OthodiPost
                quA.SQL.Add(fs(rSum1)+',');        //DolgPost
                quA.SQL.Add(fs(rSum2)+',');          //SummaTovar
                quA.SQL.Add(fs(rSum2-rSum1)+','); //Nacenka
                quA.SQL.Add(fs(0)+','); // Transport
                quA.SQL.Add(fs(0)+',0,0,'); //NDSTransport  //ReturnTara  // Strah
                quA.SQL.Add(fs(0)+',');   //SummaTara
                quA.SQL.Add(fs(0)+',');  //AmortTara
                quA.SQL.Add('0,0,'+its(0)+',0,0,'); // AcStatus  // ChekBuh  // ProvodType  //NotNDS10  //NotNDS20
                quA.SQL.Add(fs(rN10+rSum10)+','); //WithNDS10
                quA.SQL.Add(fs(rN20+rSum20)+','); //WithNDS20
                quA.SQL.Add('0,'); //Crock
                quA.SQL.Add(fs(rSumNec)+','); //OthodiPoluch
                quA.SQL.Add('0,0,0,');      //Discount  //NDS010 //NDS020
                quA.SQL.Add(fs(rSum10M)+',');  //NDS_10
                quA.SQL.Add(fs(rSum20M)+',');   //NDS_20
                quA.SQL.Add('0,0,');   //NSP_10 //NSP_20
                quA.SQL.Add(''''+ds(dDateDoc)+''',0,0,'''+sNum+''',');  //SCFDate  //GoodsNSP0 //GoodsCostNSP //OrderNumber
                quA.SQL.Add(its(IDHIN)+',0,0,0,0,0,0,'''''); // ID LinkInvoice  StartTransfer EndTransfer  GoodsWasteNDS0  GoodsWasteNDS10 GoodsWasteNDS20 REZERV
                quA.SQL.Add(')');
}

                quA.Active:=False;
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "TTNIn" values (');
                quA.SQL.Add(its(iDepN)+',');  //      Depart
                quA.SQL.Add(''''+ds(dDateDoc)+''',');    // DateInvoice
                quA.SQL.Add(''''+sNum+''','); // Number
                quA.SQL.Add(''''+sNum+''',');  //      SCFNumber
                quA.SQL.Add(its(quTTnOutIndexPoluch.AsInteger)+',');     // IndexPost
                quA.SQL.Add(its(quTTnOutCodePoluch.AsInteger)+',');  // CodePost
                quA.SQL.Add(fs(rSum1)+',');  // SummaTovarPost ����� ���������� � ���
                quA.SQL.Add(fs(rSum20+rSum10+rN20+rN10)+',');  // OblNDSTovar
                quA.SQL.Add('0,');    // LgtNDSTovar
                quA.SQL.Add(fs(rSum0)+',');   //NotNDSTovar
                quA.SQL.Add(fs(rN10)+',');   //NDS10
                quA.SQL.Add(fs(rSum10)+',');   //OutNDS10
                quA.SQL.Add(fs(rN20)+',');   //NDS20
                quA.SQL.Add(fs(rSum20)+',');   //OutNDS20
                quA.SQL.Add('0,0,');    // LgtNDS //OutLgtNDS
                quA.SQL.Add(fs(rN20+rN10)+',');    //NDSTovar
                quA.SQL.Add(fs(rSum20+rSum10)+',');    //OutNDSTovar
                quA.SQL.Add('0,');                   //ReturnTovar
                quA.SQL.Add(fs(rSum0)+',');  //OutNDSSNTovar
                quA.SQL.Add('0,0,');               //Boy  //OthodiPost
                quA.SQL.Add(fs(rSum1)+',');        //DolgPost
                quA.SQL.Add(fs(rSum2)+',');          //SummaTovar
                quA.SQL.Add(fs(rSum2-rSum1)+','); //Nacenka
                quA.SQL.Add(fs(0)+','); // Transport
                quA.SQL.Add(fs(0)+',0,0,'); //NDSTransport  //ReturnTara  // Strah
                quA.SQL.Add(fs(0)+',');   //SummaTara
                quA.SQL.Add(fs(0)+',');  //AmortTara
                quA.SQL.Add('0,0,'+its(0)+',0,0,'); // AcStatus  // ChekBuh  // ProvodType  //NotNDS10  //NotNDS20
                quA.SQL.Add(fs(rN10+rSum10)+','); //WithNDS10
                quA.SQL.Add(fs(rN20+rSum20)+','); //WithNDS20
                quA.SQL.Add('0,'); //Crock
                quA.SQL.Add(fs(rSumNec)+','); //OthodiPoluch
                quA.SQL.Add('0,0,0,');      //Discount  //NDS010 //NDS020
                quA.SQL.Add(fs(rSum10M)+',');  //NDS_10
                quA.SQL.Add(fs(rSum20M)+',');   //NDS_20
                quA.SQL.Add('0,0,');   //NSP_10 //NSP_20
                quA.SQL.Add(''''+ds(dDateDoc)+''',0,0,0,');  //SCFDate  //GoodsNSP0 //GoodsCostNSP //OrderNumber
                quA.SQL.Add(its(IDHIN)+',0,0,0,0,0,0,'''''); // ID LinkInvoice  StartTransfer EndTransfer  GoodsWasteNDS0  GoodsWasteNDS10 GoodsWasteNDS20 REZERV
                quA.SQL.Add(')');

                quA.ExecSQL;

                delay(100);

                //������� ��������� ������������
                if ptInLn.Active=False then ptINLn.Active:=True else ptINLn.Refresh;

                ptInLn.CancelRange;
                ptInLn.SetRange([iDepN,dDateDoc,AnsiToOemConvert(sNum)],[iDepN,dDateDoc,AnsiToOemConvert(sNum)]);
                ptInLn.First;

                iNum:=1;

                ptOutLn.First;
                while not ptOutLn.Eof do
                begin //����� �� ������������
                  if taCards.FindKey([ptOutLnCodeTovar.AsInteger]) then
                  begin
                    rPrIn:=ptOutLnCenaTovar.AsFloat;
                    rPrM:=taCardsCena.AsFloat;
                    try
                      rNac:=rv((rPrM-rPrIn)/rPrIn*100);
                    except
                      rNac:=0;
                    end;

                    ptInLn.Append;
                    ptInLnDepart.AsInteger:=iDepN;
                    ptInLnDateInvoice.AsDateTime:=dDateDoc;
                    ptInLnNumber.AsString:=AnsiToOemConvert(sNum); //��������� ���� ����������
                    ptInLnIndexPost.AsInteger:=InxPost;
                    ptInLnCodePost.AsInteger:=CodePost;
                    ptInLnCodeGroup.AsInteger:=0;
                    ptInLnCodePodgr.AsInteger:=0;
                    ptInLnCodeTovar.AsInteger:=ptOutLnCodeTovar.AsInteger;
                    ptInLnCodeEdIzm.AsInteger:=ptOutLnCodeEdIzm.AsInteger;
                    ptInLnTovarType.AsInteger:=ptOutLnTovarType.AsInteger;
                    ptInLnBarCode.AsString:=ptOutLnBarCode.AsString;
                    ptInLnMediatorCost.AsFloat:=iNum;
                    ptInLnNDSProc.AsFloat:=ptOutLnNDSProc.AsFloat;
                    ptInLnNDSSum.AsFloat:=ptOutLnNDSSum.AsFloat;
                    ptInLnOutNDSSum.AsFloat:=ptOutLnOutNDSSum.AsFloat;
                    ptInLnBestBefore.AsDateTime:=0;
                    ptInLnInvoiceQuant.AsFloat:=0;
                    ptInLnKolMest.AsInteger:=1;
                    ptInLnKolEdMest.AsFloat:=0;
                    ptInLnKolWithMest.AsFloat:=ptOutLnKolWithMest.AsFloat;
                    ptInLnKolWithNecond.AsFloat:=ptOutLnKolWithMest.AsFloat;
                    ptInLnKol.AsFloat:=ptOutLnKol.AsFloat;
                    ptInLnCenaTovar.AsFloat:=rPrIn;
                    ptInLnProcent.AsFloat:=rNac;
                    ptInLnNewCenaTovar.AsFloat:=rPrM;
                    ptInLnSumCenaTovarPost.AsFloat:=rv(ptOutLnSumCenaTovar.AsFloat);
                    ptInLnSumCenaTovar.AsFloat:=rv(rPrM*ptOutLnKolWithMest.AsFloat);
                    ptInLnProcentN.AsFloat:=0;
                    ptInLnNecond.AsFloat:=0;
                    ptInLnCenaNecond.AsFloat:=0;
                    ptInLnSumNecond.AsFloat:=0;
                    ptInLnProcentZ.AsFloat:=0;
                    ptInLnZemlia.AsFloat:=0;
                    ptInLnProcentO.AsFloat:=0;
                    ptInLnOthodi.AsFloat:=0;
                    ptInLnCodeTara.AsInteger:=0;
                    ptInLnVesTara.AsFloat:=0;
                    ptInLnCenaTara.AsFloat:=0;
                    ptInLnSumVesTara.AsFloat:=0;
                    ptInLnSumCenaTara.AsFloat:=0;
                    ptInLnChekBuh.AsBoolean:=False;
                    ptInLnSertBeginDate.AsDateTime:=date;
                    ptInLnSertEndDate.AsDateTime:=date;
                    ptInLnSertNumber.AsString:='';
                    ptInLn.Post;
                  end;
                  ptOutLn.Next;   inc(iNum);
                end;
                Memo1.Lines.Add('   ������������ ��');
                delay(10);
                end else Memo1.Lines.Add('   --- ������!!! �������� � ����� ������� ��� ����.');
              end;
            end;
          end;

          ViewDocsOut.EndUpdate;
          Memo1.Lines.Add('������� ��������.');
        end;
      end;
    end;
  end;
end;

procedure prClearFromInv(Depart:Integer;SDate:TDate;Number:String);
begin
  with fmInvVoz do
  with dmMC do
  begin
    quSpecOut2.Active:=False;
    quSpecOut2.ParamByName('IDEP').AsInteger:=Depart;
    quSpecOut2.ParamByName('SDATE').AsDate:=SDate;
    quSpecOut2.ParamByName('SNUM').AsString:=Number;
    quSpecOut2.Active:=True;

    if quSpecOut2.RecordCount<1 then
    begin
      fmInvVoz.Memo1.Lines.Add('��������� �����!');
      exit;
    end;

    if MessageDlg('������ ����� ��������� �'+Number+' �� ����� ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin

      ViewInvVoz.BeginUpdate;

      quSpecOut2.First;
      while not quSpecOut2.Eof do
      begin
        if teInvVoz.Locate('IdCode1',quSpecOut2CodeTovar.AsInteger,[]) then
        begin
          teInvVoz.Edit;
          teInvVozQFact.AsFloat:=(teInvVozQFact.AsFloat-quSpecOut2Kol.AsFloat);
          teInvVozQRazn.AsFloat:=(teInvVozQFact.AsFloat-teInvVozQOut.AsFloat);
          if teInvVozQRazn.AsFloat<0 then teInvVozQRazn.AsFloat:=0;

          teInvVoz.Post;
        end
        else
          fmInvVoz.Memo1.Lines.Add('����� � ����� '+its(quSpecOut2CodeTovar.AsInteger)+' �� ������!');

        quSpecOut2.Next;
      end;
      quSpecOut2.Active:=False;

      ViewInvVoz.EndUpdate;

    end;
  end;
end;


procedure TfmDocs2.acSendProExecute(Sender: TObject);
Var bErr:Boolean;
    sGlnMHPro,sGLNCliPro:String;
    iDepPro,iCliPro:Integer;
    StrWk:String;
    IDHPRO:INteger;
    rSumNDS0,rSumNDS10,rSumNDS20:Real;
    sName,sMessur:String;
    iCodePro,iMPro:Integer;
    rKCB,rKM:Real;
begin
  //�������� � ������������
  with dmMc do
  with dmPro do
  with dmMt do
  begin
    Memo1.Clear;
    if quTTnOut.RecordCount>0 then
    begin
      if quTTNOutAcStatus.AsInteger<>100 then // =3  �������� ��� �����
      begin
        if MessageDlg('�������� ��������� �'+quTTnOutNumber.AsString+' �� '+ds1(quTTnOutDateInvoice.AsDateTime)+' ('+quTTnOutNameCli.AsString+') � ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          bErr:=True;
          try
            prButtonSet(False);

            prWM('�����.. ���� �������� ������.',Memo1);
            try
              OfficeRnDb.Close;
              OfficeRnDb.DatabaseName:=CommonSet.PathProDb;
              OfficeRnDb.Open;
              prWM('    ���� PRO - ��',Memo1);

              //����� ������� ������������ �� ��
              msConnection.Close;
              Strwk:='FILE NAME='+CurDir+'Ecr.udl';
              msConnection.CommandTimeout := CommonSet.iCTimeOut;
              msConnection.ConnectionTimeout := CommonSet.iTimeOut;

              msConnection.ConnectionString:=Strwk;
              delay(10);
              try
                msConnection.Connected:=True;
                prWM('    ���� �� - ��',Memo1);
              except
                prWM('  ������ ����� � ����� ��.',Memo1);
              end;

              if msConnection.Connected then
              begin

              //���� ������������ ������������
              sGlnMHPro:=prFindGLNCli(quTTnOutCodePoluch.asINteger); // ����� � ������������
              sGLNCliPro:=prFindGLNDep(quTTnOutDepart.AsINteger);    // ��������� � ������������

              if (sGlnMHPro>'')and(sGLNCliPro>'') then
              begin
                iDepPro:=0;
                iCliPro:=0;
                //��� ��� ����� ��������
                quMH.Active:=False;
                quMH.ParamByName('SGLN').AsString:=Trim(sGlnMHPro);
                quMH.Active:=True;
                if quMH.RecordCount>0 then
                begin
                  quMH.First;
//                  iSS:=quMHISS.AsInteger;
                  iDepPro:=quMHID.AsInteger;
                  prWM('              ����: �� '+quMHNAMEMH.AsString+' ('+its(quMHID.AsInteger)+') ��� '+sGlnMHPro,Memo1);
                end else prWM('              ����: �� ������ - ��� '+sGlnMHPro,Memo1);
                quMH.Active:=False;

                //��� ��� �����������
                quCli.Active:=False;
                quCli.ParamByName('SGLN').AsString:=Trim(sGLNCliPro);
                quCli.Active:=True;
                if quCli.RecordCount>0 then
                begin
                  quCli.First;
                  iCliPro:=quCliID.AsInteger;
                  prWM('              �� ����: '+quCliNAMECL.AsString+' ('+its(quCliID.AsInteger)+') ��� '+sGLNCliPro,Memo1);
                end else prWM('              �� ����: �� ������ - ��� '+sGLNCliPro,Memo1);
                quCli.Active:=False;

                if (iCliPro>0) and (iDepPro>0) then
                begin

                  //��������� ���� ����� �������� ��� ���
                  quDocInRec.Active:=False;
                  quDocInRec.ParamByName('ICLI').AsInteger:=iCliPro;
                  quDocInRec.ParamByName('ISKL').AsInteger:=iDepPro;
                  quDocInRec.ParamByName('DDATE').AsDateTime:=quTTnOutDateInvoice.AsDateTime;
                  quDocInRec.ParamByName('SNUM').AsString:=Trim(quTTnOutNumber.AsString);
                  quDocInRec.Active:=True;
                  if quDocInRec.RecordCount=0 then
                  begin //��������� ����� ��������
                    prWM('      ������������ ������ ��������� ...',Memo1);

                    IDHPRO:=GetId('DocIn');

                    quDocInRec.Append;
                    quDocInRecID.AsInteger:=IDHPRO;
                    quDocInRecDATEDOC.AsDateTime:=quTTnOutDateInvoice.AsDateTime;
                    quDocInRecNUMDOC.AsString:=Trim(quTTnOutNumber.AsString);
                    quDocInRecDATESF.AsDateTime:=quTTnOutDateInvoice.AsDateTime;
                    quDocInRecNUMSF.AsString:=trim(quTTnOutSCFNumber.AsString);
                    quDocInRecIDCLI.AsInteger:=iCliPro;
                    quDocInRecIDSKL.AsInteger:=iDepPro;
                    quDocInRecSUMIN.AsFloat:=rv(quTTnOutSummaTovar.AsFloat);
                    quDocInRecSUMUCH.AsFloat:=0;
                    quDocInRecSUMTAR.AsFloat:=0;
                    quDocInRecSUMNDS0.AsFloat:=0;
                    quDocInRecSUMNDS1.AsFloat:=0;
                    quDocInRecSUMNDS2.AsFloat:=0;
                    quDocInRecPROCNAC.AsFloat:=0;
                    quDocInRecIACTIVE.AsInteger:=0;
                    quDocInRecCOMMENT.AsString:='';
                    quDocInRecOPRIZN.AsInteger:=0;
                    quDocInRecCREATETYPE.AsInteger:=1;
                    quDocInRec.Post;

                    //������������
                    rSumNDS0:=0; rSumNDS10:=0; rSumNDS20:=0;

                    if ptOutLn.Active=False then ptOutLn.Active:=True else ptOutLn.Refresh;
                    ptOutLn.IndexFieldNames:='Depart;DateInvoice;Number';

                    quSpecInSel.Active:=False;
                    quSpecInSel.ParamByName('IDHD').AsInteger:=IDHPRO;
                    quSpecInSel.Active:=True;

                    quSpecInSel.First; //������� ������
                    while not quSpecInSel.Eof do quSpecInSel.Delete;

                    ptOutLn.CancelRange;
                    ptOutLn.SetRange([quTTnOutDepart.AsInteger,quTTnOutDateInvoice.AsDateTime,AnsiToOemConvert(quTTnOutNumber.AsString)],[quTTnOutDepart.AsInteger,quTTnOutDateInvoice.AsDateTime,AnsiToOemConvert(quTTnOutNumber.AsString)]);
                    ptOutLn.First;
                    while not ptOutLn.Eof do
                    begin
                      if ptOutLnCodeTovar.AsInteger>0 then
                      begin
                        sName:='';
                        sMessur:='��.';
                        if taCards.FindKey([ptOutLnCodeTovar.AsInteger]) then
                        begin
                          sName:=OemToAnsiConvert(taCardsName.AsString);
                          if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                        end;

                        prFindCodePro(ptOutLnCodeTovar.AsInteger,iCodePro,iMPro,rKCB,rKM);

                        if rKCB=0 then rKCB:=1;

                        quSpecInSel.Append;
                        quSpecInSelIDHEAD.AsInteger:=IDHPRO;
                        quSpecInSelID.AsInteger:=GetId('SpecIn');
                        quSpecInSelNUM.AsInteger:=RoundEx(ptOutLnSumCenaTaraSpis.AsInteger);
                        quSpecInSelIDCARD.AsInteger:=iCodePro;

                        quSpecInSelQUANT.AsFloat:=ptOutLnKol.AsFloat*rKCB;
                        quSpecInSelPRICEIN.AsFloat:=rv(ptOutLnCenaTovar.AsFloat/rKCB);
                        quSpecInSelSUMIN.AsFloat:=ptOutLnSumCenaTovar.AsFloat;
                        quSpecInSelPRICEUCH.AsFloat:=0;
                        quSpecInSelSUMUCH.AsFloat:=0;

                        if ptOutLnNDSProc.AsFloat>15 then
                        begin
                          quSpecInSelIDNDS.AsInteger:=3;
                          rSumNDS20:=rSumNDS20+ptOutLnNDSSum.AsFloat;
                        end else
                        begin
                          if ptOutLnNDSProc.AsFloat>5 then
                          begin
                            quSpecInSelIDNDS.AsInteger:=2;
                            rSumNDS10:=rSumNDS10+ptOutLnNDSSum.AsFloat;
                          end else
                          begin
                            quSpecInSelIDNDS.AsInteger:=1;
                          end;
                        end;

                        quSpecInSelSUMNDS.AsFloat:=ptOutLnNDSSum.AsFloat;
                        quSpecInSelIDM.AsInteger:=iMPro;
                        quSpecInSelKM.AsFloat:=rKM;
                        try
                          quSpecInSelPRICE0.AsFloat:=ptOutLnOutNDSSum.AsFloat/ptOutLnKol.AsFloat;
                        except
                          quSpecInSelPRICE0.AsFloat:=0;
                        end;
                        quSpecInSelSUM0.AsFloat:=ptOutLnOutNDSSum.AsFloat;

                        quSpecInSelNDSPROC.AsFloat:=ptOutLnNDSProc.AsFloat;

                        quSpecInSelOPRIZN.AsInteger:=0;
                        quSpecInSelICODECB.AsInteger:=ptOutLnCodeTovar.AsInteger;
                        quSpecInSelNAMECB.AsString:=sName;
                        quSpecInSelIMCB.AsInteger:=taCardsEdIzm.AsInteger;
                        quSpecInSelSMCB.AsString:=sMessur;
                        quSpecInSelKBCB.AsFloat:=rKCB;
                        quSpecInSelSBARCB.AsString:=ptOutLnBarCode.AsString;
                        quSpecInSelQUANTCB.AsFloat:=ptOutLnKol.AsFloat;
                        quSpecInSel.Post;
                      end;

                      ptOutLn.Next;
                    end;
                    quSpecInSel.Active:=False;


                    quDocInRec.Edit;
                    quDocInRecSUMNDS0.AsFloat:=rSumNDS0;
                    quDocInRecSUMNDS1.AsFloat:=rSumNDS10;
                    quDocInRecSUMNDS2.AsFloat:=rSumNDS20;
                    quDocInRec.Post;

                  end else
                  begin //����� �������� ��� ���� - ���� ������ �� ������
                    prWM('      �������� ��� ���� � ������� ...',Memo1);




                  end;
                  quDocInRec.Active:=False;
                end;
              end;
              end;
              prWM('    ��������� ���� Pro',Memo1);
              OfficeRnDb.Close;
              prWM('    ��������� ���� ��',Memo1);
              msConnection.Close;
            except
              prWM('    ���� PRO - ����������.',Memo1);
            end;

          finally
            if bErr then prWM('  �������� ��������� ��.',Memo1)
            else prWM('  ������ !!! �������� �� �����������.',Memo1);
            prButtonSet(True);
          end;
        end;
      end else prWM('�������� ������ ���������.',Memo1);
    end;
  end;
end;

end.
