unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxPC, cxControls, cxContainer, cxEdit, cxLabel, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxGraphics, cxDropDownEdit, cxMemo, VaClasses, VaComm,
  VaSystem, DB, dxmdaset, cxSpinEdit, cxRadioGroup;

type
  TfmMainLoader = class(TForm)
    OpenDialog1: TOpenDialog;
    DevPrint: TVaComm;
    Memo1: TcxMemo;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    Page1: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    cxLabel1: TcxLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButton2: TcxButton;
    cxLabel2: TcxLabel;
    cxComboBox1: TcxComboBox;
    Page2: TcxTabSheet;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxLabel3: TcxLabel;
    taArt: TdxMemData;
    taArtCode: TIntegerField;
    Label3: TLabel;
    cxLabel4: TcxLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxRButton1: TcxRadioButton;
    cxRButton2: TcxRadioButton;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxButtonEdit2: TcxButtonEdit;
    cxComboBox2: TcxComboBox;
    cxLabel8: TcxLabel;
    cxSpinEdit2: TcxSpinEdit;
    cxLabel7: TcxLabel;
    cxButton3: TcxButton;
    Label6: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxButton4: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(S:String);
  end;

procedure Delay(MSecs: Longint);

var
  fmMainLoader: TfmMainLoader;
  CurDir:String;

const
  iTimeOut:Integer=50; //��������

implementation

{$R *.dfm}

procedure TfmMainLoader.prWr(S:String);
begin
  Memo1.Lines.Add(FormatDateTime('hh:nn ss:zzz ',now)+S);
end;


procedure TfmMainLoader.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainLoader.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  cxPageControl1.Align:=AlClient;
  OpenDialog1.InitialDir:=CurDir;
  cxButtonEdit1.Text:=CurDir+'Referenc.dat';
  cxButtonEdit2.Text:=CurDir+'Inventry.dat';
  Memo1.Align:=AlClient;
  Memo1.Clear;
  cxPageControl1.ActivePageIndex:=0;
end;

procedure TfmMainLoader.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

procedure TfmMainLoader.cxButton2Click(Sender: TObject);
Var bErr:Boolean;
    f:TextFile;
    StrWk:String;
    iC,iCode,n,iD,iErr,i:INteger;
    sCode:String;
    Buff:Array[1..50] of Char;
    BuffR:Array[1..5] of Char;
begin
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  iD:=cxSpinEdit1.Value;
  Memo1.Clear;
  prWr('������ ��������');
  iC:=0;
  Label2.Caption:=INtToStr(iC);
  Label3.Caption:='0';

  bErr:=False;
  try
    prWr('  ��������� ���� COM'+IntToStr(cxComboBox1.ItemIndex+1));
    DevPrint.DeviceName:='COM'+IntToStr(cxComboBox1.ItemIndex+1);
    try
      DevPrint.Open;
    except
      prWr('  ������ �������� �����.');
      bErr:=True;
      try
        DevPrint.Close;
      except
      end;
    end;

    if not bErr then
    begin
      prWr('  ���� ��');
      prWr('  ��������� ���� - '+cxButtonEdit1.Text);
      if FileExists(cxButtonEdit1.Text) then
      begin
        prWr('  ���� - '+cxButtonEdit1.Text+'  ����������.');
        assignfile(f,cxButtonEdit1.Text);
        try
          Reset(f);
          prWr('');
          prWr('  ������������� � ���������� ...');
          if cxRButton1.Checked then
          begin
            taArt.Active:=False;
            taArt.Active:=True;

            iErr:=0;

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then begin bErr:=True; iErr:=11; end;

            Buff[1]:=#$30;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then begin bErr:=True; iErr:=12; end;

            Buff[1]:=#$00;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then begin bErr:=True; iErr:=13; end;

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then begin bErr:=True; iErr:=14; end;


            prWr('    ����� ... ���� ��������.');

            Buff[1]:=#$04;
//          DevPrint.WriteBuf(Buff,1); delay(iD);
            for n:=2 to 49 do Buff[n]:=chr(n-1);
            DevPrint.WriteBuf(Buff,49); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$22 then begin bErr:=True; iErr:=22; end;

            while (EOF(f)=False)and(bErr=False) do
            begin
              inc(iC); Label2.Caption:=INtToStr(iC); Delay(10);
              ReadLn(f,StrWk);
              if Length(StrWk)>=49 then
              begin
                Label3.Caption:='0';
                Buff[1]:=#$01;
                DevPrint.WriteBuf(Buff,1); delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]<>#$02 then
                begin
                  for i:=1 to iTimeOut do
                  begin
                    delay(iD);
                    DevPrint.ReadBuf(BuffR,1);
                    if BuffR[1]=#$02 then break;
                    Label3.Caption:=IntToStr(i);
                  end;
                  if i=iTimeOut then  begin bErr:=True; iErr:=2; end;
                end;
                if not bErr then
                begin
                  Buff[1]:=#$04;
                  for n:=2 to 49 do Buff[n]:= StrWk[n-1];
                  DevPrint.WriteBuf(Buff,49); delay(iD);
                  DevPrint.ReadBuf(BuffR,1);
                  if BuffR[1]<>#$22 then
                  begin
                    for i:=1 to iTimeOut do
                    begin
                      delay(iD);
                      DevPrint.ReadBuf(BuffR,1);
                      if BuffR[1]=#$22 then break;
                      Label3.Caption:=IntToStr(i);
                    end;
                    if i=iTimeOut then  begin bErr:=True; iErr:=22; end;
                  end;
                end;

                sCode:=Copy(StrWk,14,5); while pos(' ',sCode)>0 do delete(sCode,pos(' ',sCode),1);
                iCode:=StrToINtDef(sCode,0);
                if iCode>0 then
                begin
                  if taArt.Locate('Code',iCode,[])=False then
                  begin
                    taArt.Append;
                    taArtCode.AsInteger:=iCode;
                    taArt.Post;
                  end;
                end else
                begin
                  prWr('      - ������ �������������� ���. '+IntToStr(iC)+' ('+Copy(StrWk,1,18)+')');
                end;  
              end;
            end;

            if bErr then
            begin
              prWr('      ����� �� ������ ('+IntToStr(iErr)+')');
            end;

            Buff[1]:=#$01;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);

            Buff[1]:=#$0F;
            Buff[2]:=#$30;
            Buff[3]:=#$00;
//          DevPrint.WriteBuf(Buff,3); delay(iD);
            for n:=4 to 49 do Buff[n]:=chr(n+2);
            DevPrint.WriteBuf(Buff,49); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if iErr>0 then
              prWr('      ������ '+INtToStr(iErr));

            prWr('    ����� ��������� '+INtToStr(iC)+'- �� '+IntToStr(taArt.RecordCount)+'- ��������.');
            taArt.Active:=False;
          end;
          if cxRButton2.Checked then
          begin
            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            Buff[1]:=#$05;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            Buff[1]:=#$00;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            prWr('    ����� ... ���� ��������.');

            Buff[1]:=#$04;
            for n:=2 to 6 do Buff[n]:=chr(n-1);
            DevPrint.WriteBuf(Buff,6); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$22 then bErr:=True;

            while (EOF(f)=False)and(bErr=False) do
            begin
              inc(iC); Label2.Caption:=INtToStr(iC); Delay(10);
              ReadLn(f,StrWk);
              if (Length(StrWk)>=4) and (Length(StrWk)<6) then
              begin
                if Length(StrWk)<5 then StrWk:=StrWk+' ';

                Buff[1]:=#$01;
                DevPrint.WriteBuf(Buff,1); delay(iD);
                DevPrint.ReadBuf(BuffR,1);
                if BuffR[1]<>#$02 then bErr:=True;
                if not bErr then
                begin
                  Buff[1]:=#$04;
                  for n:=2 to 6 do Buff[n]:= StrWk[n-1];
                  DevPrint.WriteBuf(Buff,6); delay(iD);
                  DevPrint.ReadBuf(BuffR,1);
                  if BuffR[1]<>#$22 then bErr:=True;
                end;
              end;
            end;

            Buff[1]:=#$01;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);

            Buff[1]:=#$0F;
            Buff[2]:=#$05;
            Buff[3]:=#$00;
//          DevPrint.WriteBuf(Buff,3); delay(iD);
            for n:=4 to 6 do Buff[n]:=chr(n-1);
            DevPrint.WriteBuf(Buff,6); delay(iD);
            DevPrint.ReadBuf(BuffR,1);


            if bErr then  prWr('      -- ������ ��� ��������.');
            prWr('    ����� ��������� '+INtToStr(iC)+'- ����� �� .');
          end;
          prWr('');
          CloseFile(f);
        except
          prWr('  ������ ������ � ������.');
        end;
      end else
        prWr('  ���� - '+cxButtonEdit1.Text+'  �� ������.');
    end;
  finally
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    DevPrint.Close;
    prWr('�������� ���������.');
  end;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TfmMainLoader.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  cxButtonEdit2.Text:=OpenDialog1.FileName;
end;

procedure TfmMainLoader.cxButton3Click(Sender: TObject);
//�������
Var bErr:Boolean;
    f:TextFile;
    StrWk:String;
    iC,n,iD:INteger;
    Buff:Array[1..5] of Char;
    BuffR:Array[1..50] of Char;

  procedure prClBuf;
  var i:SmallINt;
  begin
    for i:=1 to 27 do BuffR[i]:='0';
  end;

begin
  cxButton3.Enabled:=False;
  cxButton4.Enabled:=False;
  cxButton2.Enabled:=False;
  iD:=cxSpinEdit2.Value;
  Memo1.Clear;
  prWr('������ �������');
  iC:=0;
  Label5.Caption:=INtToStr(iC);

  bErr:=False;
  try
    prWr('  ��������� ���� COM'+IntToStr(cxComboBox2.ItemIndex+1));
    DevPrint.DeviceName:='COM'+IntToStr(cxComboBox2.ItemIndex+1);
    try
      DevPrint.Open;
    except
      prWr('  ������ �������� �����.');
      bErr:=True;
      try
        DevPrint.Close;
      except
      end;
    end;

    if not bErr then
    begin
      prWr('  ���� ��');
      if FileExists(cxButtonEdit2.Text) then
      begin //���� ���������� - ������ ����������
        prWr('  ���� - '+cxButtonEdit2.Text+' ����������. ������� ����������.');
      end else
      begin //����� ��� ����� ��������� ������
        try
          assignfile(f,cxButtonEdit2.Text);
          rewrite(f);
          try
            prWr('  ���� ������ ��');
            prWr('');
            prWr('  �������������...');

            Buff[1]:=#$01;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            Buff[1]:=#$00;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1);
            if BuffR[1]<>#$02 then bErr:=True;

            Buff[1]:=#$1A;
            BuffR[1]:=#$00;
            DevPrint.WriteBuf(Buff,1); delay(iD);
            DevPrint.ReadBuf(BuffR,1); delay(iD);
            if BuffR[1]<>#$02 then bErr:=True;


            BuffR[1]:=#$00;
            DevPrint.ReadBuf(BuffR,1); delay(10);
            if BuffR[1]=#$01 then
            begin          //��� � ��� �������������
              Buff[1]:=#$02;
              DevPrint.WriteBuf(Buff,1); delay(iD);
            end else bErr:=True;

            prWr('    ����� ... ���� ��������.');

            if not bErr then
            begin
              prClBuf;
              DevPrint.ReadBuf(BuffR,27); delay(10); //������ ��� ������
              if BuffR[1]<>#$04 then bErr:=True
              else
              begin
                while (BuffR[1]<>#$0F)and(bErr=False) do
                begin
                  //������� 22 ������������� ���������� ��������
                  Buff[1]:=#$22;
                  DevPrint.WriteBuf(Buff,1); delay(iD);

                  //��� 01
                  prClBuf;
                  DevPrint.ReadBuf(BuffR,1); delay(10); //������ ��� ������

                  if BuffR[1]=#$01 then  //������
                  begin //������� 02 �� �����������
                    Buff[1]:=#$02;
                    DevPrint.WriteBuf(Buff,1); delay(iD);

                    prClBuf;
                    DevPrint.ReadBuf(BuffR,27); delay(10); //������ ��� ������
                    if BuffR[1]=#$04 then //����� ��� �����
                    begin
                      inc(iC); Label5.Caption:=INtToStr(iC); Delay(10);
                      StrWk:='';
                      for n:=2 to 27 do StrWk:=StrWk+BuffR[n];
                      StrWk:=StrWk+'1';
//                      StrWk:=IntToStr(iC);
                      writeLn(f,StrWk);
                    end;
                  end else
                  begin
                    bErr:=True;
                  end;
                end;
              end;
              //��� ��������� ���������
              if bErr=False then
              begin
                Buff[1]:=#$22;
                DevPrint.WriteBuf(Buff,1); delay(iD);
              end else
              begin
                prWr('    ������ ��� ������� ������...');
              end;
            end;
            prWr('');
          finally
            CloseFile(f);
          end;
        except
          prWr('  ������ �������� �����. ������� ����������.');
        end;
      end;
    end;
  finally
    cxButton3.Enabled:=True;
    cxButton4.Enabled:=True;
    cxButton2.Enabled:=True;
    DevPrint.Close;
    prWr('�������� ���������.');
  end;
end;

procedure TfmMainLoader.cxButton4Click(Sender: TObject);
Var f:TextFile;
begin
  prWr('');
  prWr('������ - '+cxButtonEdit2.Text);
  if FileExists(cxButtonEdit2.Text) then
  begin
    try
      assignfile(f,cxButtonEdit2.Text);
      erase(f);
      prWr('�������� ��.');
    except
      prWr('������ ��� ��������.');
    end;
  end else
  begin
    prWr('����� ��� - ������� ������.');
  end;
end;

end.
