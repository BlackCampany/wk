unit AvSpeed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, cxLookAndFeelPainters, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid;

type
  TfmAvSpeed = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    Label5: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    cxCalcEdit2: TcxCalcEdit;
    Label7: TLabel;
    cxCalcEdit3: TcxCalcEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ViewAvReal: TcxGridDBTableView;
    LeAR: TcxGridLevel;
    GrAR: TcxGrid;
    ViewAvRealRecId: TcxGridDBColumn;
    ViewAvRealiDep: TcxGridDBColumn;
    ViewAvRealiPrih: TcxGridDBColumn;
    ViewAvRealdDateP: TcxGridDBColumn;
    ViewAvRealQIn: TcxGridDBColumn;
    ViewAvRealQReal: TcxGridDBColumn;
    ViewAvRealiDateReal: TcxGridDBColumn;
    ViewAvRealrAvrR: TcxGridDBColumn;
    ViewAvRealkDov: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAvSpeed: TfmAvSpeed;

implementation

uses MDB, MT, Un1;

{$R *.dfm}

procedure TfmAvSpeed.Excel1Click(Sender: TObject);
begin
 //
  prNExportExel5(ViewAvReal);
end;

end.
