unit GrafPostavok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfmGrafPostavok = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem10: TSpeedItem;
    ViewGrafPostavok: TcxGridDBTableView;
    LevelGrafPostavok: TcxGridLevel;
    GridGrafPostavok: TcxGrid;
    ViewGrafPostavokICLI: TcxGridDBColumn;
    ViewGrafPostavokFullName: TcxGridDBColumn;
    ViewGrafPostavokDDATEP: TcxGridDBColumn;
    ViewGrafPostavokDDATEZ: TcxGridDBColumn;
    ViewGrafPostavokName: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGrafPostavok: TfmGrafPostavok;

implementation

uses MDB, Un1;

{$R *.dfm}

procedure TfmGrafPostavok.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGrafPostavok.SpeedItem10Click(Sender: TObject);
begin
   //������� � ������
  prNExportExel5(ViewGrafPostavok);
end;

procedure TfmGrafPostavok.FormCreate(Sender: TObject);
begin
  GridGrafPostavok.Align:=AlClient;
end;

end.
