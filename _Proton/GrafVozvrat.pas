unit GrafVozvrat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxCalendar, StdCtrls,
  ComCtrls;

type
  TfmGrafVozvrat = class(TForm)
    ViewGrafVozvrat: TcxGridDBTableView;
    LevelGrafVozvrat: TcxGridLevel;
    GridGrafVozvrat: TcxGrid;
    ViewGrafVozvratFullName: TcxGridDBColumn;
    ViewGrafVozvratIDATEV: TcxGridDBColumn;
    ViewGrafVozvratName: TcxGridDBColumn;
    ViewGrafVozvratICLI: TcxGridDBColumn;
    ViewGrafVozvratIDATEZ: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem10: TSpeedItem;
    ViewGrafVozvratMOLVOZ: TcxGridDBColumn;
    ViewGrafVozvratPHONEVOZ: TcxGridDBColumn;
    ViewGrafVozvratEMAILVOZ: TcxGridDBColumn;
    CheckBox1: TCheckBox;
    StatusBar1: TStatusBar;
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure SpeedItem10Click0(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGrafVozvrat: TfmGrafVozvrat;

implementation

uses MDB, Un1, Period1, dmPS;

{$R *.dfm}

procedure TfmGrafVozvrat.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGrafVozvrat.SpeedItem10Click(Sender: TObject);
begin
   //������� � ������
  prNExportExel5(ViewGrafVozvrat);
end;

procedure TfmGrafVozvrat.FormCreate(Sender: TObject);
begin
  GridGrafVozvrat.Align:=AlClient;
  ViewGrafVozvrat.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmGrafVozvrat.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    ViewGrafVozvrat.BeginUpdate;
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmP do
    begin
      if  CheckBox1.Checked=false then
      begin
        quGrafVozvrat2.Active:=False;
        quGrafVozvrat2.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
        quGrafVozvrat2.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);
        quGrafVozvrat2.Active:=True;
      end
      else
      begin
        quGrafVozvrat.Active:=False;
        quGrafVozvrat.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
        quGrafVozvrat.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);
        quGrafVozvrat.Active:=True;
      end;


  {    ViewGrafVozvrat.BeginUpdate;
      quGrafVozvrat.Active:=False;

      quGrafVozvrat.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quGrafVozvrat.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);

      quGrafVozvrat.Active:=True;
      ViewGrafVozvrat.EndUpdate;   }
    end;
    ViewGrafVozvrat.EndUpdate;
  end;
end;

procedure TfmGrafVozvrat.SpeedItem1Click0(Sender: TObject);
begin
  close;
end;

procedure TfmGrafVozvrat.SpeedItem10Click0(Sender: TObject);
begin
  prNExportExel5(ViewGrafVozvrat);
end;

procedure TfmGrafVozvrat.CheckBox1Click(Sender: TObject);
begin
  with dmP do
  begin
    if  CheckBox1.Checked=true then
    begin
      //CheckBox1.Checked:=true;

      ViewGrafVozvrat.BeginUpdate;
      dsquGrafVozvrat.DataSet:=nil;
      quGrafVozvrat2.Active:=False;
      quGrafVozvrat2.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quGrafVozvrat2.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);
      quGrafVozvrat2.Active:=True;
      dsquGrafVozvrat.DataSet:=quGrafVozvrat2;
      ViewGrafVozvrat.EndUpdate;
    end
    else
    begin
   //   CheckBox1.Checked:=false;

      ViewGrafVozvrat.BeginUpdate;
      dsquGrafVozvrat.DataSet:=nil;
      quGrafVozvrat.Active:=False;
      quGrafVozvrat.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.DateBeg);
      quGrafVozvrat.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.DateEnd);
      quGrafVozvrat.Active:=True;
      dsquGrafVozvrat.DataSet:=quGrafVozvrat;
      ViewGrafVozvrat.EndUpdate;
    end;
  end;
end;

end.
