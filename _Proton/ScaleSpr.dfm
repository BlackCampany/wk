object fmScaleSpr: TfmScaleSpr
  Left = 560
  Top = 244
  BorderStyle = bsDialog
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1077#1089#1086#1074
  ClientHeight = 266
  ClientWidth = 722
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 577
    Top = 0
    Width = 145
    Height = 266
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 168
      Width = 32
      Height = 13
      Caption = 'Label1'
      Visible = False
    end
    object Label2: TLabel
      Left = 20
      Top = 232
      Width = 32
      Height = 13
      Caption = 'Label2'
      Visible = False
    end
    object cxButton1: TcxButton
      Left = 12
      Top = 20
      Width = 117
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 12
      Top = 52
      Width = 117
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxSEdit1: TcxSpinEdit
      Left = 8
      Top = 96
      TabOrder = 2
      Value = 859058691
      Visible = False
      Width = 121
    end
    object Button1: TButton
      Left = 16
      Top = 128
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 3
      Visible = False
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 20
      Top = 188
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
      Visible = False
      OnClick = Button2Click
    end
  end
  object GrSc: TcxGrid
    Left = 16
    Top = 12
    Width = 541
    Height = 237
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewSc: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaSc
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = False
      object ViewScRecId: TcxGridDBColumn
        Caption = #8470#1087#1087
        DataBinding.FieldName = 'RecId'
        Visible = False
        Width = 43
      end
      object ViewScNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1074#1077#1089#1086#1074
        DataBinding.FieldName = 'Number'
        Width = 61
      end
      object ViewScModel: TcxGridDBColumn
        Caption = #1052#1086#1076#1077#1083#1100
        DataBinding.FieldName = 'Model'
        Width = 63
      end
      object ViewScName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 156
      end
      object ViewScPluCount: TcxGridDBColumn
        Caption = #1052#1072#1082#1089'. '#1082#1086#1083'-'#1074#1086' PLU'
        DataBinding.FieldName = 'PluCount'
      end
      object ViewScIp1: TcxGridDBColumn
        Caption = 'IP1'
        DataBinding.FieldName = 'Ip1'
        Width = 33
      end
      object ViewScIp2: TcxGridDBColumn
        Caption = 'IP2'
        DataBinding.FieldName = 'Ip2'
        Width = 36
      end
      object ViewScIp3: TcxGridDBColumn
        Caption = 'IP3'
        DataBinding.FieldName = 'Ip3'
        Width = 37
      end
      object ViewScIp4: TcxGridDBColumn
        Caption = 'IP4'
        DataBinding.FieldName = 'Ip4'
        Width = 36
      end
      object ViewScIp5: TcxGridDBColumn
        Caption = 'IP5'
        DataBinding.FieldName = 'Ip5'
        Width = 36
      end
    end
    object LevelSc: TcxGridLevel
      GridView = ViewSc
    end
  end
  object taSc: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 84
    Top = 152
    object taScNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object taScModel: TStringField
      FieldName = 'Model'
    end
    object taScName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object taScPluCount: TIntegerField
      FieldName = 'PluCount'
    end
    object taScIp1: TStringField
      FieldName = 'Ip1'
      Size = 3
    end
    object taScIp2: TStringField
      FieldName = 'Ip2'
      Size = 3
    end
    object taScIp3: TStringField
      FieldName = 'Ip3'
      Size = 3
    end
    object taScIp4: TStringField
      FieldName = 'Ip4'
      Size = 3
    end
    object taScIp5: TStringField
      FieldName = 'Ip5'
      Size = 3
    end
  end
  object dstaSc: TDataSource
    DataSet = taSc
    Left = 140
    Top = 152
  end
end
