program CasherRnFF17;

uses
  Windows,
  Forms,
  MainFF17 in 'MainFF17.pas' {fmMainFF},
  Un1 in 'Un1.pas',
  Dm in 'Dm.pas' {dmC: TDataModule},
  UnCash in 'UnCash.pas',
  PreFF in 'PreFF.pas' {fmPreFF},
  ModifFF in 'ModifFF.pas' {fmModifFF},
  CashEnd in 'CashEnd.pas' {fmCash},
  Calc in 'Calc.pas' {fmCalc},
  Discont in 'Discont.pas' {fmDiscount},
  Attention in 'Attention.pas' {fmAttention},
  CredCards in 'CredCards.pas' {fmCredCards},
  UnitBN in 'UnitBN.pas',
  PreFF1 in 'PreFF1.pas' {fmPre};

{$R *.res}

Const AppID='SoftUr';

Var Handle:THandle;

begin
  // ������� � ���������� ������ 1-�������� "����" � ����������
  // ������ AppID, ���������� ��� � ���� �������� ������������
  // � ���������, ��� �� �� ������ ��� ������ ������.
  Handle:=CreateFileMapping($FFFFFFFF,Nil,PAGE_READONLY,0,1,AppID);
  If GetLastError=ERROR_ALREADY_EXISTS then MessageBox(0,'������ ������ ����� ��������� ����������.',AppID,MB_OK or MB_ICONSTOP or MB_TOPMOST)
  else
  begin
    Application.Initialize;
    Application.CreateForm(TfmMainFF, fmMainFF);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmPreFF, fmPreFF);
  Application.CreateForm(TfmCash, fmCash);
  Application.CreateForm(TfmCalc, fmCalc);
  Application.CreateForm(TfmDiscount, fmDiscount);
  Application.CreateForm(TfmAttention, fmAttention);
  Application.CreateForm(TfmPre, fmPre);
  Application.Run;
  end;
  // ����������� ������ � ��� ����� ��������� ��������� ������.
  CloseHandle(Handle)
end.
