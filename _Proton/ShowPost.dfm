object fmShowPost: TfmShowPost
  Left = 497
  Top = 191
  Width = 494
  Height = 162
  Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090' '#1074' '#1087#1088#1077#1076#1079#1072#1103#1074#1082#1072#1093
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GrShowPost: TcxGrid
    Left = 0
    Top = 0
    Width = 486
    Height = 129
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewShowPost: TcxGridDBTableView
      DragMode = dmAutomatic
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmP.dsquQPost
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewShowPostIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCLI'
        Width = 47
      end
      object ViewShowPostNameCli: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'FullNamecli'
        Width = 336
      end
      object ViewShowPostQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
        Width = 58
      end
    end
    object LevShowPost: TcxGridLevel
      GridView = ViewShowPost
    end
  end
end
