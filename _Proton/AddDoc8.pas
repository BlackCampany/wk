unit AddDoc8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  QDialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox, cxSpinEdit,
  cxCheckBox, FR_DSet, FR_DBSet, FR_Class, dxmdaset, cxProgressBar;

type
   TfmAddDoc8 = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    dstaSpecVoz: TDataSource;
    cxLabel6: TcxLabel;
    amDocVoz: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    acReadBar: TAction;
    acReadBar1: TAction;
    acPostTov: TAction;
    Vi1: TcxGridDBTableView;
    Lev1: TcxGridLevel;
    Gr1: TcxGrid;
    Vi1Name: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi2: TcxGridDBTableView;
    Vi2Name: TcxGridDBColumn;
    Vi2DateInvoice: TcxGridDBColumn;
    Vi2Number: TcxGridDBColumn;
    Vi2Kol: TcxGridDBColumn;
    Vi2CenaTovar: TcxGridDBColumn;
    Vi2CenaTovarSpis: TcxGridDBColumn;
    Vi2NDSProc: TcxGridDBColumn;
    Vi2NDSSum: TcxGridDBColumn;
    Vi2NameCli: TcxGridDBColumn;
    Panel1: TPanel;
    Label12: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxTextEdit1: TcxTextEdit;
    cxButton8: TcxButton;
    Label1: TLabel;
    cxTextEdit2: TcxTextEdit;
    Panel4: TPanel;
    Edit1: TEdit;
    GridDoc8: TcxGrid;
    ViewDoc8: TcxGridDBTableView;
    ViewDoc8DOCDATE: TcxGridDBColumn;
    ViewDoc8Number: TcxGridDBColumn;
    ViewDoc8Depart: TcxGridDBColumn;
    ViewDoc8DepartName: TcxGridDBColumn;
    ViewDoc8Code: TcxGridDBColumn;
    ViewDoc8NameCode: TcxGridDBColumn;
    ViewDoc8idcli: TcxGridDBColumn;
    ViewDoc8NameCli: TcxGridDBColumn;
    ViewDoc8Quant: TcxGridDBColumn;
    ViewDoc8QuantOut: TcxGridDBColumn;
    ViewDoc8QRem: TcxGridDBColumn;
    ViewDoc8PriceIn0: TcxGridDBColumn;
    ViewDoc8PriceIn: TcxGridDBColumn;
    ViewDoc8RPrice: TcxGridDBColumn;
    ViewDoc8RSumIn0: TcxGridDBColumn;
    ViewDoc8RSumIn: TcxGridDBColumn;
    ViewDoc8RsumNDS: TcxGridDBColumn;
    ViewDoc8Status: TcxGridDBColumn;
    ViewDoc8DateVoz: TcxGridDBColumn;
    ViewDoc8Reason: TcxGridDBColumn;
    LevelDoc8: TcxGridLevel;
    ViewDoc8QALLREM: TcxGridDBColumn;
    PBar1: TcxProgressBar;
    LVD8: TcxGridLevel;
    VD8: TcxGridDBTableView;
    VD8DOCDATE: TcxGridDBColumn;
    VD8Number: TcxGridDBColumn;
    VD8Depart: TcxGridDBColumn;
    VD8Code: TcxGridDBColumn;
    VD8NameCode: TcxGridDBColumn;
    VD8idcli: TcxGridDBColumn;
    VD8Quant: TcxGridDBColumn;
    VD8QuantOut: TcxGridDBColumn;
    VD8QRem: TcxGridDBColumn;
    VD8QALLREM: TcxGridDBColumn;
    VD8PriceIn0: TcxGridDBColumn;
    VD8PriceIn: TcxGridDBColumn;
    VD8RPrice: TcxGridDBColumn;
    VD8RSumIn0: TcxGridDBColumn;
    VD8RSumIn: TcxGridDBColumn;
    VD8RsumNDS: TcxGridDBColumn;
    VD8Status: TcxGridDBColumn;
    VD8DateVoz: TcxGridDBColumn;
    VD8Reason: TcxGridDBColumn;
    taSpecVoz: TdxMemData;
    taSpecVoznumber: TIntegerField;
    taSpecVozID: TIntegerField;
    taSpecVozDOCDATE: TIntegerField;
    taSpecVozCode: TIntegerField;
    taSpecVozNameCode: TStringField;
    taSpecVozDepart: TIntegerField;
    taSpecVozDepartName: TStringField;
    taSpecVozidcli: TIntegerField;
    taSpecVoznamecli: TStringField;
    taSpecVozquant: TFloatField;
    taSpecVozquantout: TFloatField;
    taSpecVozqrem: TFloatField;
    taSpecVozQAllRem: TFloatField;
    taSpecVozPriceIn0: TFloatField;
    taSpecVozPriceIn: TFloatField;
    taSpecVozRPrice: TFloatField;
    taSpecVozRSumIn0: TFloatField;
    taSpecVozRSumIn: TFloatField;
    taSpecVozRSumNDS: TFloatField;
    taSpecVozdatevoz: TIntegerField;
    taSpecVozStatus: TStringField;
    taSpecVozReason: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure ViewDoc8Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure acReadBar1Execute(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPostTovExecute(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure ViewDoc8SelectionChanged(Sender: TcxCustomGridTableView);
    procedure taSpecVozquantChange(Sender: TField);
    procedure taSpecVozPriceIn0Change(Sender: TField);
    procedure taSpecVozPriceInChange(Sender: TField);
    procedure taSpecVozRPriceChange(Sender: TField);
    procedure ViewDoc8CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewDoc8CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Vi1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure taSpecVozDepartNameChange(Sender: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure taSpecVozCalcFields(DataSet: TDataSet);
    procedure ViewDoc8EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtons(bSt:Boolean);
  end;
    procedure typevozvrat(icli,flag:integer;sBarVes:string);
    procedure typenevozvrat(icli,flag:integer;sBarVes:string);
    procedure predzgrafvoz(i,j,icli:integer);
    procedure predztoday(i,j,icli:integer);
var
  fmAddDoc8: TfmAddDoc8;
  bAdd:Boolean = False;

implementation

uses Un1,MDB, Clients, mFind, mCards, mTara, sumprops, Move, MT, QuantMess,
  u2fdk, MainMCryst, TBuff, DocHVoz, AddDoc2, dmPS, PostGood;

{$R *.dfm}
procedure typevozvrat(icli,flag:integer; sBarVes:string);
  var i:integer;
begin

  with dmP do
  with fmAddDoc8 do
  begin
    quDateVoz.Active:=False;
    quDateVoz.ParamByName('IDATEB').AsInteger:=trunc(Date);
    quDateVoz.ParamByName('IDATEE').AsInteger:=trunc(Date)+180;
    quDateVoz.ParamByName('IDATEZ').AsInteger:=trunc(Date);
    quDateVoz.ParamByName('IDCli').AsInteger:=quPostVozidcli.AsInteger;
    quDateVoz.Active:=True;
                                                          //��������� ���� �� ������ ��������
    if quDateVoz.RecordCount<1 then
    begin
        if MessageDlg('� ���������� ���������� ��� ������� ���������, ��������� �� ������� ����?',mtConfirmation,[mbYes, mbNo], 0)=3 then
        begin
          predztoday(1,1,icli);
          if flag=1 then
          begin
            taSpecVoz.Edit;
            taSpecVozquant.AsFloat:=strtoint(trim(sBarVes))/1000;
            taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;
            taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
            taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
            taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
            taSpecVoz.Post;
          end;
        end
        else
        begin                                          //�������� ���� �� ������ ���������� ��� ��������
          icli:=quPostVozidcli.AsInteger;
          i:=0;
          CommonSet.DateBeg:=Date-730;
          CommonSet.DateEnd:=Date;
          fmP.VP.BeginUpdate;
          quPost2.Active:=False;
          quPost2.ParamByName('IDCARD').AsInteger:=quPostVozCode.AsInteger;
          quPost2.ParamByName('DDATEB').AsDate:=trunc(Date-730);
          quPost2.ParamByName('DDATEE').AsDate:=trunc(Date);
          quPost2.Active:=True;
          fmP.VP.EndUpdate;
          if quPost2.RecordCount<1 then exit       //���� ������ ���
          else
          begin
            quPost2.First;
            while not quPost2.Eof do
            begin
              if quPost2idcli.AsInteger<>icli then
              begin
                i:=1;                //���� ������ ����������
              end;
              quPost2.Next;
            end;

            if i=1 then                 // ���� ����
            begin
              if MessageDlg('�������� ����� ������ ����������� ��� ��������?',mtConfirmation,[mbYes, mbNo], 0)=3 then
              begin
                fmP.Caption:=quPost2NameCode.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
                bDrVoz:=false;
                fmP.Show;
              end else exit;
            end;
          end;
        end;
    end
    else
    begin
      predzgrafvoz(1,1,icli);
      if flag=1 then
      begin
        taSpecVoz.Edit;
        taSpecVozquant.AsFloat:=(strtoint(trim(sBarVes)))/1000;
        taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;
        taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
        taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
        taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
        taSpecVoz.Post;
      end;
    end;


  end;
end;

procedure typenevozvrat(icli,flag:integer;sBarVes:string);
begin
  with dmP do
  with fmAddDoc8 do
  begin
    CommonSet.DateBeg:=Date-730;
    CommonSet.DateEnd:=Date;
    fmP.VP.BeginUpdate;               //���� ���� �� ������ ���������� ����������
    quPost2.Active:=False;
    quPost2.ParamByName('IDCARD').AsInteger:=quPostVozCode.AsInteger;
    quPost2.ParamByName('DDATEB').AsDate:=trunc(Date-730);
    quPost2.ParamByName('DDATEE').AsDate:=trunc(Date);
    quPost2.Active:=True;
    fmP.VP.EndUpdate;
    if quPost2.RecordCount<1 then                //���� ���
    begin

      if MessageDlg('��� �����������,������� ����� ������� �����. ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=3  then
      begin
        predztoday(2,1,icli);
        if flag=1 then
        begin
          taSpecVoz.Edit;
          taSpecVozquant.AsFloat:=strtoint(trim(sBarVes))/1000;
          taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;
          taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
          taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
          taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
          taSpecVoz.Post;
        end;
      end
      else  exit;
    end
    else            //���� ���� �� ���������� ������� ����� � ���������� ������������
    begin
      showmessage('��������� ��������� �� ����� ������� ��������,'+chr(13)+'�������� ���������� ��� ��������');
      fmP.Caption:=quPost2NameCode.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
      bDrVoz:=false;
      fmP.Show;
    end;
  end;
end;

procedure predzgrafvoz(i,j,icli:integer);
 var iMax:integer;
     cenaformat:real;
begin
  with dmP do
  with fmAddDoc8 do
  begin
    taSpecVoz.Last;
    iMax:=taSpecVozNumber.AsInteger+1;
    taSpecVoz.Append;
    taSpecVozNumber.AsInteger:=iMax;

    if i=0 then    //���������� ���������� �� ����� ������ �����������   ������ �� �������
    begin
      taSpecVozCode.AsInteger:=quPost2Code.AsInteger;
      taSpecVozNameCode.AsString:=quPost2NameCode.AsString;

      quNamePost.Active:=false;
      quNamePost.ParamByName('icli').AsInteger:=icli;
      quNamePost.Active:=True;
      if quNamePost.RecordCount>0 then
      begin

        taSpecVozidcli.AsInteger:=icli;
        taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
      end
      else
      begin
        taSpecVozidcli.AsInteger:=quPost2idcli.AsInteger;
        taSpecVozNamecli.AsString:=quPost2NamePost.AsString;
      end;

    {  taSpecVozidcli.AsInteger:=quPost2idcli.AsInteger;
      taSpecVozNamecli.AsString:=quPost2NamePost.AsString;  }
      taSpecVozDepart.AsInteger:=quPost2Depart.AsInteger;
      taSpecVozDepartName.AsString:=quPost2NameDep.AsString;

      if quPost2kol.AsFloat>0 then
      cenaformat:=((quPost2SumPost.AsFloat-quPost2NDSSum.AsFloat)/quPost2kol.AsFloat)
      else cenaformat:=0;

      taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPost2PriceIn.AsFloat;
      taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPost2Rprice.AsFloat;
      taSpecVozRPrice.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));

    end;
    if (i=1) then            //��������� ��������� ������ ������ �� �������
    begin
      taSpecVozCode.AsInteger:=quPostVozCode.AsInteger;
      taSpecVozNameCode.AsString:=quPostVozNameCode.AsString;

      quNamePost.Active:=false;
      quNamePost.ParamByName('icli').AsInteger:=icli;
      quNamePost.Active:=True;
      if quNamePost.RecordCount>0 then
      begin
        taSpecVozidcli.AsInteger:=icli;
        taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
      end
      else
      begin
        taSpecVozidcli.AsInteger:=quPostVozidcli.AsInteger;
        taSpecVozNamecli.AsString:=quPostVozNamecli.AsString;
      end;

    {  taSpecVozidcli.AsInteger:=quPostVozidcli.AsInteger;
      taSpecVozNamecli.AsString:=quPostVozNamecli.AsString;  }
      taSpecVozDepart.AsInteger:=quPostVozDepart.AsInteger;
      taSpecVozDepartName.AsString:=quPostVozDepartName.AsString;

      if quPostVozkol.AsFloat>0 then
      cenaformat:=(quPostVozSumPost.AsFloat-quPostVozNDSSum.AsFloat)/quPostVozkol.AsFloat
      else cenaformat:=0;

      taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPostVozPriceIn.AsFloat;
      taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPostVozRprice.AsFloat;
      taSpecVozRPrice.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));

    end;

    if j=0 then
    begin
      if dmMC.quCardsTovarType.AsInteger=0 then taSpecVozQuant.AsFloat:=1             //����� ������� ��� �������
      else taSpecVozQuant.AsFloat:=dmMC.quCardsWeght.AsFloat;
    end else  taSpecVozQuant.AsFloat:=1;

    taSpecVozQuantOut.AsFloat:=0;
    taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;

    cenaformat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
    cenaformat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
    cenaformat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumNDS.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));

    taSpecVozDateVoz.AsInteger:=quDateVozIDATEV.AsInteger;                         //���� �� �������


    if dmP.quReasV.Active=false then dmP.quReasV.Active:=true;

    if quReasV.RecordCount>0 then
    begin
      quReasV.Locate('id',6,[]);
      taSpecVozReason.AsInteger:=quReasVid.AsInteger;
    end;
    taSpecVoz.Post;

    ViewDoc8.Controller.FocusRecord(ViewDoc8.DataController.FocusedRowIndex,True);
    griddoc8.SetFocus;
    ViewDoc8Quant.Focused:=true;
    viewdoc8quant.Selected:=true;
    ViewDoc8.Controller.SelectAllColumns;
    ViewDoc8Quant.Options.Editing:=True;
    ViewDoc8Reason.Options.Editing:=true;

    if taSpecVozidcli.AsInteger=2471 then
    begin
      Vi1.BeginUpdate;
      quPost1.Active:=False;
      Vi1.EndUpdate;

    end
    else
    begin
      Vi1.BeginUpdate;
      quPost1.Active:=False;
      quPost1.ParamByName('IDCARD').AsInteger:=taSpecVozCode.AsInteger;
      quPost1.ParamByName('DDATEB').AsDate:=trunc(Date-730);
      quPost1.ParamByName('DDATEE').AsDate:=trunc(Date);
      quPost1.Active:=True;
      Vi1.EndUpdate;
    end;

    fmAddDoc8.Show;
  end;
end;

procedure predztoday(i,j,icli:integer);
 var iMax:integer;
     cenaformat:real;
   //  NameCli:string;
begin
  with dmP do
  with fmAddDoc8 do
  begin
    taSpecVoz.Last;
    iMax:=taSpecVozNumber.AsInteger+1;
    taSpecVoz.Append;
    taSpecVozNumber.AsInteger:=iMax;

    if i=0 then              //���������� ���������� �� ����� ������ ����������� ������� ��� ������� �� �������
    begin
      quNamePost.Active:=false;
      quNamePost.ParamByName('icli').AsInteger:=icli;
      quNamePost.Active:=True;
      if quNamePost.RecordCount>0 then
      begin
        taSpecVozidcli.AsInteger:=icli;
        taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
      end
      else
      begin
        taSpecVozidcli.AsInteger:=quPost2idcli.AsInteger;
        taSpecVozNamecli.AsString:=quPost2NamePost.AsString;
      end;

      taSpecVozCode.AsInteger:=quPost2Code.AsInteger;
      taSpecVozNameCode.AsString:=quPost2NameCode.AsString;

      taSpecVozDepart.AsInteger:=quPost2Depart.AsInteger;
      taSpecVozDepartName.AsString:=quPost2NameDep.AsString;

      if quPost2kol.AsFloat>0 then
      cenaformat:=((quPost2SumPost.AsFloat-quPost2NDSSum.AsFloat)/quPost2kol.AsFloat)
      else cenaformat:=0;
      taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPost2PriceIn.AsFloat;
      taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPost2Rprice.AsFloat;
      taSpecVozRPrice.AsFloat:= strtofloat(FormatFloat('0.00', cenaformat));
    end;

    if (i=1) or (i=2) then              //��������� ��������� ������ ��� ��������� "������������ �����" ������� ��� ������� �� �������
    begin
      taSpecVozCode.AsInteger:=quPostVozCode.AsInteger;
      taSpecVozNameCode.AsString:=quPostVozNameCode.AsString;
      if i=2 then
      begin
        dmP.quCliNotVoz.Active:=false;
        dmP.quCliNotVoz.Active:=true;

        taSpecVozidcli.AsInteger:=dmP.quCliNotVozidcli.AsInteger;
        taSpecVozNamecli.AsString:=dmP.quCliNotVozFullName.AsString;
      end
      else
      begin
        quNamePost.Active:=false;
        quNamePost.ParamByName('icli').AsInteger:=icli;
        quNamePost.Active:=True;
        if quNamePost.RecordCount>0 then
        begin
          taSpecVozidcli.AsInteger:=icli;
          taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
        end
        else
        begin
          taSpecVozidcli.AsInteger:=quPostVozidcli.AsInteger;
          taSpecVozNamecli.AsString:=quPostVozNamecli.AsString;
        end;
      end;

      taSpecVozDepart.AsInteger:=quPostVozDepart.AsInteger;
      taSpecVozDepartName.AsString:=quPostVozDepartName.AsString;

      if quPostVozkol.AsFloat>0 then
      cenaformat:=(quPostVozSumPost.AsFloat-quPostVozNDSSum.AsFloat)/quPostVozkol.AsFloat
      else cenaformat:=0;
      taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));

      cenaformat:=quPostVozPriceIn.AsFloat;
      taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
      cenaformat:=quPostVozRprice.AsFloat;
      taSpecVozRPrice.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
    end;

    if i=3 then             //�� ������� ������ �� ������
    begin
      taSpecVozCode.AsInteger:=dmMC.quCardsID.AsInteger;
      taSpecVozNameCode.AsString:=dmMC.quCardsName.AsString;

      if dmP.quCliNotVoz.Active=false then dmP.quCliNotVoz.Active:=true;
      if dmP.quDepVoz.Active=false then dmP.quDepVoz.Active:=true;
      dmP.quDepVoz.First;

      taSpecVozidcli.AsInteger:=dmP.quCliNotVozidcli.AsInteger;
      taSpecVozNamecli.AsString:=dmP.quCliNotVozFullName.AsString;

      taSpecVozDepart.AsInteger:=dmP.quDepVozID.AsInteger;
      taSpecVozDepartName.AsString:=dmP.quDepVozName.AsString;
      taSpecVozPriceIn0.AsFloat:=0;
      taSpecVozPriceIn.AsFloat:=0;
      taSpecVozRPrice.AsFloat:=0;
      taSpecVozQuant.AsFloat:=1;
      taSpecVozQuantOut.AsFloat:=0;
      taSpecVozQRem.AsFloat:=1;
      taSpecVozRSumIn0.AsFloat:=0;
      taSpecVozRSumIn.AsFloat:=0;
      taSpecVozRSumNDS.AsFloat:=0;
   //   taSpecVozDateVoz.AsInteger:=trunc(Date);
    end;

    if j=0 then               //���� �� �� �������
    begin
      if dmMC.quCardsTovarType.AsInteger=0 then taSpecVozQuant.AsFloat:=1          //����� ������� ��� �������
      else taSpecVozQuant.AsFloat:=dmMC.quCardsWeght.AsFloat;
    end
    else  taSpecVozQuant.AsFloat:=1;

    taSpecVozQuantOut.AsFloat:=0;
    taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;

    
    cenaformat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
    cenaformat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
    cenaformat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumNDS.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));


    taSpecVozDateVoz.AsInteger:=trunc(date);                                    //���� �������� ������ �������


    if dmP.quReasV.Active=false then dmP.quReasV.Active:=true;
    if quReasV.RecordCount>0 then
    begin
      quReasV.Locate('id',6,[]);
      taSpecVozReason.AsInteger:=quReasVid.AsInteger;
    end;

    taSpecVoz.Post;

    ViewDoc8.Controller.FocusRecord(ViewDoc8.DataController.FocusedRowIndex,True);
    griddoc8.SetFocus;
    ViewDoc8Quant.Focused:=true;
    viewdoc8quant.Selected:=true;
    ViewDoc8.Controller.SelectAllColumns;
    ViewDoc8Quant.Options.Editing:=True;
    ViewDoc8Reason.Options.Editing:=true;
    fmAddDoc8.Show;
  end;
end;


procedure TfmAddDoc8.prButtons(bSt:Boolean);
begin
  cxButton2.Enabled:=bSt;
end;


procedure TfmAddDoc8.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridDoc8.Align:=alClient;
  ViewDoc8.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmAddDoc8.cxLabel1Click(Sender: TObject);
  var i:integer;
begin
   i:=0;
   ViewDoc8.BeginUpdate;

   taSpecVoz.First;
   while not taSpecVoz.Eof do
   begin
     if taSpecVozReason.AsInteger=6 then
     begin
       i:=1;
       break;
     end;
     taSpecVoz.Next;
   end;
   ViewDoc8.EndUpdate;
   if i=1 then
   begin
     showmessage('��������� ������� ��������!');
     iCol:=4;
     griddoc8.SetFocus;
     ViewDoc8Reason.Focused:=true;
     viewdoc8Reason.Selected:=true;
     ViewDoc8Reason.Options.Editing:=true;
     exit;
   end;
   fmAddDoc8.acAddList.Execute;
   bDrVoz:=True;
end;

procedure TfmAddDoc8.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc8.ViewDoc8Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8Quant' then iCol:=1;
  if ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8quantout' then iCol:=2;
  if ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8DepartName' then iCol:=3;
  if ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8Reason' then iCol:=4;
end;

procedure TfmAddDoc8.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  cxTextEdit2.SetFocus;
  if dmP.quReasV.Active=false then dmP.quReasV.Active:=true;
  if dmP.quSpecVoz.Active=false then dmP.quSpecVoz.Active:=true;
  if dmP.quDepVoz.Active=false then dmP.quDepVoz.Active:=true;
  if dmP.quReasV.Active=false then dmP.quReasV.Active:=true;

end;

procedure TfmAddDoc8.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc8.acAddPosExecute(Sender: TObject);
//Var iMax:Integer;
begin
{  //�������� �������
  iMax:=1;
  ViewDoc8.BeginUpdate;

  taSpecVoz.First;
  if not taSpecVoz.Eof then
  begin
    taSpecVoz.Last;
    iMax:=taSpecVozNumber.AsInteger+1;
  end;

  iCol:=0;

  taSpecVoz.Append;

  taSpecVozNumber.AsInteger:=iMax;
  taSpecVozCode.AsInteger:=0;
  taSpecVozNameCode.AsString:='';
  taSpecVozidcli.AsInteger:=0;
  taSpecVozNamecli.AsString:='';
  taSpecVozDepart.AsInteger:=0;
  taSpecVozDepartName.AsString:='';
  taSpecVozQuant.AsFloat:=1;
  taSpecVozQuantOut.AsFloat:=0;
  taSpecVozPriceIn0.AsFloat:=0;
  taSpecVozPriceIn.AsFloat:=0;
  taSpecVozRPrice.AsFloat:=0;
  taSpecVozRSumIn0.AsFloat:=0;
  taSpecVozRSumIn.AsFloat:=0;
  taSpecVozRSumNDS.AsFloat:=0;

  taSpecVoz.Post;

  ViewDoc8.EndUpdate;

  griddoc8.SetFocus;
  ViewDoc8Quant.Focused:=true;
  viewdoc8quant.Selected:=true;;
  ViewDoc8.Controller.SelectAllColumns;
  ViewDoc8Quant.Options.Editing:=True;         }

end;

procedure TfmAddDoc8.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecVoz.RecordCount>1 then
  begin
    taSpecVoz.Delete;

    if taSpecVozidcli.AsInteger=2471 then
    begin
      Vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      Vi1.EndUpdate;

    end
    else
    begin

      vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      dmP.quPost1.ParamByName('IDCARD').AsInteger:=taSpecVozCode.AsInteger;
      dmP.quPost1.ParamByName('DDATEB').AsDate:=Date-730;
      dmP.quPost1.ParamByName('DDATEE').AsDate:=Date;
      dmP.quPost1.Active:=True;
      vi1.EndUpdate;
    end;

    fmAddDoc8.griddoc8.SetFocus;
    fmAddDoc8.ViewDoc8.Controller.FocusRecord(ViewDoc8.DataController.FocusedRowIndex,True);
    fmAddDoc8.ViewDoc8Quant.Focused:=true;
    fmAddDoc8.viewdoc8quant.Selected:=true;
    ViewDoc8.Controller.SelectAllColumns;

  end
  else
  begin
    if taSpecVoz.RecordCount=1 then
    begin
      taSpecVoz.Delete;
      vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      vi1.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc8.acAddListExecute(Sender: TObject);
begin
  iDirect:=25;
  fmCards.Show;
end;

procedure TfmAddDoc8.cxButton1Click(Sender: TObject);
Var IdH,t,user:string;
    iNum,i:INteger;

begin
  //��������
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  cxButton8.Enabled:=False;

  if MessageDlg('��������� ���������� � �����?',mtConfirmation,[mbYes, mbNo], 0, mbNo)=3 then
  begin
    if taspecvoz.RecordCount>0 then
    begin

      i:=0;
      ViewDoc8.BeginUpdate;
      taSpecVoz.First;
      while not taSpecVoz.Eof do
      begin
        if taSpecVozReason.AsInteger=6 then
        begin
          i:=1;
          break;
        end;
        taSpecVoz.Next;
      end;
      ViewDoc8.EndUpdate;
      if i=1 then
      begin
        showmessage('��������� ������� ��������!');
        iCol:=4;
        griddoc8.SetFocus;
        ViewDoc8Reason.Focused:=true;
        viewdoc8Reason.Selected:=true;
        ViewDoc8Reason.Options.Editing:=True;
        cxButton1.Enabled:=true;
        cxButton2.Enabled:=true;
        cxButton8.Enabled:=true;
        exit;
      end;

      if taspecvoz.State in [dsEdit,dsInsert] then taspecvoz.Post;

      IdH:=cxTextEdit1.Text;

      if cxTextEdit1.Tag=0 then
      begin
        with dmMC do
        begin
          //�������� ������������
          quC.Active:=False;
          quC.SQL.Clear;
          quC.SQL.Add('Select Count(*) as RecCount from "A_DocHRet"');
          quC.SQL.Add('where DocNum='+cxTextEdit1.Text);
          quC.Active:=True;

          if quCRecCount.AsInteger>0 then
          begin
            

            showmessage('�������� � ����� ������� ��� ����. ���������� ����������.');
            showmessage('������� ���������, ����� �������� ���������� �� ����. ���������� �������');
            cxButton1.Enabled:=True;
            cxButton2.Enabled:=True;
            cxButton8.Enabled:=True;
            iNum:=StrToINt(cxTextEdit1.Text)+1;
            cxTextEdit1.Text:=inttostr(iNum);
            exit;
          end;
          ViewDoc8.BeginUpdate;
          taSpecVoz.First;
          while not taSpecVoz.Eof do
          begin
            if (taSpecVozquant.AsFloat<0.001) or (taSpecVozqrem.AsFloat<0.001) then
            begin
              showmessage('���� ������� ��� ���-�� �� ������� ��� ���-�� �������� ������� <0.001 , ���� ���������!');
              cxButton1.Enabled:=true;
              cxButton2.Enabled:=true;
              cxButton8.Enabled:=true;
              ViewDoc8.EndUpdate;
              exit;
            end;
            if ((taSpecVozPriceIn.AsFloat<0.01) or (taSpecVozRSumIn.AsFloat<0.01)) and (taSpecVozidcli.AsInteger<>2471) then
            begin
              showmessage('���� ������� ��� ���� ������ ��� ����� ������ <0.01 , ���� ���������!');
              cxButton1.Enabled:=true;
              cxButton2.Enabled:=true;
              cxButton8.Enabled:=true;
              ViewDoc8.EndUpdate;
              exit;
            end;

            taSpecVoz.Next;
          end;
          ViewDoc8.EndUpdate;

          user:=Person.Name;
          t:=ts(now());
        //  try                           //�������� ���������
            quA.Active:=False;
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "A_DocHRet" values (');
            quA.SQL.Add(IDH+',');
            quA.SQL.Add(''+cxTextEdit1.Text+',');         //DocNum
            quA.SQL.Add(its(Trunc(cxDateEdit1.Date))+',');    //DocDate
            quA.SQL.Add(''''+t+''',');                        //DocTime
            quA.SQL.Add(''''+cxTextEdit2.Text+''',');         //Comment
            quA.SQL.Add(''''+user+''','+'''0'''+')');                     //User   �������
            //quA.SQL.SaveToFile('C:\1.txt');
            quA.ExecSQL;

            //�������� ������������
            taSpecVoz.First;
            while not taSpecVoz.Eof do
            begin
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('INSERT into "A_DocSRet" values (');
              quE.SQL.Add(IDH+','''',');
              quE.SQL.Add(its(taSpecVozCode.AsInteger)+',');
              quE.SQL.Add(its(taSpecVozDepart.AsInteger)+',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozquant.AsFloat,ffFixed,10,3)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozPriceIn0.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozPriceIn.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozRSumIn0.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozRSumIn.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozRSumNDS.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozRPrice.AsFloat,ffFixed,10,2)+''',');
              quE.SQL.Add(its(taSpecVozidcli.AsInteger)+',');
              quE.SQL.Add(its(taSpecVozReason.AsInteger)+',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozquantout.AsFloat,ffFixed,10,3)+''',');
              quE.SQL.Add('''0'''+',');
              quE.SQL.Add(''''+FloatToStrF(taSpecVozquant.AsFloat,ffFixed,10,3)+''','); //qrem
              quE.SQL.Add(its(taSpecVozdatevoz.AsInteger)+',');
              quE.SQL.Add(its(Trunc(cxDateEdit1.Date))+')');    //DocDate
           //   quE.SQL.SaveToFile('C:\1.txt');
              quE.ExecSQL;
              taSpecVoz.Next;
            end;
     //     finally
           { iNum:=StrToINt(cxTextEdit1.Text);
            quC.Active:=false;
            quC.SQL.Clear;
            quC.SQL.Add('Select Count(*) as RecCount from "IdPool"');
            quC.SQL.Add('where CodeObject=25');
            quC.Active:=true;
            quC.Active:=false;
            if quCRecCount.AsInteger<1 then
            begin
             quE.Active:=false;
             quE.SQL.Clear;
             quE.SQL.Add('Insert into "IdPool" values (25,'+its(iNum)+')');
             quE.ExecSQL;
            end ;         }
            {else
            begin
              quE.SQL.Clear;
              quE.SQL.Add('Update "IdPool" Set ID='+its(iNum)+'');
              quE.SQL.Add(' where CodeObject=25');
              quE.ExecSQL;
            end;     }
       //   end;
        end;
        if fmDocs8.Showing then fmDocs8.Close;
        fmAddDoc8.Close;
        fmDocs8.Show;

      end else
      begin
        showmessage('������ ���������');
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
        cxButton8.Enabled:=True;
      end;
    end
    else
    begin
      cxButton1.Enabled:=True;
      cxButton2.Enabled:=True;
      cxButton8.Enabled:=True;
      showmessage('��� ���������� ��� ����������');
    end;
  end
  else
  begin
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton8.Enabled:=True;
  end;
end;

procedure TfmAddDoc8.acDelAllExecute(Sender: TObject);
begin

  if taSpecVoz.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation,[mbYes, mbNo], 0, mbNo)=3 then
    begin
      delay(10);
      taSpecVoz.First;
      while not taSpecVoz.Eof do
      begin
        taSpecVoz.Delete;
      end;
      vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      vi1.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc8.cxLabel6Click(Sender: TObject);
begin
  fmAddDoc8.acDelAll.Execute;
end;

procedure TfmAddDoc8.cxButton2Click(Sender: TObject);
begin
// Close;
  if cxButton1.Enabled then
  begin
    cxButton1.Enabled:=False;
    cxButton2.Enabled:=False;
    cxButton8.Enabled:=False;
    if MessageDlg('����� �� ��������?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      dmP.quSpecVoz.Active:=false;
      dmP.quPostVoz.Active:=false;
      dmP.quDepVoz.Active:=false;
      vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      vi1.EndUpdate;
      ViewDoc8.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
      fmdocs8.speeditem2.Enabled:=true;
      fmdocs8.speeditem3.Enabled:=true;
      fmdocs8.speeditem5.Enabled:=true;
      fmdocs8.speeditem10.Enabled:=true;
      fmAddDoc8.Close;
    end
    else
    begin
      cxButton1.Enabled:=True;
      cxButton2.Enabled:=True;
      cxButton8.Enabled:=True;
    end;
  end
  else
  begin
    cxButton2.Enabled:=False;
    if MessageDlg('������� ������������?',mtConfirmation, [mbYes, mbNo], 0, mbNo)=3 then
    begin
      dmP.quSpecVoz.Active:=false;
      dmP.quPostVoz.Active:=false;
      dmP.quDepVoz.Active:=false;
      vi1.BeginUpdate;
      dmP.quPost1.Active:=False;
      vi1.EndUpdate;
      ViewDoc8.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
      fmdocs8.speeditem2.Enabled:=true;
      fmdocs8.speeditem3.Enabled:=true;
      fmdocs8.speeditem5.Enabled:=true;
      fmdocs8.speeditem10.Enabled:=true;
      fmAddDoc8.close;
    end
    else     cxButton2.Enabled:=True;
  end;
end;

procedure TfmAddDoc8.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc8.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc8.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmAddDoc8.acReadBar1Execute(Sender: TObject);
Var sBar,sBarVes:String;
    iC,flag,fl,icli,j:INteger;
begin
  j:=0;
  if taSpecVoz.RecordCount>0 then
  begin
    ViewDoc8.BeginUpdate;
    taSpecVoz.First;
    while not taSpecVoz.Eof do
    begin
      if taSpecVozReason.AsInteger=6 then
      begin
        j:=1;
        break;
      end;
      taSpecVoz.Next;
    end;
    ViewDoc8.EndUpdate;
    if j=1 then
    begin
      showmessage('��������� ������� ��������!');
      iCol:=4;
      griddoc8.SetFocus;
      ViewDoc8Reason.Focused:=true;
      viewdoc8Reason.Selected:=true;
      exit;
    end;
  end;

  Edit1.SetFocus;
  Edit1.SelectAll;
  flag:=0;
  sBar:=Edit1.Text;
  if sBar='' then exit;
  if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then        //�������
  begin
    if length(sBar)>=12 then
    begin
      sBarVes:=copy(sBar,8,5);
      flag:=1;
    end;
    sBar:=copy(sBar,1,7);
  end;

  if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);

  if prFindBarC(sBar,0,iC) then
  begin

    with fmAddDoc8 do
    with dmP do
    begin
      quPostVoz.Active:=False;
      quPostVoz.ParamByName('IDCARD').Value:=iC;
      quPostVoz.Active:=True;
      icli:=0;
      if (quPostVoz.RecordCount>0) then
      begin
        quPostVoz.Last;
        if (quPostVozIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
        begin
          fl:=1;
                                           //���� ���� ��������� �����������
          quTypeCli.Active:=false;
          quTypeCli.ParamByName('ICLI').AsInteger:=quPostVozIDALTCLI.AsInteger;
          quTypeCli.Active:=True;
          if quTypeCli.RecordCount>0 then         //���� �� ���������� ���� ������ (����������, ������������)
          begin
            if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPostVozIDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
            else icli:=0;                          //���� ��� �� ��������
          end
          else icli:=0;                            //��� ������, ����� ���� ��������
        end
        else
        begin
          fl:=0;                                 //���� ��� ��������� ������������
          icli:=quPostVozidcli.AsInteger;        //����������� ����. ����������
        end;


        if (quPostVozTypeVoz.AsInteger=4) then
        begin
          showmessage('���������� �� ������ ����� ������� ������, �� �������� ������ � ���������!');
          exit;
        end;
        if (quPostVozTypeVoz.AsInteger=3) then      //��������� ��� �������� � ������  1-�������,2-�����,3-�������������� 4-����� � ���������
        begin
          if MessageDlg('����� �� �������� "������������". ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=3  then
          begin
            predztoday(2,1,icli);
            if flag=1 then
            begin
              taSpecVoz.Edit;
              taSpecVozquant.AsFloat:=strtoint(trim(sBarVes))/1000;
              taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;
              taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
              taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
              taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
              taSpecVoz.Post;
            end;
            exit;
          end
          else  exit;
        end;            //���� ����� ����������
                       //��������� ���� � ����. ���������� ������ �������� "�� ������������",��

        if fl=0 then    //���� � ���������� ���������� ��� ���������� ���������� ������ �������� �� ����
        begin
          if (quPostVozCliTypeV.AsInteger=3) then typenevozvrat(icli,flag,sBarVes)
          else typevozvrat(icli,flag,sBarVes);                      //���� � ���������� ���� ������ ��������
        end
        else
        begin
          if icli=0 then typenevozvrat(icli,flag,sBarVes)  //���� ��� � ���������� ���������� ������������
          else typevozvrat(icli,flag,sBarVes);
        end;
      end
      else
      begin
        if MessageDlg('��� ������ �� ������ ��� ��������. ������� ���������� �� ���������� "������������ �����"?',mtConfirmation,[mbYes, mbNo], 0)=3
        then
        begin
          predztoday(3,1,icli);
          exit;
        end;
      end;
    end;
  end else StatusBar1.Panels[0].Text:='�������� � �� '+sBar+' ���.';
end;

procedure TfmAddDoc8.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
  begin
    cxDateEdit1.SetFocus;
  end;
end;


procedure TfmAddDoc8.acPostTovExecute(Sender: TObject);
begin
  if cxbutton1.Enabled=false then exit;
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmP do
  begin
    if taSpecVoz.RecordCount>0 then
    begin
      if taSpecVozidcli.AsInteger=2471 then exit;

      CommonSet.DateBeg:=Date-730;
      CommonSet.DateEnd:=Date;
      fmP.VP.BeginUpdate;
      quPost2.Active:=False;
      quPost2.ParamByName('IDCARD').AsInteger:=taSpecVozCode.AsInteger;
      quPost2.ParamByName('DDATEB').AsDate:=CommonSet.DateBeg;
      quPost2.ParamByName('DDATEE').AsDate:=CommonSet.DateEnd;
      quPost2.Active:=True;
      quPost2.First;

      fmP.VP.EndUpdate;

      if (quPost2.RecordCount<1) then
      begin
        showmessage('��� ����������� �� �������� ��������');
        exit;
      end;  
      fmP.Caption:=taSpecVozNameCode.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',Date-730)+' �� '+FormatDateTime('dd.mm.yyyy ',Date);
      bDrVoz:=true;
      fmP.Show;
    end;
  end;
end;

procedure TfmAddDoc8.cxButton8Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc8);
end;

procedure TfmAddDoc8.ViewDoc8SelectionChanged(Sender: TcxCustomGridTableView);
begin
  if (ViewDoc8.Controller.SelectedRecordCount>1) or (cxButton1.Enabled=false) then exit;
  with dmP do
  begin
    if taSpecVozidcli.AsInteger=2471 then
    begin
      Vi1.BeginUpdate;
      quPost1.Active:=False;
      Vi1.EndUpdate;
      exit;
    end;

    Vi1.BeginUpdate;
    quPost1.Active:=False;
    quPost1.ParamByName('IDCARD').AsInteger:=taSpecVozCode.AsInteger;
    quPost1.ParamByName('DDATEB').AsDate:=Date-730;
    quPost1.ParamByName('DDATEE').AsDate:=Date;
    quPost1.Active:=True;
    Vi1.EndUpdate;

  end;
end;

procedure TfmAddDoc8.taSpecVozquantChange(Sender: TField);
begin
  if (iCol=1) then
  begin
    taSpecVoz.Edit;
  {  if (taSpecVozQuantOut.AsFloat>taSpecVozQuant.AsFloat) then
    taSpecVozQuant.AsFloat:=taSpecVozQuantOut.AsFloat;
   }
    taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
    taSpecVozQRem.AsFloat:=taSpecVozQuant.AsFloat;//-taSpecVozQuantOut.AsFloat;
    taSpecVoz.Post;
    taSpecVoz.Edit;
    iCol:=4;
    ViewDoc8Reason.Focused:=true;
    viewdoc8Reason.Selected:=true;
    ViewDoc8Reason.Options.Editing:=True;
  end;
end;

procedure TfmAddDoc8.taSpecVozPriceIn0Change(Sender: TField);
begin
  taSpecVoz.Edit;
  taSpecVozRSumIn0.AsFloat:=(taSpecVozPriceIn0.AsFloat*taSpecVozQuant.AsFloat);
  taSpecVoz.Post;
  taSpecVoz.Edit;
end;

procedure TfmAddDoc8.taSpecVozPriceInChange(Sender: TField);
begin
  taSpecVoz.Edit;
  taSpecVozRSumIn.AsFloat:=(taSpecVozPriceIn.AsFloat*taSpecVozQuant.AsFloat);
  taSpecVoz.Post;
  taSpecVoz.Edit;
end;

procedure TfmAddDoc8.taSpecVozRPriceChange(Sender: TField);
begin
  taSpecVoz.Edit;
  taSpecVozRSumNDS.AsFloat:=(taSpecVozRPrice.AsFloat*taSpecVozQuant.AsFloat);
  taSpecVoz.Post;
  taSpecVoz.Edit;
end;

procedure TfmAddDoc8.ViewDoc8CellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if cxButton1.Enabled then
  begin
    if (ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8Quant') then
    begin
      iCol:=1;
      ViewDoc8Quant.Options.Editing:=True;
      ViewDoc8Quant.Focused:=true;
    end
    else
    begin
      if (ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8DepartName') then
      begin
       iCol:=3;
       ViewDoc8DepartName.Focused:=true;
       ViewDoc8DepartName.Options.Editing:=True;
      end
      else
      begin
        if (ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8Reason') then
        begin
          iCol:=4;
          ViewDoc8Reason.Focused:=true;
          ViewDoc8Reason.Options.Editing:=True;
        end;
      end;
    end;

  end;
end;

procedure TfmAddDoc8.ViewDoc8CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin
 if AViewInfo.GridRecord.Selected then
 begin
    ACanvas.Canvas.Brush.Color := $00FFB1A4;
    if AViewInfo.Focused=true then
    begin
      ACanvas.Canvas.Brush.Color := clwhite;
      ACanvas.Canvas.Font.Color := clblack;
    end;
  end;
end;

procedure TfmAddDoc8.Vi1CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);

  var cenaformat:real;
      icli, fl:integer;
begin
  if cxButton1.Enabled then
  begin
    with dmP do
    begin

      if (quPost1.RecordCount>0) and (taSpecVoz.RecordCount>0) then
      begin

        if (quPostVozIDALTCLI.AsInteger>0) then //��������� ���� �� ��������� ���������
        begin

          fl:=1;
          quTypeCli.Active:=false;
          quTypeCli.ParamByName('ICLI').AsInteger:=quPost1IDALTCLI.AsInteger;
          quTypeCli.Active:=True;
          if quTypeCli.RecordCount>0 then         //���� �� ���������� ���� ������ (����������, ������������)
          begin
            if  (quTypeCliCliTypeV.AsInteger<3) then icli:=quPost1IDALTCLI.AsInteger //��������� ���� ���������� �� ����� ���������� ����������
            else icli:=0;                          //���� ��� �� ��������
          end
          else icli:=0;                            //��� ������, ����� ���� ��������
        end
        else
        begin
          fl:=0;
          icli:=quPost1idcli.AsInteger;        //����������� ����. ����������
        end;

        ViewDoc8.BeginUpdate;
        taSpecVoz.Edit;

        if fl=0 then
        begin
          taSpecVozidcli.AsInteger:=quPost1idcli.AsInteger;
          taSpecVozNamecli.AsString:=quPost1NamePost.AsString;
        end
        else
        begin
          quNamePost.Active:=false;
          quNamePost.ParamByName('icli').AsInteger:=icli;
          quNamePost.Active:=True;

          if quNamePost.RecordCount>0 then
          begin
            taSpecVozidcli.AsInteger:=icli;
            taSpecVozNamecli.AsString:=quNamePostFNamePost.AsString;
          end;
        end;

        taSpecVozDepart.AsInteger:=quPost1Depart.AsInteger;
        taSpecVozDepartName.AsString:=quPost1NameDep.AsString;

        cenaformat:=((quPost1SumPost.AsFloat-quPost1NDSSum.AsFloat)/quPost1kol.AsFloat);
        taSpecVozPriceIn0.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
        cenaformat:=quPost1PriceIn.AsFloat;
        taSpecVozPriceIn.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
        cenaformat:=quPost1Rprice.AsFloat;
        taSpecVozRPrice.AsFloat:=strtofloat(FormatFloat('0.00', cenaformat));
        taSpecVoz.Post;
        ViewDoc8.EndUpdate;
      end
      else exit;
    end;
  end;
end;

procedure TfmAddDoc8.taSpecVozDepartNameChange(Sender: TField);
begin
  if (iCol=3) then
  begin
    if dmP.quDepVoz.Active=false then dmP.quDepVoz.Active:=true;
    dmP.quDepVoz.Locate('Name',taSpecVozDepartName.AsString,[]);
    taSpecVoz.Edit;
    taSpecVozDepart.AsInteger:=dmP.quDepVozID.AsInteger;
    taSpecVoz.Post;
    taSpecVoz.Edit;
  end;
end;

procedure TfmAddDoc8.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with fmDocs8 do
  begin
    speeditem2.Enabled:=true;
    speeditem3.Enabled:=true;
    speeditem5.Enabled:=true;
    speeditem10.Enabled:=true;
  end;
  ViewDoc8.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmAddDoc8.taSpecVozCalcFields(DataSet: TDataSet);
begin
  with dmP do
  begin
    if cxbutton1.Enabled=false then taSpecVozQAllRem.AsFloat:=quSpecVozQALLREM.AsFloat
    else
    begin
      quAllRemCard.Active:=false;
      quAllRemCard.ParamByName('CODE').AsInteger:= taSpecVozCode.AsInteger;
      quAllRemCard.Active:=true;
      taSpecVozQAllRem.AsFloat:=quAllRemCardQALLREM.AsFloat;
      quAllRemCard.Active:=false;
    end;
  end;
end;

procedure TfmAddDoc8.ViewDoc8EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if ViewDoc8.Controller.FocusedColumn.Name='ViewDoc8Quant' then
  begin
    if (Key=$0D) then
    begin
      taSpecVoz.Edit;
      taSpecVoz.Post;
      iCol:=4;

      taSpecVoz.Edit;
      ViewDoc8Reason.Focused:=true;
      viewdoc8Reason.Selected:=true;
      ViewDoc8Reason.Options.Editing:=True;
    end;
  end;
end;

end.
