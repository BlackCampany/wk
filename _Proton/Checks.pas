unit Checks;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan, Menus;

type
  TfmChecks = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridChecks: TcxGrid;
    LevelChecks: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    SpeedItem4: TSpeedItem;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    ViewChecks: TcxGridDBTableView;
    amChecks: TActionManager;
    acMoveRepOb: TAction;
    ViewChecksQuant: TcxGridDBColumn;
    ViewChecksOperation: TcxGridDBColumn;
    ViewChecksDateOperation: TcxGridDBColumn;
    ViewChecksPrice: TcxGridDBColumn;
    ViewChecksStore: TcxGridDBColumn;
    ViewChecksCk_Number: TcxGridDBColumn;
    ViewChecksCk_Curs: TcxGridDBColumn;
    ViewChecksCk_CurAbr: TcxGridDBColumn;
    ViewChecksCk_Disg: TcxGridDBColumn;
    ViewChecksCk_Disc: TcxGridDBColumn;
    ViewChecksQuant_S: TcxGridDBColumn;
    ViewChecksGrCode: TcxGridDBColumn;
    ViewChecksCode: TcxGridDBColumn;
    ViewChecksCassir: TcxGridDBColumn;
    ViewChecksCash_Code: TcxGridDBColumn;
    ViewChecksCk_Card: TcxGridDBColumn;
    ViewChecksContr_Code: TcxGridDBColumn;
    ViewChecksContr_Cost: TcxGridDBColumn;
    ViewChecksSeria: TcxGridDBColumn;
    ViewChecksBestB: TcxGridDBColumn;
    ViewChecksNDSx1: TcxGridDBColumn;
    ViewChecksNDSx2: TcxGridDBColumn;
    ViewChecksTimes: TcxGridDBColumn;
    ViewChecksSumma: TcxGridDBColumn;
    ViewChecksSumNDS: TcxGridDBColumn;
    ViewChecksSumNSP: TcxGridDBColumn;
    ViewChecksPriceNSP: TcxGridDBColumn;
    ViewChecksNSmena: TcxGridDBColumn;
    ViewChecksName: TcxGridDBColumn;
    acCalcSumCheck: TAction;
    SpeedItem6: TSpeedItem;
    PopupMenu1: TPopupMenu;
    EditOn: TMenuItem;
    SpeedItem7: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure acCalcSumCheckExecute(Sender: TObject);
    procedure EditOnClick(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmChecks: TfmChecks;

implementation

uses Un1, MDB, MT, Move, CashRep, CheckEdit;

{$R *.dfm}

procedure TfmChecks.FormCreate(Sender: TObject);
begin
  GridChecks.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewChecks.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmChecks.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewChecks.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  dmMT.ptCqRep.Active:=False;
end;

procedure TfmChecks.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmChecks.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmChecks.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewChecks);
end;

procedure TfmChecks.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepChecks') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmChecks.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmChecks.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmChecks.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewChecks.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=ptCqRepCode.AsInteger;
      prFillMove(ptCqRepCode.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=ptCqRepName.AsString+'('+ptCqRepCode.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmChecks.acCalcSumCheckExecute(Sender: TObject);
Var iCh,iZ,iNc:Integer;
    rSum:Real;
    iC:Integer;
begin
  if MessageDlg('����������� ����� �����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    with dmMt do
    begin
      try
        ViewChecks.BeginUpdate;
        dsptCqRep.DataSet:=nil;
        ptCqRep.First;
        iC:=0;
        iCh:=0;iZ:=0;iNc:=0; rSum:=0;
        while not ptCqRep.Eof do
        begin
          if (iCh<>ptCqRepCk_Number.AsInteger) or (iZ<>ptCqRepNSmena.AsInteger) or (iNc<>ptCqRepCash_Code.AsInteger) then
          begin  //�������� ���
            if (iCh+iZ+iNc)>0 then //�� ��������� ��������
            begin
              //����������� ����� - ���������� �����
              ptCqRep.Prior; // ��� �����
              while (iCh=ptCqRepCk_Number.AsInteger)
                    and(iZ=ptCqRepNSmena.AsInteger)
                    and(iNc=ptCqRepCash_Code.AsInteger)
                    and(ptCqRep.Bof=False) do
              begin
                ptCqRep.Edit;
                ptCqRepSumNSP.AsFloat:=rSum;
                ptCqRep.Post;

                ptCqRep.Prior;
              end;
              //����� ��� ����� ������ �� ����� ������
              ptCqRep.Next; // ��� ������
              while (iCh=ptCqRepCk_Number.AsInteger)
                    and(iZ=ptCqRepNSmena.AsInteger)
                    and(iNc=ptCqRepCash_Code.AsInteger)
                    and(ptCqRep.Eof=False) do
              begin
                ptCqRep.Next;
              end;

              if ptCqRep.Eof=False then
              begin
                rSum:=0;
//                rSum:=ptCqRepSumma.AsFloat;
                iCh:=ptCqRepCk_Number.AsInteger;
                iZ:=ptCqRepNSmena.AsInteger;
                iNc:=ptCqRepCash_Code.AsInteger;

                iC:=iC+1;

                if iC mod 5 = 0 then
                begin
                  StatusBar1.Panels[0].Text:=its(iC);
                  delay(20);
                end;
              end;
            end else
            begin
              iCh:=ptCqRepCk_Number.AsInteger;
              iZ:=ptCqRepNSmena.AsInteger;
              iNc:=ptCqRepCash_Code.AsInteger;

              rSum:=ptCqRepSumma.AsFloat;
//              rSum:=0;

              ptCqRep.Next;
            end;
          end else
          begin
            rSum:=rSum+ptCqRepSumma.AsFloat;
            ptCqRep.Next;
          end;
        end;

      finally
        dsptCqRep.DataSet:=ptCqRep;
        ViewChecks.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmChecks.EditOnClick(Sender: TObject);
begin
  if ViewChecks.Controller.SelectedRecordCount=0 then exit;
  if (CommonSet.Single=1)or(Pos('�����',Person.Name)>0)or(Pos('OPER CB',Person.Name)>0)or(Pos('OPERZAK',Person.Name)>0) then
  begin
    with dmMT do
    begin
      try
        fmCheckEdit:=tfmCheckEdit.Create(Application);
        fmCheckEdit.cxSpinEdit1.Value:=ptCqRepCash_Code.AsInteger;
        fmCheckEdit.cxSpinEdit2.Value:=ptCqRepNSmena.AsInteger;
        fmCheckEdit.cxSpinEdit3.Value:=ptCqRepCk_Number.AsInteger;
        fmCheckEdit.cxSpinEdit4.Value:=ptCqRepCassir.AsInteger;
        fmCheckEdit.cxSpinEdit5.Value:=ptCqRepCode.AsInteger;
        fmCheckEdit.cxSpinEdit6.Value:=ptCqRepGrCode.AsInteger;
        fmCheckEdit.cxCalcEdit1.Value:=ptCqRepQuant.AsFloat;
        fmCheckEdit.cxCalcEdit2.Value:=ptCqRepPrice.AsFloat;
        fmCheckEdit.cxCalcEdit3.Value:=ptCqRepCk_Disc.AsFloat;
        fmCheckEdit.cxCalcEdit4.Value:=ptCqRepSumma.AsFloat;
        fmCheckEdit.cxTextEdit1.Text:=ptCqRepOperation.AsString;
        fmCheckEdit.cxSpinEdit7.Value:=ptCqRepCk_Card.AsInteger;
        fmCheckEdit.ShowModal;
        if fmCheckEdit.ModalResult=mrOk then
        begin
          delay(10);
          Memo1.Clear;
          Memo1.Lines.Add('����� ���� ����������...'); delay(10);
          ViewChecks.BeginUpdate;
          ptCQRep.Edit;
          ptCqRepCode.AsInteger:=fmCheckEdit.cxSpinEdit5.Value;
          ptCqRepGrCode.AsInteger:=fmCheckEdit.cxSpinEdit6.Value;
          ptCqRepQuant.AsFloat:=fmCheckEdit.cxCalcEdit1.Value;
          ptCqRepPrice.AsFloat:=fmCheckEdit.cxCalcEdit2.Value;
          ptCqRepCk_Disc.AsFloat:=fmCheckEdit.cxCalcEdit3.Value;
          ptCqRepSumma.AsFloat:=fmCheckEdit.cxCalcEdit4.Value;
          ptCqRepOperation.AsString:=fmCheckEdit.cxTextEdit1.Text;
          ptCqRepCk_Card.AsInteger:=fmCheckEdit.cxSpinEdit7.Value;
          ptCQRep.Post;
          ViewChecks.EndUpdate;
          Memo1.Lines.Add('���������� ��.'); delay(10);
          ShowMessage('�� ������� ����������� ���������� �� �������..');
        end;
      finally
        fmCheckEdit.Release;
      end;
    end;
  end else showmessage('� ��� ������������ ����.');
end;

procedure TfmChecks.SpeedItem7Click(Sender: TObject);
begin
  with dmMT do
  with dmMC do
  begin
    if ptCQRep.State in [dsEdit,dsInsert] then ptCQRep.Post;
    ViewChecks.OptionsData.Editing:=False;
      //����� ��� �����������
    fmCashRep.acRecalcDeps.Execute;

  end;
end;

end.
