object fmChangeT: TfmChangeT
  Left = 449
  Top = 239
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1090#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 178
  ClientWidth = 342
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 12
    Width = 301
    Height = 37
    Alignment = taCenter
    AutoSize = False
    Caption = 'Label1'
    Transparent = True
    Layout = tlCenter
    WordWrap = True
  end
  object Label2: TLabel
    Left = 48
    Top = 76
    Width = 137
    Height = 13
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 216
    Top = 72
    Properties.AssignedValues.MinValue = True
    Properties.MaxValue = 99.000000000000000000
    Properties.UseCtrlIncrement = True
    Style.Shadow = True
    TabOrder = 0
    Width = 65
  end
  object Panel1: TPanel
    Left = 0
    Top = 116
    Width = 342
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = 16760962
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 48
      Top = 16
      Width = 101
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 204
      Top = 16
      Width = 99
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
end
