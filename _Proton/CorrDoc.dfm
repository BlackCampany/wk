object fmDocCorr: TfmDocCorr
  Left = 396
  Top = 412
  BorderStyle = bsDialog
  Caption = #1057#1091#1084#1084#1099' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
  ClientHeight = 286
  ClientWidth = 508
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 40
    Width = 99
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  end
  object Label2: TLabel
    Left = 24
    Top = 72
    Width = 125
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1090#1086#1074#1072#1088#1072' '#1089' '#1053#1044#1057' 0%'
  end
  object Label3: TLabel
    Left = 24
    Top = 160
    Width = 143
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1090#1086#1074#1072#1088#1072' '#1073#1077#1079' '#1053#1044#1057' 18%'
  end
  object Label4: TLabel
    Left = 24
    Top = 184
    Width = 84
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057' 18%'
  end
  object Label5: TLabel
    Left = 24
    Top = 104
    Width = 143
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1090#1086#1074#1072#1088#1072' '#1073#1077#1079' '#1053#1044#1057' 10%'
  end
  object Label6: TLabel
    Left = 24
    Top = 128
    Width = 84
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057' 10%'
  end
  object GroupBox1: TGroupBox
    Left = 188
    Top = 16
    Width = 137
    Height = 201
    Caption = #1055#1086' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
    TabOrder = 0
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 16
      Top = 20
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 0
      Width = 101
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 16
      Top = 52
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 101
    end
    object cxCurrencyEdit3: TcxCurrencyEdit
      Left = 16
      Top = 84
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 2
      Width = 101
    end
    object cxCurrencyEdit4: TcxCurrencyEdit
      Left = 16
      Top = 108
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 3
      Width = 101
    end
    object cxCurrencyEdit5: TcxCurrencyEdit
      Left = 16
      Top = 140
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 4
      Width = 101
    end
    object cxCurrencyEdit6: TcxCurrencyEdit
      Left = 16
      Top = 164
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 5
      Width = 101
    end
  end
  object GroupBox2: TGroupBox
    Left = 336
    Top = 16
    Width = 137
    Height = 201
    Caption = #1055#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
    TabOrder = 1
    object cxCurrencyEdit7: TcxCurrencyEdit
      Left = 16
      Top = 20
      TabStop = False
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 0
      Width = 101
    end
    object cxCurrencyEdit8: TcxCurrencyEdit
      Left = 16
      Top = 52
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 101
    end
    object cxCurrencyEdit9: TcxCurrencyEdit
      Left = 16
      Top = 84
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 2
      Width = 101
    end
    object cxCurrencyEdit10: TcxCurrencyEdit
      Left = 16
      Top = 108
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 3
      Width = 101
    end
    object cxCurrencyEdit11: TcxCurrencyEdit
      Left = 16
      Top = 140
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 4
      Width = 101
    end
    object cxCurrencyEdit12: TcxCurrencyEdit
      Left = 16
      Top = 164
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00;-0.00'
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 5
      Width = 101
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 234
    Width = 508
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 120
      Top = 12
      Width = 93
      Height = 29
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 296
      Top = 12
      Width = 93
      Height = 29
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
end
