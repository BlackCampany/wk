object fmDepSel: TfmDepSel
  Left = 552
  Top = 438
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1054#1090#1076#1077#1083
  ClientHeight = 257
  ClientWidth = 242
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 208
    Width = 242
    Height = 49
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765606
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 32
      Top = 12
      Width = 75
      Height = 25
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 144
      Top = 12
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrDeps: TcxGrid
    Left = 8
    Top = 8
    Width = 229
    Height = 192
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViDeps: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellDblClick = ViDepsCellDblClick
      DataController.DataSource = dsteDeps
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViDepsId: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Id'
        Width = 29
      end
      object ViDepsName: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Visible = False
      end
      object ViDepsNameFull: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameFull'
        Width = 186
      end
    end
    object LeDeps: TcxGridLevel
      GridView = ViDeps
    end
  end
  object teDeps: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 56
    Top = 20
    object teDepsId: TIntegerField
      FieldName = 'Id'
    end
    object teDepsName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object teDepsNameFull: TStringField
      FieldName = 'NameFull'
      Size = 100
    end
  end
  object dsteDeps: TDataSource
    DataSet = teDeps
    Left = 56
    Top = 72
  end
end
