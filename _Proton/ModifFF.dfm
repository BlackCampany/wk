object fmModifFF: TfmModifFF
  Left = 393
  Top = 141
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1052#1086#1076#1080#1092#1080#1082#1072#1090#1086#1088#1099' '#1073#1083#1102#1076#1072
  ClientHeight = 453
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 376
    Width = 337
    Height = 77
    Align = alBottom
    BevelInner = bvLowered
    Color = 12621940
    TabOrder = 0
    object Button3: TcxButton
      Left = 219
      Top = 8
      Width = 90
      Height = 60
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = Button3Click
      Colors.Default = 14864576
      Colors.Normal = 9989189
      Colors.Pressed = 13875616
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 27
      Top = 8
      Width = 90
      Height = 60
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton1Click
      Colors.Default = 14864576
      Colors.Normal = 9989189
      Colors.Pressed = 13875616
      LookAndFeel.Kind = lfFlat
    end
  end
  object GridMo: TcxGrid
    Left = 0
    Top = 0
    Width = 337
    Height = 344
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object ViewMo: TcxGridDBTableView
      OnDblClick = cxButton1Click
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewMoCustomDrawCell
      DataController.DataSource = dmC.dsModif
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      Styles.Selection = dmC.cxStyle21
      object ViewMoSIFR: TcxGridDBColumn
        DataBinding.FieldName = 'SIFR'
        Visible = False
      end
      object ViewMoNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 315
      end
      object ViewMoPARENT: TcxGridDBColumn
        DataBinding.FieldName = 'PARENT'
        Visible = False
      end
      object ViewMoPRICE: TcxGridDBColumn
        DataBinding.FieldName = 'PRICE'
        Visible = False
      end
      object ViewMoREALPRICE: TcxGridDBColumn
        DataBinding.FieldName = 'REALPRICE'
        Visible = False
      end
      object ViewMoIACTIVE: TcxGridDBColumn
        DataBinding.FieldName = 'IACTIVE'
        Visible = False
      end
      object ViewMoIEDIT: TcxGridDBColumn
        DataBinding.FieldName = 'IEDIT'
        Visible = False
      end
    end
    object LevelMo: TcxGridLevel
      GridView = ViewMo
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 344
    Width = 337
    Height = 32
    Align = alBottom
    BevelInner = bvLowered
    Color = 9989189
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 264
    Top = 16
  end
  object amMod: TActionManager
    Left = 72
    Top = 112
    StyleName = 'XP Style'
    object acSelMod: TAction
      Caption = 'acSelMod'
      ShortCut = 13
      OnExecute = acSelModExecute
    end
    object acExitMod: TAction
      Caption = 'acExitMod'
      ShortCut = 121
      OnExecute = acExitModExecute
    end
  end
end
