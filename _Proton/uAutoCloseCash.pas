unit uAutoCloseCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxMemo, cxCheckBox, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan,
  ComCtrls, IdMessage, IdIntercept, IdLogBase, IdLogFile, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, idCoderHeader,
  cxProgressBar;

type
  TfmMainAutoCloseCash = class(TForm)
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    Timer1: TTimer;
    am1: TActionManager;
    acStart: TAction;
    StatusBar1: TStatusBar;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    IdSMTP: TIdSMTP;
    IdLogFile: TIdLogFile;
    IdMessage: TIdMessage;
    acCashLoad: TAction;
    cxProgressBar1: TcxProgressBar;
    acRePutCh: TAction;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acCashLoadExecute(Sender: TObject);
    procedure acRePutChExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function prTrEMailAC(sMess:String):Boolean;

var
  fmMainAutoCloseCash: TfmMainAutoCloseCash;

implementation

uses Un1, MDB, MT, PXDB, u2fdk, MFB, PerToAC;

{$R *.dfm}

function prTrEMailAC(sMess:String):Boolean;
begin
  result:=True;
  with fmMainAutoCloseCash do
  begin
    IdMessage.Clear;
    with IdMessage do
    begin
      From.Text := 'emessage@Eliseysm.ru';
      Sender.Text := 'emessage@Eliseysm.ru';
//      Recipients.EMailAddresses := 'cash_all@eliseysm.ru';
      Recipients.EMailAddresses:= 'sys4@eliseysm.ru';
      Recipients.Add.Address:= 'cash_all@eliseysm.ru';

      CharSet := 'UTF-8';
      Subject:=EncodeHeader('CashError',[],'B',bit8, 'windows-1251');

//      Subject := 'CashError';
      ContentType:= 'text/plain';

      Body.Clear;
      Body.Add(UTF8Encode(sMess));
    end;

    try
      if IdSMTP.Connected=False then
      begin
        IdSMTP.Host:='79.172.8.90';
        IdSMTP.Username:='emessage@Eliseysm.ru';
        IdSMTP.Password:='z7241z';

        IdSMTP.Connect(1000);
      end;

      IdSMTP.Send(IdMessage);

      Memo1.Lines.Add('    ������� ��.');
    except
      Memo1.Lines.Add('    ������� �� ������.');
      Result:=False;
    end;
  end;
end;


procedure TfmMainAutoCloseCash.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date-1;

  ReadIni;

//  if (Frac(time)>=0.041666666)and(Frac(time)<=0.208333333) then  // c 1:00 �� 5:00


  if CommonSet.ACAStart=1 then
  begin
    cxCheckBox1.Checked:=True;
    Timer1.Enabled:=True;
  end
  else
  begin
    cxCheckBox1.Checked:=False;
    Timer1.Enabled:=False;
  end;
end;

procedure TfmMainAutoCloseCash.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStart.Execute;
    Close;
  end;
end;

procedure TfmMainAutoCloseCash.acStartExecute(Sender: TObject);
  //�������� ����������
type tsale = record
     ch:INteger;
     su,ma:Real;
     end;

Var DateB,DateE,iCurD,NumC,CountCheck,DateBM:INteger;
    StrWk,SPath,StrTa,SNumCash,sY:String;
    iC,k,zNum,iDel:INteger;
    rSum,rSumD,rPr,rSumRet,rSumBn,rSumBnRet,rSumBnD,rDiscPos:Real;
    ChTime,CurDate,CurDate1:TDateTime;
    bReadRec:Boolean;
    iMainDep,iAddDep,iCurDep:Integer;

    CurD:TDateTime;
    asale:Array[1..24] of TSale;
    allpos,allcli:INteger;
    allsu,allma:Real;
    Cli20,Cli50,cli100,cli200,cli500,cli1000,cliM:INteger;
    n:INteger;
    iCh,iH:INteger;
    rSumCh:Real;
    CashN:INteger;
    sName:String;
    LDate,DateZ:TDateTime;
    par1:Variant;

   //���������
    iDep:INteger;
    iD:Integer;
    bStart:Boolean;

    vRest,vRestOut:Real;

    par:Variant;

    iPriv,iPostDep,i:INteger;
    iDeps:array[1..20] of integer;
    rTimeShift:Real;

    rQ,rQSpis:Real;
    iStatus,iLastDayIn,iNov:INteger;
    iT:INteger;
    rRemn:Real;
    sMessur,sCSz:String;
    rFisSumNal,rFisSumBN,rFisSumDisc,rFisSumRetNal,rFisSumRetBN:Real;
    rSumCorNal,rSumCorBn,rSumCorDNal,rSumCorDBn:Real;

    rSumR:Real;
    t1,t2:Real;
    sBar:String;
    iBar:Int64;
    iCassir:Integer;
    

function fTestDep(iDep:INteger):Integer;
Var k:INteger;
    bDep:Boolean;
begin
  Result:=5;
  for k:=1 to 20 do if iDeps[k]>0 then Result:=iDeps[k]; //�� ��������� ���������
  bDep:=False;
  for k:=1 to 20 do
  begin
    if iDeps[k]=iDep then bDep:=True;
  end;
  if bDep then Result:=iDep;
end;

begin
  //������ ������
  with dmMC do
  with dmMt do
  with dmPx do
  begin
    DateB:=Trunc(cxDateEdit1.Date);
    DateE:=Trunc(cxDateEdit1.Date);

    if DateB<=prOpenDate then
    begin
      prWH('������ ������.',Memo1);
      showmessage('������ ������.');
      if cxCheckBox1.Checked then
      begin
        delay(2000);
        close;
      end else exit;
    end else
    begin

      StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

      par1 := VarArrayCreate([0,2], varInteger);

      prSetTA(DateB);

      cxButton1.Enabled:=False;
      prWH('������ ������ ���� ('+StrWk+') ... �����.',Memo1);
      try
        dmPx.Session1.Active:=False;
        dmPx.Session1.NetFileDir:=CommonSet.NetPath;
        dmPx.Session1.Active:=True;

          quCash.Active:=False;
          quCash.Active:=True;
          quCash.First;
          while not quCash.Eof do
          begin
            if (quCashTake.AsInteger=1)and(quCashShRealiz.AsInteger=1)  then //����������� �������
            begin
              iMainDep:=RoundEx(quCashRezerv.AsFloat);
              iAddDep:=quCashTechnolog.AsInteger;

              prWH(' '+quCashName.AsString+'  ����� : '+IntToStr(iMainDep)+' ('+INtToStr(iAddDep)+')',Memo1);
              sPath:=CommonSet.CashPath+quCashNumber.AsString+'\OUTPUT\';
              prWH('  '+sPath,Memo1);
              if FileExists(sPath+'CASHSAIL.DB') then
              begin
                taCs.Active:=False;
                taCs.DatabaseName:=sPath; //M:\POS\1\OUTPUT
                taCs.Active:=True;

                taDc.Active:=False;
                taDc.DatabaseName:=sPath;  //M:\POS\1\OUTPUT
                taDc.Active:=True;

                taDCRD.Active:=False;
                taDCRD.DatabaseName:=sPath;  //M:\POS\1\OUTPUT
                taDCRD.Active:=True;

                for iCurD:=DateB to DateB do   //����� ��� �� ��������� ��� ������������ - ������ ����
                begin
                  // ���� ���� ������� �� ����� ����� � �����
                  StrTa:='cq'+formatDateTime('yyyymm',iCurD);
                  StrWk:=formatDateTime('yyyymm',iCurD);

                  if CommonSet.PathCryst[length(CommonSet.PathCryst)]<>'\' then CommonSet.PathCryst:=CommonSet.PathCryst+'\';

                  if Fileexists(CommonSet.PathCryst+'POSDATA\'+'cq'+StrWk)=False then
                  begin //����� ��� - ������� ����� � ������ ���� ����� �����
                    CopyFile(PChar(CommonSet.TrfPath+'cq.btr'),PChar(CommonSet.TmpPath+'cq'+StrWk),False); delay(10);
                    MoveFile(PChar(CommonSet.TmpPath+'cq'+StrWk),PChar(CommonSet.PathCryst+'POSDATA\'+'cq'+StrWk));
                  end;

                  if Fileexists(CommonSet.PathCryst+'POSDATA\'+'cn'+StrWk)=False then
                  begin //����� ��� - ������� ����� � ������ ���� ����� �����
                    CopyFile(PChar(CommonSet.TrfPath+'cn.btr'),PChar(CommonSet.TmpPath+'cn'+StrWk),False); delay(10);
                    MoveFile(PChar(CommonSet.TmpPath+'cn'+StrWk),PChar(CommonSet.PathCryst+'POSDATA\'+'cn'+StrWk));
                  end;


                  zNum:=-1;
                  rSum:=0;
                  rSumD:=0;
                  rSumRet:=0;
                  rSumBn:=0;
                  rSumBnD:=0;
                  rSumBnRet:=0;
                  NumC:=0;
                  CountCheck:=0;
                  DateZ:=taCSDate.AsDateTime;


                  prWH('    - ������ ..',Memo1);
                  if cxCheckBox3.Checked=False then
                  begin
                    quD.Active:=False;
                    quD.SQL.Clear;
                    quD.SQL.Add('Delete from "'+StrTa+'"' );
                    quD.SQL.Add('where  DateOperation='''+ds(iCurD)+'''');
                    quD.SQL.Add('and Cash_Code='+its(quCashNumber.AsInteger));
                    quD.ExecSQL;
                  end else prWH('    - ��� �������� ..',Memo1);

                  prWH('    - ��������� ..',Memo1);
                  taCs.First;
                  if taCS.Locate('Date',iCurD,[]) then
                  begin
                    iC:=0;
                    bReadRec:=True;
//                    while (Trunc(taCSDate.AsDateTime)=iCurD)and(taCS.Eof=False) do
                    while bReadRec and (taCS.Eof=False) do
                    begin
                      k:=1;
                      if taCSZNumber.AsInteger<>zNum then
                      begin
                        if zNum>0 then  //�� ������ ��� ,�.�.  ���������� � ���� �����
                        begin
                          quA.SQL.Clear;
                          quA.SQL.Add('INSERT into "Zreport" values (');
                          quA.SQL.Add(its(quCashNumber.AsInteger)+',');//CashNumber
                          quA.SQL.Add(its(zNum)+',');//ZNumber
                          quA.SQL.Add(fs(rSum)+',');//ZSale
                          quA.SQL.Add(fs(rSumRet)+',');//ZReturn
                          quA.SQL.Add(fs(rSumD-rSumBnD)+',');//ZDiscount
                          quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
                          quA.SQL.Add('''00:00'',');//ZTime
                          quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
                          quA.SQL.Add(fs(rSumBnRet)+',');//ZCrReturn
                          quA.SQL.Add(fs(rSumBnD)+',');//ZCrDiscount
                          quA.SQL.Add('0,');//TSale
                          quA.SQL.Add('0,');//TReturn
                          quA.SQL.Add('0,');//CrSale
                          quA.SQL.Add('0,');//CrReturn
                          quA.SQL.Add(its(CountCheck));//CurMoney
                          quA.SQL.Add(')');
                          quA.ExecSQL;

                          if ptCashDCRD.Active=False then ptCashDCRD.Active:=True;

                          taDCRD.CancelRange;
                          taDCRD.SetRange([1,quCashNumber.AsInteger,zNum],[1,quCashNumber.AsInteger,zNum]);
                          taDCRD.First;
                          while not taDCRD.Eof do
                          begin
                            ptCashDCrd.Append;
                            ptCashDCrdShopIndex.AsInteger:=taDCRDShopIndex.AsInteger;
                            ptCashDCrdCashNumber.AsInteger:=quCashNumber.AsInteger;
                            ptCashDCrdZNumber.AsInteger:=taDCRDZNumber.AsInteger;
                            ptCashDCrdCheckNumber.AsInteger:=taDCRDCheckNumber.AsInteger;
                            ptCashDCrdCardType.AsInteger:=0;
                            ptCashDCrdCardNumber.AsString:=taDCRDCardNumber.AsString;
                            ptCashDCrdDiscountRub.AsFloat:=rv(taDCRDDiscountRub.AsFloat);
                            ptCashDCrdDiscountCur.AsFloat:=rv(taDCRDDiscountRub.AsFloat);
                            ptCashDCrdCasher.AsString:=its(quCashNumber.AsInteger);
                            ptCashDCrdDateSale.AsDateTime:=DateZ;
                            ptCashDCrdTimeSale.AsDateTime:=0.5;
                            ptCashDCrd.Post;

                            taDCRD.Next;
                          end;

                        end;
                        zNum:=taCSZNumber.AsInteger;
                        rSum:=0;
                        rSumD:=0;
                        rSumRet:=0;
                        rSumBn:=0;
                        rSumBnD:=0;
                        rSumBnRet:=0;
                        NumC:=0;
                        CountCheck:=0;

                        quD.SQL.Clear;
                        quD.SQL.Add('Delete from "Zreport"' );
                        quD.SQL.Add('where ZDate='''+ds(iCurD)+'''');
                        quD.SQL.Add('and CashNumber='+its(quCashNumber.AsInteger));
                        quD.SQL.Add('and ZNumber='+its(taCSZNumber.AsInteger));
                        quD.ExecSQL;

                        quD.SQL.Clear;
                        quD.SQL.Add('Delete from "CashDCrd"' );
                        quD.SQL.Add('where ');
                        quD.SQL.Add('CashNumber='+its(quCashNumber.AsInteger));
                        quD.SQL.Add('and ZNumber='+its(taCSZNumber.AsInteger));
                        quD.ExecSQL;

                      end;

                      if taCSReplace.AsInteger=0 then k:=-1;

                      StrWk:=taCSTime.AsString;
                      while Length(StrWk)<4 do StrWk:='0'+StrWk;
                      StrWk:=Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);

                      ChTime:=Trunc(taCSDate.AsDateTime)+StrToTime(StrWk); //����� ���� � ����������� ��������� ���������

                      if CommonSet.h12=1 then rTimeShift:=0 else rTimeShift:=0.0833333;

                      if (ChTime>=(iCurD+rTimeShift)) and (ChTime<(iCurD+1+rTimeShift)) then //� 2-�� ���� �� 2-�� ���� ���������� ���
                      begin
                        if taCSCheckNumber.AsInteger>NumC then
                        begin
                          NumC:=taCSCheckNumber.AsInteger;
                          inc(CountCheck);
                        end;

                        //������� ��� ������
                        rDiscPos:=0;
                        taDC.First;
                        while not taDC.Eof do
                        begin
                          rDiscPos:=rDiscPos+taDCDiscountRub.AsFloat;
                          taDC.Next;
                        end;

                        if abs(taCSQuantity.AsFloat)>0.0001 then
                        begin
                          if taCSCardArticul.AsString>='0' then
                          begin
                            quA.SQL.Clear;
                            quA.SQL.Add('INSERT into "'+StrTa+'" values (');
                            quA.SQL.Add(fs(taCSQuantity.AsFloat*k)+',');//Quant
                            if taCSReplace.AsInteger=1 then quA.SQL.Add('''P'',') //Operation
                                                     else quA.SQL.Add('''R'',');//Operation
                            quA.SQL.Add(''''+ds(iCurD)+''',');//DateOperation
                            quA.SQL.Add(fs(taCSPriceRub.AsFloat)+',');//Price
                            quA.SQL.Add(''''',');//Store
                            quA.SQL.Add(its(taCSCheckNumber.AsInteger)+',');//Ck_Number
                            quA.SQL.Add('0,');//Ck_Curs
                            quA.SQL.Add('0,');//Ck_CurAbr
                            try
                              rPr:=RoundEx(((taCSQuantity.AsFloat*taCSPriceRub.AsFloat)-taCSTotalRub.AsFloat)/(taCSQuantity.AsFloat*taCSPriceRub.AsFloat)*100);
                            except
                              rPr:=0;
                            end;
                            quA.SQL.Add(fs(rPr)+',');//Ck_Disg  % ������
                            quA.SQL.Add(fs(rDiscPos)+',');//Ck_Disc
                            quA.SQL.Add('0,');//Quant_S

                            iCurDep:=taCSUsingIndex.AsInteger;
                            if iCurDep=0 then iCurDep:=iMainDep;
                            if iCurDep<>iMainDep then
                            begin
                              if iCurDep<>iAddDep then iCurDep:=iMainDep;
                            end;
                            quA.SQL.Add(its(iCurDep)+',');//GrCode  ����� - �� ��������� ��������

                            quA.SQL.Add(its(taCSCardArticul.AsInteger)+',');//Code
                            quA.SQL.Add(''''+taCSCasher.AsString+''',');//Cassir
                            quA.SQL.Add(its(taCSCashNumber.AsInteger)+',');//Cash_Code
                            if (taCSOperation.AsInteger=5)or(taCSOperation.AsInteger=4)
                              then quA.SQL.Add('1,') //Ck_Card
                              else quA.SQL.Add('0,');
                            quA.SQL.Add(''''',');//Contr_Code
                            quA.SQL.Add('0,');//Contr_Cost
                            quA.SQL.Add(''''',');//Seria
                            quA.SQL.Add(''''',');//BestB
                            quA.SQL.Add('0,');//NDSx1
                            quA.SQL.Add('0,');//NDSx2

                            StrWk:=taCSTime.AsString;
                            while Length(StrWk)<4 do StrWk:='0'+StrWk;
                            StrWk:=Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);
                            quA.SQL.Add(''''+StrWk+''',');//Times

                            quA.SQL.Add(fs(taCSTotalRub.AsFloat*k)+',');//Summa
                            quA.SQL.Add('0,');//SumNDS
                            quA.SQL.Add('0,');//SumNSP
                            quA.SQL.Add('0,');//PriceNSP
                            quA.SQL.Add(its(taCSZNumber.AsInteger));//NSmena
                            quA.SQL.Add(')');
                            quA.ExecSQL;
                          end else  prWH('     ���.  '+taCSCardArticul.AsString,Memo1);
                        end;

                        if k>0 then  //������� ��� ����� ���������
                        begin
                          rSum:=rSum+taCSTotalRub.AsFloat;  // �����
                          if (taCSOperation.AsInteger=5)or(taCSOperation.AsInteger=4) then
                          begin
                            rSumBn:=rSumBn+taCSTotalRub.AsFloat; //������ �������
                          end;
                        end;

                        rSumD:=rSumD+rDiscPos*k;

                        if (taCSOperation.AsInteger=5)or(taCSOperation.AsInteger=4) then
                          rSumBnD:=rSumBnD+rDiscPos*k;

                        if k<0 then   //������ ��������
                        begin
                          rSumRet:=rSumRet+taCSTotalRub.AsFloat; //��� ��������
                          if (taCSOperation.AsInteger=5)or(taCSOperation.AsInteger=4) then
                          begin
                            rSumBnRet:=rSumBnRet+taCSTotalRub.AsFloat; //�������� �� �������
                          end;
                        end;
                      end; //������� �� ������� ����

                      taCs.Next; inc(iC); StatusBar1.Panels[0].Text:=its(iC);  Delay(10);

                      if taCS.Eof=False then
                      begin
                        StrWk:=taCSTime.AsString;
                        while Length(StrWk)<4 do StrWk:='0'+StrWk;
                        StrWk:=Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);

                        ChTime:=Trunc(taCSDate.AsDateTime)+StrToTime(StrWk); //����� ���� � ����������� ��������� ���������

                        if ChTime>=(iCurD+1+rTimeShift) then bReadRec:=False; //������ ������ ������
                      end else bReadRec:=False; //������ ������ ������
                    end;

                    prWH('     '+ds(iCurD)+' ���������� '+its(iC)+' �������.',Memo1);

                  end else
                     prWH('     '+ds(iCurD)+' ������ ���. �������� ��������� �������� � ����.',Memo1);

                  if zNum>0 then
                  begin
                    quA.SQL.Clear;
                    quA.SQL.Add('INSERT into "Zreport" values (');
                    quA.SQL.Add(its(quCashNumber.AsInteger)+',');//CashNumber
                    quA.SQL.Add(its(zNum)+',');//ZNumber
                    quA.SQL.Add(fs(rSum)+',');//ZSale
                    quA.SQL.Add(fs(rSumRet)+',');//ZReturn
                    quA.SQL.Add(fs(rSumD-rSumBnD)+',');//ZDiscount
                    quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
                    quA.SQL.Add('''00:00'',');//ZTime
                    quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
                    quA.SQL.Add(fs(rSumBnRet)+',');//ZCrReturn
                    quA.SQL.Add(fs(rSumBnD)+',');//ZCrDiscount
                    quA.SQL.Add('0,');//TSale
                    quA.SQL.Add('0,');//TReturn
                    quA.SQL.Add('0,');//CrSale
                    quA.SQL.Add('0,');//CrReturn
                    quA.SQL.Add(its(CountCheck));//CurMoney
                    quA.SQL.Add(')');
                    quA.ExecSQL;

                    if ptCashDCRD.Active=False then ptCashDCRD.Active:=True;

                    taDCRD.CancelRange;
                    taDCRD.SetRange([1,quCashNumber.AsInteger,zNum],[1,quCashNumber.AsInteger,zNum]);
                    taDCRD.First;
                    while not taDCRD.Eof do
                    begin
                      ptCashDCrd.Append;
                      ptCashDCrdShopIndex.AsInteger:=taDCRDShopIndex.AsInteger;
                      ptCashDCrdCashNumber.AsInteger:=quCashNumber.AsInteger;
                      ptCashDCrdZNumber.AsInteger:=taDCRDZNumber.AsInteger;
                      ptCashDCrdCheckNumber.AsInteger:=taDCRDCheckNumber.AsInteger;
                      ptCashDCrdCardType.AsInteger:=0;
                      ptCashDCrdCardNumber.AsString:=taDCRDCardNumber.AsString;
                      ptCashDCrdDiscountRub.AsFloat:=rv(taDCRDDiscountRub.AsFloat);
                      ptCashDCrdDiscountCur.AsFloat:=rv(taDCRDDiscountRub.AsFloat);
                      ptCashDCrdCasher.AsString:=its(quCashNumber.AsInteger);
                      ptCashDCrdDateSale.AsDateTime:=DateZ;
                      ptCashDCrdTimeSale.AsDateTime:=0.5;
                      ptCashDCrd.Post;

                      taDCRD.Next;
                    end;
                  end;
                end;
                Delay(100);
                StatusBar1.Panels[0].Text:='';
                taDCRD.Active:=False;
                taDC.Active:=False;
                taCs.Active:=False;

                if ptCashDCRD.Active then ptCashDCRD.Active:=False;
              end else
                prWH('   ���� ������ �� ������.',Memo1);
            end;

            if (quCashTake.AsInteger=1)and(quCashShRealiz.AsInteger=2)  then //����� FB
            begin
              iMainDep:=RoundEx(quCashRezerv.AsFloat);
              iAddDep:=quCashTechnolog.AsInteger;

              prWH(' '+quCashName.AsString+'  ����� : '+IntToStr(iMainDep)+' ('+INtToStr(iAddDep)+')',Memo1);
              iCurD:=DateB;
              // ���� ���� ������� �� ����� ����� � �����
              StrTa:='cq'+formatDateTime('yyyymm',iCurD);

              zNum:=-1;
              rSum:=0;
              rSumD:=0;
              rSumRet:=0;
              rSumBn:=0;
              rSumBnD:=0;
              rSumBnRet:=0;
              NumC:=0;
              CountCheck:=0;

              prWH('    - ������ ..',Memo1);
              if cxCheckBox3.Checked=False then
              begin
                quD.Active:=False;
                quD.SQL.Clear;
                quD.SQL.Add('Delete from "'+StrTa+'"' );
                quD.SQL.Add('where  DateOperation='''+ds(iCurD)+'''');
                quD.SQL.Add('and Cash_Code='+its(quCashNumber.AsInteger));

                quD.ExecSQL;
              end else prWH('    - ��� �������� ..',Memo1);

              delay(100);

              ptCQ.Active:=False;
              ptCQ.TableName:=StrTa;
              ptCQ.Active:=True;

              prWH('    - ��������� ..',Memo1);
              with dmFB do
              begin
                if Cash.Connected then
                begin
                  prWH('    ����� � ����� �� ..',Memo1);
                  iC:=0; NumC:=0;

                  SNumCash:=quCashNumber.AsString; if length(SNumCash)<2 then SNumCash:='0'+SNumCash;
                  if CommonSet.h12=1 then rTimeShift:=0 else rTimeShift:=0.0833333;

                  prWH('      ������ � '+dts(iCurD+rTimeShift)+'  �� '+dts(iCurD+1+rTimeShift),Memo1);

                  quCashSail.SelectSQL.Clear;
                  quCashSail.Active:=False;
                  quCashSail.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,ID,CHDATE,ARTICUL,BAR,CARDSIZE,QUANTITY,PRICERUB,DPROC,DSUM,TOTALRUB,CASHER,DEPART,OPERATION');
                  quCashSail.SelectSQL.Add('FROM CASHS'+SNumCash);
                  quCashSail.SelectSQL.Add('WHERE CHDATE>='''+dts(iCurD+rTimeShift)+'''');
                  quCashSail.SelectSQL.Add('and CHDATE<'''+dts(iCurD+1+rTimeShift)+'''');
                  quCashSail.SelectSQL.Add('order by  SHOPINDEX, CASHNUMBER,ZNUMBER,CHECKNUMBER,ID');

                  quCashSail.Active:=True;
                  quCashSail.First;
                  while not quCashSail.Eof do
                  begin
                    k:=1;
                    if quCashSailZNumber.AsInteger<>zNum then
                    begin
                      if zNum>0 then  //�� ������ ��� ,�.�.  ���������� � ���� �����
                      begin
                        //�������� �������������� � ���� �������������� ���� ��� �������������

                        prGetFisSum(quCashNumber.AsInteger,ZNum,iCurD,rFisSumNal,rFisSumBN,rFisSumDisc,rFisSumRetNal,rFisSumRetBN);

                        if ((rFisSumNal>0) or (rFisSumBN>0))
                        and(
                           (abs(rSum-rFisSumNal-rFisSumBN)>=0.01)or (abs(rSumRet-rFisSumRetNal-rFisSumRetBN)>=0.01)
                           )
                        then //���� ������� ����������� - ����� ���-�� ������
                        begin
                          if (abs(rSum-rFisSumNal-rFisSumBN)>=100)
                          or (abs(rSumRet-rFisSumRetNal-rFisSumRetBN)>=100)
                          then //����������� ������� ����
                          begin
                            prWH('         ������� ����������� � ����.�������. ����������.',Memo1);
                            prWH('������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.',Memo1);

                            prTrEMailAC('������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.');
                          end else //���������� � �������� �����������
                          begin
                            prWH('         ��������� ����������� � ����.�������. ���������.',Memo1);

                            //���������
                            rSumCorNal:=(rFisSumNal-rFisSumRetNal)-(rSum-rSumBn-(rSumRet-rSumBnRet));
                            rSumCorBn:=(rFisSumBN-rFisSumRetBN)-(rSumBn-rSumBnRet);
                            rSumCorDNal:=rFisSumDisc-rSumD;
                            rSumCorDBn:=0; //������ ������� ������ �� ���

                            prWH('���������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.',Memo1);
//                            prTrEMailAC('���������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.');

                            par1[0]:=quCashNumber.AsInteger;
                            par1[1]:=ZNum;
                            par1[2]:=CommonSet.CorrCheckNum;

                            //�� ������ ������
                            if ptCQ.Locate('Cash_Code;NSmena;Ck_Number',par1,[]) then ptCQ.Delete;

                            par1[0]:=quCashNumber.AsInteger;
                            par1[1]:=ZNum;
                            par1[2]:=CommonSet.CorrCheckNum+1;

                            if ptCQ.Locate('Cash_Code;NSmena;Ck_Number',par1,[]) then ptCQ.Delete;

                            delay(100);

                            if (abs(rSumCorNal)>0.001) then
                            begin //������ ��� -  ������ ����� ���� �������
                              ptCQ.Append;
                              ptCQQuant.AsFloat:=1;
                              ptCQOperation.AsString:='P';
                              ptCQDateOperation.AsDateTime:=iCurD;
                              ptCQPrice.AsFloat:=rSumCorNal;
                              ptCQStore.AsString:='';
                              ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum;
                              ptCQCk_Curs.AsFloat:=0;
                              ptCQCk_CurAbr.AsInteger:=0;
                              ptCQCk_Disg.AsFloat:=0;
                              ptCQCk_Disc.AsFloat:=rSumCorDNal;
                              ptCQQuant_S.AsFloat:=0;
                              ptCQGrCode.AsFloat:=iMainDep;
                              ptCQCode.AsInteger:=CommonSet.CorrArt;
                              ptCQCassir.AsString:='10'+quCashNumber.AsString;
                              ptCQCash_Code.AsInteger:=quCashNumber.AsInteger;
                              ptCQCk_Card.AsInteger:=0;//���
                              ptCQContr_Code.AsString:='';
                              ptCQContr_Cost.AsFloat:=0;
                              ptCQSeria.AsString:='';
                              ptCQBestB.AsString:='';
                              ptCQNDSx1.AsFloat:=0;
                              ptCQNDSx2.AsFloat:=0;
                              ptCQTimes.AsDateTime:=StrToTime('07:10');
                              ptCQSumma.AsFloat:=rSumCorNal;
                              ptCQSumNDS.AsFloat:=0;
                              ptCQSumNSP.AsFloat:=0;
                              ptCQPriceNSP.AsFloat:=0;
                              ptCQNSmena.AsInteger:=zNum;
                              ptCQ.Post;
                            end;

                            if (abs(rSumCorBn)>0.001) then
                            begin //������ ��� -  ������ ����� ���� �������
                              ptCQ.Append;
                              ptCQQuant.AsFloat:=1;
                              ptCQOperation.AsString:='P';
                              ptCQDateOperation.AsDateTime:=iCurD;
                              ptCQPrice.AsFloat:=rSumCorBn;
                              ptCQStore.AsString:='';
                              ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum+1;
                              ptCQCk_Curs.AsFloat:=0;
                              ptCQCk_CurAbr.AsInteger:=0;
                              ptCQCk_Disg.AsFloat:=0;
                              ptCQCk_Disc.AsFloat:=0;
                              ptCQQuant_S.AsFloat:=0;
                              ptCQGrCode.AsFloat:=iMainDep;
                              ptCQCode.AsInteger:=CommonSet.CorrArt;
                              ptCQCassir.AsString:='10'+quCashNumber.AsString;
                              ptCQCash_Code.AsInteger:=quCashNumber.AsInteger;
                              ptCQCk_Card.AsInteger:=1;//���-���
                              ptCQContr_Code.AsString:='';
                              ptCQContr_Cost.AsFloat:=0;
                              ptCQSeria.AsString:='';
                              ptCQBestB.AsString:='';
                              ptCQNDSx1.AsFloat:=0;
                              ptCQNDSx2.AsFloat:=0;
                              ptCQTimes.AsDateTime:=StrToTime('07:10');
                              ptCQSumma.AsFloat:=rSumCorBn;
                              ptCQSumNDS.AsFloat:=0;
                              ptCQSumNSP.AsFloat:=0;
                              ptCQPriceNSP.AsFloat:=0;
                              ptCQNSmena.AsInteger:=zNum;
                              ptCQ.Post;
                            end;

                            rSum:=rFisSumNal+rFisSumBN;
                            rSumBn:=rFisSumBN;
                            rSumRet:=rFisSumRetNal-rFisSumRetBN;
                            rSumBnRet:=rFisSumRetBN;
                            rSumBnD:=rFisSumDisc;

                          end;
                        end else
                        begin
                          prWH('         ����������� � ����. ������� ���.',Memo1);
                        end;

                        quA.SQL.Clear;
                        quA.SQL.Add('INSERT into "Zreport" values (');
                        quA.SQL.Add(its(quCashNumber.AsInteger)+',');//CashNumber
                        quA.SQL.Add(its(zNum)+',');//ZNumber
                        quA.SQL.Add(fs(rSum)+',');//ZSale  ������ ����� ������
                        quA.SQL.Add(fs(rSumRet)+',');//ZReturn
                        quA.SQL.Add(fs(rSumD-rSumBnD)+',');//ZDiscount
                        quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
                        quA.SQL.Add('''00:00'',');//ZTime
                        quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
                        quA.SQL.Add(fs(rSumBnRet)+',');//ZCrReturn
                        quA.SQL.Add(fs(rSumBnD)+',');//ZCrDiscount
                        quA.SQL.Add('0,');//TSale
                        quA.SQL.Add('0,');//TReturn
                        quA.SQL.Add('0,');//CrSale
                        quA.SQL.Add('0,');//CrReturn
                        quA.SQL.Add(its(CountCheck));//CurMoney
                        quA.SQL.Add(')');
                        quA.ExecSQL;
                      end;
                      zNum:=quCashSailZNumber.AsInteger;
                      rSum:=0;
                      rSumD:=0;
                      rSumRet:=0;
                      rSumBn:=0;
                      rSumBnD:=0;
                      rSumBnRet:=0;
                      NumC:=0;
                      CountCheck:=0;

                      quD.SQL.Clear;
                      quD.SQL.Add('Delete from "Zreport"' );
                      quD.SQL.Add('where ZDate='''+ds(iCurD)+'''');
                      quD.SQL.Add('and CashNumber='+its(quCashNumber.AsInteger));
                      quD.SQL.Add('and ZNumber='+its(quCashSailZNumber.AsInteger));
                      quD.ExecSQL;
                    end;

                    Case quCashSailOPERATION.AsInteger of
                    0,4: k:=-1;
                    end;

                    ChTime:=quCashSailCHDATE.AsDateTime; //����� ���� � ����������� ��������� ���������
                    t2:=quCashSailCHDATE.AsDateTime;
                                                                                             // ��� � 12 �� 12
                    if (ChTime>=(iCurD+rTimeShift)) and (ChTime<(iCurD+1+rTimeShift)) then //� 2-�� ���� �� 2-�� ���� ���������� ���
                    begin
                      if quCashSailCHECKNUMBER.AsInteger>NumC then
                      begin
                        NumC:=quCashSailCHECKNUMBER.AsInteger;
                        inc(CountCheck);  //���-�� ����� �������
                      end;

                      if quCashSailARTICUL.AsString>='0' then
                      begin
                        ptCQ.Append;
                        ptCQQuant.AsFloat:=quCashSailQuantity.AsFloat*k;
                        if k=1 then ptCQOperation.AsString:='P' //Operation
                               else ptCQOperation.AsString:='R';//Operation
                        ptCQDateOperation.AsDateTime:=iCurD;
                        ptCQPrice.AsFloat:=RoundVal(quCashSailPRICERUB.AsFloat);
                        ptCQStore.AsString:='';
                        ptCQCk_Number.AsInteger:=quCashSailCHECKNUMBER.AsInteger;

                        ptCQCk_Curs.AsFloat:=quCashSailID.AsInteger;  //����� ������� � ����

                        sBar:=Trim(quCashSailBAR.AsString);
                        iBar:=0;
                        for n:=1 to Length(sBar) do
                        begin
                          iBar:=iBar*10+StrToIntDef(sBar[n],0);
                        end;

                        ptCQQuant_S.AsFloat:=iBar+(Length(sBar)/100);

                        if quCashSailID.AsInteger=1 then ptCQContr_Cost.AsFloat:=0
                        else ptCQContr_Cost.AsFloat:=t2-t1; //����� ���������� ������� ���� - ������� �� 2-��

                        ptCQCk_CurAbr.AsInteger:=0;

                        ptCQCk_Disg.AsFloat:=quCashSailDPROC.AsFloat;
                        ptCQCk_Disc.AsFloat:=quCashSailDSUM.AsFloat;

                        iCurDep:=quCashSailDEPART.AsInteger;
                        if iCurDep=0 then iCurDep:=iMainDep;
                        if iCurDep<>iMainDep then
                        begin
                          if iCurDep<>iAddDep then iCurDep:=iMainDep;
                        end;

                        ptCQGrCode.AsFloat:=iCurDep;
                        ptCQCode.AsInteger:=StrToINtDef(quCashSailARTICUL.AsString,0);
                        ptCQCassir.AsString:=quCashSailCASHER.AsString;
                        ptCQCash_Code.AsInteger:=quCashSailCASHNUMBER.AsInteger;

                        if (quCashSailOPERATION.AsInteger=5)or(quCashSailOPERATION.AsInteger=4)
                          then ptCQCk_Card.AsInteger:=1 //Ck_Card
                          else ptCQCk_Card.AsInteger:=0;

                        ptCQContr_Code.AsString:='';
                        ptCQSeria.AsString:='';
                        ptCQBestB.AsString:='';
                        ptCQNDSx1.AsFloat:=0;
                        ptCQNDSx2.AsFloat:=0;
                        ptCQTimes.AsDateTime:=StrToTime(formatDateTime('hh:nn',quCashSailCHDATE.AsDateTime));
                        ptCQSumma.AsFloat:=quCashSailTOTALRUB.AsFloat*k;
                        ptCQSumNDS.AsFloat:=0;
                        ptCQSumNSP.AsFloat:=0;
                        ptCQPriceNSP.AsFloat:=0;
                        ptCQNSmena.AsInteger:=quCashSailZNumber.AsInteger;
                        ptCQ.Post;

                      end else prWH('     ���.  '+quCashSailARTICUL.AsString,Memo1);

                      if k>0 then  //������� ��� ����� ���������
                      begin
                        rSum:=rSum+quCashSailTotalRub.AsFloat;  // �����
                        if (quCashSailOperation.AsInteger=5)or(quCashSailOperation.AsInteger=4) then
                        begin
                          rSumBn:=rSumBn+quCashSailTotalRub.AsFloat; //������ �������
                        end;
                      end;

                      rSumD:=rSumD+rv(quCashSailDSUM.AsFloat)*k;

                      if (quCashSailOperation.AsInteger=5)or(quCashSailOperation.AsInteger=4) then
                        rSumBnD:=rSumBnD+rv(quCashSailDSUM.AsFloat)*k;

                      if k<0 then   //������ ��������
                      begin
                        rSumRet:=rSumRet+rv(quCashSailTotalRub.AsFloat); //��� ��������
                        if (quCashSailOperation.AsInteger=5)or(quCashSailOperation.AsInteger=4) then
                        begin
                          rSumBnRet:=rSumBnRet+rv(quCashSailTotalRub.AsFloat); //�������� �� �������
                        end;
                      end;
                    end; //������� �� ������� ����

                    quCashSail.Next;
                    t1:=t2;
                    inc(iC);
                    if iC mod 100 = 0 then
                    begin
                      StatusBar1.Panels[0].Text:=its(iC);
                      Delay(10);
                    end;
                  end;

                  StatusBar1.Panels[0].Text:=its(iC);
                  Delay(10);

                  if zNum>0 then
                  begin
                    //�������� �������������� � ���� �������������� ���� ��� �������������

                    prGetFisSum(quCashNumber.AsInteger,ZNum,iCurD,rFisSumNal,rFisSumBN,rFisSumDisc,rFisSumRetNal,rFisSumRetBN);

                    if ((rFisSumNal>0) or (rFisSumBN>0))
                    and((abs(rSum-rFisSumNal-rFisSumBN)>=0.01)or (abs(rSumRet-rFisSumRetNal-rFisSumRetBN)>=0.01))
                    then //���� ������� ����������� - ����� ���-�� ������
                    begin
                      if (abs(rSum-rFisSumNal-rFisSumBN)>=100)
                      or (abs(rSumRet-rFisSumRetNal-rFisSumRetBN)>=100)
                      then //����������� ������� ����
                      begin
                        prWH('         ������� ����������� � ����.�������. ����������.',Memo1);

                        prWH('������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.',Memo1);
                        prTrEMailAC('������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.');

                      end else //���������� � �������� �����������
                      begin
                        prWH('         ��������� ����������� � ����.�������. ���������.',Memo1);
                        prWH('���������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.',Memo1);
//                            prTrEMailAC('���������. ������� � '+its(fShop)+'. ����� '+quCashNumber.AsString+'. ��� ��� '+fs(rFisSumNal)+'  ���� '+fs(rSum-rSumBn)+'. ������ ��� '+fs(rFisSumBN)+'  ���� '+fs(rSumBn)+'. ������� '+fs(rFisSumNal+rFisSumBN-rSum)+'.');

                            //���������
                        rSumCorNal:=(rFisSumNal-rFisSumRetNal)-(rSum-rSumBn-(rSumRet-rSumBnRet));
                        rSumCorBn:=(rFisSumBN-rFisSumRetBN)-(rSumBn-rSumBnRet);
                        rSumCorDNal:=rFisSumDisc-rSumD;
                        rSumCorDBn:=0; //������ ������� ������ �� ���

                        par1[0]:=quCashNumber.AsInteger;
                        par1[1]:=ZNum;
                        par1[2]:=CommonSet.CorrCheckNum;

                        //�� ������ ������
                        if ptCQ.Locate('Cash_Code;NSmena;Ck_Number',par1,[]) then ptCQ.Delete;

                        par1[0]:=quCashNumber.AsInteger;
                        par1[1]:=ZNum;
                        par1[2]:=CommonSet.CorrCheckNum+1;

                        if ptCQ.Locate('Cash_Code;NSmena;Ck_Number',par1,[]) then ptCQ.Delete;

                        delay(100);

                        if (abs(rSumCorNal)>0.001) then
                        begin //������ ��� -  ������ ����� ���� �������
                          ptCQ.Append;
                          ptCQQuant.AsFloat:=1;
                          ptCQOperation.AsString:='P';
                          ptCQDateOperation.AsDateTime:=iCurD;
                          ptCQPrice.AsFloat:=rSumCorNal;
                          ptCQStore.AsString:='';
                          ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum;
                          ptCQCk_Curs.AsFloat:=0;
                          ptCQCk_CurAbr.AsInteger:=0;
                          ptCQCk_Disg.AsFloat:=0;
                          ptCQCk_Disc.AsFloat:=rSumCorDNal;
                          ptCQQuant_S.AsFloat:=0;
                          ptCQGrCode.AsFloat:=iMainDep;
                          ptCQCode.AsInteger:=CommonSet.CorrArt;
                          ptCQCassir.AsString:='10'+quCashNumber.AsString;
                          ptCQCash_Code.AsInteger:=quCashNumber.AsInteger;
                          ptCQCk_Card.AsInteger:=0;//���
                          ptCQContr_Code.AsString:='';
                          ptCQContr_Cost.AsFloat:=0;
                          ptCQSeria.AsString:='';
                          ptCQBestB.AsString:='';
                          ptCQNDSx1.AsFloat:=0;
                          ptCQNDSx2.AsFloat:=0;
                          ptCQTimes.AsDateTime:=StrToTime('07:10');
                          ptCQSumma.AsFloat:=rSumCorNal;
                          ptCQSumNDS.AsFloat:=0;
                          ptCQSumNSP.AsFloat:=0;
                          ptCQPriceNSP.AsFloat:=0;
                          ptCQNSmena.AsInteger:=zNum;
                          ptCQ.Post;
                        end;

                        if (abs(rSumCorBn)>0.001) then
                        begin
                          ptCQ.Append;
                          ptCQQuant.AsFloat:=1;
                          ptCQOperation.AsString:='P';
                          ptCQDateOperation.AsDateTime:=iCurD;
                          ptCQPrice.AsFloat:=rSumCorBn;
                          ptCQStore.AsString:='';
                          ptCQCk_Number.AsInteger:=CommonSet.CorrCheckNum+1;
                          ptCQCk_Curs.AsFloat:=0;
                          ptCQCk_CurAbr.AsInteger:=0;
                          ptCQCk_Disg.AsFloat:=0;
                          ptCQCk_Disc.AsFloat:=0;
                          ptCQQuant_S.AsFloat:=0;
                          ptCQGrCode.AsFloat:=iMainDep;
                          ptCQCode.AsInteger:=CommonSet.CorrArt;
                          ptCQCassir.AsString:='10'+quCashNumber.AsString;
                          ptCQCash_Code.AsInteger:=quCashNumber.AsInteger;
                          ptCQCk_Card.AsInteger:=1;//���-���
                          ptCQContr_Code.AsString:='';
                          ptCQContr_Cost.AsFloat:=0;
                          ptCQSeria.AsString:='';
                          ptCQBestB.AsString:='';
                          ptCQNDSx1.AsFloat:=0;
                          ptCQNDSx2.AsFloat:=0;
                          ptCQTimes.AsDateTime:=StrToTime('07:10');
                          ptCQSumma.AsFloat:=rSumCorBn;
                          ptCQSumNDS.AsFloat:=0;
                          ptCQSumNSP.AsFloat:=0;
                          ptCQPriceNSP.AsFloat:=0;
                          ptCQNSmena.AsInteger:=zNum;
                          ptCQ.Post;
                        end;

                        //����������� ����� z ������

                        rSum:=rFisSumNal+rFisSumBN;
                        rSumBn:=rFisSumBN;
                        rSumRet:=rFisSumRetNal-rFisSumRetBN;
                        rSumBnRet:=rFisSumRetBN;
                        rSumBnD:=rFisSumDisc;

                      end;
                    end else
                    begin
                      prWH('         ����������� � ����. ������� ���.',Memo1);
                    end;

                    quA.SQL.Clear;
                    quA.SQL.Add('INSERT into "Zreport" values (');
                    quA.SQL.Add(its(quCashNumber.AsInteger)+',');//CashNumber
                    quA.SQL.Add(its(zNum)+',');//ZNumber
                    quA.SQL.Add(fs(rSum)+',');//ZSale
                    quA.SQL.Add(fs(rSumRet)+',');//ZReturn
                    quA.SQL.Add(fs(rSumD-rSumBnD)+',');//ZDiscount
                    quA.SQL.Add(''''+ds(iCurD)+''',');//ZDate
                    quA.SQL.Add('''00:00'',');//ZTime
                    quA.SQL.Add(fs(rSumBn)+',');//ZCrSale
                    quA.SQL.Add(fs(rSumBnRet)+',');//ZCrReturn
                    quA.SQL.Add(fs(rSumBnD)+',');//ZCrDiscount
                    quA.SQL.Add('0,');//TSale
                    quA.SQL.Add('0,');//TReturn
                    quA.SQL.Add('0,');//CrSale
                    quA.SQL.Add('0,');//CrReturn
                    quA.SQL.Add(its(CountCheck));//CurMoney
                    quA.SQL.Add(')');
                    quA.ExecSQL;
                  end;

                  quCashSail.Active:=False;

                  prWH('      ���� - '+its(iC),Memo1);

                  //��������� ������

                  quCashPay.Active:=False;
                  quCashPay.SelectSQL.Clear;
                  quCashPay.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,DBAR,max(CHDATE) as CHDATE,SUM(CHECKSUM) as CHECKSUM,SUM(PAYSUM) as PAYSUM,SUM(DSUM) as DSUM');
                  quCashPay.SelectSQL.Add('FROM CASHP'+SNumCash);
                  quCashPay.SelectSQL.Add('WHERE CHDATE>='''+ds(iCurD+rTimeShift)+'''');
                  quCashPay.SelectSQL.Add('and CHDATE<'''+ds(iCurD+1+rTimeShift)+'''');
                  quCashPay.SelectSQL.Add('group by SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,DBAR');
                  quCashPay.SelectSQL.Add('order by  SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER');

                  zNum:=0;
                  //������� ����

                  quCashPay.Active:=True;
                  quCashPay.First;
                  while not quCashPay.Eof do
                  begin
                    if zNum<>quCashPayZNUMBER.AsInteger then
                    begin
                      zNum:=quCashPayZNUMBER.AsInteger;
                      prWH('        ������ CashDCRD ('+its(quCashNumber.AsInteger)+' '+its(zNum)+')',Memo1);
                      try
                        quD.SQL.Clear;
                        quD.SQL.Add('Delete from "CashDCrd"' );
                        quD.SQL.Add('where ');
                        quD.SQL.Add('CashNumber='+its(quCashNumber.AsInteger));
                        quD.SQL.Add('and ZNumber='+its(zNum));
                        quD.ExecSQL;
                      except
                        prWH('        ������ ������ CashDCRD ('+its(quCashNumber.AsInteger)+' '+its(zNum)+')',Memo1);
                        delay(1000);
                      end;
                    end;
                    quCashPay.Next;
                  end;
                  //��� ��������� - ������ ����������
                  try
                    if ptCashDCRD.Active=False then ptCashDCRD.Active:=True;
                  except
                    prWH('      ������ �������� ptCashDCRD',Memo1);
                    delay(1000);
                  end;

                  quCashPay.First;
                  while not quCashPay.Eof do
                  begin
                    try
                      ptCashDCrd.Append;
                      ptCashDCrdShopIndex.AsInteger:=1;
                      ptCashDCrdCashNumber.AsInteger:=quCashPayCASHNUMBER.AsInteger;
                      ptCashDCrdZNumber.AsInteger:=quCashPayZNUMBER.AsInteger;
                      ptCashDCrdCheckNumber.AsInteger:=quCashPayCHECKNUMBER.AsInteger;
                      ptCashDCrdCardType.AsInteger:=0;
                      ptCashDCrdCardNumber.AsString:=quCashPayDBAR.AsString;
                      ptCashDCrdDiscountRub.AsFloat:=rv(quCashPayDSUM.AsFloat);
                      ptCashDCrdDiscountCur.AsFloat:=rv(quCashPayDSUM.AsFloat);
                      ptCashDCrdCasher.AsString:=its(quCashNumber.AsInteger);
                      ptCashDCrdDateSale.AsDateTime:=Trunc(quCashPayCHDATE.AsDateTime);
                      ptCashDCrdTimeSale.AsDateTime:=Frac(quCashPayCHDATE.AsDateTime);
                      ptCashDCrd.Post;

                    except
                      prWH('         err '+its(quCashPayCASHNUMBER.AsInteger)+' '+its(quCashPayZNUMBER.AsInteger)+' '+its(quCashPayCHECKNUMBER.AsInteger)+' '+quCashPayDBAR.AsString,Memo1);
                      delay(1000);
                    end;

                    quCashPay.Next;
                  end;

                  if ptCashDCRD.Active then ptCashDCRD.Active:=False;
                  quCashPay.Active:=False;

                end else
                begin
                  prWH('    ��� ����� � �����. �������� �����������.',Memo1);

                end;
              end;


            end;
            prWH('  ��',Memo1);
            quCash.Next;
          end;

          //�������� ����������
          prWH('  �������� ���������� �� ������.',Memo1); delay(10);
          try
            try
              for iCurD:=DateB to DateE do
              begin
                iC:=0;

                prWH(fmt+'',Memo1);
                prWH(fmt+'  ��������� ����  '+ FormatDateTime('dd.mm.yyyy',iCurD),Memo1); delay(10);

// ������� ��� �������
            for n:=1 to 24 do
            begin
              asale[n].ch:=0;
              asale[n].su:=0;
              asale[n].ma:=0;
            end;

            allpos:=0; allcli:=0;
            allsu:=0; allma:=0;
            Cli20:=0;
            Cli50:=0;
            cli100:=0;
            cli200:=0;
            cli500:=0;
            cli1000:=0;
            cliM:=0;

            CurD:=iCurD;

            quCQCheck.Active:=False;
            quCQCheck.SQL.Clear;
            quCQCheck.SQL.Add('select DateOperation,Cash_Code,NSmena,Ck_Number,Operation,SUM(Summa) as CheckSum, Count(*) as QuantPos,max(Times) as Times');
            quCQCheck.SQL.Add('from "cq'+FormatDateTime('yyyymm',CurD)+'"');
            quCQCheck.SQL.Add('where DateOperation='''+ds(CurD)+'''');
//            quCQCheck.SQL.Add('where DateOperation='''+ds(CurD)+''' and Operation=''P''');
            quCQCheck.SQL.Add('group by DateOperation,Cash_Code,NSmena,Ck_Number,Operation');
            quCQCheck.Active:=True;

            quCQCheck.First;
            CashN:=quCQCheckCash_Code.AsInteger;
            prWH(fmt+'    ����� � '+IntToStr(CashN),Memo1); delay(10);

            while not quCQCheck.Eof do
            begin
              if CashN<>quCQCheckCash_Code.AsInteger then //����� ��������� - ���� ���������� � ��������
              begin
                //������
                quD.SQL.Clear;
                quD.SQL.Add('Delete from "CashDay"' );
                quD.SQL.Add('where DateSale='''+ds(iCurD)+'''');
                quD.SQL.Add('and Number='+its(CashN));
                quD.ExecSQL;
                delay(100);

                //���������
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "CashDay" values (');
                quA.SQL.Add(its(CashN)+',');
                quA.SQL.Add(''''+ds(iCurD)+''',');

                for n:=1 to 24 do
                begin
                  quA.SQL.Add(its(asale[n].ch)+',');
                  quA.SQL.Add(fs(rv(asale[n].su))+',');
                  quA.SQL.Add(fs(rv(asale[n].ma))+',');
                  if asale[n].ch>0 then quA.SQL.Add(fs(rv(asale[n].su/asale[n].ch))+',')
                  else quA.SQL.Add('0,');
                end;

                quA.SQL.Add(its(allpos)+','); //QuantSale
                quA.SQL.Add(its(allcli)+',');//QuantClient
                quA.SQL.Add(fs(rv(allsu))+',');//SumSale
                quA.SQL.Add(fs(rv(allma))+',');//SumMax
                if allcli>0 then quA.SQL.Add(fs(rv(allsu/allcli))+',')
                else quA.SQL.Add('0,');//SumAverage
                quA.SQL.Add(its(Cli20)+',');//Client_20
                quA.SQL.Add(its(Cli50)+',');//Client_50
                quA.SQL.Add(its(Cli100)+',');//Client_100
                quA.SQL.Add(its(Cli200)+',');//Client_200
                quA.SQL.Add(its(Cli500)+',');//Client_500
                quA.SQL.Add(its(Cli1000)+',');//Client_1000
                quA.SQL.Add(its(CliM));//Client_Greater
                quA.SQL.Add(')');

{                fmSt.prWH('');
                for n:=0 to quA.SQL.Count-1 do fmSt.prWH(quA.SQL.Strings[n]);
                fmSt.prWH('');
}
                quA.ExecSQL;

                //������� ��� ���������
                for n:=1 to 24 do
                begin
                  asale[n].ch:=0;
                  asale[n].su:=0;
                  asale[n].ma:=0;
                end;

                allpos:=0; allcli:=0;
                allsu:=0; allma:=0;
                Cli20:=0;
                Cli50:=0;
                cli100:=0;
                cli200:=0;
                cli500:=0;
                cli1000:=0;
                cliM:=0;

                CashN:=quCQCheckCash_Code.AsInteger;
                prWH(fmt+'    ����� � '+IntToStr(CashN),Memo1); delay(10);
              end;

              iH:=StrToINtDef(FormatDateTime('hh',quCQCheckTimes.AsDateTime),0)+1;
              if quCQCheckOperation.AsString='P' then //�������
              begin
                asale[iH].su:=asale[iH].su+quCQCheckCheckSum.AsFloat;
                allsu:=allsu+quCQCheckCheckSum.AsFloat;
                allpos:=allpos+quCQCheckQuantPos.AsInteger;

                rSumCh:=quCQCheckCheckSum.AsFloat;

                inc(asale[iH].ch);
                inc(allcli);

                if (rSumCh>=0) and (rSumCh<20) then inc(Cli20);
                if (rSumCh>=20) and (rSumCh<50) then inc(Cli50);
                if (rSumCh>=50) and (rSumCh<100) then inc(Cli100);
                if (rSumCh>=100) and (rSumCh<200) then inc(Cli200);
                if (rSumCh>=200) and (rSumCh<500) then inc(Cli500);
                if (rSumCh>=500) and (rSumCh<1000) then inc(Cli1000);
                if (rSumCh>=1000) then inc(CliM);


                if rSumCh>asale[iH].ma then asale[iH].ma:=rSumCh;
                if rSumCh>allma then allma:=rSumCh;

              end else   //�������
              begin
                asale[iH].su:=asale[iH].su+quCQCheckCheckSum.AsFloat;
                allsu:=allsu+quCQCheckCheckSum.AsFloat;
              end;

              quCQCheck.Next;

              inc(iC); if iC mod 100=0 then
              begin
//                fmSt.prWH(fmt+'  ..'+INtToStr(iC));
                delay(10);
              end;
            end;

            // ���� ��������� �����

            //������
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "CashDay"' );
            quD.SQL.Add('where DateSale='''+ds(iCurD)+'''');
            quD.SQL.Add('and Number='+its(CashN));
            quD.ExecSQL;
            delay(100);

            //���������
            quA.SQL.Clear;
            quA.SQL.Add('INSERT into "CashDay" values (');
            quA.SQL.Add(its(CashN)+',');
            quA.SQL.Add(''''+ds(iCurD)+''',');

            for n:=1 to 24 do
            begin
              quA.SQL.Add(its(asale[n].ch)+',');
              quA.SQL.Add(fs(rv(asale[n].su))+',');
              quA.SQL.Add(fs(rv(asale[n].ma))+',');
              if asale[n].ch>0 then quA.SQL.Add(fs(rv(asale[n].su/asale[n].ch))+',')
              else quA.SQL.Add('0,');
            end;

            quA.SQL.Add(its(allpos)+','); //QuantSale
            quA.SQL.Add(its(allcli)+',');//QuantClient
            quA.SQL.Add(fs(rv(allsu))+',');//SumSale
            quA.SQL.Add(fs(rv(allma))+',');//SumMax
            if allcli>0 then quA.SQL.Add(fs(rv(allsu/allcli))+',')
            else quA.SQL.Add('0,');//SumAverage
            quA.SQL.Add(its(Cli20)+',');//Client_20
            quA.SQL.Add(its(Cli50)+',');//Client_50
            quA.SQL.Add(its(Cli100)+',');//Client_100
            quA.SQL.Add(its(Cli200)+',');//Client_200
            quA.SQL.Add(its(Cli500)+',');//Client_500
            quA.SQL.Add(its(Cli1000)+',');//Client_1000
            quA.SQL.Add(its(CliM));//Client_Greater
            quA.SQL.Add(')');
            quA.ExecSQL; delay(100);

            quCQCheck.Active:=False;

                prWH('  ��',Memo1); delay(10);
              end;

            except;
              prWH('  ������ ��� ��������� ���������� �� ������.',Memo1); delay(10);
            end;
          finally
            prWH('�������� ���������� �� ������ �������.',Memo1); delay(10);
          end;

          //�������� ������ �������������

          prWH('  ������������ ...',Memo1);
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('INSERT into "A_CLOSEHIST" values (');
          quA.SQL.Add(its(prMax('Close')+1)+',');
          quA.SQL.Add(''''+ds(Date-CommonSet.CloseDay)+''',');
          quA.SQL.Add(its(Person.Id)+',');
          quA.SQL.Add(''''+dts(now)+'''');
          quA.SQL.Add(')');
          quA.ExecSQL;

//---------------------------------------------------------
   //�������� �� �������

          if cxCheckBox2.Checked then
          begin
            DateB:=Trunc(cxDateEdit1.Date);
            DateE:=Trunc(cxDateEdit1.Date);

            StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

            try
              try
          quPriorD.Active:=False;
          quPriorD.Active:=True;

          if ptDep.Active=False then ptDep.Active:=True;

          iMainDep:=0;
          ptDep.First;
          while not ptDep.Eof do
          begin
            if ptDepV02.AsInteger>iMainDep then iMainDep:=ptDepID.AsInteger;
            ptDep.Next;
          end;

          for i:=1 to 20 do iDeps[i]:=0;
          i:=1;
          quPriorD.First;
          while (quPriorD.Eof=False)and (i<=20) do
          begin
            iDeps[i]:=quPriorDID.AsInteger;
            quPriorD.Next; inc(i);
          end;
          quPriorD.First;


          par := VarArrayCreate([0,1], varInteger);

          prWH('������ ��������� ('+INtToStr(iMainDep)+')'+StrWk,Memo1);
          for iCurD:=DateB to DateE do
          begin
            CloseTe(taRemove);

            iC:=0;

            prWH('',Memo1);
            prWH('  ��������� ����  '+ FormatDateTime('dd.mm.yyyy',iCurD),Memo1); delay(10);

            ptCQ.Active:=False;
            ptCQ.TableName:='cq'+FormatDateTime('yyyymm',iCurD);
            ptCQ.Active:=True;
            ptCQ.CancelRange;
            CurD:=iCurD;
            ptCQ.SetRange([CurD],[CurD]);
            ptCQ.First;
            while not ptCQ.Eof do
            begin
              iDep:=ptCQGrCode.AsInteger;
              if ptCQCk_CurAbr.AsInteger>0 then //����� ������ ��� �� �����
              begin
                iDep:=ptCQCk_CurAbr.AsInteger;
                ptCQ.Edit;
                ptCQGrCode.AsInteger:=iDep;
                ptCQCk_CurAbr.AsInteger:=0;
                ptCQ.post;
              end;

{             if ptCQGrCode.AsInteger=4 then //������� ������
              begin
                ptCQ.Edit;
                ptCQGrCode.AsInteger:=5;
                ptCQCk_CurAbr.AsInteger:=0;
                ptCQ.post;
                iDep:=5;
              end;
}
//              if (ptCQOperation.AsString='P')and(iDep=iMainDep) then //������������


              if (ptCQOperation.AsString='P')and(iDep=iMainDep) then //������������ ������ (130 - ��� �������)
              begin
                  //����������� ��� ������
                iPriv:=0;
                quPriorD.First;
                while (quPriorD.Eof=False)and(iPriv=0) do
                begin
                  //������� �� ������ ��� � �� ����.
                  vRest:=prFindTRemnDepD(ptCQCode.AsInteger,quPriorDID.AsInteger,iCurD);
                  if vRest>0 then
                  begin //��������� ������� � ������ ������
                    vRestOut:=0;
                    par[0]:=ptCQCode.AsInteger;  //��� ������
                    par[1]:=ptCQGrCode.AsInteger; //�����
                    if taRemove.Locate('Articul;Depart',par,[]) then  vRestOut:=taRemoveQuant.AsFloat;
                    vRest:=vRest-vRestOut-ptCQQuant.AsFloat;
                    if vRest>=0 then
                    begin
                      iPriv:=quPriorDID.AsInteger;
                      break;
                    end;
                  end;
                  quPriorD.Next;
                end;
                if iPriv>0 then //������ ����� � ������������ ���������
                begin
                  iDep:=ptCQGrCode.AsInteger;
                  ptCQ.Edit;
                  ptCQGrCode.AsInteger:=iPriv;
                  ptCQCk_CurAbr.AsInteger:=iDep;
                  ptCQ.post;

                  par[0]:=ptCQCode.AsInteger;  //��� ������
                  par[1]:=iPriv; //�����
                  if taRemove.Locate('Articul;Depart',par,[]) then
                  begin
                    taRemove.Edit;
                    taRemoveQuant.AsFloat:=taRemoveQuant.AsFloat+ptCQQuant.AsFloat;
                    taRemoveRes.AsInteger:=iPriv;
                    taRemove.Post;
                  end else
                  begin
                    taRemove.Append;
                    taRemoveArticul.AsInteger:=ptCQCode.AsInteger;
                    taRemoveDepart.AsInteger:=iPriv;
                    taRemoveQuant.AsFloat:=ptCQQuant.AsFloat;
                    taRemoveRes.AsInteger:=iPriv;
                    taRemove.Post;
                  end;
                end else //�� ������ ����� � ������������ ���������
                begin
                  iPostDep:=prFindTLastDep(ptCQCode.AsInteger);
                  if iPostDep>0 then
                  begin
                    iPostDep:=fTestDep(iPostDep); //��������� ��������� ����� - ����� ������������ ��� ���
                    if iPostDep<>ptCQGrCode.AsInteger then
                    begin
                      iDep:=ptCQGrCode.AsInteger;
                      ptCQ.Edit;
                      ptCQGrCode.AsInteger:=iPostDep;
                      ptCQCk_CurAbr.AsInteger:=iDep;
                      ptCQ.post;
                    end;
                  end;
                end;
              end;
              if (ptCQOperation.AsString='R')and(ptCQGrCode.AsInteger=iMainDep) then
              begin
                iPostDep:=prFindTLastDep(ptCQCode.AsInteger);
                if iPostDep>0 then
                begin
                  iPostDep:=fTestDep(iPostDep); //��������� ��������� ����� - ����� ������������ ��� ���
                  if iPostDep<>ptCQGrCode.AsInteger then
                  begin
                    iDep:=ptCQGrCode.AsInteger;
                    ptCQ.Edit;
                    ptCQGrCode.AsInteger:=iPostDep;
                    ptCQCk_CurAbr.AsInteger:=iDep;
                    ptCQ.post;
                  end;
                end;
              end;

              if ptCQCode.AsInteger=910 then     //������ �� ������
              begin
                iDep:=ptCQGrCode.AsInteger;
                ptCQ.Edit;
                ptCQGrCode.AsInteger:=6;
                ptCQCk_CurAbr.AsInteger:=iDep;
                ptCQ.post;
              end;

              ptCQ.Next;  inc(iC); if iC mod 100=0 then
              begin
                prWH('  ..'+INtToStr(iC),Memo1);
                delay(10);
              end;
            end;

            ptCQ.Active:=False;
            prWH('  ��',Memo1); delay(10);
            taRemove.Active:=False;

            prWH('  �������� ���������� �� �������.',Memo1); delay(10);

            StrTa:='cq'+formatDateTime('yyyymm',iCurD);

            quD.SQL.Clear;
            quD.SQL.Add('delete from "SaleByDepart" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
            quD.ExecSQL;
            quD.SQL.Clear;
            quD.SQL.Add('delete from "SaleByDepartBN" where "DateSale"='''+FormatDateTime('yyyy-mm-dd',iCurD)+'''');
            quD.ExecSQL;

            quCq.Active:=False;
            quCq.SQL.Clear;
            quCq.SQL.Add('select cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card,');
            quCq.SQL.Add('SUM(cq.Ck_Disc) as CorSum');
            quCq.SQL.Add(', sum(cq.Summa) as RSum');

            quCq.SQL.Add(', SUM(if(gd.NDS<=13,cq.Ck_Disc,0))as CorSum10');
            quCq.SQL.Add(', sum(if(gd.NDS<=13,cq.Summa,0))as RSum10');
            quCq.SQL.Add(', SUM(if(gd.NDS>13,cq.Ck_Disc,0))as CorSum18');
            quCq.SQL.Add(', sum(if(gd.NDS>13,cq.Summa,0))as RSum18');
            quCq.SQL.Add('from "'+StrTa+'" cq');
            quCq.SQL.Add('left join Goods gd on gd.ID=cq.Code');
            quCq.SQL.Add('where cq.DateOperation='''+ds(iCurD)+'''');
            quCq.SQL.Add('group by cq.DateOperation, cq.GrCode, cq.Cassir, cq.Cash_Code, cq.Ck_Card');
            quCq.SQL.Add('order by cq.GrCode,cq.Cash_Code,cq.Cassir,cq.Ck_Card');

            quCq.Active:=True;

            quCq.First;
            while not quCq.Eof do
            begin
              iCassir:=quCqCassir.AsInteger;
              if iCassir>1000000 then iCassir:=(iCassir-1000000)*-1;

              quA.SQL.Clear;
              if quCqCk_Card.AsInteger=0 then //���
              begin
                quA.SQL.Add('INSERT into "SaleByDepart" values (');
              end else //������
              begin
                quA.SQL.Add('INSERT into "SaleByDepartBN" values (');
              end;
              quA.SQL.Add(''''+ds(iCurD)+''','); //DateSale
              quA.SQL.Add(its(quCqCash_Code.AsInteger)+',');  //CassaNumber
              quA.SQL.Add(its(quCqGrCode.AsInteger)+',');//Otdel
              quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat))+',');//Summa
              quA.SQL.Add(fs(roundVal(quCqCorSum.AsFloat))+',');//SummaCor
              quA.SQL.Add(its(iCassir)+',');//Employee
//              quA.SQL.Add(fs(roundVal(quCqRSum10.AsFloat))+',');//SummaNDSx10
              quA.SQL.Add(fs(roundVal(quCqRSum.AsFloat-quCqRSum18.AsFloat))+',');//SummaNDSx10
              quA.SQL.Add(fs(roundVal(quCqRSum18.AsFloat))+',');//SummaNDSx20
              quA.SQL.Add(fs(roundVal(quCqCorSum10.AsFloat))+',');//CorNDSx10
              quA.SQL.Add(fs(roundVal(quCqCorSum18.AsFloat))+',');//CorNDSx20
              quA.SQL.Add('0,');//SummaNSPx10
              quA.SQL.Add('0,');//SummaNSPx20
              quA.SQL.Add('0,');//SummaOblNSP
              quA.SQL.Add('0,');//SummaNDSx0
              quA.SQL.Add('0,');//CorNDSx0
              quA.SQL.Add('0,');//SummaNSP0
              quA.SQL.Add('0,');//NDSx10
              quA.SQL.Add('0,');//NDSx20
              quA.SQL.Add('0,');//CorNSPx0
              quA.SQL.Add('0,');//CorNSPx10
              quA.SQL.Add('0,');//CorNSPx20
              quA.SQL.Add('0,');//Rezerv
              quA.SQL.Add('3,');//ShopIndex
              quA.SQL.Add('0,');//RezervSumma
              quA.SQL.Add('0,');//RezervSumNSP
              quA.SQL.Add('0,');//RezervSkid
              quA.SQL.Add('0,');//RezervSkidNSP
              quA.SQL.Add('0,');//SenderSumma
              quA.SQL.Add('0,');//SenderSumNSP
              quA.SQL.Add('0,');//SenderSkid
              quA.SQL.Add('0');//SenderSkidNSP
              quA.SQL.Add(')');
              quA.ExecSQL;

              quCq.Next;
            end;

            prWH('  C��������� ���������.',Memo1); delay(10);
            prWH('  �������� ����� ...',Memo1);

            prRecalcTO(DateB,DateE,Memo1,1,0);
          end;

              except;
              end;

            finally
              quPriorD.Active:=False;
              prWH('�������� �������.',Memo1); delay(10);
            end;


          end;


//---------------------------------------------------------

  //������������
        //����� ������� ��������� - ���� �� ��� ���
        {
        bStart:=True;
        for iCurD:=DateB to DateE do
        begin
            //������ Id ��������� �� �������� ������ � ���
          StrWk:=FormatDateTime('mmdd',iCurD);
          iD:=StrToINtDef(StrWk,0)*1000;

          if prTestGen(iCurD,Id)=False then bStart:=False;
        end;

        if bStart=False then
        begin
          ShowMessage('�� ��������� ������ ���� ��������������� ������. ��������� ������� �� ��������.');
          close;
        end;
        }

        //������ ��������� ����� - ������� ������� ������

        StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

        prWH(fmt+'',Memo1);
        prWH(fmt+'������ ��������('+StrWk+') ... �����.',Memo1);
        try
          for iCurD:=DateB to DateE do
          begin
            prWH('',Memo1);
            prWH('    - ������ ���������.. '+ FormatDateTime('dd.mm.yyyy',iCurD),Memo1);
            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "TovCassa"' );
            quD.SQL.Add('where  DateReport='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            prWH('    - ������ cn .. '+ FormatDateTime('dd.mm.yyyy',iCurD),Memo1);
            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "cn'+FormatDateTime('yyyymm',iCurD)+'"' );
            quD.SQL.Add('where  RDate='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            //������ Id ��������� �� �������� ������ � ���
            StrWk:=FormatDateTime('mmdd',iCurD);
            sY:=FormatDateTime('yy',iCurD);
            iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);
//            iD:=StrToINtDef(StrWk,0)*1000;

            prWH(fmt+'    - ��������� �������������� ..',Memo1);

            prWH(fmt+'      ��������� ..',Memo1);
            if ptCM2.Active=False then ptCM2.Active:=True;
            CurDate:=iCurD;
            ptCM2.CancelRange;
            ptCM2.SetRange([CurDate],[CurDate]);
            ptCM2.First;
            prWH(fmt+'        �������� �������� ..',Memo1);

            CloseTe(meCg);
            iC:=0;
            while not ptCM2.Eof do
            begin
{select * from "CardGoodsMove"
where DOCUMENT=:IDDOC
and DOC_TYPE=:DT
}
              //������� ��� ������� � �����������
              if (ptCM2DOCUMENT.AsInteger=iD)and(ptCM2DOC_TYPE.AsInteger=7) then
              begin
                meCg.Append;
                meCgCard.AsInteger:=ptCM2CARD.AsInteger;
                meCg.Post;

                ptCM2.Delete;
                inc(iC);
              end else ptCM2.Next;
              if iC>0 then
                if iC mod 100 = 0 then
                begin
                  StatusBar1.Panels[0].Text:=its(iC);
                  delay(20);
                end;
            end;
            ptCM2.Active:=False;

            prWH(fmt+'        �������� �������� ..',Memo1);
            iC:=0;
            meCg.First;
            while not meCg.Eof do
            begin
              prReCalcTMove(meCgCard.AsInteger,iCurD);

              meCg.Next;
              inc(iC);
              if iC mod 100 = 0 then
              begin
                StatusBar1.Panels[0].Text:=its(iC);
                delay(20);
              end;
            end;

            prWH('��.',Memo1);
          end;
        finally
          prWH(fmt+'������� �������� ��������.',Memo1);
          prWH(fmt+'',Memo1);
          quGens.Active:=False;
          quGens.Active:=True;
        end;

        StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

        prWH('������ ('+StrWk+') ... �����.',Memo1);
        try
          for iCurD:=DateB to DateE do
          begin
            //������ Id ��������� �� �������� ������ � ���
            StrWk:=FormatDateTime('mmdd',iCurD);
            sY:=FormatDateTime('yy',iCurD);
            iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);
//            iD:=StrToINtDef(StrWk,0)*1000;

            prWH('    - ��������� ..',Memo1);
            quCashGood.Active:=False;
            quCashGood.SQL.Clear;
            quCashGood.SQL.Add('select');
            quCashGood.SQL.Add('cg.GrCode,');
            quCashGood.SQL.Add('cg.Code,');
            quCashGood.SQL.Add('de.Name,');
            quCashGood.SQL.Add('Sum(cg.Quant) as Quant,');
            quCashGood.SQL.Add('Sum(cg.Quant*cg.Price) as RSumD,');
            quCashGood.SQL.Add('Sum(cg.Summa) as RSum');
            quCashGood.SQL.Add('from "cq'+FormatDateTime('yyyymm',iCurD)+'" cg');
            quCashGood.SQL.Add('left join Depart de on de.ID=cg.GrCode');
            quCashGood.SQL.Add('where cg.DateOperation='''+ds(iCurD)+'''');
            quCashGood.SQL.Add('group by cg.GrCode, cg.Code, de.Name');
            quCashGood.SQL.Add('order by cg.GrCode, cg.Code');
            quCashGood.Active:=True;

            prWH(fmt+'    - ������������ ..',Memo1);
            idep:=-1;
            iC:=0;
            rSum:=0;
            rSumD:=0;

            quCashGood.First;
            while not quCashGood.Eof do
            begin
              if iDep<>quCashGoodGrCode.AsInteger then
              begin
                if iDep>0 then
                begin //��������
                  quA.SQL.Clear;
                  quA.SQL.Add('INSERT into "TovCassa" values (');
                  quA.SQL.Add(its(iDep)+',');//Depart
                  quA.SQL.Add(''''+ds(iCurD)+''',');//DateReport
                  quA.SQL.Add(fs(rSum)+',');//Summa
                  quA.SQL.Add(fs(rSumd-rSum)+',');//Skidka
                  quA.SQL.Add('0,');//ChekBuh
                  quA.SQL.Add('0,');//RezervSumma
                  quA.SQL.Add('0,');//RezervSkidka
                  quA.SQL.Add('0,');//SenderSumma
                  quA.SQL.Add('0,');//SenderSkidka
                  quA.SQL.Add(''''+'''');//Rezerv
                  quA.SQL.Add(')');
                  quA.ExecSQL;
                end;

                iDep:=quCashGoodGrCode.AsInteger;
                rSum:=0;
                rSumD:=0;
              end;

              rSum:=rSum+quCashGoodRSum.AsFloat;
              rSumD:=rSumD+quCashGoodRSumD.AsFloat;

              //����� ��������� � cn � �������� ��������������
              try
                quA.SQL.Clear;
                quA.SQL.Add('INSERT into "cn'+FormatDateTime('yyyymm',iCurD)+'" values (');
                quA.SQL.Add('0,');//XZ
                quA.SQL.Add(''''+ds(iCurD)+''',');//RDate

                if abs(quCashGoodQuant.AsFloat)>0 then quA.SQL.Add(fs(RoundVal(quCashGoodRSumD.AsFloat/quCashGoodQuant.AsFloat))+',')
                                                else quA.SQL.Add('0,');//RPrice
                quA.SQL.Add(its(quCashGoodGrCode.AsInteger)+',');//Depart
                quA.SQL.Add(its(quCashGoodCode.AsInteger)+',');//Code
                quA.SQL.Add('0,');//XZ1
                if abs(quCashGoodQuant.AsFloat)>0 then quA.SQL.Add(fs(RoundVal(quCashGoodRSumD.AsFloat/quCashGoodQuant.AsFloat))+',')//RSum
                                                else quA.SQL.Add('0,');//RSum
                quA.SQL.Add('0,');//XZ2
                quA.SQL.Add('0,');//XZ3
                quA.SQL.Add(fs(quCashGoodQuant.AsFloat)+',');//Quant
                quA.SQL.Add(fs(quCashGoodRSum.AsFloat));//RSumCor
                quA.SQL.Add(')');
                quA.ExecSQL;
              except;
              end;
//              prAddMoveId(quCashGoodGrCode.AsInteger,quCashGoodCode.AsInteger,iCurD,iD,7,(-1)*quCashGoodQuant.AsFloat);
              prAddTMoveId(quCashGoodGrCode.AsInteger,quCashGoodCode.AsInteger,iCurD,iD,7,(-1)*quCashGoodQuant.AsFloat);

              quCashGood.Next; inc(iC); StatusBar1.Panels[0].Text:=its(iC);  Delay(10);
            end;

            prWH('     '+ds(iCurD)+' ���������� '+its(iC)+' �������.',Memo1);
            if iDep>0 then
            begin
              quA.SQL.Clear;
              quA.SQL.Add('INSERT into "TovCassa" values (');
              quA.SQL.Add(its(iDep)+',');//Depart
              quA.SQL.Add(''''+ds(iCurD)+''',');//DateReport
              quA.SQL.Add(fs(rSum)+',');//Summa
              quA.SQL.Add(fs(rSumd-rSum)+',');//Skidka
              quA.SQL.Add('0,');//ChekBuh
              quA.SQL.Add('0,');//RezervSumma
              quA.SQL.Add('0,');//RezervSkidka
              quA.SQL.Add('0,');//SenderSumma
              quA.SQL.Add('0,');//SenderSkidka
              quA.SQL.Add(''''+'''');//Rezerv
              quA.SQL.Add(')');
              quA.ExecSQL;
            end;

            quCashGood.Active:=False;
          end;
        finally
          prWH('������� ��������� ��������.',Memo1);
          quGens.Active:=False;
          quGens.Active:=True;
        end;

//---------------------------------------------------------

// ��� �� ������� �������� ��������������

//     { ����� ��������

//         DateBM:=DateE-1; //��������� 2-� ���
        DateBM:=DateE-7; //��������� 7� ����
        if cxCheckBox1.Checked=False then
          if (frac(Time)>=0.29166) then DateBM:=DateE;   // c 7 - ����  - �������������� �� 7 ���� �� �����

        if DateBM<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateBM)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
        else StrWk:=FormatDateTime('dd.mm.yyyy',DateBM);

        prWH('',Memo1);
        prWH('  ������ �������� �������������� ('+StrWk+') ... �����.',Memo1);
        try
          prWH('',Memo1);
          prWH('     ������ ������ ('+StrWk+') ... �����.',Memo1);

          ptCM.Active:=False;
          ptCG.Active:=False;
          ptCM1.Active:=False;
          ptCM2.Active:=False;

          CurDate:=DateBM;
          CurDate1:=DateE;

          ptCM2.Active:=True;
          ptCM2.CancelRange;
          ptCM2.SetRange([CurDate],[CurDate1]);
          ptCM2.First;
          iC:=0;
          while not ptCM2.Eof do
          begin
            ptCM2.Delete;
            inc(iC);
            if iC mod 100 = 0 then
            begin
              StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
              delay(20);
            end;
          end;
          ptCM2.Active:=False;

          prWH('     ������ ��.',Memo1);
          prWH('',Memo1);

          for iCurD:=DateBM to DateE do
          begin //������������ �� �����
            CurD:=iCurD;

            prWH('     ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD),Memo1); delay(100);

            prWH('       - ������� ..',Memo1); delay(10);

            try
              quTTNIn.Active:=False;
              quTTNIn.ParamByName('DATEB').AsDate:=iCurD;
              quTTNIn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
              quTTNIn.Active:=True;

              quTTNIn.First; iC:=0;
              while not quTTNIn.Eof do
              begin
                if quTTnInAcStatus.AsInteger=3 then
                begin
                  quSpecIn1.Active:=False;
                  quSpecIn1.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                  quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                  quSpecIn1.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                  quSpecIn1.Active:=True;

                  quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
                  while not quSpecIn1.Eof do
                  begin
                    prAddTMoveId(quTTnInDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecIn1Kol.AsFloat);
                    quSpecIn1.Next;
                  end;
                  quSpecIn1.Active:=False;


                  if quTTnInSummaTara.AsFloat<>0 then
                  begin //�������� �� ����
                    quSpecInT.Active:=False;
                    quSpecInT.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
                    quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
                    quSpecInT.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
                    quSpecInT.Active:=True;

                    quSpecInT.First;  // ��������������
                    while not quSpecInT.Eof do
                    begin
                      prAddTaraMoveId(quTTnInDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTnInDateInvoice.AsDateTime),quTTnInID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);
                      quSpecInT.Next;
                    end;
                    quSpecInT.Active:=False;
                  end;
                end;
                quTTNIn.Next;
                inc(iC);
                StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
              end;

              //�������� ���������� ������������
              prWH('       - �������� ���������� ������������ ..',Memo1); delay(10);
              ptInLn.Active:=False ; ptINLn.Active:=True;
              if ptDep.Active=False then ptDep.Active:=True;

              ptTTNIn.Active:=False; ptTTNIn.Active:=True;
              ptTTnIn.IndexFieldNames:='Depart;DateInvoice;Number';

              iC:=0; iDel:=0;

              ptDep.First;
              while not ptDep.Eof do
              begin
                if ptDepStatus1.AsInteger=9 then
                begin
                  ptInLn.CancelRange;
                  ptInLn.SetRange([ptDepId.AsInteger,CurD],[ptDepId.AsInteger,CurD]);
                  ptInLn.First;
                  while not ptInLn.Eof do
                  begin
                    if ptTTnIn.FindKey([ptInLnDepart.AsInteger,ptInLnDateInvoice.AsDateTime,ptInLnNumber.AsString]) then
                    begin
                     ptInLn.Next;
                    end
                    else
                    begin
                      inc(iDel);
                      ptInLn.Delete;
                    end;

                    inc(iC);
                    if iC mod 100 =0 then
                    begin
                       prWH('       - ���������� '+its(iC)+'  ���. '+its(iDel),Memo1); delay(10);
                    end;
                  end;
                end;

                ptDep.Next;
              end;
              prWH('       - ��. ����� '+its(iC)+'  ���. '+its(iDel),Memo1); delay(10);
            finally
            end;

            prWH('       - �������� ..',Memo1); delay(10);
            try
              quTTNOut.Active:=False;
              quTTNOut.ParamByName('DATEB').AsDate:=iCurD;
              quTTNOut.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
              quTTNOut.Active:=True;

              quTTNOut.First; iC:=0;
              while not quTTNOut.Eof do
              begin
                if quTTNOutAcStatus.AsInteger=3 then
                begin
                  quSpecOut2.Active:=False;
                  quSpecOut2.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                  quSpecOut2.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                  quSpecOut2.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                  quSpecOut2.Active:=True;

                  quSpecOut2.First;  //��������� ����� ���  � ��������� ��������������
                  while not quSpecOut2.Eof do
                  begin
                    prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOut2Kol.AsFloat*-1));


                    if quSpecOut2ChekBuh.AsBoolean=True then
                    begin
                      if abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat)>0.001 then
                      begin
                        rQSpis:=abs(quSpecOut2SumVesTara.AsFloat-quSpecOut2Kol.AsFloat);
                        prAddTMoveId(quTTnOutDepart.AsInteger,quSpecOut2CodeTovar.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,4,(rQSpis*-1));
                      end;
                    end;

                    quSpecOut2.Next;
                  end;
                  quSpecOut2.Active:=False;

                  if quTTnOutSummaTara.AsFloat<>0 then
                  begin //�������� �� ����
                    quSpecOutT.Active:=False;
                    quSpecOutT.ParamByName('IDEP').AsInteger:=quTTnOutDepart.AsInteger;
                    quSpecOutT.ParamByName('SDATE').AsDate:=Trunc(quTTnOutDateInvoice.AsDateTime);
                    quSpecOutT.ParamByName('SNUM').AsString:=quTTnOutNumber.AsString;
                    quSpecOutT.Active:=True;

                    quSpecOutT.First;  // ��������������
                    while not quSpecOutT.Eof do
                    begin
                      prAddTaraMoveId(quTTnOutDepart.AsInteger,quSpecOutTCodeTara.AsInteger,Trunc(quTTnOutDateInvoice.AsDateTime),quTTnOutID.AsInteger,2,(quSpecOutTKolMest.AsFloat*-1),quSpecOutTCenaTara.AsFloat);

                      quSpecOutT.Next;
                    end;
                    quSpecOutT.Active:=False;
                  end;
                end;
                quTTNOut.Next;
                inc(iC);
                StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
              end;
            finally
            end;

            prWH('       - ����������� ..',Memo1); delay(10);
            try
              quTTNVn.Active:=False;
              quTTNVn.ParamByName('DATEB').AsDate:=iCurD;
              quTTNVn.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
              quTTNVn.Active:=True;

              quTTNVn.First; iC:=0;
              while not quTTNVn.Eof do
              begin
                if quTTnVnAcStatusP.AsInteger=3 then
                begin
                  quSpecVn1.Active:=False;
                  quSpecVn1.ParamByName('IDEP').AsInteger:=quTTnVnDepart.AsInteger;
                  quSpecVn1.ParamByName('SDATE').AsDate:=Trunc(quTTnVnDateInvoice.AsDateTime);
                  quSpecVn1.ParamByName('SNUM').AsString:=quTTnVnNumber.AsString;
                  quSpecVn1.Active:=True;

                  quSpecVn1.First;  //��������� ����� ���  � ��������� ��������������
                  while not quSpecVn1.Eof do
                  begin
                    prAddTMoveId(quTTnVnDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,4,(-1)*quSpecVn1Kol.AsFloat);

                    if (quTTnVnDepart.AsInteger<>quTTnVnFlowDepart.AsInteger) and (pos('��������',quTTnVnNameD2.AsString)=0) then //������ ������ - ����� ������
                    begin
                      prAddTMoveId(quTTnVnFlowDepart.AsInteger,quSpecVn1CodeTovar.AsInteger,Trunc(quTTnVnDateInvoice.AsDateTime),quTTnVnID.AsInteger,3,quSpecVn1Kol.AsFloat);
                    end;

                    quSpecVn1.Next;
                  end;
                  quSpecVn1.Active:=False;
                end;
                quTTNVn.Next;
                inc(iC);
                StatusBar1.Panels[0].Text:=IntToStr(iC); delay(20);
              end;
            finally
            end;

            prWH('       - ���������� ..',Memo1); delay(10);

            ptCn.Active:=False;
            ptCn.TableName:='cn'+FormatDateTime('yyyymm',iCurD);
            try
              ptCn.Active:=True;
              ptCn.CancelRange;
              CurD:=iCurD;
              ptCn.SetRange([CurD],[CurD]);
              ptCn.First;

              iC:=0;
              while not ptCn.Eof do
              begin
                StrWk:=FormatDateTime('mmdd',iCurD);
                sY:=FormatDateTime('yy',iCurD);
                iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);
//                iD:=StrToINtDef(StrWk,0)*1000;

                prAddTMoveId(ptCnDepart.AsInteger,ptCnCode.AsInteger,iCurD,iD,7,(-1)*ptCnQuant.AsFloat);

                ptCn.Next; inc(iC);
                if iC mod 100 = 0 then
                begin
                  StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                  delay(20);
                end;
              end;
            except
              ptCn.Active:=False;
            end;

            prWH('     - �������������� ..',Memo1); delay(10);

            try
              quTTNInv.Active:=False;
              quTTNInv.ParamByName('DATEB').AsDate:=iCurD;
              quTTNInv.ParamByName('DATEE').AsDate:=iCurD;  // ��� <=
              quTTNInv.Active:=True;

              quTTnInv.First;
              while not quTTnInv.Eof do
              begin
                if quTTnInvStatus.AsString='�' then
                begin
                  iDep:=quTTnInvDepart.AsInteger;

                  iT:=quTTnInvxDopStatus.AsInteger;
                  prWH('     - '+its(quTTnInvInventry.AsInteger)+' ('+its(iT)+')',Memo1); delay(10);

                  if iT=0 then
                  begin
                    quSpecInv.Active:=False;
                    quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
                    quSpecInv.ParamByName('ID').AsInteger:=quTTnInvInventry.AsInteger;
                    quSpecInv.Active:=True;

                    iC:=0;
                    quSpecInv.First;  //��������� ����� ���  � ��������� ��������������
                    while not quSpecInv.Eof do
                    begin
                      if (quSpecInvItem.AsInteger>0) then
                        rRemn:=prFindTRemnDepD(quSpecInvItem.AsInteger,iDep,Trunc(quTTnInvDateInventry.AsDateTime))
                      else rRemn:=0;

                      if abs(quSpecInvQuantActual.AsFloat-rRemn)>0.001 then
                      begin
// ������� �� ���� � ��� ���� ������ �������  prDelTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
                        prAddTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-rRemn));
                      end;

                      quSpecInv.Next;
                      inc(iC);
                      if iC mod 100 = 0 then
                      begin
                        fmMainAutoCloseCash.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                        delay(20);
                      end;
                    end;
                    quSpecInv.Active:=False;
                  end;
                  if iT=1 then
                  begin
                    if ptInvLine1.Active=False then ptInvLine1.Active:=True;
                    ptInvLine1.Refresh;
                    ptInvLine1.CancelRange;
                    ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
                    iC:=0;
                    ptInvLine1.First;
                    while not ptInvLine1.Eof do
                    begin
                      if (ptInvLine1ITEM.AsInteger>0) then
                        rRemn:=prFindTRemnDepD(ptInvLine1ITEM.AsInteger,iDep,Trunc(quTTnInvDateInventry.AsDateTime))
                      else rRemn:=0;

                      if abs(ptInvLine1QUANTF.AsFloat-rRemn)>0.001 then
                      begin
                        prAddTMoveId(quTTnInvDepart.AsInteger,ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(ptInvLine1QUANTF.AsFloat-rRemn));
                      end;

                      ptInvLine1.Next;   inc(iC);

                      if iC mod 100 = 0 then
                      begin
                        fmMainAutoCloseCash.StatusBar1.Panels[0].Text:=IntToStr(iC div 100);
                        delay(20);
                      end;
                    end;
                  end;
                end;
                quTTnInv.Next; delay(10);
              end;
            finally

            end;

            prWH('   ��������� ���� '+FormatDateTime('dd.mm.yyyy',iCurD)+'  ��.',Memo1); delay(10);

            prWH('',Memo1);delay(10);
          end;

        finally
        end;

        //�������� �������� �� ����

        try
          prWH('',Memo1);
          prWH('  �������� �������� ... �����.',Memo1);

          quCds.Active:=False;
          quCds.Active:=True;

          prWH('    ����� - '+INtToStr(quCds.RecordCount),Memo1);

          iC:=0;
          quCds.Last;
          while not quCDS.Bof do
          begin
            rQ:=prFindTabRemn(quCdsID.AsInteger);

            if abs(rQ-quCdsReserv1.AsFloat)>0.1 then
            begin //���� ��������
              prSetR(quCdsID.AsInteger,rQ);
            end;

            quCds.Prior; inc(iC);
            if iC mod 100 =0 then begin StatusBar1.Panels[0].Text:=IntToStr(iC); delay(10); end;
          end;
          quCds.Active:=False;
        finally
          prWH('    �������� �������� ('+its(iC)+')',Memo1);
          prWH('',Memo1);
        end;

        try
          prWH('',Memo1);
          prWH('  �������� �������������� ... �����.',Memo1);

          if taCards.Active=False then taCards.Active:=True;

          iD:=0;
          iC:=0;
          taCards.First;
          while not taCards.Eof do
          begin
            if taCardsStatus.asinteger<100 then
            begin
              if CommonSet.Single=1 then
              begin
                rQ:=prFindLastDateRemn(taCardsID.AsInteger,LDate);
                iStatus:=1;
                iNov:=0;


                //�� �������
                if abs(rQ)<0.002 then rQ:=0;

                iLastDayIn:=prFindTFirstDate(taCardsID.AsInteger);
                if iLastDayIn=0 then iNov:=1 //�������
                else
                  if (Date-iLastDayIn)< CommonSet.DaysNoMove then iNov:=1; //91 �� ���������

                sName:=OemToAnsiConvert(taCardsName.AsString);
                if sName[1]='*' then
                begin
                  if abs(rQ)<0.002 then
                  begin
                    if (Date-LDate)>CommonSet.DaysNoMove then  //91 �� ���������
                    begin
                      iStatus:=255;  //����� ����� ���-��

                      taCards.Edit;
                      taCardsStatus.asinteger:=255;
                      taCardsReserv1.AsFloat:=rQ;
//                      taCardsV05.asinteger:=iNov;
                      taCards.Post;

                      inc(iD);
                    end;
                  end;
                end;
              end else
              begin
                sName:=OemToAnsiConvert(taCardsName.AsString);
                if sName[1]='*' then
                begin
                  if taCardsReserv1.AsFloat<-0.001 then //��������������, � ������� ������� ���� �������������
                  begin
                    rQ:=prFindTabRemn(taCardsID.AsInteger);

                    if rQ<0.001 then //�� ������� � ������� �������������
                    begin
                      prWH(IntToStr(iC)+' ������� � ����.',Memo1);

                      if abs(rQ)<0.002 then rQ:=0;

                      taCards.Edit;
                      taCardsStatus.asinteger:=255;
                      taCardsReserv1.AsFloat:=rQ;
//                    taCardsV05.asinteger:=iNov;
                      taCards.Post;

                      //������� � ���� DELETED=1
                      prAddPosFB(1,taCardsID.AsInteger,0,0,taCardsGoodsGroupID.AsInteger,0,0,'','',OemToAnsiConvert(taCardsName.AsString),'','',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));
                      //Function prAddPosFB(ITYPE,ARTICUL,GROUP1,GROUP2,GROUP3,CLIINDEX,DELETED:INTEGER; BARCODE,CARDSIZE,FULLNAME,MESSUR,PASSW:String;QUANT,DSTOP,PRESISION,PRICE:Real):Boolean;

                      inc(iD);
                    end;
                  end;
                end;
              end;
            end else //����������
            begin
              if taCardsReserv1.AsFloat>0.001 then //��������������, � ������� ������� ���� �������������
              begin
                rQ:=prFindTabRemn(taCardsID.AsInteger);

                taCards.Edit;
                taCardsStatus.asinteger:=1;
                taCardsReserv1.AsFloat:=rQ;
                taCards.Post;

                if rQ>0.001 then
                begin
                  //������ �� �����
                  if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

                  prAddPosFB(1,taCardsID.AsInteger,0,0,taCardsGoodsGroupID.AsInteger,0,1,taCardsBARCODE.AsString,'',OemToAnsiConvert(taCardsName.AsString),sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));
                  //Function prAddPosFB(ITYPE,ARTICUL,GROUP1,GROUP2,GROUP3,CLIINDEX,DELETED:INTEGER; BARCODE,CARDSIZE,FULLNAME,MESSUR,PASSW:String;QUANT,DSTOP,PRESISION,PRICE:Real):Boolean;

                  quBars.Active:=False;
                  quBars.ParamByName('GOODSID').AsInteger:=taCardsID.AsInteger;
                  quBars.Active:=True;

                  prAddCashLoad(taCardsID.AsInteger,5,rv(taCardsCena.AsFloat),0);

                  quBars.First;
                  while not quBars.Eof do
                  begin
                    if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                    else sCSz:='QUANTITY';

                    prAddPosFB(2,taCardsID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(taCardsCena.AsFloat));

                    quBars.Next;
                  end;
                  quBars.Active:=False;

                end;
              end;
            end;

            taCards.Next;
            inc(iC);
            if iC mod 1000 = 0 then
            begin
              StatusBar1.Panels[0].Text:=IntToStr(iC)+' '+IntToStr(iD);
              delay(10);
            end;
          end;
          prWH(IntToStr(iC)+' '+IntToStr(iD),Memo1);
          taCards.Active:=False;
        except
        end;
        prWH('�������������� ��.',Memo1);

        //�������� ����������
        prWH('�������� ����������. (�� �������� ���������)',Memo1);

        try
          quTTNInv.Active:=False;
          quTTNInv.ParamByName('DATEB').AsDate:=DateBM;
          quTTNInv.ParamByName('DATEE').AsDate:=DateE;  // ��� <=
          quTTNInv.Active:=True;

          if ptInvLine1.Active=False then ptInvLine1.Active:=True;

          quTTNInv.First;
          while not quTTNInv.Eof do
          begin
            if quTTNInvItem.AsInteger=0 then //�� ��������
            begin
//              ptInvLine1.Refresh;

              prWH(' - �������� '+quTTnInvNumber.AsString,Memo1);

              iC:=0;

              ptInvLine1.CancelRange;
              ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
              ptInvLine1.First;
              while not ptInvLine1.Eof do
              begin
                if (ptInvLine1ITEM.AsInteger>0) then
                begin
                  rRemn:=prFindTRemnDepDInv(ptInvLine1ITEM.AsInteger,quTTnInvDepart.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime))
                end else rRemn:=0;

                if abs(rRemn-ptInvLine1QUANTR.AsFloat)>0.001 then
                begin
                  prCalcSumInRemn(ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvDepart.AsInteger,rRemn,rSumR);

                  ptInvLine1.Edit;
                  ptInvLine1QUANTR.AsFloat:=rRemn;
                  ptInvLine1QUANTD.AsFloat:=ptInvLine1QUANTF.AsFloat-rREmn;
                  ptInvLine1SUMR.AsFloat:=rv(rREmn*ptInvLine1PRICER.AsFloat);
                  ptInvLine1SUMD.AsFloat:=ptInvLine1SUMF.AsFloat-rv(rREmn*ptInvLine1PRICER.AsFloat);
                  ptInvLine1SUMRSS.AsFloat:=rSumR;
                  ptInvLine1SUMDSS.AsFloat:=ptInvLine1SUMFSS.AsFloat-rSumR;
                  ptInvLine1QUANTC.AsFloat:=0;
                  ptInvLine1SUMC.AsFloat:=0;
                  ptInvLine1SUMRSC.AsFloat:=0;
                  ptInvLine1.Post;
                end;
                ptInvLine1.Next;  inc(iC);

                if iC mod 300 =0 then
                begin
                  prWH('     ���������� '+its(iC),Memo1); delay(10);
                end;
              end;
              prWH('     ���������� '+its(iC),Memo1); delay(10);
            end;
            quTTNInv.Next; delay(10);
          end;

          quTTNInv.Active:=True;
        finally
          prWH('�������� ���������� ��.',Memo1);
        end;

      finally
        cxButton1.Enabled:=True;
        dmPx.Session1.Active:=False;
      end;

      prWH('��.',Memo1);

      if (frac(Time)>=0.041)and(frac(Time)<=0.29166) then
      begin
//        acCashLoad.Execute;
      end;
    end;
  end; //�������� �� ����
//    end;
end;

procedure TfmMainAutoCloseCash.cxButton1Click(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxCheckBox1.Checked:=False;
  cxCheckBox1.Caption:='������ ����������.';
  delay(10);
  acStart.Execute;

end;

procedure TfmMainAutoCloseCash.acCashLoadExecute(Sender: TObject);
Var iStep,iC,iZ:INteger;
    bTr:Boolean;
    sPre:String;
    sDb:String;

  function sTi:String;
  begin
    sTi:=FormatDateTime('hh:nn sss',now)+' ';
  end;

begin
  //������ �������� ����
  with dmMt do
  with dmMC do
  with dmFb do
  begin
    prWH('',Memo1);
    prWH('������ ��������� ����',Memo1);

//    if ptCashPath.Active=False then ptCashPath.Active:=True else ptCashPath.Refresh;
    quCashPath.Active:=False;
    quCashPath.Active:=True;

    quCashPath.First;
    while not quCashPath.Eof do
    begin
      sDb:=quCashPathPATH.AsString;

      prWH('   ����� - '+its(quCashPathID.AsInteger)+'('+sDb+')',Memo1);

      try

        Casher.Close;
        Casher.DBName:=sDb;
        Casher.Open;
        if Casher.Connected then
        begin
          prWH(sTi+'  ���� FB ��.',Memo1);
          prWH(sTi+'    ������ ������.',Memo1);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from CARDSCLA');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except
            prWH(sTi+'    ������.',Memo1);
          end;

          prWH(sTi+'    ������ ��.',Memo1);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from BAR');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except
            prWH(sTi+'    ������.',Memo1);
          end;

          prWH(sTi+'    ������ ����-������.',Memo1);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from DISCSTOP');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except
            prWH(sTi+'    ������.',Memo1);
          end;

          prWH('',Memo1);

          if taCards.Active=False then taCards.Active:=True
          else taCards.Refresh;

          if ptPluLim.Active=False then ptPluLim.Active:=True
          else ptPluLim.Refresh;

          if ptBar.Active=False then ptBar.Active:=True
          else ptBar.Refresh;
          ptBar.IndexFieldNames:='GoodsID';

          iStep:=taCards.RecordCount div 100;
          if iStep=0 then iStep:=taCards.RecordCount;
          iC:=0;
          iZ:=0;

          prWH(sTi+'    �������� ��������.('+its(taCards.RecordCount)+')',Memo1);

          cxProgressBar1.Position:=0;
          cxProgressBar1.Visible:=True;

          taCardsFB.Active:=True;
          taBarFB.Active:=True;
          taDCStopFB.Active:=True;

          trUpd1.StartTransaction;
          trUpd2.StartTransaction;
          trUpd3.StartTransaction;

          taCards.First;
          while not taCards.Eof do
          begin
            if taCardsStatus.AsInteger<100 then bTr:=True else bTr:=False;

            if bTr then //������
            begin
              sPre:='';
              if ptPluLim.FindKey([taCardsID.AsInteger]) then
                if ptPluLimPercent.AsFloat>99 then sPre:='-';

              try
                taCardsFB.Append;
                taCardsFBARTICUL.AsString:=its(taCardsID.AsInteger);
                taCardsFBCLASSIF.AsInteger:=taCardsSubGroupID.AsInteger;
                taCardsFBDEPART.AsInteger:=0;
                taCardsFBNAME.AsString:=sPre+OemToAnsiConvert(taCardsFullName.AsString);
                taCardsFBCARD_TYPE.AsInteger:=taCardsTovarType.AsInteger+1;
                if taCardsEdIzm.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
                taCardsFBPRICE_RUB.AsFloat:=rv(taCardsCena.AsFloat);
                taCardsFBDISCQUANT.AsFloat:=0;
                taCardsFBDISCOUNT.AsFloat:=0;
                taCardsFBSCALE.AsString:='NOSIZE';
                taCardsFB.Post;

                ptBar.CancelRange;
                ptBar.SetRange([taCardsID.AsInteger],[taCardsID.AsInteger]);
                ptBar.First;
                while not ptBar.Eof do
                begin
                  try
                    taBarFB.Append;
                    taBarFBBARCODE.AsString:=ptBarID.AsString;
                    taBarFBCARDARTICUL.AsString:=its(taCardsID.AsInteger);
                    if ptBarBarStatus.AsInteger=0 then  taBarFBCARDSIZE.AsString:='NOSIZE' else taBarFBCARDSIZE.AsString:='QUANTITY';
                    taBarFBQUANTITY.AsFloat:=ptBarQuant.AsFloat;
                    taBarFBPRICERUB.AsFloat:=0;
                    taBarFB.Post;
                  except
                    taBarFB.Cancel;
                    prWH(sTi+'    ������ - '+its(taCardsID.AsInteger)+' '+ptBarID.AsString,Memo1);
                  end;

                  ptBar.Next;
                end;
                {
                if sPre='-' then
                begin
                  taDCStopFB.Append;
                  taDCSTOPFBART.AsInteger:=taCardsID.AsInteger;
                  taDCSTOPFBCOMMENT.AsString:='no discount';
                  taDCStopFB.Post;
                end;}
              except
//                prWH(sTi+'    ������ - '+its(taCardsID.AsInteger)+' '+ptBarID.AsString,Memo1);
              end;
              inc(iZ);
            end;

            taCards.Next;
            inc(iC);
            if (iC mod iStep = 0) then
            begin
              cxProgressBar1.Position:=(iC div iStep);
              delay(10);
            end;
//            if bStop then break;
          end;

          trUpd1.Commit;
          trUpd2.Commit;
          trUpd3.Commit;
          delay(100);

          taCardsFB.Active:=False;
          taBarFB.Active:=False;
          taDCStopFB.Active:=False;

          cxProgressBar1.Position:=100;
          prWH(sTi+'    �������� Ok.('+its(iZ)+')',Memo1);
          delay(200);
          cxProgressBar1.Visible:=False;

          ptBar.IndexFieldNames:='ID';
        end;
        prWH(sTi+'  ��������� ���� FB.',Memo1);
        Casher.Close;
      except
        prWH(sTi+'  ������ �������� ���� FB.',Memo1);
      end;
      quCashPath.Next;
    end;

    quCashPath.Active:=False;

    prWH('��������� ���������',Memo1);
    prWH('',Memo1);
  end;
end;

procedure TfmMainAutoCloseCash.acRePutChExecute(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    iMainDep,iAddDep,iCurDep,iC,NumC:Integer;
    StrTa,SNumCash:String;
    t1,t2:Real;
    sBar:String;
    iBar:Int64;
    rTimeShift:Real;
    k,n:INteger;
    ChTime:TDateTime;
    
begin
  //������������ ����
  fmPeriodForAC.cxDateEdit1.Date:=Date;
  fmPeriodForAC.cxDateEdit2.Date:=Date;
  fmPeriodForAC.ShowModal;
  if fmPeriodForAC.ModalResult=mrOk then
  begin
    prWH('������.',Memo1);
    DateB:=Trunc(fmPeriodForAC.cxDateEdit1.Date);
    DateE:=Trunc(fmPeriodForAC.cxDateEdit2.Date);
    for iCurD:=DateB to DateE do
    begin
      prWH('  ���� - '+ds1(iCurD),Memo1);
      with dmMC do
      with dmMt do
      with dmPx do
      begin
        quCash.Active:=False;
        quCash.Active:=True;
        quCash.First;
        while not quCash.Eof do
        begin
            if (quCashTake.AsInteger=1)and(quCashShRealiz.AsInteger=2)  then //����� FB
            begin
              t1:=0;
              iMainDep:=RoundEx(quCashRezerv.AsFloat);
              iAddDep:=quCashTechnolog.AsInteger;

              prWH(' '+quCashName.AsString+'  ����� : '+IntToStr(iMainDep)+' ('+INtToStr(iAddDep)+')',Memo1);

              // ���� ���� ������� �� ����� ����� � �����
              StrTa:='cq'+formatDateTime('yyyymm',iCurD);

              prWH('    - ������ ..',Memo1);

              quD.Active:=False;
              quD.SQL.Clear;
              quD.SQL.Add('Delete from "'+StrTa+'"' );
              quD.SQL.Add('where DateOperation='''+ds(iCurD)+'''');
              quD.SQL.Add('and Cash_Code='+its(quCashNumber.AsInteger));
              quD.SQL.Add('and Ck_Number<>'+its(CommonSet.CorrCheckNum));
              quD.SQL.Add('and Ck_Number<>'+its(CommonSet.CorrCheckNum+1));
              quD.ExecSQL;

              delay(100);

              ptCQ.Active:=False;
              ptCQ.TableName:=StrTa;
              ptCQ.Active:=True;

              prWH('    - ��������� ..',Memo1);
              with dmFB do
              begin
                if Cash.Connected then
                begin
                  prWH('    ����� � ����� �� ..',Memo1);
                  iC:=0; NumC:=0;

                  SNumCash:=quCashNumber.AsString; if length(SNumCash)<2 then SNumCash:='0'+SNumCash;
                  if CommonSet.h12=1 then rTimeShift:=0 else rTimeShift:=0.0833333;

                  prWH('      ������ � '+dts(iCurD+rTimeShift)+'  �� '+dts(iCurD+1+rTimeShift),Memo1);

                  quCashSail.SelectSQL.Clear;
                  quCashSail.Active:=False;
                  quCashSail.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,ID,CHDATE,ARTICUL,BAR,CARDSIZE,QUANTITY,PRICERUB,DPROC,DSUM,TOTALRUB,CASHER,DEPART,OPERATION');
                  quCashSail.SelectSQL.Add('FROM CASHS'+SNumCash);
                  quCashSail.SelectSQL.Add('WHERE CHDATE>='''+dts(iCurD+rTimeShift)+'''');
                  quCashSail.SelectSQL.Add('and CHDATE<'''+dts(iCurD+1+rTimeShift)+'''');
                  quCashSail.SelectSQL.Add('order by  SHOPINDEX, CASHNUMBER,ZNUMBER,CHECKNUMBER,ID');

                  quCashSail.Active:=True;
                  quCashSail.First;
                  while not quCashSail.Eof do
                  begin
                    k:=1;

                    Case quCashSailOPERATION.AsInteger of
                    0,4: k:=-1;
                    end;

                    ChTime:=quCashSailCHDATE.AsDateTime; //����� ���� � ����������� ��������� ���������
                    t2:=quCashSailCHDATE.AsDateTime;
                                                                                             // ��� � 12 �� 12
                    if (ChTime>=(iCurD+rTimeShift)) and (ChTime<(iCurD+1+rTimeShift)) then //� 2-�� ���� �� 2-�� ���� ���������� ���
                    begin
                      if quCashSailCHECKNUMBER.AsInteger>NumC then
                      begin
                        NumC:=quCashSailCHECKNUMBER.AsInteger;
//                        inc(CountCheck);  //���-�� ����� �������
                      end;

                      if quCashSailARTICUL.AsString>='0' then
                      begin
                        ptCQ.Append;
                        ptCQQuant.AsFloat:=quCashSailQuantity.AsFloat*k;
                        if k=1 then ptCQOperation.AsString:='P' //Operation
                               else ptCQOperation.AsString:='R';//Operation
                        ptCQDateOperation.AsDateTime:=iCurD;
                        ptCQPrice.AsFloat:=RoundVal(quCashSailPRICERUB.AsFloat);
                        ptCQStore.AsString:='';
                        ptCQCk_Number.AsInteger:=quCashSailCHECKNUMBER.AsInteger;

                        ptCQCk_Curs.AsFloat:=quCashSailID.AsInteger;  //����� ������� � ����

                        sBar:=Trim(quCashSailBAR.AsString);
                        iBar:=0;
                        for n:=1 to Length(sBar) do
                        begin
                          iBar:=iBar*10+StrToIntDef(sBar[n],0);
                        end;

                        ptCQQuant_S.AsFloat:=iBar+(Length(sBar)/100);

                        if quCashSailID.AsInteger=1 then ptCQContr_Cost.AsFloat:=0
                        else ptCQContr_Cost.AsFloat:=t2-t1; //����� ���������� ������� ���� - ������� �� 2-��

                        ptCQCk_CurAbr.AsInteger:=0;

                        ptCQCk_Disg.AsFloat:=quCashSailDPROC.AsFloat;
                        ptCQCk_Disc.AsFloat:=quCashSailDSUM.AsFloat;

                        iCurDep:=quCashSailDEPART.AsInteger;
                        if iCurDep=0 then iCurDep:=iMainDep;
                        if iCurDep<>iMainDep then
                        begin
                          if iCurDep<>iAddDep then iCurDep:=iMainDep;
                        end;

                        ptCQGrCode.AsFloat:=iCurDep;
                        ptCQCode.AsInteger:=StrToINtDef(quCashSailARTICUL.AsString,0);
                        ptCQCassir.AsString:=quCashSailCASHER.AsString;
                        ptCQCash_Code.AsInteger:=quCashSailCASHNUMBER.AsInteger;

                        if (quCashSailOPERATION.AsInteger=5)or(quCashSailOPERATION.AsInteger=4)
                          then ptCQCk_Card.AsInteger:=1 //Ck_Card
                          else ptCQCk_Card.AsInteger:=0;

                        ptCQContr_Code.AsString:='';
                        ptCQSeria.AsString:='';
                        ptCQBestB.AsString:='';
                        ptCQNDSx1.AsFloat:=0;
                        ptCQNDSx2.AsFloat:=0;
                        ptCQTimes.AsDateTime:=StrToTime(formatDateTime('hh:nn',quCashSailCHDATE.AsDateTime));
                        ptCQSumma.AsFloat:=quCashSailTOTALRUB.AsFloat*k;
                        ptCQSumNDS.AsFloat:=0;
                        ptCQSumNSP.AsFloat:=0;
                        ptCQPriceNSP.AsFloat:=0;
                        ptCQNSmena.AsInteger:=quCashSailZNumber.AsInteger;
                        ptCQ.Post;

                      end else prWH('     ���.  '+quCashSailARTICUL.AsString,Memo1);
                    end; //������� �� ������� ����

                    quCashSail.Next;
                    t1:=t2;
                    inc(iC);
                    if iC mod 100 = 0 then
                    begin
                      StatusBar1.Panels[0].Text:=its(iC);
                      Delay(10);
                    end;
                  end;

                  StatusBar1.Panels[0].Text:=its(iC);
                  Delay(10);

                  quCashSail.Active:=False;

                  prWH('      ���� - '+its(iC),Memo1);
                end else
                begin
                  prWH('    ��� ����� � �����. �������� �����������.',Memo1);
                end;
              end;
            end;
          quCash.Next;
        end;
        quCash.Active:=False;
      end;
    end;
    prWH('������� ��������.',Memo1);
  end;
end;

end.
