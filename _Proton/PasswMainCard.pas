unit PasswMainCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGraphics,
  Grids, DBGrids;

type
  TfmPerA = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TcxLookupComboBox;
    Edit1: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Enter(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPerA: TfmPerA;
  CountEnter:Integer;
  iLastP:Integer;

implementation

uses Un1, MDB;

{$R *.dfm}

procedure TfmPerA.FormCreate(Sender: TObject);
begin
  Left:=1;
  Top:=1;
  CountEnter:=0;
  iLastP:=Person.Id;
  Person.Id:=0;

  with dmMC do
  begin
    quPassw.SQL.Clear;
    if Person.Modul='Adm' then
    begin
      quPassw.SQL.Add('SELECT * FROM "A_RPERSONAL"');
      quPassw.SQL.Add('where Uvolnen=0 and Modul1=1 and Id_Parent>0');
      quPassw.SQL.Add('order by Name');
    end;

    if Person.Modul='Crd' then
    begin
      quPassw.SQL.Add('SELECT * FROM "A_RPERSONAL"');
      quPassw.SQL.Add('where Uvolnen=0 and Modul2=1 and Id_Parent>0');
      quPassw.SQL.Add('order by Name');
    end;

    if Person.Modul='Rep' then
    begin
      quPassw.SQL.Add('SELECT * FROM "A_RPERSONAL"');
      quPassw.SQL.Add('where Uvolnen=0 and Modul3=1 and Id_Parent>0');
      quPassw.SQL.Add('order by Name');
    end;

    quPassw.Active:=True;
    ComboBox1.EditValue:=iLastP;
  end;
end;

procedure TfmPerA.Edit1Enter(Sender: TObject);
begin
  Text:='';
end;

procedure TfmPerA.Button1Click(Sender: TObject);
begin
  with dmMC do
  begin
    quPassw.Locate('ID',ComboBox1.EditValue,[]);
    if quPassw.FieldByName('PASSW').AsString=Edit1.Text then
    begin //������� ������
      Person.Id:=quPasswID.AsInteger;
      Person.Name:=quPasswNAME.AsString;
      quPassw.Active:=False;
      ModalResult:=1;
    end
    else
    begin
      inc(CountEnter);
      if CountEnter>2 then close;
      showmessage('������ ������������. �������� '+IntToStr(3-CountEnter)+' �������.');
      Edit1.Text:='';
      Edit1.SetFocus;
    end;
  end;
end;

procedure TfmPerA.Button2Click(Sender: TObject);
begin
  dmMC.quPassw.Active:=False;
  Close;
end;

procedure TfmPerA.FormShow(Sender: TObject);
begin
//  StatusBar1.Panels[0].Text:='5';
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

end.
