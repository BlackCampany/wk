unit AddGoods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxCheckBox, cxGraphics, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCalc, DBClient, cxImageComboBox,
  cxCheckListBox, cxCurrencyEdit, cxButtonEdit;

type
  TfmAddGood = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    VBar: TcxGridDBTableView;
    LBar: TcxGridLevel;
    GBar: TcxGrid;
    Label6: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    tBar: TClientDataSet;
    Label7: TLabel;
    Image1: TImage;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    tBarBarNew: TStringField;
    tBarBarOld: TStringField;
    tBarQuant: TFloatField;
    tBariStatus: TSmallintField;
    sBar: TDataSource;
    VBarBarNew: TcxGridDBColumn;
    VBarBarOld: TcxGridDBColumn;
    VBarQuant: TcxGridDBColumn;
    VBariStatus: TcxGridDBColumn;
    Timer1: TTimer;
    Label8: TLabel;
    GridEU: TcxGrid;
    ViewEu: TcxGridDBTableView;
    ViewEuIDATEB: TcxGridDBColumn;
    ViewEuIDATEE: TcxGridDBColumn;
    ViewEuSDATEB: TcxGridDBColumn;
    ViewEuSDATEE: TcxGridDBColumn;
    ViewEuTO100GRAMM: TcxGridDBColumn;
    LevelEU: TcxGridLevel;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    tEU: TClientDataSet;
    tEUiDateB: TIntegerField;
    tEUiDateE: TIntegerField;
    tEUto100g: TFloatField;
    dsEU: TDataSource;
    tEUsDateb: TStringField;
    tEUsDateE: TStringField;
    Label9: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    Label10: TLabel;
    CheckListBox1: TcxCheckListBox;
    Label11: TLabel;
    cxCEdit1: TcxCurrencyEdit;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cxButton3: TcxButton;
    Label16: TLabel;
    cxCalcEdit5: TcxCalcEdit;
    Label17: TLabel;
    cxLookupComboBox4: TcxLookupComboBox;
    cxButton8: TcxButton;
    Label18: TLabel;
    cxSpinEdit2: TcxSpinEdit;
    Label19: TLabel;
    Label20: TLabel;
    cxLookupComboBox5: TcxLookupComboBox;
    cxLookupComboBox6: TcxLookupComboBox;
    Label21: TLabel;
    cxCalcEdit6: TcxCalcEdit;
    procedure cxButton7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure tEUCalcFields(DataSet: TDataSet);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure GridEUExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddGood: TfmAddGood;
  bClearAddG:Boolean = False;

implementation

uses dmOffice, Un1, AddBar, TCard, AddEUGoods, Goods, SprBGU, AddBGU,
  DMOReps;

{$R *.dfm}

procedure TfmAddGood.Timer1Timer(Sender: TObject);
begin
  if bClearAddG=True then begin StatusBar1.Panels[0].Text:=''; bClearAddG:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearAddG:=True;
end;


procedure TfmAddGood.cxButton7Click(Sender: TObject);
Var StrWk:String;
begin
  //������������ �� �� ���� ����
  if not CanDo('prGenBar') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if cxCheckBox1.Checked then
  begin //������� �����
    StrWk:=IntToStr(cxSpinEdit1.EditValue);
    while Length(StrWk)<5 do StrWk:='0'+StrWk;
    StrWk:=CommonSet.PrefixVes+StrWk;

    if tBar.Locate('BarNew',StrWk,[])=False then
    begin
      tBar.Append;
      tBarBarNew.AsString:=StrWk;
      tBarBarOld.AsString:='';
      tBarQuant.AsFloat:=1;
      tBariStatus.AsInteger:=1;
      tBar.Post;
    end else showmessage('����������� ��� ��� ����.');
  end else
  begin //�������
    StrWk:=IntToStr(cxSpinEdit1.EditValue);
    while Length(StrWk)<10 do StrWk:='0'+StrWk;
    StrWk:=CommonSet.PrefixVes+StrWk;
    //����� ��������� ����������� ������
    StrWk:=ToStandart(StrWk);

    if tBar.Locate('BarNew',StrWk,[])=False then
    begin
      tBar.Append;
      tBarBarNew.AsString:=StrWk;
      tBarBarOld.AsString:='';
      tBarQuant.AsFloat:=1;
      tBariStatus.AsInteger:=1;
      tBar.Post;
    end else showmessage('����������� ��� ��� ����.');
  end;
end;


procedure TfmAddGood.cxButton4Click(Sender: TObject);
Var StrWk:String;
begin
  //���������� ���������
  if not CanDo('prAddBar') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddBar:=TfmAddBar.Create(Application);
  try
    fmAddBar.cxTextEdit1.Text:='';
    fmAddBar.cxCalcEdit1.Value:=1;
    fmAddBar.Caption:='���������� ��';
    fmAddBar.ShowModal;
    if fmAddBar.ModalResult=mrOk then
    begin
      //�������� �����������
      StrWk:=fmAddBar.cxTextEdit1.Text;
      with dmO do
      begin
        if tBar.Locate('BarNew',StrWk,[])=False then
        begin
          quBarFind.Active:=False;
          quBarFind.ParamByName('SBAR').AsString:=fmAddBar.cxTextEdit1.Text;
          quBarFind.Active:=True;
          if quBarFind.RecordCount=0 then
          begin
            tBar.Append;
            tBarBarNew.AsString:=StrWk;
            tBarBarOld.AsString:='';
            tBarQuant.AsFloat:=fmAddBar.cxCalcEdit1.EditValue;
            tBariStatus.AsInteger:=1;
            tBar.Post;
          end else showmessage('����� �� � ���� ��� ����, ����� � ����� - '+quBarFindGOODSID.AsString);
          quBarFind.Active:=False;
        end else showmessage('����� �� ��� ����.');
      end;
    end;
  finally
    fmAddBar.Release;
  end;
end;

procedure TfmAddGood.cxButton6Click(Sender: TObject);
Var StrWk:String;
begin
  //�������������� ��
  if not CanDo('prEditBar') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if tBar.Eof then
  begin
    showmessage('�������� �� ��� ��������������.');
    exit;
  end;

  StrWk:=tBarBarNew.AsString;
  if StrWk[1]='2' then
  begin
    showmessage('������������� ���������� ��� ���������.');
    exit;
  end;

  fmAddBar:=TfmAddBar.Create(Application);
  try
    fmAddBar.cxTextEdit1.Text:=tBarBarNew.AsString;
    fmAddBar.cxCalcEdit1.Value:=tBarQuant.AsFloat;
    fmAddBar.Caption:='�������������� �� - '+tBarBarNew.AsString;
    fmAddBar.ShowModal;
    if fmAddBar.ModalResult=mrOk then
    begin
      //����������
      StrWk:=fmAddBar.cxTextEdit1.Text;
      with dmO do
      begin
        quBarFind.Active:=False;
        quBarFind.ParamByName('SBAR').AsString:=fmAddBar.cxTextEdit1.Text;
        quBarFind.Active:=True;
        if (quBarFind.RecordCount=0)or((quBarFind.RecordCount=1)and(quBarFindGOODSID.AsInteger=cxSpinEdit1.Value)) then
        begin
          tBar.Edit;
          tBarBarNew.AsString:=StrWk;
          tBarQuant.AsFloat:=fmAddBar.cxCalcEdit1.EditValue;
          tBar.Post;
        end else showmessage('����� �� � ���� ��� ����, ����� � ����� - '+quBarFindGOODSID.AsString);
        quBarFind.Active:=False;
      end;
    end;
  finally
    fmAddBar.Release;
  end;
end;

procedure TfmAddGood.cxButton5Click(Sender: TObject);
begin
  //��������
  if not CanDo('prDelBar') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if tBar.Eof then
  begin
    showmessage('�������� �� ��� ��������.');
    exit;
  end;
  if MessageDlg('�� ������������� ������ ������� �� - '+tBarBarNew.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    tBar.Edit;
    tBariStatus.AsInteger:=0;
    tBar.Post;

    showmessage('�������� - '+tBarBarNew.AsString+' ������� �� ��������.');
  end;
end;

procedure TfmAddGood.cxButton3Click(Sender: TObject);
begin
  bEE:=True;
  if fmSprBGU.Visible then fmSprBGU.Visible:=False;
  delay(10);
  fmSprBGU.Show;
end;

procedure TfmAddGood.tEUCalcFields(DataSet: TDataSet);
Var StrWk,StrWk1:String;
begin
  StrWk:=IntToStr(tEUIDATEB.AsInteger-Trunc(tEUIDATEB.AsInteger/100)*100);
  StrWk1:=IntToStr(Trunc(tEUIDATEB.AsInteger/100));
  if Length(StrWk)<2 then StrWk:='0'+StrWk;   if Length(StrWk1)<2 then StrWk1:='0'+StrWk1;
  tEUSDATEB.AsString:=StrWk+'.'+StrWk1;

  StrWk:=IntToStr(tEUIDATEE.AsInteger-Trunc(tEUIDATEE.AsInteger/100)*100);
  StrWk1:=IntToStr(Trunc(tEUIDATEE.AsInteger/100));
  if Length(StrWk)<2 then StrWk:='0'+StrWk;   if Length(StrWk1)<2 then StrWk1:='0'+StrWk1;
  tEUSDATEE.AsString:=StrWk+'.'+StrWk1;
end;

procedure TfmAddGood.cxButton9Click(Sender: TObject);
begin
  //��������
  fmAddEUGoods.cxDateEdit1.Date:=Date;
  fmAddEUGoods.cxDateEdit2.Date:=Date;
  fmAddEUGoods.cxCalcEdit1.EditValue:=0;
  fmAddEUGoods.ShowModal;
end;

procedure TfmAddGood.cxButton10Click(Sender: TObject);
begin
  if tEU.RecordCount=0 then exit;
  if MessageDlg('������������� ������� �������� � '+tEUSDATEB.AsString+' �� '+tEUSDATEE.AsString+' ?',
  mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    tEU.Delete;
  end;
end;

procedure TfmAddGood.GridEUExit(Sender: TObject);
begin
  if tEU.State in [dsInsert,dsEdit] then tEu.Post;
end;

procedure TfmAddGood.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bAddG:=False;
end;

procedure TfmAddGood.FormShow(Sender: TObject);
begin
  if bAddG then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else
  begin
    cxCEdit1.SetFocus;
    cxCEdit1.SelectAll;
  end;
end;

procedure TfmAddGood.cxButton8Click(Sender: TObject);
begin
  fmAddBGU.taBGUGr.Active:=False;
  fmAddBGU.taBGUGr.Active:=True;
  fmAddBGU.taBGUGr.First;

  fmAddBGU.cxTextEdit1.Text:=cxTextEdit1.Text;
  fmAddBGU.cxCalcEdit2.Value:=cxCalcEdit2.Value;
  fmAddBGU.cxCalcEdit3.Value:=cxCalcEdit3.Value;
  fmAddBGU.cxCalcEdit4.Value:=cxCalcEdit4.Value;
  fmAddBGU.cxCalcEdit5.Value:=cxCalcEdit5.Value;

  fmAddBGU.ShowModal;
  fmAddBGU.taBGUGr.Active:=False;
end;

end.
