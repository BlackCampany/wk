unit mTara;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmTara = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewTara: TcxGridDBTableView;
    LevelTara: TcxGridLevel;
    GridTara: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    ViewTaraCode: TcxGridDBColumn;
    ViewTaraName: TcxGridDBColumn;
    amTara: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    acEnter: TAction;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure ViewTaraDblClick(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTara: TfmTara;
  bClearTara:Boolean = False;

implementation

uses Un1, MDB, AddSingle;

{$R *.dfm}

procedure TfmTara.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmTara.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmTara.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmTara.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmTara.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.FormCreate(Sender: TObject);
begin
  GridTara.Align:=AlClient;
  ViewTara.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  dmMc.quTara.Active:=True;
end;

procedure TfmTara.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTara.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmTara.Timer1Timer(Sender: TObject);
begin
  if bClearTara=True then begin StatusBar1.Panels[0].Text:=''; bClearTara:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearTara:=True;
end;

procedure TfmTara.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmTara.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelTara') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if CommonSet.Single=0 then
  begin
    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0)and(pos('OPERZAK',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  with dmMC do
  begin
    if quTara.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quTaraName.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewTara.BeginUpdate;
        try
          try
            iR:=quTaraCode.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from "RPack"');
            quD.SQL.Add('where Code='+IntToStr(iR));
            quD.ExecSQL;

            quTara.Close;
            quTara.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewTara.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmTara.acEditExecute(Sender: TObject);
Var iR:Integer;
begin
//�������������
  if not CanDo('prEditTara') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if CommonSet.Single=0 then
  begin
    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0)and(pos('OPERZAK',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  with dmMC do
  begin
    if quTara.RecordCount>0 then
    begin
      fmAddSingle.prSetParams('����: ��������������.',0);
      fmAddSingle.cxTextEdit1.Text:=quTaraName.AsString;
      fmAddSingle.ShowModal;
      if fmAddSingle.ModalResult=mrOk then
      begin
        ViewTara.BeginUpdate;
        try
          try
            iR:=quTaraCode.AsInteger;

            quA.SQL.Clear;
            quA.SQL.Add('Update"RPack" Set Name='''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+'''');
            quA.SQL.Add('where Code='+IntToStr(iR));
            quA.ExecSQL;

            quTara.Close;
            quTara.Open;
            quTara.Locate('Code',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewTara.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmTara.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prAddTara') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  if CommonSet.Single=0 then
  begin
    if (pos('OPER CB',Person.Name)=0)and(pos('�����',Person.Name)=0)and(pos('OPERZAK',Person.Name)=0) then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  end;

  fmAddSingle.prSetParams('����: ����������.',0);
  fmAddSingle.cxTextEdit1.Text:='';
  fmAddSingle.ShowModal;
  if fmAddSingle.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewTara.BeginUpdate;
      try
        if quTara.Locate('Name',fmAddSingle.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          iMax:=1;
          if quTara.RecordCount>0 then begin quTara.Last; iMax:=quTaraCode.AsInteger+1; end;

          quE.SQL.Clear;
          quE.SQL.Add('INSERT into "RPack" values ('+IntToStr(iMax)+','''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+''')');
          quE.ExecSQL;

          quTara.Close;
          quTara.Open;
          quTara.Locate('Code',iMax,[]);
        end;
      finally
        ViewTara.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmTara.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmTara.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmTara.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmTara.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmTara.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmTara.ViewTaraDblClick(Sender: TObject);
begin
  acEnter.Execute;
end;

procedure TfmTara.acEnterExecute(Sender: TObject);
begin
  with dmMC do
  begin
    if quTara.Active and (quTara.RecordCount>0) then
    begin
      if (iDirect=2) or (iDirect=3) then //����� ���� � ������ ��� �������
      begin
        PosP.Id:=quTaraCode.AsInteger;
        PosP.Name:=quTaraName.AsString;
        modalresult:=mrOk;
        Close;
      end;
    end else
    begin
      showmessage('�������� ������...');
    end;
  end;
end;

end.
