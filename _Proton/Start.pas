unit Start;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxProgressBar;

type
  TfmStart = class(TForm)
    Memo1: TcxMemo;
    PBar1: TcxProgressBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStart: TfmStart;

implementation

{$R *.dfm}

procedure TfmStart.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
end;

end.
