object fmSInvVoz: TfmSInvVoz
  Left = 568
  Top = 267
  Width = 411
  Height = 227
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090' "'#1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103' '#1074#1086#1079#1074#1088#1072#1090#1086#1074'"'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GrSInvVoz: TcxGrid
    Left = 0
    Top = 0
    Width = 403
    Height = 194
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewSInvVoz: TcxGridDBTableView
      DragMode = dmAutomatic
      PopupMenu = PopupMenu1
      OnDblClick = ViewSInvVozDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmP.dsquDInvV
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewSInvVozNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088
        DataBinding.FieldName = 'Number'
        Width = 151
      end
      object ViewSInvVozDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'Date'
        Options.Editing = False
        Width = 203
      end
    end
    object LevSInvVoz: TcxGridLevel
      GridView = ViewSInvVoz
    end
  end
  object PBar1: TcxProgressBar
    Left = 88
    Top = 93
    Align = alCustom
    ParentColor = False
    Position = 100.000000000000000000
    Properties.BarBevelOuter = cxbvRaised
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clBlue
    Properties.EndColor = 16745090
    Properties.PeakValue = 100.000000000000000000
    Style.Color = clWhite
    TabOrder = 1
    Visible = False
    Width = 249
  end
  object PopupMenu1: TPopupMenu
    Left = 64
    Top = 64
    object N1: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1077#1088#1080#1086#1076
      OnClick = N1Click
    end
  end
end
