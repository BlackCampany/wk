unit HistoryOper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, cxGridChartView, cxTextEdit,
  Placemnt;

type
  TfmHistOp = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    GrHistOp: TcxGrid;
    LevelHistOp: TcxGridLevel;
    GrHistOpDBTableView2: TcxGridDBTableView;
    GrHistOpDBTableView2Column1: TcxGridDBColumn;
    GrHistOpDBTableView2Column2: TcxGridDBColumn;
    GrHistOpDBTableView2Column3: TcxGridDBColumn;
    GrHistOpDBTableView2Column4: TcxGridDBColumn;
    GrHistOpDBTableView2Column5: TcxGridDBColumn;
    GrHistOpDBTableView2Column6: TcxGridDBColumn;
    GrHistOpDBTableView2Column7: TcxGridDBColumn;
    GrHistOpDBTableView2Column8: TcxGridDBColumn;
    GrHistOpDBTableView2Column9: TcxGridDBColumn;
    GrHistOpDBTableView2Column10: TcxGridDBColumn;
    ViewHistOp: TcxGridDBTableView;
    ViewHistOpIDPERS: TcxGridDBColumn;
    ViewHistOpDOCDATE: TcxGridDBColumn;
    ViewHistOpDOCNUM: TcxGridDBColumn;
    ViewHistOpDOCID: TcxGridDBColumn;
    ViewHistOpIOP: TcxGridDBColumn;
    ViewHistOpNAMEPERS: TcxGridDBColumn;
    ViewHistOpIDOP: TcxGridDBColumn;
    ViewHistOpDATEEDIT: TcxGridDBColumn;
    ViewHistOpName: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmHistOp: TfmHistOp;

procedure GetHistOp(DocId:integer;DocNum:string);

implementation

uses FV, Period1, Un1, dbe;

{$R *.dfm}

procedure TfmHistOp.SpeedItem3Click(Sender: TObject);
begin
 prNExportExel5(ViewHistOp);
end;

procedure TfmHistOp.SpeedItem1Click(Sender: TObject);
begin
   Close;
end;

procedure GetHistOp(DocId:integer;DocNum:string);
var DtSB,DtSE:string;
begin
  with dmE do
  begin
    fmHistOp.ViewHistOp.BeginUpdate;
    dsHistOp.DataSet:=nil;
    try
      quDocHist.Active:=false;
      quDocHist.SQL.Clear;
      DtsB:=ds(CommonSet.DateBeg);
      DtsE:=ds(CommonSet.DateEnd);
      quDocHist.SQL.Add('SELECT dt."Name",');
      quDocHist.SQL.Add('dh."TYPED",');
      quDocHist.SQL.Add('dh."IDPERS",');
      quDocHist.SQL.Add('dh."DATEEDIT",');
      quDocHist.SQL.Add('dh."DOCDATE",');
      quDocHist.SQL.Add('dh."DOCNUM",');
      quDocHist.SQL.Add('dh."DOCID",');
      quDocHist.SQL.Add('dh."IOP",');
      quDocHist.SQL.Add('dh."NAMEPERS",');
      quDocHist.SQL.Add('dh."IDOP"');
      quDocHist.SQL.Add('from "A_DOCHIST" dh');
      quDocHist.SQL.Add('left join DocTYpeName dt on dt.Code=dh."TYPED"');
      quDocHist.SQL.Add('where');
      quDocHist.SQL.Add('DOCDATE>='''+DtsB+'''');
      quDocHist.SQL.Add('and DOCDATE<='''+DtsE+'''');
      if DocId>0 then
        quDocHist.SQL.Add('and DOCID='''+Its(DocId)+'''');
      if not(DocNum='') then
        quDocHist.SQL.Add('and DOCNUM='''+DocNum+'''');
      quDocHist.Active:=true;
    finally
      dsHistOp.DataSet:=quDocHist;
      fmHistOp.ViewHistOp.EndUpdate;
      fmHistOp.Caption:='������� �������� �� ������ � '+ds1(CommonSet.DateBeg)+' �� '+ds1(CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmHistOp.SpeedItem2Click(Sender: TObject);
begin
//������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    GetHistOp(0,''); //�������� ������� �������� �� �����
  end;
end;

end.
