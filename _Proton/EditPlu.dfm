object fmEditPlu: TfmEditPlu
  Left = 283
  Top = 475
  BorderStyle = bsDialog
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
  ClientHeight = 228
  ClientWidth = 354
  Color = 16767468
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 21
    Height = 13
    Caption = 'PLU'
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 40
    Width = 57
    Height = 13
    Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 68
    Width = 89
    Height = 13
    Caption = #1055#1077#1088#1074#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label5: TLabel
    Left = 16
    Top = 92
    Width = 87
    Height = 13
    Caption = #1042#1090#1086#1088#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label6: TLabel
    Left = 16
    Top = 124
    Width = 53
    Height = 13
    Caption = #1057#1088#1086#1082' ('#1089#1091#1090'.)'
    Transparent = True
  end
  object Label7: TLabel
    Left = 16
    Top = 148
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
    Transparent = True
  end
  object Label3: TLabel
    Left = 208
    Top = 16
    Width = 59
    Height = 13
    Caption = #1060#1086#1088#1084#1072#1090' '#1101#1090'.'
    Transparent = True
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 100
    Top = 12
    Properties.ReadOnly = True
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Width = 73
  end
  object cxSpinEdit2: TcxSpinEdit
    Left = 100
    Top = 36
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 73
  end
  object cxTextEdit2: TcxTextEdit
    Left = 112
    Top = 64
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 213
  end
  object cxTextEdit3: TcxTextEdit
    Left = 112
    Top = 88
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 209
  end
  object cxSpinEdit3: TcxSpinEdit
    Left = 100
    Top = 120
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 4
    Width = 73
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 100
    Top = 144
    EditValue = 0.000000000000000000
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Width = 73
  end
  object Panel1: TPanel
    Left = 0
    Top = 182
    Width = 354
    Height = 46
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 6
    object cxButton1: TcxButton
      Left = 76
      Top = 12
      Width = 75
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 12
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxComboBox1: TcxComboBox
    Left = 276
    Top = 12
    Properties.Items.Strings = (
      '1'
      '2'
      '3')
    Style.BorderStyle = ebsOffice11
    TabOrder = 7
    Text = '1'
    Width = 49
  end
end
