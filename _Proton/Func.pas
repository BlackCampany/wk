unit Func;

interface

uses MDB,Windows, Messages, SysUtils;

function ByteToBCD(bt:byte):byte;
function WordToBCD(wd:word):word;
procedure PluNumToBCD(Nplu:word; var PluNum:TArray4Bytes);
procedure PluBarCodeToBCD(Code:string; var PluBar:TArray4Bytes);
procedure PluUnitPriceToBCD(UnPrice:real; var PluPrice:TArray4Bytes);

implementation


function ByteToBCD(bt:byte):byte;
begin
Result:=StrToInt('$'+IntToStr(bt));
end;

function WordToBCD(wd:word):word;
 var s:string[5];
     i:byte;
begin
Str(wd:4,s);
For i:=1 to 4 do if Ord(s[i])=$20 then s[i]:='0';
s:='$'+copy(s,length(s)-1,2)+copy(s,length(s)-3,2);
Result:=StrToInt(s);
end;

procedure PluNumToBCD(Nplu:word; var PluNum:TArray4Bytes);
var b:byte;
begin
PluNum[1]:=0;
PluNum[2]:=0;
b:=trunc(Nplu/100);
PluNum[3]:=ByteToBCD(b);
b:=Nplu-trunc(Nplu/100)*100;
PluNum[4]:=ByteToBCD(b);
end;


procedure PluUnitPriceToBCD(UnPrice:real; var PluPrice:TArray4Bytes);
var b:byte;
    iprice:integer;
begin                                     //1234 ��� 56 ���
iprice:=Trunc(Unprice);                   //iprice=1234
b:=trunc(iprice/10000);                   //b=0
PluPrice[1]:=ByteToBCD(b);
b:=trunc(iprice/100-b*100);                     //b=12
PluPrice[2]:=ByteToBCD(b);
b:=iprice-Trunc(iprice/100)*100;          //b=34
PluPrice[3]:=ByteToBCD(b);
b:=Round(Unprice*100-Trunc(Unprice)*100); //b=56
PluPrice[4]:=ByteToBCD(b);
end;

procedure PluBarCodeToBCD(Code:string; var PluBar:TArray4Bytes);
var s:string[8]; i:byte;
begin
s:=Copy(Code,1,7)+'0';
for i:=1 to 8 do if Ord(s[i])=$20 then s[i]:='0';
for i:=1 to 4 do
  PluBar[i]:=StrToInt('$'+Copy(s,i*2-1,2));
end;


end.
