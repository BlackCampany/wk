unit CashFBLoad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxCheckBox, cxProgressBar, Menus, cxLookAndFeelPainters, cxButtons;

type
  TfmCashFBLoad = class(TForm)
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    cxProgressBar1: TcxProgressBar;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxCheckBox2: TcxCheckBox;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashFBLoad: TfmCashFBLoad;
  bStop:Boolean;
  
implementation

uses MFB, MT, Un1, u2fdk;

{$R *.dfm}

procedure TfmCashFBLoad.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmCashFBLoad.cxButton1Click(Sender: TObject);
Var iStep,iC,iZ:INteger;
    bTr:Boolean;
    sPre:String;
    iAVid:INteger;
    rAlcVol:Real;

  function sTi:String;
  begin
    sTi:=FormatDateTime('hh:nn sss',now)+' ';
  end;

begin
//�������
  with dmFb do
  with dmMT do
  begin
    try
      Memo1.Clear;
      cxButton1.Enabled:=False;
      cxButton3.Enabled:=False;
      cxButton2.Enabled:=True;
      bStop:=False;
      Memo1.Lines.Add(sTi+'������.');
      Memo1.Lines.Add(sTi+'  ��������� ���� FB.'); delay(10);

      try
        Casher.Close;
        Casher.DBName:=cxTextEdit1.Text;
        Casher.Open;
        if Casher.Connected then
        begin
          Memo1.Lines.Add(sTi+'  ���� FB ��.'); delay(10);
          Memo1.Lines.Add(sTi+'    ������ ������.'); delay(10);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from CARDSCLA');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except on Er:Exception do
          begin
            Memo1.Lines.Add(sTi+'    ������.'+Er.Message); delay(10);
          end;
          end;

          Memo1.Lines.Add(sTi+'    ������ ��.'); delay(10);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from BAR');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except on Er:Exception do
          begin
            Memo1.Lines.Add(sTi+'    ������.'+Er.Message); delay(10);
          end;
          end;

          Memo1.Lines.Add(sTi+'    ������ ����-������.'); delay(10);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from FIXDISC');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except on Er:Exception do
          begin
            Memo1.Lines.Add(sTi+'    ������.'+Er.Message); delay(10);
          end;
          end;

{
          Memo1.Lines.Add(sTi+'    ������ �������������.'); delay(10);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from CLASSIF');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except
            Memo1.Lines.Add(sTi+'    ������.'); delay(10);
          end;
}
          Memo1.Lines.Add(''); delay(10);

          if taCards.Active=False then taCards.Active:=True
          else taCards.Refresh;

          if ptPluLim.Active=False then ptPluLim.Active:=True
          else ptPluLim.Refresh;

          if ptBar.Active=False then ptBar.Active:=True
          else ptBar.Refresh;
          ptBar.IndexFieldNames:='GoodsID';

          iStep:=taCards.RecordCount div 100;
          if iStep=0 then iStep:=taCards.RecordCount;
          iC:=0;
          iZ:=0;

          Memo1.Lines.Add(sTi+'    �������� ��������.('+its(taCards.RecordCount)+')'); delay(10);

          cxProgressBar1.Position:=0;
          cxProgressBar1.Visible:=True;

          taCardsFB.Active:=True;
          taBarFB.Active:=True;
          taDCStopFB.Active:=True;

          trUpd1.StartTransaction;
          trUpd2.StartTransaction;
//          trUpd3.StartTransaction;

          taCards.First;
          while not taCards.Eof do
          begin
            if cxCheckBox1.Checked then //������ ��������
            begin
              if taCardsStatus.AsInteger<100 then bTr:=True else bTr:=False;
            end else bTr:=True;

            if bTr then //������
            begin
              sPre:='';
              if ptPluLim.FindKey([taCardsID.AsInteger]) then
                if ptPluLimPercent.AsFloat>99 then sPre:='-';

              try
                iAVid:=0;
                rAlcVol:=0;

                if cxCheckBox2.Checked then  //���� �������� � ���������� ��
                begin
                  iAVid:=taCardsV11.AsInteger;
                  rAlcVol:=0;
                end;

                taCardsFB.Append;
                taCardsFBARTICUL.AsString:=its(taCardsID.AsInteger);
                taCardsFBCLASSIF.AsInteger:=taCardsSubGroupID.AsInteger;
                taCardsFBDEPART.AsInteger:=0;
                taCardsFBNAME.AsString:=sPre+OemToAnsiConvert(taCardsFullName.AsString);
                taCardsFBCARD_TYPE.AsInteger:=taCardsTovarType.AsInteger+1;
                if taCardsEdIzm.AsInteger=1 then taCardsFBMESURIMENT.AsString:='��.' else taCardsFBMESURIMENT.AsString:='��.';
                taCardsFBPRICE_RUB.AsFloat:=rv(taCardsCena.AsFloat);
                taCardsFBDISCQUANT.AsFloat:=0;
                taCardsFBDISCOUNT.AsFloat:=0;
                taCardsFBSCALE.AsString:='NOSIZE';
                taCardsFBAVID.AsInteger:=iAVid;
                taCardsFBALCVOL.AsFloat:=rAlcVol;
                taCardsFB.Post;

                ptBar.CancelRange;
                ptBar.SetRange([taCardsID.AsInteger],[taCardsID.AsInteger]);
                ptBar.First;
                while not ptBar.Eof do
                begin
                  try
                    taBarFB.Append;
                    taBarFBBARCODE.AsString:=ptBarID.AsString;
                    taBarFBCARDARTICUL.AsString:=its(taCardsID.AsInteger);
                    if ptBarBarStatus.AsInteger=0 then  taBarFBCARDSIZE.AsString:='NOSIZE' else taBarFBCARDSIZE.AsString:='QUANTITY';
                    taBarFBQUANTITY.AsFloat:=ptBarQuant.AsFloat;
                    taBarFBPRICERUB.AsFloat:=0;
                    taBarFB.Post;
                  except
                    taBarFB.Cancel;
                    Memo1.Lines.Add(sTi+'    ������ - '+its(taCardsID.AsInteger)+' '+ptBarID.AsString); delay(10);
                  end;

                  ptBar.Next;
                end;

                if sPre='-' then
                begin
                  taDCStopFB.Append;
                  taDCSTOPFBSART.AsString:=taCardsID.AsString;
                  taDCSTOPFBPROC.AsFloat:=100;
                  taDCStopFB.Post;
                end;
              except
//                Memo1.Lines.Add(sTi+'    ������ - '+its(taCardsID.AsInteger)+' '+ptBarID.AsString); delay(10);
              end;
              inc(iZ);
            end;

            taCards.Next;
            inc(iC);
            if (iC mod iStep = 0) then
            begin
              cxProgressBar1.Position:=(iC div iStep);
              delay(10);
            end;
            if bStop then break;
          end;

          Memo1.Lines.Add(sTi+'    ����� - '+its(iZ)); delay(10);
          Memo1.Lines.Add(sTi+'    Commit 1.'); delay(10);
          trUpd1.Commit;
          delay(100);

          Memo1.Lines.Add(sTi+'    Commit 2.'); delay(10);
          trUpd2.Commit;
          delay(100);
{
          Memo1.Lines.Add(sTi+'    Commit 3.'); delay(10);
          trUpd3.Commit;
          delay(100);
}
          taCardsFB.Active:=False;
          taBarFB.Active:=False;
          taDCStopFB.Active:=False;

          cxProgressBar1.Position:=100;
          Memo1.Lines.Add(sTi+'    �������� Ok.('+its(iZ)+')'); delay(10);
          delay(200);
          cxProgressBar1.Visible:=False;

          ptBar.IndexFieldNames:='ID';
        end;
        Memo1.Lines.Add(sTi+'  ��������� ���� FB.'); delay(10);
        Casher.Close;
      except
        Memo1.Lines.Add(sTi+'  ������ �������� ���� FB. ������� ����������.'); delay(10);
      end;
    finally
      Memo1.Lines.Add(sTi+'�������� ���������.');
      cxButton1.Enabled:=True;
      cxButton3.Enabled:=True;
      cxButton2.Enabled:=False;
    end;
  end;
end;

procedure TfmCashFBLoad.cxButton2Click(Sender: TObject);
begin
  bStop:=True;
  Memo1.Lines.Add('  ���� ��������� �������� ...');
  delay(10);
end;

procedure TfmCashFBLoad.cxButton3Click(Sender: TObject);
begin
  Close;
end;

end.
