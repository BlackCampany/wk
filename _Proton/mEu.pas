unit mEu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmEU = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewEU: TcxGridDBTableView;
    LevelEU: TcxGridLevel;
    GridEU: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    ViewEUCode: TcxGridDBColumn;
    ViewEUName: TcxGridDBColumn;
    amEU: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    ViewEUProcent1: TcxGridDBColumn;
    ViewEUProcent2: TcxGridDBColumn;
    ViewEUProcent3: TcxGridDBColumn;
    ViewEUProcent4: TcxGridDBColumn;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEU: TfmEU;
  bClearEU:Boolean = False;

implementation

uses Un1, MDB, AddSingle, AddEU;

{$R *.dfm}

procedure TfmEU.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmEU.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmEU.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmEU.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmEU.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.FormCreate(Sender: TObject);
begin
  GridEU.Align:=AlClient;
  ViewEU.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmEU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewEU.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmEU.Timer1Timer(Sender: TObject);
begin
  if bClearEU=True then begin StatusBar1.Panels[0].Text:=''; bClearEU:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearEU:=True;
end;

procedure TfmEU.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmEU.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelEU') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('EUSPR')>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quEUName.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewEU.BeginUpdate;
        try
          try
            iR:=quEUCode.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from EUSPR');
            quD.SQL.Add('where Code='+IntToStr(iR));
            quD.ExecSQL;

            quEU.Close;
            quEU.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewEU.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmEU.acEditExecute(Sender: TObject);
Var iR:Integer;
begin
//�������������
  if not CanDo('prEditEU') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('EUSPR')>0 then
    begin
      fmAddEU.Caption:='������������ �����: ��������������.';
      fmAddEU.cxTextEdit1.Text:=quEUName.AsString;
      fmAddEU.cxCalcEdit1.Value:=RoundEx(quEUProcent1.AsFloat*1000)/1000;
      fmAddEU.cxCalcEdit2.Value:=RoundEx(quEUProcent2.AsFloat*1000)/1000;
      fmAddEU.cxCalcEdit3.Value:=RoundEx(quEUProcent3.AsFloat*1000)/1000;
      fmAddEU.cxCalcEdit4.Value:=RoundEx(quEUProcent4.AsFloat*1000)/1000;

      fmAddEU.ShowModal;
      if fmAddEU.ModalResult=mrOk then
      begin
        ViewEU.BeginUpdate;
        try
          try
            iR:=quEUCode.AsInteger;

            quE.SQL.Clear;
            quE.SQL.Add('Update EUSPR Set Name='''+Copy(fmAddEU.cxTextEdit1.Text,1,30)+'''');
            quE.SQL.Add(', Procent1='+fRToStr(fmAddEU.cxCalcEdit1.Value));
            quE.SQL.Add(', Procent2='+fRToStr(fmAddEU.cxCalcEdit2.Value));
            quE.SQL.Add(', Procent3='+fRToStr(fmAddEU.cxCalcEdit3.Value));
            quE.SQL.Add(', Procent4='+fRToStr(fmAddEU.cxCalcEdit4.Value));
            quE.SQL.Add('where Code='+IntToStr(iR));
            quE.ExecSQL;

            quEU.Refresh;
            quEU.Locate('Code',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewEU.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmEU.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prAddEU') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  fmAddEU.Caption:='������������ �����: ����������.';
  fmAddEU.cxTextEdit1.Text:='';
  fmAddEU.cxCalcEdit1.Value:=0;
  fmAddEU.cxCalcEdit2.Value:=0;
  fmAddEU.cxCalcEdit3.Value:=0;
  fmAddEU.cxCalcEdit4.Value:=0;

  fmAddEU.ShowModal;
  if fmAddEU.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewEU.BeginUpdate;
      try
        if quEU.Locate('Name',fmAddEU.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          quEUMax.Open;
          iMax:=quEUMaxId.AsInteger+1;
          quEUMax.Close;

          quA.SQL.Clear;
          quA.SQL.Add('INSERT into EUSPR values ('+IntToStr(iMax)+','''+Copy(fmAddEU.cxTextEdit1.Text,1,30)+''','+fRToStr(fmAddEU.cxCalcEdit1.Value)+','+fRToStr(fmAddEU.cxCalcEdit2.Value)+','+fRToStr(fmAddEU.cxCalcEdit3.Value)+','+fRToStr(fmAddEU.cxCalcEdit4.Value)+')');
          quA.ExecSQL;

          quEU.Refresh;
          quEU.Locate('Code',iMax,[]);
        end;
      finally
        ViewEU.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmEU.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmEU.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmEU.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmEU.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmEU.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmEU.FormShow(Sender: TObject);
begin
  cxTextEdit1.Clear;
end;

procedure TfmEU.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  with dmMC do
  begin
    ViewEU.BeginUpdate;
    try
      quEU.Locate('Name',cxTextEdit1.Text,[loCaseInsensitive,loPartialKey]);
    finally
      ViewEU.EndUpdate;
    end;
  end;
end;

end.
