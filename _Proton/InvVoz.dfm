object fmInvVoz: TfmInvVoz
  Left = 298
  Top = 281
  Width = 934
  Height = 834
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103' '#1089#1082#1083#1072#1076#1072' '#1074#1086#1079#1074#1088#1072#1090#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 926
    Height = 65
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 12
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    InternalVer = 1
    object Label5: TLabel
      Left = 16
      Top = 9
      Width = 33
      Height = 13
      Caption = #1054#1090#1076#1077#1083
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 381
      Top = 11
      Width = 113
      Height = 41
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1076#1072#1085#1085#1099#1077
      TabOrder = 0
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 161
      Top = 23
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Width = 208
    end
    object cxLabel1: TcxLabel
      Left = 162
      Top = 6
      Caption = #1060#1072#1081#1083
      Transparent = True
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 14
      Top = 23
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'Name'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmP.dsquDepVoz2
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      Width = 121
    end
    object cxButton1: TcxButton
      Left = 655
      Top = 11
      Width = 89
      Height = 41
      Caption = #1054#1073#1088#1072#1073#1086#1090#1072#1090#1100
      TabOrder = 4
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 512
      Top = 11
      Width = 129
      Height = 41
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      TabOrder = 5
      WordWrap = True
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'SpeedItem1'
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = 'SpeedItem1|'
      Spacing = 1
      Left = 844
      Top = 12
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel|'
      Spacing = 1
      Left = 764
      Top = 12
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 604
    Align = alBottom
    Style.BorderStyle = ebsOffice11
    TabOrder = 1
    Height = 197
    Width = 926
  end
  object PBar1: TcxProgressBar
    Left = 0
    Top = 583
    Align = alBottom
    ParentColor = False
    Position = 100.000000000000000000
    Properties.BarBevelOuter = cxbvRaised
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clBlue
    Properties.EndColor = 16745090
    Properties.PeakValue = 100.000000000000000000
    Style.Color = clWhite
    TabOrder = 2
    Visible = False
    Width = 926
  end
  object Panel1: TPanel
    Left = 0
    Top = 65
    Width = 926
    Height = 518
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 3
    object Edit1: TEdit
      Left = 264
      Top = 72
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object GrInvVoz: TcxGrid
      Left = 1
      Top = 1
      Width = 924
      Height = 516
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      object ViewInvVoz: TcxGridDBTableView
        DragMode = dmAutomatic
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        OnCellClick = ViewInvVozCellClick
        OnCellDblClick = ViewInvVozCellDblClick
        DataController.DataSource = dsteInvVoz
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object ViewInvVozRecId: TcxGridDBColumn
          Caption = #8470
          DataBinding.FieldName = 'RecId'
          Options.Editing = False
          Width = 42
        end
        object ViewInvVozIdCode1: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
          DataBinding.FieldName = 'IdCode1'
          Options.Editing = False
          Options.Grouping = False
          Width = 67
        end
        object ViewInvVozNameC: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NameC'
          Options.Editing = False
          Options.Grouping = False
          Width = 360
        end
        object ViewInvVozQ: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
          DataBinding.FieldName = 'QOut'
          Options.Editing = False
          Options.Grouping = False
          Styles.Content = dmMC.cxStyle5
          Width = 90
        end
        object ViewInvVozQFact: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086' '#1092#1072#1082#1090'.'
          DataBinding.FieldName = 'QFact'
          Options.Editing = False
          Options.Grouping = False
          Styles.Content = dmMC.cxStyle23
          Width = 84
        end
        object ViewInvVozQRazn: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086'.'#1056#1072#1079#1085#1080#1094#1072
          DataBinding.FieldName = 'QRazn'
          Options.Editing = False
          Styles.Content = dmMC.cxStyle23
          Width = 96
        end
        object ViewInvVozEdIzm: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'EdIzm'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1064#1090'.'
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1050#1075'.'
              Value = 2
            end>
          Options.Editing = False
        end
        object ViewInvVozRemnCode: TcxGridDBColumn
          Caption = #1054#1073#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'RemnCode'
          Options.Editing = False
          Styles.Content = dmMC.cxStyle23
          Width = 104
        end
        object ViewInvVozLastCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1083#1077#1076#1085#1080#1081' '#1087#1086#1089#1090'.'
          DataBinding.FieldName = 'NameLastCli'
        end
      end
      object LevInvVoz: TcxGridLevel
        GridView = ViewInvVoz
      end
    end
  end
  object teInvVoz: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 56
    Top = 96
    object teInvVozIdCode1: TIntegerField
      FieldName = 'IdCode1'
    end
    object teInvVozNameC: TStringField
      FieldName = 'NameC'
      Size = 100
    end
    object teInvVozEdIzm: TIntegerField
      FieldName = 'EdIzm'
    end
    object teInvVozQOut: TFloatField
      FieldName = 'QOut'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object teInvVozQFact: TFloatField
      FieldName = 'QFact'
      OnChange = teInvVozQFactChange
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object teInvVozQRazn: TFloatField
      FieldName = 'QRazn'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object teInvVozTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
    object teInvVozRemnCode: TFloatField
      FieldName = 'RemnCode'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object teInvVozNameLastCli: TStringField
      FieldName = 'NameLastCli'
      Size = 50
    end
  end
  object dsteInvVoz: TDataSource
    DataSet = teInvVoz
    Left = 52
    Top = 148
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 464
    Top = 88
  end
  object amInvVoz: TActionManager
    Left = 388
    Top = 88
    StyleName = 'XP Style'
    object acChCli: TAction
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
    end
    object acRdBar: TAction
      ShortCut = 16449
      OnExecute = acRdBarExecute
    end
    object acRdBar1: TAction
      Caption = 'acRdBar1'
      ShortCut = 16450
      OnExecute = acRdBar1Execute
    end
    object acShowPost: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      OnExecute = acShowPostExecute
    end
    object acFactFromPredz: TAction
      Caption = #1060#1072#1082#1090' '#1082#1072#1082' '#1082#1086#1083'-'#1074#1086' '#1074' '#1087#1088#1077#1076#1079#1072#1103#1074#1082#1077
      OnExecute = acFactFromPredzExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 192
    Top = 96
    object N5: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      OnClick = N5Click
    end
    object N3: TMenuItem
      Action = acShowPost
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1086#1074
    end
    object N4: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1092#1072#1082#1090
      OnClick = N4Click
    end
    object N6: TMenuItem
      Action = acFactFromPredz
    end
    object N7: TMenuItem
      Caption = #1059#1073#1088#1072#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1099
      OnClick = N7Click
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 304
    Top = 96
  end
  object taFact: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 128
    Top = 100
    object taFactCode: TIntegerField
      FieldName = 'Code'
    end
    object taFactQF: TFloatField
      FieldName = 'QF'
      DisplayFormat = '0.000'
    end
  end
  object dstaFact: TDataSource
    DataSet = taFact
    Left = 132
    Top = 148
  end
end
