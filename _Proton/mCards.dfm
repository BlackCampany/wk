object fmCards: TfmCards
  Left = 517
  Top = 221
  Width = 1080
  Height = 727
  Caption = #1050#1072#1088#1090#1086#1095#1082#1080' '#1090#1086#1074#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 670
    Width = 1064
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 574
    Width = 1064
    Height = 96
    Align = alBottom
    BevelInner = bvLowered
    Color = 16776176
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 22
      Width = 32
      Height = 13
      Caption = #1055#1086#1080#1089#1082
    end
    object TextEdit1: TcxTextEdit
      Left = 64
      Top = 16
      BeepOnEnter = False
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = TextEdit1Click
      Width = 133
    end
    object cxButton1: TcxButton
      Left = 220
      Top = 52
      Width = 69
      Height = 25
      Caption = #1087#1086' '#1082#1086#1076#1091
      TabOrder = 4
      OnClick = cxButton1Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxCheckBox1: TcxCheckBox
      Left = 208
      Top = 18
      Caption = #1053#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      TabOrder = 1
      Width = 121
    end
    object cxButton2: TcxButton
      Left = 60
      Top = 51
      Width = 81
      Height = 25
      Action = acFindName
      Caption = #1087#1086' '#1085#1072#1079#1074#1072#1085#1080#1102
      Default = True
      TabOrder = 2
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 148
      Top = 51
      Width = 65
      Height = 25
      Action = acFindBar
      Caption = #1087#1086' '#1064#1050
      TabOrder = 3
      Colors.Default = clWhite
      Colors.Normal = clWhite
      Glyph.Data = {
        96030000424D9603000000000000360000002800000010000000120000000100
        18000000000060030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFEFA000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
    object Gr1: TcxGrid
      Left = 590
      Top = 2
      Width = 472
      Height = 92
      Align = alRight
      TabOrder = 5
      LookAndFeel.Kind = lfOffice11
      object Vi1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMC.dsquPost1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object Vi1Name: TcxGridDBColumn
          Caption = #1054#1090#1076#1077#1083
          DataBinding.FieldName = 'Name'
          Visible = False
          Width = 56
        end
        object Vi1NameCli: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'NameCli'
          Width = 132
        end
        object Vi1DateInvoice: TcxGridDBColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'DateInvoice'
        end
        object Vi1Number: TcxGridDBColumn
          Caption = #8470
          DataBinding.FieldName = 'Number'
          Width = 49
        end
        object Vi1Kol: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Kol'
          Width = 52
        end
        object Vi1CenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'CenaTovar'
          Width = 50
        end
        object Vi1Procent: TcxGridDBColumn
          Caption = '% '#1085#1072#1094
          DataBinding.FieldName = 'Procent'
          Width = 49
        end
        object Vi1NewCenaTovar: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'NewCenaTovar'
          Width = 47
        end
        object Vi1SumCenaTovarPost: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
          DataBinding.FieldName = 'SumCenaTovarPost'
          Visible = False
        end
        object Vi1SumCenaTovar: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076'.'
          DataBinding.FieldName = 'SumCenaTovar'
          Visible = False
        end
      end
      object ViCenn: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmMC.dstaCen
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViCennRecId: TcxGridDBColumn
          Caption = #8470' '#1087#1087
          DataBinding.FieldName = 'RecId'
          Visible = False
        end
        object ViCennIdCard: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'IdCard'
          Width = 53
        end
        object ViCennFullName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'FullName'
          Width = 145
        end
        object ViCennEdIzm: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084
          DataBinding.FieldName = 'EdIzm'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1096#1090'.'
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1082#1075'.'
              Value = 2
            end>
          Width = 46
        end
        object ViCennPrice1: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'Price1'
          Styles.Content = dmMC.cxStyle5
        end
        object ViCennCountry: TcxGridDBColumn
          Caption = #1057#1090#1088#1072#1085#1072
          DataBinding.FieldName = 'Country'
          Width = 84
        end
        object ViCennBarCode: TcxGridDBColumn
          Caption = #1064#1090#1088#1080#1093#1082#1086#1076
          DataBinding.FieldName = 'BarCode'
          Width = 53
        end
        object ViCennQuant: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'Quant'
        end
        object ViCennQuanrR: TcxGridDBColumn
          Caption = #1054#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'QuanrR'
        end
      end
      object le1: TcxGridLevel
        GridView = Vi1
      end
      object Le2: TcxGridLevel
        GridView = ViCenn
        Visible = False
      end
    end
    object cxButton7: TcxButton
      Left = 332
      Top = 64
      Width = 129
      Height = 25
      Caption = #1055#1077#1095'.'#1101#1090#1080#1082#1077#1090#1082#1080
      TabOrder = 9
      OnClick = cxButton7Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton5: TcxButton
      Left = 332
      Top = 36
      Width = 129
      Height = 25
      Caption = #1055#1077#1095#1072#1090#1100' '#1062#1077#1085#1085#1080#1082#1080
      TabOrder = 7
      OnClick = cxButton5Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton6: TcxButton
      Left = 400
      Top = 8
      Width = 61
      Height = 25
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      TabOrder = 8
      OnClick = cxButton6Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton4: TcxButton
      Left = 332
      Top = 8
      Width = 61
      Height = 25
      Caption = #1054#1095#1077#1088#1077#1076#1100
      TabOrder = 6
      OnClick = cxButton4Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 488
      Top = 16
      Width = 41
      Height = 37
      TabOrder = 10
      OnClick = cxButton8Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton9: TcxButton
      Left = 468
      Top = 64
      Width = 81
      Height = 25
      Caption = #1058#1072#1073#1072#1095#1085'. '#1080#1079#1076
      TabOrder = 11
      OnClick = cxButton9Click
      Colors.Default = clWhite
      Colors.Normal = clWhite
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 217
    Height = 526
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 2
    object CardsTree: TTreeView
      Left = 2
      Top = 2
      Width = 213
      Height = 522
      Align = alClient
      Images = dmMC.ImageList1
      Indent = 19
      PopupMenu = PopupMenu1
      ReadOnly = True
      RightClickSelect = True
      TabOrder = 0
      OnChange = CardsTreeChange
      OnDragDrop = CardsTreeDragDrop
      OnDragOver = CardsTreeDragOver
      OnExpanding = CardsTreeExpanding
    end
  end
  object RxSplitter1: TRxSplitter
    Left = 217
    Top = 48
    Width = 3
    Height = 526
    ControlFirst = Panel2
    Align = alLeft
  end
  object Panel3: TPanel
    Left = 220
    Top = 48
    Width = 844
    Height = 526
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 4
    object Edit1: TEdit
      Left = 636
      Top = 484
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
      OnKeyDown = Edit1KeyDown
    end
    object CardGr: TcxGrid
      Left = 2
      Top = 2
      Width = 840
      Height = 522
      Align = alClient
      PopupMenu = PopupMenu2
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object CardsView: TcxGridDBTableView
        DragMode = dmAutomatic
        OnStartDrag = CardsViewStartDrag
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = CardsViewCellDblClick
        OnCustomDrawCell = CardsViewCustomDrawCell
        OnSelectionChanged = CardsViewSelectionChanged
        DataController.DataModeController.GridModeBufferCount = 500
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = dmMC.dsquCards
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object CardsViewID: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'ID'
          Styles.Content = dmMC.cxStyle5
        end
        object CardsViewName: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'Name'
          Styles.Content = dmMC.cxStyle5
          Width = 208
        end
        object CardsViewBarCode: TcxGridDBColumn
          Caption = #1064#1090#1088#1080#1093#1082#1086#1076
          DataBinding.FieldName = 'BarCode'
          Width = 87
        end
        object CardsViewShRealize: TcxGridDBColumn
          Caption = #1064#1050' '#1090#1080#1087
          DataBinding.FieldName = 'ShRealize'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1060#1086#1088#1084#1080#1088#1091#1077#1084#1099#1081
              Value = 1
            end
            item
              Description = #1050#1086#1088#1086#1090#1082#1080#1081
              Value = 2
            end>
          Width = 68
        end
        object CardsViewTovarType: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1090#1086#1074#1072#1088#1072
          DataBinding.FieldName = 'TovarType'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1064#1090#1091#1095#1085#1099#1081
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1042#1077#1089#1086#1074#1086#1081
              Value = 1
            end>
          Width = 65
        end
        object CardsViewReserv1: TcxGridDBColumn
          Caption = #1054#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'Reserv1'
          Styles.Content = dmMC.cxStyle1
        end
        object CardsViewRemn: TcxGridDBColumn
          Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
          DataBinding.FieldName = 'Qremn'
          Visible = False
          Options.Editing = False
          Styles.Content = dmMC.cxStyle23
          Styles.Header = dmMC.cxStyle8
          Width = 121
        end
        object CardsViewFullName: TcxGridDBColumn
          Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'FullName'
          Styles.Content = dmMC.cxStyle5
          Width = 116
        end
        object CardsViewEdIzm: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'EdIzm'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1064#1090'.'
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1050#1075'.'
              Value = 2
            end>
        end
        object CardsViewNDS: TcxGridDBColumn
          Caption = #1053#1044#1057
          DataBinding.FieldName = 'NDS'
        end
        object CardsViewOKDP: TcxGridDBColumn
          Caption = #1054#1050#1044#1055
          DataBinding.FieldName = 'OKDP'
        end
        object CardsViewWeght: TcxGridDBColumn
          Caption = #1042#1077#1089
          DataBinding.FieldName = 'Weght'
        end
        object CardsViewSrokReal: TcxGridDBColumn
          Caption = #1057#1088#1086#1082' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
          DataBinding.FieldName = 'SrokReal'
        end
        object CardsViewTareWeight: TcxGridDBColumn
          Caption = #1042#1077#1089' '#1090#1072#1088#1099
          DataBinding.FieldName = 'TareWeight'
        end
        object CardsViewKritOst: TcxGridDBColumn
          Caption = #1050#1088#1080#1090#1080#1095#1077#1089#1082#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
          DataBinding.FieldName = 'KritOst'
        end
        object CardsViewBHTStatus: TcxGridDBColumn
          Caption = #1058#1077#1085#1076#1077#1088#1085#1099#1081' '#1090#1086#1074#1072#1088
          DataBinding.FieldName = 'BHTStatus'
        end
        object CardsViewUKMAction: TcxGridDBColumn
          Caption = #1050#1072#1089#1089#1072
          DataBinding.FieldName = 'UKMAction'
        end
        object CardsViewDataChange: TcxGridDBColumn
          Caption = #1048#1079#1084#1077#1085#1077#1085
          DataBinding.FieldName = 'DataChange'
        end
        object CardsViewNSP: TcxGridDBColumn
          Caption = #1053#1057#1055
          DataBinding.FieldName = 'NSP'
        end
        object CardsViewWasteRate: TcxGridDBColumn
          Caption = #1054#1090#1093#1086#1076#1099
          DataBinding.FieldName = 'WasteRate'
        end
        object CardsViewCena: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'Cena'
          Styles.Content = dmMC.cxStyle5
        end
        object CardsViewStatus: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'Status'
        end
        object CardsViewPrsision: TcxGridDBColumn
          Caption = #1058#1086#1095#1085#1086#1089#1090#1100
          DataBinding.FieldName = 'Prsision'
        end
        object CardsViewMargGroup: TcxGridDBColumn
          Caption = #1052#1072#1088#1075#1080#1085#1072#1083#1100#1085#1072#1103' '#1075#1088#1091#1087#1087#1072
          DataBinding.FieldName = 'MargGroup'
        end
        object CardsViewNameCountry: TcxGridDBColumn
          Caption = #1057#1090#1088#1072#1085#1072
          DataBinding.FieldName = 'NameCountry'
          Width = 171
        end
        object CardsViewNAMEBRAND: TcxGridDBColumn
          Caption = #1041#1088#1101#1085#1076
          DataBinding.FieldName = 'NAMEBRAND'
          Width = 129
        end
        object CardsViewV02: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089' '#1090#1086#1074#1072#1088#1072
          DataBinding.FieldName = 'V02'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'SID'
            end>
          Properties.ListSource = dmMC.dsquCateg
        end
        object CardsViewReserv2: TcxGridDBColumn
          Caption = #1053#1072#1094#1077#1085#1082#1072' '#1085#1072' '#1090#1086#1074#1072#1088
          DataBinding.FieldName = 'Reserv2'
          Styles.Content = dmMC.cxStyle13
        end
        object CardsViewFixPrice: TcxGridDBColumn
          Caption = #1060#1080#1082#1089#1080#1088#1086#1074#1072#1085#1085#1072#1103' '#1094#1077#1085#1072
          DataBinding.FieldName = 'FixPrice'
        end
        object CardsViewV04: TcxGridDBColumn
          Caption = #1058#1086#1087
          DataBinding.FieldName = 'V04'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = ' '
              ImageIndex = 0
              Value = 0
            end
            item
              Description = 'T'
              Value = 1
            end>
          Options.Editing = False
        end
        object CardsViewV05: TcxGridDBColumn
          Caption = #1053#1086#1074#1080#1085#1082#1072
          DataBinding.FieldName = 'V05'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 0
            end
            item
              Description = 'N'
              Value = 1
            end>
          Options.Editing = False
        end
        object CardsViewKol: TcxGridDBColumn
          Caption = #1052#1080#1085'. '#1082#1086#1083'-'#1074#1086' '#1076#1083#1103' '#1079#1072#1082#1072#1079#1072
          DataBinding.FieldName = 'Kol'
        end
        object CardsViewV06: TcxGridDBColumn
          Caption = #1052#1080#1085'.'#1079#1072#1087#1072#1089' '#1074' '#1076#1085#1103#1093
          DataBinding.FieldName = 'V06'
        end
        object CardsViewV07: TcxGridDBColumn
          Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1079#1072#1087#1072#1089' '#1074' '#1076#1085#1103#1093
          DataBinding.FieldName = 'V07'
        end
        object CardsViewV08: TcxGridDBColumn
          Caption = #1050#1085#1086#1087#1082#1072' '#1074' '#1074#1077#1089#1072#1093
          DataBinding.FieldName = 'V08'
        end
        object CardsViewVol: TcxGridDBColumn
          Caption = #1054#1073#1098#1077#1084
          DataBinding.FieldName = 'Vol'
          Options.Editing = False
        end
        object CardsViewV11: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
          DataBinding.FieldName = 'V11'
        end
        object CardsViewNAMEM: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
          DataBinding.FieldName = 'NAMEM'
          Width = 100
        end
        object CardsViewTypeV: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
          DataBinding.FieldName = 'TYPEVOZ'
          Width = 109
        end
      end
      object CardLevel: TcxGridLevel
        GridView = CardsView
      end
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1064
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 5
    InternalVer = 1
    object cxCheckBox2: TcxCheckBox
      Left = 88
      Top = 13
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1086#1089#1090#1072#1090#1082#1080' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
      TabOrder = 0
      OnClick = cxCheckBox2Click
      Width = 185
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 764
      Top = 4
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E21
        0E210E211F7C1F7C1F7C1F7C1F7C1F7CD435FF73BE6FBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C1F7C1F7C1F7C1F7CD435BE6F7E5F5E5F5E5F5D5B5D5B7D5F
        5C5F0E211F7C1F7C1F7C1F7C1F7C1F7CD435DF737E5F5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C1F7C1F7C1F7C1F7CD435DF777F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7C1F7C1F7C1F7C1F7CD435FF7B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1F7C1F7C1F7CD4351F7C9F679F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7C1F7C6003C001E00060031F7CFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7CE000F75FF75FE0001F7C1F7C1F7C1F7CD435D435
        D435D4351F7C1F7C6003C001C001F75FF75FC001E00060031F7C1F7CD4359C2A
        D62D1F7C1F7C1F7CC001E313F75FF75FF75FF75F6003E000D435D435D435B535
        1F7C1F7C1F7C1F7CE31360036003ED3BE313C001C001E3131F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C6003ED3BED3BC0011F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CED3BE313E313ED3B1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      Spacing = 1
      Left = 24
      Top = 4
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acEdit
      BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E210E21
        0E210E211F7C1F7C00001F7C1F7CD435FF73BE6FBE6BBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C000000001F7CD435BE6F7E5F5E5F5E5F5E5F5D5B5D5B7D5F
        5C5F0E211F7C1F7C42082129C2410000DF737E5F5E5B5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C10426277036FC56A4A297F5F7F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7CD65AA17B4277E46E00001F4B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1042817B2373C46A4A299F639F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7CD65AC07F6277036F0000FF7FFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7C1042A17B4277E46E4A294A291F7C1F7CD435D435
        D435D4351F7C1F7C1F7C1F7CD65AE07F817B2373000000001F7C1F7CD4359C2A
        D62D1F7C1F7C1F7C1F7C1F7C1F7C1042C07F0000000000000000D435D435B535
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C10423967524A524A10424A291F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD65ABD77D65AD65A104200001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C10421042104210421F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Spacing = 1
      Left = 84
      Top = 4
      OnClick = acEditExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C6B451F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CC36524556B45104200000000000000000000AD35CF35AD350000
        1F7C1F7C1F7C1F7C637EA06944594C452B5AFF7FF239573EB746D84A353A123A
        1F7C1F7C1F7C1F7C1F7C1F7C437AA2690D5255461C4BFF63FF73FF7BFF7F9846
        B02D1F7C1F7C1F7C1F7C1F7C1042FF7FD05E593AFF5BFF63FF6FFF7FFF7BFF6F
        1432AA351F7C1F7C1F7C1F7C1042FF7F13633D4FBF53DF5BFF6BFF73FF6FFF6B
        773AAD311F7C1F7C1F7C1F7C1042FF7FFF7F5B579F4F9F4FDF5BFF67FF63FF5F
        993AAE311F7C1F7C1F7C1F7C1042FF7F1042F84EDF5B9F579F539F537F4BDF57
        1532A9391F7C1F7C1F7C1F7C1042FF7FFF7FD8565D5BFF7FBF6BDF5BFF5BDB42
        F03D1F7C1F7C1F7C1F7C1F7C1042FF7F10421042B25AFA5E5B633B53773E3342
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FCF56AF56FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7F104210421042FF7F0000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F0000186300001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C10421042104210421042104200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088
      Spacing = 1
      Left = 14
      Top = 4
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CD4350E210E210E210E210E210E210E210E21
        0E210E211F7C1F7C1F7C1F7C1F7CD435FF73BE6FBE6BBE6BBE6BBE6B9E67BE6B
        7D630E211F7C1F7C1F7C1F7C1F7CD435BF6F1F4B1F4B1F4B1F4B1F4B1F4B1F4B
        5C5F0E211F7C1F7C1F7C1F7C1F7CD435DF737E5F5E5B5E5B5E5B5E5B5E577E63
        5C6350251F7C1F7C1F7C1F7C1F7CD435DF777F5F7F5F7F5F7F5B5E5B5E5B7E63
        5C6350251F7C1F7C1F7C1F7C1F7CD435FF7B1F4B1F4B1F4B1F4B1F4B1F4B1F4B
        7D67712D1F7C1F7C1F7C1F7C1F7CD4351F7C9F679F639F637F637F639F63DF6F
        5C67712D1F7C1F7C1F7C1F7C1F7CD4351F7C1F7CFF7FFF7FFF7BFF7B5C6BD856
        544A932D1F7C1F7C1F7C1F7C1F7CD4351F7C1F7C1F7C1F7C1F7C1F7CD435D435
        D435D4351F7C1F7C0048007C007CC67CC67CC67CCE7DCE7DCE7D1F7CD4359C2A
        D62D1F7C1F7C1F7C007CCE7DB57EB57EB57ECE7DC67CC67C00481F7C1F7C1F7C
        1F7C1F7C1F7C1F7CCE7DCE7DC67CC67CC67C007C007C007C00481F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100
      Spacing = 1
      Left = 314
      Top = 4
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      Action = acBarView
      BtnCaption = #1064#1090#1088#1080#1093#1082#1086#1076
      Caption = #1064#1090#1088#1080#1093#1082#1086#1076
      Glyph.Data = {
        96030000424D9603000000000000360000002800000010000000120000000100
        18000000000060030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFEFA000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF
        000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFF
        FF000000FFFFFF000000000000FFFFFF000000FFFFFF000000000000FFFFFF00
        0000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1064#1090#1088#1080#1093#1082#1086#1076'|'
      Spacing = 1
      Left = 394
      Top = 4
      Visible = True
      OnClick = acBarViewExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem7: TSpeedItem
      Action = acJurnal
      BtnCaption = #1046#1091#1088#1085#1072#1083
      Caption = #1046#1091#1088#1085#1072#1083
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF29200829200829
        2008FFFFFFFFFFFF0000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF292008292008EFE3CEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF292008
        2920089C694AEFE3CEC68E4AC68E4A292008FFFFFFFFFFFF0000FFFFFF101410
        084D52107184000000FFFFFFFFFFFF292008292008CEC3B5DED7C69C694AC68E
        4AEFE3CEEFE3CE292008FFFFFFFFFFFF0000FFFFFF84828410DFEF18C7DE29B2
        D6525552211808735139BDB6ADA57539B586429C694AEFE7D6C68E4AC68E4A29
        2008FFFFFFFFFFFF0000FFFFFFB5B2B508EFF710D7EF21BEDE000000BDBEBD73
        5139946D39CEC7BDE7DBCE9C694AC68E4AF7E7D6EFE3D6292008FFFFFFFFFFFF
        0000FFFFFFFFFFFF84828408E7F718CFE721B6D6525552735139BDBAB5A57539
        B586429C694AF7E7DE9C694A9C694A292008FFFFFFFFFFFF0000FFFFFFFFFFFF
        B5B2B500F7FF10DFEF18C7DE0000007351399C6D39CECBC6E7DFD69C694A9C69
        4AC67121C67121292008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF84828408EF
        F710D7EF21BEDE525552C6C3BD8C5D4294654ACE7529CE7521CE7121C6712129
        2008FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFB5B2B500FBFF08E7F718CFE700
        00008C5D42C67529CE7929CE7929CE7929EFDFCEEFDFCE292008FFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF29200884828400F7FF000000000000000000D68231
        D67D31EFDFC6EFDFCEBD7152BD7152FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF292008B5B2B5EFEFEFB5B2B5848284000000E7D7BDEFDBC6BD7152BD71
        52FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF92
        42848284CECFCE949294848284525552BD7152FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF292008EF9242B5B2B5EFEFEFB5
        B2B5848284000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF846129D6BE94BD7152848284848284848284FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFBD7152BD7152FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Hint = #1046#1091#1088#1085#1072#1083' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
      Spacing = 1
      Left = 454
      Top = 4
      OnClick = acJurnalExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      Action = acNotUse
      BtnCaption = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Caption = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6848284CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D684828484
        8284848284CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6848284848284848284848284848284CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6848284848284848284CE
        D3D6848284848284848284CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6848284848284CED3D6CED3D6CED3D6848284848284848284CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6848284CED3D6CED3D6CE
        D3D6CED3D6CED3D6848284848284848284CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D68482848482
        84CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6848284CED3D6000000FFFFFFFFFFFF000000
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6000000FFFFFFFFFFFF000000CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6000000FFFFFFFFFFFF000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
      Spacing = 1
      Left = 234
      Top = 4
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      Action = acMove1
      BtnCaption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Glyph.Data = {
        1E060000424D1E06000000000000360000002800000018000000150000000100
        180000000000E8050000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF846100FF9E29
        8C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84
        6100FF9E29DEA67BFF9E298C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF846100DEA67BDEA67BDEA67BDEA67BFF9E298C6D18FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF846100DEA67BFFFFC6FFFFC6DEA67BDEA67BDEA67BFF9E
        298C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7B8C6D188C6D188C6D18FFFFC6FFFFC6
        DEA67B8C6D188C6D188C6D18E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C
        7D31FFFFC6FFFFC6FFF3AD9C7D31FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCB84D6B26BD6B26BD6B26BD6B26BFFFF
        FFFFFFFFFFFFFFAD8A42FFFFC6FFFFC6DEA67BFFD794FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7BFFD794DEA67B
        AD8A39FFFFFFFFFFFFFFFFFFBD964ADEA67BFFF3ADFFFFC6AD8A42FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD794E7BE73FF
        F3ADFFE79CBD964AFFFFFFFFFFFFFFFFFFFFFFFFBD964AFFE79CFFF3ADE7BE73
        FFD794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFAD8A42FFFFC6FFF3ADDEA67BBD964AFFFFFFFFFFFFFFFFFFAD8A39DEA67BFF
        D794E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFD794DEA67BFFFFC6FFFFC6AD8A42FFFFFFFFFFFFFFFFFFD6B2
        6BD6B26BD6B26BD6B26BEFCB84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF9C7D31FFF3ADFFFFC6FFFFC69C7D31FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFE7BE7B8C6D188C6D188C6D18DEA67BFFFFC6FF
        FFC68C6D188C6D188C6D18E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA6
        7BDEA67BDEA67BFFFFC6FFFFC6DEA67B846100FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C6D18FF9E29DEA67BDEA67BDEA67BDEA67B846100FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA67BFF9E29846100FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E2984
        6100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF8C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF}
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      Spacing = 1
      Left = 534
      Top = 4
      Visible = True
      OnClick = acMove1Execute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      Action = acRemns2
      BtnCaption = #1054#1089#1090#1072#1090#1086#1082
      Caption = #1054#1089#1090#1072#1090#1086#1082
      Glyph.Data = {
        66020000424D660200000000000036000000280000000D0000000E0000000100
        18000000000030020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C6D18FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF846100FF9E298C
        6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF8461
        00FF9E29DEA67BFF9E298C6D18FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFF846100DEA67BDEA67BDEA67BDEA67BFF9E298C6D18FFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFF846100DEA67BFFFFC6FFFFC6DEA67BDEA67BDEA67BFF9E29
        8C6D18FFFFFFFFFFFF00FFFFFFE7BE7B8C6D188C6D188C6D18FFFFC6FFFFC6DE
        A67B8C6D188C6D188C6D18E7BE7BFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF9C7D
        31FFFFC6FFFFC6FFF3AD9C7D31FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFAD8A42FFFFC6FFFFC6DEA67BFFD794FFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFBD964ADEA67BFFF3ADFFFFC6AD8A42FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFBD964AFFE79CFFF3ADE7BE73FF
        D794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFAD8A39DEA67BFFD7
        94E7BE7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFD6B26B
        D6B26BD6B26BD6B26BEFCB84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00}
      Hint = #1054#1089#1090#1072#1090#1086#1082
      Spacing = 1
      Left = 594
      Top = 4
      Visible = True
      OnClick = acRemns2Execute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem11: TSpeedItem
      Action = acExcel
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel|'
      Spacing = 1
      Left = 674
      Top = 4
      Visible = True
      OnClick = acExcelExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 428
    Top = 168
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 428
    Top = 96
  end
  object acCard: TActionManager
    Left = 536
    Top = 96
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 16
      ShortCut = 45
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 14
      ShortCut = 16453
      SecondaryShortCuts.Strings = (
        'F4')
      OnExecute = acEditExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 15
      ShortCut = 114
      OnExecute = acViewExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      ImageIndex = 13
      OnExecute = acDelExecute
    end
    object acFindBar: TAction
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1064#1050
      ShortCut = 123
      OnExecute = acFindBarExecute
    end
    object acBarView: TAction
      Caption = 'acBarView'
      ShortCut = 32841
      OnExecute = acBarViewExecute
    end
    object acJurnal: TAction
      Caption = #1046#1091#1088#1085#1072#1083
      OnExecute = acJurnalExecute
    end
    object acMove: TAction
      Caption = #1055#1077#1088#1077#1085#1086#1089
    end
    object acNotUse: TAction
      Caption = #1042' '#1085#1077#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077
    end
    object acMove1: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      SecondaryShortCuts.Strings = (
        'Ctrl+F6'
        'Ctrl+6')
      OnExecute = acMove1Execute
    end
    object acTransGr: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      OnExecute = acTransGrExecute
    end
    object acRemns2: TAction
      Caption = #1054#1089#1090#1072#1090#1086#1082
      ShortCut = 117
      OnExecute = acRemns2Execute
    end
    object acCardUse: TAction
      Caption = #1057#1085#1103#1090#1100' '#1053#1086#1074#1072#1103
    end
    object acFilials: TAction
      Caption = #1055#1086' '#1092#1080#1083#1080#1072#1083#1072#1084
      ShortCut = 119
    end
    object acTransList: TAction
      Caption = #1041#1091#1092#1077#1088' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1094#1077#1085
      ShortCut = 120
      OnExecute = acTransListExecute
    end
    object acExcel: TAction
      OnExecute = acExcelExecute
    end
    object acManCat: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1082#1072#1090'. '#1084#1077#1085#1077#1076#1078#1077#1088#1072
      ShortCut = 16500
    end
    object acManCat1: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1082#1072#1090'. '#1084#1077#1085#1077#1076#1078#1077#1088#1072
    end
    object acTPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1101#1090#1080#1082#1077#1090#1086#1082
      ShortCut = 113
      OnExecute = acTPrintExecute
    end
    object acFindName: TAction
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1085#1072#1079#1074#1072#1085#1080#1102
      OnExecute = acFindNameExecute
    end
    object acReadBar: TAction
      ShortCut = 16449
      SecondaryShortCuts.Strings = (
        'F12')
      OnExecute = acReadBarExecute
    end
    object acReadBar1: TAction
      Caption = 'acReadBar1'
      ShortCut = 16450
      OnExecute = acReadBar1Execute
    end
    object acCashLoad: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1082#1072#1089#1089
      ShortCut = 116
      OnExecute = acCashLoadExecute
    end
    object acPrintCen: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074'.'
      ShortCut = 118
      OnExecute = acPrintCenExecute
    end
    object acCalcRemns: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1086#1089#1090#1072#1090#1082#1080
      ShortCut = 49234
      OnExecute = acCalcRemnsExecute
    end
    object acCardsGoods: TAction
      Caption = #1055#1088#1080#1074#1103#1079#1082#1072' '#1087#1086' '#1086#1090#1076#1077#1083#1072#1084
      ShortCut = 32836
      OnExecute = acCardsGoodsExecute
    end
    object acPostTov: TAction
      Caption = 'acPostTov'
      ShortCut = 32884
      OnExecute = acPostTovExecute
    end
    object acAddPrint: TAction
      Caption = 'acAddPrint'
      ShortCut = 16416
      OnExecute = acAddPrintExecute
    end
    object acPrintQuPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1086#1095'.'
      ShortCut = 32886
      OnExecute = acPrintQuPrintExecute
    end
    object acTermFile: TAction
      Caption = #1055#1086#1076#1075#1086#1090#1086#1074#1082#1072' '#1092#1072#1081#1083#1072' '#1076#1083#1103' '#1090#1077#1088#1084#1080#1085#1072#1083#1072
      OnExecute = acTermFileExecute
    end
    object acScaleIn: TAction
      Caption = #1042' '#1074#1077#1089#1072#1093
      ShortCut = 16498
      SecondaryShortCuts.Strings = (
        'Alt+F3')
      OnExecute = acScaleInExecute
    end
    object acAddInScale: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1087#1086#1079#1080#1094#1080#1080' '#1074' '#1074#1077#1089#1099
      OnExecute = acAddInScaleExecute
    end
    object acRecalcRemn1: TAction
      Caption = 'acRecalcRemn1'
      ShortCut = 49233
      OnExecute = acRecalcRemn1Execute
    end
    object acLoadScale: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1074' '#1074#1077#1089#1099' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1075#1086' '#1090#1086#1074#1072#1088#1072
      OnExecute = acLoadScaleExecute
    end
    object acReceipt: TAction
      Caption = #1057#1086#1089#1090#1072#1074
      OnExecute = acReceiptExecute
    end
    object acSelectAll: TAction
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      ShortCut = 49217
      OnExecute = acSelectAllExecute
    end
    object acViewLoad: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1079#1072#1075#1088#1091#1079#1086#1082
      ShortCut = 16460
      OnExecute = acViewLoadExecute
    end
    object acCalcMoveLine: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1083#1080#1085#1080#1080' '#1076#1074#1080#1078#1077#1085#1080#1103' '#1087#1086' '#1090#1086#1074#1072#1088#1091
      ShortCut = 24698
      OnExecute = acCalcMoveLineExecute
    end
    object acPartSel: TAction
      Caption = #1055#1072#1088#1090#1080#1086#1085#1085#1099#1081' '#1091#1095#1077#1090
      ShortCut = 16464
      OnExecute = acPartSelExecute
    end
    object acSearchCode: TAction
      Caption = 'acSearchCode'
      ShortCut = 16397
      OnExecute = acSearchCodeExecute
    end
    object acSetGroup: TAction
      Caption = #1043#1088#1091#1087#1087#1086#1074#1086#1077' '#1080#1079#1084#1077#1085#1077#1085#1080#1077
      ShortCut = 49223
      OnExecute = acSetGroupExecute
    end
    object acGetVReal: TAction
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078
      ShortCut = 49238
      OnExecute = acGetVRealExecute
    end
    object acLoadSDisc: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1057#1090#1086#1087'-'#1089#1082#1080#1076#1082#1080' '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084' '#1087#1086#1079#1080#1094#1080#1103#1084' '#1085#1072' '#1082#1072#1089#1089#1099
      OnExecute = acLoadSDiscExecute
    end
    object acPrintCutTail: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1101#1090#1080#1082#1077#1090#1086#1082' '#1085#1072' '#1086#1095#1077#1088#1077#1076#1100
      OnExecute = acPrintCutTailExecute
    end
    object acSaveAlco: TAction
      Caption = 'acSaveAlco'
      ShortCut = 24689
      OnExecute = acSaveAlcoExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16467
      OnExecute = acAddListExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 104
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 8
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1075#1088#1091#1087#1087#1091
      ImageIndex = 8
      OnClick = N2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1085#1072#1079#1074#1072#1085#1080#1077
      OnClick = N6Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ('#1087#1086#1076#1075#1088#1091#1087#1087#1091')'
      ImageIndex = 9
      OnClick = N4Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Action = acTransGr
    end
    object N10: TMenuItem
      Action = acManCat1
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1085#1072' '#1075#1088#1091#1087#1087#1091
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 532
    Top = 168
    object N7: TMenuItem
      Action = acCashLoad
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1074' '#1082#1072#1089#1089#1099' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1090#1086#1074#1072#1088#1086#1074
    end
    object N22: TMenuItem
      Action = acLoadSDisc
    end
    object N17: TMenuItem
      Action = acPrintCen
      Caption = #1055#1077#1095#1072#1090#1100' '#1094#1077#1085#1085#1080#1082#1086#1074
    end
    object N13: TMenuItem
      Action = acTPrint
    end
    object N23: TMenuItem
      Action = acPrintCutTail
    end
    object N19: TMenuItem
      Action = acViewLoad
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Action = acTermFile
    end
    object acAddInScale1: TMenuItem
      Action = acAddInScale
    end
    object N14: TMenuItem
      Action = acScaleIn
    end
    object N15: TMenuItem
      Action = acLoadScale
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Action = acReceipt
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N21: TMenuItem
      Action = acSetGroup
    end
  end
  object Timer2: TTimer
    Interval = 3000
    OnTimer = Timer2Timer
    Left = 312
    Top = 88
  end
  object teBuff: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 624
    Top = 100
    object teBuffCode: TIntegerField
      FieldName = 'Code'
    end
  end
end
