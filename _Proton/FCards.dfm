object fmFCards: TfmFCards
  Left = 235
  Top = 278
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088
  ClientHeight = 290
  ClientWidth = 781
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GridFCards: TcxGrid
    Left = 0
    Top = 0
    Width = 627
    Height = 290
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewFCards: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewFCardsCustomDrawCell
      DataController.DataSource = dmO.dsFCard
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewFCardsID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Styles.Content = dmO.cxStyle1
        Width = 52
      end
      object ViewFCardsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmO.cxStyle25
        Width = 145
      end
      object ViewFCardsTCARD: TcxGridDBColumn
        Caption = #1058#1050
        DataBinding.FieldName = 'TCARD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Description = #1085#1077#1090' '#1058#1050
            Value = 0
          end
          item
            Description = #1077#1089#1090#1100' '#1058#1050
            ImageIndex = 11
            Value = 1
          end>
        Width = 58
      end
      object ViewFCardsRemn: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Remn'
      end
      object ViewFCardsGR: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'GR'
        Width = 118
      end
      object ViewFCardsSGR: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGR'
        Width = 162
      end
    end
    object LevelFCards: TcxGridLevel
      GridView = ViewFCards
    end
  end
  object Panel1: TPanel
    Left = 627
    Top = 0
    Width = 154
    Height = 290
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 21
      Top = 128
      Width = 32
      Height = 13
      Caption = 'Label1'
      Transparent = True
    end
    object Label2: TLabel
      Left = 35
      Top = 208
      Width = 106
      Height = 13
      Caption = #1058#1077#1082#1091#1097#1080#1077' '#1086#1089#1090#1072#1090#1082#1080' '#1087#1086' '
    end
    object cxButton1: TcxButton
      Left = 20
      Top = 16
      Width = 105
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 20
      Top = 56
      Width = 105
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 18
      Top = 228
      TabStop = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnEditValueChanged = cxLookupComboBox1PropertiesEditValueChanged
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 123
    end
  end
  object ActionManager1: TActionManager
    Left = 128
    Top = 48
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 27
      OnExecute = Action1Execute
    end
  end
end
