unit dmFbPro;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, DB, FIBDataSet, pFIBDataSet,
  FIBQuery, pFIBQuery, pFIBStoredProc, ImgList, Controls, ADODB;

type
  TdmPro = class(TDataModule)
    OfficeRnDb: TpFIBDatabase;
    trSelRH: TpFIBTransaction;
    trUpdRH: TpFIBTransaction;
    trSelCards: TpFIBTransaction;
    quDocsRId: TpFIBDataSet;
    quDocsRIdID: TFIBIntegerField;
    quDocsRIdDATEDOC: TFIBDateField;
    quDocsRIdNUMDOC: TFIBStringField;
    quDocsRIdDATESF: TFIBDateField;
    quDocsRIdNUMSF: TFIBStringField;
    quDocsRIdIDCLI: TFIBIntegerField;
    quDocsRIdIDSKL: TFIBIntegerField;
    quDocsRIdSUMIN: TFIBFloatField;
    quDocsRIdSUMUCH: TFIBFloatField;
    quDocsRIdSUMTAR: TFIBFloatField;
    quDocsRIdSUMNDS0: TFIBFloatField;
    quDocsRIdSUMNDS1: TFIBFloatField;
    quDocsRIdSUMNDS2: TFIBFloatField;
    quDocsRIdPROCNAC: TFIBFloatField;
    quDocsRIdIACTIVE: TFIBIntegerField;
    quDocsRIdIDFROM: TFIBIntegerField;
    quDocsRIdIEDIT: TFIBSmallIntField;
    quDocsRIdPERSEDIT: TFIBStringField;
    quDocsRIdIPERSEDIT: TFIBIntegerField;
    quSpecRSel: TpFIBDataSet;
    quSpecRSelIDHEAD: TFIBIntegerField;
    quSpecRSelID: TFIBIntegerField;
    quSpecRSelIDCARD: TFIBIntegerField;
    quSpecRSelQUANT: TFIBFloatField;
    quSpecRSelIDM: TFIBIntegerField;
    quSpecRSelKM: TFIBFloatField;
    quSpecRSelPRICEIN: TFIBFloatField;
    quSpecRSelSUMIN: TFIBFloatField;
    quSpecRSelPRICER: TFIBFloatField;
    quSpecRSelSUMR: TFIBFloatField;
    quSpecRSelTCARD: TFIBSmallIntField;
    quSpecRSelNAMEC: TFIBStringField;
    quSpecRSelSM: TFIBStringField;
    quSpecRSelIDNDS: TFIBIntegerField;
    quSpecRSelSUMNDS: TFIBFloatField;
    quSpecRSelNAMENDS: TFIBStringField;
    quSpecRSelOPER: TFIBSmallIntField;
    quSpecRSelPROC: TFIBFloatField;
    quSpecRSelOUTNDSSUM: TFIBFloatField;
    quSpecRSelNAMECTO: TFIBStringField;
    quSpecRSelID1: TFIBIntegerField;
    quSpecRSelCATEGORY: TFIBIntegerField;
    quSpecRSelPRICEIN0: TFIBFloatField;
    quSpecRSelSUMIN0: TFIBFloatField;
    quSpecRSelSUMOUT0: TFIBFloatField;
    quSpecRSelSUMNDSOUT: TFIBFloatField;
    quSpecRSelINDS: TFIBIntegerField;
    quSpecRSelBZQUANTR: TFIBFloatField;
    quSpecRSelBZQUANTF: TFIBFloatField;
    quSpecRSelBZQUANTS: TFIBFloatField;
    trSelRS: TpFIBTransaction;
    trUpdRS: TpFIBTransaction;
    quC: TpFIBDataSet;
    quCID: TFIBIntegerField;
    quCPARENT: TFIBIntegerField;
    quCNAME: TFIBStringField;
    quCTTYPE: TFIBIntegerField;
    quCIMESSURE: TFIBIntegerField;
    quCINDS: TFIBIntegerField;
    quCMINREST: TFIBFloatField;
    quCLASTPRICEIN: TFIBFloatField;
    quCLASTPRICEOUT: TFIBFloatField;
    quCLASTPOST: TFIBIntegerField;
    quCIACTIVE: TFIBIntegerField;
    quCTCARD: TFIBIntegerField;
    quCCATEGORY: TFIBIntegerField;
    quCSPISSTORE: TFIBStringField;
    quCBB: TFIBFloatField;
    quCGG: TFIBFloatField;
    quCU1: TFIBFloatField;
    quCU2: TFIBFloatField;
    quCEE: TFIBFloatField;
    quCRCATEGORY: TFIBIntegerField;
    quCCOMMENT: TFIBStringField;
    quCCTO: TFIBIntegerField;
    quCCODEZAK: TFIBIntegerField;
    quMH: TpFIBDataSet;
    quMHID: TFIBIntegerField;
    quMHPARENT: TFIBIntegerField;
    quMHITYPE: TFIBIntegerField;
    quMHNAMEMH: TFIBStringField;
    quMHDEFPRICE: TFIBIntegerField;
    quMHEXPNAME: TFIBStringField;
    quMHNUMSPIS: TFIBSmallIntField;
    quMHISS: TFIBSmallIntField;
    quMHGLN: TFIBStringField;
    quCli: TpFIBDataSet;
    quCliID: TFIBIntegerField;
    quCliNAMECL: TFIBStringField;
    quCliFULLNAMECL: TFIBStringField;
    quCliINN: TFIBStringField;
    quCliPHONE: TFIBStringField;
    quCliMOL: TFIBStringField;
    quCliCOMMENT: TFIBStringField;
    quCliINDS: TFIBIntegerField;
    quCliIACTIVE: TFIBSmallIntField;
    quCliKPP: TFIBStringField;
    quCliADDRES: TFIBStringField;
    quCliNAMEOTP: TFIBStringField;
    quCliADROTPR: TFIBStringField;
    quCliRSCH: TFIBStringField;
    quCliKSCH: TFIBStringField;
    quCliBANK: TFIBStringField;
    quCliBIK: TFIBStringField;
    quCliSROKPLAT: TFIBIntegerField;
    quCliGLN: TFIBStringField;
    taNums: TpFIBDataSet;
    taNumsIT: TFIBIntegerField;
    taNumsSPRE: TFIBStringField;
    taNumsCURNUM: TFIBIntegerField;
    prGetId: TpFIBStoredProc;
    trSel: TpFIBTransaction;
    trUpd: TpFIBTransaction;
    quCPROC: TFIBFloatField;
    trSetSync: TpFIBTransaction;
    trGetSync: TpFIBTransaction;
    quSetSync: TpFIBQuery;
    quDocsRIdBZTYPE: TFIBSmallIntField;
    quDocsRIdBZSTATUS: TFIBSmallIntField;
    quDocsRIdBZSUMR: TFIBFloatField;
    quDocsRIdBZSUMF: TFIBFloatField;
    quDocsRIdBZSUMS: TFIBFloatField;
    quDocsRIdIDHCB: TFIBIntegerField;
    quDocsRCBID: TpFIBDataSet;
    taNumSCHF: TpFIBDataSet;
    taNumSCHFIMH: TFIBIntegerField;
    taNumSCHFIYY: TFIBIntegerField;
    taNumSCHFSPRE: TFIBStringField;
    taNumSCHFCURNUM: TFIBIntegerField;
    quDocsRCBIDID: TFIBIntegerField;
    quDocsRCBIDDATEDOC: TFIBDateField;
    quDocsRCBIDNUMDOC: TFIBStringField;
    quDocsRCBIDDATESF: TFIBDateField;
    quDocsRCBIDNUMSF: TFIBStringField;
    quDocsRCBIDIDCLI: TFIBIntegerField;
    quDocsRCBIDIDSKL: TFIBIntegerField;
    quDocsRCBIDSUMIN: TFIBFloatField;
    quDocsRCBIDSUMUCH: TFIBFloatField;
    quDocsRCBIDSUMTAR: TFIBFloatField;
    quDocsRCBIDSUMNDS0: TFIBFloatField;
    quDocsRCBIDSUMNDS1: TFIBFloatField;
    quDocsRCBIDSUMNDS2: TFIBFloatField;
    quDocsRCBIDPROCNAC: TFIBFloatField;
    quDocsRCBIDIACTIVE: TFIBIntegerField;
    quDocsRCBIDIDFROM: TFIBIntegerField;
    quDocsRCBIDBZTYPE: TFIBSmallIntField;
    quDocsRCBIDBZSTATUS: TFIBSmallIntField;
    quDocsRCBIDBZSUMR: TFIBFloatField;
    quDocsRCBIDBZSUMF: TFIBFloatField;
    quDocsRCBIDBZSUMS: TFIBFloatField;
    quDocsRCBIDIEDIT: TFIBSmallIntField;
    quDocsRCBIDPERSEDIT: TFIBStringField;
    quDocsRCBIDIPERSEDIT: TFIBIntegerField;
    quDocsRCBIDIDHCB: TFIBIntegerField;
    quSpecRSelBZQUANTSHOP: TFIBFloatField;
    quClassTree: TpFIBDataSet;
    imState: TImageList;
    quCardsSel: TpFIBDataSet;
    quCardsSelID: TFIBIntegerField;
    quCardsSelPARENT: TFIBIntegerField;
    quCardsSelNAME: TFIBStringField;
    quCardsSelTTYPE: TFIBIntegerField;
    quCardsSelIMESSURE: TFIBIntegerField;
    quCardsSelINDS: TFIBIntegerField;
    quCardsSelMINREST: TFIBFloatField;
    quCardsSelLASTPRICEIN: TFIBFloatField;
    quCardsSelLASTPRICEOUT: TFIBFloatField;
    quCardsSelLASTPOST: TFIBIntegerField;
    quCardsSelIACTIVE: TFIBIntegerField;
    quCardsSelNAMESHORT: TFIBStringField;
    quCardsSelNAMENDS: TFIBStringField;
    quCardsSelPROC: TFIBFloatField;
    quCardsSelTCARD: TFIBIntegerField;
    quCardsSelCATEGORY: TFIBIntegerField;
    quCardsSelKOEF: TFIBFloatField;
    quCardsSelSPISSTORE: TFIBStringField;
    quCardsSelRemn: TFloatField;
    quCardsSelCOMMENT: TFIBStringField;
    quCardsSelBB: TFIBFloatField;
    quCardsSelGG: TFIBFloatField;
    quCardsSelU1: TFIBFloatField;
    quCardsSelU2: TFIBFloatField;
    quCardsSelEE: TFIBFloatField;
    quCardsSelRCATEGORY: TFIBIntegerField;
    quCardsSelCTO: TFIBIntegerField;
    quCardsSelCODEZAK: TFIBIntegerField;
    quCardsSelALGCLASS: TFIBIntegerField;
    quCardsSelALGMAKER: TFIBIntegerField;
    quCardsSelNAMEM: TFIBStringField;
    quCardsSelVOL: TFIBFloatField;
    dsCardsSel: TDataSource;
    quFind: TpFIBDataSet;
    quFindID: TFIBIntegerField;
    quFindPARENT: TFIBIntegerField;
    quFindNAME: TFIBStringField;
    quFindTCARD2: TFIBIntegerField;
    quFindGROUP: TStringField;
    quFindSGROUP: TStringField;
    dsFind: TDataSource;
    quFindCl: TpFIBDataSet;
    quFindClID_PARENT: TFIBIntegerField;
    quFindClNAMECL: TFIBStringField;
    trSel1: TpFIBTransaction;
    quDocInRec: TpFIBDataSet;
    quDocInRecID: TFIBIntegerField;
    quDocInRecDATEDOC: TFIBDateField;
    quDocInRecNUMDOC: TFIBStringField;
    quDocInRecDATESF: TFIBDateField;
    quDocInRecNUMSF: TFIBStringField;
    quDocInRecIDCLI: TFIBIntegerField;
    quDocInRecIDSKL: TFIBIntegerField;
    quDocInRecSUMIN: TFIBFloatField;
    quDocInRecSUMUCH: TFIBFloatField;
    quDocInRecSUMTAR: TFIBFloatField;
    quDocInRecSUMNDS0: TFIBFloatField;
    quDocInRecSUMNDS1: TFIBFloatField;
    quDocInRecSUMNDS2: TFIBFloatField;
    quDocInRecPROCNAC: TFIBFloatField;
    quDocInRecIACTIVE: TFIBIntegerField;
    quDocInRecCOMMENT: TFIBStringField;
    quDocInRecOPRIZN: TFIBSmallIntField;
    quDocInRecCREATETYPE: TFIBSmallIntField;
    msConnection: TADOConnection;
    quSpecInSel: TpFIBDataSet;
    quSpecInSelIDHEAD: TFIBIntegerField;
    quSpecInSelID: TFIBIntegerField;
    quSpecInSelNUM: TFIBIntegerField;
    quSpecInSelIDCARD: TFIBIntegerField;
    quSpecInSelQUANT: TFIBFloatField;
    quSpecInSelPRICEIN: TFIBFloatField;
    quSpecInSelSUMIN: TFIBFloatField;
    quSpecInSelPRICEUCH: TFIBFloatField;
    quSpecInSelSUMUCH: TFIBFloatField;
    quSpecInSelIDNDS: TFIBIntegerField;
    quSpecInSelSUMNDS: TFIBFloatField;
    quSpecInSelIDM: TFIBIntegerField;
    quSpecInSelKM: TFIBFloatField;
    quSpecInSelPRICE0: TFIBFloatField;
    quSpecInSelSUM0: TFIBFloatField;
    quSpecInSelNDSPROC: TFIBFloatField;
    quSpecInSelOPRIZN: TFIBIntegerField;
    quSpecInSelICODECB: TFIBIntegerField;
    quSpecInSelNAMECB: TFIBStringField;
    quSpecInSelIMCB: TFIBSmallIntField;
    quSpecInSelSMCB: TFIBStringField;
    quSpecInSelKBCB: TFIBFloatField;
    quSpecInSelSBARCB: TFIBStringField;
    quSpecInSelQUANTCB: TFIBFloatField;
    quFCardPro: TADOQuery;
    quFCardProiCode: TIntegerField;
    quFCardProKb: TFloatField;
    quFCardProiCodePro: TIntegerField;
    quFCardProiM: TIntegerField;
    quFCardProKm: TFloatField;
    quFCardProNamePro: TStringField;
    quFCardProSM: TStringField;
    procedure quFindCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function GetId(ta:String):Integer;
Function prGetNumFb(iT,iAdd:INteger):String;
procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
Function prGetNumSCHF(iMH,iYY:INteger):String;
procedure prFind2group(IdCl:Integer;Var S1,S2:String);
procedure prFindCodePro(iCodeCB:INteger; Var iCodePro,iM:Integer; Var rKCB,Km:REal);

var
  dmPro: TdmPro;

implementation

uses Un1, MFB;

{$R *.dfm}

procedure prFindCodePro(iCodeCB:INteger; Var iCodePro,iM:Integer; Var rKCB,Km:REal);
begin
  iCodePro:=0;
  rKCB:=1;
  iM:=0;
  Km:=1;
  with dmPro do
  begin
    quFCardPro.Active:=False;
    quFCardPro.Parameters.ParamByName('ICODE').Value:=iCodeCB;
    quFCardPro.Active:=True;
    if quFCardPro.RecordCount>0 then
    begin
      quFCardPro.First;
      iCodePro:=quFCardProiCodePro.AsInteger;
      rKCB:=quFCardProKb.AsFloat;
      iM:=quFCardProiM.AsInteger;
      Km:=quFCardProKm.AsFloat;
    end;

    quFCardPro.Active:=False;
  end;
end;


procedure prFind2group(IdCl:Integer;Var S1,S2:String);
Var Id:INteger;
begin
  with dmPro do
  begin
    S1:=''; S2:=''; Id:=IdCl;
    while iD<>0 do
    begin
      quFindCl.Active:=False;
      quFindCl.ParamByName('IDCL').AsInteger:=Id;
      quFindCl.Active:=True;
      S2:=S1;
      S1:=Copy(quFindClNAMECL.AsString,1,50);
      Id:=quFindClID_PARENT.AsInteger;
      quFindCl.Active:=False;
    end;
  end;
end;

Function prGetNumSCHF(iMH,iYY:INteger):String;
begin
end;


Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmPro do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all
    if ta='MESS' then iType:=100; //������� ���������
    if ta='Class' then iType:=101; //������������� ������� � �����
    if ta='MH' then iType:=102; //����� �������� � ������������
    if ta='GD' then iType:=103; //�������
    if ta='Cli' then iType:=104; //�����������
    if ta='DocIn' then iType:=105; //��������� ��������� ����������
    if ta='SpecIn' then iType:=106; //������������ ��������� ����������
    if ta='TCards' then iType:=107; //��������������� �����
    if ta='PartIn' then iType:=108; //��������� ������
    if ta='InvH' then iType:=109; //��������� ��������������
    if ta='InvS' then iType:=110; //������������ ��������������
    if ta='Pars' then iType:=111; //��������� ������
    if ta='DocOut' then iType:=112; //��������� ��������� ����������
    if ta='SpecOut' then iType:=113; //������������ ��������� ����������
    if ta='DocOutB' then iType:=114; //��������� ��������� ���������� ����
    if ta='SpecOutB' then iType:=115; //������������ ��������� ���������� ����
    if ta='SpecOutC' then iType:=116; //������������ ��������� ���������� ��������
    if ta='HeadCompl' then iType:=117; //��������� ������������
    if ta='DocAct' then iType:=118; //��������� ����� �����������
    if ta='DocVn' then iType:=119; //��������� ����������
    if ta='SpecVn' then iType:=120; //��������� ����������
    if ta='DocR' then iType:=121; //��������� ���������� �� �������

    prGetId.ParamByName('ITYPE').Value:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;

Function prGetNumFb(iT,iAdd:INteger):String;
var iNum:Integer;
    sPre:String;
begin
  Result:='';
  with dmPro do
  begin
    taNums.Active:=False;
    taNums.ParamByName('ITYPE').AsInteger:=iT;
    taNums.Active:=True;

    taNums.First;

    if taNums.RecordCount>0  then
    begin
      iNum:=taNumsCURNUM.AsInteger+1;
      sPre:=taNumsSPRE.AsString;
      Trim(sPre);
      Result:=sPre+INtToStr(iNum);
      if iAdd=1 then
      begin
        taNums.Edit;
        taNumsCURNUM.AsInteger:=iNum;
        taNums.Post;
      end;
    end;

  end;
end;

procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
begin
  with dmPro do
  begin
    quSetSync.SQL.Clear;
    if sDocT='DocR' then
    begin
      quSetSync.SQL.Add('insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)');
      quSetSync.SQL.Add('values (8,'+its(ISKL)+','+its(Person.Id)+','''+formatdatetime('dd.mm.yyyy hh:nn',now)+''','''+sOp+''','+its(IDH)+')');

//insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)
//values (4,5,6,'23.07.2012 18:00','OPN',10000)

    end;
    quSetSync.ExecQuery;
  end;
end;


procedure TdmPro.quFindCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  prFind2group(quFindPARENT.AsInteger,S1,S2);
  quFindGROUP.AsString:=S1;
  quFindSGROUP.AsString:=S2;
end;

end.
