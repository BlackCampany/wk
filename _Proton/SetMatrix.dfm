object fmSetMatrix: TfmSetMatrix
  Left = 302
  Top = 370
  Width = 780
  Height = 282
  Caption = #1052#1072#1090#1088#1080#1094#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GrMatrix: TcxGrid
    Left = 0
    Top = 0
    Width = 769
    Height = 193
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewMatrix: TcxGridDBTableView
      DragMode = dmAutomatic
      NavigatorButtons.ConfirmDelete = False
      OnCellDblClick = ViewMatrixCellDblClick
      DataController.DataModeController.GridModeBufferCount = 500
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = dmP.dsquSetMatrix
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnHiding = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object ViewMatrixID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewMatrixName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmMC.cxStyle5
        Width = 208
      end
      object ViewMatrixBarCode: TcxGridDBColumn
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        DataBinding.FieldName = 'BarCode'
        Width = 87
      end
      object ViewMatrixShRealize: TcxGridDBColumn
        Caption = #1064#1050' '#1090#1080#1087
        DataBinding.FieldName = 'ShRealize'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1060#1086#1088#1084#1080#1088#1091#1077#1084#1099#1081
            Value = 1
          end
          item
            Description = #1050#1086#1088#1086#1090#1082#1080#1081
            Value = 2
          end>
        Width = 68
      end
      object ViewMatrixTovarType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'TovarType'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1064#1090#1091#1095#1085#1099#1081
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1042#1077#1089#1086#1074#1086#1081
            Value = 1
          end>
        Width = 65
      end
      object ViewMatrixReserv1: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Reserv1'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewMatrixRemn: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
        DataBinding.FieldName = 'Qremn'
        Visible = False
        Options.Editing = False
        Styles.Content = dmMC.cxStyle23
        Styles.Header = dmMC.cxStyle8
        Width = 121
      end
      object ViewMatrixFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Styles.Content = dmMC.cxStyle5
        Width = 116
      end
      object ViewMatrixEdIzm: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1064#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1050#1075'.'
            Value = 2
          end>
      end
      object ViewMatrixNDS: TcxGridDBColumn
        Caption = #1053#1044#1057
        DataBinding.FieldName = 'NDS'
      end
      object ViewMatrixOKDP: TcxGridDBColumn
        Caption = #1054#1050#1044#1055
        DataBinding.FieldName = 'OKDP'
      end
      object ViewMatrixWeght: TcxGridDBColumn
        Caption = #1042#1077#1089
        DataBinding.FieldName = 'Weght'
      end
      object ViewMatrixSrokReal: TcxGridDBColumn
        Caption = #1057#1088#1086#1082' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'SrokReal'
      end
      object ViewMatrixTareWeight: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1090#1072#1088#1099
        DataBinding.FieldName = 'TareWeight'
      end
      object ViewMatrixKritOst: TcxGridDBColumn
        Caption = #1050#1088#1080#1090#1080#1095#1077#1089#1082#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'KritOst'
      end
      object ViewMatrixBHTStatus: TcxGridDBColumn
        Caption = #1058#1077#1085#1076#1077#1088#1085#1099#1081' '#1090#1086#1074#1072#1088
        DataBinding.FieldName = 'BHTStatus'
      end
      object ViewMatrixUKMAction: TcxGridDBColumn
        Caption = #1050#1072#1089#1089#1072
        DataBinding.FieldName = 'UKMAction'
      end
      object ViewMatrixDataChange: TcxGridDBColumn
        Caption = #1048#1079#1084#1077#1085#1077#1085
        DataBinding.FieldName = 'DataChange'
      end
      object ViewMatrixNSP: TcxGridDBColumn
        Caption = #1053#1057#1055
        DataBinding.FieldName = 'NSP'
      end
      object ViewMatrixWasteRate: TcxGridDBColumn
        Caption = #1054#1090#1093#1086#1076#1099
        DataBinding.FieldName = 'WasteRate'
      end
      object ViewMatrixCena: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Cena'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewMatrixStatus: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'Status'
      end
      object ViewMatrixPrsision: TcxGridDBColumn
        Caption = #1058#1086#1095#1085#1086#1089#1090#1100
        DataBinding.FieldName = 'Prsision'
      end
      object ViewMatrixMargGroup: TcxGridDBColumn
        Caption = #1052#1072#1088#1075#1080#1085#1072#1083#1100#1085#1072#1103' '#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'MargGroup'
      end
      object ViewMatrixNameCountry: TcxGridDBColumn
        Caption = #1057#1090#1088#1072#1085#1072
        DataBinding.FieldName = 'NameCountry'
        Width = 171
      end
      object ViewMatrixNAMEBRAND: TcxGridDBColumn
        Caption = #1041#1088#1101#1085#1076
        DataBinding.FieldName = 'NAMEBRAND'
        Width = 129
      end
      object ViewMatrixV02: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'V02'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'SID'
          end>
        Properties.ListSource = dmMC.dsquCateg
      end
      object ViewMatrixReserv2: TcxGridDBColumn
        Caption = #1053#1072#1094#1077#1085#1082#1072' '#1085#1072' '#1090#1086#1074#1072#1088
        DataBinding.FieldName = 'Reserv2'
        Styles.Content = dmMC.cxStyle13
      end
      object ViewMatrixFixPrice: TcxGridDBColumn
        Caption = #1060#1080#1082#1089#1080#1088#1086#1074#1072#1085#1085#1072#1103' '#1094#1077#1085#1072
        DataBinding.FieldName = 'FixPrice'
      end
      object ViewMatrixV04: TcxGridDBColumn
        Caption = #1058#1086#1087
        DataBinding.FieldName = 'V04'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = ' '
            ImageIndex = 0
            Value = 0
          end
          item
            Description = 'T'
            Value = 1
          end>
        Options.Editing = False
      end
      object ViewMatrixV05: TcxGridDBColumn
        Caption = #1053#1086#1074#1080#1085#1082#1072
        DataBinding.FieldName = 'V05'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 0
          end
          item
            Description = 'N'
            Value = 1
          end>
        Options.Editing = False
      end
      object ViewMatrixKol: TcxGridDBColumn
        Caption = #1052#1080#1085'. '#1082#1086#1083'-'#1074#1086' '#1076#1083#1103' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'Kol'
      end
      object ViewMatrixV06: TcxGridDBColumn
        Caption = #1052#1080#1085'.'#1079#1072#1087#1072#1089' '#1074' '#1076#1085#1103#1093
        DataBinding.FieldName = 'V06'
      end
      object ViewMatrixV07: TcxGridDBColumn
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1079#1072#1087#1072#1089' '#1074' '#1076#1085#1103#1093
        DataBinding.FieldName = 'V07'
      end
      object ViewMatrixV08: TcxGridDBColumn
        Caption = #1050#1085#1086#1087#1082#1072' '#1074' '#1074#1077#1089#1072#1093
        DataBinding.FieldName = 'V08'
      end
      object ViewMatrixVol: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084
        DataBinding.FieldName = 'Vol'
        Options.Editing = False
      end
      object ViewMatrixV11: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'V11'
      end
      object ViewMatrixNAMEM: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'NAMEM'
        Width = 100
      end
      object ViewMatrixTypeV: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1074#1086#1079#1074#1088#1072#1090#1072
        DataBinding.FieldName = 'TYPEVOZ'
        Width = 109
      end
    end
    object LevMatrix: TcxGridLevel
      GridView = ViewMatrix
    end
  end
  object cxButton2: TcxButton
    Left = 408
    Top = 200
    Width = 137
    Height = 33
    Hint = #1047#1072#1082#1088#1099#1090#1100
    Caption = #1042#1099#1093#1086#1076
    ModalResult = 2
    TabOrder = 1
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton1: TcxButton
    Left = 208
    Top = 200
    Width = 137
    Height = 33
    Hint = #1042#1099#1073#1088#1072#1090#1100
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
  end
end
