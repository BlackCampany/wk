program MCManager;

uses
  Forms,
  Manger in 'Manger.pas' {fmManager},
  MT in 'MT.pas' {dmMT: TDataModule},
  Un1 in 'Un1.pas',
  u2fdk in 'U2FDK.PAS',
  PXDB in 'PXDB.pas' {dmPx: TDataModule},
  MFB in 'MFB.pas' {dmFB: TDataModule},
  uTermElisey in 'uTermElisey.pas' {Service1: TService};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmManager, fmManager);
  Application.CreateForm(TdmMT, dmMT);
  Application.CreateForm(TdmPx, dmPx);
  Application.CreateForm(TdmFB, dmFB);
  Application.CreateForm(TService1, Service1);
  Application.Run;
end.
