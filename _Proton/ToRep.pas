unit ToRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ActnList,
  XPStyleActnCtrls, ActnMan, FR_DSet, FR_DBSet, FR_Class, pvtables,
  sqldataset, dxmdaset;

type
  TfmTORep = class(TForm)
    FormPlacement1: TFormPlacement;
    ViewTO: TcxGridDBTableView;
    LevelTO: TcxGridLevel;
    GridTO: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ViewTOOtdel: TcxGridDBColumn;
    ViewTOName: TcxGridDBColumn;
    ViewTOxDate: TcxGridDBColumn;
    ViewTOOldOstatokTovar: TcxGridDBColumn;
    ViewTOPrihodTovar: TcxGridDBColumn;
    ViewTOSelfPrihodTovar: TcxGridDBColumn;
    ViewTOPrihodTovarSumma: TcxGridDBColumn;
    ViewTORashodTovar: TcxGridDBColumn;
    ViewTOSelfRashodTovar: TcxGridDBColumn;
    ViewTORealTovar: TcxGridDBColumn;
    ViewTOSkidkaTovar: TcxGridDBColumn;
    ViewTOOutTovar: TcxGridDBColumn;
    ViewTORashodTovarSumma: TcxGridDBColumn;
    ViewTOPereoTovarSumma: TcxGridDBColumn;
    ViewTONewOstatokTovar: TcxGridDBColumn;
    ViewTOOldOstatokTara: TcxGridDBColumn;
    ViewTOPrihodTara: TcxGridDBColumn;
    ViewTOSelfPrihodTara: TcxGridDBColumn;
    ViewTOPrihodTaraSumma: TcxGridDBColumn;
    ViewTORashodTara: TcxGridDBColumn;
    ViewTOSelfRashodTara: TcxGridDBColumn;
    ViewTOOutTara: TcxGridDBColumn;
    ViewTORashodTaraSumma: TcxGridDBColumn;
    ViewTONewOstatokTara: TcxGridDBColumn;
    ViewTOOprihod: TcxGridDBColumn;
    amTO: TActionManager;
    acGenTO: TAction;
    SpeedItem4: TSpeedItem;
    acPrint: TAction;
    SpeedItem5: TSpeedItem;
    RepTO: TfrReport;
    frtaTOIn: TfrDBDataSet;
    frtaTOOut: TfrDBDataSet;
    quTTnInR: TPvQuery;
    quTTnInRDepart: TSmallintField;
    quTTnInRDateInvoice: TDateField;
    quTTnInRNumber: TStringField;
    quTTnInRSCFNumber: TStringField;
    quTTnInRIndexPost: TSmallintField;
    quTTnInRCodePost: TSmallintField;
    quTTnInRSummaTovarPost: TFloatField;
    quTTnInRReturnTovar: TFloatField;
    quTTnInRSummaTovar: TFloatField;
    quTTnInRNacenka: TFloatField;
    quTTnInRReturnTara: TFloatField;
    quTTnInRSummaTara: TFloatField;
    quTTnInRAcStatus: TWordField;
    quTTnInRChekBuh: TWordField;
    quTTnInRSCFDate: TDateField;
    quTTnInROrderNumber: TIntegerField;
    quTTnInRID: TIntegerField;
    quTTnInRNamePost: TStringField;
    quTTnInRNameInd: TStringField;
    quTTnInRNameCli: TStringField;
    quTTnOutR: TPvQuery;
    quTTnOutRDepart: TSmallintField;
    quTTnOutRDateInvoice: TDateField;
    quTTnOutRNumber: TStringField;
    quTTnOutRSCFNumber: TStringField;
    quTTnOutRIndexPoluch: TSmallintField;
    quTTnOutRCodePoluch: TSmallintField;
    quTTnOutRSummaTovar: TFloatField;
    quTTnOutRSummaTovarSpis: TFloatField;
    quTTnOutRNacenkaTovar: TFloatField;
    quTTnOutRSummaTara: TFloatField;
    quTTnOutRSummaTaraSpis: TFloatField;
    quTTnOutRNacenkaTara: TFloatField;
    quTTnOutRAcStatus: TWordField;
    quTTnOutRChekBuh: TWordField;
    quTTnOutRSCFDate: TDateField;
    quTTnOutRID: TIntegerField;
    quTTnOutRNamePost: TStringField;
    quTTnOutRNameInd: TStringField;
    quTTnOutRNameCli: TStringField;
    taIn: TdxMemData;
    taIniType: TSmallintField;
    taInNameGr: TStringField;
    taInNameGr1: TStringField;
    taInNameCli: TStringField;
    taInNumDoc: TStringField;
    taInSumIn: TFloatField;
    taInSumOut: TFloatField;
    taOut: TdxMemData;
    taOutiType: TSmallintField;
    taOutNameGr: TStringField;
    taOutNameGr1: TStringField;
    taOutNameCli: TStringField;
    taOutNumDoc: TStringField;
    taOutSumIn: TFloatField;
    taOutSumOut: TFloatField;
    quTTnSelfOut: TPvQuery;
    quTTnSelfOutDepart: TSmallintField;
    quTTnSelfOutDateInvoice: TDateField;
    quTTnSelfOutNumber: TStringField;
    quTTnSelfOutFlowDepart: TSmallintField;
    quTTnSelfOutSummaTovarSpis: TFloatField;
    quTTnSelfOutSummaTovarMove: TFloatField;
    quTTnSelfOutSummaTovarNew: TFloatField;
    quTTnSelfOutSummaTara: TFloatField;
    quTTnSelfOutOprihod: TBooleanField;
    quTTnSelfOutAcStatusP: TWordField;
    quTTnSelfOutChekBuhP: TWordField;
    quTTnSelfOutAcStatusR: TWordField;
    quTTnSelfOutChekBuhR: TWordField;
    quTTnSelfOutProvodTypeP: TWordField;
    quTTnSelfOutProvodTypeR: TWordField;
    quTTnSelfOutDepFrom: TStringField;
    quTTnSelfOutDepTo: TStringField;
    quTTnSelfIn: TPvQuery;
    quTTnSelfInDepart: TSmallintField;
    quTTnSelfInDateInvoice: TDateField;
    quTTnSelfInNumber: TStringField;
    quTTnSelfInFlowDepart: TSmallintField;
    quTTnSelfInSummaTovarSpis: TFloatField;
    quTTnSelfInSummaTovarMove: TFloatField;
    quTTnSelfInSummaTovarNew: TFloatField;
    quTTnSelfInSummaTara: TFloatField;
    quTTnSelfInOprihod: TBooleanField;
    quTTnSelfInAcStatusP: TWordField;
    quTTnSelfInChekBuhP: TWordField;
    quTTnSelfInAcStatusR: TWordField;
    quTTnSelfInChekBuhR: TWordField;
    quTTnSelfInProvodTypeP: TWordField;
    quTTnSelfInProvodTypeR: TWordField;
    quTTnSelfInDepFrom: TStringField;
    quTTnSelfInDepTo: TStringField;
    taInSumInT: TFloatField;
    taOutSumOutT: TFloatField;
    ViewTOSenderSumma: TcxGridDBColumn;
    ViewTOSenderSkidka: TcxGridDBColumn;
    ViewTOV03: TcxGridDBColumn;
    taSvod: TdxMemData;
    taSvodiDep: TIntegerField;
    taSvodNameDep: TStringField;
    taSvodrIn: TFloatField;
    taSvodrOut: TFloatField;
    frtaSvod: TfrDBDataSet;
    taSvodrPrih: TFloatField;
    taSvodrRet: TFloatField;
    taSvodrReal: TFloatField;
    taSvodrInv: TFloatField;
    taSvodrPereoc: TFloatField;
    quTTnInROutNDSTovar: TFloatField;
    quTTnInRNDS10: TFloatField;
    quTTnInRNDS20: TFloatField;
    quTTnInROutNDS10: TFloatField;
    quTTnInROutNDS20: TFloatField;
    taInOutNDSTovar: TFloatField;
    taInOutNDS10: TFloatField;
    taInOutNDS20: TFloatField;
    taInNds10: TFloatField;
    taInNds20: TFloatField;
    taOutSumOutOutNDS: TFloatField;
    taOutSumNDS10: TFloatField;
    taOutSumNDS20: TFloatField;
    quTTnOutRNDS10: TFloatField;
    quTTnOutRNDS20: TFloatField;
    ViewTOxRezerv: TcxGridDBColumn;
    quTTnOutR3: TPvQuery;
    quTTnOutR3Depart: TSmallintField;
    quTTnOutR3DateInvoice: TDateField;
    quTTnOutR3Number: TStringField;
    quTTnOutR3CodeGroup: TSmallintField;
    quTTnOutR3CodePodgr: TSmallintField;
    quTTnOutR3CodeTovar: TIntegerField;
    quTTnOutR3CodeEdIzm: TSmallintField;
    quTTnOutR3TovarType: TSmallintField;
    quTTnOutR3BarCode: TStringField;
    quTTnOutR3OKDP: TFloatField;
    quTTnOutR3NDSProc: TFloatField;
    quTTnOutR3NDSSum: TFloatField;
    quTTnOutR3OutNDSSum: TFloatField;
    quTTnOutR3Akciz: TFloatField;
    quTTnOutR3KolMest: TIntegerField;
    quTTnOutR3KolEdMest: TFloatField;
    quTTnOutR3KolWithMest: TFloatField;
    quTTnOutR3Kol: TFloatField;
    quTTnOutR3CenaPost: TFloatField;
    quTTnOutR3CenaTovar: TFloatField;
    quTTnOutR3CenaTovarSpis: TFloatField;
    quTTnOutR3SumCenaTovar: TFloatField;
    quTTnOutR3SumCenaTovarSpis: TFloatField;
    quTTnOutR3CodeTara: TIntegerField;
    quTTnOutR3VesTara: TFloatField;
    quTTnOutR3CenaTara: TFloatField;
    quTTnOutR3CenaTaraSpis: TFloatField;
    quTTnOutR3SumVesTara: TFloatField;
    quTTnOutR3SumCenaTara: TFloatField;
    quTTnOutR3SumCenaTaraSpis: TFloatField;
    quTTnOutR3ChekBuh: TBooleanField;
    quTTnOutR3Depart_1: TSmallintField;
    quTTnOutR3DateInvoice_1: TDateField;
    quTTnOutR3Number_1: TStringField;
    quTTnOutR3SCFNumber: TStringField;
    quTTnOutR3IndexPoluch: TSmallintField;
    quTTnOutR3CodePoluch: TSmallintField;
    quTTnOutR3SummaTovar: TFloatField;
    quTTnOutR3NDS10: TFloatField;
    quTTnOutR3OutNDS10: TFloatField;
    quTTnOutR3NDS20: TFloatField;
    quTTnOutR3OutNDS20: TFloatField;
    quTTnOutR3LgtNDS: TFloatField;
    quTTnOutR3OutLgtNDS: TFloatField;
    quTTnOutR3NDSTovar: TFloatField;
    quTTnOutR3OutNDSTovar: TFloatField;
    quTTnOutR3NotNDSTovar: TFloatField;
    quTTnOutR3OutNDSSNTovar: TFloatField;
    quTTnOutR3SummaTovarSpis: TFloatField;
    quTTnOutR3NacenkaTovar: TFloatField;
    quTTnOutR3SummaTara: TFloatField;
    quTTnOutR3SummaTaraSpis: TFloatField;
    quTTnOutR3NacenkaTara: TFloatField;
    quTTnOutR3AcStatus: TWordField;
    quTTnOutR3ChekBuh_1: TWordField;
    quTTnOutR3NAvto: TStringField;
    quTTnOutR3NPList: TStringField;
    quTTnOutR3FIOVod: TStringField;
    quTTnOutR3PrnDate: TDateField;
    quTTnOutR3PrnNumber: TStringField;
    quTTnOutR3PrnKol: TFloatField;
    quTTnOutR3PrnAkt: TStringField;
    quTTnOutR3ProvodType: TWordField;
    quTTnOutR3NotNDS10: TFloatField;
    quTTnOutR3NotNDS20: TFloatField;
    quTTnOutR3WithNDS10: TFloatField;
    quTTnOutR3WithNDS20: TFloatField;
    quTTnOutR3Crock: TFloatField;
    quTTnOutR3Discount: TFloatField;
    quTTnOutR3NDS010: TFloatField;
    quTTnOutR3NDS020: TFloatField;
    quTTnOutR3NDS_10: TFloatField;
    quTTnOutR3NDS_20: TFloatField;
    quTTnOutR3NSP_10: TFloatField;
    quTTnOutR3NSP_20: TFloatField;
    quTTnOutR3SCFDate: TDateField;
    quTTnOutR3GoodsNSP0: TFloatField;
    quTTnOutR3GoodsCostNSP: TFloatField;
    quTTnOutR3OrderNumber: TIntegerField;
    quTTnOutR3ID: TIntegerField;
    quTTnOutR3LinkInvoice: TWordField;
    quTTnOutR3StartTransfer: TWordField;
    quTTnOutR3EndTransfer: TWordField;
    quTTnOutR3REZERV: TStringField;
    acCorrPartOut: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    quTTnOutROutNDSTovar: TFloatField;
    quTTnOutRNotNDSTovar: TFloatField;
    quTTnOutRSpis: TPvQuery;
    quTTnOutRSpisDepart: TSmallintField;
    quTTnOutRSpisDateInvoice: TDateField;
    quTTnOutRSpisNumber: TStringField;
    quTTnOutRSpisCrock: TFloatField;
    teSSR: TdxMemData;
    teSSRiDate: TIntegerField;
    teSSRdDate: TDateField;
    teSSRiDep: TIntegerField;
    teSSRNameDep: TStringField;
    teSSRRSUM: TFloatField;
    teSSRRSUMIN: TFloatField;
    teSSRRSUMINPP: TFloatField;
    LevelSSR: TcxGridLevel;
    ViewSSR: TcxGridDBTableView;
    dsteSSR: TDataSource;
    acSSR: TAction;
    ViewSSRiDate: TcxGridDBColumn;
    ViewSSRdDate: TcxGridDBColumn;
    ViewSSRiDep: TcxGridDBColumn;
    ViewSSRNameDep: TcxGridDBColumn;
    ViewSSRRSUM: TcxGridDBColumn;
    ViewSSRRSUMIN: TcxGridDBColumn;
    ViewSSRRSUMINPP: TcxGridDBColumn;
    teSSRRSUMDIF: TFloatField;
    ViewSSRRSUMDIF: TcxGridDBColumn;
    acGenSv: TAction;
    taSvodrRealBn: TFloatField;
    taSvodrOutVn: TFloatField;
    N2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGenTOExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure quTTnInRCalcFields(DataSet: TDataSet);
    procedure quTTnOutRCalcFields(DataSet: TDataSet);
    procedure acCorrPartOutExecute(Sender: TObject);
    procedure acSSRExecute(Sender: TObject);
    procedure acGenSvExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTORep: TfmTORep;

implementation

uses Un1, MDB, Period1, Status, MT, CorrTO, u2fdk;

{$R *.dfm}

procedure TfmTORep.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridTO.Align:=AlClient;
end;

procedure TfmTORep.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmTORep.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmTORep.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      fmTORep.ViewTO.BeginUpdate;

      quReports.Active:=False;
      quReports.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quReports.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quReports.Active:=True;

      fmTORep.ViewTO.EndUpdate;
      fmTORep.Caption:='�������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmTORep.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(ViewTO);
end;

procedure TfmTORep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  fmTORep.Release;
end;

procedure TfmTORep.acGenTOExecute(Sender: TObject);
Var DateB,DateE:INteger;
{    rSumB,rSumIn,rSumOut,rSumReal,rSumDisc,rSumAC,rSumRealBn,rSumDiscBn,rSumSelfIn,rSumSelfOut,rSumInv:Real;
    rSumBT,rSumInT,rSumOutT:Real;
    iD,iTInv,iCurD:INteger;
    IdMain:INteger;}
begin
// ������������ ��
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    if CommonSet.DateBeg<=prOpenDate then
    begin
      ShowMessage('������ ������.');
      exit;
    end;

    with dmMC do
    begin
      //�������
      fmSt.Memo1.Clear;
      fmSt.Show;
      with fmSt do
      begin
        cxButton1.Enabled:=False;

        DateB:=Trunc(CommonSet.DateBeg);
        DateE:=Trunc(CommonSet.DateEnd);

        prRecalcTO(DateB,DateE,fmSt.Memo1,0,0); //�������� �������� �������

        cxButton1.Enabled:=True;
        Memo1.Lines.Add('��.');
      end;

      fmTORep.ViewTO.BeginUpdate;

      quReports.Active:=False;
      quReports.Active:=True;

      fmTORep.ViewTO.EndUpdate;
      fmTORep.Caption:='�������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmTORep.acPrintExecute(Sender: TObject);
Var
    iC1,iC2:INteger;
    iSS:Integer;
    rSumOutNDS,rNDS10,rNDS20:Real;
    rSumOutNDSOut,rNDS10Out,rNDS20Out:Real;
    rSaleSS,rSaleSSOutNDS,rSaleSSNDS10,rSaleSSNDS20, rSumPost:Real;
    NameCli,Sinn:String;
    SumOut,SumOutT:Real;
    bBonus,bApp:Boolean;
    rSumOutDoc,rSumOutPart:Real;
    iCliCB:INteger;
begin
  //������ ��
  with dmMC do
  with dmMT do
  begin
    if (quReports.RecordCount>0)and(quReportsV03.AsInteger=0) then
    begin

      if fShopB=0 then bBonus:=False else bBonus:=True; // bBonus=True -  ��� ����� �������� ��������

      CloseTe(taIn);
      CloseTe(taOut);

      iSS:=0;
      if ptDep.Active=False then ptDep.Active:=True;
      if ptDep.Locate('ID',quReportsOtdel.AsInteger,[]) then iSS:=ptDepStatus5.AsInteger;

      //������ ������

      iC1:=0;

      rSumOutNDS:=0;
      rNDS10:=0;
      rNDS20:=0;
      rSumPost:=0;

      with dmMt do   //���� ��������� ������ ����� ��� ������� ������� ��� ����������� ���������� ���
      begin
        if ptPartO.Active=False then ptPartO.Active:=True;
        ptPartO.Refresh;
        ptPartO.IndexFieldNames:='ITYPE;IDDOC;ARTICUL';

        if ptPartIN.Active=False then ptPartIN.Active:=True;
        ptPartIN.Refresh;
        ptPartIN.IndexFieldNames:='ID';

      end;


      quTTnInR.Active:=False;
      quTTnInR.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
      quTTnInR.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
      quTTnInR.Active:=True;
      quTTnInR.First;
      while not quTTnInR.Eof do
      begin
        if bBonus then   //����� �� ����������� ��� �������
        begin
          if (quTTnInRSummaTovarPost.AsFloat>0)or(quTTnInRSummaTara.AsFloat>0) then bApp:=True
          else bApp:=False;  // ���� BBonus �� �������� ��������� ���������� � �� �� �����
        end else bApp:=True;

        if bApp then
        begin
          taIn.Append;
          taIniType.AsInteger:=1;
          taInNameGr.AsString:='������ �� �����������';
          taInNameGr1.AsString:='���������';
          taInNameCli.AsString:=quTTnInRNameCli.AsString;
          taInNumDoc.AsString:=quTTnInRNumber.AsString;
          taInSumIn.AsFloat:=rv(quTTnInRSummaTovarPost.AsFloat);
          taInSumOut.AsFloat:=quTTnInRSummaTovar.AsFloat;
          taInSumInT.AsFloat:=quTTnInRSummaTara.AsFloat;
          taInOutNDSTovar.AsFloat:=quTTnInROutNDSTovar.AsFloat;
          taInOutNDS10.AsFloat:=quTTnInROutNDS10.AsFloat;
          taInOutNDS20.AsFloat:=quTTnInROutNDS20.AsFloat;
          taInNds10.AsFloat:=quTTnInRNDS10.AsFloat;
          taInNds20.AsFloat:=quTTnInRNDS20.AsFloat;
          taIn.Post;

          rSumOutNDS:=rSumOutNDS+quTTnInROutNDSTovar.AsFloat;
          rNDS10:=rNDS10+quTTnInRNDS10.AsFloat;
          rNDS20:=rNDS20+quTTnInRNDS20.AsFloat;
          rSumPost:=rSumPost+rv(quTTnInRSummaTovarPost.AsFloat);
        end;
        inc(iC1);
        quTTnInR.Next;
      end;
      quTTnInR.Active:=False;

      if iSS=0 then
      begin
        quTTnSelfIn.Active:=False;
        quTTnSelfIn.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quTTnSelfIn.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
        quTTnSelfIn.Active:=True;
        quTTnSelfIn.First;
        while not quTTnSelfIn.Eof do
        begin
          taIn.Append;
          taIniType.AsInteger:=2;
          taInNameGr.AsString:='���������� ������';
          taInNameGr1.AsString:='�����������';
          taInNameCli.AsString:=quTTnSelfInDepFrom.AsString;
          taInNumDoc.AsString:=quTTnSelfInNumber.AsString;
          if Trunc(quReportsxDate.AsDateTime)<40575 then
          begin
            taInSumIn.AsFloat:=rv(quTTnSelfInSummaTovarMove.AsFloat);
            taInSumOut.AsFloat:=rv(quTTnSelfInSummaTovarSpis.AsFloat);
          end else //�������� � 1-��� �������
          begin
            taInSumIn.AsFloat:=rv(quTTnSelfInSummaTovarSpis.AsFloat);
            taInSumOut.AsFloat:=rv(quTTnSelfInSummaTovarMove.AsFloat);
          end;
          taInOutNDSTovar.AsFloat:=0;
          taInOutNDS10.AsFloat:=0;
          taInOutNDS20.AsFloat:=0;
          taInNds10.AsFloat:=0;
          taInNds20.AsFloat:=0;
          taIn.Post;

          inc(iC1);
          quTTnSelfIn.Next;
        end;
        quTTnSelfIn.Active:=False;
      end;

      if iSS>0 then
      begin
        quTTnSelfIn.Active:=False;
        quTTnSelfIn.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quTTnSelfIn.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
        quTTnSelfIn.Active:=True;
        quTTnSelfIn.First;
        while not quTTnSelfIn.Eof do
        begin
          taIn.Append;
          taIniType.AsInteger:=2;
          taInNameGr.AsString:='���������� ������';
          taInNameGr1.AsString:='�����������';
          taInNameCli.AsString:=quTTnSelfInDepFrom.AsString;
          taInNumDoc.AsString:=quTTnSelfInNumber.AsString;
          taInSumIn.AsFloat:=rv(quTTnSelfInSummaTovarSpis.AsFloat);
          taInSumOut.AsFloat:=rv(quTTnSelfInSummaTovarMove.AsFloat);
          taInOutNDSTovar.AsFloat:=0;
          taInOutNDS10.AsFloat:=0;
          taInOutNDS20.AsFloat:=0;
          taInNds10.AsFloat:=0;
          taInNds20.AsFloat:=0;
          taIn.Post;

          inc(iC1);
          quTTnSelfIn.Next;
        end;
        quTTnSelfIn.Active:=False;
      end;

      if ptInventryHead.Active=False then ptInventryHead.Active:=True;
      ptInventryHead.CancelRange;
      ptInventryHead.SetRange([quReportsOtdel.AsInteger,quReportsxDate.AsDateTime],[quReportsOtdel.AsInteger,quReportsxDate.AsDateTime]);
      ptInventryHead.First;
      while not ptInventryHead.Eof do
      begin
        if OemToAnsiConvert(ptInventryHeadStatus.AsString)<>'�' then
        begin
          taIn.Append;
          taIniType.AsInteger:=2;
          taInNameGr.AsString:='���������� ������';
          taInNameGr1.AsString:='��������������';
          taInNameCli.AsString:='��������������';
          taInNumDoc.AsString:=OemToAnsiConvert(ptInventryHeadNumber.AsString);
          taInSumIn.AsFloat:=rv(ptInventryHeadSumActual.AsFloat-ptInventryHeadSumRecord.AsFloat);
          taInSumOut.AsFloat:=0;
          taInSumInT.AsFloat:=0;
          taInOutNDSTovar.AsFloat:=rv(ptInventryHeadSumActual.AsFloat-ptInventryHeadSumRecord.AsFloat);
          taInOutNDS10.AsFloat:=0;
          taInOutNDS20.AsFloat:=0;
          taInNds10.AsFloat:=0;
          taInNds20.AsFloat:=0;
          taIn.Post;
        end;
        ptInventryHead.Next;
      end;

      if taIn.RecordCount=0 then
      begin
        taIn.Append;
        taIniType.AsInteger:=1;
        taInNameGr.AsString:='������ �� �����������';
        taInNameGr1.AsString:='���������';
        taInNameCli.AsString:='';
        taInNumDoc.AsString:='';
        taInSumIn.AsFloat:=0;
        taInSumOut.AsFloat:=0;
        taIn.Post;
      end;

{      taIn.Append;
      taIniType.AsInteger:=2;
      taInNameGr.AsString:='���������� ������';
      taInNameGr1.AsString:='�����������';
      taInNameCli.AsString:='';
      taInNumDoc.AsString:='';
      taInSumIn.AsFloat:=0;
      taInSumOut.AsFloat:=0;
      taIn.Post;
}
      iC2:=0;
      rSumOutNDSOut:=0;
      rNDS10Out:=0;
      rNDS20Out:=0;

      //��� ����������� ������� - ����� ��� ��� �� �������
      quTTnOutR.Active:=False;
      quTTnOutR.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
      quTTnOutR.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
      quTTnOutR.Active:=True;
      quTTnOutR.First;
      while not quTTnOutR.Eof do
      begin
        taOut.Append;
        taOutiType.AsInteger:=1;
        taOutNameGr.AsString:='������ �����������';
        taOutNameGr1.AsString:='����������';
        taOutNameCli.AsString:=quTTnOutRNameCli.AsString;
        taOutNumDoc.AsString:=quTTnOutRNumber.AsString;
        taOutSumIn.AsFloat:=rv(quTTnOutRSummaTovar.AsFloat);
        taOutSumOut.AsFloat:=quTTnOutRSummaTovarSpis.AsFloat;
        taOutSumOutT.AsFloat:=quTTnOutRSummaTaraSpis.AsFloat;
//        taOutSumOutOutNDS.AsFloat:=quTTnOutRSummaTovar.AsFloat-quTTnOutRNDS10.AsFloat-quTTnOutRNDS20.AsFloat;
        taOutSumOutOutNDS.AsFloat:=quTTnOutROutNDSTovar.AsFloat+quTTnOutRNotNDSTovar.AsFloat;
        taOutSumNDS10.AsFloat:=quTTnOutRNDS10.AsFloat;
        taOutSumNDS20.AsFloat:=quTTnOutRNDS20.AsFloat;
        taOut.Post;

//        rSumOutNDSOut:=rSumOutNDSOut+(quTTnOutRSummaTovar.AsFloat-quTTnOutRNDS10.AsFloat-quTTnOutRNDS20.AsFloat);
        rSumOutNDSOut:=rSumOutNDSOut+quTTnOutROutNDSTovar.AsFloat+quTTnOutRNotNDSTovar.AsFloat;
        rNDS10Out:=rNDS10Out+quTTnOutRNDS10.AsFloat;
        rNDS20Out:=rNDS20Out+quTTnOutRNDS20.AsFloat;

        inc(iC2);
        quTTnOutR.Next;
      end;
      quTTnOutR.Active:=False;



      //��� ����� �������

      if iSS=2 then    //������������ ������� - �� ��������
      begin
        quSOutH3.Active:=False;
        quSOutH3.SQL.Clear;
        quSOutH3.SQL.Add('select * from "TTNOut"');
        quSOutH3.SQL.Add('where DateInvoice='''+ds(Trunc(quReportsxDate.AsDateTime))+'''');
        quSOutH3.SQL.Add('and "Depart"='+its(quReportsOtdel.AsInteger));
        quSOutH3.SQL.Add('and "AcStatus"=3');
        quSOutH3.SQL.Add('and ("ProvodType"=3 or "ProvodType"=4)');
//        quSOutH3.SQL.Add('and "ProvodType"=3');
        quSOutH3.SQL.Add('Order by Depart, DateInvoice, Number');

        quSOutH3.Active:=True;
        quSOutH3.First;
        while not quSOutH3.Eof do
        begin
          rSumOutDoc:=0;
          rSumOutPart:=0;

          SumOut:=0;
          SumOutT:=0;

          //���� ����� �� ������� ���������   - ������ �� ������� ����� ���� CenaTaraSpis � ������������
      {    with dmMt do
          begin
//                'ITYPE;IDDOC;ARTICUL'
             ptPartO.CancelRange;
             ptPartO.SetRange([2,quSOutH3ID.AsInteger],[2,quSOutH3ID.AsInteger]);
             ptPartO.First;
             while not ptPartO.Eof do
             begin
               rSumOutPart:=rSumOutPart+ptPartOSUMIN.AsFloat;
               ptPartO.Next;
             end;
           end;}

           quSOut3.Active:=False;
           quSOut3.SQL.Clear;       //��� �� ����������� ���������

           quSOut3.SQL.Add('select * from "TTNOutLn" ln');
           quSOut3.SQL.Add('left join "TTNOut" hd on');
           quSOut3.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');
           quSOut3.SQL.Add('where ln.DateInvoice='''+ds(Trunc(quReportsxDate.AsDateTime))+'''');
           quSOut3.SQL.Add('and ln."Depart"='+its(quReportsOtdel.AsInteger));
//                quSOut3.SQL.Add('and hd."AcStatus"=3');
//                quSOut3.SQL.Add('and (hd."ProvodType"=3 or hd."ProvodType"=4)');
           quSOut3.SQL.Add('and hd."ID"='+its(quSOutH3ID.AsInteger));

           quSOut3.Active:=True;
           quSOut3.First;
           while not quSOut3.Eof do
           begin            
             rSumOutDoc:=rSumOutDoc+quSOut3SumCenaTovar.AsFloat;
             SumOut:=SumOut+quSOut3SumCenaTovarSpis.AsFloat;
             SumOutT:=SumOutT+quSOut3SumCenaTaraSpis.AsFloat;
             rSumOutPart:=rSumOutPart+rv(quSOut3CenaTaraSpis.AsFloat*quSOut3Kol.AsFloat); //��� ����� ��� ��� �� �������
             quSOut3.Next;
           end;

           quSOut3.Active:=False;

           NameCli:=prFindCliName1(quSOutH3IndexPoluch.AsInteger,quSOutH3CodePoluch.AsInteger,Sinn,iCliCB);

           taOut.Append;
           taOutiType.AsInteger:=1;
           taOutNameGr.AsString:='������ �����������';
           taOutNameGr1.AsString:='����������';
           taOutNameCli.AsString:=NameCli;
           taOutNumDoc.AsString:=quSOutH3Number.AsString;
           taOutSumIn.AsFloat:=rSumOutDoc;
           taOutSumOut.AsFloat:=SumOut;
           taOutSumOutT.AsFloat:=SumOutT;
           taOutSumOutOutNDS.AsFloat:=rSumOutPart;
           taOutSumNDS10.AsFloat:=rSumOutDoc-rSumOutPart; //��� ������ �� � ��� ������, �������� � �������� � 1 � ��� �������� ��� ������.
           taOutSumNDS20.AsFloat:=0;
           taOut.Post;

           quSOutH3.Next;
         end;
         quSOutH3.Active:=True;
       end;


      //��� ������ �������
      {
      quSOut3.Active:=False;
      quSOut3.SQL.Clear;

      quSOut3.SQL.Add('select * from "TTNOutLn" ln');
      quSOut3.SQL.Add('left join "TTNOut" hd on');
      quSOut3.SQL.Add('(hd.Depart=ln.Depart and hd.DateInvoice=Ln.DateInvoice and hd.Number=ln.Number)');
      quSOut3.SQL.Add('where ln.DateInvoice='''+ds(quReportsxDate.AsDateTime)+'''');
      quSOut3.SQL.Add('and ln."Depart"='+its(quReportsOtdel.AsInteger));
      quSOut3.SQL.Add('and hd."AcStatus"=3');
      quSOut3.SQL.Add('and ((hd."ProvodType"=3)or(hd."ProvodType"=4))');

      quSOut3.Active:=True;
      quSOut3.First;

      sNum:='`~`';
      NameCli:='';
      NumDoc:='';
      SumIn:=0;
      SumOut:=0;
      SumOutT:=0;
      SumOutOutNDS:=0;
      SumNDS10:=0;
      SumNDS20:=0;

      while not quSOut3.Eof do
      begin
        if sNum<>quSOut3Number.AsString then
        begin
          if sNum<>'`~`' then //��� �� ������ - ���� ����������
          begin
            taOut.Append;
            taOutiType.AsInteger:=1;
            taOutNameGr.AsString:='������ �����������';
            taOutNameGr1.AsString:='����������';
            taOutNameCli.AsString:=NameCli;
            taOutNumDoc.AsString:=NumDoc;
            taOutSumIn.AsFloat:=SumIn;
            taOutSumOut.AsFloat:=SumOut;
            taOutSumOutT.AsFloat:=SumOutT;
            taOutSumOutOutNDS.AsFloat:=SumOutOutNDS;
            taOutSumNDS10.AsFloat:=SumNDS10;
            taOutSumNDS20.AsFloat:=SumNDS20;
            taOut.Post;
          end;
          sNum:=quSOut3Number.AsString;
          NameCli:='';
          NumDoc:='';
          SumIn:=0;
          SumOut:=0;
          SumOutT:=0;
          SumOutOutNDS:=0;
          SumNDS10:=0;
          SumNDS20:=0;
        end;

        NameCli:=prFindCliName1(quSOut3IndexPoluch.AsInteger,quSOut3CodePoluch.AsInteger,Sinn);
        NumDoc:=quSOut3Number.AsString;
        SumIn:=SumIn+quSOut3SumCenaTovar.AsFloat;
        SumOut:=SumOut+quSOut3SumCenaTovarSpis.AsFloat;
        SumOutT:=SumOutT+quSOut3SumCenaTaraSpis.AsFloat;
        SumOutOutNDS:=SumOutOutNDS+rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100);
        if (quSOut3NDSProc.AsFloat>5) and (quSOut3NDSProc.AsFloat<15) then
        begin
          SumNDS10:=SumNDS10+(quSOut3SumCenaTovar.AsFloat-rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100));
          rNDS10Out:=rNDS10Out+(quSOut3SumCenaTovar.AsFloat-rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100));
        end;
        if (quSOut3NDSProc.AsFloat>=15) then
        begin
          SumNDS20:=SumNDS20+(quSOut3SumCenaTovar.AsFloat-rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100));
          rNDS20Out:=rNDS20Out+(quSOut3SumCenaTovar.AsFloat-rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100));
        end;
        rSumOutNDSOut:=rSumOutNDSOut+rv(quSOut3SumCenaTovar.AsFloat/(100+quSOut3NDSProc.AsFloat)*100);

        quSOut3.Next;
      end;
      quSOut3.Active:=False;

      if sNum<>'`~`' then //��� �� ������ - ���� ����������
      begin
        taOut.Append;
        taOutiType.AsInteger:=1;
        taOutNameGr.AsString:='������ �����������';
        taOutNameGr1.AsString:='����������';
        taOutNameCli.AsString:=NameCli;
        taOutNumDoc.AsString:=NumDoc;
        taOutSumIn.AsFloat:=SumIn;
        taOutSumOut.AsFloat:=SumOut;
        taOutSumOutT.AsFloat:=SumOutT;
        taOutSumOutOutNDS.AsFloat:=SumOutOutNDS;
        taOutSumNDS10.AsFloat:=SumNDS10;
        taOutSumNDS20.AsFloat:=SumNDS20;
        taOut.Post;
      end;
      }


      if iSS=0 then
      begin
        quTTnSelfOut.Active:=False;
        quTTnSelfOut.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quTTnSelfOut.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
        quTTnSelfOut.Active:=True;
        quTTnSelfOut.First;

        while not quTTnSelfOut.Eof do
        begin
          taOut.Append;
          taOutiType.AsInteger:=2;
          taOutNameGr.AsString:='���������� ������';
          taOutNameGr1.AsString:='����������';
          taOutNameCli.AsString:=quTTnSelfOutDepTo.AsString;
          taOutNumDoc.AsString:=quTTnSelfOutNumber.AsString;
          if Trunc(quReportsxDate.AsDateTime)<40575 then
          begin
            taOutSumIn.AsFloat:=rv(quTTnSelfOutSummaTovarMove.AsFloat);
            taOutSumOut.AsFloat:=rv(quTTnSelfOutSummaTovarSpis.AsFloat);
          end else
          begin
            taOutSumIn.AsFloat:=quTTnSelfOutSummaTovarSpis.AsFloat;
            taOutSumOut.AsFloat:=quTTnSelfOutSummaTovarMove.AsFloat;
          end;
          taOutSumOutOutNDS.AsFloat:=0;
          taOutSumNDS10.AsFloat:=0;
          taOutSumNDS20.AsFloat:=0;
          taOut.Post;

          inc(iC2);
          quTTnSelfOut.Next;
        end;
        quTTnSelfOut.Active:=False;
      end;

      if iSS>0 then
      begin
        quTTnSelfOut.Active:=False;
        quTTnSelfOut.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quTTnSelfOut.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
        quTTnSelfOut.Active:=True;
        quTTnSelfOut.First;

        while not quTTnSelfOut.Eof do
        begin
          taOut.Append;
          taOutiType.AsInteger:=2;
          taOutNameGr.AsString:='���������� ������';
          taOutNameGr1.AsString:='����������';
          taOutNameCli.AsString:=quTTnSelfOutDepTo.AsString;
          taOutNumDoc.AsString:=quTTnSelfOutNumber.AsString;
          if iSS=1 then
          begin
            taOutSumIn.AsFloat:=rv(quTTnSelfOutSummaTovarSpis.AsFloat);
            taOutSumOutOutNDS.AsFloat:=0;
            taOutSumNDS10.AsFloat:=0;
            taOutSumNDS20.AsFloat:=0;
          end;
          if iSS=2 then
          begin
            taOutSumOutOutNDS.AsFloat:=rv(quTTnSelfOutSummaTovarSpis.AsFloat);
            taOutSumNDS10.AsFloat:=0;
            taOutSumNDS20.AsFloat:=0;
            taOutSumIn.AsFloat:=rv(quTTnSelfOutSummaTovarSpis.AsFloat);
          end;
          taOutSumOut.AsFloat:=rv(quTTnSelfOutSummaTovarMove.AsFloat);
          taOut.Post;

          inc(iC2);
          quTTnSelfOut.Next;
        end;
        quTTnSelfOut.Active:=False;

        quTTnOutRSpis.Active:=False;
        quTTnOutRSpis.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quTTnOutRSpis.ParamByName('DEPART').AsInteger:=quReportsOtdel.AsInteger;
        quTTnOutRSpis.Active:=True;
        quTTnOutRSpis.First;

        while not quTTnOutRSpis.Eof do
        begin
          taOut.Append;
          taOutiType.AsInteger:=2;
          taOutNameGr.AsString:='���������� ������';
          taOutNameGr1.AsString:='����������';
          taOutNameCli.AsString:='��������';
          taOutNumDoc.AsString:=quTTnOutRSpisNumber.AsString;
          if iSS=1 then
          begin
            taOutSumIn.AsFloat:=rv(quTTnOutRSpisCrock.AsFloat);
            taOutSumOutOutNDS.AsFloat:=0;
            taOutSumNDS10.AsFloat:=0;
            taOutSumNDS20.AsFloat:=0;
          end;
          if iSS=2 then
          begin
            taOutSumOutOutNDS.AsFloat:=rv(quTTnOutRSpisCrock.AsFloat);
            taOutSumNDS10.AsFloat:=0;
            taOutSumNDS20.AsFloat:=0;
            taOutSumIn.AsFloat:=rv(quTTnOutRSpisCrock.AsFloat);
          end;
          taOutSumOut.AsFloat:=rv(quTTnOutRSpisCrock.AsFloat);
          taOut.Post;

          inc(iC2);
          quTTnOutRSpis.Next;
        end;
        quTTnOutRSpis.Active:=False;
      end;

      if taOut.RecordCount=0 then
      begin
        taOut.Append;
        taOutiType.AsInteger:=1;
        taOutNameGr.AsString:='������ �����������';
        taOutNameGr1.AsString:='����������';
        taOutNameCli.AsString:='';
        taOutNumDoc.AsString:='';
        taOutSumIn.AsFloat:=0;
        taOutSumOut.AsFloat:=0;
        taOut.Post;
      end;



      if iSS=0 then
      begin
        RepTO.LoadFromFile(CommonSet.Reports + 'RepTO.frf');

        frVariables.Variable['DocDate']:=quReportsxDate.AsString;
        frVariables.Variable['Depart']:=quReportsName.AsString;
        frVariables.Variable['SumB']:=quReportsOldOstatokTovar.AsFloat;
        frVariables.Variable['SumBT']:=quReportsOldOstatokTara.AsFloat;
        frVariables.Variable['SumInv']:=quReportsOutTovar.AsFloat;
        frVariables.Variable['SumIn']:=quReportsPrihodTovarSumma.AsFloat+quReportsOutTovar.AsFloat;
        frVariables.Variable['SumInT']:=quReportsPrihodTaraSumma.AsFloat;
        frVariables.Variable['SumOut']:=quReportsRashodTovarSumma.AsFloat+quReportsPereoTovarSumma.AsFloat*(-1);
        frVariables.Variable['SumOutT']:=quReportsRashodTaraSumma.AsFloat;
        frVariables.Variable['CountDocIn']:=iC1;
        frVariables.Variable['CountDocOut']:=iC2;
        frVariables.Variable['SumSale']:=quReportsRealTovar.AsFloat;
        frVariables.Variable['SumSaleBn']:=quReportsSenderSumma.AsFloat;
        frVariables.Variable['SumDisc']:=quReportsSkidkaTovar.AsFloat;
        frVariables.Variable['SumDiscBn']:=quReportsSenderSkidka.AsFloat;
        frVariables.Variable['SumAC']:=quReportsPereoTovarSumma.AsFloat*(-1);
        frVariables.Variable['SumE']:=quReportsNewOstatokTovar.AsFloat;
        frVariables.Variable['SumET']:=quReportsNewOstatokTara.AsFloat;
      end;

      if (iSS=1)or(iSS=2) then
      begin
        //������� ��� �� ����������
        rSaleSS:=0;
        rSaleSSOutNDS:=0;
        rSaleSSNDS10:=0;
        rSaleSSNDS20:=0;

        quSale.Active:=False;
        quSale.ParamByName('IDATE').AsInteger:=RoundEx(quReportsxDate.AsDateTime);
        quSale.ParamByName('IDEP').AsInteger:=quReportsOtdel.AsInteger;
        quSale.Active:=True;
        quSale.First;

        if iSS=1 then  //������������� � ���
        begin
          while not quSale.Eof do
          begin
            rSaleSS:=rSaleSS+rv(quSaleSUMIN.AsFloat);
//          if (quSaleNDS.AsFloat<5) then rSaleSSNDS0:=quSaleSUMIN.AsFloat;
            if (quSaleNDS.AsFloat>5) and (quSaleNDS.AsFloat<15) then rSaleSSNDS10:=rv(quSaleSUMIN.AsFloat);
            if (quSaleNDS.AsFloat>=15) then rSaleSSNDS20:=rv(quSaleSUMIN.AsFloat);
            quSale.Next;
          end;

          rSaleSS:=rv(rSaleSS);
          rSaleSSNDS10:=rv(rSaleSSNDS10/110*10); //����� ��� 10
          rSaleSSNDS20:=rv(rSaleSSNDS20/118*18); //����� ��� 18
          rSaleSSOutNDS:=rSaleSS-rSaleSSNDS10-rSaleSSNDS20;

//          frVariables.Variable['SumIn']:=rSumOutNDS+rNDS10+rNDS20+quReportsOutTovar.AsFloat;
          frVariables.Variable['SumIn']:=rSumPost;
        end;

        if iSS=2 then  //������������� ��� ���
        begin
          while not quSale.Eof do
          begin
            rSaleSS:=rSaleSS+rv(quSaleSUMIN0.AsFloat);
//          if (quSaleNDS.AsFloat<5) then rSaleSSNDS0:=quSaleSUMIN.AsFloat;
            if (quSaleNDS.AsFloat>5) and (quSaleNDS.AsFloat<15) then rSaleSSNDS10:=rv(quSaleSUMIN0.AsFloat);
            if (quSaleNDS.AsFloat>=15) then rSaleSSNDS20:=rv(quSaleSUMIN0.AsFloat);
            quSale.Next;
          end;

          rSaleSS:=rv(rSaleSS);
          rSaleSSOutNDS:=rSaleSS;
          rSaleSSNDS10:=rv(rSaleSSNDS10/100*10); //����� ��� 10
          rSaleSSNDS20:=rv(rSaleSSNDS20/100*18); //����� ��� 18
          rSaleSS:=rSaleSS+rSaleSSNDS10+rSaleSSNDS20;

          frVariables.Variable['SumIn']:=rSumOutNDS+quReportsOutTovar.AsFloat;
        end;

        quSale.Active:=False;

        RepTO.LoadFromFile(CommonSet.Reports + 'RepTO1.frf');

        frVariables.Variable['DocDate']:=quReportsxDate.AsString;
        frVariables.Variable['Depart']:=quReportsName.AsString;
        frVariables.Variable['SumB']:=quReportsOldOstatokTovar.AsFloat;
        frVariables.Variable['SumBT']:=quReportsOldOstatokTara.AsFloat;
        frVariables.Variable['SumInv']:=quReportsOutTovar.AsFloat;
        frVariables.Variable['SumInT']:=quReportsPrihodTaraSumma.AsFloat;
        frVariables.Variable['SumOut']:=quReportsRashodTovarSumma.AsFloat;
        frVariables.Variable['SumOutT']:=quReportsRashodTaraSumma.AsFloat;
        frVariables.Variable['CountDocIn']:=iC1;
        frVariables.Variable['CountDocOut']:=iC2;
        frVariables.Variable['SumSale']:=quReportsRealTovar.AsFloat;
        frVariables.Variable['SumSaleBn']:=quReportsSenderSumma.AsFloat;
        frVariables.Variable['SumDisc']:=quReportsSkidkaTovar.AsFloat;
        frVariables.Variable['SumDiscBn']:=quReportsSenderSkidka.AsFloat;
        frVariables.Variable['SumAC']:=quReportsPereoTovarSumma.AsFloat*(-1);
        frVariables.Variable['SumE']:=quReportsNewOstatokTovar.AsFloat;
        frVariables.Variable['SumET']:=quReportsNewOstatokTara.AsFloat;
        frVariables.Variable['SumInOutNDS']:=rSumOutNDS;
        frVariables.Variable['NDS10']:=rNDS10;
        frVariables.Variable['NDS20']:=rNDS20;
        frVariables.Variable['SumSaleSS']:=rSaleSS;
        frVariables.Variable['SumSaleSSOutNDS']:=rSaleSSOutNDS;
        frVariables.Variable['SumSaleNDS10']:=rSaleSSNDS10;
        frVariables.Variable['SumSaleNDS20']:=rSaleSSNDS20;
      end;

      RepTO.ReportName:='�������� �����.';
      RepTO.PrepareReport;
      RepTO.ShowPreparedReport;
    end
    else
    begin
      if quReportsV03.AsInteger=1 then
      begin
        CloseTe(taSvod);

        quReportSvod.Active:=False;
        quReportSvod.ParamByName('DATEB').AsDate:=quReportsxDate.AsDateTime;
        quReportSvod.ParamByName('DATEE').AsDate:=quReportsxDate.AsDateTime;
        quReportSvod.Active:=True;

        quReportSvod.First;
        while not quReportSvod.Eof do
        begin
          taSvod.Append;
          taSvodiDep.AsInteger:=quReportSvodOtdel.AsInteger;
          taSvodNameDep.AsString:=quReportSvodName.AsString;
          taSvodrIn.AsFloat:=quReportSvodOldOstatokTovar.AsFloat;
//        PrihodTovarSumma - ����� ������
//RashodTovar - �������
//RealTovar - ����������
//OutTovar - ��������������
//PereoTovarSumma

          taSvodrRealBn.AsFloat:=quReportSvodSenderSumma.AsFloat;
          taSvodrPrih.AsFloat:=quReportSvodPrihodTovarSumma.AsFloat;
          taSvodrRet.AsFloat:=quReportSvodRashodTovar.AsFloat;
          taSvodrReal.AsFloat:=quReportSvodRealTovar.AsFloat;
          taSvodrInv.AsFloat:=quReportSvodOutTovar.AsFloat;
          taSvodrPereoc.AsFloat:=quReportSvodPereoTovarSumma.AsFloat;

          taSvodrOutVn.AsFloat:=quReportSvodSelfRashodTovar.AsFloat;

          taSvodrOut.AsFloat:=quReportSvodNewOstatokTovar.AsFloat;
          taSvod.Post;

          quReportSvod.Next;
        end;
        quReportSvod.Active:=False;

        RepTO.LoadFromFile(CommonSet.Reports + 'RepTOSv.frf');

        frVariables.Variable['DocDate']:=quReportsxDate.AsString;
        frVariables.Variable['Depart']:=quReportsName.AsString;

        RepTO.ReportName:='�������� �����. (�������)';
        RepTO.PrepareReport;
        RepTO.ShowPreparedReport;

      end;
    end;
  end;
end;

procedure TfmTORep.quTTnInRCalcFields(DataSet: TDataSet);
begin
  quTTnInRNameCli.AsString:='';
  if quTTnInRIndexPost.AsInteger=1 then quTTnInRNameCli.AsString:=quTTnInRNamePost.AsString;
  if quTTnInRIndexPost.AsInteger=2 then quTTnInRNameCli.AsString:=quTTnInRNameInd.AsString;
end;

procedure TfmTORep.quTTnOutRCalcFields(DataSet: TDataSet);
begin
  quTTnOutRNameCli.AsString:='';
  if quTTnOutRIndexPoluch.AsInteger=1 then quTTnOutRNameCli.AsString:=quTTnOutRNamePost.AsString;
  if quTTnOutRIndexPoluch.AsInteger=2 then quTTnOutRNameCli.AsString:=quTTnOutRNameInd.AsString;
end;

procedure TfmTORep.acCorrPartOutExecute(Sender: TObject);
var rSaleSS:Real;
    iSS:INteger;
    DateCorr,DateCloseBuh:TDateTime;
    bMay:Boolean;
begin
//��������� ���������� � �����.�����
  if ViewTO.Controller.SelectedRecordCount>0 then
  begin
    if pos('Oper',Person.Name)=0 then
    begin
      with dmMC do
      begin
        DateCorr:=Trunc(quReportsxDate.AsDateTime);
        bMay:=True;

        quCloseHBuh.Active:=False;
        quCloseHBuh.Active:=True;
        if quCloseHBuh.RecordCount>0 then
        begin
          DateCloseBuh:=quCloseHBuhDateClose.AsDateTime;
          if DateCloseBuh>=DateCorr then
          begin
            bMay:=False;
            ShowMessage('�������� ��������� !!! ���������� ����������� - '+ds(DateCloseBuh+1));
          end;
        end;
        quCloseHBuh.Active:=False;

        if bMay then
        begin


        iSS:=fSS(quReportsOtdel.AsInteger);
        if iSS=0 then exit;

        rSaleSS:=0;

        try
          if (iSS=1)or(iSS=2) then
          begin
            //������� ��� �� ����������
//            rSaleSSOutNDS:=0;
//            rSaleSSNDS10:=0;
//            rSaleSSNDS20:=0;

            quSale.Active:=False;
            quSale.ParamByName('IDATE').AsInteger:=RoundEx(quReportsxDate.AsDateTime);
            quSale.ParamByName('IDEP').AsInteger:=quReportsOtdel.AsInteger;
            quSale.Active:=True;
            quSale.First;
            while not quSale.Eof do
            begin
              if iSS=1 then rSaleSS:=rSaleSS+rv(quSaleSUMIN.AsFloat);
              if iSS=2 then rSaleSS:=rSaleSS+rv(quSaleSUMIN0.AsFloat);
//              if (quSaleNDS.AsFloat>5) and (quSaleNDS.AsFloat<15) then rSaleSSNDS10:=rv(quSaleSUMIN.AsFloat);
//              if (quSaleNDS.AsFloat>=15) then rSaleSSNDS20:=rv(quSaleSUMIN.AsFloat);
              quSale.Next;
            end;
            quSale.Active:=False;
          end;

          fmCorrTO:=tfmCorrTO.Create(Application);
          fmCorrTO.Caption:='��������� �� '+quReportsxDate.AsString;
          fmCorrTO.cxCalcEdit1.Value:=quReportsOldOstatokTovar.AsFloat;
          fmCorrTO.cxCalcEdit2.Value:=quReportsPrihodTovarSumma.AsFloat+quReportsOutTovar.AsFloat;
          fmCorrTO.cxCalcEdit3.Value:=rSaleSS;
          fmCorrTO.cxCalcEdit4.Value:=quReportsRashodTovar.AsFloat+quReportsPereoTovarSumma.AsFloat*(-1)+quReportsSelfRashodTovar.AsFloat;
          fmCorrTO.cxCalcEdit5.Value:=quReportsOldOstatokTovar.AsFloat+quReportsPrihodTovarSumma.AsFloat+quReportsOutTovar.AsFloat-rSaleSS-(quReportsRashodTovar.AsFloat+quReportsPereoTovarSumma.AsFloat*(-1)+quReportsSelfRashodTovar.AsFloat);
          fmCorrTO.cxCalcEdit6.Value:=quReportsNewOstatokTovar.AsFloat;
          fmCorrTO.cxCalcEdit7.Value:=quReportsNewOstatokTovar.AsFloat-(quReportsOldOstatokTovar.AsFloat+quReportsPrihodTovarSumma.AsFloat+quReportsOutTovar.AsFloat-rSaleSS-(quReportsRashodTovar.AsFloat+quReportsPereoTovarSumma.AsFloat*(-1)+quReportsSelfRashodTovar.AsFloat));

          fmCorrTO.ShowModal;
          if fmCorrTO.ModalResult=mrOk then
          begin //���������
              //1.�������� ����� �� �� ������� - ���� ���� ���������
            if rv(fmCorrTO.cxCalcEdit6.Value)<>quReportsNewOstatokTovar.AsFloat then
            begin
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Clear;
              quE.SQL.Add('Update "REPORTS" Set ');
              quE.SQL.Add('NewOstatokTovar='+fs(rv(fmCorrTO.cxCalcEdit6.Value)));
              quE.SQL.Add('where "xDate"='''+ds(quReportsxDate.AsDateTime)+'''');
              quE.SQL.Add('and Otdel='+its(quReportsOtdel.AsInteger));
              quE.ExecSQL;

              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Clear;
              quE.SQL.Add('Update "REPORTS" Set ');
              quE.SQL.Add('OldOstatokTovar='+fs(rv(fmCorrTO.cxCalcEdit6.Value)));
              quE.SQL.Add('where "xDate"='''+ds(quReportsxDate.AsDateTime+1)+'''');
              quE.SQL.Add('and Otdel='+its(quReportsOtdel.AsInteger));
              quE.ExecSQL;
            end;

              //2.����� ����������� ���������� ����
            if (rv(fmCorrTO.cxCalcEdit7.Value))<>0 then
            begin
              rSaleSS:=rv(fmCorrTO.cxCalcEdit3.Value)+(-1)*rv(fmCorrTO.cxCalcEdit7.Value);

              //����������� ���������  - ���������� � ����� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "REPORTS" Set ');
              quE.SQL.Add('xRezerv='+fs(rSaleSS));
              quE.SQL.Add('where "xDate"='''+ds(quReportsxDate.AsDateTime)+'''');
              quE.SQL.Add('and Otdel='+its(quReportsOtdel.AsInteger));
              quE.ExecSQL;

              //����������� ���������� ����

              quPOut.Active:=False;
              quPOut.SQL.Clear;
              quPOut.SQL.Add('select top 1 * from "A_PARTOUT"');
              quPOut.SQL.Add('where IDATE='+its(Trunc(quReportsxDate.AsDateTime)));
              quPOut.SQL.Add('and IDSTORE='+its(quReportsOtdel.AsInteger));
              quPOut.SQL.Add('and ITYPE=7');
              if (rv(fmCorrTO.cxCalcEdit7.Value))>0 then //���� �����
                quPOut.SQL.Add('and SUMIN>'+fs(100+rv(fmCorrTO.cxCalcEdit7.Value)));
              quPOut.SQL.Add('order by ARTICUL');
              quPOut.Active:=True;

              if quPOut.RecordCount>0 then
              begin //������� ������ ��� ��������������
                rSaleSS:=quPOutSUMIN.AsFloat+(-1)*rv(fmCorrTO.cxCalcEdit7.Value);

                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_PARTOUT" Set ');
                quE.SQL.Add('SUMIN='+fs(rSaleSS));
                quE.SQL.Add('where ');
                quE.SQL.Add('ARTICUL='+its(quPOutARTICUL.AsInteger));
                quE.SQL.Add('and IDATE='+its(quPOutIDATE.AsInteger));
                quE.SQL.Add('and IDSTORE='+its(quPOutIDSTORE.AsInteger));
                quE.SQL.Add('and IDDOC='+its(quPOutIDDOC.AsInteger));
                quE.SQL.Add('and PARTIN='+its(quPOutPARTIN.AsInteger));
                quE.SQL.Add('and ID='+its(quPOutID.AsInteger));
                quE.ExecSQL;

                ShowMessage('��������� ��');

              end else
                Showmessage('��������� ����������� ����� ����������.');
            end;

            fmTORep.ViewTO.BeginUpdate;

            quReports.Active:=False;
            quReports.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
            quReports.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
            quReports.Active:=True;

            fmTORep.ViewTO.EndUpdate;
            fmTORep.Caption:='�������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

          end;
        finally
          fmCorrTO.Release;
        end;
        end; //  if bMay then
      end;
    end else
      showmessage('��� ����.');
  end else
  begin
    showmessage('�������� �� ��� ���������.');
  end;
end;

procedure TfmTORep.acSSRExecute(Sender: TObject);
Var iSS:INteger;
    rSaleSS:Real;
begin
  if LevelTO.Visible then
  begin
    CloseTe(teSSR);
    
    LevelTO.Visible:=False;
    LevelSSR.Visible:=True;

    ViewTO.BeginUpdate;
    ViewSSR.BeginUpdate;

    with dmMC do
    begin
      quReports.First;
      while not quReports.Eof do
      begin
        if quReportsRealTovar.AsFloat>0 then
        begin
          iSS:=fSS(quReportsOtdel.AsInteger);
          if iSS=0 then exit;

          rSaleSS:=0;

          try
            if (iSS=1)or(iSS=2) then
            begin
            //������� ��� �� ����������
//            rSaleSSOutNDS:=0;
//            rSaleSSNDS10:=0;
//            rSaleSSNDS20:=0;

              quSale.Active:=False;
              quSale.ParamByName('IDATE').AsInteger:=RoundEx(quReportsxDate.AsDateTime);
              quSale.ParamByName('IDEP').AsInteger:=quReportsOtdel.AsInteger;
              quSale.Active:=True;
              quSale.First;
              while not quSale.Eof do
              begin
                if iSS=1 then rSaleSS:=rSaleSS+rv(quSaleSUMIN.AsFloat);
                if iSS=2 then rSaleSS:=rSaleSS+rv(quSaleSUMIN0.AsFloat);
//              if (quSaleNDS.AsFloat>5) and (quSaleNDS.AsFloat<15) then rSaleSSNDS10:=rv(quSaleSUMIN.AsFloat);
//              if (quSaleNDS.AsFloat>=15) then rSaleSSNDS20:=rv(quSaleSUMIN.AsFloat);
                quSale.Next;
              end;
              quSale.Active:=False;
            end;
          except
          end;

          teSSR.Append;
          teSSRiDate.AsInteger:=Trunc(quReportsxDate.AsDateTime);
          teSSRdDate.AsDateTime:=quReportsxDate.AsDateTime;
          teSSRiDep.AsInteger:=quReportsOtdel.AsInteger;
          teSSRNameDep.AsString:=quReportsName.AsString;
          teSSRRSUM.AsFloat:=quReportsRealTovar.AsFloat;
          teSSRRSUMIN.AsFloat:=quReportsxRezerv.AsFloat;
          teSSRRSUMINPP.AsFloat:=rSaleSS;
          teSSRRSUMDIF.AsFloat:=quReportsxRezerv.AsFloat-rSaleSS;
          teSSR.Post;
        end;
        quReports.Next;
      end;
    end;

    ViewTO.EndUpdate;
    ViewSSR.EndUpdate;

  end else
  begin
    LevelTO.Visible:=True;
    LevelSSR.Visible:=False;
  end;
end;

procedure TfmTORep.acGenSvExecute(Sender: TObject);
Var DateB,DateE:INteger;
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      //�������
      fmSt.Memo1.Clear;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add('������.');
        cxButton1.Enabled:=False;

        DateB:=Trunc(CommonSet.DateBeg);
        DateE:=Trunc(CommonSet.DateEnd);

        prRecalcTOSv(DateB,DateE,fmSt.Memo1,0); //�������� ��������

        cxButton1.Enabled:=True;
        Memo1.Lines.Add('��.');

      end;
      fmTORep.ViewTO.BeginUpdate;

      quReports.Active:=False;
      quReports.Active:=True;

      fmTORep.ViewTO.EndUpdate;
      fmTORep.Caption:='�������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

end.
