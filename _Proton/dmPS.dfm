object dmP: TdmP
  Left = 252
  Top = 177
  Width = 880
  Height = 822
  Caption = 'dmP'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 865
    Height = 225
    Caption = #1055#1088#1077#1076#1079#1072#1103#1074#1082#1072' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
    TabOrder = 0
  end
  object GroupBox2: TGroupBox
    Left = 4
    Top = 228
    Width = 653
    Height = 233
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1074#1086#1079#1074#1088#1072#1090'('#1086#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085#1080#1077') '#1080' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' '#1087#1088#1077#1076#1079#1072#1103#1074#1086#1082
    TabOrder = 1
  end
  object GroupBox3: TGroupBox
    Left = 662
    Top = 232
    Width = 203
    Height = 113
    Caption = #1050#1072#1088#1090#1086#1095#1082#1080' '#1090#1086#1074#1072#1088#1072' ('#1086#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1074#1086#1079#1074#1088'.)'
    TabOrder = 2
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 464
    Width = 233
    Height = 129
    Caption = #1054#1090#1095#1105#1090' '#1087#1088#1086#1089#1088#1086#1095#1077#1085#1085#1099#1093' '#1074#1086#1079#1074#1088#1072#1090#1086#1074
    TabOrder = 3
  end
  object GroupBox5: TGroupBox
    Left = 664
    Top = 352
    Width = 201
    Height = 113
    Caption = #1055#1077#1088#1077#1085#1086#1089' '#1089' 31.12'
    TabOrder = 4
  end
  object GroupBox6: TGroupBox
    Left = 240
    Top = 464
    Width = 89
    Height = 129
    Caption = #1057#1074#1103#1079'-'#1099#1077' '#1087#1086#1089#1090'.'
    TabOrder = 5
  end
  object GroupBox7: TGroupBox
    Left = 336
    Top = 464
    Width = 81
    Height = 129
    Caption = #1044#1086#1073'. '#1084#1072#1090#1088#1080#1094
    TabOrder = 6
  end
  object GroupBox8: TGroupBox
    Left = 424
    Top = 464
    Width = 113
    Height = 129
    Caption = #1045#1089#1090#1100' '#1087#1088#1077#1076#1079'. '#1091' '#1087#1086#1089#1090'.'
    TabOrder = 7
  end
  object GroupBox9: TGroupBox
    Left = 540
    Top = 462
    Width = 325
    Height = 243
    Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
    TabOrder = 8
  end
  object quDocVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  '
      'dhr."ID",'
      'dhr."DocNum",'
      'dhr."DocDate",'
      'dhr."DocTime",'
      'dhr."Comment",'
      'dhr."User",'
      'dhr."Status"'
      ''
      'from "A_DocHRet" dhr'
      ''
      'where dhr.DocDate>=:DATEB'
      'and dhr.DocDate<=:DATEE'
      'order by dhr.ID asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 20
    Top = 20
    object quDocVozID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object quDocVozDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Required = True
      Size = 15
    end
    object quDocVozDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quDocVozDOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
    object quDocVozCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 250
    end
    object quDocVozUSER: TStringField
      FieldName = 'USER'
      Required = True
      Size = 15
    end
    object quDocVozSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 18
    end
  end
  object dsquDocVoz: TDataSource
    DataSet = quDocVoz
    Left = 20
    Top = 72
  end
  object quSpecVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    dsr.IDHead,'
      '    dsr.ID,'
      '    dsr.DOCDATE,'
      '    dsr.Code,'
      '    gd.FullName as NameCode,'
      '    gd.Reserv1 as QALLREM,'
      '    dsr.Depart,'
      '    de.Name as DepartName,'
      '    dsr.idCli,'
      '    v.FullName as Namecli ,       '
      '    dsr.Quant,'
      '    dsr.QuantOut,'
      '    dsr.QRem,'
      '    dsr.PriceIn0,'
      '    dsr.PriceIn,'
      '    dsr.RPrice,'
      '    dsr.RsumIn0,'
      '    dsr.RsumIn,'
      '    dsr.RsumNDS,'
      '    dsr.Status,'
      '    dsr.DateVoz,'
      '    dsr.IDReason'
      ''
      '   from "A_DocSRet" dsr    '
      '     left join "Goods" gd  on gd.ID=dsr.Code'
      '     left join "Depart" de  on de.ID=dsr.Depart'
      '     left join "RVendor" v  on v.Status=dsr.idCli'
      ''
      '   where dsr.IDHead=:IHEAD'
      '   and dsr.docdate=:idate')
    Params = <
      item
        DataType = ftInteger
        Name = 'IHEAD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'idate'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 90
    Top = 20
    object quSpecVozIDHEAD: TStringField
      FieldName = 'IDHEAD'
      Required = True
      Size = 19
    end
    object quSpecVozID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object quSpecVozDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quSpecVozCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quSpecVozNameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quSpecVozQALLREM: TFloatField
      FieldName = 'QALLREM'
    end
    object quSpecVozDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quSpecVozDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quSpecVozIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quSpecVozNamecli: TStringField
      FieldName = 'Namecli'
      Size = 100
    end
    object quSpecVozQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quSpecVozQUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
    end
    object quSpecVozQRem: TFloatField
      FieldName = 'QRem'
    end
    object quSpecVozPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecVozPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecVozRPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quSpecVozRSUMIN0: TFloatField
      FieldName = 'RSUMIN0'
    end
    object quSpecVozRSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quSpecVozRSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object quSpecVozStatus: TStringField
      FieldName = 'Status'
    end
    object quSpecVozDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object quSpecVozIDReason: TIntegerField
      FieldName = 'IDReason'
    end
  end
  object quPostVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select'
      '            tl.Depart,'
      '            de.Name as DepartName,'
      '            tl.CodePost,'
      '            v.Status as idcli,'
      '           v.Status, '
      '           v.FullName as Namecli,'
      '            cv.IDTYPEVOZ as CliTypeV,'
      '            tl.CodeTovar as Code,'
      '            gd.FullName as NameCode,'
      '            gd.V14 as TypeVoz,'
      '            tl.CenaTovar as PriceIn,'
      '            tl.NewCenaTovar as Rprice,'
      '            tl.SumCenaTovarPost as SumPost,'
      '            tl.NDSSum,'
      '            tl.NDSProc,'
      '            tl.kol,'
      '            alp.IDALTCLI'
      ''
      '   from "TTNInLn" tl'
      '   left  join Depart de on de.ID=tl.Depart'
      '   left  join RVendor v on v.Vendor=tl.CodePost   '
      '   left  join A_CLIVOZ cv on v.Status=cv.IDCB'
      '   left  join Goods gd on gd.ID=tl.CodeTovar'
      '   left  join A_ALTCLI alp on  v.Status=alp.IDCLI'
      ''
      '   where tl.CodeTovar=:IDCARD'
      '            and v.Status>0'
      '            and tl.Depart>0')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 168
    Top = 21
    object quPostVozDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quPostVozDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quPostVozCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPostVozidcli: TSmallintField
      FieldName = 'idcli'
      Required = True
    end
    object quPostVozNamecli: TStringField
      FieldName = 'Namecli'
      Required = True
      Size = 100
    end
    object quPostVozCode: TIntegerField
      FieldName = 'Code'
    end
    object quPostVozNameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quPostVozPriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object quPostVozRprice: TFloatField
      FieldName = 'Rprice'
    end
    object quPostVozSumPost: TFloatField
      FieldName = 'SumPost'
    end
    object quPostVozNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quPostVozKol: TFloatField
      FieldName = 'Kol'
    end
    object quPostVozTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
    object quPostVozCliTypeV: TSmallintField
      FieldName = 'CliTypeV'
    end
    object quPostVozNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quPostVozIDALTCLI: TIntegerField
      FieldName = 'IDALTCLI'
    end
  end
  object dsquSpecVoz: TDataSource
    DataSet = quSpecVoz
    Left = 92
    Top = 72
  end
  object dsquPostVoz: TDataSource
    DataSet = quPostVoz
    Left = 164
    Top = 72
  end
  object dsquDepVoz: TDataSource
    DataSet = quDepVoz
    Left = 236
    Top = 72
  end
  object quDepVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  SELECT '
      '                 ID,'
      '                 Name,'
      '                 Status1,'
      '                 ObjectTypeID '
      '  '
      '  FROM "Depart"'
      '  Where ObjectTypeID=6 and Status1=9 and Status4<>0'
      '  Order by Name')
    Params = <>
    Left = 236
    Top = 21
    object quDepVozID: TSmallintField
      FieldName = 'ID'
    end
    object quDepVozName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDepVozStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object quDepVozObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
  end
  object quGrafVozvrat: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  gv.ICLI,'
      '             gv.IDATEZ,'
      '             gv.IDATEV,'
      '             post.FullName,'
      '             post.Name,'
      '             cv.MOLVOZ,'
      '             cv.PHONEVOZ,'
      '             cv.EMAILVOZ'
      '  '
      '  from "A_GRAFVOZ" gv'
      '  left join "RVendor" post on post.Status=gv.ICLI'
      '  left join A_CLIVOZ cv on gv.ICLI=cv.IDCB '
      ''
      '  where gv.IDATEV>=:IDATEB'
      '  and gv.IDATEV<=:IDATEE'
      '  order by ICLI,IDATEZ asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 312
    Top = 22
    object quGrafVozvratICLI: TIntegerField
      FieldName = 'ICLI'
      Required = True
    end
    object quGrafVozvratIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
      Required = True
    end
    object quGrafVozvratIDATEV: TIntegerField
      FieldName = 'IDATEV'
      Required = True
    end
    object quGrafVozvratFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quGrafVozvratName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quGrafVozvratMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object quGrafVozvratPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object quGrafVozvratEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
  end
  object dsquGrafVozvrat: TDataSource
    DataSet = quGrafVozvrat
    Left = 351
    Top = 80
  end
  object quDateVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '    select  top 1 gv.IDATEV'
      '    from "A_GRAFVOZ" gv '
      '    where gv.IDATEV>=:IDATEB'
      '    and gv.IDATEV<=:IDATEE'
      '    and gv.IDATEZ>=:IDATEZ'
      '    and gv.ICLI=:IDCli'
      '    order by IDATEV asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEZ'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDCli'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 464
    Top = 22
    object quDateVozIDATEV: TIntegerField
      FieldName = 'IDATEV'
      Required = True
    end
  end
  object dsquDateVoz: TDataSource
    DataSet = quDateVoz
    Left = 463
    Top = 72
  end
  object quReasV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  *'
      '  from "A_REASONVOZ"'
      '  order by id asc')
    Params = <>
    Left = 537
    Top = 22
    object quReasVid: TStringField
      FieldName = 'id'
      Size = 19
    end
    object quReasVReason: TStringField
      FieldName = 'Reason'
      Size = 150
    end
  end
  object dsquReasV: TDataSource
    DataSet = quReasV
    Left = 536
    Top = 72
  end
  object quPost1: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select top 5'
      '    tl.CodeTovar as Code,'
      '    gd.FullName as NameCode,'
      '    tl.Depart,'
      '    de.Name as NameDep,'
      '    tl.DateInvoice,'
      '    tl.CodePost,'
      '    v.Status as idcli,'
      '    v."FullName" as NamePost,'
      '    i."Name" as NameInd,'
      '    tl.CodeEdIzm,'
      '    tl.CenaTovar as PriceIn,'
      '    tl.NewCenaTovar as Rprice,'
      '   tl.SumCenaTovarPost as SumPost,'
      '   tl.NDSProc,'
      '   tl.NDSSum,'
      '   tl.kol,'
      '   cv.IDTYPEVOZ ,'
      '   alp.IDALTCLI'
      ''
      ''
      '  from "TTNInLn" tl'
      '    left  join Goods gd on gd.ID=tl.CodeTovar'
      '    left join Depart de on de.ID=tl.Depart'
      '    left outer join RVendor v on v.Vendor=tl."CodePost"'
      '    left outer join RIndividual i on i.Code=tl."CodePost"'
      '    left  join A_CLIVOZ cv on v.Status=cv.IDCB'
      '    left  join A_ALTCLI alp on  v.Status =alp.IDCLI'
      ''
      '  where tl.CodeTovar=:IDCARD'
      '    and tl.DateInvoice>=:DDATEB'
      '    and tl.DateInvoice<=:DDATEE    '
      '    and ( cv.IDTYPEVOZ <3)'
      '  order by DateInvoice DESC')
    Params = <
      item
        DataType = ftSmallint
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DDATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 605
    Top = 20
    object quPost1Code: TIntegerField
      FieldName = 'Code'
    end
    object quPost1NameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quPost1Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost1NameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quPost1DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost1CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost1idcli: TSmallintField
      FieldName = 'idcli'
    end
    object quPost1NamePost: TStringField
      FieldName = 'NamePost'
      Size = 100
    end
    object quPost1NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost1CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost1PriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object quPost1Rprice: TFloatField
      FieldName = 'Rprice'
    end
    object quPost1SumPost: TFloatField
      FieldName = 'SumPost'
    end
    object quPost1NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quPost1NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quPost1Kol: TFloatField
      FieldName = 'Kol'
    end
    object quPost1IDTYPEVOZ: TSmallintField
      FieldName = 'IDTYPEVOZ'
    end
    object quPost1IDALTCLI: TIntegerField
      FieldName = 'IDALTCLI'
    end
  end
  object dsquPost1: TDataSource
    DataSet = quPost1
    Left = 605
    Top = 76
  end
  object quTypeV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  *'
      '  from "A_TypeVoz"'
      '  order by ID asc')
    Params = <>
    Left = 737
    Top = 22
    object quTypeVID: TStringField
      FieldName = 'ID'
      Required = True
      Size = 19
    end
    object quTypeVTYPEVOZ: TStringField
      FieldName = 'TYPEVOZ'
      Size = 100
    end
  end
  object dsquTypeV: TDataSource
    DataSet = quTypeV
    Left = 736
    Top = 72
  end
  object quFilVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        gd.Name as NameCode,'
      '        gd.FullName as FNameCode,'
      '        gd.BarCode, '
      '        gd.EdIzm,'
      '        gd.NDS,'
      '        v.Status as idvendor,'
      '        sum (dsr.Quant) as Quant,'
      '        sum (dsr.QuantOut) as QuantOut,'
      '        sum (dsr.QRem) as QRem,   '
      '        dsr.PRICEIN,'
      '        gd.Cena as RPrice,'
      '        sum (dsr.RSUMIN) as RSUMIN,   '
      '        sum (dsr.RsumNDS) as RsumNDS'
      ''
      ' '
      '   from  "A_DOCSRET" dsr '
      '   left join "Goods" as gd on gd.id =  dsr.Code'
      '   left outer join RVendor v on v.Status=dsr.idCli'
      ''
      '   where  v.Vendor=:ICLI'
      '   and dsr.Depart=:DEP'
      '   and dsr.STATUS<>'#39'2'#39' '
      ''
      
        '   group by dsr.Code, gd.Name, gd.FullName ,v.Status, gd.BarCode' +
        ', '
      '   gd.EdIzm, gd.NDS, dsr.PRICEIN,gd.Cena')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 24
    Top = 254
    object quFilVozCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quFilVozNameCode: TStringField
      FieldName = 'NameCode'
      Size = 30
    end
    object quFilVozFNameCode: TStringField
      FieldName = 'FNameCode'
      Size = 60
    end
    object quFilVozBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFilVozEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFilVozNDS: TFloatField
      FieldName = 'NDS'
    end
    object quFilVozQuant: TFloatField
      FieldName = 'Quant'
    end
    object quFilVozQuantOut: TFloatField
      FieldName = 'QuantOut'
    end
    object quFilVozQRem: TFloatField
      FieldName = 'QRem'
    end
    object quFilVozPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quFilVozRPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quFilVozRSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quFilVozRsumNDS: TFloatField
      FieldName = 'RsumNDS'
    end
    object quFilVozidvendor: TSmallintField
      FieldName = 'idvendor'
      Required = True
    end
  end
  object dsquFilVoz: TDataSource
    DataSet = quFilVoz
    Left = 23
    Top = 304
  end
  object quPost2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    tl.CodeTovar as Code,'
      '    gd.FullName as NameCode,'
      '    tl.Depart,'
      '    de.Name as NameDep,'
      '    tl.DateInvoice,'
      '    tl.CodePost,'
      '    v.Status as idcli,'
      '    v."FullName" as NamePost,'
      '    i."Name" as NameInd,'
      '    tl.CodeEdIzm,'
      '    tl.CenaTovar as PriceIn,'
      '    tl.NewCenaTovar as Rprice,'
      '   tl.SumCenaTovarPost as SumPost,'
      '   tl.NDSProc,'
      '   tl.NDSSum,'
      '   tl.kol,'
      '   cv.IDTYPEVOZ ,'
      '   alp.IDALTCLI'
      ''
      ''
      '  from "TTNInLn" tl'
      '    left  join Goods gd on gd.ID=tl.CodeTovar'
      '    left join Depart de on de.ID=tl.Depart'
      '    left outer join RVendor v on v.Vendor=tl."CodePost"'
      '    left outer join RIndividual i on i.Code=tl."CodePost"'
      '    left  join A_CLIVOZ cv on v.Status=cv.IDCB'
      '    left  join A_ALTCLI alp on  v.Status=alp.IDCLI'
      ''
      '  where tl.CodeTovar=:IDCARD'
      '    and tl.DateInvoice>=:DDATEB'
      '    and tl.DateInvoice<=:DDATEE    '
      '    and ( cv.IDTYPEVOZ <3)'
      '  order by DateInvoice DESC')
    Params = <
      item
        DataType = ftSmallint
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DDATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 669
    Top = 20
    object quPost2Code: TIntegerField
      FieldName = 'Code'
    end
    object quPost2NameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quPost2Depart: TSmallintField
      FieldName = 'Depart'
    end
    object quPost2NameDep: TStringField
      FieldName = 'NameDep'
      Size = 30
    end
    object quPost2DateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object quPost2CodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPost2idcli: TSmallintField
      FieldName = 'idcli'
    end
    object quPost2NamePost: TStringField
      FieldName = 'NamePost'
      Size = 100
    end
    object quPost2NameInd: TStringField
      FieldName = 'NameInd'
      Size = 30
    end
    object quPost2CodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object quPost2PriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object quPost2Rprice: TFloatField
      FieldName = 'Rprice'
    end
    object quPost2SumPost: TFloatField
      FieldName = 'SumPost'
    end
    object quPost2NDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object quPost2NDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quPost2Kol: TFloatField
      FieldName = 'Kol'
    end
    object quPost2IDTYPEVOZ: TSmallintField
      FieldName = 'IDTYPEVOZ'
    end
    object quPost2IDALTCLI: TIntegerField
      FieldName = 'IDALTCLI'
    end
  end
  object dsquPost2: TDataSource
    DataSet = quPost2
    Left = 669
    Top = 76
  end
  object quSelSpV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '    dsr.IDHead,'
      '    dsr.ID,'
      '    dsr.DOCDATE,'
      '    dhr."DocTime",'
      '    dsr.Code,'
      '    dsr.Depart,'
      '    dsr.idCli,'
      '    dsr.Quant,'
      '    dsr.QuantOut,'
      '    dsr.QRem,'
      '    dsr.PriceIn0,'
      '    dsr.PriceIn,'
      '    dsr.RPrice,'
      '    dsr.RsumIn0,'
      '    dsr.RsumIn,'
      '    dsr.RsumNDS,'
      '    dsr.Status,'
      '    dsr.DateVoz'
      ''
      '   from  "A_DOCSRET" dsr '
      ''
      '--   left outer join RVendor v on v.Status=dsr.idCli'
      '   left join "A_DocHRet" dhr  on dhr.ID=dsr.IDHead'
      ' '
      '  where  dsr.idCli=:ICLI'
      '   and dsr.Depart=:DEP'
      '   and dsr.Code=:CODE'
      '   and dsr.STATUS<>'#39'2'#39' '
      ''
      '  order by dsr.DOCDATE,dhr.DocTime asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 146
    Top = 254
    object quSelSpVID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object quSelSpVIDHEAD: TStringField
      FieldName = 'IDHEAD'
      Required = True
      Size = 19
    end
    object quSelSpVDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quSelSpVCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quSelSpVDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quSelSpVIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quSelSpVQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quSelSpVQUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
    end
    object quSelSpVQRem: TFloatField
      FieldName = 'QRem'
    end
    object quSelSpVPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object quSelSpVPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quSelSpVRPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quSelSpVRSUMIN0: TFloatField
      FieldName = 'RSUMIN0'
    end
    object quSelSpVRSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quSelSpVRSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object quSelSpVStatus: TStringField
      FieldName = 'Status'
    end
    object quSelSpVDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object quSelSpVDOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
  end
  object dsquSelSpV: TDataSource
    DataSet = quSelSpV
    Left = 145
    Top = 304
  end
  object ptSPredz: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_DOCSRET'
    Left = 268
    Top = 252
    object ptSPredzIDHEAD: TBCDField
      FieldName = 'IDHEAD'
      Required = True
      Precision = 19
      Size = 0
    end
    object ptSPredzID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptSPredzCODE: TIntegerField
      FieldName = 'CODE'
    end
    object ptSPredzDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object ptSPredzQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object ptSPredzPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object ptSPredzPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object ptSPredzRSUMIN0: TFloatField
      FieldName = 'RSUMIN0'
    end
    object ptSPredzRSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object ptSPredzRSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object ptSPredzRPrice: TFloatField
      FieldName = 'RPrice'
    end
    object ptSPredzIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object ptSPredzIDReason: TIntegerField
      FieldName = 'IDReason'
    end
    object ptSPredzQUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
    end
    object ptSPredzStatus: TStringField
      FieldName = 'Status'
    end
    object ptSPredzQRem: TFloatField
      FieldName = 'QRem'
    end
    object ptSPredzDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object ptSPredzDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
  end
  object quAllDepart: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  SELECT  de.ID,'
      '                 de.Name,'
      '                 de.Status1,'
      '                 de.Status2,'
      '                 de.Status3,'
      '                 de.Status4,'
      '                 de.Status5,'
      '                 de.ParentID,'
      '                 de.ObjectTypeID'
      '  from "Depart" as de'
      '  left join  "A_docsret" as dsr on dsr.depart=de.id'
      '  where '
      '  dsr.STATUS<>'#39'2'#39' '
      '  or '
      '  (Status1=9 '
      '  and de.Status4<>0'
      '  and de.ObjectTypeID=6)'
      ''
      '  group by de.ID,'
      '                 de.Name,'
      '                 de.Status1,'
      '                 de.Status2,'
      '                 de.Status3,'
      '                 de.Status4,'
      '                 de.Status5,'
      '                 de.ParentID,'
      '                 de.ObjectTypeID')
    Params = <>
    Left = 324
    Top = 253
    object quAllDepartID: TSmallintField
      FieldName = 'ID'
    end
    object quAllDepartName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quAllDepartStatus1: TSmallintField
      FieldName = 'Status1'
    end
    object quAllDepartStatus2: TSmallintField
      FieldName = 'Status2'
    end
    object quAllDepartStatus3: TSmallintField
      FieldName = 'Status3'
    end
    object quAllDepartStatus4: TIntegerField
      FieldName = 'Status4'
    end
    object quAllDepartStatus5: TIntegerField
      FieldName = 'Status5'
    end
    object quAllDepartParentID: TSmallintField
      FieldName = 'ParentID'
    end
    object quAllDepartObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
  end
  object dsquAllDepart: TDataSource
    DataSet = quAllDepart
    Left = 326
    Top = 305
  end
  object quAllRem: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        sum (dsr.QRem) as QRem   '
      ' '
      '   from  "A_DOCSRET" dsr '
      ''
      '   where  dsr.STATUS<>'#39'2'#39' '
      '   and dsr.Code=:CODE'
      ''
      '   group by dsr.Code')
    Params = <
      item
        DataType = ftFloat
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 680
    Top = 246
    object quAllRemCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quAllRemQRem: TFloatField
      FieldName = 'QRem'
    end
  end
  object dsquAllRem: TDataSource
    DataSet = quAllRem
    Left = 679
    Top = 296
  end
  object quDepRem: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        dsr.Depart,'
      '        sum (dsr.QRem) as QRem   '
      ' '
      '   from  "A_DOCSRET" dsr '
      ''
      '   where  dsr.STATUS<>'#39'2'#39' '
      '   and dsr.Code=:CODE'
      '  '
      '   group by dsr.Code,dsr.Depart'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 752
    Top = 246
    object quDepRemCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quDepRemDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quDepRemQRem: TFloatField
      FieldName = 'QRem'
    end
  end
  object dsquDepRem: TDataSource
    DataSet = quDepRem
    Left = 751
    Top = 296
  end
  object quObrPredz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        gd.Name as NameCode,'
      '        gd.GoodsGroupID,'
      '        gd.SubGroupID,'
      '        gd.FullName as FNameCode,'
      '        gd.BarCode, '
      '        gd.EdIzm,'
      '        gd.Reserv1 as QALLREM,'
      '        dsr.IDCLI,'
      '        v.Vendor,'
      '        v.Name as NameCli,'
      '        v.FullName as FNameCli,'
      '        cv.MOLVOZ,'
      '        cv.PHONEVOZ,'
      '        cv.EMAILVOZ,'
      '        dsr.Depart,'
      '        de.FullName as NameDep,'
      ''
      
        '         isnull (case when (gd.V14 =1) and (cv.IDTYPEVOZ=1) then' +
        ' '#39#1042#1086#1079#1074#1088#1072#1090#39' else'
      
        '         case when ((gd.V14 =2) or (gd.V14 =4)) and ((cv.IDTYPEV' +
        'OZ=1) or (cv.IDTYPEVOZ=2) or (cv.IDTYPEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39' else'
      
        '         case when (gd.V14 =3) or (cv.IDTYPEVOZ=3) then '#39#1053#1077' '#1087#1088#1077#1076 +
        #1091#1089#1084#1086#1090#1088#1077#1085#39' else'
      
        '         case when (gd.V14 =1) and ((cv.IDTYPEVOZ=2) or (cv.IDTY' +
        'PEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39'  end end end end,'#39#1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1105#1085#39') as TypeV' +
        'oz,   '
      ''
      '        sum (dsr.Quant) as Quant,'
      '        sum (dsr.QuantOut) as QuantOut,'
      '        sum (dsr.QRem) as QRem  '
      ''
      '   from  "A_DOCSRET" dsr '
      '   left join "Goods" as gd on gd.id =  dsr.Code'
      '   left join "RVendor" v  on v.Status=dsr.idCli'
      '   left join "A_CLIVOZ" cv on cv.IDCB=dsr.idCli'
      '   left join "Depart" as de on dsr.Depart=de.ID'
      ''
      '   where   dsr.STATUS<>'#39'2'#39' '
      ''
      '   group by dsr.Code, gd.Name, gd.FullName ,gd.BarCode, '
      
        '   gd.EdIzm,gd.Reserv1, dsr.IDCLI,v.Vendor,v.Name,v.FullName,cv.' +
        'MOLVOZ,'
      '   cv.PHONEVOZ,cv.EMAILVOZ,dsr.Depart,de.FullName,'
      '   gd.GoodsGroupID, gd.SubGroupID'
      '   ,gd.V14,cv.IDTYPEVOZ'
      ''
      '   order by v.Name '
      ''
      '')
    Params = <>
    Left = 395
    Top = 253
    object quObrPredzCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quObrPredzNameCode: TStringField
      FieldName = 'NameCode'
      Size = 30
    end
    object quObrPredzGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quObrPredzSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quObrPredzFNameCode: TStringField
      FieldName = 'FNameCode'
      Size = 60
    end
    object quObrPredzBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quObrPredzEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quObrPredzQALLREM: TFloatField
      FieldName = 'QALLREM'
    end
    object quObrPredzIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quObrPredzVendor: TSmallintField
      FieldName = 'Vendor'
    end
    object quObrPredzNameCli: TStringField
      FieldName = 'NameCli'
      Size = 30
    end
    object quObrPredzFNameCli: TStringField
      FieldName = 'FNameCli'
      Size = 100
    end
    object quObrPredzMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object quObrPredzPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object quObrPredzEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
    object quObrPredzDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quObrPredzNameDep: TStringField
      FieldName = 'NameDep'
      Size = 100
    end
    object quObrPredzQuant: TFloatField
      FieldName = 'Quant'
    end
    object quObrPredzQuantOut: TFloatField
      FieldName = 'QuantOut'
    end
    object quObrPredzQRem: TFloatField
      FieldName = 'QRem'
    end
    object quObrPredzTypeVoz: TStringField
      FieldName = 'TypeVoz'
      Size = 15
    end
  end
  object dsquObrPredz: TDataSource
    DataSet = quObrPredz
    Left = 403
    Top = 304
  end
  object ptHPredz: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_DOCHRET'
    Left = 268
    Top = 304
    object ptHPredzID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ptHPredzDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Required = True
      Size = 15
    end
    object ptHPredzDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object ptHPredzDOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
    object ptHPredzCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 250
    end
    object ptHPredzUSER: TStringField
      FieldName = 'USER'
      Required = True
      Size = 15
    end
    object ptHPredzSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 18
    end
  end
  object quSelCena: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select   gd.BarCode,  '
      '            dsr.PriceIn0,'
      '            dsr.PriceIn,'
      '             dsr.RPrice'
      ''
      '   from  "A_DOCSRET" dsr '
      ''
      '   left join "Goods" as gd on gd.id =  dsr.Code'
      ''
      '   where  dsr.idCli=:ICLI'
      '   and dsr.Depart=:DEP'
      '   and dsr.Code=:CODE'
      '   and dsr.STATUS<>'#39'2'#39' '
      '  '
      '   order by dsr.DOCDATE asc ')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 467
    Top = 252
    object quSelCenaPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quSelCenaRPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quSelCenaBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSelCenaPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
  end
  object dsquSelCena: TDataSource
    DataSet = quSelCena
    Left = 467
    Top = 304
  end
  object quObPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    tl.CodeTovar as Code,   '
      '    v.Status as idcli,'
      '    v.FullName as FNamePost,'
      '    v.Name as NamePost'
      ''
      '  from "TTNInLn" tl    '
      '    left outer join RVendor v on v.Vendor=tl."CodePost"    '
      '    left  join A_CLIVOZ cv on v.Status=cv.IDCB'
      ''
      '  where tl.CodeTovar=:IDCARD'
      '    and tl.DateInvoice>=:DDATEB'
      '    and tl.DateInvoice<=:DDATEE    '
      '    and cv.IDTYPEVOZ<3'
      '    '
      '  group by  tl.CodeTovar,'
      '                  v.Status,'
      '                  v.FullName,'
      '                  v.Name          '
      ''
      '  order by v.Name asc')
    Params = <
      item
        DataType = ftSmallint
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDATEB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DDATEE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 535
    Top = 252
    object quObPostCode: TIntegerField
      FieldName = 'Code'
    end
    object quObPostidcli: TSmallintField
      FieldName = 'idcli'
    end
    object quObPostFNamePost: TStringField
      FieldName = 'FNamePost'
      Size = 100
    end
    object quObPostNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
  end
  object dsquObPost: TDataSource
    DataSet = quObPost
    Left = 535
    Top = 303
  end
  object dsquFilVoz2: TDataSource
    DataSet = quFilVoz2
    Left = 87
    Top = 304
  end
  object quFilVoz2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        gd.Name as NameCode,'
      '        gd.FullName as FNameCode,'
      '        gd.BarCode, '
      '        gd.EdIzm,'
      '        gd.NDS,'
      '        v.Status as idvendor,'
      '        sum (dsr.Quant) as Quant,'
      '        sum (dsr.QuantOut) as QuantOut,'
      '        sum (dsr.QRem) as QRem,   '
      '        dsr.PRICEIN,'
      '        gd.Cena as RPrice,'
      '        sum (dsr.RSUMIN) as RSUMIN,   '
      '        sum (dsr.RsumNDS) as RsumNDS'
      ''
      ' '
      '   from  "A_DOCSRET" dsr '
      '   left join "Goods" as gd on gd.id =  dsr.Code'
      '   left outer join RVendor v on v.Status=dsr.idCli'
      ''
      '   where  v.Vendor=:ICLI'
      '   and dsr.Depart=:DEP'
      '   and dsr.STATUS<>'#39'2'#39' '
      '   and dsr.DateVoz<=:Date'
      ''
      
        '   group by dsr.Code, gd.Name, gd.FullName ,v.Status, gd.BarCode' +
        ', '
      '   gd.EdIzm, gd.NDS, dsr.PRICEIN,gd.Cena ')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'Date'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 88
    Top = 254
    object quFilVoz2CODE: TIntegerField
      FieldName = 'CODE'
    end
    object quFilVoz2NameCode: TStringField
      FieldName = 'NameCode'
      Size = 30
    end
    object quFilVoz2FNameCode: TStringField
      FieldName = 'FNameCode'
      Size = 60
    end
    object quFilVoz2BarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quFilVoz2EdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quFilVoz2NDS: TFloatField
      FieldName = 'NDS'
    end
    object quFilVoz2idvendor: TSmallintField
      FieldName = 'idvendor'
      Required = True
    end
    object quFilVoz2Quant: TFloatField
      FieldName = 'Quant'
    end
    object quFilVoz2QuantOut: TFloatField
      FieldName = 'QuantOut'
    end
    object quFilVoz2QRem: TFloatField
      FieldName = 'QRem'
    end
    object quFilVoz2PRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quFilVoz2RPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quFilVoz2RSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quFilVoz2RsumNDS: TFloatField
      FieldName = 'RsumNDS'
    end
  end
  object quDelayVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    OnCalcFields = quDelayVozCalcFields
    SQL.Strings = (
      'select '
      '    dsr.IDHead,'
      '    dsr.Code,'
      '    gd.FullName as NameCode,'
      '    dsr.Depart,'
      '    de.Name as DepartName,'
      '    dsr.idCli,'
      '    v.Name as Namecli,'
      '    v.FullName as FullNamecli,'
      '    (sum(dsr.Quant) )as Quant,'
      '    (sum(dsr.QuantOut) )as QuantOut,'
      '   ( sum(dsr.QRem) )as QRem,'
      '    dsr.DOCDATE,'
      '    dsr.DateVoz,'
      '    dhr."User",'
      '    dsr.IDReason,'
      '    cv.MOLVOZ,'
      '    cv.PHONEVOZ,'
      '    cv.EMAILVOZ,'
      '   dsr.PriceIn,'
      ''
      
        '   isnull (case when (gd.V14 =1) and (cv.IDTYPEVOZ=1) then '#39#1042#1086#1079#1074 +
        #1088#1072#1090#39' else'
      
        '             case when ((gd.V14 =2) or (gd.V14 =4)) and ((cv.IDT' +
        'YPEVOZ=1) or (cv.IDTYPEVOZ=2) or (cv.IDTYPEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39' ' +
        'else'
      
        '             case when (gd.V14 =3) or (cv.IDTYPEVOZ=3) then '#39#1053#1077' ' +
        #1087#1088#1077#1076#1091#1089#1084#1086#1090#1088#1077#1085#39' else'
      
        '             case when (gd.V14 =1) and ((cv.IDTYPEVOZ=2) or (cv.' +
        'IDTYPEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39'  end end end end,'#39#1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1105#1085#39') as T' +
        'ypeVoz,   '
      ''
      '   sum(dsr.QRem*dsr.PriceIn ) as RSUMIN'
      '   '
      '   from "A_DocSRet" dsr'
      '     left join "A_DocHRet" dhr  on dhr.ID=dsr.IDHead'
      '     left join "Goods" gd  on gd.ID=dsr.Code'
      '     left join "Depart" de  on de.ID=dsr.Depart'
      '     left join "RVendor" v  on v.Status=dsr.idCli'
      '     left join "A_CLIVOZ" cv on cv.IDCB=dsr.idCli'
      '    '
      '   where '
      '      dsr.DateVoz<:IDATE        '
      '      and    dsr.STATUS<>'#39'2'#39' '
      ''
      
        '    group by dsr.IDHead, dsr.Code,gd.FullName, dsr.Depart, de.Na' +
        'me,'
      
        '    dsr.idCli,v.Name,v.FullName ,dsr.DOCDATE, dsr.DateVoz, dhr."' +
        'User",'
      
        '    dsr.IDReason, cv.MOLVOZ, cv.PHONEVOZ, cv.EMAILVOZ,dsr.PriceI' +
        'n'
      '    ,gd.V14 ,cv.IDTYPEVOZ'
      ''
      '   order by gd.FullName asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 34
    Top = 484
    object quDelayVozIDHEAD: TStringField
      FieldName = 'IDHEAD'
      Required = True
      Size = 19
    end
    object quDelayVozCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quDelayVozNameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quDelayVozDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quDelayVozDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quDelayVozIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quDelayVozNamecli: TStringField
      FieldName = 'Namecli'
      Size = 30
    end
    object quDelayVozFullNamecli: TStringField
      FieldName = 'FullNamecli'
      Size = 100
    end
    object quDelayVozQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDelayVozQUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
    end
    object quDelayVozQRem: TFloatField
      FieldName = 'QRem'
    end
    object quDelayVozDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quDelayVozDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object quDelayVozUSER: TStringField
      FieldName = 'USER'
      Size = 15
    end
    object quDelayVozIDReason: TIntegerField
      FieldName = 'IDReason'
    end
    object quDelayVozMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object quDelayVozPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object quDelayVozEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
    object quDelayVozProcent: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Procent'
      Calculated = True
    end
    object quDelayVozDelay: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Delay'
      Calculated = True
    end
    object quDelayVozRSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quDelayVozPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quDelayVozTypeVoz: TStringField
      FieldName = 'TypeVoz'
      Size = 15
    end
  end
  object dsquDelayVoz: TDataSource
    DataSet = quDelayVoz
    Left = 68
    Top = 536
  end
  object quDocVoz2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  '
      'dhr."ID",'
      'dhr."DocNum",'
      'dhr."DocDate",'
      'dhr."DocTime",'
      'dhr."Status"'
      ''
      'from "A_DocHRet" dhr'
      ''
      'where dhr.DocDate>=:DATEB'
      'and dhr.DocDate<=:DATEE'
      'and dhr.status<>2'
      'order by dhr.ID asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'DATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 20
    Top = 356
    object quDocVoz2ID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object quDocVoz2DOCNUM: TStringField
      FieldName = 'DOCNUM'
      Required = True
      Size = 15
    end
    object quDocVoz2DOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quDocVoz2DOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
    object quDocVoz2STATUS: TStringField
      FieldName = 'STATUS'
      Size = 18
    end
  end
  object quSpecVoz2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    dsr.IDHead,'
      '    dsr.ID,'
      '    dsr.DOCDATE,    '
      '    dsr.Status'
      ''
      '   from "A_DocSRet" dsr   '
      ''
      '   where dsr.IDHead=:HEAD'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'HEAD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 90
    Top = 356
    object quSpecVoz2IDHEAD: TStringField
      FieldName = 'IDHEAD'
      Size = 19
    end
    object quSpecVoz2ID: TAutoIncField
      FieldName = 'ID'
    end
    object quSpecVoz2DOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quSpecVoz2Status: TStringField
      FieldName = 'Status'
    end
  end
  object dsquDocVoz2: TDataSource
    DataSet = quDocVoz2
    Left = 20
    Top = 408
  end
  object dsquSpecVoz2: TDataSource
    DataSet = quSpecVoz2
    Left = 92
    Top = 408
  end
  object quAllVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      '    dsr.IDHead,'
      '    dsr.Code,'
      '    gd.FullName as NameCode,'
      '    dsr.Depart,'
      '    de.Name as DepartName,'
      '    dsr.idCli,'
      '    v.Name as Namecli,'
      '    v.FullName as FullNamecli,'
      '    (sum(dsr.Quant) )as Quant,'
      '    (sum(dsr.QuantOut) )as QuantOut,'
      '   ( sum(dsr.QRem) )as QRem,'
      '    dsr.DOCDATE,'
      '    dsr.DateVoz,'
      '    dhr."User",'
      '    dsr.IDReason,'
      '    cv.MOLVOZ,'
      '    cv.PHONEVOZ,'
      '    cv.EMAILVOZ,'
      '   dsr.PriceIn,'
      ''
      
        '   isnull (case when (gd.V14 =1) and (cv.IDTYPEVOZ=1) then '#39#1042#1086#1079#1074 +
        #1088#1072#1090#39' else'
      
        '             case when ((gd.V14 =2) or (gd.V14 =4)) and ((cv.IDT' +
        'YPEVOZ=1) or (cv.IDTYPEVOZ=2) or (cv.IDTYPEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39' ' +
        'else'
      
        '             case when (gd.V14 =3) or (cv.IDTYPEVOZ=3) then '#39#1053#1077' ' +
        #1087#1088#1077#1076#1091#1089#1084#1086#1090#1088#1077#1085#39' else'
      
        '             case when (gd.V14 =1) and ((cv.IDTYPEVOZ=2) or (cv.' +
        'IDTYPEVOZ=4)) then '#39#1054#1073#1084#1077#1085#39'  end end end end,'#39#1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1105#1085#39') as T' +
        'ypeVoz,   '
      '   (sum(dsr.QRem*dsr.PriceIn )) as RSUMIN'
      ''
      '    from "A_DocSRet" dsr'
      '     left join "A_DocHRet" dhr  on dhr.ID=dsr.IDHead'
      '     left join "Goods" gd  on gd.ID=dsr.Code'
      '     left join "Depart" de  on de.ID=dsr.Depart'
      '     left join "RVendor" v  on v.Status=dsr.idCli'
      '     left join "A_CLIVOZ" cv on cv.IDCB=dsr.idCli'
      ''
      '   where   dsr.STATUS<'#39'2'#39' '
      ''
      
        '    group by dsr.IDHead, dsr.Code, gd.FullName, dsr.Depart, de.N' +
        'ame,'
      
        '    dsr.idCli,v.Name,v.FullName ,dsr.DOCDATE, dsr.DateVoz, dhr."' +
        'User",'
      
        '    dsr.IDReason, cv.MOLVOZ, cv.PHONEVOZ, cv.EMAILVOZ,dsr.PriceI' +
        'n'
      '    ,gd.V14,cv.IDTYPEVOZ'
      ''
      '   '
      '   order by gd.FullName asc')
    Params = <>
    Left = 106
    Top = 484
    object quAllVozIDHEAD: TStringField
      FieldName = 'IDHEAD'
      Required = True
      Size = 19
    end
    object quAllVozCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quAllVozNameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quAllVozDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quAllVozDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quAllVozIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quAllVozNamecli: TStringField
      FieldName = 'Namecli'
      Size = 30
    end
    object quAllVozFullNamecli: TStringField
      FieldName = 'FullNamecli'
      Size = 100
    end
    object quAllVozQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object quAllVozQuantOut: TFloatField
      FieldName = 'QuantOut'
      DisplayFormat = '0.000'
    end
    object quAllVozQRem: TFloatField
      FieldName = 'QRem'
      DisplayFormat = '0.000'
    end
    object quAllVozDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quAllVozDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
    object quAllVozUSER: TStringField
      FieldName = 'USER'
      Size = 15
    end
    object quAllVozIDReason: TIntegerField
      FieldName = 'IDReason'
    end
    object quAllVozMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object quAllVozPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object quAllVozEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
    object quAllVozPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object quAllVozRSUMIN: TFloatField
      FieldName = 'RSUMIN'
      DisplayFormat = '0.00'
    end
    object quAllVozProcent: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Procent'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object quAllVozDelay: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Delay'
      Calculated = True
    end
    object quAllVozTypeVoz: TStringField
      FieldName = 'TypeVoz'
      Size = 15
    end
  end
  object quCliNotVoz: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    Status  as idcli,'
      '    Name,'
      '    FullName'
      '    '
      '  from RVendor where status=2471 ')
    Params = <>
    Left = 805
    Top = 20
    object quCliNotVozidcli: TSmallintField
      FieldName = 'idcli'
    end
    object quCliNotVozName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCliNotVozFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
  end
  object dsquCliNotVoz: TDataSource
    DataSet = quCliNotVoz
    Left = 805
    Top = 76
  end
  object quAllRemCard: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    Reserv1 as QALLREM'
      '    from  Goods'
      ''
      '   where ID=:CODE'
      ' ')
    Params = <
      item
        DataType = ftInteger
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 178
    Top = 356
    object quAllRemCardQALLREM: TFloatField
      FieldName = 'QALLREM'
    end
  end
  object dsquAllRemCard: TDataSource
    DataSet = quAllRemCard
    Left = 180
    Top = 408
  end
  object PTSVOZ: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'Depart;DateInvoice;Number'
    TableName = 'TTNOutLn'
    Left = 674
    Top = 368
    object PTSVOZDepart: TSmallintField
      FieldName = 'Depart'
    end
    object PTSVOZDateInvoice: TDateField
      FieldName = 'DateInvoice'
    end
    object PTSVOZNumber: TStringField
      FieldName = 'Number'
      Size = 10
    end
    object PTSVOZCodeGroup: TSmallintField
      FieldName = 'CodeGroup'
    end
    object PTSVOZCodePodgr: TSmallintField
      FieldName = 'CodePodgr'
    end
    object PTSVOZCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
    end
    object PTSVOZCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
    end
    object PTSVOZTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object PTSVOZBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object PTSVOZOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object PTSVOZNDSProc: TFloatField
      FieldName = 'NDSProc'
    end
    object PTSVOZNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object PTSVOZOutNDSSum: TFloatField
      FieldName = 'OutNDSSum'
    end
    object PTSVOZAkciz: TFloatField
      FieldName = 'Akciz'
    end
    object PTSVOZKolMest: TIntegerField
      FieldName = 'KolMest'
    end
    object PTSVOZKolEdMest: TFloatField
      FieldName = 'KolEdMest'
    end
    object PTSVOZKolWithMest: TFloatField
      FieldName = 'KolWithMest'
    end
    object PTSVOZKol: TFloatField
      FieldName = 'Kol'
    end
    object PTSVOZCenaPost: TFloatField
      FieldName = 'CenaPost'
    end
    object PTSVOZCenaTovar: TFloatField
      FieldName = 'CenaTovar'
    end
    object PTSVOZCenaTovarSpis: TFloatField
      FieldName = 'CenaTovarSpis'
    end
    object PTSVOZSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
    end
    object PTSVOZSumCenaTovarSpis: TFloatField
      FieldName = 'SumCenaTovarSpis'
    end
    object PTSVOZCodeTara: TIntegerField
      FieldName = 'CodeTara'
    end
    object PTSVOZVesTara: TFloatField
      FieldName = 'VesTara'
    end
    object PTSVOZCenaTara: TFloatField
      FieldName = 'CenaTara'
    end
    object PTSVOZCenaTaraSpis: TFloatField
      FieldName = 'CenaTaraSpis'
    end
    object PTSVOZSumVesTara: TFloatField
      FieldName = 'SumVesTara'
    end
    object PTSVOZSumCenaTara: TFloatField
      FieldName = 'SumCenaTara'
    end
    object PTSVOZSumCenaTaraSpis: TFloatField
      FieldName = 'SumCenaTaraSpis'
    end
    object PTSVOZChekBuh: TBooleanField
      FieldName = 'ChekBuh'
    end
  end
  object quTypeCode: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select V14 as TypeVoz'
      '           '
      '   from Goods '
      ''
      '   where ID=:CODE')
    Params = <
      item
        DataType = ftInteger
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 736
    Top = 365
    object quTypeCodeTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
  end
  object dsquTypeCode: TDataSource
    DataSet = quTypeCode
    Left = 740
    Top = 416
  end
  object quCateg: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  '
      'ssg.Name,'
      'sg.Name as NameSg,  '
      'gr.Name as Namegr'
      'from "SubGroup" ssg'
      'left join "SubGroup" sg on sg.ID=ssg.GoodsGroupID'
      'left join "SubGroup" gr on gr.ID=sg.GoodsGroupID'
      'where ssg.ID=:idcode'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'idcode'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 896
    Top = 318
    object quCategName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCategNameSg: TStringField
      FieldName = 'NameSg'
      Size = 30
    end
    object quCategNamegr: TStringField
      FieldName = 'Namegr'
      Size = 30
    end
  end
  object PtProsroch: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'ID'
    TableName = 'A_DOCSRET'
    Left = 898
    Top = 364
    object BCDField1: TBCDField
      FieldName = 'IDHEAD'
      Required = True
      Precision = 19
      Size = 0
    end
    object AutoIncField1: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IntegerField1: TIntegerField
      FieldName = 'CODE'
    end
    object IntegerField2: TIntegerField
      FieldName = 'DEPART'
    end
    object FloatField1: TFloatField
      FieldName = 'QUANT'
    end
    object FloatField2: TFloatField
      FieldName = 'PRICEIN0'
    end
    object FloatField3: TFloatField
      FieldName = 'PRICEIN'
    end
    object FloatField4: TFloatField
      FieldName = 'RSUMIN0'
    end
    object FloatField5: TFloatField
      FieldName = 'RSUMIN'
    end
    object FloatField6: TFloatField
      FieldName = 'RSUMNDS'
    end
    object FloatField7: TFloatField
      FieldName = 'RPrice'
    end
    object IntegerField3: TIntegerField
      FieldName = 'IDCLI'
    end
    object IntegerField4: TIntegerField
      FieldName = 'IDReason'
    end
    object FloatField8: TFloatField
      FieldName = 'QUANTOUT'
    end
    object StringField1: TStringField
      FieldName = 'Status'
    end
    object FloatField9: TFloatField
      FieldName = 'QRem'
    end
    object IntegerField5: TIntegerField
      FieldName = 'DateVoz'
    end
    object IntegerField6: TIntegerField
      FieldName = 'DOCDATE'
    end
  end
  object PTCV: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexFieldNames = 'IDCB'
    TableName = 'A_CLIVOZ'
    Left = 794
    Top = 368
    object PTCVIDCB: TBCDField
      FieldName = 'IDCB'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 19
      Size = 0
    end
    object PTCVMOLVOZ: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object PTCVPHONEVOZ: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object PTCVEMAILVOZ: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
    object PTCVIDTYPEVOZ: TSmallintField
      FieldName = 'IDTYPEVOZ'
    end
    object PTCVSTBLOCK: TSmallintField
      FieldName = 'STBLOCK'
    end
  end
  object PtHist: TPvTable
    DatabaseName = '\\192.168.1.150\k$\SCrystal\data'
    IndexName = 'Idx_Doc'
    TableName = 'A_DOCHIST'
    Left = 604
    Top = 360
    object PtHistID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object PtHistTYPED: TSmallintField
      FieldName = 'TYPED'
      Required = True
    end
    object PtHistIDPERS: TIntegerField
      FieldName = 'IDPERS'
      Required = True
    end
    object PtHistDOCDATE: TDateField
      FieldName = 'DOCDATE'
    end
    object PtHistDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Size = 30
    end
    object PtHistDOCID: TIntegerField
      FieldName = 'DOCID'
    end
    object PtHistIOP: TStringField
      FieldName = 'IOP'
      Size = 10
    end
    object PtHistNAMEPERS: TStringField
      FieldName = 'NAMEPERS'
      Size = 30
    end
    object PtHistIDOP: TSmallintField
      FieldName = 'IDOP'
    end
  end
  object dsquSelSpV2: TDataSource
    DataSet = quSelSpV2
    Left = 209
    Top = 304
  end
  object quSelSpV2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '    dsr.IDHead,'
      '    dsr.ID,'
      '    dsr.DOCDATE,'
      '    dhr."DocTime",'
      '    dsr.Code,'
      '    dsr.Depart,'
      '    dsr.idCli,'
      '    dsr.Quant,'
      '    dsr.QuantOut,'
      '    dsr.QRem,'
      '    dsr.PriceIn0,'
      '    dsr.PriceIn,'
      '    dsr.RPrice,'
      '    dsr.RsumIn0,'
      '    dsr.RsumIn,'
      '    dsr.RsumNDS,'
      '    dsr.Status,'
      '    dsr.DateVoz'
      ''
      '   from  "A_DOCSRET" dsr '
      ''
      '   left outer join RVendor v on v.Status=dsr.idCli'
      '   left join "A_DocHRet" dhr  on dhr.ID=dsr.IDHead'
      ' '
      '  where  v.Vendor=:ICLI'
      '   and dsr.Depart=:DEP'
      '   and dsr.Code=:CODE'
      '   and dsr.STATUS<>'#39'2'#39' '
      ''
      '  order by dsr.DOCDATE,dhr.DocTime asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 210
    Top = 254
    object quSelSpV2IDHEAD: TStringField
      FieldName = 'IDHEAD'
      Size = 19
    end
    object quSelSpV2ID: TAutoIncField
      FieldName = 'ID'
    end
    object quSelSpV2DOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quSelSpV2DOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
    object quSelSpV2CODE: TIntegerField
      FieldName = 'CODE'
    end
    object quSelSpV2DEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quSelSpV2IDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quSelSpV2QUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quSelSpV2QUANTOUT: TFloatField
      FieldName = 'QUANTOUT'
    end
    object quSelSpV2QRem: TFloatField
      FieldName = 'QRem'
    end
    object quSelSpV2PRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object quSelSpV2PRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quSelSpV2RPrice: TFloatField
      FieldName = 'RPrice'
    end
    object quSelSpV2RSUMIN0: TFloatField
      FieldName = 'RSUMIN0'
    end
    object quSelSpV2RSUMIN: TFloatField
      FieldName = 'RSUMIN'
    end
    object quSelSpV2RSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object quSelSpV2Status: TStringField
      FieldName = 'Status'
    end
    object quSelSpV2DateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
  end
  object quGrafVozvrat2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      ' select  gv.ICLI,'
      '             gv.IDATEZ,'
      '             gv.IDATEV,'
      '             post.FullName,'
      '             post.Name,'
      '             cv.MOLVOZ,'
      '             cv.PHONEVOZ,'
      '             cv.EMAILVOZ,'
      '        --     dsr.Status'
      
        '           (select count(*) from  A_DOCSRET as dsr where gv.ICLI' +
        '=dsr.IDCLI and dsr.Status<2) as kolstr   '
      ''
      '  from "A_GRAFVOZ" gv'
      '  left join "RVendor" post on post.Status=gv.ICLI'
      '  left join A_CLIVOZ cv on gv.ICLI=cv.IDCB '
      '--  left join A_DOCSRET as dsr on gv.ICLI=dsr.IDCLI'
      '  '
      'where gv.IDATEV>=:IDATEB'
      '  and gv.IDATEV<=:IDATEE'
      '  and kolstr>0  '
      '  order by ICLI,IDATEZ asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDATEB'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'IDATEE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 392
    Top = 22
    object IntegerField7: TIntegerField
      FieldName = 'ICLI'
      Required = True
    end
    object IntegerField8: TIntegerField
      FieldName = 'IDATEZ'
      Required = True
    end
    object IntegerField9: TIntegerField
      FieldName = 'IDATEV'
      Required = True
    end
    object StringField2: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object StringField3: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'MOLVOZ'
      Size = 100
    end
    object StringField5: TStringField
      FieldName = 'PHONEVOZ'
      Size = 50
    end
    object StringField6: TStringField
      FieldName = 'EMAILVOZ'
      Size = 100
    end
  end
  object quAltPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  '
      '             alt.IDCLI,'
      '             alt.IDALTCLI,             '
      '             post.Name,'
      '             post.FullName,'
      '             alp.Name        AName,'
      '             alp.FullName  AFullName '
      ''
      '  from "A_AltCli"  alt'
      
        '  left join "RVendor" post on cast(abs(post.Status) as int)=alt.' +
        'IDCLI'
      
        '  left join "RVendor" alp on cast(abs(alp.Status) as int)=alt.ID' +
        'ALTCLI'
      '  order by post.Name asc')
    Params = <>
    Left = 265
    Top = 486
    object quAltPostIDCLI: TIntegerField
      FieldName = 'IDCLI'
      Required = True
    end
    object quAltPostIDALTCLI: TIntegerField
      FieldName = 'IDALTCLI'
      Required = True
    end
    object quAltPostName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quAltPostFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quAltPostAName: TStringField
      FieldName = 'AName'
      Size = 30
    end
    object quAltPostAFullName: TStringField
      FieldName = 'AFullName'
      Size = 100
    end
  end
  object dsquAltPost: TDataSource
    DataSet = quAltPost
    Left = 264
    Top = 536
  end
  object quTypeCli: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select'
      ''
      '  cv.IDTYPEVOZ as CliTypeV'
      ''
      '   from "A_CLIVOZ" cv'
      ''
      '   where   cv.IDCB=:ICLI')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 24
    Top = 124
    object quTypeCliCliTypeV: TSmallintField
      FieldName = 'CliTypeV'
    end
  end
  object dsquTypeCli: TDataSource
    DataSet = quTypeCli
    Left = 20
    Top = 175
  end
  object quNamePost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    v.FullName as FNamePost,'
      '    v.Name as NamePost'
      ''
      '     from RVendor v '
      ''
      '   where v.Status=:icli'
      '                  ')
    Params = <
      item
        DataType = ftInteger
        Name = 'icli'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 95
    Top = 123
    object quNamePostFNamePost: TStringField
      FieldName = 'FNamePost'
      Size = 100
    end
    object quNamePostNamePost: TStringField
      FieldName = 'NamePost'
      Size = 30
    end
  end
  object dsquNamePost: TDataSource
    DataSet = quNamePost
    Left = 95
    Top = 174
  end
  object qufacli: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select '
      '    alp.IDALTCLI as ID,'
      '    v.Name as Name'
      '  '
      '  from "A_ALTCLI"  alp    '
      '  left join RVendor v on v.Status=alp.IDALTCLI      '
      ''
      ' where alp.IDCLI=:ICLI'
      ''
      ' order by v.Name asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 600
    Top = 252
    object qufacliID: TIntegerField
      FieldName = 'ID'
    end
    object qufacliName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsqufacli: TDataSource
    DataSet = qufacli
    Left = 599
    Top = 303
  end
  object quAllCli: TPvQuery
    AutoCalcFields = False
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select  Status as ID,'
      '           Name'
      '     from "RVendor"'
      ''
      'order by Name asc')
    Params = <>
    LoadBlobOnOpen = False
    Left = 192
    Top = 124
    object quAllCliID: TSmallintField
      FieldName = 'ID'
      Required = True
    end
    object quAllCliName: TStringField
      FieldName = 'Name'
      Required = True
      Size = 50
    end
  end
  object dsquAllCli: TDataSource
    DataSet = quNamePost
    Left = 191
    Top = 174
  end
  object quNUM: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select max(ID) as ID  from "A_DocHRet"  ')
    Params = <>
    Left = 256
    Top = 124
    object quNUMID: TIntegerField
      FieldName = 'ID'
    end
  end
  object quSetMatrix: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    ParamCheck = False
    SQL.Strings = (
      
        'SELECT "Goods"."ID", "Goods"."Name", "Goods"."BarCode", "Goods".' +
        '"ShRealize", "Goods"."TovarType", '
      
        '"Goods"."EdIzm", "Goods"."NDS", "Goods"."OKDP", "Goods"."Weght",' +
        ' "Goods"."SrokReal", '
      
        '"Goods"."TareWeight", "Goods"."KritOst", "Goods"."BHTStatus", "G' +
        'oods"."UKMAction", "Goods"."DataChange", '
      
        '"Goods"."NSP", "Goods"."WasteRate", "Goods"."Cena", "Goods"."Kol' +
        '", "Goods"."FullName", '
      
        '"Goods"."Country", "Goods"."Status", "Goods"."Prsision", "Goods"' +
        '."PriceState", "Goods"."SetOfGoods", '
      
        '"Goods"."PrePacking", "Goods"."MargGroup", "Goods"."ObjectTypeID' +
        '", "Goods"."FixPrice", '
      
        '"Goods"."PrintLabel", "Goods"."ImageID", "Country"."Name" as Nam' +
        'eCountry, "A_BRANDS"."NAMEBRAND" ,'
      '"Goods"."GoodsGroupId","Goods"."SubGroupId",'
      '"Goods"."V01"'
      ',"Goods"."V02"'
      ',"Goods"."V03"'
      ',"Goods"."V04"'
      ',"Goods"."V05"'
      ',"Goods"."V06"'
      ',"Goods"."V07"'
      ',"Goods"."V08"'
      ',"Goods"."V09"'
      ',"Goods"."V10"'
      ',"Goods"."V11"'
      ',"Goods"."V12"'
      ',"Goods"."V14"'
      
        ',"Goods"."Reserv1","Goods"."Reserv2","Goods"."Reserv3","Goods"."' +
        'Reserv4","Goods"."Reserv5"'
      ',"A_MAKER"."NAMEM"'
      ',"A_TYPEVOZ"."TYPEVOZ"'
      'FROM "Goods"'
      'left join "A_BRANDS" on "A_BRANDS"."ID" = "Goods"."V01"'
      'left join "Country" on "Country"."ID" = "Goods"."Country"'
      'left join "A_MAKER" on "A_MAKER"."ID" = "Goods"."V12"'
      'left join "A_TYPEVOZ" on "A_TYPEVOZ"."ID" = "Goods"."V14"'
      'where "Goods"."ID"=56573 or "Goods"."ID"=71801 '
      '')
    Params = <>
    LoadBlobOnOpen = False
    Left = 360
    Top = 488
    object quSetMatrixID: TIntegerField
      FieldName = 'ID'
    end
    object quSetMatrixName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quSetMatrixBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quSetMatrixShRealize: TSmallintField
      FieldName = 'ShRealize'
    end
    object quSetMatrixTovarType: TSmallintField
      FieldName = 'TovarType'
    end
    object quSetMatrixEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quSetMatrixNDS: TFloatField
      FieldName = 'NDS'
    end
    object quSetMatrixOKDP: TFloatField
      FieldName = 'OKDP'
    end
    object quSetMatrixWeght: TFloatField
      FieldName = 'Weght'
    end
    object quSetMatrixSrokReal: TSmallintField
      FieldName = 'SrokReal'
    end
    object quSetMatrixTareWeight: TSmallintField
      FieldName = 'TareWeight'
    end
    object quSetMatrixKritOst: TFloatField
      FieldName = 'KritOst'
    end
    object quSetMatrixBHTStatus: TSmallintField
      FieldName = 'BHTStatus'
    end
    object quSetMatrixUKMAction: TSmallintField
      FieldName = 'UKMAction'
    end
    object quSetMatrixDataChange: TDateField
      FieldName = 'DataChange'
    end
    object quSetMatrixNSP: TFloatField
      FieldName = 'NSP'
    end
    object quSetMatrixWasteRate: TFloatField
      FieldName = 'WasteRate'
    end
    object quSetMatrixCena: TFloatField
      FieldName = 'Cena'
      DisplayFormat = '0.00'
    end
    object quSetMatrixKol: TFloatField
      FieldName = 'Kol'
      DisplayFormat = '0.0'
    end
    object quSetMatrixFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quSetMatrixCountry: TSmallintField
      FieldName = 'Country'
    end
    object quSetMatrixStatus: TSmallintField
      FieldName = 'Status'
    end
    object quSetMatrixPrsision: TFloatField
      FieldName = 'Prsision'
    end
    object quSetMatrixPriceState: TWordField
      FieldName = 'PriceState'
    end
    object quSetMatrixSetOfGoods: TWordField
      FieldName = 'SetOfGoods'
    end
    object quSetMatrixPrePacking: TStringField
      FieldName = 'PrePacking'
      Size = 5
    end
    object quSetMatrixMargGroup: TStringField
      FieldName = 'MargGroup'
      Size = 2
    end
    object quSetMatrixObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
    object quSetMatrixFixPrice: TFloatField
      FieldName = 'FixPrice'
      DisplayFormat = '0.00'
    end
    object quSetMatrixPrintLabel: TWordField
      FieldName = 'PrintLabel'
    end
    object quSetMatrixImageID: TIntegerField
      FieldName = 'ImageID'
    end
    object quSetMatrixNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
    object quSetMatrixNameCountry: TStringField
      FieldName = 'NameCountry'
      Size = 30
    end
    object quSetMatrixGoodsGroupID: TSmallintField
      FieldName = 'GoodsGroupID'
    end
    object quSetMatrixSubGroupID: TSmallintField
      FieldName = 'SubGroupID'
    end
    object quSetMatrixV01: TIntegerField
      FieldName = 'V01'
    end
    object quSetMatrixV02: TIntegerField
      FieldName = 'V02'
    end
    object quSetMatrixV03: TIntegerField
      FieldName = 'V03'
    end
    object quSetMatrixV04: TIntegerField
      FieldName = 'V04'
    end
    object quSetMatrixV05: TIntegerField
      FieldName = 'V05'
    end
    object quSetMatrixReserv1: TFloatField
      FieldName = 'Reserv1'
      DisplayFormat = '0.000'
    end
    object quSetMatrixReserv2: TFloatField
      FieldName = 'Reserv2'
      DisplayFormat = '0.0'
    end
    object quSetMatrixReserv3: TFloatField
      FieldName = 'Reserv3'
    end
    object quSetMatrixReserv4: TFloatField
      FieldName = 'Reserv4'
    end
    object quSetMatrixReserv5: TFloatField
      FieldName = 'Reserv5'
    end
    object quSetMatrixV06: TIntegerField
      FieldName = 'V06'
      DisplayFormat = '0'
    end
    object quSetMatrixV07: TIntegerField
      FieldName = 'V07'
      DisplayFormat = '0'
    end
    object quSetMatrixV08: TIntegerField
      FieldName = 'V08'
    end
    object quSetMatrixV09: TIntegerField
      FieldName = 'V09'
    end
    object quSetMatrixV10: TIntegerField
      FieldName = 'V10'
    end
    object quSetMatrixVol: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Vol'
      Calculated = True
    end
    object quSetMatrixV11: TIntegerField
      FieldName = 'V11'
    end
    object quSetMatrixV12: TIntegerField
      FieldName = 'V12'
    end
    object quSetMatrixNAMEM: TStringField
      FieldName = 'NAMEM'
      Size = 200
    end
    object quSetMatrixV14: TIntegerField
      FieldName = 'V14'
    end
    object quSetMatrixTYPEVOZ: TStringField
      FieldName = 'TYPEVOZ'
      Size = 100
    end
    object quSetMatrixQremn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Qremn'
      LookupDataSet = quAllRem
      LookupKeyFields = 'QRem'
      LookupResultField = 'QRem'
      EditFormat = '0.000'
      Calculated = True
    end
  end
  object dsquSetMatrix: TDataSource
    DataSet = quSetMatrix
    Left = 360
    Top = 536
  end
  object quPredzCli: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'Select count(*)  as kol'
      'from  A_DOCSRET '
      'where IDCLI=:ICLI '
      '        and Status<2   '
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'ICLI'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 440
    Top = 486
    object quPredzClikol: TIntegerField
      FieldName = 'kol'
    end
  end
  object quInventV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '        dsr.Code,'
      '        gd.FullName as FNameCode,'
      '        gd.BarCode, '
      '        gd.EdIzm,'
      '        gd.V14 as TypeVoz,'
      '        sum (dsr.QRem) as QRem  '
      ''
      '   from  "A_DOCSRET" dsr '
      '   left join "Goods" as gd on gd.id =  dsr.Code'
      ''
      '   where  dsr.STATUS<>'#39'2'#39' '
      '   and dsr.Depart=:IDEP'
      ''
      '   group by dsr.Code, gd.FullName ,gd.BarCode, '
      '   gd.EdIzm, gd.V14,gd.Reserv1')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 555
    Top = 482
    object quInventVCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quInventVFNameCode: TStringField
      FieldName = 'FNameCode'
      Size = 60
    end
    object quInventVBarCode: TStringField
      FieldName = 'BarCode'
      Size = 13
    end
    object quInventVEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quInventVQRem: TFloatField
      FieldName = 'QRem'
    end
    object quInventVTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
  end
  object dsquInventV: TDataSource
    DataSet = quInventV
    Left = 555
    Top = 533
  end
  object quNameCode: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select     '
      '           FullName as NameCode,'
      '           EdIzm,'
      '           V14 as TypeVoz'
      ''
      '   from  Goods'
      ''
      '   where ID=:IDCARD        '
      ' ')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 688
    Top = 477
    object quNameCodeNameCode: TStringField
      FieldName = 'NameCode'
      Size = 60
    end
    object quNameCodeEdIzm: TSmallintField
      FieldName = 'EdIzm'
    end
    object quNameCodeTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
  end
  object quQPost: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select '
      '    dsr.Code,'
      '    dsr.Depart,'
      '    dsr.idCli,'
      '    v.FullName as FullNamecli,'
      '   ( sum(dsr.QRem) ) as Quant'
      ''
      '    from "A_DocSRet" dsr'
      '     left join "RVendor" v  on v.Status=dsr.idCli'
      ''
      '   where   dsr.STATUS<>'#39'2'#39' '
      '   and dsr.Depart=:IDEP'
      '   and dsr.Code=:CODE'
      ''
      '   group by dsr.Code,dsr.Depart, dsr.idCli,v.FullName '
      '   '
      '   order by v.FullName asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 618
    Top = 477
    object quQPostCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quQPostDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quQPostIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quQPostFullNamecli: TStringField
      FieldName = 'FullNamecli'
      Size = 100
    end
    object quQPostQuant: TFloatField
      FieldName = 'Quant'
    end
  end
  object dsquQPost: TDataSource
    DataSet = quQPost
    Left = 616
    Top = 525
  end
  object quPredzV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select'
      '    dsr.IDHead,'
      '    dsr.ID,'
      '    dsr.DOCDATE,'
      '    dhr."DocTime",'
      '    dsr.Code,'
      '    dsr.Depart,'
      '    dsr.idCli,'
      '    dsr.Status,'
      '    dsr.DateVoz'
      ''
      '   from  "A_DOCSRET" dsr '
      ''
      '   left join "A_DocHRet" dhr  on dhr.ID=dsr.IDHead'
      ' '
      '  where  dsr.Depart=:DEP'
      '   and dsr.Code=:CODE'
      '   and dsr.STATUS<>'#39'2'#39' '
      ''
      
        '  group by dsr.IDHead, dsr.ID, dsr.DOCDATE, dhr."DocTime", dsr.C' +
        'ode,  dsr.Depart,'
      '                 dsr.idCli,    dsr.Status,    dsr.DateVoz'
      ''
      '   order by dsr.DOCDATE,dhr.DocTime asc')
    Params = <
      item
        DataType = ftInteger
        Name = 'DEP'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'CODE'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 746
    Top = 478
    object quPredzVIDHEAD: TStringField
      FieldName = 'IDHEAD'
      Required = True
      Size = 19
    end
    object quPredzVID: TAutoIncField
      FieldName = 'ID'
      Required = True
    end
    object quPredzVDOCDATE: TIntegerField
      FieldName = 'DOCDATE'
    end
    object quPredzVDOCTIME: TStringField
      FieldName = 'DOCTIME'
      Size = 8
    end
    object quPredzVCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quPredzVDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quPredzVIDCLI: TIntegerField
      FieldName = 'IDCLI'
    end
    object quPredzVStatus: TStringField
      FieldName = 'Status'
    end
    object quPredzVDateVoz: TIntegerField
      FieldName = 'DateVoz'
    end
  end
  object quPV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select top 1'
      '            tl.Depart,'
      '            tl.CodePost,'
      '            v.Status as idcli,'
      '            v.Status, '
      '            cv.IDTYPEVOZ as CliTypeV,'
      '            tl.CodeTovar as Code,'
      '            gd.V14 as TypeVoz,'
      '            tl.SumCenaTovarPost as SumPost,'
      '            tl.NDSSum,'
      '            tl.CenaTovar as PriceIn,'
      '            tl.NewCenaTovar as Rprice,'
      '            alp.IDALTCLI,'
      '            tl.kol'
      ''
      '   from "TTNInLn" tl'
      '   left  join RVendor v on v.Vendor=tl.CodePost   '
      '   left  join A_CLIVOZ cv on v.Status=cv.IDCB'
      '   left  join Goods gd on gd.ID=tl.CodeTovar'
      '   left  join A_ALTCLI alp on  v.Status=alp.IDCLI'
      ''
      '   where tl.CodeTovar=:IDCARD'
      '            and v.Status>0'
      ''
      '  order by tl.DateInvoice DESC')
    Params = <
      item
        DataType = ftInteger
        Name = 'IDCARD'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 752
    Top = 533
    object quPVDepart: TSmallintField
      FieldName = 'Depart'
    end
    object quPVCodePost: TSmallintField
      FieldName = 'CodePost'
    end
    object quPVidcli: TSmallintField
      FieldName = 'idcli'
      Required = True
    end
    object quPVStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object quPVCliTypeV: TSmallintField
      FieldName = 'CliTypeV'
    end
    object quPVCode: TIntegerField
      FieldName = 'Code'
    end
    object quPVTypeVoz: TIntegerField
      FieldName = 'TypeVoz'
    end
    object quPVPriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object quPVIDALTCLI: TIntegerField
      FieldName = 'IDALTCLI'
    end
    object quPVRprice: TFloatField
      FieldName = 'Rprice'
    end
    object quPVSumPost: TFloatField
      FieldName = 'SumPost'
    end
    object quPVNDSSum: TFloatField
      FieldName = 'NDSSum'
    end
    object quPVKol: TFloatField
      FieldName = 'Kol'
    end
    object quPVPriceIn0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PriceIn0'
      Calculated = True
    end
  end
  object quDInvV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  Distinct Number,Date from "A_INVENTVOZ"'
      '  where Date Between :DDateB and :DDateE'
      '  order by Date DESC')
    Params = <
      item
        DataType = ftDate
        Name = 'DDateB'
        ParamType = ptUnknown
        Value = 0d
      end
      item
        DataType = ftDate
        Name = 'DDateE'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 816
    Top = 477
    object quDInvVNumber: TIntegerField
      FieldName = 'Number'
    end
    object quDInvVDate: TDateField
      FieldName = 'Date'
    end
  end
  object dsquDInvV: TDataSource
    DataSet = quDInvV
    Left = 816
    Top = 533
  end
  object quSpInvV: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  select  inv.Code,'
      '             gd.FullName,'
      '             inv. Depart,'
      '             de.Name as DepartName,'
      '             inv.QPREDZ,'
      '             inv.QFACT,'
      '             inv.QRAZN,'
      '             inv.QOTHERCLI,'
      '             inv.NEWCLI,'
      '             v.FullName as Namecli'
      ''
      '  from "A_INVENTVOZ"  inv'
      '     left join "Goods" gd  on gd.ID=inv.Code'
      '     left join "Depart" de  on de.ID=inv.Depart'
      '     left join "RVendor" v  on v.Status=inv.NEWCLI'
      ''
      '  where  inv.Number=:Num '
      '            and inv.Date=:DDate'
      '  ')
    Params = <
      item
        DataType = ftInteger
        Name = 'Num'
        ParamType = ptUnknown
        Value = 0
      end
      item
        DataType = ftDateTime
        Name = 'DDate'
        ParamType = ptUnknown
        Value = 0d
      end>
    Left = 560
    Top = 597
    object quSpInvVCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object quSpInvVFullName: TStringField
      FieldName = 'FullName'
      Size = 60
    end
    object quSpInvVDepart: TIntegerField
      FieldName = 'Depart'
      Required = True
    end
    object quSpInvVDepartName: TStringField
      FieldName = 'DepartName'
      Size = 30
    end
    object quSpInvVQPREDZ: TFloatField
      FieldName = 'QPREDZ'
    end
    object quSpInvVQFACT: TFloatField
      FieldName = 'QFACT'
    end
    object quSpInvVQRAZN: TFloatField
      FieldName = 'QRAZN'
    end
    object quSpInvVQOTHERCLI: TFloatField
      FieldName = 'QOTHERCLI'
    end
    object quSpInvVNEWCLI: TIntegerField
      FieldName = 'NEWCLI'
    end
    object quSpInvVNamecli: TStringField
      FieldName = 'Namecli'
      Size = 100
    end
  end
  object dsquSpInvV: TDataSource
    DataSet = quSpInvV
    Left = 560
    Top = 653
  end
  object quDepVoz2: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  SELECT '
      '                 ID,'
      '                 Name,'
      '                 Status1,'
      '                 ObjectTypeID '
      '  '
      '  FROM "Depart"'
      '  Where ObjectTypeID=6 and Status1=9 and Status4<>0'
      '  Order by Name')
    Params = <>
    Left = 636
    Top = 597
    object quDepVoz2ID: TSmallintField
      FieldName = 'ID'
    end
    object quDepVoz2Name: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quDepVoz2Status1: TSmallintField
      FieldName = 'Status1'
    end
    object quDepVoz2ObjectTypeID: TSmallintField
      FieldName = 'ObjectTypeID'
    end
  end
  object dsquDepVoz2: TDataSource
    DataSet = quDepVoz2
    Left = 636
    Top = 648
  end
  object quNameLCli: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      '  SELECT Name'
      ' '
      '  FROM "RVendor"'
      '  Where Status=:idcli')
    Params = <
      item
        DataType = ftInteger
        Name = 'idcli'
        ParamType = ptUnknown
        Value = 0
      end>
    Left = 716
    Top = 597
    object quNameLCliName: TStringField
      FieldName = 'Name'
      Size = 30
    end
  end
  object dsquNameLCli: TDataSource
    DataSet = quNameLCli
    Left = 716
    Top = 648
  end
end
