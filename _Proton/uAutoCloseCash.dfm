object fmMainAutoCloseCash: TfmMainAutoCloseCash
  Left = 450
  Top = 126
  Width = 509
  Height = 616
  Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1086#1077' '#1089#1085#1103#1090#1080#1077' '#1082#1072#1089#1089
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit1: TcxDateEdit
    Left = 24
    Top = 16
    TabOrder = 0
    Width = 145
  end
  object cxButton1: TcxButton
    Left = 304
    Top = 16
    Width = 97
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 296
    Top = 48
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    State = cbsChecked
    TabOrder = 2
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 120
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 439
    Width = 493
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 559
    Width = 493
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Text = '13.06'
        Width = 300
      end>
  end
  object cxCheckBox2: TcxCheckBox
    Left = 24
    Top = 44
    Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1087#1086' '#1086#1090#1076#1077#1083#1072#1084
    State = cbsChecked
    TabOrder = 5
    Width = 145
  end
  object cxCheckBox3: TcxCheckBox
    Left = 24
    Top = 60
    Caption = #1073#1077#1079' '#1091#1076#1072#1083#1077#1085#1080#1103' '#1089#1090#1072#1088#1099#1093' '#1076#1072#1085#1085#1099#1093
    TabOrder = 6
    Width = 209
  end
  object cxProgressBar1: TcxProgressBar
    Left = 24
    Top = 92
    ParentColor = False
    Properties.BarStyle = cxbsGradient
    Properties.BeginColor = clGreen
    Properties.EndColor = 9830293
    Style.BorderStyle = ebsOffice11
    Style.Color = clWhite
    Style.Shadow = True
    TabOrder = 7
    Width = 225
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 24
    Top = 156
  end
  object am1: TActionManager
    Left = 320
    Top = 108
    StyleName = 'XP Style'
    object acStart: TAction
      Caption = 'acStart'
      OnExecute = acStartExecute
    end
    object acCashLoad: TAction
      Caption = 'acCashLoad'
      ShortCut = 49228
      OnExecute = acCashLoadExecute
    end
    object acRePutCh: TAction
      Caption = 'acRePutCh'
      ShortCut = 49234
      OnExecute = acRePutChExecute
    end
  end
  object IdSMTP: TIdSMTP
    Intercept = IdLogFile
    MaxLineAction = maException
    ReadTimeout = 0
    Host = 'mail.softur.ru'
    Port = 25
    AuthenticationType = atLogin
    Password = '314159314159'
    Username = 'info@softur.ru'
    Left = 60
    Top = 192
  end
  object IdLogFile: TIdLogFile
    Filename = 'MailSend.Log'
    Left = 112
    Top = 192
  end
  object IdMessage: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    Recipients = <>
    ReplyTo = <>
    Left = 164
    Top = 192
  end
end
