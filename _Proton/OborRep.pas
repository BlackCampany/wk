unit OborRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, ExtCtrls,
  ComCtrls, cxProgressBar, SpeedBar, DB, dxmdaset, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfmOborRep = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Memo1: TcxMemo;
    SpeedBar1: TSpeedBar;
    PBar1: TcxProgressBar;
    teObor: TdxMemData;
    teOborNum: TIntegerField;
    teOborsPer: TStringField;
    teOborDep1: TFloatField;
    teOborDep2: TFloatField;
    teOborDep3: TFloatField;
    teOborDep4: TFloatField;
    teOborDep5: TFloatField;
    teOborDep6: TFloatField;
    teOborDep7: TFloatField;
    teOborDep8: TFloatField;
    teOborDep9: TFloatField;
    teOborDep10: TFloatField;
    teOborItog: TFloatField;
    dsteObor: TDataSource;
    ViewObor: TcxGridDBTableView;
    LevelObor: TcxGridLevel;
    GridObor: TcxGrid;
    ViewOborNum: TcxGridDBColumn;
    ViewOborsPer: TcxGridDBColumn;
    ViewOborDep1: TcxGridDBColumn;
    ViewOborDep2: TcxGridDBColumn;
    ViewOborDep3: TcxGridDBColumn;
    ViewOborDep4: TcxGridDBColumn;
    ViewOborDep5: TcxGridDBColumn;
    ViewOborDep6: TcxGridDBColumn;
    ViewOborDep7: TcxGridDBColumn;
    ViewOborDep8: TcxGridDBColumn;
    ViewOborDep9: TcxGridDBColumn;
    ViewOborDep10: TcxGridDBColumn;
    ViewOborItog: TcxGridDBColumn;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOborRep: TfmOborRep;

implementation

uses Un1;

{$R *.dfm}

procedure TfmOborRep.FormCreate(Sender: TObject);
begin
  GridObor.Align:=AlClient;
end;

procedure TfmOborRep.SpeedItem2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmOborRep.SpeedItem1Click(Sender: TObject);
begin
 //������� � Excel
  prNExportExel5(ViewObor);
end;

end.
