unit CashDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Placemnt, SpeedBar, ExtCtrls, cxImageComboBox;

type
  TfmCashDep = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    FormPlacement1: TFormPlacement;
    GridCDep: TcxGrid;
    ViewCDep: TcxGridDBTableView;
    LevelCDep: TcxGridLevel;
    ViewCDepRecId: TcxGridDBColumn;
    ViewCDepValT: TcxGridDBColumn;
    ViewCDepiDep: TcxGridDBColumn;
    ViewCDepsDep: TcxGridDBColumn;
    ViewCDepDateSale: TcxGridDBColumn;
    ViewCDepCashNum: TcxGridDBColumn;
    ViewCDepCassir: TcxGridDBColumn;
    ViewCDepRSum: TcxGridDBColumn;
    ViewCDepRSumD: TcxGridDBColumn;
    ViewCDepRSumN10: TcxGridDBColumn;
    ViewCDepRSumN20: TcxGridDBColumn;
    ViewCDeprSumN0: TcxGridDBColumn;
    ViewCDepRSumD10: TcxGridDBColumn;
    ViewCDepRSumD20: TcxGridDBColumn;
    ViewCDepRSumD0: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashDep: TfmCashDep;

implementation

uses Un1, MDB;

{$R *.dfm}

procedure TfmCashDep.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridCDep.Align:=AlClient;
end;

procedure TfmCashDep.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCashDep.SpeedItem3Click(Sender: TObject);
begin
  //������� � ������
  prNExportExel5(ViewCDep);
end;

procedure TfmCashDep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  fmCashDep.Release;
end;

end.
