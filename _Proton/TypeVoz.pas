unit TypeVoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons;

type
  TfmTypeVoz = class(TForm)
    GridTypeV: TcxGrid;
    ViewTypeV: TcxGridDBTableView;
    ViewTypeVID: TcxGridDBColumn;
    ViewTypeVReason: TcxGridDBColumn;
    LevelTypeV: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTypeVoz: TfmTypeVoz;

implementation
uses Un1;
{$R *.dfm}

procedure TfmTypeVoz.FormCreate(Sender: TObject);
begin
  GridTypeV.Align:=AlClient;
  ViewTypeV.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmTypeVoz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTypeV.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmTypeVoz.cxButton1Click(Sender: TObject);
begin
 close;
end;

end.
