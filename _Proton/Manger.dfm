object fmManager: TfmManager
  Left = 306
  Top = 201
  Width = 467
  Height = 640
  Caption = #1052#1077#1085#1077#1076#1078#1077#1088
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit1: TcxDateEdit
    Left = 24
    Top = 16
    TabOrder = 0
    Width = 145
  end
  object cxButton1: TcxButton
    Left = 304
    Top = 16
    Width = 97
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 296
    Top = 48
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    State = cbsChecked
    TabOrder = 2
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 4
    Top = 76
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 497
    Width = 453
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 587
    Width = 459
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 300
      end>
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 28
    Top = 108
  end
  object am1: TActionManager
    Left = 400
    Top = 92
    StyleName = 'XP Style'
    object acStart: TAction
      OnExecute = acStartExecute
    end
  end
  object teAC: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 140
    Top = 232
    object teACCode: TIntegerField
      FieldName = 'Code'
    end
    object teACQuant: TFloatField
      FieldName = 'Quant'
    end
    object teACAPrice: TFloatField
      FieldName = 'APrice'
    end
    object teACOldPrice: TFloatField
      FieldName = 'OldPrice'
    end
    object teACNewPrice: TFloatField
      FieldName = 'NewPrice'
    end
    object teACiDep: TIntegerField
      FieldName = 'iDep'
    end
    object teACDateB: TIntegerField
      FieldName = 'DateB'
    end
  end
  object PvSQL: TPvSqlDatabase
    AliasName = 'SCRYSTAL'
    DatabaseName = 'PSQL'
    SessionName = 'PvSqlDefault'
    Left = 228
    Top = 92
  end
  object quA: TPvQuery
    CachedUpdates = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "A_AKCIYA"'
      'where StatusOn=0 or StatusOf=0'
      'order by DateB,DateE,AType desc,Code')
    Params = <>
    Left = 288
    Top = 92
    object quACode: TIntegerField
      FieldName = 'Code'
    end
    object quAAType: TSmallintField
      FieldName = 'AType'
    end
    object quADateB: TDateField
      FieldName = 'DateB'
    end
    object quADateE: TDateField
      FieldName = 'DateE'
    end
    object quAPrice: TFloatField
      FieldName = 'Price'
    end
    object quADStop: TSmallintField
      FieldName = 'DStop'
    end
    object quADProc: TFloatField
      FieldName = 'DProc'
    end
    object quAStatusOn: TSmallintField
      FieldName = 'StatusOn'
    end
    object quAStatusOf: TSmallintField
      FieldName = 'StatusOf'
    end
    object quAOldPrice: TFloatField
      FieldName = 'OldPrice'
    end
    object quAOldDStop: TSmallintField
      FieldName = 'OldDStop'
    end
    object quAOldDProc: TFloatField
      FieldName = 'OldDProc'
    end
  end
  object ptAkciya: TPvTable
    AutoRefresh = True
    DatabaseName = '\\btr1\k$\SCrystal\data'
    IndexFieldNames = 'Code;AType;DateB;DateE'
    TableName = 'A_AKCIYA'
    Left = 340
    Top = 92
    object ptAkciyaCode: TIntegerField
      FieldName = 'Code'
      Required = True
    end
    object ptAkciyaAType: TSmallintField
      FieldName = 'AType'
      Required = True
    end
    object ptAkciyaDateB: TDateField
      FieldName = 'DateB'
      Required = True
    end
    object ptAkciyaDateE: TDateField
      FieldName = 'DateE'
      Required = True
    end
    object ptAkciyaPrice: TFloatField
      FieldName = 'Price'
    end
    object ptAkciyaDStop: TSmallintField
      FieldName = 'DStop'
    end
    object ptAkciyaDProc: TFloatField
      FieldName = 'DProc'
    end
    object ptAkciyaStatusOn: TSmallintField
      FieldName = 'StatusOn'
    end
    object ptAkciyaStatusOf: TSmallintField
      FieldName = 'StatusOf'
    end
    object ptAkciyaOldPrice: TFloatField
      FieldName = 'OldPrice'
    end
    object ptAkciyaOldDStop: TSmallintField
      FieldName = 'OldDStop'
    end
    object ptAkciyaOldDProc: TFloatField
      FieldName = 'OldDProc'
    end
  end
  object quAdd: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 32
    Top = 172
  end
  object quM: TPvQuery
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select max(ID) as iMax from "TTnIn"')
    Params = <>
    Left = 32
    Top = 228
    object quMiMax: TIntegerField
      FieldName = 'iMax'
    end
  end
  object quE: TPvQuery
    DatabaseName = 'PSQL'
    Params = <>
    Left = 88
    Top = 176
  end
  object teAD: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 188
    Top = 232
    object teADCode: TIntegerField
      FieldName = 'Code'
    end
    object teADDStopNew: TFloatField
      FieldName = 'DStopNew'
    end
    object teADDStopOld: TFloatField
      FieldName = 'DStopOld'
    end
    object teADDateB: TIntegerField
      FieldName = 'DateB'
    end
  end
  object teACat: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 244
    Top = 232
    object teACatCode: TIntegerField
      FieldName = 'Code'
    end
    object teACatCatNew: TIntegerField
      FieldName = 'CatNew'
    end
    object teACatCatOld: TIntegerField
      FieldName = 'CatOld'
    end
    object teACatDateB: TIntegerField
      FieldName = 'DateB'
    end
  end
  object teNac: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 304
    Top = 232
    object teNacCode: TIntegerField
      FieldName = 'Code'
    end
    object teNacNewDMin: TFloatField
      FieldName = 'NewDMin'
    end
    object teNacNewNac: TFloatField
      FieldName = 'NewNac'
    end
    object teNacNewDMax: TFloatField
      FieldName = 'NewDMax'
    end
    object teNacOldDMin: TFloatField
      FieldName = 'OldDMin'
    end
    object teNacOldNac: TFloatField
      FieldName = 'OldNac'
    end
    object teNacOldDMax: TFloatField
      FieldName = 'OldDMax'
    end
    object teNacDateB: TIntegerField
      FieldName = 'DateB'
    end
  end
  object quBars: TPvQuery
    AutoCalcFields = False
    AutoRefresh = True
    DatabaseName = 'PSQL'
    SQL.Strings = (
      'select * from "BarCode"'
      'where GoodsId=:GOODSID')
    Params = <
      item
        DataType = ftInteger
        Name = 'GOODSID'
        ParamType = ptUnknown
        Value = 0
      end>
    LoadBlobOnOpen = False
    Left = 252
    Top = 144
    object quBarsGoodsID: TIntegerField
      FieldName = 'GoodsID'
    end
    object quBarsID: TStringField
      FieldName = 'ID'
      Size = 13
    end
    object quBarsQuant: TFloatField
      FieldName = 'Quant'
    end
    object quBarsBarFormat: TSmallintField
      FieldName = 'BarFormat'
    end
    object quBarsDateChange: TDateField
      FieldName = 'DateChange'
    end
    object quBarsBarStatus: TSmallintField
      FieldName = 'BarStatus'
    end
    object quBarsCost: TFloatField
      FieldName = 'Cost'
    end
    object quBarsStatus: TSmallintField
      FieldName = 'Status'
    end
  end
end
