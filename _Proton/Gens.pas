unit Gens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, cxImageComboBox, ComCtrls;

type
  TfmGens = class(TForm)
    FormPlacement1: TFormPlacement;
    ViewGens: TcxGridDBTableView;
    LevelGens: TcxGridLevel;
    GridGens: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ViewGensDepart: TcxGridDBColumn;
    ViewGensName: TcxGridDBColumn;
    ViewGensDateReport: TcxGridDBColumn;
    ViewGensSumma: TcxGridDBColumn;
    ViewGensSkidka: TcxGridDBColumn;
    ViewGensChekBuh: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGens: TfmGens;

implementation

uses Un1, MDB, Period1, Status, MT;

{$R *.dfm}

procedure TfmGens.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridGens.Align:=AlClient;
end;

procedure TfmGens.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmGens.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmGens.SpeedItem2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      fmGens.ViewGens.BeginUpdate;
      quGens.Active:=False;
      quGens.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quGens.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quGens.Active:=True;
      fmGens.ViewGens.EndUpdate;

      fmGens.Caption:='��������������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);
    end;
  end;
end;

procedure TfmGens.SpeedItem3Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(ViewGens);
end;

procedure TfmGens.SpeedItem4Click(Sender: TObject);
Var DateB,DateE,iCurD,iDep:INteger;
    StrWk,sY:String;
    iC,iD:Integer;
    rSum,rSumD:Real;
    bStart:Boolean;
    bInv:Boolean;
begin
  //������������
  with dmMC do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

      bInv:=False;

      if ptDep.Active=False then ptDep.Active:=True;
      ptDep.Refresh;

      ptDep.First;
      while not ptDep.Eof do
      begin
        if fTestInv(ptDepID.AsInteger,DateB)=False then
        begin
          bInv:=True;
          Break;
        end;
        ptDep.Next;
      end;


      if (DateB<=prOpenDate)or(bInv=True) then
      begin
        if bInv then ShowMessage('������ ������.(��������������)')
        else ShowMessage('������ ������.');
      end else
      begin
      //����� ������� ��������� - ���� �� ��� ���
        bStart:=True;
        for iCurD:=DateB to DateE do
        begin
            //������ Id ��������� �� �������� ������ � ���
          StrWk:=FormatDateTime('mmdd',iCurD);
          sY:=FormatDateTime('yy',iCurD);
          iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

          if prTestGen(iCurD,Id)=False then bStart:=False;
        end;

        if bStart=False then
        begin
          ShowMessage('�� ��������� ������ ���� ��������������� ������. ��������� ������� �� ��������.');
          exit;
        end;

      if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
      else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
        try
          for iCurD:=DateB to DateE do
          begin
            //������ Id ��������� �� �������� ������ � ���
            StrWk:=FormatDateTime('mmdd',iCurD);
            sY:=FormatDateTime('yy',iCurD);
            iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

{           Memo1.Lines.Add('    - ������ ..');
            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "TovCassa"' );
            quD.SQL.Add('where  DateReport='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "cn'+FormatDateTime('yyyymm',iCurD)+'"' );
            quD.SQL.Add('where  RDate='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            Memo1.Lines.Add(fmt+'    - ��������� �������������� ..');
            //���� ������� � ��� ��������������
            quM2.Active:=False;
            quM2.ParamByName('IDDOC').AsInteger:=ID;
            quM2.ParamByName('DT').AsInteger:=7;
            quM2.Active:=True;

            iC:=0;
            quM2.First;
            while not quM2.Eof do
            begin
              prDelMoveIdC(quM2CARD.AsInteger,iCurD,iD,7);
              quM2.Next; inc(iC); fmSt.StatusBar1.Panels[0].Text:=its(iC);  Delay(10);
            end;

            quM2.Active:=False;

         }
            Memo1.Lines.Add(fmt+'    - ��������� ..');
            quCashGood.Active:=False;
            quCashGood.SQL.Clear;
            quCashGood.SQL.Add('select');
            quCashGood.SQL.Add('cg.GrCode,');
            quCashGood.SQL.Add('cg.Code,');
            quCashGood.SQL.Add('de.Name,');
            quCashGood.SQL.Add('Sum(cg.Quant) as Quant,');
            quCashGood.SQL.Add('Sum(cg.Quant*cg.Price) as RSumD,');
            quCashGood.SQL.Add('Sum(cg.Summa) as RSum');
            quCashGood.SQL.Add('from "cq'+FormatDateTime('yyyymm',iCurD)+'" cg');
            quCashGood.SQL.Add('left join Depart de on de.ID=cg.GrCode');
            quCashGood.SQL.Add('where cg.DateOperation='''+ds(iCurD)+'''');
            quCashGood.SQL.Add('group by cg.GrCode, cg.Code, de.Name');
            quCashGood.SQL.Add('order by cg.GrCode, cg.Code');
            quCashGood.Active:=True;

            Memo1.Lines.Add(fmt+'    - ������������ ..');
            idep:=-1;
            iC:=0;
            rSum:=0;
            rSumD:=0;

            quCashGood.First;
            while not quCashGood.Eof do
            begin
              if iDep<>quCashGoodGrCode.AsInteger then
              begin
                if iDep>0 then
                begin //��������
                  quA.SQL.Clear;
                  quA.SQL.Add('INSERT into "TovCassa" values (');
                  quA.SQL.Add(its(iDep)+',');//Depart
                  quA.SQL.Add(''''+ds(iCurD)+''',');//DateReport
                  quA.SQL.Add(fs(rSum)+',');//Summa
                  quA.SQL.Add(fs(rSumd-rSum)+',');//Skidka
                  quA.SQL.Add('0,');//ChekBuh
                  quA.SQL.Add('0,');//RezervSumma
                  quA.SQL.Add('0,');//RezervSkidka
                  quA.SQL.Add('0,');//SenderSumma
                  quA.SQL.Add('0,');//SenderSkidka
                  quA.SQL.Add(''''+'''');//Rezerv
                  quA.SQL.Add(')');
                  quA.ExecSQL;
                end;

                iDep:=quCashGoodGrCode.AsInteger;
                rSum:=0;
                rSumD:=0;
              end;

              rSum:=rSum+quCashGoodRSum.AsFloat;
              rSumD:=rSumD+quCashGoodRSumD.AsFloat;

//              prWH(its(quCashGoodCode.AsInteger),nil);

              //����� ��������� � cn � �������� ��������������
              quA.SQL.Clear;
              quA.SQL.Add('INSERT into "cn'+FormatDateTime('yyyymm',iCurD)+'" values (');
              quA.SQL.Add('0,');//XZ
              quA.SQL.Add(''''+ds(iCurD)+''',');//RDate

              if abs(quCashGoodQuant.AsFloat)>0 then quA.SQL.Add(fs(RoundVal(quCashGoodRSumD.AsFloat/quCashGoodQuant.AsFloat))+',')
                                                else quA.SQL.Add('0,');//RPrice
              quA.SQL.Add(its(quCashGoodGrCode.AsInteger)+',');//Depart
              quA.SQL.Add(its(quCashGoodCode.AsInteger)+',');//Code
              quA.SQL.Add('0,');//XZ1
              if abs(quCashGoodQuant.AsFloat)>0 then quA.SQL.Add(fs(RoundVal(quCashGoodRSumD.AsFloat/quCashGoodQuant.AsFloat))+',')//RSum
                                                else quA.SQL.Add('0,');//RSum
              quA.SQL.Add('0,');//XZ2
              quA.SQL.Add('0,');//XZ3
              quA.SQL.Add(fs(quCashGoodQuant.AsFloat)+',');//Quant
              quA.SQL.Add(fs(quCashGoodRSum.AsFloat));//RSumCor
              quA.SQL.Add(')');
              quA.ExecSQL;

//              prAddMoveId(quCashGoodGrCode.AsInteger,quCashGoodCode.AsInteger,iCurD,iD,7,(-1)*quCashGoodQuant.AsFloat);
              prAddTMoveId(quCashGoodGrCode.AsInteger,quCashGoodCode.AsInteger,iCurD,iD,7,(-1)*quCashGoodQuant.AsFloat);

              quCashGood.Next; inc(iC); fmSt.StatusBar1.Panels[0].Text:=its(iC);  Delay(10);
            end;

            Memo1.Lines.Add(fmt+'     '+ds(iCurD)+' ���������� '+its(iC)+' �������.');
            if iDep>0 then
            begin
              quA.SQL.Clear;
              quA.SQL.Add('INSERT into "TovCassa" values (');
              quA.SQL.Add(its(iDep)+',');//Depart
              quA.SQL.Add(''''+ds(iCurD)+''',');//DateReport
              quA.SQL.Add(fs(rSum)+',');//Summa
              quA.SQL.Add(fs(rSumd-rSum)+',');//Skidka
              quA.SQL.Add('0,');//ChekBuh
              quA.SQL.Add('0,');//RezervSumma
              quA.SQL.Add('0,');//RezervSkidka
              quA.SQL.Add('0,');//SenderSumma
              quA.SQL.Add('0,');//SenderSkidka
              quA.SQL.Add(''''+'''');//Rezerv
              quA.SQL.Add(')');
              quA.ExecSQL;
            end;

            quCashGood.Active:=False;
          end;
        finally
          Memo1.Lines.Add(fmt+'������� ��������.');
          fmSt.cxButton1.Enabled:=True;
          quGens.Active:=False;
          quGens.Active:=True;
        end;
      end;
      end;
    end;
  end;
end;

procedure TfmGens.SpeedItem5Click(Sender: TObject);
Var DateB,DateE,iCurD:INteger;
    StrWk,sY:String;
    iC,iD:Integer;
    CurDate:tDateTime;
    bInv:Boolean;
begin
  //�������
  with dmMC do
  with dmMt do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(Date)-1; // ���� ����� �� ���������
    fmPeriod1.cxDateEdit2.Date:=Trunc(Date)-1;
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      DateE:=Trunc(fmPeriod1.cxDateEdit2.Date);

      if ptDep.Active=False then ptDep.Active:=True;

      bInv:=False;
      ptDep.First;
      while not ptDep.Eof do
      begin
        if fTestInv(ptDepID.AsInteger,DateB)=False then
        begin
          bInv:=True;
          Break;
        end;
        ptDep.Next;
      end;

      if (DateB<=prOpenDate)or(bInv=True) then
      begin
        if bInv then ShowMessage('������ ������.(��������������)')
        else ShowMessage('������ ������.');
      end else
      begin

      if DateB<>DateE then StrWk:=' c '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE)
      else StrWk:=FormatDateTime('dd.mm.yyyy',DateB);

      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      with fmSt do
      begin
        Memo1.Lines.Add(fmt+'������ ('+StrWk+') ... �����.');
        try
          for iCurD:=DateB to DateE do
          begin
            Memo1.Lines.Add('');
            Memo1.Lines.Add('    - ������ ���������.. '+ FormatDateTime('dd.mm.yyyy',iCurD));
            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "TovCassa"' );
            quD.SQL.Add('where  DateReport='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            Memo1.Lines.Add('    - ������ cn .. '+ FormatDateTime('dd.mm.yyyy',iCurD));
            quD.Active:=False;
            quD.SQL.Clear;
            quD.SQL.Add('Delete from "cn'+FormatDateTime('yyyymm',iCurD)+'"' );
            quD.SQL.Add('where  RDate='''+ds(iCurD)+'''');
            quD.ExecSQL;
            Delay(10);

            //������ Id ��������� �� �������� ������ � ���
            StrWk:=FormatDateTime('mmdd',iCurD);
            sY:=FormatDateTime('yy',iCurD);
            iD:=StrToINtDef(StrWk,0)*1000+StrToINtDef(sY,10);

            Memo1.Lines.Add(fmt+'    - ��������� �������������� ..');

            //���� ������� � ��� ��������������

{
            quM2.Active:=False;
            quM2.ParamByName('IDDOC').AsInteger:=ID;
            quM2.ParamByName('DT').AsInteger:=7;
            quM2.Active:=True;

            iC:=0;
            quM2.First;
            while not quM2.Eof do
            begin
//              prDelMoveIdC(quM2CARD.AsInteger,iCurD,iD,7);
//���� ������ ��� ����� ������� �.�. ��� ��� ��������� �������� �� �������� � �����
              prDelTMoveIdC(quM2CARD.AsInteger,iCurD,iD,7);
              quM2.Next; inc(iC); fmSt.StatusBar1.Panels[0].Text:=its(iC);  Delay(10);
            end;
            quM2.Active:=False;
            }

            Memo1.Lines.Add(fmt+'      ��������� ..');
            if ptCM2.Active=False then ptCM2.Active:=True;
            CurDate:=iCurD;
            ptCM2.CancelRange;
            ptCM2.SetRange([CurDate],[CurDate]);
            ptCM2.First;
            Memo1.Lines.Add(fmt+'        �������� �������� ..');

            CloseTe(meCg);
            iC:=0;
            while not ptCM2.Eof do
            begin
{select * from "CardGoodsMove"
where DOCUMENT=:IDDOC
and DOC_TYPE=:DT
}
              //������� ��� ������� � �����������
              if (ptCM2DOCUMENT.AsInteger=iD)and(ptCM2DOC_TYPE.AsInteger=7) then
              begin
                meCg.Append;
                meCgCard.AsInteger:=ptCM2CARD.AsInteger;
                meCg.Post;

                ptCM2.Delete;
                inc(iC);
              end else ptCM2.Next;

              if iC>0 then
                if iC mod 100 = 0 then
                begin
                  fmSt.StatusBar1.Panels[0].Text:=its(iC);
                  delay(20);
                end;
            end;
            ptCM2.Active:=False;

            Memo1.Lines.Add(fmt+'        �������� �������� ..');
            iC:=0;
            meCg.First;
            while not meCg.Eof do
            begin
              prReCalcTMove(meCgCard.AsInteger,iCurD);

              meCg.Next;
              inc(iC);
              if iC mod 100 = 0 then
              begin
                fmSt.StatusBar1.Panels[0].Text:=its(iC);
                delay(20);
              end;
            end;

            Memo1.Lines.Add('��.');
          end;
        finally
          Memo1.Lines.Add(fmt+'������� ��������.');
          fmSt.cxButton1.Enabled:=True;
          quGens.Active:=False;
          quGens.Active:=True;
        end;
      end;
      end;
    end;
  end;
end;

end.
