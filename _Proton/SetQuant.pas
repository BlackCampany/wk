unit SetQuant;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalc, StdCtrls, Menus,
  cxLookAndFeelPainters, cxButtons;

type
  TfmSetQuant = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetQuant: TfmSetQuant;

implementation

{$R *.dfm}

procedure TfmSetQuant.FormShow(Sender: TObject);
begin
  cxCalcEdit1.SetFocus;
  cxCalcEdit1.SelectAll;
end;

end.
