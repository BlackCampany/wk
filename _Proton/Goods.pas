unit Goods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls, Placemnt,
  ToolWin, ActnCtrls, ActnMenus, cxControls, cxContainer, cxTreeView,
  ExtCtrls, SpeedBar, cxSplitter, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxImageComboBox,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxTextEdit, StdCtrls, cxCalc,
  Menus, cxLookAndFeelPainters, cxButtons,cxCheckListBox,cxCheckBox,
  cxCurrencyEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, dxmdaset;

type
  TfmGoods = class(TForm)
    StatusBar1: TStatusBar;
    amG: TActionManager;
    FormPlacement1: TFormPlacement;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Panel1: TPanel;
    ClassTree: TcxTreeView;
    SpeedBar1: TSpeedBar;
    cxSplitter1: TcxSplitter;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Timer1: TTimer;
    ViewGoods: TcxGridDBTableView;
    LevelGoods: TcxGridLevel;
    GrGoods: TcxGrid;
    ViewGoodsID: TcxGridDBColumn;
    ViewGoodsNAME: TcxGridDBColumn;
    ViewGoodsTTYPE: TcxGridDBColumn;
    ViewGoodsMINREST: TcxGridDBColumn;
    ViewGoodsIACTIVE: TcxGridDBColumn;
    ViewGoodsNAMESHORT: TcxGridDBColumn;
    ViewGoodsNAMENDS: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    acAddGoods: TAction;
    acEditGoods: TAction;
    acDelGoods: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    Action9: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewGoodsTCARD: TcxGridDBColumn;
    acTCard: TAction;
    SpeedItem5: TSpeedItem;
    acCardMove: TAction;
    SpeedItem6: TSpeedItem;
    N10: TMenuItem;
    N11: TMenuItem;
    acInputB: TAction;
    Excel1: TMenuItem;
    ViewGoodsLASTPRICEOUT: TcxGridDBColumn;
    ViewGoodsRemn: TcxGridDBColumn;
    Label2: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    acReDel: TAction;
    ViewGoodsCOMMENT: TcxGridDBColumn;
    ViewGoodsEE: TcxGridDBColumn;
    acCalcTTK: TAction;
    acSetStatus: TAction;
    N12: TMenuItem;
    N13: TMenuItem;
    ViewGoodsIACTIVE1: TcxGridDBColumn;
    ViewGoodsCATEGORY: TcxGridDBColumn;
    ViewGoodsRCATEGORY: TcxGridDBColumn;
    N14: TMenuItem;
    ViewGoodsCTO: TcxGridDBColumn;
    ViewGoodsCODEZAK: TcxGridDBColumn;
    ViewGoodsALGCLASS: TcxGridDBColumn;
    ViewGoodsNAMEM: TcxGridDBColumn;
    acGroupEdit: TAction;
    N15: TMenuItem;
    teBuff: TdxMemData;
    teBuffCode: TIntegerField;
    ViewGoodsVOL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGoodsExecute(Sender: TObject);
    procedure acEditGoodsExecute(Sender: TObject);
    procedure acDelGoodsExecute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure ViewGoodsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ClassTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ClassTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewGoodsDblClick(Sender: TObject);
    procedure acTCardExecute(Sender: TObject);
    procedure acCardMoveExecute(Sender: TObject);
    procedure ViewGoodsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewGoodsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acInputBExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure acReDelExecute(Sender: TObject);
    procedure acCalcTTKExecute(Sender: TObject);
    procedure acSetStatusExecute(Sender: TObject);
    procedure ViewGoodsCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N14Click(Sender: TObject);
    procedure acGroupEditExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGoods: TfmGoods;
  bClearClass:Boolean = False;
  bAddG:Boolean = False;

implementation

uses dmOffice, Un1, AddClass, AddGoods, FindResult, AddDoc1, TCard,
  GoodsSel, AddDoc2, CardsMove, AddInv, TransMenuBack, uTransM, AddCompl,
  AddAct, AddDoc3, Input, DMOReps, SelPerSkl, AddDoc4, DOBSpec, of_TTKView,
  SetStatus, AddBGU, SetCardsPars;

{$R *.dfm}

procedure TfmGoods.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;

  ClassTree.Items.BeginUpdate;
  ClassifExpand(nil,ClassTree,dmO.quClassTree,Person.Id,1);
  ClassTree.FullExpand;
  ClassTree.FullCollapse;
  ClassTree.Items.EndUpdate;

  Timer1.Enabled:=True;
  GrGoods.Align:=AlClient;
  ViewGoods.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';

  ViewGoodsCOMMENT.Options.Editing:=CanDo('prEditGoods');

  with dmO do
  begin
    if quMHr.Active=False then quMHr.Active:=True;
    quMHr.FullRefresh;

    cxLookupComboBox1.EditValue:=0;
    cxLookupComboBox1.Text:='';
    cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH1<>0 then
    begin
      cxLookupComboBox1.EditValue:=CurVal.IdMH1;
      cxLookupComboBox1.Text:=CurVal.NAMEMH1;
    end else
    begin
       quMHr.First;
       if not quMHr.Eof then
       begin
         CurVal.IdMH1:=quMHrID.AsInteger;
         CurVal.NAMEMH1:=quMHrNAMEMH.AsString;
         cxLookupComboBox1.EditValue:=CurVal.IdMH1;
         cxLookupComboBox1.Text:=CurVal.NAMEMH1;
       end;
    end;
  end;
end;

procedure TfmGoods.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmGoods.Action1Execute(Sender: TObject);
begin
  bAddSpecIn:= False;
  bAddSpecB:= False;
  bAddSpecB1:= False;
  bAddSpecInv:= False;
  bAddSpecCompl:= False;
  bAddTSpec:= False;
  bAddSpecAO:= False;
  bAddSpecVn:=False;
  bAddSpecR:=False;

  close;
end;

procedure TfmGoods.Action3Execute(Sender: TObject);
begin
 //�������������
end;

procedure TfmGoods.Action5Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
    StrWk:String;
begin
// �������� ��������  ������
  if not CanDo('prAddClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddClass.Caption:='���������� ������.';
  fmAddClass.label4.Caption:='���������� ������.';
  fmAddClass.cxTextEdit1.Text:='';

  fmAddClass.ShowModal;
  if fmAddClass.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('Class');

      taClass.Active:=False;
      taClass.Active:=True;
      taClass.Append;
      taClassID.AsInteger:=iId;
      taClassID_PARENT.AsInteger:=0;
      taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
      taClassITYPE.AsInteger:=1; //������������� �������
      taClass.Post;

      StrWk:=taClass.FieldByName('NAMECL').AsString;

      ClassTree.Items.BeginUpdate;
      TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      ClassTree.Items.AddChildObject(TreeNode,'', nil);
      ClassTree.Items.EndUpdate;

      with fmGoodsSel do
      begin
        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'',nil);
        ClassTree.Items.EndUpdate;
        ClassTree.Repaint;
        delay(10);
      end;

      for i:=0 To ClassTree.Items.Count-1 do
        if Integer(ClassTree.Items[i].Data) = iId then
        begin
          ClassTree.Items[i].Expand(False);
          ClassTree.Items[i].Selected:=True;
          ClassTree.Repaint;
          Break;
        end;

      taClass.Active:=False;
    end;
  end;
end;

procedure TfmGoods.Action6Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    CurNode : TTreeNode;
    iId,i,iParent:Integer;
    StrWk:String;
begin
//�������� ���������
  if not CanDo('prAddSubClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ���������� ���������.');
    exit;
  end;

  iParent:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iParent,[]) then
    begin
      fmAddClass.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.label4.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        iId:=GetId('Class');

        taClass.Active:=False;
        taClass.Active:=True;
        taClass.Append;
        taClassID.AsInteger:=iId;
        taClassID_PARENT.AsInteger:=iParent;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        StrWk:=taClass.FieldByName('NAMECL').AsString;

        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(CurNode,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'', nil);
        ClassTree.Items.EndUpdate;

        with fmGoodsSel do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) =iParent  then
            begin
              ClassTree.Items.BeginUpdate;
              TreeNode:=ClassTree.Items.AddChildObject(ClassTree.Items[i],StrWk,Pointer(iId));
              TreeNode.ImageIndex:=8;
              TreeNode.SelectedIndex:=7;
              ClassTree.Items.AddChildObject(TreeNode,'', nil);
              ClassTree.Items.EndUpdate;

              ClassTree.Repaint;
              Break;
            end;
        end;


        for i:=0 To ClassTree.Items.Count-1 do
          if Integer(ClassTree.Items[i].Data) = iId then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Items[i].Selected:=True;
            ClassTree.Repaint;
            Break;
          end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoods.Action7Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iId,i:Integer;
begin
// �������������
  if not CanDo('prEditClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������������.');
    exit;
  end;

  iId:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iId,[]) then
    begin
      fmAddClass.Caption:='�������������� ������ "'+taClassNAMECL.AsString+'"';
      fmAddClass.label4.Caption:='�������������� ������"'+taClassNAMECL.AsString+'"';
      fmAddClass.cxTextEdit1.Text:=taClassNAMECL.AsString;

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        taClass.Edit;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        CurNode.Text:=fmAddClass.cxTextEdit1.Text;

        with fmGoodsSel do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) = iId then
            begin
              ClassTree.Items[i].Text:=fmAddClass.cxTextEdit1.Text;
              ClassTree.Repaint;
              Break;
            end;
        end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoods.Action8Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iRes:Integer;
    i,iId:Integer;
begin
//�������
  if not CanDo('prDelClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������.');
    exit;
  end;

  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ "'+CurNode.Text+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prCanDelClass.ParamByName('IDCL').AsInteger:=Integer(CurNode.Data);
      prCanDelClass.ExecProc;
      iRes:=prCanDelClass.ParamByName('RESULT').AsInteger;

      if iRes=0 then
      begin //�������� ���������
        taClass.Active:=False;
        taClass.Active:=True;
        if taClass.Locate('ID',Integer(CurNode.Data),[]) then
        begin
          iId:=Integer(CurNode.Data);
          taClass.Delete;
          CurNode.Delete;
          ClassTree.Repaint;

          with fmGoodsSel do
          begin
            for i:=0 To ClassTree.Items.Count-1 do
              if Integer(ClassTree.Items[i].Data)=iId then
              begin
                ClassTree.Items[i].Delete;
                ClassTree.Repaint;
                Break;
              end;
          end;
        end;
        taClass.Active:=False;
      end;

      if iRes=1 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ���� ���������.');
      end;

      if iRes=2 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ������ �� �����.');
      end;

    end;
  end;
end;

procedure TfmGoods.Timer1Timer(Sender: TObject);
begin
  if bClearClass=True then begin StatusBar1.Panels[0].Text:=''; bClearClass:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearClass:=True;
end;

procedure TfmGoods.ClassTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if dmO=nil then exit;
  with dmO do
  begin
    ViewGoods.BeginUpdate;
    quCardsSel.Active:=False;
    quCardsSel.ParamByName('PARENTID').AsInteger:=Integer(Node.Data);
    quCardsSel.Active:=True;
    ViewGoods.EndUpdate;
  end;
end;

procedure TfmGoods.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewGoods.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmGoods.acAddGoodsExecute(Sender: TObject);
Var iId:Integer;
    StrSp:String;
    i:Integer;
begin
// �������� �����
  if not CanDo('prAddGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if ClassTree.Selected=Nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;

  with dmO do
  with dmORep do
  begin
    iId:=GetId('GD');
    while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
    begin
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then break
      else iId:=GetId('GD');
    end;
    quGdsFind.Active:=False;
    if iId=100000 then
    begin
      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
      exit;
    end;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;
    quAlgClass.Active:=False;
    quAlgClass.Active:=True;
    quMakers.Active:=False;
    quMakers.Active:=True;

    with fmAddGood do
    begin
      CheckListBox1.Items.BeginUpdate;
      CheckListBox1.Items.Clear;
      quMHSpis.Active:=False;
      quMHSpis.Active:=True;
      quMHSpis.First;
      while not quMHSpis.Eof do
      begin
        with  CheckListBox1.Items.add do
        begin
          Enabled:=True;
          State:=cbsUnChecked;
          Text:=quMHSpisNAMEMH.AsString;
          Tag:=quMHSpisNUMSPIS.AsInteger;
        end;
        quMHSpis.Next;
      end;
      CheckListBox1.Items.EndUpdate;
      CheckListBox1.ReadOnly:=False;
      quMHSpis.Active:=False;

      Caption:='���������� ������.';
      Label1.Caption:='���������� ������ � ������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=''; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=''; cxTextEdit2.Properties.ReadOnly:=False;
      cxCEdit1.EditValue:=0; cxCEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=False;
      cxLookUpComboBox1.EditValue:=2; cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=2; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=1; cxLookUpComboBox3.Properties.ReadOnly:=False;
      cxLookUpComboBox4.EditValue:=1; cxLookUpComboBox4.Properties.ReadOnly:=False;
      cxLookUpComboBox5.EditValue:=0; cxLookUpComboBox5.Properties.ReadOnly:=False;
      cxLookUpComboBox6.EditValue:=0; cxLookUpComboBox6.Properties.ReadOnly:=False;

      cxCalcEdit1.Value:=0; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=0; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=1; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=True;

      cxCalcEdit2.Value:=0; cxCalcEdit2.Properties.ReadOnly:=False;
      cxCalcEdit3.Value:=0; cxCalcEdit3.Properties.ReadOnly:=False;
      cxCalcEdit4.Value:=0; cxCalcEdit4.Properties.ReadOnly:=False;
      cxCalcEdit5.Value:=0; cxCalcEdit5.Properties.ReadOnly:=False;
      cxCalcEdit6.Value:=0; cxCalcEdit6.Properties.ReadOnly:=False;

      cxSpinEdit2.Value:=0; cxSpinEdit2.Properties.ReadOnly:=False;

//      tBar.Active:=False;
      CloseTa(tBar);

      tEU.Active:=False;
      CloseTa(tEU);

      if quCardsSel.RecordCount>0 then
      begin
        cxTextEdit1.Text:=quCardsSelNAME.AsString;
        cxTextEdit2.Text:=quCardsSelCOMMENT.AsString;
        cxCEdit1.EditValue:=quCardsSelLASTPRICEOUT.AsFloat;
        cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;
        cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger;
        cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger;
        cxLookUpComboBox4.EditValue:=quCardsSelRCATEGORY.AsInteger;
        cxLookUpComboBox5.EditValue:=quCardsSelALGCLASS.AsInteger;

        cxCalcEdit1.Value:=quCardsSelMINREST.AsFloat;
        cxCheckBox1.EditValue:=quCardsSelTTYPE.AsInteger;

        cxCalcEdit2.Value:=quCardsSelBB.AsFloat;
        cxCalcEdit3.Value:=quCardsSelGG.AsFloat;
        cxCalcEdit4.Value:=quCardsSelU1.AsFloat;
        cxCalcEdit5.Value:=quCardsSelEE.AsFloat;
        cxCalcEdit6.Value:=quCardsSelVOL.AsFloat;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
        quGoodsEU.Active:=True;
        quGoodsEu.First;
        while not quGoodsEU.Eof do
        begin
          tEu.Append;
          tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
          tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
          tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
          tEU.Post;

          quGoodsEU.Next;
        end;
      end;
    end;

    bAddG:=True; //�������� ����������

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      //��������
      if fmAddGood.cxLookUpComboBox5.EditValue=null then fmAddGood.cxLookUpComboBox5.EditValue:=0;
      if fmAddGood.cxLookUpComboBox6.EditValue=null then fmAddGood.cxLookUpComboBox6.EditValue:=0;

      iId:=fmAddGood.cxSpinEdit1.Value;
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then
      begin
        StrSp:='00000000000000000000';
        for i:=0 to fmAddGood.CheckListBox1.Items.Count-1 do
          if fmAddGood.CheckListBox1.Items[i].State=cbsChecked then StrSp[fmAddGood.CheckListBox1.Items[i].Tag]:='1';

        try
          quCardsSel.Append;
          quCardsSelID.AsInteger:=iId;
          quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
          quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
          quCardsSelCOMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
          quCardsSelLASTPRICEOUT.AsFloat:=fmAddGood.cxCEdit1.EditValue;
          quCardsSelTTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
          quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
          quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
          quCardsSelMINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
          quCardsSelIACTIVE.AsInteger:=1;
          quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
          quCardsSelRCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox4.EditValue;
          quCardsSelSPISSTORE.AsString:=StrSp;
          quCardsSelBB.AsFloat:=fmAddGood.cxCalcEdit2.Value;
          quCardsSelGG.AsFloat:=fmAddGood.cxCalcEdit3.Value;
          quCardsSelU1.AsFloat:=fmAddGood.cxCalcEdit4.Value;
          quCardsSelEE.AsFloat:=fmAddGood.cxCalcEdit5.Value;
          quCardsSelCODEZAK.AsInteger:=fmAddGood.cxSpinEdit2.Value;
          quCardsSelALGCLASS.AsInteger:=integer(fmAddGood.cxLookUpComboBox5.EditValue);
          quCardsSelALGMAKER.AsInteger:=integer(fmAddGood.cxLookUpComboBox6.EditValue);
          quCardsSelVOL.AsFloat:=fmAddGood.cxCalcEdit6.Value;
          quCardsSel.Post;

          with fmAddGood do
          begin
            quBars.Active:=False;
            quBars.ParamByName('IID').AsInteger:=iId;
            quBars.Active:=True;

            tBar.First;
            while not tBar.Eof do
            begin
              if tBariStatus.AsInteger=1 then
              begin
                quBars.Append;
                quBarsBAR.AsString:=tBarBarNew.AsString;
                quBarsGOODSID.AsInteger:=iId;
                quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
                quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
                quBarsPRICE.AsFloat:=0;
                quBars.Post;
              end;
              tBar.Next;
            end;
            quBars.Active:=False;

            quGoodsEU.Active:=False;
            quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
            quGoodsEU.Active:=True;
            quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

            tEU.first;
            while not tEu.Eof do
            begin
              quGoodsEU.Append;
              quGoodsEUGOODSID.AsInteger:=iId;
              quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
              quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
              quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
              quGoodsEU.Post;

              tEU.next;
            end;
            tEu.Active:=False;
          end;
        except
          Showmessage('������ ��� ���������� , ��������� ������������ ���������� ����������.');
        end;
        quCardsSel.Refresh;
      end else
      begin
        Showmessage('�������� � ����� ����� '+IntToStr(iId)+' ��� ����������, ���������� ����������.');
      end;
      quGdsFind.Active:=False;
    end;

    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    quAlgClass.Active:=False;
    quMakers.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoods.acEditGoodsExecute(Sender: TObject);
Var IId:Integer;
    StrF:String;
    StrSp:String;
    i:Integer;
begin
//������������� �����
  if not CanDo('prEditGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  with dmORep do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    iId:=quCardsSelID.AsInteger;
    StrF:=quCardsSelSPISSTORE.AsString;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;
    quALGCLASS.Active:=False;
    quALGCLASS.Active:=True;
    quMakers.Active:=False;
    quMakers.Active:=True;

    with fmAddGood do
    begin
      CheckListBox1.Items.BeginUpdate;
      CheckListBox1.Items.Clear;
      quMHSpis.Active:=False;
      quMHSpis.Active:=True;
      quMHSpis.First;
      while not quMHSpis.Eof do
      begin
        with  CheckListBox1.Items.add do
        begin
          Enabled:=True;
          if StrF[quMHSpisNUMSPIS.AsInteger]='0' then State:=cbsUnChecked
          else State:=cbsChecked;
          Text:=quMHSpisNAMEMH.AsString;
          Tag:=quMHSpisNUMSPIS.AsInteger;
        end;
        quMHSpis.Next;
      end;
      CheckListBox1.Items.EndUpdate;
      CheckListBox1.ReadOnly:=False;
      quMHSpis.Active:=False;

      Caption:='�������������� ������.';
      Label1.Caption:='������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=quCardsSelNAME.AsString; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=quCardsSelCOMMENT.AsString; cxTextEdit2.Properties.ReadOnly:=False;

      cxCEdit1.EditValue:=quCardsSelLASTPRICEOUT.AsFloat; cxCEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=True;
      cxLookUpComboBox1.EditValue:=quCardsSelIMESSURE.AsInteger;cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=quCardsSelINDS.AsInteger; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=quCardsSelCATEGORY.AsInteger; cxLookUpComboBox3.Properties.ReadOnly:=False;
      cxLookUpComboBox4.EditValue:=quCardsSelRCATEGORY.AsInteger; cxLookUpComboBox4.Properties.ReadOnly:=False;
      cxLookUpComboBox5.EditValue:=quCardsSelALGCLASS.AsInteger; cxLookUpComboBox5.Properties.ReadOnly:=False;
      cxLookUpComboBox6.EditValue:=quCardsSelALGMAKER.AsInteger; cxLookUpComboBox6.Properties.ReadOnly:=False;

      cxCalcEdit1.Value:=quCardsSelMINREST.AsFloat; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=quCardsSelTTYPE.AsInteger; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=quCardsSelIACTIVE.AsInteger; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=True;

      cxCalcEdit2.Value:=quCardsSelBB.AsFloat; cxCalcEdit2.Properties.ReadOnly:=False;
      cxCalcEdit3.Value:=quCardsSelGG.AsFloat; cxCalcEdit3.Properties.ReadOnly:=False;
      cxCalcEdit4.Value:=quCardsSelU1.AsFloat; cxCalcEdit4.Properties.ReadOnly:=False;
      cxCalcEdit5.Value:=quCardsSelEE.AsFloat; cxCalcEdit5.Properties.ReadOnly:=False;
      cxCalcEdit6.Value:=quCardsSelVOL.AsFloat; cxCalcEdit5.Properties.ReadOnly:=False;

      cxSpinEdit2.Value:=quCardsSelCODEZAK.AsInteger; cxSpinEdit2.Properties.ReadOnly:=False;

      CloseTa(tBar);

      CloseTa(tEu);

      quBars.Active:=False;
      quBars.ParamByName('IID').AsInteger:=iID;
      quBars.Active:=True;
      quBars.First;
      while not quBars.Eof do
      begin
        tBar.Append;
        tBarBarNew.AsString:=quBarsBAR.AsString;
        tBarBarOld.AsString:=quBarsBAR.AsString;
        tBarQuant.AsFloat:=quBarsQUANT.AsFloat;
        tBariStatus.AsInteger:=1;
        tBar.Post;

        quBars.Next;
      end;

      quBars.Active:=False;

      quGoodsEU.Active:=False;
      quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
      quGoodsEU.Active:=True;
      quGoodsEu.First;
      while not quGoodsEU.Eof do
      begin
        tEu.Append;
        tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
        tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
        tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
        tEU.Post;

        quGoodsEU.Next;
      end;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      if fmAddGood.cxLookUpComboBox5.EditValue=null then fmAddGood.cxLookUpComboBox5.EditValue:=0;
      if fmAddGood.cxLookUpComboBox6.EditValue=null then fmAddGood.cxLookUpComboBox6.EditValue:=0;

      StrSp:='00000000000000000000';
      for i:=0 to fmAddGood.CheckListBox1.Items.Count-1 do
        if fmAddGood.CheckListBox1.Items[i].State=cbsChecked then StrSp[fmAddGood.CheckListBox1.Items[i].Tag]:='1';

        //����������
      quCardsSel.Edit;
      quCardsSelNAME.AsString:=fmAddGood.cxTextEdit1.Text;
      quCardsSelCOMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
      quCardsSelLASTPRICEOUT.AsFloat:=fmAddGood.cxCEdit1.EditValue;
      quCardsSelTTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
      quCardsSelIMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
      quCardsSelINDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
      quCardsSelMINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
      quCardsSelIACTIVE.AsInteger:=fmAddGood.cxCheckBox2.EditValue;
      quCardsSelCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
      quCardsSelRCATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox4.EditValue;
      quCardsSelSPISSTORE.AsString:=StrSp;
      quCardsSelBB.AsFloat:=fmAddGood.cxCalcEdit2.Value;
      quCardsSelGG.AsFloat:=fmAddGood.cxCalcEdit3.Value;
      quCardsSelU1.AsFloat:=fmAddGood.cxCalcEdit4.Value;
      quCardsSelEE.AsFloat:=fmAddGood.cxCalcEdit5.Value;
      quCardsSelCODEZAK.AsInteger:=fmAddGood.cxSpinEdit2.Value;
      quCardsSelALGCLASS.AsInteger:=integer(fmAddGood.cxLookUpComboBox5.EditValue);
      quCardsSelALGMAKER.AsInteger:=integer(fmAddGood.cxLookUpComboBox6.EditValue);
      quCardsSelVOL.AsFloat:=fmAddGood.cxCalcEdit6.Value;
      quCardsSel.Post;

      with fmAddGood do
      begin
        quBars.Active:=False;
        quBars.ParamByName('IID').AsInteger:=iId;
        quBars.Active:=True;

        quBars.First;
        while not quBars.Eof do quBars.Delete;

        tBar.First;
        while not tBar.Eof do
        begin
          if tBariStatus.AsInteger=1 then
          begin
            quBars.Append;
            quBarsBAR.AsString:=tBarBarNew.AsString;
            quBarsGOODSID.AsInteger:=iId;
            quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
            quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
            quBarsPRICE.AsFloat:=0;
            quBars.Post;
          end;
          tBar.Next;
        end;
        quBars.Active:=False;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
        quGoodsEU.Active:=True;
        quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

        tEU.first;
        while not tEu.Eof do
        begin
          quGoodsEU.Append;
          quGoodsEUGOODSID.AsInteger:=iId;
          quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
          quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
          quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
          quGoodsEU.Post;

          tEU.next;
        end;
        tEu.Active:=False;
      end;

      quCardsSel.Refresh;
    end;
    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    quAlgClass.Active:=False;
    quMakers.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoods.acDelGoodsExecute(Sender: TObject);
Var iRes:Integer;
begin
// ������� �����
  if not CanDo('prDelGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel.Eof then
    begin
      showmessage('�������� ����� ��� ��������.');
      exit;
    end;

    if MessageDlg('�� ������������� ������ ������� �����: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� �������� ������: '+quCardsSelNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        prDelCard.ParamByName('SIFR').AsInteger:=quCardsSelID.AsInteger;
        prDelCard.ExecProc;
        iRes:=prDelCard.ParamByName('RESULT').AsInteger;

        if iRes=0 then
        begin //�������� ���������
          quGoodsEU.Active:=False;
          quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
          quGoodsEU.Active:=True;
          quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

          quCardsSel.Delete;
          quCardsSel.Refresh;
        end else
          ShowMessage('����� �������������. �������� ����������.');

      end;
    end;
  end;
end;

procedure TfmGoods.Action9Execute(Sender: TObject);
begin
// �����������
  showmessage('�������� ����������� ������� ��� �������� � ���������� �� ������ � ������ ������.');
end;

procedure TfmGoods.ViewGoodsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if CanDo('prMoveGoods') then
  begin bDr:=True;  end;
  if CanDo('prEditDocIn') then
  begin bDrIn:=True;  end;
  if CanDo('prEditDocRet') then
  begin bDrRet:=True;  end;
  if CanDo('prTransMenu') then
  begin bDM:=True;  end;
  if CanDo('prEditInvSpec') then
  begin bDInv:=True;  end;
  if CanDo('prEditCompl') then
  begin bDCompl:=True;  end;
  if CanDo('prEditActs') then
  begin bDAct:=True;  end;
  if CanDo('prEditDocVn') then
  begin bDrVn:=True;  end;
  if CanDo('prEditDocR') then
  begin bDrR:=True;  end;
  bStartGoods:=True;
end;

procedure TfmGoods.ClassTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmGoods.ClassTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if not CanDo('prMoveGoods') then begin StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if bDr then
  begin
    bDr:=False;
    sGr:=ClassTree.DropTarget.Text;
    iGr:=Integer(ClassTree.DropTarget.data);
    iCo:=ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              quCardsSel.Edit;
              quCardsSelPARENT.AsInteger:=iGr;
              quCardsSel.Post;
            end;
          end;
          quCardsSel.FullRefresh;
        end;
      end;
    end;
  end;
end;

procedure TfmGoods.ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,ClassTree,dmO.quClassTree,Person.Id,1);
  end;
end;

procedure TfmGoods.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFind.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      quFind.Locate('NAME',cxTextEdit1.Text,[loCaseInsensitive, loPartialKey]);
      prRowFocus(fmFind.ViewFind);

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          if quCardsSel.locate('ID',quFindID.AsInteger,[]) then prRowFocus(ViewGoods);

        end;
      end;
//      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.cxButton2Click(Sender: TObject);
//�� ����
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where ID ='+IntToStr(iCode));
      quFind.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          if quCardsSel.locate('ID',quFindID.AsInteger,[]) then prRowFocus(ViewGoods);

        end;
      end;
//      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.cxButton3Click(Sender: TObject);
Var i:INteger;
begin
{
SELECT c.ID,c.PARENT,c.NAME
FROM OF_CARDS c
left Join OF_BARCODE b on b.GOODSID=c.ID
where b.BAR like '%2222%'
}
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT c.ID,c.PARENT,c.NAME,c.TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS c');
      quFind.SelectSQL.Add('left Join OF_BARCODE b on b.GOODSID=c.ID');
      quFind.SelectSQL.Add('where b.BAR like ''%'+cxTextEdit1.Text+'%''');
      quFind.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel.First;
          if quCardsSel.locate('ID',quFindID.AsInteger,[]) then prRowFocus(ViewGoods);
        end;
      end;
//      cxTextEdit1.Text:='';
      delay(10);
      GrGoods.SetFocus;
    end;
  end;
end;

procedure TfmGoods.ViewGoodsDblClick(Sender: TObject);
Var iMax,iM:INteger;
    sM:String;
    kM:Real;
//    kBrutto:Real;
//    iCurDate:Integer;
    iSS:INteger;
begin
  if bAddSpecIn or bAddSpecB or bAddSpecB1 or bAddSpecInv or bAddSpecCompl or bAddSpecAO or bAddSpecVn or bAddSpecRet or bAddSpecR then
  begin //�������� � ������������ ���������
    with dmO do
    begin
      if bAddSpecIn then
      begin
        if fmAddDoc1.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            if fmAddDoc1.PageControl1.ActivePageIndex=0 then
            begin
              iMax:=1;
              fmAddDoc1.taSpec.First;
              if not fmAddDoc1.taSpec.Eof then
              begin
                fmAddDoc1.taSpec.Last;
                iMax:=fmAddDoc1.taSpecNum.AsInteger+1;
              end;

              with fmAddDoc1 do
              begin
                iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

                ViewDoc1.BeginUpdate;
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecQuant.AsFloat:=1;
                taSpecQuantRemn.AsFloat:=0;
                taSpecPrice1.AsFloat:=0;
                taSpecSum1.AsFloat:=0;
                taSpecPrice2.AsFloat:=0;
                taSpecSum2.AsFloat:=0;
                if iSS<>2 then
                begin
                  taSpecINds.AsInteger:=1;
                  taSpecSNds.AsString:='��� ���';
                  taSpecNDSProc.AsFloat:=0;
                end else
                begin
                  taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                  taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                  taSpecNDSProc.AsFloat:=(quCardsSelPROC.AsFloat)*fmAddDoc1.Label4.Tag;
                end;
                taSpecRNds.AsFloat:=0;
                taSpecSumNac.AsFloat:=0;
                taSpecProcNac.AsFloat:=0;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsINteger;
                taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;

                taSpecPrice0.AsFloat:=0;
                taSpecSum0.AsFloat:=0;

                taSpec.Post;
                ViewDoc1.EndUpdate;
              end;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecR then
      begin
        if fmAddDoc4.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            if fmAddDoc4.PageControl1.ActivePageIndex=0 then
            begin
              iMax:=1;
              fmAddDoc4.taSpec.First;
              if not fmAddDoc4.taSpec.Eof then
              begin
                fmAddDoc4.taSpec.Last;
                iMax:=fmAddDoc4.taSpecNum.AsInteger+1;
              end;

              with fmAddDoc4 do
              begin
                iSS:=prIss(fmAddDoc4.cxLookupComboBox1.EditValue);

                ViewDoc4.BeginUpdate;
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecQuant.AsFloat:=1;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                taSpecSumR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                if iSS<>2 then
                begin
                  taSpecINds.AsInteger:=1;
                  taSpecSNds.AsString:='��� ���';
                end else
                begin
                  taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                  taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                end;
                taSpecRNds.AsFloat:=0;
                taSpecSumNac.AsFloat:=0;
                taSpecProcNac.AsFloat:=0;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpecOper.AsInteger:=0;
                taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                taSpecPriceIn0.AsFloat:=0;
                taSpecSumIn0.AsFloat:=0;
                taSpecSumOut0.AsFloat:=0;
                taSpecRNdsOut.AsFloat:=0;
                taSpec.Post;
                ViewDoc4.EndUpdate;
              end;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecRet then
      begin
        if fmAddDoc2.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            if fmAddDoc2.PageControl1.ActivePageIndex=0 then
            begin
              iMax:=1;
              fmAddDoc2.taSpecOut.First;
              if not fmAddDoc2.taSpecOut.Eof then
              begin
                fmAddDoc2.taSpecOut.Last;
                iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
              end;

              with fmAddDoc2 do
              begin
                ViewDoc2.BeginUpdate;
                taSpecOut.Append;
                taSpecOutNum.AsInteger:=iMax;
                taSpecOutIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecOutNameG.AsString:=quCardsSelNAME.AsString;
                taSpecOutIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecOutSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecOutKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecOutQuant.AsFloat:=1;
                taSpecOutPrice1.AsFloat:=0;
                taSpecOutSum1.AsFloat:=0;
                taSpecOutPrice2.AsFloat:=0;
                taSpecOutSum2.AsFloat:=0;
                taSpecOutINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecOutSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecOutRNds.AsFloat:=0;
                taSpecOutSumNac.AsFloat:=0;
                taSpecOutProcNac.AsFloat:=0;
                taSpecOutCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                taSpecOut.Post;
                ViewDoc2.EndUpdate;
              end;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecB1 then
      begin
        with fmDobSpec do
        begin
          if (teSpecB.Active=True) and (teSpecB.RecordCount>0) then
          begin
            prFindSM(quCardsSelIMESSURE.AsInteger,sM,iM); //����� � ��������

            teSpecB.Edit;
            teSpecBCODEB.AsString:=quCardsSelID.AsString;
            teSpecBKB.AsFloat:=1;
            teSpecBIDCARD.AsInteger:=quCardsSelID.AsInteger;
            teSpecBIDM.AsInteger:=iM;
            teSpecBKM.AsFloat:=1;
            teSpecBNAME.AsString:=quCardsSelNAME.AsString;
            teSpecBNAMESHORT.AsString:=sM;
            teSpecBTCard.AsInteger:=quCardsSelTCARD.AsInteger;
            teSpecBSALET.AsInteger:=0;
            teSpecBSSALET.AsString:='';
            teSpecB.Post;

            bAddSpecB1:=False;
          end;
        end;
        Close;
      end;
      if bAddSpecB then
      begin
        with fmDobSpec do
        begin
          if teSpecB.Active=True then
          begin
            prFindSM(quCardsSelIMESSURE.AsInteger,sM,iM); //����� � ��������

            iMax:=1;
            teSpecB.First;
            if not teSpecB.Eof then
            begin
              teSpecB.Last;
              iMax:=teSpecBID.AsInteger+1;
            end;

            teSpecB.Append;
            teSpecBID.AsInteger:=iMax;
            teSpecBSIFR.AsInteger:=0;
            teSpecBNAMEB.AsString:='';
            teSpecBCODEB.AsString:=quCardsSelID.AsString;
            teSpecBKB.AsFloat:=1;
            teSpecBDSUM.AsFloat:=0;
            teSpecBRSUM.AsFloat:=0;
            teSpecBQUANT.AsFloat:=1;
            teSpecBIDCARD.AsInteger:=quCardsSelID.AsInteger;
            teSpecBIDM.AsInteger:=iM;
            teSpecBNAME.AsString:=quCardsSelNAME.AsString;
            teSpecBNAMESHORT.AsString:=sM;
            teSpecBKM.AsFloat:=1;
            teSpecBPRICER.AsFloat:=0;
            teSpecBTCard.AsInteger:=quCardsSelTCARD.AsInteger;
            teSpecBSALET.AsInteger:=0;
            teSpecBSSALET.AsString:='';
            teSpecB.Post;
          end;
        end;
      end;
      if bAddSpecInv then
      begin
        if fmAddInv.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            iMax:=1;
            fmAddInv.taSpec1.First;
            if not fmAddInv.taSpec1.Eof then
            begin
              fmAddInv.taSpec1.Last;
              iMax:=fmAddInv.taSpec1Num.AsInteger+1;
            end;

            with fmAddInv do
            begin
              ViewInv.BeginUpdate;
              if taSpec1.Locate('IdGoods',quCardsSelID.AsInteger,[])=False then
              begin
                taSpec1.Append;
                taSpec1Num.AsInteger:=iMax;
                taSpec1IdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpec1NameG.AsString:=quCardsSelNAME.AsString;
                taSpec1IM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpec1SM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpec1Km.AsFloat:=quCardsselKOEF.AsFloat;
                taSpec1QuantFact.AsFloat:=0;
                taSpec1PriceIn.AsFloat:=0;
                taSpec1SumIn.AsFloat:=0;
                taSpec1PriceIn0.AsFloat:=0;
                taSpec1SumIn0.AsFloat:=0;
                taSpec1PriceUch.AsFloat:=0;
                taSpec1SumUch.AsFloat:=0;
                taSpec1PriceInF.AsFloat:=0;
                taSpec1SumInF.AsFloat:=0;
                taSpec1PriceInF0.AsFloat:=0;
                taSpec1SumInF0.AsFloat:=0;
                taSpec1PriceUchF.AsFloat:=0;
                taSpec1SumUchF.AsFloat:=0;
                taSpec1TCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpec1IdGroup.AsInteger:=quCardsSelPARENT.AsInteger;
//                taSpec1NameGr.AsString:=prFindGrN(quCardsSelPARENT.AsInteger);
                taSpec1NoCalc.AsInteger:=0;
                taSpec1CType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                taSpec1NDSProc.AsFloat:=quCardsSelPROC.AsFloat;
                taSpec1.Post;
//                inc(iMax);
               end;
              ViewInv.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecCompl then
      begin
        if fmAddCompl.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            iMax:=1;
            fmAddCompl.taSpec.First;
            if not fmAddCompl.taSpec.Eof then
            begin
              fmAddCompl.taSpec.Last;
              iMax:=fmAddCompl.taSpecNum.AsInteger+1;
            end;

            with fmAddCompl do
            begin
              ViewCom.BeginUpdate;
              if taSpec.Locate('IdGoods',quCardsSelID.AsInteger,[])=False then
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecQuantFact.AsFloat:=0;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceUch.AsFloat:=0;
                taSpecSumUch.AsFloat:=0;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpec.Post;
               end;
              ViewCom.EndUpdate;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecAO then
      begin
        if fmAddAct.Visible then
        begin
          if not quCardsSel.Eof then
          begin
            if fmAddAct.PageControl1.ActivePageIndex=0 then
            begin
              iMax:=1;
              fmAddAct.taSpecO.First;
              if not fmAddAct.taSpecO.Eof then
              begin
                fmAddAct.taSpecO.Last;
                iMax:=fmAddAct.taSpecONum.AsInteger+1;
              end;

              with fmAddAct do
              begin
                ViewAO.BeginUpdate;
                if taSpecO.Locate('IdGoods',quCardsSelID.AsInteger,[])=False then
                begin
//                  if quCardsSelTCard.AsInteger=0 then
//                  begin
                    taSpecO.Append;
                    taSpecONum.AsInteger:=iMax;
                    taSpecOIdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpecONameG.AsString:=quCardsSelNAME.AsString;
                    taSpecOIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpecOSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpecOQuant.AsFloat:=0;
                    taSpecOPriceIn.AsFloat:=0;
                    taSpecOSumIn.AsFloat:=0;
                    taSpecOPriceUch.AsFloat:=0;
                    taSpecOSumUch.AsFloat:=0;
                    taSpecOKm.AsFloat:=Km;
                    taSpecOPriceIn0.AsFloat:=0;
                    taSpecOSumIn0.AsFloat:=0;
                    taSpecOINds.AsInteger:=quCardsSelINDS.AsINteger;
                    taSpecOSNds.AsString:=quCardsSelNAMENDS.AsString;
                    taSpecORNds.AsFloat:=0;
                    taSpecO.Post;
//                  end else showmessage('���������� ���� � ��� ����������� ���������.');
                end;
                ViewAO.EndUpdate;
              end;
            end;
            if fmAddAct.PageControl1.ActivePageIndex=1 then
            begin
              iMax:=1;
              fmAddAct.taSpecI.First;
              if not fmAddAct.taSpecI.Eof then
              begin
                fmAddAct.taSpecI.Last;
                iMax:=fmAddAct.taSpecINum.AsInteger+1;
              end;

              with fmAddAct do
              begin
                ViewAI.BeginUpdate;
                if taSpecI.Locate('IdGoods',quCardsSelID.AsInteger,[])=False then
                begin
//                  if quCardsSelTCard.AsInteger=0 then
//                  begin
                    taSpecI.Append;
                    taSpecINum.AsInteger:=iMax;
                    taSpecIIdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpecINameG.AsString:=quCardsSelNAME.AsString;
                    taSpecIIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpecISM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpecIQuant.AsFloat:=0;
                    taSpecIPriceIn.AsFloat:=0;
                    taSpecISumIn.AsFloat:=0;
                    taSpecIPriceUch.AsFloat:=0;
                    taSpecISumUch.AsFloat:=0;
                    taSpecIKm.AsFloat:=Km;
                    taSpecIProcPrice.AsFloat:=100;
                    taSpecIPriceIn0.AsFloat:=0;
                    taSpecISumIn0.AsFloat:=0;
                    taSpecIINds.AsInteger:=quCardsSelINDS.AsINteger;
                    taSpecISNds.AsString:=quCardsSelNAMENDS.AsString;
                    taSpecIRNds.AsFloat:=0;
                    taSpecI.Post;
//                  end else showmessage('���������� ���� � ��� ����������� ���������.');
                end;
                ViewAI.EndUpdate;
              end;
            end;
          end else
          begin
            showmessage('�������� ������� ��� ���������� � ������������.');
          end;
        end;
      end;
      if bAddSpecVn then
      begin
        if not quCardsSel.Eof then
        begin
          if fmAddDoc3.PageControl1.ActivePageIndex=0 then
          begin
            iMax:=1;
            fmAddDoc3.taSpec.First;
            if not fmAddDoc3.taSpec.Eof then
            begin
              fmAddDoc3.taSpec.Last;
              iMax:=fmAddDoc3.taSpecNum.AsInteger+1;
            end;

            with fmAddDoc3 do
            begin
              ViewDoc3.BeginUpdate;
              taSpec.Append;
              taSpecNum.AsInteger:=iMax;
              taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
              taSpecNameG.AsString:=quCardsSelNAME.AsString;
              taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
              taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
              taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
              taSpecQuant.AsFloat:=1;
              taSpecPrice1.AsFloat:=0;
              taSpecSum1.AsFloat:=0;
              taSpecPrice2.AsFloat:=0;
              taSpecSum2.AsFloat:=0;
              taSpecPrice3.AsFloat:=0;
              taSpecSum3.AsFloat:=0;
              taSpecSumNac.AsFloat:=0;
              taSpecProcNac.AsFloat:=0;
              taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
              taSpec.Post;
              ViewDoc3.EndUpdate;
            end;
          end;
        end else
        begin
          showmessage('�������� ������� ��� ���������� � ������������.');
        end;
      end;
    end;
  end else
  begin
    if bAddTSpec then
    begin
      //������� ������� ��������� ����� � ������������ ��
      //��� ������� �� ���� �� ������� ��� ����� ������
     { with dmO do
      begin
        kBrutto:=0;
        iCurDate:=prDateToI(Date);
        quFindEU.Active:=False;
        quFindEU.ParamByName('GOODSID').AsInteger:=quCardsSelID.AsInteger;
        quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
        quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
        quFindEU.Active:=True;

        if quFindEU.RecordCount>0 then
        begin
          quFindEU.First;
          kBrutto:=quFindEUTO100GRAMM.AsFloat;
        end;

        quFindEU.Active:=False;
        fmTCard.ViewTSpec.BeginUpdate;
        quTSpec.Append;
        quTSpecIDC.AsInteger:=quTCardsIDCARD.AsInteger;
        quTSpecIDT.AsInteger:=quTCardsID.AsInteger;
        quTSpecIDCARD.AsInteger:=quCardsSelID.AsInteger;
        quTSpecCURMESSURE.AsInteger:=quCardsSelIMESSURE.AsInteger;
        quTSpecNETTO.AsFloat:=100;
        quTSpecBRUTTO.AsFloat:=100+kBrutto;
        quTSpecKNB.AsFloat:=kBrutto;
        quTSpec.Post;
        quTSpec.FullRefresh;
        quTSpec.Locate('IDCARD',quCardsSelID.AsInteger,[]);
        fmTCard.ViewTSpec.EndUpdate;
      end; }
    end else
    begin
      if ViewGoods.Controller.FocusedColumn.Name='ViewGoodsTCARD' then acTCard.Execute
      else acEditGoods.Execute;
    end;
  end;
end;

procedure TfmGoods.acTCardExecute(Sender: TObject);
Var iCurDate:INteger;
    kBrutto,rN:Real;
    iMax:Integer;
begin
  if not CanDo('prViewTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmTCard.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  bSet:=False;
  with dmO do
  begin
    if not quCardsSel.Eof then
    begin
      quTCards.Active:=False;
      quTCards.ParamByName('IDGOOD').AsInteger:=quCardsSelID.AsInteger;
      quTCards.Active:=True;

      fmTCard.Label6.Caption:= IntToStr(quCardsSelID.AsInteger);
      fmTCard.cxTextEdit1.Text:=quCardsSelNAME.AsString;
      fmTCard.cxTextEdit1.Tag:=quCardsSelID.AsInteger;

      CloseTa(fmTCard.taTSpec);
//      fmTCard.taTSpec.First; while not fmTCard.taTSpec.Eof do fmTCard.taTSpec.delete;

      if taTermoObr.Active=False then taTermoObr.Active:=True;

      if quTCards.RecordCount=0 then
      begin
        fmTCard.Panel3.Visible:=False;
        quTSpec.Active:=False;
      end  else
      begin
        with fmTCard do
        begin
          Panel3.Visible:=True;
          iColTC:=0;  //��� ���� ��������� ������������� �� ����

          quTCards.Last;
          cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
          cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
          cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
          cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
          cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

          cxLookupComboBox1.EditValue:=quTCardsIOBR.AsInteger;

          quTSpec.Active:=False;
          quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
          quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
          quTSpec.Active:=True;

          ViewTSpec.BeginUpdate;
          iMax:=1;
          quTSpec.First;
          while not quTSpec.Eof do
          begin
            taTSpec.Append;
            taTSpecNum.AsInteger:=iMax;
            taTSpecIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
            taTSpecIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
            taTSpecSM.AsString:=quTSpecNAMESHORT.AsString;
            taTSpecKm.AsFloat:=quTSpecKOEF.AsFloat;
            taTSpecName.AsString:=quTSpecNAME.AsString;
            taTSpecNetto.AsFloat:=quTSpecNETTO.AsFloat;
            taTSpecBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
            taTSpecKnb.AsFloat:=0;
            taTSpecTCard.AsInteger:=quTSpecTCARD.AsInteger;
            taTSpecNetto1.AsFloat:=quTSpecNETTO1.AsFloat;
            taTSpecComment.AsString:=quTSpecComment.AsString;

            taTSpecPr1.AsFloat:=0;
            if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;

            taTSpecNetto2.AsFloat:=quTSpecNETTO2.AsFloat;
            taTSpecPr2.AsFloat:=0;
            if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;

            taTSpecIOBR.AsInteger:=quTSpecIOBR.AsInteger;
            taTSpec.Post;

            quTSpec.Next;   inc(iMax);
          end;
          quTSpec.Active:=False;

          //������������� ������
          iCurDate:=prDateToI(Date);

          taTSpec.First;
          while not taTSpec.Eof do
          begin
            kBrutto:=0;
            quFindEU.Active:=False;
            quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
            quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
            quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
            quFindEU.Active:=True;

            if quFindEU.RecordCount>0 then
            begin
              quFindEU.First;
              kBrutto:=quFindEUTO100GRAMM.AsFloat;
            end;

            rN:=taTSpecNETTO.AsFloat;
            quFindEU.Active:=False;

            taTSpec.Edit;
            taTSpecBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
            taTSpecKNB.AsFloat:=kBrutto;
            taTSpec.Post;
            taTSpec.Next;
          end;
          taTSpec.First;
          ViewTSpec.EndUpdate;
        end;
      end;

//      fmTCard.ViewTSpec.OptionsData.Editing:=False;
      fmTCard.prColsEnables(False);
      
      fmTCard.cxLabel1.Enabled:=False;
      fmTCard.cxLabel2.Enabled:=False;
      fmTCard.Image1.Enabled:=False;
      fmTCard.Image2.Enabled:=False;
      fmTCard.cxButton1.Enabled:=False;
      fmTCard.cxButton10.Enabled:=False;

      TK.Add:=False;
      TK.Edit:=False;

      fmTCard.Caption:='��������������� �����. '+IntToStr(quCardsSelID.AsInteger)+' '+quCardsSelNAME.AsString;

      fmTCard.Show;
    end else
    begin
      showmessage('�������� ������ ��� �������������� !!');
    end;
  end;
  bSet:=True;
end;

procedure TfmGoods.acCardMoveExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
begin
  //�������� �� ������
  with dmO do
  begin
    if quCardsSel.RecordCount>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
//        if fmSelPerSkl.cxRadioButton1.Checked then iType:=0 else iType:=1;
        // ���� ������� �� ���������� �������� �� ���� ��� ������ - ����� ��������� ����������� ����

        //������ �� ����� � ������

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=quCardsSelID.AsInteger;
        CardSel.Name:=quCardsSelNAME.AsString;
        prFindSM(quCardsSelIMESSURE.AsInteger,CardSel.mesuriment,iM);
        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(quCardsSelID.AsInteger,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,quCardsSelNAME.AsString);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmGoods.ViewGoodsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmGoods.ViewGoodsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iID:INteger;
//    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    if bStartGoods then begin ResetAddVars; exit;  end;
    ResetAddVars;
    if fmMenuCr.cxButton3.Tag=0 then
    begin
      iCo:=fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount;
      if iCo>1 then
      begin
        if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���������� �������, ����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          with dmO do
          begin
            ViewGoods.BeginUpdate;
            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
             //��� ���
              with dmO do
              begin
                if fmMenuCr.quMenuSel.Locate('SIFR',iNum,[]) then
                begin
                  try  //�������� �������� � ��������� ����
                    iId:=GetId('GD');
                    while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
                    begin
                      quGdsFind.Active:=False;
                      quGdsFind.ParamByName('IID').AsInteger:=iId;
                      quGdsFind.Active:=True;
                      if quGdsFind.RecordCount=0 then break
                      else iId:=GetId('GD');
                    end;
                    quGdsFind.Active:=False;
                    if iId=100000 then
                    begin
                      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
                      exit;
                    end;

                    quCardsSel.Append;
                    quCardsSelID.AsInteger:=iId;
                    quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
                    quCardsSelNAME.AsString:=fmMenuCr.quMenuSelNAME.AsString;
                    quCardsSelTTYPE.AsInteger:=1;   //���� �������
                    quCardsSelIMESSURE.AsInteger:=3;  //����� �� ���������
                    quCardsSelINDS.AsInteger:=1;
                    quCardsSelMINREST.AsFloat:=0;
                    quCardsSelIACTIVE.AsInteger:=1;
                    quCardsSelCATEGORY.AsInteger:=1; //�����
                    quCardsSelLASTPRICEOUT.AsFloat:=fmMenuCr.quMenuSelPRICE.AsFloat;
                    quCardsSel.Post;

                    with fmMenuCr do
                    begin
                      trUpdM.StartTransaction;
                      quMenusel.Edit;
                      quMenuSelCODE.AsString:=INtToStr(iId);
                      quMenuSelCONSUMMA.AsFloat:=1;  //����������� =1 �� ���������
                      quMenuSel.Post;
                      trUpdM.Commit;
                      quMenuSel.Refresh;

                      // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                      prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
                      prAddKb.ParamByName('ITYPE').AsInteger:=1;
                      prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
                      prAddKb.ParamByName('CODEB').AsInteger:=iId;
                      prAddKb.ParamByName('KB').AsFloat:=1;
                      prAddKb.ExecProc;

                    end;
                  except
                  end;
                  quCardsSel.Refresh;
                end;
              end;
            end;
            ViewGoods.EndUpdate;
          end;//}
        end;
      end;
      if iCo=1 then //������ ���� ��������� �������
      begin
        if Sender is TcxGridSite then
        begin
          bAdd:=False;
          k:=1;
          AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
          with dmO do
          if AHitTest.HitTestCode=107 then //������
          begin
            Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
            fmTransM:=tfmTransM.Create(Application);
            for j:=0 to Rec1.ValueCount-1 do
            begin
              if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;
            iNum:=Rec1.Values[j];
            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmTransM do
              begin
                cxTextEdit1.Text:=fmMenuCr.quMenuSelNAME.AsString;
                cxCalcEdit1.EditValue:=StrToINtDef(fmMenuCr.quMenuSelCODE.AsString,0);
                cxCalcEdit2.EditValue:=fmMenuCr.quMenuSelCONSUMMA.AsFloat;

                cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
                cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
                cxCalcEdit4.EditValue:=1;
              end;
              fmTransM.ShowModal;
              if fmTransM.ModalResult=mrYes then
              begin //������
                with fmMenuCr do
                begin
                  trUpdM.StartTransaction;
                  quMenusel.Edit;
                //��� ����
//                quMenuSelNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
                  quMenuSelCODE.AsString:=INtToStr(Trunc(fmTransM.cxCalcEdit3.EditValue));
                  quMenuSelCONSUMMA.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                  quMenuSel.Post;
                  trUpdM.Commit;
                  quMenuSel.Refresh;


                  // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                  prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
                  prAddKb.ParamByName('ITYPE').AsInteger:=1;
                  prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
                  prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                  prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                  prAddKb.ExecProc;

                end;
              end;
              if fmTransM.ModalResult=mrOk then //����������
              begin
                bAdd:=True;
                k:=fmTransM.cxCalcEdit4.EditValue;
              end;
            end else bAdd:=True;
            fmTransM.Release;
          end;
          if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
          with dmO do
          if bAdd then   //����������
          begin
            iId:=GetId('GD');
            while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
            begin
              quGdsFind.Active:=False;
              quGdsFind.ParamByName('IID').AsInteger:=iId;
              quGdsFind.Active:=True;
              if quGdsFind.RecordCount=0 then break
              else iId:=GetId('GD');
            end;
            quGdsFind.Active:=False;
            if iId=100000 then
            begin
              showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
              exit;
            end;

            quCardsSel.Append;
            quCardsSelID.AsInteger:=iId;
            quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
            quCardsSelNAME.AsString:=fmMenuCr.quMenuSelNAME.AsString;
            quCardsSelTTYPE.AsInteger:=1;   //���� �������
            quCardsSelIMESSURE.AsInteger:=3;  //����� �� ���������
            quCardsSelINDS.AsInteger:=1;
            quCardsSelMINREST.AsFloat:=0;
            quCardsSelIACTIVE.AsInteger:=1;
            quCardsSelCATEGORY.AsInteger:=1; //�����
            quCardsSelLASTPRICEOUT.AsFloat:=fmMenuCr.quMenuSelPRICE.AsFloat;
            quCardsSel.Post;

            quCardsSel.Refresh;

            with fmMenuCr do
            begin
              trUpdM.StartTransaction;
              quMenusel.Edit;
              quMenuSelCODE.AsString:=INtToStr(iId);
              quMenuSelCONSUMMA.AsFloat:=k;  //����������� =1 �� ���������
              quMenuSel.Post;
              trUpdM.Commit;
              quMenuSel.Refresh;


              // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
              prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
              prAddKb.ParamByName('ITYPE').AsInteger:=1;
              prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
              prAddKb.ParamByName('CODEB').AsInteger:=iId;
              prAddKb.ParamByName('KB').AsFloat:=k;
              prAddKb.ExecProc;

            end;

          end;
        end;
      end;
    end;
    if fmMenuCr.cxButton3.Tag=1 then
    begin
      iCo:=fmMenuCr.ViewMods.Controller.SelectedRecordCount;
      if iCo>1 then
      begin
        if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���������� �������, ����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          with dmO do
          begin
            ViewGoods.BeginUpdate;
            for i:=0 to fmMenuCr.ViewMods.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMods.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMods.Columns[j].Name='ViewModsSIFR' then break;
              end;

              iNum:=Rec.Values[j];
             //��� ���
              with dmO do
              begin
                if fmMenuCr.quMods.Locate('SIFR',iNum,[]) then
                begin
                  try  //�������� �������� � ��������� ����
                    iId:=GetId('GD');
                    while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
                    begin
                      quGdsFind.Active:=False;
                      quGdsFind.ParamByName('IID').AsInteger:=iId;
                      quGdsFind.Active:=True;
                      if quGdsFind.RecordCount=0 then break
                      else iId:=GetId('GD');
                    end;
                    quGdsFind.Active:=False;
                    if iId=100000 then
                    begin
                      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
                      exit;
                    end;

                    quCardsSel.Append;
                    quCardsSelID.AsInteger:=iId;
                    quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
                    quCardsSelNAME.AsString:=fmMenuCr.quModsNAME.AsString;
                    quCardsSelTTYPE.AsInteger:=1;   //���� �������
                    quCardsSelIMESSURE.AsInteger:=3;  //����� �� ���������
                    quCardsSelINDS.AsInteger:=1;
                    quCardsSelMINREST.AsFloat:=0;
                    quCardsSelIACTIVE.AsInteger:=1;
                    quCardsSelCATEGORY.AsInteger:=1; //�����
                    quCardsSelLASTPRICEOUT.AsFloat:=0;
                    quCardsSel.Post;

                    with fmMenuCr do
                    begin
                      trUpdM.StartTransaction;
                      quMods.Edit;
                      quModsCODEB.AsString:=INtToStr(iId);
                      quModsKB.AsFloat:=1;  //����������� =1 �� ���������
                      quMods.Post;
                      trUpdM.Commit;
                      quMods.Refresh;

                     // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                      prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
                      prAddKb.ParamByName('ITYPE').AsInteger:=2;
                      prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
                      prAddKb.ParamByName('CODEB').AsInteger:=iId;
                      prAddKb.ParamByName('KB').AsFloat:=1;
                      prAddKb.ExecProc;

                    end;
                  except
                  end;
                  quCardsSel.Refresh;
                end;
              end;
            end;
            ViewGoods.EndUpdate;
          end;//}
        end;
      end;
      if iCo=1 then //������ ���� ��������� �������
      begin
        if Sender is TcxGridSite then
        begin
          bAdd:=False;
          k:=1;
          AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
          with dmO do
          if AHitTest.HitTestCode=107 then //������
          begin
            Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
            fmTransM:=tfmTransM.Create(Application);
            for j:=0 to Rec1.ValueCount-1 do
            begin
              if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;
            iNum:=Rec1.Values[j];
            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmTransM do
              begin
                cxTextEdit1.Text:=fmMenuCr.quModsNAME.AsString;
                cxCalcEdit1.EditValue:=StrToINtDef(fmMenuCr.quModsCODEB.AsString,0);
                cxCalcEdit2.EditValue:=fmMenuCr.quModsKB.AsFloat;

                cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
                cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
                cxCalcEdit4.EditValue:=1;
              end;
              fmTransM.ShowModal;
              if fmTransM.ModalResult=mrYes then
              begin //������
                with fmMenuCr do
                begin
                  trUpdM.StartTransaction;
                  quMods.Edit;
                //��� ����
//                quModsNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
                  quModsCODEB.AsString:=INtToStr(Trunc(fmTransM.cxCalcEdit3.EditValue));
                  quModsKB.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                  quMods.Post;
                  trUpdM.Commit;
                  quMods.Refresh;

                 // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                  prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
                  prAddKb.ParamByName('ITYPE').AsInteger:=2;
                  prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
                  prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                  prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                  prAddKb.ExecProc;

                end;
              end;
              if fmTransM.ModalResult=mrOk then //����������
              begin
                bAdd:=True;
                k:=fmTransM.cxCalcEdit4.EditValue;
              end;
            end else bAdd:=True;
            fmTransM.Release;
          end;
          if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
          with dmO do
          if bAdd then   //����������
          begin
            iId:=GetId('GD');
            while iId<100000 do   //��� ���� ��� ����� ����� ������ 100000 �������� ��������� ����� � 0, ������ ��� ��������
            begin
              quGdsFind.Active:=False;
              quGdsFind.ParamByName('IID').AsInteger:=iId;
              quGdsFind.Active:=True;
              if quGdsFind.RecordCount=0 then break
              else iId:=GetId('GD');
            end;
            quGdsFind.Active:=False;
            if iId=100000 then
            begin
              showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
              exit;
            end;

            quCardsSel.Append;
            quCardsSelID.AsInteger:=iId;
            quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
            quCardsSelNAME.AsString:=fmMenuCr.quModsNAME.AsString;
            quCardsSelTTYPE.AsInteger:=1;   //���� �������
            quCardsSelIMESSURE.AsInteger:=3;  //����� �� ���������
            quCardsSelINDS.AsInteger:=1;
            quCardsSelMINREST.AsFloat:=0;
            quCardsSelIACTIVE.AsInteger:=1;
            quCardsSelCATEGORY.AsInteger:=1; //�����
            quCardsSelLASTPRICEOUT.AsFloat:=0;
            quCardsSel.Post;

            quCardsSel.Refresh;

            with fmMenuCr do
            begin
              trUpdM.StartTransaction;
              quMods.Edit;
              quModsCODEB.AsString:=INtToStr(iId);
              quModsKB.AsFloat:=k;  //����������� =1 �� ���������
              quMods.Post;
              trUpdM.Commit;
              quMods.Refresh;

             // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
              prAddKb.ParamByName('IDDB').AsInteger:=fmMenuCr.cxLookupComboBox1.EditValue;
              prAddKb.ParamByName('ITYPE').AsInteger:=2;
              prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
              prAddKb.ParamByName('CODEB').AsInteger:=iId;
              prAddKb.ParamByName('KB').AsFloat:=k;
              prAddKb.ExecProc;

            end;

          end;
        end;
      end;
    end;
  end;
end;

procedure TfmGoods.acInputBExecute(Sender: TObject);
Var i:INteger;
begin
  //��������� � �����
  fmInput:=TfmInput.Create(Application);
  fmInput.ViewF.BeginUpdate;
  try
    with fmInput do
    begin
      quInputF.Active:=False;
      quInputF.ParamByName('IDC').AsInteger:=dmO.quCardsSelID.AsInteger;
      quInputF.ParamByName('CURDATE').AsDateTime:=Trunc(Date+1);
      quInputF.Active:=True;
    end;
  finally
    fmInput.ViewF.EndUpdate;
  end;
  fmInput.ShowModal;
  if fmInput.ModalResult=mrOk then
  begin
    with fmInput do
    begin
      if quInputF.RecordCount>0 then
      begin
        for i:=0 to ClassTree.Items.Count-1 Do
        if Integer(ClassTree.Items[i].Data) = quInputFPARENT.AsInteger Then
        begin
          ClassTree.Items[i].Expand(False);
          ClassTree.Repaint;
          ClassTree.Items[i].Selected:=True;
          Break;
        End;
        delay(10);
        dmO.quCardsSel.First;
        dmO.quCardsSel.locate('ID',quInputFIDC.AsInteger,[]);
      end;
    end;
  end;
  fmInput.quInputF.Active:=False;
  fmInput.Release;
end;

procedure TfmGoods.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewGoods);
end;

procedure TfmGoods.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
  if dmO.quCateg.Active=False then dmO.quCateg.Active:=True;
  if dmO.quCTO.Active=False then dmO.quCTO.Active:=True;
end;

procedure TfmGoods.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
  CurVal.IdMH1:=cxLookupComboBox1.EditValue;
  CurVal.NAMEMH1:=cxLookupComboBox1.Text;
  if ClassTree.Selected<>nil then
  begin
    with dmO do
    begin
      try
        ViewGoods.BeginUpdate;
        quCardsSel.Active:=False;
        quCardsSel.ParamByName('PARENTID').AsInteger:=Integer(ClassTree.Selected.Data);
        quCardsSel.Active:=True;
      finally
        ViewGoods.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmGoods.acReDelExecute(Sender: TObject);
Var iRes,iId:INteger;
begin
  // �������������� ������� � ���������
  StatusBar1.Panels[0].Text:='����� ���� ��������������'; Delay(10);
  with dmO do
  with dmORep do
  begin
    ViewGoods.BeginUpdate;
    taDelCard.Active:=False;
    taDelCard.Active:=True;
    taDelCard.First;
    while not taDelCard.Eof do
    begin
      prDelCard.ParamByName('SIFR').AsInteger:=taDelCardSIFR.AsInteger;
      prDelCard.ExecProc;
      iRes:=prDelCard.ParamByName('RESULT').AsInteger;

      if iRes=1 then
      begin //���� ���������������

        iId:=taDelCardSIFR.AsInteger;

        quGdsFind.Active:=False;
        quGdsFind.ParamByName('IID').AsInteger:=iId;
        quGdsFind.Active:=True;
        if quGdsFind.RecordCount=0 then
        begin
          quCardsSel.Append;
          quCardsSelID.AsInteger:=iId;
          quCardsSelPARENT.AsInteger:=Integer(ClassTree.Selected.Data);
          quCardsSelNAME.AsString:=taDelCardNAME.AsString;
          quCardsSelLASTPRICEOUT.AsFloat:=0;
          quCardsSelTTYPE.AsInteger:=taDelCardTTYPE.AsInteger;
          quCardsSelIMESSURE.AsInteger:=taDelCardIMESSURE.AsInteger;
          quCardsSelINDS.AsInteger:=0;
          quCardsSelMINREST.AsFloat:=0;
          quCardsSelIACTIVE.AsInteger:=0;
          quCardsSelCATEGORY.AsInteger:=1;
          quCardsSelSPISSTORE.AsString:='00000000000000000000';
          quCardsSel.Post;
        end;
      end;
      taDelCard.Next;
    end;
    taDelCard.Active:=False;
    quCardsSel.FullRefresh;
    ViewGoods.EndUpdate;
  end;
  StatusBar1.Panels[0].Text:='������ ��'; Delay(10);
end;

procedure TfmGoods.acCalcTTKExecute(Sender: TObject);
Var  i,j,iNum,iTTk,iCountB:INteger;
  Rec:TcxCustomGridRecord;
  iCurDate,iMax:INteger;
  kBrutto,rN:Real;
  bBludo:Boolean;
  k,rMessure,rPrice,rSum:Real;
begin
  //����������� ������������� �� ��������� ���
  with dmO do
  begin
    if ViewGoods.Controller.SelectedRecordCount>0 then
    begin          //ViewGoodsTCARD
      iNum:=0; iTTK:=0;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          try
            if ViewGoods.Columns[j].Name='ViewGoodsID' then iNum:=Rec.Values[j];
            if ViewGoods.Columns[j].Name='ViewGoodsTCARD' then
            begin
              try
                iTTk:=Rec.Values[j];
              except
                iTTk:=0;
              end;
            end;
          except
          end;  
        end;
      end;

      if (iNum>0) and (iTTk>0) then
      begin  //������� �������
//        showmessage('Ok');
        quTC.Active:=False;
        quTC.ParamByName('IDGOOD').AsInteger:=iNum;
        quTC.Active:=True;

        CloseTa(fmTTKView.taTSpecV);
//      fmTCard.taTSpec.First; while not fmTCard.taTSpec.Eof do fmTCard.taTSpec.delete;

        if quTC.RecordCount=0 then
        begin
          fmTTKView.taTSpecV.Active:=False;
          quTC.Active:=False;
          showmessage('��� ����� �� �������.');
        end  else
        begin
          quTC.Last;

          fmTTKView.Caption:=quTSpecNAME.AsString+' ( c '+FormatDateTime('dd.mm.yyyy',quTCDATEB.AsDateTime)+')';

          fmTTKView.cxTextEdit3.Text:=quTCRECEIPTNUM.AsString;
          fmTTKView.cxTextEdit4.Text:=quTCPOUTPUT.AsString;
          fmTTKView.cxSpinEdit1.EditValue:=quTCPCOUNT.AsInteger;
          fmTTKView.cxCalcEdit1.EditValue:=quTCPVES.AsFloat;

          fmTTKView.ViewTSpecV.BeginUpdate;

          quTSpec.Active:=False;
          quTSpec.ParamByName('IDC').AsInteger:=quTCIDCARD.AsInteger;
          quTSpec.ParamByName('IDTC').AsInteger:=quTCID.AsInteger;
          quTSpec.Active:=True;

          iMax:=1;
          quTSpec.First;
          while not quTSpec.Eof do
          begin
            fmTTKView.taTSpecV.Append;
            fmTTKView.taTSpecVNum.AsInteger:=iMax;
            fmTTKView.taTSpecVIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
            fmTTKView.taTSpecVIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
            fmTTKView.taTSpecVSM.AsString:=quTSpecNAMESHORT.AsString;
            fmTTKView.taTSpecVKm.AsFloat:=quTSpecKOEF.AsFloat;
            fmTTKView.taTSpecVName.AsString:=quTSpecNAME.AsString;
            fmTTKView.taTSpecVNetto.AsFloat:=quTSpecNETTO1.AsFloat;
            fmTTKView.taTSpecVBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
            fmTTKView.taTSpecVKnb.AsFloat:=0;
            fmTTKView.taTSpecVTCard.AsInteger:=quTSpecTCARD.AsInteger;
            fmTTKView.taTSpecVPrice.AsFloat:=0;
            fmTTKView.taTSpecVPrice1000.AsFloat:=0;
            fmTTKView.taTSpecVSumma.AsFloat:=0;
            fmTTKView.taTSpecV.Post;

            quTSpec.Next;   inc(iMax);
          end;
          quTSpec.Active:=False;

          //������������� ������
          iCurDate:=prDateToI(Date);

          fmTTKView.taTSpecV.First;
          while not fmTTKView.taTSpecV.Eof do
          begin
            kBrutto:=0;
            quFindEU.Active:=False;
            quFindEU.ParamByName('GOODSID').AsInteger:=fmTTKView.taTSpecVIDCARD.AsInteger;
            quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
            quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
            quFindEU.Active:=True;

            if quFindEU.RecordCount>0 then
            begin
              quFindEU.First;
              kBrutto:=quFindEUTO100GRAMM.AsFloat;
            end;

            rN:=fmTTKView.taTSpecVNETTO.AsFloat;
            quFindEU.Active:=False;

            fmTTKView.taTSpecV.Edit;
            fmTTKView.taTSpecVBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
            fmTTKView.taTSpecVKNB.AsFloat:=kBrutto;
            fmTTKView.taTSpecV.Post;
            fmTTKView.taTSpecV.Next;
          end;
          fmTTKView.taTSpecV.First;
          quTC.Active:=False;

          //��� ���� - ������� �����

          with fmTTKView do
          begin
            iCountB:=1; //����� ��������� �� ���� ������, �.�. �� 0 ������ - ������ ���, ���� ����� �� ��������� �� 0

            taTSpecV.First;
            while not taTSpecV.Eof do
            begin
              k:=1;
              rMessure:=taTSpecVKm.AsFloat; //�� ��������� ������� � 1-�� �.�.������� ��
              if taTSpecVTCard.AsInteger=1 then bBludo:=True else bBludo:=False;

              if bBludo=False then
              begin
                prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taTSpecVIDCARD.AsInteger;
                prCalcLastPrice1.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
                prCalcLastPrice1.ExecProc;
                rPrice:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
                rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
                if (rMessure<>0) then
                begin
                  if taTSpecVKm.AsFloat<>rMessure then
                  begin
                    k:=taTSpecVKm.AsFloat/rMessure;
                  end;
                end else rMessure:=1; //������� �� ������ ������
                rSum:=RoundEx((taTSpecVBRUTTO.AsFloat/iCountB)*rPrice*k*100)/100;
              end else
              begin //��� ����� - ���� �������� ������������� ����� - ������� ������� - ��������� �� fmCalcSeb
                rSum:=prCalcSebV;
                if taTSpecVBRUTTO.AsFloat<>0 then rPrice:=rSum/(taTSpecVBRUTTO.AsFloat/iCountB)
                else rPrice:=0;
              end;

              taTSpecV.Edit;
              taTSpecVPrice.AsFloat:=rPrice*k;
              taTSpecVPrice1000.AsFloat:=RoundEx(rPrice/rMessure*100)/100;
              taTSpecVSumma.AsFloat:=rSum;
              taTSpecV.Post;

              taTSpecV.Next;
            end;
            taTSpecV.First;
          end;

          fmTTKView.ViewTSpecV.EndUpdate;

          fmTTKView.ViewTSpecVPrice1000.Visible:=True;
          fmTTKView.ViewTSpecVSumma.Visible:=True;

          fmTTKView.ShowModal;
          fmTTKView.taTSpecV.Active:=False;
        end;
      end;
    end;
  end;
end;

procedure TfmGoods.acSetStatusExecute(Sender: TObject);
Var
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    sList:String;
begin
  //���������� ������ ��� ���������� �������
  if ViewGoods.Controller.SelectedRecordCount>0 then
  begin
    fmSetSt:=tfmSetSt.Create(Application);
    fmSetSt.Label1.Caption:='�������� '+IntToStr(ViewGoods.Controller.SelectedRecordCount)+' �������. �������� ������?';
    fmSetSt.ShowModal;

    if fmSetSt.ModalResult=mrOk then
    begin
//      showmessage('Ok');
      sList:='';
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set IACTIVE=1');
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;

      ViewGoods.EndUpdate;
    end;

    if fmSetSt.ModalResult=mrNo then
    begin
//      showmessage('No');
      sList:='';
      ViewGoods.BeginUpdate;
      for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewGoods.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
        end;

        iNum:=Rec.Values[j];
        //��� ��� - ����������
        sList:=sList+','+INtToStr(iNum);
      end;
      if sList>'' then
      begin
        delete(sList,1,1); //������ ������ �������
        with dmO do
        begin
          quE.SQL.Clear;
          quE.SQL.Add('update OF_CARDS set IACTIVE=0');
          quE.SQL.Add('where ID in ('+sList+')');
          quE.ExecQuery;
          delay(10);

          quCardsSel.FullRefresh;
        end;
      end;

      ViewGoods.EndUpdate;
    end;

    fmSetSt.Release;

    {
    if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      with dmO do
      begin
        for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewGoods.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewGoods.Columns[j].Name='ViewGoodsID' then break;
          end;

          iNum:=Rec.Values[j];
          //��� ��� - ����������
    }
  end;
end;

procedure TfmGoods.ViewGoodsCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var iType,i:SmallInt;
begin
  iType:=0;
  for i:=0 to ViewGoods.ColumnCount-1 do
  begin
    if ViewGoods.Columns[i].Name='ViewGoodsIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType<1 then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;
end;

procedure TfmGoods.N14Click(Sender: TObject);
begin
  with dmO do
  begin
    fmAddBGU.taBGUGr.Active:=False;
    fmAddBGU.taBGUGr.Active:=True;
    fmAddBGU.taBGUGr.First;

    fmAddBGU.cxTextEdit1.Text:=quCardsSelNAME.AsString;
    fmAddBGU.cxCalcEdit2.Value:=quCardsSelBB.AsFloat;
    fmAddBGU.cxCalcEdit3.Value:=quCardsSelGG.AsFloat;
    fmAddBGU.cxCalcEdit4.Value:=quCardsSelU1.AsFloat;
    fmAddBGU.cxCalcEdit5.Value:=quCardsSelEE.AsFloat;
  end;

  fmAddBGU.ShowModal;
  fmAddBGU.taBGUGr.Active:=False;
end;

procedure TfmGoods.acGroupEditExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
begin
  //��������� ��������� �� ���������
  if not CanDo('prSetSelectCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit; end;
  with dmO do
  with dmORep do
  begin
    if ViewGoods.Controller.SelectedRecordCount=0 then exit;

    if quAlgClass.Active=False then quAlgClass.Active:=True;
    if quMakers.Active=False then quMakers.Active:=True;

    fmSetCardsParams.Label1.Caption:='�������� - ' +its(ViewGoods.Controller.SelectedRecordCount)+' ��������.';
    fmSetCardsParams.ShowModal;
    if fmSetCardsParams.ModalResult=mrOk then
    begin
      if fmSetCardsParams.cxCheckBox1.Checked
      or fmSetCardsParams.cxCheckBox2.Checked
      or fmSetCardsParams.cxCheckBox3.Checked
      or fmSetCardsParams.cxCheckBox4.Checked
      or fmSetCardsParams.cxCheckBox5.Checked
      or fmSetCardsParams.cxCheckBox6.Checked
      then
      begin
        CloseTe(teBuff);

        for i:=0 to ViewGoods.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewGoods.Controller.SelectedRecords[i];
          iNum:=0;
          for j:=0 to Rec.ValueCount-1 do
            if ViewGoods.Columns[j].Name='ViewGoodsID' then begin iNum:=Rec.Values[j]; break; end;;

          if (iNum>0) then
          begin
            teBuff.Append; teBuffCode.AsInteger:=iNum; teBuff.Post;
          end;
        end;

        fmGoods.StatusBar1.Panels[0].Text:='����� ���� ��������� �������� ��������.'; delay(10);

        ViewGoods.BeginUpdate;

        teBuff.First;
        while not teBuff.Eof do
        begin
          if quCardsSel.Locate('ID',teBuffCode.AsInteger,[]) then
          begin
            quCardsSel.Edit;

//          if fmSetCardsParams.cxCheckBox1.Checked then begin quCardsSelV06.AsInteger:=fmSetCardsParams.cxSpinEdit1.Value end;
//          if fmSetCardsParams.cxCheckBox2.Checked then begin quCardsSelV07.AsInteger:=fmSetCardsParams.cxSpinEdit2.Value end;
//          if fmSetCardsParams.cxCheckBox3.Checked then begin quCardsSelKol.AsInteger:=fmSetCardsParams.cxCalcEdit3.EditValue end;

            if fmSetCardsParams.cxCheckBox4.Checked then quCardsSelVOL.AsFloat:=fmSetCardsParams.cxCalcEdit1.EditValue;

            if fmSetCardsParams.cxCheckBox5.Checked then
            begin
              if fmSetCardsParams.cxLookupComboBox1.EditValue>0 then
                quCardsSelALGCLASS.AsInteger:=fmSetCardsParams.cxLookupComboBox1.EditValue
              else
                quCardsSelALGCLASS.AsInteger:=0;
            end;
            if fmSetCardsParams.cxCheckBox6.Checked then
            begin
              if fmSetCardsParams.cxButtonEdit1.Tag>0 then
                quCardsSelALGMAKER.AsInteger:=fmSetCardsParams.cxButtonEdit1.Tag
              else
                quCardsSelALGMAKER.AsInteger:=0;
            end;

            quCardsSel.Post;
          end;

          teBuff.Next;
        end;

        if teBuff.RecordCount>0 then quCardsSel.FullRefresh;

        teBuff.Active:=False;

        ViewGoods.EndUpdate;
      end;
      fmGoods.StatusBar1.Panels[0].Text:='Ok.'; delay(10);
    end;
  end;
end;

end.
