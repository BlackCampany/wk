object fmSyncCash: TfmSyncCash
  Left = 596
  Top = 217
  BorderStyle = bsDialog
  Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074' '#1082#1072#1089#1089
  ClientHeight = 767
  ClientWidth = 875
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 553
    Height = 748
    Align = alLeft
    Color = clWhite
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 551
      Height = 52
      Align = alTop
      BevelInner = bvLowered
      Color = 16765864
      TabOrder = 0
      object cxButton1: TcxButton
        Left = 44
        Top = 4
        Width = 75
        Height = 41
        Caption = #1057#1090#1072#1088#1090
        TabOrder = 0
        OnClick = cxButton1Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton2: TcxButton
        Left = 440
        Top = 4
        Width = 75
        Height = 41
        Action = acExit
        TabOrder = 1
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton3: TcxButton
        Left = 144
        Top = 4
        Width = 75
        Height = 41
        Caption = #1057#1090#1086#1087
        Enabled = False
        TabOrder = 2
        OnClick = cxButton3Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton4: TcxButton
        Left = 280
        Top = 3
        Width = 75
        Height = 41
        Caption = #1040#1083#1082#1086#1075#1086#1083#1100
        TabOrder = 3
        OnClick = cxButton4Click
        LookAndFeel.Kind = lfOffice11
      end
    end
    object GridSyncCash: TcxGrid
      Left = 1
      Top = 53
      Width = 551
      Height = 694
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      object ViewSyncCash: TcxGridDBBandedTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dsMeC
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        Bands = <
          item
            Width = 107
          end
          item
            Width = 218
          end
          item
            Width = 193
          end>
        object ViewSyncCashCashNum: TcxGridDBBandedColumn
          Caption = #8470' '
          DataBinding.FieldName = 'CashNum'
          Options.Editing = False
          Width = 57
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object ViewSyncCashCashName: TcxGridDBBandedColumn
          Caption = #1050#1072#1089#1089#1072
          DataBinding.FieldName = 'CashName'
          Options.Editing = False
          Width = 164
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object ViewSyncCashCashStart: TcxGridDBBandedColumn
          Caption = #1042#1099#1073#1088#1072#1085#1072
          DataBinding.FieldName = 'CashStart'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 70
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object ViewSyncCashPosCards: TcxGridDBBandedColumn
          Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1082#1072#1088#1090#1086#1095#1077#1082
          DataBinding.FieldName = 'PosCards'
          PropertiesClassName = 'TcxProgressBarProperties'
          Properties.BeginColor = 25088
          Options.Editing = False
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object ViewSyncCashPosDisc: TcxGridDBBandedColumn
          Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1089#1082#1080#1076#1086#1082
          DataBinding.FieldName = 'PosDisc'
          PropertiesClassName = 'TcxProgressBarProperties'
          Properties.BeginColor = 25088
          Options.Editing = False
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 2
        end
        object ViewSyncCashPosClass: TcxGridDBBandedColumn
          Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1082#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1072
          DataBinding.FieldName = 'PosClass'
          PropertiesClassName = 'TcxProgressBarProperties'
          Properties.BeginColor = 25088
          Options.Editing = False
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 3
        end
        object ViewSyncCashPosPers: TcxGridDBBandedColumn
          Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1087#1077#1088#1089#1086#1085#1072#1083#1072
          DataBinding.FieldName = 'PosPers'
          PropertiesClassName = 'TcxProgressBarProperties'
          Properties.BeginColor = 16512
          Visible = False
          Options.Editing = False
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 4
        end
        object ViewSyncCashCashPath: TcxGridDBBandedColumn
          Caption = #1055#1091#1090#1100
          DataBinding.FieldName = 'CashPath'
          Options.Editing = False
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 1
        end
        object ViewSyncCashPosBar: TcxGridDBBandedColumn
          Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1064#1050
          DataBinding.FieldName = 'PosBar'
          PropertiesClassName = 'TcxProgressBarProperties'
          Properties.BeginColor = 25088
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 1
        end
      end
      object LevelSyncCash: TcxGridLevel
        GridView = ViewSyncCash
      end
    end
  end
  object Memo1: TcxMemo
    Left = 564
    Top = 48
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 241
    Width = 265
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 748
    Width = 875
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 300
      end>
  end
  object meC: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 384
    Top = 284
    object meCCashNum: TIntegerField
      FieldName = 'CashNum'
    end
    object meCCashName: TStringField
      FieldName = 'CashName'
      Size = 50
    end
    object meCCashStart: TSmallintField
      FieldName = 'CashStart'
    end
    object meCPosCards: TIntegerField
      FieldName = 'PosCards'
    end
    object meCPosBar: TIntegerField
      FieldName = 'PosBar'
    end
    object meCPosDisc: TIntegerField
      FieldName = 'PosDisc'
    end
    object meCPosClass: TIntegerField
      FieldName = 'PosClass'
    end
    object meCPosPers: TIntegerField
      FieldName = 'PosPers'
    end
    object meCCashPath: TStringField
      FieldName = 'CashPath'
      Size = 300
    end
  end
  object dsMeC: TDataSource
    DataSet = meC
    Left = 388
    Top = 332
  end
  object acSyncCash: TActionManager
    Left = 468
    Top = 336
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      OnExecute = acExitExecute
    end
    object acSetPrice: TAction
      Caption = 'acSetPrice'
      ShortCut = 49228
      OnExecute = acSetPriceExecute
    end
  end
  object teBar1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 604
    Top = 428
    object teBar1Bar: TStringField
      FieldName = 'Bar'
      Size = 14
    end
    object teBar1Articul: TIntegerField
      FieldName = 'Articul'
    end
    object teBar1CardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object teBar1Quant: TFloatField
      FieldName = 'Quant'
    end
    object teBar1Price: TFloatField
      FieldName = 'Price'
    end
  end
  object teBar: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 668
    Top = 428
    object teBarBar: TStringField
      FieldName = 'Bar'
      Size = 14
    end
    object teBarArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teBarCardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object teBarQuant: TFloatField
      FieldName = 'Quant'
    end
    object teBarPrice: TFloatField
      FieldName = 'Price'
    end
  end
  object teDS1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 608
    Top = 500
    object teDS1Articul: TIntegerField
      FieldName = 'Articul'
    end
    object teDS1Comment: TStringField
      FieldName = 'Comment'
      Size = 40
    end
  end
  object teDS: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 668
    Top = 500
    object teDSArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teDSComment: TStringField
      FieldName = 'Comment'
      Size = 40
    end
  end
  object teClass1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 608
    Top = 556
    object teClass1Id: TIntegerField
      FieldName = 'Id'
    end
    object teClass1IdParent: TIntegerField
      FieldName = 'IdParent'
    end
    object teClass1Name: TStringField
      FieldName = 'Name'
      Size = 80
    end
  end
  object teClass0: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 668
    Top = 556
    object teClass0Id: TIntegerField
      FieldName = 'Id'
    end
    object teClass0IdParent: TIntegerField
      FieldName = 'IdParent'
    end
    object teClass0Name: TStringField
      FieldName = 'Name'
      Size = 80
    end
  end
  object teCards1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 604
    Top = 368
    object teCards1Articul: TIntegerField
      FieldName = 'Articul'
    end
    object teCards1Classif: TIntegerField
      FieldName = 'Classif'
    end
    object teCards1Depart: TIntegerField
      FieldName = 'Depart'
    end
    object teCards1Name: TStringField
      FieldName = 'Name'
      Size = 80
    end
    object teCards1Card_type: TSmallintField
      FieldName = 'Card_type'
    end
    object teCards1Messuriment: TSmallintField
      FieldName = 'Messuriment'
    end
    object teCards1Price: TFloatField
      FieldName = 'Price'
    end
    object teCards1Discquant: TFloatField
      FieldName = 'Discquant'
    end
    object teCards1Discount: TFloatField
      FieldName = 'Discount'
    end
    object teCards1Scale: TStringField
      FieldName = 'Scale'
      Size = 10
    end
    object teCards1AVid: TIntegerField
      FieldName = 'AVid'
    end
    object teCards1Krep: TFloatField
      FieldName = 'Krep'
    end
  end
  object teCards: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 668
    Top = 368
    object teCardsArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teCardsClassif: TIntegerField
      FieldName = 'Classif'
    end
    object teCardsDepart: TIntegerField
      FieldName = 'Depart'
    end
    object teCardsName: TStringField
      FieldName = 'Name'
      Size = 80
    end
    object teCardsCard_type: TSmallintField
      FieldName = 'Card_type'
    end
    object teCardsMessuriment: TSmallintField
      FieldName = 'Messuriment'
    end
    object teCardsPrice: TFloatField
      FieldName = 'Price'
    end
    object teCardsDiscquant: TFloatField
      FieldName = 'Discquant'
    end
    object teCardsDiscount: TFloatField
      FieldName = 'Discount'
    end
    object teCardsScale: TStringField
      FieldName = 'Scale'
      Size = 10
    end
    object teCardsAVid: TIntegerField
      FieldName = 'AVid'
    end
    object teCardsKrep: TFloatField
      FieldName = 'Krep'
    end
  end
end
