unit mCountry;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, cxContainer, cxLabel,
  Placemnt, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit;

type
  TfmCountry = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewCountry: TcxGridDBTableView;
    LevelCountry: TcxGridLevel;
    GridCountry: TcxGrid;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    Label10: TcxLabel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    ViewCountryID: TcxGridDBColumn;
    ViewCountryName: TcxGridDBColumn;
    amCountry: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    acCopyList: TAction;
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure ViewCountryDblClick(Sender: TObject);
    procedure acCopyListExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCountry: TfmCountry;
  bClearCountry:Boolean = False;

implementation

uses Un1, MDB, AddSingle, MT;

{$R *.dfm}

procedure TfmCountry.Label1Click(Sender: TObject);
begin
//  Label1.Properties.LabelStyle:=cxlsLowered;
//  Label1.Properties.LabelStyle:=cxlsNormal;
  acAdd.Execute;
end;

procedure TfmCountry.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Label1.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmCountry.Label1MouseLeave(Sender: TObject);
begin
  Label1.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmCountry.Label10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Label10.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmCountry.Label10MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label10MouseLeave(Sender: TObject);
begin
  Label10.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label2MouseLeave(Sender: TObject);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label2.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.FormCreate(Sender: TObject);
begin
  GridCountry.Align:=AlClient;
  ViewCountry.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmCountry.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCountry.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmCountry.Timer1Timer(Sender: TObject);
begin
  if bClearCountry=True then begin StatusBar1.Panels[0].Text:=''; bClearCountry:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearCountry:=True;
end;

procedure TfmCountry.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCountry.acDelExecute(Sender: TObject);
Var iR:Integer;
begin
//�������
  if not CanDo('prDelCountry') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('Country')>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �����������: '+quCountryName.AsString, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewCountry.BeginUpdate;
        try
          try
            iR:=quCountryID.AsInteger;

            quD.SQL.Clear;
            quD.SQL.Add('delete from Country');
            quD.SQL.Add('where ID='+IntToStr(iR));
            quD.ExecSQL;
            
            quCountry.Close;
            quCountry.Open;
          except
            showmessage('������.');
          end;
        finally
          ViewCountry.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmCountry.acEditExecute(Sender: TObject);
Var iR:Integer;
begin
//�������������
  if not CanDo('prEditCountry') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if CountRec('Country')>0 then
    begin
      fmAddSingle.prSetParams('������: ��������������.',0);
      fmAddSingle.cxTextEdit1.Text:=quCountryName.AsString;

      fmAddSingle.ShowModal;
      if fmAddSingle.ModalResult=mrOk then
      begin
        ViewCountry.BeginUpdate;
        try
          try
            iR:=quCountryID.AsInteger;

            quA.SQL.Clear;
            quA.SQL.Add('Update Country Set Name='''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+'''');
            quA.SQL.Add('where ID='+IntToStr(iR));
            quA.ExecSQL;

            quCountry.Active:=False;
            quCountry.Active:=True;
            quCountry.Locate('ID',iR,[]);
          except
            showmessage('������. ��������� ������������ ������ ��������.');
          end;
        finally
          ViewCountry.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmCountry.acAddExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if not CanDo('prAddCountry') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;

  fmAddSingle.prSetParams('������: ����������.',0);
  fmAddSingle.cxTextEdit1.Text:='';
  fmAddSingle.ShowModal;
  if fmAddSingle.ModalResult=mrOk then
  begin
    with dmMC do
    begin
      ViewCountry.BeginUpdate;
      try
        if quCountry.Locate('Name',fmAddSingle.cxTextEdit1.Text,[loCaseInsensitive]) then ShowMessage('���������� ����������. ����� �������� ��� ����������.')
        else
        begin
          iMax:=1;
          if CountRec('Country')>0 then begin quCountry.Last; iMax:=quCountryID.AsInteger+1; end;

          quE.SQL.Clear;
          quE.SQL.Add('INSERT into Country values ('+IntToStr(iMax)+','''+Copy(fmAddSingle.cxTextEdit1.Text,1,30)+''')');
          quE.ExecSQL;

          quCountry.Active:=False;
          quCountry.Active:=True;
          quCountry.Locate('ID',iMax,[]);
        end;
      finally
        ViewCountry.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmCountry.Label10Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmCountry.Label2Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmCountry.Label3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsLowered;
end;

procedure TfmCountry.Label3MouseLeave(Sender: TObject);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Label3.Properties.LabelStyle:=cxlsNormal;
end;

procedure TfmCountry.Label3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmCountry.FormShow(Sender: TObject);
begin
  cxTextEdit1.Clear;
end;

procedure TfmCountry.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  with dmMC do
  begin
    ViewCountry.BeginUpdate;
    try
      quCountry.Locate('Name',cxTextEdit1.Text,[loCaseInsensitive,loPartialKey]);
    finally
      ViewCountry.EndUpdate;
    end;
  end;
end;

procedure TfmCountry.ViewCountryDblClick(Sender: TObject);
begin
  with dmMC do
  begin
    if quCountry.Active and (quCountry.RecordCount>0) then
    begin
      PosP.Id:=quCountryID.AsInteger;
      PosP.Name:=quCountryName.AsString;
      ModalResult:=mrOk;
    end else
    begin
      showmessage('�������� ������...');
    end;
  end;
end;

procedure TfmCountry.acCopyListExecute(Sender: TObject);
begin
  with dmMt do
  begin
    if ptCountry.Active=False then ptCountry.Active:=True else ptCountry.Refresh;
    if ptCountry1.Active=False then ptCountry1.Active:=True else ptCountry1.Refresh;

    ptCountry.First;
    while not ptCountry.Eof do
    begin
      ptCountry1.Append;
      ptCountry1ID.AsInteger:=ptCountryID.AsInteger;
      ptCountry1NAME.AsString:=ptCountryName.AsString;
      ptCountry1ABR.AsString:=' ';
      ptCountry1CODE.AsString:='';
      ptCountry1.Post;

      ptCountry.Next;
    end;

    ShowMessage('����������� ����������� ����� ��');
  end;
end;

end.
