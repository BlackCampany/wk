object fmFindKassir: TfmFindKassir
  Left = 417
  Top = 184
  Width = 610
  Height = 343
  Caption = #1055#1077#1088#1089#1086#1085#1072#1083
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 477
    Top = 0
    Width = 125
    Height = 309
    Align = alRight
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 12
      Top = 8
      Width = 101
      Height = 33
      Caption = #1055#1077#1088#1077#1081#1090#1080
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 12
      Top = 52
      Width = 101
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridFKassir: TcxGrid
    Left = 8
    Top = 12
    Width = 453
    Height = 281
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewFKassir: TcxGridDBTableView
      OnDblClick = ViewFKassirDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = fmKadr.dsquFCassir
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewFKassirID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 47
      end
      object ViewFKassirNAMECASSIR: TcxGridDBColumn
        Caption = #1060#1048#1054
        DataBinding.FieldName = 'NAMECASSIR'
        Width = 197
      end
      object ViewFKassirSDOLG: TcxGridDBColumn
        Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
        DataBinding.FieldName = 'SDOLG'
        Width = 128
      end
      object ViewFKassirIKASSIR: TcxGridDBColumn
        Caption = #1050#1072#1089#1089#1080#1088
        DataBinding.FieldName = 'IKASSIR'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1085#1077#1090
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1050#1072#1089#1089#1080#1088
            Value = 1
          end>
      end
    end
    object LevelFKassir: TcxGridLevel
      GridView = ViewFKassir
    end
  end
end
