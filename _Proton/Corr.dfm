object fmCorr: TfmCorr
  Left = 381
  Top = 403
  Width = 638
  Height = 252
  Caption = #1050#1086#1088#1088#1077#1082#1094#1080#1103' '#1089#1091#1084#1084
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 160
    Top = 20
    Width = 97
    Height = 13
    Caption = #1058#1077#1082#1091#1097#1080#1077' '#1089#1091#1084#1084#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 348
    Top = 20
    Width = 81
    Height = 13
    Caption = #1080#1089#1087#1088#1072#1074#1080#1090#1100' '#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 72
    Width = 60
    Height = 13
    Caption = #1053#1072#1083#1080#1095#1085#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 24
    Top = 104
    Width = 80
    Height = 13
    Caption = #1041#1077#1079#1085#1072#1083#1080#1095#1085#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 24
    Top = 136
    Width = 73
    Height = 13
    Caption = #1057#1082#1080#1076#1082#1072' '#1085#1072#1083'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 24
    Top = 168
    Width = 90
    Height = 13
    Caption = #1057#1082#1080#1076#1082#1072' '#1073#1077#1079#1085#1072#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 144
    Top = 48
    Width = 145
    Height = 149
    BevelInner = bvLowered
    Color = 14408667
    TabOrder = 0
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 12
      Top = 20
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 121
    end
    object cxCurrencyEdit3: TcxCurrencyEdit
      Left = 12
      Top = 52
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxCurrencyEdit5: TcxCurrencyEdit
      Left = 12
      Top = 84
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      TabOrder = 2
      Width = 121
    end
    object cxCurrencyEdit7: TcxCurrencyEdit
      Left = 12
      Top = 116
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
  end
  object Panel2: TPanel
    Left = 324
    Top = 48
    Width = 145
    Height = 149
    BevelInner = bvLowered
    Color = 14408667
    TabOrder = 1
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 12
      Top = 20
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 121
    end
    object cxCurrencyEdit4: TcxCurrencyEdit
      Left = 12
      Top = 52
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxCurrencyEdit6: TcxCurrencyEdit
      Left = 12
      Top = 84
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 2
      Width = 121
    end
    object cxCurrencyEdit8: TcxCurrencyEdit
      Left = 12
      Top = 116
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
  end
  object cxButton1: TcxButton
    Left = 496
    Top = 52
    Width = 109
    Height = 33
    Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
    ModalResult = 1
    TabOrder = 2
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 496
    Top = 160
    Width = 109
    Height = 33
    Caption = #1042#1099#1093#1086#1076
    ModalResult = 2
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
  end
end
