unit DocsSElf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocs4 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsVn: TcxGrid;
    ViewDocsVn: TcxGridDBTableView;
    LevelDocsVn: TcxGridLevel;
    amDocsVn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc4: TAction;
    acEditDoc4: TAction;
    acViewDoc4: TAction;
    acDelDoc4: TAction;
    acOnDoc4: TAction;
    acOffDoc4: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewDocsVnDepart: TcxGridDBColumn;
    ViewDocsVnDateInvoice: TcxGridDBColumn;
    ViewDocsVnNumber: TcxGridDBColumn;
    ViewDocsVnFlowDepart: TcxGridDBColumn;
    ViewDocsVnSummaTovarMove: TcxGridDBColumn;
    ViewDocsVnSummaTara: TcxGridDBColumn;
    ViewDocsVnOprihod: TcxGridDBColumn;
    ViewDocsVnNameD1: TcxGridDBColumn;
    ViewDocsVnNameD2: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc4Execute(Sender: TObject);
    procedure acEditDoc4Execute(Sender: TObject);
    procedure acViewDoc4Execute(Sender: TObject);
    procedure ViewDocsVnDblClick(Sender: TObject);
    procedure acDelDoc4Execute(Sender: TObject);
    procedure acOnDoc4Execute(Sender: TObject);
    procedure acOffDoc4Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

var
  fmDocs4: TfmDocs4;
  bClearDocIn:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, AddDoc4;

{$R *.dfm}

procedure TfmDocs4.prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������
begin
  with dmMC do
  begin
    bOpen:=True;
    {if iT=0 then
    begin
      fmAddDoc4.Caption:='���������: ����������.';
      fmAddDoc4.cxTextEdit1.Text:='';
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc4.cxTextEdit1.Tag:=0;

      fmAddDoc4.cxTextEdit10.Text:='';
      fmAddDoc4.cxTextEdit10.Tag:=0;
      fmAddDoc4.cxDateEdit10.Date:=0;
      fmAddDoc4.cxDateEdit10.Tag:=0;

      fmAddDoc4.cxTextEdit2.Text:='';
      fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc4.cxDateEdit1.Date:=date;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc4.cxDateEdit2.Date:=date;
      fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc4.cxCurrencyEdit1.EditValue:=0;
      fmAddDoc4.cxButtonEdit1.Tag:=1;
      fmAddDoc4.cxButtonEdit1.EditValue:=0;
      fmAddDoc4.cxButtonEdit1.Text:='';
      fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True; quDeparts.First;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quDepartsID.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quDepartsName.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc4.cxLabel1.Enabled:=True;
      fmAddDoc4.cxLabel2.Enabled:=True;
      fmAddDoc4.cxLabel3.Enabled:=True;
      fmAddDoc4.cxLabel4.Enabled:=True;
      fmAddDoc4.cxLabel5.Enabled:=True;
      fmAddDoc4.cxLabel6.Enabled:=True;
      fmAddDoc4.cxButton1.Enabled:=True;

      fmAddDoc4.ViewDoc1.OptionsData.Editing:=True;
      fmAddDoc4.ViewDoc1.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmAddDoc4.Caption:='���������: ��������������.';
      fmAddDoc4.cxTextEdit1.Text:=quTTnInNumber.AsString;
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc4.cxTextEdit1.Tag:=quTTnInID.AsInteger;

      fmAddDoc4.cxTextEdit10.Text:=quTTnInNumber.AsString;
      fmAddDoc4.cxTextEdit10.Tag:=quTTnInID.AsInteger;
      fmAddDoc4.cxDateEdit10.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit10.Tag:=quTTnInDepart.AsInteger;

      fmAddDoc4.cxTextEdit2.Text:=quTTnInSCFNumber.AsString;
      fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
      fmAddDoc4.cxDateEdit1.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddDoc4.cxDateEdit2.Date:=quTTnInSCFDate.AsDateTime;
      fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddDoc4.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;

      fmAddDoc4.Label4.Tag:=quTTnInIndexPost.AsInteger;
      fmAddDoc4.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
      fmAddDoc4.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
      fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc4.cxLabel1.Enabled:=True;
      fmAddDoc4.cxLabel2.Enabled:=True;
      fmAddDoc4.cxLabel3.Enabled:=True;
      fmAddDoc4.cxLabel4.Enabled:=True;
      fmAddDoc4.cxLabel5.Enabled:=True;
      fmAddDoc4.cxLabel6.Enabled:=True;
      fmAddDoc4.cxButton1.Enabled:=True;

      fmAddDoc4.ViewDoc1.OptionsData.Editing:=True;
      fmAddDoc4.ViewDoc1.OptionsData.Deleting:=True;
    end;
    if iT=2 then
    begin
      fmAddDoc4.Caption:='���������: ��������.';
      fmAddDoc4.cxTextEdit1.Text:=quTTnInNumber.AsString;
      fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxTextEdit1.Tag:=quTTnInID.AsInteger;

      fmAddDoc4.cxTextEdit10.Text:=quTTnInNumber.AsString;
      fmAddDoc4.cxTextEdit10.Tag:=quTTnInID.AsInteger;
      fmAddDoc4.cxDateEdit10.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit10.Tag:=quTTnInDepart.AsInteger;

      fmAddDoc4.cxTextEdit2.Text:=quTTnInSCFNumber.AsString;
      fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc4.cxDateEdit1.Date:=quTTnInDateInvoice.AsDateTime;
      fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc4.cxDateEdit2.Date:=quTTnInSCFDate.AsDateTime;
      fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc4.cxCurrencyEdit1.EditValue:=quTTnInSummaTovarPost.AsFloat;

      fmAddDoc4.Label4.Tag:=quTTnInIndexPost.AsInteger;
      fmAddDoc4.cxButtonEdit1.Tag:=quTTnInCodePost.AsInteger;
      fmAddDoc4.cxButtonEdit1.Text:=quTTnInNameCli.AsString;
      fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=True;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc4.cxLookupComboBox1.EditValue:=quTTnInDepart.AsInteger;
      fmAddDoc4.cxLookupComboBox1.Text:=quTTnInNameDep.AsString;
      fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc4.cxLabel1.Enabled:=False;
      fmAddDoc4.cxLabel2.Enabled:=False;
      fmAddDoc4.cxLabel3.Enabled:=False;
      fmAddDoc4.cxLabel4.Enabled:=False;
      fmAddDoc4.cxLabel5.Enabled:=False;
      fmAddDoc4.cxLabel6.Enabled:=False;
      fmAddDoc4.cxButton1.Enabled:=False;

      fmAddDoc4.ViewDoc1.OptionsData.Editing:=False;
      fmAddDoc4.ViewDoc1.OptionsData.Deleting:=False;
    end;}
    bOpen:=False;
  end;
end;

procedure TfmDocs4.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs4.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs4.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsVn.Align:=AlClient;
  ViewDocsVn.RestoreFromIniFile(CurDir+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsVn.StoreToIniFile(CurDir+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocs4.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelDocsIn.Visible then
      begin
        fmDocs4.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs4.ViewDocsVn.BeginUpdate;
        try
          quTTNIn.Active:=False;
          quTTNIn.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNIn.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTNIn.Active:=True;
        finally
          fmDocs4.ViewDocsVn.EndUpdate;
        end;

      end else
      begin
        fmDocs4.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        ViewCards.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
        quDocsInCard.Active:=True;}
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocs4.acAddDoc4Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    CloseTa(fmAddDoc4.taSpecIn);
    fmAddDoc4.acSaveDoc.Enabled:=True;
    fmAddDoc4.Show;
  end;
end;

procedure TfmDocs4.acEditDoc4Execute(Sender: TObject);
Var //IDH:INteger;
//    rSum1,rSum2:Real;
    iC:INteger;
    StrWk:String;
begin
  //�������������
  if not CanDo('prEditDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        prSetValsAddDoc1(1); //0-����������, 1-��������������, 2-��������

        CloseTa(fmAddDoc4.taSpecIn);

        fmAddDoc4.acSaveDoc.Enabled:=True;

//        IDH:=quTTnInID.AsInteger;

        quSpecIn.Active:=False;
        quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
        quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
        quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
        quSpecIn.Active:=True;

        quSpecIn.First;
        iC:=1;
        while not quSpecIn.Eof do
        begin
          with fmAddDoc4 do
          begin
            taSpecIn.Append;
            taSpecInNum.AsInteger:=iC;
            taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
            taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
            taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
            taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
            taSpecInNDSSum.AsFloat:=quSpecInNDSSum.AsFloat;
            taSpecInOutNDSSum.AsFloat:=quSpecInOutNDSSum.AsFloat;
            taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
            taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
            taSpecInKolEdMest.AsFloat:=0;
            taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat+quSpecInKolEdMest.AsFloat;
            taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
            taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
            taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
            taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
            taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
            taSpecInSumCenaTovarPost.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
            taSpecInSumCenaTovar.AsFloat:=quSpecInSumCenaTovar.AsFloat;
            taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
            taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
            taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
            taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
            taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
            taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
            taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
            taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
            taSpecInName.AsString:=quSpecInName.AsString;
            taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

            StrWk:='';
            if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
            taSpecInNameTara.AsString:=StrWk;

            taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
            taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
            taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
            taSpecInSumCenaTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
            taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
            taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
            taSpecIn.Post;
          end;
          quSpecIn.Next; inc(iC);
        end;
        fmAddDoc4.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs4.acViewDoc4Execute(Sender: TObject);
Var iC:INteger;
    StrWk:String;
begin
  //��������
  if not CanDo('prViewDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc1(2); //0-����������, 1-��������������, 2-��������

      CloseTa(fmAddDoc4.taSpecIn);

      fmAddDoc4.acSaveDoc.Enabled:=True;

//        IDH:=quTTnInID.AsInteger;

      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTnInDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTnInDateInvoice.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTnInNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      iC:=1;
      while not quSpecIn.Eof do
      begin
        with fmAddDoc4 do
        begin
          taSpecIn.Append;
          taSpecInNum.AsInteger:=iC;
          taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
          taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
          taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
          taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
          taSpecInNDSSum.AsFloat:=quSpecInNDSSum.AsFloat;
          taSpecInOutNDSSum.AsFloat:=quSpecInOutNDSSum.AsFloat;
          taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
          taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
          taSpecInKolEdMest.AsFloat:=0;
          taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat+quSpecInKolEdMest.AsFloat;
          taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
          taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
          taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
          taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
          taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
          taSpecInSumCenaTovarPost.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
          taSpecInSumCenaTovar.AsFloat:=quSpecInSumCenaTovar.AsFloat;
          taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
          taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
          taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
          taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
          taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
          taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
          taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
          taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
          taSpecInName.AsString:=quSpecInName.AsString;
          taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

          StrWk:='';
          if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
          taSpecInNameTara.AsString:=StrWk;

          taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
          taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
          taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
          taSpecInSumCenaTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
          taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
          taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
          taSpecIn.Post;
        end;
        quSpecIn.Next; inc(iC);
      end;
      fmAddDoc4.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs4.ViewDocsVnDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quTTnInAcStatus.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocs4.acDelDoc4Execute(Sender: TObject);
begin
  if not CanDo('prDelDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTNInLn"');
          quD.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);
          quD.SQL.Clear;
          quD.SQL.Add('Delete from "TTnIn"');
          quD.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quD.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quD.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quD.ExecSQL;
          delay(100);

          prRefrID(quTTnIn,ViewDocsVn);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs4.acOnDoc4Execute(Sender: TObject);
begin
//������������
  with dmMC do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {if prTOFind(Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnInNameDep.AsString+' � '+FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem7.Enabled:=True;
              exit;
            end;
          end;}
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);
         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������)
         // 2 - �������� ����� ������
         // 3 - �������� ��������������
         // 4 - �������� ������

          {
         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������) ��� �������� ������ ��������� ��������� ������
          prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPart.ParamByName('DTYPE').AsInteger:=1;
          prDelPart.ExecProc;
          Delay(100);

          //���� ��������� ������� ������ ���������

          quSpecIn.Active:=False;
          quSpecIn.ParamByName('IDHD').AsInteger:=IDH;
          quSpecIn.Active:=True;

          quSpecIn.First;
          while not quSpecIn.Eof do
          begin
            //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
            rQRemn:=prCalcRemn(quSpecInIDCARD.AsInteger,Trunc(quTTnInDATEDOC.AsDateTime)-1,quTTnInIDSKL.AsInteger);
            if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
            begin
              quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecInIDCARD.AsInteger;
              quClosePartIn.ParamByName('IDSKL').AsInteger:=quTTnInIDSKL.AsInteger;
              quClosePartIn.ParamByName('IDATE').AsInteger:=Trunc(quTTnInDATEDOC.AsDateTime)-1;
              quClosePartIn.ExecQuery;
            end;

            quSpecIn.Next;
          end;
          quSpecIn.Active:=False;

         // 2 - �������� ����� ������
         // 3 - �������� �������������� � ����� ��������� ��� �����
         //     � ��������� ������� ������

          prAddPartIn.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
          i:=quTTnInIDSKL.AsInteger;
          prAddPartIn.ParamByName('IDSKL').AsInteger:=i;
          i:=quTTnInIDCLI.AsInteger;
          prAddPartIn.ParamByName('IDCLI').AsInteger:=i;
          i:=Trunc(quTTnInDATEDOC.AsDateTime);
          prAddPartIn.ParamByName('IDATE').AsInteger:=i;
          prAddPartIn.ExecProc;
          }
         // 4 - �������� ������

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
          quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTnIn,ViewDocsVn);
          
          prWh('������ ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs4.acOffDoc4Execute(Sender: TObject);
//Var iCountPartOut:Integer;
//    bStart:Boolean;
//    StrWk:String;
begin
//��������
  with dmMC do
  begin
    if not CanDo('prOffDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTnInDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    if quTTnIn.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInAcStatus.AsInteger=3 then
      begin
        if MessageDlg('�������� �������� �'+quTTnInNumber.AsString+' �� '+quTTnInNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {
          if prTOFind(Trunc(quTTnInDATEDOC.AsDateTime),quTTnInIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnInNAMEMH.AsString+' � '+quTTnInNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnInDATEDOC.AsDateTime),quTTnInIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem8.Enabled:=True;
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� �� ������� ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quTTnInID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=1;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin //���� ��������, ��� �� ��������� ��������� � �������
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������.('+StrWk+') ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              //����� ������� - ���������� �������� � ������������ ������� ������


              bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quTTnInDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            prWH('����� ... ���� ������.',Memo1); Delay(10);
           // 1 - �������� ��������� ������
           // 2 - ������� ��������� ������ �� ���������
           // 3 - �������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quTTnInID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=1;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quTTnInDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

           // 4 - �������� ������
            quTTnIn.Edit;
            quTTnInAcStatus.AsInteger:=0;
            quTTnIn.Post;
            quTTnIn.Refresh;

            prWH('������ ��.',Memo1); Delay(10);
          end;}

          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=0');
          quE.SQL.Add('where Depart='+its(quTTnInDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTnInDateInvoice.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTnInNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTnIn,ViewDocsVn);

          prWh('������ ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs4.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocs4.acVidExecute(Sender: TObject);
begin
  //���
  with dmMC do
  begin
    {if LevelDocsIn.Visible then
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs4.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs4.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs4.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs4.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsVn.BeginUpdate;
      quTTnIn.Active:=False;
      quTTnIn.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quTTnIn.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quTTnIn.Active:=True;
      ViewDocsVn.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocs4.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs4.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsIn.Visible=False then exit;
  with dmMC do
  begin
    if quTTnIn.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTnInNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnInNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnInNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs4.acCopyExecute(Sender: TObject);
//var Par:Variant;
begin
  //����������
  with dmMC do
  begin
    {if quTTnIn.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnInID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnInID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnInDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnInNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quTTnInIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnInNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnInIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnInNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnInSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnInSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecIn.Active:=True;

      quSpecIn.First;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnInID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecInNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecInIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecInQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecInIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecInSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecInIDM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecInIDM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDoc.Post;

        quSpecIn.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocs4.acInsertDExecute(Sender: TObject);
{Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;}
begin
  // ��������
  with dmMC do
  begin
    {taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prAllViewOff;

          fmAddDoc4.Caption:='���������: ����� ��������.';
          fmAddDoc4.cxTextEdit1.Text:=prGetNum(1,0);
          fmAddDoc4.cxTextEdit1.Tag:=0;
          fmAddDoc4.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxTextEdit2.Text:='';
          fmAddDoc4.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit1.Date:=Date;
          fmAddDoc4.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc4.cxDateEdit2.Date:=Date;
          fmAddDoc4.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc4.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc4.cxCurrencyEdit2.EditValue:=0;

          if taHeadDocIType.AsInteger=1 then
          begin
            fmAddDoc4.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc4.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc4.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc4.cxButtonEdit1.Tag:=0;
            fmAddDoc4.cxButtonEdit1.EditValue:=0;
            fmAddDoc4.cxButtonEdit1.Text:='';
          end;

          fmAddDoc4.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc4.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc4.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc4.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quDeparts.Active=False then quDeparts.Active:=True;
          quDeparts.FullRefresh;

          if quDeparts.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc4.Label15.Caption:='��. ����: '+quDepartsNAMEPRICE.AsString;
            fmAddDoc4.Label15.Tag:=quDepartsDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc4.Label15.Caption:='��. ����: ';
            fmAddDoc4.Label15.Tag:=0;
          end;

          fmAddDoc4.cxLabel1.Enabled:=True;
          fmAddDoc4.cxLabel2.Enabled:=True;
          fmAddDoc4.cxLabel3.Enabled:=True;
          fmAddDoc4.cxLabel4.Enabled:=True;
          fmAddDoc4.cxLabel5.Enabled:=True;
          fmAddDoc4.cxLabel6.Enabled:=True;
          fmAddDoc4.N1.Enabled:=True;

          fmAddDoc4.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc4.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc4.cxButton1.Enabled:=True;

          CloseTa(fmAddDoc4.taSpec);

          fmAddDoc4.ViewTara.OptionsData.Editing:=True;
          fmAddDoc4.ViewTara.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc4.taTaraS);

          fmAddDoc4.acSaveDoc.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

     //     fmAddDoc4.ShowModal;
          fmAddDoc4.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocs4.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsIn.Visible then
  begin
    prNExportExel4(ViewDocsVn);
  end else
  begin
    prNExportExel4(ViewCards);
  end;
end;

procedure TfmDocs4.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs4.Excel1Click(Sender: TObject);
begin
  prNExportExel4(ViewDocsVn);
end;

end.
