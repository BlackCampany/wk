unit MoveSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit,
  cxImageComboBox, cxCurrencyEdit, cxContainer, cxMemo, dxmdaset,
  cxProgressBar;

type
  TfmMoveSel = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    ViewMoveSel: TcxGridDBTableView;
    LevelMoveSel: TcxGridLevel;
    GridMoveSel: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    amMoveSel: TActionManager;
    acExit: TAction;
    ViewMoveSelID: TcxGridDBColumn;
    ViewMoveSelPARENT: TcxGridDBColumn;
    ViewMoveSelNAME: TcxGridDBColumn;
    ViewMoveSelIMESSURE: TcxGridDBColumn;
    ViewMoveSelMINREST: TcxGridDBColumn;
    ViewMoveSelTCARD: TcxGridDBColumn;
    ViewMoveSelNAMECL: TcxGridDBColumn;
    ViewMoveSelNAMEMESSURE: TcxGridDBColumn;
    ViewMoveSelKOEF: TcxGridDBColumn;
    ViewMoveSelPOSTIN: TcxGridDBColumn;
    ViewMoveSelPOSTOUT: TcxGridDBColumn;
    ViewMoveSelVNIN: TcxGridDBColumn;
    ViewMoveSelVNOUT: TcxGridDBColumn;
    ViewMoveSelINV: TcxGridDBColumn;
    ViewMoveSelQREAL: TcxGridDBColumn;
    ViewMoveSelItog: TcxGridDBColumn;
    Panel1: TPanel;
    acPerSkl: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    acExportEx: TAction;
    LevelRemn: TcxGridLevel;
    ViewRemn: TcxGridDBTableView;
    Memo1: TcxMemo;
    meRemns: TdxMemData;
    meRemnsArticul: TIntegerField;
    meRemnsName: TStringField;
    meRemnsGr: TStringField;
    meRemnsSGr: TStringField;
    meRemnsIdM: TIntegerField;
    meRemnsKM: TFloatField;
    meRemnsSM: TStringField;
    meRemnsTCard: TIntegerField;
    meRemnsQuant: TFloatField;
    meRemnsRSum: TFloatField;
    dsmeRemns: TDataSource;
    ViewRemnRecId: TcxGridDBColumn;
    ViewRemnArticul: TcxGridDBColumn;
    ViewRemnName: TcxGridDBColumn;
    ViewRemnGr: TcxGridDBColumn;
    ViewRemnSGr: TcxGridDBColumn;
    ViewRemnIdM: TcxGridDBColumn;
    ViewRemnSM: TcxGridDBColumn;
    ViewRemnTCard: TcxGridDBColumn;
    ViewRemnQuant: TcxGridDBColumn;
    ViewRemnRSum: TcxGridDBColumn;
    PBar1: TcxProgressBar;
    acMovePos: TAction;
    meRemnsCType: TSmallintField;
    ViewRemnCType: TcxGridDBColumn;
    ViewMoveSelCATEGORY: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPerSklExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ViewRemnDblClick(Sender: TObject);
    procedure ViewMoveSelDblClick(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMoveSel: TfmMoveSel;
  bClearMoveSel: Boolean = False;

implementation

uses Un1, dmOffice, SelPerSkl, CardsMove, DMOReps;

{$R *.dfm}

procedure TfmMoveSel.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridMoveSel.Align:=AlClient;
  ViewMoveSel.RestoreFromIniFile(CurDir+GridIni);
  ViewRemn.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmMoveSel.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmMoveSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMoveSel.StoreToIniFile(CurDir+GridIni,False);
  ViewRemn.StoreToIniFile(CurDir+GridIni,False);
  meRemns.Active:=False;
end;

procedure TfmMoveSel.Timer1Timer(Sender: TObject);
begin
  if bClearMoveSel=True then begin StatusBar1.Panels[0].Text:=''; bClearMoveSel:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMoveSel:=True;
end;

procedure TfmMoveSel.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMoveSel.acPerSklExecute(Sender: TObject);
Var IdPar,iDate:INteger;
    iC,iM:Integer;
    rQ,rSum,rSum0:Real;
    S1,S2:String;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� , ���� ������������.');
  if LevelMoveSel.Visible then
  begin
    //�������� �� ������ �� ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=CommonSet.DateFrom;
      quMHAll1.Active:=False;
      quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=CommonSet.IdStore;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      Cursor:=crHourGlass; Delay(10);
      CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

      if CommonSet.DateTo>=iMaxDate then fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
      with dmO do
      begin
        idPar:=GetId('Pars');
        taParams.Active:=False;
        taParams.Active:=True;
        taParams.Append;
        taParamsID.AsInteger:=idPar;
        taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom);
        taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateTo-1);
        taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
        taParams.Post;
        taParams.Active:=False;

        ViewMovesel.BeginUpdate;
        quMoveSel.Active:=False;
        quMoveSel.Active:=True;
        ViewMovesel.EndUpdate;
      end;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
  if Levelremn.Visible then
  begin
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      if CommonSet.DateTo>=iMaxDate then CommonSet.DateTo:=Date;
      cxDateEdit1.Date:=CommonSet.DateTo;
      quMHAll1.Active:=False;
      quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      cxCheckBox1.Visible:=False;
      Label1.Caption:='�� �����';
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit1.Date)+1;
      iDate:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      fmMoveSel.Caption:='������� �� �� '+CommonSet.NameStore+' �� ����� '+FormatDateTiMe('dd.mm.yyyy',iDate);
      fmMoveSel.LevelRemn.Visible:=True;
      fmMoveSel.LevelMoveSel.Visible:=False;

      Cursor:=crDefault; Delay(10);
      fmMoveSel.Memo1.Clear;
      fmMoveSel.Memo1.Lines.Add('����� ... ���� ������������ ������.');

      fmMoveSel.Show;
      Delay(100);

      fmMoveSel.ViewRemn.BeginUpdate;
      iC:=0;
      CloseTe(fmMoveSel.meRemns);

      with dmO do
      begin
        quRemnDate.Active:=False;
        quRemnDate.Active:=True;
        if quRemnDate.RecordCount>100 then iM:=Trunc(quRemnDate.RecordCount/100)
        else iM:=1;

        fmMoveSel.PBar1.Properties.Min:=0;
        fmMoveSel.PBar1.Properties.Max:=quRemnDate.RecordCount;
        fmMoveSel.PBar1.Visible:=True;

        quRemnDate.First;
        while not quRemnDate.Eof do
        begin
          rQ:=prCalcRemn(quRemnDateID.AsInteger,iDate,CommonSet.IdStore);

//        rQ:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
//        rSum:=prCalcRemnSumF(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);

          if abs(rQ)>=0.001 then
          begin
            rSum:=prCalcRemnSumF(quRemnDateID.AsInteger,iDate,CommonSet.IdStore,rQ,rSum0);

            prFind2group(quRemnDatePARENT.AsInteger,S1,S2);

            meRemns.Append;
            meRemnsArticul.AsInteger:=quRemnDateID.AsInteger;
            meRemnsName.AsString:=quRemnDateNAME.AsString;
            meRemnsGr.AsString:=S1;
            meRemnsSGr.AsString:=S2;
            meRemnsIdM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            meRemnsKM.AsFloat:=quRemnDateKOEF.AsFloat;
            meRemnsSM.AsString:=quRemnDateNAMESHORT.AsString;
            meRemnsTCard.AsInteger:=quRemnDateTCARD.AsInteger;
            if quRemnDateKOEF.AsFloat>0 then meRemnsQuant.AsFloat:=rQ/quRemnDateKOEF.AsFloat
            else meRemnsQuant.AsFloat:=rQ;
            meRemnsRSum.AsFloat:=rSum;
            meRemnsCType.AsInteger:=quRemnDateCATEGORY.AsInteger;
            meRemns.Post;
          end;

          quRemnDate.Next;
          inc(iC);
          if iC mod iM = 0 then
          begin
            fmMoveSel.PBar1.Position:=iC;
            delay(10);
          end;
        end;
      end;
      fmMoveSel.PBar1.Position:=iC; Delay(100);
      fmMoveSel.ViewRemn.EndUpdate;
      fmMoveSel.PBar1.Visible:=False;
      fmMoveSel.Memo1.Lines.Add('������������ ��.');

    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
  Cursor:=crDefault; Delay(10);
  Memo1.Lines.Add('������������ ��.');
end;

procedure TfmMoveSel.acExportExExecute(Sender: TObject);
begin
  // ������� � ������
  if LevelMoveSel.Visible then prNExportExel5(ViewMoveSel);
  if LevelRemn.Visible then prNExportExel5(ViewRemn);
end;

procedure TfmMoveSel.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMoveSel.ViewRemnDblClick(Sender: TObject);
//Var iM:Integer;
begin
  //�������� ������
  with dmO do if not quRemnDate.Eof then
  begin
{    CardSel.Id:=quRemnDateID.AsInteger;
    CardSel.Name:=quRemnDateNAME.AsString;
    prFindSM(quRemnDateIMESSURE.AsInteger,CardSel.mesuriment,iM);
    CardSel.NameGr:=quRemnDateNAMECL.AsString;

    prPreShowMove(quRemnDateID.AsInteger,0,CommonSet.DateTo,CommonSet.IdStore,quRemnDateNAME.AsString,0);
 }
  end;
//  fmCardsMove.PageControl1.ActivePageIndex:=2;
  fmCardsMove.ShowModal;
end;

procedure TfmMoveSel.ViewMoveSelDblClick(Sender: TObject);
Var iM:Integer;
begin
  //�������� ������
  with dmO do if not quMoveSel.Eof then
  begin
    CardSel.Id:=quMoveSelID.AsInteger;
    CardSel.Name:=quMoveSelNAME.AsString;
    prFindSM(quMoveSelIMESSURE.AsInteger,CardSel.mesuriment,iM);
    CardSel.NameGr:=quMoveSelNAMECL.AsString;

    prPreShowMove(quMoveSelID.AsInteger,CommonSet.DateFrom,CommonSet.DateTo,CommonSet.IdStore,quMoveSelNAME.AsString);
  end;
//  fmCardsMove.PageControl1.ActivePageIndex:=2;
  fmCardsMove.ShowModal;
end;

procedure TfmMoveSel.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe,RecC:INteger;
begin
  //�������� �� ������
  with dmO do
  begin
    RecC:=0;
    IdCard:=0;
    iMe:=0;

    if Levelremn.Visible then
    begin
      RecC:=meRemns.RecordCount;
      IdCard:=meRemnsArticul.AsInteger;
      NameC:=meRemnsName.AsString;
      iMe:=meRemnsIdM.AsInteger;
    end;

    if LevelMoveSel.Visible then
    begin
      RecC:=quMoveSel.RecordCount;
      IdCard:=quMoveSelID.AsInteger;
      NameC:=quMoveSelNAME.AsString;
      iMe:=quMoveSelIMESSURE.AsInteger;
    end;

    if RecC>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

end.
