unit LoadCashAVid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxCheckBox, cxProgressBar, Menus, cxLookAndFeelPainters, cxButtons;

type
  TfmCashFBLoadAVid = class(TForm)
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Memo1: TcxMemo;
    cxProgressBar1: TcxProgressBar;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashFBLoadAVid: TfmCashFBLoadAVid;
  bStop:Boolean;
  
implementation

uses MFB, MT, Un1, u2fdk, MDB;

{$R *.dfm}

procedure TfmCashFBLoadAVid.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmCashFBLoadAVid.cxButton1Click(Sender: TObject);
Var iStep,iC:INteger;

  function sTi:String;
  begin
    sTi:=FormatDateTime('hh:nn sss',now)+' ';
  end;

begin
//�������
  with dmFb do
  with dmMT do
  with dmMC do
  begin
    try
      Memo1.Clear;
      cxButton1.Enabled:=False;
      cxButton3.Enabled:=False;
      bStop:=False;
      Memo1.Lines.Add(sTi+'������.');
      Memo1.Lines.Add(sTi+'  ��������� ���� FB.'); delay(10);

      try
        Casher.Close;
        Casher.DBName:=cxTextEdit1.Text;
        Casher.Open;
        if Casher.Connected then
        begin
          Memo1.Lines.Add(sTi+'  ���� FB ��.'); delay(10);
          Memo1.Lines.Add(sTi+'    ������ ���� �� ��� ������� ��.'); delay(10);

          try
            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from fixavid');
            trDel.StartTransaction;
            quDel.ExecQuery;
            trDel.Commit;
            delay(33);
          except on Er:Exception do
          begin
            Memo1.Lines.Add(sTi+'    ������.'+Er.Message); delay(10);
          end;
          end;

          Memo1.Lines.Add(''); delay(10);

          quGetAVid.Active:=False;
          quGetAVid.Active:=True;

          iStep:=quGetAVid.RecordCount div 100;
          if iStep=0 then iStep:=quGetAVid.RecordCount;
          iC:=0;
 
          Memo1.Lines.Add(sTi+'    �������� ��������.('+its(quGetAVid.RecordCount)+')'); delay(10);

          cxProgressBar1.Position:=0;
          cxProgressBar1.Visible:=True;

          taFixAVid.Active:=False;
          taFixAVid.Active:=True;

          quGetAVid.First;
          while not quGetAVid.Eof do
          begin
            taFixAVid.Append;
            taFixAVidIID.AsInteger:=quGetAVidID.AsInteger;
            taFixAVidAVIDNAME.AsString:=quGetAVidNAMECLA.AsString;
            taFixAVidSID.AsString:=quGetAVidID.AsString;
            taFixAVid.Post;

            quGetAVid.Next;
            inc(iC);
            if (iC mod iStep = 0) then
            begin
              cxProgressBar1.Position:=(iC div iStep);
              delay(10);
            end;
            if bStop then break;
          end;

          cxProgressBar1.Position:=100;
          Memo1.Lines.Add(sTi+'    �������� Ok.('+its(iC)+')'); delay(10);
          delay(200);
          cxProgressBar1.Visible:=False;

          taFixAVid.Active:=False;
          quGetAVid.Active:=False;
        end;
        Memo1.Lines.Add(sTi+'  ��������� ���� FB.'); delay(10);
        Casher.Close;
      except
        Memo1.Lines.Add(sTi+'  ������ �������� ���� FB. ������� ����������.'); delay(10);
      end;
    finally
      Memo1.Lines.Add(sTi+'�������� ���������.');
      cxButton1.Enabled:=True;
      cxButton3.Enabled:=True;
    end;
  end;
end;

procedure TfmCashFBLoadAVid.cxButton2Click(Sender: TObject);
begin
  bStop:=True;
  Memo1.Lines.Add('  ���� ��������� �������� ...');
  delay(10);
end;

procedure TfmCashFBLoadAVid.cxButton3Click(Sender: TObject);
begin
  Close;
end;

end.
