object fmAvSpeed: TfmAvSpeed
  Left = 323
  Top = 402
  BorderStyle = bsDialog
  Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078
  ClientHeight = 332
  ClientWidth = 792
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 16
    Width = 31
    Height = 13
    Caption = #1058#1086#1074#1072#1088
    Transparent = True
  end
  object Label2: TLabel
    Left = 20
    Top = 36
    Width = 19
    Height = 13
    Caption = #1050#1086#1076
    Transparent = True
  end
  object Label3: TLabel
    Left = 80
    Top = 16
    Width = 39
    Height = 13
    Caption = 'Label3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 80
    Top = 36
    Width = 39
    Height = 13
    Caption = 'Label4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 20
    Top = 60
    Width = 89
    Height = 13
    Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078
  end
  object Label6: TLabel
    Left = 20
    Top = 88
    Width = 88
    Height = 13
    Caption = #1058#1077#1082#1091#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
  end
  object Label7: TLabel
    Left = 20
    Top = 116
    Width = 77
    Height = 13
    Caption = #1054#1089#1090#1072#1090#1086#1082' '#1074' '#1076#1085#1103#1093
  end
  object Label8: TLabel
    Left = 20
    Top = 148
    Width = 49
    Height = 13
    Caption = #1055#1088#1080#1093#1086#1076#1086#1074
    Transparent = True
  end
  object Label9: TLabel
    Left = 100
    Top = 148
    Width = 39
    Height = 13
    Caption = 'Label9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 20
    Top = 172
    Width = 138
    Height = 13
    Caption = #1052#1072#1082#1089'. '#1082#1086#1083'-'#1074#1086' '#1076#1085#1077#1081' '#1072#1085#1072#1083#1080#1079#1072
    Transparent = True
  end
  object Label11: TLabel
    Left = 176
    Top = 172
    Width = 46
    Height = 13
    Caption = 'Label11'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label12: TLabel
    Left = 20
    Top = 200
    Width = 116
    Height = 13
    Caption = #1053#1077' '#1089#1095#1080#1090#1072#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1080#1077' '
    Transparent = True
  end
  object Label13: TLabel
    Left = 152
    Top = 200
    Width = 46
    Height = 13
    Caption = 'Label13'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label14: TLabel
    Left = 208
    Top = 200
    Width = 24
    Height = 13
    Caption = #1076#1085#1077#1081
  end
  object Label15: TLabel
    Left = 20
    Top = 228
    Width = 75
    Height = 13
    Caption = #1050#1086#1101#1092'. '#1076#1086#1074#1077#1088#1080#1103
    Transparent = True
  end
  object Label16: TLabel
    Left = 176
    Top = 228
    Width = 46
    Height = 13
    Caption = 'Label16'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 792
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 28
      Top = 12
      Width = 105
      Height = 25
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 124
    Top = 56
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.000'
    TabOrder = 1
    Width = 117
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 124
    Top = 84
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.000'
    TabOrder = 2
    Width = 117
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 124
    Top = 112
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.000'
    TabOrder = 3
    Width = 117
  end
  object GrAR: TcxGrid
    Left = 260
    Top = 0
    Width = 532
    Height = 280
    Align = alRight
    TabOrder = 4
    LookAndFeel.Kind = lfOffice11
    object ViewAvReal: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmMT.dsteAvSp
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewAvRealRecId: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewAvRealiDep: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'iDep'
      end
      object ViewAvRealiPrih: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'iPrih'
      end
      object ViewAvRealdDateP: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072
        DataBinding.FieldName = 'dDateP'
        Width = 86
      end
      object ViewAvRealQIn: TcxGridDBColumn
        Caption = #1055#1088#1080#1096#1083#1086
        DataBinding.FieldName = 'QIn'
      end
      object ViewAvRealQReal: TcxGridDBColumn
        Caption = #1056#1077#1072#1083#1080#1079#1086#1074#1072#1085#1086
        DataBinding.FieldName = 'QReal'
      end
      object ViewAvRealiDateReal: TcxGridDBColumn
        Caption = #1044#1085#1077#1081' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'iDateReal'
      end
      object ViewAvRealrAvrR: TcxGridDBColumn
        Caption = #1057#1088#1077#1076#1085#1103#1103' '#1089#1082#1086#1088#1086#1089#1090#1100' '#1074' '#1076#1077#1085#1100
        DataBinding.FieldName = 'rAvrR'
      end
      object ViewAvRealkDov: TcxGridDBColumn
        Caption = #1050' '#1076#1086#1074#1077#1088#1080#1103
        DataBinding.FieldName = 'kDov'
        Width = 40
      end
    end
    object LeAR: TcxGridLevel
      GridView = ViewAvReal
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 116
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
  end
end
