unit Kadr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  dxmdaset, cxImageComboBox, cxContainer, cxTextEdit, cxMemo, FR_DSet,
  FR_DBSet, FR_Class, FileUtil, dxfProgressBar,ShellApi, pvtables,
  sqldataset;

type
  TfmKadr = class(TForm)
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    amKadr: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    ViKassir: TcxGridDBTableView;
    leKassir: TcxGridLevel;
    GrKassir: TcxGrid;
    teKadr: TdxMemData;
    dsteKadr: TDataSource;
    ViKassirID: TcxGridDBColumn;
    ViKassirNAMECASSIR: TcxGridDBColumn;
    ViKassirPASSW: TcxGridDBColumn;
    teKadrID: TIntegerField;
    teKadrNAMECASSIR: TStringField;
    teKadrPASSW: TStringField;
    teKadrIACTIVE: TSmallintField;
    ViKassirIACTIVE: TcxGridDBColumn;
    SpeedItem6: TSpeedItem;
    acLoadToCash: TAction;
    Memo1: TcxMemo;
    teKadrITYPE: TIntegerField;
    teKadrSDOC: TStringField;
    acPrintB: TAction;
    tePrB: TdxMemData;
    tePrBID: TIntegerField;
    tePrBNAME: TStringField;
    tePrBPASSW: TStringField;
    SpeedItem7: TSpeedItem;
    frdstePrB: TfrDBDataSet;
    RepB: TfrReport;
    acLoadToCash1: TAction;
    dxfProgressBar1: TdxfProgressBar;
    acViewDoc: TAction;
    N1: TMenuItem;
    teKadrSDOLG: TStringField;
    ViKassirSDOLG: TcxGridDBColumn;
    tePrBSDOLG: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    quFCassir: TPvQuery;
    quFCassirID: TAutoIncField;
    quFCassirNAMECASSIR: TStringField;
    quFCassirPASSW: TStringField;
    quFCassirIACTIVE: TSmallintField;
    quFCassirISPEC: TSmallintField;
    quFCassirITYPE: TSmallintField;
    quFCassirSDOC: TStringField;
    quFCassirSDOLG: TStringField;
    quFCassirIKASSIR: TSmallintField;
    dsquFCassir: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acLoadToCashExecute(Sender: TObject);
    procedure acPrintBExecute(Sender: TObject);
    procedure acLoadToCash1Execute(Sender: TObject);
    procedure acViewDocExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmKadr: TfmKadr;

implementation

uses MT, Un1, AddAlgClass, u2fdk, MDB, ImpExcel, AddCassir, MFB,
  FindKassir;

{$R *.dfm}

procedure TfmKadr.FormCreate(Sender: TObject);
begin
  GrKassir.Align:=AlClient;
  Memo1.Clear;
  cxTextEdit1.Text:='';
end;

procedure TfmKadr.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViKassir);
end;

procedure TfmKadr.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmKadr.acAddExecute(Sender: TObject);
Var sPassw:String;
    fName:String;
begin
  try
    fmAddCassir:=tfmAddCassir.Create(Application);

    fmAddCassir.cxTextEdit1.Text:='';
    fmAddCassir.cxTextEdit2.Text:='';
    fmAddCassir.cxCheckBox1.Checked:=False;
    fmAddCassir.cxButtonEdit1.Text:='';

    fmAddCassir.ShowModal;
    if fmAddCassir.ModalResult=mrOk then
    begin
      try
        with dmMt do
        with dmMc do
        begin
          ptKadr.Append;
          ptKadrNAMECASSIR.AsString:=AnsiToOemConvert(fmAddCassir.cxTextEdit1.Text);
//          ptKadrPASSW.AsString:=AnsiToOemConvert(fmAddCassir.cxTextEdit2.Text); //ToStandart
          ptKadrPASSW.AsString:='28'; //ToStandart
          ptKadrIACTIVE.AsInteger:=fmAddCassir.cxCheckBox1.EditValue;
          ptKadrISPEC.AsInteger:=0;
          ptKadrITYPE.AsInteger:=2;
          ptKadrSDOC.AsString:='';
          ptKadrIKASSIR.AsInteger:=1;
          ptKadr.Post;

          ptKadr.Refresh;

          sPassw:=its(ptKadrID.AsInteger);

          if ptKadrID.AsInteger<1000000 then
          begin
            sPassw:='1000000';
            sPassw:=ToStandart('28000'+sPassw);

            ptKadr.Edit;
            ptKadrID.AsInteger:=1000000;
            ptKadrPASSW.AsString:=sPassw; //ToStandart
            ptKadr.Post;

          end else
          begin
            sPassw:=its(ptKadrID.AsInteger);
            sPassw:=ToStandart('28000'+sPassw);

            ptKadr.Edit;
            ptKadrPASSW.AsString:=sPassw; //ToStandart
            ptKadr.Post;
          end;

          fName:=fmAddCassir.cxButtonEdit1.Text;
          if fName>'' then
          begin
            if FileExists(fName) then
            begin
              try
                dxfProgressBar1.Visible:=True;
                dxfProgressBar1.Position:=0;
                CopyFileEx(fName,CommonSet.PathFotoDB+its(ptKadrID.AsInteger)+'.pdf',True,False,dxfProgressBar1);
//               DestName: string;OverwriteReadOnly, ShellDialog: Boolean; ProgressControl: TControl);
                dxfProgressBar1.Visible:=False;

                ptKadr.Edit;
                ptKadrSDOC.AsString:=AnsiToOemConvert(CommonSet.PathFotoDB+its(ptKadrID.AsInteger)+'.pdf');
                ptKadr.Post;

              except

              end;
            end;
          end;

          teKadr.Append;
          teKadrID.AsInteger:=ptKadrID.AsInteger;
          teKadrNAMECASSIR.AsString:=OemToAnsiConvert(ptKadrNAMECASSIR.asstring);
          teKadrPASSW.AsString:=sPassw;
          teKadrIACTIVE.AsInteger:=ptKadrIACTIVE.AsInteger;
          teKadrITYPE.AsInteger:=2;
          teKadr.Post;

        end;
      finally
      end;
    end;

  finally
    fmAddCassir.Release;
  end;
end;

procedure TfmKadr.acEditExecute(Sender: TObject);
Var fName:String;
begin
  //�������������
  if (teKadr.RecNo>0)and(teKadrITYPE.AsInteger=2) then
  begin
    try
      fmAddCassir:=tfmAddCassir.Create(Application);

      fmAddCassir.cxTextEdit1.Text:=teKadrNAMECASSIR.AsString;
      fmAddCassir.cxTextEdit2.Text:=teKadrPASSW.AsString;
      fmAddCassir.cxCheckBox1.EditValue:=teKadrIACTIVE.AsInteger;
      fmAddCassir.cxButtonEdit1.Text:='';

      fmAddCassir.ShowModal;
      if fmAddCassir.ModalResult=mrOk then
      begin
        try
          fmKadr.ViKassir.BeginUpdate;
          with dmMt do
          with dmMc do
          begin
            if ptKadr.FindKey([teKadrID.AsInteger]) then
            begin
              ptKadr.Edit;
              ptKadrNAMECASSIR.AsString:=AnsiToOemConvert(fmAddCassir.cxTextEdit1.Text);
//              ptKadrPASSW.AsString:=AnsiToOemConvert(fmAddCassir.cxTextEdit2.Text);
              ptKadrIACTIVE.AsInteger:=fmAddCassir.cxCheckBox1.EditValue;
              ptKadrISPEC.AsInteger:=0;
              ptKadr.Post;

              ptKadr.Refresh;


              fName:=fmAddCassir.cxButtonEdit1.Text;
              if fName>'' then
              begin
                if FileExists(fName) then
                begin
                  try
                    dxfProgressBar1.Visible:=True;
                    dxfProgressBar1.Position:=0;
                    CopyFileEx(fName,CommonSet.PathFotoDB+its(ptKadrID.AsInteger)+'.pdf',True,False,dxfProgressBar1);
//                  DestName: string;OverwriteReadOnly, ShellDialog: Boolean; ProgressControl: TControl);
                    dxfProgressBar1.Visible:=False;

                    ptKadr.Edit;
                    ptKadrSDOC.AsString:=AnsiToOemConvert(CommonSet.PathFotoDB+its(ptKadrID.AsInteger)+'.pdf');
                    ptKadr.Post;
                  except
                  end;
                end;
              end;

              teKadr.Edit;
              teKadrNAMECASSIR.AsString:=OemToAnsiConvert(ptKadrNAMECASSIR.asstring);
//              teKadrPASSW.AsString:=OemToAnsiConvert(ptKadrPASSW.AsString);
              teKadrIACTIVE.AsInteger:=ptKadrIACTIVE.AsInteger;
              teKadr.Post;
            end;
          end;
        finally
          fmKadr.ViKassir.EndUpdate;
        end;
      end;
    finally
      fmAddCassir.Release;
    end;
  end;
end;

procedure TfmKadr.acDelExecute(Sender: TObject);
begin
  if pos('�����',Person.Name)=0 then exit;
  with dmMt do
  with dmMc do
  begin
    if teKadr.RecordCount>0 then
    begin
      if MessageDlg('������� "'+teKadrNAMECASSIR.AsString+'" ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if MessageDlg('��� �������� �������� ��� ������� ������ �� ������� �������. �� �������.',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
        begin
          if ptKadr.FindKey([teKadrID.AsInteger]) then
          begin
            try
              ptKadr.Delete;
              teKadr.Delete;
            finally
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmKadr.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViKassir);
end;

procedure TfmKadr.acLoadToCashExecute(Sender: TObject);
Var sName:String;
begin
// ���������
  with dmMt do
  with dmMc do
  begin
    Memo1.Clear;
    Memo1.Lines.Add('�������� ������ ��������.');
    try
      if ptKadr.Active=False then ptKadr.Open;

      if dmFB.Cash.Connected then
      begin
        Memo1.Lines.Add('  FB ����� ����.');

        prAddPosFB(107,0,0,0,0,0,0,'','','','','',0,0,0,0); //������� �� �������� ����� ������ ��������

        ptKadr.First;
        while not ptKadr.Eof do
        begin
          if (ptKadrIACTIVE.AsInteger=1) and (ptKadrIKASSIR.AsInteger=1) then
          begin
            sName:=OemToAnsiConvert(ptKadrNAMECASSIR.asstring);
            if pos('(',sName)>0 then sName:=Copy(sName,1,pos('(',sName)-1);
            if pos(' ',sName)>0 then sName:=Copy(sName,1,pos(' ',sName)-1);
            if Length(sName)>15 then sName:=Copy(sName,1,15);

            Memo1.Lines.Add('  �������� - '+sName); delay(10);

            prAddPosFB(7,ptKadrID.AsInteger,0,0,0,0,1,OemToAnsiConvert(ptKadrPASSW.AsString),'',sName,'','',0,0,0,0);
          end;

          ptKadr.Next;
        end;

      end else Memo1.Lines.Add('  ������ PX.');
    finally
      Memo1.Lines.Add('  �������� ���������.');
    end;
  end;
end;

procedure TfmKadr.acPrintBExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sPassw,sName,sDolg:String;
begin
  // ������ ��������
  CloseTe(tePrB);

  for i:=0 to ViKassir.Controller.SelectedRecordCount-1 do
  begin
    Rec:=ViKassir.Controller.SelectedRecords[i];
    iNum:=0;  sPassw:=''; sName:=''; sDolg:='';
    for j:=0 to Rec.ValueCount-1 do
    begin
      if ViKassir.Columns[j].Name='ViKassirID' then begin iNum:=Rec.Values[j]; end;
      if ViKassir.Columns[j].Name='ViKassirNAMECASSIR' then begin sName:=Rec.Values[j]; end;
      if ViKassir.Columns[j].Name='ViKassirPASSW' then begin sPassw:=Rec.Values[j]; end;
      if ViKassir.Columns[j].Name='ViKassirSDOLG' then begin sDolg:=Rec.Values[j]; end;
    end;

    if pos('(',sName)>0 then sName:=Copy(sName,1,pos('(',sName)-1);

    tePrB.Append;
    tePrBID.AsInteger:=iNum;
    tePrBNAME.AsString:=sName;
    tePrBPASSW.AsString:=sPassw;
    tePrBSDOLG.AsString:=sDolg;
    tePrB.Post;
  end;

  RepB.LoadFromFile(CommonSet.Reports+'badge.frf');
  RepB.ReportName:='��������.';
  RepB.PrepareReport;
  RepB.ShowPreparedReport;

  CloseTe(tePrB);
end;

procedure TfmKadr.acLoadToCash1Execute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    sName:String;
begin
  for i:=0 to ViKassir.Controller.SelectedRecordCount-1 do
  begin
    Rec:=ViKassir.Controller.SelectedRecords[i];
    iNum:=0;
    for j:=0 to Rec.ValueCount-1 do
    begin
      if ViKassir.Columns[j].Name='ViKassirID' then begin iNum:=Rec.Values[j]; Break; end;
    end;

    if iNum>0 then
    begin
      with dmMt do
      begin
        if ptKadr.FindKey([iNum]) then
        begin
          if ptKadrIKASSIR.AsInteger=1 then
          begin
            sName:=OemToAnsiConvert(ptKadrNAMECASSIR.asstring);
            if pos('(',sName)>0 then sName:=Copy(sName,1,pos('(',sName)-1);
            if pos(' ',sName)>0 then sName:=Copy(sName,1,pos(' ',sName)-1);
            if Length(sName)>15 then sName:=Copy(sName,1,15);

            Memo1.Lines.Add('  �������� - '+sName); delay(10);

            prAddPosFB(7,ptKadrID.AsInteger,0,0,0,0,ptKadrIACTIVE.AsInteger,OemToAnsiConvert(ptKadrPASSW.AsString),'',sName,'','',0,0,0,0);
          end;
        end;
      end;
    end;
  end;
  Memo1.Lines.Add('  �������� ���������.');
end;

procedure TfmKadr.acViewDocExecute(Sender: TObject);
Var fName:String;
begin
 //
  with dmMT do
  begin
    if teKadr.RecordCount>0 then
    begin
      if ptKadr.FindKey([teKadrID.AsInteger]) then
      begin
        fName:=CommonSEt.PathFotoDB+its(teKadrID.AsInteger)+'.pdf';
        if FileExists(fName) then
        begin
          try
            ShellExecute(handle, 'open', PChar('acrord32.exe'),PChar(fName),'', SW_SHOWNORMAL);
          except
          end;  
        end else
        begin
          showmessage('���� - '+fName+'  �� ������.');
        end;
      end;
    end;
  end;
end;

procedure TfmKadr.cxButton1Click(Sender: TObject);
begin
  if Trim(cxTextEdit1.Text)>'' then
  begin
    quFCassir.Active:=False;
    quFCassir.SQL.Clear;
    quFCassir.SQL.Add('select * from "A_CASSIR"');
    quFCassir.SQL.Add('where NAMECASSIR like ''%'+Trim(cxTextEdit1.Text)+'%''');
    quFCassir.Active:=True;
    quFCassir.First;

    if quFCassir.RecordCount=1 then
    begin
      teKadr.Locate('ID',quFCassirID.AsInteger,[]);
      GrKassir.SetFocus;
      ViKassir.Controller.FocusRecord(ViKassir.DataController.FocusedRowIndex,True);

      quFCassir.Active:=False;
    end else
    begin
      if quFCassir.RecordCount>1 then
      begin
        fmFindKassir:=tfmFindKassir.Create(Application);
        fmFindKassir.ShowModal;
        if fmFindKassir.ModalResult=mrOk then
        begin
          teKadr.Locate('ID',quFCassirID.AsInteger,[]);
          GrKassir.SetFocus;
          ViKassir.Controller.FocusRecord(ViKassir.DataController.FocusedRowIndex,True);
        end;

        fmFindKassir.Release;
      end else ShowMessage('������ - '+Trim(cxTextEdit1.Text)+' �� ������.');

      quFCassir.Active:=False;
    end
  end;
end;

end.
