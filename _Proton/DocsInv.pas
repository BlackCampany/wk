unit DocsInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset;

type
  TfmDocs5 = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsInv: TcxGrid;
    ViewDocsInv: TcxGridDBTableView;
    LevelDocsInv: TcxGridLevel;
    amDocsInv: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    acPrint1: TAction;
    frRepDocsInv: TfrReport;
    frquSpecInvSel: TfrDBDataSet;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    ViewDocsInvInventry: TcxGridDBColumn;
    ViewDocsInvDepart: TcxGridDBColumn;
    ViewDocsInvDateInventry: TcxGridDBColumn;
    ViewDocsInvNumber: TcxGridDBColumn;
    ViewDocsInvCardIndex: TcxGridDBColumn;
    ViewDocsInvInputMode: TcxGridDBColumn;
    ViewDocsInvPriceMode: TcxGridDBColumn;
    ViewDocsInvDetail: TcxGridDBColumn;
    ViewDocsInvQuantActual: TcxGridDBColumn;
    ViewDocsInvQuantRecord: TcxGridDBColumn;
    ViewDocsInvSumRecord: TcxGridDBColumn;
    ViewDocsInvSumActual: TcxGridDBColumn;
    ViewDocsInvStatus: TcxGridDBColumn;
    ViewDocsInvNameDep: TcxGridDBColumn;
    ViewDocsInvSumD: TcxGridDBColumn;
    ViewDocsInvItem: TcxGridDBColumn;
    SpeedItem11: TSpeedItem;
    taInvSum: TdxMemData;
    taInvSumIdDepart: TIntegerField;
    taInvSumNameDepart: TStringField;
    taInvSumDateInv: TDateField;
    taInvSumSumR: TFloatField;
    taInvSumSumTO: TFloatField;
    taInvSumSumF: TStringField;
    taInvSumSumDR: TFloatField;
    taInvSumSumDTO: TFloatField;
    frRepRINV: TfrReport;
    frtaInvSum: TfrDBDataSet;
    acAdd1: TAction;
    acCreateDepInv: TAction;
    N1: TMenuItem;
    N5: TMenuItem;
    acDelInv1: TAction;
    acSEtSpecO: TAction;
    acSEtSpecX: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInvDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SpeedItem11Click(Sender: TObject);
    procedure acAdd1Execute(Sender: TObject);
    procedure acCreateDepInvExecute(Sender: TObject);
    procedure acDelInv1Execute(Sender: TObject);
    procedure acSEtSpecOExecute(Sender: TObject);
    procedure acSEtSpecXExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������
    procedure prButton(St:Boolean);
  end;

var
  fmDocs5: TfmDocs5;
  bClearDocIn:Boolean = false;

implementation

uses Un1, MDB, Period1, AddDoc1, AddDoc5, MT, u2fdk, Status, PreInvView,
  MFB;

{$R *.dfm}

procedure TfmDocs5.prButton(St:Boolean);
begin
  SpeedItem1.Enabled:=St;
  SpeedItem2.Enabled:=St;
  SpeedItem3.Enabled:=St;
  SpeedItem4.Enabled:=St;
  SpeedItem5.Enabled:=St;
  SpeedItem6.Enabled:=St;
  SpeedItem7.Enabled:=St;
  SpeedItem8.Enabled:=St;
  SpeedItem9.Enabled:=St;
  SpeedItem10.Enabled:=St;
end;

procedure TfmDocs5.prSetValsAddDoc1(iT:SmallINt); //0-����������, 1-��������������, 2-��������
begin
  with dmMC do
  begin
    bOpen:=True;
    if iT=0 then
    begin
      fmAddDoc5.Caption:='��������������: ����������.';
      fmAddDoc5.cxTextEdit1.Text:='';
      fmAddDoc5.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc5.cxTextEdit1.Tag:=0;

      fmAddDoc5.cxTextEdit10.Text:='';
      fmAddDoc5.cxTextEdit10.Tag:=0;
      fmAddDoc5.cxDateEdit10.Date:=0;
      fmAddDoc5.cxDateEdit10.Tag:=0;

      fmAddDoc5.cxDateEdit1.Date:=date;
      fmAddDoc5.cxDateEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True; quDeparts.First;

      fmAddDoc5.cxLookupComboBox1.EditValue:=quDepartsID.AsInteger;
      fmAddDoc5.cxLookupComboBox1.Text:=quDepartsName.AsString;
      fmAddDoc5.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc5.cxLabel1.Enabled:=True;
      fmAddDoc5.cxLabel2.Enabled:=True;

      fmAddDoc5.cxLabel3.Enabled:=True;
      fmAddDoc5.cxCheckBox2.Enabled:=True;

      fmAddDoc5.cxLabel4.Enabled:=True;
      fmAddDoc5.cxLabel5.Enabled:=True;
      fmAddDoc5.cxLabel6.Enabled:=True;
      fmAddDoc5.cxButton1.Enabled:=True;

      fmAddDoc5.cxComboBox1.ItemIndex:=0; fmAddDoc5.cxComboBox1.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox2.ItemIndex:=0; fmAddDoc5.cxComboBox2.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox3.ItemIndex:=0; fmAddDoc5.cxComboBox3.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox4.ItemIndex:=2; fmAddDoc5.cxComboBox4.Properties.ReadOnly:=False;

      fmAddDoc5.cxCheckBox3.Checked:=True;
      fmAddDoc5.cxCheckBox3.Enabled:=True;

      fmAddDoc5.ViewDoc5.OptionsData.Editing:=True;
//      fmAddDoc5.ViewDoc5.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmAddDoc5.Caption:='��������������: ��������������.';
      fmAddDoc5.cxTextEdit1.Text:=quTTnInvNumber.AsString;
      fmAddDoc5.cxTextEdit1.Properties.ReadOnly:=False;
      fmAddDoc5.cxTextEdit1.Tag:=quTTnInvInventry.AsInteger;

      if pos('Auto',quTTnInvNumber.AsString)>0 then
      begin //������������� ����� ���� ���������� ���� ������
        fmAddDoc5.cxTextEdit1.Properties.ReadOnly:=True;
      end;

      fmAddDoc5.cxTextEdit10.Text:=quTTnInvNumber.AsString;
      fmAddDoc5.cxTextEdit10.Tag:=quTTnInvInventry.AsInteger;
      fmAddDoc5.cxDateEdit10.Date:=quTTnInvDateInventry.AsDateTime;
      fmAddDoc5.cxDateEdit10.Tag:=quTTnInvDepart.AsInteger;

      fmAddDoc5.cxDateEdit1.Date:=quTTnInvDateInventry.AsDateTime;
      fmAddDoc5.cxDateEdit1.Properties.ReadOnly:=False;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc5.cxLookupComboBox1.EditValue:=quTTnInvDepart.AsInteger;
      fmAddDoc5.cxLookupComboBox1.Text:=quTTnInvNameDep.AsString;
      fmAddDoc5.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmAddDoc5.cxLabel1.Enabled:=True;
      fmAddDoc5.cxLabel2.Enabled:=True;
      fmAddDoc5.cxLabel3.Enabled:=True;
      fmAddDoc5.cxCheckBox2.Enabled:=True;

      fmAddDoc5.cxLabel4.Enabled:=True;
      fmAddDoc5.cxLabel5.Enabled:=True;
      fmAddDoc5.cxLabel6.Enabled:=True;
      fmAddDoc5.cxButton1.Enabled:=True;

      fmAddDoc5.cxComboBox1.ItemIndex:=quTTnInvCardIndex.AsInteger; fmAddDoc5.cxComboBox1.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox2.ItemIndex:=quTTnInvInputMode.AsInteger; fmAddDoc5.cxComboBox2.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox3.ItemIndex:=quTTnInvPriceMode.AsInteger; fmAddDoc5.cxComboBox3.Properties.ReadOnly:=False;
      fmAddDoc5.cxComboBox4.ItemIndex:=quTTnInvDetail.AsInteger; fmAddDoc5.cxComboBox4.Properties.ReadOnly:=False;

      if quTTnInvItem.AsInteger=1 then fmAddDoc5.cxCheckBox3.Checked:=True
      else fmAddDoc5.cxCheckBox3.Checked:=False;
      fmAddDoc5.cxCheckBox3.Enabled:=True;

      fmAddDoc5.ViewDoc5.OptionsData.Editing:=True;
//      fmAddDoc5.ViewDoc5.OptionsData.Deleting:=True;
    end;
    if iT=2 then
    begin
      fmAddDoc5.Caption:='��������������: ��������.';
      fmAddDoc5.cxTextEdit1.Text:=quTTnInvNumber.AsString;
      fmAddDoc5.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc5.cxTextEdit1.Tag:=quTTnInvInventry.AsInteger;

      fmAddDoc5.cxTextEdit10.Text:=quTTnInvNumber.AsString;
      fmAddDoc5.cxTextEdit10.Tag:=quTTnInvInventry.AsInteger;
      fmAddDoc5.cxDateEdit10.Date:=quTTnInvDateInventry.AsDateTime;
      fmAddDoc5.cxDateEdit10.Tag:=quTTnInvDepart.AsInteger;

      fmAddDoc5.cxDateEdit1.Date:=quTTnInvDateInventry.AsDateTime;
      fmAddDoc5.cxDateEdit1.Properties.ReadOnly:=True;

      quDeparts.Active:=False; quDeparts.Active:=True;

      fmAddDoc5.cxLookupComboBox1.EditValue:=quTTnInvDepart.AsInteger;
      fmAddDoc5.cxLookupComboBox1.Text:=quTTnInvNameDep.AsString;
      fmAddDoc5.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmAddDoc5.cxLabel1.Enabled:=False;
      fmAddDoc5.cxLabel2.Enabled:=False;
      fmAddDoc5.cxLabel3.Enabled:=False;
      fmAddDoc5.cxCheckBox2.Enabled:=False;

      fmAddDoc5.cxLabel4.Enabled:=False;
      fmAddDoc5.cxLabel5.Enabled:=False;
      fmAddDoc5.cxLabel6.Enabled:=False;
      fmAddDoc5.cxButton1.Enabled:=False;

      fmAddDoc5.cxComboBox1.ItemIndex:=quTTnInvCardIndex.AsInteger; fmAddDoc5.cxComboBox1.Properties.ReadOnly:=True;
      fmAddDoc5.cxComboBox2.ItemIndex:=quTTnInvInputMode.AsInteger; fmAddDoc5.cxComboBox2.Properties.ReadOnly:=True;
      fmAddDoc5.cxComboBox3.ItemIndex:=quTTnInvPriceMode.AsInteger; fmAddDoc5.cxComboBox3.Properties.ReadOnly:=True;
      fmAddDoc5.cxComboBox4.ItemIndex:=quTTnInvDetail.AsInteger; fmAddDoc5.cxComboBox4.Properties.ReadOnly:=True;

      if quTTnInvItem.AsInteger=1 then fmAddDoc5.cxCheckBox3.Checked:=True
      else fmAddDoc5.cxCheckBox3.Checked:=False;
      fmAddDoc5.cxCheckBox3.Enabled:=False;

      fmAddDoc5.ViewDoc5.OptionsData.Editing:=False;
      fmAddDoc5.ViewDoc5.OptionsData.Deleting:=False;
    end;
    bOpen:=False;
  end;
end;

procedure TfmDocs5.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocs5.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs5.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsInv.Align:=AlClient;
  ViewDocsInv.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocs5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsInv.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmDocs5.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMC do
    begin
      if LevelDocsInv.Visible then
      begin
        fmDocs5.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

        fmDocs5.ViewDocsInv.BeginUpdate;
        try
          quTTNInv.Active:=False;
          quTTNInv.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
          quTTNInv.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
          quTTNInv.Active:=True;
        finally
          fmDocs5.ViewDocsInv.EndUpdate;
        end;

      end else
      begin
        fmDocs5.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

        ViewCards.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
        quDocsInCard.Active:=True;}
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocs5.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

    CloseTa(fmAddDoc5.taSpecInv);
    fmAddDoc5.acSaveDoc.Enabled:=True;
    fmAddDoc5.cxTextEdit1.Tag:=0;

    if CommonSet.InvDefType=0 then
    begin
      fmAddDoc5.ViewDoc5PriceSS.Visible:=False;
      fmAddDoc5.ViewDoc5SumRSS.Visible:=False;
      fmAddDoc5.ViewDoc5SumDSS.Visible:=False;
      fmAddDoc5.ViewDoc5SumFSS.Visible:=False;

      fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
      fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;

      fmAddDoc5.ViewDoc5QuantC.Visible:=True;
      fmAddDoc5.ViewDoc5SumC.Visible:=True;
      fmAddDoc5.ViewDoc5SumRI.Visible:=True;

      fmAddDoc5.ViewDoc5.Tag:=0;

      fmAddDoc5.cxCheckBox5.Visible:=False;
      fmAddDoc5.cxCheckBox6.Visible:=False;
    end;
    if CommonSet.InvDefType=1 then
    begin
      fmAddDoc5.ViewDoc5PriceSS.Visible:=True;
      fmAddDoc5.ViewDoc5SumRSS.Visible:=True;
      fmAddDoc5.ViewDoc5SumDSS.Visible:=True;
      fmAddDoc5.ViewDoc5SumFSS.Visible:=True;

      fmAddDoc5.ViewDoc5.Tag:=1;
      fmAddDoc5.cxCheckBox5.Visible:=True;
      fmAddDoc5.cxCheckBox6.Visible:=True;

      fmAddDoc5.ViewDoc5SumRTO.Visible:=True;
      fmAddDoc5.ViewDoc5SumDTO.Visible:=True;

      if fmAddDoc5.cxCheckBox3.Checked then
      begin
        fmAddDoc5.ViewDoc5SumRSC.Visible:=True;
        fmAddDoc5.ViewDoc5SumRSSI.Visible:=True;
        fmAddDoc5.ViewDoc5QuantC.Visible:=True;
        fmAddDoc5.ViewDoc5SumC.Visible:=True;
        fmAddDoc5.ViewDoc5SumRI.Visible:=True;
      end else
      begin
        fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
        fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
        fmAddDoc5.ViewDoc5QuantC.Visible:=False;
        fmAddDoc5.ViewDoc5SumC.Visible:=False;
        fmAddDoc5.ViewDoc5SumRI.Visible:=False;
      end;
    end;
    fmAddDoc5.Show;
  end;
end;

procedure TfmDocs5.acEditDoc1Execute(Sender: TObject);
Var IDH,iT:INteger;
    rQuantC,rSumC,rSumRSC:Real;
    iSS:INteger;
begin
  //�������������
  if not CanDo('prEditDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quTTnInv.RecordCount>0 then //���� ��� �������������
    begin
      if quTTnInvStatus.AsString='�' then
      begin
        if quTTnInvDateInventry.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        prButton(False);
        Memo1.Clear;
        Memo1.Lines.Add('����� .... ���� �������� ���������.'); Delay(10);

        iSS:=fSS(quTTnInvDepart.AsInteger);

        prSetValsAddDoc1(1); //0-����������, 1-��������������, 2-��������

        fmAddDoc5.ViewDoc5.BeginUpdate;
        CloseTa(fmAddDoc5.taSpecInv);

        fmAddDoc5.acSaveDoc.Enabled:=True;
        fmAddDoc5.cxTextEdit1.Tag:=quTTnInvInventry.AsInteger;

        IDH:=quTTnInvInventry.AsInteger;
        iT:=quTTnInvxDopStatus.AsInteger;
        if iT=0 then
        begin
          fmAddDoc5.ViewDoc5PriceSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumRSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumDSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumRTO.Visible:=False;
          fmAddDoc5.ViewDoc5SumDTO.Visible:=False;
          fmAddDoc5.ViewDoc5SumFSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
          fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
          fmAddDoc5.ViewDoc5QuantC.Visible:=False;
          fmAddDoc5.ViewDoc5SumC.Visible:=False;
          fmAddDoc5.ViewDoc5SumRI.Visible:=False;

          fmAddDoc5.ViewDoc5.Tag:=iT;
          fmAddDoc5.cxCheckBox5.Visible:=False;
          fmAddDoc5.cxCheckBox6.Visible:=False;

          quSpecInv.Active:=False;
          quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
          quSpecInv.ParamByName('ID').AsInteger:=IDH;
          quSpecInv.Active:=True;

          quSpecInv.First;
          while not quSpecInv.Eof do
          begin
            with fmAddDoc5 do
            begin
              taSpecInv.Append;
              taSpecInvCodeTovar.AsInteger:=quSpecInvItem.AsInteger;
              taSpecInvName.AsString:=quSpecInvName.AsString;
              taSpecInvCodeEdIzm.AsInteger:=quSpecInvEdIzm.AsInteger;
              taSpecInvNDSProc.AsFloat:=quSpecInvNDS.AsFloat;
              taSpecInvPrice.AsFloat:=quSpecInvPrice.AsFloat;
              taSpecInvQuantR.AsFloat:=quSpecInvQuantRecord.AsFloat;
              taSpecInvSumR.AsFloat:=rv(quSpecInvQuantRecord.AsFloat*quSpecInvPrice.AsFloat);
              taSpecInvSumF.AsFloat:=rv(quSpecInvQuantActual.AsFloat*quSpecInvPrice.AsFloat);
              taSpecInvQuantF.AsFloat:=quSpecInvQuantActual.AsFloat;
              taSpecInvQuantD.AsFloat:=quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat;
              taSpecInvSumD.AsFloat:=rv(quSpecInvQuantActual.AsFloat*quSpecInvPrice.AsFloat)-rv(quSpecInvQuantRecord.AsFloat*quSpecInvPrice.AsFloat);

              taSpecInvidSSGr.AsInteger:=quSpecInvGoodsGroupID.AsInteger;
              taSpecInvNum.AsInteger:=quSpecInvIndexPost.AsInteger;

              taSpecInv.Post;
            end;
            quSpecInv.Next;
          end;
        end;
        if iT=1 then      //������ ��� 1
        begin
          if ptInvLine1.Active=False then ptInvLine1.Active:=True;

          fmAddDoc5.ViewDoc5.Tag:=iT;

          if iSS=0 then
          begin
            fmAddDoc5.ViewDoc5PriceSS.Visible:=False;
            fmAddDoc5.ViewDoc5SumRSS.Visible:=False;
            fmAddDoc5.ViewDoc5SumDSS.Visible:=False;
            fmAddDoc5.ViewDoc5SumFSS.Visible:=False;

            fmAddDoc5.cxCheckBox5.Visible:=False;
            fmAddDoc5.cxCheckBox6.Visible:=False;
          end else
          begin
            fmAddDoc5.ViewDoc5PriceSS.Visible:=True;
            fmAddDoc5.ViewDoc5SumRSS.Visible:=True;
            fmAddDoc5.ViewDoc5SumDSS.Visible:=True;
            fmAddDoc5.ViewDoc5SumFSS.Visible:=True;

            fmAddDoc5.cxCheckBox5.Visible:=True;
            fmAddDoc5.cxCheckBox6.Visible:=True;
          end;

          fmAddDoc5.ViewDoc5SumRTO.Visible:=True;
          fmAddDoc5.ViewDoc5SumDTO.Visible:=True;


          if fmAddDoc5.cxCheckBox3.Checked then
          begin
            if iSS=0 then
            begin
              fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
              fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
            end else
            begin
              fmAddDoc5.ViewDoc5SumRSC.Visible:=True;
              fmAddDoc5.ViewDoc5SumRSSI.Visible:=True;
            end;

            fmAddDoc5.ViewDoc5QuantC.Visible:=True;
            fmAddDoc5.ViewDoc5SumC.Visible:=True;
            fmAddDoc5.ViewDoc5SumRI.Visible:=True;
          end else
          begin
            fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
            fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
            fmAddDoc5.ViewDoc5QuantC.Visible:=False;
            fmAddDoc5.ViewDoc5SumC.Visible:=False;
            fmAddDoc5.ViewDoc5SumRI.Visible:=False;
          end;

          ptInvLine1.CancelRange;
          ptInvLine1.SetRange([idh],[idh]);
          ptInvLine1.First;
          while not ptInvLine1.Eof do
          begin
            with fmAddDoc5 do
            begin
              if taCards.FindKey([ptInvLine1ITEM.AsInteger])=False then
                 Memo1.Lines.Add('  - ������ ���� '+its(ptInvLine1ITEM.AsInteger));

              taSpecInv.Append;
              taSpecInvCodeTovar.AsInteger:=ptInvLine1ITEM.AsInteger;
              taSpecInvName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              taSpecInvCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              taSpecInvNDSProc.AsFloat:=taCardsNDS.AsFloat;
              taSpecInvPrice.AsFloat:=ptInvLine1PRICER.AsFloat;
              taSpecInvQuantR.AsFloat:=ptInvLine1QUANTR.AsFloat;
              taSpecInvSumR.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat);
              taSpecInvSumF.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat);

              //�������������� ��������
              if fmAddDoc5.cxCheckBox3.Checked then
              begin
                rQuantC:=ptInvLine1QUANTC.AsFloat;
                rSumC:=ptInvLine1SUMC.AsFloat;
                rSumRSC:=ptInvLine1SUMRSC.AsFloat;
              end else
              begin
                rQuantC:=0;
                rSumC:=0;
                rSumRSC:=0;
              end;

              taSpecInvQuantC.AsFloat:=rQuantC;
              taSpecInvSumC.AsFloat:=rSumC;
              taSpecInvSumRI.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat)+rSumC;

              taSpecInvQuantF.AsFloat:=ptInvLine1QUANTF.AsFloat;

              //����� � ������ ��������� ���� ��� � ��������� ���������
              taSpecInvQuantD.AsFloat:=ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat-rQuantC;
              taSpecInvSumD.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat)-rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat)-rSumC;

              taSpecInvidSSGr.AsInteger:=taCardsGoodsGroupID.AsInteger;
              taSpecInvNum.AsInteger:=ptInvLine1NUM.AsInteger;

              taSpecInvSumRSS.AsFloat:=ptInvLine1SUMRSS.AsFloat;
              taSpecInvSumFSS.AsFloat:=ptInvLine1SUMFSS.AsFloat;

              //�������������� �������� �� �������������
              taSpecInvSumRSC.AsFloat:=rSumRSC;
              taSpecInvSumRSSI.AsFloat:=ptInvLine1SUMRSS.AsFloat+rSumRSC;

              taSpecInvSumDSS.AsFloat:=ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat-rSumRSC;
              if (ptInvLine1QUANTR.AsFloat+rQuantC)<>0 then taSpecInvPriceSS.AsFloat:=(ptInvLine1SUMRSS.AsFloat+rSumRSC)/(ptInvLine1QUANTR.AsFloat+rQuantC)
              else
              begin
                if ptInvLine1QUANTF.AsFloat<>0 then taSpecInvPriceSS.AsFloat:=ptInvLine1SUMFSS.AsFloat/ptInvLine1QUANTF.AsFloat
                else taSpecInvPriceSS.AsFloat:=0;
              end;

              taSpecInvSumRTO.AsFloat:=ptInvLine1SUMDSS.AsFloat;
              taSpecInvSumDTO.AsFloat:=ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMDSS.AsFloat;

              taSpecInv.Post;
            end;

            ptInvLine1.Next;
          end;
        end;
        Memo1.Lines.Add('��.'); Delay(100);
        prButton(True);
        fmAddDoc5.ViewDoc5.EndUpdate;
        fmAddDoc5.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocs5.acViewDoc1Execute(Sender: TObject);
Var iGr:Integer;
    Idh,iT, iTv:Integer;
    rQuantC,rSumC,rSumRSC:Real;
    bAdd:Boolean;
    iSS:INteger;

begin
  //��������
  if not CanDo('prViewDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  with dmMT do
  begin
    if quTTnInv.RecordCount>0 then //���� ��� �������������
    begin
      fmPreInvView.ShowModal;
      if fmPreInvView.ModalResult=mrOk then
      begin
        iTv:=fmPreInvView.cxRadioGroup1.ItemIndex;

        prButton(False);
        Memo1.Clear;
        Memo1.Lines.Add('����� .... ���� �������� ���������.'); Delay(10);

      prSetValsAddDoc1(2); //0-����������, 1-��������������, 2-��������

      fmAddDoc5.acSaveDoc.Enabled:=False;
      fmAddDoc5.cxTextEdit1.Tag:=quTTnInvInventry.AsInteger;

      iSS:=fSS(quTTnInvDepart.AsInteger);

      fmAddDoc5.ViewDoc5.BeginUpdate;
      with fmAddDoc5 do
      begin
        CloseTa(fmAddDoc5.taSpecInv);
        CloseTe(fmAddDoc5.teClass);
//        teClass.Active:=True;
        with dmMT do
        begin
          if ptSGr.Active=False then ptSGr.Active:=True;

          ptSGr.First;
          while not ptSGr.Eof do
          begin
            fmAddDoc5.teClass.Append;
            fmAddDoc5.teClassId.AsInteger:=ptSGrID.AsInteger;
            fmAddDoc5.teClassIdParent.AsInteger:=ptSGrGoodsGroupID.AsInteger;
            fmAddDoc5.teClassNameClass.AsString:=ptSGrName.AsString;
            fmAddDoc5.teClass.Post;

            ptSGr.next;
          end;
          ptSGr.Active:=False;
        end;
      end;

      IDH:=quTTnInvInventry.AsInteger;
      iT:=quTTnInvxDopStatus.AsInteger;
      if iT=0 then
      begin

        fmAddDoc5.ViewDoc5PriceSS.Visible:=False;
        fmAddDoc5.ViewDoc5SumRSS.Visible:=False;
        fmAddDoc5.ViewDoc5SumDSS.Visible:=False;
        fmAddDoc5.ViewDoc5SumFSS.Visible:=False;
        fmAddDoc5.ViewDoc5.Tag:=iT;
        fmAddDoc5.cxCheckBox5.Visible:=False;
        fmAddDoc5.cxCheckBox6.Visible:=False;
        fmAddDoc5.ViewDoc5SumRTO.Visible:=False;
        fmAddDoc5.ViewDoc5SumDTO.Visible:=False;
        fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
        fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
        fmAddDoc5.ViewDoc5QuantC.Visible:=False;
        fmAddDoc5.ViewDoc5SumC.Visible:=False;
        fmAddDoc5.ViewDoc5SumRI.Visible:=False;

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
        quSpecInv.ParamByName('ID').AsInteger:=Idh;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddDoc5 do
          begin
            bAdd:=False;
            if iTv=0 then bAdd:=True;
            if iTv=1 then
            begin
              if (abs(quSpecInvQuantRecord.AsFloat)>0.001)or(abs(quSpecInvQuantActual.AsFloat)>0.001)then bAdd:=True;
            end;
            if iTv=2 then
            begin
              if (abs(quSpecInvQuantRecord.AsFloat)>0.001)or(abs(quSpecInvQuantActual.AsFloat)>0.001)then
              begin
                if abs(quSpecInvQuantRecord.AsFloat-quSpecInvQuantActual.AsFloat)>0.001 then  bAdd:=True;
              end;
            end;

//            if (abs(quSpecInvQuantRecord.AsFloat)>0.001)or(abs(quSpecInvQuantActual.AsFloat)>0)then
//            begin
            if bAdd then
            begin

              taSpecInv.Append;
              taSpecInvCodeTovar.AsInteger:=quSpecInvItem.AsInteger;
              taSpecInvName.AsString:=quSpecInvName.AsString;
              taSpecInvCodeEdIzm.AsInteger:=quSpecInvEdIzm.AsInteger;
              taSpecInvNDSProc.AsFloat:=quSpecInvNDS.AsFloat;
              taSpecInvPrice.AsFloat:=rv(quSpecInvPrice.AsFloat);
              taSpecInvQuantR.AsFloat:=RoundEx(quSpecInvQuantRecord.AsFloat*1000)/1000;
              taSpecInvSumR.AsFloat:=rv(quSpecInvQuantRecord.AsFloat*quSpecInvPrice.AsFloat);
              taSpecInvSumF.AsFloat:=rv(quSpecInvQuantActual.AsFloat*quSpecInvPrice.AsFloat);
              taSpecInvQuantF.AsFloat:=RoundEx(quSpecInvQuantActual.AsFloat*1000)/1000;
              taSpecInvQuantD.AsFloat:=RoundEx((quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat)*1000)/1000;
              taSpecInvSumD.AsFloat:=rv(quSpecInvQuantActual.AsFloat*quSpecInvPrice.AsFloat)-rv(quSpecInvQuantRecord.AsFloat*quSpecInvPrice.AsFloat);

              taSpecInvidSSGr.AsInteger:=quSpecInvGoodsGroupID.AsInteger;

              iGr:=quSpecInvGoodsGroupID.AsInteger;
              if teClass.Locate('Id',iGr,[]) then
              begin
                iGr:=teClassIdParent.AsInteger;
                taSpecInvSSSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end else
              begin
                iGr:=0;
                taSpecInvSSSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
              if iGr>0 then
              begin
                if teClass.Locate('Id',iGr,[]) then
                begin
                  taSpecInvidSGr.AsInteger:=iGr;
                  taSpecInvSSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                  iGr:=teClassIdParent.AsInteger;
                end else
                begin
                  taSpecInvidSGr.AsInteger:=0;
                  taSpecInvSSGr.AsString:='';
                  iGr:=0;
                end;
              end;
              if iGr>0 then
              begin
                if teClass.Locate('Id',iGr,[]) then
                begin
                  taSpecInvidGr.AsInteger:=iGr;
                  taSpecInvSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                end else
                begin
                  taSpecInvidGr.AsInteger:=0;
                  taSpecInvSGr.AsString:='';
                end;
              end;

              //�������� ������� ���� ������ ������ ������ ���� ���������

              if taSpecInvSGr.AsString='' then
              begin
                taSpecInvidGr.AsInteger:=taSpecInvidSGr.AsInteger;
                taSpecInvSGr.AsString:=taSpecInvSSGr.AsString;
              end;

              taSpecInvNum.AsInteger:=quSpecInvIndexPost.AsInteger;

              taSpecInvBarCode.AsString:=quSpecInvBarCode.AsString;

              if CommonSet.Single=1 then
              begin
                taSpecInviVid.AsInteger:=0;
                taSpecInviMaker.AsInteger:=0;
                taSpecInvsMaker.AsString:='';
                taSpecInvVol.AsFloat:=0;
                taSpecInvVolInv.AsFloat:=0;

                if taCards.FindKey([quSpecInvItem.AsInteger]) then
                begin
                  taSpecInviVid.AsInteger:=taCardsV11.AsInteger;
                  taSpecInviMaker.AsInteger:=taCardsV12.AsInteger;
                  taSpecInvsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  taSpecInvVol.AsFloat:=taCardsV10.AsInteger/1000;
                  taSpecInvVolInv.AsFloat:=(taCardsV10.AsInteger/1000)*(RoundEx(quSpecInvQuantActual.AsFloat*1000)/1000);
                end;
              end;

              taSpecInv.Post;
            end;
          end;
          quSpecInv.Next;
        end;
      end;

      if iT=1 then
      begin
        if ptInvLine1.Active=False then ptInvLine1.Active:=True;

        if iSS=0 then
        begin
          fmAddDoc5.ViewDoc5PriceSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumRSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumDSS.Visible:=False;
          fmAddDoc5.ViewDoc5SumFSS.Visible:=False;

          fmAddDoc5.cxCheckBox5.Visible:=False;
          fmAddDoc5.cxCheckBox6.Visible:=False;
        end else
        begin
          fmAddDoc5.ViewDoc5PriceSS.Visible:=True;
          fmAddDoc5.ViewDoc5SumRSS.Visible:=True;
          fmAddDoc5.ViewDoc5SumDSS.Visible:=True;
          fmAddDoc5.ViewDoc5SumFSS.Visible:=True;

          fmAddDoc5.cxCheckBox5.Visible:=True;
          fmAddDoc5.cxCheckBox6.Visible:=True;
        end;

        fmAddDoc5.ViewDoc5.Tag:=iT;

        fmAddDoc5.ViewDoc5SumRTO.Visible:=True;
        fmAddDoc5.ViewDoc5SumDTO.Visible:=True;
        if fmAddDoc5.cxCheckBox3.Checked then
        begin
          fmAddDoc5.ViewDoc5SumRSC.Visible:=True;
          fmAddDoc5.ViewDoc5SumRSSI.Visible:=True;
          fmAddDoc5.ViewDoc5QuantC.Visible:=True;
          fmAddDoc5.ViewDoc5SumC.Visible:=True;
          fmAddDoc5.ViewDoc5SumRI.Visible:=True;

          if iSS=0 then
          begin
            fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
            fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
          end else
          begin
            fmAddDoc5.ViewDoc5SumRSC.Visible:=True;
            fmAddDoc5.ViewDoc5SumRSSI.Visible:=True;
          end;

          fmAddDoc5.ViewDoc5QuantC.Visible:=True;
          fmAddDoc5.ViewDoc5SumC.Visible:=True;
          fmAddDoc5.ViewDoc5SumRI.Visible:=True;
        end else
        begin
          fmAddDoc5.ViewDoc5SumRSC.Visible:=False;
          fmAddDoc5.ViewDoc5SumRSSI.Visible:=False;
          fmAddDoc5.ViewDoc5QuantC.Visible:=False;
          fmAddDoc5.ViewDoc5SumC.Visible:=False;
          fmAddDoc5.ViewDoc5SumRI.Visible:=False;
        end;

        ptInvLine1.CancelRange;
        ptInvLine1.SetRange([idh],[idh]);
        ptInvLine1.First;
        while not ptInvLine1.Eof do
        begin
          with fmAddDoc5 do
          begin
            bAdd:=False;
            if iTv=0 then bAdd:=True;
            if iTv=1 then
            begin
              if (abs(ptInvLine1QUANTR.AsFloat)>0.001)or(abs(ptInvLine1QUANTF.AsFloat)>0.001)then bAdd:=True;
            end;
            if iTv=2 then
            begin
              if (abs(ptInvLine1QUANTR.AsFloat)>0.001)or(abs(ptInvLine1QUANTF.AsFloat)>0.001)then
              begin
                if abs(ptInvLine1QUANTR.AsFloat-ptInvLine1QUANTF.AsFloat)>0.001 then  bAdd:=True;
              end;
            end;

//            if (abs(ptInvLine1QUANTR.AsFloat)>0.001)or(abs(ptInvLine1QUANTF.AsFloat)>0)or(abs(ptInvLine1SUMDSS.AsFloat)>0) then
//            begin
            if bAdd then
            begin
              if taCards.FindKey([ptInvLine1ITEM.AsInteger])=False then
                 Memo1.Lines.Add('  - ������ ���� '+its(ptInvLine1ITEM.AsInteger));


              taSpecInv.Append;
              taSpecInvCodeTovar.AsInteger:=ptInvLine1ITEM.AsInteger;
              taSpecInvName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              taSpecInvCodeEdIzm.AsInteger:=taCardsEdIzm.AsInteger;
              taSpecInvNDSProc.AsFloat:=taCardsNDS.AsFloat;
              taSpecInvPrice.AsFloat:=ptInvLine1PRICER.AsFloat;
              taSpecInvQuantR.AsFloat:=ptInvLine1QUANTR.AsFloat;
              taSpecInvSumR.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat);
              taSpecInvSumF.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat);

              //�������������� ��������
              if fmAddDoc5.cxCheckBox3.Checked then
              begin
                rQuantC:=ptInvLine1QUANTC.AsFloat;
                rSumC:=ptInvLine1SUMC.AsFloat;
                rSumRSC:=ptInvLine1SUMRSC.AsFloat;
              end else
              begin
                rQuantC:=0;
                rSumC:=0;
                rSumRSC:=0;
              end;

              taSpecInvQuantC.AsFloat:=rQuantC;
              taSpecInvSumC.AsFloat:=rSumC;
              taSpecInvSumRI.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat)+rSumC;

              taSpecInvQuantF.AsFloat:=ptInvLine1QUANTF.AsFloat;

              //����� � ������ ��������� ���� ��� � ��������� ���������
              taSpecInvQuantD.AsFloat:=ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat-rQuantC;
              taSpecInvSumD.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat)-rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat)-rSumC;

//              taSpecInvQuantD.AsFloat:=ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat;
//              taSpecInvSumD.AsFloat:=rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTF.AsFloat)-rv(ptInvLine1PRICER.AsFloat*ptInvLine1QUANTR.AsFloat);

              taSpecInvidSSGr.AsInteger:=taCardsGoodsGroupID.AsInteger;

              iGr:=taCardsGoodsGroupID.AsInteger;
              if teClass.Locate('Id',iGr,[]) then
              begin
                iGr:=teClassIdParent.AsInteger;
                taSpecInvSSSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
              end;
              if iGr>0 then
              begin
                if teClass.Locate('Id',iGr,[]) then
                begin
                  taSpecInvidSGr.AsInteger:=iGr;
                  taSpecInvSSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                  iGr:=teClassIdParent.AsInteger;
                end;
              end;
              if iGr>0 then
              begin
                if teClass.Locate('Id',iGr,[]) then
                begin
                  taSpecInvidGr.AsInteger:=iGr;
                  taSpecInvSGr.AsString:=OemToAnsiConvert(teClassNameClass.AsString);
                end;
              end;

              taSpecInvNum.AsInteger:=ptInvLine1NUM.AsInteger;

              taSpecInvSumRSS.AsFloat:=ptInvLine1SUMRSS.AsFloat;
              taSpecInvSumFSS.AsFloat:=ptInvLine1SUMFSS.AsFloat;

              //�������������� �������� �� �������������
              taSpecInvSumRSC.AsFloat:=rSumRSC;
              taSpecInvSumRSSI.AsFloat:=ptInvLine1SUMRSS.AsFloat+rSumRSC;

              taSpecInvSumDSS.AsFloat:=ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat-rSumRSC;
              if (ptInvLine1QUANTR.AsFloat+rQuantC)<>0 then taSpecInvPriceSS.AsFloat:=(ptInvLine1SUMRSS.AsFloat+rSumRSC)/(ptInvLine1QUANTR.AsFloat+rQuantC)
              else
              begin
                if ptInvLine1QUANTF.AsFloat<>0 then taSpecInvPriceSS.AsFloat:=ptInvLine1SUMFSS.AsFloat/ptInvLine1QUANTF.AsFloat
                else taSpecInvPriceSS.AsFloat:=0;
              end;

//              taSpecInvSumDSS.AsFloat:=ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMRSS.AsFloat;
//              if ptInvLine1QUANTR.AsFloat<>0 then taSpecInvPriceSS.AsFloat:=ptInvLine1SUMRSS.AsFloat/ptInvLine1QUANTR.AsFloat
//              else taSpecInvPriceSS.AsFloat:=0;

              taSpecInvSumRTO.AsFloat:=ptInvLine1SUMDSS.AsFloat;
              taSpecInvSumDTO.AsFloat:=ptInvLine1SUMFSS.AsFloat-ptInvLine1SUMDSS.AsFloat;

              taSpecInvBarCode.AsString:=taCardsBarCode.AsString;

              if CommonSet.Single=1 then
              begin
                taSpecInviVid.AsInteger:=taCardsV11.AsInteger;
                taSpecInviMaker.AsInteger:=taCardsV12.AsInteger;
                taSpecInvsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                taSpecInvVol.AsFloat:=taCardsV10.AsInteger/1000;
                taSpecInvVolInv.AsFloat:=(taCardsV10.AsInteger/1000)*(ptInvLine1QUANTF.AsFloat);
              end;

              taSpecInv.Post;
            end;
          end;

          ptInvLine1.Next;
        end;
      end;

        Memo1.Lines.Add('��.');
        prButton(True); Delay(100);
        fmAddDoc5.teClass.Active:=False;
        fmAddDoc5.ViewDoc5.EndUpdate;
        fmAddDoc5.Show;
      end;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocs5.ViewDocsInvDblClick(Sender: TObject);
begin
  //������� �������
  with dmMC do
  begin
    if quTTnInvStatus.AsString='�' then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocs5.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnInv.RecordCount>0 then //���� ��� �������
    begin
      if quTTnInvStatus.AsString='�' then
      begin
        if MessageDlg('�� ������������� ������ ������� �������������� �'+quTTnInvNumber.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .... ���� �������� ���������.'); Delay(10);

          if pos('Auto',quTTnInvNumber.AsString)=0 then
          begin
            prAddHist(20,quTTnInvInventry.AsInteger,trunc(quTTnInvDateInventry.AsDateTime),quTTnInvNumber.AsString,'���.',0);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "InventryLine"');
            quD.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
            quD.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "A_INVLN"');
            quD.SQL.Add('where IDH='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "InventryHead" ');
            quD.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
            quD.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

//          prRefrID(quTTnInv,ViewDocsInv);

            ViewDocsInv.BeginUpdate;
            quTTnInv.Active:=False;
            quTTnInv.Active:=True;
            quTTnInv.First;
            ViewDocsInv.EndUpdate;

            Memo1.Lines.Add('��.'); Delay(10);
          end else Memo1.Lines.Add('�������� ��������� (������������� ��������� ���).');
          Delay(10);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs5.acOnDoc1Execute(Sender: TObject);
Var iT:INteger;
    sName,sMessur,sCSz:String;
begin
//������������
  with dmMC do
  with dmMt do
  begin
    if not CanDo('prOnDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    if quTTnInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInvStatus.AsString='�' then
      begin
        if quTTnInvDateInventry.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if MessageDlg('������������ �������� �'+quTTnInvNumber.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {if prTOFind(Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnInNameDep.AsString+' � '+FormatDateTime('dd.mm.yyyy',quTTnInDateInvoice.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnInDateInvoice.AsDateTime),quTTnInDepart.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem7.Enabled:=True;
              exit;
            end;
          end;}
          prButton(False);

          prAddHist(20,quTTnInvInventry.AsInteger,trunc(quTTnInvDateInventry.AsDateTime),quTTnInvNumber.AsString,'�����.',1);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);
          // 1 - �������� ��������������
          // 3 - �������� ������
          iT:=quTTnInvxDopStatus.AsInteger;

          if iT=0 then
          begin
            quSpecInv.Active:=False;
            quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
            quSpecInv.ParamByName('ID').AsInteger:=quTTnInvInventry.AsInteger;
            quSpecInv.Active:=True;

            quSpecInv.First;  //��������� ����� ���  � ��������� ��������������
            while not quSpecInv.Eof do
            begin
              if abs(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat)>0.001 then
              begin
                prDelTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
                prAddTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));

                if quSpecInvQuantActual.AsFloat>0 then
                begin
                  if taCards.FindKey([quSpecInvItem.AsInteger]) then
                  begin
                    if taCardsStatus.AsInteger>=100 then
                    begin //��������� � �������� � ������ �� ����� �����
                      taCards.Edit;
                      taCardsStatus.AsInteger:=1;
                      taCards.Post;

                      quBars.Active:=False;
                      quBars.ParamByName('GOODSID').AsInteger:=taCardsID.AsInteger;
                      quBars.Active:=True;

                      prAddCashLoad(taCardsID.AsInteger,5,rv(taCardsCena.AsFloat),0);

                      sName:=OemToAnsiConvert(prRetDName(taCardsID.AsInteger)+taCardsFullName.AsString);
                      if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

                      prAddPosFB(1,taCardsID.AsInteger,taCardsV11.AsInteger,0,taCardsGoodsGroupID.AsInteger,0,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));

                      quBars.First;
                      while not quBars.Eof do
                      begin
                        if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                        else sCSz:='QUANTITY';

                        prAddPosFB(2,taCardsID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(taCardsCena.AsFloat));

                        quBars.Next;
                      end;

                    end;
                  end;
                end;
              end;
              quSpecInv.Next;
            end;
            quSpecInv.Active:=False;
          end;

          if iT=1 then
          begin
            if ptInvLine1.Active=False then ptInvLine1.Active:=True;
            if taCards.Active=False then taCards.Active:=True;
            ptInvLine1.Refresh;
            ptInvLine1.CancelRange;
            ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
            ptInvLine1.First;
            while not ptInvLine1.Eof do
            begin
              if (abs(ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat)>0.001)or(abs(ptInvLine1QUANTF.AsFloat)<>0) then
              begin
                prDelTMoveId(quTTnInvDepart.AsInteger,ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat-ptInvLine1QUANTC.AsFloat));
                prAddTMoveId(quTTnInvDepart.AsInteger,ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat-ptInvLine1QUANTC.AsFloat));

                if ptInvLine1QUANTF.AsFloat>0 then
                begin
                  if taCards.FindKey([ptInvLine1ITEM.AsInteger]) then
                  begin
                    if taCardsStatus.AsInteger>=100 then
                    begin //��������� � �������� � ������ �� ����� �����
                      taCards.Edit;
                      taCardsStatus.AsInteger:=1;
                      taCards.Post;

                      quBars.Active:=False;
                      quBars.ParamByName('GOODSID').AsInteger:=ptInvLine1ITEM.AsInteger;
                      quBars.Active:=True;

                      prAddCashLoad(taCardsID.AsInteger,5,rv(taCardsCena.AsFloat),0);

                      sName:=OemToAnsiConvert(prRetDName(taCardsID.AsInteger)+taCardsFullName.AsString);
                      if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

                      prAddPosFB(1,ptInvLine1ITEM.AsInteger,taCardsV11.AsInteger,0,taCardsGoodsGroupID.AsInteger,0,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));

                      quBars.First;
                      while not quBars.Eof do
                      begin
                        if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                        else sCSz:='QUANTITY';

                        prAddPosFB(2,taCardsID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(taCardsCena.AsFloat));

                        quBars.Next;
                      end;

                    end;
                  end;
                end;
              end;
              ptInvLine1.Next;
            end;
          end;
         // 4 - �������� ������

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "InventryHead" Set Status=''�''');
          quE.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
          quE.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
          quE.ExecSQL;

          ViewDocsInv.BeginUpdate;
          quTTnInv.Active:=False;
          quTTnInv.Active:=True;
          quTTnInv.First;
          ViewDocsInv.EndUpdate;

          //�������� ��
          if quTTnInvItem.AsInteger=1 then
          begin
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnInvDateInventry.AsDateTime))>=1 then prRecalcTO(trunc(quTTnInvDateInventry.AsDateTime),trunc(Date)-1,nil,1,0);
          end;

          prWh('������ ��.',Memo1);
          prButton(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs5.acOffDoc1Execute(Sender: TObject);
//Var iCountPartOut:Integer;
//    bStart:Boolean;
//    StrWk:String;
Var iT:Integer;
begin
//��������
  with dmMC do
  with dmMt do
  begin
    if not CanDo('prOffDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
//    if not CanEdit(Trunc(quTTnInDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    if quTTnInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTnInvStatus.AsString='�' then
      begin
        if quTTnInvDateInventry.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if MessageDlg('�������� �������� �'+quTTnInvNumber.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
         {
          if prTOFind(Trunc(quTTnInDATEDOC.AsDateTime),quTTnInIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quTTnInNAMEMH.AsString+' � '+quTTnInNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quTTnInDATEDOC.AsDateTime),quTTnInIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem8.Enabled:=True;
              exit;
            end;
          end;}

          prButton(False);

          prAddHist(20,quTTnInvInventry.AsInteger,trunc(quTTnInvDateInventry.AsDateTime),quTTnInvNumber.AsString,'�����.',0);

          Memo1.Clear;
          prWH('����� ... ���� ������.',Memo1);
          // 1 - �������� ��������������
          // 3 - �������� ������
          iT:=quTTnInvxDopStatus.AsInteger;

          if iT=0 then
          begin
            quSpecInv.Active:=False;
            quSpecInv.ParamByName('IDEP').AsInteger:=quTTnInvDepart.AsInteger;
            quSpecInv.ParamByName('ID').AsInteger:=quTTnInvInventry.AsInteger;
            quSpecInv.Active:=True;

            quSpecInv.First;
            while not quSpecInv.Eof do
            begin
              if abs(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat)>0.001 then
              begin
                prDelTMoveId(quTTnInvDepart.AsInteger,quSpecInvItem.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(quSpecInvQuantActual.AsFloat-quSpecInvQuantRecord.AsFloat));
              end;
              quSpecInv.Next;
            end;
            quSpecInv.Active:=False;
          end;
          if iT=1 then
          begin
            if ptInvLine1.Active=False then ptInvLine1.Active:=True;
            ptInvLine1.Refresh;
            ptInvLine1.CancelRange;
            ptInvLine1.SetRange([quTTnInvInventry.AsInteger],[quTTnInvInventry.AsInteger]);
            ptInvLine1.First;
            while not ptInvLine1.Eof do
            begin
              if abs(ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat)>0.001 then
              begin
                prDelTMoveId(quTTnInvDepart.AsInteger,ptInvLine1ITEM.AsInteger,Trunc(quTTnInvDateInventry.AsDateTime),quTTnInvInventry.AsInteger,20,(ptInvLine1QUANTF.AsFloat-ptInvLine1QUANTR.AsFloat));
              end;
              ptInvLine1.Next;
            end;
          end;

         // 4 - �������� ������

          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "InventryHead" Set Status=''�''');
          quE.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
          quE.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
          quE.ExecSQL;

          ViewDocsInv.BeginUpdate;
          quTTnInv.Active:=False;
          quTTnInv.Active:=True;
          quTTnInv.First;
          ViewDocsInv.EndUpdate;

          //�������� ��
          if quTTnInvItem.AsInteger=1 then
          begin
            prWh('  �������� ��.',Memo1);
            if (trunc(Date)-trunc(quTTnInvDateInventry.AsDateTime))>=1 then prRecalcTO(trunc(quTTnInvDateInventry.AsDateTime),trunc(Date)-1,nil,1,0);
          end;

          prWh('������ ��.',Memo1);
          prButton(True);
        end;
      end;
    end;
  end;
end;

procedure TfmDocs5.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocs5.acVidExecute(Sender: TObject);
begin
  //���
  with dmMC do
  begin
    {if LevelDocsInv.Visible then
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs1.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs1.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsInv.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateEnd>=iMaxDate then fmDocs1.Caption:='��������� �������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)
      else fmDocs1.Caption:='��������� �������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      LevelDocsInv.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsIn.BeginUpdate;
      quTTnInv.Active:=False;
      quTTnInv.ParamByName('DATEB').AsDate:=CommonSet.DateBeg;
      quTTnInv.ParamByName('DATEE').AsDate:=CommonSet.DateEnd;
      quTTnInv.Active:=True;
      ViewDocsIn.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocs5.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocs5.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsInv.Visible=False then exit;
  with dmMC do
  begin
    if quTTnInv.RecordCount>0 then //���� ��� �������������
    begin
      quSpecInv.Active:=False;
      quSpecInv.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecInv.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTnInNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTnInNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTnInNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecInv.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocs5.acCopyExecute(Sender: TObject);
//var Par:Variant;
begin
  //����������
  with dmMC do
  begin
    {if quTTnInv.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTnInID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTnInID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTnInDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTnInNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quTTnInIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTnInNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTnInIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTnInNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTnInSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTnInSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecInv.Active:=False;
      quSpecInv.ParamByName('IDHD').AsInteger:=quTTnInID.AsInteger;
      quSpecInv.Active:=True;

      quSpecInv.First;
      while not quSpecInv.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTnInID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecInNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecInIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecInQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecInIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecInSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecInIDM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecInIDM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecInPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecInSUMUCH.AsFloat;
        taSpecDoc.Post;

        quSpecInv.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocs5.acInsertDExecute(Sender: TObject);
{Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;}
begin
  // ��������
  with dmMC do
  begin
    {taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocInv') then
        begin
          prAllViewOff;

          fmAddDoc5.Caption:='��������������: ����� ��������.';
          fmAddDoc5.cxTextEdit1.Text:=prGetNum(1,0);
          fmAddDoc5.cxTextEdit1.Tag:=0;
          fmAddDoc5.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc5.cxTextEdit2.Text:='';
          fmAddDoc5.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc5.cxDateEdit1.Date:=Date;
          fmAddDoc5.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc5.cxDateEdit2.Date:=Date;
          fmAddDoc5.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc5.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc5.cxCurrencyEdit2.EditValue:=0;

          if taHeadDocIType.AsInteger=1 then
          begin
            fmAddDoc5.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc5.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc5.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc5.cxButtonEdit1.Tag:=0;
            fmAddDoc5.cxButtonEdit1.EditValue:=0;
            fmAddDoc5.cxButtonEdit1.Text:='';
          end;

          fmAddDoc5.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc5.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc5.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc5.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quDeparts.Active=False then quDeparts.Active:=True;
          quDeparts.FullRefresh;

          if quDeparts.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc5.Label15.Caption:='��. ����: '+quDepartsNAMEPRICE.AsString;
            fmAddDoc5.Label15.Tag:=quDepartsDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc5.Label15.Caption:='��. ����: ';
            fmAddDoc5.Label15.Tag:=0;
          end;

          fmAddDoc5.cxLabel1.Enabled:=True;
          fmAddDoc5.cxLabel2.Enabled:=True;
          fmAddDoc5.cxLabel3.Enabled:=True;
          fmAddDoc5.cxLabel4.Enabled:=True;
          fmAddDoc5.cxLabel5.Enabled:=True;
          fmAddDoc5.cxLabel6.Enabled:=True;
          fmAddDoc5.N1.Enabled:=True;

          fmAddDoc5.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc5.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc5.cxButton1.Enabled:=True;

          CloseTa(fmAddDoc5.taSpec);

          fmAddDoc5.ViewTara.OptionsData.Editing:=True;
          fmAddDoc5.ViewTara.OptionsData.Deleting:=True;

          CloseTa(fmAddDoc5.taTaraS);

          fmAddDoc5.acSaveDoc.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc1 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

     //     fmAddDoc5.ShowModal;
          fmAddDoc5.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocs5.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsInv.Visible then
  begin
    prNExportExel5(ViewDocsInv);
  end else
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocs5.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocs5.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsInv);
end;

procedure TfmDocs5.SpeedItem11Click(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j:INteger;
    rSumF,rSumTO,rSumR:Real;
    iDep,Item:INteger;
    DateINv:TDateTime;
    NameDep,St:String;
begin
// ������ ��������� �������
  with dmMc do
  begin
    if ViewDocsInv.Controller.SelectedRecordCount>0 then
    begin
      CloseTe(taInvSum);
      for i:=0 to ViewDocsInv.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDocsInv.Controller.SelectedRecords[i];

        rSumR:=0; rSumF:=0; iDep:=0; DateInv:=Date; NameDep:=''; Item:=0; St:='';

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDocsInv.Columns[j].Name='ViewDocsInvSumRecord' then rSumR:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvSumActual' then rSumF:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvStatus' then St:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvItem' then Item:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvDepart' then iDep:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvNameDep' then NameDep:=Rec.Values[j];
          if ViewDocsInv.Columns[j].Name='ViewDocsInvDateInventry' then DateInv:=Rec.Values[j];
        end;

        if (St='�') and (Item=1) then
        begin
          rSumTO:=prFindSUMTO(iDep,Trunc(DateInv));



          taInvSum.Append;
          taInvSumIdDepart.AsInteger:=iDep;
          taInvSumNameDepart.AsString:=NameDep;
          taInvSumDateInv.AsDateTime:=DateInv;
          taInvSumSumR.AsFloat:=rSumR;
          taInvSumSumTO.AsFloat:=rSumTO;
          taInvSumSumF.AsFloat:=rSumF;
          taInvSumSumDR.AsFloat:=rSumF-rSumR;
          taInvSumSumDTO.AsFloat:=rSumF-rSumTO;
          taINvSum.Post;
        end;
        taInvSum.First;

        frRepRINV.LoadFromFile(CommonSet.Reports + 'InventrySum.frf');

        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',taInvSumDateInv.AsDateTime);

        frRepRINV.ReportName:='������ ��������������.';
        frRepRINV.PrepareReport;
        frRepRINV.ShowPreparedReport;

    {    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDINV.ShowPreparedReport
    else frRepDINV.PrintPreparedReport('',1,False,vPP);
    }

      end;
    end;
  end;
end;

procedure TfmDocs5.acAdd1Execute(Sender: TObject);
begin
  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMC do
  begin
    prSetValsAddDoc1(0); //0-����������, 1-��������������, 2-��������

//    CloseTa(fmAddDoc5.taSpecInv);
    if FileExists(CurDir+'SpecInv.cds') then fmAddDoc5.taSpecInv.Active:=True
    else fmAddDoc5.taSpecInv.CreateDataSet;

    fmAddDoc5.acSaveDoc.Enabled:=True;
    fmAddDoc5.cxTextEdit1.Tag:=0;

    fmAddDoc5.Show;
  end;
end;

procedure TfmDocs5.acCreateDepInvExecute(Sender: TObject);
var iMainDep,i,iRes,iInv:INteger;
    iDeps,iInvh:array[1..20] of integer;
    StrWk,StrNum:String;
    IDH,iCurD,iLastD,iInvHead,iC:Integer;
    rQ,rQr,rPriceM,rQrec:Real;
    par:Variant;
    rSumF,rPrF:Real;
begin
//������������ ��������� ���������
  with dmMc do
  with dmMt do
  begin
    if quTTnInv.RecordCount>0 then
    begin
      iRes:= MessageDlg('������������ ��������� �� �������?', mtConfirmation, [mbYes, mbNo], 0);
      if iRes = 6 then
      begin         
        fmSt.Memo1.Clear;
        fmSt.cxButton1.Enabled:=False;
        fmSt.Show; delay(100);
        try
          try
            quPriorD.Active:=False;
            quPriorD.Active:=True;

            if ptDep.Active=False then ptDep.Active:=True;

            iMainDep:=0;
            ptDep.First;
            while not ptDep.Eof do
            begin
              if ptDepV02.AsInteger>iMainDep then iMainDep:=ptDepID.AsInteger;
              ptDep.Next;
            end;

            for i:=1 to 20 do begin iDeps[i]:=0; iInvh[i]:=0; end;
            i:=1;
            StrWk:='';
            quPriorD.First;
            while (quPriorD.Eof=False)and (i<=20) do
            begin
              iDeps[i]:=quPriorDID.AsInteger;
              StrWk:=StrWk+its(quPriorDID.AsInteger)+' ';
              quPriorD.Next; inc(i);
            end;
            quPriorD.First;

            fmSt.Memo1.Lines.Add(fmt+'  ������ - '+StrWk); delay(10);
            fmSt.Memo1.Lines.Add(fmt+'  ��������� ���������..'); delay(10);

            StrNum:=Copy(quTTnInvNumber.AsString,1,4);

            iCurD:=Trunc(quTTnInvDateInventry.AsDateTime);
            iInv:=quTTnInvInventry.AsInteger;
//            iDep:=quTTnInvDepart.AsInteger;

            for i:=1 to 20 do
            begin
              if iDeps[i]>0 then
              begin
                if quTTnInv.Locate('Number',StrNum+'~'+its(i)+'_'+its(iDeps[i]),[])=False then
                begin //��������
                  fmSt.Memo1.Lines.Add(fmt+'  �������� - '+StrNum+'~'+its(i)+'_'+its(iDeps[i])); delay(10);
                  IDH:=prMax('DocsInv')+1;

                  iInvh[i]:=IDH; //��� �������������� ���������������� ������

                  quA.Active:=False;
                  StrWk:='';
                  quA.SQL.Clear;
                  quA.SQL.Add('INSERT into "InventryHead" values (');

                  StrWk:=StrWk+(its(idh)+',');//Inventry
                  StrWk:=StrWk+(its(iDeps[i])+',');//Depart
                  StrWk:=StrWk+(''''+ds(iCurD)+''',');//DateInventry
                  StrWk:=StrWk+(''''+StrNum+'~'+its(i)+'_'+its(iDeps[i])+''',');//Number
                  StrWk:=StrWk+(its(0)+',');//CardIndex //�����
                  StrWk:=StrWk+(its(0)+',');//InputMode  //���-��
                  StrWk:=StrWk+(its(0)+',');//PriceMode   //���������
                  StrWk:=StrWk+(its(2)+',');//Detail   //�����������
                  StrWk:=StrWk+('0,');//GGroup
                  StrWk:=StrWk+('0,');//SubGroup
                  StrWk:=StrWk+('0,');//Item
                  StrWk:=StrWk+('0,');//CountLost
                  StrWk:=StrWk+(''''+ds(iCurD)+''',');//DateLost
                  StrWk:=StrWk+(fs(0)+',');//QuantRecord
                  StrWk:=StrWk+(fs(0)+',');//SumRecord
                  StrWk:=StrWk+(fs(0)+',');//QuantActual
                  StrWk:=StrWk+(fs(0)+',');//SumActual
                  StrWk:=StrWk+('0,');//NumberOfLines
                  StrWk:=StrWk+(''''+ds(iCurD)+''',');//DateStart
                  StrWk:=StrWk+('''23:59:59'',');//TimeStart
                  StrWk:=StrWk+(''''',');//President
                  StrWk:=StrWk+(''''',');//PresidentPost
                  StrWk:=StrWk+(''''',');//Member1
                  StrWk:=StrWk+(''''',');//Member1_Post
                  StrWk:=StrWk+(''''',');//Member2
                  StrWk:=StrWk+(''''',');//Member2_Post
                  StrWk:=StrWk+(''''',');//Member3
                  StrWk:=StrWk+(''''',');//Member3_Post
                  StrWk:=StrWk+('1,');//xDopStatus //������ ��� 1-��
//                  StrWk:=StrWk+('0,');//xDopStatus
                  StrWk:=StrWk+(''''',');//Response1
                  StrWk:=StrWk+(''''',');//Response1_Post
                  StrWk:=StrWk+(''''',');//Response2
                  StrWk:=StrWk+(''''',');//Response2_Post
                  StrWk:=StrWk+(''''',');//Response3
                  StrWk:=StrWk+(''''',');//Response3_Post
                  StrWk:=StrWk+(''''',');//Reason
                  StrWk:=StrWk+(''''',');//CASHER
                  StrWk:=StrWk+('''�'',');//Status
                  StrWk:=StrWk+('0,');//LinkInvoice
                  StrWk:=StrWk+('0,');//StartTransfer
                  StrWk:=StrWk+('0,');//EndTransfer
                  StrWk:=StrWk+('''''');//Rezerv

                  quA.SQL.Add(StrWk);

                  quA.SQL.Add(')');
                  quA.ExecSQL;
                end else
                begin
                  fmSt.Memo1.Lines.Add(fmt+'  ��� ����, ���� - '+StrNum+'~'+its(i)+'_'+its(iDeps[i])); delay(10);
                  iInvh[i]:=quTTnInvInventry.AsInteger;

                  if quTTnInvStatus.AsString='�' then
                  begin
                    quD.SQL.Clear;
                    quD.SQL.Add('delete from "A_INVLN"');
                    quD.SQL.Add('where IDH='+its(quTTnInvInventry.AsInteger));
                    quD.ExecSQL;
                  end;
                end;
              end;
            end;

            ViewDocsInv.BeginUpdate;
            quTTnInv.Active:=False;
            quTTnInv.Active:=True;
            delay(10);
            ViewDocsInv.EndUpdate;

            //��������� ������� - ���� ������� �������

            fmSt.Memo1.Lines.Add(fmt+'  ������ �� �������� ...'); delay(10);

            ///------------
           {
            ptInvLine1.Active:=False;
            ptInvLine1.Active:=True;

            fmSt.Memo1.Lines.Add(fmt+'  ����� ...'); delay(10);

            ptInvLine2.Active:=False;
            ptInvLine2.Active:=True;
            ptInvLine2.CancelRange;
            ptInvLine2.SetRange([iInv],[iInv]);
            ptInvLine2.First;

            iC:=0;
            while not ptInvLine2.Eof do
            begin

              if ptInvLine2PRICER.AsFloat>0 then rPriceM:=ptInvLine2PRICER.AsFloat
              else rPriceM:=prFindPrice(ptInvLine2ITEM.AsInteger);

              rQ:=ptInvLine2QUANTF.AsFloat;

              if abs(rQ)>0.0001 then
              begin  //����� ����������� �� �������

                if rQ>0 then
                begin
                  for i:=1 to 20 do
                  begin
                    if iDeps[i]>0 then
                    begin
//                      iD:=iDeps[i];
                      rQrec:=0;
                      rQr:=prFindTRemnDepD(ptInvLine2ITEM.AsInteger,iDeps[i],Trunc(iCurD));//������� �� ������
                      if rQr>0 then
                      begin
                        if (rQr+0.01)>=rQ then //�������� ������� �� ��� ������ ,��� �� ����� - ��� ����� ����
                        begin
                          rQRec:=rQ;
                          rQ:=0;   //����� ��� ����
                        end else
                        begin      //����� ����� �.�. ������� ������������ > 10� - �� ��������� �� �������
                          rQRec:=rQr;
                          rQ:=rQ-rQr;
                        end;
                      end;

                      //���� ��� ���� � ����� ������

                      ptInvLine1.Append;
                      ptInvLine1IDH.AsInteger:=iInvH[i];
                      ptInvLine1Item.AsInteger:=ptInvLine2ITEM.AsInteger;
                      ptInvLine1PRICER.AsFloat:=rPriceM;
                      ptInvLine1QUANTR.AsFloat:=rQr;
                      ptInvLine1QUANTF.AsFloat:=rQrec;
                      ptInvLine1QUANTD.AsFloat:=rQrec-rQr;
                      ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                      ptInvLine1SUMF.AsFloat:=rPriceM*rQrec;
                      ptInvLine1SUMD.AsFloat:=rPriceM*(rQrec-rQr);
                      ptInvLine1SUMRSS.AsFloat:=0;
                      ptInvLine1SUMFSS.AsFloat:=0;
                      ptInvLine1SUMDSS.AsFloat:=0;
                      ptInvLine1NUM.AsInteger:=iC;
                      ptInvLine1QUANTC.AsFloat:=0;
                      ptInvLine1SUMC.AsFloat:=0;
                      ptInvLine1SUMRSC.AsFloat:=0;
                      ptInvLine1.Post;
                    end;
                  end;
                  //��������� ��� ���� - ����� ������ ���� �� ������� � ���� �� ������
                end;

                if (rQ>0)or(rQ<0) then
                begin //������� ���� - ����� ������ �� ����� ���������� �������, ���� ��� �� �� ��������� � ����������
                  iLastD:=0;

                  quPost3.Active:=False;
                  quPost3.ParamByName('IDCARD').Value:=ptInvLine2ITEM.AsInteger;
                  quPost3.Active:=True;

                  if quPost3.RecordCount>0 then
                  begin
                    quPost3.First;
                    iLastD:=quPost3Depart.AsInteger;
                  end;
                  quPost3.Active:=False;

                  if iLastD>0 then //����� ��������� ����� ������� - ������� ���� ��������
                  begin
                    for i:=1 to 20 do
                    begin
                      if iDeps[i]=iLastD then
                      begin //��� - ���� ����� � �����������- ��� ����� ����
                        iInvHead:=iInvH[i];
                        if ptInvLine1.FindKey([iInvHead,ptInvLine2ITEM.AsInteger]) then
                        begin //�� ����� ������ �����, ����� � �� �������� - ������ �� ��������� ������
                          rQrec:=ptInvLine1QUANTF.AsFloat+rQ;
                          rQr:=ptInvLine1QUANTR.AsFloat;
                          rQ:=0;

                          ptInvLine1.Edit;
                          ptInvLine1QUANTR.AsFloat:=rQr;
                          ptInvLine1QUANTF.AsFloat:=rQrec;
                          ptInvLine1QUANTD.AsFloat:=rQrec-rQr;

                          ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                          ptInvLine1SUMF.AsFloat:=rPriceM*rQrec;
                          ptInvLine1SUMD.AsFloat:=rPriceM*(rQrec-rQr);

                          ptInvLine1.Post;
                        end;
                      end;
                    end;
                  end;

                  if (rQ>0)or(rQ<0) then
                  begin     //�� ������ - ���� �� �� ����� ����� �� ������ - ����� � ���������
                    for i:=1 to 19 do
                    begin
                      if iDeps[i+1]=0 then
                      begin //��� �� ��������� � �����������
                        if ptInvLine1.FindKey([iInvH[i],ptInvLine2ITEM.AsInteger]) then
                        begin
                          rQrec:=ptInvLine1QUANTF.AsFloat+rQ;
                          rQr:=ptInvLine1QUANTR.AsFloat;
//                          rQ:=0;

                          ptInvLine1.Edit;
                          ptInvLine1QUANTR.AsFloat:=rQr;
                          ptInvLine1QUANTF.AsFloat:=rQrec;
                          ptInvLine1QUANTD.AsFloat:=rQrec-rQr;

                          ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                          ptInvLine1SUMF.AsFloat:=rPriceM*rQrec;
                          ptInvLine1SUMD.AsFloat:=rPriceM*(rQrec-rQr);

                          ptInvLine1.Post;


                        end else
                        begin //������ �������� - ������ ��������� - ��� �� ������ ����
                          try
                            ptInvLine1.Append;
                            ptInvLine1IDH.AsInteger:=iInvH[i];
                            ptInvLine1Item.AsInteger:=ptInvLine2ITEM.AsInteger;
                            ptInvLine1PRICER.AsFloat:=rPriceM;
                            ptInvLine1QUANTR.AsFloat:=0;
                            ptInvLine1QUANTF.AsFloat:=rQ;
                            ptInvLine1QUANTD.AsFloat:=rQ;

                            ptInvLine1SUMR.AsFloat:=0;
                            ptInvLine1SUMF.AsFloat:=rPriceM*rQ;
                            ptInvLine1SUMD.AsFloat:=rPriceM*rQ;
                            ptInvLine1SUMRSS.AsFloat:=0;
                            ptInvLine1SUMFSS.AsFloat:=0;
                            ptInvLine1SUMDSS.AsFloat:=0;
                            ptInvLine1NUM.AsInteger:=iC;
                            ptInvLine1QUANTC.AsFloat:=0;
                            ptInvLine1SUMC.AsFloat:=0;
                            ptInvLine1SUMRSC.AsFloat:=0;

                            ptInvLine1.Post;
                          except
                          end;
                        end;

                        break;
                      end;
                    end;
                  end;
                end;
              end else //rQ=0
              begin  //��������� ��� �����, �� ������� �������  //��������� � ������ �.�. ����� - � ��������� �������� (�����)
                for i:=1 to 20 do
                begin
                  if iDeps[i]>0 then
                  begin
                    rQr:=prFindTRemnDepD(ptInvLine2ITEM.AsInteger,iDeps[i],Trunc(iCurD));

                    ptInvLine1.Append;

                    ptInvLine1IDH.AsInteger:=iInvH[i];
                    ptInvLine1Item.AsInteger:=ptInvLine2ITEM.AsInteger;
                    ptInvLine1PRICER.AsFloat:=rPriceM;
                    ptInvLine1QUANTR.AsFloat:=rQr;
                    ptInvLine1QUANTF.AsFloat:=0;
                    ptInvLine1QUANTD.AsFloat:=(-1)*rQr;

                    ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                    ptInvLine1SUMF.AsFloat:=0;
                    ptInvLine1SUMD.AsFloat:=(-1)*rPriceM*rQr;
                    ptInvLine1SUMRSS.AsFloat:=0;
                    ptInvLine1SUMFSS.AsFloat:=0;
                    ptInvLine1SUMDSS.AsFloat:=0;
                    ptInvLine1NUM.AsInteger:=iC;
                    ptInvLine1QUANTC.AsFloat:=0;
                    ptInvLine1SUMC.AsFloat:=0;
                    ptInvLine1SUMRSC.AsFloat:=0;

                    ptInvLine1.Post;
                  end;
                end;
              end;

              StrWk:=its(iC+1)+' '+its(ptInvLine2ITEM.AsInteger);

              ptInvLine2.Next;  inc(iC);

              fmSt.StatusBar1.Panels[0].Text:=StrWk;
              delay(20);
            end;
            ptInvLine1.Active:=False;
            ptInvLine2.Active:=False;
            }
            //----------------

            par := VarArrayCreate([0,1], varInteger);

            ptInvLine1.Active:=False;
            ptInvLine1.Active:=True;
            fmSt.Memo1.Lines.Add(fmt+'  ����� ...'); delay(10);

            quSpecInv1.Active:=False;
            quSpecInv1.ParamByName('ID').AsInteger:=iInv;
            quSpecInv1.Active:=True;

            iC:=0;

            quSpecInv1.First;
            while not quSpecInv1.Eof do
            begin
              if quSpecInv1PRICER.AsFloat>0 then rPriceM:=quSpecInv1PRICER.AsFloat
              else rPriceM:=prFindPrice(quSpecInv1ITEM.AsInteger);

              rQ:=quSpecInv1QUANTF.AsFloat;
              if abs(rQ)>0.0001 then
              begin  //����� ����������� �� �������

                rPrF:=quSpecInv1SUMF.AsFloat/rQ;

                if rQ>0 then
                begin
                  for i:=1 to 20 do
                  begin
                    if iDeps[i]>0 then
                    begin
//                      iD:=iDeps[i];
                      rQrec:=0;
                      rSumF:=0;
                      rQr:=prFindTRemnDepD(quSpecInv1ITEM.AsInteger,iDeps[i],Trunc(iCurD));//������� �� ������
                      if rQr>0 then
                      begin
                        if (rQr+0.01)>=rQ then //�������� ������� �� ��� ������ ,��� �� ����� - ��� ����� ����
                        begin
                          rQRec:=rQ;
                          rQ:=0;   //����� ��� ����
                          rSumF:=quSpecInv1SUMF.AsFloat;
                        end else
                        begin      //����� ����� �.�. ������� ������������ > 10� - �� ��������� �� �������
                          rQRec:=rQr;
                          rSumF:=(quSpecInv1SUMF.AsFloat/rQ)*rQRec;
                          rQ:=rQ-rQr;
                        end;
                      end;
                      //���� ��� ����

                      ptInvLine1.Append;
                      ptInvLine1IDH.AsInteger:=iInvH[i];
                      ptInvLine1ITEM.AsInteger:=quSpecInv1Item.AsInteger;
                      ptInvLine1PRICER.AsFloat:=rPriceM;
                      ptInvLine1QUANTR.AsFloat:=rQr;
                      ptInvLine1QUANTF.AsFloat:=rQrec;
                      ptInvLine1QUANTD.AsFloat:=rQrec-rQr;
                      ptInvLine1SUMF.AsFloat:=rSumF;
                      ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                      ptInvLine1SUMD.AsFloat:=rSumF-(rPriceM*rQr);
                      ptInvLine1.Post;
                    end;
                  end;
                  //��������� ��� ���� - ����� ������ ���� �� ������� � ���� �� ������
                end;

                if (rQ>0)or(rQ<0) then
                begin //������� ���� - ����� ������ �� ����� ���������� �������, ���� ��� �� �� ��������� � ����������
                  iLastD:=0;

                  quPost3.Active:=False;
                  quPost3.ParamByName('IDCARD').Value:=quSpecInv1ITEM.AsInteger;
                  quPost3.Active:=True;

                  if quPost3.RecordCount>0 then
                  begin
                    quPost3.First;
                    iLastD:=quPost3Depart.AsInteger;
                  end;
                  quPost3.Active:=False;

                  if iLastD>0 then //����� ��������� ����� ������� - ������� ���� ��������
                  begin
                    for i:=1 to 20 do
                    begin
                      if iDeps[i]=iLastD then
                      begin //��� - ���� ����� � �����������- ��� ����� ����
                        iInvHead:=iInvH[i];

                        par[0]:=iInvHead;
                        par[1]:=quSpecInv1ITEM.AsInteger;

                        if ptInvLine1.Locate('IDH;ITEM',par,[]) then
                        begin //�� ����� ������ �����, ����� � �� �������� - ������ �� ��������� ������

                          rQrec:=ptInvLine1QUANTF.AsFloat+rQ;
                          rQr:=ptInvLine1QUANTR.AsFloat;
                          rQ:=0;

                          ptInvLine1.Edit;
                          ptInvLine1QUANTR.AsFloat:=rQr;
                          ptInvLine1QUANTF.AsFloat:=rQrec;
                          ptInvLine1QUANTD.AsFloat:=rQrec-rQr;

                          ptInvLine1SUMF.AsFloat:=rPrF*rQrec;
                          ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                          ptInvLine1SUMD.AsFloat:=(rPrF*rQrec)-(rPriceM*rQr);

                          ptInvLine1.Post;
                        end;
                      end;
                    end;
                  end;

                  if (rQ>0)or(rQ<0) then
                  begin     //�� ������ - ���� �� �� ����� ����� �� ������ - ����� � ���������
                    for i:=1 to 19 do
                    begin
                      if iDeps[i+1]=0 then
                      begin //��� �� ��������� � �����������

                        par[0]:=iInvH[i];
                        par[1]:=quSpecInv1ITEM.AsInteger;

                        if ptInvLine1.Locate('IDH;ITEM',par,[]) then
                        begin
                          rQrec:=ptInvLine1QUANTF.AsFloat+rQ;
                          rQr:=ptInvLine1QUANTR.AsFloat;
                          rQ:=0;

                          ptInvLine1.Edit;
                          ptInvLine1QUANTR.AsFloat:=rQr;
                          ptInvLine1QUANTF.AsFloat:=rQrec;
                          ptInvLine1QUANTD.AsFloat:=rQrec-rQr;

                          ptInvLine1SUMF.AsFloat:=rPrF*rQrec;
                          ptInvLine1SUMR.AsFloat:=rPriceM*rQr;
                          ptInvLine1SUMD.AsFloat:=(rPrF*rQrec)-(rPriceM*rQr);

                          ptInvLine1.Post;
                        end else
                        begin //������ �������� - ������ ��������� - ��� �� ������ ����
                          try
                        {   ptInvLine1.Append;
                            ptInvLine1IDH.AsInteger:=iInvH[i];
                            ptInvLine1Item.AsInteger:=quSpecInv1ITEM.AsInteger;
                            ptInvLine1PRICER.AsFloat:=rPriceM;
                            ptInvLine1QUANTR.AsFloat:=0;
                            ptInvLine1QUANTF.AsFloat:=rQ;
                            ptInvLine1QUANTD.AsFloat:=rQ;

                            ptInvLine1SUMF.AsFloat:=rPrF*rQ;
                            ptInvLine1SUMR.AsFloat:=0;
                            ptInvLine1SUMD.AsFloat:=rPrF*rQ;

                            ptInvLine1.Post;}
                          except
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end else //rQ=0
              begin  //��������� ��� �����, �� ������� �������  //��������� � ������ �.�. ����� - � ��������� �������� (�����)
                for i:=1 to 20 do
                begin
                  if iDeps[i]>0 then
                  begin
                    rQr:=prFindTRemnDepD(quSpecInv1ITEM.AsInteger,iDeps[i],Trunc(iCurD));

                    ptInvLine1.Append;
                    ptInvLine1IDH.AsInteger:=iInvH[i];
                    ptInvLine1Item.AsInteger:=quSpecInv1ITEM.AsInteger;
                    ptInvLine1PRICER.AsFloat:=rPriceM;
                    ptInvLine1QUANTR.AsFloat:=rQr;
                    ptInvLine1QUANTF.AsFloat:=0;
                    ptInvLine1QUANTD.AsFloat:=(-1)*rQr;

                    ptInvLine1SUMF.AsFloat:=rQr*rPriceM;
                    ptInvLine1SUMR.AsFloat:=0;
                    ptInvLine1SUMD.AsFloat:=(-1)*(rQr*rPriceM);

                    ptInvLine1.Post;
                  end;
                end;
              end;

              StrWk:=its(iC+1)+' '+its(quSpecInv1ITEM.AsInteger);
              quSpecInv1.Next;  inc(iC);
              fmSt.StatusBar1.Panels[0].Text:=StrWk;
              delay(20);
            end;
            ptInvLine1.Active:=False;


          except;
          end;
        finally
          quPriorD.Active:=False;
          fmSt.Memo1.Lines.Add(fmt+'������������ ��������.'); delay(10);
          fmSt.cxButton1.Enabled:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmDocs5.acDelInv1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  prButtonSet(False);
  with dmMC do
  begin
    if quTTnInv.RecordCount>0 then //���� ��� �������
    begin
      if quTTnInvStatus.AsString='�' then
      begin
        if MessageDlg('�� ������������� ������ ������� �������������� �'+quTTnInvNumber.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .... ���� �������� ���������.'); Delay(10);

//          if pos('Auto',quTTnInvNumber.AsString)=0 then
//          begin
            prAddHist(20,quTTnInvInventry.AsInteger,trunc(quTTnInvDateInventry.AsDateTime),quTTnInvNumber.AsString,'���.',0);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "InventryLine"');
            quD.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
            quD.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "A_INVLN"');
            quD.SQL.Add('where IDH='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

            quD.SQL.Clear;
            quD.SQL.Add('Delete from "InventryHead" ');
            quD.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
            quD.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
            quD.ExecSQL;
            delay(100);

//          prRefrID(quTTnInv,ViewDocsInv);

            ViewDocsInv.BeginUpdate;
            quTTnInv.Active:=False;
            quTTnInv.Active:=True;
            quTTnInv.First;
            ViewDocsInv.EndUpdate;

            Memo1.Lines.Add('��.'); Delay(10);
//          end else Memo1.Lines.Add('�������� ��������� (������������� ��������� ���).');
          Delay(10);
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocs5.acSEtSpecOExecute(Sender: TObject);
begin
//������������
  with dmMC do
  with dmMt do
  begin
    //��� ���������
    if quTTnInv.RecordCount>0 then //���� ��� ������������
    begin
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "InventryHead" Set Status=''�''');
      quE.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
      quE.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
      quE.ExecSQL;

      ViewDocsInv.BeginUpdate;
      quTTnInv.Active:=False;
      quTTnInv.Active:=True;
      quTTnInv.First;
      ViewDocsInv.EndUpdate;
      prButton(True);
    end;
  end;
end;

procedure TfmDocs5.acSEtSpecXExecute(Sender: TObject);
begin
//��������
  with dmMC do
  with dmMt do
  begin
    //��� ���������
    if quTTnInv.RecordCount>0 then //���� ��� ������������
    begin
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('Update "InventryHead" Set Status=''�''');
      quE.SQL.Add('where Depart='+its(quTTnInvDepart.AsInteger));
      quE.SQL.Add('and Inventry='+its(quTTnInvInventry.AsInteger));
      quE.ExecSQL;

      ViewDocsInv.BeginUpdate;
      quTTnInv.Active:=False;
      quTTnInv.Active:=True;
      quTTnInv.First;
      ViewDocsInv.EndUpdate;
      prButton(True);
    end;
  end;
end;

end.
