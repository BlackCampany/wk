unit Parts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxmdaset, cxCalendar, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmParts = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    Panel2: TPanel;
    ViewP1: TcxGridDBTableView;
    LeP1: TcxGridLevel;
    GrP1: TcxGrid;
    tePartIn: TdxMemData;
    tePartInID: TIntegerField;
    tePartInIDSTORE: TIntegerField;
    tePartInIDATE: TIntegerField;
    tePartInIDDOC: TIntegerField;
    tePartInARTICUL: TIntegerField;
    tePartInDTYPE: TIntegerField;
    tePartInQPART: TFloatField;
    tePartInQREMN: TFloatField;
    tePartInPRICEIN: TFloatField;
    tePartInPRICEOUT: TFloatField;
    tePartInSSTORE: TStringField;
    Label1: TLabel;
    cxButton2: TcxButton;
    dstePartIn: TDataSource;
    ViewP1ID: TcxGridDBColumn;
    ViewP1IDSTORE: TcxGridDBColumn;
    ViewP1SSTORE: TcxGridDBColumn;
    ViewP1IDATE: TcxGridDBColumn;
    ViewP1IDDOC: TcxGridDBColumn;
    ViewP1ARTICUL: TcxGridDBColumn;
    ViewP1DTYPE: TcxGridDBColumn;
    ViewP1QPART: TcxGridDBColumn;
    ViewP1QREMN: TcxGridDBColumn;
    ViewP1PRICEIN: TcxGridDBColumn;
    ViewP1PRICEOUT: TcxGridDBColumn;
    tePartInSINNCLI: TStringField;
    tePartInSCLI: TStringField;
    ViewP1SINNCLI: TcxGridDBColumn;
    ViewP1SCLI: TcxGridDBColumn;
    tePartO: TdxMemData;
    dstePartO: TDataSource;
    tePartOARTICUL: TIntegerField;
    tePartOIDATE: TIntegerField;
    tePartOIDSTORE: TIntegerField;
    tePartOITYPE: TIntegerField;
    tePartOIDDOC: TIntegerField;
    tePartOPARTIN: TIntegerField;
    tePartOQUANT: TFloatField;
    tePartOSUMIN: TFloatField;
    tePartOPRIN: TFloatField;
    tePartOSUMOUT: TFloatField;
    tePartOPROUT: TFloatField;
    tePartOSSTORE: TStringField;
    tePartOSINN: TStringField;
    tePartOSCLI: TStringField;
    tePartOID: TIntegerField;
    LeP2: TcxGridLevel;
    ViewP2: TcxGridDBTableView;
    ViewP2ARTICUL: TcxGridDBColumn;
    ViewP2IDATE: TcxGridDBColumn;
    ViewP2ITYPE: TcxGridDBColumn;
    ViewP2IDDOC: TcxGridDBColumn;
    ViewP2PARTIN: TcxGridDBColumn;
    ViewP2QUANT: TcxGridDBColumn;
    ViewP2SUMIN: TcxGridDBColumn;
    ViewP2PRIN: TcxGridDBColumn;
    ViewP2SUMOUT: TcxGridDBColumn;
    ViewP2PROUT: TcxGridDBColumn;
    ViewP2SSTORE: TcxGridDBColumn;
    ViewP2SINN: TcxGridDBColumn;
    ViewP2SCLI: TcxGridDBColumn;
    tePartInSumIn: TFloatField;
    tePartInSumOut: TFloatField;
    ViewP1SumIn: TcxGridDBColumn;
    ViewP1SumOut: TcxGridDBColumn;
    tePartInPRICEIN0: TFloatField;
    tePartInSumIn0: TFloatField;
    tePartOPRIN0: TFloatField;
    tePartOSUMIN0: TFloatField;
    ViewP1PRICEIN0: TcxGridDBColumn;
    ViewP1SumIn0: TcxGridDBColumn;
    ViewP2PRIN0: TcxGridDBColumn;
    ViewP2SUMIN0: TcxGridDBColumn;
    amPart: TActionManager;
    acTestPO: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure tePartInCalcFields(DataSet: TDataSet);
    procedure acTestPOExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmParts: TfmParts;

implementation

uses MT, Period1, Un1, MDB, Status;

{$R *.dfm}

procedure TfmParts.FormCreate(Sender: TObject);
begin
  GrP1.Align:=AlClient;
end;

procedure TfmParts.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmParts.cxButton2Click(Sender: TObject);
begin
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    if fmParts.Label1.Tag>0 then prFillPart(fmParts.Label1.Tag,fmParts.Label1.Caption);
  end;
end;

procedure TfmParts.tePartInCalcFields(DataSet: TDataSet);
begin
  tePartInSumIn.AsFloat:=rv(tePartInQPART.AsFloat*tePartInPRICEIN.AsFloat);
  tePartInSumOut.AsFloat:=rv(tePartInQPART.AsFloat*tePartInPRICEOUT.AsFloat);
end;

procedure TfmParts.acTestPOExecute(Sender: TObject);
Var rSumss,rSumSS0:Real;
    sInn:String;
    iCli:INteger;
begin
  if tePartO.RecordCount>0 then
  begin
    fmSt.Memo1.Clear;
    fmSt.cxButton1.Enabled:=False;
    fmSt.Show;
    try
      fmSt.Memo1.Lines.Add('����� .. ���� ������'); delay(10);

      if prCalcPartO(tePartOIDDOC.AsInteger,tePartOITYPE.AsInteger,tePartOARTICUL.AsInteger,tePartOIDATE.AsInteger,tePartOIDSTORE.AsInteger,False,tePartOQUANT.AsFloat,tePartOPROUT.AsFloat,tePartOSUMOUT.AsFloat,0,0,'',rSumss,rSumSS0,sInn,iCli)=False then
      begin
        fmSt.Memo1.Lines.Add('  ������ ��� �������.');
      end else
      begin
        if tePartOQUANT.AsFloat<>0 then
        begin
          fmSt.Memo1.Lines.Add('  ��� �������� - '+its(tePartOARTICUL.AsInteger));
          fmSt.Memo1.Lines.Add('  ���� - '+its(tePartOIDATE.AsInteger));
          fmSt.Memo1.Lines.Add('  ��� ������� - '+its(tePartOITYPE.AsInteger));
          fmSt.Memo1.Lines.Add('  ����� - '+its(tePartOIDSTORE.AsInteger));
          fmSt.Memo1.Lines.Add('  ');
          fmSt.Memo1.Lines.Add('  ����� ������ � ���  - '+fs1(rv(rSumss)));
          fmSt.Memo1.Lines.Add('  ���� ������ � ���  - '+fs1(rv(rSumss/tePartOQUANT.AsFloat)));
          fmSt.Memo1.Lines.Add('  ����� ������ ��� ���  - '+fs1(rv(rSumss0)));
          fmSt.Memo1.Lines.Add('  ���� ������ ��� ���  - '+fs1(rv(rSumss0/tePartOQUANT.AsFloat)));
          fmSt.Memo1.Lines.Add('  ��� ���������� - '+sInn);
          fmSt.Memo1.Lines.Add('  ��� ���������� - '+its(iCli));
        end;
      end;
    finally
      fmSt.Memo1.Lines.Add('������ ��������.');
      fmSt.cxButton1.Enabled:=True;
    end;
  end;
end;

end.
