unit Labels;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, pvtables, btvtables, Menus,
  cxLookAndFeelPainters, dxmdaset, StdCtrls, cxButtons, ExtCtrls;

type
  TfmLabels = class(TForm)
    ViewLabels: TcxGridDBTableView;
    LevelLabels: TcxGridLevel;
    GrLabels: TcxGrid;
    dstaL: TDataSource;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    taL: TdxMemData;
    taLID: TSmallintField;
    taLNAME: TStringField;
    taLFILEN: TStringField;
    ViewLabelsID: TcxGridDBColumn;
    ViewLabelsNAME: TcxGridDBColumn;
    ViewLabelsFILEN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLabels: TfmLabels;

implementation

uses MDB;

{$R *.dfm}

procedure TfmLabels.FormCreate(Sender: TObject);
begin
  GrLabels.Align:=AlClient;
end;

end.
