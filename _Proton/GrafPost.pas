unit GrafPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxStyles, cxGraphics, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerDateNavigator, Menus,
  cxLookAndFeelPainters, cxButtons, cxControls, cxContainer,
  cxDateNavigator, cxRadioGroup, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmGrafPost = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    cxDateNavigator1: TcxDateNavigator;
    cxButton1: TcxButton;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    Storage1: TcxSchedulerStorage;
    amGrafZ: TActionManager;
    acGrafSet: TAction;
    procedure acGrafSetExecute(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGrafPost: TfmGrafPost;

implementation

uses MDB, MT;

{$R *.dfm}

procedure TfmGrafPost.acGrafSetExecute(Sender: TObject);
Var iCli:INteger;
    vItem:tCxschedulerStorageResourceItem;
    dDate:TDateTime;
begin
  with dmMt do
  begin
    iCli:=Label2.Tag;

    fmGrafPost.Storage1.BeginUpdate;
    fmGrafPost.Storage1.Clear;
    fmGrafPost.Storage1.Resources.Items.BeginUpdate;
    fmGrafPost.Storage1.Resources.Items.Clear;

    vItem:=fmGrafPost.Storage1.Resources.Items.Add;
    vItem.ResourceID:=1;
    vItem.Index:=0;
    vItem.Name:='';

    if ptGrafPost.Active=False then ptGrafPost.Active:=True;
    ptGrafPost.CancelRange;
    ptGrafPost.SetRange([iCli],[iCli]);
    ptGrafPost.First;
    while not ptGrafPost.Eof do
    begin
      with fmGrafPost.Storage1.createEvent do
      begin
        dDate:=Date+365;
        if cxRadioButton1.Checked then dDate:=ptGrafPostIDATEP.AsInteger;
        if cxRadioButton2.Checked then dDate:=ptGrafPostIDATEZ.AsInteger;
        Start :=dDate;
        Finish :=dDate+0.001;
        Caption :='';
        ResourceID:=1;
        if cxRadioButton1.Checked then ParentId:=ptGrafPostIDATEP.AsInteger;
        if cxRadioButton2.Checked then ParentId:=ptGrafPostIDATEZ.AsInteger;

        fmGrafPost.Storage1.PostEvents;
      end;
      ptGrafPost.Next;
    end;

    fmGrafPost.Storage1.Resources.Items.EndUpdate;
    fmGrafPost.Storage1.EndUpdate;
  end;
end;

procedure TfmGrafPost.cxRadioButton1Click(Sender: TObject);
begin
  acGrafSet.Execute;
end;

end.
