unit SetMatrix;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxDBLookupComboBox,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmSetMatrix = class(TForm)
    GrMatrix: TcxGrid;
    ViewMatrix: TcxGridDBTableView;
    ViewMatrixID: TcxGridDBColumn;
    ViewMatrixName: TcxGridDBColumn;
    ViewMatrixBarCode: TcxGridDBColumn;
    ViewMatrixShRealize: TcxGridDBColumn;
    ViewMatrixTovarType: TcxGridDBColumn;
    ViewMatrixReserv1: TcxGridDBColumn;
    ViewMatrixRemn: TcxGridDBColumn;
    ViewMatrixFullName: TcxGridDBColumn;
    ViewMatrixEdIzm: TcxGridDBColumn;
    ViewMatrixNDS: TcxGridDBColumn;
    ViewMatrixOKDP: TcxGridDBColumn;
    ViewMatrixWeght: TcxGridDBColumn;
    ViewMatrixSrokReal: TcxGridDBColumn;
    ViewMatrixTareWeight: TcxGridDBColumn;
    ViewMatrixKritOst: TcxGridDBColumn;
    ViewMatrixBHTStatus: TcxGridDBColumn;
    ViewMatrixUKMAction: TcxGridDBColumn;
    ViewMatrixDataChange: TcxGridDBColumn;
    ViewMatrixNSP: TcxGridDBColumn;
    ViewMatrixWasteRate: TcxGridDBColumn;
    ViewMatrixCena: TcxGridDBColumn;
    ViewMatrixStatus: TcxGridDBColumn;
    ViewMatrixPrsision: TcxGridDBColumn;
    ViewMatrixMargGroup: TcxGridDBColumn;
    ViewMatrixNameCountry: TcxGridDBColumn;
    ViewMatrixNAMEBRAND: TcxGridDBColumn;
    ViewMatrixV02: TcxGridDBColumn;
    ViewMatrixReserv2: TcxGridDBColumn;
    ViewMatrixFixPrice: TcxGridDBColumn;
    ViewMatrixV04: TcxGridDBColumn;
    ViewMatrixV05: TcxGridDBColumn;
    ViewMatrixKol: TcxGridDBColumn;
    ViewMatrixV06: TcxGridDBColumn;
    ViewMatrixV07: TcxGridDBColumn;
    ViewMatrixV08: TcxGridDBColumn;
    ViewMatrixVol: TcxGridDBColumn;
    ViewMatrixV11: TcxGridDBColumn;
    ViewMatrixNAMEM: TcxGridDBColumn;
    ViewMatrixTypeV: TcxGridDBColumn;
    LevMatrix: TcxGridLevel;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    procedure ViewMatrixCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetMatrix: TfmSetMatrix;

implementation

{$R *.dfm}

procedure TfmSetMatrix.ViewMatrixCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  fmSetMatrix.ModalResult:=mrOK;
end;

end.
