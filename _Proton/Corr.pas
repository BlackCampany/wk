unit Corr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, Menus, cxLookAndFeelPainters, cxButtons;

type
  TfmCorr = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxCurrencyEdit7: TcxCurrencyEdit;
    cxCurrencyEdit8: TcxCurrencyEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCorr: TfmCorr;

implementation

uses un1;

{$R *.dfm}

procedure TfmCorr.cxButton1Click(Sender: TObject);
begin
  if  //�������������� ��� ��� ������ �� ����� 100 ���
      ((((ABS(cxCurrencyEdit1.Value-cxCurrencyEdit2.Value)<=100)
      and
      (ABS(cxCurrencyEdit3.Value-cxCurrencyEdit4.Value)=0))
      or
      ((ABS(cxCurrencyEdit1.Value-cxCurrencyEdit2.Value)=0)
      and
      (ABS(cxCurrencyEdit3.Value-cxCurrencyEdit4.Value)<=100)))

      or

      //������������� ��� � ������ �� ����� 500 ��� ������
      (((ABS(cxCurrencyEdit1.Value-cxCurrencyEdit2.Value)<=500)
      and
      ((ABS(cxCurrencyEdit3.Value-cxCurrencyEdit4.Value)<>0)))

      and
      ((ABS(cxCurrencyEdit3.Value-cxCurrencyEdit4.Value)<=500)
      and
      ((ABS(cxCurrencyEdit1.Value-cxCurrencyEdit2.Value)<>0)))
      ))


     then fmCorr.ModalResult:=mrOk
     else
       begin
         if pos(Person.Name,'�����')>0 then fmCorr.ModalResult:=mrOk
                                       else ShowMessage('������������� �� ����� 100 ���');

       end;

end;

end.
