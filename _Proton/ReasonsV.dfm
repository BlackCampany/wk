object fmReasonVoz: TfmReasonVoz
  Left = 495
  Top = 172
  Width = 382
  Height = 417
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' "'#1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088#1072#1090#1072'"'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridReasV: TcxGrid
    Left = 0
    Top = 0
    Width = 374
    Height = 340
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewReasV: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellDblClick = ViewReasVCellDblClick
      DataController.DataSource = dmP.dsquReasV
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object ViewReasVID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'id'
      end
      object ViewReasVReason: TcxGridDBColumn
        Caption = #1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
        DataBinding.FieldName = 'Reason'
        Width = 214
      end
    end
    object LevelReasV: TcxGridLevel
      GridView = ViewReasV
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 343
    Width = 374
    Height = 41
    Align = alBottom
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 113
      Top = 7
      Width = 145
      Height = 25
      Caption = #1042#1067#1061#1054#1044
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 148
    Top = 56
  end
end
