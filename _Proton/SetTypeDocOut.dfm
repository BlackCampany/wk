object fmSetTypeDocOut: TfmSetTypeDocOut
  Left = 518
  Top = 268
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1090#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 344
  ClientWidth = 365
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 288
    Width = 365
    Height = 56
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 64
      Top = 16
      Width = 97
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 200
      Top = 16
      Width = 97
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072'  Esc'
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 4
    Top = 4
    Properties.Items = <
      item
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1090#1072#1088#1099' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084' (99)'
        Value = 6
      end
      item
        Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072' '#1085#1072' '#1076#1088#1091#1075#1080#1077' '#1092#1080#1083#1080#1072#1083#1099' '#1074#1085#1091#1090#1088#1080' '#1102#1088'.'#1083#1080#1094#1072' (0)'
        Value = 1
      end
      item
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1090#1086#1074#1072#1088#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084' (1)'
        Value = 2
      end
      item
        Caption = #1055#1088#1086#1076#1072#1078#1072' '#1090#1086#1074#1072#1088#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103#1084' (2)'
        Value = 3
      end>
    ItemIndex = 2
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Height = 269
    Width = 349
  end
end
