object fmParts: TfmParts
  Left = 333
  Top = 263
  Width = 1013
  Height = 451
  Caption = #1055#1072#1088#1090#1080#1080' '#1090#1086#1074#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 368
    Width = 1005
    Height = 49
    Align = alBottom
    BevelInner = bvLowered
    Color = 16771797
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 40
      Top = 8
      Width = 105
      Height = 33
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 116
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 4
      Width = 53
      Height = 33
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16771797
      Colors.Normal = 16771797
      Colors.Hot = 16771797
      Colors.Pressed = 16771797
      Colors.PressedText = 16771797
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfFlat
    end
  end
  object GrP1: TcxGrid
    Left = 12
    Top = 44
    Width = 885
    Height = 321
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewP1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstePartIn
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QPART'
          Column = ViewP1QPART
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QREMN'
          Column = ViewP1QREMN
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn'
          Column = ViewP1SumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumOut'
          Column = ViewP1SumOut
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn0'
          Column = ViewP1SumIn0
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QPART'
          Column = ViewP1QPART
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QREMN'
          Column = ViewP1QREMN
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn'
          Column = ViewP1SumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumOut'
          Column = ViewP1SumOut
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn0'
          Column = ViewP1SumIn0
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      Styles.Background = dmMC.cxStyle18
      object ViewP1ARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 45
      end
      object ViewP1IDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewP1QPART: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QPART'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewP1QREMN: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QREMN'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewP1PRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewP1PRICEIN0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN0'
      end
      object ViewP1SumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn'
        Styles.Content = dmMC.cxStyle12
      end
      object ViewP1SumIn0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn0'
      end
      object ViewP1PRICEOUT: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076'.'
        DataBinding.FieldName = 'PRICEOUT'
      end
      object ViewP1SumOut: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075'.'
        DataBinding.FieldName = 'SumOut'
        Styles.Content = dmMC.cxStyle12
      end
      object ViewP1SSTORE: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'SSTORE'
        Width = 60
      end
      object ViewP1SCLI: TcxGridDBColumn
        Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
        DataBinding.FieldName = 'SCLI'
        Width = 80
      end
      object ViewP1ID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1055#1055
        DataBinding.FieldName = 'ID'
      end
      object ViewP1IDSTORE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'IDSTORE'
        Visible = False
      end
      object ViewP1IDDOC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082'.'
        DataBinding.FieldName = 'IDDOC'
      end
      object ViewP1DTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1055#1055
        DataBinding.FieldName = 'DTYPE'
      end
      object ViewP1SINNCLI: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'SINNCLI'
      end
    end
    object ViewP2: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstePartO
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT'
          Column = ViewP2QUANT
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMIN'
          Column = ViewP2SUMIN
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMOUT'
          Column = ViewP2SUMOUT
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMIN0'
          Column = ViewP2SUMIN0
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewP2QUANT
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMIN'
          Column = ViewP2SUMIN
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMOUT'
          Column = ViewP2SUMOUT
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMIN0'
          Column = ViewP2SUMIN0
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewP2ARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Styles.Content = dmMC.cxStyle1
      end
      object ViewP2IDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewP2ITYPE: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'ITYPE'
      end
      object ViewP2IDDOC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082'.'
        DataBinding.FieldName = 'IDDOC'
      end
      object ViewP2PARTIN: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1055#1055
        DataBinding.FieldName = 'PARTIN'
      end
      object ViewP2QUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewP2PRIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
        DataBinding.FieldName = 'PRIN'
      end
      object ViewP2SUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewP2PRIN0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PRIN0'
      end
      object ViewP2SUMIN0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN0'
      end
      object ViewP2SUMOUT: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075'.'
        DataBinding.FieldName = 'SUMOUT'
        Styles.Content = dmMC.cxStyle5
      end
      object ViewP2PROUT: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075'.'
        DataBinding.FieldName = 'PROUT'
      end
      object ViewP2SSTORE: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'SSTORE'
      end
      object ViewP2SINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'SINN'
      end
      object ViewP2SCLI: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'SCLI'
      end
    end
    object LeP1: TcxGridLevel
      Caption = #1055#1088#1080#1093#1086#1076#1099
      GridView = ViewP1
    end
    object LeP2: TcxGridLevel
      Caption = #1056#1072#1089#1093#1086#1076#1099
      GridView = ViewP2
    end
  end
  object tePartIn: TdxMemData
    Indexes = <>
    SortOptions = []
    OnCalcFields = tePartInCalcFields
    Left = 316
    Top = 108
    object tePartInID: TIntegerField
      FieldName = 'ID'
    end
    object tePartInIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object tePartInSSTORE: TStringField
      FieldName = 'SSTORE'
      Size = 50
    end
    object tePartInIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object tePartInIDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object tePartInARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object tePartInDTYPE: TIntegerField
      FieldName = 'DTYPE'
    end
    object tePartInQPART: TFloatField
      FieldName = 'QPART'
      DisplayFormat = '0.000'
    end
    object tePartInQREMN: TFloatField
      FieldName = 'QREMN'
      DisplayFormat = '0.000'
    end
    object tePartInPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object tePartInPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object tePartInSINNCLI: TStringField
      FieldName = 'SINNCLI'
    end
    object tePartInSCLI: TStringField
      FieldName = 'SCLI'
      Size = 100
    end
    object tePartInSumIn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object tePartInSumOut: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SumOut'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object tePartInPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
    object tePartInSumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
  end
  object dstePartIn: TDataSource
    DataSet = tePartIn
    Left = 316
    Top = 168
  end
  object tePartO: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 396
    Top = 108
    object tePartOARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object tePartOIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object tePartOIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object tePartOITYPE: TIntegerField
      FieldName = 'ITYPE'
    end
    object tePartOIDDOC: TIntegerField
      FieldName = 'IDDOC'
    end
    object tePartOPARTIN: TIntegerField
      FieldName = 'PARTIN'
    end
    object tePartOQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object tePartOSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object tePartOPRIN: TFloatField
      FieldName = 'PRIN'
      DisplayFormat = '0.00'
    end
    object tePartOSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object tePartOPROUT: TFloatField
      FieldName = 'PROUT'
      DisplayFormat = '0.00'
    end
    object tePartOSSTORE: TStringField
      FieldName = 'SSTORE'
      Size = 50
    end
    object tePartOSINN: TStringField
      FieldName = 'SINN'
    end
    object tePartOSCLI: TStringField
      FieldName = 'SCLI'
      Size = 50
    end
    object tePartOID: TIntegerField
      FieldName = 'ID'
    end
    object tePartOPRIN0: TFloatField
      FieldName = 'PRIN0'
      DisplayFormat = '0.00'
    end
    object tePartOSUMIN0: TFloatField
      FieldName = 'SUMIN0'
      DisplayFormat = '0.00'
    end
  end
  object dstePartO: TDataSource
    DataSet = tePartO
    Left = 396
    Top = 164
  end
  object amPart: TActionManager
    Left = 608
    Top = 156
    StyleName = 'XP Style'
    object acTestPO: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1088#1072#1089#1095#1077#1090#1072' '#1088#1072#1089#1093#1086#1076#1072' '#1087#1086' '#1087#1086#1079#1080#1094#1080#1080
      OnExecute = acTestPOExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 620
    Top = 224
    object N1: TMenuItem
      Action = acTestPO
    end
  end
end
