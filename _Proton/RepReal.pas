unit RepReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView,
  dxmdaset, cxProgressBar, cxDBProgressBar, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList,
  XPStyleActnCtrls, ActnMan, Menus;

type
  TfmRepReal = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridGrReal: TcxGrid;
    LevelGrReal: TcxGridLevel;
    dsteR: TDataSource;
    FormPlacement1: TFormPlacement;
    SpeedItem4: TSpeedItem;
    teR: TdxMemData;
    teRIdCode1: TIntegerField;
    teRNameC: TStringField;
    teRiM: TSmallintField;
    teRIdGroup: TIntegerField;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    Print1: TdxComponentPrinter;
    Print1Link1: TdxGridReportLink;
    teRNameGr1: TStringField;
    teRNameGr2: TStringField;
    teRIdSGroup: TIntegerField;
    ViewGrReal: TcxGridDBTableView;
    ViewGrRealRecId: TcxGridDBColumn;
    ViewGrRealIdCode1: TcxGridDBColumn;
    ViewGrRealNameC: TcxGridDBColumn;
    ViewGrRealiM: TcxGridDBColumn;
    ViewGrRealIdGroup: TcxGridDBColumn;
    ViewGrRealIdSGroup: TcxGridDBColumn;
    ViewGrRealNameGr1: TcxGridDBColumn;
    ViewGrRealNameGr2: TcxGridDBColumn;
    teRiBrand: TIntegerField;
    teRiCat: TSmallintField;
    ViewGrRealiCat: TcxGridDBColumn;
    amRepOb: TActionManager;
    acMoveRepOb: TAction;
    teRDepName: TStringField;
    teRQuant: TFloatField;
    teRRSum: TFloatField;
    teRRDisc: TFloatField;
    teROper: TStringField;
    teRPlType: TIntegerField;
    ViewGrRealiDep: TcxGridDBColumn;
    ViewGrRealDepName: TcxGridDBColumn;
    teRiDep: TIntegerField;
    ViewGrRealPlType: TcxGridDBColumn;
    ViewGrRealOper: TcxGridDBColumn;
    ViewGrRealQuant: TcxGridDBColumn;
    ViewGrRealRSum: TcxGridDBColumn;
    ViewGrRealRDisc: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    teRNameGr3: TStringField;
    ViewGrRealNameGr3: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acMoveRepObExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepReal: TfmRepReal;

implementation

uses Un1, MDB, MT, Move;

{$R *.dfm}

procedure TfmRepReal.FormCreate(Sender: TObject);
begin
  GridGrReal.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewGrReal.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
end;

procedure TfmRepReal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewGrReal.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
  teR.Active:=False;
end;

procedure TfmRepReal.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepReal.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepReal.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewGrReal);
end;

procedure TfmRepReal.SpeedItem2Click(Sender: TObject);
begin
//��������� ���������
  if not CanDo('prRepGrReal') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmRepReal.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRepReal.Caption;
  Print1Link1.ReportTitle.Text:=StrWk;
  Print1.Preview(True,nil);
end;

procedure TfmRepReal.acMoveRepObExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  with dmMT do
  begin
    if ViewGrReal.Controller.SelectedRecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=teRIdCode1.AsInteger;
      prFillMove(teRIdCode1.AsInteger);

      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=teRNameC.AsString+'('+teRIdCode1.AsString+')'+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmRepReal.N1Click(Sender: TObject);
begin
  ViewGrReal.DataController.Groups.FullCollapse;
end;

procedure TfmRepReal.N2Click(Sender: TObject);
begin
  ViewGrReal.DataController.Groups.FullExpand;
end;

end.
