program RnOffice;

uses
  Forms,
  MainRnOffice in 'MainRnOffice.pas' {fmMainRnOffice},
  dmOffice in 'dmOffice.pas' {dmO: TDataModule},
  Un1 in 'Un1.pas',
  OMessure in 'OMessure.pas' {fmMessure},
  AddMess in 'AddMess.pas' {fmAddMess},
  Goods in 'Goods.pas' {fmGoods},
  AddClass in 'AddClass.pas' {fmAddClass},
  PriceType in 'PriceType.pas' {fmPriceType},
  AddPriceT in 'AddPriceT.pas' {fmAddPriceT},
  MH in 'MH.pas' {fmMH},
  AddMH in 'AddMH.pas' {fmAddMH},
  AddGoods in 'AddGoods.pas' {fmAddGood},
  AddBar in 'AddBar.pas' {fmAddBar},
  FindResult in 'FindResult.pas' {fmFind},
  AddEUGoods in 'AddEUGoods.pas' {fmAddEUGoods},
  Clients in 'Clients.pas' {fmClients},
  AddClient in 'AddClient.pas' {fmAddClients},
  DocsIn in 'DocsIn.pas' {fmDocsIn},
  PeriodUni in 'PeriodUni.pas' {fmPeriodUni},
  AddDoc1 in 'AddDoc1.pas' {fmAddDoc1},
  TCard in 'TCard.pas' {fmTCard},
  CurMessure in 'CurMessure.pas' {fmCurMessure},
  GoodsSel in 'GoodsSel.pas' {fmGoodsSel},
  TransMenuBack in 'TransMenuBack.pas' {fmMenuCr},
  uTransM in 'uTransM.pas' {fmTransM},
  SelPerSkl in 'SelPerSkl.pas' {fmSelPerSkl},
  MoveSel in 'MoveSel.pas' {fmMoveSel},
  DocsOut in 'DocsOut.pas' {fmDocsOut},
  AddDoc2 in 'AddDoc2.pas' {fmAddDoc2},
  SelPartIn in 'SelPartIn.pas' {fmPartIn},
  SelPartIn1 in 'SelPartIn1.pas' {fmPartIn1},
  CalcSeb in 'CalcSeb.pas' {fmCalcSeb},
  DocOutB in 'DocOutB.pas' {fmDocsOutB},
  Period3 in 'Period3.pas' {fmSelPerSkl1},
  DOBSpec in 'DOBSpec.pas' {fmDOBSpec},
  Message in 'Message.pas' {fmMessage},
  DMOReps in 'DMOReps.pas' {dmORep: TDataModule},
  ExportFromOf in 'ExportFromOf.pas' {fmExport},
  u2fdk in 'U2FDK.PAS',
  TOSel in 'TOSel.pas' {fmTO},
  CardsMove in 'CardsMove.pas' {fmCardsMove},
  TBuff in 'TBuff.pas' {fmTBuff},
  AddInv in 'AddInv.pas' {fmAddInv},
  DocInv in 'DocInv.pas' {fmDocsInv},
  FCards in 'FCards.pas' {fmFCards},
  RepPrib in 'RepPrib.pas' {fmRepPrib},
  RepObSa in 'RepObSa.pas' {fmRepOb},
  DocCompl in 'DocCompl.pas' {fmDocsCompl},
  AddCompl in 'AddCompl.pas' {fmAddCompl},
  ActPer in 'ActPer.pas' {fmDocsActs},
  AddAct in 'AddAct.pas' {fmAddAct},
  Refer in 'Refer.pas' {fmComm},
  AddDoc3 in 'AddDoc3.pas' {fmAddDoc3},
  DocsVn in 'DocsVn.pas' {fmDocsVn},
  Input in 'Input.pas' {fmInput},
  OperType in 'OperType.pas' {fmOperType},
  AddOperT in 'AddOperT.pas' {fmAddOper},
  DocOutR in 'DocOutR.pas' {fmDocsReal},
  AddDoc4 in 'AddDoc4.pas' {fmAddDoc4},
  RecalcPer in 'RecalcPer.pas' {fmRecalcPer},
  RecalcReal in 'RecalcReal.pas' {fmRecalcPer1},
  PerA_Office in 'PerA_Office\PerA_Office.pas' {fmPerA_Office},
  ClassSel in 'ClassSel.pas' {fmClassSel},
  ParamSel in 'ParamSel.pas' {fmParamSel},
  RepPost in 'RepPost.pas' {fmRepPost},
  FindSebPr in 'FindSebPr.pas' {fmFindSebPr},
  SummaryReal in 'SummaryReal.pas' {fmSummary},
  RecalcPart in 'RecalcPart.pas' {fmRecalcPer2},
  of_TTKView in 'of_TTKView.pas' {fmTTKView},
  SortPar in 'SortPar.pas' {fmSortF},
  of_EditPosMenu in 'of_EditPosMenu.pas' {fmEditPosM},
  of_AvansRep in 'of_AvansRep.pas' {fmRepAvans},
  SprBGU in 'SprBGU.pas' {fmSprBGU},
  SetStatus in 'SetStatus.pas' {fmSetSt},
  TestPrice in 'TestPrice.pas' {fmTestPrice},
  TermoObr in 'TermoObr.pas' {fmTermoObr},
  AddTermoObr in 'AddTermoObr.pas' {fmAddTermoObr},
  InputMess in 'InputMess.pas' {fmInputMess},
  CalcCal in 'CalcCal.pas' {fmCalcCal},
  sumprops in 'SummaProp\sumprops.pas',
  rCategory in 'rCategory.pas' {fmRCategory},
  ExcelList in 'ExcelList.pas' {fmExcelList},
  SelPerSkl2 in 'SelPerSkl2.pas' {fmSelPerSkl2},
  RemnsDay in 'RemnsDay.pas' {fmRemnsSpeed},
  AddBGU in 'AddBGU.pas' {fmAddBGU},
  SummaryRDoc in 'SummaryRDoc.pas' {fmSummary1},
  TCardAddPars in 'TCardAddPars.pas' {fmTCardsAddPars},
  ImportDoc in 'ImportDoc.pas' {fmImportDocR},
  SetStatusBZ in 'SetStatusBZ.pas' {fmSetStatusBZ},
  CB in 'CB.pas' {dmCB: TDataModule},
  ClassAlg in 'ClassAlg.pas' {fmAlgClass},
  ImpExcelStr in 'ImpExcelStr.pas' {fmImpExcel: Unit1},
  AddAlgClass in 'AddAlgClass.pas' {fmAddAlgClass},
  Makers in 'Makers.pas' {fmMakers},
  AddMaker in 'AddMaker.pas' {fmAddMaker},
  SetCardsPars in 'SetCardsPars.pas' {fmSetCardsParams},
  CliLic in 'CliLic.pas' {fmCliLic},
  AddCliLic in 'AddCliLic.pas' {fmAddCliLic},
  PreAlcogol in 'PreAlcogol.pas' {fmPreAlg},
  RepAlcogol in 'RepAlcogol.pas' {fmAlg},
  SElPerSkl3 in 'SElPerSkl3.pas' {fmSelPerSkl3},
  Tara in 'Tara.pas' {fmTara},
  AlcoDecl in 'AlcoDecl.pas' {fmAlcoE},
  AddAlcoDecl in 'AddAlcoDecl.pas' {fmAddAlco};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainRnOffice, fmMainRnOffice);
  Application.CreateForm(TdmO, dmO);
  Application.CreateForm(TfmMessure, fmMessure);
  Application.CreateForm(TfmAddMess, fmAddMess);
  Application.CreateForm(TfmAddClass, fmAddClass);
  Application.CreateForm(TfmPriceType, fmPriceType);
  Application.CreateForm(TfmMH, fmMH);
  Application.CreateForm(TfmAddGood, fmAddGood);
  Application.CreateForm(TfmFind, fmFind);
  Application.CreateForm(TfmAddEUGoods, fmAddEUGoods);
  Application.CreateForm(TfmClients, fmClients);
  Application.CreateForm(TfmAddClients, fmAddClients);
  Application.CreateForm(TfmDocsIn, fmDocsIn);
  Application.CreateForm(TfmPeriodUni, fmPeriodUni);
  Application.CreateForm(TfmAddDoc1, fmAddDoc1);
  Application.CreateForm(TfmTCard, fmTCard);
  Application.CreateForm(TfmCurMessure, fmCurMessure);
  Application.CreateForm(TfmMoveSel, fmMoveSel);
  Application.CreateForm(TfmDocsOut, fmDocsOut);
  Application.CreateForm(TfmAddDoc2, fmAddDoc2);
  Application.CreateForm(TfmDocsOutB, fmDocsOutB);
  Application.CreateForm(TfmDOBSpec, fmDOBSpec);
  Application.CreateForm(TdmORep, dmORep);
  Application.CreateForm(TfmTO, fmTO);
  Application.CreateForm(TfmCardsMove, fmCardsMove);
  Application.CreateForm(TfmAddInv, fmAddInv);
  Application.CreateForm(TfmDocsInv, fmDocsInv);
  Application.CreateForm(TfmFCards, fmFCards);
  Application.CreateForm(TfmRepPrib, fmRepPrib);
  Application.CreateForm(TfmRepOb, fmRepOb);
  Application.CreateForm(TfmDocsCompl, fmDocsCompl);
  Application.CreateForm(TfmAddCompl, fmAddCompl);
  Application.CreateForm(TfmDocsActs, fmDocsActs);
  Application.CreateForm(TfmAddAct, fmAddAct);
  Application.CreateForm(TfmAddDoc3, fmAddDoc3);
  Application.CreateForm(TfmDocsVn, fmDocsVn);
  Application.CreateForm(TfmAddOper, fmAddOper);
  Application.CreateForm(TfmAddDoc4, fmAddDoc4);
  Application.CreateForm(TfmRepPost, fmRepPost);
  Application.CreateForm(TfmTTKView, fmTTKView);
  Application.CreateForm(TfmCalcSeb, fmCalcSeb);
  Application.CreateForm(TfmTestPrice, fmTestPrice);
  Application.CreateForm(TfmRCategory, fmRCategory);
  Application.CreateForm(TfmRemnsSpeed, fmRemnsSpeed);
  Application.CreateForm(TfmAddBGU, fmAddBGU);
  Application.CreateForm(TfmSummary1, fmSummary1);
  Application.CreateForm(TfmTCardsAddPars, fmTCardsAddPars);
  Application.CreateForm(TfmSetStatusBZ, fmSetStatusBZ);
  Application.CreateForm(TdmCB, dmCB);
  Application.CreateForm(TfmMakers, fmMakers);
  Application.CreateForm(TfmSetCardsParams, fmSetCardsParams);
  Application.CreateForm(TfmCliLic, fmCliLic);
  Application.CreateForm(TfmAddCliLic, fmAddCliLic);
  Application.CreateForm(TfmPreAlg, fmPreAlg);
  Application.CreateForm(TfmAlg, fmAlg);
  Application.CreateForm(TfmTara, fmTara);
  Application.CreateForm(TfmAlcoE, fmAlcoE);
  Application.CreateForm(TfmAddAlco, fmAddAlco);
  Application.Run;
end.
