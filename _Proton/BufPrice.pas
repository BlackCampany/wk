unit BufPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxImageComboBox, Placemnt, cxContainer, cxTextEdit,
  cxMemo, cxLabel, ActnList, XPStyleActnCtrls, ActnMan, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxCheckBox, dxmdaset, FR_DSet, FR_DBSet,
  FR_Class, FR_BarC;

type
  TfmBufPrice = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewBufPr: TcxGridDBTableView;
    LevelBufPr: TcxGridLevel;
    GridBufPr: TcxGrid;
    ViewBufPrIDCARD: TcxGridDBColumn;
    ViewBufPrIDDOC: TcxGridDBColumn;
    ViewBufPrTYPEDOC: TcxGridDBColumn;
    ViewBufPrNEWP: TcxGridDBColumn;
    ViewBufPrSTATUS: TcxGridDBColumn;
    ViewBufPrNUMDOC: TcxGridDBColumn;
    ViewBufPrDATEDOC: TcxGridDBColumn;
    ViewBufPrName: TcxGridDBColumn;
    ViewBufPrCena: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    cxLabel2: TcxLabel;
    acBufPrice: TActionManager;
    acExit: TAction;
    Memo1: TcxMemo;
    acLoadCash: TAction;
    cxDateEdit1: TcxDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxDateEdit2: TcxDateEdit;
    cxButton2: TcxButton;
    acPeriod: TAction;
    cxCheckBox1: TcxCheckBox;
    ViewBufPrOLDP: TcxGridDBColumn;
    taLoad: TdxMemData;
    taLoadIdCard: TStringField;
    acPrintCenn: TAction;
    cxLabel3: TcxLabel;
    cxCheckBox2: TcxCheckBox;
    frBarCodeObject1: TfrBarCodeObject;
    cxLabel4: TcxLabel;
    PopupMenu1: TPopupMenu;
    acSetNo: TAction;
    acSeOtloj: TAction;
    N1: TMenuItem;
    N3: TMenuItem;
    acSetOk: TAction;
    N2: TMenuItem;
    acMoveT: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    acRemnT: TAction;
    N6: TMenuItem;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennQuant: TcxGridDBColumn;
    le1: TcxGridLevel;
    Le2: TcxGridLevel;
    cxButton1: TcxButton;
    ViewBufPrReserv1: TcxGridDBColumn;
    acPrintCen: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    cxLabel5: TcxLabel;
    acPost: TAction;
    acAddToScale: TAction;
    N9: TMenuItem;
    acFindPluNum: TAction;
    N10: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acLoadCashExecute(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acPrintCennExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acSetNoExecute(Sender: TObject);
    procedure acSetOkExecute(Sender: TObject);
    procedure acSeOtlojExecute(Sender: TObject);
    procedure acMoveTExecute(Sender: TObject);
    procedure acRemnTExecute(Sender: TObject);
    procedure ViewBufPrSelectionChanged(Sender: TcxCustomGridTableView);
    procedure cxButton1Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure acPostExecute(Sender: TObject);
    procedure acAddToScaleExecute(Sender: TObject);
    procedure acFindPluNumExecute(Sender: TObject);
    procedure ViewBufPrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBufPrice: TfmBufPrice;

implementation

uses MDB, Un1, PXDB, u2fdk, mCards, Move, Remn, MT, QuantMess, AddInScale,
  MainMCryst, MFB;

{$R *.dfm}

procedure TfmBufPrice.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
  GridBufPr.Align:=AlClient;
  ViewBufPr.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  Memo1.Clear;
  cxDateEdit1.Date:=Date;
  cxDateEdit2.Date:=Date;
  cxCheckBox1.Checked:=False;
end;

procedure TfmBufPrice.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewBufPr.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmBufPrice.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmBufPrice.acLoadCashExecute(Sender: TObject);
Var bOk:Boolean;
    sPath,StrWk,sCards,sBar,sNum,sCashLdd,sMessur,sCSz,sName:String;
    i,j,iNum,IdDoc,iTypeDoc:INteger;
    Rec:TcxCustomGridRecord;
    rPr,rNPr,rOPr:Real;
    NumPost:INteger;
begin
  //�������� ����
  if cxButton1.Enabled then
  begin
    with dmMc do
    begin
      try
        cxButton1.Enabled:=False;
        cxButton2.Enabled:=False;
        //������� �� ������
        bOk:=True;

//      �������� ������

        if ViewBufPr.Controller.SelectedRecordCount=0 then
        begin
          cxButton1.Enabled:=True;
          cxButton2.Enabled:=True;
          exit;
        end;

        Memo1.Clear;
        Memo1.Lines.Add('���� �������� ����..');

        if dmFB.Cash.Connected then Memo1.Lines.Add('  FB ����� ����.') else Memo1.Lines.Add('  ������ PX.');


        dmPx.Session1.Active:=False;
        dmPx.Session1.NetFileDir:=CommonSet.NetPath;
        dmPx.Session1.Active:=True;

        //������ �������� �� �������� �������
        StrWk:=FloatToStr(now);
        Delete(StrWk,1,2);
        Delete(StrWk,pos(',',StrWk),1);
        StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

        NumPost:=StrToINtDef(StrWk,0);
        sCards:='_'+IntToHex(NumPost,7);
        sBar:='_'+IntToHex(NumPost+1,7);

//        sCards:=StrWk;
//        sBar:=IntToStr(StrToInt(StrWk)+1);

        //����� �������� ������
        CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
        CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
        CopyFile(PChar(CommonSet.TrfPath+'Bars.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
        CopyFile(PChar(CommonSet.TrfPath+'Bars.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

        try
          with dmPx do
          begin
            CloseTe(taLoad);

            taCard.Active:=False;
            taCard.DatabaseName:=CommonSet.TmpPath;
            taCard.TableName:=sCards+'.db';
            taCard.Active:=True;

            taBar.Active:=False;
            taBar.DatabaseName:=CommonSet.TmpPath;
            taBar.TableName:=sBar+'.db';
            taBar.Active:=True;

            for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewBufPr.Controller.SelectedRecords[i];

              iNum:=0;
              rPr:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
                if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
              end;

              if taLoad.Locate('IdCard',iNum,[])=False then
              begin
                prAddCashLoad(iNum,1,rPr,0);    //iC,iT:INteger;rPr,rD:Real

                taLoad.Append; taLoadIdCard.AsInteger:=iNum; taLoad.Post;

                quFindC3.Active:=False;
                quFindC3.ParamByName('IDC').AsInteger:=iNum;
                quFindC3.Active:=True;

                quBars.Active:=False;
                quBars.ParamByName('GOODSID').AsInteger:=iNum;
                quBars.Active:=True;


                if quFindC3EdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

                sName:=prRetDName(quFindC3ID.AsInteger)+quFindC3FullName.AsString;
                prAddPosFB(1,quFindC3ID.AsInteger,quFindC3V11.AsInteger,0,quFindC3GoodsGroupID.AsInteger,Trunc(quCashRezerv.AsFloat),1,'','',sName,sMessur,'',0,0,RoundEx(quFindC3Prsision.AsFloat*1000)/1000,rv(rPr));

                taCard.Append;
                taCardArticul.AsString:=quFindC3ID.AsString;
                taCardName.AsString:=AnsiToOemConvert(sName);
                if quFindC3EdIzm.AsInteger=1 then
                begin
                  taCardMesuriment.AsString:=AnsiToOemConvert('��.');
                end
                else begin
                  taCardMesuriment.AsString:=AnsiToOemConvert('��.');
                end;
                taCardMesPresision.AsFloat:=RoundEx(quFindC3Prsision.AsFloat*1000)/1000;
                taCardAdd1.AsString:='';
                taCardAdd2.AsString:='';
                taCardAdd3.AsString:='';
                taCardAddNum1.AsFloat:=0;
                taCardAddNum2.AsFloat:=0;
                taCardAddNum3.AsFloat:=0;
                taCardScale.AsString:='NOSIZE';
                taCardGroop1.AsInteger:=quFindC3GoodsGroupID.AsInteger;
                taCardGroop2.AsInteger:=quFindC3SubGroupID.AsInteger;
                taCardGroop3.AsInteger:=0;
                taCardGroop4.AsInteger:=0;
                taCardGroop5.AsInteger:=0;
                taCardPriceRub.AsFloat:=rv(rPr);
                taCardPriceCur.AsFloat:=0;
                taCardClientIndex.AsInteger:=Trunc(quCashRezerv.AsFloat);
                taCardCommentary.AsString:='';
                taCardDeleted.AsInteger:=1;
                taCardModDate.AsDateTime:=Trunc(Date);
                taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
                taCardModPersonIndex.AsInteger:=0;
                taCard.Post;

                quBars.First;
                while not quBars.Eof do
                begin
                  if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                  else sCSz:='QUANTITY';

                  prAddPosFB(2,quFindC3ID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(rPr));

                  if taBar.Locate('BarCode',quBarsID.AsString,[])=False then
                  begin
                    taBar.Append;
                    taBarBarCode.AsString:=quBarsID.AsString;
                    taBarCardArticul.AsString:=quBarsGoodsID.AsString;
                    taBarCardSize.AsString:=sCSz;
                    taBarQuantity.AsFloat:=quBarsQuant.AsFloat;
                    taBar.Post;
                  end;

                  quBars.Next;
                end;

                quFindC3.Active:=False;
                quBars.Active:=False;
              end;
            end;
            taCard.Active:=False;
            taBar.Active:=False;
            taLoad.Active:=False;
          end;
        except
          Memo1.Lines.Add('������ ������������ ������.');
          Memo1.Lines.Add('  �������� ����������!!!');
          bOk:=False;
        end;

        quCash.Active:=False;
        quCash.Active:=True;
        while not quCash.Eof do
        begin
          if quCashLoad.AsInteger=1 then
          begin
            Memo1.Lines.Add(' '+quCashName.AsString);
            sPath:=CommonSet.CashPath+'\'+quCashNumber.AsString+'\';

            StrWk:=quCashNumber.AsString;
            while length(StrWk)<2 do strwk:='0'+StrWk;
            StrWk:='cash'+StrWk+'.ldd';
            sCashLdd:=StrWk;

            if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
            begin
              Memo1.Lines.Add('');
              Memo1.Lines.Add('  ����� ������������ ������.');
              Memo1.Lines.Add('  �������� ����������!!!');
              Memo1.Lines.Add('  ���������� �����...');
              Memo1.Lines.Add('');
              bOk:=False;
            end else
            begin //��� ����� ���������� �������� �����
              //���� �����������
              try
                with dmPx do
                begin
                  sNum:=quCashNumber.AsString+'\';

                  //���������

                  CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                  Delay(50);

                  CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                  CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                  CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                  CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                  StrWk:=quCashNumber.AsString;
                  while length(StrWk)<3 do strwk:='0'+StrWk;
                  StrWk:='cash'+StrWk+'.db';

                  if Fileexists(CommonSet.CashPath+StrWk)=False then
                  begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                    CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                    MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                  end;

                  taList.Active:=False;
                  taList.DatabaseName:=CommonSet.TmpPath;
                  taList.TableName:=CommonSet.CashPath+sNum+StrWk;
                  taList.Active:=True;

                  taList.Append;
                  taListTName.AsString:=sCards;
                  taListDataType.AsInteger:=1;
                  taListOperation.AsInteger:=1;
                  taList.Post;

                  taList.Append;
                  taListTName.AsString:=sBar;
                  taListDataType.AsInteger:=2;
                  taListOperation.AsInteger:=1;
                  taList.Post;

                  taList.Active:=False;

                  if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                  delay(50);
                  if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
                end; //dmPx
              except
                Memo1.Lines.Add('  ������ ������ � �������..');
                bOk:=False;
              end;
            end;

            Memo1.Lines.Add(' ��');
          end else
          begin
//            Memo1.Lines.Add(' '+quCashName.AsString+' �� �������.');
          end;

          quCash.Next;
        end;

        //������ �����

        if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
        if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
        if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
        if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

        if bOk then
        begin
          //��������� - ����� �������
          for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewBufPr.Controller.SelectedRecords[i];

            iNum:=0;
            rNPr:=0;
            IdDoc:=0;
            iTypeDoc:=0;
            rOPr:=0;

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrIDDOC' then IdDoc:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrTYPEDOC' then iTypeDoc:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrOLDP' then rOPr:=Rec.Values[j];
            end;

            //���������� ��� ����� � ��������
            quE.Active:=False;
            quE.SQL.Clear;
//           quE.SQL.Add('Update "A_BUFPRICE" Set STATUS=1, RECEDIT='''+dts(now)+''', PERSON =' +''''+Person.Name+'''');
            quE.SQL.Add('Update "A_BUFPRICE" Set STATUS=1');
            quE.SQL.Add('where IDCARD='+its(iNum));
            quE.SQL.Add('and IDDOC='+its(IdDoc));
            quE.SQL.Add('and TYPEDOC='+its(iTypeDoc));
            quE.ExecSQL;

            //������� ��������� ����
            prLogPrice(iNum,1,rOPr,rNPr);

            //��������

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "Goods" Set Cena='+fs(rNPr)+', Status=1');
            quE.SQL.Add('where ID='+its(iNum));
            quE.ExecSQL;

            //����� ����� � �����

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "ScalePLU" Set ');
            quE.SQL.Add('Price='+its(RoundEx(rNPr*100))+',');
            quE.SQL.Add('Status=0');
            quE.SQL.Add('where GoodsItem='+its(iNum));
            quE.ExecSQL;

          end;
          quBufPr.Active:=False;
          quBufPr.Active:=True;

          ViewBufPr.Controller.ClearSelection;

          Memo1.Lines.Add('�������� ��������� ��.');
        end
        else //����� ������� ������ �� ����
          Memo1.Lines.Add('������ ��� ��������. ��������� �����.');

      finally
        quCash.Active:=False;
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
        dmPx.Session1.Active:=False;
      end;
    end;
  end else
  begin
    showmessage('��������� ���������� ����������� ��������...');
  end;
end;

procedure TfmBufPrice.cxLabel2Click(Sender: TObject);
begin
  acLoadCash.Execute;
end;

procedure TfmBufPrice.acPeriodExecute(Sender: TObject);
begin
  //����� ��������� ���
  with dmMC do
  begin
    fmBufPrice.ViewBufPr.BeginUpdate;
    quBufPr.Active:=False;
    quBufPr.ParamByName('DATEB').AsDate:=Trunc(cxDateEdit1.Date);
    quBufPr.ParamByName('DATEE').AsDate:=Trunc(cxDateEdit2.Date);
    if cxCheckBox1.Checked then
      quBufPr.ParamByName('IST').AsInteger:=2 //���������� ���
    else
      quBufPr.ParamByName('IST').AsInteger:=0; //������ �������������
      
    quBufPr.Active:=True;
    fmBufPrice.ViewBufPr.EndUpdate;
  end;
end;

procedure TfmBufPrice.acPrintCennExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
    rNPr:Real;
begin
  //������ ��������
  if cxButton1.Enabled then
  begin
    with dmMc do
    begin
      try
        cxButton1.Enabled:=False;
        cxButton2.Enabled:=False;

//      �������� ������

        if ViewBufPr.Controller.SelectedRecordCount=0 then
        begin
          cxButton1.Enabled:=True;
          cxButton2.Enabled:=True;
          exit;
        end;

        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� ������������ ..');
        fmCards.ViCenn.BeginUpdate;
        CloseTe(taCen);

        for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewBufPr.Controller.SelectedRecords[i];

          iNum:=0; rNPr:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
            if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
          end;

          if iNum>0 then
          begin
            quFindC4.Active:=False;
            quFindC4.ParamByName('IDC').AsInteger:=iNum;
            quFindC4.Active:=True;
            if quFindC4.RecordCount>0 then
            begin
              taCen.Append;
              taCenIdCard.AsInteger:=iNum;
              taCenFullName.AsString:=quFindC4FullName.AsString;
              taCenCountry.AsString:=quFindC4NameCu.AsString;
              taCenPrice1.AsFloat:=rNPr;
              taCenPrice2.AsFloat:=0;
              taCenDiscount.AsFloat:=0;
              taCenBarCode.AsString:=quFindC4BarCode.AsString;
              taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
              if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
              taCenReceipt.AsString:=prFindReceipt(iNum);
              taCenDepName.AsString:=fmMainMCryst.Label3.Caption;
              taCensDisc.AsString:=prSDisc(iNum);
              taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
              taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
              taCenV02.AsInteger:=quFindC4V02.AsInteger;
              taCenV12.AsInteger:=quFindC4V12.AsInteger;
              taCenMaker.AsString:=quFindC4NAMEM.AsString;
              taCen.Post;
            end;

            quFindC4.Active:=False;
          end;

        end;
        Memo1.Lines.Add('������������ ��.');
        Memo1.Lines.Add('������ ...');

        RepCenn.LoadFromFile(CommonSet.Reports+quLabsFILEN.AsString);

        RepCenn.ReportName:='�������.';
        RepCenn.PrepareReport;
        vPP:=frAll;
        if cxCheckBox2.Checked then RepCenn.ShowPreparedReport
        else RepCenn.PrintPreparedReport('',1,False,vPP);
        CloseTe(taCen);
        fmMainMCryst.cxDEdit1.Date:=Date;
      finally
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
        fmCards.ViCenn.EndUpdate;
      end;
    end;
  end else
  begin
    showmessage('��������� ���������� ����������� ��������...');
  end;
end;

procedure TfmBufPrice.cxLabel3Click(Sender: TObject);
begin
  acPrintCenn.Execute;
end;

procedure TfmBufPrice.cxLabel4Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel5(ViewBufPr);
end;

procedure TfmBufPrice.acSetNoExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    IdDoc,iTypeDoc:INteger;
    rNPr,rOPr:Real;
    sStatus:String;
begin
  //���������� - �����������
  if not CanDo('prBufSetNo') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMc do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iNum:=0;
        rNPr:=0;
        IdDoc:=0;
        iTypeDoc:=0;
        rOPr:=0;
        sStatus:='';

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDDOC' then IdDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrTYPEDOC' then iTypeDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrOLDP' then rOPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrSTATUS' then sStatus:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_BUFPRICE" Set STATUS=0');
          quE.SQL.Add('where IDCARD='+its(iNum));
          quE.SQL.Add('and IDDOC='+its(IdDoc));
          quE.SQL.Add('and TYPEDOC='+its(iTypeDoc));
          quE.ExecSQL;

          if pos('1',sStatus)>0 then
          begin
            //������� ��������� ����
            prLogPrice(iNum,4,rOPr,rNPr);

            //��������     - �� ������ ����

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "Goods" Set Cena='+fs(rOPr)+', Status=1');
            quE.SQL.Add('where ID='+its(iNum));
            quE.ExecSQL;

            //����� ����� � ����� - �� ������ ����

            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "ScalePLU" Set ');
            quE.SQL.Add('Price='+its(RoundEx(rOPr*100))+',');
            quE.SQL.Add('Status=0');
            quE.SQL.Add('where GoodsItem='+its(iNum));
            quE.ExecSQL;
          end;
        end;
      end;
      quBufPr.Active:=False;
      quBufPr.Active:=True;
    finally
    end;
  end;
end;

procedure TfmBufPrice.acSetOkExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    IdDoc,iTypeDoc:INteger;
    rNPr,rOPr:Real;
begin
  //���������� - ���������
  if not CanDo('prBufSetOk') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMc do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iNum:=0;
        rNPr:=0;
        IdDoc:=0;
        iTypeDoc:=0;
        rOPr:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDDOC' then IdDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrTYPEDOC' then iTypeDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrOLDP' then rOPr:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_BUFPRICE" Set STATUS=1');
          quE.SQL.Add('where IDCARD='+its(iNum));
          quE.SQL.Add('and IDDOC='+its(IdDoc));
          quE.SQL.Add('and TYPEDOC='+its(iTypeDoc));
          quE.ExecSQL;

          //������� ��������� ����
          prLogPrice(iNum,5,rOPr,rNPr);
        end;
      end;
      quBufPr.Active:=False;
      quBufPr.Active:=True;
    finally
    end;
  end;
end;

procedure TfmBufPrice.acSeOtlojExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    IdDoc,iTypeDoc:INteger;
    rNPr,rOPr:Real;
begin
  //���������� - �������
  if not CanDo('prBufSetOtl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMc do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iNum:=0;
        rNPr:=0;
        IdDoc:=0;
        iTypeDoc:=0;
        rOPr:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDDOC' then IdDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrTYPEDOC' then iTypeDoc:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrOLDP' then rOPr:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "A_BUFPRICE" Set STATUS=2');
          quE.SQL.Add('where IDCARD='+its(iNum));
          quE.SQL.Add('and IDDOC='+its(IdDoc));
          quE.SQL.Add('and TYPEDOC='+its(iTypeDoc));
          quE.ExecSQL;

          //������� ��������� ����
          prLogPrice(iNum,6,rOPr,rNPr);
        end;
      end;
      quBufPr.Active:=False;
      quBufPr.Active:=True;
    finally
    end;
  end;
end;

procedure TfmBufPrice.acMoveTExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if quBufPr.RecordCount>0 then
    begin
      fmMove.ViewM.BeginUpdate;
      fmMove.GridM.Tag:=quBufPrIDCARD.AsInteger;

      prFillMove(quBufPrIDCARD.AsInteger);

{      quMove.Active:=False;
      quMove.ParamByName('IDCARD').Value:=quBufPrIDCARD.AsInteger;
      quMove.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quMove.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quMove.Active:=True;

      quMove.First;}
      
      fmMove.ViewM.EndUpdate;

      fmMove.Caption:=quCardsName.AsString+': �������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=False;
      fmMove.LevelM.Visible:=True;

      fmMove.Show;
    end;
  end;
end;

procedure TfmBufPrice.acRemnTExecute(Sender: TObject);
begin
  with dmMC do
  begin
    if quBufPr.RecordCount>0 then
    begin
      fmRemn.cxButton1.Enabled:=False;
      quRemn.Active:=False;
      delay(20);
      fmRemn.ViewR.BeginUpdate;
      quRemn.ParamByName('IDCARD').AsInteger:=quBufPrIDCARD.AsInteger;
      quRemn.Active:=True;
      fmRemn.ViewR.EndUpdate;

      fmRemn.Caption:='�������: '+quBufPrIDCARD.AsString+'  '+quBufPrName.AsString;
      fmRemn.cxButton1.Enabled:=True;
      fmRemn.Show;
    end;  
  end;
end;

procedure TfmBufPrice.ViewBufPrSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewBufPr.Controller.SelectedRecordCount>1 then exit;
  with dmMc do
  begin
    Vi1.BeginUpdate;
    quPost2.Active:=False;
    quPost2.ParamByName('IDCARD').Value:=quBufPrIDCARD.AsInteger;
    quPost2.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmBufPrice.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmBufPrice.acPrintCenExecute(Sender: TObject);
Var Str:TStringList;
    f:TextFile;
    StrWk,Str1,Str2,Str3,Str4,Str5:String;
    i,j,iNum,k,l:INteger;
    Rec:TcxCustomGridRecord;
    sBar:String;
begin
  //������ �� �����
  if ViewBufPr.Controller.SelectedRecordCount=0 then exit;

  fmQuant:=TfmQuant.Create(Application);
  fmQuant.cxSpinEdit1.Value:=1;
  fmQuant.cxCheckBox1.Checked:=False;
  fmQuant.ShowModal;
  if fmQuant.ModalResult=mrOk then
  begin
    Str:=TStringList.Create;
    try
      if FileExists(CurDir+'TRF\Termo.trf') then
      begin
        assignfile(f,CurDir+'TRF\Termo.trf');
        try
          reset(f);
          while not EOF(f) do
          begin
            ReadLn(f,StrWk);
            if StrWk[1]<>'#' then
            begin
              if fmMainMCryst.cxCheckBox1.Checked=False then  //c �����
              begin //��� ����
                if (pos('����',StrWk)=0) and (pos('�+=L:',StrWk)=0) and (Pos('@18.',StrWk)=0) then Str.Add(StrWk);
              end else
              begin // � �����
                Str.Add(StrWk);
              end;
            end;
          end;
        finally
          closefile(f);
        end;

        //���� ������� ���� , ������� �� ���������
        with dmMc do
        begin
          if fmQuant.cxCheckBox1.Checked then
          begin  //� �������
            if taCen.Active=False then CloseTe(taCen);
            fmCards.ViCenn.BeginUpdate;
            for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewBufPr.Controller.SelectedRecords[i];

              iNum:=0;
              for j:=0 to Rec.ValueCount-1 do
                if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then begin  iNum:=Rec.Values[j]; break; end;

              if iNum>0 then
              begin
                quFindC4.Active:=False;
                quFindC4.ParamByName('IDC').AsInteger:=iNum;
                quFindC4.Active:=True;
                if quFindC4.RecordCount>0 then
                begin
                  taCen.Append;
                  taCenIdCard.AsInteger:=iNum;
                  taCenFullName.AsString:=quFindC4FullName.AsString;
                  taCenCountry.AsString:=quFindC4NameCu.AsString;
                  taCenPrice1.AsFloat:=quFindC4Cena.AsFloat;
                  taCenPrice2.AsFloat:=0;
                  taCenDiscount.AsFloat:=0;
                  taCenBarCode.AsString:=quFindC4BarCode.AsString;
                  taCenEdIzm.AsInteger:=quFindC4EdIzm.AsInteger;
                  taCenQuant.AsInteger:=fmQuant.cxSpinEdit1.Value;
                  if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum) else taCenPluScale.AsString:='';
                  taCenReceipt.AsString:=prFindReceipt(iNum);
                  taCenDepName.AsString:=fmMainMCryst.Label3.Caption;
                  taCensDisc.AsString:=prSDisc(iNum);
                  taCenScaleKey.AsInteger:=quFindC4V08.AsInteger;
                  taCensDate.AsString:=ds1(fmMainMCryst.cxDEdit1.Date);
                  taCenV02.AsInteger:=quFindC4V02.AsInteger;
                  taCenV12.AsInteger:=quFindC4V12.AsInteger;
                  taCenMaker.AsString:=quFindC4NAMEM.AsString;
                  taCen.Post;
                end;
                quFindC4.Active:=False;
              end;
            end;

            fmCards.ViCenn.EndUpdate;

            StatusBar1.Panels[1].Text:='� ������� - '+IntToStr(taCen.RecordCount)+' ��������.'
          end else //������ -  ������ ������ �� �������
          begin
            if prOpenPrinter(CommonSet.TPrintN) then
            begin
              for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
              begin
                Rec:=ViewBufPr.Controller.SelectedRecords[i];

                iNum:=0;
                for j:=0 to Rec.ValueCount-1 do
                  if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then begin  iNum:=Rec.Values[j]; break; end;

                if iNum>0 then
                begin
                  quFindC4.Active:=False;
                  quFindC4.ParamByName('IDC').AsInteger:=iNum;
                  quFindC4.Active:=True;
                  if quFindC4.RecordCount>0 then
                  begin
                    for k:=0 to Str.Count-1 do
                    begin
                      StrWk:=Str[k];
                      if pos('@38.3@',StrWk)>0 then
                      begin
                        insert('E30',StrWk,pos('@38.3@',StrWk));
                        delete(StrWk,pos('@38.3@',StrWk),6)
                      end;

                      if pos('@',StrWk)>0 then
                      begin //���� ��� �� ���� ����������??
                        Str1:=Strwk;

                        Str2:=Copy(Str1,1,pos('@',Str1)-1); //A40,0,0,a,1,1,N," @3.30.36@"
                        delete(Str1,1,pos('@',Str1));//3.30.36@"

                        Str3:=Copy(Str1,1,pos('@',Str1)-1); //3.30.36
                        delete(Str1,1,pos('@',Str1)); //" - ������� ����� �������� , � ����� ���

                        Str4:=Copy(Str3,1,pos('.',Str3)-1); //3
                        if Str4='3' then   //��������
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //30.36
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=20;
                          Str5:=Copy(quFindC4FullName.AsString,1,l);
                          if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                        end;
                        if Str4='5' then  //������
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //5.30.36
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //30
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=20;
                          Str5:=Copy(quFindC4NameCu.AsString,1,l);
                          if CommonSet.TPrintCode=2 then Str5:=AnsiToOemConvert(Str5);
                        end;
                        if Str4='7' then  //�������     7.6.36
                        begin
                          Str5:=INtToStr(iNum);
                        end;
                        if Str4='18' then   //����   18.10.36
                        begin
                          Str5:=ToStr(quFindC4Cena.AsFloat);
                        end;
                        if Str4='31' then    //��
                        begin
                          Str5:=Str3;
                          delete(Str5,1,pos('.',Str5)); //31.13.8
                          Str5:=Copy(Str5,1,pos('.',Str5)-1); //13
                          l:=StrToIntDef(Str5,0);
                          if l=0 then l:=13;

                          //�� �� ������� ����
                          sBar:=quFindC4BarCode.AsString;
                          if Length(sBar)<13 then
                          begin
                            if Length(sBar)=7 then //������� ����
                            begin
                              sBar:=ToStandart(sBar);
                              l:=13;
                            end;
                          end;
                          Str5:=Copy(sBar,1,l);
                        end;
                        if Str4='39' then   //���-��
                        begin
                          Str5:=IntToStr(fmQuant.cxSpinEdit1.EditValue);
                        end;
                        StrWk:=Str2+Str5+Str1;
                      end;
                      prWritePrinter(StrWk+#$0D+#$0A);
                    end;
                  end;
                  quFindC4.Active:=False;
                end;
              end;
              prClosePrinter(CommonSet.TPrintN);
            end;
          end;
        end;
      end;
    finally
      Str.Free;
    end;
  end;
  fmQuant.Release;
end;

procedure TfmBufPrice.cxLabel5Click(Sender: TObject);
begin
  acPrintCen.Execute;
end;

procedure TfmBufPrice.acPostExecute(Sender: TObject);
begin
  //���������� ������
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMC do
  begin
    if not quBufPr.Eof then
    begin
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=quBufPrIDCARD.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=quBufPrIDCARD.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=quBufPrName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;
    end;
  end;
end;

procedure TfmBufPrice.acAddToScaleExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
begin
  //�������� ����� � ����
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMC do
  begin
    if ViewBufPr.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewBufPr.Controller.SelectedRecords[i];

      iNum:=0;
      rPr:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
        if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
      end;

      if (iNum>0) then
      begin
        quFindC4.Active:=False;
        quFindC4.ParamByName('IDC').AsInteger:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          if quFindC4TovarType.AsInteger=1 then   //������� �����
          begin
            taToScale.Append;
            taToScaleId.AsInteger:=iNum;
            taToScaleBarcode.AsString:=quFindC4BarCode.AsString;
            taToScaleName.AsString:=quFindC4FullName.AsString;
            taToScaleSrok.AsInteger:=quFindC4SrokReal.AsInteger;
            taToScalePrice.AsFloat:=rPr;
            taToScale.Post;
          end;
        end;
        quFindC4.Active:=False;
      end;
    end;
    quScSpr.Active:=False;
    quScSpr.Active:=True;
    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then  prAddToScale(quScSprNumber.AsString);

    quScSpr.Active:=False;
    taToScale.Active:=False;
    ViewBufPr.Controller.ClearSelection;
  end;
  fmAddInScale.Release;
end;

procedure TfmBufPrice.acFindPluNumExecute(Sender: TObject);
Var iNum,j:INteger;
    Rec:TcxCustomGridRecord;
    Str1:String;
begin
 //� �����
  if ViewBufPr.Controller.SelectedRecordCount>0 then
  begin
    Rec:=ViewBufPr.Controller.SelectedRecords[0];

    iNum:=0;
     for j:=0 to Rec.ValueCount-1 do
     begin
       if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then begin iNum:=Rec.Values[j]; break; end;
     end;

     Str1:=prFindPlu(iNum);

     showmessage('��� ������ '+INtToStr(iNum)+' � �����: '+Str1);
  end;
end;

procedure TfmBufPrice.ViewBufPrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var rPrM,rPrN:Real;

    i:SmallINt;
begin
//

  rPrM:=0;
  rPrN:=0;

  for i:=0 to ViewBufPr.ColumnCount-1 do
  begin
    if ViewBufPr.Columns[i].Name='ViewBufPrCena' then
      rPrM:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i],varDouble);
    if ViewBufPr.Columns[i].Name='ViewBufPrNEWP' then
      rPrN:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i],varDouble);
  end;

  if (rPrM-rPrN)>0.01 then
  begin
    ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� �����������
    ACanvas.Font.Color:=clGreen;
  end;
end;

end.
