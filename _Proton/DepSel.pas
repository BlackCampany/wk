unit DepSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxmdaset;

type
  TfmDepSel = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViDeps: TcxGridDBTableView;
    LeDeps: TcxGridLevel;
    GrDeps: TcxGrid;
    teDeps: TdxMemData;
    dsteDeps: TDataSource;
    teDepsId: TIntegerField;
    teDepsName: TStringField;
    teDepsNameFull: TStringField;
    ViDepsId: TcxGridDBColumn;
    ViDepsName: TcxGridDBColumn;
    ViDepsNameFull: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ViDepsCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDepSel: TfmDepSel;

implementation

{$R *.dfm}

procedure TfmDepSel.FormCreate(Sender: TObject);
begin
  GrDeps.Align:=AlClient;
end;

procedure TfmDepSel.ViDepsCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

end.
