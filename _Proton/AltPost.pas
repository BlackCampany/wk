unit AltPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons;

type
  TfmAltPost = class(TForm)
    GridAltPost: TcxGrid;
    ViewAltPost: TcxGridDBTableView;
    ViewAltPostIDCLI: TcxGridDBColumn;
    ViewAltPostName: TcxGridDBColumn;
    LevelAltPost: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    cxButton1: TcxButton;
    ViewAltPostFullName: TcxGridDBColumn;
    ViewAltPostIDALTCLI: TcxGridDBColumn;
    ViewAltPostAName: TcxGridDBColumn;
    ViewAltPostAFullName: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAltPost: TfmAltPost;

implementation
uses Un1;
{$R *.dfm}

procedure TfmAltPost.FormCreate(Sender: TObject);
begin
  GridAltPost.Align:=AlClient;
  ViewAltPost.RestoreFromIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni);
  FormPlacement1.IniFileName := CurDir+DelP(CommonSet.Ip)+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmAltPost.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewAltPost.StoreToIniFile(CurDir+DelP(CommonSet.Ip)+'\'+GridIni,False);
end;

procedure TfmAltPost.cxButton1Click(Sender: TObject);
begin
 close;
end;

end.
