object dmPro: TdmPro
  OldCreateOrder = False
  Left = 962
  Top = 441
  Height = 513
  Width = 968
  object OfficeRnDb: TpFIBDatabase
    DBName = '192.168.0.76:E:\_OfficeRn\DB\OFFICERN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelRH
    DefaultUpdateTransaction = trUpdRH
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'OfficeRnDb'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 12
  end
  object trSelRH: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdRH: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object trSelCards: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 184
  end
  object quDocsRId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM,'
      '    BZTYPE = :BZTYPE,'
      '    BZSTATUS = :BZSTATUS,'
      '    BZSUMR = :BZSUMR,'
      '    BZSUMF = :BZSUMF,'
      '    BZSUMS = :BZSUMS,'
      '    IEDIT = :IEDIT,'
      '    PERSEDIT = :PERSEDIT,'
      '    IPERSEDIT = :IPERSEDIT,'
      '    IDHCB = :IDHCB'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    IDHCB'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM,'
      '    :BZTYPE,'
      '    :BZSTATUS,'
      '    :BZSUMR,'
      '    :BZSUMF,'
      '    :BZSUMS,'
      '    :IEDIT,'
      '    :PERSEDIT,'
      '    :IPERSEDIT,'
      '    :IDHCB'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    IDHCB'
      'FROM'
      '    OF_DOCHEADOUTR '
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    IDHCB'
      'FROM'
      '    OF_DOCHEADOUTR '
      'Where ID=:IDH'
      '')
    Transaction = trSelRH
    Database = OfficeRnDb
    UpdateTransaction = trUpdRH
    AutoCommit = True
    Left = 612
    Top = 192
    poAskRecordCount = True
    object quDocsRIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsRIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsRIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRIdDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDocsRIdNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRIdIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsRIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsRIdSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsRIdSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDocsRIdSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDocsRIdSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDocsRIdPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsRIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsRIdIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsRIdIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quDocsRIdPERSEDIT: TFIBStringField
      FieldName = 'PERSEDIT'
      EmptyStrToNull = True
    end
    object quDocsRIdIPERSEDIT: TFIBIntegerField
      FieldName = 'IPERSEDIT'
    end
    object quDocsRIdBZTYPE: TFIBSmallIntField
      FieldName = 'BZTYPE'
    end
    object quDocsRIdBZSTATUS: TFIBSmallIntField
      FieldName = 'BZSTATUS'
    end
    object quDocsRIdBZSUMR: TFIBFloatField
      FieldName = 'BZSUMR'
    end
    object quDocsRIdBZSUMF: TFIBFloatField
      FieldName = 'BZSUMF'
    end
    object quDocsRIdBZSUMS: TFIBFloatField
      FieldName = 'BZSUMS'
    end
    object quDocsRIdIDHCB: TFIBIntegerField
      FieldName = 'IDHCB'
    end
  end
  object quSpecRSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    SUMOUT0 = :SUMOUT0,'
      '    SUMNDSOUT = :SUMNDSOUT,'
      '    OPER = :OPER,'
      '    BZQUANTR = :BZQUANTR,'
      '    BZQUANTF = :BZQUANTF,'
      '    BZQUANTS = :BZQUANTS,'
      '    BZQUANTSHOP = :BZQUANTSHOP'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS,'
      '    SUMOUT0,'
      '    SUMNDSOUT,'
      '    OPER,'
      '    BZQUANTR,'
      '    BZQUANTF,'
      '    BZQUANTS,'
      '    BZQUANTSHOP'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :SUMOUT0,'
      '    :SUMNDSOUT,'
      '    :OPER,'
      '    :BZQUANTR,'
      '    :BZQUANTF,'
      '    :BZQUANTS,'
      '    :BZQUANTSHOP'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,'
      '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP '
      ''
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,'
      '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP '
      ''
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      ''
      'where SP.IDHEAD=:IDHD'
      'ORDER BY SP.ID')
    Transaction = trSelRS
    Database = OfficeRnDb
    UpdateTransaction = trUpdRS
    AutoCommit = True
    Left = 688
    Top = 192
    object quSpecRSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecRSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecRSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecRSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRSelPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecRSelSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRSelTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecRSelNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecRSelSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSelIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecRSelSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecRSelNAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecRSelOPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
    object quSpecRSelPROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quSpecRSelOUTNDSSUM: TFIBFloatField
      FieldName = 'OUTNDSSUM'
    end
    object quSpecRSelNAMECTO: TFIBStringField
      FieldName = 'NAMECTO'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSelID1: TFIBIntegerField
      FieldName = 'ID1'
    end
    object quSpecRSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecRSelPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecRSelSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecRSelSUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quSpecRSelSUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quSpecRSelINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecRSelBZQUANTR: TFIBFloatField
      FieldName = 'BZQUANTR'
    end
    object quSpecRSelBZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quSpecRSelBZQUANTS: TFIBFloatField
      FieldName = 'BZQUANTS'
    end
    object quSpecRSelBZQUANTSHOP: TFIBFloatField
      FieldName = 'BZQUANTSHOP'
    end
  end
  object trSelRS: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 92
    Top = 72
  end
  object trUpdRS: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 92
    Top = 128
  end
  object quC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ca.ID,'
      '    ca.PARENT,'
      '    ca.NAME,'
      '    ca.TTYPE,'
      '    ca.IMESSURE,'
      '    ca.INDS,'
      '    ca.MINREST,'
      '    ca.LASTPRICEIN,'
      '    ca.LASTPRICEOUT,'
      '    ca.LASTPOST,'
      '    ca.IACTIVE,'
      '    ca.TCARD,'
      '    ca.CATEGORY,'
      '    ca.SPISSTORE,'
      '    ca.BB,'
      '    ca.GG,'
      '    ca.U1,'
      '    ca.U2,'
      '    ca.EE,'
      '    ca.RCATEGORY,'
      '    ca.COMMENT,'
      '    ca.CTO,'
      '    ca.CODEZAK,'
      '    nds.PROC'
      'FROM'
      '    OF_CARDS ca'
      'left join OF_NDS nds on nds.ID=ca.INDS'
      'where(  ca.CODEZAK=:ICODE'
      '     ) and (     CA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ca.ID,'
      '    ca.PARENT,'
      '    ca.NAME,'
      '    ca.TTYPE,'
      '    ca.IMESSURE,'
      '    ca.INDS,'
      '    ca.MINREST,'
      '    ca.LASTPRICEIN,'
      '    ca.LASTPRICEOUT,'
      '    ca.LASTPOST,'
      '    ca.IACTIVE,'
      '    ca.TCARD,'
      '    ca.CATEGORY,'
      '    ca.SPISSTORE,'
      '    ca.BB,'
      '    ca.GG,'
      '    ca.U1,'
      '    ca.U2,'
      '    ca.EE,'
      '    ca.RCATEGORY,'
      '    ca.COMMENT,'
      '    ca.CTO,'
      '    ca.CODEZAK,'
      '    nds.PROC'
      'FROM'
      '    OF_CARDS ca'
      'left join OF_NDS nds on nds.ID=ca.INDS'
      'where ca.CODEZAK=:ICODE')
    Transaction = trSelCards
    Database = OfficeRnDb
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 760
    Top = 192
    poAskRecordCount = True
    object quCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quCTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object quCIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quCINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quCMINREST: TFIBFloatField
      FieldName = 'MINREST'
    end
    object quCLASTPRICEIN: TFIBFloatField
      FieldName = 'LASTPRICEIN'
    end
    object quCLASTPRICEOUT: TFIBFloatField
      FieldName = 'LASTPRICEOUT'
    end
    object quCLASTPOST: TFIBIntegerField
      FieldName = 'LASTPOST'
    end
    object quCIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quCTCARD: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quCCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quCSPISSTORE: TFIBStringField
      FieldName = 'SPISSTORE'
      EmptyStrToNull = True
    end
    object quCBB: TFIBFloatField
      FieldName = 'BB'
    end
    object quCGG: TFIBFloatField
      FieldName = 'GG'
    end
    object quCU1: TFIBFloatField
      FieldName = 'U1'
    end
    object quCU2: TFIBFloatField
      FieldName = 'U2'
    end
    object quCEE: TFIBFloatField
      FieldName = 'EE'
    end
    object quCRCATEGORY: TFIBIntegerField
      FieldName = 'RCATEGORY'
    end
    object quCCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quCCTO: TFIBIntegerField
      FieldName = 'CTO'
    end
    object quCCODEZAK: TFIBIntegerField
      FieldName = 'CODEZAK'
    end
    object quCPROC: TFIBFloatField
      FieldName = 'PROC'
    end
  end
  object quMH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK'
      'FROM'
      '    OF_CARDS '
      'where(  CODEZAK=:ICODE'
      '     ) and (     OF_CARDS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PARENT,'
      '    ITYPE,'
      '    NAMEMH,'
      '    DEFPRICE,'
      '    EXPNAME,'
      '    NUMSPIS,'
      '    ISS,'
      '    GLN'
      'FROM'
      '    OF_MH '
      'where GLN=:SGLN')
    Transaction = trSel
    Database = OfficeRnDb
    Left = 180
    Top = 12
    poAskRecordCount = True
    object quMHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quMHNUMSPIS: TFIBSmallIntField
      FieldName = 'NUMSPIS'
    end
    object quMHISS: TFIBSmallIntField
      FieldName = 'ISS'
    end
    object quMHGLN: TFIBStringField
      FieldName = 'GLN'
      EmptyStrToNull = True
    end
  end
  object quCli: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK'
      'FROM'
      '    OF_CARDS '
      'where(  CODEZAK=:ICODE'
      '     ) and (     OF_CARDS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMECL,'
      '    FULLNAMECL,'
      '    INN,'
      '    PHONE,'
      '    MOL,'
      '    COMMENT,'
      '    INDS,'
      '    IACTIVE,'
      '    KPP,'
      '    ADDRES,'
      '    NAMEOTP,'
      '    ADROTPR,'
      '    RSCH,'
      '    KSCH,'
      '    BANK,'
      '    BIK,'
      '    SROKPLAT,'
      '    GLN'
      'FROM'
      '    OF_CLIENTS '
      'where GLN=:SGLN')
    Transaction = trSel
    Database = OfficeRnDb
    Left = 240
    Top = 12
    poAskRecordCount = True
    object quCliID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCliNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quCliFULLNAMECL: TFIBStringField
      FieldName = 'FULLNAMECL'
      Size = 300
      EmptyStrToNull = True
    end
    object quCliINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quCliPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliMOL: TFIBStringField
      FieldName = 'MOL'
      Size = 100
      EmptyStrToNull = True
    end
    object quCliCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 500
      EmptyStrToNull = True
    end
    object quCliINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quCliIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quCliKPP: TFIBStringField
      FieldName = 'KPP'
      Size = 15
      EmptyStrToNull = True
    end
    object quCliADDRES: TFIBStringField
      FieldName = 'ADDRES'
      Size = 150
      EmptyStrToNull = True
    end
    object quCliNAMEOTP: TFIBStringField
      FieldName = 'NAMEOTP'
      Size = 150
      EmptyStrToNull = True
    end
    object quCliADROTPR: TFIBStringField
      FieldName = 'ADROTPR'
      Size = 100
      EmptyStrToNull = True
    end
    object quCliRSCH: TFIBStringField
      FieldName = 'RSCH'
      EmptyStrToNull = True
    end
    object quCliKSCH: TFIBStringField
      FieldName = 'KSCH'
      EmptyStrToNull = True
    end
    object quCliBANK: TFIBStringField
      FieldName = 'BANK'
      Size = 150
      EmptyStrToNull = True
    end
    object quCliBIK: TFIBStringField
      FieldName = 'BIK'
      Size = 9
      EmptyStrToNull = True
    end
    object quCliSROKPLAT: TFIBIntegerField
      FieldName = 'SROKPLAT'
    end
    object quCliGLN: TFIBStringField
      FieldName = 'GLN'
      EmptyStrToNull = True
    end
  end
  object taNums: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCNUMS'
      'SET '
      '    SPRE = :SPRE,'
      '    CURNUM = :CURNUM'
      'WHERE'
      '    IT = :OLD_IT'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCNUMS'
      'WHERE'
      '        IT = :OLD_IT'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCNUMS('
      '    IT,'
      '    SPRE,'
      '    CURNUM'
      ')'
      'VALUES('
      '    :IT,'
      '    :SPRE,'
      '    :CURNUM'
      ')')
    RefreshSQL.Strings = (
      'SELECT first 1'
      '    IT,'
      '    SPRE,'
      '    CURNUM'
      'FROM'
      '    OF_DOCNUMS '
      'where(  IT=8'
      '     ) and (     OF_DOCNUMS.IT = :OLD_IT'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT first 1'
      '    IT,'
      '    SPRE,'
      '    CURNUM'
      'FROM'
      '    OF_DOCNUMS '
      'where IT=:ITYPE'
      'order by CURNUM desc')
    Transaction = trSel
    Database = OfficeRnDb
    AutoCommit = True
    Left = 468
    Top = 196
    object taNumsIT: TFIBIntegerField
      FieldName = 'IT'
    end
    object taNumsSPRE: TFIBStringField
      FieldName = 'SPRE'
      Size = 5
      EmptyStrToNull = True
    end
    object taNumsCURNUM: TFIBIntegerField
      FieldName = 'CURNUM'
    end
  end
  object prGetId: TpFIBStoredProc
    Transaction = trUpd
    Database = OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_GETID (?ITYPE)')
    StoredProcName = 'PR_GETID'
    Left = 368
    Top = 312
    qoStartTransaction = True
  end
  object trSel: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 16
    Top = 248
  end
  object trUpd: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 92
    Top = 248
  end
  object trSetSync: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 832
    Top = 192
  end
  object trGetSync: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 540
    Top = 196
  end
  object quSetSync: TpFIBQuery
    Transaction = trSetSync
    Database = OfficeRnDb
    Left = 544
    Top = 252
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDocsRCBID: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    IDHCB'
      'FROM'
      '    OF_DOCHEADOUTR '
      'Where IDHCB=:IDCB'
      'and DATEDOC>=:IDATEB'
      ''
      '')
    Transaction = trSelRH
    Database = OfficeRnDb
    UpdateTransaction = trUpdRH
    AutoCommit = True
    Left = 368
    Top = 368
    poAskRecordCount = True
    object quDocsRCBIDID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsRCBIDDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsRCBIDNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRCBIDDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDocsRCBIDNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRCBIDIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRCBIDIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRCBIDSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsRCBIDSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsRCBIDSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsRCBIDSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDocsRCBIDSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDocsRCBIDSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDocsRCBIDPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsRCBIDIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsRCBIDIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsRCBIDBZTYPE: TFIBSmallIntField
      FieldName = 'BZTYPE'
    end
    object quDocsRCBIDBZSTATUS: TFIBSmallIntField
      FieldName = 'BZSTATUS'
    end
    object quDocsRCBIDBZSUMR: TFIBFloatField
      FieldName = 'BZSUMR'
    end
    object quDocsRCBIDBZSUMF: TFIBFloatField
      FieldName = 'BZSUMF'
    end
    object quDocsRCBIDBZSUMS: TFIBFloatField
      FieldName = 'BZSUMS'
    end
    object quDocsRCBIDIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quDocsRCBIDPERSEDIT: TFIBStringField
      FieldName = 'PERSEDIT'
      EmptyStrToNull = True
    end
    object quDocsRCBIDIPERSEDIT: TFIBIntegerField
      FieldName = 'IPERSEDIT'
    end
    object quDocsRCBIDIDHCB: TFIBIntegerField
      FieldName = 'IDHCB'
    end
  end
  object taNumSCHF: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_NUMSSCHF'
      'SET '
      '    SPRE = :SPRE,'
      '    CURNUM = :CURNUM'
      'WHERE'
      '    IMH = :OLD_IMH'
      '    and IYY = :OLD_IYY'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_NUMSSCHF'
      'WHERE'
      '        IMH = :OLD_IMH'
      '    and IYY = :OLD_IYY'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_NUMSSCHF('
      '    IMH,'
      '    IYY,'
      '    SPRE,'
      '    CURNUM'
      ')'
      'VALUES('
      '    :IMH,'
      '    :IYY,'
      '    :SPRE,'
      '    :CURNUM'
      ')')
    RefreshSQL.Strings = (
      'SELECT IMH,'
      '       IYY,'
      '       SPRE,'
      '       CURNUM'
      'FROM OF_NUMSSCHF'
      'where(  IMH=:IMH'
      'and IYY=:IYY'
      '     ) and (     OF_NUMSSCHF.IMH = :OLD_IMH'
      '    and OF_NUMSSCHF.IYY = :OLD_IYY'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT IMH,'
      '       IYY,'
      '       SPRE,'
      '       CURNUM'
      'FROM OF_NUMSSCHF'
      'where IMH=:IMH'
      'and IYY=:IYY')
    Transaction = trSel
    Database = OfficeRnDb
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 464
    Top = 252
    poAskRecordCount = True
    object taNumSCHFIMH: TFIBIntegerField
      FieldName = 'IMH'
    end
    object taNumSCHFIYY: TFIBIntegerField
      FieldName = 'IYY'
    end
    object taNumSCHFSPRE: TFIBStringField
      FieldName = 'SPRE'
      Size = 10
      EmptyStrToNull = True
    end
    object taNumSCHFCURNUM: TFIBIntegerField
      FieldName = 'CURNUM'
    end
  end
  object quClassTree: TpFIBDataSet
    SelectSQL.Strings = (
      'Select ID, NAMECL from OF_CLASSIF'
      'where ID_PARENT=:PARENTID'
      'and ITYPE=:CLTYPE'
      'Order by NAMECL')
    Transaction = trSelCards
    Database = OfficeRnDb
    Left = 692
    Top = 256
    poAskRecordCount = True
  end
  object imState: TImageList
    Left = 772
    Top = 256
    Bitmap = {
      494C010130003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D0000000010020000000000000D0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D600C6BD
      C600FFD6DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F7F70094B5A5009C52
      5200CE212900FFD6DE0000000000000000000000000084848400848484008484
      8400848484008484840042E72100FFFFFF00CED6D600A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A50000000000000000000000000000000000000000000000
      000000000000CEC6BD00A59CBD007B7BBD007B7BB5008C8CAD00B5ADAD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6BDBD00A5B59C0084AD840084AD840094A59400B5ADA5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6D6D600847B7B00CE21
      2900FF6B7300F7E7E7000000000000000000000000000000000000C6210000C6
      21000042000000C62100FFFFFF0000C62100CED6D600CED6D600CED6D600CED6
      D600CED6D600A5A5A50042212100000000000000000000000000000000000000
      0000B5B5CE005252CE001010DE000000EF000000F7001010EF004242CE00948C
      AD00000000000000000000000000000000000000000000000000000000000000
      0000B5C6AD005AAD5A0029AD290021BD310029C6390031BD4A0052B5630094A5
      9400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF0094B5A5009C525200CE21
      2900F7CEC60000000000000000000000000000000000000000000042000000C6
      210000C6210042E721000042000000420000CED6D600A5A5A500A5A5A500A5A5
      A50084848400CED6D6004242420000000000000000000000000000000000B5B5
      C6003131C6000000D6000000E7000000EF000000F7000000FF000000FF002121
      DE00948CAD00000000000000000000000000000000000000000000000000B5C6
      B500429C310010A5080021B521008CCE940094D69C0031CE520029CE4A0042BD
      520094A594000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7DEDE00847B7B00CE212900FF6B
      7300FFF7F70000000000000000000000000000000000000000000042000042E7
      2100FFFFFF000042000000420000C6DEC600C6DEC600C6DEC600C6DEC600C6DE
      C600F7FFFF00A5A5A50042212100000000000000000000000000D6CEC6005A5A
      BD000000C6000000D6000000E7000000EF000000F7000000FF000000FF000000
      FF003939CE000000000000000000000000000000000000000000D6C6C60063A5
      5A0010940000189C080029AD2900ADC6AD00B5CEBD0039CE520029C64A0021C6
      390052AD5A000000000000000000000000000000000000000000000000000000
      000000000000F7F7F700FFF7F700EFEFEF00C6BDC600847B7B00BD949400FFD6
      DE0000000000000000000000000000000000000000000000000000C6210042E7
      21000042000084E7A50084848400FFFFFF00FFFFFF00F7FFFF00C6DEC600F7FF
      FF00F7FFFF008484840042212100000000000000000000000000BDB5C6002121
      BD002121CE002929D6002929DE002929E7002929EF002929F7002929F7002121
      F7001010E7008C8CAD0000000000000000000000000000000000BDC6B500318C
      180021940800299C180039AD3100A5BDA500ADC6B50042C6520031C64A0029C6
      390029BD310094A58C000000000000000000000000000000000000000000EFEF
      F700C6B5B500C6B5B500E7C6BD00D6B5A500D6B5A500D6B5A500EFEFEF000000
      000000000000000000000000000000000000000000000042000000420000FFFF
      FF00FFFFFF00FFFFFF000042000000420000CED6D600C6DEC600CED6D600F7FF
      FF00F7FFFF00FFFFFF0042424200000000000000000000000000B5B5CE005252
      CE00C6C6E700DEDEE700D6D6E700CECEDE00C6C6D600C6C6D600CECED600ADAD
      DE001818EF007373B50000000000000000000000000000000000BDC6B5005AA5
      42009CC69400C6D6C600BDCEB500BDBDBD00B5BDB500ADBDAD00B5C6B5008CCE
      940021BD21007BAD7B0000000000000000000000000000000000EFEFF700C6B5
      B500E7C6BD00FFEFCE00FFF7DE00FFEFCE00FFDEBD00FFCEB500F7DED6000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00F7FFFF00FFFF
      FF00F7FFFF00F7FFFF0042424200000000000000000000000000BDBDCE009494
      DE00E7E7F700EFEFF700DEDEEF00D6D6E700CECEDE00CECED600C6C6D600ADAD
      DE001818E7007B7BBD0000000000000000000000000000000000C6C6BD009CC6
      8C00D6E7CE00DEE7DE00C6D6C600CECECE00BDC6BD00A5BDA500A5BDA50084C6
      840018B5180084AD7B0000000000000000000000000000000000C6BDC600E7C6
      BD0000000000FFFFF700FFF7E700FFF7DE00FFE7CE00FFCEB500FFCEB500F7DE
      D600000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00F7FFFF00F7FFFF00C6DEC600CED6D600CED6D600CED6D600CED6D600CED6
      D600F7FFFF00F7FFFF0042424200000000000000000000000000C6C6C600A5A5
      DE00A5A5E7009494E7007373DE005252D6004242D6003939DE004242DE003939
      DE002929D600A59CBD0000000000000000000000000000000000C6C6C600ADCE
      A500A5C694008CBD7B0073B56300D6DECE00C6D6C6004AAD390039AD290031B5
      290031AD2900A5B59C00000000000000000000000000EFEFF700BD949400F7EF
      EF000000000000000000FFFFF700FFF7DE00FFE7CE00FFC6B500FFCEB500FFDE
      BD00F7EFEF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00C6DEC600F7FFFF00F7FFFF00FFFFFF00F7FFFF00C6DEC600FFFFFF00FFFF
      FF00FFFFFF00F7FFFF0042424200000000000000000000000000D6CEC600BDBD
      D600A5A5E7008C8CDE008484DE007373DE005A5ADE005252DE004242DE003939
      DE006B6BCE000000000000000000000000000000000000000000D6CECE00BDCE
      BD00ADCEA5009CC694009CC68C00EFF7EF00E7EFE70073BD63005AB54A004AB5
      390073B56B0000000000000000000000000000000000E7DEDE00BD949400FFF7
      E700FFFFF700FFFFF700FFF7E700FFF7DE00FFEFCE00FFC6B500FFCEB500FFE7
      CE00F7EFEF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00CED6D600FFFFFF00FFFF
      FF00FFFFFF00F7FFFF004242420000000000000000000000000000000000C6C6
      C600BDBDD600A5A5E7009494E7008484DE007B7BDE006B6BDE005A5ADE006B6B
      D600BDB5CE00000000000000000000000000000000000000000000000000C6C6
      C600BDCEBD00B5CEA500ADCE9C00DEEFD600D6E7CE0084C673006BBD5A0073B5
      6B00BDC6B50000000000000000000000000000000000FFF7F700BD949400FFE7
      CE00FFF7E700FFF7DE00FFF7DE00FFEFCE00FFDEBD00FFCEB500FFFFF700FFF7
      E700000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00CED6D600CED6D600CED6D600CED6D600CED6D600CED6D600FFFFFF00FFFF
      FF00FFFFFF00F7FFFF0042424200000000000000000000000000000000000000
      0000C6C6C600BDBDD600ADADDE009C9CDE008C8CDE008484D6009494CE00BDBD
      CE00000000000000000000000000000000000000000000000000000000000000
      0000C6C6C600C6CEBD00B5CEAD00ADCEA5009CC6940094BD84009CBD9400C6CE
      BD00000000000000000000000000000000000000000000000000D6D6D600D6B5
      A500FFEFCE00FFEFCE00FFDEBD00FFCEB500FFC6B500FFE7CE0000000000F7E7
      E700000000000000000000000000000000000000000000000000C6DEC600CED6
      D600F7FFFF00CED6D600C6DEC600F7FFFF00CED6D600F7FFFF00F7FFFF00CED6
      D600F7FFFF00A5A5A50042424200000000000000000000000000000000000000
      000000000000D6CEC600C6C6C600C6C6CE00BDBDCE00C6C6CE00D6CEC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6CECE00C6C6C600C6CEC600C6CEBD00C6CEC600D6CECE000000
      000000000000000000000000000000000000000000000000000000000000E7DE
      DE00F7CEC600FFDEBD00FFDEBD00FFCEB500FFDEBD00FFE7CE00F7DED600FFF7
      F70000000000000000000000000000000000000000000000000084E7E70042E7
      E70084E7E70042E7E70084E7E70084E7E70042E7E70084E7E70084E7E70042E7
      E70084E7E70000E7E70042424200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFF7E700F7CEC600F7CEC600F7DED600F7DED600F7EFEF000000
      0000000000000000000000000000000000000000000000000000F7FFFF00F7FF
      FF0084E7E700F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF0000214200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFF7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADA59C00737373005A5A
      5A00424242004A4A4A00ADADAD003939390021212100424242004A4A4A004242
      4200636363005252520052525200636363000000000000000000000000000000
      00009CA5AD000000000000000000000000000000000000000000000000000000
      000000000000848C8C0018181800848C8C000000000000000000000000000000
      0000949494006363630063636300636363006363630063636300636363006363
      6300636363006363630094949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADA59C00736B
      6B004A4A4A004A525200738CAD005A6B8C003131310018181800393939004242
      4200393939004A4A4A00948C84000000000000000000C6B59400CEB59400634A
      18009C6B21009C6B21009C6B21009C6B21009C6B21009C6B21009C6B21009C6B
      2100BDA573001008000000000000181818000000000000000000000000000000
      0000636363008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C0063636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFD6
      C600B5ADAD006B7B94005A84CE00638CDE006384B50029314200212121005A5A
      5A006B6B6300BDADA500000000000000000000000000DEC68C00E7C68C00E7C6
      8C00D6AD5A00D6AD5A00D6AD5A00D6AD5A00D6AD5A00CEA55200C69C4A00B58C
      4200BD9C52005A42180018181800848C8C000000000000000000000000000000
      0000636363008C8C8C008C8C8C008C8C8C00848C8C0039ADDE00848484008484
      84007B847B007B7B7B0052525200000000000000000000000000000000000000
      000000000000CEC6C600A5B5A50084AD840084AD840094A59400B5B5AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009CBDF70084B5F7006B9CEF005A84D600638CDE006B8CCE00424A5A00847B
      7300DECEC60000000000000000000000000000000000000000002973AD002173
      A50073A5C60021638C00216B9C00216B94006B94AD0018527B00185A7B00215A
      8400D6B57300D6BD9C00CEBD9400CED6D6000000000000000000000000000000
      00005A5A5A0084848400848484007B847B0039ADDE0039ADDE0039ADDE00737B
      7B0073737300737373004A4A4A00000000000000000000000000000000000000
      0000BDC6B5005AAD5A0029AD290031BD390029C6420031BD4A0052B5630094A5
      940000000000000000000000000000000000000000000000000000000000B5C6
      E7008CBDFF008CBDFF008CBDFF0073A5F700638CE70073A5EF0094A5CE00E7CE
      BD00000000000000000000000000000000000000000000000000297BAD0052CE
      F7008CDEF7003184AD004ABDE7004ABDE7008CCEEF00297BA50042A5D600215A
      8400DEC68C00BD9C6B009C6B21009CA5AD00000000009494940063636300B5B5
      B500525252007B7B7B007B7B7B0052525200297BB5004ACEF700297BB5004A4A
      4A006B6B6B00636363004242420000000000000000000000000000000000BDC6
      B500429C310010A508004ABD4A00A5CEA50052CE6B0029CE4A0029CE4A0042BD
      520094A594000000000000000000000000000000000000000000E7D6CE00A5BD
      F70094BDFF008CBDFF008CBDFF008CB5FF006B9CF700739CEF00A5BDE7000000
      0000000000000000000000000000000000000000000000000000297BAD004ABD
      E70073CEE700318CB5004AB5DE004AB5DE0073BDDE00317BAD00429CCE002163
      8C00DECEAD00BD9C6B00C6B594007B84840000000000636363008C8C8C00C6C6
      C6004A4A4A006B6B6B004A4A4A006B6B6B0063636300297BB500636363006363
      6300424242005A5A5A0039393900000000000000000000000000D6CECE0063A5
      5200109400006BBD6300CED6CE00D6D6D600BDD6BD008CD69C0039CE5A0021C6
      390052AD5A000000000000000000000000000000000000000000DED6DE00A5C6
      FF0094BDFF0094BDFF0094BDFF0094BDFF007BADF7006B9CEF0094ADE700EFD6
      CE000000000000000000000000000000000000000000000000004A94BD0052D6
      FF0094E7FF00318CB50052CEF70052CEF7008CD6F7003184AD004AB5E7002973
      A500E7E7CE00CEB594009CA5AD004A4A4A0000000000636363008C8C8C00C6C6
      C60042424200424242005A525A00525252005252520052525200525252005252
      5200525252003939390039393900000000000000000000000000BDC6B500318C
      1800188C00005AAD4A00C6D6C600DEDEDE00C6D6C600D6D6D6009CCEA50029C6
      390029BD310094A58C0000000000000000000000000000000000CECEDE00ADCE
      FF009CC6FF009CC6FF009CC6FF009CC6FF008CB5FF006B9CEF005A7BB500B5AD
      A5000000000000000000000000000000000000000000000000004A94BD004A94
      BD006BADCE00296B94003184B5003184B500639CC60021638C002973AD002973
      A50000000000DECEAD000000000000000000000000005A5A5A0084848400C6C6
      C600393939005252520052525200525252005252520052525200525252005252
      5200525252005252520039393900000000000000000000000000BDC6B500529C
      4200218400002194080042AD310094CE94005AC65A00A5CEAD00CED6CE004AC6
      520018BD18007BAD7B0000000000000000000000000000000000D6D6E700BDD6
      FF00A5CEFF00A5CEFF00ADCEFF00A5CEFF0094BDFF006B94E7005A84C6005252
      5A000000000000000000000000000000000000000000000000004A94BD0073CE
      EF0094D6EF004A9CB50063CEE70063C6E70073C6E700318CAD004AADDE002973
      A5000000000000000000000000000000000000000000525252007B7B7B00BDBD
      BD005A5A5A003939390039393900393939003939390039393900393939003939
      390039393900393939006B737300000000000000000000000000C6C6BD0094BD
      840084BD7300C6DEBD0073BD6B00189C100018A510008CCE8C00DEDEDE0052C6
      520010B5100084AD7B0000000000000000000000000000000000D6D6E700C6DE
      FF00B5D6FF00BDD6FF00BDDEFF00BDDEFF0094B5EF00394A6B0031426B002121
      2900B5ADA500000000000000000000000000000000000000000063ADD60084E7
      FF00B5EFFF004294B50073DEFF006BDEFF009CE7FF00318CAD0052C6EF004294
      BD0000000000000000000000000000000000000000004A4A4A006B6B6B008484
      8400B5B5B500B5B5B50094BDDE003194CE0084A5B500A5A5A500849CAD003194
      CE00000000000000000000000000000000000000000000000000CEC6C600ADC6
      A500ADCEA500F7FFF700E7EFDE006BB55A0063BD5A00D6E7CE00DEE7D6004ABD
      420031AD2900A5B59C0000000000000000000000000000000000DEDEEF00DEEF
      FF00CEE7FF00D6E7FF00DEEFFF00CEE7FF008CB5E70031313900000000001010
      1000AD9C9C00000000000000000000000000000000000000000084C6DE004A94
      BD0073ADCE00316B94004A94BD004A94BD0073ADCE00316B94004A94BD007BBD
      DE00000000000000000000000000000000000000000042424200424242005A52
      5A005252520052525200ADADAD008C8C8C003939390039393900393939008C8C
      8C00000000000000000000000000000000000000000000000000D6CEC600BDCE
      BD00ADCEA500CEE7C6000000000000000000FFFFFF00F7F7F700A5D69C004AB5
      390073B56B00000000000000000000000000000000000000000000000000BDC6
      D600C6D6EF00DEEFFF00EFF7FF00CEE7FF007B94BD0031313100080808001818
      1800ADA5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B8484004A4A4A000000000039393900525252005252
      5200525252005252520084848400ADADAD00ADADAD00ADADAD00ADADAD007B7B
      7B0000000000000000000000000000000000000000000000000000000000C6C6
      C600BDCEB500B5D6A500C6DEB500D6E7CE00CEE7CE00A5D694006BBD5A0073B5
      6B00BDC6B50000000000000000000000000000000000EFE7DE007B737B003939
      420039424A005A6B7B007B94AD006B84A500525A630039393900212121003131
      2900D6C6BD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B848400000000006B737300393939003939
      3900393939003939390039393900393939003939390039393900393939006B73
      7300000000000000000000000000000000000000000000000000000000000000
      0000C6C6C600C6CEBD00B5CEAD00ADCE9C009CC68C0094BD84009CBD9400C6C6
      BD000000000000000000000000000000000000000000EFDED600A59C9C007373
      73005A5A5A004242420031313100101010002121210052525200424242007B73
      7300F7E7DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A4A4A007B8484000000000000000000000000000000
      00003194CE00B5D6EF00DEEFEF00B5D6EF003194CE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6CEC600CEC6C600C6CEC600C6CEBD00C6CEC600D6CECE000000
      000000000000000000000000000000000000000000000000000000000000D6CE
      C600A59C9C00737373004A4A4A00292929002118180063635A00B5A5A500E7DE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009CA5AD007B8484000000000000000000000000000000
      00009C9CA5003939390039393900393939009C9CA50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7E7DE00DECEC600CEBDBD00CEC6BD00F7DEDE00F7EFDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF63000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF63000000000000000000000000000000000000000000000000
      0000FF630000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      00000000000000000000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF63000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      0000FF630000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      000000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      0000000000000000000084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000010101000084A52001073
      840000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      0000FF630000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      000000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      000000000000000000000084000084848400C6C6C60000000000000000000000
      000000000000000000000000000000000000000000008484840010DEEF0018C6
      DE0029B5D60052525200FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      0000FF840000FF840000FF840000FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      000000000000FF630000FF630000FF630000FF630000FF630000FF630000FF63
      0000FF630000FF630000FF630000000000000000000000000000000000000000
      00000000000000840000008400000084000084848400C6C6C600000000000000
      00000000000000000000000000000000000000000000B5B5B50008EFF70010D6
      EF0021BDDE0000000000FF840000FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      0000FF840000FF840000FF840000FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      000000000000FF840000FF840000FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      0000008400000084000000000000008400000084000084848400000000000000
      00000000000000000000000000000000000000000000000000008484840008E7
      F70018CEE70021B5D60052525200FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      0000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300000000000000000000000000000000000000
      000000000000FF840000FF840000FF840000FF840000FF840000FF840000FF84
      0000FF840000FF840000FF840000000000000000000000000000000000000000
      0000000000000000000000000000000000000084000084848400848484000000
      0000000000000000000000000000000000000000000000000000B5B5B50000F7
      FF0010DEEF0018C6DE0000000000FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C630000000000000000000000000000DE00000073
      00000039000000DE0000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300000000000000000000000000000000000000
      000000000000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084000084848400C6C6
      C600000000000000000000000000000000000000000000000000000000008484
      840008EFF70010D6EF0021BDDE0052525200FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C630000000000000000000000000000390000BDFF
      BD00BDFFBD0000390000CECE9C00CECE9C00CECE9C00CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C6300000000000000000000000000000000000000
      000000000000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400008484
      840000000000000000000000000000000000000000000000000000000000B5B5
      B50000FFFF0008E7F70018CEE70000000000CECE9C00CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C63000000000000DE00000073000000730000BDFF
      BD00BDFFBD00007300000039000000DE0000CECE9C00CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C63000000000000000000000000000000FF000000
      FF003131FF003131FF003131FF003131FF00CECE9C00CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C6300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840000F7FF00000000000000000000000000CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C6300000000000073000018FF2100BDFFBD00BDFF
      BD00BDFFBD00BDFFBD0000DE000000390000FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C63000000000000000000000000003131FF007373FF007373
      FF00ADADFF00ADADFF00ADADFF00ADADFF000000FF00CECE9C00CECE9C00CECE
      9C00CECE9C00CECE9C00FF9C6300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500EFEFEF00B5B5B5008484840000000000FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300000000000000000000730000FFFFCE00FFFFCE0018FF
      2100BDFFBD0018FF210018FF2100007300000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF007373FF007373
      FF00ADADFF00ADADFF00ADADFF00ADADFF003131FF00FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400CECECE00949494008484840052525200000000000000
      00000000000000000000000000000000000018FF210000DE000000DE00006BFF
      730018FF2100007300000073000018FF21000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003131FF003131
      FF003131FF003131FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B500EFEFEF00B5B5B5008484840000000000000000000000
      000000000000000000000000000000000000000000000000000000DE00006BFF
      73006BFF73000073000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840000000000000000000000
      00000000000000000000000000000000000000000000000000006BFF730018FF
      210018FF21006BFF730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A000000
      000000000000000000004A4A4A004A4A4A004A4A4A004A4A4A00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C6000000000000000000AD3900000000
      00000000000000000000AD390000AD390000AD390000AD390000FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300FF9C63000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AD000000840000008400000084000000AD000000AD00
      00008400000084000000000000000000000000000000000000004A4A4A000000
      000000000000000000004A4A4A0000000000000000004A4A4A00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C6000000000000000000AD3900000000
      00000000000000000000AD3900000000000000000000AD390000FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300FF9C63000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD000000FF5A5A00FF000000FF292900FF5A5A00FF292900FF5A
      5A00FF000000FF5A5A00840000000000000000000000000000004A4A4A000000
      000000000000000000004A4A4A0000000000000000004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A000000000000000000AD3900000000
      00000000000000000000AD3900000000000000000000AD390000AD390000AD39
      0000AD390000AD390000AD390000AD3900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084000000FF5A5A00FF000000FF292900FF000000FF000000FF42
      4200FF292900FF000000AD0000000000000000000000000000004A4A4A000000
      000000000000000000004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AD3900000000
      00000000000000000000AD390000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF00C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084000000FF000000FF000000FF000000FF292900FF5A5A00FF29
      2900FF5A5A00FF292900AD0000000000000000000000000000004A4A4A000000
      0000000000004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A00000000000000000000000000AD3900000000
      000000000000AD390000AD390000AD390000AD390000AD390000AD390000AD39
      0000AD390000AD390000AD390000000000000000000000000000000000000000
      0000000000000000FF0084848400000000000000000000000000000000008484
      8400C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000084000000FF5A5A00FF292900FF5A5A00FF000000FF000000FF00
      0000FF292900FF292900840000000000000000000000000000004A4A4A000000
      0000000000004A4A4A00C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C6004A4A4A00000000000000000000000000AD3900000000
      000000000000AD390000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300AD390000000000000000000000000000000000000000
      000000000000000000000000FF0084848400C6C6C60000000000848484000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      000000000000AD000000FF000000FF000000FF000000FF292900FF292900FF00
      0000FF5A5A00FF5A5A00AD0000000000000000000000000000004A4A4A004A4A
      4A004A4A4A004A4A4A00C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C6004A4A4A00000000000000000000000000AD390000AD39
      0000AD390000AD390000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300AD390000000000000000000000000000000000000000
      00000000000000000000000000000000FF0084848400C6C6C6000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5A5A00FF000000FF000000FF000000FF292900FF000000FF00
      0000FF000000FF292900840000000000000000000000000000004A4A4A000000
      0000000000004A4A4A00C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C6004A4A4A00000000000000000000000000AD3900000000
      000000000000AD390000FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C
      6300FF9C6300FF9C6300AD390000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6000000FF000000FF000000FF000000
      000000000000000000000000000000000000000000000000000000DE00000073
      00000039000000DE0000FF5A5A00FF292900FF000000FF000000FF424200FF29
      2900FF292900FF5A5A00840000000000000000000000000000004A4A4A000000
      0000000000004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A00000000000000000000000000AD3900000000
      000000000000AD390000AD390000AD390000AD390000AD390000AD390000AD39
      0000AD390000AD390000AD390000000000000000000000000000000000000000
      00000000000000000000848484000000FF000000FF000000FF00848484000000
      000000000000000000000000000000000000000000000000000000390000BDFF
      BD00BDFFBD0000390000FF000000FF000000FF292900FF000000FF292900FF29
      2900FF292900FF292900AD0000000000000000000000000000004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848484000000FF000000FF00000000000000FF00848484008484
      84000000000000000000000000000000000000DE00000073000000730000BDFF
      BD00BDFFBD00007300000039000000DE0000FF5A5A00FF292900FF5A5A00FF29
      2900FF5A5A00FF5A5A008400000000000000000000004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A000000
      00000000000000000000000000000000000000000000AD390000AD390000AD39
      0000AD390000AD390000AD390000AD390000AD390000AD390000AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000FF00000000000000000000000000000000000000FF008484
      8400C6C6C6000000000000000000000000000073000018FF2100BDFFBD00BDFF
      BD00BDFFBD00BDFFBD0000DE000000390000FF5A5A00AD000000840000008400
      000084000000AD0000000000000000000000000000004A4A4A00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6004A4A4A000000
      00000000000000000000000000000000000000000000AD390000FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      FF008484840000000000000000000000000000730000FFFFCE00FFFFCE0018FF
      2100BDFFBD0018FF210018FF2100007300000000000000000000000000000000
      000000000000000000000000000000000000000000004A4A4A00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6004A4A4A000000
      00000000000000000000000000000000000000000000AD390000FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF0000000000000000000000000018FF210000DE000000DE00006BFF
      730018FF2100007300000073000018FF21000000000000000000000000000000
      000000000000000000000000000000000000000000004A4A4A00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6004A4A4A000000
      00000000000000000000000000000000000000000000AD390000FF9C6300FF9C
      6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300FF9C6300AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000DE00006BFF
      73006BFF73000073000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A000000
      00000000000000000000000000000000000000000000AD390000AD390000AD39
      0000AD390000AD390000AD390000AD390000AD390000AD390000AD3900000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006BFF730018FF
      210018FF21006BFF730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084C6000084C6000084C6000084C6000084C6000084
      C6000084C6000084C60000000000000000000000000000000000000000000000
      000000000000000000000084C6000084C6000084C6000084C6000084C6000084
      C6000084C6000084C60000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000AD000000AD00
      0000840000008400000000000000000000000000000000000000000000000000
      00000000000000000000AD000000840000008400000084000000AD000000AD00
      0000840000008400000000000000000000000000000000000000000000000000
      00000000000018BDD60052CEF70052EFF70052EFF70052EFF70073CEDE0073CE
      DE0084DEDE0052EFF700007BBD00000000000000000000000000000000000000
      00000000000018BDD60052CEF70052EFF70052EFF70052EFF70073CEDE0073CE
      DE0084DEDE0052EFF700007BBD00000000000000000000000000000000000000
      000000000000AD000000FF5A5A00FF000000FF5A5A00FF5A5A00FF292900FF5A
      5A00FF000000FF5A5A0084000000000000000000000000000000000000000000
      000000000000AD000000FF5A5A00FF000000FF292900FF5A5A00FF292900FF5A
      5A00FF000000FF5A5A0084000000000000000000000000000000000000000000
      0000000000000084C60042DEF70052EFF70084DEDE0052EFF70084DEDE0052EF
      F70084DEDE0052EFF700007BBD00000000000000000000000000000000000000
      0000000000000084C60042DEF70052EFF70084DEDE0052EFF70084DEDE0052EF
      F70084DEDE0052EFF700007BBD00000000000000000010101000084A52001073
      840000000000AD000000FF000000FF000000FF424200FF292900FF000000FF42
      4200FF292900FF000000AD000000000000000000000000000000000000000000
      00000000000084000000FF5A5A00FF000000FF292900FF000000FF000000FF42
      4200FF292900FF000000AD000000000000000000000000000000000000000000
      0000000000000084C6004AE7F70052F7F70052EFF70052EFF70073CEDE0084DE
      DE0052EFF700089CCE00007BBD00000000000000000000000000000000000000
      0000000000000084C6004AE7F70052F7F70052EFF70052EFF70073CEDE0084DE
      DE0052EFF700089CCE00007BBD0000000000000000008484840010DEEF0018C6
      DE0029B5D60052525200FF000000FF5A5A00FF000000FF292900FF5A5A00FF29
      2900FF5A5A00FF292900AD000000000000000000000000000000000000000000
      00000000000084000000FF000000FF000000FF000000FF292900FF5A5A00FF29
      2900FF5A5A00FF292900AD000000000000000000000000000000000000000000
      0000000000000084C6005AE7F70073F7F70052EFF70052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD00000000000000000000000000000000000000
      0000000000000084C6005AE7F70073F7F70052EFF70052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD000000000000000000B5B5B50008EFF70010D6
      EF0021BDDE0000000000FF000000FF000000FF5A5A00FF000000FF000000FF00
      0000FF292900FF29290084000000000000000000000000000000000000000000
      00000000000084000000FF5A5A00FF292900FF5A5A00FF000000FF000000FF00
      0000FF292900FF29290084000000000000000000000000000000000000000000
      0000000000000084C60084E7F700A5F7F700A5EFEF0052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD00000000000000000000000000000000000000
      0000000000000084C60084E7F700A5F7F700A5EFEF0052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD000000000000000000000000008484840008E7
      F70018CEE70021B5D60052525200FF000000FF5A5A00FF292900FF292900FF00
      0000FF5A5A00FF5A5A00AD000000000000000000000000000000000000000000
      00000000000084000000FF000000FF000000FF000000FF292900FF292900FF00
      0000FF5A5A00FF5A5A00AD00000000000000000000000000000000DE00000073
      00000039000000DE0000ADE7EF00FFFFFF00CEEFEF007BBDCE0084DEDE0052EF
      F70052EFF7004AC6DE00007BBD00000000000000000000000000000000000000
      0000000000000084C600ADE7EF00D6EFEF00CEEFEF007BBDCE0084DEDE0052EF
      F70052EFF7004AC6DE00007BBD00000000000000000000000000B5B5B50000F7
      FF0010DEEF0018C6DE0000000000FF000000FF000000FF292900FF000000FF00
      0000FF000000FF29290084000000000000000000000000000000000000000000
      00000000000084000000FF5A5A00FF000000FF000000FF292900FF000000FF00
      0000FF000000FF2929008400000000000000000000000000000000390000BDFF
      BD00BDFFBD000039000052EFF70052EFF70052EFF70052EFF70052EFF7006BD6
      D60052EFF70052EFF700007BBD00000000000000000000000000000000000000
      000000000000089CCE0052EFF70052EFF70052EFF70052EFF70052EFF7006BD6
      D60052EFF70052EFF700007BBD00000000000000000000000000000000008484
      840008EFF70010D6EF0021BDDE0052525200FF5A5A00FF000000FF424200FF29
      2900FF292900FF5A5A0084000000000000000000000000000000000000000000
      000000000000AD000000FF000000FF292900FF000000FF000000FF424200FF29
      2900FF292900FF5A5A00840000000000000000DE00000073000000730000BDFF
      BD00BDFFBD00007300000039000000DE000063F7F70063F7F7005AEFEF0052F7
      F70052F7F7005AF7F700007BBD000000000000000000000000000000FF000000
      FF003131FF003131FF003131FF007373FF0063F7F70063F7F7005AEFEF0052F7
      F70052F7F7005AF7F700007BBD0000000000000000000000000000000000B5B5
      B50000FFFF0008E7F70018CEE70000000000FF000000FF000000FF292900FF29
      2900FF292900FF292900AD0000000000000000000000000000000000FF000000
      FF003131FF003131FF003131FF007373FF00FF292900FF000000FF292900FF29
      2900FF292900FF292900AD000000000000000073000018FF2100BDFFBD00BDFF
      BD00BDFFBD00BDFFBD0000DE00000039000052EFF70052EFF70052EFF70073F7
      F70084F7F7009CF7F700007BBD000000000000000000000094003131FF007373
      FF00ADADFF00ADADFF00ADADFF007373FF000000FF0052EFF70052EFF70073F7
      F70084F7F7009CF7F700007BBD00000000000000000000000000000000000000
      00008484840000F7FF00000000000000000000000000FF292900FF292900FF29
      2900FF000000FF000000840000000000000000000000000094003131FF007373
      FF00ADADFF00ADADFF00ADADFF007373FF000000FF00FF292900FF5A5A00FF29
      2900FF5A5A00FF5A5A00840000000000000000730000FFFFCE00FFFFCE0018FF
      2100BDFFBD0018FF210018FF2100007300000084C6000084C6000084C6000084
      C6000084C6007BC6DE000000000000000000000000000000FF007373FF00ADAD
      FF00ADADFF00ADADFF007373FF003131FF00000094000084C6000084C6000084
      C6000084C6007BC6DE0000000000000000000000000000000000000000000000
      0000B5B5B500EFEFEF00B5B5B500848484000000000084000000840000008400
      000084000000AD0000000000000000000000000000000000FF007373FF00ADAD
      FF00ADADFF00ADADFF007373FF003131FF000000940084000000840000008400
      000084000000AD000000000000000000000018FF210000DE000000DE00006BFF
      730018FF2100007300000073000018FF21000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007373FF003131
      FF003131FF003131FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400CECECE00949494008484840052525200000000000000
      00000000000000000000000000000000000000000000000000007373FF003131
      FF003131FF003131FF000000FF000000FF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000DE00006BFF
      73006BFF73000073000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B500EFEFEF00B5B5B5008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006BFF730018FF
      210018FF21006BFF730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B0073424200734242007342420073424200734242007342
      4200734242007342420073424200000000000000000000000000000000000000
      0000FFD6DE00CE2129009C52520094B5A500F7F7F70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00FFFFE700F7EFDE00F7EFD600F7EFD600F7EFD600F7E7
      CE00F7EFD600EFDEC60073424200000000000000000000000000000000000000
      0000F7E7E700FF6B7300CE212900847B7B00D6D6D60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5736B00F7EFDE00F7DEBD00F7D6BD00F7D6BD00EFD6B500EFD6
      B500EFDEBD00E7D6BD0073424200000000000000000000000000000000000000
      000000000000F7CEC600CE2129009C52520094B5A500EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084C6000084C6000084C6000084C6000084C6000084
      C6000084C6000084C60000000000000000000000000000000000000000000000
      000000000000A5736B00FFEFDE00FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400E7D6BD0073424200000000000000000000000000000000000000
      000000000000FFF7F700FF6B7300CE212900847B7B00E7DEDE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018BDD60052CEF70052EFF70052EFF70052EFF70073CEDE0073CE
      DE0084DEDE0052EFF700007BBD00000000000000000000000000000000000000
      000000000000A5736B00FFF7E700F7DEBD00F7D6B500F7D6B500F7D6B500F7D6
      AD00F7DEC600E7D6C60084524A00000000000000000000000000000000000000
      00000000000000000000FFD6DE00BD949400847B7B00C6BDC600EFEFEF00FFF7
      F700F7F7F70000000000000000000000000000000000000000000084C6000084
      C6000084C6000084C6000084C6000084C6000084C6000084C600000000000000
      0000000000000000000000000000000000000000000010101000084A52001073
      8400000000000084C60042DEF70052EFF70084DEDE0052EFF70084DEDE0052EF
      F70084DEDE0052EFF700007BBD00000000000000000000000000000000000000
      000000000000A5736B00FFF7EF00FFDEBD00FFDEBD00FFDEB500F7D6B500F7D6
      B500F7DEC600E7D6C60084524A0000000000000000000084C6000084C6000084
      C6000084C6000084C6000084C600EFEFEF00D6B5A500D6B5A500D6B5A500E7C6
      BD00C6B5B500C6B5B500EFEFF700000000000000000018BDD60052CEF70052EF
      F70052EFF70052EFF70073CEDE0073CEDE0084DEDE0052EFF700007BBD000000
      000000000000000000000000000000000000000000008484840010DEEF0018C6
      DE0029B5D600525252004AE7F70052F7F70052EFF70052EFF70073CEDE0084DE
      DE0052EFF700089CCE00007BBD00000000000000000000000000000000000000
      000000000000A5736B00FFFFF700FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400EFDECE008C5A5A000000000018BDD60052CEF70052EFF70052EF
      F70052EFF70073CEDE0073CEDE00F7DED600FFCEB500FFDEBD00FFEFCE00FFF7
      DE00FFEFCE00E7C6BD00C6B5B500EFEFF700000000000084C60042DEF70052EF
      F70084DEDE0052EFF70084DEDE0052EFF70084DEDE0052EFF700007BBD000000
      00000000000000000000000000000000000000000000B5B5B50008EFF70010D6
      EF0021BDDE00000000005AE7F70073F7F70052EFF70052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD00000000000000000000000000000000000000
      000000000000A5736B00FFFFFF00FFE7CE00FFE7C600FFDEC600FFDEC600FFE7
      C600FFF7DE00E7D6CE008C5A5A00000000000084C60042DEF70052EFF70084DE
      DE0052EFF70084DEDE00F7DED600FFCEB500FFCEB500FFE7CE00FFF7DE00FFF7
      E700FFFFF70000000000E7C6BD00C6BDC600000000000084C6004AE7F70052F7
      F70052EFF70052EFF70073CEDE0084DEDE0052EFF700089CCE00007BBD000000
      00000000000000000000000000000000000000000000000000008484840008E7
      F70018CEE70021B5D60052525200A5F7F700A5EFEF0052EFF70084DEDE0052EF
      F70052EFF70052EFF700007BBD000000000000000000000000000000000000DE
      0000007300000039000000DE0000FFFFFF00FFFFFF00FFFFF700FFFFF700E7D6
      D600C6B5AD00A59494009C635A00000000000084C6004AE7F70052F7F70052EF
      F70052EFF7009CF7F700FFDEBD00FFCEB500FFC6B500FFE7CE00FFF7DE00FFFF
      F7000000000000000000F7EFEF00BD949400000000000084C6005AE7F70073F7
      F70052EFF70052EFF70084DEDE0052EFF70052EFF70052EFF700007BBD000000
      0000000000000000000000000000000000000000000000000000B5B5B50000F7
      FF0010DEEF0018C6DE0000000000FFFFFF00CEEFEF007BBDCE0084DEDE0052EF
      F70052EFF7004AC6DE00007BBD00000000000000000000000000000000000039
      0000BDFFBD00BDFFBD000039000000000000FFFFFF00FFFFFF00FFFFFF00A573
      6B00A5736B00A5736B00A5736B00000000000084C6005AE7F70073F7F70052EF
      F70052EFF7009CF7F700FFE7CE00FFCEB500FFC6B500FFEFCE00FFF7DE00FFF7
      E700FFFFF700FFFFF700FFF7E700BD949400000000000084C60084E7F700A5F7
      F700A5EFEF0052EFF70084DEDE0052EFF70052EFF70052EFF700007BBD000000
      0000000000000000000000000000000000000000000000000000000000008484
      840008EFF70010D6EF0021BDDE005252520052EFF70052EFF70052EFF7006BD6
      D60052EFF70052EFF700007BBD00000000000000000000DE0000007300000073
      0000BDFFBD00BDFFBD00007300000039000000DE00000000000000000000A573
      6B00E7A55200B5735A0000000000000000000084C60084E7F700A5F7F700A5EF
      EF0052EFF70084DEDE00FFF7E700FFFFF700FFCEB500FFDEBD00FFEFCE00FFF7
      DE00FFF7DE00FFF7E700FFE7CE00BD949400000000000084C600ADE7EF00D6EF
      EF00CEEFEF007BBDCE0084DEDE0052EFF70052EFF7004AC6DE00007BBD000000
      000000000000000000000000000000000000000000000000000000000000B5B5
      B50000FFFF0008E7F70018CEE7000000000063F7F70063F7F7005AEFEF0052F7
      F70052F7F7005AF7F700007BBD0000000000000000000073000018FF2100BDFF
      BD00BDFFBD00BDFFBD00BDFFBD0000DE000000390000A5736B00A5736B00A573
      6B00AD6B6B000000000000000000000000000084C600ADE7EF00D6EFEF00CEEF
      EF007BBDCE0084DEDE00F7E7E700FFF7E700FFE7CE00FFC6B500FFCEB500FFDE
      BD00FFEFCE00FFEFCE00D6B5A500D6D6D60000000000089CCE0052EFF70052EF
      F70052EFF70052EFF70052EFF7006BD6D60052EFF70052EFF700007BBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840000F7FF0000000000000000000000000052EFF70052EFF70073F7
      F70084F7F7009CF7F700007BBD00000000000000000000730000FFFFCE00FFFF
      CE0018FF2100BDFFBD0018FF210018FF21000073000000000000000000000000
      000000000000000000000000000000000000089CCE0052EFF70052EFF70052EF
      F70052EFF70052EFF7009CF7F700F7DED600FFE7CE00FFDEBD00FFCEB500FFDE
      BD00FFDEBD00F7CEC600E7DEDE0000000000000000000084C60042DEE70063F7
      F70063F7F70063F7F7005AEFEF0052F7F70052F7F7005AF7F700007BBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500EFEFEF00B5B5B50084848400000000000084C6000084C6000084
      C6000084C6007BC6DE0000000000000000000000000018FF210000DE000000DE
      00006BFF730018FF2100007300000073000018FF210000000000000000000000
      0000000000000000000000000000000000000084C60042DEE70063F7F70063F7
      F70063F7F7005AEFEF0052F7F7009CF7F700F7DED600F7DED600F7CEC600F7CE
      C600FFF7E700000000000000000000000000000000000084C60052EFF70052EF
      F70052EFF70052EFF70052EFF70073F7F70084F7F7009CF7F700007BBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400CECECE00949494008484840052525200000000000000
      00000000000000000000000000000000000000000000000000000000000000DE
      00006BFF73006BFF730000730000000000000000000000000000000000000000
      0000000000000000000000000000000000000084C60052EFF70052EFF70052EF
      F70052EFF70052EFF70073F7F70084F7F7009CF7F700007BBD00FFF7F7000000
      00000000000000000000000000000000000000000000000000000084C6000084
      C6000084C6000084C6000084C6000084C6000084C6007BC6DE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B500EFEFEF00B5B5B5008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006BFF
      730018FF210018FF21006BFF7300000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084C6000084C6000084
      C6000084C6000084C6000084C6000084C6007BC6DE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD007373
      730052392900523929008C6B1800523929005239290052392900523929005239
      290052392900A5A5A50000000000000000000000000000000000000000000000
      0000ADADAD007373730052392900523929005239290052392900523929005239
      2900523929005239290052392900A5A5A5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094949400ADFF
      FF00ADFFFF0084630000FF9C29008C6B18009CFFFF00ADFFFF009CFFFF00ADFF
      FF009CFFFF005239290000000000000000000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF009CFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF00523929000000000000000000000000000000
      0000A5736B007342420073424200734242007342420073424200734242007342
      420073424200734242007342420000000000000000000000FF00000084000000
      84008484840000000000000000000000000000000000000000000000FF008484
      840000000000000000000000000000000000000000000000000094949400ADFF
      FF0084630000FF9C2900DEA57B00FF9C29008C6B18009CFFFF00ADFFFF009CFF
      FF00ADFFFF005239290000000000000000000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF009CFF
      FF00ADFFFF009CFFFF00ADFFFF00523929000000000000000000000000000000
      0000A5736B00FFFFE700F7EFDE00F7EFD600F7EFD600F7EFD600F7EFD600F7E7
      CE00F7EFD600EFDEC6007342420000000000000000000000FF00000084000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400848484000000000000000000000000000000000000000000949494008463
      0000DEA57B00DEA57B00DEA57B00DEA57B00FF9C29008C6B18009CFFFF00ADFF
      FF009CFFFF005239290000000000000000000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF00523929000000000000000000000000000000
      0000A5736B00F7EFDE00F7DEBD00F7D6BD00F7D6BD00F7D6BD00EFD6B500EFD6
      B500EFDEBD00E7D6BD007342420000000000000000000000FF00000084000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      840000008400848484000000000000000000000000000000000084630000DEA5
      7B00FFFFC600FFFFC600DEA57B00DEA57B00DEA57B00FF9C29008C6B18009CFF
      FF00ADFFFF007373730000000000000000000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF00ADFFFF009CFFFF00ADFFFF00737373000000000010101000084A52001073
      840000000000FFF7E700F7DEBD00F7D6B500F7D6B500F7D6B500F7D6B500F7D6
      AD00F7DEC600E7D6C60084524A000000000000000000000000000000FF000000
      8400000084000000840000008400848484000000840000008400000084000000
      84000000840084848400000000000000000000000000E7BD7B008C6B18008C6B
      18008C6B1800FFFFC600FFFFC600DEA57B008C6B18008C6B18008C6B1800E7BD
      7B009CFFFF008484840000000000000000000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF0084848400000000008484840010DEEF0018C6
      DE0029B5D60052525200FFDEBD00FFDEBD00FFDEBD00FFDEB500F7D6B500F7D6
      B500F7DEC600E7D6C60084524A00000000000000000000000000000000000000
      FF00000084000000840000008400000084000000840000008400000084000000
      8400848484000000000000000000000000000000000000000000949494009CF7
      FF009C7B3100FFFFC600FFFFC600FFF7AD009C7B31009CFFFF00ADFFFF009CFF
      FF00ADFFFF008C8C8C0000000000000000000000000000000000000000000000
      0000949494009CF7FF00ADFFFF009CFFFF00ADFFFF009CFFFF00ADFFFF009CFF
      FF00ADFFFF009CFFFF00ADFFFF008C8C8C0000000000B5B5B50008EFF70010D6
      EF0021BDDE0000000000FFC69400FFC69400FFC69400FFC69400FFC69400FFC6
      9400FFC69400EFDECE008C5A5A00000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000C6C6C6009CF7
      FF00AD8C4200FFFFC600FFFFC600DEA57B00FFD69400ADFFFF009CFFFF00ADFF
      FF009CFFFF009C9C9C0000000000000000000000000000000000000000000000
      0000C6C6C600C6C6C6009CF7FF00ADFFFF009CFFFF00ADFFFF009CFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF009C9C9C0000000000000000008484840008E7
      F70018CEE70021B5D60052525200FFE7C600FFE7C600FFDEC600FFDEC600FFE7
      C600FFF7DE00E7D6CE008C5A5A00000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000C6C6C600BD94
      4A00DEA57B00FFF7AD00FFFFC600AD8C42009CF7FF009CF7FF009CF7FF009CF7
      FF009CF7FF00A5A5A5000000000000000000000000000000000000DE00000073
      00000039000000DE0000C6C6C6009CF7FF009CF7FF009CF7FF009CF7FF009CF7
      FF009CF7FF009CF7FF009CF7FF00A5A5A5000000000000000000B5B5B50000F7
      FF0010DEEF0018C6DE0000000000FFFFFF00FFFFFF00FFFFF700FFFFF700E7D6
      D600C6B5AD00A59494009C635A00000000000000000000000000000000000000
      0000000000000000FF0000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000C6C6C600BD94
      4A00FFE79C00FFF7AD00E7BD7300FFD694009CF7FF009CF7FF0052DEFF0052DE
      FF0052BDD600737373000000000000000000000000000000000000390000BDFF
      BD00BDFFBD0000390000C6C6C600C6C6C6009CF7FF009CF7FF009CF7FF009CF7
      FF0052DEFF0052DEFF0052BDD600737373000000000000000000000000008484
      840008EFF70010D6EF0021BDDE0052525200525252000000000000000000A573
      6B00A5736B00A5736B00A5736B00000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000AD8C3900DEA5
      7B00FFD69400E7BD7B009CF7FF009CF7FF009CF7FF00B5B5B500A5A5A5007373
      730073737300B5B5B500000000000000000000DE00000073000000730000BDFF
      BD00BDFFBD00007300000039000000DE0000C6C6C6009CF7FF009CF7FF00B5B5
      B500A5A5A5007373730073737300B5B5B500000000000000000000000000B5B5
      B50000FFFF0008E7F70018CEE70000000000000000000000000000000000A573
      6B00E7A55200B5735A0000000000000000000000000000000000000000000000
      FF00000084000000840000008400848484000000840000008400000084008484
      84000000000000000000000000000000000000000000D6B56B00D6B56B00D6B5
      6B00D6B56B00EFCE84009CF7FF009CF7FF009CF7FF00A5A5A500E7E7E700DEDE
      DE00DEDEDE00B5B5B50000000000000000000073000018FF2100BDFFBD00BDFF
      BD00BDFFBD00BDFFBD0000DE000000390000C6C6C6009CF7FF009CF7FF00A5A5
      A500E7E7E700DEDEDE00DEDEDE00B5B5B5000000000000000000000000000000
      00008484840000F7FF0000000000000000000000000000000000A5736B00A573
      6B00AD6B6B0000000000000000000000000000000000000000000000FF000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400848484000000000000000000000000000000000000000000523929009CF7
      FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF00A5A5A500ADFFFF00E7E7
      E700B5B5B50000000000000000000000000000730000FFFFCE00FFFFCE0018FF
      2100BDFFBD0018FF210018FF210000730000C6C6C6009CF7FF009CF7FF00A5A5
      A500ADFFFF00E7E7E700B5B5B500000000000000000000000000000000000000
      00000000000084848400CECECE00949494009494940084848400525252000000
      00000000000000000000000000000000000000000000000000000000FF000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400000084008484840000000000000000000000000000000000523929009CF7
      FF00CED6D6009CF7FF00CED6D600ADFFFF00E7E7E7008C8C8C00CED6D600B5B5
      B5000000000000000000000000000000000018FF210000DE000000DE00006BFF
      730018FF2100007300000073000018FF210000000000ADFFFF00E7E7E7008C8C
      8C0000000000B5B5B50000000000000000000000000000000000000000000000
      000000000000B5B5B500EFEFEF00B5B5B500B5B5B50084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000840000000000000000000000000000000000000000000000FF000000
      8400000084000000840000000000000000000000000000000000523929005239
      2900523929005239290073737300737373008C8C8C008C8C8C00C6C6C6000000
      000000000000000000000000000000000000000000000000000000DE00006BFF
      73006BFF730000730000B5B5B500CECECE00C6C6C600B5B5B5008C8C8C008C8C
      8C00C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006BFF730018FF
      210018FF21006BFF730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFEFEF003131310031313100313131003131
      3100313131003131310063636300EFEFEF00000000006B6B6B006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006B6B6B006B6B6B006B6B6B006B6B6B000000000000000000000000000000
      0000ADADAD007373730052392900523929005239290052392900523929005239
      2900523929005239290052392900A5A5A5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A4A4A00636363009C9C9C00D6D6D6009C9C9C00D6D6
      D6009C9C9C00D6D6D6009C9C9C0063636300000000004A4A4A004A4A4A006B6B
      6B00000000000000000000000000000000004A4A4A004A4A4A006B6B6B007B73
      6B006B6B6B004A4A4A00E7E7E7006B6B6B000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF009CFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF00523929000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000004A4A4A00636363009C9C9C00EFEFEF009C9C9C00EFEFEF00D6D6
      D600EFEFEF009C9C9C009C9C9C003131310000000000000000004A4A4A004A4A
      4A004A4A4A006B6B6B006B6B6B004A4A4A00947B7300BD947B00BDAD8C00C6B5
      9400AD8C730094847300B5B5B5006B6B6B000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF009CFF
      FF00ADFFFF009CFFFF00ADFFFF00523929000000000000000000FFFFFF000000
      FF000000FF000000FF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF00FFFFFF0000000000000000000000000000000000000000000000
      00004A4A4A009C9C9C00D6D6D600D6D6D600D6D6D600EFEFEF00EFEFEF00EFEF
      EF00D6D6D600EFEFEF00D6D6D600313131000000000000000000000000009C9C
      9C00000000004A4A4A006B84A500AD948C00E7C69400FFFFC600FFFFE700FFFF
      F70000000000C6A58C00846B5A006B6B6B000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF00523929000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000004A4A
      4A00EFEFEF00D6D6D600D6D6D600D6D6D600EFEFEF00D6D6D600EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00D6D6D6003131310000000000000000009C9C9C009C9C
      9C004A4A4A00B5B5B50084B5BD00CE947300FFFFB500FFFFC600FFFFDE000000
      0000FFFFF700FFFFDE00A5846300526B6B000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF00ADFFFF009CFFFF00ADFFFF00737373000000000000000000FFFFFF000000
      FF000000FF000000FF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF00FFFFFF00000000000000000000000000000000004A4A4A006363
      6300D6D6D600D6D6D600EFEFEF00D6D6D600EFEFEF00D6D6D600EFEFEF00D6D6
      D600D6D6D600EFEFEF00D6D6D6003131310000000000000000004A4A4A009C9C
      9C00D6D6D600D6D6D6009CC6C600EFCE9C00FFEFA500FFF7B500FFFFD600FFFF
      E700FFFFDE00FFFFD600BD9C73006B6B63000000000000000000000000000000
      000094949400ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF00848484000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000004A4A4A00EFEFEF006363
      6300D6D6D600D6D6D600D6D6D600D6D6D600EFEFEF00EFEFEF00D6D6D600D6D6
      D600EFEFEF00D6D6D6004A4A4A0031313100000000009C9C9C009C9C9C009C9C
      9C00D6D6D600D6D6D60084C6BD00DED6AD00FFE79C00FFE79C00FFF7B500FFFF
      CE00FFFFC600FFFFBD00CEA57300736B63000000000000000000000000000000
      0000949494009CF7FF00ADFFFF009CFFFF00ADFFFF009CFFFF00ADFFFF009CFF
      FF00ADFFFF009CFFFF00ADFFFF008C8C8C000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000004A4A4A0063636300EFEFEF009C9C
      9C00D6D6D600EFEFEF00D6D6D600D6D6D600EFEFEF00D6D6D600EFEFEF00D6D6
      D600636363004A4A4A00D6D6D60031313100000000004A4A4A00E7E7E7009C9C
      9C00D6D6D600E7E7E700B5B5B500C6BD9C00FFF7B500FFE7AD00FFE7A500FFE7
      A500FFDE9400FFF7AD00AD8463004A6B73000000000000000000000000000000
      0000C6C6C6009CF7FF009CF7FF00ADFFFF009CFFFF00ADFFFF009CFFFF00ADFF
      FF009CFFFF00ADFFFF009CFFFF009C9C9C000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000004A4A4A00EFEFEF00EFEFEF009C9C
      9C00EFEFEF00EFEFEF00EFEFEF00D6D6D600EFEFEF00D6D6D600D6D6D600EFEF
      EF004A4A4A00D6D6D600D6D6D6003131310000000000B5B5B500E7E7E7009C9C
      9C00D6D6D600E7E7E700D6D6D600C6B5AD00EFD6B50000000000FFEFD600FFF7
      B500FFFFB500DEB58400847B7B006B6B6B000000000000000000000000000000
      0000C6C6C6009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7
      FF009CF7FF009CF7FF009CF7FF00A5A5A5000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000004A4A4A00EFEFEF00EFEFEF009C9C
      9C00EFEFEF00D6EFEF00EFEFEF009C9C9C00EFEFEF009C9C9C00636363004A4A
      4A00D6D6D600D6D6D600636363003131310000000000D6D6D600D6D6D6009C9C
      9C00B5B5B500D6D6D600D6D6D6004A4A4A0094ADB500D6BDBD00DED6C600DECE
      A500BD9C7B009C8C84009C9C9C006B6B6B000000000000000000000000000000
      0000C6C6C600C6C6C6009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7
      FF0052DEFF0052DEFF0052BDD600737373000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000636363004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A0063636300D6D6
      D600EFEFEF00D6D6D600D6D6D60031313100000000006B6B6B006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B004A4A4A00D6D6D6009C9C9C007BB5AD007BAD
      AD009C9C9C00D6D6D600D6D6D6006B6B6B003139FF000000EF000000AD000000
      AD000000AD000000AD000000AD003139FF009CF7FF009CF7FF009CF7FF00B5B5
      B500A5A5A5007373730073737300B5B5B5000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000004A4A
      4A00EFEFEF00D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600313131000000000000000000000000006B6B
      6B00C6C6C600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D6009C9C9C00D6D6D600D6D6D6006B6B6B000000EF008484FF003139FF003139
      FF003139FF003139FF003139FF000000AD00C6C6C6009CF7FF009CF7FF00A5A5
      A500E7E7E700DEDEDE00DEDEDE00B5B5B5000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000004A4A
      4A00EFEFEF009C9C9C00EFEFEF009C9C9C004A4A4A004A4A4A0063636300EFEF
      EF00D6D6D60000000000000000004A4A4A000000000000000000000000006B6B
      6B00C6C6C600C6C6C600C6C6C600C6C6C6006B6B6B006B6B6B00C6C6C600D6D6
      D600D6D6D600D6D6D600D6D6D6006B6B6B000000EF00FFFFCE00FFFFCE008484
      FF008484FF008484FF008484FF000000EF00C6C6C6009CF7FF009CF7FF00A5A5
      A500ADFFFF00E7E7E700B5B5B500000000000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00004A4A4A004A4A4A004A4A4A004A4A4A0000000000000000004A4A4A006363
      63009C9C9C009C9C9C009C9C9C004A4A4A000000000000000000000000000000
      00006B6B6B006B6B6B006B6B6B006B6B6B0000000000000000006B6B6B00C6C6
      C600C6C6C600C6C6C600C6C6C6006B6B6B008484FF003139FF003139FF00B5B5
      FF008484FF000000EF000000EF008484FF0000000000ADFFFF00E7E7E7008C8C
      8C0000000000B5B5B50000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A4A
      4A004A4A4A004A4A4A004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006B6B
      6B006B6B6B006B6B6B006B6B6B00000000000000000000000000000000000000
      00008C8C8C008C8C8C00B5B5B500CECECE00C6C6C600B5B5B5008C8C8C008C8C
      8C00C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000000000000000FF000000FF00000000000000
      00000000FF000000FF0000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B00D69C6300CE9C5A00CE945200CE944A00CE944A00CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000FF000000FF00
      0000FF000000FF0000000000000000000000FF000000FF000000000000000000
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000084FFFF0084FFFF0084FFFF0084FFFF0084FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF00000000000000FF000000FF0000000000000000000000FF000000
      00000000FF000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE94520000000000000000000000000000000000FF00
      0000FF00000000000000FF000000FF0000000000000000000000FF0000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      000084FFFF0084FFFF0084FFFF0000FF000000FF000000FF000084FFFF0084FF
      FF0084FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000BDBDBD0000000000000000000000
      0000000000000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B0000000000CE9C5A00CE945200CE944A0010080800CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      000000000000FF000000FF00000000000000BDBDBD0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000084FF
      FF0084FFFF0000FF000000FF000084FFFF0084FFFF0084FFFF0000FF000000FF
      000084FFFF0084FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE945200000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD00000000000000
      00000000000000000000000000000000000000000000000000000000000084FF
      FF0000FF000084FFFF0084FFFF0084FFFF0084FFFF0084FFFF0084FFFF0084FF
      FF0000FF000084FFFF0000000000000000000000000000000000000000000000
      00000000000000000000BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B0000000000CE9C5A00CE945200CE944A0010080800CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      00000000000000000000BDBDBD00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084FFFF0084FF
      FF0000FF000084FFFF0084FFFF0084FFFF0084FFFF0084FFFF0084FFFF0084FF
      FF0000FF000084FFFF0084FFFF00000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD00000000000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE945200000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD0000000000000000000000000000000000000000000000000084FFFF0000FF
      000084FFFF0084FFFF0084FFFF0084FFFF0084FFFF0000840000008400000084
      00000084000000FF000084FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      0000000000000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B0000000000CE9C5A00CE945200CE944A0010080800CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      000000000000000000000000000000000000000000000000000084FFFF0000FF
      000084FFFF0084FFFF0084FFFF0084FFFF000000000000000000000000000000
      000084FFFF0000FF000084FFFF00000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD00000000000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE945200000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD0000000000BDBDBD0000000000BDBD
      BD0000000000000000000000000000000000000000000000000084FFFF0000FF
      000084FFFF0084FFFF0084FFFF0084FFFF00000000000084000084FFFF0084FF
      FF0084FFFF0000FF000084FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      0000BDBDBD000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B0000000000CE9C5A00CE945200CE944A0010080800CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000BDBDBD000000
      0000BDBDBD00000000000000000000000000000000000000000084FFFF0084FF
      FF0000FF000084FFFF0084FFFF0084FFFF00000000000084000084FFFF0084FF
      FF0000FF000084FFFF0084FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD0000000000BDBD
      BD00000000000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE945200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD0000000000BDBD
      BD000000000000000000000000000000000000000000000000000000000084FF
      FF0000FF000084FFFF0084FFFF0084FFFF00000000000084000084FFFF0084FF
      FF0000FF000084FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000000000000000
      0000000000000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B0000000000CE9C5A00CE945200CE944A0010080800CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000084FF
      FF0084FFFF0000FF000000FF000084FFFF00000000000084000000FF000000FF
      000084FFFF0084FFFF0000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00BDBDBD0000000000BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000DEB584000000
      000000000000FFFFF70000000000FFF7EF00F7F7EF00F7EFE70010080800F7EF
      DE00F7E7DE00EFE7D600CE945200000000000000000000000000000000000000
      000000000000000000007B7B7B00BDBDBD0000000000BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084FFFF0084FFFF0084FFFF0000FF0000000000000084000084FFFF0084FF
      FF0084FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000DEB58400D6AD
      7B00D6AD7300D6A56B00D69C6300CE9C5A00CE945200CE944A00CE944A00CE94
      4A00CE944A00CE944A00CE945200000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084FFFF0084FFFF000000000084FFFF0084FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000018BDD6000084C6000084C6000084C6000084
      C6000084C6000084C600007BBD0018BDD6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      840084848400848484008484840084848400848484008484840000000000C6C6
      C60084848400C6C6C60000000000C6C6C60000000000AD9C7B006B4A18006B4A
      18006B4A18006B4A18006B4A18006B4A1800AD9C7B0000000000000000000000
      00000000000000000000A5735200180800000000000000000000000000000000
      00000000000000000000089CCE00089CCE007BBDCE0042DEF7007BBDCE0042DE
      F70084DEDE0042DEF70042DEF700007BBD000000000000000000427308000000
      00000000000000000000427308000000000042730800B5F76300B5F76300B5F7
      6300B5F76300B5F76300B5F76300B5F763008484840084848400848484008484
      840084848400848484008484840084848400848484008484840084848400FFFF
      FF0084848400FFFFFF0084848400FFFFFF00000000006B4A1800E7EFEF00D6DE
      E700C6CED600B5C6CE00B5C6CE00ADBDC6006B4A180000000000000000000000
      0000BD8C6B00A5735200DEEFEF00211000000000000000000000000000000000
      000000000000089CCE0018BDD6007BBDCE0084DEDE0052EFF70073CEDE0052EF
      F70073CEDE0052EFF7006BA5BD00007BBD000000000000000000427308000000
      00000000000000000000427308004273080042730800B5F76300B5F76300B5F7
      6300B5F76300B5F76300B5F76300B5F76300A57300007B5A00007B5A00007B5A
      00004A4A4A004A4A4A00848484008484840084848400FFFFFF00FFFFFF00FFFF
      FF007B5A00007B5A00007B5A0000A5730000000000006B4A1800F7F7FF00E7EF
      EF0000000000C6CED600B5C6CE00ADBDC6006B4A1800000000008C6B52009463
      4A00D6E7E700DEEFEF00DEEFEF00291000000000000000000000000000000000
      000018BDD60018BDD60052EFF70052EFF70052EFF70084DEDE0084DEDE0073CE
      DE0052EFF70084DEDE0052EFF700007BBD000000000000000000427308000000
      00000000000000000000427308000000000042730800B5F76300B5F76300B5F7
      6300B5F76300B5F76300B5F76300B5F763000000000000000000000000007B5A
      0000A5730000A57300004A4A4A004A4A4A0084848400FFFFFF00FFFFFF00FFFF
      FF007B5A0000000000000000000000000000000000006B4A1800000000000000
      0000E7EFEF0000000000C6CED600B5C6CE006B4A18005A392900ADB5B5008C94
      9C00DEE7EF00DEEFEF00DEEFEF003118000000000000000000000000000018BD
      D60052CEF70052EFF70052EFF70052EFF70073CEDE0052EFF70073CEDE0084DE
      DE0073CEDE0084DEDE0052EFF700007BBD000000000000000000427308000000
      0000000000000000000042730800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007B5A0000000000000000000000000000000000006B4A1800000000000000
      0000F7F7FF00E7EFEF0000000000C6CED6006B4A18008C949400B5BDBD008C9C
      9C00DEEFEF00DEEFEF00DEEFEF00391800000000000000000000089CCE0031C6
      F70042DEF70052EFF70084DEDE0052EFF70084DEDE0052EFF70073CEDE0052EF
      F70052EFF70084DEDE0052EFF700007BBD000000000000000000427308000000
      0000000000004273080042730800427308004273080042730800427308004273
      0800427308004273080000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007B5A0000000000000000000000000000000000006B4A1800000000000000
      000000000000F7F7FF00E7EFEF00D6DEE7006B4A1800A5ADAD00C6CECE008C9C
      A500DEEFEF00DEEFEF00DEEFEF004A2100000000000018BDD6005AD6EF0039BD
      F7004AE7F70052F7F70052EFF70052EFF70073CEDE0084DEDE0052EFF70052EF
      F70084DEDE0052EFF700089CCE00007BBD000000000000000000427308000000
      00000000000042730800B5F76300B5F76300B5F76300B5F76300B5F76300B5F7
      6300B5F763004273080000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFFDE00FFFF8400F7EF7300FFFF
      00007B5A0000000000000000000000000000000000006B4A1800000000000000
      00000000000000000000F7F7FF00E7EFEF006B4A1800C6CECE00D6DEDE0094A5
      A500E7EFF700A5735200A57352004A210000089CCE00089CCE009CEFF70039BD
      F7005AE7F70073F7F70052EFF70052EFF70084DEDE0052EFF70084DEDE0052EF
      F70018BDD600089CCE0052EFF700007BBD000000000000000000427308004273
      08004273080042730800B5F76300B5F76300B5F76300B5F76300B5F76300B5F7
      6300B5F763004273080000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFF8400FFFF8400FFFF8400FFFF
      84007B5A000000000000000000000000000000000000AD9C7B006B4A18006B4A
      18006B4A18006B4A18006B4A18006B4A1800AD9C7B00E7E7E700E7EFEF009C6B
      5200A5735200C6732100C66B180052210000089CCE00A5DEE700BDEFF70042BD
      F70084E7F700A5F7F700A5EFEF0052EFF70084DEDE0052EFF70052EFF70073CE
      DE00089CCE0052EFF70052EFF700007BBD000000000000000000427308000000
      00000000000042730800B5F76300B5F76300B5F76300B5F76300B5F76300B5F7
      6300B5F763004273080000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFF8400FFFF8400FFFF8400FFFF
      84007B5A00000000000000000000000000000000000000000000000000000000
      000052210000F7F7F700F7F7F70094A5A500F7F7F7009C6B5200A56B5200D684
      3100D67B290000000000C66B21005A290000089CCE00E7EFEF00C6E7EF006BC6
      EF00ADE7EF00D6EFEF00CEEFEF007BBDCE0084DEDE0073CEDE0018BDD600089C
      CE0052EFF70052EFF7004AC6DE00007BBD000000000000000000427308000000
      0000000000004273080042730800427308004273080042730800427308004273
      0800427308004273080000000000000000000000000000000000000000007B5A
      0000A5730000A5730000A57300004A4A4A00FFFF0000FFFF8400FFFF8400FFFF
      DE007B5A00000000000000000000000000000000000000000000000000000000
      0000522100000000000000000000A5735200A5735200EF944200E78C39000000
      0000D6843100D6BD9400D6B58C0063290000089CCE00089CCE00089CCE00089C
      CE00089CCE00089CCE00089CCE00089CCE00089CCE00089CCE0018BDD60052EF
      F7006BD6D60052EFF70052EFF700007BBD000000000000000000427308000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A573
      00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A00007B5A
      0000A57300000000000000000000000000000000000000000000000000000000
      000052210000A5735200A5735200EF944200EF944200EF944200EF944200E7CE
      AD00DEC6A500D6BD9C00BD735200D68C6B000000000000000000000000000084
      C60042DEE70063F7F70063F7F70063F7F7005AEFEF006BEFEF006BEFEF0052F7
      F70052F7F70052F7F7005AF7F700007BBD000000000042730800427308004273
      0800427308004273080042730800427308004273080042730800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EF944200EF944200EF944200EF944200EFDECE00EFDEBD00E7D6
      B500BD735200D68C6B0000000000000000000000000000000000000000000084
      C60042C6DE006BD6E7007BD6E70073CEDE000084C6000084C60073CEDE008CF7
      F70073F7F70084F7F7009CF7F700007BBD000000000042730800B5F76300B5F7
      6300B5F76300B5F76300B5F76300B5F76300B5F7630042730800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B6B6B004A4A4A004A4A4A004A4A4A004A4A4A006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EF944200EF944200EFE7CE00EFE7CE00EFE7CE00BD735200D68C
      6B00000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C6000084C6000084C60000000000000000000084C60073CE
      DE0073D6DE008CCEE7007BC6DE00007BBD000000000042730800B5F76300B5F7
      6300B5F76300B5F76300B5F76300B5F76300B5F7630042730800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A4A4A0000DE000000DE000000DE000000DE00004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EFE7CE00EFE7CE00EFE7CE00BD735200D68C6B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C6000084C6000084C6000084C600000000000000000042730800427308004273
      0800427308004273080042730800427308004273080042730800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B6B6B004A4A4A004A4A4A004A4A4A004A4A4A006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052210000EFE7CE00BD735200D68C6B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBDA500BD73520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400848484008484840084848400000000005A5A8C005A5A8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C600007BBD00007BBD000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000848484008484
      8400848484008484840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000001873CE00214AAD005A5A
      8C00000000000000000000000000000000001884BD004294BD006B6B6B007B73
      6B006B6B6B0029A5DE0084C6E700007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF0084848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF000000000000000000189CFF00006B
      D6002152B50063528C005A8CB500398CB500947B7300BD947B00BDAD8C00C6B5
      9400AD8C7300948473006BA5BD00007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF008484840000FFFF00FFFFFF0000FFFF000000840000000000FFFF
      FF0000FFFF00FFFFFF000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000039B5
      F7001894F700106BD6006B84A500AD948C00E7C69400FFFFC600FFFFE700FFFF
      F70000000000C6A58C00846B5A00007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF008484840000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF00000000008484840000000000000000000000000084848400FFFF
      FF00008400000084000000840000FFFFFF00FFFFFF0000000000FFFFFF000000
      000000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000000018BDD60029B5
      F70052CEF7005ACEF70084B5BD00CE947300FFFFB500FFFFC600FFFFDE000000
      0000FFFFF700FFFFDE00A5846300526B6B000000000000000000000000000000
      000084848400FFFFFF00FFFFFF000084000000840000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF0084848400FFFFFF0000FFFF000000840000000000FFFF
      FF0000FFFF00000000008484840000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
      FF000000000000FFFF00FFFFFF00FFFFFF000000000000000000089CCE0031C6
      F70042DEF70052EFF7009CC6C600EFCE9C00FFEFA500FFF7B500FFFFD600FFFF
      E700FFFFDE00FFFFD600BD9C73006B6B63000000000000840000008400000084
      0000008400000084000000840000008400000084000000840000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF0000840000FFFFFF00FFFFFF0084848400FFFFFF00000084000000000000FF
      FF0000000000848484000000000000000000000000000000000084848400FFFF
      FF000084000000840000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF000000000000FFFF00C6C6C6000000000018BDD6005AD6EF0039BD
      F7004AE7F70052F7F70084C6BD00DED6AD00FFE79C00FFE79C00FFF7B500FFFF
      CE00FFFFC600FFFFBD00CEA57300736B63000000000000840000008400000084
      000000840000008400000084000000840000008400000084000000840000FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008484840000FFFF000000840000000000FFFF
      FF0000000000848484000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00000000000000000000000000089CCE009CEFF70039BD
      F7005AE7F70073F7F7007BDED600C6BD9C00FFF7B500FFE7AD00FFE7A500FFE7
      A500FFDE9400FFF7AD00AD8463004A6B73000000000000840000008400000084
      0000008400000084000000840000008400000084000000840000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF000084000000840000FFFFFF00FFFFFF008484840000FFFF00FFFFFF000000
      000084848400000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF0000000000000000000000000000000000A5DEE700BDEFF70042BD
      F70084E7F700A5F7F700A5EFEF00C6B5AD00EFD6B50000000000FFEFD600FFF7
      B500FFFFB500DEB58400847B7B00007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF000084000000840000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF0000FFFF000000
      000084848400000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF000000000000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF000000000000000000000000000000000000000000E7EFEF00C6E7EF006BC6
      EF00ADE7EF00D6EFEF00CEEFEF007BBDCE0094ADB500D6BDBD00DED6C600DECE
      A500BD9C7B009C8C84004AC6DE00007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00008400000084000000840000FFFFFF00FFFFFF0000000000000000008484
      840000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF0000000000FFFFFF000000
      00000000000000000000000000000000000000000000089CCE00089CCE00089C
      CE00089CCE00089CCE00089CCE0018BDD60063E7E70084DEDE007BB5AD007BAD
      AD006BD6D60052EFF70052EFF700007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000084
      C60042DEE70063F7F70063F7F70063F7F7005AEFEF006BEFEF006BEFEF0052F7
      F70052F7F70052F7F7005AF7F700007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000000000000000000000000000000000000000000084848400FFFF
      FF000084000000840000008400000084000000840000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00008400000084000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000084
      C60042C6DE006BD6E7007BD6E70073CEDE000084C6000084C60073CEDE008CF7
      F70073F7F70084F7F7009CF7F700007BBD000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084C6000084C6000084C6000084C60000000000000000000084C60073CE
      DE0073D6DE008CCEE7007BC6DE00007BBD000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      C6000084C6000084C6000084C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000084000084848400C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000000FF0084848400C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF0000840000008400000084000084848400C6C6C600FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF000000FF000000FF000000FF0084848400C6C6C600FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF000084000000840000FFFFFF00008400000084000084848400FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF000000FF000000FF00FFFFFF000000FF000000FF0084848400FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00008400008484840084848400FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF008484840084848400FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000084848400C6C6
      C600FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF0084848400C6C6
      C600FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00008400008484
      8400FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF008484
      8400FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084
      0000FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00C6C6C6000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFC7FFFFFFFFFFFFFF838003
      F81FF81FFF83C001F00FF00FFF07C001E007E007FF07C001C007C007F80FC001
      C003C003E01F8001C003C003C01FC001C003C003C80FC001C003C0038C07C001
      C007C0078007C001E007E007800FC001F00FF00FC02FC001F81FF81FE00FC001
      FFFFFFFFF81FC001FFFFFFFFFEFFFFFF8000E7F8F001FFFFC0018000F001FFFF
      E0038000F001F81FF007C000F001F00FE00FC0008001E007C01FC0008001C007
      C00FC0008001C003C00FC00A8001C003C00FC00E8001C003C007C00E800FC003
      C007C00E800FC307E007FFFC800FE0078007FFFC800FF00F8007FFFCF07FF81F
      E00FFFFCF07FFFFFF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFF803FFFF
      FFFF9C03F001FC03FFFF8801F001F801FCFF8001F001F801FC7F8001F001F801
      F83F8001F001F801F23FC001F001F801FF1FC001C001F801FF8FE001C001F801
      FFCFE0010001C001FFEFF00100038001FFFFF00300FF8003FFFFF83F00FFC0FF
      FFFFF83FC3FFFFFFFFFFFC7FC3FFFFFFFFFFDC00DC00FFFFFC03DD80DD80FFFF
      F801DD80DD80FFFFF801DDFFDDFFF3FFF801D801D801F9E7F801D801D801FC47
      F801C001C001FE1FF801D801D801FE1FC001D801D801FC1FC001DFFFDFFFF88F
      0001801F801FF3C70003801F801FF7E700FF801F801FEFF700FF801F801FFFFF
      C3FF801F801FFFFFC3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFF
      FC03FC039C03FC03F801F8018801F801F801F8018001F801F801F8018001F801
      F801F8018001F801F801F801C001F801C001F801C001F801C001F801E001F801
      0001C001E001C00100018001F001800100038003F003800300FFC0FFF83FC0FF
      C3FFFFFFF83FFFFFC3FFFFFFFC7FFFFFF801F07FFFFFFFFFF801F07FFFFFBFFF
      F801F83FFFFF9C03F801F83FFFFF8801F801FC07C03F8001F8018001801F8001
      F8010000801F8001F8010004801FC001E001000C801FC001E1010000801FE001
      80630000801FE00180070000801FF001807F0001801FF003807F0007801FF83F
      E1FF001FC03FF83FE1FF807FFFFFFC7FC003F000FFFFCFFFC003F000F00187CF
      C003F000B0018387C003F00090018103C003F0008001C0038003F0008001E007
      C003F0008001F00FC003F000C001F81FC003C000C001F81FC003C000E061F01F
      C0030000E063E00F80030000F007C107C0070001F81FC383C00F008BF81FE7C3
      C01FC007FC3FFFE3FFFFC3FFFFFFFFFFFFFFFE009FF0F0008001FC008F00F000
      8001F800C000F0008001F000E008F0008001E000C010F0008001C000C000F000
      800180008000F000800100008000F000800100008040F000800100008000F000
      80010000800000008001E000E00000008001E006E00000018001F0C0F0C0008B
      8001FFE1FFE1F0078001FFFFFFFFFFFFFFFFFFFFFFFFFC1FC003C001C003F007
      C003D801C003E003E047C001E047C001F09FD801F09FC001F90FC001F90F8000
      F8AFD801F8AF8000F14FC001F14F8000E0A7D801E0A78000E153C001E1538000
      C0A7D801C0A7C001C14FC001C14FC001C0BFD801C0BFE003E05FC001E05FF007
      E00FFFFFE00FFC1FF81FFFFFF81FFFFFFE00FFFF0022807CFC00DD0000008070
      F800DC0000008040F000DD00E007A000E000DDFFE007B000C000D803E007B800
      8000D803E007BC000000C003E00780000000D803E007F0040000D803E007F610
      0000DFFFE007F000E000803FFFFFF003E000803FF81FF00FF0C0803FF81FF03F
      FFE1803FF81FF0FFFFFFFFFFFFFFF3FFFFFFFC03FE009FF0F003C001C0008F00
      F003C001C000C000F003C001C000E008F003C001C000C010F003C001C000C000
      8003C003C00080008003C003C00180008003C007C0038040F003C007C0078000
      F003C00FC00F8000F003C00FC00FE000F007C00FC00FE000F00FC00FC00FF0C0
      F01FC00FC00FFFE1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1FC003C003E007FE1F
      C003C003E007FE1FC003C003E007FE3FC003C003E007FF1FC003C003E007FE1F
      C003C003E007FE1FC003C003E007FE1FC003C003E007FE1FC003C003E007FE1F
      C003C003E007FE1FC003C003E007FE1FC003C003E00FFE1FC003C003E01FFE1F
      C003C003E03FFE3FFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object quCardsSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    COMMENT = :COMMENT,'
      '    RCATEGORY = :RCATEGORY,'
      '    CTO = :CTO,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    CODEZAK = :CODEZAK,'
      '    ALGCLASS = :ALGCLASS,'
      '    ALGMAKER = :ALGMAKER,'
      '    VOL = :VOL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    COMMENT,'
      '    RCATEGORY,'
      '    CTO,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    CODEZAK,'
      '    ALGCLASS,'
      '    ALGMAKER,'
      '    VOL'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :COMMENT,'
      '    :RCATEGORY,'
      '    :CTO,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :CODEZAK,'
      '    :ALGCLASS,'
      '    :ALGMAKER,'
      '    :VOL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT c.ID,c.PARENT,c.NAME,c.TTYPE,c.IMESSURE,c.INDS,c.MINREST,' +
        'c.LASTPRICEIN,c.LASTPRICEOUT,c.LASTPOST,c.IACTIVE,c.TCARD'
      
        ',me.NAMESHORT,n.NAMENDS,n.PROC,c.CATEGORY,me.KOEF,c.SPISSTORE,c.' +
        'COMMENT,c.RCATEGORY,c.CTO,'
      
        'c.BB,c.GG,c.U1,c.U2,c.EE,c.CODEZAK,c.ALGCLASS,c.ALGMAKER,c.VOL,m' +
        'k.NAMEM'
      'FROM OF_CARDS c'
      'left join OF_MESSUR me on me.ID=c.IMESSURE'
      'left join OF_NDS n on n.ID=c.INDS'
      'left join OF_MAKERS mk on mk.ID=c.ALGMAKER'
      ''
      'where(  c.PARENT=:PARENTID'
      '     ) and (     C.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT c.ID,c.PARENT,c.NAME,c.TTYPE,c.IMESSURE,c.INDS,c.MINREST,' +
        'c.LASTPRICEIN,c.LASTPRICEOUT,c.LASTPOST,c.IACTIVE,c.TCARD'
      
        ',me.NAMESHORT,n.NAMENDS,n.PROC,c.CATEGORY,me.KOEF,c.SPISSTORE,c.' +
        'COMMENT,c.RCATEGORY,c.CTO,'
      
        'c.BB,c.GG,c.U1,c.U2,c.EE,c.CODEZAK,c.ALGCLASS,c.ALGMAKER,c.VOL,m' +
        'k.NAMEM'
      'FROM OF_CARDS c'
      'left join OF_MESSUR me on me.ID=c.IMESSURE'
      'left join OF_NDS n on n.ID=c.INDS'
      'left join OF_MAKERS mk on mk.ID=c.ALGMAKER'
      ''
      'where c.PARENT=:PARENTID'
      'Order by c.NAME')
    Transaction = trSelRH
    Database = OfficeRnDb
    AutoCommit = True
    Left = 848
    Top = 256
    poAskRecordCount = True
    object quCardsSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCardsSelPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quCardsSelNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quCardsSelTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object quCardsSelIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quCardsSelINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quCardsSelMINREST: TFIBFloatField
      FieldName = 'MINREST'
    end
    object quCardsSelLASTPRICEIN: TFIBFloatField
      FieldName = 'LASTPRICEIN'
    end
    object quCardsSelLASTPRICEOUT: TFIBFloatField
      FieldName = 'LASTPRICEOUT'
    end
    object quCardsSelLASTPOST: TFIBIntegerField
      FieldName = 'LASTPOST'
    end
    object quCardsSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quCardsSelNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quCardsSelNAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quCardsSelPROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quCardsSelTCARD: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quCardsSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quCardsSelKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
    object quCardsSelSPISSTORE: TFIBStringField
      FieldName = 'SPISSTORE'
      EmptyStrToNull = True
    end
    object quCardsSelRemn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Remn'
      DisplayFormat = '0.000'
      Calculated = True
    end
    object quCardsSelCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quCardsSelBB: TFIBFloatField
      FieldName = 'BB'
      DisplayFormat = '0.#'
    end
    object quCardsSelGG: TFIBFloatField
      FieldName = 'GG'
      DisplayFormat = '0.#'
    end
    object quCardsSelU1: TFIBFloatField
      FieldName = 'U1'
      DisplayFormat = '0.#'
    end
    object quCardsSelU2: TFIBFloatField
      FieldName = 'U2'
      DisplayFormat = '0.#'
    end
    object quCardsSelEE: TFIBFloatField
      FieldName = 'EE'
      DisplayFormat = '0.#'
    end
    object quCardsSelRCATEGORY: TFIBIntegerField
      FieldName = 'RCATEGORY'
    end
    object quCardsSelCTO: TFIBIntegerField
      FieldName = 'CTO'
    end
    object quCardsSelCODEZAK: TFIBIntegerField
      FieldName = 'CODEZAK'
    end
    object quCardsSelALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quCardsSelALGMAKER: TFIBIntegerField
      FieldName = 'ALGMAKER'
    end
    object quCardsSelNAMEM: TFIBStringField
      FieldName = 'NAMEM'
      Size = 200
      EmptyStrToNull = True
    end
    object quCardsSelVOL: TFIBFloatField
      FieldName = 'VOL'
      DisplayFormat = '0.0##'
    end
  end
  object dsCardsSel: TDataSource
    DataSet = quCardsSel
    Left = 852
    Top = 308
  end
  object quFind: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID,PARENT,NAME,TCARD'
      'FROM OF_CARDS ')
    OnCalcFields = quFindCalcFields
    Transaction = trSel
    Database = OfficeRnDb
    Left = 688
    Top = 324
    poAskRecordCount = True
    object quFindID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quFindNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quFindTCARD2: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quFindGROUP: TStringField
      FieldKind = fkCalculated
      FieldName = 'GROUP'
      Size = 100
      Calculated = True
    end
    object quFindSGROUP: TStringField
      FieldKind = fkCalculated
      FieldName = 'SGROUP'
      Size = 100
      Calculated = True
    end
  end
  object dsFind: TDataSource
    DataSet = quFind
    Left = 688
    Top = 376
  end
  object quFindCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID_PARENT,NAMECL'
      'FROM OF_CLASSIF'
      'where ITYPE=1'
      'and ID=:IDCL')
    Transaction = trSel1
    Database = OfficeRnDb
    AutoCommit = True
    Left = 28
    Top = 304
    poAskRecordCount = True
    object quFindClID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object quFindClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object trSel1: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 28
    Top = 372
  end
  object quDocInRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADIN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    COMMENT = :COMMENT,'
      '    OPRIZN = :OPRIZN,'
      '    CREATETYPE = :CREATETYPE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADIN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADIN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    COMMENT,'
      '    OPRIZN,'
      '    CREATETYPE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :COMMENT,'
      '    :OPRIZN,'
      '    :CREATETYPE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,'
      '       DATEDOC,'
      '       NUMDOC,'
      '       DATESF,'
      '       NUMSF,'
      '       IDCLI,'
      '       IDSKL,'
      '       SUMIN,'
      '       SUMUCH,'
      '       SUMTAR,'
      '       SUMNDS0,'
      '       SUMNDS1,'
      '       SUMNDS2,'
      '       PROCNAC,'
      '       IACTIVE,'
      '       COMMENT,'
      '       OPRIZN,'
      '       CREATETYPE'
      'FROM OF_DOCHEADIN'
      'where(  IDCLI=:ICLI'
      'and IDSKL=:ISKL'
      'and DATEDOC=:DDATE'
      'and CREATETYPE=1'
      'and NUMDOC=:SNUM'
      '     ) and (     OF_DOCHEADIN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,'
      '       DATEDOC,'
      '       NUMDOC,'
      '       DATESF,'
      '       NUMSF,'
      '       IDCLI,'
      '       IDSKL,'
      '       SUMIN,'
      '       SUMUCH,'
      '       SUMTAR,'
      '       SUMNDS0,'
      '       SUMNDS1,'
      '       SUMNDS2,'
      '       PROCNAC,'
      '       IACTIVE,'
      '       COMMENT,'
      '       OPRIZN,'
      '       CREATETYPE'
      'FROM OF_DOCHEADIN'
      'where IDCLI=:ICLI'
      'and IDSKL=:ISKL'
      'and DATEDOC=:DDATE'
      'and CREATETYPE=1'
      'and NUMDOC=:SNUM')
    Transaction = trSelRH
    Database = OfficeRnDb
    UpdateTransaction = trUpdRH
    AutoCommit = True
    Left = 308
    Top = 12
    poAskRecordCount = True
    object quDocInRecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocInRecDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocInRecNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocInRecDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDocInRecNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocInRecIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocInRecIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocInRecSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocInRecSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocInRecSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocInRecSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDocInRecSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDocInRecSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDocInRecPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocInRecIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocInRecCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
    object quDocInRecOPRIZN: TFIBSmallIntField
      FieldName = 'OPRIZN'
    end
    object quDocInRecCREATETYPE: TFIBSmallIntField
      FieldName = 'CREATETYPE'
    end
  end
  object msConnection: TADOConnection
    CommandTimeout = 100
    ConnectionString = 'FILE NAME=C:\_TransportSQL\Bin\ECr.udl'
    ConnectionTimeout = 100
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'SQLOLEDB.1'
    Left = 392
    Top = 13
  end
  object quSpecInSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICE0 = :PRICE0,'
      '    SUM0 = :SUM0,'
      '    NDSPROC = :NDSPROC,'
      '    OPRIZN = :OPRIZN,'
      '    ICODECB = :ICODECB,'
      '    NAMECB = :NAMECB,'
      '    IMCB = :IMCB,'
      '    SMCB = :SMCB,'
      '    KBCB = :KBCB,'
      '    SBARCB = :SBARCB,'
      '    QUANTCB = :QUANTCB'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDNDS,'
      '    SUMNDS,'
      '    IDM,'
      '    KM,'
      '    PRICE0,'
      '    SUM0,'
      '    NDSPROC,'
      '    OPRIZN,'
      '    ICODECB,'
      '    NAMECB,'
      '    IMCB,'
      '    SMCB,'
      '    KBCB,'
      '    SBARCB,'
      '    QUANTCB'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :IDM,'
      '    :KM,'
      '    :PRICE0,'
      '    :SUM0,'
      '    :NDSPROC,'
      '    :OPRIZN,'
      '    :ICODECB,'
      '    :NAMECB,'
      '    :IMCB,'
      '    :SMCB,'
      '    :KBCB,'
      '    :SBARCB,'
      '    :QUANTCB'
      ')')
    RefreshSQL.Strings = (
      'SELECT IDHEAD,'
      '       ID,'
      '       NUM,'
      '       IDCARD,'
      '       QUANT,'
      '       PRICEIN,'
      '       SUMIN,'
      '       PRICEUCH,'
      '       SUMUCH,'
      '       IDNDS,'
      '       SUMNDS,'
      '       IDM,'
      '       KM,'
      '       PRICE0,'
      '       SUM0,'
      '       NDSPROC,'
      '       OPRIZN,'
      '       ICODECB,'
      '       NAMECB,'
      '       IMCB,'
      '       SMCB,'
      '       KBCB,'
      '       SBARCB,'
      '       QUANTCB'
      'FROM OF_DOCSPECIN'
      'where(  IDHEAD=:IDHD'
      '     ) and (     OF_DOCSPECIN.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT IDHEAD,'
      '       ID,'
      '       NUM,'
      '       IDCARD,'
      '       QUANT,'
      '       PRICEIN,'
      '       SUMIN,'
      '       PRICEUCH,'
      '       SUMUCH,'
      '       IDNDS,'
      '       SUMNDS,'
      '       IDM,'
      '       KM,'
      '       PRICE0,'
      '       SUM0,'
      '       NDSPROC,'
      '       OPRIZN,'
      '       ICODECB,'
      '       NAMECB,'
      '       IMCB,'
      '       SMCB,'
      '       KBCB,'
      '       SBARCB,'
      '       QUANTCB'
      'FROM OF_DOCSPECIN'
      'where IDHEAD=:IDHD'
      '')
    Transaction = trSelRH
    Database = OfficeRnDb
    AutoCommit = True
    Left = 308
    Top = 68
    object quSpecInSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInSelNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecInSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInSelPRICEUCH: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object quSpecInSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInSelIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecInSelSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecInSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecInSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInSelPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object quSpecInSelSUM0: TFIBFloatField
      FieldName = 'SUM0'
    end
    object quSpecInSelNDSPROC: TFIBFloatField
      FieldName = 'NDSPROC'
    end
    object quSpecInSelOPRIZN: TFIBIntegerField
      FieldName = 'OPRIZN'
    end
    object quSpecInSelICODECB: TFIBIntegerField
      FieldName = 'ICODECB'
    end
    object quSpecInSelNAMECB: TFIBStringField
      FieldName = 'NAMECB'
      Size = 60
      EmptyStrToNull = True
    end
    object quSpecInSelIMCB: TFIBSmallIntField
      FieldName = 'IMCB'
    end
    object quSpecInSelSMCB: TFIBStringField
      FieldName = 'SMCB'
      Size = 10
      EmptyStrToNull = True
    end
    object quSpecInSelKBCB: TFIBFloatField
      FieldName = 'KBCB'
    end
    object quSpecInSelSBARCB: TFIBStringField
      FieldName = 'SBARCB'
      Size = 15
      EmptyStrToNull = True
    end
    object quSpecInSelQUANTCB: TFIBFloatField
      FieldName = 'QUANTCB'
    end
  end
  object quFCardPro: TADOQuery
    Connection = msConnection
    CommandTimeout = 1000
    Parameters = <
      item
        Name = 'ICODE'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [iCode]'
      '      ,[Kb]'
      '      ,[iCodePro]'
      '      ,[iM]'
      '      ,[Km]'
      '      ,[NamePro]'
      '      ,[SM]'
      '  FROM [dbo].[GoodsPro]'
      '  where iCode=:ICODE')
    Left = 488
    Top = 12
    object quFCardProiCode: TIntegerField
      FieldName = 'iCode'
    end
    object quFCardProKb: TFloatField
      FieldName = 'Kb'
    end
    object quFCardProiCodePro: TIntegerField
      FieldName = 'iCodePro'
    end
    object quFCardProiM: TIntegerField
      FieldName = 'iM'
    end
    object quFCardProKm: TFloatField
      FieldName = 'Km'
    end
    object quFCardProNamePro: TStringField
      FieldName = 'NamePro'
      Size = 100
    end
    object quFCardProSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
  end
end
