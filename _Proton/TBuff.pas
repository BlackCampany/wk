unit TBuff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxCalendar, cxImageComboBox;

type
  TfmTBuff = class(TForm)
    GridTH: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    LevelD: TcxGridLevel;
    ViewD: TcxGridDBTableView;
    LevelDSpec: TcxGridLevel;
    ViewDSpec: TcxGridDBTableView;
    ViewDIType: TcxGridDBColumn;
    ViewDId: TcxGridDBColumn;
    ViewDDateDoc: TcxGridDBColumn;
    ViewDNumDoc: TcxGridDBColumn;
    ViewDIdCli: TcxGridDBColumn;
    ViewDNameCli: TcxGridDBColumn;
    ViewDIdSkl: TcxGridDBColumn;
    ViewDNameSkl: TcxGridDBColumn;
    ViewDSumIN: TcxGridDBColumn;
    ViewDSumUch: TcxGridDBColumn;
    ViewDSpecIType: TcxGridDBColumn;
    ViewDSpecIdHead: TcxGridDBColumn;
    ViewDSpecNum: TcxGridDBColumn;
    ViewDSpecIdCard: TcxGridDBColumn;
    ViewDSpecQuant: TcxGridDBColumn;
    ViewDSpecPriceIn: TcxGridDBColumn;
    ViewDSpecSumIn: TcxGridDBColumn;
    ViewDSpecPriceUch: TcxGridDBColumn;
    ViewDSpecSumUch: TcxGridDBColumn;
    ViewDSpecIdNds: TcxGridDBColumn;
    ViewDSpecSumNds: TcxGridDBColumn;
    ViewDSpecNameC: TcxGridDBColumn;
    ViewDSpecSm: TcxGridDBColumn;
    ViewDSpecIdM: TcxGridDBColumn;
    ViewDSpecColumn1: TcxGridDBColumn;
    ViewDSpecProcPrice: TcxGridDBColumn;
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTBuff: TfmTBuff;

implementation

uses Un1, MT;

{$R *.dfm}

procedure TfmTBuff.cxButton4Click(Sender: TObject);
begin
  //�������
  if LevelD.Visible then
  begin
    with dmMT do
    begin
      taSpecDoc.First;
      while not taSpecDoc.Eof do
      begin
        if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then taSpecDoc.Delete
        else taSpecDoc.Next;
      end;

      if not taHeadDoc.Eof then taHeadDoc.Delete;
    end;
  end;
end;

procedure TfmTBuff.cxButton3Click(Sender: TObject);
begin
//��������
  if LevelD.Visible then
  begin
    with dmMT do
    begin
      taHeadDoc.First;
      taSpecDoc.First;
      while not taSpecDoc.Eof do taSpecDoc.Delete;
      while not taHeadDoc.Eof do taHeadDoc.Delete;
    end;
  end;
end;

end.
