unit SendMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, StdCtrls, cxButtons,
  ExtCtrls, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxImageComboBox;

type
  TfmSendMessage = class(TForm)
    Panel1: TPanel;
    Button3: TcxButton;
    cxButton1: TcxButton;
    Panel2: TPanel;
    GridMessage: TcxGrid;
    ViewMessage: TcxGridDBTableView;
    ViewMessageMESSAG: TcxGridDBColumn;
    LevelMessage: TcxGridLevel;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxIComboBox1: TcxImageComboBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSendMessage: TfmSendMessage;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmSendMessage.FormCreate(Sender: TObject);
Var Rec:tcxImageComboBoxItem;
begin
  GridMessage.Align:=AlClient;
  cxIComboBox1.Properties.Items.Clear;
  Rec:=cxIComboBox1.Properties.Items.Add;
  Rec.Value:=1;  Rec.Description:=CommonSet.Pgroup1N;
  Rec:=cxIComboBox1.Properties.Items.Add;
  Rec.Value:=2;  Rec.Description:=CommonSet.Pgroup2N;
  Rec:=cxIComboBox1.Properties.Items.Add;
  Rec.Value:=3;  Rec.Description:=CommonSet.Pgroup3N;
  Rec:=cxIComboBox1.Properties.Items.Add;
  Rec.Value:=4;  Rec.Description:=CommonSet.Pgroup4N;
  Rec:=cxIComboBox1.Properties.Items.Add;
  Rec.Value:=5;  Rec.Description:=CommonSet.Pgroup5N;
end;

end.
