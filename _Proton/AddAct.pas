unit AddAct;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLabel, ExtCtrls, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxContainer, cxTextEdit,
  StdCtrls, ComCtrls, cxButtons, Placemnt, DBClient, cxButtonEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit, cxImageComboBox, cxMemo,
  cxGroupBox, cxRadioGroup, FR_DSet, FR_DBSet, FR_Class,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxCheckBox, dxmdaset;

type
  TfmAddAct = class(TForm)
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    dsSpecO: TDataSource;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    amAct: TActionManager;
    acAddPos: TAction;
    Label2: TLabel;
    Panel5: TPanel;
    Panel4: TPanel;
    Memo1: TcxMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridAO: TcxGrid;
    ViewAO: TcxGridDBTableView;
    ViewAONum: TcxGridDBColumn;
    ViewAOIdGoods: TcxGridDBColumn;
    ViewAONameG: TcxGridDBColumn;
    ViewAOSM: TcxGridDBColumn;
    ViewAOQuant: TcxGridDBColumn;
    ViewAOPriceIn: TcxGridDBColumn;
    ViewAOSumIn: TcxGridDBColumn;
    ViewAOPriceUch: TcxGridDBColumn;
    ViewAOSumUch: TcxGridDBColumn;
    LevelAO: TcxGridLevel;
    TabSheet2: TTabSheet;
    acSave: TAction;
    dsSpecI: TDataSource;
    GridAI: TcxGrid;
    ViewAI: TcxGridDBTableView;
    LevelAI: TcxGridLevel;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    ViewAINum: TcxGridDBColumn;
    ViewAIIdGoods: TcxGridDBColumn;
    ViewAINameG: TcxGridDBColumn;
    ViewAISM: TcxGridDBColumn;
    ViewAIQuant: TcxGridDBColumn;
    ViewAIPriceIn: TcxGridDBColumn;
    ViewAISumIn: TcxGridDBColumn;
    acCalc1: TAction;
    ViewAIPriceUch: TcxGridDBColumn;
    ViewAISumUch: TcxGridDBColumn;
    Label3: TLabel;
    ViewAIProcPrice: TcxGridDBColumn;
    Label4: TLabel;
    cxTextEdit2: TcxTextEdit;
    acExit: TAction;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    ViewAOPriceIn0: TcxGridDBColumn;
    ViewAOSumIn0: TcxGridDBColumn;
    ViewAIPriceIn0: TcxGridDBColumn;
    ViewAISumIn0: TcxGridDBColumn;
    taSpecO: TdxMemData;
    taSpecONum: TIntegerField;
    taSpecOIdGoods: TIntegerField;
    taSpecONameG: TStringField;
    taSpecOIM: TIntegerField;
    taSpecOSM: TStringField;
    taSpecOQuant: TFloatField;
    taSpecOPriceIn: TFloatField;
    taSpecOSumIn: TFloatField;
    taSpecOPriceUch: TFloatField;
    taSpecOSumUch: TFloatField;
    taSpecOKm: TFloatField;
    taSpecOPriceIn0: TFloatField;
    taSpecOSumIn0: TFloatField;
    taSpecOINds: TSmallintField;
    taSpecOSNds: TStringField;
    taSpecORNds: TFloatField;
    ViewAOSNds: TcxGridDBColumn;
    ViewAORNds: TcxGridDBColumn;
    taSpecI: TdxMemData;
    taSpecINum: TIntegerField;
    taSpecIIdGoods: TIntegerField;
    taSpecINameG: TStringField;
    taSpecIIM: TIntegerField;
    taSpecISM: TStringField;
    taSpecIQuant: TFloatField;
    taSpecIPriceIn: TFloatField;
    taSpecISumIn: TFloatField;
    taSpecIPriceUch: TFloatField;
    taSpecISumUch: TFloatField;
    taSpecIKm: TFloatField;
    taSpecIPriceIn0: TFloatField;
    taSpecISumIn0: TFloatField;
    taSpecIProcPrice: TFloatField;
    taSpecIINds: TSmallintField;
    taSpecISNds: TStringField;
    taSpecIRNds: TFloatField;
    ViewAISNds: TcxGridDBColumn;
    ViewAIRNds: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewAOEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewAOEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel7Click(Sender: TObject);
    procedure ViewAODragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewAODragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure ViewAOEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taSpecO1QuantChange(Sender: TField);
    procedure taSpecO1PriceInChange(Sender: TField);
    procedure taSpecO1SumInChange(Sender: TField);
    procedure taSpecO1PriceUchChange(Sender: TField);
    procedure taSpecO1SumUchChange(Sender: TField);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure ViewAIDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewAIDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewAIEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure ViewAIEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewAIEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure taSpecI1QuantChange(Sender: TField);
    procedure taSpecI1PriceInChange(Sender: TField);
    procedure taSpecI1SumInChange(Sender: TField);
    procedure taSpecI1PriceUchChange(Sender: TField);
    procedure taSpecI1SumUchChange(Sender: TField);
    procedure cxLabel9Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure ViewAISMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ViewAOSMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure taSpecOQuantChange(Sender: TField);
    procedure taSpecOPriceInChange(Sender: TField);
    procedure taSpecOSumInChange(Sender: TField);
    procedure taSpecOPriceUchChange(Sender: TField);
    procedure taSpecOSumUchChange(Sender: TField);
    procedure taSpecOPriceIn0Change(Sender: TField);
    procedure taSpecOSumIn0Change(Sender: TField);
    procedure taSpecIQuantChange(Sender: TField);
    procedure taSpecIPriceInChange(Sender: TField);
    procedure taSpecISumInChange(Sender: TField);
    procedure taSpecIPriceUchChange(Sender: TField);
    procedure taSpecISumUchChange(Sender: TField);
    procedure taSpecIPriceIn0Change(Sender: TField);
    procedure taSpecISumIn0Change(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddAct: TfmAddAct;
  bAdd:Boolean = False;
  iCol,iMax:INteger;
  Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;

implementation

uses Un1, dmOffice, FCards, Goods, DMOReps, DocInv, Message, ActPer,
  OMessure;

{$R *.dfm}

procedure TfmAddAct.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  ViewAO.RestoreFromIniFile(CurDir+GridIni);
  ViewAI.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddAct.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewAO.StoreToIniFile(CurDir+GridIni,False);
      ViewAI.StoreToIniFile(CurDir+GridIni,False);
      Action:= caHide;
    end
    else Action:= caNone;
  end;
end;

procedure TfmAddAct.acAddPosExecute(Sender: TObject);
Var iMax:INteger;
begin
//�������� �������
  iMax:=1;
  if PageControl1.ActivePageIndex=0 then
  begin
    taSpecO.First;
    if not taSpecO.Eof then
    begin
      taSpecO.Last;
      iMax:=taSpecONum.AsInteger+1;
    end;

    taSpecO.Append;
    taSpecONum.AsInteger:=iMax;
    taSpecOIdGoods.AsInteger:=0;
    taSpecONameG.AsString:='';
    taSpecOIM.AsInteger:=0;
    taSpecOSM.AsString:='';
    taSpecOQuant.AsFloat:=0;
    taSpecOPriceIn.AsFloat:=0;
    taSpecOSumIn.AsFloat:=0;
    taSpecOPriceUch.AsFloat:=0;
    taSpecOSumUch.AsFloat:=0;
    taSpecOKm.AsFloat:=0;
    taSpecOPriceIn0.AsFloat:=0;
    taSpecOSumIn0.AsFloat:=0;
    taSpecOINds.AsInteger:=0;
    taSpecOSNds.AsString:='';
    taSpecORNds.AsFloat:=0;
    taSpecO.Post;
    GridAO.SetFocus;

    ViewAONameG.Options.Editing:=True;
    ViewAONameG.Focused:=True;

    prRowFocus(ViewAO);

  end;
  if PageControl1.ActivePageIndex=1 then
  begin
    taSpecI.First;
    if not taSpecI.Eof then
    begin
      taSpecI.Last;
      iMax:=taSpecINum.AsInteger+1;
    end;

    taSpecI.Append;
    taSpecINum.AsInteger:=iMax;
    taSpecIIdGoods.AsInteger:=0;
    taSpecINameG.AsString:='';
    taSpecIIM.AsInteger:=0;
    taSpecISM.AsString:='';
    taSpecIQuant.AsFloat:=0;
    taSpecIPriceIn.AsFloat:=0;
    taSpecISumIn.AsFloat:=0;
    taSpecIPriceUch.AsFloat:=0;
    taSpecISumUch.AsFloat:=0;
    taSpecIKm.AsFloat:=0;
    taSpecIProcPrice.AsFloat:=100;
    taSpecIPriceIn0.AsFloat:=0;
    taSpecISumIn0.AsFloat:=0;
    taSpecIINds.AsInteger:=0;
    taSpecISNds.AsString:='';
    taSpecIRNds.AsFloat:=0;
    taSpecI.Post;
    GridAI.SetFocus;

    ViewAINameG.Options.Editing:=True;
    ViewAINameG.Focused:=True;

    prRowFocus(ViewAI);

  end;
end;

procedure TfmAddAct.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
end;

procedure TfmAddAct.cxLabel1Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddAct.ViewAOEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    rPrNDS:Real;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewAO.Controller.FocusedColumn.Name='ViewAOIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewAO.BeginUpdate;
          taSpecO.Edit;
          taSpecOIdGoods.AsInteger:=iCode;
          taSpecONameG.AsString:=quFCardNAME.AsString;
          taSpecOIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecOSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecOKm.AsFloat:=Km;
          taSpecOQuant.AsFloat:=0;
          taSpecOPriceIn.AsFloat:=0;
          taSpecOSumIn.AsFloat:=0;
          taSpecOPriceUch.AsFloat:=0;
          taSpecOSumUch.AsFloat:=0;
          taSpecOPriceIn0.AsFloat:=0;
          taSpecOSumIn0.AsFloat:=0;
          taSpecOINds.AsInteger:=quFCardINDS.AsINteger;
          taSpecOSNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
          taSpecORNds.AsFloat:=0;
          taSpecO.Post;

          ViewAO.EndUpdate;
        end;
      end;
      if ViewAO.Controller.FocusedColumn.Name='ViewAONameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewAO.BeginUpdate;
            taSpecO.Edit;
            taSpecOIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecONameG.AsString:=quFCardNAME.AsString;
            taSpecOIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecOSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecOKm.AsFloat:=Km;
            taSpecOQuant.AsFloat:=0;
            taSpecOPriceIn.AsFloat:=0;
            taSpecOSumIn.AsFloat:=0;
            taSpecOPriceUch.AsFloat:=0;
            taSpecOSumUch.AsFloat:=0;
            taSpecOPriceIn0.AsFloat:=0;
            taSpecOSumIn0.AsFloat:=0;
            taSpecOINds.AsInteger:=quFCardINDS.AsINteger;
            taSpecOSNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
            taSpecORNds.AsFloat:=0;
            taSpecO.Post;
            ViewAO.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      ViewAOQuant.Focused:=True;
    end else
      if ViewAO.Controller.FocusedColumn.Name='ViewAOIdGoods' then
        if fTestKey(Key)=False then
          if taSpecO.State in [dsEdit,dsInsert] then taSpecO.Cancel;
  end;
end;

procedure TfmAddAct.ViewAOEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
    rPrNDS:Real;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewAO.Controller.FocusedColumn.Name='ViewAONameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewAO.BeginUpdate;
          taSpecO.Edit;
          taSpecOIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecONameG.AsString:=quFCardNAME.AsString;
          taSpecOIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecOSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecOKm.AsFloat:=Km;
          taSpecOQuant.AsFloat:=0;
          taSpecOPriceIn.AsFloat:=0;
          taSpecOSumIn.AsFloat:=0;
          taSpecOPriceUch.AsFloat:=0;
          taSpecOSumUch.AsFloat:=0;
          taSpecOPriceIn0.AsFloat:=0;
          taSpecOSumIn0.AsFloat:=0;
          taSpecOINds.AsInteger:=quFCardINDS.AsINteger;
          taSpecOSNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
          taSpecORNds.AsFloat:=0;
          taSpecO.Post;
          ViewAO.EndUpdate;
          AEdit.SelectAll;

          ViewAONameG.Options.Editing:=False;
          ViewAONameG.Focused:=True;

          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddAct.cxLabel7Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddAct.ViewAODragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDAct then  Accept:=True;
end;

procedure TfmAddAct.ViewAODragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bDAct then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewAO.BeginUpdate;
          iMax:=1;
          taSpecO.First;
          if not taSpecO.Eof then
          begin
            taSpecO.Last;
            iMax:=taSpecONum.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
            //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                if quCardsSelTCard.AsInteger=0 then
                begin
                  try
                    if taSpecO.Locate('IdGoods',iNum,[])=False then
                    begin
                      taSpecO.Append;
                      taSpecONum.AsInteger:=iMax;
                      taSpecOIdGoods.AsInteger:=quCardsSelID.AsInteger;
                      taSpecONameG.AsString:=quCardsSelNAME.AsString;
                      taSpecOIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                      taSpecOSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                      taSpecOQuant.AsFloat:=0;
                      taSpecOPriceIn.AsFloat:=0;
                      taSpecOSumIn.AsFloat:=0;
                      taSpecOPriceUch.AsFloat:=0;
                      taSpecOSumUch.AsFloat:=0;
                      taSpecOKm.AsFloat:=Km;
                      taSpecOPriceIn0.AsFloat:=0;
                      taSpecOSumIn0.AsFloat:=0;
                      taSpecOINds.AsInteger:=quCardsSelINDS.AsINteger;
                      taSpecOSNds.AsString:=quCardsSelNAMENDS.AsString;
                      taSpecORNds.AsFloat:=0;
                      taSpecO.Post;
                      delay(10);
                      inc(iMax);
                    end;
                  except
                  end;
                end;
              end;
            end;
          end;
          ViewAO.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddAct.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddAct.acSaveExecute(Sender: TObject);
Var Idh:Integer;
    rSumIn0,rSumIn,rSumUch:Real;
    iDate:Integer;
    iSS:INteger;
begin
  //��������� ��������������
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;

  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ������.'); delay(10);

  iDate:=Trunc(date);
  if cxDateEdit1.Date>3000 then iDate:=Trunc(cxDateEdit1.Date)
  else cxDateEdit1.Date:=iDate;

  with dmO do
  with dmORep do
  begin
    if taSpecO.State in [dsEdit,dsInsert] then taSpecO.Post;
    if taSpecI.State in [dsEdit,dsInsert] then taSpecI.Post;

    //������� ������ ������
    ViewAO.BeginUpdate;
    ViewAI.BeginUpdate;

    iSS:=prISS(cxLookupComboBox1.EditValue);

    Memo1.Lines.Add('   ��������.'); delay(10);

    //����������
    Memo1.Lines.Add('   ����������.'); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('DocAct');
      cxTextEdit1.Tag:=IDH;      
      if cxTextEdit1.Text=prGetNum(5,0) then prGetNum(5,1); //��������
    end;

    if taSpecO.RecordCount>0 then
    begin
      if cxTextEdit2.Text='' then
      begin
        taSpecO.First;
        cxTextEdit2.Text:='����������� - '+taSpecONameG.AsString+' ('+taSpecOIdGoods.AsString+') ���-�� '+FloatToStr(RoundEx(taSpecOQuant.AsFloat*1000)/1000);
      end;
    end;

    quDocsActsId.Active:=False;

    quDocsActsId.ParamByName('IDH').AsInteger:=IDH;
    quDocsActsId.Active:=True;

    quDocsActsId.First;
    if quDocsActsId.RecordCount=0 then quDocsActsId.Append else quDocsActsId.Edit;

    quDocsActsIdID.AsInteger:=IDH;
    quDocsActsIdDATEDOC.AsDateTime:=iDate;
    quDocsActsIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsActsIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsActsIdSUMIN.AsFloat:=0;
    quDocsActsIdSUMUCH.AsFloat:=0;
    quDocsActsIdOPER.AsString:='AP';
    quDocsActsIdIACTIVE.AsInteger:=0;
    quDocsActsIdCOMMENT.AsString:=cxTextEdit2.Text;
    quDocsActsId.Post;

    cxTextEdit1.Tag:=IDH;

    //�������� ������������ ������ � �����
    quSpecAO.Active:=False;
    quSpecAO.ParamByName('IDH').AsInteger:=IDH;
    quSpecAO.Active:=True;

    quSpecAO.First;
    while not quSpecAO.Eof do quSpecAO.Delete;

    rSumIn0:=0;
    rSumIn:=0;
    rSumUch:=0;

    taSpecO.First;
    while not taSpecO.Eof do
    begin
      quSpecAO.Append;
      quSpecAOIDHEAD.AsInteger:=IDH;
      quSpecAOID.AsInteger:=taSpecONum.AsInteger;
      quSpecAOIDCARD.AsInteger:=taSpecOIdGoods.AsInteger;
      quSpecAOQUANT.AsFloat:=taSpecOQuant.AsFloat;
      quSpecAOIDM.AsInteger:=taSpecOIM.AsInteger;
      quSpecAOKM.AsFloat:=taSpecOKm.AsFloat;
      quSpecAOPRICEIN.AsFloat:=taSpecOPriceIn.AsFloat;
      quSpecAOSUMIN.AsFloat:=taSpecOSumIn.AsFloat;
      quSpecAOPRICEINUCH.AsFloat:=taSpecOPriceUch.AsFloat;
      quSpecAOSUMINUCH.AsFloat:=taSpecOSumUch.AsFloat;
      quSpecAOTCARD.AsInteger:=0;
      quSpecAOPRICEIN0.AsFloat:=taSpecOPriceIn0.AsFloat;
      quSpecAOSUMIN0.AsFloat:=taSpecOSumIn0.AsFloat;
      quSpecAOINDS.AsInteger:=taSpecOINds.AsInteger;
      quSpecAORNDS.AsFloat:=taSpecORNds.AsFloat;
      quSpecAO.Post;

      rSumIn0:=rSumIn0+taSpecOSumIn0.AsFloat;
      rSumIn:=rSumIn+taSpecOSumIn.AsFloat;
      rSumUch:=rSumUch+taSpecOSumUch.AsFloat;

      taSpecO.Next; delay(10);
    end;
    quSpecAO.Active:=False;


    //�������� ������������ ������ � �����
    quSpecAI.Active:=False;
    quSpecAI.ParamByName('IDH').AsInteger:=IDH;
    quSpecAI.Active:=True;

    quSpecAI.First;
    while not quSpecAI.Eof do quSpecAI.Delete;

    taSpecI.First;
    while not taSpecI.Eof do
    begin
      quSpecAI.Append;
      quSpecAIIDHEAD.AsInteger:=IDH;
      quSpecAIID.AsInteger:=taSpecINum.AsInteger;
      quSpecAIIDCARD.AsInteger:=taSpecIIdGoods.AsInteger;
      quSpecAIQUANT.AsFloat:=taSpecIQuant.AsFloat;
      quSpecAIIDM.AsInteger:=taSpecIIM.AsInteger;
      quSpecAIKM.AsFloat:=taSpecIKm.AsFloat;
      quSpecAIPRICEIN.AsFloat:=taSpecIPriceIn.AsFloat;
      quSpecAISUMIN.AsFloat:=taSpecISumIn.AsFloat;
      quSpecAIPRICEINUCH.AsFloat:=taSpecIPriceUch.AsFloat;
      quSpecAISUMINUCH.AsFloat:=taSpecISumUch.AsFloat;
      quSpecAITCARD.AsInteger:=0;
      quSpecAIProcPrice.AsFloat:=taSpecIProcPrice.AsFloat;
      quSpecAIPRICEIN0.AsFloat:=taSpecIPriceIn0.AsFloat;
      quSpecAISUMIN0.AsFloat:=taSpecISumIn0.AsFloat;
      quSpecAIINDS.AsInteger:=taSpecIINds.AsInteger;
      quSpecAIRNDS.AsFloat:=taSpecIRNds.AsFloat;
      quSpecAI.Post;

      taSpecI.Next; delay(10);
    end;
    quSpecAI.Active:=False;

    quDocsActsId.Edit;
    if iSS<>2 then quDocsActsIdSUMIN.AsFloat:=rv(rSumIn)
    else quDocsActsIdSUMIN.AsFloat:=rv(rSumIn0);

    quDocsActsIdSUMUCH.AsFloat:=rv(rSumUch);
    quDocsActsId.Post;

    quDocsActsId.Active:=False;

    fmDocsActs.ViewActs.BeginUpdate;
    quDocsActs.FullRefresh;
    quDocsActs.Locate('ID',IDH,[]);
    fmDocsActs.ViewActs.EndUpdate;

    ViewAO.EndUpdate;
    ViewAI.EndUpdate;
  end;
  Memo1.Lines.Add('���������� ��.'); delay(10);
  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
end;

procedure TfmAddAct.ViewAOEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOQuant' then iCol:=1;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOPriceIn' then iCol:=2;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOSumIn' then iCol:=3;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOPriceUch' then iCol:=4;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOSumUch' then iCol:=5;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOPriceIn0' then iCol:=6;
  if ViewAO.Controller.FocusedColumn.Name='ViewAOSumIn0' then iCol:=7;
end;

procedure TfmAddAct.taSpecO1QuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOSumIn0.AsFloat:=taSpecOPriceIn0.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumIn.AsFloat:=taSpecOPriceIn.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumUch.AsFloat:=taSpecOPriceUch.AsFloat*taSpecOQuant.AsFloat;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecO1PriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOSumIn.AsFloat:=taSpecOPriceIn.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumIn0.AsFloat:=rv(taSpecOSumIn.AsFloat*100/(100+vNds[taSpecOINds.AsInteger]));
    if taSpecOQuant.AsFloat<>0 then taSpecOPriceIn0.AsFloat:=taSpecOSumIn0.AsFloat/taSpecOQuant.AsFloat
    else taSpecOPriceIn0.AsFloat:=0;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;

//    taSpecSumUchF.AsFloat:=taSpecPriceUchF.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecO1SumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    taSpecOSumIn0.AsFloat:=rv(taSpecOSumIn.AsFloat*100/(100+vNds[taSpecOINds.AsInteger]));
    if abs(taSpecOQuant.AsFloat)>0 then
    begin
      taSpecOPriceIn.AsFloat:=taSpecOSumIn.AsFloat/taSpecOQuant.AsFloat;
      taSpecOPriceIn0.AsFloat:=taSpecOSumIn0.AsFloat/taSpecOQuant.AsFloat;
    end
    else
    begin
      taSpecOPriceIn.AsFloat:=0;
      taSpecOPriceIn0.AsFloat:=0;
    end;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecO1PriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
//    taSpecSumInF.AsFloat:=taSpecPriceInF.AsFloat*taSpecQuantFact.AsFloat;
    taSpecOSumUch.AsFloat:=taSpecOPriceUch.AsFloat*taSpecOQuant.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecO1SumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� ����� �������
  begin
    if abs(taSpecOQuant.AsFloat)>0 then taSpecOPriceUch.AsFloat:=RoundEx(taSpecOSumUch.AsFloat/taSpecOQuant.AsFloat*100)/100;
  end;
end;


procedure TfmAddAct.acAddListExecute(Sender: TObject);
begin
  bAddSpecAO:=True;
  fmGoods.Show;
end;

procedure TfmAddAct.acDelPosExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
    if taSpecO.RecordCount>0 then taSpecO.Delete;
  if PageControl1.ActivePageIndex=1 then
    if taSpecI.RecordCount>0 then taSpecI.Delete;
end;

procedure TfmAddAct.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if MessageDlg('�������� ������ �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpecO.First;
      while not taSpecO.Eof do taSpecO.Delete;
    end;
  end;
  if PageControl1.ActivePageIndex=1 then
  begin
    if MessageDlg('�������� ������ �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpecI.First;
      while not taSpecI.Eof do taSpecI.Delete;
    end;
  end;
end;

procedure TfmAddAct.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmAddAct.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    quMHAll.FullRefresh;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddAct.ViewAIDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bAddSpecAO then  Accept:=True;
end;

procedure TfmAddAct.ViewAIDragDrop(Sender, Source: TObject; X, Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bAddSpecAO then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewAI.BeginUpdate;
          iMax:=1;
          taSpecI.First;
          if not taSpecI.Eof then
          begin
            taSpecI.Last;
            iMax:=taSpecINum.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                if quCardsSelTCard.AsInteger=0 then
                begin
                  try
                    if taSpecI.Locate('IdGoods',iNum,[])=False then
                    begin
                      taSpecI.Append;
                      taSpecINum.AsInteger:=iMax;
                      taSpecIIdGoods.AsInteger:=quCardsSelID.AsInteger;
                      taSpecINameG.AsString:=quCardsSelNAME.AsString;
                      taSpecIIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                      taSpecISM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                      taSpecIQuant.AsFloat:=0;
                      taSpecIPriceIn.AsFloat:=0;
                      taSpecISumIn.AsFloat:=0;
                      taSpecIPriceUch.AsFloat:=0;
                      taSpecISumUch.AsFloat:=0;
                      taSpecIKm.AsFloat:=Km;
                      taSpecIProcPrice.AsFloat:=100;
                      taSpecIPriceIn0.AsFloat:=0;
                      taSpecISumIn0.AsFloat:=0;
                      taSpecIINds.AsInteger:=quCardsSelINDS.AsINteger;
                      taSpecISNds.AsString:=quCardsSelNAMENDS.AsString;
                      taSpecIRNds.AsFloat:=0;
                      taSpecI.Post;
                      delay(10);
                      inc(iMax);
                    end;
                  except
                  end;
                end;
              end;
            end;
          end;
          ViewAI.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddAct.ViewAIEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewAI.Controller.FocusedColumn.Name='ViewAIQuant' then iCol:=1;
  if ViewAI.Controller.FocusedColumn.Name='ViewAIPriceIn' then iCol:=2;
  if ViewAI.Controller.FocusedColumn.Name='ViewAISumIn' then iCol:=3;
  if ViewAI.Controller.FocusedColumn.Name='ViewAIPriceUch' then iCol:=4;
  if ViewAI.Controller.FocusedColumn.Name='ViewAISumUch' then iCol:=5;
  if ViewAI.Controller.FocusedColumn.Name='ViewAIPriceIn0' then iCol:=6;
  if ViewAI.Controller.FocusedColumn.Name='ViewAISumIn0' then iCol:=7;
end;

procedure TfmAddAct.PageControl1Change(Sender: TObject);
begin
  iCol:=0;
end;

procedure TfmAddAct.ViewAIEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    rPrNDS:Real;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewAI.Controller.FocusedColumn.Name='ViewAIIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewAI.BeginUpdate;
          taSpecI.Edit;
          taSpecIIdGoods.AsInteger:=iCode;
          taSpecINameG.AsString:=quFCardNAME.AsString;
          taSpecIIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecISM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecIKm.AsFloat:=Km;
          taSpecIQuant.AsFloat:=0;
          taSpecIPriceIn.AsFloat:=0;
          taSpecISumIn.AsFloat:=0;
          taSpecIPriceUch.AsFloat:=0;
          taSpecISumUch.AsFloat:=0;
          taSpecIProcPrice.AsFloat:=100;
          taSpecIPriceIn0.AsFloat:=0;
          taSpecISumIn0.AsFloat:=0;
          taSpecIINds.AsInteger:=quFCardINDS.AsINteger;
          taSpecISNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
          taSpecIRNds.AsFloat:=0;
          taSpecI.Post;

          ViewAI.EndUpdate;
        end;
      end;
      if ViewAI.Controller.FocusedColumn.Name='ViewAINameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewAI.BeginUpdate;
            taSpecI.Edit;
            taSpecIIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecINameG.AsString:=quFCardNAME.AsString;
            taSpecIIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecISM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecIKm.AsFloat:=Km;
            taSpecIQuant.AsFloat:=0;
            taSpecIPriceIn.AsFloat:=0;
            taSpecISumIn.AsFloat:=0;
            taSpecIPriceUch.AsFloat:=0;
            taSpecISumUch.AsFloat:=0;
            taSpecIProcPrice.AsFloat:=100;
            taSpecIPriceIn0.AsFloat:=0;
            taSpecISumIn0.AsFloat:=0;
            taSpecIINds.AsInteger:=quFCardINDS.AsINteger;
            taSpecISNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
            taSpecIRNds.AsFloat:=0;
            taSpecI.Post;
            ViewAI.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      ViewAIQuant.Focused:=True;
    end else
      if ViewAI.Controller.FocusedColumn.Name='ViewAIIdGoods' then
        if fTestKey(Key)=False then
          if taSpecI.State in [dsEdit,dsInsert] then taSpecI.Cancel;
  end;
end;

procedure TfmAddAct.ViewAIEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
    rPrNDS:Real;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewAI.Controller.FocusedColumn.Name='ViewAINameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewAI.BeginUpdate;
          taSpecI.Edit;
          taSpecIIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecINameG.AsString:=quFCardNAME.AsString;
          taSpecIIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecISM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecIKm.AsFloat:=Km;
          taSpecIQuant.AsFloat:=0;
          taSpecIPriceIn.AsFloat:=0;
          taSpecISumIn.AsFloat:=0;
          taSpecIPriceUch.AsFloat:=0;
          taSpecISumUch.AsFloat:=0;
          taSpecIProcPrice.AsFloat:=100;
          taSpecIPriceIn0.AsFloat:=0;
          taSpecISumIn0.AsFloat:=0;
          taSpecIINds.AsInteger:=quFCardINDS.AsINteger;
          taSpecISNds.AsString:=prGetNDS(quFCardINDS.AsINteger,rPrNDS);
          taSpecIRNds.AsFloat:=0;
          taSpecI.Post;
          ViewAI.EndUpdate;
          AEdit.SelectAll;

          ViewAINameG.Options.Editing:=False;
          ViewAINameG.Focused:=True;

          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddAct.taSpecI1QuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecISumIn.AsFloat:=taSpecIPriceIn.AsFloat*taSpecIQuant.AsFloat;
    taSpecISumUch.AsFloat:=taSpecIPriceUch.AsFloat*taSpecIQuant.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecI1PriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecISumIn.AsFloat:=taSpecIPriceIn.AsFloat*taSpecIQuant.AsFloat;
//    taSpecSumUchF.AsFloat:=taSpecPriceUchF.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecI1SumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    if abs(taSpecIQuant.AsFloat)>0 then taSpecIPriceIn.AsFloat:=RoundEx(taSpecISumIn.AsFloat/taSpecIQuant.AsFloat*100)/100;
  end;
end;

procedure TfmAddAct.taSpecI1PriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
//    taSpecSumInF.AsFloat:=taSpecPriceInF.AsFloat*taSpecQuantFact.AsFloat;
    taSpecISumUch.AsFloat:=taSpecIPriceUch.AsFloat*taSpecIQuant.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecI1SumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� �����
  begin
    if abs(taSpecIQuant.AsFloat)>0 then taSpecIPriceUch.AsFloat:=RoundEx(taSpecISumUch.AsFloat/taSpecIQuant.AsFloat*100)/100;
  end;
end;

procedure TfmAddAct.cxLabel9Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddAct.Label3Click(Sender: TObject);
Var PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure:Real;
    rSumRemn,rSumRemn0:Real;
    FullProc,rP:Real;
    PriceSp0,rSumIn0:Real;
begin
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;

  iCol:=0;
  //����������� ���� ������
  with dmO do
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      //������� ������ �� �������, ����� �� �������
      ViewAO.BeginUpdate;
      taSpecO.First;
      while not taSpecO.Eof do
      begin
        //���� ��� ����� �� ���� ��������� �������

        PriceSp:=0;
        rSumIn:=0;
        rSumUch:=0;
        PriceSp0:=0;
        rSumIn0:=0;

        rQs:=taSpecOQuant.AsFloat*taSpecOKm.AsFloat; //�������� � ��������
        prSelPartIn(taSpecOIdGoods.AsInteger,cxLookupComboBox1.EditValue,0,0);

        quSelPartIn.First;
        if rQs>0 then
        begin
          while (not quSelPartIn.Eof) and (rQs>0) do
          begin
            //���� �� ���� ������� ���� �����, ��������� �������� ���
            rQp:=quSelPartInQREMN.AsFloat;
            if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
            rQs:=rQs-rQ;

            PriceSp0:=quSelPartInPRICEIN0.AsFloat;
            PriceSp:=quSelPartInPRICEIN.AsFloat;
            PriceUch:=quSelPartInPRICEOUT.AsFloat;
            rSumIn0:=rSumIn0+PriceSp0*rQ;
            rSumIn:=rSumIn+PriceSp*rQ;
            rSumUch:=rSumUch+PriceUch*rQ;
            quSelPartIn.Next;
          end;

          if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
          begin
            if PriceSp=0 then
            begin //��� ���� ���������� ������� � ���������� ����������
              prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecOIdGoods.AsInteger;
              prCalcLastPrice1.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
              prCalcLastPrice1.ExecProc;
              PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
              PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;
              rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
              if (rMessure<>0)and(rMessure<>1) then
              begin
                PriceSp:=PriceSp/rMessure;
                PriceSp0:=PriceSp0/rMessure;
              end;
            end;
            rSumIn:=rSumIn+PriceSp*rQs;
            rSumIn0:=rSumIn0+PriceSp0*rQs;
          end;
        end;
        quSelPartIn.Active:=False;

        //�������� ���������
        taSpecO.Edit;
        if taSpecOQuant.AsFloat<>0 then
        begin
          taSpecOSumIn.AsFloat:=rv(rSumIn);
          taSpecOSumUch.AsFloat:=rv(rSumUch);
          taSpecOPriceIn.AsFloat:=rSumIn/taSpecOQuant.AsFloat;
          taSpecOPriceUch.AsFloat:=rSumUch/taSpecOQuant.AsFloat;
          taSpecOSumIn0.AsFloat:=rv(rSumIn0);
          taSpecOPriceIn0.AsFloat:=rSumIn0/taSpecOQuant.AsFloat;
          taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
        end else
        begin
          taSpecOSumIn.AsFloat:=0;
          taSpecOSumUch.AsFloat:=0;
          taSpecOPriceIn.AsFloat:=0;
          taSpecOPriceUch.AsFloat:=0;
          taSpecORNds.AsFloat:=0;
        end;
        taSpecO.Post;

        taSpecO.Next;
        delay(10);
      end;
      taSpecO.First;
      ViewAO.EndUpdate;
    end;
    // ����� ������� ���� ����� ������� = ����� �������
    // ������ ������ ������ - ��������������� ����� ������� �������� ��� ����� �������
    if PageControl1.ActivePageIndex=1 then
    begin
      //1.������ ����� �������
      rSumIn0:=0;
      rSumIn:=0;
      rSumUch:=0;
      ViewAO.BeginUpdate;
      taSpecO.First;
      while not taSpecO.Eof do
      begin
        rSumIn0:=rSumIn0+taSpecOSumIn0.AsFloat;
        rSumIn:=rSumIn+taSpecOSumIn.AsFloat;
        rSumUch:=rSumUch+taSpecOSumUch.AsFloat;
        taSpecO.Next;
      end;
      ViewAO.EndUpdate;

      ViewAI.BeginUpdate;
      rQs:=0; FullProc:=0;

      rSumRemn:=rSumIn; //��� ����� ����������, ��� �������� 0-��
      rSumRemn0:=rSumIn0; //��� ����� ����������, ��� �������� 0-�� ��� ���

      //2. ������ ��� ���-�� ������� � ������ ������� ����
      taSpecI.First;
      while not taSpecI.Eof do
      begin
        FullProc:=FullProc+taSpecIProcPrice.AsFloat;
        rQs:=rQs+taSpecIQuant.AsFloat*taSpecIKm.AsFloat;
        taSpecI.Next;
      end;

      if FullProc=0 then
      begin   //������������� �� ���-��
        taSpecI.First;
        while not taSpecI.Eof do
        begin
          rQ:=taSpecIQuant.AsFloat*taSpecIKm.AsFloat;
          taSpecI.Edit;
          if (rQs<>0)and(taSpecIKm.AsFloat<>0) then
          begin
            taSpecISumIn0.AsFloat:=rv(rQ/rQs*rSumIn0);
            taSpecISumIn.AsFloat:=rv(rQ/rQs*rSumIn);
            taSpecISumUch.AsFloat:=rv(rQ/rQs*rSumUch);

            taSpecIPriceIn0.AsFloat:=(rv(rQ/rQs*rSumIn0))/(rQ/taSpecIKm.AsFloat);
            taSpecIPriceIn.AsFloat:=(rv(rQ/rQs*rSumIn))/(rQ/taSpecIKm.AsFloat);
            taSpecIPriceUch.AsFloat:=(rv(rQ/rQs*rSumUch))/(rQ/taSpecIKm.AsFloat);

            rSumRemn:=rSumRemn-rv(rQ/rQs*rSumIn);
            rSumRemn0:=rSumRemn0-rv(rQ/rQs*rSumIn0);
          end else
          begin
            taSpecISumIn0.AsFloat:=0;
            taSpecISumIn.AsFloat:=0;
            taSpecISumUch.AsFloat:=0;
            taSpecIPriceIn0.AsFloat:=0;
            taSpecIPriceIn.AsFloat:=0;
            taSpecIPriceUch.AsFloat:=0;
          end;
          taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
          taSpecI.Post;

          taSpecI.Next;
        end;
      end else //������������� �� ��������
      begin
        taSpecI.First;
        while not taSpecI.Eof do
        begin
          rP:=taSpecIProcPrice.AsFloat;
          rQ:=taSpecIQuant.AsFloat*taSpecIKm.AsFloat;
          taSpecI.Edit;
          if (FullProc<>0)and(rQ<>0)and(taSpecIKm.AsFloat<>0)  then
          begin
            taSpecISumIn0.AsFloat:=rv(rP/FullProc*rSumIn0);
            taSpecISumIn.AsFloat:=rv(rP/FullProc*rSumIn);
            taSpecISumUch.AsFloat:=rv(rP/FullProc*rSumUch);
            taSpecIPriceIn0.AsFloat:=(rv(rP/FullProc*rSumIn0))/(rQ/taSpecIKm.AsFloat);
            taSpecIPriceIn.AsFloat:=(rv(rP/FullProc*rSumIn))/(rQ/taSpecIKm.AsFloat);
            taSpecIPriceUch.AsFloat:=(rv(rP/FullProc*rSumUch))/(rQ/taSpecIKm.AsFloat);

            rSumRemn:=rSumRemn-rv(rP/FullProc*rSumIn);
            rSumRemn0:=rSumRemn0-rv(rP/FullProc*rSumIn0);
          end else
          begin
            taSpecISumIn0.AsFloat:=0;
            taSpecISumIn.AsFloat:=0;
            taSpecISumUch.AsFloat:=0;
            taSpecIPriceIn.AsFloat:=0;
            taSpecIPriceUch.AsFloat:=0;
            taSpecIPriceIn0.AsFloat:=0;
          end;
          taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
          taSpecI.Post;

          taSpecI.Next;
        end;
      end;

      if (rSumRemn<>0)or(rSumRemn0<>0) then
      begin //�������� ���������������� �����-�� �����
        taSpecI.Last;
        while (taSpecI.Bof=False) do
        begin
          if taSpecIQuant.AsFloat<>0 then
          begin
            rSumRemn:=rSumRemn+taSpecISumIn.AsFloat;
            rSumRemn0:=rSumRemn0+taSpecISumIn0.AsFloat;
            rQ:=taSpecIQuant.AsFloat;
            taSpecI.Edit;
            taSpecISumIn.AsFloat:=rSumRemn;
            taSpecISumIn0.AsFloat:=rSumRemn0;
            taSpecIPriceIn.AsFloat:=rSumRemn/rQ;
            taSpecIPriceIn0.AsFloat:=rSumRemn0/rQ;
            taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
            taSpecI.Post;
            rSumRemn:=0;
            rSumRemn0:=0;
            break;
          end;
          taSpecI.Prior;
        end;
      end;

      if (rSumRemn<>0)or(rSumRemn0<>0) then ShowMessage('��������� ����� ������� �� �������, ��������������� ������ �� ������� �������.');
      ViewAI.EndUpdate;

    end;
  end;
  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
end;

procedure TfmAddAct.ViewAISMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpecI.Edit;
        taSpecIIM.AsInteger:=iM;
        taSpecIKm.AsFloat:=rK;
        taSpecISM.AsString:=Sm;
        taSpecI.Post;
      end;
    end;
  end;
end;

procedure TfmAddAct.ViewAOSMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecOIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpecO.Edit;
        taSpecOIM.AsInteger:=iM;
        taSpecOKm.AsFloat:=rK;
        taSpecOSM.AsString:=Sm;
        taSpecO.Post;
      end;
    end;
  end;
end;

procedure TfmAddAct.acExitExecute(Sender: TObject);
begin
  if cxButton2.Enabled then Close;
end;

procedure TfmAddAct.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddAct.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewAO);
end;

procedure TfmAddAct.MenuItem1Click(Sender: TObject);
begin
  prNExportExel5(ViewAI);
end;

procedure TfmAddAct.taSpecOQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOSumIn0.AsFloat:=taSpecOPriceIn0.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumIn.AsFloat:=taSpecOPriceIn.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumUch.AsFloat:=taSpecOPriceUch.AsFloat*taSpecOQuant.AsFloat;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecOPriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOSumIn.AsFloat:=taSpecOPriceIn.AsFloat*taSpecOQuant.AsFloat;
    taSpecOSumIn0.AsFloat:=rv(taSpecOSumIn.AsFloat*100/(100+vNds[taSpecOINds.AsInteger]));
    if taSpecOQuant.AsFloat<>0 then taSpecOPriceIn0.AsFloat:=taSpecOSumIn0.AsFloat/taSpecOQuant.AsFloat
    else taSpecOPriceIn0.AsFloat:=0;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecOSumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    taSpecOSumIn0.AsFloat:=rv(taSpecOSumIn.AsFloat*100/(100+vNds[taSpecOINds.AsInteger]));
    if abs(taSpecOQuant.AsFloat)>0 then
    begin
      taSpecOPriceIn.AsFloat:=taSpecOSumIn.AsFloat/taSpecOQuant.AsFloat;
      taSpecOPriceIn0.AsFloat:=taSpecOSumIn0.AsFloat/taSpecOQuant.AsFloat;
    end
    else
    begin
      taSpecOPriceIn.AsFloat:=0;
      taSpecOPriceIn0.AsFloat:=0;
    end;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecOPriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
    taSpecOSumUch.AsFloat:=taSpecOPriceUch.AsFloat*taSpecOQuant.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecOSumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� ����� �������
  begin
    if abs(taSpecOQuant.AsFloat)>0 then taSpecOPriceUch.AsFloat:=RoundEx(taSpecOSumUch.AsFloat/taSpecOQuant.AsFloat*100)/100;
  end;
end;

procedure TfmAddAct.taSpecOPriceIn0Change(Sender: TField);
begin
  if iCol=6 then  //���������� ���� ��� ���
  begin
    taSpecOSumIn0.AsFloat:=rv(taSpecOPriceIn0.AsFloat*taSpecOQuant.AsFloat);
    taSpecOSumIn.AsFloat:=rv(taSpecOSumIn0.AsFloat/100*(100+vNds[taSpecOINds.AsInteger]));
    if taSpecOQuant.AsFloat<>0 then taSpecOPriceIn.AsFloat:=taSpecOSumIn.AsFloat/taSpecOQuant.AsFloat
    else taSpecOPriceIn.AsFloat:=0;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecOSumIn0Change(Sender: TField);
begin
  if iCol=7 then  //���������� ����� ��� ���
  begin
    taSpecOSumIn.AsFloat:=rv(taSpecOSumIn0.AsFloat/100*(100+vNds[taSpecOINds.AsInteger]));
    if abs(taSpecOQuant.AsFloat)>0 then
    begin
      taSpecOPriceIn.AsFloat:=taSpecOSumIn.AsFloat/taSpecOQuant.AsFloat;
      taSpecOPriceIn0.AsFloat:=taSpecOSumIn0.AsFloat/taSpecOQuant.AsFloat;
    end
    else
    begin
      taSpecOPriceIn.AsFloat:=0;
      taSpecOPriceIn0.AsFloat:=0;
    end;
    taSpecORNds.AsFloat:=taSpecOSumIn.AsFloat-taSpecOSumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecIQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecISumIn0.AsFloat:=taSpecIPriceIn0.AsFloat*taSpecIQuant.AsFloat;
    taSpecISumIn.AsFloat:=taSpecIPriceIn.AsFloat*taSpecIQuant.AsFloat;
    taSpecISumUch.AsFloat:=taSpecIPriceUch.AsFloat*taSpecIQuant.AsFloat;
    taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecIPriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecISumIn.AsFloat:=taSpecIPriceIn.AsFloat*taSpecIQuant.AsFloat;
    taSpecISumIn0.AsFloat:=rv(taSpecISumIn.AsFloat*100/(100+vNds[taSpecIINds.AsInteger]));
    if taSpecIQuant.AsFloat<>0 then taSpecIPriceIn0.AsFloat:=taSpecISumIn0.AsFloat/taSpecIQuant.AsFloat
    else taSpecIPriceIn0.AsFloat:=0;
    taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecISumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    taSpecISumIn0.AsFloat:=rv(taSpecISumIn.AsFloat*100/(100+vNds[taSpecIINds.AsInteger]));
    if abs(taSpecIQuant.AsFloat)>0 then
    begin
      taSpecIPriceIn.AsFloat:=taSpecISumIn.AsFloat/taSpecIQuant.AsFloat;
      taSpecIPriceIn0.AsFloat:=taSpecISumIn0.AsFloat/taSpecIQuant.AsFloat;
    end
    else
    begin
      taSpecIPriceIn.AsFloat:=0;
      taSpecIPriceIn0.AsFloat:=0;
    end;
    taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecIPriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
    taSpecISumUch.AsFloat:=taSpecIPriceUch.AsFloat*taSpecIQuant.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecISumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� ����� �������
  begin
    if abs(taSpecIQuant.AsFloat)>0 then taSpecIPriceUch.AsFloat:=RoundEx(taSpecISumUch.AsFloat/taSpecIQuant.AsFloat*100)/100;
  end;
end;

procedure TfmAddAct.taSpecIPriceIn0Change(Sender: TField);
begin
  if iCol=6 then  //���������� ���� ��� ���
  begin
    taSpecISumIn0.AsFloat:=rv(taSpecIPriceIn0.AsFloat*taSpecIQuant.AsFloat);
    taSpecISumIn.AsFloat:=rv(taSpecISumIn0.AsFloat/100*(100+vNds[taSpecIINds.AsInteger]));
    if taSpecIQuant.AsFloat<>0 then taSpecIPriceIn.AsFloat:=taSpecISumIn.AsFloat/taSpecIQuant.AsFloat
    else taSpecIPriceIn.AsFloat:=0;
    taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
  end;
end;

procedure TfmAddAct.taSpecISumIn0Change(Sender: TField);
begin
  if iCol=7 then  //���������� ����� ��� ���
  begin
    taSpecISumIn.AsFloat:=rv(taSpecISumIn0.AsFloat/100*(100+vNds[taSpecIINds.AsInteger]));
    if abs(taSpecIQuant.AsFloat)>0 then
    begin
      taSpecIPriceIn.AsFloat:=taSpecISumIn.AsFloat/taSpecIQuant.AsFloat;
      taSpecIPriceIn0.AsFloat:=taSpecISumIn0.AsFloat/taSpecIQuant.AsFloat;
    end
    else
    begin
      taSpecIPriceIn.AsFloat:=0;
      taSpecIPriceIn0.AsFloat:=0;
    end;
    taSpecIRNds.AsFloat:=taSpecISumIn.AsFloat-taSpecISumIn0.AsFloat;
  end;
end;

end.
