program RnEdit;

uses
  Forms,
  MainRnEdit in 'MainRnEdit.pas' {fmMainRnEdit},
  dmRnEdit in 'dmRnEdit.pas' {dmC: TDataModule},
  Un1 in 'Un1.pas',
  MenuCr in 'MenuCr.pas' {fmMenuCr},
  ModifCr in 'ModifCr.pas' {fmModCr},
  Categories in 'Categories.pas' {fmCateg},
  AddCateg in 'AddCateg.pas' {fmAddCateg},
  AddM in 'AddM.pas' {fmAddM},
  PerA in 'PerA.pas' {fmPerA},
  messagecash in 'messagecash.pas' {fmMessage},
  MenuDayList in 'MenuDayList.pas' {fmMenuDayList},
  AddMDL in 'AddMDL.pas' {fmAddMDL},
  PeriodRn in 'PeriodRn.pas' {fmPeriod},
  MenuDay in 'MenuDay.pas' {fmMenuDay},
  AddMDG in 'AddMDG.pas' {fmMDG},
  FindResult1 in 'FindResult1.pas' {fmFind},
  DelPars in 'DelPars.pas' {fmDelPar},
  CategSale in 'CategSale.pas' {fmCategSale},
  AddMessage in 'AddMessage.pas' {fmAddMessage},
  AddBar in 'AddBar.pas' {fmAddBar},
  StopList in 'StopList.pas' {fmStopList},
  SetQuant in 'SetQuant.pas' {fmSetQuant};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainRnEdit, fmMainRnEdit);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmCateg, fmCateg);
  Application.CreateForm(TfmAddM, fmAddM);
  Application.CreateForm(TfmPerA, fmPerA);
  Application.CreateForm(TfmMessage, fmMessage);
  Application.CreateForm(TfmMenuDayList, fmMenuDayList);
  Application.CreateForm(TfmMenuDay, fmMenuDay);
  Application.CreateForm(TfmFind, fmFind);
  Application.CreateForm(TfmDelPar, fmDelPar);
  Application.CreateForm(TfmCategSale, fmCategSale);
  Application.CreateForm(TfmAddBar, fmAddBar);
  Application.CreateForm(TfmStopList, fmStopList);
  Application.CreateForm(TfmSetQuant, fmSetQuant);
  Application.Run;
end.
