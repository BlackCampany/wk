unit Manger;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxMemo, cxCheckBox, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan,
  ComCtrls, DB, pvtables, btvtables, dxmdaset, sqldataset, pvsqltables;

type
  TfmManager = class(TForm)
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    Timer1: TTimer;
    am1: TActionManager;
    acStart: TAction;
    StatusBar1: TStatusBar;
    teAC: TdxMemData;
    PvSQL: TPvSqlDatabase;
    quA: TPvQuery;
    quACode: TIntegerField;
    quAAType: TSmallintField;
    quADateB: TDateField;
    quADateE: TDateField;
    quAPrice: TFloatField;
    quADStop: TSmallintField;
    quADProc: TFloatField;
    quAStatusOn: TSmallintField;
    quAStatusOf: TSmallintField;
    teACCode: TIntegerField;
    teACQuant: TFloatField;
    teACAPrice: TFloatField;
    teACOldPrice: TFloatField;
    teACNewPrice: TFloatField;
    teACiDep: TIntegerField;
    quAOldPrice: TFloatField;
    quAOldDStop: TSmallintField;
    quAOldDProc: TFloatField;
    ptAkciya: TPvTable;
    ptAkciyaCode: TIntegerField;
    ptAkciyaAType: TSmallintField;
    ptAkciyaDateB: TDateField;
    ptAkciyaDateE: TDateField;
    ptAkciyaPrice: TFloatField;
    ptAkciyaDStop: TSmallintField;
    ptAkciyaDProc: TFloatField;
    ptAkciyaStatusOn: TSmallintField;
    ptAkciyaStatusOf: TSmallintField;
    ptAkciyaOldPrice: TFloatField;
    ptAkciyaOldDStop: TSmallintField;
    ptAkciyaOldDProc: TFloatField;
    quAdd: TPvQuery;
    quM: TPvQuery;
    quMiMax: TIntegerField;
    quE: TPvQuery;
    teAD: TdxMemData;
    teADCode: TIntegerField;
    teADDStopNew: TFloatField;
    teADDStopOld: TFloatField;
    teACat: TdxMemData;
    teACatCode: TIntegerField;
    teACatCatNew: TIntegerField;
    teACatCatOld: TIntegerField;
    teNac: TdxMemData;
    teNacCode: TIntegerField;
    teNacNewDMin: TFloatField;
    teNacNewNac: TFloatField;
    teNacNewDMax: TFloatField;
    teNacOldDMin: TFloatField;
    teNacOldNac: TFloatField;
    teNacOldDMax: TFloatField;
    teADDateB: TIntegerField;
    teACDateB: TIntegerField;
    teACatDateB: TIntegerField;
    teNacDateB: TIntegerField;
    quBars: TPvQuery;
    quBarsGoodsID: TIntegerField;
    quBarsID: TStringField;
    quBarsQuant: TFloatField;
    quBarsBarFormat: TSmallintField;
    quBarsDateChange: TDateField;
    quBarsBarStatus: TSmallintField;
    quBarsCost: TFloatField;
    quBarsStatus: TSmallintField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function prMax(sTab:String):INteger;
procedure prOffOldA(iDb,iCode,iType:Integer);

var
  fmManager: TfmManager;

implementation

uses Un1,u2fdk, MT, PXDB, MFB, uTermElisey;

{$R *.dfm}

procedure prOffOldA(iDb,iCode,iType:Integer);
Var iT:Integer;
begin
  with fmManager do
  begin
    if iType=4 then iT:=1 else iT:=4; //��� ������������� ���� ��� �������(4)
    quE.Active:=False;
    quE.SQL.Clear;
    quE.SQL.Add('Update "A_AKCIYA" Set ');
    quE.SQL.Add('StatusOn=1,StatusOf=1');
    quE.SQL.Add('where Code='+its(iCode)+ ' and DateB<='''+ds(iDB)+'''');
    quE.SQL.Add('and AType='+its(iT)); //�� ������ ��� ��������������� ������ �� �� ��� ��� �������� �������
    quE.SQL.Add('and ((StatusOf=0) or (StatusOn=0))');
    quE.ExecSQL;
    delay(10);
  end;
end;

function prMax(sTab:String):INteger;
begin
  Result:=1;
  with fmManager do
  begin
    quM.Close;
    quM.SQL.Clear;
    if sTab='CG' then quM.SQL.Add('select max(CARD) as iMax from "CardGoods"');
    if sTab='Group' then quM.SQL.Add('select max(ID) as iMax from "GdsGroup"');
    if sTab='SGroup' then quM.SQL.Add('select max(ID) as iMax from "SubGroup"');
    if sTab='DocsIn' then quM.SQL.Add('select max(ID) as iMax from "TTNIn"');
    if sTab='DocsInv' then quM.SQL.Add('select max(Inventry) as iMax from "InventryHead"');
    if sTab='Cli' then quM.SQL.Add('select top 1 Vendor as iMax from "RVendor" order by Vendor DESC');
    if sTab='ChP' then quM.SQL.Add('select top 1 Code as iMax from "RIndividual" order by Code DESC');
    if sTab='DocsOut' then quM.SQL.Add('select max(ID) as iMax from "TTNOut"');
    if sTab='DocsAc' then quM.SQL.Add('select max(ID) as iMax from "TTNOvr"');
    if sTab='DocsVn' then quM.SQL.Add('select max(ID) as iMax from "TTNSelf"');
    if sTab='Hist' then quM.SQL.Add('select max(ID) as iMax from "A_DOCHIST"');
    if sTab='Close' then quM.SQL.Add('select max(ID) as iMax from "A_CLOSEHIST"');
    if sTab='CT' then quM.SQL.Add('select max(CARD) as iMax from "CardPak"');
    quM.Open; quM.First;
    if quM.RecordCount>0 then Result:=quMiMax.AsInteger;
    quM.Close;
  end;
end;


procedure TfmManager.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date;
  Timer1.Enabled:=True;
  ReadIni;
end;

procedure TfmManager.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStart.Execute;
    Close;
  end;
end;

procedure TfmManager.acStartExecute(Sender: TObject);
Var iCurD,iD,IdH,N,NumPost:Integer;
    sNum:String;
//    bOk:Boolean;
    StrWk,sCards:String;
    sPath,sCashLdd:String;
    rDiscOld,rDProc,rPr:Real;
    iCatOld:INteger;
    DateB:TDateTime;
    rNacOld,rDMinOld,rDMaxOld:Real;
    iTop:INteger;
    sMessur,sName,sBar,sCSz:String;
begin
  //�������
  with dmMt do
  begin
    try
      iCurD:=Trunc(cxDateEdit1.Date);
      CloseTe(teAC);
      CloseTe(teAD);
      CloseTe(teACat);
      CloseTe(teNac);
      if ptAkciya.Active=False then ptAkciya.Active:=True;
      if taCards.Active=False then taCards.Active:=True;

      prWH('������ - ��������� IP.',Memo1);
      CommonSet.Ip:=prGetIP;
      prWH('   '+CommonSet.Ip,Memo1);
      prWH('',Memo1);

      prWH('������ - ��������� ����.',Memo1);


      PvSQL.Connected:=False;
      PvSQL.Connected:=True;
      if PvSQL.Connected=False then prWH('�� ������� ������������ � ���� ������.',Memo1)
      else
      begin
        prWH('���� �������. �������� ��������� ������.',Memo1);
        delay(10);
        quA.Active:=False;
        quA.Active:=True;
        quA.First;
        while not quA.Eof do
        begin
          if quAAType.AsInteger=1 then //��� ���������� �� ������������� ����
          begin
            if ((Trunc(quADateB.AsDateTime)<=iCurD)and(quAStatusOn.AsInteger=0)) or ((Trunc(quADateE.AsDateTime)<=iCurD)and(quAStatusOf.AsInteger=0)) then
            begin
              //���� ��������
              iD:=prFindTLastDep(quACode.AsInteger);
              if Id>0 then
              begin
                teAC.Append;
                teACCode.AsInteger:=quACode.AsInteger;
//              teACQuant.AsFloat:=prFindTRemnDep(quACode.AsInteger,iD);
                teACQuant.AsFloat:=0;
                teACiDep.AsInteger:=iD;
                teACAPrice.AsFloat:=rv(quAPrice.AsFloat);
                if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
                begin
                  teACOldPrice.AsFloat:=rv(prFindPrice(quACode.AsInteger));
                  teACNewPrice.AsFloat:=quAPrice.AsFloat;
                  teACDateB.AsInteger:=Trunc(quADateB.AsDateTime);
                end;
                if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
                begin
                  //����� ����� ��������� ������ �� ������ �����
                  rPr:=prFindPriceDateMC(quACode.AsInteger,Trunc(quADateB.AsDateTime));
                  if rPr<=0.01 then rPr:=quAOldPrice.AsFloat;
                  if rPr<=0.01 then rPr:=quAPrice.AsFloat;
//                if rPr<=0.01 then rPr:= 

                  teACOldPrice.AsFloat:=quAPrice.AsFloat;
                  teACNewPrice.AsFloat:=rPr;
                  teACDateB.AsInteger:=0;
                end;
                teAC.Post;
              end;
              //����� ����� � ��������

              taCards.Refresh;
              if taCards.FindKey([quACode.AsInteger]) then
              begin
                rPr:=100000000;
                if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
                begin
                  prWH('  ��� '+its(quACode.AsInteger)+' ����. ���� '+fs(quAPrice.AsFloat),Memo1);
                  prWH('  ��� '+its(quACode.AsInteger)+' ������� 0 ',Memo1);
                  //������ �������� ����� �� ������������� ���� -
                  //����� ����� ��� ����� ����������� �� ������ ������ �� ������� � ������������� ���� �� ������ ����
                  rPr:=quAPrice.AsFloat;
                  prOffOldA(iCurD,quACode.AsInteger,quAAType.AsInteger);
                end;
                if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
                begin
                  //��������� �������� � ����� �� �����
                  rPr:=prFindPriceDateMC(quACode.AsInteger,Trunc(quADateB.AsDateTime));
                  if rPr<=0.01 then rPr:=quAOldPrice.AsFloat;
                  if rPr<=0.01 then rPr:=quAPrice.AsFloat;
                end;
                prWH('  ��� '+its(quACode.AsInteger)+' ����� ���� '+fs(rv(rPr)),Memo1);

                if CommonSet.Test=0 then
                begin
                  taCards.Edit;
                  if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
                  begin
                    if taCardsV02.AsInteger<>4 then taCardsCena.AsFloat:=quAPrice.AsFloat;
                    taCardsFixPrice.AsFloat:=quAPrice.AsFloat;
                    taCardsReserv2.AsFloat:=0;
                    taCardsReserv3.AsFloat:=0;
                    taCardsReserv4.AsFloat:=0;
                  end;
                  if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
                  begin
                    taCardsCena.AsFloat:=rPr;
                    taCardsFixPrice.AsFloat:=0;
                  end;
                  taCards.Post;
                end;

              end;
            end;
          end;
          if quAAType.AsInteger=2 then // ���� ������
          begin
            if ((Trunc(quADateB.AsDateTime)<=iCurD)and(quAStatusOn.AsInteger=0)) or ((Trunc(quADateE.AsDateTime)<=iCurD)and(quAStatusOf.AsInteger=0)) then
            begin
              if ptDStop.Active=False then ptDStop.Active:=True;
              rDiscOld:=0;
              if ptDStop.FindKey([quACode.AsInteger]) then rDiscOld:=ptDStopPercent.AsFloat;


              rDproc:=rv(quADStop.AsInteger); if rDProc>0 then rDProc:=0 else rDProc:=100; //1 - ��������� 0- �� ���������

              teAD.Append;
              teADCode.AsInteger:=quACode.AsInteger;
              if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
              begin
                teADDStopNew.AsFloat:=rDProc;
                teADDStopOld.AsFloat:=rDiscOld;
                teADDateB.AsInteger:=Trunc(quADateB.AsDateTime);
              end;
              if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
              begin
                teADDStopNew.AsFloat:=rv(quAOldDStop.AsFloat);
                teADDStopOld.AsFloat:=rDProc;
                teADDateB.AsInteger:=0;
              end;
              teAD.Post;
            end;
          end;
          if quAAType.AsInteger=3 then // ���������
          begin
            if ((Trunc(quADateB.AsDateTime)<=iCurD)and(quAStatusOn.AsInteger=0)) or ((Trunc(quADateE.AsDateTime)<=iCurD)and(quAStatusOf.AsInteger=0)) then
            begin
              if taCards.Active=False then taCards.Active:=True;
              iCatOld:=1;

              if taCards.FindKey([quACode.AsInteger]) then iCatOld:=taCardsV02.AsINteger;

              teACat.Append;
              teACatCode.AsInteger:=quACode.AsInteger;
              if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
              begin
                teACatCatNew.AsInteger:=RoundEx(quADStop.AsInteger);
                teACatCatOld.AsInteger:=iCatOld;
                teACatDateB.AsInteger:=Trunc(quADateB.AsDateTime);
              end;
              if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
              begin
                teACatCatNew.AsFloat:=RoundEx(quAOldDStop.AsFloat);
                teACatCatOld.AsFloat:=RoundEx(quADStop.AsInteger);
                teACatDateB.AsInteger:=0;
              end;
              teACat.Post;

              if taCards.FindKey([quACode.AsInteger]) then //���� ������� ��������
              begin
                if CommonSet.Test=0 then
                begin
                  taCards.Edit;
                  if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
                        taCardsReserv5.AsFloat:=quAPrice.AsFloat;
                  if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
                        taCardsReserv5.AsFloat:=0;
                  taCards.Post;
                end;
              end;
            end;
          end;
          if quAAType.AsInteger=4 then //������� �� �����
          begin
            if ((Trunc(quADateB.AsDateTime)<=iCurD)and(quAStatusOn.AsInteger=0)) or ((Trunc(quADateE.AsDateTime)<=iCurD)and(quAStatusOf.AsInteger=0)) then
            begin
              if taCards.Active=False then taCards.Active:=True;

              rNacOld:=0;
              rDMinOld:=0;
              rDMaxOld:=0;

              if taCards.FindKey([quACode.AsInteger]) then
              begin
                rNacOld:=taCardsReserv2.AsFloat;
                rDMinOld:=taCardsReserv3.AsFloat;
                rDMaxOld:=taCardsReserv4.AsFloat;
              end;

              teNac.Append;
              teNacCode.AsInteger:=quACode.AsInteger;
              if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
              begin
                teNacNewDMin.AsFloat:=quAPrice.AsFloat;
                teNacNewNac.AsFloat:=quADStop.AsInteger/100;
                teNacNewDMax.AsFloat:=quADProc.AsFloat;
                teNacOldDMin.AsFloat:=rDMinOld;
                teNacOldNac.AsFloat:=rNacOld;
                teNacOldDMax.AsFloat:=rDMaxOld;
                teNacDateB.AsInteger:=Trunc(quADateB.AsDateTime);

                if taCards.FindKey([quACode.AsInteger]) then
                begin
                  prWH('  ��� '+its(quACode.AsInteger)+' ����� �� '+fs(quADStop.AsInteger/100)+' ������ '+fs(taCardsReserv2.AsFloat),Memo1);
                  prWH('  ��� '+its(quACode.AsInteger)+' ����.���� 0',Memo1);
                  if CommonSet.Test=0 then
                  begin
                    taCards.Edit;
                    taCardsFixPrice.AsFloat:=0;
                    taCardsReserv2.AsFloat:=quADStop.AsInteger/100;
                    taCardsReserv3.AsFloat:=quAPrice.AsFloat;
                    taCardsReserv4.AsFloat:=quADProc.AsFloat;
                    taCards.Post;
                  end;
                end;
              end;
              if Trunc(quADateE.AsDateTime)<=iCurD then //����� �������� �����
              begin
                teNacNewDMin.AsFloat:=quAOldPrice.AsFloat;
                teNacNewNac.AsFloat:=quAOldDStop.AsInteger/100;
                teNacNewDMax.AsFloat:=quAOldDProc.AsFloat;
                teNacOldNac.AsFloat:=quADStop.AsInteger/100;
                teNacOldDMin.AsFloat:=quAPrice.AsFloat;;
                teNacOldDMax.AsFloat:=quADProc.AsFloat;
                teNacDateB.AsInteger:=0;

                if taCards.FindKey([quACode.AsInteger]) then
                begin
                  prWH('  ��� '+its(quACode.AsInteger)+' ����� �� '+fs(quADStop.AsInteger/100)+' ������ '+fs(taCardsReserv2.AsFloat),Memo1);
                  if CommonSet.Test=0 then
                  begin
                    taCards.Edit;
                    taCardsReserv2.AsFloat:=0;
                    taCardsReserv3.AsFloat:=0;
                    taCardsReserv4.AsFloat:=0;
                    taCards.Post;
                  end;
                end;
              end;
              teNac.Post;

              if Trunc(quADateB.AsDateTime)<=iCurD then //������ �������� �����
              begin
                //������ �������� ����� �� ������������� ���� -
                //����� ����� ��� ����� ����������� �� ������ ������ �� ������� � ������������� ���� �� ������ ����

                prOffOldA(iCurD,quACode.AsInteger,quAAType.AsInteger);

              end;
            end;
          end;
          quA.Next;
        end;

        if teAD.RecordCount>0 then
        begin
          prWH('������� ���� ���� ������.',Memo1);

          teAD.First;
          while not teAD.Eof do
          begin
            //������� �������� � ���������� ����� �����
            if ptDStop.Active=False then ptDStop.Active:=True;

            if ptDStop.FindKey([teADCode.AsInteger]) then
            begin
              prWH('  ��� '+its(teADCode.AsInteger)+' ����� '+fs(rv(teADDStopNew.AsFloat))+' ������ '+fs(ptDStopPercent.AsFloat),Memo1);

              if CommonSet.Test=0 then
              begin
                ptDStop.Edit;
                ptDStopPercent.AsFloat:=rv(teADDStopNew.AsFloat);
                ptDStop.Post;
              end;
            end else
            begin
              prWH('  ��� '+its(teADCode.AsInteger)+' ��������� '+fs(rv(teADDStopNew.AsFloat)),Memo1);

              if CommonSet.Test=0 then
              begin
                ptDStop.Append;
                ptDStopCode.AsInteger:=teADCode.AsInteger;
                ptDStopPercent.AsFloat:=rv(teADDStopNew.AsFloat);
                ptDStopStatus.AsInteger:=1;
                ptDStopStatus_1.AsInteger:=0;
                ptDStop.Post;
              end;
            end;

            teAD.Next;
          end;
          prWH('��.',Memo1);
          prWH('������� ��������������� �����.',Memo1);
          teAD.First; N:=0;
          while not teAD.Eof do
          begin
            DateB:=teADDateB.AsInteger;
            if teADDateB.AsInteger>0 then
            begin
              if ptAkciya.FindKey([teACCode.AsInteger,2,DateB]) then  //������
              begin
                ptAkciya.Edit;
                ptAkciyaOldDStop.AsFloat:=teADDStopOld.AsFloat;  //���������� ������ �������� ���� ��� ��������� ��������������
                ptAkciya.Post;
                inc(N);
              end;
            end;
            teAD.Next;
          end;
          prWH('��. ('+its(N)+')',Memo1);

          // �������� �������� ����
          try
//     if fmSt.cxButton1.Enabled=False then exit;
            if CommonSet.Test=0 then
            begin

            prWH('���� �������� ����..',Memo1);
            dmPx.Session1.Active:=False;
            dmPx.Session1.NetFileDir:=CommonSet.NetPath;
            dmPx.Session1.Active:=True;

            //������ �������� �� �������� �������
            //������ �������� �� �������� �������
            StrWk:=FloatToStr(now);
            Delete(StrWk,1,2);
            Delete(StrWk,pos(',',StrWk),1);
            StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

            NumPost:=StrToINtDef(StrWk,0);
            sCards:='_'+IntToHex(NumPost,7);
            sBar:='_'+IntToHex(NumPost+1,7);

            //����� �������� ������
            CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
            CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
            CopyFile(PChar(CommonSet.TrfPath+'DStop.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
            CopyFile(PChar(CommonSet.TrfPath+'DStop.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

            try
              with dmPx do
              begin
                taDStop.Active:=False;
                taDStop.DatabaseName:=CommonSet.TmpPath;
                taDStop.TableName:=sBar+'.db';
                taDStop.Active:=True;

                taCard.Active:=False;
                taCard.DatabaseName:=CommonSet.TmpPath;
                taCard.TableName:=sCards+'.db';
                taCard.Active:=True;

                teAD.First;
                while not teAD.Eof do
                begin
                  prWH('  d '+teADCode.AsString+' '+fs(teADDStopNew.AsFloat),Memo1);

                  prAddCashLoad(teADCode.AsInteger,2,0,teADDStopNew.AsFloat);
                  prAddPosFB(18,teADCode.AsInteger,0,0,0,0,1,'','','','','',0,teADDStopNew.AsFloat,0,0);

                  taDStop.Append;
                  taDStopCardArticul.AsString:=teADCode.AsString;
                  taDStopPercent.AsFloat:=teADDStopNew.AsFloat;
                  taDStop.Post;

//                  prAddCashLoad(teADCode.AsInteger,2,0,teADDStopNew.AsFloat);

                  //����� ��� �������� ������� ���� � �������� ���� ���
                  if taCards.FindKey([teADCode.AsInteger]) then
                  begin
                    sName:=OemToAnsiConvert(prRetDName(taCardsID.AsInteger)+taCardsName.AsString);

                    if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';

                    taCard.Append;
                    taCardArticul.AsString:=taCardsID.AsString;
                    taCardName.AsString:=AnsiToOemConvert(sName);
                    taCardMesuriment.AsString:=AnsiToOemConvert(sMessur);
                    taCardAdd1.AsString:='';
                    taCardAdd2.AsString:='';
                    taCardAdd3.AsString:='';
                    taCardAddNum1.AsFloat:=0;
                    taCardAddNum2.AsFloat:=0;
                    taCardAddNum3.AsFloat:=0;
                    taCardScale.AsString:='NOSIZE';
                    taCardGroop1.AsInteger:=taCardsGoodsGroupID.AsInteger;
                    taCardGroop2.AsInteger:=taCardsSubGroupID.AsInteger;
                    taCardGroop3.AsInteger:=0;
                    taCardGroop4.AsInteger:=0;
                    taCardGroop5.AsInteger:=0;
                    taCardMesPresision.AsFloat:=RoundEx(taCardsPrsision.AsFloat*1000)/1000;
                    taCardPriceRub.AsFloat:=rv(taCardsCena.AsFloat);
                    taCardPriceCur.AsFloat:=0;
                    taCardClientIndex.AsInteger:=0;
                    taCardCommentary.AsString:='';
                    taCardDeleted.AsInteger:=1;
                    taCardModDate.AsDateTime:=Trunc(Date);
                    taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
                    taCardModPersonIndex.AsInteger:=0;
                    taCard.Post;

                    prAddPosFB(1,taCardsID.AsInteger,0,0,taCardsGoodsGroupID.AsInteger,0,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(taCardsCena.AsFloat));

                    prWH('  d '+taCardsID.AsString+' '+sName,Memo1);
                  end;

                  teAD.Next;
                end;
                taDStop.Active:=False;
                taCard.Active:=False;
              end;
            except
              prWH('������ ������������ ������.',Memo1);
              prWH('  �������� ����������!!!',Memo1);
            end;

            if ptCashs.Active=False then ptCashs.Active:=True;
            ptCashs.First;
            while not ptCashs.Eof do
            begin
              if ptCashsLoad.AsInteger=1 then
              begin
                prWH(' '+OemToAnsiConvert(ptCashsName.AsString),Memo1);
                if CommonSet.CashPath[length(CommonSet.CashPath)]<>'\' then CommonSet.CashPath:=CommonSet.CashPath+'\';
                sPath:=CommonSet.CashPath+ptCashsNumber.AsString+'\';

                StrWk:=ptCashsNumber.AsString;
                while length(StrWk)<2 do strwk:='0'+StrWk;
                StrWk:='cash'+StrWk+'.ldd';
                sCashLdd:=StrWk;

                if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
                begin
                  prWH('',Memo1);
                  prWH('  ����� ������������ ������.',Memo1);
                  prWH('  �������� ����������!!!',Memo1);
                  prWH('  ���������� �����...',Memo1);
                  prWH('',Memo1);
                end else
                begin //��� ����� ���������� �������� �����
                  //���� �����������
                  try
                    sNum:=ptCashsNumber.AsString+'\';

                   //���������

                    CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                    Delay(50);

                    CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                    StrWk:=ptCashsNumber.AsString;
                    while length(StrWk)<3 do strwk:='0'+StrWk;
                    StrWk:='cash'+StrWk+'.db';

                    if Fileexists(CommonSet.CashPath+StrWk)=False then
                    begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                      CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                      MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                    end;

                    with dmPx do
                    begin
                      taList.Active:=False;
                      taList.DatabaseName:=CommonSet.CashPath+sNum;
                      taList.TableName:=StrWk;
                      taList.Active:=True;

                      taList.Append;
                      taListTName.AsString:=sBar;
                      taListDataType.AsInteger:=18;   //������ ������ ����������� ������ �� ������
                      taListOperation.AsInteger:=1;
                      taList.Post;

                      taList.Append;
                      taListTName.AsString:=sCards;
                      taListDataType.AsInteger:=1;   //�������� �� ������� ������ �� ���������������
                      taListOperation.AsInteger:=1;
                      taList.Post;

                      taList.Active:=False;
                    end;

                    if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                    delay(50);
                    if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
                  except
                    prWH('  ������ ������ � �������..',Memo1);
//                    bOk:=False;
                  end;
                end;

                prWH(' ��',Memo1);
              end;

              ptCashs.Next;
            end;

           //������ �����

            if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
            if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
            if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
            if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

            end;
          finally
            ptCashs.Active:=False;
            dmPx.Session1.Active:=False;
          end;

        end;

        if teACat.RecordCount>0 then
        begin
          prWH('������� ��������� ������ � ���������.',Memo1);
          teACat.First;
          while not teACat.Eof do
          begin
            if taCards.Active=False then taCards.Active:=True;
            if taCards.FindKey([teACatCode.AsInteger]) then
            begin
              if CommonSet.Test=0 then
              begin
                taCards.Edit;
                if (abs(teACatCatNew.AsInteger)=3)or(abs(teACatCatOld.AsFloat)=3) then
                begin
                  if teACatCatNew.AsInteger=3 then iTop:=1
                  else iTop:=0; //-3 ����

                  prWH('  ��� '+its(teACatCode.AsInteger)+' ����� ������ ��� '+its(iTop)+' ������ '+its(taCardsV04.AsInteger),Memo1);

                  taCardsV04.AsInteger:=iTop;

                end else
                begin
                  prWH('  ��� '+its(teACatCode.AsInteger)+' ����� '+its(teACatCatNew.AsINteger)+' ������ '+its(taCardsV02.AsInteger),Memo1);
                  taCardsV02.AsInteger:=teACatCatNew.AsInteger;
                end;
                taCards.Post;
              end;
            end;

            teACat.Next;
          end;
          prWH('��.',Memo1);
          prWH('������� ��������������� �����.',Memo1);
          teACat.First; N:=0;
          while not teACat.Eof do
          begin
            DateB:=teACatDateB.AsInteger;
            if teACatDateB.AsInteger>0 then
            begin
              if ptAkciya.FindKey([teACatCode.AsInteger,3,DateB]) then  //������
              begin
                ptAkciya.Edit;
                ptAkciyaOldDStop.AsFloat:=teACatCatOld.AsINteger; //���������� ������ �������� ��������� ������
                ptAkciya.Post;
                inc(N);
              end;
            end;

            teACat.Next;
          end;
          prWH('��. ('+its(N)+')',Memo1);
        end;

        if teNac.RecordCount>0 then
        begin
          prWH('������� ������� �� ����� � ���������.',Memo1);
          teNac.First;
          while not teNac.Eof do
          begin
{
            if taCards.Active=False then taCards.Active:=True;
            if taCards.FindKey([teNacCode.AsInteger]) then
            begin
              prWH('  ��� '+its(teNacCode.AsInteger)+' ����� '+fs(teNacNewNac.AsFloat)+' ������ '+fs(teNacOldNac.AsFloat),Memo1);
              taCards.Edit;
              taCardsFixPrice.AsFloat:=0;
              taCardsReserv2.AsFloat:=teNacNewNac.AsFloat;
              taCardsReserv3.AsFloat:=teNacNewDMin.AsFloat;
              taCardsReserv4.AsFloat:=teNacNewDMax.AsFloat;
              taCards.Post;
            end;
}
            teNac.Next;
          end;
          prWH('��.',Memo1);
          prWH('������� ��������������� �����.',Memo1);
          teNac.First; N:=0;
          while not teNac.Eof do
          begin
            DateB:=teNacDateB.AsInteger;
            if teNacDateB.AsInteger>0 then
            begin
              if ptAkciya.FindKey([teNacCode.AsInteger,4,DateB]) then  //������
              begin
                ptAkciya.Edit;
                ptAkciyaOldPrice.AsFloat:=teNacOldDMin.AsFloat;
                ptAkciyaOldDStop.AsInteger:=RoundEx(teNacOldNac.AsFloat*100);
                ptAkciyaOldDProc.AsFloat:=teNacOldDMax.AsFloat;
                ptAkciya.Post;
                inc(N);
              end;
            end;

            teNac.Next;
          end;
          prWH('��. ('+its(N)+')',Memo1);
        end;


        if teAc.RecordCount>0 then //������� ������ ��� �� ����� ���� ���� ��� �������
        begin
//���������� �� ����� ������
          with dmPx do
          begin
            prWH('���� �������� ����..',Memo1);

//            bOk:=True;
            if CommonSet.Test=0 then
            begin

            if dmFB.Cash.Connected then prWH('  FB ����� ����.',Memo1) else prWH('  ������ PX.',Memo1);

            dmPx.Session1.Active:=False;
            dmPx.Session1.NetFileDir:=CommonSet.NetPath;
            dmPx.Session1.Active:=True;

            //������ �������� �� �������� �������
            StrWk:=FloatToStr(now);
            Delete(StrWk,1,2);
            Delete(StrWk,pos(',',StrWk),1);
            StrWk:=Copy(StrWk,1,8); //999 ���� � �� ������

            NumPost:=StrToINtDef(StrWk,0);
            sCards:='_'+IntToHex(NumPost,7);
            sBar:='_'+IntToHex(NumPost+1,7);

            //����� �������� ������
            CopyFile(PChar(CommonSet.TrfPath+'Cards.db'),PChar(CommonSet.TmpPath+sCards+'.db'),False);
            CopyFile(PChar(CommonSet.TrfPath+'Cards.px'),PChar(CommonSet.TmpPath+sCards+'.px'),False);
            CopyFile(PChar(CommonSet.TrfPath+'Bars.db'),PChar(CommonSet.TmpPath+sBar+'.db'),False);
            CopyFile(PChar(CommonSet.TrfPath+'Bars.px'),PChar(CommonSet.TmpPath+sBar+'.px'),False);

            try
              taCard.Active:=False;
              taCard.DatabaseName:=CommonSet.TmpPath;
              taCard.TableName:=sCards+'.db';
              taCard.Active:=True;

              taBar.Active:=False;
              taBar.DatabaseName:=CommonSet.TmpPath;
              taBar.TableName:=sBar+'.db';
              taBar.Active:=True;

              teAC.First;
              while not teAc.Eof do
              begin
                if taCards.FindKey([teACCode.AsInteger]) then
                begin
                  if taCardsV02.AsInteger<>4 then
                  begin

                    prAddCashLoad(taCardsID.AsInteger,1,rv(teACNewPrice.AsFloat),0);
                    if taCardsEdIzm.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                    sName:=prRetDName(taCardsID.AsInteger)+OemToAnsiConvert(taCardsFullName.AsString);
                    prAddPosFB(1,taCardsID.AsInteger,0,0,taCardsGoodsGroupID.AsInteger,teACiDep.AsInteger,1,'','',sName,sMessur,'',0,0,RoundEx(taCardsPrsision.AsFloat*1000)/1000,rv(teACNewPrice.AsFloat));

                    prWH('  c '+taCardsID.AsString+' '+sName+' '+fs(rv(teACNewPrice.AsFloat)),Memo1);

                    taCard.Append;
                    taCardArticul.AsString:=taCardsID.AsString;
                    taCardName.AsString:=taCardsFullName.AsString;
                    if taCardsEdIzm.AsInteger=1 then
                    begin
                      taCardMesuriment.AsString:=AnsiToOemConvert('��.');
                    end
                    else begin
                      taCardMesuriment.AsString:=AnsiToOemConvert('��.');
                    end;
                    taCardMesPresision.AsFloat:=RoundEx(taCardsPrsision.AsFloat*1000)/1000;
                    taCardAdd1.AsString:='';
                    taCardAdd2.AsString:='';
                    taCardAdd3.AsString:='';
                    taCardAddNum1.AsFloat:=0;
                    taCardAddNum2.AsFloat:=0;
                    taCardAddNum3.AsFloat:=0;
                    taCardScale.AsString:='NOSIZE';
                    taCardGroop1.AsInteger:=taCardsGoodsGroupID.AsInteger;
                    taCardGroop2.AsInteger:=taCardsSubGroupID.AsInteger;
                    taCardGroop3.AsInteger:=0;
                    taCardGroop4.AsInteger:=0;
                    taCardGroop5.AsInteger:=0;
                    taCardPriceRub.AsFloat:=rv(teACNewPrice.AsFloat);
                    taCardPriceCur.AsFloat:=0;
                    taCardClientIndex.AsInteger:=teACiDep.AsInteger;
                    taCardCommentary.AsString:='';
                    taCardDeleted.AsInteger:=1;
                    taCardModDate.AsDateTime:=Trunc(Date);
                    taCardModTime.AsInteger:=StrToInt(FormatDateTime('hhmm',time));
                    taCardModPersonIndex.AsInteger:=0;
                    taCard.Post;

                    quBars.Active:=False;
                    quBars.ParamByName('GOODSID').AsInteger:=taCardsID.AsInteger;
                    quBars.Active:=True;

                    quBars.First;
                    while not quBars.Eof do
                    begin
                      if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                      else sCSz:='QUANTITY';

                      prAddPosFB(2,taCardsID.AsInteger,0,0,0,0,1,quBarsID.AsString,sCSz,'','','',quBarsQuant.AsFloat,0,0,rv(teACNewPrice.AsFloat));

                      if taBar.Locate('BarCode',quBarsID.AsString,[])=False then
                      begin
                        taBar.Append;
                        taBarBarCode.AsString:=quBarsID.AsString;
                        taBarCardArticul.AsString:=quBarsGoodsID.AsString;
                        taBarCardSize.AsString:=sCSz;
                        taBarQuantity.AsFloat:=quBarsQuant.AsFloat;
                        taBar.Post;
                      end;

                      quBars.Next;
                    end;
                    quBars.Active:=False;

                  end; //taCardsV02.AsInteger<>4 
                end;
                teAc.Next;
              end;
              taCard.Active:=False;
              taBar.Active:=False;
            except
              prWH('������ ������������ ������.',Memo1);
              prWH('  �������� ����������!!!',Memo1);
//              bOk:=False;
            end;

            if ptCashs.Active=False then ptCashs.Active:=True;
            ptCashs.First;
            while not ptCashs.Eof do
            begin
              if ptCashsLoad.AsInteger=1 then
              begin
                prWH(' '+OemToAnsiConvert(ptCashsName.AsString),Memo1);
                if CommonSet.CashPath[length(CommonSet.CashPath)]<>'\' then CommonSet.CashPath:=CommonSet.CashPath+'\';
                sPath:=CommonSet.CashPath+ptCashsNumber.AsString+'\';

                StrWk:=ptCashsNumber.AsString;
                while length(StrWk)<2 do strwk:='0'+StrWk;
                StrWk:='cash'+StrWk+'.ldd';
                sCashLdd:=StrWk;

                if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
                begin
                  prWH('',Memo1);
                  prWH('  ����� ������������ ������.',Memo1);
                  prWH('  �������� ����������!!!',Memo1);
                  prWH('  ���������� �����...',Memo1);
                  prWH('',Memo1);
                end else
                begin //��� ����� ���������� �������� �����
                  //���� �����������
                  try
                    sNum:=ptCashsNumber.AsString+'\';

                   //���������

                    CopyFile(PChar(CommonSet.TrfPath+CashNon),PChar(CommonSet.CashPath+sNum+CashNon),False);
                    Delay(50);

                    CopyFile(PChar(CommonSet.TmpPath+sCards+'.db'),PChar(CommonSet.CashPath+sNum+sCards+'.db'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sCards+'.px'),PChar(CommonSet.CashPath+sNum+sCards+'.px'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sBar+'.db'),PChar(CommonSet.CashPath+sNum+sBar+'.db'),False);
                    CopyFile(PChar(CommonSet.TmpPath+sBar+'.px'),PChar(CommonSet.CashPath+sNum+sBar+'.px'),False);

                    StrWk:=ptCashsNumber.AsString;
                    while length(StrWk)<3 do strwk:='0'+StrWk;
                    StrWk:='cash'+StrWk+'.db';

                    if Fileexists(CommonSet.CashPath+StrWk)=False then
                    begin //���� cashxxx.db ��� - ������� ����� � ������ ���� ����� �����
                      CopyFile(PChar(CommonSet.TrfPath+'Cash.db'),PChar(CommonSet.TmpPath+StrWk),False); delay(10);
                      MoveFile(PChar(CommonSet.TmpPath+StrWk),PChar(CommonSet.CashPath+sNum+StrWk));
                    end;

                    taList.Active:=False;
                    taList.DatabaseName:=CommonSet.CashPath+sNum;
                    taList.TableName:=StrWk;
                    taList.Active:=True;

                    taList.Append;
                    taListTName.AsString:=sCards;
                    taListDataType.AsInteger:=1;
                    taListOperation.AsInteger:=1;
                    taList.Post;

                    taList.Append;
                    taListTName.AsString:=sBar;
                    taListDataType.AsInteger:=2;
                    taListOperation.AsInteger:=1;
                    taList.Post;

                    taList.Active:=False;

                    if FileExists(CommonSet.TmpPath+StrWk) then DeleteFile(CommonSet.TmpPath+StrWk);
                    delay(50);
                    if FileExists(CommonSet.CashPath+sNum+CashNon) then DeleteFile(CommonSet.CashPath+sNum+CashNon);
                  except
                    prWH('  ������ ������ � �������..',Memo1);
//                    bOk:=False;
                  end;
                end;

                prWH(' ��',Memo1);
              end else
              begin
//             prWH(' '+ptCashsName.AsString+' �� �������.',Memo1);
              end;

              ptCashs.Next;
            end;

           //������ �����

            if FileExists(CommonSet.TmpPath+sCards+'.db') then DeleteFile(CommonSet.TmpPath+sCards+'.db');
            if FileExists(CommonSet.TmpPath+sCards+'.px') then DeleteFile(CommonSet.TmpPath+sCards+'.px');
            if FileExists(CommonSet.TmpPath+sBar+'.db') then DeleteFile(CommonSet.TmpPath+sBar+'.db');
            if FileExists(CommonSet.TmpPath+sBar+'.px') then DeleteFile(CommonSet.TmpPath+sBar+'.px');

            end;
          end; //with dmPx
        end;

        while teAc.RecordCount>0 do  //
        begin
          prWH('  ������������ ��������� ����.',Memo1);


          teAc.First;

          iD:=teACiDep.AsInteger;
          sNum:=its(fShop);
          if length(sNum)<2 then sNum:='0'+sNum;
          sNum:='au'+FormatDateTime('ddhhnn',now)+sNum;
          IDH:=prMax('DocsAc')+1;

          prWH(' ',Memo1);
          prWH('    ������ ��� ���������� - '+sNum,Memo1);

          quAdd.Active:=False;
          quAdd.SQL.Clear;
          quAdd.SQL.Add('INSERT into "TTNOvr" values (');

          quAdd.SQL.Add(its(iD)+','''+ds(cxDateEdit1.Date)+''','''+sNum+''','); // Depart DateAct   Number
          quAdd.SQL.Add('0,0,0,');
          quAdd.SQL.Add('0,0,0,'); //MatOldSum MatNewSum MatRaznica
          quAdd.SQL.Add('0,0,0,'); //RaznOldSum RaznNewSum RaznRaznica
          quAdd.SQL.Add('0,0,0,0,'); //ChekBuh,NDS10OldSum NDS10NewSum NDS10Raznica
          quAdd.SQL.Add('0,0,0,'); //NDS20OldSum NDS20NewSum NDS20Raznica
          quAdd.SQL.Add('0,'); //AcStatus
          quAdd.SQL.Add('0,0,0,'); //NDS0OldSum NDS0NewSum NDS0Raznica
          quAdd.SQL.Add('0,0,0,0,0,0,0,0,0,');//NSP10OldSum  NSP10NewSum NSP10Raznica NSP20OldSum NSP20NewSum NSP20Raznica NSP0OldSum NSP0NewSum NSP0Raznica
          quAdd.SQL.Add(its(IDH)+',0,0,0,0'); //ID LinkInvoice StartTransfer EndTransfer rezerv
          quAdd.SQL.Add(')');

          quAdd.ExecSQL;  //�������� ��������� ������ �� ��� ���-�� 0 �� � ����� ����� = 0
          delay(20);

          n:=1;

          while not teAc.Eof do
          begin
            if teACiDep.AsInteger=iD then  //��������� ������������
            begin
              if taCards.FindKey([teACCode.AsInteger]) then
              begin
                prWH('      -- '+its(teACCode.AsInteger)+' '+OemToAnsiConvert(taCardsName.AsString)+' ������ ����:'+fs(teACOldPrice.AsFloat)+'  ����� ����:'+fs(teACNewPrice.AsFloat),Memo1);

                quAdd.SQL.Clear;
                quAdd.SQL.Add('INSERT into "TTNOvrLn" values (');
                quAdd.SQL.Add(its(iD)); //  Depart
                quAdd.SQL.Add(','''+ds(cxDateEdit1.Date)+''''); //DateAct
                quAdd.SQL.Add(','''+sNum+''''); //  Number
                quAdd.SQL.Add(',0');  // GGroup
                quAdd.SQL.Add(',0');  // Podgr
                quAdd.SQL.Add(','+its(teACCode.AsInteger));  // Tovar
                quAdd.SQL.Add(','+its(taCardsEdIzm.AsInteger));  // EdIzm
                quAdd.SQL.Add(','+its(n));  // TovarType  //����� ������� �� ���������������� �� ������
                quAdd.SQL.Add(','''+taCardsBarCode.AsString+'''');  // BarCode
                quAdd.SQL.Add(',0');  // Kol

                if teACNewPrice.AsFloat>=teACOldPrice.AsFloat then
                begin
                  quAdd.SQL.Add(',0');  // PostCenaOldSum
                  quAdd.SQL.Add(',0');  // PostCenaNewSum
                  quAdd.SQL.Add(',0');  // PostCenaRaznica
                end else
                begin
                  quAdd.SQL.Add(','+fs(teACOldPrice.AsFloat));  // PostCenaOldSum
                  quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat));  // PostCenaNewSum
                  quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat-teACOldPrice.AsFloat));  // PostCenaRaznica
                end;

                quAdd.SQL.Add(','+fs(teACOldPrice.AsFloat));  // MatCenaOldSum
                quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat));  // MatCenaNewSum
                quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat-teACOldPrice.AsFloat));  // MatCenaRaznica

                if teACNewPrice.AsFloat>=teACOldPrice.AsFloat then
                begin
                  quAdd.SQL.Add(','+fs(teACOldPrice.AsFloat));  // RaznCenaOldSum
                  quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat));  // RaznCenaNewSum
                  quAdd.SQL.Add(','+fs(teACNewPrice.AsFloat-teACOldPrice.AsFloat));  // RaznCenaRaznica
                end else
                begin
                  quAdd.SQL.Add(',0');  // RaznCenaOldSum
                  quAdd.SQL.Add(',0');  // RaznCenaNewSum
                  quAdd.SQL.Add(',0');  // RaznCenaRaznica
                end;

                //����� 0 �� ���-�� 0

                quAdd.SQL.Add(',0');  // PostSumOldSum
                quAdd.SQL.Add(',0');  // PostSumNewSum
                quAdd.SQL.Add(',0');  // PostSumRaznica

                quAdd.SQL.Add(',0');  // MatSumOldSum
                quAdd.SQL.Add(',0');  // MatSumNewSum
                quAdd.SQL.Add(',0');  // MatSumRaznica

                quAdd.SQL.Add(',0');  // RaznSumOldSum
                quAdd.SQL.Add(',0');  // RaznSumNewSum
                quAdd.SQL.Add(',0');  // RaznSumRaznica

                quAdd.SQL.Add(')');
                quAdd.ExecSQL;

                inc(n);
              end;

              if teACAPrice.AsFloat=teACNewPrice.AsFloat then //���������� � ����� ������ ���� - �� �����
              begin
                if teACDateB.AsInteger>0 then  //��� ������ ����� - ����� ���������� ������ ����
                begin
                  DateB:=teACDateB.AsInteger;

                  if ptAkciya.FindKey([teACCode.AsInteger,1,DateB]) then
                  begin
                    ptAkciya.Edit;
                    ptAkciyaOldPrice.AsFloat:=teACOldPrice.AsFloat;  //���������� ������ �������� ���� ��� ��������� ��������������
                    ptAkciya.Post;
                  end;
                end;
              end;

              teAC.Delete;
            end else teAC.Next;
          end;

          prWH(' ',Memo1);

          //������������� ��������� � ����� �� ���� �� ���-�� ����� = 0 ���� ����������� �������
          //��������� ������� ���������

          if CommonSet.Test=0 then prAddZadan(1,IdH,'������������� ������ ��� ���������� � '+sNum+'. ��������� ����������� ������� � ����������� ��������. ����������� ����������� ��������.');
        end;

        //������������ ������ ���������
        if CommonSet.Test=0 then
        begin

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "A_AKCIYA" Set ');
        quE.SQL.Add('StatusOn=1');
        quE.SQL.Add('where StatusOn=0 and DateB<='''+ds(iCurD)+'''');
        quE.ExecSQL;
        delay(10);

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "A_AKCIYA" Set ');
        quE.SQL.Add('StatusOf=1');
        quE.SQL.Add('where StatusOf=0 and DateE<='''+ds(iCurD)+'''');
        quE.ExecSQL;
        delay(10);
        
        end;

        ptAkciya.Active:=False;
        ptDStop.Active:=False;
        teAC.Active:=False;
        teAD.Active:=False;
        teACat.Active:=False;
        teNac.Active:=False;
        if ptBufPrice.Active=True then ptBufPrice.Active:=False; 
        prWH('��������� ����.',Memo1);
        PvSQL.Connected:=False;
        prWH('��������� ���������.',Memo1);
      end;
    except
      prWH('������ ��������� ������.',Memo1);
    end;
  end;
end;

procedure TfmManager.cxButton1Click(Sender: TObject);
begin
  acStart.Execute;
end;

end.
