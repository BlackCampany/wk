unit CategSale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, ActnList,
  XPStyleActnCtrls, ActnMan, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls;

type
  TfmCategSale = class(TForm)
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GridCategSale: TcxGrid;
    ViewCategSale: TcxGridDBTableView;
    LevelCategSale: TcxGridLevel;
    am3: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    Timer1: TTimer;
    ViewCategSaleID: TcxGridDBColumn;
    ViewCategSaleNAMECS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCategSale: TfmCategSale;
  bClear3: Boolean;

implementation

uses Un1, dmRnEdit, AddCateg;

{$R *.dfm}

procedure TfmCategSale.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridCategSale.Align:=AlClient;
//  ViewCategSale.RestoreFromIniFile(CurDir+GridIni);
  delay(10);
end;

procedure TfmCategSale.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCategSale.acAddExecute(Sender: TObject);
Var Id:Integer;
begin
  //���������� ���������
  if not CanDo('prAddCategSale') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ��������� ������ ����.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        Id:=1;
        if taCategSale.RecordCount>0 then
        begin
          taCategSale.Last;
          Id:=taCategSaleID.AsInteger+1;
        end;

        taCategSale.Append;
        taCategSaleID.AsInteger:=Id;
        taCategSaleNAMECS.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
        taCategSale.Post;
      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmCategSale.acEditExecute(Sender: TObject);
begin
  //�������������� ���������
  if not CanDo('prEditCategSale') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taCategSale.RecordCount>0 then
    begin
      fmAddCateg:=TFmAddCateg.create(Application);
      fmAddCateg.Caption:='�������������� ��������� ������ ����.';
      fmAddCateg.cxTextEdit1.Text:=taCategSaleNAMECS.AsString;
      fmAddCateg.ShowModal;
      if fmAddCateg.ModalResult=mrOk then
      begin
        try
          taCategSale.Edit;
          taCategSaleNAMECS.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
          taCategSale.Post;
        except
        end;
      end;
      fmAddCateg.Release;
    end;
  end;
end;

procedure TfmCategSale.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;

procedure TfmCategSale.acDelExecute(Sender: TObject);
begin
  //������� ���������
  if not CanDo('prDelCategSale') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taCategSale.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ��������� ������ ���� - '+taCategSaleNAMECS.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          taCategSale.Delete;
        except
        end;
      end;
    end;
  end;
end;

end.
